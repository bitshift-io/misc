#include "../Core/GameBase.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"
#include "../Engine/Font.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "../Engine/Renderer.h"
#include "../Engine/Time.h"
#include "../Engine/Material.h"
#include "../Engine/Window.h"

#include "../Util/Log.h"
#include "../Util/Macros.h"

#include "../Cint/ScriptMgr.h"
#include "../Cint/ScriptClass.h"

#include "CIntInterface.h"
#include "TestClass.h"

using namespace Siphon;


class TestClass1
{
public:
	TestClass1() : scriptClass("TestClass1")
    {
		
    }

	~TestClass1()
	{

	}

    int TestFn()
    {
		return scriptClass.CallIntMethod("TestFn");
    }

protected:
    ScriptClass scriptClass;
};

class TestCINT : public GameBase
{
public:
	TestCINT(int argc, char **argv) : GameBase(argc, argv)
	{
		ScriptMgr::Create();
		ScriptMgr::GetInstance().LoadFile("script.cxx");

		// call a function
		int returnValue = ScriptMgr::GetInstance().CallIntCommand("foo()");
		Log::Print("Foo returned %i.\n", returnValue);

		// now we show 2 different ways of using the script class

		// 1. wrapped
		TestClass1* testClass = new TestClass1();
		int result = testClass->TestFn();

		// 2. using it raw
		ScriptClass* test = new ScriptClass("TestClass1");
		int result2 = test->CallIntMethod("TestFn");
		
		delete testClass;
		delete test;

		ScriptMgr::Destroy();

		Log::Print("Loading complete.\n");
	}

	virtual void Shutdown()
	{

	}

	virtual bool UpdateGame()
	{
		return true;
	}

	virtual void UpdatePhysics()
	{

	}

	virtual void Render()
	{

	}

protected:
	Array<TestClass*> mTestClasses;
};

MAIN(TestCINT);
