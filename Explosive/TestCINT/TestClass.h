#ifndef _TESTCLASS_
#define _TESTCLASS_

class TestClass
{
public:
	virtual int Foo() = 0;
};

#endif