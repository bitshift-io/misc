#include "TestClass.h"

/*
so another idea im having is to make wrapper classes

so if i have the header file in script (TestClass1.cxx):
*/
class TestClass1
{
public:
    static int CreateInstance()
    {
       TestClass1* c = new TestClass1();
	   current = c;
       return 0; //instances.Append(c);
    }

    static void SetIndex(int index)
    {
       current = current; //instances[index];
    }

    int TestFn()
	{
		return 4;
	}

	static TestClass1* current;

protected:	
	//static TestClass1* instances[4];
	//static int count;
};

TestClass1* TestClass1::current = NULL;

/*
then in my exe make a wrapper:

class TestClass1
{
public
    TestClass1()
    {
       G__loadfile("script.cxx");
   
       // get some value from the script file
       index = G_Calc("TestFn.CreateInstance()");
    }

    void TestFn()
    {
       G_Calc("TestFn.SetIndex(//index here//)");
       G_Calc("current.TestFn()");
    }

    int index;
}*/


int foo()
{
	return 5;
}
