/********************************************************************
* CIntInterface.h
********************************************************************/
#ifdef __CINT__
#error CIntInterface.h/C is only for compilation. Abort cint.
#endif
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define G__ANSIHEADER
#define G__DICTIONARY
#include "G__ci.h"
extern "C" {
extern void G__cpp_setup_tagtableScriptAPI();
extern void G__cpp_setup_inheritanceScriptAPI();
extern void G__cpp_setup_typetableScriptAPI();
extern void G__cpp_setup_memvarScriptAPI();
extern void G__cpp_setup_globalScriptAPI();
extern void G__cpp_setup_memfuncScriptAPI();
extern void G__cpp_setup_funcScriptAPI();
extern void G__set_cpp_environmentScriptAPI();
}


#include "TestClass.h"

#ifndef G__MEMFUNCBODY
#endif

extern G__linked_taginfo G__ScriptAPILN_TestClass;

/* STUB derived class for protected member access */
