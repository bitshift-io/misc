/********************************************************
* CIntInterface.cpp
********************************************************/
#include "CIntInterface.h"

#ifdef G__MEMTEST
#undef malloc
#undef free
#endif

extern "C" void G__cpp_reset_tagtableScriptAPI();

extern "C" void G__set_cpp_environmentScriptAPI() {
  G__add_ipath("..\\Engine");
  G__add_ipath("..\\Util");
  G__add_ipath("..\\Core");
  G__add_compiledheader("TestClass.h");
  G__cpp_reset_tagtableScriptAPI();
}
class G__CIntInterfacedOcpp_tag {};

void* operator new(size_t size,G__CIntInterfacedOcpp_tag* p) {
  if(p && G__PVOID!=G__getgvp()) return((void*)p);
#ifndef G__ROOT
  return(malloc(size));
#else
  return(::operator new(size));
#endif
}

/* dummy, for exception */
#ifdef G__EH_DUMMY_DELETE
void operator delete(void *p,G__CIntInterfacedOcpp_tag* x) {
  if((long)p==G__getgvp() && G__PVOID!=G__getgvp()) return;
#ifndef G__ROOT
  free(p);
#else
  ::operator delete(p);
#endif
}
#endif

static void G__operator_delete(void *p) {
  if((long)p==G__getgvp() && G__PVOID!=G__getgvp()) return;
#ifndef G__ROOT
  free(p);
#else
  ::operator delete(p);
#endif
}

void G__DELDMY_CIntInterfacedOcpp() { G__operator_delete(0); }

extern "C" int G__cpp_dllrevScriptAPI() { return(30051515); }

/*********************************************************
* Member function Interface Method
*********************************************************/

/* TestClass */
static int G__ScriptAPI_0_0_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
      G__letint(result7,105,(long)((TestClass*)(G__getstructoffset()))->Foo());
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic destructor
typedef TestClass G__TTestClass;
static int G__ScriptAPI_0_1_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   if(0==G__getstructoffset()) return(1);
   if(G__getaryconstruct())
     if(G__PVOID==G__getgvp())
       delete[] (TestClass *)(G__getstructoffset());
     else
       for(int i=G__getaryconstruct()-1;i>=0;i--)
         ((TestClass *)((G__getstructoffset())+sizeof(TestClass)*i))->~G__TTestClass();
   else {
     long G__Xtmp=G__getgvp();
     G__setgvp(G__PVOID);
     ((TestClass *)(G__getstructoffset()))->~G__TTestClass();
     G__setgvp(G__Xtmp);
     G__operator_delete((void*)G__getstructoffset());
   }
      G__setnull(result7);
   return(1 || funcname || hash || result7 || libp) ;
}

// automatic assignment operator
static int G__ScriptAPI_0_2_0(G__value *result7,G__CONST char *funcname,struct G__param *libp,int hash) {
   TestClass *dest = (TestClass*)(G__getstructoffset());
   *dest = (*(TestClass*)libp->para[0].ref);
   const TestClass& obj = *dest;
   result7->ref=(long)(&obj); result7->obj.i=(long)(&obj);
   return(1 || funcname || hash || result7 || libp) ;
}


/* Setting up global function */

/*********************************************************
* Member function Stub
*********************************************************/

/* TestClass */

/*********************************************************
* Global function Stub
*********************************************************/

/*********************************************************
* Get size of pointer to member function
*********************************************************/
class G__Sizep2memfuncScriptAPI {
 public:
  G__Sizep2memfuncScriptAPI() {p=&G__Sizep2memfuncScriptAPI::sizep2memfunc;}
    size_t sizep2memfunc() { return(sizeof(p)); }
  private:
    size_t (G__Sizep2memfuncScriptAPI::*p)();
};

size_t G__get_sizep2memfuncScriptAPI()
{
  G__Sizep2memfuncScriptAPI a;
  G__setsizep2memfunc((int)a.sizep2memfunc());
  return((size_t)a.sizep2memfunc());
}


/*********************************************************
* virtual base class offset calculation interface
*********************************************************/

   /* Setting up class inheritance */

/*********************************************************
* Inheritance information setup/
*********************************************************/
extern "C" void G__cpp_setup_inheritanceScriptAPI() {

   /* Setting up class inheritance */
}

/*********************************************************
* typedef information setup/
*********************************************************/
extern "C" void G__cpp_setup_typetableScriptAPI() {

   /* Setting up typedef entry */
}

/*********************************************************
* Data Member information setup/
*********************************************************/

   /* Setting up class,struct,union tag member variable */

   /* TestClass */
static void G__setup_memvarTestClass(void) {
   G__tag_memvar_setup(G__get_linked_tagnum(&G__ScriptAPILN_TestClass));
   { TestClass *p; p=(TestClass*)0x1000; if (p) { }
   }
   G__tag_memvar_reset();
}

extern "C" void G__cpp_setup_memvarScriptAPI() {
}
/***********************************************************
************************************************************
************************************************************
************************************************************
************************************************************
************************************************************
************************************************************
***********************************************************/

/*********************************************************
* Member function information setup for each class
*********************************************************/
static void G__setup_memfuncTestClass(void) {
   /* TestClass */
   G__tag_memfunc_setup(G__get_linked_tagnum(&G__ScriptAPILN_TestClass));
   G__memfunc_setup("Foo",292,G__ScriptAPI_0_0_0,105,-1,-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,3);
   // automatic destructor
   G__memfunc_setup("~TestClass",1044,G__ScriptAPI_0_1_0,(int)('y'),-1,-1,0,0,1,1,0,"",(char*)NULL,(void*)NULL,0);
   // automatic assignment operator
   G__memfunc_setup("operator=",937,G__ScriptAPI_0_2_0,(int)('u'),G__get_linked_tagnum(&G__ScriptAPILN_TestClass),-1,1,1,1,1,0,"u 'TestClass' - 11 - -",(char*)NULL,(void*)NULL,0);
   G__tag_memfunc_reset();
}


/*********************************************************
* Member function information setup
*********************************************************/
extern "C" void G__cpp_setup_memfuncScriptAPI() {
}

/*********************************************************
* Global variable information setup for each class
*********************************************************/
static void G__cpp_setup_global0() {

   /* Setting up global variables */
   G__resetplocal();

   G__memvar_setup((void*)G__PVOID,112,0,0,-1,-1,-1,1,"_TESTCLASS_=0",1,(char*)NULL);

   G__resetglobalenv();
}
extern "C" void G__cpp_setup_globalScriptAPI() {
  G__cpp_setup_global0();
}

/*********************************************************
* Global function information setup for each class
*********************************************************/
static void G__cpp_setup_func0() {
   G__lastifuncposition();


   G__resetifuncposition();
}

extern "C" void G__cpp_setup_funcScriptAPI() {
  G__cpp_setup_func0();
}

/*********************************************************
* Class,struct,union,enum tag information setup
*********************************************************/
/* Setup class/struct taginfo */
G__linked_taginfo G__ScriptAPILN_TestClass = { "TestClass" , 99 , -1 };

/* Reset class/struct taginfo */
extern "C" void G__cpp_reset_tagtableScriptAPI() {
  G__ScriptAPILN_TestClass.tagnum = -1 ;
}


extern "C" void G__cpp_setup_tagtableScriptAPI() {

   /* Setting up class,struct,union tag entry */
   G__tagtable_setup(G__get_linked_tagnum(&G__ScriptAPILN_TestClass),sizeof(TestClass),-1,1,(char*)NULL,G__setup_memvarTestClass,G__setup_memfuncTestClass);
}
extern "C" void G__cpp_setupScriptAPI(void) {
  G__check_setup_version(30051515,"G__cpp_setupScriptAPI()");
  G__set_cpp_environmentScriptAPI();
  G__cpp_setup_tagtableScriptAPI();

  G__cpp_setup_inheritanceScriptAPI();

  G__cpp_setup_typetableScriptAPI();

  G__cpp_setup_memvarScriptAPI();

  G__cpp_setup_memfuncScriptAPI();
  G__cpp_setup_globalScriptAPI();
  G__cpp_setup_funcScriptAPI();

   if(0==G__getsizep2memfunc()) G__get_sizep2memfuncScriptAPI();
  return;
}
