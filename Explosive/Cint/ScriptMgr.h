#ifndef _SIPHON_SCRIPTMGR_
#define _SIPHON_SCRIPTMGR_

#pragma comment(lib, "libcint.lib");

#include "../Util/Singleton.h"

#include "G__ci.h"

namespace Siphon 
{

class ScriptMgr : public Singleton<ScriptMgr>
{
public:

	ScriptMgr()
    {
		int value = G__init_cint("cint"); 

		if (value == G__INIT_CINT_SUCCESS)
			Log::Print("CINT initialised.\n");
		else if (value == G__INIT_CINT_SUCCESS_MAIN)
			Log::Print("CINT initialised, main found.\n");
		else if (value == G__INIT_CINT_FAILURE)
			Log::Print("CINT init failed.\n");	
    }

	~ScriptMgr()
    {
		G__scratch_all();	
    }

	int CallIntCommand(const char* command)
	{
		return G__int(G__calc(command));
	}

	bool LoadFile(const char* file)
	{
		int value = G__loadfile(file);

		if (value == G__INIT_CINT_FAILURE)
			return false;

		return true;
	}

	bool UnloadFile(const char* file)
	{
		G__unloadfile(file);
		return true;
	}

protected:

};

}

#endif