#ifndef _SIPHON_SCRIPTCLASS_
#define _SIPHON_SCRIPTCLASS_

namespace Siphon 
{

class ScriptClass
{
public:

	ScriptClass(const char* className)
    {
		char buffer[256];
		this->className = className;
		sprintf(buffer, "new %s()", className);
		pClass = G__int(G__calc(buffer));
    }

	~ScriptClass()
	{
		char buffer[256];
		sprintf(buffer, "delete (%s*)%d", className.c_str(), pClass);
		G__calc(buffer);
	}

	int CallIntMethod(const char* name)
	{
		return G__int(CallMethod(name));
	}

	G__value CallMethod(const char* name)
	{
		char buffer[256];
		sprintf(buffer, "((%s*)%d)->%s()", className.c_str(), pClass, name);
		return G__calc(buffer);
	}

protected:
	int			pClass; // internal cint pointer
	std::string className;
};

}

#endif