//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4 	worldIT 		: WorldInverseTranspose;
float4x4 	worldViewProj 	: WorldViewProjection;
float4x4 	worldView 		: WorldView;
float4x4 	worldInv 		: WorldInverse;

float3   	lightPos 		: LightPosition;
float3   	eyePos 		: EyePosition;

sampler2D bumpTexture = sampler_state
{
	minFilter = LinearMipMapLinear;
	magFilter = Linear;
};

sampler2D diffuseTexture = sampler_state
{
	minFilter = LinearMipMapLinear;
	magFilter = Linear;
};

sampler2D parallaxTexture = sampler_state
{
	minFilter = LinearMipMapLinear;
	magFilter = Linear;
//	WrapS = ClampToEdge;
//	WrapT = ClampToEdge;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
	float3 position 	: POSITION;
	float3 normal 	: NORMAL;
	float3 tangent 	: TEXCOORD0;
	float3 binormal 	: TEXCOORD1;
	float2 uvCoord	: TEXCOORD2;	
};

struct VS_OUTPUT
{
	float4 position 	: POSITION;
	float2 uvCoord	: TEXCOORD0;	
	float3 lightPos 	: TEXCOORD1;
	float3 eyeVec 	: TEXCOORD2;
};

struct PS_OUTPUT
{
	float4 colour	: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform float4x4 worldView,
			uniform float4x4 worldInv,
			uniform float3   lightPos,
			uniform float3   eyePos )
{
    	VS_OUTPUT OUT;
   	OUT.position = mul( modelViewProjection, float4(IN.position, 1.0) );
	OUT.uvCoord = IN.uvCoord;

	// convert from object space to texture(tangent) space...
	float3x3 texSpaceTrans = float3x3(IN.tangent,
					IN.binormal,
					IN.normal);
					
	float3 lightModelPos = mul( worldInv, float4(lightPos, 1.0) ).xyz;
	lightModelPos = lightModelPos - IN.position;
	OUT.lightPos = mul( texSpaceTrans, lightModelPos );
	
	float3 eyeModelPos = mul( worldInv, float4(eyePos, 1.0) ).xyz;
	eyeModelPos = eyeModelPos - IN.position;
	OUT.eyeVec = mul( texSpaceTrans, eyeModelPos );
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

void TraceFoward(inout float3 increment, uniform sampler2D height, inout float2 uvCoord, inout float desiredDepth)
{
	float curDepth;
	
	while (desiredDepth < 1.0)
	{
		curDepth = 1 - tex2D(height, uvCoord).a;
		
		if (curDepth <= desiredDepth) // we have gone to far
			break;
		
		desiredDepth += increment.z;
		uvCoord += increment.xy;	
	}
}

void TraceBackward(inout float3 increment, uniform sampler2D height, inout float2 uvCoord, inout float desiredDepth)
{
	float curDepth;
	
	while (desiredDepth > 0.0)
	{
		curDepth = 1 - tex2D(height, uvCoord).a; 
		
		if (curDepth > desiredDepth) // we have gone to far
			break;
		
		desiredDepth -= increment.z;
		uvCoord -= increment.xy;	
	}
}

float LightTraceFoward(float3 increment, uniform sampler2D height, float2 uvCoord, float lightRayDepth)
{
	float returnValue = 1.0;
	float curDepth;

	while (lightRayDepth > 0.0)
	{
		curDepth = 1 - tex2D(height, uvCoord).a;
		
		if (curDepth < lightRayDepth) // we intersect the heightmap
		{
			returnValue = 0.4f;
			break;
		}
		
		lightRayDepth += increment.z;	
		uvCoord += increment.xy;	
	}

	
	return returnValue;
}

float2 GetReliefOffset(uniform sampler2D height, float2 uvCoord, float3 eyeVec)
{
	float desiredDepth = 0;	
	float2 uv = uvCoord;
	float3 increment = (-eyeVec * 0.005);
	
	// adjust camera depth, i higher multipleyer means less extrusion
	increment.z = increment.z * 20.0f;
	
	for (int i = 0; i < 2; ++i)
	{
		TraceFoward(increment, height, uv, desiredDepth);
		increment = (increment / 2.0);
		TraceBackward(increment, height, uv, desiredDepth);
		increment = (increment / 2.0);
	}		
		
	return uv; 
}

float2 GetReliefOffsetSelfShadow(inout PS_OUTPUT IN, uniform sampler2D height, float2 uvCoord, float3 eyeVec, float3 lightPos, out float light)
{
	float desiredDepth = 0;	
	float2 uv = uvCoord;
	float3 increment = (-eyeVec * 0.005);
	
	// adjust camera depth, i higher multipleyer means less extrusion
	increment.z = increment.z * 20.0f;
	
	for (int i = 0; i < 2; ++i)
	{
		TraceFoward(increment, height, uv, desiredDepth);
		increment = (increment / 2.0);
		TraceBackward(increment, height, uv, desiredDepth);
		increment = (increment / 2.0);
	}
	
	// ideally we should create new light vector and stuff
	// here acording to the cast rays, but this will do for the time being
	
	//float3 pixelPosition = float3(uv - uvCoord, desiredDepth);
	//float3 lightVec = lightPos - pixelPosition;
	//float3 lightVecNorm = normalize(lightVec);

	increment = (normalize(lightPos) * 0.005);
	increment.z = increment.z * 20.0f;

	light = LightTraceFoward(increment, height, uv, desiredDepth);
	
	//IN.colour = float4(light, light, light, 0); 
	return uv; 
}


PS_OUTPUT myps(VS_OUTPUT IN, uniform sampler2D bump, uniform sampler2D diffuse, uniform sampler2D height)
{
	PS_OUTPUT OUT;

	float3 eyeVec = normalize( IN.eyeVec );
	float3 lightVec = normalize(IN.lightPos);
	
	float light = 1.0;
	float2 uvCoord = GetReliefOffsetSelfShadow(OUT, height, IN.uvCoord, eyeVec, IN.lightPos, light);
	
	//float2 uvCoord = GetReliefOffset(height, IN.uvCoord, eyeVec);

	// in texture(tangent) space....
	float3 bumpTexNormal = tex2D( bump, uvCoord ).rgb;
	bumpTexNormal.x = 1.0 - bumpTexNormal.x; // invert r to get Nvidia style normal mapping
	bumpTexNormal = (2 * bumpTexNormal) - 1.0;

	//float4 final = light * /*max( dot(bumpTexNormal, lightVec), 0.0) **/ tex2D(diffuse, uvCoord);
	OUT.colour = light * /*max( dot(bumpTexNormal, lightVec), 0.0) **/ tex2D(diffuse, uvCoord);
	
	//OUT.colour = OUT.colour + final * 0.0;
	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		//Zenable  = true;
		//CullMode = CCW;
		
		VertexProgram = compile vp40 myvs(worldViewProj, worldView, worldInv, lightPos, eyePos);
		FragmentProgram  = compile fp40 myps(bumpTexture, diffuseTexture, parallaxTexture);
    }
}
