//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4 worldViewProj : WorldViewProjection;

texture 	diffuseTexture;

sampler2D DiffuseSampler = sampler_state
{
    Texture = <diffuseTexture>;
    MinFilter = Anisotropic;
    MagFilter = Anisotropic;
    MipFilter = Anisotropic;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
    float3 position 	: POSITION;
	float2 uvCoord		: TEXCOORD0;	
};

struct VS_OUTPUT
{
	float4 position : POSITION;
	float2 uvCoord	: TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 colour	: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection)
{
    VS_OUTPUT OUT;


   	OUT.position = mul( modelViewProjection, float4(IN.position, 1.0) );
	OUT.uvCoord = IN.uvCoord;
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

float2 GetParallaxOffset(uniform sampler2D height, float2 uvCoord, float3 eyeVec)
{	
	float depth = pow(tex2D( height, uvCoord ).a / 6.0, 2.0);
	eyeVec = eyeVec * depth;
	return eyeVec.xy;
}

PS_OUTPUT myps( VS_OUTPUT IN, uniform sampler2D diffuse)
{
	PS_OUTPUT OUT;

	OUT.colour = tex2D(diffuse, IN.uvCoord );

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		Zenable  = true;
		Zwriteenable = true;
		CullMode = None; //CCW;
		
		VertexShader = compile arbvp1 myvs( worldViewProj );
        PixelShader  = compile arbfp1 myps(DiffuseSampler);
    }
}
