//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4 worldViewProj : WorldViewProjection;
float3   	eyePos 			: EyePosition;


sampler2D diffuseTexture< 
    string image = "checkerboard";
    int frequency = 15;
    float3 color0 = float3(1,1,0);
    float3 color1 = float3(0,0,1);
> = sampler_state {
        generateMipMap = true;
        minFilter = LinearMipMapLinear;
        magFilter = Linear;
        WrapS = ClampToEdge;
        WrapT = ClampToEdge;
        MaxAnisotropy = 8;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
    float3 position 	: POSITION;
	float2 uvCoord		: TEXCOORD0;	
};

struct VS_OUTPUT
{
	float4 position : POSITION;
	float2 uvCoord	: TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 colour	: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection, uniform float3 eyePos)
{
    VS_OUTPUT OUT;


   	OUT.position = mul( modelViewProjection, float4(IN.position + eyePos, 1.0) );
	OUT.uvCoord = IN.uvCoord;
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN, uniform sampler2D diffuse)
{
	PS_OUTPUT OUT;

	OUT.colour = tex2D(diffuse, IN.uvCoord );
	//OUT.colour = float4(1,0,0,0);

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		//Zenable  = false;

		//Zenable  = false;
		//Zwriteenable = false;
		
		VertexProgram = compile arbvp1 myvs( worldViewProj, eyePos );
        FragmentProgram = compile arbfp1 myps(diffuseTexture);
    }
}
