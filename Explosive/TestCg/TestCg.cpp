#include "../Core/GameBase.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"
#include "../Engine/Font.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "../Engine/Renderer.h"
#include "../Engine/Time.h"
#include "../Engine/Material.h"
#include "../Engine/Window.h"

#include "../Util/Log.h"
#include "../Util/Macros.h"

using namespace Siphon;

class TestCg : public GameBase<TestCg>
{
public:
	TestCg() : 	
		angle(0.0f),
		lightAngle(0.0f)
	{

	}

	virtual bool Init(int argc, char **argv)	
	{
		GameBase::Init(argc, argv);

		char basePath[256] = "data/";
		if (argc >= 2)
		{
			sprintf(basePath, "%s/", argv[1]); 
			SetBasePath(basePath);	
			Mesh::SetBasePath(basePath);
		}

		Device::GetInstance().CreateDevice();

		window = new Window();
		window->Create("TestCg", 800, 600, 16, false);
		window->ShowWindow();
		Device::GetInstance().SetActiveWindow(window);
		Device::GetInstance().SetClearColour(0.5, 0.5, 0.5);

		Input::GetInstance().CreateInput(window);
		
		camera = new Camera();
		camera->SetLookAt( Vector3(0, 0.1, 0.3), Vector3(0, 0, 0) );
		camera->SetSpeed(0.005f);
		camera->SetPerspective(70, 1.33, 100.0f, 0.0001f);

		renderMgr.SetCamera(camera);
		renderMgr.InsertLight(&light);
/*
		cubeCamera = new Camera();
		cubeCamera->SetPerspective(60.0f, 1.33); 
		cubeCamera->SetFreeCam(false);
		cubeCamera->SetLookAt( Vector3(0, 0, 0.2), Vector3(0, 0, 0) );

		// lets try loading a HDR image
		//hdrTex = TextureMgr::LoadTex("sample.hdr");
		//testTex = TextureMgr::LoadTex("font.dds");

		sprintf(basePath, "%s/light.MDL", argv[1]); 
		lightMesh = Mesh::LoadMesh(basePath);
		lightRender.InsertMesh(lightMesh);
		cubeRenderTarget.Create(512, 512, RenderTexture::CubeMap);
		cubeRenderTarget.SetClearColour(1, 0, 0);

		lightRender.SetRenderTarget(&cubeRenderTarget);
		lightRender.SetCamera(cubeCamera);

		// testing
		//renderMgr.InsertMesh(lightMesh);
*/
//		skybox = Mesh::LoadMesh("skybox.MDL");
//		renderMgr.InsertMesh(skybox);

		scene = Mesh::LoadMesh("scene.MDL");
		renderMgr.InsertMesh(scene);
/*
		// assign render target texture to materials
		for (int i = 0; i < scene->GetMeshMat().material.GetSize(); ++i)
		{
			Material* mat = scene->GetMeshMat().material[i].material;

			Material::Param* param = mat->GetParameterByName("specularTexture");
			if (!param || param->type != Material::Param::ParamTexture)
				continue;

			Material::TextureParam* texParam = (Material::TextureParam*)param;

			if (!texParam->texture)
				texParam->texture = &cubeRenderTarget;
		}*/

		font = new SFont;
		font->CreateFont("font.dds");

		Device::GetInstance().CheckErrors();
		Log::Print("Loading complete.\n");
		return true;
	}

	virtual void Shutdown()
	{
		SAFE_DELETE(font);
		SAFE_DELETE(camera);
		SAFE_DELETE(window);
	}

	virtual bool UpdateGame()
	{
		camera->Update();
//		cubeCamera->Update();

		renderMgr.SetTime(Time::GetInstance().GetTimeSeconds());

		Vector3 pos = light.GetPosition();

		// update light
		angle += 1.0f;
		pos[Vector3::X] = cos(Math::DtoR(angle)) / 10.0f;
		pos[Vector3::Y] = sin(Math::DtoR(angle)) / 10.0f + 0.11f;
		//pos[Vector3::Z] = 0.2f + (sin(Math::DtoR(angle)) / 10.0f);
		pos[Vector3::Z] = -0.05f;

		light.SetPosition(pos);
/*
		// animate light mesh
		Matrix4 trans(Matrix4::Identity);
		trans.RotateZ(lightAngle);
		lightMesh->SetTransform(trans);
*/
		lightAngle += 0.05f;
		
		if (window->HandleMessages() || Input::GetInstance().KeyPressed(DIK_ESCAPE))
			return false;

		return true;
	}

	virtual void UpdatePhysics()
	{

	}

	void DrawDebug()
	{
		Matrix4 identity(Matrix4::Identity);
		identity.Draw();

		const Sphere& sphere = scene->GetBoundSphere();
		sphere.Draw();
	}

	virtual void Render()
	{
//		lightRender.Render();

		Device::GetInstance().BeginRender();


		renderMgr.Render();

		Material::Disable();

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		//scene->GetMeshMat().vertexBuffer->DebugRender();

		camera->LookAt();
		light.Draw(0.02f);	

		//camera->LookAt();
		//scene->GetTransform().Draw();
		
		unsigned int fps = Device::GetInstance().GetFPS();
		font->Update();
		font->Print(0.1f, 0.1f, "FPS: %i", fps);
		font->Render();

	
		//hdrTex->Draw();
		//testTex->Draw();

		//cubeRenderTarget.Draw();
		Device::GetInstance().EndRender();
	}

protected:
	Window*			window;
	SFont*			font;
	Camera*			camera;
	Camera*			cubeCamera;
	Mesh*			scene;
	Mesh*			lightMesh;
	Mesh*			skybox;
	float			angle;
	Light			light;

	float			lightAngle;

	Renderer		renderMgr;
	Renderer		lightRender; // renders emissive objects to cube map for specular
	RenderTexture	cubeRenderTarget;

	//Texture*		hdrTex;
	//Texture*		testTex;
};

MAIN(TestCg)
