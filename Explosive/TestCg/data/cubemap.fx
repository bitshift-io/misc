//-----------------------------------------------------------------------------
//           Name: ogl_cgfx_simple.fx
//         Author: Kevin Harris (kevin@codesampler.com)
//  Last Modified: 04/15/05
//    Description: This .fx file demonstrates how to write vertex and pixel
//                 shaders using nVIDIA's Cg shader language.
//-----------------------------------------------------------------------------


float4x4 worldViewProj : WorldViewProjection;
float4x4 worldInv : WorldInverse;
float4x4 world : World;
float4x4 worldIT : WorldInverseTranspose;
float3 eyePos : EyePosition;

texture testTexture
<
    string ResourceType = "Cube";
>;

samplerCUBE Sampler = sampler_state
{
    	Texture = <testTexture>;
    	MinFilter = Linear;
    	MagFilter = Linear;
    	MipFilter = Linear;
};

struct VS_INPUT
{
    float3 position 	: POSITION;
	float3 normal 		: NORMAL;
    float2 texture0 	: TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 position 	: POSITION;
    float2 texture0 	: TEXCOORD0;
    float4 color		: COLOR0;
    float3 reflect		: TEXCOORD1;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs(const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform float4x4 worldInv,
			uniform float4x4 worldIT,
			uniform float4x4 world,
			uniform float3 eyePos )
{
    VS_OUTPUT OUT;
    OUT.position = mul(modelViewProjection, float4(IN.position, 1.0));
	
	// convert normal to word space
	float3 norm = normalize( mul((float3x3)worldIT, IN.normal) );
	
	// convert position to word space
	float4 pos = mul(float4(IN.position, 1.0), world);
	pos /= pos.w;
	
	// calculate incident ray
	float3 incident = pos.xyz - eyePos;
	
	// calcualte reflection angle
	OUT.reflect = reflect(incident, norm);
	
    OUT.texture0 = IN.texture0; 
	OUT.color = float4(0.5,0.5,0.5,1);

	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN )
{
	PS_OUTPUT OUT;

	//OUT.color = tex2D( Sampler, IN.texture0 ) * IN.color;
	OUT.color = texCUBE( Sampler, normalize(IN.reflect) ) * IN.color;
	//OUT.color = float4(normalize(IN.reflect), 1.0);

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		Zenable  = true;
		CullMode = CCW;
		
		VertexShader = compile arbvp1 myvs( worldViewProj, worldInv, worldIT, world, eyePos);
       	PixelShader  = compile arbfp1 myps();
    }
}
