//-----------------------------------------------------------------------------
//           Name: ogl_cgfx_simple.fx
//         Author: Kevin Harris (kevin@codesampler.com)
//  Last Modified: 04/15/05
//    Description: This .fx file demonstrates how to write vertex and pixel
//                 shaders using nVIDIA's Cg shader language.
//-----------------------------------------------------------------------------


float4x4 worldViewProj : WorldViewProjection;
float4x4 worldInv : WorldInverse;
float3 eyeDir : EyeDirection;
texture testTexture;

float4 secondaryCol;
float4 primaryCol;

sampler2D Sampler = sampler_state
{
    	Texture = <testTexture>;
    	MinFilter = Linear;
    	MagFilter = Linear;
    	MipFilter = Linear;
};

struct VS_INPUT
{
    float3 position 	: POSITION;
	float3 normal 		: NORMAL;
    float2 texture0 	: TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 position 	: POSITION;
    float2 texture0 	: TEXCOORD0;
    float4 color		: COLOR0;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs(const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform float4x4 worldInv,
			uniform float3 eyeDir,
			uniform float4 primary,
			uniform float4 secondary )
{
    VS_OUTPUT OUT;
    OUT.position = mul(modelViewProjection, float4(IN.position, 1.0));

	float4 n = float4(IN.normal, 1.0); //normal vector
	
	// convert eye to object space
	float4 objEye = normalize( mul(float4(eyeDir, 1.0), worldInv) );
	
    OUT.texture0 = IN.texture0; 

	float d = min( pow( max(dot(objEye, n), 0.0), 4.0), 1.0); 
	float p = 1.0 - d;
	
	OUT.color = (primary * d + secondary * p);

	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN )
{
	PS_OUTPUT OUT;

	OUT.color = tex2D( Sampler, IN.texture0 ) * IN.color;
	//OUT.color = IN.color;

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		Zenable  = true;
		CullMode = CCW;
		
		VertexShader = compile arbvp1 myvs( worldViewProj, worldInv, eyeDir, primaryCol, secondaryCol );
       	PixelShader  = compile arbfp1 myps();
    }
}
