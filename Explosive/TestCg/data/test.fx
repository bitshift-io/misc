
float4x4 worldViewProj : WorldViewProjection;
float4x4 worldInv : WorldInverse;
float4x4 world : World;
float4x4 worldIT : WorldInverseTranspose;
float3 eyePos : EyePosition;

samplerCUBE testTexture = sampler_state
{
	minFilter = LinearMipMapLinear;
	magFilter = Linear;
};

struct VS_INPUT
{
    float3 position 	: POSITION;
	float3 normal 		: NORMAL;
    float2 texture0 	: TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 position 	: POSITION;
    float2 texture0 	: TEXCOORD0;
    float4 color		: COLOR0;
    float3 reflect		: TEXCOORD1;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};

float3 CubeReflect(float3 position, float3 eyePos, float3 normal, float4x4 world, float4x4 worldIT)
{
	// convert normal to word space
	float3 norm = normalize( mul((float3x3)worldIT, normal) );
	
	// convert position to word space
	float4 pos = mul(float4(position, 1.0), world);
	pos /= pos.w;
	
	// calculate incident ray
	float3 incident = pos.xyz - eyePos;
	
	// calcualte reflection angle
	return reflect(incident, norm);
}

float3 CubeRefract(float3 position, float3 eyePos, float3 normal, float4x4 world, float4x4 worldIT)
{
	// convert normal to word space
	float3 norm = normalize( mul((float3x3)worldIT, normal) );
	
	// convert position to word space
	float4 pos = mul(float4(position, 1.0), world);
	pos /= pos.w;
	
	// calculate incident ray
	float3 incident = pos.xyz - eyePos;
	
	// calcualte reflection angle
	return refract(incident, norm, 0.5);
}

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs(const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform float4x4 worldInv,
			uniform float4x4 worldIT,
			uniform float4x4 world,
			uniform float3 eyePos )
{
    VS_OUTPUT OUT;
    OUT.position = mul(modelViewProjection, float4(IN.position, 1.0));
	
	OUT.reflect = CubeReflect(IN.position, eyePos, IN.normal, world, worldIT);
	
    OUT.texture0 = IN.texture0; 
	OUT.color = float4(0.5,0.5,0.5,1);

	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN )
{
	PS_OUTPUT OUT;

	OUT.color = texCUBE( Sampler, normalize(IN.reflect) ) * IN.color;

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		Zenable  = true;
		CullMode = CCW;
		
		VertexShader = compile arbvp1 myvs( worldViewProj, worldInv, worldIT, world, eyePos);
       	PixelShader  = compile arbfp1 myps();
    }
}
