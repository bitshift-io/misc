//-----------------------------------------------------------------------------
//           Name: ogl_cgfx_simple.fx
//         Author: Kevin Harris (kevin@codesampler.com)
//  Last Modified: 04/15/05
//    Description: This .fx file demonstrates how to write vertex and pixel
//                 shaders using nVIDIA's Cg shader language.
//-----------------------------------------------------------------------------

float4x4 worldIT : WorldInverseTranspose;
float4x4 worldViewProj : WorldViewProjection;
float4x4 worldView : WorldView;
float4x4 worldInv : WorldInverse;

float3   lightPos : LightPosition;

sampler2D testTexture = sampler_state
{
	minFilter = LinearMipMapLinear;
	magFilter = Linear;
};

struct VS_INPUT
{
    	float3 position 	: POSITION;
	float3 normal 	: NORMAL;
    	float2 texture0 	: TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 position 	: POSITION;
    	float2 texture0 	: TEXCOORD0;
    	float4 color	: COLOR0;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform float4x4 worldView,
			uniform float4x4 worldInv,
			uniform float3   lightPos )
{
    VS_OUTPUT OUT;
    OUT.position = mul( modelViewProjection, float4(IN.position, 1.0) );

    OUT.texture0 = IN.texture0; 

	// convert to objectspace
	float3 lightModelPos = mul( worldInv, float4(lightPos, 1.0) ).xyz;
	lightModelPos = lightModelPos - IN.position;
	OUT.color = max( dot(lightModelPos , IN.normal), 0.0 );

	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN )
{
	PS_OUTPUT OUT;

	//OUT.color = tex2D( testTexture, IN.texture0 ) * IN.color;
	OUT.color = IN.color;

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		Zenable  = true;
		CullMode = None;
		
		VertexShader = compile arbvp1 myvs( worldViewProj, worldView, worldInv, lightPos );
        	PixelShader  = compile arbfp1 myps();
    	}
}
