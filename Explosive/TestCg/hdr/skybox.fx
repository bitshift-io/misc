//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4 worldViewProj : WorldViewProjection;
float3   	eyePos 			: EyePosition;

samplerRECT diffuseTexture = sampler_state
{
	minFilter = NearestMipMapNearest;
	magFilter = Nearest;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
    float3 position 	: POSITION;
	float2 uvCoord		: TEXCOORD0;	
};

struct VS_OUTPUT
{
	float4 position : POSITION;
	float2 uvCoord	: TEXCOORD0;
};

struct PS_OUTPUT
{
	half4 colour	: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection, uniform float3 eyePos)
{
    VS_OUTPUT OUT;


   	OUT.position = mul( modelViewProjection, float4(IN.position + eyePos, 1.0) );
	OUT.uvCoord = IN.uvCoord;
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

// Lookup in RGBE-encoded rectangle texture
float3 tex2D_RGBE(uniform samplerRECT tex, float2 t)
{
   float4 rgbe = texRECT(tex, t * 512.0f);
   float e = (rgbe[3] * 255) - 128;
   return rgbe.xyz * exp2(e);
}
/*
// Lookup in RGBE-encoded cube map texture
float3 texCUBE_RGBE(uniform samplerCUBE tex, float3 t)
{
   float4 rgbe = texCUBE(tex, t);
   float e = (rgbe[3] * 255) - 128;
   return rgbe.xyz * exp2(e);
}

// Lookup in RGBE-encoded rectangle texture with filtering
float3 texRECT_RGBE_Bilinear(uniform samplerRECT tex, half2 t)
{
  float2 f = frac(t);
  vec3 t0 = texRECT_RGBE(tex, t);
  vec3 t1 = texRECT_RGBE(tex, t + half2(1,0) );
  vec3 t2  = lerp(t0, t1, f[0]);
  t0 = texRECT_RGBE(tex, t + half2(0,1) );
  t1 = texRECT_RGBE(tex, t + half2(1,1) );
  t0 = lerp(t0, t1, f[0]);
  t0 = lerp(t2, t0, f[1]);
  return t0;
}

// bilinear lookup in float texture
float4 texRECT_bilinear(uniform samplerRECT tex, half2 t)
{
  float2 f = frac(t);
  float4 t0 = texRECT(tex, t);
  float4 t1 = texRECT(tex, t + half2(1,0) );
  float4 t2  = lerp(t0, t1, f[0]);
  t0 = texRECT(tex, t + half2(0,1) );
  t1 = texRECT(tex, t + half2(1,1) );
  t0 = lerp(t0, t1, f[0]);
  t0 = lerp(t2, t0, f[1]);
  return t0;
}
*/
PS_OUTPUT myps( VS_OUTPUT IN, uniform samplerRECT diffuse)
{
	PS_OUTPUT OUT;
	
	OUT.colour = texRECT(diffuse, IN.uvCoord * 512.0f);

	//OUT.colour = half4(tex2D_RGBE(diffuse, IN.uvCoord ), 1.0);

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		DepthTestEnable = false;
		
		VertexProgram = compile arbvp1 myvs( worldViewProj, eyePos );
        FragmentProgram  = compile arbfp1 myps(diffuseTexture);
    }
}
