//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4 worldViewProj : WorldViewProjection;
float3   	eyePos 			: EyePosition;

texture 	diffuseTexture;

sampler2D DiffuseSampler = sampler_state
{
    Texture = <diffuseTexture>;
    MinFilter = Anisotropic;
    MagFilter = Anisotropic;
    MipFilter = Anisotropic;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
    float3 position 	: POSITION;
	float2 uvCoord		: TEXCOORD0;	
};

struct VS_OUTPUT
{
	float4 position : POSITION;
	float2 uvCoord	: TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 colour	: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection, uniform float3 eyePos)
{
    VS_OUTPUT OUT;


   	OUT.position = mul( modelViewProjection, float4(IN.position + eyePos, 1.0) );
	OUT.uvCoord = IN.uvCoord;
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN, uniform sampler2D diffuse)
{
	PS_OUTPUT OUT;

	OUT.colour = tex2D(diffuse, IN.uvCoord );

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		Zenable  = false;

		//Zenable  = false;
		//Zwriteenable = false;
		
		VertexShader = compile arbvp1 myvs( worldViewProj, eyePos );
        PixelShader  = compile arbfp1 myps(DiffuseSampler);
    }
}
