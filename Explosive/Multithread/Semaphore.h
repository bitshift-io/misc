#ifndef _SEMAPHORE_H_
#define _SEMAPHORE_H_

#include <windows.h>

class Semaphore
{
public:
	Semaphore(int count = 0);
	~Semaphore();
protected:
	HANDLE mHandle;
};

#endif