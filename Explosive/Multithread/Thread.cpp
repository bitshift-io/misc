#include "Thread.h"

DWORD WINAPI Thread::ThreadFn(LPVOID ptr)
{
	Thread* thread = static_cast<Thread*>(ptr);
	return thread->Update();
}

Thread::Thread() :
	mRunning(false),
	mThreadHandle(0)
{

}

void Thread::Start(const Task* task)
{
	if (task)
		mTask = *task;

	mRunning = true;
	mThreadHandle = CreateThread(0, 0, Thread::ThreadFn, this, 0, 0); 
}

void Thread::Stop()
{
	mRunning = false;
}


void Thread::Pause()
{
	SetThreadPriority(mThreadHandle, THREAD_PRIORITY_IDLE);
}

void Thread::Resume()
{
	ResumeThread(mThreadHandle);
}


void Thread::Wait(unsigned long timeout)
{
	WaitForSingleObject(mThreadHandle, timeout == 0 ? INFINITE : timeout);
}

unsigned long Thread::Update()
{
	unsigned long ret = mTask.mTaskFn(mTask.mPtr, this);
	mRunning = false;
	return ret;
}

bool Thread::IsRunning()
{
	return mRunning;
}