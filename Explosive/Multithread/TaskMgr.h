#ifndef _TASKMGR_H_
#define _TASKMGR_H_

#include "../Template/Singleton.h"
#include "Thread.h"
#include <vector>
#include <queue>

using namespace std;

#define gTaskMgr TaskMgr::GetInstance()

class TaskMgr : public SingletonBC<TaskMgr>
{
public:
	void Init(int numThreads);
	void Deinit();

	void Start();

	void AddTask(const Task& task);
	bool PopLastTask(Task& task);

	void Wait();

protected:
	static unsigned long TaskFn(void* ptr, Thread* thread);

	queue<Task>		mTasks;
	vector<Thread>	mThreads;
};

#endif