#include "Mutex.h"

//#include <iostream>

//using namespace std;

Mutex::Mutex()
{
	mHandle = CreateMutex(0, false, 0);
}

Mutex::~Mutex()
{
	CloseHandle(mHandle);
}

void Mutex::Lock()
{
	//cout << "lock" << endl;
	WaitForSingleObject(mHandle, INFINITE);
}

void Mutex::Unlock()
{
	ReleaseMutex(mHandle);
	//cout << "unlock" << endl;
}
