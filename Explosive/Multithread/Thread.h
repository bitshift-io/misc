#ifndef _THREAD_H_
#define _THREAD_H_

#include <windows.h>

class Thread;

class Task
{
public:
	typedef unsigned long (*TaskFn)(void*, Thread*);

	Task() : mTaskFn(0), mPtr(0) 
	{
	}

	Task(TaskFn task, void* ptr = 0)
	{
		mTaskFn = task;
		mPtr = ptr;
	}

	TaskFn			mTaskFn;
	void*			mPtr;
};

class Thread
{
public:
	Thread();

	void Start(const Task* task = 0);
	void Stop();
	void Wait(unsigned long timeout = 0);
	bool IsRunning();

	void Pause();
	void Resume();

protected:
	virtual unsigned long Update();

	static DWORD WINAPI ThreadFn(LPVOID ptr);

	Task			mTask;
	HANDLE			mThreadHandle;
	bool			mRunning;
};

#endif