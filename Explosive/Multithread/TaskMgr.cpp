#include "TaskMgr.h"
#include "Mutex.h"

unsigned long TaskMgr::TaskFn(void* ptr, Thread* thread)
{
	Task task;
	while (gTaskMgr.PopLastTask(task))
	{
		task.mTaskFn(task.mPtr, thread);
	}
	return 0;
}

void TaskMgr::Init(int numThreads)
{
	//mThreads.resize(numThreads);
	for (int i = 0; i < numThreads; ++i)
		mThreads.push_back(Thread());
}

void TaskMgr::Deinit()
{

}

void TaskMgr::Start()
{
	Task task(TaskMgr::TaskFn, 0);
	for (unsigned int i = 0; i < mThreads.size(); ++i)
	{
		mThreads[i].Start(&task);//(TaskMgr::TaskFn));
	}
}

void TaskMgr::Wait()
{
	for (unsigned int i = 0; i < mThreads.size(); ++i)
		mThreads[i].Wait();
}

void TaskMgr::AddTask(const Task& task)
{
	MUTEX_BEGIN
	mTasks.push(task);
	MUTEX_END
}

bool TaskMgr::PopLastTask(Task& task)
{
	MUTEX_BEGIN
	if (mTasks.size() <= 0)
		return false;

	task = mTasks.back();
	mTasks.pop();
	MUTEX_END
	return true;
}