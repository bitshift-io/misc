#ifndef _MUTEX_H_
#define _MUTEX_H_

#include <windows.h>

#define MUTEX_BEGIN	\
static Mutex m;		\
m.Lock();

#define MUTEX_END	\
m.Unlock();

class Mutex
{
public:
	Mutex();
	~Mutex();

	void Lock();
	void Unlock();	

protected:
	HANDLE mHandle;
};

#endif