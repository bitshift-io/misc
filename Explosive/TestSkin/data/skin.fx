
float4x4 	worldViewProj	: WorldViewProjection;
float4x4 	world			: World;
float4x4 	worldInv			: WorldInverse;
float3   	lightPos 		: LightPosition;

struct VS_INPUT
{
    float3 position 	: POSITION;
    float3 normal 	: NORMAL;
};

struct VS_OUTPUT
{
	float4 position 	: POSITION;
	float4 colour	: COLOR;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs(	const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform float4x4 model,
			uniform float4x4 worldInv,
			uniform float3   lightPos)
{
	VS_OUTPUT OUT;
	OUT.position = mul(modelViewProjection, float4(IN.position, 1.0));

	//float3 normal = normalize(mul(model, float4(IN.normal, 1.0)).xyz);

	// convert light to model space
	float3 lightModelPos = mul(worldInv, float4(lightPos, 1.0)).xyz;
	float3 lightVec = normalize(lightModelPos - IN.position);

	float angle = dot(IN.normal, lightVec);

	OUT.colour = float4(angle, angle, angle, 0.0);

	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN )
{
	PS_OUTPUT OUT;
	OUT.color = IN.colour;
	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		DepthTestEnable = true;
		DepthMask = true;
		DepthFunc = lequal;
		FrontFace = CW;
		CullFace = front;

		FragmentProgram = compile arbvp1 myvs(worldViewProj, world, worldInv, lightPos);
       	VertexProgram  = compile arbfp1 myps();
    }
}
