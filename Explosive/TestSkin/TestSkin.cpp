
#include "../Core/GameBase.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"

#include "../Engine/Font.h"
#include "../Engine/Camera.h"
#include "../Engine/Renderer.h"
#include "../Engine/Material.h"
#include "../Engine/Animation.h"

#include "../Util/Log.h"
#include "../Util/Macros.h"

namespace Siphon
{

class TestSkin : public GameBase<TestSkin>
{
public:

	bool Init(int argc, char **argv)
	{
		GameBase<TestSkin>::Init(argc, argv);

		window = new Window();
		window->Create("TestSkin", 800, 600, 32, false, 16);

		//Effect* e = new Effect();
        //    e->Load("data/skin.fx");
		Device::GetInstance().SetActiveWindow(window);
		window->ShowWindow();

		Device::GetInstance().CreateDevice();
		Device::GetInstance().SetClearColour(0.0, 0.0, 0.0);

		Input::GetInstance().CreateInput(window);

		camera = new Camera();
		camera->SetLookAt( Vector3(0, 0, 3.0), Vector3(0, 0, 0) );
		camera->SetSpeed(0.1f);
		camera->SetFreeCam(false);
		camera->SetPerspective(45.0f, 1.33, 100.0f, 0.1f);

		font = FontMgr::Load("arial.fnt");

		box.SetMin(Vector3(-0.05f,-0.05f,-0.05f));
		box.SetMax(Vector3(0.05f,0.05f,0.15f));

		skin = Mesh::LoadMesh("skin.MDL");
		if (!skin)
            return false;

        renderer.InsertMesh(skin);

		renderer.SetCamera(camera);

		skelAnim.SetSkeleton(SkeletonMgr::Load("skin.skl"));
		skelAnim.AddAnimation(AnimationMgr::Load("skin.anm"));
		skin->SetSkeletonAnimation(&skelAnim);
		skelAnim.Evaluate(0);
        skin->Update();

		light.SetPosition(Vector3(0,0,1));
		renderer.InsertLight(&light);

		//if (gDevice.MultisampleSupported())
		//	gDevice.EnableMulisample(false);

		glSampleCoverageARB(1.0f, GL_FALSE);

		gDevice.CheckErrors();
		Log::Print("Loading complete.\n");

		angle = 0.0f;
		return true;
	}

	virtual void Shutdown()
	{
		SAFE_DELETE(window);
		SAFE_DELETE(font);
		SAFE_DELETE(camera);

		GameBase<TestSkin>::Shutdown();
	}

	virtual bool UpdateGame()
	{
	    gInput.Update();

		skin->Update();
		camera->Update();

		gDevice.SetActiveWindow(window);

		skelAnim.Evaluate(Time::GetInstance().GetTicks() * 2);

		if (window->HandleMessages() || gInput.KeyPressed(Input::KEY_Esc))
			return false;

		return true;
	}

	virtual void UpdatePhysics()
	{

	}

	virtual void Render()
	{
		Device::GetInstance().BeginRender();

		renderer.Render();

		camera->LookAt();

		// debugging
		skelAnim.Draw(Matrix4(Matrix4::Identity));

		Matrix4 identity(Matrix4::Identity);
		identity.Draw();

		unsigned int fps = Device::GetInstance().GetFPS();
		font->Update();
		font->Print(0.1f, 0.1f, 0.5f ,"FPS: %i", fps);
		font->Render();

		Device::GetInstance().EndRender();
	}

protected:
	Light			light;

	SkeletonAnimation skelAnim;

	Siphon::Window*	window;
	Mesh*			skin;
	Font*			font;
	Camera*			camera;
	Box				box;
	float			angle;
	Renderer		renderer;
};

}

MAIN(TestSkin)
