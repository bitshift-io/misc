#include "GEnum.h"
#include "GCursor.h"

#ifdef WIN32
	#include "../Engine/Win32Input.h"
#else
	#include "../Engine/LinuxInput.h"
#endif

GEnum::GEnum() : GWindow(),
	type(List),
	numVisibleElements(-1),
	topSelection(0),
	currentSelection(0),
	spacing(0),
	textureWidth(-1),
	scrolling(false),
	lastSelection(0)
{
	upArrow.SetCallback(this);
	downArrow.SetCallback(this);
	scrollBar.SetCallback(this);
}

GEnum::GEnum(Type type, int numVisibleElements) : GWindow(""),
	type(type),
	numVisibleElements(numVisibleElements),
	topSelection(0),
	currentSelection(0),
	spacing(0),
	textureWidth(-1),
	scrolling(false),
	lastSelection(0)
{
	upArrow.SetCallback(this);
	downArrow.SetCallback(this);
	scrollBar.SetCallback(this);
}

void GEnum::Update(GCursor* cursor, GWindow* parent)
{
	SetParent(parent);

	int xOffset = 0;
	int yOffset = 0;
/*	if (parent)
	{
		xOffset = parent->GetDimensions().x1;
		yOffset = parent->GetDimensions().y1;
	}*/

	int texSpacing = GetTextSpacing();
	int texWidth = GetTextureWidth();
	if (numVisibleElements == -1)
	{
		numVisibleElements = floor(float(dimensions.y2 - dimensions.y1) / float(texSpacing));
	}

	// update children
	if (font)
	{
		int end = min(textArray.GetSize(), topSelection + numVisibleElements);

		int c = 0;
		for (int i = topSelection; i < end; ++i, ++c)
		{
			GWindow::Rect rect(	dimensions.x1 + xOffset, dimensions.y1 + yOffset + (c * texSpacing), 
				dimensions.x2 + xOffset - texWidth, dimensions.y1 + yOffset + ((c + 1) * texSpacing));

			textArray[i]->SetDimensions(rect);
			textArray[i]->Update(cursor, this);
		}		
	}

	UpdateScrollBar(cursor);
	upArrow.Update(cursor, this);
	downArrow.Update(cursor, this);
}

void GEnum::AddElement(const char* text) 
{ 
	GWindow* w = new GWindow(text);
	w->SetFont(font);
	w->SetFontScale(textScale);
	textArray.Insert(w); 
}

void GEnum::RemoveElement(int idx)
{
	textArray.RemoveShift(idx);

	if (currentSelection >= idx)
		currentSelection = max(currentSelection - 1, 0);

	if (lastSelection >= idx)
		lastSelection = max(lastSelection - 1, 0);
}

const char* GEnum::GetElement(int idx) 
{ 
	if (textArray.GetSize() <= 0)
		return 0;

	return textArray[idx]->GetText(); 
}

void GEnum::RemoveAllElements()
{
	while (textArray.GetSize())
		textArray.Remove(0);
}

bool GEnum::Render(GWindow* child, GWindow* parent)
{
	if (child == &upArrow && upArrow.mouseOver)
	{
		upArrow.mouseOver->Bind(0);
		glColor3f(1, 1, 1);

		GWindow::Rect rect = upArrow.GetDimensions();
		glBegin(GL_QUADS);
		{	
			glTexCoord2f(1, 1);
			glVertex2i(rect.x1, rect.y1);

			glTexCoord2f(0, 1);
			glVertex2i(rect.x1, rect.y2);

			glTexCoord2f(0, 0);
			glVertex2i(rect.x2, rect.y2);		

			glTexCoord2f(1, 0);
			glVertex2i(rect.x2, rect.y1);				
		}
		glEnd();
	}

	if (child == &downArrow && downArrow.mouseOver)
	{
		downArrow.mouseOver->Bind(0);
		glColor3f(1, 1, 1);

		GWindow::Rect rect = downArrow.GetDimensions();
		glBegin(GL_QUADS);
		{	
			glTexCoord2f(0, 0);
			glVertex2i(rect.x1, rect.y1);

			glTexCoord2f(1, 0);
			glVertex2i(rect.x1, rect.y2);

			glTexCoord2f(1, 1);
			glVertex2i(rect.x2, rect.y2);		

			glTexCoord2f(0, 1);
			glVertex2i(rect.x2, rect.y1);				
		}
		glEnd();
	}

	if (child == &scrollBar && scrollBar.mouseOver)
	{
		scrollBar.mouseOver->Bind(0);
		glColor3f(1, 1, 1);

		GWindow::Rect rect = scrollBar.GetDimensions();
		glBegin(GL_QUADS);
		{	
			glTexCoord2f(0, 0);
			glVertex2i(rect.x1, rect.y1);

			glTexCoord2f(1, 0);
			glVertex2i(rect.x1, rect.y2);

			glTexCoord2f(1, 1);
			glVertex2i(rect.x2, rect.y2);		

			glTexCoord2f(0, 1);
			glVertex2i(rect.x2, rect.y1);				
		}
		glEnd();
	}

	//child->DebugDraw(parent);
	return false;
}

void GEnum::OnMousePressed(GWindow* child)
{
	int idx = GetElementIndex(child);
	if (idx >= 0)
	{
		lastSelection = currentSelection;
		currentSelection = idx;
	}

	if (child == &upArrow)
	{
		topSelection -= numVisibleElements / 2;
		topSelection = max(0, topSelection);
	}

	if (child == &downArrow)
	{
		if (numVisibleElements <= GetElementCount())
		{
			topSelection += numVisibleElements / 2;
			topSelection = min(GetElementCount() - numVisibleElements, topSelection);
		}
	}

	GWindow::OnMousePressed(child);
}

void GEnum::Render(GWindow* parent)
{
	int xOffset = 0;
	int yOffset = 0;
/*	if (parent)
	{
		xOffset = parent->GetDimensions().x1;
		yOffset = parent->GetDimensions().y1;
	}*/

//	DebugDraw(parent);

	if (background)
		background->Draw(dimensions.x1 + xOffset, dimensions.y1 + yOffset, 
		dimensions.x2 + xOffset, dimensions.y2 + yOffset);

	int texWidth = GetTextureWidth();

	glMatrixMode(GL_PROJECTION); // and initalize Ortho mode
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0f, 800, 600, 0.0f, -1.0f, 1.0f);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(GL_FALSE);

	GLint blendSrc;
	GLint blendDst;
	GLint blendOn;

	blendOn = glIsEnabled(GL_BLEND);
	glGetIntegerv(GL_BLEND_SRC, &blendSrc);
	glGetIntegerv(GL_BLEND_DST, &blendDst);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	upArrow.Render(this);
	downArrow.Render(this);
	scrollBar.Render(this);

	if (blendOn)
		glEnable(GL_BLEND);
	else
		glDisable(GL_BLEND);

	glBlendFunc(blendSrc, blendDst);

	if (font)
	{
		int end = min(textArray.GetSize(), topSelection + numVisibleElements);
		for (int i = topSelection; i < end; ++i)
		{
			textArray[i]->Render(this);
		}		
	}

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

int GEnum::GetTextureWidth()
{
	return textureWidth != -1 ? textureWidth : GetTextSpacing();
}

int GEnum::GetTextSpacing()
{
	return font->GetLineHeight() * textScale;
}

void GEnum::SetArrowTexture(Texture* arrow)
{ 
	upArrow.SetMouseDownImage(arrow); 
	upArrow.SetMouseOutImage(arrow); 
	upArrow.SetMouseOverImage(arrow); 

	downArrow.SetMouseDownImage(arrow); 
	downArrow.SetMouseOutImage(arrow); 
	downArrow.SetMouseOverImage(arrow); 
}

void GEnum::SetScrollBarTexture(Texture* scrollBar) 
{ 
	this->scrollBar.SetMouseDownImage(scrollBar); 
	this->scrollBar.SetMouseOutImage(scrollBar); 
	this->scrollBar.SetMouseOverImage(scrollBar); 
}

void GEnum::SetDimensions(const Rect& dimensions)
{
	GWindow::SetDimensions(dimensions);
	GWindow::Rect rect;

	rect.x1 = dimensions.x2 - GetTextureWidth();
	rect.y1 = dimensions.y1;
	rect.x2 = dimensions.x2;
	rect.y2 = dimensions.y1 + GetTextureWidth();
	upArrow.SetDimensions(rect);

	rect.x1 = dimensions.x2 - GetTextureWidth();
	rect.y1 = dimensions.y2 - GetTextureWidth();
	rect.x2 = dimensions.x2;
	rect.y2 = dimensions.y2;
	downArrow.SetDimensions(rect);
}

void GEnum::UpdateScrollBar(GCursor* cursor)
{
	// how many pages are there?
	float elementCount = max(1.0f, float(GetElementCount()));
	float numPages = max(1.0f, elementCount / float(numVisibleElements));

	// calculate the height of the bar
	float top = dimensions.y1 + GetTextureWidth();
	float bottom = dimensions.y2 - GetTextureWidth();
	float height = bottom - top;

	// calculate scroll bar size
	float scrollBarSize = height / numPages;

	// if we a re moving the scroll bar
	if (scrolling)
	{
		// get cursor position
		float x, y;
		cursor->GetPosition(x, y);

		// calculate the percentage it is from the top
		float mousePercent = min(max(y - top, 0) / height, 1.0f);

		// clamp top selection
		topSelection = mousePercent * GetElementCount();

		topSelection = max(0, topSelection);
		topSelection = min(GetElementCount() - numVisibleElements, topSelection);
	}

	// what % are we from the top
	float visiblePercent = float(topSelection) / elementCount;

	GWindow::Rect rect;
	rect.x1 = dimensions.x2 - GetTextureWidth();
	rect.x2 = dimensions.x2;
	rect.y1 = top + height * visiblePercent;
	rect.y2 = rect.y1 + scrollBarSize;
	scrollBar.SetDimensions(rect);

	scrollBar.Update(cursor);

	// if we clicked on the button we are scrolling
	if (scrollBar.GetMouseState() == GWindow::MouseState::Down)
		scrolling = true;
	
	// if we let go, we arent scrolling any more!
	// or if there is no enough pages
	if (numPages <= 1 || !cursor->MouseDown())
		scrolling = false;
}

int GEnum::GetElementIndex(GWindow* child)
{
	for (unsigned int i = 0; i < GetElementCount(); ++i)
	{
		if (child == textArray[i])
			return i;
	}

	return -1;
}

void GEnum::SelectLastIndex() 
{ 
	currentSelection = textArray.GetSize(); 
	topSelection = max(0, currentSelection - numVisibleElements);
}

void GEnum::SelectIndex(int idx)
{
	currentSelection = idx; 
	topSelection = max(0, currentSelection - numVisibleElements);
}