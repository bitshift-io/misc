#include "GWindow.h"
#include "GCursor.h"

#ifdef WIN32
	#include "../Engine/Win32Input.h"
#else
	#include "../Engine/LinuxInput.h"
#endif

GWindow::GWindow() :
	parent(0),
	callbacks(0),
	background(0),
	font(0),
	textScale(1.0f),
	inputFocus(false)
{

}

GWindow::GWindow(const char* text) : 
	parent(0),
	text(text),
	callbacks(0),
	background(0),
	font(0),
	textScale(1.0f),
	inputFocus(false)
{

}

GWindow::GWindow(const char* text, const Rect& dimensions) : 
	parent(0),
	text(text ? text : ""),
	dimensions(dimensions),
	callbacks(0),
	background(0),
	font(0),
	textScale(1.0f),
	inputFocus(false)
{

}

void GWindow::Update(GCursor* cursor, GWindow* parent)
{
	SetParent(parent);

	if (callbacks && callbacks->Update(this, cursor, parent) == false)
		return;


	// check if mouse is over window
	float x, y;
	cursor->GetPosition(x, y);

	bool mouseDown = cursor->MouseDown();

	Rect dim = dimensions;
/*	if (parent)
	{
		dim.x1 += parent->GetDimensions().x1;
		dim.y1 += parent->GetDimensions().y1;
		dim.x2 += parent->GetDimensions().x1;
		dim.y2 += parent->GetDimensions().y1;
	}*/

	// mouse is over the button
	if (x >= dim.x1 && x <= dim.x2 && y >= dim.y1 && y <= dim.y2)
	{
		// if mouse down
		if (mouseDown)
		{
			// if mouse not already down, notify of mouse down
			if (mouseState != Down)
				OnMouseDown();
		}
		else
		{
			if (mouseState == Down)
			{
				OnMousePressed();
				OnMouseOver();
			}

			// if mouse is not down, and not over, change to mouse over
			// or mouse state was down but now isnt
			if (mouseState == Out)
				OnMouseOver();
		}
	}
	else
	{
		// if we werent out last time
		if (mouseState != Out)
			OnMouseOut(); // we are now
	}

	// if a key was pressed
	if (inputFocus)
	{
		std::string pressedKey = Input::GetInstance().GetPressedKey();;
		if (pressedKey.length())
			OnKeyPressed(pressedKey);
	}

	// iterate over all children
	for (int i = 0; i < children.GetSize(); ++i)
		children[i]->Update(cursor, this);
}

void GWindow::Render(GWindow* parent)
{
	if (callbacks && callbacks->Render(this, parent) == false)
		return;

	int xOffset = 0;
	int yOffset = 0;
/*	if (parent)
	{
		xOffset = parent->GetDimensions().x1;
		yOffset = parent->GetDimensions().y1;
	}
*/
//	DebugDraw(parent);

	if (background)
		background->Draw(dimensions.x1 + xOffset, dimensions.y1 + yOffset, 
						dimensions.x2 + xOffset, dimensions.y2 + yOffset);

	if (font)
		font->RenderText(dimensions.x1 + xOffset, dimensions.y1 + yOffset, textScale, text.c_str());

	// iterate over all children to draw thier GUI
	for (int i = 0; i < children.GetSize(); ++i)
		children[i]->Render(this);
}

void GWindow::DebugDraw(GWindow* parent)
{
	int xOffset = 0;
	int yOffset = 0;
/*	if (parent)
	{
		xOffset = parent->GetDimensions().x1;
		yOffset = parent->GetDimensions().y1;
	}*/

	glMatrixMode(GL_PROJECTION); // and initalize Ortho mode
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0f, 800, 600, 0.0f, -1.0f, 1.0f);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(GL_FALSE);

	glBegin(GL_LINES);
	{
		glColor4f(1, 1, 1, 0.5f);
		glVertex2i(dimensions.x1 + xOffset, dimensions.y1 + yOffset);
		glVertex2i(dimensions.x1 + xOffset, dimensions.y2 + yOffset);

		glVertex2i(dimensions.x1 + xOffset, dimensions.y1 + yOffset);
		glVertex2i(dimensions.x2 + xOffset, dimensions.y1 + yOffset);

		glVertex2i(dimensions.x1 + xOffset, dimensions.y2 + yOffset);
		glVertex2i(dimensions.x2 + xOffset, dimensions.y2 + yOffset);

		glVertex2i(dimensions.x2 + xOffset, dimensions.y1 + yOffset);
		glVertex2i(dimensions.x2 + xOffset, dimensions.y2 + yOffset);
	}
	glEnd();

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void GWindow::SetFont(Font* font) 
{ 
	this->font = font; 
}

void GWindow::SetFontScale(float scale)
{
	textScale = scale; 
}

void GWindow::OnMouseOut(GWindow* child)
{
	if (parent)
		parent->OnMouseOut(child);

	if (callbacks)
		callbacks->OnMouseOut(child);
}

void GWindow::OnMouseOver(GWindow* child)
{
	if (parent)
		parent->OnMouseOver(child);

	if (callbacks)
		callbacks->OnMouseOver(child);
}

void GWindow::OnMouseDown(GWindow* child)
{
	if (parent)
		parent->OnMouseDown(child);

	if (callbacks)
		callbacks->OnMouseDown(child);
}

void GWindow::OnMousePressed(GWindow* child)
{
	if (parent)
		parent->OnMousePressed(child);

	if (callbacks)
		callbacks->OnMousePressed(child);
}

void GWindow::OnKeyPressed(GWindow* child, const std::string& pressedKey)
{
	if (parent)
		parent->OnKeyPressed(child, pressedKey);

	if (callbacks)
		callbacks->OnKeyPressed(child, pressedKey);
}

void GWindow::OnMouseOut()
{
	mouseState = Out;

	if (parent)
		parent->OnMouseOut(this);
}

void GWindow::OnMouseOver()
{
	mouseState = Over;

	if (parent)
		parent->OnMouseOver(this);
}

void GWindow::OnMouseDown()
{
	mouseState = Down;

	if (parent)
		parent->OnMouseDown(this);
}

void GWindow::OnMousePressed()
{
	mouseState = Pressed;

	if (parent)
		parent->OnMousePressed(this);
}


void GWindow::OnKeyPressed(const std::string& pressedKey)
{
	if (parent)
		parent->OnKeyPressed(this, pressedKey);
}

void GWindow::AddChild(GWindow* child)
{
	children.Insert(child);
}