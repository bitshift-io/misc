#ifndef _SIPHON_GENUM_
#define _SIPHON_GENUM_

#include "GWindow.h"
#include "GButton.h"

class GEnum : public GWindow, public GWindow::Callback
{
public:
	enum Type
	{
		DropDown,
		List
	};

	GEnum();

	// -1 indicates auto
	GEnum(Type type, int numVisibleElements = -1);
	virtual ~GEnum() { }

	void			SetTextSpacing(int spacing) { this->spacing = spacing; } // additional spacing between lines
	unsigned int	GetElementCount() { return textArray.GetSize(); }
	void			AddElement(const char* text);
	void			RemoveElement(int idx);
	const char*		GetElement(int idx);
	void			RemoveAllElements();

	// return index of selected element
	void			SelectLastIndex();
	int				GetSelectedIndex() { return currentSelection; }
	int				GetLastSelectedIndex() { return lastSelection; }
	void			SelectIndex(int idx);
	int				GetSize()					{ return textArray.GetSize(); }

	// returns -1 if not in this list
	virtual int		GetElementIndex(GWindow* child);
	GWindow*		GetElementWindow(int idx) { return textArray[idx]; }

	virtual bool	Render(GWindow* child, GWindow* parent);

	virtual void	Update(GCursor* cursor, GWindow* parent = 0);
	virtual void	Render(GWindow* parent = 0);

	void SetArrowTexture(Texture* arrow);
	void SetScrollBarTexture(Texture* scrollBar);

	// the width of the textures, this should generally be line height of the font
	// -1 indicates use line height from the font
	void SetTextureWidth(int width) { textureWidth = width; }

	virtual void SetDimensions(const Rect& dimensions);

	int GetTextureWidth();
	int GetTextSpacing();

	virtual void OnMousePressed(GWindow* child);

protected:
	void UpdateScrollBar(GCursor* cursor = 0);

	Array<GWindow*> textArray;

	// num visible items in the list or the num visible in the drop down combo box
	int	numVisibleElements;
	int topSelection;
	int currentSelection;
	int lastSelection;
	int spacing;
	int textureWidth;

	GButton upArrow;
	GButton downArrow;
	GButton scrollBar;
	bool	scrolling;

	Type type;
};

#endif