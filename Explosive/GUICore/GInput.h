#ifndef _SIPHON_GINPUT_
#define _SIPHON_GINPUT_

#include "GWindow.h"

class GInput : public GWindow
{
public:
	
	GInput(const Rect& dimensions);

	virtual void OnKeyPressed(const std::string& pressedKey);

	virtual void Update(GCursor* cursor, GWindow* parent = 0);
	virtual void Render(GWindow* parent = 0);

protected:

};

#endif