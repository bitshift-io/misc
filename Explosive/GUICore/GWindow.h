#ifndef _GWINDOW_
#define _GWINDOW_

#include <string>

#include "../Util/Array.h"

#include "../Engine/Font.h"
#include "../Engine/Texture.h"

using namespace Siphon;

class GCursor;

/**
 * Base class for all GUI objects, G stands for GUI
 * children notify thier parents of input,
 * parent notifies children of updates and rendering
 */
class GWindow
{
public:
	enum MouseState
	{
		Out,
		Over,
		Down,
		Pressed
	};

	class Rect
	{
	public:
		Rect() : x1(0), y1(0), x2(0), y2(0) { }
		Rect(float x1, float y1, float x2, float y2) : x1(x1), y1(y1), x2(x2), y2(y2) { }

		float x1, y1; // top left
		float x2, y2; // bottom right
	};

	/**
	 * Add a callback to the main window, and you will recive all events for all windows
	 * using this class
	 */
	class Callback
	{
	public:
		virtual void OnMouseOut(GWindow* child) { }
		virtual void OnMouseOver(GWindow* child) { }
		virtual void OnMouseDown(GWindow* child) { }
		virtual void OnMousePressed(GWindow* child) { }
		virtual void OnKeyPressed(GWindow* child, const std::string& pressedKey) { }

		virtual bool Render(GWindow* child, GWindow* parent) { return true; }
		virtual bool Update(GWindow* child, GCursor* cursor, GWindow* parent) { return true; }
	};

	GWindow();
	GWindow(const char* text);
	GWindow(const char* text, const Rect& dimensions);
	virtual ~GWindow() { }

	void SetCallback(Callback* callbacks) { this->callbacks = callbacks; }
	void SetParent(GWindow* parent) { this->parent = parent; }
	void SetText(const char* text) { this->text = text; }
	virtual void SetDimensions(const Rect& dimensions) { this->dimensions = dimensions; }
	virtual void SetFont(Font* font);
	virtual void SetFontScale(float scale);
	virtual void SetBackgroundImage(Texture* background) { this->background = background; }

	virtual Texture* GetBackgroundImage() { return background; }
	const char* GetText() { return text.c_str(); }
	const Rect& GetDimensions() { return dimensions; }
	float GetFontScale() { return textScale; }

	// Call this on the very top level window
	// to handle input for it and its children
	virtual void Update(GCursor* cursor, GWindow* parent = 0);

	virtual void OnMouseOut(GWindow* child);
	virtual void OnMouseOver(GWindow* child);
	virtual void OnMouseDown(GWindow* child);
	virtual void OnMousePressed(GWindow* child);
	virtual void OnKeyPressed(GWindow* child, const std::string& pressedKey);

	virtual void OnMouseOut();
	virtual void OnMouseOver();
	virtual void OnMouseDown();
	virtual void OnMousePressed();
	virtual void OnKeyPressed(const std::string& pressedKey);

	// so with windows, FPS isnt an issue,
	// so we render from top level to lower level (ie, back to front)
	virtual void Render(GWindow* parent = 0);

	void AddChild(GWindow* child);

	void DebugDraw(GWindow* parent = 0);
	MouseState GetMouseState() { return mouseState; }

	bool HasInputFocus() { return inputFocus; }
	void SetInputFocus(bool focus) { inputFocus = focus; }

protected:
	Callback*		callbacks;
	GWindow*		parent;
	std::string		text;
	Rect			dimensions;

	bool			inputFocus;
	Font*			font;
	float			textScale;

	Texture*		background;
	MouseState		mouseState;

	Array<GWindow*> children;
};

#endif