#include "GButton.h"

GButton::GButton() : GWindow(),
	mouseOver(0),
	mouseDown(0),
	mouseOut(0)
{

}

GButton::GButton(const char* text) : GWindow(text),
	mouseOver(0),
	mouseDown(0),
	mouseOut(0)
{

}

GButton::GButton(const char* text, const Rect& dimensions) : GWindow(text, dimensions),
	mouseOver(0),
	mouseDown(0),
	mouseOut(0)
{

}

void GButton::SetMouseOutImage(Texture* mouseOut) 
{ 
	SetBackgroundImage(mouseOut);
	this->mouseOut = mouseOut; 
}

void GButton::OnMouseOut()
{
	SetBackgroundImage(mouseOut);
	GWindow::OnMouseOut();
}

void GButton::OnMouseOver()
{
	SetBackgroundImage(mouseOver);
	GWindow::OnMouseOver();
}

void GButton::OnMouseDown()
{
	SetBackgroundImage(mouseDown);
	GWindow::OnMouseDown();
}