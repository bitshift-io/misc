#ifndef _GCURSOR_
#define _GCURSOR_

#include "GWindow.h"

/**
 * Cursor
 */
class GCursor
{
public:
	enum PositionType
	{
		Centre,
		TopLeft
	};

	GCursor();

	void SetFont(Font* font) { this->font = font; }
	void SetWindow(Window* window) { this->window = window; }

	void SetImage(Texture* background) { this->background = background; }
	void SetDimensions(const GWindow::Rect& dimensions);

	void GetPosition(float& x, float& y);
	bool MouseDown() { return mouseDown; }

	void Update();
	void Render();
protected:
	GWindow::Rect	dimensions;
	Texture*		background;
	PositionType	positionType;	
	bool			mouseDown;
	Window*			window;
	Font*			font;

	float			width;
	float			height;
};

#endif