#include "GCursor.h"

#ifdef WIN32
	#include "../Engine/Win32Input.h"
	#include "../Engine/Win32Window.h"
#else
	#include "../Engine/LinuxInput.h"
	#include "../Engine/LinuxWindow.h"
#endif

GCursor::GCursor() :
	positionType(TopLeft),
	font(0)
{

}

void GCursor::GetPosition(float& x, float& y)
{
	if (positionType == Centre)
	{
		x = dimensions.x1 + (dimensions.x2 - dimensions.x1) / 2.0f;
		y = dimensions.y1 + (dimensions.y2 - dimensions.y1) / 2.0f;
	}
	else // topleft
	{
		x = dimensions.x1;
		y = dimensions.y1;
	}
}

void GCursor::Update()
{
	int cursorWidth = width; //dimensions.x2 - dimensions.x1;
	int cursorHeight = height; //dimensions.y2 - dimensions.y1;

	int xCursor;
	int yCursor;

	if (cursorWidth != 30 || cursorHeight != 30)
	{
		int nothing = 0;
	}

	//window->GetCursorPosition(xCursor, yCursor);
	
	if (window->GetCursorPosition(xCursor, yCursor))
	{
		if (window->IsCursorVisible())
			window->SetCursorVisibility(false);
	}
	else if (!window->IsCursorVisible())
	{
		window->SetCursorVisibility(true);
	}


	// convert x and y to align up with the windows mouse
	float width = float(window->GetWidth());
	float height = float(window->GetHeight());

	float fx = (800.0f / width) *float(xCursor);
	float fy = (600.0f / height) * float(yCursor);

	if (fx != 0.0f)
		int nothing = 0;

	dimensions.x1 = fx;
	dimensions.x2 = fx + cursorWidth;
	dimensions.y1 = fy;
	dimensions.y2 = fy + cursorHeight;

	// stop the cursor point going offscreen
	GetPosition(fx, fy);

	int xClamp = 0;
	int yClamp = 0;

	if (fx < 0)
		xClamp = -fx;

	if (fx > 800)
		xClamp = 800 - fx;

	if (fy < 0)
		yClamp = -fy;

	if (fy > 600)
		yClamp = 600 - fy;

	dimensions.x1 += xClamp;
	dimensions.x2 += xClamp;

	dimensions.y1 += yClamp;
	dimensions.y2 += yClamp;

	mouseDown = Input::GetInstance().MouseDown(0);
}

void GCursor::Render()
{/*
	float fx, fy;
	GetPosition(fx, fy);
	background->Draw(fx, fy, fx + 10, fy + 10);

	char buffer[256];
	sprintf(buffer, "%f, %f", fx, fy);

	if (font)
		font->DrawText(0, 0, buffer);
*/

	if (background)
	{
		GLint blendSrc;
		GLint blendDst;
		GLint blendOn;

		blendOn = glIsEnabled(GL_BLEND);
  		glGetIntegerv(GL_BLEND_SRC, &blendSrc);
		glGetIntegerv(GL_BLEND_DST, &blendDst);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		background->Draw(dimensions.x1, dimensions.y1, dimensions.x2, dimensions.y2);

		if (blendOn)
			glEnable(GL_BLEND);
		else
			glDisable(GL_BLEND);

		glBlendFunc(blendSrc, blendDst);
	}
}

void GCursor::SetDimensions(const GWindow::Rect& dimensions)
{ 
	this->dimensions = dimensions; 
	width = dimensions.x2 - dimensions.x1;
	height = dimensions.y2 - dimensions.y1;
}