#include "GInput.h"

GInput::GInput(const Rect& dimensions) : GWindow("", dimensions)
{

}

void GInput::Update(GCursor* cursor, GWindow* parent)
{
	GWindow::Update(cursor, parent);
}

void GInput::Render(GWindow* parent)
{
	GWindow::Render(parent);
}

void GInput::OnKeyPressed(const std::string& pressedKey)
{
	GWindow::OnKeyPressed(pressedKey);
}