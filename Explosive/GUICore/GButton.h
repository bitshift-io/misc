#ifndef _GBUTTON_
#define _GBUTTON_

#include <string>

#include "GWindow.h"

#include "../Util/Array.h"

#include "../Engine/Font.h"
#include "../Engine/Texture.h"

using namespace Siphon;

/**
 * something you can click on
 */
class GButton : public GWindow
{
	friend class GEnum;

public:
	
	GButton();
	GButton(const char* text);
	GButton(const char* text, const Rect& dimensions);

	void SetMouseOutImage(Texture* mouseOut);
	void SetMouseOverImage(Texture* mouseOver) { this->mouseOver = mouseOver; }
	void SetMouseDownImage(Texture* mouseDown) { this->mouseDown = mouseDown; }

	virtual void OnMouseOut();
	virtual void OnMouseOver();
	virtual void OnMouseDown();

protected:
	GWindow*		parent;
	std::string		text;
	Rect			dimensions;

	Font*			font;
	Texture*		mouseOver;
	Texture*		mouseDown;
	Texture*		mouseOut;
};

#endif