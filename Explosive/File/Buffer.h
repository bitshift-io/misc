#ifndef _BUFFER_H_
#define _BUFFER_H_

class Buffer
{
public:

	int		Pack(void* data, unsigned int size) = 0;
	void	Unpack(void* data, unsigned int size) = 0;

protected:

};

class HeapBuffer : public Buffer
{
public:

	int		Create(int size);
	void	Destroy();

protected:

	char*	mBuffer;
}

template<class T>
class StackBuffer : public Buffer
{
public:

protected:

	char	mBuffer[T];
}

#endif