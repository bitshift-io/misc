#ifndef _PACKAGE_H_
#define _PACKAGE_H_

// http://www.flipcode.com/articles/article_vfs01.shtml

enum FileAccess
{
	FA_Read,
	FA_Write,
};

enum FileSeek
{
	FS_Start,
	FS_End,
	FS_Current
};

enum EntryType
{
	ET_Directory,
	ET_File
};

struct Entry
{
	EntryType		mType;
	const char*		mFile;
	Entry*			mParent;
	Entry*			mChildren;
	unsigned int	mNumChildren;
};

class Frame
{
public:
	
protected:
	unsigned int	mSize;
	unsigned int	mHeadOffset;
	unsigned int	mStreamOffset;
	const char*		mName;
	Frame*			mNext;
};

typedef bool (*FilterFn)(void* inData, unsigned int inCount, void** outData, unsigned int** outCount);

struct Filter
{
	const char* mName;
	FilterFn	mEncode;
	FilterFn	mDecode;
};

class Package
{
public:
	bool Open(const char* file, FileAccess access = FA_Read);

	bool 

protected:
	Frame	mRoot;	
};

#endif