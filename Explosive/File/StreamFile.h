#ifndef _STREAMFILE_H_
#define _STREAMFILE_H_

typedef (void)(*StreamCallback)(StreamFile*);

// maybe i should go the c++ iostream way here with all this....

class StreamFile
{
public:
	void SetCallback(StreamCallback callback) { mCallback = callback; }

protected:
	StreamCallback	mCallback;
};

#endif