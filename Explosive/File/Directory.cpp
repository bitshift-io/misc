#include "Directory.h"

#ifdef WIN32
    #include <io.h>
#else
    #include <sys/types.h>
    #include <dirent.h>
#endif

namespace Siphon
{

bool Directory::GetFileList(const char* directory, Array<std::string>& files, const char* filter)
{

	char searchQuery[256];
	sprintf(searchQuery, "%s/%s", directory, filter);

#ifdef WIN32
	_finddata_t fileInfo;

	intptr_t handle = _findfirst(searchQuery, &fileInfo);
	if (handle == -1)
		return false;

	while (_findnext(handle, &fileInfo) != -1)
	{
		if (fileInfo.name[0] != '.')
			files.Insert(std::string(fileInfo.name));
	}

	return _findclose(handle) == 0;
#else
    DIR* handle = opendir(searchQuery);

    dirent* fileInfo = 0;
    while ((fileInfo = readdir(handle)) != NULL)
	{
		if (fileInfo->d_name[0] != '.')
			files.Insert(std::string(fileInfo->d_name));
	}

    return closedir(handle) == 0;
#endif
}

bool Directory::GetDirectoryList(const char* directory, Array<std::string>& dirs, const char* filter)
{
	return true;
}

}
