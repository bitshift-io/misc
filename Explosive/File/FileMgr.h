#ifndef _PACKAGEMGR_H_
#define _PACKAGEMGR_H_

#include <vector>

#define gPackageMgr PackageMgr::GetInstance()

class PackageMgr : public Singleton<PackageMgr>
{
public:
	bool		RegisterPackage(Package* package);

	// searches to see if the raw file exists then checks in packages
	File*		GetFile(const char* name);

protected:
	vector<Package>		mPackage;
};

#endif