#ifndef _SIPHON_DIRECTORY_
#define _SIPHON_DIRECTORY_

#include "Array.h"
#include <string>

namespace Siphon
{

// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/vclib/html/_crt__findfirst.2c_._findfirsti64.2c_._wfindfirst.2c_._wfindfirsti64.asp
// currently win32 specific
class Directory
{
public:
	// get list of files in a directory
	static bool GetFileList(const char* directory, Array<std::string>& files, const char* filter = "*.*");

	// get list of direcotries in a directory
	static bool GetDirectoryList(const char* directory, Array<std::string>& dirs, const char* filter = "*.*");
};


}

#endif
