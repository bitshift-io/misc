#ifndef _FILE_H_
#define _FILE_H_

#include <istream>
#include <iostream>
#include <fstream>

// http://www.cplusplus.com/doc/tutorial/files.html

// http://www.boost.org/libs/filesystem/doc/fstream.htm
// http://www.boost.org/libs/filesystem/doc/index.htm#tutorial
// http://www.boost.org/libs/filesystem/doc/index.htm#Examples
// http://www.boost.org/



template < class charT, class traits = std::char_traits<charT> >
class basic_filebuf : public std::basic_filebuf<charT,traits>
{
public:
  virtual ~basic_filebuf() {}

  std::basic_filebuf<charT,traits> * open( const path & file_ph, std::ios_base::openmode mode );
};

typedef basic_filebuf<char> filebuf;
typedef basic_filebuf<wchar_t> wfilebuf;



template < class charT, class traits = std::char_traits<charT> >
class basic_ifstream : public std::basic_ifstream<charT,traits>
{
public:
  basic_ifstream() {}
  explicit basic_ifstream( const path & file_ph, std::ios_base::openmode mode = std::ios_base::in );
  virtual ~basic_ifstream() {}
  void open( const path & file_ph, std::ios_base::openmode mode = std::ios_base::in );
};

typedef basic_ifstream<char> ifstream;
typedef basic_ifstream<wchar_t> wifstream;



template < class charT, class traits = std::char_traits<charT> >
class basic_ofstream : public std::basic_ofstream<charT,traits>
{
public:
  basic_ofstream() {}
  explicit basic_ofstream( const path & file_ph, std::ios_base::openmode mode = std::ios_base::out );
  virtual ~basic_ofstream() {}
  void open( const path & file_ph, std::ios_base::openmode mode = std::ios_base::out );
};

typedef basic_ofstream<char> ofstream;
typedef basic_ofstream<wchar_t> wofstream;



template < class charT, class traits = std::char_traits<charT> >
class basic_fstream : public std::basic_fstream<charT,traits>
{
public:
  basic_fstream() {}
  explicit basic_fstream( const path & file_ph, std::ios_base::openmode mode = std::ios_base::in|std::ios_base::out );
  virtual ~basic_fstream() {}
  void open( const path & file_ph, std::ios_base::openmode mode = std::ios_base::in|std::ios_base::out );
};

typedef basic_fstream<char> fstream;
typedef basic_fstream<wchar_t> wfstream;



/*
class OutputFile : public std::ifstream
{
public:
	explicit __CLR_OR_THIS_CALL basic_ifstream(const char *_Filename,
		ios_base::openmode _Mode = ios_base::in,
		int _Prot = (int)ios_base::_Openprot)
		: basic_istream<_Elem, _Traits>(&_Filebuffer)
		{	// construct with named file and specified mode
		if (_Filebuffer.open(_Filename, _Mode | ios_base::in, _Prot) == 0)
			_Myios::setstate(ios_base::failbit);
		}
protected:

};

class InputFile : public std::ifstream
{
public:

protected:

};*/



#endif
