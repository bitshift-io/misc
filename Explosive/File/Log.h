#ifndef _LOG_H
#define _LOG_H

#include <stdio.h>
#include <stdarg.h>

namespace Siphon
{

class Log
{
public:
	static void SetLog(const char* file)
	{
		if (pLog)
			Close();

		pLog = fopen(file, "w");
	}

	static void Close()
	{
		fclose(pLog);
		pLog = 0;
	}

	static void Print(const char* text, ... )
	{
		va_list ap;

		if (!pLog)
			SetLog("system.log");

 		if (!text)
    		return;

  		va_start(ap, text);
		vfprintf(pLog, text, ap);
		vprintf(text, ap);
  		va_end(ap);

		fflush(pLog);
	}

protected:
	static FILE* pLog;
};

}
#endif
