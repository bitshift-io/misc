#include "../Core/GameBase.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"
#include "../Engine/Font.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "../Engine/Renderer.h"
#include "../Engine/Time.h"
#include "../Engine/Material.h"
#include "../Engine/Animation.h"
#include "../Engine/Window.h"
#include "../Engine/Client.h"

#include "../Util/Log.h"
#include "../Util/Macros.h"

#include <winsock.h>
#include <stdlib.h>
#include <iostream>

#pragma comment(lib, "Ws2_32.lib")

using namespace Siphon;
//using namespace std;

// kBufferSize must be larger than the length of kpcEchoMessage.
const int kBufferSize = 1024;
const char* kpcEchoMessage = "This is a test of the emergency data "
"transfer system.  If this had been real a real emergency, we "
"would have sent this data out-of-band.";
const int kEchoMessageLen = strlen(kpcEchoMessage);

class TestNetworkClient : public GameBase<TestNetworkClient>
{
public:
	bool Init(int argc, char **argv) 
	{
		GameBase::Init(argc, argv);

		Client::Create();
		Client::GetInstance().Init();

		if (Client::GetInstance().Connect("localhost", 1050))
		{
			Client::GetInstance().Send(kpcEchoMessage, kEchoMessageLen);

			char buffer[kBufferSize];
			Client::GetInstance().Receive(buffer, kEchoMessageLen);

			Log::Print("Received: %s\n", buffer);
		}

		Client::GetInstance().Shutdown();
		Client::Destroy();

		return false;
	}

	virtual void Shutdown()
	{
	}

	virtual bool UpdateGame()
	{
		return true;
	}

	virtual void UpdatePhysics()
	{

	}

	virtual void Render()
	{

	}

protected:

};

MAIN(TestNetworkClient)
