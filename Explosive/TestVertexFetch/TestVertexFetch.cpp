#include "../Core/GameBase.h"

#include "../Engine/Win32Window.h"
#include "../Engine/Win32Input.h"
#include "../Engine/Win32Time.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"
#include "../Engine/Font.h"
#include "../Engine/Camera.h"
#include "../Engine/Renderer.h"
#include "../Engine/Material.h"
#include "../Engine/Projector.h"

#include "../Util/Log.h"
#include "../Util/Macros.h"

using namespace Siphon;

class TestVertexFetch : public GameBase<TestVertexFetch>
{
public:
	TestVertexFetch()
	{

	}

	bool Init(int argc, char **argv)
	{
		GameBase::Init(argc, argv);

		window = new Window();
		window->Create("TestVertexFetch", 512, 512, 16, false);
		window->ShowWindow();
		Device::GetInstance().SetActiveWindow(window);

		Device::GetInstance().CreateDevice();
		Device::GetInstance().SetClearColour(0.5, 0.5, 0.5);

		Input::GetInstance().CreateInput(window);
		
		camera = new Camera();
		camera->SetLookAt( Vector3(0, 5, -10), Vector3(0, 0, 0) );
		camera->SetSpeed(0.1f);
		camera->SetFreeCam(true);
		camera->SetPerspective(90, 1.0, 10.0f, 0.5f);

		renderMgr.SetCamera(camera);
 
		TextureMgr::LoadTex("heightmap.tga", Texture::TF_VertexTexture);

		scene = Mesh::LoadMesh("water.MDL");
		renderMgr.InsertMesh(scene);

		font = FontMgr::Load("arial.fnt");

		Device::GetInstance().CheckErrors();
		Log::Print("Loading complete.\n");
		return true;
	}

	virtual void Shutdown()
	{
		SAFE_DELETE(camera);
	}

	virtual bool UpdateGame()
	{
		Input::GetInstance().Update();

		if (window->ShouldExit() || window->HandleMessages())
			return false;

		if (gInput.KeyPressed(Input::KEY_F1))
			gDevice.SaveScreenShot();

		camera->Update();

		renderMgr.SetTime(Time::GetInstance().GetTimeSeconds());

		if (Input::GetInstance().KeyPressed(DIK_ESCAPE))
			return false;

		return true;
	}

	virtual void UpdatePhysics()
	{

	}

	void DrawDebug(Mesh* scene)
	{
		Matrix4 identity(Matrix4::Identity);
		identity.Draw();
	}

	virtual void Render()
	{
		Device::GetInstance().BeginRender();
		renderMgr.Render();

		unsigned int fps = Device::GetInstance().GetFPS();
		font->Update();
		font->Print(0.1f, 0.1f, 0.5f, "FPS: %i", fps);
		font->Render();

		Device::GetInstance().EndRender();
	}

protected:
	Window*		window;
	Font*		font;
	Mesh*		scene;
	Camera*		camera;
	Renderer	renderMgr;
};

MAIN(TestVertexFetch)
