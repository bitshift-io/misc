
float4x4 mvp : WorldViewProjection;

sampler2D checkerSampler = sampler_state {
        minFilter = Nearest;
        magFilter = Nearest;
};

struct VS_INPUT
{
	float3 position 		: POSITION;
	float2 texCoord			: TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 position 	: POSITION;
	float2 texCoord		: TEXCOORD0;
	float	height			: TEXCOORD1;
};

struct PS_OUTPUT
{
	float4 color		: COLOR;
};

VS_OUTPUT myvs(const VS_INPUT IN,
			uniform float4x4 modelViewProj,
			uniform sampler2D heightTex)
{
    VS_OUTPUT OUT;
    
    float waterHeight = tex2Dlod(heightTex, float4(IN.texCoord, 0, 0) * 256).r;
    float3 position = IN.position;
   position.y += waterHeight * 100.0f;
	
	OUT.height = waterHeight * 100.0f;    
    OUT.position = mul(modelViewProj, float4(position, 1.0));
    
    OUT.texCoord = IN.texCoord;
	return OUT;
}

PS_OUTPUT myps(const VS_OUTPUT IN)
{
	PS_OUTPUT OUT;
	OUT.color = float4(IN.height,IN.height,IN.height,0);
	return OUT;
}

technique TextureSimple2 {
    pass {
		VertexProgram = compile vp40 myvs(mvp, checkerSampler );
        FragmentProgram = compile fp40 myps();
    }
}