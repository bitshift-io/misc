#include "../Core/GameBase.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"
#include "../Engine/Font.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "../Engine/Renderer.h"
#include "../Engine/Time.h"
#include "../Engine/Material.h"
#include "../Engine/Window.h"

#include "../Util/Log.h"
#include "../Util/Macros.h"

#include "Console.h"

using namespace Siphon;

class PrototypeConsole : public GameBase
{
public:
	PrototypeConsole(int argc, char **argv) : GameBase(argc, argv)
	{
		SetBasePath("data/");
	
		Device::GetInstance().CreateDevice();
		
		window = new Window();
		window->Create("PrototypeConsole", 800, 600, 16, false);
		window->ShowWindow();
		Device::GetInstance().SetActiveWindow(window);
		Device::GetInstance().SetClearColour(0.0, 0.0, 0.5);

		Input::GetInstance().CreateInput(window);

		g_Console.Create();
		g_Console.RegisterCmd("testfn", (void*)TestFunction);
		g_Console.RegisterCmd("testfn2", (void*)TestFunction2, "%i %s");
		
		camera = new Camera();
		camera->SetLookAt(Vector3(0, 0.001, -0.3), Vector3(0, 0, 0));
		camera->SetSpeed(0.05f);
		camera->SetPerspective(70, 1.33, 100.0f, 0.0001f);

		renderer.SetCamera(camera);

		camera->Update();

		font = new SFont;
		font->CreateFont("font.dds");

		Device::GetInstance().CheckErrors();
		Log::Print("Loading complete.\n");

/*
		_asm
		{
			nop
		}


		sscanf(
		_asm
		{
			nop
		}
*/

		_asm
		{
			nop
		}

		char p = 1;
		int j = 0;
		const char* chptr = "yo";
		void** test = new void*[5];
		
		for (int i = 0; i < 5; ++i)
		{
			test[i] = (void*)0xdeadbeef;
		}

		_asm
		{
			nop
		}

		Test(test[0], test[1], test[2], test[3], test[4]);
		//TestFunction2(&i, chptr);

		_asm
		{
			nop
		}
/*
0040325E  mov         eax,dword ptr [test] 
00403261  mov         ecx,dword ptr [eax+10h] 
00403264  push        ecx  
00403265  mov         edx,dword ptr [test] 
00403268  mov         eax,dword ptr [edx+0Ch] 
0040326B  push        eax  
0040326C  mov         ecx,dword ptr [test] 
0040326F  mov         edx,dword ptr [ecx+8] 
00403272  push        edx  
00403273  mov         eax,dword ptr [test] 
00403276  mov         ecx,dword ptr [eax+4] 
00403279  push        ecx  
0040327A  mov         edx,dword ptr [test] 
0040327D  mov         eax,dword ptr [edx] 
0040327F  push        eax  
00403280  call        PrototypeConsole::Test (403460h) 
00403285  add         esp,14h 
*/

		void* fnPtr = Test;
		int stackOffset = 5 * 4;
		

		for (int i = 0; i < 5; ++i)
		{
			_asm
			{
				mov eax, dword ptr [test]
				mov ecx, dword ptr [eax + edx]
				push ecx
				add edx, 4
			}
		}

		_asm
		{
			call dword ptr [fnPtr]

			mov edx, dword ptr [stackOffset]
			add esp, edx
		}

		_asm
		{
			nop
		}
	}

	static void Test(...)
	{
		int nothing = 0;
	}

	static void TestFunction()
	{
		g_Console.Print("yay!");
	}

	static void TestFunction2(int value, const char* text)
	{
		g_Console.Print("function called with parameters: %i %text", value, text);
	}

	virtual void Shutdown()
	{
		SAFE_DELETE(font);
		SAFE_DELETE(camera);
		SAFE_DELETE(window);
	}

	virtual bool UpdateGame()
	{
		window->HandleMessages();
		camera->Update();

		g_Console.Update();
		renderer.SetTime(Time::GetInstance().GetTimeSeconds());

		if (Input::GetInstance().KeyPressed(DIK_ESCAPE))
			return false;

		return true;
	}

	virtual void UpdatePhysics()
	{

	}

	virtual void Render()
	{
		Device::GetInstance().BeginRender();

		renderer.Render();
		
		unsigned int fps = Device::GetInstance().GetFPS();
		font->Update();
		font->Print(0.1f, 0.1f, "FPS: %i", fps);

		g_Console.Render(font);

		font->Render();

		Device::GetInstance().EndRender();
	}

protected:
	Window*			window;
	SFont*			font;
	Camera*			camera;
	Renderer		renderer;
};

MAIN(PrototypeConsole);
