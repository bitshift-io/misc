//--------------------------------------------------------------------------------------
//
//	(C) 2005 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_CONSOLE_
#define _SIPHON_CONSOLE_

#include <string>

#include "singleton.h"
#include "array.h"

namespace Siphon
{

#define g_Console Console::GetInstance()

int vsscanf(const char* src, const char* format, va_list argPtr);

class Console : public Singleton<Console>
{
public:
	Console();

	void RegisterCmd(const char* name, void* fnPtr, const char* args = 0);
	void ExecuteCmd(const char* cmd);

	void Update();
	void Render(SFont* font);

	void Print(const char* text, ... );

protected:

	struct Cmd
	{
		std::string name;
		std::string args;
		void*		fnPtr;
		int			argCount;
	};
	
	int GetArgParamCount(const char* args);
	void ParseArgs(void* fnPtr, const char* args, const char* suppliedArgs);

	Array<Cmd>	commands;
	bool		hasFocus;

	Array<std::string> text;	// previous lines of the console
	char		buffer[2048]; // the current text
};

}

#endif