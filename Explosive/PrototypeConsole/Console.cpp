#include "../Engine/Input.h"
#include "../Engine/Font.h"

#include "Console.h"

namespace Siphon
{

	
/*
	// copy stack pointer
	_asm
	{
		mov esi, esp;
	}

	// push variable parameters pointers on stack
	for (int i = count - 1; i >= 0; --i)
	{
		_asm
		{
			mov eax, dword ptr[i];
			mov ecx, dword ptr [argPtr];
			mov edx, dword ptr [ecx+eax*4];
			push edx;
		}
	}

	int stackAdvance = (2 + count) * 4;

	_asm
	{
		// now push on the fixed params
		mov eax, dword ptr [format];
		push eax;
		mov eax, dword ptr [src];
		push eax;

		// call fscanf, and more the result in to result
		call dword ptr [sscanf];
		mov result, eax;

		// restore stack pointer
		mov eax, dword ptr[stackAdvance];
		add esp, eax;
		//mov esp, esi;
	}* /

	return result;
}*/

Console::Console() :
	hasFocus(false)
{
	buffer[0] = '\0';
}

void Console::RegisterCmd(const char* name, void* fnPtr, const char* args)
{
	Cmd newCmd;
	newCmd.name = name;
	newCmd.fnPtr = fnPtr;

	if (args)
		newCmd.args = args;

	commands.Insert(newCmd);
}

int Console::GetArgParamCount(const char* args)
{
	// Get an upper bound for the # of args
	size_t count = 0;
	const char* p = args;

	while(1)
	{
		char c = *(p++);
		if (c == 0) 
			break;
		
		if (c == '%') 
			++count;
	}

	return count;
}

void Console::Update()
{
	if (Input::GetInstance().KeyPressed(DIK_GRAVE))
	{
		hasFocus = !hasFocus;
		return;
	}

	if (!hasFocus)
		return;

	if (Input::GetInstance().KeyPressed(DIK_RETURN))
	{
		ExecuteCmd(buffer);
		text.Insert(std::string(buffer));
		buffer[0] = '\0';
		return;
	}

	std::string str = Input::GetInstance().GetPressedKey();
	strcat(buffer, str.c_str());
}

void Console::ExecuteCmd(const char* cmdStr)
{
	// parse the command
	char fnName[256];
	const char* args = 0;
	sscanf(cmdStr, "%s", &fnName);

	for (int i = 0; i < commands.GetSize(); ++i)
	{
		// we need to check for multiple methods (same name) with different amount of args
		Cmd& cmd = commands[i];
		if (strcmpi(fnName, cmd.name.c_str()) == 0)
		{
			// get a pointer to the args
			int end = strlen(fnName) + 1;
			if (end >= strlen(cmdStr))
				end = strlen(cmdStr);

			args = &cmdStr[end];

			// parse args
			ParseArgs(cmd.fnPtr, cmd.args.c_str(), args);
		}
	}
}
/*

int vsscanf(const char *src, const char *format, va_list argPtr)
{
	// http://www.codeguru.com/Cpp/Cpp/string/comments.php/c5631/?thread=61724

	// Get an upper bound for the # of args
	size_t count = 0;
	const char* p = format;

	while(1)
	{
		char c = *(p++);
		if (c == 0) 
			break;
		
		if (c == '%' && (p[0] != '*' && p[0] != '%')) 
			++count;
	}

	if (count <= 0)
		return 0;

	int result;

	// copy stack pointer
	_asm
	{
		mov esi, esp;
	}

	// push variable parameters pointers on stack
	for (int i = count - 1; i >= 0; --i)
	{
		_asm
		{
			mov eax, dword ptr[i];
			mov ecx, dword ptr [argPtr];
			mov edx, dword ptr [ecx+eax*4];
			push edx;
		}
	}

	int stackAdvance = (2 + count) * 4;

	_asm
	{
		// now push on the fixed params
		mov eax, dword ptr [src];
		push eax;
		mov eax, dword ptr [file];
		push eax;

		// call sscanf, and more the result in to result
		call dword ptr [sscanf];
		mov result, eax;

		// restore stack pointer
		mov eax, dword ptr[stackAdvance];
		add esp, eax;
		//mov esp, esi;
	}

	return result;
}
*/
void Console::ParseArgs(void* fnPtr, const char* args, const char* suppliedArgs)
{
	va_list ap;

	//vsscanf(suppliedArgs, args, ap);

	int paramCount = GetArgParamCount(args);
	int stackOffset = paramCount * 4;

	va_start(ap);
	sscanf(suppliedArgs, args, ap);
 // 	va_end(ap);

	

	// call the function with the args
	// http://www.newty.de/fpt/fpt.html#callconv
	// we should also be able to call no static methods also

//	(*fnPtr)(va_arg(argPtr), va_arg(argPtr));
/*
	004031CB  push        offset string "yo" (422504h) 
004031D0  push        1    
004031D2  call        PrototypeConsole::TestFunction2 (403400h) 
004031D7  add         esp,8 
*/
/*	for (int i = 0; i < paramCount; ++i)
	{
		_asm
		{
			push 
		}
	}*/

//	va_start(ap, text);

	
	_asm
	{
		mov edx, 0
	}

	for (int i = 0; i < paramCount; ++i)
	{
		_asm
		{
			mov eax, dword ptr [ap]
			mov ecx, dword ptr [eax + edx]
			push ecx
			add edx, 4
		}
	}

	_asm
	{
		call dword ptr [fnPtr]

		mov edx, dword ptr [stackOffset]
		add esp, edx
	}
/*
	int stackAdvance = (2 + paramCount) * 4;

	// push variable parameters pointers on stack
	for (int i = paramCount - 1; i >= 0; --i)
	{
		_asm
		{
			mov eax, dword ptr [i];
			mov ecx, dword ptr [ap];
			mov edx, dword ptr [ecx+eax*4];
			push edx;
		}
	}

	_asm
	{/*
		// now push on the fixed params
		mov eax, dword ptr [format];
		push eax;
		mov eax, dword ptr [src];
		push eax;* /

		// call fscanf, and more the result in to result
		call dword ptr [fnPtr];
		//mov result, eax;
		// restore stack pointer
		mov eax, dword ptr[stackAdvance];
		add esp, eax;
	//	mov esp, esi;
	}
*/
	va_end(ap);
}

void Console::Render(SFont* font)
{
	if (!hasFocus)
		return;

	int size = text.GetSize();
	int start = max(size - 10, 0);

	float xPos = 0.1f;
	float yPos = 0.5f;
	for (int i = start; i < size; ++i)
	{
		font->Print(xPos, yPos, text[i].c_str());
		xPos += font->GetHeight();
	}
	font->Print(xPos, yPos, ">");
	yPos += font->GetWidth();

	font->Print(xPos, yPos, buffer);
	yPos += font->GetWidth() * strlen(buffer);

	font->Print(xPos, yPos, "*");
}

void Console::Print(const char* text, ... )
{
	va_list ap;

 	if (!text)
    	return;

	char buffer[256];

  	va_start(ap, text);
	vsprintf(buffer, text, ap);
  	va_end(ap);

	this->text.Insert(std::string(buffer));	
}

}