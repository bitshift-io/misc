#ifndef _NETWORKSTREAM_H_
#define _NETWORKSTREAM_H_

#include <winsock.h>

enum StreamState
{
	eNone	= 0x0,
	eRead	= 0x1 << 0,
	eWrite	= 0x1 << 1,
	eError	= 0x1 << 2
};

class NetworkStream
{
public:

	NetworkStream() : mSocket(0) { }

	bool IsValid() { return mSocket == 0 ? false : true; }

	//virtual void Send(Packet* packet) = 0;
	//virtual void Receive(Packet* packet) = 0;

	virtual StreamState GetState();

	void Close()
	{
		closesocket(mSocket);
		mSocket = 0;
	}

protected:

	SOCKET		mSocket;
};

#endif