#ifndef _NETWORK_H_
#define _NETWORK_H_

#include "../Template/Singleton.h"
#include <winsock.h>
#include <iostream>

typedef sockaddr_in SocketAddr;

using namespace std;
ostream& operator <<(ostream& os, const SocketAddr& addr);

#define gNetwork Network::GetInstance()

class Network : public SingletonBC<Network>
{
public:

	Network();
	~Network();

	unsigned long	ResolveAddress(const char* addr);
	SocketAddr		GetAddress(const char* addr, unsigned int port); //takes a stringed IP and gives a sockaddr
	SocketAddr		GetBroadcastAddress(unsigned int port);
	SocketAddr		GetAnyAddress(unsigned int port);
	SocketAddr		GetInvalidAddress();

	bool			Equals(const SocketAddr& a, const SocketAddr& b);

	int				GetLastError();

protected:

};

#endif