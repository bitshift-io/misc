#include "UDPStream.h"
#include "Network.h"
#include "Packet.h"
#include "Time.h"
#include "Multithread/Mutex.h"

// enable to fake packet loss
//#define FAKE_LAG
//#define FAKE_PACKETLOSS

#if defined FAKE_PACKETLOSS || defined FAKE_LAG

#include <list>

using namespace std;

const int cPacketLoss = 50;
const unsigned int cSendDelay = 300;

struct PacketInfo
{
	Packet*			packet;
	unsigned int	time;
};
list<PacketInfo>	sDelayPacket;

#endif


UDPStream::UDPStream()
{
	mSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (mSocket == INVALID_SOCKET)
	{
		mSocket = 0;
		return;
	}
}

StreamState UDPStream::GetState()
{
#ifdef FAKE_LAG

	// send any delayed packets 
	unsigned int time = gTime.GetTicks();
	list<PacketInfo>::iterator it;
	for (it = sDelayPacket.begin(); it != sDelayPacket.end();)
	{
		if ((it->time + cSendDelay) < time)
		{
			Send(it->packet->GetSockAddress(), it->packet);
			delete it->packet;
			it = sDelayPacket.erase(it);
		}
		else
		{
			++it;
		}
	}
#endif

	return NetworkStream::GetState();
}

int UDPStream::Send(const SocketAddr& toAddr, Packet* packet)
{
#ifdef FAKE_PACKETLOSS

	int r = rand() % 100;
	if (r < cPacketLoss)
		return packet->GetSize();

#endif

#ifdef FAKE_LAG

	// see if packet is already in the queue, if it is, send it
	bool found = false;
	list<PacketInfo>::iterator it;
	for (it = sDelayPacket.begin(); it != sDelayPacket.end(); ++it)
	{
		if (it->packet == packet)
		{
			found = true;
			break;
		}
	}

	if (!found)
	{
		Packet* p = new Packet();
		p->Append(packet->GetBuffer(), packet->GetSize());
		p->SetSockAddress(toAddr);

		PacketInfo info;
		info.packet = p;
		info.time = gTime.GetTicks();
		sDelayPacket.push_back(info);
		return packet->GetSize();
	}

	// send anything in the queue older than cSendDelay
#endif

	
	MUTEX_BEGIN
	int bytes = sendto(mSocket, packet->GetBuffer(), packet->GetSize(), 0, (SOCKADDR*)&toAddr, sizeof(SocketAddr));
	MUTEX_END

	//cout << "s " << bytes << endl;
	return bytes;
}

bool UDPStream::Bind(const SocketAddr& onAddr)
{
	MUTEX_BEGIN
	if (bind(mSocket, (SOCKADDR*)&onAddr, sizeof(SocketAddr)) == SOCKET_ERROR) 
	{
		MUTEX_END
		return false;
	}

	mBindAddr = onAddr;
	MUTEX_END
	return true;
}

int UDPStream::Receive(Packet* packet)
{
	MUTEX_BEGIN
	sockaddr_in rcvAddr;
	int i = sizeof(SOCKADDR);
	char buffer[UDP_BUFFER_SIZE];

	int bytesReceived = recvfrom(mSocket, buffer, UDP_BUFFER_SIZE, 0, (SOCKADDR*)&rcvAddr, &i);

	if (bytesReceived <= 0)
	{
		MUTEX_END
		return bytesReceived;
	}

	packet->Append(buffer, bytesReceived);
	packet->SetSockAddress(rcvAddr);
	MUTEX_END

	//cout << "r " << bytesReceived << endl;
	return bytesReceived;
}
