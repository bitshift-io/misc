#pragma comment(lib, "Ws2_32.lib")

#include "Network.h"
#include "File/Log.h"

ostream& operator <<(ostream& os, const SocketAddr& addr)
{
	unsigned char* ip = (unsigned char*)&(addr.sin_addr.s_addr);
	os << (int)ip[0] << "." << (int)ip[1] << "." << (int)ip[2] << "." << (int)ip[3] << ":" << (int)ntohs(addr.sin_port);
	return os;
}

Network::Network()
{
	// start winsock
	WSAData wsaData;
	int nCode;
	if ((nCode = WSAStartup(MAKEWORD(2, 2), &wsaData)) != 0) 
	{
//		Log::Print("WSAStartup returned error code %i\n", nCode);
	}
}

Network::~Network()
{
	// shutdown winsock
	WSACleanup();
}

unsigned long Network::ResolveAddress(const char* addr)
{
	unsigned long remoteAddr = inet_addr(addr);
	if (remoteAddr == INADDR_NONE) 
	{
		// pcHost isn't a dotted IP, so resolve it through DNS
		hostent* he = gethostbyname(addr);
		if (he == 0) 
			return INADDR_NONE;

		remoteAddr = *((unsigned long*)he->h_addr_list[0]);
	}

	return remoteAddr;
}

SocketAddr Network::GetAddress(const char* addr, unsigned int port)
{
	sockaddr_in adr;
	ZeroMemory(&adr, sizeof(sockaddr_in));
	adr.sin_family = AF_INET;
	adr.sin_port = htons(port);
	adr.sin_addr.s_addr = ResolveAddress(addr);

	return adr;
}

SocketAddr Network::GetBroadcastAddress(unsigned int port)
{
	sockaddr_in adr;
	ZeroMemory(&adr, sizeof(sockaddr_in));
	adr.sin_family = AF_INET;

	adr.sin_port = htons(port);
	adr.sin_addr.s_addr = htonl(INADDR_ANY);
	return adr;
}

SocketAddr Network::GetAnyAddress(unsigned int port)
{
	sockaddr_in adr;
	ZeroMemory(&adr, sizeof(sockaddr_in));
	adr.sin_family = AF_INET;

	adr.sin_port = htons(port);
	adr.sin_addr.s_addr = htonl(INADDR_ANY);
	return adr;
}

SocketAddr Network::GetInvalidAddress()
{
	sockaddr_in adr;
	memset(&adr, 0, sizeof(sockaddr_in));
	return adr;
}

bool Network::Equals(const SocketAddr& a, const SocketAddr& b)
{
	if (a.sin_addr.s_addr == b.sin_addr.s_addr 
		&& a.sin_port == b.sin_port)
		return true;

	return false;
}

int Network::GetLastError()
{
	return WSAGetLastError();
}
