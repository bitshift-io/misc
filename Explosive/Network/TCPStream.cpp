#include "TCPStream.h"
#include "Packet.h"

const int UDP_BUFFER_SIZE = 2048;

TCPStream::TCPStream(int flags)
{
	mSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mSocket == INVALID_SOCKET)
	{
		mSocket = 0;
		return;
	}

	if ((flags & eReuseAddr) == eReuseAddr)
	{
		bool yes = true;
		int err = setsockopt(mSocket, SOL_SOCKET, SO_REUSEADDR, (const char*)&yes, sizeof(yes));
		if (err != 0)
		{
			closesocket(mSocket);
			mSocket = 0;
			return;
		}
	}

	if ((flags & eNoDelay) == eNoDelay)
	{
		bool yes = true;
		int err = setsockopt(mSocket, SOL_SOCKET, TCP_NODELAY, (const char*)&yes, sizeof(bool));
		if (err == SOCKET_ERROR)
		{
			closesocket(mSocket);
			mSocket = 0;
			return;
		}
	}
}

bool TCPStream::Bind(const SocketAddr& onAddr)
{
	if (bind(mSocket, (SOCKADDR*)&onAddr, sizeof(SocketAddr)) == SOCKET_ERROR) 
		return false;

	return true;
}

bool TCPStream::Connect(const SocketAddr& toAddr)
{
	if (connect(mSocket, (SOCKADDR*)&toAddr, sizeof(SocketAddr)) == SOCKET_ERROR) 
		return false;

	return true;
}

bool TCPStream::Listen()
{
	if (listen(mSocket, 1) == SOCKET_ERROR) 
		return false;

	return true;
}

bool TCPStream::Accept(TCPStream& stream)
{
	SocketAddr clientRemote;
	int addrSize = sizeof(clientRemote);

	stream.mSocket = accept(mSocket, (sockaddr*)&clientRemote, &addrSize);
	return stream.mSocket != SOCKET_ERROR;
}

int TCPStream::Send(Packet* packet)
{
	return send(mSocket, packet->GetBuffer(), packet->GetSize(), 0);
}

int TCPStream::Receive(Packet* packet, unsigned int maxBytes)
{
	char buffer[UDP_BUFFER_SIZE];
	int bytesToReceive = UDP_BUFFER_SIZE;
	int bytesReceived = 0;
	int totalBytesReceived = 0;

	do
	{
		bytesReceived = recv(mSocket, buffer, UDP_BUFFER_SIZE, 0);
		totalBytesReceived += bytesReceived;

		if (bytesReceived)
			packet->Append(buffer, bytesReceived);

		if (maxBytes != -1)
			bytesToReceive -= bytesReceived;

	} while (bytesToReceive && bytesReceived);

	return bytesReceived;
}