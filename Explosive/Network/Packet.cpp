#include "Packet.h"

Packet::~Packet()
{
	delete mBuffer;
}

int Packet::Append(const void* data, unsigned int size)
{
	unsigned int newSize = mSize + size;
	char* newBuffer = new char[newSize];
	memcpy(newBuffer, mBuffer, mSize);
	memcpy(newBuffer + mSize, data, size);
	delete mBuffer;
	mBuffer = newBuffer;
	mSize = newSize;
	return size;
}

int Packet::Read(void* data, unsigned int size)
{
	int actualSize = min(size, mSize - mCursor);
	memcpy(data, mBuffer + mCursor, actualSize);
	mCursor += size;
	return actualSize;
}

int Packet::Peek(void* data, unsigned int size, unsigned int offset) const
{
	if (size > mSize)
		return 0;

	memcpy(data, mBuffer + offset, size);
	return size;
}

void Packet::SetSockAddress(const SocketAddr& addr)			
{ 
	mSocketAddress = addr;
}