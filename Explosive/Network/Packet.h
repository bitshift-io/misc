#ifndef _PACKET_H_
#define _PACKET_H_

#include "Network.h"

class Packet
{
public:

	Packet() : mBuffer(0), mSize(0), mCursor(0)									{ }
	~Packet();

	int					Append(const void* data, unsigned int size);
	int					Read(void* data, unsigned int size);
	int					Peek(void* data, unsigned int size, unsigned int offset = 0) const;

	char*				GetBuffer()	const								{ return mBuffer; }
	unsigned int		GetSize() const									{ return mSize; }

	void				SetSockAddress(const SocketAddr& addr);
	const SocketAddr&	GetSockAddress() const							{ return mSocketAddress; }

protected:
	
	char*				mBuffer;
	unsigned int		mSize;
	unsigned int		mCursor;

	SocketAddr			mSocketAddress;
};

#endif