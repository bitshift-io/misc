#ifndef _TCPSTREAM_H_
#define _TCPSTREAM_H_

#include "Network.h"
#include "NetworkStream.h"

class Packet;

enum TCPFlags
{
	eNoDelay	= 0x1 << 0,
	eReuseAddr	= 0x1 << 1,
};

class TCPStream : public NetworkStream
{
public:

	TCPStream(int flags = eNone);

	bool Bind(const SocketAddr& onAddr);
	bool Connect(const SocketAddr& toAddr);

	bool Listen();
	bool Accept(TCPStream& stream);

	int Send(Packet* packet);
	int Receive(Packet* packet, unsigned int maxBytes = -1);

protected:

};

#endif