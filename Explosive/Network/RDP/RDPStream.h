#ifndef _RDPSTREAM_H_
#define _RDPSTREAM_H_

#include "RDPChannel.h"
#include "../Network.h"
#include "../UDPStream.h"

class RDPState;
class Packet;

//
// Reliable Data Protocol Stream
//
class RDPStream
{
	friend class RDPClosed;
	friend class RDPListen;
	friend class RDPStreamMgr;
	friend class RDPConnecting;
	friend class RDPSyncSent;
	friend class RDPSyncReceived;
	friend class RDPWaitClose;
	friend class RDPOpen;
	friend class RDPStreamMgr;

	friend class RDPChannel;
	friend class RDPUnreliableUnorderedChannel;
	friend class RDPReliableUnorderedChannel;
	friend class RDPUnreliableOrderedChannel;
	friend class RDPReliableOrderedChannel;

public:

	typedef list<RDPChannel*>::iterator Iterator;

	RDPStream();
	RDPStream(const RDPStream& copy);
	~RDPStream();

	void operator=(const RDPStream& copy);

	void				Close();
	bool				Bind(const SocketAddr& onAddr);
	bool				Listen();
	bool				Accept(RDPStream& stream);
	bool				Connect(const SocketAddr& toAddr, bool blocking = true); // second addr for nat punch through
	StreamState			GetState(RDPChannelHandle handle = -1);

	// some packets need to be send in order,
	// but most dont, so what you do is if you have 
	// a bunch of packets you want sent in order,
	// create an ordered stream id
	// then send and receive using this id
	RDPChannelHandle	CreateChannel(RDPChannelType type);

	int					Send(Packet* packet, RDPChannelHandle handle = -1);
	int					Receive(Packet* packet, unsigned int maxBytes = -1, RDPChannelHandle handle = -1);

	const SocketAddr&	GetBindAddr()										{ return mBindAddr; }
	const SocketAddr&	GetConnectAddr()									{ return mConnectAddr; }

	bool				IsValid();

	Iterator			Begin()												{ return mChannel.begin(); }
	Iterator			End()												{ return mChannel.end(); }

protected:

	bool				IsListening();
	void				ReceiveInternal(Packet* packet);
	RDPChannel*			GetChannel(RDPChannelHandle handle);
	RDPChannel*			CreateChannel(RDPChannelType type, RDPChannelHandle handle);

	void				UpdateState();
	void				Update();

	RDPState*			mState;
	RDPState*			mNextState;
	SocketAddr			mBindAddr;
	SocketAddr			mConnectAddr;
	UDPStream*			mUDPStream;
	list<RDPChannel*>	mChannel;
};

#endif