#include "RDPChannel.h"
#include "RDPStream.h"
#include "RDPMessage.h"
#include "../UDPStream.h"
#include "../Packet.h"
#include "RDPStreamMgr.h"

const int cMaxResend = 4;

RDPChannel::RDPChannel(RDPChannelHandle handle, RDPStream* stream) : 
	mHandle(handle),
	mStream(stream)
{
	memset(&mStats, 0, sizeof(NetworkStatistics));
}

int RDPChannel::SendInternal(Packet* packet)
{
	int bytes = mStream->mUDPStream->Send(packet->GetSockAddress(), packet);
	mStats.bytesSent += bytes;
	return bytes;
}

void RDPChannel::ReceiveInternal(Packet* packet)
{
	mStats.bytesReceived += packet->GetSize();
}

void RDPChannel::Update()
{
	mStats.totalBytesSent += mStats.bytesSent;
	mStats.totalBytesReceived += mStats.bytesReceived;

	mStats.bytesSent = 0;
	mStats.bytesReceived = 0;
}


//========================================================================================
// Reliable Unordered Channel
//========================================================================================

RDPReliableUnorderedChannel::RDPReliableUnorderedChannel(RDPChannelHandle handle, RDPStream* stream) : RDPChannel(handle, stream),
	mPacketId(0),
	mHighestPacketReceived(0),
	mAllConfirmedPacket(0)
{

}

int RDPReliableUnorderedChannel::Send(Packet* packet)
{
	RDPHead head;
	head.messageType = MT_Reliable;
	head.channelId = mHandle;
	head.packetId = ++mPacketId;

	Packet* p = new Packet;
	p->Append(&head, sizeof(RDPHead));
	p->Append(packet->GetBuffer(), packet->GetSize());
	p->SetSockAddress(mStream->GetConnectAddr());
	mOut.push_back(p);
	return SendInternal(p);
}

int RDPReliableUnorderedChannel::Receive(Packet* packet, unsigned int maxBytes)
{
	mMutex.Lock();
	// if you get an error about not being able to derefernce the iterator
	// you are probably not passing the correct handle to get the correct channel
	Packet* p = mIn.front();
	mIn.erase(mIn.begin());
	mMutex.Unlock();

	packet->Append(p->GetBuffer() + sizeof(RDPHead), p->GetSize() - sizeof(RDPHead));
	packet->SetSockAddress(mStream->GetConnectAddr());
	delete p;
	return packet->GetSize();
}

void RDPReliableUnorderedChannel::ReceiveInternal(Packet* packet)
{
	RDPChannel::ReceiveInternal(packet);

	RDPHead head = RDPMessage::PeekHeader(packet);

	switch (head.messageType)
	{/*
	// we should have a message where we send across the lowest id so we can 
	// flush out packets that have been confirmed
	case MT_Request:
		{
			mMutex.Lock();

			list<Packet*>::iterator it;
			for (it = mOut.begin(); it != mOut.end(); ++it)
			{
				RDPHead itHead = RDPMessage::PeekHeader(*it);
				if (itHead.packetId == head.packetId)
				{
					SendInternal(*it);
					break;
				}
			}

			mMutex.Unlock();
		}
		break;*/

	case MT_CreateChannel:
		{
			mStream->CreateChannel((RDPChannelType)head.channelType, head.channelId);
		}
		break;

	case MT_Reliable:
		{	
			// discard any packets lower than the lowest packet id
			if (head.packetId <= mAllConfirmedPacket)
			{
				delete packet;
				return;
			}

			mMutex.Lock();

			mIn.push_back(packet);			

			// store the highest packet id received
			mHighestPacketReceived = max(mHighestPacketReceived, head.packetId);

			// if this packet is the next packet received
			if (mAllConfirmedPacket + 1 == head.packetId)
			{
				++mAllConfirmedPacket;
			}
			else
			{
				// this packet doesnt increase all confirmed packet count
				mAckId.insert(head.packetId);

				mMutex.Unlock();
				return;
			}

			// iterate through the acknowledged set till we reach our new limit
			// for all acknowledged packets
			{
				set<unsigned int, IntSort>::iterator it = mAckId.begin();
				while (it != mAckId.end() && (*it) == mAllConfirmedPacket + 1)
				{
					++mAllConfirmedPacket;
					it = mAckId.erase(it);
				}
			}

			mMutex.Unlock();
		}
		break;

	case MT_LowestId:
		{
			mMutex.Lock();

			// remove all packets with lower id than this....
			list<Packet*>::iterator it;
			for (it = mOut.begin(); it != mOut.end(); )
			{
				RDPHead outHead = RDPMessage::PeekHeader(*it);
				if (outHead.packetId <= head.packetId)
				{
					delete (*it);
					it = mOut.erase(it);
				}
				else
				{
					break;
				}
			}

			mMutex.Unlock();
			delete packet;
		}
		break;

	case MT_PacketId:
		{
			mHighestPacketReceived = max(mHighestPacketReceived, head.packetId);
			delete packet;

			// the other side still has packets, so send it the packets we know about
			{
				RDPHead head;
				head.messageType = MT_LowestId;
				head.channelId = mHandle;
				head.packetId = mAllConfirmedPacket;
			
				Packet p;
				p.Append(&head, sizeof(RDPHead));
				p.SetSockAddress(mStream->GetConnectAddr());
				SendInternal(&p);
			}
		}
		break;

	default:
		delete packet;
		break;
	}
}

void RDPReliableUnorderedChannel::CreateChannel(RDPChannelType type, RDPChannelHandle handle)
{/*
	RDPHead head;
	head.messageType = MT_CreateChannel;
	head.channelId = handle;
	head.channelType = type;
	head.packetId = ++mPacketId;

	Packet* p = new Packet;
	p->Append(&head, sizeof(RDPHead));
	mOut.push_back(p);
	SendInternal(p);*/
}

void RDPReliableUnorderedChannel::Update()
{
	mMutex.Lock();
	RDPChannel::Update();
/*
	//unsigned int requestCount = gRDPStreamMgr.GetResendPacketCount();
	if (mAllConfirmedPacket != mHighestPacketReceived)
	{
		// request missing packets
		unsigned int sendCount = 0;
		set<unsigned int, IntSort>::iterator it = mAckId.begin();
		for (unsigned int i = mAllConfirmedPacket; i <= mHighestPacketReceived; ++i)
		{
			// incease the iterator till its bigger or equal to i
			while (it != mAckId.end() && *it <= i)
				++it;

			int maxId = (it == mAckId.end()) ? mHighestPacketReceived : *it;
			for (int j = i; j <= maxId; ++j)
			{
				RDPHead head;
				head.messageType = MT_Request;
				head.channelId = mHandle;
				head.packetId = j;

				Packet p;
				p.Append(&head, sizeof(head));
				p.SetSockAddress(mStream->GetConnectAddr());
				int bytes = SendInternal(&p);
				++sendCount;

				//if (sendCount >= requestCount)
				//	goto done;
			}
		}
	}
*/
//done:

	mMutex.Unlock();
/*
	// send lowest packet id so other end can discard packets lower than this number
	if (mHighestPacketReceived != mAllConfirmedPacket)
	{
		RDPHead head;
		head.messageType = MT_LowestId;
		head.channelId = mHandle;
		head.packetId = mAllConfirmedPacket;
	
		Packet p;
		p.Append(&head, sizeof(RDPHead));
		p.SetSockAddress(mStream->GetConnectAddr());
		SendInternal(&p);
	}
*/
	// send the packet id, so other end knows if its missing any packets
	if (mOut.size())
	{
		RDPHead head;
		head.messageType = MT_PacketId;
		head.channelId = mHandle;
		head.packetId = mPacketId;
	
		Packet p;
		p.Append(&head, sizeof(RDPHead));
		p.SetSockAddress(mStream->GetConnectAddr());
		if (SendInternal(&p) == -1)
		{
			int error = gNetwork.GetLastError();
			int nothing = 0;
		}

		// spam these packets
		list<Packet*>::iterator it;
		int i = 0;
		for (it = mOut.begin(); it != mOut.end(); ++it, ++i)
		{
			SendInternal(*it);

			if (i >= cMaxResend)
				break;
		}

		//cout << "packet que size: " << mOut.size() << endl;
	}
/*
	cout << "other ends confirmed packets: " << mAllConfirmedPacket << endl;
	cout << "highest packet received: " << mHighestPacketReceived << endl;
	cout << "current packet id: " << mPacketId << endl;
	cout << "packet que size: " << mOut.size() << endl;*/
}

StreamState	RDPReliableUnorderedChannel::GetState()
{
	mMutex.Lock();
	int size = mIn.size();
	mMutex.Unlock();

	return size > 0 ? eRead : eNone;
}




//========================================================================================
// Reliable Ordered Channel
//========================================================================================

RDPReliableOrderedChannel::RDPReliableOrderedChannel(RDPChannelHandle handle, RDPStream* stream) : RDPChannel(handle, stream),
	mPacketId(0),
	mHighestPacketReceived(0),
	mAllConfirmedPacket(0)
{

}

int RDPReliableOrderedChannel::Send(Packet* packet)
{
	RDPHead head;
	head.messageType = MT_Reliable;
	head.channelId = mHandle;
	head.packetId = ++mPacketId;

	Packet* p = new Packet;
	p->Append(&head, sizeof(RDPHead));
	p->Append(packet->GetBuffer(), packet->GetSize());
	p->SetSockAddress(mStream->GetConnectAddr());
	mOut.push_back(p);
	return SendInternal(p);
}

int RDPReliableOrderedChannel::Receive(Packet* packet, unsigned int maxBytes)
{
	mMutex.Lock();
	Packet* p = mIn.front();
	RDPHead head = RDPMessage::PeekHeader(p);
	mIn.erase(mIn.begin());
	mMutex.Unlock();

	packet->Append(p->GetBuffer() + sizeof(RDPHead), p->GetSize() - sizeof(RDPHead));
	packet->SetSockAddress(mStream->GetConnectAddr());
	delete p;
	return packet->GetSize();
}

void RDPReliableOrderedChannel::ReceiveInternal(Packet* packet)
{
	RDPChannel::ReceiveInternal(packet);

	RDPHead head = RDPMessage::PeekHeader(packet);

	switch (head.messageType)
	{/*
	// we should have a message where we send across the lowest id so we can 
	// flush out packets that have been confirmed
	case MT_Request:
		{
			mMutex.Lock();

			list<Packet*>::iterator it;
			for (it = mOut.begin(); it != mOut.end(); ++it)
			{
				RDPHead itHead = RDPMessage::PeekHeader(*it);
				if (itHead.packetId == head.packetId)
				{
					SendInternal(*it);
					break;
				}
			}

			mMutex.Unlock();
		}
		break;
*/
	case MT_CreateChannel:
		{
			mStream->CreateChannel((RDPChannelType)head.channelType, head.channelId);
		}
		break;

	case MT_Reliable:
		{
			// discard any packets lower than the lowest packet id
			if (head.packetId <= mAllConfirmedPacket)
			{
				delete packet;
				return;
			}

			mMutex.Lock();

			// store the highest packet id received
			mHighestPacketReceived = max(mHighestPacketReceived, head.packetId);

			// if this packet is the next packet received
			if (mAllConfirmedPacket + 1 == head.packetId)
			{
				mIn.push_back(packet);
				++mAllConfirmedPacket;
			}
			else
			{
				// NOTE: generally this packet will go at the end of the list,
				// we could get an optimisation here be checking backwards...

				// we received an out of order packet, 
				// so insert it in order
				list<Packet*>::iterator it;
				for (it = mInUnordered.begin(); it != mInUnordered.end(); ++it)
				{
					RDPHead itHead = RDPMessage::PeekHeader(*it);

					// already in the list
					if (head.packetId == itHead.packetId)
					{
						delete packet;
						mMutex.Unlock();
						return;
					}

					if (head.packetId < itHead.packetId)
					{	
						mInUnordered.insert(it, packet);
						break;
					}
				}

				if (it == mInUnordered.end())
					mInUnordered.push_back(packet);

				// this packet doesnt increase all confirmed packet count
				mAckId.insert(head.packetId);

				mMutex.Unlock();
				return;
			}

			// so we recieved the next packet in order, so see if out list of in, unreceived packets
			// come next...

			// iterate through the acknowledged set till we reach our new limit
			// for all acknowledged packets
			{
				list<Packet*>::iterator unorderedIt = mInUnordered.begin();
				set<unsigned int, IntSort>::iterator it = mAckId.begin();
				while (it != mAckId.end() && (*it) == mAllConfirmedPacket + 1)
				{
					++mAllConfirmedPacket;
					it = mAckId.erase(it);

					mIn.push_back(*unorderedIt);
					unorderedIt = mInUnordered.erase(unorderedIt);
				}
			}

			mMutex.Unlock();
		}
		break;

	case MT_LowestId:
		{
			mMutex.Lock();

			// remove all packets with lower id than this....
			list<Packet*>::iterator it;
			for (it = mOut.begin(); it != mOut.end(); )
			{
				RDPHead outHead = RDPMessage::PeekHeader(*it);
				if (outHead.packetId <= head.packetId)
				{
					delete (*it);
					it = mOut.erase(it);
				}
				else
				{
					break;
				}
			}

			mMutex.Unlock();
			delete packet;
		}
		break;

	case MT_PacketId:
		{
			mHighestPacketReceived = max(mHighestPacketReceived, head.packetId);
			delete packet;

			// the other side still has packets, so send it the packets we know about
			{
				RDPHead head;
				head.messageType = MT_LowestId;
				head.channelId = mHandle;
				head.packetId = mAllConfirmedPacket;
			
				Packet p;
				p.Append(&head, sizeof(RDPHead));
				p.SetSockAddress(mStream->GetConnectAddr());
				SendInternal(&p);

				//cout << "other side highest packet id kown about: " << mHighestPacketReceived << "    all confirmed packets: " << mAllConfirmedPacket << endl;
			}
		}
		break;

	default:
		delete packet;
		break;
	}
}

void RDPReliableOrderedChannel::CreateChannel(RDPChannelType type, RDPChannelHandle handle)
{/*
	RDPHead head;
	head.messageType = MT_CreateChannel;
	head.channelId = handle;
	head.channelType = type;
	head.packetId = ++mPacketId;

	Packet* p = new Packet;
	p->Append(&head, sizeof(RDPHead));
	mOut.push_back(p);
	SendInternal(p);*/
}

void RDPReliableOrderedChannel::Update()
{
	mMutex.Lock();
	RDPChannel::Update();
/*
	//unsigned int requestCount = gRDPStreamMgr.GetResendPacketCount();
	if (mAllConfirmedPacket != mHighestPacketReceived)
	{
		// request missing packets
		unsigned int sendCount = 0;
		set<unsigned int, IntSort>::iterator it = mAckId.begin();
		for (unsigned int i = mAllConfirmedPacket; i <= mHighestPacketReceived; ++i)
		{
			// incease the iterator till its bigger or equal to i
			while (it != mAckId.end() && *it <= i)
				++it;

			int maxId = (it == mAckId.end()) ? mHighestPacketReceived : *it;
			for (int j = i; j <= maxId; ++j)
			{
				RDPHead head;
				head.messageType = MT_Request;
				head.channelId = mHandle;
				head.packetId = j;

				Packet p;
				p.Append(&head, sizeof(head));
				p.SetSockAddress(mStream->GetConnectAddr());
				int bytes = SendInternal(&p);
				++sendCount;

				//if (sendCount >= requestCount)
				//	goto done;
			}
		}
	}*/

//done:

	mMutex.Unlock();
/*
	// send lowest packet id so other end can discard packets lower than this number
	//if (mHighestPacketReceived != mAllConfirmedPacket)
	{
		RDPHead head;
		head.messageType = MT_LowestId;
		head.channelId = mHandle;
		head.packetId = mAllConfirmedPacket;
	
		Packet p;
		p.Append(&head, sizeof(RDPHead));
		p.SetSockAddress(mStream->GetConnectAddr());
		SendInternal(&p);

		//cout << "other side highest packet id kown about: " << mHighestPacketReceived << "    all confirmed packets: " << mAllConfirmedPacket << endl;
	}
*/
	// send the packet id, so other end knows if its missing any packets
	// if we have packets qued up, then the other end hasnt received all packets
	if (mOut.size())
	{
		RDPHead head;
		head.messageType = MT_PacketId;
		head.channelId = mHandle;
		head.packetId = mPacketId;
	
		Packet p;
		p.Append(&head, sizeof(RDPHead));
		p.SetSockAddress(mStream->GetConnectAddr());
		if (SendInternal(&p) == -1)
		{
			int error = gNetwork.GetLastError();
			int nothing = 0;
		}

		// spam these packets
		list<Packet*>::iterator it;
		int i = 0;
		for (it = mOut.begin(); it != mOut.end(); ++it, ++i)
		{
			SendInternal(*it);

			if (i >= cMaxResend)
				break;
		}

		//cout << "packet que size: " << mOut.size() << endl;
		//cout << "this side highest packet id: " << mPacketId << "    remaning packets: " << mOut.size() << endl;
	}

	//cout << "other ends confirmed packets: " << mAllConfirmedPacket << endl;
	//cout << "highest packet received: " << mHighestPacketReceived << endl;
	//cout << "current packet id: " << mPacketId << endl;
	//cout << "packet count: " << mPacketId << endl;
}

StreamState	RDPReliableOrderedChannel::GetState()
{
	mMutex.Lock();
	int size = mIn.size();
	mMutex.Unlock();

	return size > 0 ? eRead : eNone;
}




//========================================================================================
// Unreliable Unordered Channel
//========================================================================================

RDPUnreliableUnorderedChannel::RDPUnreliableUnorderedChannel(RDPChannelHandle handle, RDPStream* stream) : RDPChannel(handle, stream)
{

}

int	RDPUnreliableUnorderedChannel::Send(Packet* packet)
{
	RDPHead head;
	head.messageType = MT_Reliable;
	head.channelId = mHandle;
	head.packetId = 0;

	Packet p;
	p.Append(&head, sizeof(RDPHead));
	p.Append(packet->GetBuffer(), packet->GetSize());
	p.SetSockAddress(mStream->GetConnectAddr());
	return mStream->mUDPStream->Send(mStream->GetConnectAddr(), &p);
}

int	RDPUnreliableUnorderedChannel::Receive(Packet* packet, unsigned int maxBytes)
{
	mMutex.Lock();
	Packet* p = mIn.front();
	mIn.erase(mIn.begin());
	mMutex.Unlock();

	packet->Append(p->GetBuffer() + sizeof(RDPHead), p->GetSize() - sizeof(RDPHead));
	packet->SetSockAddress(mStream->GetConnectAddr());
	delete p;
	return packet->GetSize();
}

void RDPUnreliableUnorderedChannel::ReceiveInternal(Packet* packet)
{
	mMutex.Lock();
	RDPChannel::ReceiveInternal(packet);
	mIn.push_back(packet);
	mMutex.Unlock();
}

StreamState	RDPUnreliableUnorderedChannel::GetState()
{
	mMutex.Lock();
	int size = mIn.size();
	mMutex.Unlock();

	return size > 0 ? eRead : eNone;
}


//========================================================================================
// Unreliable Ordered Channel
//========================================================================================

RDPUnreliableOrderedChannel::RDPUnreliableOrderedChannel(RDPChannelHandle handle, RDPStream* stream) : RDPChannel(handle, stream),
	mPacketId(0),
	mHighestPacketReceived(0)
{

}

int	RDPUnreliableOrderedChannel::Send(Packet* packet)
{
	RDPHead head;
	head.messageType = MT_Reliable;
	head.channelId = mHandle;
	head.packetId = ++mPacketId;

	Packet p;
	p.Append(&head, sizeof(RDPHead));
	p.Append(packet->GetBuffer(), packet->GetSize());
	p.SetSockAddress(mStream->GetConnectAddr());
	return mStream->mUDPStream->Send(mStream->GetConnectAddr(), &p);
}

int	RDPUnreliableOrderedChannel::Receive(Packet* packet, unsigned int maxBytes)
{
	mMutex.Lock();
	Packet* p = mIn.front();
	mIn.erase(mIn.begin());
	mMutex.Unlock();

	packet->Append(p->GetBuffer() + sizeof(RDPHead), p->GetSize() - sizeof(RDPHead));
	packet->SetSockAddress(mStream->GetConnectAddr());
	delete p;
	return packet->GetSize();
}

void RDPUnreliableOrderedChannel::ReceiveInternal(Packet* packet)
{
	RDPChannel::ReceiveInternal(packet);
	RDPHead head = RDPMessage::PeekHeader(packet);
	if (head.packetId <= mHighestPacketReceived)
	{
		delete packet;
		return;
	}
	mHighestPacketReceived = head.packetId;

	mMutex.Lock();
	mIn.push_back(packet);
	mMutex.Unlock();
}

StreamState	RDPUnreliableOrderedChannel::GetState()
{
	mMutex.Lock();
	int size = mIn.size();
	mMutex.Unlock();

	return size > 0 ? eRead : eNone;
}