#ifndef _RDPMESSAGE_H_
#define _RDPMESSAGE_H_

class Packet;

// http://www.faqs.org/rfcs/rfc908.html
enum RDPMessageType
{
	MT_Synchronize		= 0x1 << 0,
	MT_Acknowledge		= 0x1 << 1,
	MT_Reset			= 0x1 << 2,
	MT_Request			= 0x1 << 3,
	MT_CreateChannel	= 0x1 << 4,
	MT_Reliable			= 0x1 << 5,	
	MT_PacketId			= 0x1 << 6,
	MT_LowestId			= 0x1 << 7,
};

struct RDPHead
{
	RDPHead() : channelId(-1)
	{
	}

	unsigned char messageType;
	unsigned int channelId;		// used for substreams

	union
	{
		unsigned int packetId;
		unsigned int channelType;
	};
};

//
// Helper functions to generate, get packet header
//
class RDPMessage
{
public:

	static RDPHead PeekHeader(const Packet* p);
};

#endif