#ifndef _RDPLISTEN_H_
#define _RDPLISTEN_H_

#include "RDPState.h"
#include <list>
#include "Multithread/Mutex.h"

using namespace std;

class RDPStream;

class RDPListen : public RDPState
{
public:

	explicit RDPListen(RDPStream* stream) : RDPState(stream) { }
 
	virtual bool			Bind(const SocketAddr& onAddr);
	virtual bool			Listen();

	// only returns true once a connection has been made and accepted!
	virtual bool			Accept(RDPStream& stream);
	virtual StreamState		GetState(RDPChannelHandle handle);

	virtual bool			Update();
	virtual bool			Connect(const SocketAddr& toAddr, bool blocking);
	virtual RDPStateType	GetType() { return ST_Listen; }
	virtual void			ReceiveInternal(Packet* packet);
	virtual void			Close() { }

	virtual int				Send(Packet* packet, RDPChannelHandle handle);
	virtual int				Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle);
	int						SendInternal(Packet* packet, const SocketAddr& toAddr);

protected:
	
	typedef list<RDPStream*>::iterator Iterator;

	Iterator				GetStream(const SocketAddr& fromAddr, list<RDPStream*>& source);

	list<RDPStream*>						mNATPunch;
	list<RDPStream*>						mConnecting;
	list<RDPStream*>						mConnected;
	//list<RDPStream*>						mAccepted;		// this could just be a list of sockaddr's
	Mutex									mMutex;
};

#endif