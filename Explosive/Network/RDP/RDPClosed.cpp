#include "RDPClosed.h"
#include "RDPStream.h"
#include "RDPListen.h"
#include "RDPConnecting.h"
#include "RDPOpen.h"
#include "RDPStreamMgr.h"

RDPClosed::RDPClosed(RDPStream* stream) : RDPState(stream), 
	mNextState(0) 
{
}

bool RDPClosed::Bind(const SocketAddr& onAddr)
{
	mStream->mBindAddr = onAddr;
	mStream->mUDPStream = gRDPStreamMgr.GetUDPStream(onAddr);
	return mStream->mUDPStream != 0;
}

void RDPClosed::SetState()
{
	if (mStream->mUDPStream)
	{
		gRDPStreamMgr.ReleaseUDPStream(mStream->mUDPStream);
		mStream->mUDPStream = 0;
	}

	mStream->mBindAddr = gNetwork.GetInvalidAddress();
	mStream->mConnectAddr = gNetwork.GetInvalidAddress();
}

bool RDPClosed::Listen()
{
	mStream->mNextState = new RDPListen(mStream);
	return true;
}

bool RDPClosed::Accept(RDPStream& stream)
{
	return false;
}

StreamState RDPClosed::GetState()
{
	return eNone;
}

bool RDPClosed::Connect(const SocketAddr& toAddr, bool blocking)
{
	if (!mStream->mUDPStream)
		return false;

	RDPSyncSent* syncState = new RDPSyncSent(mStream, toAddr, blocking);
	mNextState = syncState;
	bool connected = syncState->Update();

	if (!blocking)
	{
		mStream->mNextState = syncState;
		return connected;
	}

	mNextState = 0; // packets could go missing after this :|
	delete syncState;
	return connected;
}

int RDPClosed::Send(Packet* packet, RDPChannelHandle handle)
{
	return 0;
}

int RDPClosed::Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle)
{
	return 0;
}

void RDPClosed::ReceiveInternal(Packet* packet)
{
	if (mNextState)
		mNextState->ReceiveInternal(packet);
}