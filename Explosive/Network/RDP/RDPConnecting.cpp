#include "RDPConnecting.h"
#include "RDPStream.h"
#include "RDPOpen.h"
#include "RDPDisconnecting.h"
#include "RDPClosed.h"
#include "RDPMessage.h"
#include "../Packet.h"
#include "../Time/Time.h"

//temporary
#include <iostream>

#define CONNECTION_TIME_OUT 30000
#define RESEND_TIME			100

RDPSyncReceived::RDPSyncReceived(RDPStream* stream, const SocketAddr& toAddr, const SocketAddr* natAddr) : RDPState(stream)
{
	stream->mConnectAddr = toAddr;
}

bool RDPSyncReceived::Bind(const SocketAddr& onAddr)
{
	return false;
}

bool RDPSyncReceived::Listen()
{
	return false;
}

bool RDPSyncReceived::Accept(RDPStream& stream)
{
	return false;
}

StreamState RDPSyncReceived::GetState(RDPChannelHandle handle)
{
	return eNone;
}

bool RDPSyncReceived::Update()
{
	mMutex.Lock();

	bool found = false;
	list<Packet*>::iterator it;
	for (it = mIncomming.begin(); it != mIncomming.end(); ++it)
	{
		Packet* in = *it;
		RDPHead head = RDPMessage::PeekHeader(in);

		// we have recived an acknowledgement of our request,
		// send back an acknowledge
		if (head.messageType == (MT_Synchronize | MT_Acknowledge))
		{
			found = true;
			break;
		}
	}

	// go to the open state
	if (found)
	{
		RDPOpen* openState = new RDPOpen(mStream);
		mStream->mNextState = openState;

		// foward all remaning packets to the new state //appropriate channel
		for (it = mIncomming.begin(); it != mIncomming.end(); )
		{
			Packet* in = *it;
			it = mIncomming.erase(it);
/*
			RDPHead head = RDPMessage::PeekHeader(in);

			// we could foward the request to create a channel
			// after some packet has been sent to the channel,
			// we will get a crash here, may need to foward these packets later
			// if channel doesnt exist
			RDPChannel* channel = mStream->GetChannel(head.channelId);
			if (channel)
				channel->ReceiveInternal(in);
			else*/

			openState->ReceiveInternal(in);
		}

		mMutex.Unlock();
		return true;
	}

	mMutex.Unlock();
	
	// send an ack of the initial sync message
	Packet packet;
	RDPHead head;
	head.messageType = MT_Synchronize | MT_Acknowledge;
	packet.Append((const char*)&head, sizeof(RDPHead));
	int bytes = SendInternal(&packet);
	if (bytes != packet.GetSize())
		return false;

	return true;
}

bool RDPSyncReceived::Connect(const SocketAddr& toAddr, bool blocking)
{
	return false;
}

void RDPSyncReceived::ReceiveInternal(Packet* packet)
{
	mMutex.Lock();
	mIncomming.push_front(packet);
	mMutex.Unlock();
}

int RDPSyncReceived::SendInternal(Packet* packet)
{
	return mStream->mUDPStream->Send(mStream->mConnectAddr, packet);
}

int RDPSyncReceived::Send(Packet* packet, RDPChannelHandle handle)
{
	return 0;
}

int RDPSyncReceived::Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle)
{
	return 0;
}







RDPSyncSent::RDPSyncSent(RDPStream* stream, const SocketAddr& toAddr, bool blocking, RDPState* fowardReceived) : RDPState(stream),
	mBlocking(blocking),
	mFowardReceived(fowardReceived)
{
	stream->mConnectAddr = toAddr;
	mTime = 0;
	mSleepTime = 0;
}

bool RDPSyncSent::Bind(const SocketAddr& onAddr)
{
	return false;
}

bool RDPSyncSent::Listen()
{
	return false;
}

bool RDPSyncSent::Accept(RDPStream& stream)
{
	return false;
}

StreamState RDPSyncSent::GetState(RDPChannelHandle handle)
{
	return eNone;
}

bool RDPSyncSent::Update()
{
	if (mTime == 0)
		mTime = gTime.GetTicks();

	if (!mBlocking)
	{
		if (gTime.GetTicks() > (mTime + CONNECTION_TIME_OUT))
		{
			mStream->mNextState = new RDPClosed(mStream);
			return false;
		}

		// sleep time stops a non-blocking connect from spamming
		if ((mSleepTime + RESEND_TIME) < gTime.GetTicks())
		{
			InternalUpdate();
			mSleepTime = gTime.GetTicks() + RESEND_TIME;
		}
		return true;
	}

	while (gTime.GetTicks() < (mTime + CONNECTION_TIME_OUT))
	{
		Sleep(RESEND_TIME);

		if (InternalUpdate())
			return true;
	}

	return false;
}

bool RDPSyncSent::InternalUpdate()
{
	mMutex.Lock();

	bool found = false;
	while (mIncomming.size() && !found)
	{
		Packet* in = mIncomming.front();
		RDPHead head = RDPMessage::PeekHeader(in);

		// we have recived an acknowledgement of our request,
		// send back an acknowledge
		if (!found && head.messageType == (MT_Synchronize | MT_Acknowledge))
			found = true;

		delete in;
		mIncomming.erase(mIncomming.begin());
	}

	mMutex.Unlock();

	// go to the open state
	if (found)
	{
		// spam the packet back for a bit, then default to the open state
		//unsigned long time = gTime.GetTicks();
		//while (gTime.GetTicks() < time + RESPOND_TIME_OUT)
		{
			Packet out;
			RDPHead head;
			head.messageType = MT_Synchronize | MT_Acknowledge;
			out.Append((const char*)&head, sizeof(RDPHead));
			int bytes = SendInternal(&out);
			if (bytes != out.GetSize())
				return false;
		}

		mStream->mNextState = new RDPOpen(mStream);
		return true;
	}

	// send a sync message
	Packet out;
	RDPHead head;
	head.messageType = MT_Synchronize;
	out.Append((const char*)&head, sizeof(RDPHead));
	int bytes = SendInternal(&out);
	if (bytes != out.GetSize())
		return false;
/*
	if ((mStream->mStream.GetState() & eRead) == eRead)
	{
		Packet in;
		ReceiveInternal(&in);
		RDPHead head = RDPMessage::PeekHeader(&in);

		// we have recived an acknowledgement of our request,
		// send back an acknowledge
		if (head.messageType == (MT_Synchronize | MT_Acknowledge))
		{
			// spam the packet back for a bit, then default to the open state
			unsigned long time = gTime.GetTicks();
			while (gTime.GetTicks() < time + RESPOND_TIME_OUT)
			{
				SendInternal(&in);
			}
			return true;
		}
	}*/
	return false;
}

bool RDPSyncSent::Connect(const SocketAddr& toAddr, bool blocking)
{
	return false;
}

void RDPSyncSent::ReceiveInternal(Packet* packet)
{
	if (mFowardReceived)
	{
		mFowardReceived->ReceiveInternal(packet);
		return;
	}

	mMutex.Lock();
	mIncomming.push_front(packet);
	mMutex.Unlock();
}

int RDPSyncSent::SendInternal(Packet* packet)
{
	//cout << "sending from RDPSyncSent to: " <<  mStream->mConnectAddr  << endl;

	return mStream->mUDPStream->Send(mStream->mConnectAddr, packet);
}

int RDPSyncSent::Send(Packet* packet, RDPChannelHandle handle)
{
	return 0;
}

int RDPSyncSent::Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle)
{
	return 0;
}