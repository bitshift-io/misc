#ifndef _RDPDISCONNECTING_H_
#define _RDPDISCONNECTING_H_

#include "RDPState.h"
#include "../Packet.h"

class RDPWaitClose : public RDPState
{
public:

	explicit RDPWaitClose(RDPStream* stream);
 
	virtual bool			Bind(const SocketAddr& onAddr);
	virtual bool			Listen();
	virtual bool			Accept(RDPStream& stream);
	virtual StreamState		GetState(RDPChannelHandle handle);
	virtual bool			Connect(const SocketAddr& toAddr, bool blocking);
	virtual RDPStateType	GetType() { return ST_Closed; }
	virtual void			ReceiveInternal(Packet* packet) { }
	virtual void			Close() { }
	virtual bool			Update();
	
	virtual int				Send(Packet* packet, RDPChannelHandle handle);
	virtual int				Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle);

protected:

	unsigned long			mStartTime;
	Packet					mPacket;
};

#endif