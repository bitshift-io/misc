#ifndef _RDPCHANNEL_H_
#define _RDPCHANNEL_H_

#include <list>
#include <set>
#include "Mutex.h"
#include "NetworkStream.h"

using namespace std;

class Packet;
class RDPStream;

typedef unsigned int RDPChannelHandle;

enum RDPChannelType
{
	CT_ReliableUnordered,
	CT_ReliableOrdered,
	CT_UnreliableUnordered,
	CT_UnreliableOrdered,
};


struct NetworkStatistics
{
	unsigned int		totalBytesSent;
	unsigned int		totalBytesReceived;

	unsigned int		bytesSent;
	unsigned int		bytesReceived;
};

//
// What are channels?
// A stream inside a stream. Sub streams provide two things:
// 1. Support for sending in order OR or out of order packets packets OR unreliable transfer
// 2. They can be used for object to object communication
//
class RDPChannel
{
public:

	RDPChannel(RDPChannelHandle	handle, RDPStream* stream);

	RDPChannelHandle GetHandle() const			{ return mHandle; }

	virtual int			Send(Packet* packet) = 0;
	virtual int			Receive(Packet* packet, unsigned int maxBytes) = 0;
	virtual void		ReceiveInternal(Packet* packet);
	virtual int			SendInternal(Packet* packet);
	virtual void		Update();
	virtual StreamState	GetState() = 0;

//protected:

	RDPChannelHandle	mHandle;
	RDPStream*			mStream;

	NetworkStatistics	mStats;
};

//
// Maybe i can use the Adapter pattern here, so 
// i have a reliable stream with a unreliable transfer method
// or the other combinations
//
class RDPReliableUnorderedChannel : public RDPChannel
{
public:
	
	RDPReliableUnorderedChannel(RDPChannelHandle handle, RDPStream* stream);

	virtual int			Send(Packet* packet);
	virtual int			Receive(Packet* packet, unsigned int maxBytes);
	virtual void		ReceiveInternal(Packet* packet);
	void				CreateChannel(RDPChannelType type, RDPChannelHandle handle);
	virtual void		Update();
	virtual StreamState	GetState();

protected:

	struct IntSort
	{
	  bool operator()(const unsigned int& s1, const unsigned int& s2) const
	  {
		return s1 < s2;
	  }
	};

	list<Packet*>				mOut;	// this is be sorted
	list<Packet*>				mIn;
	set<unsigned int, IntSort>	mAckId; // packets that are acknowledged, this needs to be sorted
	unsigned int				mPacketId;
	unsigned int				mHighestPacketReceived;
	unsigned int				mAllConfirmedPacket;
	Mutex						mMutex;
};



class RDPReliableOrderedChannel : public RDPChannel
{
public:

	RDPReliableOrderedChannel(RDPChannelHandle handle, RDPStream* stream);

	virtual int			Send(Packet* packet);
	virtual int			Receive(Packet* packet, unsigned int maxBytes);
	virtual void		ReceiveInternal(Packet* packet);
	void				CreateChannel(RDPChannelType type, RDPChannelHandle handle);
	virtual void		Update();
	virtual StreamState	GetState();

protected:

	struct IntSort
	{
	  bool operator()(const unsigned int& s1, const unsigned int& s2) const
	  {
		return s1 < s2;
	  }
	};

	list<Packet*>				mOut;			// this is be sorted
	list<Packet*>				mIn;			// any packets in here can be received
	list<Packet*>				mInUnordered;	// any packets that have some in the wrong order go in here, until we have all packets
	set<unsigned int, IntSort>	mAckId;			// packets that are acknowledged, this needs to be sorted
	unsigned int				mPacketId;
	unsigned int				mHighestPacketReceived;
	unsigned int				mAllConfirmedPacket;
	Mutex						mMutex;
};



class RDPUnreliableUnorderedChannel : public RDPChannel
{
public:

	RDPUnreliableUnorderedChannel(RDPChannelHandle handle, RDPStream* stream);

	virtual int			Send(Packet* packet);
	virtual int			Receive(Packet* packet, unsigned int maxBytes);
	virtual void		ReceiveInternal(Packet* packet);
	virtual StreamState	GetState();

protected:

	list<Packet*>			mIn;
	Mutex					mMutex;
};

class RDPUnreliableOrderedChannel : public RDPChannel
{
public:

	RDPUnreliableOrderedChannel(RDPChannelHandle handle, RDPStream* stream);

	virtual int			Send(Packet* packet);
	virtual int			Receive(Packet* packet, unsigned int maxBytes);
	virtual void		ReceiveInternal(Packet* packet);
	virtual StreamState	GetState();

protected:

	list<Packet*>			mIn;
	Mutex					mMutex;
	unsigned int			mPacketId;	
	unsigned int			mHighestPacketReceived;// packet id, all packets with id lower are discarded
};


#endif