#include "RDPState.h"

RDPChannel* RDPState::CreateChannel(RDPChannelType type, RDPChannelHandle handle)
{
	switch (type)
	{
	case CT_ReliableUnordered:
		return new RDPReliableUnorderedChannel(handle, mStream);
	case CT_ReliableOrdered:
		return new RDPReliableOrderedChannel(handle, mStream);
	case CT_UnreliableUnordered:
		return new RDPUnreliableUnorderedChannel(handle, mStream);
	case CT_UnreliableOrdered:
		return new RDPUnreliableOrderedChannel(handle, mStream);
	}

	return 0;
}