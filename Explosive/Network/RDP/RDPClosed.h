#ifndef _RDPCLOSED_H_
#define _RDPCLOSED_H_

#include "RDPState.h"

class RDPSyncSent;

class RDPClosed : public RDPState
{
public:

	explicit RDPClosed(RDPStream* stream);
 
	virtual bool			Bind(const SocketAddr& onAddr);
	virtual bool			Listen();
	virtual bool			Accept(RDPStream& stream);
	virtual StreamState		GetState();
	virtual bool			Connect(const SocketAddr& toAddr, bool blocking);
	virtual RDPStateType	GetType() { return ST_Closed; }
	virtual void			ReceiveInternal(Packet* packet);
	virtual void			Close() { }
	virtual int				Send(Packet* packet, RDPChannelHandle handle);
	virtual int				Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle);
	virtual StreamState		GetState(RDPChannelHandle handle) { return eNone; }
	virtual void			SetState();

protected:

	RDPSyncSent* mNextState;
};

#endif