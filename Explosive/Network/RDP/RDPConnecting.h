#ifndef _RDPCONNECTING_H_
#define _RDPCONNECTING_H_

#include "RDPState.h"
#include "Multithread/Mutex.h"

class RDPSyncReceived : public RDPState
{
public:

	explicit RDPSyncReceived(RDPStream* stream, const SocketAddr& toAddr, const SocketAddr* natAddr);

	virtual bool			Bind(const SocketAddr& onAddr);
	virtual bool			Listen();
	virtual bool			Accept(RDPStream& stream);
	virtual StreamState		GetState(RDPChannelHandle handle);
	virtual bool			Update();
	virtual bool			Connect(const SocketAddr& toAddr, bool blocking);
	virtual RDPStateType	GetType() { return ST_SyncReceived; }
	virtual void			Close() { }

	virtual void			ReceiveInternal(Packet* packet);
	int						SendInternal(Packet* packet);

	virtual int				Send(Packet* packet, RDPChannelHandle handle);
	virtual int				Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle);

protected:

	SocketAddr				mNATPunchAddr;
	list<Packet*>			mIncomming;
	Mutex					mMutex;
};

class RDPSyncSent : public RDPState
{
public:

	explicit RDPSyncSent(RDPStream* stream, const SocketAddr& toAddr, bool blocking, RDPState* fowardReceived = 0);

	virtual bool			Bind(const SocketAddr& onAddr);
	virtual bool			Listen();
	virtual bool			Accept(RDPStream& stream);
	virtual StreamState		GetState(RDPChannelHandle handle);
	virtual bool			Update();
	virtual bool			Connect(const SocketAddr& toAddr, bool blocking);
	virtual RDPStateType	GetType() { return ST_SyncSent; }
	virtual void			Close() { }

	virtual void			ReceiveInternal(Packet* packet);
	int						SendInternal(Packet* packet);

	virtual int				Send(Packet* packet, RDPChannelHandle handle);
	virtual int				Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle);

protected:
	
	bool					InternalUpdate();

	RDPState*				mFowardReceived;
	unsigned long			mTime;
	unsigned long			mSleepTime;
	bool					mBlocking;
	bool					mNATPunch;
	SocketAddr				mNATPunchAddr;
	list<Packet*>			mIncomming;
	Mutex					mMutex;
};

#endif