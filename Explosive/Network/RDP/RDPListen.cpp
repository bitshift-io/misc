#include "RDPListen.h"
#include "RDPStream.h"
#include "RDPConnecting.h"
#include "RDPMessage.h"
#include "RDPStreamMgr.h"
#include "../Packet.h"

bool RDPListen::Bind(const SocketAddr& onAddr)
{
	return false;
}

bool RDPListen::Listen()
{
	return true;
}

bool RDPListen::Accept(RDPStream& stream)
{
	mMutex.Lock();

	if (mConnected.size())
	{
		RDPStream* internalStream = mConnected.front();
		stream = *internalStream;
		///mAccepted.push_front(internalStream);
		mConnected.erase(mConnected.begin());
		delete internalStream;
		mMutex.Unlock();
		return true;
	}

	mMutex.Unlock();
	return false;
}

StreamState	RDPListen::GetState(RDPChannelHandle handle)
{
	if (handle != -1)
		return eNone;

	return mConnected.size() ? eRead : eNone;
}

bool RDPListen::Update()
{
	// NAT punch through packets
	{
		//cout << "A" << endl;
		mMutex.Lock();

		//cout << "B" << endl;

		list<RDPStream*>::iterator it;
		for (it = mNATPunch.begin(); it != mNATPunch.end(); )
		{
			(*it)->Update();

			// clean up any timed-out NAT connections
			if ((*it)->mState->GetType() == ST_Closed)
			{
				delete (*it);
				it = mNATPunch.erase(it);
			}
			else
			{
				++it;
			}
		}		

		mMutex.Unlock();
	}

	// check if a stream has gone from connecting to open
	{
		mMutex.Lock();

		Iterator it;
		for (it = mConnecting.begin(); it != mConnecting.end(); )
		{
			if ((*it)->mState->GetType() == ST_Open)
			{
				// we better be "open"
				mConnected.push_back(*it);
				it = mConnecting.erase(it);
			}
			else
			{
				++it;
			}
		}

		mMutex.Unlock();
	}

	return true;
}

int RDPListen::SendInternal(Packet* packet, const SocketAddr& toAddr)
{
	return mStream->mUDPStream->Send(mStream->mConnectAddr, packet);
}

RDPListen::Iterator RDPListen::GetStream(const SocketAddr& fromAddr, list<RDPStream*>& source)
{
	Iterator it;
	for (it = source.begin(); it != source.end(); ++it)
	{
		if (gNetwork.Equals(fromAddr, (*it)->mConnectAddr))
			return it;
	}

	return source.end();
}

bool RDPListen::Connect(const SocketAddr& toAddr, bool blocking)
{
	if (blocking)
		return false;

	RDPStream* rdpStream = new RDPStream;
	rdpStream->Bind(mStream->GetBindAddr());
	rdpStream->mNextState = new RDPSyncSent(rdpStream, toAddr, false, this);
	rdpStream->UpdateState();

	mMutex.Lock();

	mNATPunch.push_back(rdpStream);

	mMutex.Unlock();
	return true;
}

int	RDPListen::Send(Packet* packet, RDPChannelHandle handle)
{
	return 0;
}

int	RDPListen::Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle)
{
	return 0;
}

void RDPListen::ReceiveInternal(Packet* packet)
{
	mMutex.Lock();

	// if we havent got a connection from this client, open a new connection
	if (GetStream(packet->GetSockAddress(), mConnected) == mConnected.end()
		/*&& GetStream(packet->GetSockAddress(), mAccepted) == mAccepted.end()*/)
	{
		// check we are getting a connect header
		RDPHead head = RDPMessage::PeekHeader(packet);
		if (head.messageType != MT_Synchronize)
		{
			delete packet;
			mMutex.Unlock();
			return;
		}

		Iterator it = GetStream(packet->GetSockAddress(), mNATPunch);
		if (it != mNATPunch.end())
		{
			delete (*it);
			mNATPunch.erase(it);
		}

		// check to see if we are already opening a connection to this client
		RDPStream* rdpStream = new RDPStream;
		rdpStream->mUDPStream = gRDPStreamMgr.GetUDPStream(mStream->GetBindAddr());//packet->GetSockAddress());
		rdpStream->mNextState = new RDPSyncReceived(rdpStream, packet->GetSockAddress(), 0);
		rdpStream->UpdateState();
		mConnecting.push_back(rdpStream);
	}

	delete packet;

	mMutex.Unlock();
}