#ifndef _RDPSTATE_H_
#define _RDPSTATE_H_

#include "../Network.h"
#include "../NetworkStream.h"
#include "RDPChannel.h"

class RDPStream;
class Packet;

enum RDPStateType
{
	ST_Closed,
	ST_SyncReceived,
	ST_SyncSent,
	ST_Listen,
	ST_Open,
	ST_WaitClose,
};

class RDPState
{
public:

	explicit RDPState(RDPStream* stream) : mStream(stream) { }

	virtual bool			Bind(const SocketAddr& onAddr) = 0;
	virtual bool			Listen() = 0;
	virtual bool			Accept(RDPStream& stream) = 0;
	virtual StreamState		GetState(RDPChannelHandle handle) = 0;
	virtual bool			Update() { return true; }
	virtual bool			Connect(const SocketAddr& toAddr, bool blocking) = 0;
	virtual RDPStateType	GetType() = 0;
	virtual void			Close() = 0;

	virtual int				Send(Packet* packet, RDPChannelHandle handle) = 0;
	virtual int				Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle) = 0;

	virtual void			ReceiveInternal(Packet* packet) = 0;

	virtual RDPChannel* 	CreateChannel(RDPChannelType type, RDPChannelHandle handle);

	virtual void			SetState()															{}

//protected:

	RDPStream*				mStream;
};

#endif