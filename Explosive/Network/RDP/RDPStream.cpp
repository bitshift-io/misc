#include "RDPStream.h"
#include "RDPClosed.h"
#include "RDPStreamMgr.h"
#include "RDPChannel.h"
#include "RDPOpen.h"

/*
void RDPStream::Update(void* data)
{
	RDPStream* stream = static_cast<RDPStream*>(data);
}*/

RDPStream::RDPStream() : 
	mNextState(0),
	mUDPStream(0)
{
	mState = new RDPClosed(this);

	// create the main substream
	mChannel.push_front(CreateChannel(CT_ReliableUnordered, -1));

	gRDPStreamMgr.Register(this);
}

RDPStream::RDPStream(const RDPStream& copy) :
    mState(0),
	mNextState(0),
	mUDPStream(0)
{
	operator=(copy);

	// create the main substream
	//mChannel.push_front(CreateChannel(CT_ReliableUnordered, -1));

	gRDPStreamMgr.Register(this);
}

RDPStream::~RDPStream()
{
	gRDPStreamMgr.Deregister(this);

	delete mState;
	delete mNextState;
}

void RDPStream::operator=(const RDPStream& copy)
{
	// increase the reference count on this stream
	mUDPStream = gRDPStreamMgr.GetUDPStream(copy.mUDPStream->GetBindAddr());

	mChannel = copy.mChannel;
	list<RDPChannel*>::iterator it;
	for (it = mChannel.begin(); it != mChannel.end(); ++it)
	{
		(*it)->mStream = this;
	}

	mConnectAddr = copy.mConnectAddr;
	mBindAddr = copy.mBindAddr;

	// set up the state
	switch (copy.mState->GetType())
	{
	case ST_Open:
		mNextState = new RDPOpen(this, static_cast<RDPOpen*>(copy.mState));
		break;

	case ST_Closed:
		mNextState = new RDPClosed(this);
		break;

	default:
		//assert(false, "Shouldnt be copying a stream when in an unknown state!");
		cout << "shouldnt be here!" << endl;
		break;
	}

	gRDPStreamMgr.UpdateState(this);

	mBindAddr = copy.mBindAddr;
	mConnectAddr = copy.mConnectAddr;
}

void RDPStream::Close()
{
	if (mState->GetType() == ST_Closed)
		return;

	mNextState = new RDPClosed(this);
}

bool RDPStream::Bind(const SocketAddr& onAddr)
{
	gRDPStreamMgr.UpdateState(this);
	Assert(mState);
	return mState->Bind(onAddr);
}

bool RDPStream::Listen()
{
	gRDPStreamMgr.UpdateState(this);
	Assert(mState);
	return mState->Listen();
}

bool RDPStream::Accept(RDPStream& stream)
{
	gRDPStreamMgr.UpdateState(this);
	Assert(mState);
	return mState->Accept(stream);
}

void RDPStream::UpdateState()
{
	if (mNextState)
	{
		delete mState;
		mState = mNextState;
		mNextState = 0;
		mState->SetState();
	}
}

StreamState RDPStream::GetState(RDPChannelHandle handle)
{
	gRDPStreamMgr.UpdateState(this);
	Assert(mState);
	return mState->GetState(handle);
}

bool RDPStream::Connect(const SocketAddr& toAddr, bool blocking)
{
	gRDPStreamMgr.UpdateState(this);
	Assert(mState);
	bool ret = mState->Connect(toAddr, blocking);

	if (ret && blocking)
		gRDPStreamMgr.UpdateState(this);

	return ret;
}

bool RDPStream::IsValid()
{
	Assert(mState);
	if (mState->GetType() == ST_Open)
		return true;

	return false;
}

void RDPStream::Update()
{
	gRDPStreamMgr.UpdateState(this);
	Assert(mState);
	mState->Update();
}

RDPChannel* RDPStream::GetChannel(RDPChannelHandle handle)
{
	list<RDPChannel*>::iterator it;
	for (it = mChannel.begin(); it != mChannel.end(); ++it)
	{
		if (handle == (*it)->GetHandle())
			return (*it);
	}

	return 0;
}

RDPChannel* RDPStream::CreateChannel(RDPChannelType type, RDPChannelHandle handle)
{
	Assert(mState);
	return mState->CreateChannel(type, handle);
}

RDPChannelHandle RDPStream::CreateChannel(RDPChannelType type)
{
	gRDPStreamMgr.UpdateState(this);

	RDPChannelHandle index = 0;
	list<RDPChannel*>::iterator it;
	for (it = mChannel.begin(); it != mChannel.end(); ++it)
	{
		index = max(index, (*it)->GetHandle() + 1);
	}

	// send this create channel over the network
	RDPChannel* channel = GetChannel(-1);
	if (channel)
	{
		// we could add a get type to make this safe
		RDPReliableUnorderedChannel* mainChannel = static_cast<RDPReliableUnorderedChannel*>(channel);
		mainChannel->CreateChannel(type, index);
	}

	RDPChannel* newChannel = CreateChannel(type, index);
	mChannel.push_front(newChannel);
	return index;
}

void RDPStream::ReceiveInternal(Packet* packet)
{
	gRDPStreamMgr.UpdateState(this);
	Assert(mState);
	mState->ReceiveInternal(packet);
}

bool RDPStream::IsListening()
{
	gRDPStreamMgr.UpdateState(this);
	Assert(mState);
	return mState->GetType() == ST_Listen;
}

int RDPStream::Send(Packet* packet, RDPChannelHandle handle)
{
	gRDPStreamMgr.UpdateState(this);
	Assert(mState);
	return mState->Send(packet, handle);
}

int	RDPStream::Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle)
{
	gRDPStreamMgr.UpdateState(this);
	Assert(mState);
	return mState->Receive(packet, maxBytes, handle);
}
