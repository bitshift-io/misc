#include "RDPStreamMgr.h"
#include "RDPStream.h"
#include "../UDPStream.h"
#include "../Packet.h"
#include "../Time/Time.h"

#define RESEND_TIME 10

unsigned long RDPStreamMgr::Update(void* data, Thread* thread)
{
	RDPStreamMgr* mgr = static_cast<RDPStreamMgr*>(data);
	while (thread->IsRunning())
	{
		mgr->Update();
	}
	return 0;
}


RDPStreamMgr::RDPStreamMgr()
{
	mResendTime = gTime.GetTicks();
}

RDPStreamMgr::~RDPStreamMgr()
{
	if (mUDPStream.size() != 0)
	{
		int nothing = 0;
	}

	mThread.Wait();
}

void RDPStreamMgr::Update()
{
	// read all packets from all udp streams
	// and palm them off to the appropriate rdp streams
	{
		mMutex.Lock();

		list<UDPRefCount>::iterator it;	
		for (it = mUDPStream.begin(); it != mUDPStream.end(); ++it)
		{
			UDPStream* stream = it->udpStream;
			if ((stream->GetState() & eRead) == eRead)
			{
				Packet* p = new Packet;
				if (stream->Receive(p) > 0)
				{
					RDPStream* rdpStream = GetRDPStream(p->GetSockAddress(), &stream->GetBindAddr());
					
					// if we have no RDP stream, this generally means we got a packet from the NAT punch or some one
					// trying to connect to us we dont know about if we are a client
					if (rdpStream)
						rdpStream->ReceiveInternal(p);
				}
				else
				{
					delete p;
				}
			}
		}

		mMutex.Unlock();
	}

	// update all rdp streams
	{
		unsigned long time = gTime.GetTicks();
		if (time >= mResendTime + RESEND_TIME)
		{	
			mMutex.Lock();

			list<RDPStream*>::iterator it;
			for (it = mRDPStream.begin(); it != mRDPStream.end(); ++it)
			{
				(*it)->Update();
			}
			
			mMutex.Unlock();

			mResendTime = gTime.GetTicks();
		}
	}

}

void RDPStreamMgr::UpdateState(RDPStream* stream)
{
	mMutex.Lock();
	stream->UpdateState();
	mMutex.Unlock();
}

void RDPStreamMgr::Register(RDPStream* stream)
{
	mMutex.Lock();

	mRDPStream.push_front(stream);

	if (!mThread.IsRunning())
	{
		Task t(RDPStreamMgr::Update, (void*)this);
		mThread.Start(&t);
	}

	mMutex.Unlock();
}

void RDPStreamMgr::Deregister(RDPStream* stream)
{
	mMutex.Lock();
	mRDPStream.remove(stream);

	// do ref count reduction here,
	// clean up if needed
	list<UDPRefCount>::iterator it;
	for (it = mUDPStream.begin(); it != mUDPStream.end(); ++it)
	{
		if (it->udpStream == stream->mUDPStream)
		{
			--it->count;
			if (it->count <= 0)
			{
				it->udpStream->Close();
				delete it->udpStream;
				mUDPStream.erase(it);
				int size = mUDPStream.size();
				break;
			}
		}
	}

	// stop the thread
	if (mUDPStream.size() == 0)
	{
		mThread.Stop();
	}

	// remove refcount from out udpstream
	mMutex.Unlock();
}

UDPStream* RDPStreamMgr::GetUDPStream(const SocketAddr& onAddr)
{
	mMutex.Lock();

	// see if an udp stream is already bound on this address
	list<UDPRefCount>::iterator it;
	for (it = mUDPStream.begin(); it != mUDPStream.end(); ++it)
	{
		if (gNetwork.Equals(onAddr, it->udpStream->GetBindAddr()))
		{
			++(it->count);
			mMutex.Unlock();
			return it->udpStream;
		}
	}

	// if not, make a new one and bind it
	UDPStream* stream = new UDPStream;
	if (!stream->Bind(onAddr))
	{
		delete stream;
		mMutex.Unlock();
		return 0;
	}

	UDPRefCount ref;
	ref.count = 1;
	ref.udpStream = stream;

	mUDPStream.push_front(ref);
	mMutex.Unlock();

	return stream;
}

void RDPStreamMgr::ReleaseUDPStream(UDPStream* udpStream)
{
	mMutex.Lock();

	// see if an udp stream is already bound on this address
	list<UDPRefCount>::iterator it;
	for (it = mUDPStream.begin(); it != mUDPStream.end(); ++it)
	{
		if (it->udpStream == udpStream)
		{
			--(it->count);
			if (it->count <= 0)
			{
				it->udpStream->Close();
				delete it->udpStream;
				mUDPStream.erase(it);
			}

			mMutex.Unlock();
			return;
		}
	}

	// not in the list wtf?!
	mMutex.Unlock();
	return;
}

RDPStream* RDPStreamMgr::GetRDPStream(const SocketAddr& fromAddr, const SocketAddr* onAddr)
{
	RDPStream* listen = 0;
	list<RDPStream*>::iterator it;
	for (it = mRDPStream.begin(); it != mRDPStream.end(); ++it)
	{
		RDPStream* stream = *it;
		if (gNetwork.Equals(fromAddr, stream->GetConnectAddr()))
			return stream;

		// compare bind addr
		if (onAddr && gNetwork.Equals(*onAddr, stream->GetBindAddr()))
		{
			if (stream->IsListening())
				listen = stream;
		}
	}

	return listen;
}