#include "RDPOpen.h"
#include "RDPStream.h"
#include "RDPConnecting.h"
#include "RDPDisconnecting.h"
#include "RDPClosed.h"
#include "RDPMessage.h"
#include "../Packet.h"
#include "../Time/Time.h"

#define RESYNC_TIMEOUT		4000
#define DISCONNECT_TIMEOUT	4000	// time after start of sending resync packets before closing 
#define DROP_EVERY_COUNT	2		// used to drop sync ack packets

//#define DISABLE_DEISCONNECT		1	// disable disconnects, used for debugging 

RDPOpen::RDPOpen(RDPStream* stream, RDPOpen* open) : RDPState(stream),
	mDropSync(0)
{ 
	mLastPacketTime = gTime.GetTicks();

	if (open)
		mUnreceivedPacket = open->mUnreceivedPacket;
}

bool RDPOpen::Bind(const SocketAddr& onAddr)
{
	return false;
}

bool RDPOpen::Listen()
{
	return true;
}

bool RDPOpen::Accept(RDPStream& stream)
{
	return false;
}

StreamState	RDPOpen::GetState(RDPChannelHandle handle)
{
	RDPChannel* channel = mStream->GetChannel(handle);
	if (!channel)
		return eNone;

	return channel->GetState();
}

bool RDPOpen::Connect(const SocketAddr& toAddr, bool blocking)
{
	return false;
}

void RDPOpen::Close()
{
	mStream->mNextState = new RDPWaitClose(mStream);
}

bool RDPOpen::Update()
{
	list<RDPChannel*>::iterator it;
	for (it = mStream->mChannel.begin(); it != mStream->mChannel.end(); ++it)
	{
		(*it)->Update();
	}

#ifndef DISABLE_DEISCONNECT

	// connection timed out
	if (gTime.GetTicks() > (mLastPacketTime + RESYNC_TIMEOUT + DISCONNECT_TIMEOUT))
	{
		mStream->mNextState = new RDPClosed(mStream);
		return true;
	}

#endif

	// if we havent received a packet in the alloted time, send sync packets
	// to determine if the other end is still laive
	if (gTime.GetTicks() > (mLastPacketTime + RESYNC_TIMEOUT))
	{
		Packet out;
		RDPHead head;
		head.messageType = MT_Synchronize;
		out.Append((const char*)&head, sizeof(RDPHead));
		out.SetSockAddress(mStream->GetConnectAddr());

		RDPChannel* channel = mStream->GetChannel(-1);
		int bytes = channel->SendInternal(&out);
	}


	return true;
}

void RDPOpen::ReceiveInternal(Packet* packet)
{
	mLastPacketTime = gTime.GetTicks();

	RDPHead head = RDPMessage::PeekHeader(packet);

	// close the socket on receiveing a reset command
	if (head.messageType & MT_Reset)
	{
		mStream->mNextState = new RDPClosed(mStream);
		delete packet;
		return;
	}

	if (head.messageType & MT_Synchronize)
	{
		if (head.messageType & MT_Acknowledge)
		{
			if (mDropSync >= DROP_EVERY_COUNT)
			{
				mDropSync = 0;
				delete packet;
				return;
			}

			++mDropSync;
		}

		Packet out;
		RDPHead head;
		head.messageType = MT_Synchronize | MT_Acknowledge;
		out.Append((const char*)&head, sizeof(RDPHead));
		out.SetSockAddress(mStream->GetConnectAddr());

		RDPChannel* channel = mStream->GetChannel(-1);
		int bytes = channel->SendInternal(&out);

		delete packet;
		return;
	}

	// foward to appropriate channel, or save till the channel is created laster
	RDPChannel* channel = mStream->GetChannel(head.channelId);
	if (channel)
		channel->ReceiveInternal(packet);
	else
		mUnreceivedPacket.push_front(packet);
}

RDPChannel* RDPOpen::CreateChannel(RDPChannelType type, RDPChannelHandle handle)
{
	RDPChannel* channel = RDPState::CreateChannel(type, handle);

	list<Packet*>::iterator it;
	for (it = mUnreceivedPacket.begin(); it != mUnreceivedPacket.end(); )
	{
		RDPHead head = RDPMessage::PeekHeader(*it);
		if (head.channelId == handle)
		{
			channel->ReceiveInternal(*it);
			it = mUnreceivedPacket.erase(it);
		}
		else
		{
			++it;
		}
	}

	return channel;
}

int RDPOpen::Send(Packet* packet, RDPChannelHandle handle)
{
	RDPChannel* channel = mStream->GetChannel(handle);
	return channel->Send(packet);
}

int RDPOpen::Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle)
{
	RDPChannel* channel = mStream->GetChannel(handle);
	return channel->Receive(packet, maxBytes);
}