#ifndef _RDPOPEN_H_
#define _RDPOPEN_H_

#include "RDPState.h"
#include <list>

using namespace std;

class RDPStream;

/*
    http://www.gamedev.net/community/forums/topic.asp?topic_id=381835

	Instead of continuously sending the message from the sending end, 
	and continuously sending ACKs for that message on the receiving end, 
	a better solution is to just send the message once from the sending end, 
	and on the receiving end send a NAK to request retransmission of any missing messages. 
	For example, if you receive message #1, and then #4, you'd start spitting out NAK's 
	for messages 2 and 3 until you get them. And the sending end would just send the message 
	again whenever it receives a NAK
*/
class RDPOpen : public RDPState
{
public:

	explicit RDPOpen(RDPStream* stream, RDPOpen* open = 0);
 
	virtual bool			Bind(const SocketAddr& onAddr);
	virtual bool			Listen();

	// only returns true once a connection has been made and accepted!
	virtual bool			Accept(RDPStream& stream);
	virtual StreamState		GetState(RDPChannelHandle handle);

	virtual bool			Update();
	virtual bool			Connect(const SocketAddr& toAddr, bool blocking);
	virtual RDPStateType	GetType() { return ST_Open; }
	virtual void			Close();

	virtual int				Send(Packet* packet, RDPChannelHandle handle);
	virtual int				Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle);

	virtual void			ReceiveInternal(Packet* packet);

	virtual RDPChannel* 	CreateChannel(RDPChannelType type, RDPChannelHandle handle);

protected:

	list<Packet*>			mUnreceivedPacket;	// packets that havent been sent due to channel not existing
	unsigned long			mLastPacketTime;	// used for timeout
	int						mDropSync;
};

#endif