#include "RDPMessage.h"
#include "../Packet.h"

RDPHead RDPMessage::PeekHeader(const Packet* p)
{
	RDPHead head;
	p->Peek((char*)&head, sizeof(RDPHead));
	return head;
}