#ifndef _RDPSTREAMMGR_H_
#define _RDPSTREAMMGR_H_

#include "Network.h"
#include "Thread.h"
#include "Mutex.h"
#include "Singleton.h"
#include <list>

using namespace std;

class RDPStream;
class UDPStream;

#define	Assert(x) if ((x) == false) __asm { int 3 }

//
// A manager to handle connecting, and disconnecting streams
// + updating
// this class is hidden from the user
//
#define gRDPStreamMgr RDPStreamMgr::GetInstance()

class RDPStreamMgr : public SingletonBC<RDPStreamMgr>
{
public:

	RDPStreamMgr();
	~RDPStreamMgr();

	void		Update();
	void		Register(RDPStream* stream);
	void		Deregister(RDPStream* stream);

	UDPStream*	GetUDPStream(const SocketAddr& onAddr);
	void		ReleaseUDPStream(UDPStream* udpStream);

	// searches the streams that use the fromAddr,
	// if not found it will return a listening socket
	// if onAddr is not null
	RDPStream*	GetRDPStream(const SocketAddr& fromAddr, const SocketAddr* onAddr = 0);
	
	void		UpdateState(RDPStream* stream);

	Mutex&		GetMutex()									{ return mMutex; }

protected:

	static unsigned long	Update(void* data, Thread* thread);

	struct UDPRefCount
	{
		UDPStream*	udpStream;
		int			count;
	};

	Mutex				mMutex;
	Thread				mThread;
	list<RDPStream*>	mRDPStream;
	list<UDPRefCount>	mUDPStream; // we will need to ref count these
	unsigned int		mResendTime;
};

#endif