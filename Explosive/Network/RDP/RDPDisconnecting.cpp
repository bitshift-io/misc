#include "RDPDisconnecting.h"
#include "RDPStream.h"
#include "RDPClosed.h"
#include "RDPMessage.h"
#include "../Time/Time.h"

#define CLOSE_TIME_OUT 1000 * 10

RDPWaitClose::RDPWaitClose(RDPStream* stream) : RDPState(stream)
{ 
	mStartTime = gTime.GetTicks();

	RDPHead head;
	head.messageType = MT_Reset;
	mPacket.Append((const char*)&head, sizeof(RDPHead));
}

bool RDPWaitClose::Bind(const SocketAddr& onAddr)
{
	return false;
}

bool RDPWaitClose::Listen()
{
	return false;
}

bool RDPWaitClose::Accept(RDPStream& stream)
{
	return false;
}

StreamState RDPWaitClose::GetState(RDPChannelHandle handle)
{
	return eNone;
}

bool RDPWaitClose::Connect(const SocketAddr& toAddr, bool blocking)
{
	return false;
}

bool RDPWaitClose::Update() 
{
	if (gTime.GetTicks() > mStartTime + CLOSE_TIME_OUT)
	{
		mStream->mNextState = new RDPClosed(mStream);
		return true;
	}

	mStream->mUDPStream->Send(mStream->mConnectAddr, &mPacket);
	return true;
}

int RDPWaitClose::Send(Packet* packet, RDPChannelHandle handle)
{
	return 0;
}

int	RDPWaitClose::Receive(Packet* packet, unsigned int maxBytes, RDPChannelHandle handle)
{
	return 0;
}