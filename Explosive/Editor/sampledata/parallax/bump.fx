//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4 	worldIT 		: WorldInverseTranspose;
float4x4 	worldViewProj 	: WorldViewProjection;
float4x4 	worldView 		: WorldView;
float4x4 	worldInv 		: WorldInverse;

float3   	lightPos 		: LightPosition;
float3   	eyePos 			: EyePosition;

texture 	bumpTexture;

sampler2D BumpSampler = sampler_state
{
    Texture = <bumpTexture>;
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
};

texture 	diffuseTexture;

sampler2D DiffuseSampler = sampler_state
{
    Texture = <diffuseTexture>;
    MinFilter = Anisotropic;
    MagFilter = Anisotropic;
    MipFilter = Anisotropic;
};

texture 	specularTexture
<
    string ResourceType = "Cube";
>;

samplerCUBE SpecularSampler = sampler_state
{
    Texture = <specularTexture>;
    MinFilter = Anisotropic;
    MagFilter = Anisotropic;
    MipFilter = Anisotropic;
};

texture 	parallaxTexture;

sampler2D ParallaxSampler = sampler_state
{
    Texture = <parallaxTexture>;
    MinFilter = Anisotropic;
    MagFilter = Anisotropic;
    MipFilter = Anisotropic;
    //AddressU = Clamp;
    //AddressV = Clamp;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
    float3 position 	: POSITION;
	float3 normal 		: NORMAL;
	float3 tangent 		: TEXCOORD0;
    float3 binormal 	: TEXCOORD1;
	float2 uvCoord		: TEXCOORD2;	
};

struct VS_OUTPUT
{
	float4 position : POSITION;
	float2 uvCoord	: TEXCOORD0;	
	float3 lightVec : TEXCOORD1;
	float3 eyeVec 	: TEXCOORD2;
};

struct PS_OUTPUT
{
	float4 colour	: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform float4x4 worldView,
			uniform float4x4 worldInv,
			uniform float3   lightPos,
			uniform float3   eyePos )
{
    VS_OUTPUT OUT;
   	OUT.position = mul( modelViewProjection, float4(IN.position, 1.0) );
	OUT.uvCoord = IN.uvCoord;

	// convert from object space to texture(tangent) space...
	float3x3 texSpaceTrans = float3x3(IN.tangent,
					IN.binormal,
					IN.normal);
					
	float3 lightModelPos = mul( worldInv, float4(lightPos, 1.0) ).xyz;
	lightModelPos = lightModelPos - IN.position;
	OUT.lightVec = mul( texSpaceTrans, lightModelPos );
	
	float3 eyeModelPos = mul( worldInv, float4(eyePos, 1.0) ).xyz;
	eyeModelPos = eyeModelPos - IN.position;
	OUT.eyeVec = mul( texSpaceTrans, eyeModelPos );
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

float2 GetParallaxOffset(uniform sampler2D height, float2 uvCoord, float3 eyeVec)
{	
	float depth = pow(tex2D( height, uvCoord ).a / 6.0, 2.0);
	eyeVec = eyeVec * depth;
	return eyeVec.xy;
}

PS_OUTPUT myps( VS_OUTPUT IN, uniform sampler2D bump, uniform sampler2D diffuse, uniform sampler2D height,
		uniform samplerCUBE specular )
{
	PS_OUTPUT OUT;

	// lets do some parallax mapping
	float3 eyeVec = normalize( IN.eyeVec );
	float2 uvOffset = GetParallaxOffset(height, IN.uvCoord, eyeVec);
	
	// normals in bump map are in texture(tangent) space....
	float3 bumpTexNormal = tex2D( bump, IN.uvCoord + uvOffset );
	bumpTexNormal.x = 1.0 - bumpTexNormal.x; // invert r to get Nvidia style normal mapping
	bumpTexNormal = (2 * bumpTexNormal) - 1.0;

	float3 lightVec = normalize( IN.lightVec );

	// calculate  reflection vector
	float shiney = 0.05;
	float3 reflection = reflect(eyeVec, bumpTexNormal);
	float4 reflectTex = texCUBE( specular, normalize(reflection) ) * shiney;

	OUT.colour = max( dot(bumpTexNormal, lightVec), 0.0) * tex2D(diffuse, IN.uvCoord + uvOffset) + reflectTex;

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		Zenable  = true;
		CullMode = None; //CCW;
		
		VertexShader = compile arbvp1 myvs( worldViewProj, worldView, worldInv, lightPos, eyePos );
        PixelShader  = compile arbfp1 myps( BumpSampler, DiffuseSampler, ParallaxSampler, SpecularSampler );
    }
}
