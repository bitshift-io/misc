
float4x4 worldViewProj : WorldViewProjection;



texture 	diffuseTexture;

sampler2D DiffuseSampler = sampler_state
{
    Texture = <diffuseTexture>;
    MinFilter = Anisotropic;
    MagFilter = Anisotropic;
    MipFilter = Anisotropic;
};


struct VS_INPUT
{
    	float3 position 	: POSITION;
	float2 uvCoord	: TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 position 	: POSITION;
	float2 uvCoord	: TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs(const VS_INPUT IN,
			uniform float4x4 modelViewProjection)
{
	VS_OUTPUT OUT;
	OUT.position = mul(modelViewProjection, float4(IN.position, 1.0));
	OUT.uvCoord = IN.uvCoord;
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( 	VS_OUTPUT IN,
			uniform sampler2D diffuse )
{
	PS_OUTPUT OUT;
	OUT.color = tex2D(diffuse, IN.uvCoord);
	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		Zenable  = true;
		CullMode = CCW;
		
		VertexShader = compile arbvp1 myvs(worldViewProj);
       	PixelShader  = compile arbfp1 myps(DiffuseSampler);
    }
}
