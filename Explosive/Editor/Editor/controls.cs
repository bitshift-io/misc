using System;
using System.Collections.Generic;
using System.Windows.Forms;

using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.IO;

using System.Runtime.InteropServices;

using System.Threading;

namespace Editor
{
    // FMNOTE: -these need to deal with anchoring
    // -should be able to specify left column and a secondary column
    // so we can handle indenting
    // -we should also deal with tabbing here
    // - should we have a contractable panel control? this could make things a bit more complex for coding

    public class Helper
    {
        static public bool MakeAbsoluteToApp(ref string path)
        {
            // make the path absolute
            string basePath = path;
            string appBasePath = Application.StartupPath;

            // count number of directories that go back
            int dirCount = 0;
            int idx = 0;
            bool end = false;
            while (!end)
            {
                string subStr = basePath.Substring(idx, 3);
                if (subStr.Equals("..\\") || subStr.Equals("../"))
                    ++dirCount;
                else
                    end = true;

                idx += 3;
            }

            char[] dirArray = {'\\', '/'};

            for (int i = 0; i < dirCount; ++i)
            {
                int last = appBasePath.LastIndexOfAny(dirArray);
                appBasePath = appBasePath.Substring(0, last);
            }

            basePath = basePath.Substring(dirCount * 3);
            path = appBasePath + '\\' + basePath;
            return true;
        }

        static public bool MakeRelativeToApp(ref string path)
        {
            // make the path relative
            string basePath = path;
            string appBasePath = Application.StartupPath;

            // check that the paths are ont he same drive!
            if (appBasePath[0] != path[0])
                return false;

            int diff = 0;
            while (diff < appBasePath.Length && diff < basePath.Length && appBasePath[diff] == basePath[diff])
            {
                ++diff;
            }
            --diff;

            // ends in a directory
            if (basePath[diff] == '\\' || basePath[diff] == '/')
            {
                basePath = basePath.Substring(diff + 1);
            }
            else
            {
                // go back to last dir
                int last = diff;
                while (last > 0 && (basePath[last] != '\\' && basePath[last] != '/'))
                {
                    --last;
                }

                basePath = basePath.Substring(last + 1);
            }

            if (appBasePath[diff] == '\\' || appBasePath[diff] == '/')
            {
                appBasePath = appBasePath.Substring(diff + 1);
            }
            else
            {
                // go back to last dir
                int last = diff;
                while (last > 0 && (appBasePath[last] != '\\' && appBasePath[last] != '/'))
                {
                    --last;
                }

                appBasePath = appBasePath.Substring(last + 1);
            }

            string relativePath = "..\\";
            for (int i = 0; i < appBasePath.Length; ++i)
            {
                if (appBasePath[i] == '\\' || appBasePath[i] == '/')
                {
                    relativePath += (".." + appBasePath[i]);
                }
            }

            relativePath += basePath;
            path = relativePath;
            return true;
        }
    }

    [Serializable]
    public abstract class Control
    {
        public virtual int Add(string internalName, string name, Panel pane, int location, EventHandler handler)
        {
            mTxtLabel = new Label();

            mTxtLabel.Location = new System.Drawing.Point(3, location);
            mTxtLabel.Name = "txtLabel";
            mTxtLabel.Size = new System.Drawing.Size(80, 15);
            //txtLabel.TabIndex = 19; // Note: this should also be passed
            mTxtLabel.Text = name;

            pane.Controls.Add(mTxtLabel);

            return location + 22;
        }

        [NonSerialized]
        public System.Windows.Forms.Label mTxtLabel;

        [NonSerialized]
        public static int sLeftColumn = 100;
    }

    [Serializable]
    public class BoolControl : Control
    {
        public bool mBool;

        [NonSerialized]
        public CheckBox mChkBox;        

        public BoolControl(bool boolObject)
        {
            mBool = boolObject;
        }

        public override int Add(string internalName, string name, Panel pane, int location, EventHandler handler)
        {
            int newLocation = base.Add(internalName, name, pane, location, null);

            // add float control
            mChkBox = new CheckBox();
            mChkBox.Checked = mBool;
            mChkBox.CheckStateChanged += new EventHandler(Changed);
            mChkBox.CheckStateChanged += handler;
            mChkBox.Location = new System.Drawing.Point(sLeftColumn, location);
            mChkBox.Size = new System.Drawing.Size(120, 15);
            mChkBox.Name = internalName;
            
            pane.Controls.Add(mChkBox);

            return newLocation;
        }

        private void Changed(object sender, EventArgs e)
        {
            mBool = mChkBox.Checked;
        }
    }

    [Serializable]
    public class FloatControl : Control
    {
        float mFloat;

        [NonSerialized]
        NumericUpDown mNmbrCtrl;

        public FloatControl(float floatObject)
        {
            mFloat = floatObject;
        }

        public override int Add(string internalName, string name, Panel pane, int location, EventHandler handler)
        {
            int newLocation = base.Add(internalName, name, pane, location, null);

            // add float control
            mNmbrCtrl = new NumericUpDown();

            mNmbrCtrl.Value = new Decimal(mFloat);
            mNmbrCtrl.Increment = new Decimal(0.01f);

            mNmbrCtrl.ValueChanged += new EventHandler(Changed);
            mNmbrCtrl.ValueChanged += handler;

            mNmbrCtrl.Location = new System.Drawing.Point(sLeftColumn, location);
            mNmbrCtrl.Size = new System.Drawing.Size(60, 15);

            mNmbrCtrl.DecimalPlaces = 2;
            mNmbrCtrl.Maximum = new decimal(new int[] {
                1000,
                0,
                0,
                0});
            mNmbrCtrl.Minimum = new decimal(new int[] {
                1000,
                0,
                0,
                -2147483648});

            mNmbrCtrl.Name = internalName;

            pane.Controls.Add(mNmbrCtrl);
    
            return newLocation;
        }

        private void Changed(object sender, EventArgs e)
        {
            mFloat = (float)mNmbrCtrl.Value;
        }
    }

    [Serializable]
    public class TextControl : Control
    {
        public override int Add(string internalName, string name, Panel pane, int location, EventHandler handler)
        {
            int newLocation = base.Add(internalName, name, pane, location, null);

            // add float control
            TextBox txtBox = new TextBox();
            txtBox.Location = new System.Drawing.Point(sLeftColumn, location);
            txtBox.Size = new System.Drawing.Size(130, 15);

            txtBox.Name = internalName;

            pane.Controls.Add(txtBox);

            return newLocation;
        }
    }

    [Serializable]
    public class FileControl : Control
    {
        [NonSerialized]
        public Button mBtnButton = null;

        [NonSerialized]
        public TextBox mTxtBox = null;

        [NonSerialized]
        public OpenFileDialog mFileDialog = null;

        public string mFileName = null;

        public FileControl(OpenFileDialog openDialog)
        {
            mFileDialog = openDialog;
        }

        public override int Add(string internalName, string name, Panel pane, int location, EventHandler handler)
        {
            int newLocation = base.Add(internalName, name, pane, location, null);

            // add float control
            mTxtBox = new TextBox();
            mTxtBox.Enabled = false;
            mTxtBox.Location = new System.Drawing.Point(sLeftColumn, location);
            mTxtBox.Size = new System.Drawing.Size(150, 15);

            mTxtBox.Name = internalName;

            int lastIdx = mFileDialog.FileName.LastIndexOf('\\');
            string fileName = mFileDialog.FileName.Substring(lastIdx + 1);
            mTxtBox.Text = fileName;

            pane.Controls.Add(mTxtBox);

            mBtnButton = new Button();
            mBtnButton.Text = "...";
            mBtnButton.Location = new System.Drawing.Point(sLeftColumn + 150, location);
            mBtnButton.Size = new System.Drawing.Size(20, 20);

            mBtnButton.Click += new EventHandler(Changed);
            mBtnButton.Click += handler;

            pane.Controls.Add(mBtnButton);

            return newLocation;
        }

        private void Changed(object sender, EventArgs e)
        {
            DialogResult result = mFileDialog.ShowDialog();

            if (result == DialogResult.Cancel)
            {
                mTxtBox.Text = null;
                mFileDialog.FileName = null;
            }

            int lastIdx = mFileDialog.FileName.LastIndexOf('\\');
            string fileName = mFileDialog.FileName.Substring(lastIdx + 1);

            mTxtBox.Text = fileName;
            mFileName = mFileDialog.FileName;

            Helper.MakeRelativeToApp(ref mFileName);
        }
    }

    [Serializable]
    public class Vector3Control : Control
    {
        [NonSerialized]
        public Vector3 mVector = null;

        public float mX;
        public float mY;
        public float mZ;

        [NonSerialized]
        public NumericUpDown mNmbrCtrlX;

        [NonSerialized]
        public NumericUpDown mNmbrCtrlY;

        [NonSerialized]
        public NumericUpDown mNmbrCtrlZ;

        public Vector3Control(Vector3 vector3Object)
        {
            mVector = vector3Object;
        }

        public Vector3Control(float x, float y, float z)
        {
            mX = x;
            mY = y;
            mZ = z;
        }

        public override int Add(string internalName, string name, Panel pane, int location, EventHandler handler)
        {
            int newLocation = base.Add(internalName, name, pane, location, null);

            // add float control x
            mNmbrCtrlX = new NumericUpDown();

            mNmbrCtrlX.DecimalPlaces = 2;
            mNmbrCtrlX.Maximum = new decimal(new int[] {
                1000,
                0,
                0,
                0});
            mNmbrCtrlX.Minimum = new decimal(new int[] {
                -1000,
                0,
                0,
                -2147483648});

            mNmbrCtrlX.Increment = new Decimal(0.01f);
            mNmbrCtrlX.Value = new Decimal(mX);

            mNmbrCtrlX.ValueChanged += new EventHandler(Changed);
            mNmbrCtrlX.ValueChanged += handler;

            mNmbrCtrlX.Location = new System.Drawing.Point(sLeftColumn, location);
            mNmbrCtrlX.Size = new System.Drawing.Size(55, 15);

            mNmbrCtrlX.Name = internalName + ".x";

            pane.Controls.Add(mNmbrCtrlX);

            // add float control y
            mNmbrCtrlY = new NumericUpDown();

            mNmbrCtrlY.DecimalPlaces = 2;
            mNmbrCtrlY.Maximum = new decimal(new int[] {
                1000,
                0,
                0,
                0});
            mNmbrCtrlY.Minimum = new decimal(new int[] {
                1000,
                0,
                0,
                -2147483648});

            mNmbrCtrlY.Value = new Decimal(mY);
            mNmbrCtrlY.Increment = new Decimal(0.01f);

            mNmbrCtrlY.ValueChanged += new EventHandler(Changed);
            mNmbrCtrlY.ValueChanged += handler;

            mNmbrCtrlY.Location = new System.Drawing.Point(sLeftColumn + 60, location);
            mNmbrCtrlY.Size = new System.Drawing.Size(55, 15);

            mNmbrCtrlY.Name = internalName + ".x";

            pane.Controls.Add(mNmbrCtrlY);

            // add float control z
            mNmbrCtrlZ = new NumericUpDown();

            mNmbrCtrlZ.DecimalPlaces = 2;
            mNmbrCtrlZ.Maximum = new decimal(new int[] {
                1000,
                0,
                0,
                0});
            mNmbrCtrlZ.Minimum = new decimal(new int[] {
                1000,
                0,
                0,
                -2147483648});

            mNmbrCtrlZ.Value = new Decimal(mZ);
            mNmbrCtrlZ.Increment = new Decimal(0.01f);

            mNmbrCtrlZ.ValueChanged += new EventHandler(Changed);
            mNmbrCtrlZ.ValueChanged += handler;

            mNmbrCtrlZ.Location = new System.Drawing.Point(sLeftColumn + (60 * 2), location);
            mNmbrCtrlZ.Size = new System.Drawing.Size(55, 15);

            mNmbrCtrlZ.Name = internalName + ".x";

            pane.Controls.Add(mNmbrCtrlZ);

            return newLocation;
        }

        private void Changed(object sender, EventArgs e)
        {
            mVector = new Vector3((float)mNmbrCtrlX.Value, (float)mNmbrCtrlY.Value, (float)mNmbrCtrlZ.Value);
            mX = (float)mNmbrCtrlX.Value;
            mY = (float)mNmbrCtrlY.Value;
            mZ = (float)mNmbrCtrlZ.Value;
        }
    }
}