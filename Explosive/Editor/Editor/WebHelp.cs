using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Editor
{
    public partial class WebHelp : Form
    {
        public WebHelp()
        {
            InitializeComponent();
        }

        private void WebHelp_Load(object sender, EventArgs e)
        {
            string path = Application.StartupPath;
            path += "\\help\\index.html";
            wbBrowser.Navigate(path);
        }
    }
}