using System;
using System.Collections.Generic;
using System.Windows.Forms;

using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.IO;

using System.Runtime.InteropServices;

using System.Threading;

namespace Editor
{
    static class EngineScene
    {
        /// <summary>
        /// This loads/saves and manages the scene.
        /// Static for the time being, i doubt we would want 2 engine windows at a time
        /// </summary>
        /// 

        [DllImport("user32.dll")]
        public static extern int SetParent(int hWndChild, int hWndNewParent);

        [DllImport("user32.dll")]
        public static extern bool SetWindowPos(int hWnd, int hWndInsertAfter, int X,
            int Y, int cx, int cy, uint uFlags);

        [DllImport("user32.dll")]
        public static extern bool UpdateWindow(int hWnd);

        public class SkinNode
        {
            public Skeleton mSkeleton = null;
            public Animation mAnimation = null;
            public Mesh mMesh = null;
        }

        public delegate void DebugHandler(Camera camera);

        static public DebugHandler mDebugHandler;

        static public IntPtr    mWindowHandle;

        static public Window    mWindow;

        static public int       mWidth;
        static public int       mHeight;

        static public ArrayList mRenderers = new ArrayList(1);
        static public Camera    mFreeCamera = null;
        static public ArrayList mObjects = new ArrayList(1); // track objects for cleaning up

        static public int mTime = 0;

        static public bool InitEngine(IntPtr windowHandle, int width, int height, DebugHandler debugHandler)
        {
            mDebugHandler = debugHandler;

            mWindowHandle = windowHandle;
            mWidth = width;
            mHeight = height;

            try
            {
                // set up window
                mWindow = new Window();
                mWindow.Create("c# window", width, height, 32, false);
                mWindow.ShowWindow();
                SetParent(mWindow.GetHandle(), (int)mWindowHandle);
                SetWindowPos(mWindow.GetHandle(), 0, -4, -30, width, height, 0x0040);
            }
            catch
            {
                MessageBox.Show("Failed to setup an engine window. Check you have all dll's.");
            }

            try
            {
                // set up input
                Input.Create();
                Input.GetInstance().CreateInput(mWindow);
            }
            catch
            {
                MessageBox.Show("Failed to setup engine input. Check you have all dll's.");
            }

            try
            {
                // set up device
                Device.Create();
                Device.GetInstance().SetActiveWindow(mWindow);
                Device.GetInstance().CreateDevice();

                // draw back buffer
                Device.GetInstance().EndRender();
            }
            catch
            {
                MessageBox.Show("Failed to setup an engine device. Check you have all dll's.");
            }

            return true;
        }

        static public bool ShutdownEngine()
        {
            // clean up meshes and objects

            //Input.Destroy();
            Device.Destroy();
            mWindow.Destroy();
            return true;
        }

        static public void Resize(int width, int height)
        {
            if (mWindow == null)
                return;

            mWidth = width;
            mHeight = height;

            SetWindowPos(mWindow.GetHandle(), 0, -4, -30, width, height, 0x0040);
            Device.GetInstance().ResetDevice(width, height);
            mFreeCamera.SetPerspective(65.0f, (float)mWidth / (float)mHeight, 1000.0f, 0.01f);
        }

        static public Camera GetFreeCamera()
        {
            // if there is no free camera, make one and add it to the firsdt renderer
            if (mFreeCamera == null)
            {
                mFreeCamera = new Camera();
                mFreeCamera.SetPerspective(65.0f, (float)mWidth / (float)mHeight, 1000.0f, 0.01f);
                mFreeCamera.SetFreeCam(true);
                mFreeCamera.SetPosition(0.0f, 0.0f, 0.0f);
                mFreeCamera.SetSpeed(0.01f);
            }

            return mFreeCamera;
        }

        static public void AddFreeCamera(int index)
        {
            // if there is no free camera, make one and add it to the firsdt renderer
            if (mFreeCamera == null)
            {
                mFreeCamera = new Camera();
                mFreeCamera.SetPerspective(65.0f, (float)mWidth / (float)mHeight, 1000.0f, 0.01f);
                mFreeCamera.SetFreeCam(true);
                mFreeCamera.SetPosition(0.0f, 0.0f, 0.0f);
                mFreeCamera.SetSpeed(0.01f);
            }

            ((Renderer)mRenderers[index]).SetCamera(mFreeCamera);
        }

        static void RemoveCamera(Renderer renderer)
        {
            renderer.SetCamera(null);
        }

        static public Renderer AddRenderer()
        {
            int idx = mRenderers.Add(new Renderer());
            return (Renderer)mRenderers[idx];
        }

        static public int GetRendererIndex(Renderer renderer)
        {
            return mRenderers.IndexOf(renderer);
        }

        static public void InsertRenderer(int index, Renderer renderer)
        {
            mRenderers.Insert(index, renderer);
        }

        static public void RemoveRenderer(Renderer renderer)
        {
            mRenderers.Remove(renderer);
        }

        static public void RemoveMesh(Renderer renderer, Mesh mesh)
        {
            renderer.RemoveMesh(mesh);
            mObjects.Remove(mesh);
        }

        static public Mesh AddMesh(int rendererIdx, string path)
        {
            // calulate directory for base paths
            int last = path.LastIndexOf('\\') + 1;
            string basePath = path.Substring(0, last);
           
            TextureMgr.SetBasePath(basePath);
            MaterialMgr.SetBasePath(basePath);
            EffectMgr.SetBasePath(basePath);

            Mesh mesh = Mesh.LoadMesh(path);
            mObjects.Add(mesh);
            ((Renderer)mRenderers[rendererIdx]).InsertMesh(mesh);

            return mesh;
        }

        static public void RemoveLight(Renderer renderer, Light light)
        {
            renderer.RemoveLight(light);
            mObjects.Remove(light);
        }

        static public Light AddLight(int rendererIdx)
        {
            Light light = new Light();
            ((Renderer)mRenderers[rendererIdx]).InsertLight(light);

            return light;
        }

        static public void Clear()
        {
            mRenderers.Clear();
            mObjects.Clear(); 
        }

        static public void Update()
        {
            mTime += 100;

            try
            {
                Input.GetInstance().Update();

                if (Input.GetInstance().MouseDown(1))
                    if (mFreeCamera != null)
                        mFreeCamera.Update();

                for (int i = 0; i < mObjects.Count; ++i)
                {
                    if (mObjects[i].GetType() == Type.GetType("Mesh"))
                    {
                        Mesh mesh = (Mesh)mObjects[i];
                        SkeletonAnimation skelAnim = mesh.GetSkeletonAnimation();

                        if (skelAnim != null)
                            skelAnim.Evaluate(mTime);
                    }
                }
            }
            catch (Exception exception)
            {
                string exp = exception.ToString();
            }
        }

        static public void Render()
        {
            EffectMgr.SetBasePath(System.IO.Directory.GetCurrentDirectory());

            Device.GetInstance().BeginRender();

            for (int i = 0; i < mRenderers.Count; ++i)
                ((Renderer)mRenderers[i]).Render();

            Material.Disable();
            mDebugHandler.Invoke(mFreeCamera);

            Device.GetInstance().EndRender();
        }
    }
}