using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Editor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.Run(new frmMain());
            }
            catch (Exception e)
            {
                MessageBox.Show("An unknown error has occured: " + e.ToString());
            }
        }
    }
}