using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Collections;
using System.IO;

namespace Editor
{
    static public class Scene
    {
        [Serializable]
        public class MatrixMaker
        {
            public Vector3Control mRotation = new Vector3Control(0, 0, 0);
            public Vector3Control mScale = new Vector3Control(1, 1, 1);
            public Vector3Control mTranslation = new Vector3Control(0, 0, 0);

            public Matrix4 MakeMatrix()
            {
                Matrix4 matrix = new Matrix4();
                matrix.LoadIdentity();

                Matrix4 s = new Matrix4();
                s.LoadIdentity();
                s.Scale(mScale.mX, mScale.mY, mScale.mZ);

                Matrix4 rx = new Matrix4();
                Matrix4 ry = new Matrix4();
                Matrix4 rz = new Matrix4();
                rx.LoadIdentity();
                ry.LoadIdentity();
                rz.LoadIdentity();

                rx.RotateX(Math.DtoR(mRotation.mX));
                ry.RotateY(Math.DtoR(mRotation.mY));
                rz.RotateZ(Math.DtoR(mRotation.mZ));

                matrix = matrix.Multiply(s);
                matrix = matrix.Multiply((rx.Multiply(ry)).Multiply(rz));
                
                matrix.Translate(mTranslation.mX, mTranslation.mY, mTranslation.mZ);

                return matrix;
            }

            public virtual int GeneratePropertiesPane(Panel pane, int location, EventHandler handler)
            {
                location = mRotation.Add("Rotation", "Rotation", pane, location, handler);
                mRotation.mNmbrCtrlX.Increment = new Decimal(1.0);
                mRotation.mNmbrCtrlY.Increment = new Decimal(1.0);
                mRotation.mNmbrCtrlZ.Increment = new Decimal(1.0);

                location = mTranslation.Add("Position", "Position", pane, location, handler);
                mTranslation.mNmbrCtrlX.Increment = new Decimal(0.005);
                mTranslation.mNmbrCtrlY.Increment = new Decimal(0.005);
                mTranslation.mNmbrCtrlZ.Increment = new Decimal(0.005);

                location = mScale.Add("Scale", "Scale", pane, location, handler);
                mScale.mNmbrCtrlX.Increment = new Decimal(0.01);
                mScale.mNmbrCtrlY.Increment = new Decimal(0.01);
                mScale.mNmbrCtrlZ.Increment = new Decimal(0.01);

                return location;
            }
        }

        [Serializable]
        public class Node
        {
            public BoolControl mDebugMode = new BoolControl(false);
            public BoolControl mVisible = new BoolControl(true);

            public virtual string GetName()
            {
                return "";
            }

            public virtual int GeneratePropertiesPane(Panel pane)
            {
                pane.Controls.Clear();

                int location = 10;

                // FMNOTE: all things should be able to be disabled and a debug mode enabled
                // visibility
                location = mVisible.Add("visible", "Visible", pane, location, new EventHandler(Changed));
                location = mDebugMode.Add("debug", "Debug Mode", pane, location, new EventHandler(Changed));

                // we need a HR control here (divider)
                return location;
            }

            public virtual void Changed(object sender, EventArgs e)
            {

            }

            public virtual void DebugRender(Camera camera)
            {
            
            }

            public virtual void Remove(Renderer renderer)
            {

            }
        }

        [Serializable]
        public class CameraNode : Node
        {
            public FloatControl mFov = new FloatControl(65.0f);
            public MatrixMaker mMatrix = new MatrixMaker();

            [NonSerialized]
            public Camera mCamera = null;

            public override string GetName()
            {
                return "Camera";
            }

            public virtual int GeneratePropertiesPane(Panel pane)
            {
                int location = base.GeneratePropertiesPane(pane);
                location = mFov.Add("fov", "FOV", pane, location, new EventHandler(Changed));
                location = mMatrix.GeneratePropertiesPane(pane, location, new EventHandler(Changed));
                return location;
            }

            public virtual void Changed(object sender, EventArgs e)
            {
                //mCamera.Se
            }

            public virtual void DebugRender(Camera camera)
            {

            }

            public virtual void Remove(Renderer renderer)
            {

            }
        }

        [Serializable]
        public class FreeCameraNode : CameraNode
        {
            private static FreeCameraNode mFreeCamera = null;

            public FreeCameraNode()
            {
                mFreeCamera = this;
                mCamera = EngineScene.GetFreeCamera();
            }

            public override string GetName()
            {
                return "Free Camera";
            }

            public static void Clear()
            {
                mFreeCamera = null;
            }

            public static bool FreeCameraExists()
            {
                return (mFreeCamera != null);
            }

            public static FreeCameraNode GetFreeCamera()
            {
                if (mFreeCamera == null)
                    mFreeCamera = new FreeCameraNode();

                return mFreeCamera;
            }
        }

        [Serializable]
        public class MeshNode : Node
        {
            public string mPath = null;
            public MatrixMaker mMatrix = new MatrixMaker();

            [NonSerialized] 
            public Mesh mMesh = null;            

            public MeshNode(string path)
            {
                mPath = path;
            }

            public override string GetName()
            {
                int lastIdx = mPath.LastIndexOf('\\');
                string fileName = mPath.Substring(lastIdx + 1);

                return "Mesh (" + fileName + ")";
            }

            public override int GeneratePropertiesPane(Panel pane)
            {
                int location = base.GeneratePropertiesPane(pane);

                location = mMatrix.GeneratePropertiesPane(pane, location, new EventHandler(Changed));

                return location;
            }

            public override void Changed(object sender, EventArgs e)
            {
                mMesh.SetTransform(mMatrix.MakeMatrix());
            }

            public override void DebugRender(Camera camera)
            {
                if (!mDebugMode.mBool)
                    return;

                camera.LookAt();
                mMesh.GetTransform().Draw();
            }

            public override void Remove(Renderer renderer)
            {
                EngineScene.RemoveMesh(renderer, mMesh);
            }
        }

        [Serializable]
        public class SkinNode : MeshNode
        {
            public FileControl mSkeleton = null;
            public FileControl mAnimation = null;

            [NonSerialized]
            public SkeletonAnimation mSkeletonAnimation = new SkeletonAnimation();

            public SkinNode(string path, OpenFileDialog openSkeletonDialog, OpenFileDialog openAnimationDialog) : base(path)
            {
                mSkeleton = new FileControl(openSkeletonDialog);
                mAnimation = new FileControl(openAnimationDialog);
            }

            public override string GetName()
            {
                int lastIdx = mPath.LastIndexOf('\\');
                string fileName = mPath.Substring(lastIdx + 1);

                return "Skin (" + fileName + ")";
            }

            public override int GeneratePropertiesPane(Panel pane)
            {
                int location = base.GeneratePropertiesPane(pane);

                location = mSkeleton.Add("Skeleton", "Skeleton", pane, location, new EventHandler(Changed));
                location = mAnimation.Add("Animation", "Animation", pane, location, new EventHandler(Changed));
                mAnimation.mBtnButton.Enabled = false;

                Changed(null, EventArgs.Empty);
                return location;
            }

            public override void Changed(object sender, EventArgs e)
            {
                base.Changed(sender, e);

                if (mSkeletonAnimation == null)
                    mSkeletonAnimation = new SkeletonAnimation();

                // enable animation button and load skeleton
                if (mSkeleton.mFileName != null && mSkeleton.mFileName.Length > 0)
                {
                    if (mAnimation.mBtnButton != null)
                        mAnimation.mBtnButton.Enabled = true;

                    string path = mSkeleton.mFileName;
                    Helper.MakeAbsoluteToApp(ref path);

                    SkeletonMgr.SetBasePath("");
                    mSkeletonAnimation.SetSkeleton(SkeletonMgr.Load(path));                   
                }
                
                // load and play animation
                if (mAnimation.mFileName != null && mAnimation.mFileName.Length > 0)
                {
                    AnimationMgr.SetBasePath("");

                    string path = mAnimation.mFileName;
                    Helper.MakeAbsoluteToApp(ref path);

                    mSkeletonAnimation.AddAnimation(AnimationMgr.Load(path));
                }

                if ((mSkeleton.mFileName != null && mSkeleton.mFileName.Length > 0) &&
                    (mAnimation.mFileName != null && mAnimation.mFileName.Length > 0))
                {
                    mMesh.SetSkeletonAnimation(mSkeletonAnimation); 
                }
                else
                {
                    mMesh.SetSkeletonAnimation(null);
                }
            }

            public override void DebugRender(Camera camera)
            {
                base.DebugRender(camera);

                if (!mDebugMode.mBool)
                    return;

                if (mMesh.GetSkeletonAnimation() != null)
                    mSkeletonAnimation.Draw(mMesh.GetTransform());
            }
        }

        [Serializable]
        public class LightNode : Node
        {
            public Vector3Control mPosition = new Vector3Control(0,0,0);

            [NonSerialized]
            public Light mLight = null;

            public override string GetName()
            {
                return "Light";
            }

            public override int GeneratePropertiesPane(Panel pane)
            {
                int location = base.GeneratePropertiesPane(pane);
                mPosition.Add("Position", "Position", pane, location, new EventHandler(Changed));
                return location;
            }

            public override void Changed(object sender, EventArgs e)
            {
                mLight.SetPosition(new Vector3(mPosition.mX, mPosition.mY, mPosition.mZ));
            }

            public override void DebugRender(Camera camera)
            {
                if (!mDebugMode.mBool)
                    return;

                camera.LookAt();

                Matrix4 debug = new Matrix4();
                debug.LoadIdentity();
                debug.Translate(new Vector3(mPosition.mX, mPosition.mY, mPosition.mZ));
                debug.Draw(0.5f);
            }

            public override void Remove(Renderer renderer)
            {
                EngineScene.RemoveLight(renderer, mLight);
            }
        }

        [Serializable]
        public class RendererNode : Node
        {
            public ArrayList mChildNodes = new ArrayList(1);

            [NonSerialized]
            public object mRenderer = null;

            public override string GetName()
            {
                return "Renderer";
            }

            public override void Changed(object sender, EventArgs e)
            {
                
            }

            public override void DebugRender(Camera camera)
            {
                if (!mDebugMode.mBool)
                    return;

                camera.LookAt();
                Matrix4 identity = new Matrix4();
                identity.LoadIdentity();
                identity.Draw();
            }
        }
    }
}