/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 1.3.25
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class EffectResourceMgr : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal EffectResourceMgr(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(EffectResourceMgr obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~EffectResourceMgr() {
    Dispose();
  }

  public virtual void Dispose() {
    if(swigCPtr.Handle != IntPtr.Zero && swigCMemOwn) {
      swigCMemOwn = false;
      SiphonGL2PINVOKE.delete_EffectResourceMgr(swigCPtr);
    }
    swigCPtr = new HandleRef(null, IntPtr.Zero);
    GC.SuppressFinalize(this);
  }

  public static Effect Load(string name) {
    IntPtr cPtr = SiphonGL2PINVOKE.EffectResourceMgr_Load(name);
    Effect ret = (cPtr == IntPtr.Zero) ? null : new Effect(cPtr, false);
    return ret;
  }

  public static void SetBasePath(string path) {
    SiphonGL2PINVOKE.EffectResourceMgr_SetBasePath(path);
  }

  public EffectResourceMgr() : this(SiphonGL2PINVOKE.new_EffectResourceMgr(), true) {
  }

}
