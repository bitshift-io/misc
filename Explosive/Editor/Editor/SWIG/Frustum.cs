/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 1.3.25
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class Frustum : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal Frustum(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(Frustum obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~Frustum() {
    Dispose();
  }

  public virtual void Dispose() {
    if(swigCPtr.Handle != IntPtr.Zero && swigCMemOwn) {
      swigCMemOwn = false;
      SiphonGL2PINVOKE.delete_Frustum(swigCPtr);
    }
    swigCPtr = new HandleRef(null, IntPtr.Zero);
    GC.SuppressFinalize(this);
  }

  public Frustum.State CullCheck(Sphere cullSphere) {
    Frustum.State ret = (Frustum.State)SiphonGL2PINVOKE.Frustum_CullCheck(swigCPtr, Sphere.getCPtr(cullSphere));
    if (SiphonGL2PINVOKE.SWIGPendingException.Pending) throw SiphonGL2PINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public Frustum.State QuickCullCheck(Sphere sphere) {
    Frustum.State ret = (Frustum.State)SiphonGL2PINVOKE.Frustum_QuickCullCheck(swigCPtr, Sphere.getCPtr(sphere));
    if (SiphonGL2PINVOKE.SWIGPendingException.Pending) throw SiphonGL2PINVOKE.SWIGPendingException.Retrieve();
    return ret;
  }

  public void SetUpFromProjection(Matrix4 projection) {
    SiphonGL2PINVOKE.Frustum_SetUpFromProjection(swigCPtr, Matrix4.getCPtr(projection));
    if (SiphonGL2PINVOKE.SWIGPendingException.Pending) throw SiphonGL2PINVOKE.SWIGPendingException.Retrieve();
  }

  public void Transform(Matrix4 transform) {
    SiphonGL2PINVOKE.Frustum_Transform(swigCPtr, Matrix4.getCPtr(transform));
    if (SiphonGL2PINVOKE.SWIGPendingException.Pending) throw SiphonGL2PINVOKE.SWIGPendingException.Retrieve();
  }

  public void Draw(Vector3 colour) {
    SiphonGL2PINVOKE.Frustum_Draw__SWIG_0(swigCPtr, Vector3.getCPtr(colour));
    if (SiphonGL2PINVOKE.SWIGPendingException.Pending) throw SiphonGL2PINVOKE.SWIGPendingException.Retrieve();
  }

  public void Draw() {
    SiphonGL2PINVOKE.Frustum_Draw__SWIG_1(swigCPtr);
  }

  public Frustum() : this(SiphonGL2PINVOKE.new_Frustum(), true) {
  }

  public enum Sides {
    Top,
    Bottom,
    Left,
    Right,
    Near,
    Far
  }

  public enum Points {
    TopLeftNear,
    TopLeftFar,
    TopRightNear,
    TopRightFar,
    BottomLeftNear,
    BottomLeftFar,
    BottomRightNear,
    BottomRightFar
  }

  public enum State {
    Inside,
    Intersect,
    Outside
  }

}
