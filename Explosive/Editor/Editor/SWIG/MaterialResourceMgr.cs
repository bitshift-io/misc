/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 1.3.25
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class MaterialResourceMgr : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal MaterialResourceMgr(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(MaterialResourceMgr obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~MaterialResourceMgr() {
    Dispose();
  }

  public virtual void Dispose() {
    if(swigCPtr.Handle != IntPtr.Zero && swigCMemOwn) {
      swigCMemOwn = false;
      SiphonGL2PINVOKE.delete_MaterialResourceMgr(swigCPtr);
    }
    swigCPtr = new HandleRef(null, IntPtr.Zero);
    GC.SuppressFinalize(this);
  }

  public static Material Load(string name) {
    IntPtr cPtr = SiphonGL2PINVOKE.MaterialResourceMgr_Load(name);
    Material ret = (cPtr == IntPtr.Zero) ? null : new Material(cPtr, false);
    return ret;
  }

  public static void SetBasePath(string path) {
    SiphonGL2PINVOKE.MaterialResourceMgr_SetBasePath(path);
  }

  public MaterialResourceMgr() : this(SiphonGL2PINVOKE.new_MaterialResourceMgr(), true) {
  }

}
