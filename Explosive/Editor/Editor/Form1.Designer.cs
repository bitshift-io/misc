namespace Editor
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            EngineScene.ShutdownEngine();

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msiMirrorCamera = new System.Windows.Forms.ToolStripMenuItem();
            this.msiExistingCamera = new System.Windows.Forms.ToolStripMenuItem();
            this.msiMesh = new System.Windows.Forms.ToolStripMenuItem();
            this.msiFreeCamera = new System.Windows.Forms.ToolStripMenuItem();
            this.msiRenderer = new System.Windows.Forms.ToolStripMenuItem();
            this.msiAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.msiTextureRenderer = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.msiCamera = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.msiSkin = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.msiLight = new System.Windows.Forms.ToolStripMenuItem();
            this.msiProjector = new System.Windows.Forms.ToolStripMenuItem();
            this.msiRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.msiDown = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.msiUp = new System.Windows.Forms.ToolStripMenuItem();
            this.msScene = new System.Windows.Forms.MenuStrip();
            this.openMeshFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.treeScene = new System.Windows.Forms.TreeView();
            this.engineWindow = new System.Windows.Forms.Panel();
            this.spcVertical = new System.Windows.Forms.SplitContainer();
            this.spcHorizontal = new System.Windows.Forms.SplitContainer();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.msiReset = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.msiOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.msiSave = new System.Windows.Forms.ToolStripMenuItem();
            this.msiSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.msiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpDocumentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMessage = new System.Windows.Forms.Label();
            this.pnlProperties = new System.Windows.Forms.Panel();
            this.openSceneFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveSceneFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openSkeletonFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.openAnimationFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.msScene.SuspendLayout();
            this.spcVertical.Panel1.SuspendLayout();
            this.spcVertical.Panel2.SuspendLayout();
            this.spcVertical.SuspendLayout();
            this.spcHorizontal.Panel1.SuspendLayout();
            this.spcHorizontal.Panel2.SuspendLayout();
            this.spcHorizontal.SuspendLayout();
            this.msMain.SuspendLayout();
            this.pnlProperties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // msiMirrorCamera
            // 
            this.msiMirrorCamera.Enabled = false;
            this.msiMirrorCamera.Name = "msiMirrorCamera";
            this.msiMirrorCamera.Text = "Mirror Camera";
            // 
            // msiExistingCamera
            // 
            this.msiExistingCamera.Enabled = false;
            this.msiExistingCamera.Name = "msiExistingCamera";
            this.msiExistingCamera.Text = "Existing Camera";
            // 
            // msiMesh
            // 
            this.msiMesh.Name = "msiMesh";
            this.msiMesh.Text = "Mesh";
            this.msiMesh.Click += new System.EventHandler(this.msiMesh_Click);
            // 
            // msiFreeCamera
            // 
            this.msiFreeCamera.Name = "msiFreeCamera";
            this.msiFreeCamera.Text = "Free Camera";
            this.msiFreeCamera.Click += new System.EventHandler(this.msiFreeCamera_Click);
            // 
            // msiRenderer
            // 
            this.msiRenderer.Name = "msiRenderer";
            this.msiRenderer.Text = "Renderer";
            this.msiRenderer.Click += new System.EventHandler(this.msiRenderer_Click);
            // 
            // msiAdd
            // 
            this.msiAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.msiRenderer,
            this.msiTextureRenderer,
            this.toolStripSeparator5,
            this.msiFreeCamera,
            this.msiCamera,
            this.msiExistingCamera,
            this.msiMirrorCamera,
            this.toolStripSeparator4,
            this.msiMesh,
            this.msiSkin,
            this.toolStripSeparator3,
            this.msiLight,
            this.msiProjector});
            this.msiAdd.Name = "msiAdd";
            this.msiAdd.Text = "+";
            // 
            // msiTextureRenderer
            // 
            this.msiTextureRenderer.Enabled = false;
            this.msiTextureRenderer.Name = "msiTextureRenderer";
            this.msiTextureRenderer.Text = "Render Texture";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            // 
            // msiCamera
            // 
            this.msiCamera.Enabled = false;
            this.msiCamera.Name = "msiCamera";
            this.msiCamera.Text = "Camera";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            // 
            // msiSkin
            // 
            this.msiSkin.Name = "msiSkin";
            this.msiSkin.Text = "Skin";
            this.msiSkin.Click += new System.EventHandler(this.msiSkin_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            // 
            // msiLight
            // 
            this.msiLight.Name = "msiLight";
            this.msiLight.Text = "Light";
            this.msiLight.Click += new System.EventHandler(this.msiLight_Click);
            // 
            // msiProjector
            // 
            this.msiProjector.Enabled = false;
            this.msiProjector.Name = "msiProjector";
            this.msiProjector.Text = "Projector";
            // 
            // msiRemove
            // 
            this.msiRemove.Name = "msiRemove";
            this.msiRemove.Text = "X";
            this.msiRemove.Click += new System.EventHandler(this.msiRemove_Click);
            // 
            // msiDown
            // 
            this.msiDown.Name = "msiDown";
            this.msiDown.Text = "\\/";
            this.msiDown.Click += new System.EventHandler(this.msiDown_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label3.Location = new System.Drawing.Point(-7, -47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(308, 73);
            this.label3.TabIndex = 21;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(290, 30);
            this.label2.TabIndex = 21;
            this.label2.Text = "Properties";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // msiUp
            // 
            this.msiUp.Name = "msiUp";
            this.msiUp.Text = "/\\";
            this.msiUp.Click += new System.EventHandler(this.msiUp_Click);
            // 
            // msScene
            // 
            this.msScene.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.msScene.Dock = System.Windows.Forms.DockStyle.None;
            this.msScene.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.msiAdd,
            this.msiUp,
            this.msiDown,
            this.msiRemove});
            this.msScene.Location = new System.Drawing.Point(101, 37);
            this.msScene.Name = "msScene";
            this.msScene.Size = new System.Drawing.Size(190, 24);
            this.msScene.TabIndex = 3;
            this.msScene.Text = "menuStrip1";
            // 
            // openMeshFileDialog
            // 
            this.openMeshFileDialog.Filter = "Mesh|*.mdl";
            // 
            // treeScene
            // 
            this.treeScene.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.treeScene.HideSelection = false;
            this.treeScene.Location = new System.Drawing.Point(3, 61);
            this.treeScene.Name = "treeScene";
            this.treeScene.Size = new System.Drawing.Size(290, 291);
            this.treeScene.TabIndex = 3;
            this.treeScene.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeScene_AfterSelect);
            // 
            // engineWindow
            // 
            this.engineWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.engineWindow.Location = new System.Drawing.Point(0, 0);
            this.engineWindow.Name = "engineWindow";
            this.engineWindow.Size = new System.Drawing.Size(292, 294);
            this.engineWindow.TabIndex = 0;
            this.engineWindow.Resize += new System.EventHandler(this.engineWindow_Resize);
            this.engineWindow.Paint += new System.Windows.Forms.PaintEventHandler(this.engineWindow_Paint);
            this.engineWindow.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.engineWindow_MouseDoubleClick);
            // 
            // spcVertical
            // 
            this.spcVertical.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.spcVertical.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.spcVertical.Location = new System.Drawing.Point(0, 0);
            this.spcVertical.Margin = new System.Windows.Forms.Padding(0);
            this.spcVertical.Name = "spcVertical";
            // 
            // spcVertical.Panel1
            // 
            this.spcVertical.Panel1.Controls.Add(this.spcHorizontal);
            // 
            // spcVertical.Panel2
            // 
            this.spcVertical.Panel2.Controls.Add(this.engineWindow);
            this.spcVertical.Size = new System.Drawing.Size(1000, 750);
            this.spcVertical.SplitterDistance = 300;
            this.spcVertical.TabIndex = 20;
            this.spcVertical.Text = "splitContainer1";
            // 
            // spcHorizontal
            // 
            this.spcHorizontal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.spcHorizontal.Location = new System.Drawing.Point(0, 0);
            this.spcHorizontal.Margin = new System.Windows.Forms.Padding(0);
            this.spcHorizontal.Name = "spcHorizontal";
            this.spcHorizontal.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spcHorizontal.Panel1
            // 
            this.spcHorizontal.Panel1.Controls.Add(this.msScene);
            this.spcHorizontal.Panel1.Controls.Add(this.msMain);
            this.spcHorizontal.Panel1.Controls.Add(this.label1);
            this.spcHorizontal.Panel1.Controls.Add(this.treeScene);
            this.spcHorizontal.Panel1.Controls.Add(this.label3);
            // 
            // spcHorizontal.Panel2
            // 
            this.spcHorizontal.Panel2.Controls.Add(this.label2);
            this.spcHorizontal.Panel2.Controls.Add(this.txtMessage);
            this.spcHorizontal.Panel2.Controls.Add(this.pnlProperties);
            this.spcHorizontal.Size = new System.Drawing.Size(301, 750);
            this.spcHorizontal.SplitterDistance = 355;
            this.spcHorizontal.TabIndex = 21;
            this.spcHorizontal.Text = "splitContainer2";
            // 
            // msMain
            // 
            this.msMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.msMain.AutoSize = false;
            this.msMain.Dock = System.Windows.Forms.DockStyle.None;
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(301, 24);
            this.msMain.TabIndex = 1;
            this.msMain.Text = "msMain";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.msiReset,
            this.toolStripSeparator2,
            this.msiOpen,
            this.msiSave,
            this.msiSaveAs,
            this.toolStripSeparator1,
            this.msiExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // msiReset
            // 
            this.msiReset.Name = "msiReset";
            this.msiReset.Text = "&Reset";
            this.msiReset.Click += new System.EventHandler(this.msiReset_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            // 
            // msiOpen
            // 
            this.msiOpen.Name = "msiOpen";
            this.msiOpen.Text = "&Open";
            this.msiOpen.Click += new System.EventHandler(this.msiOpen_Click);
            // 
            // msiSave
            // 
            this.msiSave.Name = "msiSave";
            this.msiSave.Text = "&Save";
            this.msiSave.Click += new System.EventHandler(this.msiSave_Click);
            // 
            // msiSaveAs
            // 
            this.msiSaveAs.Name = "msiSaveAs";
            this.msiSaveAs.Text = "Save &As";
            this.msiSaveAs.Click += new System.EventHandler(this.msiSaveAs_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            // 
            // msiExit
            // 
            this.msiExit.Name = "msiExit";
            this.msiExit.Text = "&Exit";
            this.msiExit.Click += new System.EventHandler(this.msiExit_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.helpDocumentsToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // helpDocumentsToolStripMenuItem
            // 
            this.helpDocumentsToolStripMenuItem.Name = "helpDocumentsToolStripMenuItem";
            this.helpDocumentsToolStripMenuItem.Text = "&Help Documents";
            this.helpDocumentsToolStripMenuItem.Click += new System.EventHandler(this.helpDocumentsToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Location = new System.Drawing.Point(3, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(290, 30);
            this.label1.TabIndex = 20;
            this.label1.Text = "Scene";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMessage
            // 
            this.txtMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMessage.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.txtMessage.Location = new System.Drawing.Point(3, 336);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(290, 15);
            this.txtMessage.TabIndex = 19;
            this.txtMessage.Text = "warning and errors box";
            // 
            // pnlProperties
            // 
            this.pnlProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlProperties.AutoScroll = true;
            this.pnlProperties.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlProperties.Controls.Add(this.numericUpDown1);
            this.pnlProperties.Location = new System.Drawing.Point(3, 29);
            this.pnlProperties.Name = "pnlProperties";
            this.pnlProperties.Size = new System.Drawing.Size(290, 304);
            this.pnlProperties.TabIndex = 22;
            // 
            // openSceneFileDialog
            // 
            this.openSceneFileDialog.FileName = "openSceneFileDialog";
            this.openSceneFileDialog.Filter = "Scene|*.sce";
            // 
            // saveSceneFileDialog
            // 
            this.saveSceneFileDialog.Filter = "Scene|*.sce";
            // 
            // openSkeletonFileDialog
            // 
            this.openSkeletonFileDialog.Filter = "Skeleton|*.skl";
            // 
            // openAnimationFileDialog
            // 
            this.openAnimationFileDialog.Filter = "Animation|*.anm";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(131, 120);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 0;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 716);
            this.Controls.Add(this.spcVertical);
            this.Name = "frmMain";
            this.Text = "SiphonGL Editor";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.msScene.ResumeLayout(false);
            this.spcVertical.Panel1.ResumeLayout(false);
            this.spcVertical.Panel2.ResumeLayout(false);
            this.spcVertical.ResumeLayout(false);
            this.spcHorizontal.Panel1.ResumeLayout(false);
            this.spcHorizontal.Panel1.PerformLayout();
            this.spcHorizontal.Panel2.ResumeLayout(false);
            this.spcHorizontal.ResumeLayout(false);
            this.msMain.ResumeLayout(false);
            this.pnlProperties.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem msiMirrorCamera;
        private System.Windows.Forms.ToolStripMenuItem msiExistingCamera;
        private System.Windows.Forms.ToolStripMenuItem msiMesh;
        private System.Windows.Forms.ToolStripMenuItem msiFreeCamera;
        private System.Windows.Forms.ToolStripMenuItem msiRenderer;
        private System.Windows.Forms.ToolStripMenuItem msiAdd;
        private System.Windows.Forms.ToolStripMenuItem msiTextureRenderer;
        private System.Windows.Forms.ToolStripMenuItem msiCamera;
        private System.Windows.Forms.ToolStripMenuItem msiSkin;
        private System.Windows.Forms.ToolStripMenuItem msiLight;
        private System.Windows.Forms.ToolStripMenuItem msiProjector;
        private System.Windows.Forms.ToolStripMenuItem msiRemove;
        private System.Windows.Forms.ToolStripMenuItem msiDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem msiUp;
        private System.Windows.Forms.MenuStrip msScene;
        private System.Windows.Forms.OpenFileDialog openMeshFileDialog;
        private System.Windows.Forms.TreeView treeScene;
        private System.Windows.Forms.Panel engineWindow;
        private System.Windows.Forms.SplitContainer spcVertical;
        private System.Windows.Forms.SplitContainer spcHorizontal;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label txtMessage;
        private System.Windows.Forms.OpenFileDialog openSceneFileDialog;
        private System.Windows.Forms.SaveFileDialog saveSceneFileDialog;
        private System.Windows.Forms.Panel pnlProperties;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem msiOpen;
        private System.Windows.Forms.ToolStripMenuItem msiSave;
        private System.Windows.Forms.ToolStripMenuItem msiSaveAs;
        private System.Windows.Forms.ToolStripMenuItem msiReset;
        private System.Windows.Forms.ToolStripMenuItem msiExit;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpDocumentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.OpenFileDialog openSkeletonFileDialog;
        private System.Windows.Forms.OpenFileDialog openAnimationFileDialog;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
    }
}

