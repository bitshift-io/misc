using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Threading;

using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.IO;

namespace Editor
{
    public partial class frmMain : Form
    {
        // lets us save what file we are editing
        string mCurOpenedScene;

        bool mRunning = true;
        Thread mRenderThread;

        public frmMain()
        {
            InitializeComponent();

            // set up engine windows size
            engineWindow.Size = spcVertical.Panel2.Size;

            // lets create an opengl device and attach it to our window
            if (EngineScene.InitEngine(engineWindow.Handle, engineWindow.Size.Width, engineWindow.Size.Height, new EngineScene.DebugHandler(DebugRender)) == false)
                SetMessage("Failed to initilise the engine");
        }

        private void SetMessage(string message)
        {
            txtMessage.Text = message;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            // set up defaults here
            msiReset_Click(sender, e);

            // create a thread to constantly render
            mRenderThread = new Thread(new ThreadStart(Run));
            mRenderThread.Start();
        }

        private void Run()
        {
            while (mRunning)
            {
                Invalidate(engineWindow.DisplayRectangle, false); // Force OnPaint() to get called again.
                System.Threading.Thread.Sleep(20);
            }
        }

        public void DebugRender(Camera camera)
        {
            DebugRender(treeScene.Nodes, camera);
        }

        public void DebugRender(TreeNodeCollection collection, Camera camera)
        {
            for (int i = 0; i < collection.Count; ++i)
            {
                TreeNode treeNode = collection[i];
                DebugRender(treeNode.Nodes, camera);

                Scene.Node node = (Scene.Node)treeNode.Tag;

                if (node == null)
                    continue;

                node.DebugRender(camera);
            }
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            EngineScene.Update();
            EngineScene.Render();
        }

        public TreeNode CreateTreeNode(string name, Scene.Node sceneNode)
        {
            TreeNode node = new TreeNode(name);
            node.Tag = sceneNode;
            return node;
        }

        private void msiMesh_Click(object sender, EventArgs e)
        {
            // get the file to open
            DialogResult result = openMeshFileDialog.ShowDialog();
            if (result != DialogResult.OK)
                return;

            string path = openMeshFileDialog.FileName;
            Helper.MakeRelativeToApp(ref path);

            SetMessage("Opening mesh " + path);
                        
            // iterate too root to get the renderer
            TreeNode node = GetRendererTreeNode(treeScene.SelectedNode);

            Scene.MeshNode meshNode = new Scene.MeshNode(path);
            node.Nodes.Add(CreateTreeNode(meshNode.GetName(), meshNode));

            // just add to the first renderer for a bit while testing
            // TODO: get renderer index
            meshNode.mMesh = EngineScene.AddMesh(GetRendererIndex(treeScene.SelectedNode), openMeshFileDialog.FileName);
        }

        private void msiRenderer_Click(object sender, EventArgs e)
        {
            Scene.RendererNode renderer = new Scene.RendererNode();
            TreeNode node = CreateTreeNode(renderer.GetName(), renderer);
            treeScene.Nodes.Add(node);
            renderer.mRenderer = EngineScene.AddRenderer();
            treeScene.SelectedNode = node;
        }

        private void msiOpen_Click(object sender, EventArgs e)
        {
            // get the file to open
            DialogResult result = openSceneFileDialog.ShowDialog();
            if (result != DialogResult.OK)
                return;

            mCurOpenedScene = openSceneFileDialog.FileName;
            treeScene.Nodes.Clear();
            EngineScene.Clear();

            Scene.FreeCameraNode.Clear();

            using (BinaryReader binaryFile = new BinaryReader(new FileStream(mCurOpenedScene, FileMode.Open)))
            {
                BinaryFormatter serialise = new BinaryFormatter();
                if (Open(null, treeScene.Nodes, binaryFile, serialise) == false)
                    SetMessage("Failed to open file" + mCurOpenedScene);
            }

            treeScene.ExpandAll();
        }

        private bool Open(TreeNode parent, TreeNodeCollection nodes, BinaryReader stream, BinaryFormatter serialise)
        {
            int count = stream.ReadInt32(); 
            for (int i = 0; i < count; ++i)
            {
                try
                {
                    // deserialize everything
                    Scene.Node node = (Scene.Node)serialise.Deserialize(stream.BaseStream);

                    // we only want to serialize in the first free camera, 
                    Type nodeType = node.GetType();

                    // TODO: free cam is a bit of a problem as its static,
                    // we should load the first one from file, then simply return that for the others
                    // as they are all the same. 
                    if (/*Scene.FreeCameraNode.FreeCameraExists() &&*/ nodeType == Type.GetType("Editor.Scene+FreeCameraNode"))
                    {
                        node = Scene.FreeCameraNode.GetFreeCamera();
                        EngineScene.AddFreeCamera(GetRendererIndex(parent));
                    }
                    else if (nodeType == Type.GetType("Editor.Scene+SkinNode") ||
                             nodeType == Type.GetType("Editor.Scene+MeshNode"))
                    {
                        Scene.MeshNode meshNode = (Scene.MeshNode)node;
                        string path = meshNode.mPath;
                        Helper.MakeAbsoluteToApp(ref path);
                        meshNode.mMesh = EngineScene.AddMesh(GetRendererIndex(parent), path);
                    }
                    else if (nodeType == Type.GetType("Editor.Scene+RendererNode"))
                    {
                        Scene.RendererNode rendererNode = (Scene.RendererNode)node;
                        rendererNode.mRenderer = EngineScene.AddRenderer();
                    }
                    else if (nodeType == Type.GetType("Editor.Scene+LightNode"))
                    {
                        Scene.LightNode lightNode = (Scene.LightNode)node;
                        lightNode.mLight = EngineScene.AddLight(GetRendererIndex(parent));
                    }

                    // this will force the nodes to set them selves in correct position etc...
                    node.Changed(null, EventArgs.Empty);

                    TreeNode treeNode = CreateTreeNode(node.GetName(), node);
                    nodes.Add(treeNode);

                    if (Open(treeNode, treeNode.Nodes, stream, serialise) == false)
                        return false;
                }
                catch (Exception e)
                {
                    string msg = e.ToString();
                    SetMessage(msg);
                    return false;
                }  
            }

            return true;
        }

        private void msiSave_Click(object sender, EventArgs e)
        {
            // if a scene has not been opened, open the save as dialog
            if (mCurOpenedScene == null)
            {
                msiSaveAs_Click(sender, e);
                return;
            }

            using (BinaryWriter binaryFile = new BinaryWriter(new FileStream(mCurOpenedScene, FileMode.Create)))
            {
                BinaryFormatter serialise = new BinaryFormatter();
                Save(treeScene.Nodes, binaryFile, serialise);
            }
        }

        private void Save(TreeNodeCollection nodes, BinaryWriter stream, BinaryFormatter serialise)
        {
            stream.Write(nodes.Count);
            foreach (TreeNode n in nodes)
            {
                try
                {
                    // serialize everything
                    serialise.Serialize(stream.BaseStream, n.Tag);
                }
                catch (Exception e)
                {
                    string msg = e.ToString();
                    SetMessage(msg);
                }
                               
                Save(n.Nodes, stream, serialise);
            }
        }

        private void msiSaveAs_Click(object sender, EventArgs e)
        {
            // get the file to open
            DialogResult result = saveSceneFileDialog.ShowDialog();
            if (result != DialogResult.OK)
                return;

            mCurOpenedScene = saveSceneFileDialog.FileName;
            msiSave_Click(sender, e);
        }

        private void msiReset_Click(object sender, EventArgs e)
        {
            treeScene.Nodes.Clear();
            EngineScene.Clear();

            msiRenderer_Click(sender, e);
            msiFreeCamera_Click(sender, e);

            treeScene.ExpandAll();
        }

        private void msiRemove_Click(object sender, EventArgs e)
        {
            // if we have opted to remove a renderer, remove it
            if (treeScene.Nodes.IndexOf(treeScene.SelectedNode) != -1)
            {
                EngineScene.RemoveRenderer((Renderer)((Scene.RendererNode)treeScene.SelectedNode.Tag).mRenderer);
                treeScene.SelectedNode.Remove();
                return;
            }

            Renderer rootRenderer = (Renderer)GetRendererNode(treeScene.SelectedNode).mRenderer;

            Scene.Node node = (Scene.Node)treeScene.SelectedNode.Tag;
            node.Remove(rootRenderer);

            treeScene.SelectedNode.Remove();
        }

        TreeNode GetRendererTreeNode(TreeNode node)
        {
            // iterate too root to get the renderer node
            TreeNode temp = node;
            while (temp.Parent != null)
                temp = temp.Parent;

            return temp;
        }

        Scene.RendererNode GetRendererNode(TreeNode node)
        {
            // iterate too root to get the renderer node
            TreeNode temp = GetRendererTreeNode(node);
            return (Scene.RendererNode)temp.Tag;
        }

        int GetRendererIndex(TreeNode node)
        {
            // iterate too root to get the renderer node
            TreeNode temp = GetRendererTreeNode(node);
            return treeScene.Nodes.IndexOf(temp);
        }

        private void treeScene_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // iterate too root to get the renderer
            TreeNode node = treeScene.SelectedNode;

            Scene.Node sceneNode = (Scene.Node)node.Tag;

            // disable/enable up and down controls
            bool enable = node.Parent == null;
            msiUp.Enabled = enable;
            msiDown.Enabled = enable;

            if (sceneNode == null)
            {
                pnlProperties.Controls.Clear();
                return;
            }

            sceneNode.GeneratePropertiesPane(pnlProperties);
        }

        private void msiUp_Click(object sender, EventArgs e)
        {
            TreeNode node = treeScene.SelectedNode;
            int curIdx = treeScene.Nodes.IndexOf(node);

            if (curIdx <= 0)
                return;

            int newIdx = curIdx - 1;

            treeScene.Nodes.Remove(node);
            treeScene.Nodes.Insert(newIdx, node);

            Renderer renderer = (Renderer)((Scene.RendererNode)node.Tag).mRenderer;
            //curIdx = EngineScene.GetRendererIndex(renderer); // we should check this
            EngineScene.RemoveRenderer(renderer);
            EngineScene.InsertRenderer(newIdx, renderer);

            // select the newly moved renderer
            treeScene.SelectedNode = node;
        }

        private void msiDown_Click(object sender, EventArgs e)
        {
            TreeNode node = treeScene.SelectedNode;
            int curIdx = treeScene.Nodes.IndexOf(node);

            if (curIdx >= treeScene.Nodes.Count - 1)
                return;

            int newIdx = curIdx + 1;

            treeScene.Nodes.Remove(node);
            treeScene.Nodes.Insert(newIdx, node);

            Renderer renderer = (Renderer)((Scene.RendererNode)node.Tag).mRenderer;
            //curIdx = EngineScene.GetRendererIndex(renderer); // we should check this
            EngineScene.RemoveRenderer(renderer);
            EngineScene.InsertRenderer(newIdx, renderer);

            // select the newly moved renderer
            treeScene.SelectedNode = node;
        }

        private void engineWindow_Paint(object sender, PaintEventArgs e)
        {
            Invalidate(engineWindow.DisplayRectangle, false);
        }

        private void engineWindow_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void engineWindow_Resize(object sender, EventArgs e)
        {
            EngineScene.Resize(engineWindow.Width, engineWindow.Height);
        }

        private void msiLight_Click(object sender, EventArgs e)
        {
            SetMessage("Adding light to renderer");

            // iterate too root to get the renderer
            TreeNode node = GetRendererTreeNode(treeScene.SelectedNode);

            Scene.LightNode lightNode = new Scene.LightNode();
            node.Nodes.Add(CreateTreeNode(lightNode.GetName(), lightNode));

            lightNode.mLight = EngineScene.AddLight(GetRendererIndex(treeScene.SelectedNode));
        }

        private void msiExit_Click(object sender, EventArgs e)
        {
            mRunning = false;
            Application.Exit();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            mRunning = false;
        }

        private void msiFreeCamera_Click(object sender, EventArgs e)
        {
            TreeNode node = GetRendererTreeNode(treeScene.SelectedNode);
            node.Nodes.Add(CreateTreeNode("Free Camera", Scene.FreeCameraNode.GetFreeCamera()));
            EngineScene.AddFreeCamera(GetRendererIndex(treeScene.SelectedNode));
        }

        private void msiSkin_Click(object sender, EventArgs e)
        {
            // get the file to open
            DialogResult result = openMeshFileDialog.ShowDialog();
            if (result != DialogResult.OK)
                return;

            string path = openMeshFileDialog.FileName;
            Helper.MakeRelativeToApp(ref path);

            SetMessage("Opening skin " + path);

            // iterate too root to get the renderer
            TreeNode node = GetRendererTreeNode(treeScene.SelectedNode);

            Scene.SkinNode skinNode = new Scene.SkinNode(path, openSkeletonFileDialog, openAnimationFileDialog);
            node.Nodes.Add(CreateTreeNode(skinNode.GetName(), skinNode));

            skinNode.mMesh = EngineScene.AddMesh(GetRendererIndex(treeScene.SelectedNode), openMeshFileDialog.FileName);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = new frmAboutBox();
            frm.ShowDialog();
        }

        private void helpDocumentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WebHelp help = new WebHelp();
            help.ShowDialog();
        }
    }
}