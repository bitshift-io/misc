#include "Memory.h"

unsigned int MemTracker::mAllocationCount = 0;
unsigned int MemTracker::mAllocationSize = 0;

unsigned int MemTracker::mPrefixPattern          = 0xbaadf00d; // Fill pattern for bytes preceeding allocated blocks
unsigned int MemTracker::mPostfixPattern         = 0xdeadc0de; // Fill pattern for bytes following allocated blocks
unsigned int MemTracker::mUnusedPattern          = 0xfeedface; // Fill pattern for freshly allocated blocks
unsigned int MemTracker::mReleasedPattern        = 0xdeadbeef; // Fill pattern for deallocated blocks

const char*		MemTracker::mSourceFile = "";
unsigned int	MemTracker::mSourceLine = 0; 
const char*		MemTracker::mSourceFunc = "";

MemTracker::Allocation*		MemTracker::mAllocations = 0;

const char* MemTracker::mAllocationTypes[] = 
{
	"Unknown",
	"new",     
	"new[]",  
	"malloc",   
	"calloc",
	"realloc", 
	"delete", 
	"delete[]", 
	"free"
};

//
// Turn off my macros
//

#undef	new
#undef	delete
#undef	malloc
#undef	calloc
#undef	realloc
#undef	free

void MemTracker::SetOwner(const char* file, unsigned int line, const char* func)
{
	mSourceFile = file;
	mSourceLine = line;
	mSourceFunc = func;
}

void* MemTracker::Allocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, unsigned int reportedSize)
{
	++mAllocationCount;
	mAllocationSize += reportedSize;

	// allocate enough space for the Allocation, what the app wants, as well as buffer over/under run checks
	void* memory = malloc(reportedSize + sizeof(Allocation) + sizeof(unsigned int) * 2);
	Allocation* allocation = static_cast<Allocation*>(memory);
	allocation->mRealAddress = static_cast<char*>(memory) + sizeof(Allocation) + sizeof(unsigned int);

	// set up the buffer over/under run checks

	// set up the linked list
	allocation->mPrevious = 0;
	allocation->mNext = mAllocations;

	if (mAllocations)
		mAllocations->mPrevious = allocation;

	mAllocations = allocation;
	

	// set up the debug vars

	// strip to back slash
	int len = strlen(file);
	for (int i = len; i > 0; --i)
	{
		if (file[i - 1] == '\\' || file[i - 1] == '/')
		{
			allocation->mFile = &file[i];
			break;
		}
	}
	allocation->mLine = line;
	allocation->mFunc = func;
	allocation->mAllocationType = allocationType;
	allocation->mReportedSize = reportedSize;
	allocation->mReportedAddress = 0;
	allocation->mType = (MemType)allocationType;

	return allocation->mRealAddress;
}

void* MemTracker::Reallocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, unsigned int reportedSize, void* reportedAddress)
{
	return realloc(reportedAddress, reportedSize);
}

void MemTracker::Deallocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, void* reportedAddress)
{
	if (reportedAddress == 0)
		return;

	Allocation* allocation = static_cast<Allocation*>(CalulateRealAddress(reportedAddress));

	--mAllocationCount;
	mAllocationSize -= allocation->mReportedSize;

	// remove from the doubly linked list
	if (allocation->mPrevious == 0)
	{
		mAllocations = allocation->mNext;
		if (mAllocations)
			mAllocations->mPrevious = 0;
	}
	else
	{
		Allocation* prev = allocation->mPrevious;
		Allocation* next = allocation->mNext;

		prev->mNext = next;
		if (next)
			next->mPrevious = prev;
	}

	return free(allocation);
}

void* MemTracker::CalulateRealAddress(void* reportedAddress)
{
	return static_cast<char*>(reportedAddress) - (sizeof(Allocation) + sizeof(unsigned int));
}

unsigned int MemTracker::GetAllocationSize()
{
	return mAllocationSize;
}

unsigned int MemTracker::GetAllocationCount()
{
	return mAllocationCount;
}

void MemTracker::Log()
{
	File file("memory.log", "wt");

	file.WriteString("Number of leaks :\t%i\n", mAllocationCount);
	file.WriteString("Memory leaked   :\t%i\n", mAllocationSize);
	file.WriteString("\n");

	file.WriteString("Size\tType\t\tFile:Line::FunctionSize\n");
	file.WriteString("----------------------------------------------------------------------\n");
	Allocation* al = mAllocations;
	while (al)
	{
		file.WriteString("%i\t%s\t\t%s:%i::%s\n", al->mReportedSize, mAllocationTypes[al->mType], al->mFile, al->mLine, al->mFunc);
		al = al->mNext;
	}
}


void* operator new(size_t reportedSize)
{
	return MemTracker::Allocator(MemTracker::mSourceFile, MemTracker::mSourceLine, MemTracker::mSourceFunc, MemTracker::eNew, reportedSize);
}

void* operator new[](size_t reportedSize)
{
	return MemTracker::Allocator(MemTracker::mSourceFile, MemTracker::mSourceLine, MemTracker::mSourceFunc, MemTracker::eNewArray, reportedSize);
}

void operator delete(void *reportedAddress)
{
	MemTracker::Deallocator(MemTracker::mSourceFile, MemTracker::mSourceLine, MemTracker::mSourceFunc, MemTracker::eDelete, reportedAddress);
}

void operator delete[](void *reportedAddress)
{
	MemTracker::Deallocator(MemTracker::mSourceFile, MemTracker::mSourceLine, MemTracker::mSourceFunc, MemTracker::eDeleteArray, reportedAddress);
}

#ifdef _WIN32

void* operator new(size_t reportedSize, const char *sourceFile, int sourceLine)
{
	return MemTracker::Allocator(MemTracker::mSourceFile, MemTracker::mSourceLine, "???", MemTracker::eNew, reportedSize);
}

void* operator new[](size_t reportedSize, const char *sourceFile, int sourceLine)
{
	return MemTracker::Allocator(MemTracker::mSourceFile, MemTracker::mSourceLine, "???", MemTracker::eNewArray, reportedSize);
}

#endif

class StaticLog
{
public:
	StaticLog() { }
	~StaticLog()
	{
		MemTracker::Log();
	}
};
static StaticLog sStaticLog;

