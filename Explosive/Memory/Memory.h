
#ifndef _MEMORY_H_
#define _MEMORY_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../Util/File.h"

using namespace Siphon;

#ifdef	WIN32
	#ifdef	_DEBUG
	#define	Assert(x) if ((x) == false) __asm { int 3 }
	#else
	#define	Assert(x) {}
	#endif
#else	// Linux uses assert, which we can use safely, since it doesn't bring up a dialog within the program.
	#define	Assert assert
#endif

//
// NOTE: If we make this a singleton, we should be able to override it with our memory pools
//	reportedAddress - the actual address of the data,
//	realAddress - includes padding to check for buffer over/under runs
//
class MemTracker
{
public:
	
	enum MemType
	{
		eUnknown,
		eNew,
		eNewArray,
		eMalloc,
		eCalloc,
		eRealloc,
		eDelete,
		eDeleteArray,
		eFree
	};

	struct Allocation
	{
		const char*		mFile;
		unsigned int	mLine;
		const char*		mFunc;
		unsigned int	mReportedSize;
		unsigned int	mAllocationType;
		void*			mReportedAddress;
		void*			mRealAddress;
		MemType			mType;
		Allocation*		mNext;
		Allocation*		mPrevious;
	};

	static void SetOwner(const char* file, unsigned int line, const char* func);
	static void* Allocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, unsigned int reportedSize);
	static void* Reallocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, unsigned int reportedSize, void* reportedAddress);
	static void Deallocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, void* reportedAddress);
	static void* CalulateRealAddress(void* reportedAddress);
	static unsigned int GetAllocationSize();
	static unsigned int GetAllocationCount();
	static void Log();

//protected:
	static unsigned int mAllocationCount;
	static unsigned int mAllocationSize;
	static const char*	mAllocationTypes[];

	static unsigned int	mPrefixPattern;
	static unsigned int	mPostfixPattern;
	static unsigned int	mUnusedPattern;
	static unsigned int	mReleasedPattern;

	static const char*	mSourceFile;
	static unsigned int mSourceLine; 
	static const char*	mSourceFunc;

	static Allocation*	mAllocations; // head of the doubly linked list
};

void* operator new(size_t reportedSize);
void* operator new[](size_t reportedSize);
void operator delete(void *reportedAddress);
void operator delete[](void *reportedAddress);

#ifdef _WIN32

void* operator new(size_t reportedSize, const char *sourceFile, int sourceLine);
void* operator new[](size_t reportedSize, const char *sourceFile, int sourceLine);

#endif



#define	new				(MemTracker::SetOwner(__FILE__,__LINE__,__FUNCTION__), false) ? 0 : new
#define	delete			(MemTracker::SetOwner(__FILE__,__LINE__,__FUNCTION__), false) ? MemTracker::SetOwner("",0,"") : delete
#define	malloc(sz)		MemTracker::Allocator(__FILE__,__LINE__,__FUNCTION__, MemTracker::eMalloc, sz)
#define	calloc(sz)		MemTracker::Allocator(__FILE__,__LINE__,__FUNCTION__, MemTracker::eCalloc, sz)
#define	realloc(ptr,sz)	MemTracker::Reallocator(__FILE__,__LINE__,__FUNCTION__, MemTracker::eRealloc, sz, ptr)
#define	free(ptr)		MemTracker::Deallocator(__FILE__,__LINE__,__FUNCTION__, MemTracker::eFree, ptr)

#endif