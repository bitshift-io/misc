#include "../Multithread/pch.h"
#include "../Memory/Memory.h"
#include "../Util/Log.h"
#include "../Util/Queue.h"

unsigned long MyTask(void* ptr)
{
	Log::Print("MyTask\n");
	return 0;
}

unsigned long MyTask2(void* ptr)
{
	Log::Print("MyTask2\n");
	if (gTaskMgr.IsValid())
		gTaskMgr.AddTask(Task(MyTask));

	return 0;
}

int main(int argc, void* args[])
{
	Queue<int> test;

	// add 4
	for (int i = 0; i < 4; ++i)
		test.Insert(i);

	// pop 2
	for (int i = 0; i < 2; ++i)
		test.Remove();

	test.SetCapacity(test.GetSize() * 2);

	// add 4
	for (int i = 0; i < 4; ++i)
		test.Insert(4 + i);


	// test core stuff
	Thread thread;
	Thread thread2;

	thread.Start(Task(MyTask));
	thread2.Start(Task(MyTask2));

	thread.Wait();
	thread2.Wait();

	// test mutex stuffs
	TaskMgr::Create();
	gTaskMgr.Init(2);

	for (int i = 0; i < 1000; ++i)
	{
		gTaskMgr.AddTask(Task(MyTask));
		gTaskMgr.AddTask(Task(MyTask2));
	}

	gTaskMgr.Start();

	gTaskMgr.Wait();
	gTaskMgr.Deinit();
	TaskMgr::Destroy();

	return 0;
}