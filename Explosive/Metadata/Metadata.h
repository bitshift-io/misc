#ifndef _METADATA_H_
#define _METADATA_H_

#include "../Util/File.h"

using namespace Siphon;

class Metadata
{
	friend class MetadataFactory;
public:

	Metadata(const char* name);

	//
	// Serialize to file
	//
	virtual bool SerializeTo(File& file) const = 0;
	virtual bool SerializeTo(const void* data, File& file) const = 0;
	virtual bool SerializeTo(const void* data, const char* file) const;

	//
	// Serialize from file
	//
	virtual bool SerializeFrom(File& file) const = 0;
	virtual bool SerializeFrom(const void* data, File& file) const = 0;
	virtual bool SerializeFrom(const void* data, const char* file) const;

	const char* GetName() const;

protected:
	const char* mName;
	Metadata*	mNext;
};

class MetaEntry
{
public:

	typedef void* (*FnCreate)();
	typedef void (*FnDestroy)();

protected:

	FnCreate	mCreateFn;
	FnDestroy	mDestroyFn;
	Metadata	*mMetadata;
};

#endif