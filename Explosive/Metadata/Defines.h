#ifndef _METADATA_DEFINES_
#define _METADATA_DEFINES_

//
// Notes: _C means it takes custom data
//

//
// Class defines
//

#define METADATA_CLASS_EXTERN(n)				\
extern MetadataClass s##n##Metadata;


#define METADATA_CLASS_FOWARDECLARE(n)				\
class MetadataClass s##n##Metadata;


#define METADATA_CLASS_START(n)					\
n s##n##Temp;										\
static MetadataClass::TypeInfo s##n##MetadataVariables[] = {


#define METADATA_CLASS_END(n)									\
	{0, 0, 0},													\
};																\
MetadataClass s##n##Metadata(#n, s##n##MetadataVariables, "");


#define METADATA_CLASS_END_C(n, c)				\
	{0, 0, 0},									\
};												\
MetadataClass s##n##Metadata(#n, s##n##MetadataVariables, c);



#define METADATA_CLASS_VARIABLE(n, t, p)												\
{#t, #p, (int)&s##n##Temp.p - (int)&s##n##Temp, ""},


#define METADATA_CLASS_VARIABLE_C(n, t, p, c)											\
{#t, #p, (int)&s##n##Temp.p - (int)&s##n##Temp, c},


#define METADATA_DECLARE(n)																\
public:																					\
	static n* Create() { return new n(); }												\
	static void Destory(n* ptr) { delete ptr; }											\
	const Metadata* GetMetadata() { return &s##n##Metadata; }							\
	bool SerializeTo(File& file) { return GetMetadata()->SerializeTo((const void*)this, file); }		\
	bool SerializeTo(const char* file) { return GetMetadata()->SerializeTo((const void*)this, file); }


//
// Enum defines
//

#define METADATA_ENUM_EXTERN(n)					\
extern MetadataEnum s##n##Metadata;


#define METADATA_ENUM_START(n)					\
static MetadataEnum::TypeInfo s##n##MetadataVariables[] = {


#define METADATA_ENUM_VARIABLE(n)				\
{n, #n},


#define METADATA_ENUM_END(n, f)					\
	{0, 0},										\
};												\
MetadataEnum s##n##Metadata(#n, s##n##MetadataVariables, f, sizeof(n));



//
// Basic types
//

#define METADATA_BASICTYPE_EXTERN(n)				\
extern MetadataBasicType s##n##Metadata;


//#define METADATA_BASICTYPE_EXTERN(n, m)			\
//extern MetadataBasicType s##n##m##Metadata;


#define METADATA_BASICTYPE(n, f)					\
MetadataBasicType s##n##Metadata(#n, sizeof(n), f);


//#define METADATA_BASICTYPE(n, m)	\
//MetadataBasicType s##n##m##Metadata(#n#m, sizeof(n));

#endif