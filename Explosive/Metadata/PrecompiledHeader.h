#ifndef _METADATA_PRECOMPILEDHEADER_H_
#define _METADATA_PRECOMPILEDHEADER_H_

#include "Defines.h"
#include "Metadata.h"
#include "MetadataFactory.h"
#include "MetadataClass.h"
#include "MetadataEnum.h"
#include "MetadataBasicType.h"

METADATA_BASICTYPE_EXTERN(int)

#endif