#ifndef _METADATACLASS_H_
#define _METADATACLASS_H_

#include "../Util/File.h"

using namespace Siphon;

class MetadataClass : public Metadata
{
	friend class MetadataFactory;
public:

	struct TypeInfo
	{
		const char*			type;		// eg. int, float
		const char* 		name;
		int					offset;
		const char*			custom;		// custom string
	};

	MetadataClass(const char* name, TypeInfo* vars, const char* custom);

	virtual bool SerializeTo(File& file) const;
	virtual bool SerializeTo(const void* data, File& file) const;

	virtual bool SerializeFrom(File& file) const;
	virtual bool SerializeFrom(const void* data, File& file) const;

protected:
	TypeInfo*	mVariables;
	int			mCount;
	const char* mCustom;
};

#endif