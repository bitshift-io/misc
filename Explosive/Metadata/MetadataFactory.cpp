#include "PrecompiledHeader.h"
#include "MetadataFactory.h"

Destructor MetadataFactory::mDestructor;
MetadataFactory* MetadataFactory::mInstance = 0;

MetadataFactory::MetadataFactory() :
	mMetadataList(0)
{
	
}

MetadataFactory::~MetadataFactory()
{

}

void MetadataFactory::Register(Metadata* metadata)
{
	metadata->mNext = mMetadataList;
	mMetadataList = metadata;
}

bool MetadataFactory::SerializeTo(const char* file)
{
	File f(file, "wt");
	return SerializeTo(f);
}

bool MetadataFactory::SerializeTo(File& file)
{
	if (!file.Valid())
		return false;

	Metadata* current = mMetadataList;
	while (current)
	{
		current->SerializeTo(file);
		current = current->mNext;
	}

	return true;
}

Metadata* MetadataFactory::GetMetadata(const char* name)
{
	if (name == 0)
		return 0;

	Metadata* current = mMetadataList;
	while (current)
	{
		if (strcmp(current->GetName(), name) == 0)
			return current;

		current = current->mNext;
	}

	return 0;
}

bool MetadataFactory::SerializeFrom(const char* file)
{
	File f(file, "rt");
	return SerializeFrom(f);
}

bool MetadataFactory::SerializeFrom(File& file)
{
	// TODO - finish
	return true;
}


MetadataFactory& MetadataFactory::GetInstance()
{ 
	if (!mInstance)
	{
		mInstance = new MetadataFactory();
		mDestructor.SetFnPtr(MetadataFactory::Destory);
	}

	return *mInstance; 
}

void MetadataFactory::Destory()
{
	delete mInstance;
	mInstance = 0;
}
	