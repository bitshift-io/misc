#include "PrecompiledHeader.h"
#include "Metadata.h"

Metadata::Metadata(const char* name) :
	mName(name),
	mNext(0)
{
	gMetadataFactory.Register(this);
}

const char* Metadata::GetName() const 
{ 
	return mName; 
}

bool Metadata::SerializeTo(const void* data, const char* file) const
{
	File f(file, "wt");
	return SerializeTo(data, f);
}

bool Metadata::SerializeFrom(const void* data, const char* file) const
{
	File f(file, "rt");
	return SerializeFrom(data, f);
}
