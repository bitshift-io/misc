#include "PrecompiledHeader.h"
#include "MetadataBasicType.h"

MetadataBasicType::MetadataBasicType(const char* name, int size, const char* format) : Metadata(name),
	mSize(size),
	mFormat(format)
{

}

bool MetadataBasicType::SerializeTo(File& file) const
{
	if (!file.Valid())
		return false;

	file.WriteString("BASICTYPE %s\n", mName);
	file.WriteString("\tSIZE %i\n", mSize);
	file.WriteString("END\n");
	return true;
}

bool MetadataBasicType::SerializeTo(const void* data, File& file) const
{
	char buffer[256];
	sprintf(buffer, mFormat, *(const int*)data);
	file.WriteString("%s", buffer);
	return true;
}

bool MetadataBasicType::SerializeFrom(File& file) const
{
	return true;
}

bool MetadataBasicType::SerializeFrom(const void* data, File& file) const
{
	return true;
}


