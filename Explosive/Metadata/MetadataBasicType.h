#ifndef _METADATA_BASICTYPES_
#define _METADATA_BASICTYPES_

using namespace Siphon;

class MetadataBasicType : public Metadata
{
	friend class MetadataFactory;
public:

	//
	// Last arg is for formating with sprintf and sscanf
	//
	MetadataBasicType(const char* name, int size, const char* format);

	virtual bool SerializeTo(File& file) const;
	virtual bool SerializeTo(const void* data, File& file) const;

	virtual bool SerializeFrom(File& file) const;
	virtual bool SerializeFrom(const void* data, File& file) const;

protected:
	int			mSize;
	const char*	mFormat;
};

#endif
