#include "PrecompiledHeader.h"
#include "MetadataClass.h"

MetadataClass::MetadataClass(const char* name, TypeInfo* vars, const char* custom) : Metadata(name),
	mCount(0),
	mVariables(vars),
	mCustom(custom)
{
	while (mVariables[mCount].name != 0)
		++mCount;
}

bool MetadataClass::SerializeTo(File& file) const
{
	if (!file.Valid())
		return false;

	file.WriteString("CLASS %s\n", mName);
	file.WriteString("\tCUSTOM %s\n", mCustom);

	for (int i = 0; i < mCount; ++i)
	{
		TypeInfo& typeInfo = mVariables[i];
		Metadata* md = gMetadataFactory.GetMetadata(typeInfo.type);
		file.WriteString("\tVARIABLE\t%s\t%i\t%s\t%s\n", md ? md->GetName() : "[unknown]", typeInfo.offset, typeInfo.name, typeInfo.custom);
	}

	file.WriteString("END\n");
	return true;
}

bool MetadataClass::SerializeTo(const void* data, File& file) const
{
	if (!file.Valid())
		return false;

	file.WriteString("CLASS %s\n", mName);

	bool ret = true;
	for (int i = 0; i < mCount; ++i)
	{
		TypeInfo& typeInfo = mVariables[i];
		Metadata* md = gMetadataFactory.GetMetadata(typeInfo.type);

		// we failed, but in a recoverable way
		if (!md)
		{
			ret = false;
			continue;
		}
	
		file.WriteString("\t%s = ", typeInfo.name);
		md->SerializeTo(((const char*)data + typeInfo.offset), file);
		file.WriteString("\n");
	}

	file.WriteString("END\n");
	return true;
}

bool MetadataClass::SerializeFrom(File& file) const
{
	return true;
}

bool MetadataClass::SerializeFrom(const void* data, File& file) const
{
	return true;
}
