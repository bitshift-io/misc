#ifndef _METADATAENUM_H_
#define _METADATAENUM_H_

#include "../Util/File.h"
#include "Metadata.h"

using namespace Siphon;

class MetadataEnum : public Metadata
{
	friend class MetadataFactory;
public:

	struct TypeInfo
	{
		int					value;
		const char* 		name;
	};

	//
	// flags indicates if it can be OR'ed
	//
	MetadataEnum(const char* name, TypeInfo* vars, bool flags, int size);

	//
	// conversions
	//
	const char* ToString(int value) const;
	int ToInt(const char* name) const;

	virtual bool SerializeTo(File& file) const;
	virtual bool SerializeTo(const void* data, File& file) const;

	virtual bool SerializeFrom(File& file) const;
	virtual bool SerializeFrom(const void* data, File& file) const;

protected:
	TypeInfo*	mVariables;
	int			mCount;
	int			mSize;
	bool		mFlags;
};

#endif