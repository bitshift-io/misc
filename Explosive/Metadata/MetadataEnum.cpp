#include "PrecompiledHeader.h"
#include "MetadataEnum.h"

MetadataEnum::MetadataEnum(const char* name, TypeInfo* vars, bool flags, int size) : Metadata(name),
	mCount(0),
	mVariables(vars),
	mFlags(flags),
	mSize(size)
{
	while (mVariables[mCount].name != 0)
		++mCount;
}

const char* MetadataEnum::ToString(int value) const
{
	if (!mFlags)
		return mVariables[value].name;

	// TODO - flags?
	return 0;
}

int MetadataEnum::ToInt(const char* name) const
{
	if (!mFlags)
	{
		for (int i = 0; i < mCount; ++i)
		{
			if (strcmp(name, ToString(i)) == 0)
				return mVariables[i].value;
		}
	}

	// TODO - flags?
	return -1;
}

bool MetadataEnum::SerializeTo(File& file) const
{
	if (!file.Valid())
		return false;

	file.WriteString("ENUM %s\n", mName);

	for (int i = 0; i < mCount; ++i)
	{
		TypeInfo& typeInfo = mVariables[i];
		file.WriteString("\tVARIABLE\t%i\t%s\n", typeInfo.value, typeInfo.name);
	}

	file.WriteString("END\n");
	return true;
}

bool MetadataEnum::SerializeTo(const void* data, File& file) const
{
	file.WriteString("%s", ToString(*(const int*)data));
	return true;
}

bool MetadataEnum::SerializeFrom(File& file) const
{
	return true;
}

bool MetadataEnum::SerializeFrom(const void* data, File& file) const
{
	return true;
}
