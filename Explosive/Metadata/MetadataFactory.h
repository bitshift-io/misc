#ifndef _METADATAFACTORY_H_
#define _METADATAFACTORY_H_

#define gMetadataFactory MetadataFactory::GetInstance()

class Metadata;

class Destructor
{
public:
	typedef void (*FnDestructor)();

	~Destructor()
	{
		mPtr();
	}

	void SetFnPtr(FnDestructor ptr)
	{
		mPtr = ptr;
	}

protected:
	FnDestructor mPtr;
};

class MetadataFactory
{
public:

	MetadataFactory();
	~MetadataFactory();

	void Register(Metadata* metadata);
	Metadata* GetMetadata(const char* name);

	//
	// Serialize to file
	//
	bool SerializeTo(const char* file);
	bool SerializeTo(File& file);

	//
	// Serialize from file
	//
	bool SerializeFrom(const char* file);
	bool SerializeFrom(File& file);

	static MetadataFactory& GetInstance();
	static void Destory();

protected:

	Metadata*	mMetadataList; // linked list of metadata
	static Destructor mDestructor;
	static MetadataFactory* mInstance;
};

#endif