#ifndef _JETSKI_
#define _JETSKI_

#include "boatbody.h"

class Siphon::Mesh;
class Siphon::Camera;
class Siphon::Font;

using namespace Siphon;

class JetSki : public BoatBody
{
public:
	JetSki();

	virtual void	Update();
	virtual void	UpdatePhysics();
	virtual void	Debug(Font* font);

	void			GetBasePath(char buffer[256]);
	void			LoadMesh(Renderer& renderer);
	Mesh*			GetMesh();

	void			SetCamera(Camera* camera);
protected:
	Vector3 mThrustPosition;
	Vector3 mThrustDirection;
	float	mThrustAngle;
	float	mThrottle;

	Mesh*	mMesh;
	Camera* mCamera;
	bool	mRollUp; // should we roll the boat up?
};

#endif