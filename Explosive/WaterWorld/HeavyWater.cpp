#include "../Core/GameBase.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"
#include "../Engine/Font.h"
#include "../Engine/Camera.h"
#include "../Engine/Win32Input.h"
#include "../Engine/Renderer.h"
#include "../Engine/Win32Time.h"
#include "../Engine/Material.h"
#include "../Engine/Win32Window.h"

#include "../Util/Log.h"
#include "../Util/Macros.h"

#include "rigidbody.h"
#include "boatbody.h"
#include "jetski.h"
#include "water.h"

using namespace Siphon;

class ClipPlaneCallback : public Mesh::Callback
{
public:
	virtual void PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format)
	{
		// set clip plane
		if (material == mat)
		{
			mat->GetEffect()->SetVector4(param, Vector4(plane[0], plane[1], plane[2], plane[3]));
			mat->GetEffect()->SetInt(fogParam, useFog ? 1 : 0);
		}
	}

	void SetWaterReflectPlane(float cameraY)
	{
		useFog = false;
		plane.SetDistanceFromOrigin(-0.8f); // this offset is so we dont see thru the models that intersect the water

		if (cameraY > 0.0f)
			plane.SetNormal(Vector3(0, 1, 0));
		else
 			plane.SetNormal(Vector3(0, -1, 0));
	}

	void SetWaterRefractPlane(float cameraY)
	{
		useFog = true;
		plane.SetDistanceFromOrigin(-0.8f); // this offset is so we dont see thru the models that intersect the water

		if (cameraY > 0.0f)
		{
			plane.SetNormal(Vector3(0, -1, 0));
		}
		else
		{
 			plane.SetNormal(Vector3(0, 1, 0));
			useFog = false;
		}
	}

	Material*	mat;
	CGparameter param;
	Plane		plane;

	bool		useFog;
	CGparameter fogParam;

};

class HeavyWater : public GameBase<HeavyWater>
{
public:
	bool Init(int argc, char **argv)
	{
		GameBase::Init(argc, argv);

		TextureMgr::SetBasePath("data/textures/");
		MaterialMgr::SetBasePath("data/materials/");
		EffectMgr::SetBasePath("data/fx/");
		Mesh::SetBasePath("data/meshes/");

		Device::GetInstance().CreateDevice();
		Device::GetInstance().SetClearColour(0.027, 0.108, 0.143);

		mWindow = new Window();
		mWindow->Create("Heavy Water", 512, 512, 16, false);
		mWindow->ShowWindow();
		Device::GetInstance().SetActiveWindow(mWindow);

		Input::GetInstance().CreateInput(mWindow);

		Water::Create();
		Water::GetInstance().Load();
		
		mCamera = new Camera();
		mCamera->SetLookAt( Vector3(0, 0.001, 1.5), Vector3(0, 0, 0) );
		mCamera->SetSpeed(1.0f);
		mCamera->SetPerspective(70, 1.33, 1000.0f, 0.01f);
		mRenderMgr.SetCamera(mCamera);

		// reflections
		mReflectionCamera = new Camera();
		mReflectionCamera->SetFreeCam(false);
		mReflectionCamera->SetPerspective(70, 2.0, 1000.0f, 0.01f);
		mReflectionRenderer.SetCamera(mReflectionCamera);
		
		mReflectionTexture.Create(1024, 1024, RenderTexture::TF_Colour | RenderTexture::TF_Depth);
		mReflectionRenderer.SetRenderTarget(&mReflectionTexture);

		// reflection projector
		mReflectionProjector.SetCamera(mReflectionCamera);
		mReflectionProjector.SetTexture(mReflectionTexture.GetColourTexture());
		mRenderMgr.InsertProjector(&mReflectionProjector);

		// refractions
		mRefractionCamera = new Camera();
		mRefractionCamera->SetFreeCam(false);
		mRefractionCamera->SetPerspective(70, 2.0, 1000.0f, 0.01f);
		mRefractionRenderer.SetCamera(mRefractionCamera);

		mRefractionTexture.Create(1024, 1024, RenderTexture::TF_Colour | RenderTexture::TF_Depth);
		mRefractionTexture.SetClearColour(0.027, 0.108, 0.143);
		mRefractionRenderer.SetRenderTarget(&mRefractionTexture);

		// refraction projector
		mRefractionProjector.SetCamera(mRefractionCamera);
		mRefractionProjector.SetTexture(mRefractionTexture.GetColourTexture());
		mRenderMgr.InsertProjector(&mRefractionProjector);

		mSkydome = Mesh::LoadMesh("skydome.MDL");
		mRenderMgr.InsertMesh(mSkydome);

		mReflectionRenderer.InsertMesh(mSkydome);
		mRefractionRenderer.InsertMesh(mSkydome);
		mSkydomeOn = true;

		mGodRays = Mesh::LoadMesh("godrays.MDL");

		mWater = Mesh::LoadMesh("water.MDL");
		mRenderMgr.InsertMesh(mWater);

		mLand = Mesh::LoadMesh("land.MDL");
		mRenderMgr.InsertMesh(mLand);

		mLandReflectClip = Mesh::LoadMesh("land.MDL");

		// replace material
		for (int i = 0; i < mLandReflectClip->GetMeshMat().material.GetSize(); ++i)
		{
			Material* mat = mLandReflectClip->GetMeshMat().material[i].material;
			if (strcmpi(mat->GetName(), "data/materials/land.mat") == 0)
				mLandReflectClip->GetMeshMat().material[i].material = MaterialMgr::Load("land_clip.mat");
		}

		mReflectionRenderer.InsertMesh(mLandReflectClip);

		mLandRefractClip = Mesh::LoadMesh("land.MDL");

		// replace material
		for (int i = 0; i < mLandRefractClip->GetMeshMat().material.GetSize(); ++i)
		{
			Material* mat = mLandRefractClip->GetMeshMat().material[i].material;
			if (strcmpi(mat->GetName(), "data/materials/land.mat") == 0)
				mLandRefractClip->GetMeshMat().material[i].material = MaterialMgr::Load("land_clip.mat");
		}

		mRefractionRenderer.InsertMesh(mLandRefractClip);

		//mJetSki.LoadMesh(mRenderMgr);
		//mJetSki.SetCamera(mCamera);

		// set up clip planes
		mLandReflectClip->SetCallback(&mReflectClipPlaneCallback);
		for (int i = 0; i < mLand->GetMeshMat().material.GetSize(); ++i)
		{
			Material* mat = mLandReflectClip->GetMeshMat().material[i].material;

			mReflectClipPlaneCallback.mat = mat;
			mReflectClipPlaneCallback.param = mat->GetEffect()->GetParameterByName("clipPlane");
			mReflectClipPlaneCallback.fogParam = mat->GetEffect()->GetParameterByName("useFog");

			Plane p;
			p.SetNormal(Vector3(0, 1, 0));
			p.SetDistanceFromOrigin(0.0f);
			mReflectClipPlaneCallback.plane = p;
		}

		// set up clip planes
		mLandRefractClip->SetCallback(&mRefractClipPlaneCallback);
		for (int i = 0; i < mLand->GetMeshMat().material.GetSize(); ++i)
		{
			Material* mat = mLandReflectClip->GetMeshMat().material[i].material;

			mRefractClipPlaneCallback.mat = mat;
			mRefractClipPlaneCallback.param = mat->GetEffect()->GetParameterByName("clipPlane");
			mRefractClipPlaneCallback.fogParam = mat->GetEffect()->GetParameterByName("useFog");

			Plane p;
			p.SetNormal(Vector3(0, 1, 0));
			p.SetDistanceFromOrigin(0.0f);
			mRefractClipPlaneCallback.plane = p;
		}

		// assign render target texture to materials
		for (int i = 0; i < mWater->GetMeshMat().material.GetSize(); ++i)
		{
			Material* mat = mWater->GetMeshMat().material[i].material;

			// reflection
			Material::Param* param = mat->GetParameterByName("reflectionTexture");
			if (!param || param->type != Material::Param::ParamTexture)
				continue;

			Material::TextureParam* texParam = (Material::TextureParam*)param;

			if (!texParam->texture)
				texParam->texture = mReflectionTexture.GetColourTexture();

			// refraction
			param = mat->GetParameterByName("refractionTexture");
			if (!param || param->type != Material::Param::ParamTexture)
				continue;

			texParam = (Material::TextureParam*)param;

			if (!texParam->texture)
				texParam->texture = mRefractionTexture.GetColourTexture();
		}

		// set up clip planes
		Plane p;
		p.SetNormal(Vector3(0, 1, 0));
		p.SetDistanceFromOrigin(0.0f);

		mReflectionRenderer.SetClipPlane(p);

		mFont = FontMgr::Load("arial.fnt");
		Device::GetInstance().CheckErrors();
		Log::Print("Loading complete.\n");
		return true;
	}

	virtual void Shutdown()
	{
		Water::Destroy();

		SAFE_DELETE(mFont);
		SAFE_DELETE(mCamera);
		SAFE_DELETE(mWindow);
	}

	virtual bool UpdateGame()
	{
		Input::GetInstance().Update();	

		if (mWindow->ShouldExit() || mWindow->HandleMessages())
			return false;

		if (gInput.KeyPressed(Input::KEY_F1))
			gDevice.SaveScreenShot();

		mRenderMgr.SetTime(Time::GetInstance().GetTimeSeconds());

		//mJetSki.Update();
		
				/*
		mCamera->SetFreeCam(false);
		mCamera->SetLookAt(Vector3(-0.8, 0.5, 0.0), Vector3(0.54, 0.83, -0.02), Vector3(0.04, 0.0, 0.99));
		*/mCamera->Update();

		// remove or add skybox according to if we are under water or not
		if (mSkydomeOn && mCamera->GetPosition()[Y] < 0.0f)
		{
			mRenderMgr.RemoveMesh(mSkydome);
			mRenderMgr.InsertMesh(mGodRays);
			mSkydomeOn = false;

			CGparameter param = mLand->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("useFog");
			mLand->GetMeshMat().material[0].material->GetEffect()->SetInt(param, true);

			param = mWater->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("useFog");
			mWater->GetMeshMat().material[0].material->GetEffect()->SetInt(param, true);
		}
		else if (!mSkydomeOn && mCamera->GetPosition()[Y] >= 0.0f)
		{
			mRenderMgr.InsertMesh(mSkydome);
			mRenderMgr.RemoveMesh(mGodRays);
			mSkydomeOn = true;

			CGparameter param = mLand->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("useFog");
			mLand->GetMeshMat().material[0].material->GetEffect()->SetInt(param, false);

			param = mWater->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("useFog");
			mWater->GetMeshMat().material[0].material->GetEffect()->SetInt(param, false);
		}


		Plane p;
		p.SetNormal(Vector3(0, 1, 0));
		p.SetDistanceFromOrigin(0.0f);

		Matrix4 camWorld = p.Mirror(mCamera->GetWorld());
		mReflectionCamera->SetLookAt(camWorld);
		mReflectionCamera->Update();

		// will the refraction camera be responsible for setting the clip plane?
		// if not we can simply use the normal camera in the refraction renderer
		//mCamera->GetWorld()); //
		mCamera->LookAt();
		mRefractionCamera->SetLookAt(mCamera->GetWorld());
		//mRefractionCamera->SetLookAt(p.Refract(mCamera->GetWorld(), 0.77f));
		mRefractionCamera->Update();

		mReflectClipPlaneCallback.SetWaterReflectPlane(mCamera->GetPosition()[Y]);
		mRefractClipPlaneCallback.SetWaterRefractPlane(mCamera->GetPosition()[Y]);

		if (Input::GetInstance().KeyPressed(DIK_ESCAPE))
			return false;

		return true;
	}

	virtual void UpdatePhysics()
	{
	//	mJetSki.UpdatePhysics();
	}

	void DebugDraw()
	{
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_ALWAYS); 
		//glDepthFunc(GL_LEQUAL); 

		mCamera->LookAt();
		Water::GetInstance().Debug();

		mCamera->LookAt();

		Matrix4 mat( Vector3(0.04, 0.0, 0.99),
			 Vector3(0.54, 0.83, -0.02),
			Vector3(-0.8, 0.5, 0.0),
			Vector3(0,0,0));
		//mat.Draw();

		mat.LoadIdentity();
		mat.Draw();

		mJetSki.Debug(mFont);

		mCamera->LookAt();
		mReflectionTexture.GetColourTexture()->Draw();
		mRefractionTexture.GetColourTexture()->Draw(0, 200);
	}

	virtual void Render()
	{		
		mFont->Update();
		Device::GetInstance().BeginRender();

		mRefractionRenderer.Render();
		mReflectionRenderer.Render();		
		mRenderMgr.Render();		
		DebugDraw();
		
		unsigned int fps = Device::GetInstance().GetFPS();
		mFont->Print(0.1f, 0.1f, 0.5f, "FPS: %i", fps);
		mFont->Render();

		Device::GetInstance().EndRender();
	}

protected:
	Window*			mWindow;
	Font*			mFont;
	Camera*			mCamera;
	Mesh*			mWater;
	Mesh*			mSkydome;
	Mesh*			mLand;
	Mesh*			mLandReflectClip;
	Mesh*			mLandRefractClip;
	Mesh*			mGodRays;
	Renderer		mRenderMgr;

	Renderer		mReflectionRenderer;
	Camera*			mReflectionCamera;
	RenderTexture	mReflectionTexture;
	Projector		mReflectionProjector;

	Renderer		mRefractionRenderer;
	Camera*			mRefractionCamera;
	RenderTexture	mRefractionTexture;
	Projector		mRefractionProjector;

	RigidBody		mRigidBody;
	JetSki			mJetSki;

	ClipPlaneCallback mReflectClipPlaneCallback;
	ClipPlaneCallback mRefractClipPlaneCallback;

	bool			mSkydomeOn;
};

MAIN(HeavyWater)
