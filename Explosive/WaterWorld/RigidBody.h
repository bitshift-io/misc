#ifndef _RIGIDBODY_
#define _RIGIDBODY_

#include "../Engine/Math3D.h"

using namespace Siphon;

class RigidBody
{
public:
	RigidBody();

	void			SetBodySize(const Vector3& vMin, const Vector3& vMax);
	void			GetBodySize(Vector3& vMin, Vector3& vMax);

	float			GetMass();
	void			SetMass(float mass);

	float			GetLinearDampening();
	void			SetLinearDampening(float dampening);

	float			GetAngularDampening();
	void			SetAngularDampening(float dampening);

	// these force and position are in world space
	void			ApplyAngularForce(const Vector3& force, const Vector3& position);
	void			ApplyLinearForce(const Vector3& force);

	// force and position are in local space
	void			ApplyAngularForceLocal(const Vector3& force, const Vector3& position);
	void			ApplyLinearForceLocal(const Vector3& force);

	const Matrix4&	GetTransform();
	void			SetTransform(const Matrix4& transform);
	void			SetPosition(const Vector3& position);

	const Vector3&	GetLinearVelocity();
	void			SetLinearVelocity(const Vector3& velocity);


	// in local space
	const Vector3&	GetCentreOfMass();
	void			SetCentreOfMass(const Vector3& com);

	virtual void	Debug();
	virtual void	UpdatePhysics();

protected:
	Vector3		mCentreOfMass;
	Box			mBox;
	Vector3		mLinearVelocity;
	Vector3		mAngularVelocity;
	float		mLinearDampening;
	float		mAngularDampening;
	float		mMass;
};

#endif