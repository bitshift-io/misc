#include "Water.h"
#include "../Engine/math3d.h"
#include "../Engine/device.h"
#include "../Engine/Win32Time.h"

using namespace Siphon;

Water::Water()
{
	SetWaveRange(0.0f, 8.0f); // 1 meter waves max
	SetMapSize(500.0f, 500.0f); // 2 pixels per meter
	SetAnimationLength(5000); // 5 seconds
}

void Water::SetWaveRange(float min, float max)
{
	mWaveMin = min;
	mWaveMax = max;
}

void Water::GetMapSize(float& width, float& height)
{
	width = mMapWidth;
	height = mMapHeight;
}

void Water::SetMapSize(float width, float height)
{
	mMapWidth = width;
	mMapHeight = height;
}

void Water::Load()
{
	mHeightMap = TextureMgr::LoadTex("heightmap.tga", Texture::TF_KeepCopyInMemory | Texture::TF_VertexTexture);
	Device::GetInstance().CheckErrors();
}

float Water::GetHeightAverage(float x, float y)
{
	int width = mHeightMap->GetWidth();
	int height = mHeightMap->GetHeight();
	unsigned char* data = mHeightMap->GetPixels();
	int bpp = mHeightMap->GetBPP();

	// convert the position to a percentage
	float percentX = (x + (mMapWidth / 2.0f)) / mMapWidth;
	float percentY = (y + (mMapHeight / 2.0f)) / mMapHeight;

	// calulate pixel from percentage
	int xPixel = (int)(percentX * width);
	int yPixel = (int)(percentY * height);

	if ((xPixel - 1) < 0 || (yPixel - 1) < 0)
		return 0.0f;

	if ((xPixel + 1) >= width || (yPixel + 1) >= height)
		return 0.0f;

	int averagePixel = 0;
	averagePixel += GetPixel(xPixel - 1, yPixel - 1);
	averagePixel += GetPixel(xPixel - 1, yPixel);
	averagePixel += GetPixel(xPixel - 1, yPixel + 1);

	averagePixel += GetPixel(xPixel, yPixel - 1);
	averagePixel += GetPixel(xPixel, yPixel);
	averagePixel += GetPixel(xPixel, yPixel + 1);

	averagePixel += GetPixel(xPixel + 1, yPixel - 1);
	averagePixel += GetPixel(xPixel + 1, yPixel);
	averagePixel += GetPixel(xPixel + 1, yPixel + 1);

	averagePixel /= 9;
	
	return Convert(averagePixel);
}

float Water::Convert(unsigned char pixel)
{
	return mWaveMin + ((mWaveMax - mWaveMin) * ((float)pixel / 255.0f));
}

void Water::SetAnimationLength(unsigned long int animLength)
{
	mAnimationLength = animLength;
}

unsigned char Water::GetPixel(int x, int y)
{
	unsigned long time = Time::GetInstance().GetTicks() % mAnimationLength;

	// find which 2 colours the blend between
	int bpp = mHeightMap->GetBPP();
	int timePerLayer = mAnimationLength / bpp;
	float curLayer = (float)time / (float)timePerLayer;
	int firstLayer = (int)curLayer;
	int secondLayer = (firstLayer + 1) % bpp;
	float percent = curLayer - firstLayer;

	int width = mHeightMap->GetWidth();
	unsigned char* data = mHeightMap->GetPixels();

	float fistPixel = (float)data[((x * bpp) + (y * width * bpp)) + firstLayer];
	float secondPixel = (float)data[((x * bpp) + (y * width * bpp)) + secondLayer];

	unsigned char final = (unsigned char)((fistPixel * (1.0f - percent)) + (secondPixel * percent));
	return final;
}

float Water::GetHeight(float x, float y)
{
	int width = mHeightMap->GetWidth();
	int height = mHeightMap->GetHeight();
	unsigned char* data = mHeightMap->GetPixels();
	int bpp = mHeightMap->GetBPP();

	// convert the position to a percentage
	float percentX = (x + (mMapWidth * 0.5f)) / mMapWidth;
	float percentY = (y + (mMapHeight * 0.5f)) / mMapHeight;

	// calulate pixel from percentage
	int xPixel = (int)(percentX * width);
	int yPixel = (int)(percentY * height);

	if (xPixel < 0 || yPixel < 0)
		return 0.0f;

	if (xPixel >= width || yPixel >= height)
		return 0.0f;

	// convert pixel height to a world height
	return Convert(GetPixel(xPixel, yPixel));
}

void Water::Debug()
{/*
	Box box;
	Vector3 min(-mMapWidth/2.0f, mWaveMin, -mMapHeight/2.0f);
	Vector3 max(mMapWidth/2.0f, mWaveMax, mMapHeight/2.0f);
	box.SetMin(min);
	box.SetMax(max);
	box.Draw();

	Matrix4 identity(Matrix4::Identity);
	identity.Draw();

	box.SetMin(Vector3(-0.1, -0.1, -0.1));
	box.SetMax(Vector3(0.1, 0.1, 0.1));
	Matrix4 transform(Matrix4::Identity);
	for (float w = -mMapWidth / 2.0f; w < mMapWidth / 2.0f; w += 10.0f)
	{
		for (float h = -mMapHeight / 2.0f; h < mMapHeight / 2.0f; h += 10.0f)
		{
			transform.Translate(Vector3(w, GetHeight(w, h), h));
			box.SetTransform(transform);
			box.Draw();
		}
	}*/
}
