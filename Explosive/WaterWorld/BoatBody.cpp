#include "../Engine/Win32Time.h"
#include "boatbody.h"
#include "water.h"

BuoyancyBody::BuoyancyBody() : 
	mBuoyancy(1.5f),
	mTorqueMultiplier(0.2f)
{

}

void BuoyancyBody::Debug(BoatBody* boatBody)
{
	Matrix4 transform = boatBody->GetTransform();
	Vector3 localPosition = GetTransform().GetTranslation();

	Vector3 worldPosition = transform * localPosition;

	Vector3 min, max;
	GetBodySize(min, max);

	Matrix4 boxTransform(Matrix4::Identity);
	boxTransform.Translate(worldPosition);

	Box box;
	box.SetMin(min);
	box.SetMax(max);
	box.SetTransform(boxTransform);
	box.Draw(Vector3(1.0f, 0.0f, 0.0f));

	Sphere sphere(0.005f, worldPosition);
	sphere.Draw(Vector3(1.0f, 0.0f, 0.0f));
}

float BuoyancyBody::GetBuoyancy()
{
	return mBuoyancy;
}

void BuoyancyBody::SetBuoyancy(float buoyancy)
{
	mBuoyancy = buoyancy;
}

void BuoyancyBody::SetTorqueMultiplier(float multiplier)
{
	mTorqueMultiplier = multiplier;
}

float BuoyancyBody::GetTorqueMultiplier()
{
	return mTorqueMultiplier;
}

//----------------------------------------------------

BoatBody::BoatBody() :
	mBuoyancy(1.5f),
	mWaterContactPercent(0.0f)
{

}

BoatBody::~BoatBody()
{
	for (int i = 0; i < mBuoyancyBodys.GetSize(); ++i)
		delete mBuoyancyBodys[i];
}

float BoatBody::GetHeight(float x, float y)
{
	return Water::GetInstance().GetHeight(x, y); //Average
}

void BoatBody::GetMapSize(float& width, float& hight)
{
	Water::GetInstance().GetMapSize(width, hight);		
}

void BoatBody::Debug()
{
	for (int i = 0; i < mBuoyancyBodys.GetSize(); ++i)
	{
		BuoyancyBody* body = mBuoyancyBodys[i];
		body->Debug(this);
	}

	RigidBody::Debug();
}

void BoatBody::UpdatePhysics()
{
	// apply gravity
	float updateRate = 1.0f / (float)Time::GetInstance().GetUpdateRate(Time::UPDATE_PHYSICS);
	Vector3 gravity(0, -3.0 * updateRate, 0); // 9.8ms
	Vector3 buoyancyGravity(0, -7.0 * updateRate, 0);

	Matrix4 transform = GetTransform();
	Vector3 netVelocity(0,0,0);
	mWaterContactPercent = 0.0f;

	for (int i = 0; i < mBuoyancyBodys.GetSize(); ++i)
	{
		Vector3 netForce(0,0,0);
		BuoyancyBody* body = mBuoyancyBodys[i];

		// apply gravity
		Vector3 gravityForce = gravity * body->GetMass();
		//body->ApplyForce(gravityForce);
		netForce = netForce + gravityForce;

		// apply bouyancy, if the thing will be under water
		Vector3 min, max;
		body->GetBodySize(min, max);

		Vector3 position = transform * body->GetTransform().GetTranslation();

		float waterHeight = GetHeight(position[X], position[Z]);

		// if any part of our boat is under water, there is an upward force
		// if the bottom is under water, we need to apply some upward force
		float bottomToWater = (position[Y] + min[Y]) - waterHeight;
		float contactPercent = 0.0f;
		if (bottomToWater < 0.0f)
		{
			// if the top is under water, apply the full upward force
			float topToWater = (position[Vector3::Y] + max[Vector3::Y]) - waterHeight;
			if (topToWater < 0.0f)
			{
				contactPercent = 1.0f;
				Vector3 boyancyForce(0.0f, body->GetBuoyancy() * -buoyancyGravity[Vector3::Y] * body->GetMass(), 0.0f);
				//body->ApplyForce(boyancyForce);
				netForce = netForce + boyancyForce;
			}
			else
			{
				float boyancyScale = abs(bottomToWater) / (max[Vector3::Y] - min[Vector3::Y]);
				if (boyancyScale > 1.0f || boyancyScale < 0.0f)
				{
					int noghint  = 0;
				}
				Vector3 boyancyForce(0.0f, body->GetBuoyancy() * boyancyScale * -buoyancyGravity[Vector3::Y] * body->GetMass(), 0.0f);
				//body->ApplyForce(boyancyForce);
				netForce = netForce + boyancyForce;

				contactPercent = boyancyScale;
			}
		}
		else
		{
			int thing = 0;
		}

		// netForce is the force for this frame on the buoyance object,
		// it should be used to apply a torque to the boat as well as affect its velocity
		netVelocity = netVelocity + netForce;
		ApplyAngularForce(netForce * body->GetTorqueMultiplier(), position);
		mWaterContactPercent += contactPercent;
	}

	netVelocity = netVelocity / mBuoyancyBodys.GetSize();
	mWaterContactPercent /= mBuoyancyBodys.GetSize();

	// todo: now lets apply gravity and buoyancey to our center of mass

	ApplyLinearForce(netVelocity);


	RigidBody::UpdatePhysics();

	// now force the player to be inside the world
	float width, height;
	GetMapSize(width, height);

	transform = GetTransform();
	Vector3 translation = transform.GetTranslation();
	if (translation[X] < -(width / 2.0f))
		translation[X] = -(width / 2.0f);
	else if (translation[X] > (width / 2.0f))
		translation[X] = (width / 2.0f);

	if (translation[Z] < -(width / 2.0f))
		translation[Z] = -(width / 2.0f);
	else if (translation[Z] > (width / 2.0f))
		translation[Z] = (width / 2.0f);

	transform.Translate(translation);
	SetTransform(transform);
}

float BoatBody::GetContactPercent()
{
	return mWaterContactPercent;
}
