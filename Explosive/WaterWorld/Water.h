#ifndef _WATER_
#define _WATER_

#include "singleton.h"
#include "../Engine/Texture.h"

using namespace Siphon;

class Water : public Singleton<Water>
{
public:
	Water();

	void	Load();

	float			GetHeight(float x, float y);
	float			GetHeightAverage(float x, float y);
	unsigned char	GetPixel(int x, int y);
	float			Convert(unsigned char pixel);

	void	SetAnimationLength(unsigned long int animLength);
	void	SetWaveRange(float min, float max);
	void	SetMapSize(float width, float height);
	void	GetMapSize(float& width, float& height);

	void	Debug();

protected:
	Texture*	mHeightMap;
	float		mWaveMin;
	float		mWaveMax;
	float		mMapWidth;
	float		mMapHeight;
	long		mAnimationLength;
};

#endif