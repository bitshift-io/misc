#ifndef _BOATBODY_
#define _BOATBODY_

#include "../Util/Array.h"

#include "rigidbody.h"

using namespace Siphon;

class BoatBody;

/*
 * a rigid body that doesnt have any torque
 * used to apply torque to another body
 */
class BuoyancyBody : public RigidBody
{
public:
	BuoyancyBody();

	virtual void	Debug(BoatBody* boatBody);

	float			GetBuoyancy();
	void			SetBuoyancy(float buoyancy);

	void			SetTorqueMultiplier(float multiplier);
	float			GetTorqueMultiplier();
protected:
	float			mBuoyancy;
	float			mTorqueMultiplier;
};

class BoatBody : public RigidBody
{
public:
	BoatBody();
	~BoatBody();

	virtual void	Debug();
	virtual void	UpdatePhysics();

	float			GetContactPercent();
	float			GetHeight(float x, float y);
	
	void			GetMapSize(float& width, float& hight);

protected:
	Array<BuoyancyBody*>		mBuoyancyBodys;

	float			mBuoyancy;
	float			mWaterContactPercent;
};

#endif