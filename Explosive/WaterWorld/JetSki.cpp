#include "../Engine/Win32Input.h"
#include "../Engine/mesh.h"
#include "../Engine/renderer.h"
#include "../Engine/Win32Time.h"
#include "../Engine/font.h"

#include "jetski.h"

JetSki::JetSki() : 
	mThrustPosition(0.0f, 0.0f, -0.5f),
	mThrustAngle(0.0f),
	mThrottle(0.0f),
	mCamera(0),
	mRollUp(false),
	mMesh(0)
{
	SetCentreOfMass(Vector3(0.0f, 0.5f, 0.1f));
	SetBodySize(Vector3(-0.5f, -0.5f, -1.0f),Vector3(0.5f, 0.5f, 1.0f)); 
	SetPosition(Vector3(0.0f, 6.0f, 0.0f));
	//SetMass(0.10f); 
	SetMass(0.0f); 
	SetLinearDampening(0.9f);
	SetAngularDampening(0.6f);

	// the center of mass object
	BuoyancyBody* body = new BuoyancyBody();
	body->SetBodySize(Vector3(-0.2f, -0.2f, -0.2f), Vector3(0.2f, 0.2f, 0.2f));
	//body->SetMass(0.20); // 50kg
	body->SetMass(0.0); // 50kg
	body->SetPosition(mCentreOfMass);
	//body->SetBuoyancy(1.8f);
	body->SetBuoyancy(0.0f);
	body->SetTorqueMultiplier(2.0f);
	mBuoyancyBodys.Insert(body);

	File dummyFile("data/dummies/jetski.txt", "rt");
	if (dummyFile.Valid())
	{
		while (!dummyFile.EndOfFile())
		{
			char name[256];
			float x,y,z;
			dummyFile.ReadString("%s [%f, %f, %f]\n", name, &x, &y, &z);

			BuoyancyBody* body = new BuoyancyBody();
			body->SetBodySize(Vector3(-0.1f, -0.1f, -0.1f), Vector3(0.1f, 0.1f, 0.1f));
			body->SetMass(0.0); // 50kg
			//body->SetMass(0.10); // 50kg
			body->SetPosition(Vector3(x, z, -y)); //max is backward
			//body->SetBuoyancy(1.5f);
			body->SetBuoyancy(0.0f);
			body->SetTorqueMultiplier(0.2f);
			mBuoyancyBodys.Insert(body);
		}
	}

/*
	// the frount body
	body = new BuoyancyBody();
	body->SetBodySize(Vector3(-0.1f, -1.0f, -0.1f), Vector3(0.1f, 1.0f, 0.1f));
	body->SetMass(10.0); // 50kg
	body->SetPosition(Vector3(0.0f, -0.1f, 1.2f));
	body->SetBuoyancy(2.0f);
	mBuoyancyBodys.Insert(body);

	// the back right
	body = new BuoyancyBody();
	body->SetBodySize(Vector3(-0.1f, -1.0f, -0.1f), Vector3(0.1f, 1.0f, 0.1f));
	body->SetMass(10.0); // 50kg
	body->SetPosition(Vector3(-0.7f, 0.0f, -0.9f));
	body->SetBuoyancy(2.0f);
	body->SetTorqueMultiplier(1.0f);
	mBuoyancyBodys.Insert(body);

	// the back left
	body = new BuoyancyBody();
	body->SetBodySize(Vector3(-0.1f, -1.0f, -0.1f), Vector3(0.1f, 1.0f, 0.1f));
	body->SetMass(10.0); // 50kg
	body->SetPosition(Vector3(0.7f, 0.0f, -0.9f));
	body->SetBuoyancy(2.0f);
	body->SetTorqueMultiplier(1.0f);
	mBuoyancyBodys.Insert(body);*/
/*
	// the center bottom
	body = new BuoyancyBody();
	body->SetBodySize(Vector3(-0.1f, -0.3f, -0.1f), Vector3(0.1f, 0.3f, 0.1f));
	body->SetMass(10.0); // 50kg
	body->SetPosition(Vector3(0.0f, -0.2f, 0.0f));
	body->SetBuoyancy(1.1f);
	body->SetTorqueMultiplier(1.0f);
	mBuoyancyBodys.Insert(body);*/
}

void JetSki::Update()
{/*
		// testing
	if (Input::GetInstance().KeyDown(DIK_U))
	{
		BuoyancyBody* body = mBuoyancyBodys[1];
		ApplyAngularForce(Vector3(0,0.01f,0), GetTransform() * body->GetTransform().GetTranslation());
	}

	if (Input::GetInstance().KeyDown(DIK_J))
	{
		BuoyancyBody* body = mBuoyancyBodys[1];
		ApplyAngularForce(Vector3(0,-0.01f,0), GetTransform() * body->GetTransform().GetTranslation());
	}

	if (Input::GetInstance().KeyDown(DIK_H))
	{
		BuoyancyBody* body = mBuoyancyBodys[1];
		ApplyAngularForce(Vector3(0.01f,0,0), GetTransform() * Vector3(0, 0, 1));
	}

	if (Input::GetInstance().KeyDown(DIK_K))
	{
		BuoyancyBody* body = mBuoyancyBodys[1];
		ApplyAngularForce(Vector3(-0.01f,0,0), GetTransform() * Vector3(0, 0, 1));
	}

	RigidBody::Update();
*/

	float updateRate = 1.0f / (float)Time::GetInstance().GetUpdateRate(Time::UPDATE_PHYSICS);
	
	float turnSpeed = 1.0f * updateRate;
	float throttleSpeed = 20.0f * updateRate;

	float maxTurning = 0.2f;
	float maxFowardThrottle = 2.5f * updateRate;

	// testing
	if (Input::GetInstance().KeyDown(DIK_P))
	{
		BuoyancyBody* body = mBuoyancyBodys[0];
		ApplyAngularForce(Vector3(0,0.01f,0), GetTransform() * body->GetTransform().GetTranslation());
		ApplyLinearForce(Vector3(0,0.1f,0));
	}

	if (Input::GetInstance().KeyDown(DIK_O))
	{
		//BuoyancyBody* body = mBuoyancyBodys[0];
		ApplyLinearForce(Vector3(0,20.3f,0));
	}

	// reset
	if (Input::GetInstance().KeyPressed(DIK_P))
	{
		SetPosition(Vector3(0.0f, 6.0f, 0.0f));
	}

	if (Input::GetInstance().KeyPressed(DIK_R))
	{/*
		Matrix4 transform(Matrix4::Identity);
		transform.Translate(0.0f, GetHeight(0.0f, 0.0f), 0.0f);
		SetTransform(transform);
*/
		mRollUp = true;
	}

	//if (GetContactPercent() > 0.2f)
	{
		// acceleration and turning
		bool keyDown = false;
		if (Input::GetInstance().KeyDown(DIK_LEFT))
		{
			keyDown = true;
			mThrustAngle -= turnSpeed;
		}

		if (Input::GetInstance().KeyDown(DIK_RIGHT))
		{
			keyDown = true;
			mThrustAngle += turnSpeed;
		}

		if (!keyDown)
		{
			if (mThrustAngle > turnSpeed)
				mThrustAngle -= turnSpeed;
			else if (mThrustAngle < -turnSpeed)
				mThrustAngle += turnSpeed;
			else
				mThrustAngle = 0.0f;
		}

		if (Input::GetInstance().KeyDown(DIK_UP))
			mThrottle += throttleSpeed;
		else 
			mThrottle = 0.0f;

		if (mThrustAngle < -maxTurning)
			mThrustAngle = -maxTurning;

		if (mThrustAngle > maxTurning)
			mThrustAngle = maxTurning;

		if (mThrottle > maxFowardThrottle)
			mThrottle = maxFowardThrottle;
	}

	if (mBuoyancyBodys.GetSize())
	{
		BuoyancyBody* body = mBuoyancyBodys[0];
		Vector3 position = body->GetTransform().GetTranslation();
		
		// do leaning
		float rollLean = 0.1f;
		float rollLeanMax = 0.3f;
		bool rollKeyDown = false;

		if (Input::GetInstance().KeyDown(DIK_RIGHT))
		{
			rollKeyDown = true;
			if (position[X] > (mCentreOfMass[X] - rollLeanMax))
				position[X] -= rollLean;
		}

		if (Input::GetInstance().KeyDown(DIK_LEFT))
		{
			rollKeyDown = true;
			if (position[X] < (mCentreOfMass[X] + rollLeanMax))
				position[X] += rollLean;
		}
		
		if (!rollKeyDown)
		{
			if (position[X] < (mCentreOfMass[X] - rollLean))
				position[X] += rollLean;
			else if (position[X] > (mCentreOfMass[X] + rollLean))
				position[X] -= rollLean;
			else
				position[X] = mCentreOfMass[X];
		}

		// do pitching
		float pitchLean = 0.1f;
		float pitchLeanMax = 0.5f;
		bool pitchKeyDown = false;

		if (Input::GetInstance().KeyDown(DIK_DOWN))
		{
			pitchKeyDown = true;
			if (position[Z] > (mCentreOfMass[Z] - pitchLeanMax))
				position[Z] -= pitchLean;
		}
		
		if (Input::GetInstance().KeyDown(DIK_LSHIFT))
		{
			pitchKeyDown = true;
			if (position[Z] < (mCentreOfMass[Z] + pitchLeanMax))
				position[Z] += pitchLean;
		}
		
		if (!pitchKeyDown)
		{
			if (position[Z] < (mCentreOfMass[Z] - pitchLean))
				position[Z] += pitchLean;
			else if (position[Z] > (mCentreOfMass[Z] + pitchLean))
				position[Z] -= pitchLean;
			else
				position[Z] = mCentreOfMass[Z];
		}

		body->SetPosition(position);
	}

	if (mMesh)
		mMesh->SetTransform(GetTransform());

	// set up the camera matrix
	if (mCamera)
	{
		Matrix4 transform = GetTransform();
		Vector3 position = transform.GetTranslation() - transform.GetColumn(Vector3::Z) * 3.5f + Vector3(0, 1.0f, 0);
		//position[Y] = 6.0f;
		Vector3 target = transform.GetTranslation() + transform.GetColumn(Vector3::Z) * 0.1f;
		mCamera->SetLookAt(position, target);
		mCamera->Update();
	}	
}

void JetSki::UpdatePhysics()
{
	//BuoyancyBody* body = mBuoyancyBodys[0];
	//body->SetTorqueMultiplier(0.5f);

	SetLinearDampening(0.9f);
	SetAngularDampening(0.6f);

	if (GetContactPercent() <= 0.0f)
	{
		//body->SetTorqueMultiplier(2.0f);

		SetLinearDampening(1.0f);
		//SetAngularDampening(1.0f);
	}
	else if (GetContactPercent() > 0.0f)
	{
		// if roll up enabled
		if (mRollUp)
		{
			// apply a torque untill the boat is facing up
			Matrix4 transform = GetTransform();
			Vector3 up = transform.GetColumn(Y);
			Vector3 worldUp(0.0f, 1.0f, 0.0f);

			float dot = up.Dot(worldUp);
			if (dot > 0.2)
			{
				mRollUp = false;
			}
			else
			{
				ApplyAngularForce(worldUp, transform * Vector3(1.0f, 0.0, 0.0));
			}
		}

		if (mThrottle)
		{
			Matrix4 thrust(Matrix4::Identity);
			thrust.RotateY(mThrustAngle);

			Vector3 force = -thrust.GetColumn(Vector3::Z) * mThrottle /** GetContactPercent()*/;
			ApplyAngularForceLocal(force * 10.0f, mThrustPosition);
		
			Matrix4 transform = GetTransform();
			transform.Translate(Vector3(0.0f, 0.0f, 0.0f));

			Vector3 fowardForce = transform * -force;

			// extra downward or upward force FMNOTE: testing
			//fowardForce[Y] *= 25.0f;

			ApplyLinearForce(fowardForce);
		}
	}

	BoatBody::UpdatePhysics();
}

void JetSki::Debug(Font* font)
{
	Matrix4 thrust(Matrix4::Identity);
	thrust.RotateY(mThrustAngle);
	Vector3 force = -thrust.GetColumn(Vector3::Z) * mThrottle;

	Matrix4 rotation = GetTransform();
	rotation.Translate(Vector3(0.0f, 0.0f, 0.0f));
	Vector3 startPoint = GetTransform() * mThrustPosition;
	Vector3 endPoint = startPoint + (rotation * (force * Time::GetInstance().GetUpdateRate(Time::UPDATE_PHYSICS)));
	endPoint.Draw(startPoint);

	float velocity = GetLinearVelocity().Magnitude();
	Vector3 pos = GetTransform().GetTranslation();
	font->Print(1.0f, 500.0f, 0.5f, "Position: %.2f %.2f %.2f\nPcnt Contact: %.2f\nVelocity: %.2f", 
		pos[X], pos[Y], pos[Z], mWaterContactPercent, velocity * Time::GetInstance().GetUpdateRate(Time::UPDATE_PHYSICS));

	BoatBody::Debug();
}

void JetSki::GetBasePath(char buffer[256])
{
	strcpy(buffer, "data/jetski/");
}

void JetSki::LoadMesh(Renderer& renderer)
{
	mMesh = Mesh::LoadMesh("jetski.MDL");
	if (mMesh)
		renderer.InsertMesh(mMesh);
}

Mesh* JetSki::GetMesh()
{
	return mMesh;
}

void JetSki::SetCamera(Camera* camera)
{
	mCamera = camera;
	mCamera->SetFreeCam(false);
}