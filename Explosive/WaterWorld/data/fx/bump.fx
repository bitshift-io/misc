//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4 	worldIT 		: WorldInverseTranspose;
float4x4 	worldViewProj 	: WorldViewProjection;
float4x4 	worldView 		: WorldView;
float4x4 	worldInv 		: WorldInverse;

float3   	lightPos 		: LightPosition;
float3   	eyePos 			: EyePosition;

sampler2D bumpTexture = sampler_state
{
	minFilter = LinearMipMapLinear;
	magFilter = Linear;
};

sampler2D diffuseTexture = sampler_state
{
	minFilter = LinearMipMapLinear;
	magFilter = Linear;
};

sampler2D specularTexture = sampler_state
{
	minFilter = LinearMipMapLinear;
	magFilter = Linear;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
    float3 position 	: POSITION;
	float3 normal 		: NORMAL;
	float3 tangent 		: TEXCOORD0;
    float3 binormal 	: TEXCOORD1;
	float2 uvCoord		: TEXCOORD2;	
};

struct VS_OUTPUT
{
	float4 position : POSITION;
	float2 uvCoord	: TEXCOORD0;	
	float3 lightVec : TEXCOORD1;
	float3 eyeVec 	: TEXCOORD2;
};

struct PS_OUTPUT
{
	float4 colour	: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform float4x4 worldView,
			uniform float4x4 worldInv,
			uniform float3   lightPos,
			uniform float3   eyePos )
{
    VS_OUTPUT OUT;
   	OUT.position = mul( modelViewProjection, float4(IN.position, 1.0) );
	OUT.uvCoord = IN.uvCoord;

	// convert from object space to texture(tangent) space...
	float3x3 texSpaceTrans = float3x3(
					IN.tangent,					
					IN.binormal,
					IN.normal);
	
	// we could optimise out this matrix multiply if we upload light in object space				
	float3 lightModelPos = mul( worldInv, float4(lightPos, 1.0) ).xyz;
	lightModelPos = lightModelPos - IN.position;
	OUT.lightVec = mul( texSpaceTrans, lightModelPos );
	
	// we could optimise out this matrix multiply if we upload eye in object space
	float3 eyeModelPos = mul( worldInv, float4(eyePos, 1.0) ).xyz;
	eyeModelPos = eyeModelPos - IN.position;
	OUT.eyeVec = mul( texSpaceTrans, eyeModelPos );
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN, uniform sampler2D bump, uniform sampler2D diffuse, uniform sampler2D specular)
{
	PS_OUTPUT OUT;

	float3 eyeVec = normalize( IN.eyeVec );
	
	// normals in bump map are in texture(tangent) space....
	float3 bumpTexNormal = tex2D( bump, IN.uvCoord);
	bumpTexNormal.x = 1.0 - bumpTexNormal.x; // invert r to get Nvidia style normal mapping
	bumpTexNormal = (2 * bumpTexNormal) - 1.0;

	float3 lightVec = normalize( IN.lightVec );

	float angleLightWNormal = max(dot(bumpTexNormal, lightVec), 0.0);

	float ambient = 0.5f;
	float4 diff = (angleLightWNormal + ambient) * tex2D(diffuse, IN.uvCoord);
	float4 spec = 0;
	
	if (angleLightWNormal >= 0.0f)
	{
		float4 specTex = tex2D(specular, IN.uvCoord);

		// the specular colour should be uplaoded by the light/engine
		spec = float4(1,1,1,0) * specTex.r * pow( max( dot(reflect(-eyeVec, bumpTexNormal), lightVec), 0.0), 255 * specTex.a);
	}

	OUT.colour = diff + spec;

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		DepthTestEnable = true;
		DepthMask = true;
		FrontFace = CCW;
		CullFace = front;
		DepthFunc = lequal;
		
		VertexProgram = compile arbvp1 myvs(worldViewProj, worldView, worldInv, lightPos, eyePos);
        	FragmentProgram = compile arbfp1 myps(bumpTexture, diffuseTexture, specularTexture);
    }
}
