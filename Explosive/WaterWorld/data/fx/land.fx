//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4 	worldViewProj 	: WorldViewProjection;
int			useFog = false;

sampler2D diffuseTexture = sampler_state 
{
	generateMipMap = true;
	minFilter = LinearMipMapLinear;
	magFilter = Linear;
	WrapS = ClampToEdge;
	WrapT = ClampToEdge;
	MaxAnisotropy = 8;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
    float3 position 	: POSITION;
	float2 uvCoord		: TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 position		: POSITION;
	float2 uvCoord		: TEXCOORD0;
	float  depth		: TEXCOORD1;
};

struct PS_OUTPUT
{
	float4 colour	: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs(const VS_INPUT IN)
{
	VS_OUTPUT OUT;

	float4 pos = mul( worldViewProj, float4(IN.position, 1.0) );
	OUT.position = pos;
	OUT.uvCoord = IN.uvCoord;
	
	if (useFog)
		OUT.depth = clamp(pos.z * pos.w * 0.0001, 0.0, 1.0);
	else
		OUT.depth = 0.0;
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps(VS_OUTPUT IN, uniform sampler2D diffuse)
{
	PS_OUTPUT OUT;

	float4 fog = IN.depth * float4(0.027, 0.108, 0.143, 0.0);
	OUT.colour = (1.0f - IN.depth) * tex2D(diffuse, IN.uvCoord) + fog;

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		DepthTestEnable = true;
		DepthMask = true;
		FrontFace = CCW;
		CullFace = front;
		DepthFunc = lequal;
		
		VertexProgram = compile arbvp1 myvs();
		FragmentProgram = compile arbfp1 myps(diffuseTexture);
	}
}
