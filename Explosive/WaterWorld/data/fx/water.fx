//-----------------------------------------------------------------------------
// 
//	water.fx
//
//	Uses height maps to simulate an ocean
//
//	SM3.0
//
//	(C) Fabian Mathews 2005
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Shader Parameters
//-----------------------------------------------------------------------------

float4x4	worldViewProj			: WorldViewProjection;
float4x4	world					: World;
float4x4	reflectionTextureProj	: ProjectorWorldViewProjectionTexture;
float4x4	refractionTextureProj	: ProjectorWorldViewProjectionTexture;

float		time					: Time;
float3		cameraPos				: EyePosition;
int			useFog = false;

float4		sunVec = float4(-1.33, -2.12, 0.83, 1.0); // fix vector on the sky box
/*
sampler2D reflectionTexture = sampler_state
{
        minFilter = Linear;
        magFilter = Linear;
};

sampler2D refractionTexture = sampler_state
{
        minFilter = Linear;
        magFilter = Linear;
};
*/

sampler2D dudvTexture = sampler_state
{
        minFilter = LinearMipMapLinear;
        magFilter = Linear;
};

sampler2D heightTexture = sampler_state
{
        minFilter = Nearest;
        magFilter = Nearest;
};



//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

struct VS_INPUT
{
	float3 position 		: POSITION;
	float2 texCoord			: TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 position 			: POSITION;
	float2 texCoord				: TEXCOORD0;
	float4 reflectProjCoord		: TEXCOORD1;
	float4 refractProjCoord		: TEXCOORD2;
	float  depth				: TEXCOORD3;
	float  tranparency			: TEXCOORD4;
	float  height				: TEXCOORD5;
};

VS_OUTPUT myvs(const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform sampler2D heightTex,
			uniform float4x4 reflectTexProj,
			uniform float4x4 refractTexProj)
{
    VS_OUTPUT OUT;
    
    float waterHeight = tex2Dlod(heightTex, float4(IN.texCoord, 0, 0) * 256).r;
    float3 position = IN.position;
    position.y += waterHeight * 5000.0f;
    OUT.height = waterHeight * 100.0f;
  
    float4 pos = mul( worldViewProj, float4(position, 1.0) );
	OUT.position = pos;
    
	OUT.reflectProjCoord = mul(reflectTexProj, float4(IN.position, 1.0));
	OUT.refractProjCoord = mul(refractTexProj, float4(IN.position, 1.0));
	
	if (useFog)
		OUT.depth = clamp(pos.z * pos.w * 0.0001, 0.0, 1.0);
	else
		OUT.depth = 0.0;
		
	// determine how see thruough the water is
	// calulate angle from camera to vertex
	float3 worldNormal = float3(0, 1, 0);
	float3 worldPos = mul( world, float4(IN.position, 1.0) ).xyz;
	float3 cameraDir = normalize(cameraPos - worldPos);
	OUT.tranparency = abs(min(1.0, dot(cameraDir, worldNormal) * 2.0));
	
    OUT.texCoord = IN.texCoord * 100.0f + time * 0.1;;
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

struct PS_OUTPUT
{
	float4 color		: COLOR;
};

PS_OUTPUT myps(const VS_OUTPUT IN, /*uniform sampler2D reflectTex, uniform sampler2D refractTex,*/
			uniform sampler2D dudvTex)
{
	PS_OUTPUT OUT;
	
	const float kDistortion = 0.015;
	const float kRefraction = 1.0;
/*	
	float4 dudvColor = tex2D(dudvTex, IN.texCoord) * kRefraction;
/*	float4 texColour = tex2Dproj(reflectTex, IN.reflectProjCoord + dudvColor) * (1.0f - IN.tranparency) 
			+ tex2Dproj(refractTex, IN.refractProjCoord + dudvColor) * IN.tranparency;
			
	float4 fog = IN.depth * float4(0.027, 0.108, 0.143, 0.0);
	OUT.color = (1.0f - IN.depth) * texColour + fog;
*/
	OUT.color = float4(IN.height,IN.height,IN.height,0);		
	OUT.color.a = 1.0;
	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		DepthTestEnable = true;
		DepthMask = true;
		FrontFace = CCW;
		CullFace = front;
		DepthFunc = lequal;

		BlendEnable = true;
		BlendEquation = FuncAdd;
		BlendFunc = int2(SrcAlpha, OneMinusSrcAlpha);
		
		VertexProgram = compile vp40 myvs(worldViewProj, heightTexture, reflectionTextureProj, refractionTextureProj);
       	FragmentProgram  = compile fp40 myps(/*reflectionTexture, refractionTexture,*/ dudvTexture);
    }
}
