#include "../Engine/Win32Time.h"

#include "rigidbody.h"

RigidBody::RigidBody() :
	mLinearDampening(0.9f),
	mAngularDampening(0.9f),
	mLinearVelocity(0.0f, 0.0f, 0.0f),
	mAngularVelocity(0.0f, 0.0f, 0.0f),
	mCentreOfMass(0.0f, 0.0f, 0.0f),
	mMass(1.0f)
{
	SetBodySize(Vector3(0.0f, 0.0f, 0.0f), Vector3(0.0f, 0.0f, 0.0f));
}

void RigidBody::SetBodySize(const Vector3& vMin, const Vector3& vMax)
{
	mBox.SetMin(vMin);
	mBox.SetMax(vMax);
}

void RigidBody::GetBodySize(Vector3& vMin, Vector3& vMax)
{
	vMin = mBox.GetMin();
	vMax = mBox.GetMax();
}

float RigidBody::GetMass()
{
	return mMass;
}

void RigidBody::SetMass(float mass)
{
	mMass = mass;
}

void RigidBody::ApplyAngularForce(const Vector3& torque, const Vector3& position)
{
	// Torque = direction vector cross product of the force
	// convert distance and force to local space
	Vector3 distance = GetTransform().GetTranslation() - position;
	//float distance = (position - GetTransform().GetTranslation()).Magnitude();

	//Vector3 direction = GetTransform().GetColumn(Vector3::Z) * distance;

	//Matrix4 rotation = GetTransform();
	//rotation.Translate(Vector3(0.0f,0.0f,0.0f));
	//rotation.Inverse();
	//Vector3 localTorque = /*rotation **/ torque;

	Vector3 angularVelocity = torque.CrossProduct(distance);

	// invert z temprarily;
	//angularVelocity[2] = -angularVelocity[2];

	mAngularVelocity = mAngularVelocity + angularVelocity;
}

void RigidBody::ApplyLinearForce(const Vector3& force)
{
	mLinearVelocity = mLinearVelocity + force;
}

void RigidBody::ApplyAngularForceLocal(const Vector3& torque, const Vector3& position)
{
	Vector3 angularVelocity = torque.CrossProduct(position);
	mAngularVelocity = mAngularVelocity - angularVelocity;
}

void RigidBody::ApplyLinearForceLocal(const Vector3& force)
{
	Matrix4 transform = GetTransform();
	transform.Translate(Vector3(0.0f, 0.0f, 0.0f));

	mLinearVelocity = mLinearVelocity + (GetTransform().GetTranslation() + (transform * force));
}

void RigidBody::UpdatePhysics()
{
	mLinearVelocity = mLinearVelocity * mLinearDampening;
	mAngularVelocity = mAngularVelocity * mAngularDampening;
	Matrix4 transform = mBox.GetTransform();

	Vector3 translation = transform.GetTranslation();

	// negate translation so we can rotate it
	transform.Translate(Vector3(0,0,0));

	// rotate it
	Matrix4 angluarVelocity(Matrix4::Identity);
	angluarVelocity.RotateY(mAngularVelocity[Vector3::Y]);
	angluarVelocity.RotateX(mAngularVelocity[Vector3::X]);
	angluarVelocity.RotateZ(mAngularVelocity[Vector3::Z]);

	transform = transform * angluarVelocity;

	// translate
	transform.Translate(translation + mLinearVelocity);

	mBox.SetTransform(transform);
}

void RigidBody::SetPosition(const Vector3& position)
{
	Matrix4 transform = mBox.GetTransform();
	transform.Translate(position);
	mBox.SetTransform(transform);
}

const Matrix4& RigidBody::GetTransform()
{
	return mBox.GetTransform();
}

void RigidBody::SetTransform(const Matrix4& transform)
{
	mBox.SetTransform(transform);
}

float RigidBody::GetLinearDampening()
{
	return mLinearDampening;
}

void RigidBody::SetLinearDampening(float dampening)
{
	mLinearDampening = dampening;
}

float RigidBody::GetAngularDampening()
{
	return mAngularDampening;
}

void RigidBody::SetAngularDampening(float dampening)
{
	mAngularDampening = dampening;
}

void RigidBody::Debug()
{
	mBox.Draw(Vector3(0,1,0));

	Matrix4 rotation = GetTransform();
	rotation.Translate(Vector3(0.0f, 0.0f, 0.0f));
	Vector3 startPoint = GetTransform().GetTranslation();
	Vector3 endPoint = startPoint + (GetLinearVelocity() * Time::GetInstance().GetUpdateRate(Time::UPDATE_PHYSICS));
	endPoint.Draw(startPoint);

	Sphere com(0.1f, GetTransform() * mCentreOfMass);
	com.Draw();
}

const Vector3& RigidBody::GetLinearVelocity()
{
	return mLinearVelocity;
}

void RigidBody::SetLinearVelocity(const Vector3& velocity)
{
	mLinearVelocity = velocity;
}

const Vector3& RigidBody::GetCentreOfMass()
{
	return mCentreOfMass;
}

void RigidBody::SetCentreOfMass(const Vector3& com)
{
	mCentreOfMass = com;
}