#ifndef _SIPHON_TILEENVIRONMENTMANAGER_
#define _SIPHON_TILEENVIRONMENTMANAGER_

#include "../Util/Array.h"

#include "../Core/ActorMgr.h"

using namespace Siphon;

/*
// use this class to hold data to another class
template<class T>
class PointerTile : public Tile
{
public:
	PointerTile(int x, int z, T* dataPtr) : Tile(x, z), data(dataPtr) {}

	T* GetData() { return data; } // maybe just over load the -> operator?
protected:
	T* data;
};
*/

/**
 * tile has a position and a list of actors on that tile
 */
class Tile
{
	friend class TileEnvironmentMgr;

public:
	Tile(int xPos, int zPos, const Vector3& position) : x(xPos), z(zPos), position(position) { }

	int GetX() const { return x; }
	int GetZ() const { return z; }

	// DO NOT USE these when the tile is in use by the tile mgr, use MoveTile instead!
	void SetX(int x) { this->x = x; }
	void SetZ(int z) { this->z = z; }

	const Vector3& GetPosition() const { return position; }

	Array<Actor*>& GetActors() { return actors; }
	Actor* GetActor(int idx) { return actors[idx]; }

protected:
	int x;
	int z;
	Vector3 position;

	Array<Actor*> actors;
};

class TileEnvironmentMgr
{
public:
	~TileEnvironmentMgr();

	virtual Tile* GetSpawnPoint(int index) = 0;

	Tile* GetTile(int x, int z);
	
	// clean up tiles
	virtual void Reset();

	// ray casting in all 4 directions for dynamic tiles
	void GetTiles(int x, int z, int radius, Array<Tile*>& tiles);

	Vector3 GetPosition(int x, int y);
/*
	// note these do not call delete on the tiles!
	virtual void RemoveTiles(Array<Tile*>& tiles);
	virtual void RemoveTile(Tile* tile);

	void AddTile(Tile* tile, int layer = 0);

	// call delete on the tiles
	virtual void DeleteTiles(Array<Tile*>& tiles);

	// move a tile to a new location
	virtual void MoveTile(Tile* tile, int xNew, int zNew);
*/
	// ray casting will continue till it reached edge of map, or till it reaches a tile
	// -1 indicates it reached the edge of the map
	void RayCast(int x, int z, int& xPos, int& xNeg, int& zPos, int& zNeg);

	int GetWidth() { return width; }
	int GetHeight() { return length; }
protected:

	Array<Tile*> tiles; // layers of tiles
	int width;
	int length;
};

#endif