#ifndef _SIPHON_BOMB_
#define _SIPHON_BOMB_

#include "../Engine/Mesh.h"
#include "../Engine/Sound.h"
#include "../Core/ActorMgr.h"
#include "TileEnvironmentMgr.h"
#include "../Core/NetObject.h"
#include <list>

using namespace std;

class ExplosiveCharacter;
class MeshTile;


// adds its self, and removes it self from the actor manager
// its just some gfx for flames
class Explosion : public Actor, public Mesh::Callback
{
public:
	enum ExplosionType
	{
		ZAxisMiddle,
		XAxisMiddle,
		Centre,
		ZAxisLeftEnd,
		ZAxisRightEnd,
		XAxisLeftEnd,
		XAxisRightEnd
	};

	Explosion(int radius);
	virtual ~Explosion();

	void AddExplosionSegment(Tile* tile, ExplosionType type);
	virtual bool Update();

	virtual void SetColour(const Vector3& colour) { this->colour = colour; }

	virtual void PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format);

	bool ExplosionOnTile(Tile* tile);
protected:

	Array< Pair<Mesh*, Tile*> > tileExplosion;
	
	Mesh* ring;
	float ringAlpha;

	int life;
	float speed;
	float scale;

	Effect* effect;
	CGparameter alphaParam;
	CGparameter colourParam;

	float alpha;
	Vector3 colour;

	Sound* explosionSnd;
};

class Bomb : public Actor, public NetObject
{
	friend class ExplosiveGame;
public:
	enum NetMessage
	{
		NM_Kick,
		NM_MoveToTile,
		NM_Explode
	};

	TYPE(2)

	Bomb(ExplosiveCharacter* owner, Tile* mTile, int life, int radius, unsigned int networkId, bool replicate = true);
	virtual ~Bomb();

	virtual bool Update();
	void UpdateBombMovement(Matrix4& transform);
	void Explode(Explosion* detonatingExplosion = NULL, Array<Tile*>* hitTiles = NULL, list<Bomb*>* otherBombs = NULL, bool network = false);
	void Kill(Explosion* detonatingExplosion, Array<Tile*>* hitTiles, list<Bomb*>* otherBombs, bool network = false);

	// returns true if we can be kicked,
	// returns false otherwise
	bool Kick(int xDir, int zDir, ExplosiveCharacter* kickedBy);
	bool IsKicked();

	virtual void DebugDraw();

	int GetLife()	{ return life; }
	int GetRadius() { return radius; }
	Tile* GetTile() { return tile; }

	virtual unsigned int GetConstructorDataSize() { return 0; }
	virtual void GetConstructorData(char* data) { }
	virtual NetType* GetNetType() { return NULL; }
	virtual void SetNetworkId(unsigned int netId);

	virtual void ReceiveMessage(const void* message, int length, NetExtension::NetMessageTarget target);

	static void PreCache();

	void MoveToTile(Tile* nextTile);

	void NetworkExplode(list<Bomb*>& otherBombs);

	ExplosiveCharacter* KickedBy()					{ return kickedBy; }

protected:

	void AdjustBombOffset(const Vector3& position, Vector3& offset);
	bool Explode(Tile* tile, Explosion::ExplosionType type, Explosion* explosion, Explosion::ExplosionType endType, Array<Tile*>* hitTiles, list<Bomb*>* otherBombs, bool networkExplode = false);

	ExplosiveCharacter* owner;
	int life;
	bool exploded;
	float scale; //animating scale
	int radius;
	Mesh* mesh;
	Tile* tile;

	// which way is this bomb moving
	int xDirection;
	int zDirection;
	bool networkKick;
	bool kick;

	ExplosiveCharacter* kickedBy;
};

/**
* Plays a teleport effect on a given tile
*/
class DeathEffect : public Actor, public Mesh::Callback
{
public:
	DeathEffect(Tile* tile, const Vector3& colour1 = Vector3(1.0, 1.0, 1.0));
	~DeathEffect();

	virtual bool Update();
	virtual void PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format);

protected:

	Tile*	tile;
	Vector3 colour;
	Mesh*	mesh;

	int		life;
	float	alpha;
	float	scroll;

	CGparameter alphaParam;
	CGparameter colourParam;
	CGparameter scrollParam;
};

#endif