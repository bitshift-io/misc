#include "ExplosiveCharacter.h"
#include "ExplosiveBomb.h"
#include "DestructableBlock.h"
#include "ExplosiveEnvironmentMgr.h"
#include "ExplosivePickUp.h"
#include "ExplosiveGame.h"
#include "Math3D.h"
#include "Log.h"
#include "../Core/NetServer.h"
#include "../Core/NetClient.h"

#ifdef WIN32
	#include "../Engine/Win32Time.h"
	#include "../Engine/Win32Input.h"
#else
	#include "../Engine/LinuxTime.h"
	#include "../Engine/LinuxInput.h"
#endif

#include "../Engine/Renderer.h"
#include "../Engine/Mesh.h"

int ExplosiveCharacter::charCount = 0;
int ExplosiveCharacter::skinUpdateCount = 0;

#define TWEEN_TIME 8
#define DEFAULT_BOMB_LIFE 50
#define SCALE	   0.5f, 0.5f, 0.45f

//**********************************************

TeamTag::TeamTag()
{
	visible = false;
	mesh = Mesh::LoadMesh("teammarker.mdl");
	mesh->SetCallback(this);

	alphaParam = mesh->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("alpha");
	colourParam = mesh->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("colour");
	inserted = false;
}

TeamTag::~TeamTag()
{
	// release mesh
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(mesh);
	Mesh::ReleaseMesh(mesh);
}

void TeamTag::SetVisible(bool visible)
{
	if (visible)
	{
		if (!inserted)
		{
			ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(mesh);
			inserted = true;
		}
	}
	else
	{
		inserted = false;
		ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(mesh);
	}

	this->visible = visible;
}

void TeamTag::PreReset()
{
	inserted = false;
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(mesh);
}

void TeamTag::SetTeam(int teamIdx/*, const Vector3& colour*/)
{
	this->teamIdx = teamIdx;
	this->colour = colour;

	if (visible && !inserted)
	{
		inserted = true;
		ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(mesh);
	}
}

void TeamTag::PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format)
{
	material->GetEffect()->SetVector3(colourParam, colour);
	material->GetEffect()->SetFloat(alphaParam, 0.8f);
}

void TeamTag::SetPosition(const Vector3& position)
{
	Matrix4 transform = mesh->GetTransform();
	transform.SetTranslation(position);
	mesh->SetTransform(transform);
}


//**********************************************

void ExplosiveCharacter::PreCache()
{
	Mesh::LoadMesh("character.mdl");
	Mesh::LoadMesh("teammarker.mdl");

	SkeletonMgr::Load("character.skl");

	AnimationMgr::Load("walk.anm");
	AnimationMgr::Load("idle.anm");

	Mesh::LoadMesh("blob_shadow.mdl");

	SoundMgr::Load("pickup.wav");
	SoundMgr::Load("drop.wav");
	SoundMgr::Load("kick.wav");

	SoundMgr::Load("Voice_InsaneLaugh.wav");
	SoundMgr::Load("Voice_EvilLaugh.wav");
}

//**********************************************

ExplosiveCharacter::ExplosiveCharacter(int colourIdx, Controller* controller, const char* skinFile, int characterId) : Character(controller, skinFile), 
	currentTile(NULL),
	nextTile(NULL),
	pickupSnd(0),
	deathSnd(0),
	firstDirection(ExplosiveController::Button(-1)),
	kickBomb(false),
	skinFile(skinFile),
	colourIdx(colourIdx)
{
	//colourId = colourIdx;
	networkCounter = 0.f;

	if (characterId == -1)
	{
		this->characterId = charCount;
		++charCount;
	}
	else
	{
		this->characterId = characterId;
		charCount = max(charCount, 	characterId + 1);
	}

	SetNetworkId(this->characterId + 10);
	RegisterWithNetwork(false, false);
	
//	if (registerWithNetwork)
//		RegisterWithNetwork(true, true);


	switch (colourIdx)
	{
	case 0:
		colour = Vector3(1.0, 1.0, 0.0);
		break;
	case 1:
		colour = Vector3(0.0, 1.0, 0.0);
		break;
	case 2:
		colour = Vector3(0.0, 0.0, 1.0);
		break;
	case 3:
		colour = Vector3(0.0, 1.0, 1.0);
		break;
	case 4:
		colour = Vector3(1.0, 0.0, 0.0);
		break;
	case 5:
		colour = Vector3(1.0, 0.5, 0.0);
		break;
	case 6:
		colour = Vector3(1.0, 0.0, 1.0);
		break;
	case 7:
		colour = Vector3(1.0, 1.0, 1.0);
		break;
	};
	teamTag.colour = colour;

	name = GetPlayerName(colourIdx);

	skin->SetCallback(this);
	effect = skin->GetMeshMat().material[0].material->GetEffect();
	colourParam = effect->GetParameterByName("colour");

	skelAnim.SetSkeleton(SkeletonMgr::Load("character.skl"));

	Animation* walkAnim = AnimationMgr::Load("walk.anm");
	Animation* idleAnim = AnimationMgr::Load("idle.anm");
	walkAnimInst = skelAnim.AddAnimation(walkAnim, 0.0f, rand() % walkAnim->GetLength());
	idleAnimInst = skelAnim.AddAnimation(idleAnim, 1.0f, rand() % walkAnim->GetLength());

	// load blob shadow
	shadow = Mesh::LoadMesh("blob_shadow.mdl");

	skin->SetSkeletonAnimation(&skelAnim);
	Reset();

	// scale
	Matrix4 scale(Matrix4::Identity);
	scale.SetScale(SCALE);
	Matrix4 transform = skin->GetTransform();
	skin->SetTransform(scale * transform);

	pickupSnd = SoundMgr::Load("pickup.wav");
	dropSnd = SoundMgr::Load("drop.wav");
	kickSnd = SoundMgr::Load("kick.wav");
}

ExplosiveCharacter::~ExplosiveCharacter()
{
	PreReset();

	--charCount;

	SoundMgr::Release(pickupSnd);
	//AnimationMgr::Release(skelAnim.GetAnimationInst(walkAnimInst).animation, true);
	//AnimationMgr::Release(skelAnim.GetAnimationInst(idleAnimInst).animation, true);
	Mesh::ReleaseMesh(shadow, false);
}


const Vector3& ExplosiveCharacter::GetColour() 
{ 
	return colour; 
}

void ExplosiveCharacter::SetColour(int colourIdx) 
{ 
	teamColourIdx = colourIdx;

	switch (colourIdx)
	{
	case 0:
		colour = Vector3(1.0, 1.0, 0.0);
		break;
	case 1:
		colour = Vector3(0.0, 1.0, 0.0);
		break;
	case 2:
		colour = Vector3(0.0, 0.0, 1.0);
		break;
	case 3:
		colour = Vector3(0.0, 1.0, 1.0);
		break;
	case 4:
		colour = Vector3(1.0, 0.0, 0.0);
		break;
	case 5:
		colour = Vector3(1.0, 0.5, 0.0);
		break;
	case 6:
		colour = Vector3(1.0, 0.0, 1.0);
		break;
	case 7:
		colour = Vector3(1.0, 1.0, 1.0);
		break;
	};
}


unsigned int ExplosiveCharacter::GetConstructorDataSize()
{
	return sizeof(int) + strlen(skinFile.c_str()) + 1;
}

void ExplosiveCharacter::GetConstructorData(char* data)
{
	memcpy(data, &colourIdx, sizeof(int));
	memcpy(data + sizeof(int), skinFile.c_str(), strlen(skinFile.c_str()) + 1);
}

const char* ExplosiveCharacter::GetPlayerName(int idx)
{
	switch (idx)
	{
	case 0:
		return "Yellow";
	case 1:
		return "Green";
	case 2:
		return "Blue";
	case 3:
		return "Cyan";
	case 4:
		return "Red";
	case 5:
		return "Orange";
	case 6:
		return "Purple";
	case 7:
		return "White";
	};

	return "?";
}

void ExplosiveCharacter::DropPickups()
{
	// if im not the owner, and we are in a network game, return
	if (!IsOwner() && (NetServer::IsValid() || NetClient::IsValid()))
		return;

	// pinyata!
	int height = ExplosiveEnvironmentMgr::GetInstance().GetHeight();
	int width = ExplosiveEnvironmentMgr::GetInstance().GetWidth();

	for (int i = 0; i < PICKUP_COUNT; ++i)
	{
		int count = pickups[i];

		for (int p = 0; p < count; ++p)
		{
			Tile* t = 0;

			do 
			{	
				t = 0;

				// spawn this pickup on a random tile
				int z = rand() % height;
				int x = rand() % width;

				t = ExplosiveEnvironmentMgr::GetInstance().GetTile(x, z);
				if (t == 0)
					continue;

				// check that we arent spawning a pickup on a tile we cant
				for (int a = 0; a < t->GetActors().GetSize(); ++a)
				{
					Actor* actor = t->GetActor(a);
					if (actor->GetType() == Bomb::Type ||
						actor->GetType() == ExplosiveCharacter::Type ||
						actor->GetType() == ExplosivePickup::Type ||
						actor->GetType() == DestructableBlock::Type)
					{
						t = 0;
						break;
					}
				}

			} while(!t);
		
			DropPickup(i, t->GetX(), t->GetZ());
		}
	}
}

void ExplosiveCharacter::DropPickup(int type, int x, int z)
{
	Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(x, z);

	// so now we have a tile to spawn a pickup
	switch(type)
	{
	case BombExpandPickUp::PickupType:
		new BombExpandPickUp(t, "pickup.mdl", "pickup_fire.mat");
		break;
	case BombCountPickUp::PickupType:
		new BombCountPickUp(t, "pickup.mdl", "pickup_bomb.mat");
		break;
	case SnailPickUp::PickupType:
		new SnailPickUp(t, "pickup.mdl", "pickup_snail.mat");
		break;
	case TeleportPickUp::PickupType:
		new TeleportPickUp(t, "pickup.mdl", "pickup_teleport.mat");
		break;
	case ShieldPickUp::PickupType:
		new ShieldPickUp(t, "pickup.mdl", "pickup_shield.mat");
		break;
	case RandomPickUp::PickupType:
		new RandomPickUp(t, "pickup.mdl", "pickup_random.mat");
		break;
	case DiseasePickUp::PickupType:
		new DiseasePickUp(t, "pickup.mdl", "pickup_disease.mat");
		break;
	case BombKickPickUp::PickupType:
		new BombKickPickUp(t, "pickup.mdl", "pickup_kick.mat");
		break;
	case SpeedPickUp::PickupType:
		new SpeedPickUp(t, "pickup.mdl", "pickup_speed.mat");
		break;
	case ReversePickUp::PickupType:
		new ReversePickUp(t, "pickup.mdl", "pickup_reverse.mat");
		break;
	}

	int data[4] = { NM_DropPickup, type, t->GetX(), t->GetZ() };
	Send(data, sizeof(int) * 4);
}

void ExplosiveCharacter::PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format)
{
	if (material->GetEffect() == effect)
		mesh->GetMeshMat().material[0].material->GetEffect()->SetVector3(colourParam, colour);
}

void ExplosiveCharacter::NotifyReceivePickup(ExplosivePickup* pickup)
{
	SendPosition(true);

	// append pickup types and a count to the pickups array
	//pickups
	++pickups[pickup->GetPickupType()];

	Log::Print("about to play pick up sound\n");

	if (pickupSnd)
		pickupSnd->Play();

	Log::Print("now playing\n");

	GetStats().AddPickup();
}

void ExplosiveCharacter::NotifyKilledPlayer(ExplosiveCharacter* player)
{
	if (player != this)
		GetStats().AddKill();
}

bool ExplosiveCharacter::Update()
{
	if (IsDead())
		return true; // dont destroy this actor!

	if (ExplosiveGame::GetInstance().IsDebuggerAttached())
		ExplosiveGame::GetInstance().Crash1();

	if (IsOwner() || (!NetServer::IsValid() && !NetClient::IsValid()))
		Character::Update();

	for (int i = 0; i < modifiers.GetSize(); ++i)
	{
		Modifier* m = modifiers[i];

		if (m->GetType() == DiseaseModifier::Type)
		{
			// only owner can update this as it cases them to drop bombs!
			if (!IsOwner())
				continue;
		}

		if (m->Update() == false)
		{
			modifiers.Remove(m);
			delete m;
			--i;
		}
	}

	UpdateAnimations();
	UpdateNetwork();

	skelAnim.Evaluate(5);

//	if (characterId == skinUpdateCount)
		skin->Update();

	// inverse out the scale
	Matrix4 scaleInv(Matrix4::Identity);
	scaleInv.SetScale(SCALE);
	scaleInv.Inverse();
	shadow->SetTransform(scaleInv * skin->GetTransform());
	teamTag.SetPosition(skin->GetTransform().GetTranslation());

	return true;
}

void ExplosiveCharacter::PreReset()
{
	if (currentTile)
	{
		for (int idx = 0; idx < currentTile->GetActors().GetSize(); ++idx)
		{
			if (currentTile->GetActor(idx) == this)
			{
				currentTile->GetActors().Remove(idx);
				break;
			}
		}				
	}

	// clean up any modifiers
	while (modifiers.GetSize())
	{
		Modifier* m = modifiers[0];
		modifiers.Remove(m);
		delete m;
	}

	currentTile = 0;
	nextTile = 0;

	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(shadow);
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(skin);

	teamTag.PreReset();
}

void ExplosiveCharacter::Reset()
{
	Character::Reset();

	// clean up any modifiers
	while (modifiers.GetSize())
	{
		Modifier* m = modifiers[0];
		modifiers.Remove(m);
		delete m;
	}

	reverseControls = false;
	kickBomb = false;
	bombCount = 1;
	bombRadius = 1;
	bombLife = DEFAULT_BOMB_LIFE;
	curBombCount = 0;
	direction = ExplosiveController::DOWN;

	skinDirection = Quaternion(Vector3(0, 1, 0), Math::DtoR(90.0f));

	timeFromIdle = 0;
	timeFromWalk = TWEEN_TIME;

	speed = PLAYER_SPEED;

	// clear pickups
	for (int i = 0; i < PICKUP_COUNT; ++i)
		pickups[i] = 0;

	if (currentTile)
	{
		for (int idx = 0; idx < currentTile->GetActors().GetSize(); ++idx)
		{
			if (currentTile->GetActor(idx) == this)
			{
				currentTile->GetActors().Remove(idx);
				break;
			}
		}				
	}

	currentTile = ExplosiveEnvironmentMgr::GetInstance().GetSpawnPoint(characterId);
	//SetLocation(ExplosiveEnvironmentMgr::GetInstance().GetSpawnPoint(characterId));
	
	if (currentTile)
	{
		currentTile->GetActors().Insert(this);
		Matrix4 transform = skin->GetTransform();
		transform.SetTranslation(currentTile->GetPosition());
		skin->SetTransform(transform);
	}

	// clear death sound
	if (deathSnd)
		SoundMgr::Release(deathSnd);
	deathSnd = 0;

	// add to renderer
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(shadow);
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(skin);

	SetColour(colourIdx);
	teamTag.SetTeam(colourIdx/*, colour*/);

	UpdateNetwork();
}

void ExplosiveCharacter::DropBomb()
{
	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	if (curBombCount >= bombCount)
		return;

	// check to make sure a bomb isnt already on this tile
	for (int i = 0; i < currentTile->GetActors().GetSize(); ++i)
	{
		Actor* a = currentTile->GetActor(i);
		if (a->GetType() == Bomb::Type)
			return;
	}

	if (dropSnd)
		dropSnd->Play();

	++curBombCount;
	Bomb* bomb = new Bomb(this, currentTile, bombLife, bombRadius, -1);
	ActorMgr::GetInstance().AddActor(static_cast<Actor*>(bomb), 1); // bombs go in to group 1

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
}

void ExplosiveCharacter::DestroyBomb(Bomb* bomb)
{
	--curBombCount;
	if (curBombCount < 0)
		curBombCount = 0;
}

void ExplosiveCharacter::CheckInput()
{
	ExplosiveController* inputController = static_cast<ExplosiveController*>(controller);

	wasMoving = moving;
	moving = false;
	bool keyPressed = false;
	ExplosiveInputController::Button pressedKey;

	bool directionCheck[4] = { true, true, true, true };

	bool keyDown[4];
	for (int i = 0; i < 4; ++i)
		keyDown[i] = inputController->KeyDown(ExplosiveController::Button(i));

	// reverse controls on humans only
	if (reverseControls && inputController->IsHuman())
	{
		// reverse up and down
		bool temp = keyDown[ExplosiveInputController::UP];
		keyDown[ExplosiveInputController::UP] = keyDown[ExplosiveInputController::DOWN];
		keyDown[ExplosiveInputController::DOWN] = temp;

		// reverse left and right
		temp = keyDown[ExplosiveInputController::LEFT];
		keyDown[ExplosiveInputController::LEFT] = keyDown[ExplosiveInputController::RIGHT];
		keyDown[ExplosiveInputController::RIGHT] = temp;
	}

/*
	// button mashing stats
	for (int i = 0; i < 5; ++i)
	{
		if (inputController->KeyPressed(ExplosiveInputController::Button(i)))
			GetStats().AddButtonPress();
	}*/

	if (firstDirection != -1 && keyDown[firstDirection]) //!inputController->KeyDown(firstDirection))
	{
		// the firstDirection key is now not pressed,
		// so check to see if another key is pressed and make it the first key
		if (keyDown[ExplosiveInputController::UP]) //inputController->KeyDown(ExplosiveInputController::UP))
			firstDirection = ExplosiveInputController::UP;
		else if (keyDown[ExplosiveInputController::DOWN]) //inputController->KeyDown(ExplosiveInputController::DOWN))
			firstDirection = ExplosiveInputController::DOWN;
		else if (keyDown[ExplosiveInputController::LEFT]) //inputController->KeyDown(ExplosiveInputController::LEFT))
			firstDirection = ExplosiveInputController::LEFT;
		else if (keyDown[ExplosiveInputController::RIGHT]) //inputController->KeyDown(ExplosiveInputController::RIGHT))
			firstDirection = ExplosiveInputController::RIGHT;
		else 
			firstDirection = ExplosiveInputController::Button(-1);
	}

	if (firstDirection != -1 && keyDown[firstDirection]) //inputController->KeyDown(firstDirection))
	{
		if (firstDirection == ExplosiveInputController::UP)
			directionCheck[ExplosiveInputController::DOWN] = false;

		if (firstDirection == ExplosiveInputController::DOWN)
			directionCheck[ExplosiveInputController::UP] = false;

		if (firstDirection == ExplosiveInputController::LEFT)
			directionCheck[ExplosiveInputController::RIGHT] = false;

		if (firstDirection == ExplosiveInputController::RIGHT)
			directionCheck[ExplosiveInputController::LEFT] = false;
	}
	
	if (directionCheck[ExplosiveInputController::UP] && firstDirection != ExplosiveInputController::UP && keyDown[ExplosiveInputController::UP]) //inputController->KeyDown(ExplosiveInputController::UP))
	{
		moving = Move(ExplosiveInputController::UP);
		keyPressed = true;
		pressedKey = ExplosiveInputController::UP;
	}

	if (directionCheck[ExplosiveInputController::DOWN] && !keyPressed && firstDirection != ExplosiveInputController::DOWN && keyDown[ExplosiveInputController::DOWN]) // inputController->KeyDown(ExplosiveInputController::DOWN))
	{
		moving = Move(ExplosiveInputController::DOWN);
		keyPressed = true;
		pressedKey = ExplosiveInputController::DOWN;
	}

	if (directionCheck[ExplosiveInputController::RIGHT] && !keyPressed && firstDirection != ExplosiveInputController::RIGHT && keyDown[ExplosiveInputController::RIGHT]) //inputController->KeyDown(ExplosiveInputController::RIGHT))
	{
		moving = Move(ExplosiveInputController::RIGHT);
		keyPressed = true;
		pressedKey = ExplosiveInputController::RIGHT;
	}

	if (directionCheck[ExplosiveInputController::LEFT] && !keyPressed && firstDirection != ExplosiveInputController::LEFT && keyDown[ExplosiveInputController::LEFT]) //inputController->KeyDown(ExplosiveInputController::LEFT))
	{
		moving = Move(ExplosiveInputController::LEFT);
		keyPressed = true;
		pressedKey = ExplosiveInputController::LEFT;
	}

	// check the direction key we are probably holing down last
	if (firstDirection != -1 && keyDown[firstDirection]) //inputController->KeyDown(firstDirection))
	{
		if (!moving || (moving && !nextTile))
		{
			moving = Move(firstDirection);
			keyPressed = true;
		}
	}

	if (keyPressed)
	{
		// if we just pressed a key, record it so we can check it last
		if (firstDirection == -1)
			firstDirection = pressedKey;
	}
	else
	{
		// no keys pressed, so say so
		firstDirection = ExplosiveController::Button(-1);
	}
		
	// if we are closer to nextTile, nextTile will become the currentTile, nextTile will become null
	if (nextTile)
	{
		float distToNext = (skin->GetTransform().GetTranslation() - nextTile->GetPosition()).MagnitudeSquared();
		float distToCurret = (skin->GetTransform().GetTranslation() - currentTile->GetPosition()).MagnitudeSquared();
		if (distToNext <= distToCurret)
		{
			if (currentTile)
			{
				for (int idx = 0; idx < currentTile->GetActors().GetSize(); ++idx)
				{
					if (currentTile->GetActor(idx) == this)
					{
						currentTile->GetActors().Remove(idx);
						break;
					}
				}				
			}

			currentTile = nextTile;
			nextTile = NULL;

			currentTile->GetActors().Insert(this);

			// register that the character has moved in the environment
			ExplosiveEnvironmentMgr::GetInstance().MoveToTile(currentTile, this);
		}
	}

	if (inputController->KeyPressed(ExplosiveInputController::FIRE))
		DropBomb();

	//
	// make the character face the correct direction
	//
	Quaternion axis;
	switch (direction)
	{
	case ExplosiveController::DOWN:
		axis.CreateFromAxisAngle(Vector3(0, 1.0, 0), Math::DtoR(90.0f));
		break;
	case ExplosiveController::UP:
		axis.CreateFromAxisAngle(Vector3(0, 1.0, 0), Math::DtoR(-90.0f));
		break;
	case ExplosiveController::LEFT:
		axis.CreateFromAxisAngle(Vector3(0, 1.0, 0), Math::DtoR(0.0f));
		break;
	case ExplosiveController::RIGHT:
		axis.CreateFromAxisAngle(Vector3(0, 1.0, 0), Math::DtoR(180.0f));
		break;
	}

	skinDirection = skinDirection.Interpolate(axis, 0.4f);
	Matrix4 transform = skinDirection.GetRotationMatrix();
	Matrix4 scale(Matrix4::Identity);
	scale.SetScale(SCALE);
	transform = scale * transform;
	transform.SetTranslation(skin->GetTransform().GetTranslation());	
	skin->SetTransform(transform);
}

void ExplosiveCharacter::UpdateAnimations()
{
	//
	// tween from idle to walking and vice versa
	//
	float walkWeight = 0.f;
	float idleWeight = 0.f;

	if (moving)
	{
		++timeFromIdle;
		timeFromWalk = 0;

		walkWeight = (float)min(timeFromIdle, TWEEN_TIME) / (float)TWEEN_TIME;
		idleWeight = 1.f - walkWeight;
	}
	else
	{
		++timeFromWalk;
		timeFromIdle = 0;

		idleWeight = (float)min(timeFromWalk, TWEEN_TIME) / (float)TWEEN_TIME;
		walkWeight = 1.f - idleWeight;
	}

	SkeletonAnimation::AnimationInst& walkInst = skelAnim.GetAnimationInst(walkAnimInst);
	walkInst.weight = walkWeight;

	SkeletonAnimation::AnimationInst& idleInst = skelAnim.GetAnimationInst(idleAnimInst);
	idleInst.weight = idleWeight;
}

bool ExplosiveCharacter::Move(ExplosiveController::Button dir)
{
	float localSpeed = speed;
	Vector3 position = skin->GetTransform().GetTranslation();
	int axis = Vector3::X;
	int xTileOffset = 0;
	int zTileOffset = 1;
	bool checkForNextTile = false;
	bool allowMove = false;

	switch (dir)
	{
	case ExplosiveController::DOWN:
		{
			localSpeed = -localSpeed;
			axis = Vector3::Z;
			xTileOffset = 0;
			zTileOffset = -1;

			if (position[axis] <= currentTile->GetPosition()[axis])
				checkForNextTile = true;
			else
				allowMove = true;
		}
		break;
	case ExplosiveController::UP:
		{
			axis = Vector3::Z;
			xTileOffset = 0;
			zTileOffset = 1;

			if (position[axis] >= currentTile->GetPosition()[axis])
				checkForNextTile = true;
			else
				allowMove = true;
		}
		break;
	case ExplosiveController::LEFT:
		{
			axis = Vector3::X;
			xTileOffset = 1;
			zTileOffset = 0;

			if (position[axis] >= currentTile->GetPosition()[axis])
				checkForNextTile = true;
			else
				allowMove = true;
		}
		break;
	case ExplosiveController::RIGHT:
		{
			localSpeed = -localSpeed;
			axis = Vector3::X;
			xTileOffset = -1;
			zTileOffset = 0;

			if (position[axis] <= currentTile->GetPosition()[axis])
				checkForNextTile = true;
			else
				allowMove = true;
		}
		break;
	}

	Matrix4 transform(Matrix4::Identity);

	int otherAxis = (axis == Vector3::X) ? Vector3::Z : Vector3::X;
	Tile* tile = 0;

	// if we are moving towards the center of the current tile,
	// allow this to happen
	if (checkForNextTile)
	{
		tile = ExplosiveEnvironmentMgr::GetInstance().GetTile(currentTile->GetX() + xTileOffset, currentTile->GetZ() + zTileOffset);

		if (!tile)
		{
			if (nextTile)
			{
				tile = ExplosiveEnvironmentMgr::GetInstance().GetTile(nextTile->GetX() + xTileOffset, nextTile->GetZ() + zTileOffset);

				if (!tile || tile == currentTile)
				{
					nextTile = NULL;
					return false;
				}
			}
			else
			{
				nextTile = NULL;
				return false;
			}
		}
		else
		{
			nextTile = tile;
		}
	}

	if (nextTile)
	{
		for (int i = 0; i < nextTile->GetActors().GetSize(); ++i)
		{
			Actor* a = nextTile->GetActor(i);
			if (a->GetType() == Bomb::Type)
			{
				if (!KickBomb(static_cast<Bomb*>(a), xTileOffset, zTileOffset))
				{
					nextTile = 0;
					return false;
				}
			}
			else if (a->GetType() == DestructableBlock::Type)
			{
				nextTile = 0;
				return false;
			}
		}
	}

	if (nextTile || allowMove)
	{
		// gradually align the player towards the center, so we dont pop there
		if (nextTile)
		{	
			float direction = position[otherAxis] - nextTile->GetPosition()[otherAxis];
			if (direction > speed || direction < -speed)
			{
				position[otherAxis] += direction > 0 ? -speed : speed;
				localSpeed = 0;
			}
			else
			{
				position[otherAxis] = nextTile->GetPosition()[otherAxis];
			}
		}

		position[axis] += localSpeed;
		direction = dir;

		transform.SetTranslation(position);
		skin->SetTransform(transform);
		return true;
	}

	return false;
}

bool ExplosiveCharacter::KickBomb(Bomb* bomb, int xDir, int zDir)
{
	if (!kickBomb || bomb->KickedBy() == this)
		return false;

	bool kicked = bomb->Kick(xDir, zDir, this);

	if (kicked && kickSnd && !kickSnd->IsPlaying())
		kickSnd->Play();

	return kicked;
}

void ExplosiveCharacter::Kill()
{
	// move player from tile
	if (currentTile)
	{
		for (int idx = 0; idx < currentTile->GetActors().GetSize(); ++idx)
		{
			if (currentTile->GetActor(idx) == this)
			{
				currentTile->GetActors().Remove(idx);
				break;
			}
		}				
	}

	// remove from renderer
	PreReset();

	Character::Kill(0);
}

bool ExplosiveCharacter::Kill(Actor* actor)
{
	// already dead
	if (!currentTile)
		return false;

	// spawn death fx
	new DeathEffect(GetLocation(), colour);

	// check to see if we have the shield modifier
	for (int i = 0; i < modifiers.GetSize(); ++i)
	{
		Modifier* m = modifiers[i];
		if (m->GetType() == ShieldModifier::Type)
		{
			static_cast<ShieldModifier*>(m)->PlayerKilled();
			modifiers.Remove(m);
			delete m;

			if (NetServer::IsValid())
			{
				int msg = NM_DestroyShield;
				Send(&msg, sizeof(int));
			}

			return false;
		}
	}

	// play random death sound
	int randDeath = rand() % 3;
	switch (randDeath)
	{
	case 0:
		deathSnd = SoundMgr::Load("Voice_InsaneLaugh.wav");
		break;
	case 1:
		deathSnd = SoundMgr::Load("Voice_EvilLaugh.wav");
		break;
	};

	if (deathSnd)
		deathSnd->Play();


	// drop pickups if drop pickups enabled
	DropPickups();

	if (actor == this)
		GetStats().AddSuicide();
	else
		GetStats().AddDeath();

	Kill();

	int message = NM_Kill;
	Send(&message, sizeof(int));

	return true;
}

void ExplosiveCharacter::DebugDraw()
{
	if (IsDead())
		return;

//	Character::DebugDraw();

	Box box;
	box.SetMin(Vector3(-1.5f,-1.5f,-1.5f));
	box.SetMax(Vector3(1.5f, 1.5f, 1.5f));

	Matrix4 trans(Matrix4::Identity);
	trans.Draw(50.0f);

	//draw next tile
	if (nextTile)
	{
		trans.SetTranslation(nextTile->GetPosition());		
		box.SetTransform(trans);
		box.Draw(Vector3(1,1,0));
	}

	//draw current tile
	trans.SetTranslation(currentTile->GetPosition());
	box.SetTransform(trans);
	box.Draw(Vector3(0,1,1));

	trans.SetTranslation(Vector3(0.f, 10.f, 0.f));
	Camera* cam = RenderMgr::GetInstance().GetDefaultCamera();
	cam->LookAt();
	skin->SetTransform(trans);

	trans.SetTranslation(Vector3(0.f, 10.f, -8.f));

	skelAnim.Draw(trans);
}

void ExplosiveCharacter::UpdateSkin()
{ 
	if (charCount == 0)
		return;

	++skinUpdateCount; 
	skinUpdateCount = skinUpdateCount % charCount; 
}

void ExplosiveCharacter::SetLocation(Tile* t, bool fromNetwork)
{
	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	// remove from old tile
	if (currentTile)
	{
		for (int idx = 0; idx < currentTile->GetActors().GetSize(); ++idx)
		{
			if (currentTile->GetActor(idx) == this)
			{
				currentTile->GetActors().Remove(idx);
				break;
			}
		}				
	}

	// assign new tiles
	currentTile = t;
	nextTile = 0;

	if (!currentTile)
		return;

	// insert to new tile
	currentTile->GetActors().Insert(this);

	if (fromNetwork)
		return;

	// translate
	Matrix4 transform = skin->GetTransform();
	transform.SetTranslation(currentTile->GetPosition());
	skin->SetTransform(transform);

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
}

NetType* ExplosiveCharacter::GetNetType()
{
	return NULL; //&ExplosiveCharacterNetType::instance;
}

void ExplosiveCharacter::ReceiveMessage(const void* message, int length, NetExtension::NetMessageTarget target)
{
	BlockSendBegin();

	if (!ExplosiveEnvironmentMgr::GetInstance().Valid())
	{
		BlockSendEnd();
		return;
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	int msg = *((const int*)message);
	const char* data = (const char*)message + sizeof(int);
	switch (msg)
	{
	case NM_DestroyShield:
		{
			// spawn death fx
			new DeathEffect(GetLocation(), colour);

			// check to see if we have the shield modifier
			for (int i = 0; i < modifiers.GetSize(); ++i)
			{
				Modifier* m = modifiers[i];
				if (m->GetType() == ShieldModifier::Type)
				{
					static_cast<ShieldModifier*>(m)->PlayerKilled();
					modifiers.Remove(m);
					delete m;
					break;
				}
			}
		}
		break;

	case NM_Position:
		{
			int extraData[3];
			memcpy(&extraData, data, sizeof(int) * 3);

			moving = extraData[0];

			int x = extraData[1];
			int z = extraData[2];

			float compressed[7];
			memcpy(&compressed, data + sizeof(int) * 3, sizeof(float) * 7);

			Quaternion rotation = Quaternion(compressed[0], compressed[1], compressed[2], compressed[3]);
			Matrix4 transform = rotation.GetRotationMatrix();
			transform.SetTranslation(compressed[4], compressed[5], compressed[6]);

			/*
			memcpy(&transform, data, sizeof(Matrix4));

			int x = transform[3];
			int z = transform[7];
			moving = extraData[0];


			transform[3] = 0.f;
			transform[7] = 0.f;
			transform[11] = 0.f;*/

			if (x == -1)
			{
				SetLocation(NULL, true);
			}
			else
			{
				Tile* newTile = ExplosiveEnvironmentMgr::GetInstance().GetTile(x, z);
				if (newTile && currentTile != newTile)
				{
					ExplosiveEnvironmentMgr::GetInstance().MoveToTile(newTile, this);
					SetLocation(newTile, true);
				}
			}

			//moving = true;
			Matrix4 scale(Matrix4::Identity);
			scale.SetScale(SCALE);
			transform = scale * transform;
			skin->SetTransform(transform);
		}
		break;
/*	case NM_TileInfo:
		{
			const int* tileInfo = (const int*)data;

			if (tileInfo[0] == -1)
			{
				SetLocation(NULL, true);
			}
			else
			{
				Tile* newTile = ExplosiveEnvironmentMgr::GetInstance().GetTile(tileInfo[0], tileInfo[1]);
				if (currentTile != newTile)
				{
					ExplosiveEnvironmentMgr::GetInstance().MoveToTile(newTile, this);
					SetLocation(newTile, true);
				}
			}
		}
		break;*/
	case NM_DropBomb:
		{
			//cout << "received a message to drop a bombfor id:" << GetNetworkId() << endl;

			const int* intData = (const int*)data;
			Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(intData[1], intData[2]);
			Bomb* bomb = new Bomb(this, t, intData[4], intData[3], intData[0]);
			ActorMgr::GetInstance().AddActor(static_cast<Actor*>(bomb), 1); // bombs go in to group 1
		}
		break;
	case NM_Kill:
		{
			Kill(0);
		}
		break;
	case NM_Teleport:
		{
			const int* intData = (const int*)data;
			int charId = intData[0];

			Array<Actor*>& characters = ActorMgr::GetInstance().GetActorGroup(0);
			for (int i = 0; i < characters.GetSize(); ++i)
			{
				ExplosiveCharacter* e = static_cast<ExplosiveCharacter*>(characters[i]);
				if (e->GetCharacterId() == charId)
				{
					Teleport(e, ExplosiveEnvironmentMgr::GetInstance().GetTile(intData[1], intData[2]), ExplosiveEnvironmentMgr::GetInstance().GetTile(intData[3], intData[4]));
				}
			}

			/*
			if (GetCharacterId() == charId)
			{
				TeleportEffect* te = new TeleportEffect(GetLocation(), NULL, GetColour());
				ActorMgr::GetInstance().AddActor(te, 2); // special effects go in to group 2
			}
			else
			{
				// get the character using thier id
				Array<Actor*>& characters = ActorMgr::GetInstance().GetActorGroup(0);
				for (int i = 0; i < characters.GetSize(); ++i)
				{
					ExplosiveCharacter* e = static_cast<ExplosiveCharacter*>(characters[i]);
					if (e->GetCharacterId() == charId)
					{
						TeleportEffect* te = new TeleportEffect(e->GetLocation(), GetLocation(),
							GetColour(), e->GetColour());
						ActorMgr::GetInstance().AddActor(te, 2); // special effects go in to group 2
					}
				}	
			}

			if (intData[1] == -1)
			{
				SetLocation(NULL, true);
			}
			else
			{
				Tile* newTile = ExplosiveEnvironmentMgr::GetInstance().GetTile(intData[1], intData[2]);
				ExplosiveEnvironmentMgr::GetInstance().MoveToTile(newTile, this);
				SetLocation(newTile, false); //cheating here :D
			}*/
		}
		break;
	case NM_DropPickup:
		{
			const int* intData = (const int*)data;
			DropPickup(intData[0], intData[1], intData[2]);
		}	
		break;
	};

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
	BlockSendEnd();
}

void ExplosiveCharacter::Teleport(ExplosiveCharacter* other, Tile* otherLocation, Tile* thisLocation)
{
	// other is the person pickuping up the teleport
	// this is the person who is being swapped with

	if (otherLocation == 0 && thisLocation == 0)
	{
		int message = NM_Teleport;

		int tileInfo[6];
		tileInfo[0] = NM_Teleport;
		tileInfo[1] = other->GetCharacterId();
		tileInfo[2] = other->GetLocation()->GetX();
		tileInfo[3] = other->GetLocation()->GetZ();
		tileInfo[4] = GetLocation()->GetX();
		tileInfo[5] = GetLocation()->GetZ();

		Send(tileInfo, sizeof(int) * 6);
	}
	else
	{
		ExplosiveEnvironmentMgr::GetInstance().KillPickup(otherLocation ? otherLocation : other->GetLocation());
		ExplosiveEnvironmentMgr::GetInstance().KillPickup(thisLocation ? thisLocation : GetLocation());
	}

	// teleporting to self
	if (other == this)
	{
		TeleportEffect* te = new TeleportEffect(GetLocation(), NULL, GetColour());
		ActorMgr::GetInstance().AddActor(te, 2); // special effects go in to group 2
		return;
	}

	Tile* t = otherLocation ? otherLocation : other->GetLocation();
	other->SetLocation(thisLocation ? thisLocation : GetLocation());
	SetLocation(t);

	TeleportEffect* te = new TeleportEffect(otherLocation ? otherLocation : other->GetLocation(), thisLocation ? thisLocation : GetLocation(), 
			other->GetColour(), GetColour());
	ActorMgr::GetInstance().AddActor(te, 2); // special effects go in to group 2
}

void ExplosiveCharacter::UpdateNetwork()
{
	// owner can only send the following goodies
	if (!IsOwner())
		return;
/*
	++networkCounter;

	// send less packets when not moving
	if (!moving)
	{
		if (networkCounter < 5)
			return;
		else
			networkCounter = 0.f;
	}
*/
	if (wasMoving && !moving)
	{
		SendPosition(true);
		return;
	}

	if (!moving)
		return;

	SendPosition(false);
/*
	// send tile info
	{
		int tileInfo[5];
		tileInfo[0] = NM_TileInfo;
		if (currentTile)
		{
			tileInfo[1] = currentTile->GetX();
			tileInfo[2] = currentTile->GetZ();
		}
		else
		{
			tileInfo[1] = -1;
			tileInfo[2] = -1;
		}

		Send(tileInfo, sizeof(int) * 3);
	}*/
}

void ExplosiveCharacter::SendPosition(bool reliable)
{
	// send position/transform
	{
		int message[4];
		message[0] = NM_Position;
		char buffer[256];
		
		// this matrix contains redundant info, so i can pack in extra info
		Matrix4 transform = skin->GetTransform();

		float compressed[7];
		for (int i = 0; i < 4; ++i)
			compressed[i] = skinDirection[i];

		Vector3 pos = transform.GetTranslation();
		for (int i = 0; i < 3; ++i)
			compressed[i + 4] = pos[i];

		message[1] = moving;

		if (currentTile)
		{
			message[2] = currentTile->GetX();
			message[3] = currentTile->GetZ();
		}
		else
		{
			message[2] = -1;
			message[3] = -1;
		}

		memcpy(buffer, &message, sizeof(int) * 4);
		memcpy(buffer + (sizeof(int) * 4), &compressed, sizeof(float) * 7);
		Send(buffer, (sizeof(int) * 4) + (sizeof(float) * 7), NetExtension::All, reliable ? SocketStream::Reliable : SocketStream::UnReliable);
	}
}

void ExplosiveCharacter::BombReceiveNetworkId(unsigned int networkId, Bomb* bomb)
{
	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	// tell the network to create a bomb
	int buffer[6];
	buffer[0] = NM_DropBomb;
	buffer[1] = networkId;
	buffer[2] = bomb->GetTile()->GetX();
	buffer[3] = bomb->GetTile()->GetZ();
	buffer[4] = bomb->GetRadius();
	buffer[5] = bomb->GetLife();
	Send((const char*)buffer, sizeof(int) * 6);

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
}
