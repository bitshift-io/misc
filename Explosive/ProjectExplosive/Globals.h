#ifndef _GLOBALS_H_

#define MASTER_SERVER_URL	"www.blackcarbon.net"
#define MASTER_SERVER_PATH	"explosive/PHPMasterServer"

#define SERVER_PORT		6464
#define CLIENT_PORT		6465

#define MASTER_SERVER_PING		10 * 60 * 1000		// 10 minutes
#define NAT_PUNCH_CHECK			10 * 1000			// 10 seconds

#define VERSION			"1.0"

#define ABOUT_TEXT			\
"Explosive v1.0				\n\
Black Carbon				\n\
							\n\
Programming		    		\n\
   Fabian Mathews			\n\
							\n\
Art 						\n\
   Bronson Mathews			\n\
							\n\
www.blackcarbon.net			\n\
Copyright 2006				\n\
"

#define DEMO_STRING	"DEMO"

#ifdef _DEMO
	#define MAX_PLAYERS 6
//4 - demo is 6, beta has 8

#else
	#define MAX_PLAYERS 8
#endif

#ifndef DEBUG
//	#define COPY_PROTECTION
#endif


#ifdef _DEMO
	#define EXPLOSIVE_GAME_ID 0x63
#else
	#define EXPLOSIVE_GAME_ID 0x64
#endif


#endif