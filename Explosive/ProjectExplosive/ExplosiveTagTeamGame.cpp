#include "ExplosiveTagTeamGame.h"
#include "ExplosiveGame.h"
#include "ExplosiveCharacter.h"

bool DeathmatchGame::PlayerKilled(Character* killer, Character* killed)
{
	if (GameMode::PlayerKilled(killer, killed))
	{
		ExplosiveCharacter* e = static_cast<ExplosiveCharacter*>(killer);
		e->NotifyKilledPlayer(static_cast<ExplosiveCharacter*>(killed));

		// check to see if there are any humans left
		// if not, end game
		int numAliveHumans = 0;
		int numAlive = 0;
		int numHumans = 0;
		Array<Character*>& characters = ExplosiveGame::GetInstance().GetCharacters();
		for (int i = 0; i < characters.GetSize(); ++i)
		{
			Character* character = characters[i];
			ExplosiveController* control = static_cast<ExplosiveController*>(character->GetController());
			if (!character->IsDead())
			{
				++numAlive;

				if (control->IsHuman())
					++numAliveHumans;
			}

			if (control->IsHuman())
			{
				++numHumans;
			}
		}

		// if only 1 human left, they win, is ai cant win
		// if ai can win, there can only 1 be 1 survivor
		if (	(!ExplosiveGame::GetInstance().GetAICanWin() && numAliveHumans <= 1 && numHumans >= 2) 
			||  (!ExplosiveGame::GetInstance().GetAICanWin() && numAliveHumans <= 0 && numHumans <= 1) 
			||	(numAlive <= 1))
		{
			// get the winner
			for (int i = 0; i < characters.GetSize(); ++i)
			{
				ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(characters[i]);
				if (!c->IsDead())
				{
					c->GetStats().AddWin();
					break;
				}
			}

			ExplosiveGame::GetInstance().SetEndRound();
		}

		ExplosiveGame::GetInstance().DoCameraShake(0.5, 20);
		return true;
	}

	return false;
}

//-------------------------------------------------------------------------

ExplosiveTagTeamGame::ExplosiveTagTeamGame()
{
	for (int i = 0; i < MAX_PLAYERS; ++i)
		players[i] = false;

	for (int i = 0; i < MAX_PLAYERS; ++i)
		teams[i] = 0;
}

void ExplosiveTagTeamGame::Reset()
{
	for (int i = 0; i < MAX_PLAYERS; ++i)
		teams[i] = players[i] ? 1 : 0;
}

bool ExplosiveTagTeamGame::PlayerKilled(Character* killer, Character* killed)
{
	ExplosiveGame::GetInstance().DoCameraShake(0.5, 20);

	ExplosiveCharacter* eKilled = static_cast<ExplosiveCharacter*>(killed);
	ExplosiveCharacter* eKiller = static_cast<ExplosiveCharacter*>(killer);

	TeamTag& tagKilled = eKilled->GetTeamTag();
	TeamTag& tagKiller = eKiller->GetTeamTag();

	// suicide!
	if (killer == killed)
	{
		bool waskilled = GameMode::PlayerKilled(killer, killed);
		if (waskilled)
		{
			--teams[tagKilled.teamIdx];
			CheckEndGame();
		}

		return waskilled;
	}

	// if the killer and killed are on the same team, kill the killed
	if (tagKilled.teamIdx == tagKiller.teamIdx)
	{
		bool waskilled = GameMode::PlayerKilled(killer, killed);
		if (waskilled)
		{
			--teams[tagKilled.teamIdx];
			CheckEndGame();
		}

		return waskilled;
	}

	// else we killed some one not on our team :D
	--teams[tagKilled.teamIdx];
	++teams[tagKiller.teamIdx];
	
	eKilled->SetColour(eKiller->GetColourIdx());

	tagKilled.SetTeam(tagKiller.teamIdx/*, tagKiller.colour*/);
	CheckEndGame();

	return false;
}

void ExplosiveTagTeamGame::CheckEndGame()
{
	if (ExplosiveGame::GetInstance().GetGameState() == ExplosiveGame::GameEndGame)
		return;

	// now check if there is only 1 team left
	int teamCount = 0;
	for (int i = 0; i < MAX_PLAYERS; ++i)
	{
		if (teams[i] > 0)
			++teamCount;
	}

	if (teamCount <= 1)
	{
		ExplosiveGame::GetInstance().SetEndRound();
	}
}

void ExplosiveTagTeamGame::AddPlayer(Character* player)
{
	ExplosiveCharacter* ePlayer = static_cast<ExplosiveCharacter*>(player);
	players[ePlayer->colourIdx] = true;
	teams[ePlayer->colourIdx] = 1;
	ePlayer->GetTeamTag().SetVisible(true);
}

void ExplosiveTagTeamGame::RemovePlayer(Character* player)
{
	ExplosiveCharacter* ePlayer = static_cast<ExplosiveCharacter*>(player);
	players[ePlayer->colourIdx] = false;
	--teams[ePlayer->GetTeamTag().teamIdx];
	CheckEndGame();
}