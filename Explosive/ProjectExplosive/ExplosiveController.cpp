#include "ExplosiveController.h"

#ifdef WIN32
	#include "../Engine/Win32Time.h"
	#include "../Engine/Win32Input.h"
#else
	#include "../Engine/LinuxTime.h"
	#include "../Engine/LinuxInput.h"
#endif

#include "../Engine/Client.h"

int ExplosiveInputController::inputControllerCount = 0;
int NetworkController::clientCount = 0;


ExplosiveInputController::ExplosiveInputController(Keyboard keys) : 	
	joystick(-1),
	clientId(-1)
{
	controllerId = inputControllerCount;
	++inputControllerCount;

	keyMap.SetSize(5);

	switch (keys)
	{
	default:
	case Right:
		keyMap[UP] = DIK_UP;
		keyMap[DOWN] = DIK_DOWN;
		keyMap[LEFT] = DIK_LEFT;
		keyMap[RIGHT] = DIK_RIGHT;
		keyMap[FIRE] = DIK_RCONTROL;
		break;
	case Left:
		keyMap[UP] = DIK_W;
		keyMap[DOWN] = DIK_S;
		keyMap[LEFT] = DIK_A;
		keyMap[RIGHT] = DIK_D;
		keyMap[FIRE] = DIK_LCONTROL;
		break;
	}
}

ExplosiveInputController::ExplosiveInputController(int joystickIdx) :
	clientId(-1)
{
	controllerId = inputControllerCount;
	++inputControllerCount;

	joystick = joystickIdx;
}
/*
ExplosiveInputController::ExplosiveInputController() : 	
	joystick(-1),
	clientId(-1)
{
	controllerId = inputControllerCount;
	++inputControllerCount;

	keyMap.SetSize(5);

	switch (controllerId)
	{
	case 0:
		keyMap[UP] = DIK_UP;
		keyMap[DOWN] = DIK_DOWN;
		keyMap[LEFT] = DIK_LEFT;
		keyMap[RIGHT] = DIK_RIGHT;
		keyMap[FIRE] = DIK_RCONTROL;
		break;
	case 1:
		keyMap[UP] = DIK_W;
		keyMap[DOWN] = DIK_S;
		keyMap[LEFT] = DIK_A;
		keyMap[RIGHT] = DIK_D;
		keyMap[FIRE] = DIK_LCONTROL;
		break;
	case 2:
		joystick = 0;
		break;
	}
}
*/
ExplosiveInputController::~ExplosiveInputController()
{
	--inputControllerCount;
}

void ExplosiveInputController::Update(Actor* object)
{
	// if we are hooked up to a network
	// send out key info
	if (clientId != -1)
	{
		// send client id
		Client::GetInstance().Send((const char*)&clientId, sizeof(int));

		// send key states
		bool state;
		for (int i = 0; i < 4; ++i)
		{
			state = KeyDown(ExplosiveController::Button(i));
			Client::GetInstance().Send((const char*)&state, sizeof(bool));
		}

		// and the bomb key
		state = KeyPressed(ExplosiveController::Button(4));
		Client::GetInstance().Send((const char*)&state, sizeof(bool));
	}
}

bool ExplosiveInputController::KeyDown(Button button)
{
	if (joystick != -1)
	{
		switch (button)
		{
		case UP:
			return Input::GetInstance().JoystickAxisState(joystick, Input::Y) < -20000;
		case DOWN:
			return Input::GetInstance().JoystickAxisState(joystick, Input::Y) > 20000;
		case LEFT:
			return Input::GetInstance().JoystickAxisState(joystick, Input::X) < -20000;
		case RIGHT:
			return Input::GetInstance().JoystickAxisState(joystick, Input::X) > 20000;
		}
	}
	return Input::GetInstance().KeyDown(keyMap[button]);
}

bool ExplosiveInputController::KeyPressed(Button button)
{
	if (joystick != -1)
	{
		switch (button)
		{
		case FIRE: 
			return Input::GetInstance().JoystickButtonPressed(joystick, 0);
		}
	}
	return Input::GetInstance().KeyPressed(keyMap[button]);
}

//*************************************************************

NetworkController::NetworkController(int clientId) : 
	clientId(clientId)
{
	for (int i = 0; i < 5; ++i)
		keys[i] = false;
}

bool NetworkController::KeyDown(Button button)
{
	return keys[button];
}

bool NetworkController::KeyPressed(Button button)
{
	return keys[button];
}

void NetworkController::Update(Actor* object)
{
	// update array bool keys[5];
}

int NetworkController::GetClientId()
{
	++clientCount;
	return clientCount - 1;
}
