#include "ExplosiveGame.h"
#include "ExplosiveCharacter.h"
#include "ExplosiveAIController.h"
#include "../Core/RenderMgr.h"
#include "../Core/NetServer.h"
#include "../Core/NetClient.h"
#include "../Network/RDP/RDPStreamMgr.h"

#define SETTINGS_INI "settings.ini"

MAIN(ExplosiveGame)

#define CHAT_TIME 5000
#define CAMERA_POSITION Vector3(0, 120, -60)

bool ExplosiveGame::Init(int argc, char **argv)
{
	// force it to run on a single thread
	SetProcessAffinityMask(GetCurrentThread(), 1);

	//natPunchCheckTime = 0;
	masterServerPing = 0;
	removeChatTimer = -1;

	fileName = argv[0];

	GameBase::Init(argc, argv);
	NetworkBase::InitSocket();

	Log::Print("Network Init		[OK]\n");

	debuggerAttached = true;

	setGameState = GameUnknown;
	disconnect = false;

	SetNetworkId(0);

	iniSettings.Load(SETTINGS_INI);
	cameraShakeTime = 0;

	int width = 1024;
	int height = 768;
	bool fullscreen = true;
	int aa = 2;

	if (iniSettings.Valid())
	{
		iniSettings.GetValue("video", "width", width);
		iniSettings.GetValue("video", "height", height);	
		iniSettings.GetValue("video", "fullscreen", fullscreen);
		iniSettings.GetValue("video", "antialiasing", aa);

		iniSettings.GetValue("gameoptions", "roundsPerMap", roundsPerMap);
		iniSettings.GetValue("gameoptions", "timeLimit", timeLimit);
		iniSettings.GetValue("gameoptions", "aiCanWin", AICanWin);
		iniSettings.GetValue("gameoptions", "aiInStats", AIInStats);

		serverName = iniSettings.GetValue("network", "servername");
		clientName = iniSettings.GetValue("network", "clientname");

		int temp = 0;
		//iniSettings.GetValue("network", "resendtime", temp);
		//gRDPStreamMgr.SetResendTime(temp);

		//iniSettings.GetValue("network", "resendpacketcount", temp);
		//gRDPStreamMgr.SetResendPacketCount(temp);
	}
	else
	{
		iniSettings.SetValue("network", "resendtime", 10);
		iniSettings.SetValue("network", "resendpacketcount", 3);

		iniSettings.SetValue("video", "width", width);
		iniSettings.SetValue("video", "height", height);	
		iniSettings.SetValue("video", "fullscreen", fullscreen);
		iniSettings.SetValue("video", "antialiasing", aa);

		SetServerName("Unnamed");
		SetClientName("Unnamed");
		SetRoundsPerMap(3);
		SetTimeLimit(10);
		SetAICanWin(true);
		SetAIInStats(true);
	}

	window = new Window();
#ifdef _DEMO
	window->Create("Explosive DEMO", width, height, 32, fullscreen, aa);
#else
	window->Create("Explosive", width, height, 32, fullscreen, aa);
#endif
	window->ShowWindow();

	Log::Print("Window Init		[OK]\n");

	Input::GetInstance().CreateInput(window);
	Log::Print("Input Init		[OK]\n");
	//Input::GetInstance().SetAltTabStyleMouse(true);

	Device::GetInstance().CreateDevice(window);
	Device::GetInstance().SetClearColour(0.0, 0.0, 0.0, 0.0);
	Log::Print("Device Init		[OK]\n");

	SoundMgr::Create();
	Log::Print("Sound Init		[OK]\n");

	RenderMgr::Create();
	ActorMgr::Create();
	ActorMgr::GetInstance().Init(4);

	ExplosiveEnvironmentMgr::Create();

	// set up game modes
	AddGameMode(&tagTeamGame);
	AddGameMode(&deathmatchGame);

	char buffer[256];
	std::string gameMode = iniSettings.GetValue("gameoptions", "gameMode");
	SetActiveGameMode(&deathmatchGame);
	for (int i = 0; i < GetNumGameModes(); ++i)
	{
		if (strcmp(gameMode.c_str(), GetGameMode(i)->GetName()) == 0)
			SetActiveGameMode(GetGameMode(i));
	}

	menu.Init(window);

	// set up rendermgr camera
	Camera* cam = RenderMgr::GetInstance().GetDefaultCamera();
	cam->SetFreeCam(false);
	cam->SetSpeed(0.5f);
	cam->SetLookAt(CAMERA_POSITION, Vector3(0,0,-1));
	cam->SetPerspective(20, 1.33, 500.0f, 10.0f);
	cameraOffset = Vector3(0.0f, 0.0f, 0.0f);

	actorDebug = false;
	iniSettings.GetValue("debug", "actors", actorDebug);

	fpsDebug = false;
	iniSettings.GetValue("debug", "fps", fpsDebug);

	cameraDebug = false;
	iniSettings.GetValue("debug", "camera", cameraDebug);
	if (cameraDebug)
	{
		cam->SetFreeCam(true);
		cam->SetPosition(0, 0, 0);
		cam->SetPerspective(60, 1.33, 500.0f, 1.0f);
	}

	debugCharacter = false;
	iniSettings.GetValue("debug", "character", debugCharacter);

	serverName = "Unknown";

	MD5Check();

	ExplosiveCharacter::PreCache();
	Bomb::PreCache();

	Log::Print("Game Init		[OK]\n");
	return true;
}

void ExplosiveGame::SetActiveGameMode(GameMode* gameMode)
{
	GameMgr<ExplosiveGame>::SetActiveGameMode(gameMode);
	iniSettings.SetValue("gameoptions", "gameMode", gameMode->GetName());
}

void ExplosiveGame::SetAICanWin(bool value) 
{ 
	iniSettings.SetValue("gameoptions", "aiCanWin", value);
	AICanWin = value; 
}

void ExplosiveGame::SetRoundsPerMap(int rounds) 
{ 
	iniSettings.SetValue("gameoptions", "roundsPerMap", rounds);
	roundsPerMap = rounds; 
}

void ExplosiveGame::SetTimeLimit(int time)
{
	iniSettings.SetValue("gameoptions", "timeLimit", time);
	timeLimit = time; 
}

void ExplosiveGame::SetAIInStats(bool value) 
{ 
	iniSettings.SetValue("gameoptions", "aiInStats", value);
	AIInStats = value; 
}

void ExplosiveGame::SetServerName(const char* serverName)
{
	iniSettings.SetValue("network", "servername", serverName);
	this->serverName = serverName;
}

const char* ExplosiveGame::GetServerName()
{
	return serverName.c_str();
}

void ExplosiveGame::SetClientName(const char* clientName)
{
	iniSettings.SetValue("network", "clientname", clientName);
	this->clientName = clientName;
}

const char* ExplosiveGame::GetClientName()
{
	return clientName.c_str();
}

void ExplosiveGame::Shutdown()
{
	iniSettings.Save(SETTINGS_INI);
	Input::Destroy();
	SoundMgr::Destroy();
	ExplosiveEnvironmentMgr::Destroy();			
	ActorMgr::Destroy();
	Device::Destroy();
	RenderMgr::Destroy();
	Mesh::DeleteResources();
	NetworkBase::ShutdownSocket();
}

ExplosiveCharacter* ExplosiveGame::AddAICharacter(int idx, int characterId)
{
	Controller* controller = new ExplosiveAIController();

	ExplosiveCharacter* character = new ExplosiveCharacter(idx, controller, "character.mdl", characterId);
	ActorMgr::GetInstance().AddActor(character, 0);

	if (NetServer::IsValid())
		character->SetOwnerClientId(NetServer::GetInstance().GetClientId());
	else if (NetClient::IsValid())
		character->SetOwnerClientId(NetClient::GetInstance().GetClientId());

	if (character)
		AddCharacter(character);

	return character;
}

ExplosiveCharacter* ExplosiveGame::AddNetworkCharacter(int idx, int characterId)
{
	Controller* controller = new NetworkController(0);

	ExplosiveCharacter* character = new ExplosiveCharacter(idx, controller, "character.mdl", characterId);
	ActorMgr::GetInstance().AddActor(character, 0);

	if (character)
		AddCharacter(character);

	return character;
}

ExplosiveCharacter* ExplosiveGame::AddHumanCharacter(int idx, int joystickIdx, int characterId)
{
	Controller* controller = new ExplosiveInputController(joystickIdx);

	ExplosiveCharacter* character = new ExplosiveCharacter(idx, controller, "character.mdl", characterId);
	ActorMgr::GetInstance().AddActor(character, 0);

	if (NetServer::IsValid())
		character->SetOwnerClientId(NetServer::GetInstance().GetClientId());
	else if (NetClient::IsValid())
		character->SetOwnerClientId(NetClient::GetInstance().GetClientId());

	if (character)
		AddCharacter(character);

	return character;
}

ExplosiveCharacter* ExplosiveGame::AddHumanCharacter(int idx, ExplosiveInputController::Keyboard keys, int characterId)
{
	Controller* controller = new ExplosiveInputController(keys);

	ExplosiveCharacter* character = new ExplosiveCharacter(idx, controller, "character.mdl", characterId);
	ActorMgr::GetInstance().AddActor(character, 0);

	if (NetServer::IsValid())
		character->SetOwnerClientId(NetServer::GetInstance().GetClientId());
	else if (NetClient::IsValid())
		character->SetOwnerClientId(NetClient::GetInstance().GetClientId());

	if (character)
		AddCharacter(character);

	return character;
}

void ExplosiveGame::AddCharacter(Character* character)
{
	GameMgr::AddCharacter(character);
	GetCurrentGameMode()->AddPlayer(character);
}

void ExplosiveGame::RemoveCharacter(Character* character)
{
	// remove from the array or characters
	GameMgr::RemoveCharacter(character);
	GetCurrentGameMode()->RemovePlayer(character);

	delete character->GetController();
	delete character;
}

void ExplosiveGame::DebugDraw()
{
	RenderMgr::GetInstance().GetDefaultCamera()->LookAt();

	for (int i = 0; i < players.GetSize(); ++i)
		players[i]->DebugDraw();
}

void ExplosiveGame::Start()
{
	SetGameState(GamePlaying);
	RenderMgr::GetInstance().SetPauseTime(false);
	RegisterCallbacks();
	masterServerPing = gTime.GetTicks();

	roundsRemaning = GetRoundsPerMap();
	timeRemaning = gTime.GetTicks() + GetTimeLimit() * 60 * 1000;

	menu.enmChatBox.RemoveAllElements();

	//if (NetServer::IsValid())
	//	GetMasterServer().Listen(CONNECTION_PORT);// - 1);
}

void ExplosiveGame::RegisterCallbacks()
{
	RegisterWithNetwork(false, false);
	ExplosiveEnvironmentMgr::GetInstance().Register();

	if (NetServer::IsValid())
		NetServer::GetInstance().SetCallback(this);

	if (NetClient::IsValid())
	{
		NetClient::GetInstance().SetCallback(this);
		//AddServerToMaster();
	}
}

void ExplosiveGame::PreRestartPlayers()
{
	// restart player
	for (int i = 0; i < players.GetSize(); ++i)
		static_cast<ExplosiveCharacter*>(players[i])->PreReset();
}

void ExplosiveGame::Restart()
{
	SetGameState(GamePlaying);
	GetCurrentGameMode()->Reset();
 
	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	// server will send this info
	if (!NetClient::IsValid())
		ExplosiveEnvironmentMgr::GetInstance().LoadMap(NULL); // null reloads existin level

	// wait for the server to send us map info
	if (NetClient::IsValid())
	{
		while (!ExplosiveEnvironmentMgr::GetInstance().Valid())
		{
			//cout << "wating for server" << endl;
			NetClient::GetInstance().Update();
		}
	}

	// Looks like some stuff was sneaking in to the level from the previous level! evil!
	ExplosiveEnvironmentMgr::GetInstance().ClearAllTiles();

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	
	//cout << "we got it from server" << endl;
	Array<Actor*>& actorGroup = ActorMgr::GetInstance().GetActorGroup(1);
	while (actorGroup.GetSize())
	{
		Actor* a = actorGroup[0];
		Bomb* b = static_cast<Bomb*>(a);
		//b->MoveToTile(0);
		actorGroup.Remove(a);
		delete a;
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	Array<Actor*>& actorGroup2 = ActorMgr::GetInstance().GetActorGroup(2);
	while (actorGroup2.GetSize())
	{
		Actor* a = actorGroup2[0];
		actorGroup2.Remove(a);
		delete a;
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	for (int i = 0; i < players.GetSize(); ++i)
		players[i]->Reset();
	
	RenderMgr::GetInstance().SetPauseTime(false);
//	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
}

void ExplosiveGame::Pause()
{
	menu.DisplayMenu(Menu::Pause);
	SetGameState(GamePaused);
	RenderMgr::GetInstance().SetPauseTime(true);
}

void ExplosiveGame::EndRound()
{
	menu.DisplayMenu(Menu::Victory);
	SetGameState(GameEndRound);
	RenderMgr::GetInstance().SetPauseTime(true);
}

void ExplosiveGame::Resume()
{
	SetGameState(GamePlaying);
	RenderMgr::GetInstance().SetPauseTime(false);
}

void ExplosiveGame::SetEndGame()
{
	setGameState = GameEndGame;

	int data[2];
	data[0] = NM_SetGameState;
	data[1] = setGameState;
	Send(data, sizeof(int) * 2);
}

void ExplosiveGame::SetRestart(bool viaNetwork)
{
	timeRemaning = gTime.GetTicks() + GetTimeLimit() * 60 * 1000;

	// change the level if needed, and reset the time remaning
	// single player or we are a server
	if (!NetClient::IsValid())
	{
		--roundsRemaning;
		if (roundsRemaning <= 0)
		{
			int idx = menu.enmLevels.GetSelectedIndex() + 1;
			if (idx >= menu.enmLevels.GetElementCount())
				idx = 0;

			menu.enmLevels.SelectIndex(idx);
			const char* level = menu.enmLevels.GetElement(idx);
/*
			// notify clients
			if (NetServer::IsValid())
			{
				bool blocking = IsBlocking();
				if (blocking)
					BlockSendEnd();

				int len = strlen(level) + 1;
				char* buffer = new char[sizeof(int) * 2 + len];
				int* data = (int*)buffer;
				data[0] = NM_SetLevel;
				data[1] = len;
				memcpy(buffer + sizeof(int) * 2, level, len); 

				Send(data, sizeof(int) * 2 + len);
				delete buffer;

				if (blocking)
					BlockSendBegin();
			}
*/
			ExplosiveEnvironmentMgr::GetInstance().LoadMap(level);

			roundsRemaning = GetRoundsPerMap();
		}
	}

	// if called via network, or we are playing single player
	if (viaNetwork || !NetClient::IsValid())
	{
		menu.DisplayMenu(Menu::None);
		setGameState = GameRestart;
	}

	ExplosiveEnvironmentMgr::GetInstance().Invalidate();

	// if im a client and the client clicked the button,
	// notify server only
	if (!viaNetwork && NetClient::IsValid())
	{
		int data[2];
		data[0] = NM_SetGameState;
		data[1] = GameRestart;
		Send(data, sizeof(int) * 2, NetExtension::Server);
	}

	// so if we are the server, notify the clients
	if (NetServer::IsValid())
	{	
		bool blocking = IsBlocking();
		if (blocking)
			BlockSendEnd();

		int data[2];
		data[0] = NM_SetGameState;
		data[1] = GameRestart;
		Send(data, sizeof(int) * 2);

		if (blocking)
			BlockSendBegin();
	}
}

void ExplosiveGame::SetResume(bool viaNetwork)
{
	// if called via network, or we are playing single player
	if (viaNetwork || !NetClient::IsValid())
	{
		menu.DisplayMenu(Menu::None);
		setGameState = GameResume;
	}

	// if im a client and the client clicked the button,
	// notify server only
	if (!viaNetwork && NetClient::IsValid())
	{
		int data[2];
		data[0] = NM_SetGameState;
		data[1] = GameResume;
		Send(data, sizeof(int) * 2, NetExtension::Server);
	}
	
	// so if we are the server, notify the clients
	if (NetServer::IsValid())
	{	
		BlockSendEnd();

		int data[2];
		data[0] = NM_SetGameState;
		data[1] = GameResume;
		Send(data, sizeof(int) * 2);

		BlockSendBegin();
	}
}

void ExplosiveGame::SetEndRound()
{
	setGameState = GameEndRound;

	int data[2];
	data[0] = NM_SetGameState;
	data[1] = setGameState;
	Send(data, sizeof(int) * 2);
}

void ExplosiveGame::EndGame()
{
	menu.DisplayMenu(Menu::Main);
	SetGameState(GameEndGame);

	// delete players
	while (players.GetSize())
		RemoveCharacter(players[0]);

	ActorMgr::GetInstance().GetActorGroup(0).SetSize(0);
	
	ExplosiveEnvironmentMgr::GetInstance().UnloadMap(false);

	Array<Actor*>& actorGroup = ActorMgr::GetInstance().GetActorGroup(1);
	while (actorGroup.GetSize())
	{
		Actor* a = actorGroup[0];
		Bomb* b = static_cast<Bomb*>(a);
		//b->MoveToTile(0);
		actorGroup.Remove(a);
		delete a;
	}

	Array<Actor*>& actorGroup2 = ActorMgr::GetInstance().GetActorGroup(2);
	while (actorGroup2.GetSize())
	{
		Actor* a = actorGroup2[0];
		actorGroup2.Remove(a);
		delete a;
	}

	
	RenderMgr::GetInstance().SetPauseTime(true);
}

void ExplosiveGame::DoCameraShake(float camShake, int time)
{
	Camera* cam = RenderMgr::GetInstance().GetDefaultCamera();
	cameraShakeTime += time;
	cameraShake = camShake;
}

void ExplosiveGame::UpdateCameraShake()
{
	if (cameraDebug)
		return;

	Camera* cam = RenderMgr::GetInstance().GetDefaultCamera();

	if (cameraShakeTime <= 0)
	{
		cam->SetPosition(CAMERA_POSITION);
		cameraOffset = Vector3(0.0f, 0.0f, 0.0f);
		return;
	}

	// create a random velocity
	cameraOffset = Vector3(float(rand() % 1000) / 1000.0f, float(rand() % 1000) / 1000.0f, float(rand() % 1000) / 1000.0f) * cameraShake;
	cam->SetPosition(CAMERA_POSITION + cameraOffset);
	cameraShake *= 0.9f;

	--cameraShakeTime;
}

bool ExplosiveGame::UpdateGame()
{
	//DebuggerCheck1();
	//DebuggerCheck2();

	Input::GetInstance().Update();
	RenderMgr::GetInstance().Update();
	gSoundMgr.Update();

	CheckDisconnect();

	if (gInput.KeyPressed(DIK_F1))
		gDevice.SaveScreenShot();

	// paused so adjust time ramaning
	if (GetGameState() == GamePaused)
	{
		timeRemaning += gTime.GetTicks() - lastTime;
	}
	lastTime = gTime.GetTicks();

	// round is over!
	if (gTime.GetTicks() > timeRemaning && GetGameState() == GamePlaying && !NetClient::IsValid())
	{
		SetEndRound();
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	// update network stuff
	if (NetServer::IsValid())
	{/*
		if (gTime.GetTicks() > natPunchCheckTime)
		{
			GetMasterServer().CheckNATPunchThrough(NetServer::GetInstance().GetRDPStream(), CLIENT_PORT);
			natPunchCheckTime = gTime.GetTicks() + NAT_PUNCH_CHECK;
		}*/

		//GetMasterServer().UpdateNATPunchThrough();

		// every hour, ping the master server
		if (gTime.GetTicks() > masterServerPing)
		{
			Log::Print("Pinging master server\n");
			AddServerToMaster();
			masterServerPing = gTime.GetTicks() + MASTER_SERVER_PING;			
		}

		NetServer::GetInstance().SetNumPlayers(players.GetSize());
		NetServer::GetInstance().Update();
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
	if (NetClient::IsValid())
	{
		NetClient::GetInstance().Update();
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
	
	// while in the menu screens, flush unreliable packets
	if (GetGameState() != GamePlaying)
	{
		if (NetClient::IsValid())
			NetClient::GetInstance().FlushUnreliable();
		else if (NetServer::IsValid())
			NetServer::GetInstance().FlushUnreliable();
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	switch(setGameState)
	{
	case GameEndGame:
		{
			// progress game foward a few frames
			for (int i = 0; i < 4; ++i)
				Update();

			EndGame();
			ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
		}
		break;
	case GameRestart:
		{
			Restart();
			ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
		}
		break;
	case GameResume:
		{
			Resume();
			ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
		}
		break;
	case GameEndRound:
		{
			// progress game foward a few frames
			for (int i = 0; i < 4; ++i)
				Update();
		
			EndRound();
			ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
		}
		break;
	}
	setGameState = GameUnknown;

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	// incase the window is resized
	Device::GetInstance().SetActiveWindow(window);

	if (window->HandleMessages())
	{
		if (menu.ShouldExit())
			return false;

		//return false;

		ExplosiveGame::GetInstance().GetMenu().DisplayMenu(Menu::Pause);
		ExplosiveGame::GetInstance().GetMenu().OnMousePressed(ExplosiveGame::GetInstance().GetMenu().btnExit);

		ExplosiveGame::GetInstance().GetMenu().DisplayMenu(Menu::Main);
		ExplosiveGame::GetInstance().GetMenu().OnMousePressed(ExplosiveGame::GetInstance().GetMenu().btnExit);

		disconnect = false;
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	if (menu.MenuDisplayed())
	{
		menu.Update();
		
		if (menu.ShouldExit() || (menu.visibleMenu == Menu::Main && Input::GetInstance().KeyPressed(DIK_ESCAPE)))
			return false;

		if (NetClient::IsValid() || NetServer::IsValid())
			Update(false);
	}
	else
	{
		Update();

		if (Input::GetInstance().KeyPressed(DIK_ESCAPE))
		{	
			Pause();
		}
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
	return true;
}

void ExplosiveGame::Update(bool updateActors)
{
	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	if ((removeChatTimer + CHAT_TIME) < Time::GetInstance().GetTicks())
	{
		removeChatTimer = Time::GetInstance().GetTicks() + CHAT_TIME;

		if (menu.enmChatBox.GetSize() > 1)
			menu.enmChatBox.RemoveElement(0);
	}

	if (GetGameState() == GameNotStarted)
		return;

	if (GetGameState() == GamePaused && !NetServer::IsValid() && !NetClient::IsValid())
		return;

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	if (updateActors)
	{
		ExplosiveCharacter::UpdateSkin();
		ActorMgr::GetInstance().Update();
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	if ((NetClient::IsValid() || NetServer::IsValid())
		&& (!menu.ipChat->HasInputFocus() && gInput.KeyPressed(Input::KEY_Enter)))
		menu.ipChat->SetInputFocus(true);

	if (menu.ipChat->HasInputFocus())
		menu.menuChat.Update(&menu.cursor);

	menu.menuChatBox.Update(&menu.cursor);

	UpdateCameraShake();

	if (!(GetGameState() == GameEndGame || GetGameState() == GameEndRound))
	{
		// draw time remaning
		unsigned int secondsRemaning = (timeRemaning - gTime.GetTicks()) / 1000;
		unsigned int minutes = secondsRemaning / 60;
		unsigned int seconds = secondsRemaning - (minutes * 60);
		char timeBuffer[256];
		sprintf(timeBuffer, minutes < 10 ? "0%i:" : "%i:", minutes);
		sprintf(timeBuffer + strlen(timeBuffer), seconds < 10 ? "0%i" : "%i", seconds);
		RenderMgr::GetInstance().GetFont()->Print(400.f, 1.0f, 0.5f, timeBuffer);
		SentTimeRemaning();
	}

	if (fpsDebug)
	{
		unsigned int fps = Device::GetInstance().GetFPS();
		unsigned int polys = RenderMgr::GetInstance().GetPolyCount();
		RenderMgr::GetInstance().GetFont()->Print(0.1f, 0.1f, 0.4f, "FPS: %i\nPolys: %i", fps, polys);
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
}

void ExplosiveGame::Render()
{
	Device::GetInstance().BeginRender();
	
	if (debugCharacter)
	{
		for (int i = 0; i < players.GetSize(); ++i)
		{
			ExplosiveCharacter* character = static_cast<ExplosiveCharacter*>(players[i]);
			character->DebugDraw();
		}
	}

	// render if paused, or not in the menu
	if (!menu.MenuDisplayed() || GetGameState() == GamePaused || GetGameState() == GameEndRound)
	{
		RenderMgr::GetInstance().Render();

		if (menu.ipChat->HasInputFocus())
			menu.menuChat.Render();

		menu.menuChatBox.Render();

		if (actorDebug)
			DebugDraw();
	}

	if (menu.MenuDisplayed())
	{
		menu.Render();	
	}

	Device::GetInstance().EndRender();
}

void ExplosiveGame::ReceiveMessage(const void* message, int length, NetExtension::NetMessageTarget target)
{
	BlockSendBegin();

	if (FilterTarget(target))
		return;

	int msg = *(const int*)message;

	switch (msg)
	{
	case NM_SetGameState:
		{
			int state = *((const int*)message + 1);

			switch(state)
			{
			case GameNotStarted:
				break;
			case GamePaused:
				break;
			case GamePlaying:
				break;
			case GameEndGame:
				SetEndGame();
				break;
			case GameRestart:
				SetRestart(true);
				break;
			case GameResume:
				SetResume(true);
				break;
			case GameEndRound:
				SetEndRound();
				break;
			case GameUnknown:
				break;
			};
		}
		break;
	case NM_AddCharacterSelf:
		{
			//cout << "NM_AddCharacterSelf" << endl;
			const int* data = (const int*)message + 1;
			menu.AddPlayerFromButton(data[1], menu.lbpControls.button, data[0]);
			//ExplosiveGame::GetInstance().AddHumanCharacter(data[1], ExplosiveInputController::Right, data[0]);
		}
		break;
	case NM_AddCharacterNetwork:
		{
			//cout << "NM_AddCharacterNetwork" << endl;
			const int* data = (const int*)message + 1;
			ExplosiveCharacter* character = ExplosiveGame::GetInstance().AddNetworkCharacter(data[1], data[0]);
			if (data[2])
				character->Kill();

			character->GetTeamTag().teamIdx = data[3];
			character->SetColour(data[4]);
		}
		break;
	case NM_SetGameMode:
		{
			const int* data = (const int*)message + 1;
			SetActiveGameMode(GetGameMode(data[0]));
		}
		break;
	case NM_Bomb:
		{
			const int* data = (const int*)message + 1;
			int ownerIdx = data[0];
			ExplosiveCharacter* owner = ownerIdx == -1 ? 0 : static_cast<ExplosiveCharacter*>(players[ownerIdx]);

			Bomb* bomb = new Bomb(owner, ExplosiveEnvironmentMgr::GetInstance().GetTile(data[3], data[4]), data[1], data[8], data[7], false);
			bomb->xDirection = data[5];
			bomb->zDirection = data[6];
			bomb->exploded = data[2];
			ActorMgr::GetInstance().AddActor(static_cast<Actor*>(bomb), 1); // bombs go in to group 1
		}
		break;
	case NM_RemoveCharacterNetwork:
		{
			const int* data = (const int*)message + 1;
			DeleteClient(data[0]);
		}
		break;
	case NM_Chat:
		{
			const int* data = (const int*)message + 1;
			int length = data[0];
			++data;
			char text[2048];
			memcpy(text, data, length);
			menu.enmChatBox.AddElement(text);
			menu.enmChatBox.SelectLastIndex();

			if (removeChatTimer == -1)
				removeChatTimer = 0;

			if (menu.enmChatBox.GetSelectedIndex() > 5)
				menu.enmChatBox.RemoveElement(0);
		}
		break;
	case NM_Disconnect:
		{
			// a client has disconnected
			const int* data = (const int*)message + 1;

			// if server has closed down....
			if (*data == -1)
			{
				disconnect = true;
				//menu.OnMousePressed(menu.btnExit);
			}

			if (NetServer::IsValid())
			{
				// disconenct the appropriate player
				int i = 0;
				bool found = false;
				for (; i < NetServer::GetInstance().GetNumClients(); ++i)
				{
					if (NetServer::GetInstance().GetClientConnection(i).clientId == *data)
					{
						found = true;
						break;
					}
				}

				if (found)
				{
					// DO NOTE DLEETE THE CLIENT HERE AS WE ARE UPDATING THIS CLIENT!
					Connection& client = NetServer::GetInstance().GetClientConnection(i);
					client.stream.Close();
					//NetServer::GetInstance().ClientDisconnect(i, client);
				}
			}
		}
		break;

	case NM_TimeRemaning:
		{
			const int* data = (const int*)message + 1;
			unsigned long realTimeRemaining = (unsigned long)*data;
			timeRemaning = gTime.GetTicks() + realTimeRemaining;
		}
		break;
		/*
	case NM_SetLevel:
		{
			const int* data = (const int*)message + 1;
			int len = data[0];
			char* level = new char[len];
			memcpy(level, data + sizeof(int), len);

			// map failed to load, so disconnect
			if (!ExplosiveEnvironmentMgr::GetInstance().LoadMap(level))
				menu.OnMousePressed(menu.btnExit);

			delete[] level;
		}
		break;*/
	}

	BlockSendEnd();
}

void ExplosiveGame::SentTimeRemaning()
{
	if (!NetServer::IsValid())
		return;

	int data[8];
	data[0] = NM_TimeRemaning;
	unsigned long realTimeRemaining = timeRemaning - gTime.GetTicks();
	memcpy(&data[1], &realTimeRemaining, sizeof(unsigned long));
	Send(data, sizeof(int) + sizeof(unsigned long), NetExtension::All, SocketStream::UnReliable);
}

void ExplosiveGame::AcceptConnection(int index, Connection& client)
{/*
	int request = -1;
	while (NetServer::GetInstance().Receive(index, &request, sizeof(int)) != sizeof(int))
	{
	}

	if (request == NM_ServerInfo)
	{
		// any server info here
		client.reliable.Close();
	}
*/
	// can only have 8 clients
	if (players.GetSize() >= MAX_PLAYERS)
	{
		client.reliable.Close();
		return;
	}

	// send the map specific stuff
	ExplosiveEnvironmentMgr::GetInstance().AcceptConnection(index, client);

	// send the client the game mode
	int gameMode = 0;
	for (int i = 0; i < GetNumGameModes(); ++i)
	{
		if (GetGameMode(i) == GetCurrentGameMode())
		{
			gameMode = i;
			break;
		}
	}
	int gameModeData[2] = { NM_SetGameMode, gameMode };
	SendToClient(gameModeData, sizeof(int) * 2, index);
	

	// when a client connects, we tell them what characters to make :D
	// this way all clients have a list of characters in the same order!

	int characterIds[MAX_PLAYERS];
	int colourIds[MAX_PLAYERS];

	for (int i = 0; i < MAX_PLAYERS; ++i)
	{
		characterIds[i] = false;
		colourIds[i] = false;
	}

	for (int i = 0; i < players.GetSize(); ++i)
	{
		ExplosiveCharacter* character = static_cast<ExplosiveCharacter*>(players[i]);

		int data[6];
		data[0] = NM_AddCharacterNetwork;
		data[1] = character->GetCharacterId();
		characterIds[character->GetCharacterId()] = true;
		data[2] = character->GetColourIdx();
		colourIds[character->GetColourIdx()] = true;
		data[3] = character->IsDead();
		data[4] = character->GetTeamTag().teamIdx;
		data[5] = character->GetTeamColourIdx();
		SendToClient(data, sizeof(int) * 6, index);
		//NetServer::GetInstance().SendNetObjectMessage(index, GetNetworkId(), data, sizeof(int) * 4);
	}

	// create a new character
	int data[3];
	data[0] = NM_AddCharacterSelf;

	// get a id for the player
	int i = 0;
	for (; i < MAX_PLAYERS; ++i)
	{
		if (characterIds[i] == false)
		{
			data[1] = i;
			break;
		}
	}

	// colour for player
	int j = 0;
	for (; j < MAX_PLAYERS; ++j)
	{
		if (colourIds[j] == false)
		{
			data[2] = j;
			break;
		}
	}

	SendToClient(data, sizeof(int) * 3, index);
	
	// create a new player on the server
	ExplosiveCharacter* e = AddNetworkCharacter(j, i);
	e->SetOwnerClientId(client.clientId);

	// notify all other players that to create a new character
	int dataNewChar[6];
	dataNewChar[0] = NM_AddCharacterNetwork;
	dataNewChar[1] = e->GetCharacterId();
	characterIds[e->GetCharacterId()] = true;
	dataNewChar[2] = e->GetColourIdx();
	colourIds[e->GetColourIdx()] = true;
	dataNewChar[3] = e->IsDead();
	dataNewChar[4] = e->GetTeamTag().teamIdx;
	dataNewChar[5] = e->GetTeamColourIdx();
	SendToAllButClient(dataNewChar, sizeof(int) * 6, index);

	// send bombs
	Array<Actor*>& bombs = ActorMgr::GetInstance().GetActorGroup(1);
	for (int i = 0; i < bombs.GetSize(); ++i)
	{
		Bomb* bomb = static_cast<Bomb*>(bombs[i]);

		int data[10];
		data[0] = NM_Bomb;

		int ownerIdx = -1;
		for (int i = 0; i < players.GetSize(); ++i)
		{
			ExplosiveCharacter* character = static_cast<ExplosiveCharacter*>(players[i]);
			if (character == bomb->owner)
			{
				ownerIdx = i;
				break;
			}
		}

		data[1] = ownerIdx;
		data[2] = bomb->life;
		data[3] = bomb->exploded;
		data[4] = bomb->tile->GetX();
		data[5] = bomb->tile->GetZ();
		data[6] = bomb->xDirection;
		data[7] = bomb->zDirection;
		data[8] = bomb->GetNetworkId();
		data[9] = bomb->radius;

		SendToClient(data, sizeof(int) * 10, index);
	}

	// send game state
	int gameState[2];
	gameState[0] = NM_SetGameState;
	gameState[1] = GetGameState();
	SendToClient(gameState, sizeof(int) * 2, index);

	// send player positions
	for (int i = 0; i < players.GetSize(); ++i)
	{
		ExplosiveCharacter* character = static_cast<ExplosiveCharacter*>(players[i]);
		character->SendPosition(true);
	}

	//AddServerToMaster();
}

void ExplosiveGame::AcceptConnection(int flags)
{/*
	int request = NM_Join;
	NetClient::GetInstance().Send(request);*/
}

void ExplosiveGame::ClientDisconnect(int index, Connection& client)
{
	for (int i = 0; i < players.GetSize(); ++i)
	{
		ExplosiveCharacter* e = static_cast<ExplosiveCharacter*>(players[i]);
		if (e->GetOwnerClientId() == client.clientId)
		{
			DeleteClient(i);
			int data[2] = { NM_RemoveCharacterNetwork, i };
			Send(data, sizeof(int) * 2);
			return;
		}
	}
	AddServerToMaster();
}

void ExplosiveGame::DeleteClient(int index)
{
	Array<Actor*>& characters = ActorMgr::GetInstance().GetActorGroup(0);
	characters.Remove(players[index]);
	//if (index != (characters.GetSize() - 1))
	//	characters[index] = characters[characters.GetSize() - 1];
	//--characters.size;
	RemoveCharacter(players[index]);
}

void ExplosiveGame::ClientDisconnect()
{
	disconnect = true;
}

void ExplosiveGame::Disconnect()
{
	// spam a few disconnect packets
	int data[2] = { NM_Disconnect, NetServer::IsValid() ? -1 : NetClient::GetInstance().GetClientId() };
	
	for (int i = 0; i < 10; ++i)
		Send(data, sizeof(int) * 2, NetServer::IsValid() ? NetExtension::All : NetExtension::Server, SocketStream::UnReliable);
}

void ExplosiveGame::CheckDisconnect()
{
	if (!disconnect)
		return;

	if (!NetClient::IsValid())
		return;

	// go back to main menu
	// when the client disconnects
	// emulate exit button pressed
	menu.DisplayMenu(Menu::Pause);
	menu.OnMousePressed(menu.btnExit);
	disconnect = false;
}

void ExplosiveGame::AddServerToMaster()
{	
	masterServer.Connect(MASTER_SERVER_URL, MASTER_SERVER_PATH, 80, EXPLOSIVE_GAME_ID);
	masterServer.AddServer(GetServerName(), players.GetSize());
}

void ExplosiveGame::RemoveServerFromMaster()
{
	masterServer.Connect(MASTER_SERVER_URL, MASTER_SERVER_PATH, 80, EXPLOSIVE_GAME_ID);
	masterServer.RemoveServer();
	masterServer.StopNATPunchThroughCheck();
}

void ExplosiveGame::SendChat(const char* msg)
{
	char strColour[256] = "Unknown";
	for (int i = 0; i < players.GetSize(); ++i)
	{
		ExplosiveCharacter* eChar = static_cast<ExplosiveCharacter*>(players[i]);
		ExplosiveController* expControl = static_cast<ExplosiveController*>(eChar->GetController());
		if (expControl->IsLocalHuman())
		{
			strcpy(strColour, eChar->GetName());
			break;
		}
	}

	char strBuffer[2048];
	sprintf(strBuffer, "%s [%s]: %s", clientName.c_str(), strColour, msg);

	char buffer[2048];
	int netMsg[2] = { NM_Chat, sizeof(char) * (strlen(strBuffer) + 1)};
	memcpy(buffer, &netMsg, sizeof(int) * 2);
	memcpy(buffer + sizeof(int) * 2, strBuffer, sizeof(char) * (strlen(strBuffer) + 1));
	Send(buffer, sizeof(char) * (strlen(strBuffer) + 1) + sizeof(int) * 2);
	menu.enmChatBox.AddElement(strBuffer);
	menu.enmChatBox.SelectLastIndex();

	if (removeChatTimer == -1)
		removeChatTimer = 0;

	if (menu.enmChatBox.GetSelectedIndex() > 5)
		menu.enmChatBox.RemoveElement(0);
}
