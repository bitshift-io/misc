#ifndef _SIPHON_EXPLOSIVECHARACTER_
#define _SIPHON_EXPLOSIVECHARACTER_

#include "../Core/Character.h"
#include "../Core/Modifier.h"
#include "../Core/NetObject.h"
#include "TileEnvironmentMgr.h"
#include "ExplosiveController.h"

#include "../Engine/Sound.h"
#include "../Engine/Animation.h"

class Bomb;
class ExplosivePickup;
using namespace Siphon;

#define PICKUP_COUNT 10
#define PLAYER_SPEED 0.4f

class Statistics
{
public:
	Statistics() : wins(0), kills(0), numPickups(0), longestExplosion(0), mostBombs(0), deaths(0),
		suicides(0), buttonsPressed(0) { }

	void AddKill()	{ ++kills; }
	int GetKills() { return kills; }

	void AddPickup() { ++numPickups; }
	int GetPickups() { return numPickups; }

	void AddDeath() { ++deaths; }
	int GetDeaths() { return deaths; }

	void AddButtonPress() { ++buttonsPressed; }
	int GetButtonPress() { return buttonsPressed; }

	void AddSuicide() { ++suicides; }
	int GetSuicides() { return suicides; }

	void AddWin() { ++wins; }
	int GetWins() { return wins; }

protected:
	int wins;
	int kills;
	int numPickups;
	int longestExplosion;
	int mostBombs;
	int deaths;
	int suicides;
	int buttonsPressed;
};
/*
class ExplosiveCharacterNetType : public NetType
{
public:
	ExplosiveCharacterNetType() : NetType() { }
	virtual NetObject* Create(unsigned int ownerClientId, unsigned int networkId, unsigned int size, char* data);

	static ExplosiveCharacterNetType instance;
};
*/

//
// the tag above a players head in team games
//
class TeamTag : public Mesh::Callback
{
public:
	TeamTag();
	~TeamTag();

	void SetTeam(int teamIdx/*, const Vector3& colour*/);
	void SetVisible(bool visible);

	void SetPosition(const Vector3& position);
	virtual void PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format);

	void PreReset();

	bool		visible;
	int			teamIdx;
	Vector3		colour;
	Mesh*		mesh;
	CGparameter colourParam;
	CGparameter alphaParam;
	bool		inserted;
};

class ExplosiveCharacter : public Character, public Mesh::Callback, public NetObject
{
	friend class PickUp;
	friend class BombExpand;
	friend class BombCount;
	friend class ExplosiveAIController;

	friend class SnailModifier;

public:

	enum NetworkMessage
	{
		NM_Position,
		//NM_TileInfo,
		NM_DropBomb,
		NM_Kill,
		NM_Teleport,
		NM_DropPickup,
		NM_DestroyShield,
	};

	TYPE(3);

	ExplosiveCharacter(int colourIdx, Controller* controller, const char* skinFile, int characterId = -1);
	virtual ~ExplosiveCharacter();

	virtual unsigned int GetConstructorDataSize();
	virtual void GetConstructorData(char* data);

	static const char* GetPlayerName(int idx);

	virtual void CheckInput();
	bool Move(ExplosiveController::Button direction);

	void DropPickups();
	void DropPickup(int type, int x, int z);

	virtual bool Update();
	void UpdateNetwork();

	void BombReceiveNetworkId(unsigned int networkId, Bomb* bomb);

	void DropBomb();
	void DestroyBomb(Bomb* bomb);

	Tile* GetLocation() { return currentTile; }
	void SetLocation(Tile* t, bool fromNetwork = false);

	virtual void Reset();
	void PreReset();

	virtual void Respawn() {}

	virtual void DebugDraw();

	void Kill();
	virtual bool Kill(Actor* actor);

	static void UpdateSkin();

	void NotifyReceivePickup(ExplosivePickup* pickup);
	void NotifyKilledPlayer(ExplosiveCharacter* player); // so we know we killed some 1

	virtual void PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format);
	
	const Vector3& GetColour();
	void SetColour(int colourIdx);
	
	bool KickBomb(Bomb* bomb, int xDir, int zDir);

	Statistics& GetStats() { return stats; }
	const char* GetName() { return name.c_str(); }

	int GetCharacterId() { return characterId; }
	int GetColourIdx() { return colourIdx; }
	int	GetTeamColourIdx() { return teamColourIdx; }


	virtual NetType* GetNetType();
	virtual void ReceiveMessage(const void* message, int length, NetExtension::NetMessageTarget target);
	void Teleport(ExplosiveCharacter* other, Tile* otherLocation = 0, Tile* thisLocation = 0);

	TeamTag& GetTeamTag() { return teamTag; }

	void UpdateAnimations();

	void SendPosition(bool reliable);

	static void PreCache();

//protected:
	int curBombCount; //number out on the field
	int bombCount;
	int bombRadius;
	int bombLife;

	int characterId;
	//int colourId;

	float speed;
	ExplosiveController::Button direction;
	ExplosiveController::Button firstDirection;
	Quaternion skinDirection;

	// AI can use nextTile also :)
	Tile* currentTile;
	Tile* nextTile;

	SkeletonAnimation skelAnim;
	int walkAnimInst;
	int idleAnimInst;

	int networkCounter;
	bool wasMoving;
	bool moving;

	int timeFromIdle;
	int timeFromWalk;

	Array<Modifier*> modifiers;
	int pickups[PICKUP_COUNT]; // pickup type followed by the count

	// other modifiers
	bool kickBomb;
	bool reverseControls;

	Sound* pickupSnd;
	Sound* deathSnd;
	Sound* dropSnd;
	Sound* kickSnd;

	Effect* effect;
	CGparameter colourParam;
	Vector3 colour;
	std::string name;

	Mesh* shadow;

	Statistics stats;

	// constructor data
	std::string skinFile;
	int colourIdx;
	int teamColourIdx;

	TeamTag teamTag;

	static int charCount;
	static int skinUpdateCount;
};

#endif

