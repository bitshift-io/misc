#include "ExplosivePickUp.h"
#include "ExplosiveCharacter.h"
#include "ExplosiveEnvironmentMgr.h"
#include "../Engine/Renderer.h"

#define MAX_BOMB_RADIUS 500
#define MAX_BOMB_COUNT  500

#define TELEPORT_INCREMENT 0.4f
#define TELEPORT_LIFE 20
#define TELEPORT_ANGLE_INCREMENT 5.0f

#define PICKUP_EFFECT_LIFE 20
#define SNAIL_LIFE 300
#define SHIELD_LIFE 1000
#define DISEASE_LIFE 300
#define REVERSE_LIFE 200

#define SPEED_CHANGE 0.15f

#define max(a, b) a > b ? a : b
#define min(a, b) a < b ? a : b

ExplosivePickup::ExplosivePickup(Tile* tile, const char* meshName, const char* matName) : PickUp(meshName),
	tile(tile),
	scale(0.1f)
{
	// pickups all use the same model, just different skin
	mesh->GetMeshMat().material[0].material = MaterialMgr::Load(matName);

	Matrix4 transform(Matrix4::Identity);
	transform.SetTranslation(tile->GetPosition());
	mesh->SetTransform(transform);

	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(GetMesh());
	tile->GetActors().Insert(this);

	ActorMgr::GetInstance().AddActor(this, 3); // pickups go in to group 3

	Update();
}

ExplosivePickup::~ExplosivePickup()
{
	ActorMgr::GetInstance().RemoveActor(this, 3, true); 
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(GetMesh());
}

bool ExplosivePickup::GiveToPlayer(Character* player)
{
	Log::Print("about to spawn a pickup effect\n");
	// spawn pickup effect
	new PickUpEffect(tile, static_cast<ExplosiveCharacter*>(player)->GetColour());

	Log::Print("now spawned\n");
	return true;
}

//*******************************************************************************************

PickUpEffect::PickUpEffect(Tile* tile, const Vector3& colour) :
	colour(colour),
	mesh(0),
	life(PICKUP_EFFECT_LIFE),
	alpha(1.0f)
{
	mesh = Mesh::LoadMesh("fx_pickup.mdl");
	mesh->SetCallback(this);
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(mesh);

	Matrix4 transform(Matrix4::Identity);
	transform.SetTranslation(tile->GetPosition());
	mesh->SetTransform(transform);

	alphaParam = mesh->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("alpha");
	colourParam = mesh->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("colour");
	scrollParam = mesh->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("texScroll");

	ActorMgr::GetInstance().AddActor(this, 2); // add to fx group
}

PickUpEffect::~PickUpEffect()
{
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(mesh);
	Mesh::ReleaseMesh(mesh);
}

void PickUpEffect::PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format)
{
	material->GetEffect()->SetFloat(alphaParam, alpha);
	material->GetEffect()->SetVector3(colourParam, colour);

	float scrl[2] = {0.0f, -1.0f};
	material->GetEffect()->SetFloat2(scrollParam, scrl);
}

bool PickUpEffect::Update()
{
	-- life;

	if (life <= 0)
		return false;

	return true;
}

//*******************************************************************************************

bool ExplosivePickup::Update()
{
	if (scale == 1.0f)
		return true;

	scale = scale * 2.0f;
	scale = min(1.0f, scale);

	Matrix4 transform(Matrix4::Identity);
	transform.SetTranslation(tile->GetPosition());
	transform.SetScale(scale);
	mesh->SetTransform(transform);
	return true;
}

bool BombExpandPickUp::GiveToPlayer(Character* player)
{
	ExplosivePickup::GiveToPlayer(player);
	ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(player);

	if (c->bombRadius >= MAX_BOMB_RADIUS)
		return true;

	c->bombRadius += 1;
	c->NotifyReceivePickup(this);

	return true;
}

bool BombCountPickUp::GiveToPlayer(Character* player)
{
	ExplosivePickup::GiveToPlayer(player);
	ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(player);
	
	if (c->bombCount >= MAX_BOMB_COUNT)
		return true;

	c->bombCount += 1;
	c->NotifyReceivePickup(this);
	return true;
}

//*******************************************************************************************

SnailModifier::SnailModifier(ExplosiveCharacter* player) : Modifier(player)
{
	Reset();
	player->speed = max(player->speed - SPEED_CHANGE, PLAYER_SPEED - SPEED_CHANGE);
	player->modifiers.Insert(this);
}

void SnailModifier::Reset()
{
	life = SNAIL_LIFE;
}

bool SnailModifier::Update()
{
	--life;

	if (life <= 0)
	{
		ExplosiveCharacter* player = static_cast<ExplosiveCharacter*>(owner);
		player->speed = min(player->speed + SPEED_CHANGE, PLAYER_SPEED + SPEED_CHANGE);
		return false;
	}

	return true;
}

bool SnailPickUp::GiveToPlayer(Character* player)
{
	ExplosivePickup::GiveToPlayer(player);

	// create a modifier and add it to the player,
	ExplosiveCharacter* e = static_cast<ExplosiveCharacter*>(player);
	e->NotifyReceivePickup(this);

	for (int i = 0; i < e->modifiers.GetSize(); ++i)
	{
		Modifier* m = e->modifiers[i];

		// if we already have a turtle modifier, reset the life so it lasts longer
		if (m->GetType() == SnailModifier::Type)
		{
			SnailModifier* tMod = static_cast<SnailModifier*>(m);
			tMod->Reset();
			return true;
		}
	}

	new SnailModifier(e);
	return true;
}

//*******************************************************************************************

SpeedModifier::SpeedModifier(ExplosiveCharacter* player) : Modifier(player)
{
	Reset();
	player->speed = min(player->speed + SPEED_CHANGE, PLAYER_SPEED + SPEED_CHANGE);
	player->modifiers.Insert(this);
}

void SpeedModifier::Reset()
{
	life = SNAIL_LIFE;
}

bool SpeedModifier::Update()
{
	--life;

	if (life <= 0)
	{
		ExplosiveCharacter* player = static_cast<ExplosiveCharacter*>(owner);

		player->speed = max(player->speed - SPEED_CHANGE, PLAYER_SPEED - SPEED_CHANGE);
		return false;
	}

	return true;
}

bool SpeedPickUp::GiveToPlayer(Character* player)
{
	ExplosivePickup::GiveToPlayer(player);

	// create a modifier and add it to the player,
	ExplosiveCharacter* e = static_cast<ExplosiveCharacter*>(player);
	e->NotifyReceivePickup(this);

	for (int i = 0; i < e->modifiers.GetSize(); ++i)
	{
		Modifier* m = e->modifiers[i];

		// if we already have a turtle modifier, reset the life so it lasts longer
		if (m->GetType() == SpeedModifier::Type)
		{
			SpeedModifier* tMod = static_cast<SpeedModifier*>(m);
			tMod->Reset();
			return true;
		}
	}

	new SpeedModifier(e);
	return true;
}

//*******************************************************************************************

TeleportEffect::TeleportEffect(Tile* t1, Tile* t2, const Vector3& colour1, const Vector3& colour2) :
	t1(t1),
	t2(t2),
	colour1(colour1),
	colour2(colour2),
	mesh1(0),
	mesh2(0),
	life(TELEPORT_LIFE),
	alpha(1.0f)
{
	teleportSnd = SoundMgr::Load("teleport.wav");
	if (teleportSnd)
		teleportSnd->Play();

	if (t1)
	{
		mesh1 = Mesh::LoadMesh("fx_teleport.mdl");
		mesh1->SetCallback(this);
		ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(mesh1);

		Matrix4 transform(Matrix4::Identity);
		transform.SetTranslation(t1->GetPosition());
		mesh1->SetTransform(transform);
	}

	if (t2)
	{
		mesh2 = Mesh::LoadMesh("fx_teleport.mdl");
		mesh2->SetCallback(this);
		ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(mesh2);

		Matrix4 transform(Matrix4::Identity);
		transform.SetTranslation(t2->GetPosition());
		mesh2->SetTransform(transform);
	}
}

TeleportEffect::~TeleportEffect()
{
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(mesh1);
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(mesh2);
	Mesh::ReleaseMesh(mesh1);
	Mesh::ReleaseMesh(mesh2);
}

bool TeleportEffect::Update()
{
	alpha = float(life) / float(TELEPORT_LIFE) + 0.5f;

	--life;

	if (mesh1)
	{
		Vector3 position = mesh1->GetTransform().GetTranslation();
		Matrix4 transform(Matrix4::Identity);
		transform.RotateY(Math::DtoR(life * TELEPORT_ANGLE_INCREMENT));
		transform.SetTranslation(position + Vector3(0, TELEPORT_INCREMENT, 0.0));		
		mesh1->SetTransform(transform);

		alphaParam = mesh1->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("alpha");
		colourParam = mesh1->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("colour");
	}

	if (mesh2)
	{
		Vector3 position = mesh2->GetTransform().GetTranslation();
		Matrix4 transform(Matrix4::Identity);
		transform.RotateY(Math::DtoR(life * TELEPORT_ANGLE_INCREMENT));
		transform.SetTranslation(position + Vector3(0, TELEPORT_INCREMENT, 0.0));		
		mesh2->SetTransform(transform);

		alphaParam = mesh2->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("alpha");
		colourParam = mesh2->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("colour");
	}

	if (life <= 0)
	{
		if (teleportSnd && teleportSnd->IsPlaying())
			return true;

		return false;
	}

	return true;
}

void TeleportEffect::PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format)
{
	material->GetEffect()->SetFloat(alphaParam, alpha);

	if (mesh == mesh1)
		material->GetEffect()->SetVector3(colourParam, colour1);
	else if (mesh == mesh2)
		material->GetEffect()->SetVector3(colourParam, colour2);
}


//*******************************************************************************************

bool TeleportPickUp::GiveToPlayer(Character* player)
{
	ExplosivePickup::GiveToPlayer(player);

	Array<Actor*>& characters = ActorMgr::GetInstance().GetActorGroup(0);

	Array<Actor*> liveCharacters(characters.GetSize());
	for (int i = 0; i < characters.GetSize(); ++i)
	{
		ExplosiveCharacter* a = static_cast<ExplosiveCharacter*>(characters[i]);
		if (!a->IsDead())
			liveCharacters.Insert(a);
	}

	int randChar = rand() % liveCharacters.GetSize();

	ExplosiveCharacter* ePlayer = static_cast<ExplosiveCharacter*>(player);
	ExplosiveCharacter* other = static_cast<ExplosiveCharacter*>(liveCharacters[randChar]);
	
	// network stuff
	other->Teleport(ePlayer);
	return true;
}

//*******************************************************************************************

ShieldModifier::ShieldModifier(ExplosiveCharacter* player) : Modifier(player),
	innerRotation(0.0f),
	outterRotation(0.0f),
	pulseLife(0)
{
	Reset();
	colour = player->GetColour();
	alpha = 1.0f;

	player->modifiers.Insert(this);
	meshInner = Mesh::LoadMesh("fx_shield_inner.mdl");
	meshOutter = Mesh::LoadMesh("fx_shield_outter.mdl");
	meshInner->SetCallback(this);
	meshOutter->SetCallback(this);
	alphaParam = meshInner->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("alpha");
	colourParam = meshInner->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("colour");

	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(meshInner);
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(meshOutter);
}

ShieldModifier::~ShieldModifier()
{
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(meshInner);
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(meshOutter);
	Mesh::ReleaseMesh(meshInner);
	Mesh::ReleaseMesh(meshOutter);
}

void ShieldModifier::Reset()
{
	life = SHIELD_LIFE;
}

void ShieldModifier::PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format)
{
	material->GetEffect()->SetFloat(alphaParam, alpha);
	material->GetEffect()->SetVector3(colourParam, colour);
}

bool ShieldModifier::Update()
{
	--life;

	--pulseLife;
	if (pulseLife <= 0)
	{
		pulseLife = 10;
		alpha = (float(rand() % 1000) / 2000.0f) + 0.5f; // 0.5 - 1.0f
	}

	innerRotation += 0.1f;
	outterRotation -= 0.13f;

	ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(GetOwner());
	Vector3 position = c->GetPosition();
	Matrix4 transform(Matrix4::Identity);
	transform.RotateY(innerRotation);
	transform.SetTranslation(position);
	meshInner->SetTransform(transform);

	transform = Matrix4(Matrix4::Identity);
	transform.RotateY(outterRotation);
	transform.SetTranslation(position);
	meshOutter->SetTransform(transform);
	
	if (life <= 0)
		return false;

	return true;
}

void ShieldModifier::PlayerKilled()
{
	// play shield effect
}

bool ShieldPickUp::GiveToPlayer(Character* player)
{
	ExplosivePickup::GiveToPlayer(player);

	// create a modifier and add it to the player,
	ExplosiveCharacter* e = static_cast<ExplosiveCharacter*>(player);
	e->NotifyReceivePickup(this);

	for (int i = 0; i < e->modifiers.GetSize(); ++i)
	{
		Modifier* m = e->modifiers[i];

		// if we already have this modifier
		if (m->GetType() == ShieldModifier::Type)
		{
			ShieldModifier* tMod = static_cast<ShieldModifier*>(m);
			tMod->Reset();
			return true;
		}
	}

	new ShieldModifier(e);
	return true;
}

//*******************************************************************************************

DiseaseModifier::DiseaseModifier(ExplosiveCharacter* player) : Modifier(player)
{
	player->modifiers.Insert(this);
	Reset();
}

bool DiseaseModifier::Update()
{
	--life;

	ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(GetOwner());
	c->DropBomb();

	if (life <= 0)
		return false;

	return true;
}

void DiseaseModifier::Reset()
{
	life = DISEASE_LIFE;
}

bool DiseasePickUp::GiveToPlayer(Character* player)
{
	ExplosivePickup::GiveToPlayer(player);

	// create a modifier and add it to the player,
	ExplosiveCharacter* e = static_cast<ExplosiveCharacter*>(player);
	e->NotifyReceivePickup(this);

	for (int i = 0; i < e->modifiers.GetSize(); ++i)
	{
		Modifier* m = e->modifiers[i];

		// if we already have this modifier
		if (m->GetType() == DiseaseModifier::Type)
		{
			DiseaseModifier* tMod = static_cast<DiseaseModifier*>(m);
			tMod->Reset();
			return true;
		}
	}

	new DiseaseModifier(e);
	return true;
}

//*******************************************************************************************

bool BombKickPickUp::GiveToPlayer(Character* player)
{
	ExplosivePickup::GiveToPlayer(player);
	static_cast<ExplosiveCharacter*>(player)->kickBomb = true;
	return true;
}


//*******************************************************************************************

bool RandomPickUp::GiveToPlayer(Character* player)
{
	ExplosivePickup::GiveToPlayer(player);

	int randPickup = rand() % 9;
	ExplosivePickup* pickup = 0;
	switch(randPickup)
	{
	case 0:
		pickup = new BombExpandPickUp(tile, "pickup.mdl", "pickup_fire.mat");
		break;
	case 1:
		pickup = new BombCountPickUp(tile, "pickup.mdl", "pickup_bomb.mat");
		break;
	case 2:
		pickup = new SnailPickUp(tile, "pickup.mdl", "pickup_snail.mat");
		break;
	case 3:
		pickup = new TeleportPickUp(tile, "pickup.mdl", "pickup_teleport.mat");
		break;
	case 4:
		pickup = new ShieldPickUp(tile, "pickup.mdl", "pickup_shield.mat");
		break;
	case 5:
		pickup = new BombKickPickUp(tile, "pickup.mdl", "pickup_kick.mat");
		break;
	case 6:
		pickup = new DiseasePickUp(tile, "pickup.mdl", "pickup_disease.mat");
		break;
	case 7:
		pickup = new SpeedPickUp(tile, "pickup.mdl", "pickup_speed.mat");
		break;
	case 8:
		pickup = new ReversePickUp(tile, "pickup.mdl", "pickup_reverse.mat");
		break;
	}

	pickup->GiveToPlayer(player);
	ExplosiveEnvironmentMgr::GetInstance().KillPickup(tile, pickup);
	return true;
}

//*******************************************************************************************

ReverseModifier::ReverseModifier(ExplosiveCharacter* player) : Modifier(player)
{
	player->modifiers.Insert(this);
	player->reverseControls = true;
	Reset();
}

bool ReverseModifier::Update()
{
	--life;

	if (life <= 0)
	{
		ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(GetOwner());
		c->reverseControls = false;
		return false;
	}

	return true;
}

void ReverseModifier::Reset()
{
	life = REVERSE_LIFE;
}


bool ReversePickUp::GiveToPlayer(Character* player)
{
	ExplosivePickup::GiveToPlayer(player);

	// create a modifier and add it to the player,
	ExplosiveCharacter* e = static_cast<ExplosiveCharacter*>(player);
	e->NotifyReceivePickup(this);

	for (int i = 0; i < e->modifiers.GetSize(); ++i)
	{
		Modifier* m = e->modifiers[i];

		// if we already have this modifier
		if (m->GetType() == ReverseModifier::Type)
		{
			ReverseModifier* tMod = static_cast<ReverseModifier*>(m);
			tMod->Reset();
			return true;
		}
	}

	new ReverseModifier(e);
	return true;
}



