#ifndef _SIPHON_EXPLOSIVEAICONTROLLER_
#define _SIPHON_EXPLOSIVEAICONTROLLER_

#include "../Core/Character.h"
#include "ExplosiveCharacter.h"

class Tile;
class ExplosiveCharacter;

class ExplosiveAIController : public ExplosiveController
{
public:
	ExplosiveAIController();

	virtual void Update(Actor* object);

	virtual bool IsHuman() { return false; }

	virtual bool KeyDown(Button button);
	virtual bool KeyPressed(Button button);

protected:
	enum State
	{
		Moving,
		SafeAndWatingForBombToBlow, // this means we are stopped
		RunningFromBomb
	};

	enum TileType
	{
		TypeEmpty			= 0x00,
		TypeBlocked			= 0xFF,
		TypeDynamicBlocked	= 0xF0,
		TypeBomb			= 0x01,
		TypeCharacter		= 0x02,
		TypePickup			= 0x04,
	};

	// given a tile, find out which direction has the longest ray cast
	int GetLongestDirection(Tile* tile, ExplosiveCharacter* characte);

	// pick a new direction
	void ChooseDirection(ExplosiveCharacter* character);

	// should the ai change direction?
	bool ShouldChooseNewDirection(ExplosiveCharacter* character);

	// check to see if we are in danger of being bombed
	bool InBombRadius(ExplosiveCharacter* character, Tile* tile);

	// if we dropped a bomb, would the radius of this bomb hit any characters besidies us >-)
	bool CharacterInBombRadius(ExplosiveCharacter* character);

	TileType CheckTileType(Tile* tile, ExplosiveCharacter* character);

	ExplosiveCharacter* FindTarget(ExplosiveCharacter* character);
	void HeadTowardsTarget(ExplosiveCharacter* character, bool* direction, int directionCount);

	ExplosiveCharacter* target;
	Array<bool> stateMap;
	State state;
	int rayDirectionPreference;
	int randomizeCounter;
	int pickNewDirectionCounter;

	bool debuggerAttached;
};

#endif

