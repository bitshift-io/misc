#include "TileEnvironmentMgr.h"

using namespace Siphon;

TileEnvironmentMgr::~TileEnvironmentMgr()
{
	Reset();
}

void TileEnvironmentMgr::Reset()
{

}
/*
void TileEnvironmentMgr::MoveTile(Tile* tile, int xNew, int zNew)
{
	tiles[tile->GetZ() + (width * tile->GetX())] = NULL;

	tile->x = xNew;
	tile->z = zNew;

	tiles[tile->GetZ() + (width * tile->GetX())] = tile;
}*/

Tile* TileEnvironmentMgr::GetTile(int x, int z)
{
	if (z < 0 || z >= length)
		return NULL;

	if (x < 0 || x >= width)
		return NULL;

	return tiles[x + (width * z)];
}
/*
void TileEnvironmentMgr::AddTile(Tile* tile, int layer)
{
	if (layer <= tiles.GetSize())
	{
		// this should go in some sort of setsize method
		Array<Tile*> newLayer(width * length);
		for (int i = 0; i < width * length; ++i)
			newLayer.Insert(NULL);

		tiles.Insert(newLayer);
	}

	tiles[layer][tile->GetZ() + (width * tile->GetX())] = tile;
}

void TileEnvironmentMgr::RemoveTile(Tile* tile, int layer)
{
//	tiles[layer][tile->GetZ() + (width * tile->GetX())] = 0;
}*/

void TileEnvironmentMgr::GetTiles(int x, int z, int radius, Array<Tile*>& tiles)
{
	tiles.SetCapacity(radius*4 + 1);

	for (int xd = x - radius; xd <= (x + radius); ++xd)
	{
		// dont insert null tiles
		Tile* tile = GetTile(xd, z);

		if (tile)
			tiles.Insert(tile);
	}

	for (int zd = z - radius; zd <= (z + radius); ++zd)
	{
		// so we dont add the center tile twice
		if (zd == z)
			continue;

		// dont insert null tiles
		Tile* tile = GetTile(x, zd);

		if (tile)
			tiles.Insert(tile);
	}
}
/*
void TileEnvironmentMgr::RemoveTiles(Array<Tile*>& tiles)
{
	for (int i = 0; i < tiles.GetSize(); ++i)
	{
		Tile* tile = tiles[i];
		this->tiles[layer][tile->GetZ() + (width * tile->GetX())] = 0;
	}
}

void TileEnvironmentMgr::DeleteTiles(Array<Tile*>& tiles)
{
	for (int i = 0; i < tiles.GetSize(); ++i)
	{
		Tile* tile = tiles[i];
		delete tile;
	}

	tiles.SetSize(0);
}*/

void TileEnvironmentMgr::RayCast(int x, int z, int& xPos, int& xNeg, int& zPos, int& zNeg)
{
	for (xPos = x + 1; xPos <= width; ++xPos)
	{
		if (GetTile(xPos, z))
			break;
	}

	for (xNeg = x - 1; xNeg >= 0; --xNeg)
	{
		if (GetTile(xNeg, z))
			break;
	}

	for (zPos = z + 1; zPos <= length; ++zPos)
	{
		if (GetTile(x, zPos))
			break;
	}

	for (zNeg = z - 1; zNeg >= 0; --zNeg)
	{
		if (GetTile(x, zNeg))
			break;
	}

	if (xNeg != -1)
		xNeg = x - xNeg;

	if (xPos > width)
		xPos = -1;
	else
		xPos = xPos - x;

	if (zNeg != -1)
		zNeg = z - xNeg;

	if (zPos > length)
		zPos = -1;
	else
		zPos = zPos - z;
}
