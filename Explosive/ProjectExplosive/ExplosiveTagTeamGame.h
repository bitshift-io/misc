#ifndef _SIPHON_TAGTEAMGAMEMODE_
#define _SIPHON_TAGTEAMGAMEMODE_

#include "../Core/GameModeMgr.h"
#include "Globals.h"

class DeathmatchGame : public GameMode
{
public:
	// the name of the game mode
	virtual const char* GetName() { return "Deathmatch"; }

	// get the description
	virtual const char* GetDescription() { 
		return "If it moves, kill it."; }

	virtual void Reset() {}
	virtual void Update() {}

	virtual bool PlayerKilled(Character* killer, Character* killed);

	virtual bool IsTeamGame() { return false; }

protected:

};

class ExplosiveTagTeamGame : public GameMode
{
public:
	ExplosiveTagTeamGame();

	// the name of the game mode
	virtual const char* GetName() { return "Tag Team"; }

	// get the description
	virtual const char* GetDescription() { 
		return "All players start on thier own team. When a player is killed, they join the team that killed them. game continues until no players left."; }

	virtual void Reset();
	virtual void Update() { }

	virtual bool PlayerKilled(Character* killer, Character* killed);
	virtual void AddPlayer(Character* player);
	virtual void RemovePlayer(Character* player);

	virtual bool IsTeamGame() { return true; }

protected:
	void CheckEndGame();

	int		teams[MAX_PLAYERS];
	bool	players[MAX_PLAYERS];
};

#endif