#ifndef _MENU_
#define _MENU_

#include "../GUICore/GCursor.h"
#include "../GUICore/GWindow.h"
#include "../GUICore/GButton.h"
#include "../GUICore/GEnum.h"
#include "../GUICore/GInput.h"
#include "../PHPMasterServer/MasterServer.h"
#include "Globals.h"

using namespace Siphon;

class Siphon::Window;

class Menu : public GWindow::Callback
{
public:
	enum VisibleMenu
	{
		None,
		Main,
		Options,
		MapSelection,
		GameOptions,
		PlayerSetup,
		Join,
		Pause,
		Victory,
		LAN,
		Internet,
		Chat,
		JoinIP,
		About,
		MultiplayerOptions,
		Help,
	};

	Menu();
	void Init(Window* window);

	void Update();
	void Render();

	void DisplayMenu(VisibleMenu menu);

	//virtual void OnMouseDown(GWindow* child);
	virtual void OnMouseOver(GWindow* child);
	virtual void OnMouseOut(GWindow* child);
	virtual void OnMousePressed(GWindow* child);
	virtual void OnKeyPressed(GWindow* child, const std::string& pressedKey);

	bool MenuDisplayed();
	bool ShouldExit() { return shouldExit; }

	bool	AddPlayerFromButton(int idx, GButton* btn, int characterId = -1);
//protected:
	struct LabelButtonPair
	{
		GWindow* window;
		GButton* button;
	};

	struct LabelInputPair
	{
		GWindow* window;
		GInput*	input;
	};

	void GenerateVictoryMenu();

	void UpdateLAN();
	void UpdateInternet();
	void UpdateMapDetails();

	Window*		window;
	bool		shouldExit;
	VisibleMenu	visibleMenu;
	Font*		font;

	GCursor cursor;

	GWindow menuMain;
	GWindow menuPause;
	GWindow menuOptions;
	GWindow menuMapSelection;
	GWindow menuGameOptions;
	GWindow menuVictory;
	GWindow menuPlayerSetup;
	GWindow menuJoinLAN;
	GWindow menuJoinInternet;
	GWindow menuJoinIP;
	GWindow menuChat;
	GWindow menuChatBox;
	GWindow menuAbout;
	GWindow menuMultiplayerOptions;
	GWindow menuHelp;

	GWindow* imgCharacter;
	GWindow* imgLeftKeyboard;
	GWindow* imgRightKeyboard;
	GWindow* imgPickups1;
	GWindow* imgPickups2;
	GWindow* lblLeftKeyboard;
	GWindow* lblRightKeyboard;
	GWindow* lblPickups1;
	GWindow* lblPickups2;

	GWindow* lblAbout;

	GWindow* btnAbout;
	GButton* btnJoin;
	GButton* btnBack;
	GButton* btnPlay;
	GButton* btnGameOptions;
	GButton* btnOptions;
	GButton* btnMultiplayerOptions;
	GButton* btnExit;
	GButton* btnHelp;

	GButton* btnMultiplayer;
	GButton* btnLAN;
	GButton* btnInternet;
	GButton* btnJoinIP;

	GWindow* lblRefreshLAN;
	GWindow* lblRefreshInternet;

	GButton* btnContinue;
	GButton* btnRestart;

	// video options
	LabelButtonPair lbpResolution;
	LabelButtonPair lbpFullscreen;
	LabelButtonPair lbpAntialiasing;
	GWindow* lblRestart;

	// victory menu
	GWindow* lblStats;
	GWindow* lblLevelDetails;
	GWindow* lblLevelImage;

	// play buttons
	GButton* btnPlayerSetup;
	GButton* btnMapSelection;
	GButton* btnStart;
	GButton* btnHost;

	// game options
	LabelButtonPair lbpRoundsPerMap;
	LabelButtonPair lbpTimeLimit;
	LabelButtonPair lbpAICanWin;
	LabelButtonPair lbpAIInStats;
	LabelButtonPair lbpGameMode;

	// muliplayer options
	LabelButtonPair lbpControls;
	LabelInputPair	lipServerName;
	LabelInputPair	lipClientName;

	LabelButtonPair players[MAX_PLAYERS];
	GEnum			enmLevels;

	// join LAN
	GEnum			enmLAN;
	GEnum			enmInternet;

	GInput*			ipIP;
	GWindow*		lblIP;

	GInput*			ipChat;
	GWindow*		lblChat;
	GEnum			enmChatBox;

	GWindow*		lblDemo;

	bool multiplayer;
	Array<ServerInfo> internetServers;
	Array<ServerInfo> lanServers;
};

#endif