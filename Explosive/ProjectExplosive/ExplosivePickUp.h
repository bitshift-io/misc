#ifndef _SIPHON_EXPLOSIVEPICKUP_
#define _SIPHON_EXPLOSIVEPICKUP_

#include "../Core/PickUp.h"
#include "../Core/Modifier.h"

#include "../Engine/Sound.h"

class Tile;
class ExplosiveCharacter;

#define PICKUPTYPE(t) enum { PickupType = t }; virtual int GetPickupType() const { return PickupType; }

// game specific pickup
class ExplosivePickup : public PickUp
{
public:
	TYPE(4)

	virtual int GetPickupType() const = 0;

	ExplosivePickup(Tile* tile, const char* meshName, const char* matName);
	virtual ~ExplosivePickup();

	virtual bool Update(); // animate scale bounce here

	virtual bool GiveToPlayer(Character* player);
protected:
	Tile*	tile;
	float	scale;
};

//*******************************************************************************************

class PickUpEffect : public Actor, public Mesh::Callback
{
public:

	TYPE(1) // we use a different numbering system as we aren't an actor

	PickUpEffect(Tile* tile, const Vector3& colour);
	virtual ~PickUpEffect();

	virtual void PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format);

	virtual bool Update();

protected:

	int life;
	int pulseLife;

	Mesh* mesh;

	CGparameter alphaParam;
	CGparameter colourParam;
	CGparameter scrollParam;

	float	scroll;
	float	alpha;
	Vector3 colour;
};

//*******************************************************************************************

class BombExpandPickUp : public ExplosivePickup
{
public:
	PICKUPTYPE(0)

	BombExpandPickUp(Tile* tile, const char* meshName, const char* matName) : ExplosivePickup(tile, meshName, matName) {}
	virtual bool GiveToPlayer(Character* player);
};

class BombCountPickUp : public ExplosivePickup
{
public:
	PICKUPTYPE(1)

	BombCountPickUp(Tile* tile, const char* meshName, const char* matName) : ExplosivePickup(tile, meshName, matName) {}
	virtual bool GiveToPlayer(Character* player);
};

//*******************************************************************************************

class SnailModifier : public Modifier
{
public:
	TYPE(0) // we use a different numbering system as we arent an actor

	SnailModifier(ExplosiveCharacter* player);

	virtual bool Update();

	void Reset();
protected:
	float	currentSpeed;
	int		life;
};

class SnailPickUp : public ExplosivePickup
{
public:
	PICKUPTYPE(2)

	SnailPickUp(Tile* tile, const char* meshName, const char* matName) : ExplosivePickup(tile, meshName, matName) {}
	virtual bool GiveToPlayer(Character* player);
};

//*******************************************************************************************

class SpeedModifier : public Modifier
{
public:
	TYPE(4) // we use a different numbering system as we arent an actor

	SpeedModifier(ExplosiveCharacter* player);

	virtual bool Update();

	void Reset();
protected:
	float	currentSpeed;
	int		life;
};

class SpeedPickUp : public ExplosivePickup
{
public:
	PICKUPTYPE(8)

	SpeedPickUp(Tile* tile, const char* meshName, const char* matName) : ExplosivePickup(tile, meshName, matName) {}
	virtual bool GiveToPlayer(Character* player);
};

//*******************************************************************************************

/**
 * Plays a teleport effect on a given tile
 */
class TeleportEffect : public Actor, public Mesh::Callback
{
public:
	TeleportEffect(Tile* t1, Tile* t2, const Vector3& colour1 = Vector3(1.0, 1.0, 1.0), const Vector3& colour2 = Vector3(1.0, 1.0, 1.0));
	~TeleportEffect();

	virtual bool Update();
	virtual void PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format);

protected:
	Sound* teleportSnd;

	Tile* t1;
	Tile* t2;
	
	Vector3 colour1;
	Vector3 colour2;

	Mesh* mesh1;
	Mesh* mesh2;

	int life;
	float alpha;

	CGparameter alphaParam;
	CGparameter colourParam;
};

//*******************************************************************************************

class TeleportPickUp : public ExplosivePickup
{
public:
	PICKUPTYPE(3)

	TeleportPickUp(Tile* tile, const char* meshName, const char* matName) : ExplosivePickup(tile, meshName, matName) {}
	virtual bool GiveToPlayer(Character* player);
};

//*******************************************************************************************

class ShieldModifier : public Modifier, public Mesh::Callback
{
public:

	TYPE(1) // we use a different numbering system as we aren't an actor

	ShieldModifier(ExplosiveCharacter* player);
	virtual ~ShieldModifier();

	virtual void PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format);

	virtual bool Update();
	void Reset();
	void PlayerKilled();
protected:
	int life;
	int pulseLife;

	Mesh* meshInner;
	Mesh* meshOutter;

	float innerRotation;
	float outterRotation;

	CGparameter alphaParam;
	CGparameter colourParam;

	float	alpha;
	Vector3 colour;
};

class ShieldPickUp : public ExplosivePickup
{
public:
	PICKUPTYPE(4)

	ShieldPickUp(Tile* tile, const char* meshName, const char* matName) : ExplosivePickup(tile, meshName, matName) {}
	virtual bool GiveToPlayer(Character* player);
};

//*******************************************************************************************

class DiseaseModifier : public Modifier
{
public:

	TYPE(2) // we use a different numbering system as we aren't an actor

	DiseaseModifier(ExplosiveCharacter* player);

	virtual bool Update();
	void Reset();

protected:
	int life;
};

class DiseasePickUp : public ExplosivePickup
{
public:
	PICKUPTYPE(5)

	DiseasePickUp(Tile* tile, const char* meshName, const char* matName) : ExplosivePickup(tile, meshName, matName) {}
	virtual bool GiveToPlayer(Character* player);
};

//*******************************************************************************************

class BombKickPickUp : public ExplosivePickup
{
public:
	PICKUPTYPE(6)

	BombKickPickUp(Tile* tile, const char* meshName, const char* matName) : ExplosivePickup(tile, meshName, matName) {}
	virtual bool GiveToPlayer(Character* player);
};

//*******************************************************************************************

class RandomPickUp : public ExplosivePickup
{
public:
	PICKUPTYPE(7)

	RandomPickUp(Tile* tile, const char* meshName, const char* matName) : ExplosivePickup(tile, meshName, matName) {}
	virtual bool GiveToPlayer(Character* player);
};

//*******************************************************************************************

class ReverseModifier : public Modifier
{
public:

	TYPE(3) // we use a different numbering system as we aren't an actor

	ReverseModifier(ExplosiveCharacter* player);

	virtual bool Update();
	void Reset();

protected:
	int life;
};

class ReversePickUp : public ExplosivePickup
{
public:
	PICKUPTYPE(9)

	ReversePickUp(Tile* tile, const char* meshName, const char* matName) : ExplosivePickup(tile, meshName, matName) {}
	virtual bool GiveToPlayer(Character* player);
};




#endif