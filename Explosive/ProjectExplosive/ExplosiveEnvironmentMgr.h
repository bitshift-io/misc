#ifndef _SIPHON_EXPLOSIVEENVIRONMENTMANAGER_
#define _SIPHON_EXPLOSIVEENVIRONMENTMANAGER_

#include "../Engine/Renderer.h"
#include "../Engine/Mesh.h"
#include "../Util/Singleton.h"

#include "../Core/Character.h"
#include "../Core/NetObject.h"
#include "../Core/NetServer.h"
#include "../Util/INI.h"
#include "TileEnvironmentMgr.h"

using namespace Siphon;

#define TILE_WIDTH 2.54f
#define TILE_LENGTH 2.54f
#define TILE_HEIGHT 2.54f
#define TILE_FLOOR_HEIGHT 0.5

class ExplosiveEnvironmentMgr : public TileEnvironmentMgr, public Siphon::Singleton<ExplosiveEnvironmentMgr>, public NetObject
{
public:
	enum Group
	{
		Floor = 0,
		Static = 1,
		Dynamic = 2,
		PickUp = 5
	};

	enum NetworkMessage
	{
		NM_KillDynamicTile,
		NM_KillPickup,
		NM_LoadMap,
		NM_Spawns
	};

	ExplosiveEnvironmentMgr();
	~ExplosiveEnvironmentMgr();

	bool GetMap(const char* mapFileName, INIFile& file);

	void GetLevelList(Array<std::string>& levels, const char* dir);
	bool LoadMap(const char* mapFileName);
	bool LoadMapNetwork(const char* mapFileName, const char* tileList);
	void UnloadMap(bool restart = false);
	std::string GetCurMap()	{ return level; }

	void RandomiseSpawns();
	virtual Tile* GetSpawnPoint(int index);
	virtual Tile* GetTile(int x, int y);

	void DebugDraw();

	virtual void Reset();

	Renderer* GetRenderer();

	// this will spawn goodies
	void KillDynamicTile(Tile* tile, Actor* actor);
	void KillPickup(Tile* tile, Actor* actor);

	// network friendly version
	void KillDynamicTile(Tile* tile, int pickupType);
	void KillPickup(Tile* tile);

	void MoveToTile(Tile* tile, Character* character);

	// network shenanigens
	void Register();
	void AcceptConnection(int index, Connection& client);
	void SendMapInfo();
	virtual unsigned int GetConstructorDataSize() { return 0; }
	virtual void GetConstructorData(char* data) { }
	virtual void ReceiveMessage(const void* message, int length, NetExtension::NetMessageTarget target);
	virtual NetType* GetNetType() { return 0; }

	void Invalidate() { valid = false; }
	bool Valid() { return valid; }

	void SetClearColour();

	void VerifyAllTiles(bool empty = false);

	void ClearAllTiles();

protected:
	int renderIdx; // index for our renderer
	Mesh* staticLevel; // this is a single plane we can generate our selves

	Array<Tile*> spawn;
	std::string level;

	int	pickupPcnt;
	Pair<int, int> teleportPcnt;
	Pair<int, int> kickPcnt;
	Pair<int, int> bombPcnt;
	Pair<int, int> firePcnt;
	Pair<int, int> diseasePcnt;
	Pair<int, int> snailPcnt;
	Pair<int, int> speedPcnt;
	Pair<int, int> shieldPcnt;
	Pair<int, int> reversePcnt;
	Pair<int, int> randomPcnt;

	bool valid;
	float colour[3];
};

#endif