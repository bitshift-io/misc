#ifndef _DESTRUCTABLEBLOCK_
#define _DESTRUCTABLEBLOCK_

#include "../Engine/Math3D.h"
#include "../Engine/Mesh.h"

#include "../Core/ActorMgr.h"

using namespace Siphon;

class Tile;

/**
 * The dynamic blocks which block the player
 */
class DestructableBlock : public Actor
{
public:
	TYPE(1)

	DestructableBlock(const Tile* tile, const char* meshFile);
	virtual ~DestructableBlock();

	virtual bool Update() { return true; }

	Vector3 GetPosition() const { return block->GetTransform().GetTranslation(); }
	const Matrix4& GetTransform() const { return block->GetTransform(); }

	Mesh* GetMesh() { return block; }

protected:
	Mesh* block;
	const Tile* tile;
};

#endif