#include "Menu.h"
#include "ExplosiveGame.h"
#include "Core/RenderMgr.h"
#include "Core/NetServer.h"
#include "Core/NetClient.h"
#include "INI.h"

#include <string>

using namespace Siphon;

Menu::Menu() : multiplayer(false)
{

}

void Menu::Init(Window* window)
{
//	btnSelectedLevel = 0;

	shouldExit = false;
	font = FontMgr::Load("five_cents.fnt");

	cursor.SetWindow(window);
	cursor.SetImage(TextureMgr::LoadTex("mouse_pointer.tga"));
	cursor.SetDimensions(GWindow::Rect(0, 0, 30, 30));
	//cursor.SetFont(RenderMgr::GetInstance().GetFont());

	//
	// menu constants
	//
	const float fontScale = 1.0f;
	const int yBtnHeight = 60;
	const int yBtnPad = 0;

	const int xBtnWidth = 250;
	const int xBtnDistFromRight = 20;

	const int yBtnDistFromTop = 50;

	//
	// main menu
	//
	menuMain.SetDimensions(GWindow::Rect(0, 0, 800, 600));
	menuMain.SetCallback(this);

	float x = (800 - xBtnWidth) - xBtnDistFromRight;
	float y = yBtnDistFromTop;
	btnPlay = new GButton("Play", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
	btnPlay->SetFont(font);
	btnPlay->SetFontScale(fontScale);
	menuMain.AddChild(btnPlay);

	y += yBtnHeight + yBtnPad;
	btnMultiplayer = new GButton("Multiplayer", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
	btnMultiplayer->SetFont(font);
	btnMultiplayer->SetFontScale(fontScale);
	menuMain.AddChild(btnMultiplayer);	

	y += yBtnHeight + yBtnPad;
	btnOptions = new GButton("Options", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
	btnOptions->SetFont(font);
	btnOptions->SetFontScale(fontScale);
	menuMain.AddChild(btnOptions);

	y += yBtnHeight + yBtnPad;
	btnAbout = new GButton("Credits", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
	btnAbout->SetFont(font);
	btnAbout->SetFontScale(fontScale);
	menuMain.AddChild(btnAbout);	

	y += yBtnHeight + yBtnPad;
	btnHelp = new GButton("Help", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
	btnHelp->SetFont(font);
	btnHelp->SetFontScale(fontScale);
	menuMain.AddChild(btnHelp);	

	y += (yBtnHeight + yBtnPad) * 3;
	btnExit = new GButton("Exit", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
	btnExit->SetFont(font);
	btnExit->SetFontScale(fontScale);
	menuMain.AddChild(btnExit);

	btnBack = new GButton("Back to Menu", btnExit->GetDimensions());
	btnBack->SetFont(font);
	btnBack->SetFontScale(fontScale);

	imgCharacter = new GWindow("", GWindow::Rect(0, 0, 500, 600));
	imgCharacter->SetBackgroundImage(TextureMgr::LoadTex("character.tga"));
	menuMain.AddChild(imgCharacter);

#ifdef _DEMO
	lblDemo = new GWindow(DEMO_STRING, GWindow::Rect(0, 0, 0, 0));
	menuMain.AddChild(lblDemo);
#endif

	//
	// menu paused, same as main menu, but a resume button and no background
	//
	menuPause.SetDimensions(GWindow::Rect(0, 0, 800, 600));
	menuPause.SetCallback(this);

	menuPause.AddChild(btnExit);

	btnContinue = new GButton("Continue", btnPlay->GetDimensions());
	btnContinue->SetFont(font);
	btnContinue->SetFontScale(fontScale);
	menuPause.AddChild(btnContinue);

	btnRestart = new GButton("Restart", btnMultiplayer->GetDimensions());
	btnRestart->SetFont(font);
	btnRestart->SetFontScale(fontScale);
	menuPause.AddChild(btnRestart);

	//
	// victory menu
	// 
	menuVictory.SetDimensions(GWindow::Rect(0, 0, 800, 600));
	menuVictory.SetCallback(this);

	const float statsFontScale = 0.8f;
	const int	xStatsDistFromLeft = 50;
	const int	yStatsDistFromTop = 200;

	lblStats = new GWindow(0, GWindow::Rect(xStatsDistFromLeft, yStatsDistFromTop, xStatsDistFromLeft, yStatsDistFromTop));
	lblStats->SetFont(FontMgr::Load("five_cents_mouseover.fnt"));
	lblStats->SetFontScale(statsFontScale);
	menuVictory.AddChild(lblStats);

	menuVictory.AddChild(btnContinue);
	menuVictory.AddChild(btnExit);

	// 
	// About menu / credits
	//
	{
		menuAbout.SetDimensions(GWindow::Rect(0, 0, 800, 600));
		menuAbout.SetCallback(this);

		const float statsFontScale = 0.6f;
		const int	xStatsDistFromLeft = 50;
		const int	yStatsDistFromTop = 50;

		lblAbout = new GWindow(ABOUT_TEXT, GWindow::Rect(xStatsDistFromLeft, yStatsDistFromTop, xStatsDistFromLeft, yStatsDistFromTop));
		lblAbout->SetFont(font);
		lblAbout->SetFontScale(statsFontScale);
		menuAbout.AddChild(lblAbout);

		menuAbout.AddChild(btnPlay);
		menuAbout.AddChild(btnMultiplayer);
		menuAbout.AddChild(btnOptions);
		menuAbout.AddChild(btnAbout);
		menuAbout.AddChild(btnHelp);
		menuAbout.AddChild(btnExit);
	}

	//
	// help menu
	//
	{
		const float fontScale = 0.7f;

		menuHelp.SetDimensions(GWindow::Rect(0, 0, 800, 600));
		menuHelp.SetCallback(this);

		int	x = 50;
		int	y = 50;

		imgLeftKeyboard = new GWindow("", GWindow::Rect(x, y, x + 139, y + 111));
		imgLeftKeyboard->SetBackgroundImage(TextureMgr::LoadTex("LeftKeyboard.tga"));
		menuHelp.AddChild(imgLeftKeyboard);

		x = 250;
		y = 180;

		imgRightKeyboard = new GWindow("", GWindow::Rect(x, y, x + 165, y + 111));
		imgRightKeyboard->SetBackgroundImage(TextureMgr::LoadTex("RightKeyboard.tga"));
		menuHelp.AddChild(imgRightKeyboard);

		x = 250;
		y = 70;

		lblLeftKeyboard = new GWindow("Left Keyboard", GWindow::Rect(x, y, x, y));
		lblLeftKeyboard->SetFont(font);
		lblLeftKeyboard->SetFontScale(fontScale);
		menuHelp.AddChild(lblLeftKeyboard);

		x = 50;
		y = 200;

		lblRightKeyboard = new GWindow("Right Keyboard", GWindow::Rect(x, y, x, y));
		lblRightKeyboard->SetFont(font);
		lblRightKeyboard->SetFontScale(fontScale);
		menuHelp.AddChild(lblRightKeyboard);


		x = 50;
		y = 330;

		imgPickups1 = new GWindow("", GWindow::Rect(x, y, x + 64, y + 256));
		imgPickups1->SetBackgroundImage(TextureMgr::LoadTex("Pickups1.tga"));
		menuHelp.AddChild(imgPickups1);

		x = 250;
		y = 330;

		imgPickups2 = new GWindow("", GWindow::Rect(x, y, x + 64, y + 256));
		imgPickups2->SetBackgroundImage(TextureMgr::LoadTex("Pickups2.tga"));
		menuHelp.AddChild(imgPickups2);


		x = 120;
		y = 330;

		lblPickups1 = new GWindow("Bomb\nFire\nKick\nShield\nSpeed", GWindow::Rect(x, y, x, y));
		lblPickups1->SetFont(font);
		lblPickups1->SetFontScale(fontScale);
		menuHelp.AddChild(lblPickups1);

		x = 320;
		y = 330;

		lblPickups2 = new GWindow("Reverse\nSlow\nDisease\nTeleport\nRandom", GWindow::Rect(x, y, x, y));
		lblPickups2->SetFont(font);
		lblPickups2->SetFontScale(fontScale);
		menuHelp.AddChild(lblPickups2);


		menuHelp.AddChild(btnPlay);
		menuHelp.AddChild(btnMultiplayer);
		menuHelp.AddChild(btnOptions);
		menuHelp.AddChild(btnAbout);
		menuHelp.AddChild(btnHelp);
		menuHelp.AddChild(btnExit);
	}


	//
	// options
	//
	{
		const float fontScale = 0.8f;
		const int	yHeight = 50;
		const int	yPad = 0;

		const int	xWidth = 200;
		const int	xDistFromLeft = 50;
		const int	yDistFromTop = 50;

		const int	xColumnDist = 250;

		float x = xDistFromLeft;
		float y = yDistFromTop;

		menuOptions.SetDimensions(GWindow::Rect(0, 0, 800, 600));
		menuOptions.SetCallback(this);

		int width = 800;
		int height = 600;
		ExplosiveGame::GetInstance().GetIniFile().GetValue("video", "width", width);
		ExplosiveGame::GetInstance().GetIniFile().GetValue("video", "height", height);
		ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "width", width);
		ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "height", height);
		char buffer[256];
		sprintf(buffer, "%ix%i", width, height);
		
		lbpResolution.window = new GWindow("Resolution", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
		lbpResolution.window->SetFont(font);
		lbpResolution.window->SetFontScale(fontScale);

		lbpResolution.button = new GButton(buffer, GWindow::Rect(x + xColumnDist, y, x + xWidth + xColumnDist, y + yHeight));
		lbpResolution.button->SetFont(font);
		lbpResolution.button->SetFontScale(fontScale);

		menuOptions.AddChild(lbpResolution.button);
		menuOptions.AddChild(lbpResolution.window);


		y += yHeight + yPad;
		bool fullscreen = false;
		ExplosiveGame::GetInstance().GetIniFile().GetValue("video", "fullscreen", fullscreen);
		ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "fullscreen", fullscreen);

		lbpFullscreen.window = new GWindow("Window", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
		lbpFullscreen.window->SetFont(font);
		lbpFullscreen.window->SetFontScale(fontScale);

		lbpFullscreen.button = new GButton(fullscreen ? "fullscreen" : "window", GWindow::Rect(x + xColumnDist, y, x + xWidth + xColumnDist, y + yHeight));
		lbpFullscreen.button->SetFont(font);
		lbpFullscreen.button->SetFontScale(fontScale);

		menuOptions.AddChild(lbpFullscreen.button);
		menuOptions.AddChild(lbpFullscreen.window);

		y += yHeight + yPad;
		int aa = 2;
		ExplosiveGame::GetInstance().GetIniFile().GetValue("video", "antialiasing", aa);
		ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "antialiasing", aa);
		sprintf(buffer, "%ix", aa);

		lbpAntialiasing.window = new GWindow("Antialiasing", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
		lbpAntialiasing.window->SetFont(font);
		lbpAntialiasing.window->SetFontScale(fontScale);

		lbpAntialiasing.button = new GButton(aa == 0 ? "off" : buffer, GWindow::Rect(x + xColumnDist, y, x + xWidth + xColumnDist, y + yHeight));
		lbpAntialiasing.button->SetFont(font);
		lbpAntialiasing.button->SetFontScale(fontScale);

		menuOptions.AddChild(lbpAntialiasing.button);	
		menuOptions.AddChild(lbpAntialiasing.window);	

		y += (yHeight + yPad) * 6.0f;
		lblRestart = new GWindow("Any changes require restarting the game\nto take affect", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
		lblRestart->SetFont(font);
		lblRestart->SetFontScale(0.5f);
		menuOptions.AddChild(lblRestart);	

		menuOptions.AddChild(btnPlay);
		menuOptions.AddChild(btnMultiplayer);
		menuOptions.AddChild(btnOptions);
		menuOptions.AddChild(btnAbout);
		menuOptions.AddChild(btnHelp);
		menuOptions.AddChild(btnExit);
	}

	//
	// player setup
	//
	{
		const float fontScale = 1.0f;
		const int yBtnHeight = 60;
		const int yBtnPad = 0;

		const int xBtnWidth = 250;
		const int xBtnDistFromRight = 20;

		const int yBtnDistFromTop = 50;

		float x = (800 - xBtnWidth) - xBtnDistFromRight;
		float y = yBtnDistFromTop;

		btnPlayerSetup = new GButton("Player Setup", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
		btnPlayerSetup->SetFont(font);

		y += yBtnHeight + yBtnPad;
		btnGameOptions = new GButton("Game Options", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
		btnGameOptions->SetFont(font);

		y += yBtnHeight + yBtnPad;
		btnMapSelection = new GButton("Map Selection", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
		btnMapSelection->SetFont(font);

		y += yBtnHeight + yBtnPad;
		btnStart = new GButton("Start", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
		btnStart->SetFont(font);

		{	
			const float fontScale = 0.8f;
			const int	yHeight = 50;
			const int	yPad = 0;

			const int	xWidth = 200;
			const int	xDistFromLeft = 50;
			const int	yDistFromTop = 50;

			const int	xColumnDist = 150;

			float x = xDistFromLeft;
			float y = yDistFromTop;

			for (int i = 0; i < MAX_PLAYERS; ++i)
			{
				char strPlayer[256];
				sprintf(strPlayer, "%i", i);
				std::string str = ExplosiveGame::GetInstance().GetIniFile().GetValue("players", strPlayer);
				const char* strController = str.c_str();

				players[i].window = new GWindow(ExplosiveCharacter::GetPlayerName(i), GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
				players[i].window->SetFont(font);
				players[i].window->SetFontScale(fontScale);

				players[i].button = new GButton("AI", GWindow::Rect(x +xColumnDist, y, x + xWidth + xColumnDist, y + yHeight));

				// player 0 should be right keyboard by default
				if (i == 0)
					players[i].button->SetText("Right Keyboard");

				if (strController && strlen(strController))
					players[i].button->SetText(strController);
				else
					ExplosiveGame::GetInstance().GetIniFile().SetValue("players", strPlayer, players[i].button->GetText());


				players[i].button->SetFont(font);
				players[i].button->SetFontScale(fontScale);
			
				menuPlayerSetup.AddChild(players[i].window);
				menuPlayerSetup.AddChild(players[i].button);

				y += yHeight + yPad;
			}

			menuPlayerSetup.SetCallback(this);
			menuPlayerSetup.AddChild(btnStart);
			menuPlayerSetup.AddChild(btnPlayerSetup);
			menuPlayerSetup.AddChild(btnGameOptions);
			menuPlayerSetup.AddChild(btnMapSelection);
			menuPlayerSetup.AddChild(btnBack);
		}
	}

	//
	// game options menu
	//
	{
		const float fontScale = 0.8f;
		const int	yHeight = 50;
		const int	yPad = 0;

		const int	xWidth = 200;
		const int	xDistFromLeft = 50;
		const int	yDistFromTop = 50;

		const int	xColumnDist = 250;

		float x = xDistFromLeft;
		float y = yDistFromTop;

		lbpGameMode.window = new GWindow("Game Mode", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
		lbpGameMode.window->SetFont(font);
		lbpGameMode.window->SetFontScale(fontScale);

		lbpGameMode.button = new GButton(ExplosiveGame::GetInstance().GetCurrentGameMode()->GetName(), GWindow::Rect(x +xColumnDist, y, x + xWidth + xColumnDist, y + yHeight));
		lbpGameMode.button->SetFont(font);
		lbpGameMode.button->SetFontScale(fontScale);

		menuGameOptions.AddChild(lbpGameMode.window);
		menuGameOptions.AddChild(lbpGameMode.button);

		y += yHeight + yPad;
		lbpRoundsPerMap.window = new GWindow("Rounds per map", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
		lbpRoundsPerMap.window->SetFont(font);
		lbpRoundsPerMap.window->SetFontScale(fontScale);

		char buffer[256];
		sprintf(buffer, "%i", ExplosiveGame::GetInstance().GetRoundsPerMap());
		lbpRoundsPerMap.button = new GButton(buffer, GWindow::Rect(x +xColumnDist, y, x + xWidth + xColumnDist, y + yHeight));
		lbpRoundsPerMap.button->SetFont(font);
		lbpRoundsPerMap.button->SetFontScale(fontScale);

		menuGameOptions.AddChild(lbpRoundsPerMap.window);
		menuGameOptions.AddChild(lbpRoundsPerMap.button);

		
		y += yHeight + yPad;
		lbpTimeLimit.window = new GWindow("Time Per Round", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
		lbpTimeLimit.window->SetFont(font);
		lbpTimeLimit.window->SetFontScale(fontScale);

		sprintf(buffer, "%i", ExplosiveGame::GetInstance().GetTimeLimit());
		lbpTimeLimit.button = new GButton(buffer, GWindow::Rect(x +xColumnDist, y, x + xWidth + xColumnDist, y + yHeight));
		lbpTimeLimit.button->SetFont(font);
		lbpTimeLimit.button->SetFontScale(fontScale);

		menuGameOptions.AddChild(lbpTimeLimit.window);
		menuGameOptions.AddChild(lbpTimeLimit.button);


		y += yHeight + yPad;
		lbpAICanWin.window = new GWindow("AI can win", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
		lbpAICanWin.window->SetFont(font);
		lbpAICanWin.window->SetFontScale(fontScale);

		lbpAICanWin.button = new GButton(ExplosiveGame::GetInstance().GetAICanWin() ? "true" : "false", GWindow::Rect(x +xColumnDist, y, x + xWidth + xColumnDist, y + yHeight));
		lbpAICanWin.button->SetFont(font);
		lbpAICanWin.button->SetFontScale(fontScale);

		menuGameOptions.AddChild(lbpAICanWin.window);
		menuGameOptions.AddChild(lbpAICanWin.button);

		y += yHeight + yPad;
		lbpAIInStats.window = new GWindow("AI in stats", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
		lbpAIInStats.window->SetFont(font);
		lbpAIInStats.window->SetFontScale(fontScale);

		lbpAIInStats.button = new GButton(ExplosiveGame::GetInstance().GetAIInStats() ? "true" : "false", GWindow::Rect(x +xColumnDist, y, x + xWidth + xColumnDist, y + yHeight));
		lbpAIInStats.button->SetFont(font);
		lbpAIInStats.button->SetFontScale(fontScale);

		menuGameOptions.AddChild(lbpAIInStats.window);
		menuGameOptions.AddChild(lbpAIInStats.button);


		menuGameOptions.SetCallback(this);
		menuGameOptions.AddChild(btnPlayerSetup);
		menuGameOptions.AddChild(btnGameOptions);
		menuGameOptions.AddChild(btnMapSelection);
		menuGameOptions.AddChild(btnStart);
		menuGameOptions.AddChild(btnBack);
	}

	//
	// map selection
	//
	{	
		Array<std::string> levels;
		ExplosiveEnvironmentMgr::GetInstance().GetLevelList(levels, "data/levels");

		const float fontScale = 0.7f;
		const int	yHeight = 300;
		const int	yPad = 20;
		const int	xPad = 20;

		const int	xWidth = 400;
		const int	xDistFromLeft = 50;
		const int	yDistFromTop = 50;

		const int	xColumnDist = 150;

		float x = xDistFromLeft;
		float y = yDistFromTop;

		float xImgWidth = 171;
		float yImgHeight = 128;

		lblLevelImage = new GWindow(0, GWindow::Rect(x, y, x + xImgWidth, y + yImgHeight));
		lblLevelImage->SetFont(font);
		lblLevelImage->SetFontScale(fontScale);
		menuMapSelection.AddChild(lblLevelImage);

		float xLabelWidth = 256;
		float yLabelHeight = 200;

		x += xImgWidth + xPad;
		lblLevelDetails = new GWindow(0, GWindow::Rect(x, y, x + xLabelWidth, y + yLabelHeight));
		lblLevelDetails->SetFont(font);
		lblLevelDetails->SetFontScale(fontScale);
		menuMapSelection.AddChild(lblLevelDetails);

		x = xDistFromLeft;
		y += yLabelHeight + yPad;
		enmLevels.SetArrowTexture(TextureMgr::LoadTex("button_right.tga"));
		enmLevels.SetScrollBarTexture(TextureMgr::LoadTex("button_generic.tga"));
		enmLevels.SetTextureWidth(40);
		enmLevels.SetFont(font);
		enmLevels.SetFontScale(fontScale);
		enmLevels.SetDimensions(GWindow::Rect(x, y, x + xWidth, y + yHeight));		
		menuMapSelection.AddChild(&enmLevels);

		for (unsigned int i = 0; i < levels.GetSize(); ++i)
		{
			enmLevels.AddElement(levels[i].c_str());
		}

		menuMapSelection.SetCallback(this);
		menuMapSelection.AddChild(btnStart);
		menuMapSelection.AddChild(btnPlayerSetup);
		menuMapSelection.AddChild(btnGameOptions);
		menuMapSelection.AddChild(btnMapSelection);
		menuMapSelection.AddChild(btnBack);

		UpdateMapDetails();
	}

	//
	// join LAN
	//
	{
		menuJoinLAN.SetDimensions(GWindow::Rect(0, 0, 800, 600));
		menuJoinLAN.SetCallback(this);

		{
			const float fontScale = 0.7f;
			const int	yHeight = 400;
			const int	yPad = 20;

			const int	xWidth = 400;
			const int	xDistFromLeft = 50;
			const int	yDistFromTop = 50;

			const int	xColumnDist = 150;

			float x = xDistFromLeft;
			float y = yDistFromTop;

			enmLAN.SetArrowTexture(TextureMgr::LoadTex("button_right.tga"));
			enmLAN.SetScrollBarTexture(TextureMgr::LoadTex("button_generic.tga"));
			enmLAN.SetTextureWidth(40);
			enmLAN.SetFont(font);
			enmLAN.SetFontScale(fontScale);
			enmLAN.SetDimensions(GWindow::Rect(x, y, x + xWidth, y + yHeight));		
			menuJoinLAN.AddChild(&enmLAN);

			y += (yHeight + yPad);
			lblRefreshLAN = new GWindow("Click LAN to refresh", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
			lblRefreshLAN->SetFont(font);
			lblRefreshLAN->SetFontScale(0.5f);
			menuJoinLAN.AddChild(lblRefreshLAN);	
		}

		float x = (800 - xBtnWidth) - xBtnDistFromRight;
		float y = yBtnDistFromTop;
		btnLAN = new GButton("LAN", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
		btnLAN->SetFont(font);
		btnLAN->SetFontScale(fontScale);
		menuJoinLAN.AddChild(btnLAN);
#ifdef INTERNETPLAY
		y += yBtnHeight + yBtnPad;
		btnInternet = new GButton("Internet", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
		btnInternet->SetFont(font);
		btnInternet->SetFontScale(fontScale);
		menuJoinLAN.AddChild(btnInternet);	
#endif
		y += yBtnHeight + yBtnPad;
		btnMultiplayerOptions = new GButton("Options", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
		btnMultiplayerOptions->SetFont(font);
		btnMultiplayerOptions->SetFontScale(fontScale);
		menuJoinLAN.AddChild(btnMultiplayerOptions);	

		y += yBtnHeight + yBtnPad;
		btnHost = new GButton("Host", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
		btnHost->SetFont(font);
		btnHost->SetFontScale(fontScale);
		menuJoinLAN.AddChild(btnHost);

		y += yBtnHeight + yBtnPad;
		btnJoin = new GButton("Join", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
		btnJoin->SetFont(font);
		btnJoin->SetFontScale(fontScale);
		menuJoinLAN.AddChild(btnJoin);

		y += yBtnHeight + yBtnPad;
		btnJoinIP = new GButton("Join IP", GWindow::Rect(x, y, x + xBtnWidth, y + yBtnHeight));
		btnJoinIP->SetFont(font);
		btnJoinIP->SetFontScale(fontScale);
		menuJoinLAN.AddChild(btnJoinIP);

		menuJoinLAN.AddChild(btnBack);
	}

	//
	// join internet
	//
	{
		menuJoinInternet.SetDimensions(GWindow::Rect(0, 0, 800, 600));
		menuJoinInternet.SetCallback(this);

		{
			const float fontScale = 0.7f;
			const int	yHeight = 400;
			const int	yPad = 20;

			const int	xWidth = 400;
			const int	xDistFromLeft = 50;
			const int	yDistFromTop = 50;

			const int	xColumnDist = 150;

			float x = xDistFromLeft;
			float y = yDistFromTop;

			enmInternet.SetArrowTexture(TextureMgr::LoadTex("button_right.tga"));
			enmInternet.SetScrollBarTexture(TextureMgr::LoadTex("button_generic.tga"));
			enmInternet.SetTextureWidth(40);
			enmInternet.SetFont(font);
			enmInternet.SetFontScale(fontScale);
			enmInternet.SetDimensions(GWindow::Rect(x, y, x + xWidth, y + yHeight));		
			menuJoinInternet.AddChild(&enmInternet);

			y += (yHeight + yPad);
			lblRefreshInternet = new GWindow("Click Internet to refresh", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
			lblRefreshInternet->SetFont(font);
			lblRefreshInternet->SetFontScale(0.5f);
			menuJoinInternet.AddChild(lblRefreshInternet);
		}

		menuJoinInternet.AddChild(btnLAN);
#ifdef INTERNETPLAY
		menuJoinInternet.AddChild(btnInternet);	
#endif
		menuJoinInternet.AddChild(btnHost);
		menuJoinInternet.AddChild(btnMultiplayerOptions);
		menuJoinInternet.AddChild(btnJoin);
		menuJoinInternet.AddChild(btnJoinIP);
		menuJoinInternet.AddChild(btnBack);
	}

	

	//
	// multiplayer options
	//
	{
		menuMultiplayerOptions.SetDimensions(GWindow::Rect(0, 0, 800, 600));
		menuMultiplayerOptions.SetCallback(this);

		{
			const float fontScale = 0.8f;
			const int	yHeight = 50;
			const int	yPad = 0;

			const int	xWidth = 200;
			const int	xDistFromLeft = 50;
			const int	yDistFromTop = 50;

			const int	xColumnDist = 150;

			float x = xDistFromLeft;
			float y = yDistFromTop;


			//y += (yHeight + yPad) * 2;
			lbpControls.window = new GWindow("Input", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
			lbpControls.window->SetFont(font);
			lbpControls.window->SetFontScale(fontScale);

			
			lbpControls.button = new GButton("Right Keyboard", GWindow::Rect(x + xColumnDist, y, x + xWidth + xColumnDist, y + yHeight));
			lbpControls.button->SetFont(font);
			lbpControls.button->SetFontScale(fontScale);

			// set up controls from ini
			std::string str = ExplosiveGame::GetInstance().GetIniFile().GetValue("network", "controls");
			const char* strController = str.c_str();
			
			if (strController && strlen(strController))
				lbpControls.button->SetText(strController);
			else
				ExplosiveGame::GetInstance().GetIniFile().SetValue("network", "controls", lbpControls.button->GetText());


			menuMultiplayerOptions.AddChild(lbpControls.window);
			menuMultiplayerOptions.AddChild(lbpControls.button);

			y += (yHeight + yPad) * 2;
			lipClientName.window = new GWindow("Client Name", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
			lipClientName.window->SetFont(font);
			lipClientName.window->SetFontScale(fontScale);

			y += yHeight + yPad;
			lipClientName.input = new GInput(GWindow::Rect(x + xColumnDist / 5, y, x + xWidth + xColumnDist / 5, y + yHeight));
			lipClientName.input->SetText(ExplosiveGame::GetInstance().GetInstance().GetClientName());
			lipClientName.input->SetFont(font);
			lipClientName.input->SetFontScale(fontScale);
			lipClientName.input->SetInputFocus(true);

			menuMultiplayerOptions.AddChild(lipClientName.window);
			menuMultiplayerOptions.AddChild(lipClientName.input);


			y += (yHeight + yPad) * 2;
			lipServerName.window = new GWindow("Server Name", GWindow::Rect(x, y, x/* + xWidth*/, y/* + yHeight*/));
			lipServerName.window->SetFont(font);
			lipServerName.window->SetFontScale(fontScale);

			y += yHeight + yPad;
			lipServerName.input = new GInput(GWindow::Rect(x + xColumnDist / 5, y, x + xWidth + xColumnDist / 5, y + yHeight));
			lipServerName.input->SetText(ExplosiveGame::GetInstance().GetInstance().GetServerName());
			lipServerName.input->SetFont(font);
			lipServerName.input->SetFontScale(fontScale);
			lipServerName.input->SetInputFocus(false);

			menuMultiplayerOptions.AddChild(lipServerName.window);
			menuMultiplayerOptions.AddChild(lipServerName.input);
		}

		menuMultiplayerOptions.AddChild(btnLAN);
#ifdef INTERNETPLAY
		menuMultiplayerOptions.AddChild(btnInternet);
#endif
		menuMultiplayerOptions.AddChild(btnMultiplayerOptions);
		menuMultiplayerOptions.AddChild(btnHost);
		menuMultiplayerOptions.AddChild(btnJoin);
		menuMultiplayerOptions.AddChild(btnJoinIP);
		menuMultiplayerOptions.AddChild(btnBack);
	}


	//
	// Join IP
	//
	menuJoinIP.SetDimensions(GWindow::Rect(0, 0, 800, 600));
	menuJoinIP.SetCallback(this);

	ipIP = new GInput(GWindow::Rect(100, 150, 50, 150));
	ipIP->SetInputFocus(true);
	ipIP->SetFont(font);
	ipIP->SetFontScale(0.7f);

	lblIP = new GWindow("IP:", GWindow::Rect(50, 150, 50, 150));
	lblIP->SetFont(font);
	lblIP->SetFontScale(0.7f);

	menuJoinIP.AddChild(btnLAN);
#ifdef INTERNETPLAY
	menuJoinIP.AddChild(btnInternet);	
#endif
	menuJoinIP.AddChild(btnMultiplayerOptions);
	menuJoinIP.AddChild(btnHost);
	menuJoinIP.AddChild(btnJoin);
	menuJoinIP.AddChild(btnJoinIP);
	menuJoinIP.AddChild(btnBack);
	menuJoinIP.AddChild(ipIP);
	menuJoinIP.AddChild(lblIP);

	//
	// Chat
	//
	menuChat.SetDimensions(GWindow::Rect(0, 0, 800, 600));
	menuChat.SetCallback(this);

	ipChat = new GInput(ipIP->GetDimensions());
	ipChat->SetInputFocus(false);
	ipChat->SetFont(font);
	ipChat->SetFontScale(0.7f);

	lblChat = new GWindow("Say:", lblIP->GetDimensions());
	lblChat->SetFont(font);
	lblChat->SetFontScale(0.7f);

	menuChat.AddChild(ipChat);
	menuChat.AddChild(lblChat);

	menuChatBox.SetDimensions(GWindow::Rect(0, 0, 800, 600));
	menuChatBox.SetCallback(this);

	enmChatBox.SetFont(font);
	enmChatBox.SetFontScale(0.4f);
	enmChatBox.SetDimensions(GWindow::Rect(5, 5, 50, 150));		
	menuChatBox.AddChild(&enmChatBox);

	// make main menu visible
	DisplayMenu(Main);
}

bool Menu::AddPlayerFromButton(int idx, GButton* btn, int characterId)
{
	if (stricmp(btn->GetText(), "Left Keyboard") == 0)
	{
		ExplosiveGame::GetInstance().AddHumanCharacter(idx, ExplosiveInputController::Left, characterId);
		return true;
	}
	else if (stricmp(btn->GetText(), "Right Keyboard") == 0)
	{
		ExplosiveGame::GetInstance().AddHumanCharacter(idx, ExplosiveInputController::Right, characterId);
		return true;
	}
	else if (stricmp(btn->GetText(), "AI") == 0)
	{
		ExplosiveGame::GetInstance().AddAICharacter(idx, characterId);
		return true;
	}
	else
	{
		// check for joystick
		int joyCount = Input::GetInstance().GetJoystickCount();
		for (int j = 0; j < joyCount; ++j)
		{
			char buffer[256];
			sprintf(buffer, "Joystick %i", j);

			if (stricmp(btn->GetText(), buffer) == 0)
			{
				ExplosiveGame::GetInstance().AddHumanCharacter(idx, j, characterId);
			}
		}
		return true;
	}

	return false;
}

void Menu::OnMousePressed(GWindow* child)
{
	// if element of the enum control
	int idx = enmLevels.GetElementIndex(child);
	if (idx == enmLevels.GetSelectedIndex())
	{
		int lastIdx = enmLevels.GetLastSelectedIndex();
		if (lastIdx != idx)
		{
			GWindow* last = enmLevels.GetElementWindow(lastIdx);
			last->SetFont(FontMgr::Load("five_cents.fnt"));
		}

		child->SetFont(FontMgr::Load("five_cents_mouseover.fnt"));
		UpdateMapDetails();
		return;
	}

	// if element of the enum control
	idx = enmLAN.GetElementIndex(child);
	if (idx == enmLAN.GetSelectedIndex())
	{
		int lastIdx = enmLAN.GetLastSelectedIndex();
		if (lastIdx != idx)
		{
			GWindow* last = enmLAN.GetElementWindow(lastIdx);
			last->SetFont(FontMgr::Load("five_cents.fnt"));
		}

		child->SetFont(FontMgr::Load("five_cents_mouseover.fnt"));
		return;
	}

	// if element of the enum control
	idx = enmInternet.GetElementIndex(child);
	if (idx == enmInternet.GetSelectedIndex())
	{
		int lastIdx = enmInternet.GetLastSelectedIndex();
		if (lastIdx != idx)
		{
			GWindow* last = enmInternet.GetElementWindow(lastIdx);
			last->SetFont(FontMgr::Load("five_cents.fnt"));
		}

		child->SetFont(FontMgr::Load("five_cents_mouseover.fnt"));
		return;
	}

	//
	// main menu
	//
	if (child == btnPlay)
	{
		DisplayMenu(PlayerSetup);
		multiplayer = false;
	}

	if (child == btnGameOptions)
		DisplayMenu(GameOptions);

	if (child == btnOptions)
		DisplayMenu(Options);	

	if (child == btnMultiplayerOptions)
		DisplayMenu(MultiplayerOptions);

	if (child == btnExit)
	{
		if (NetServer::IsValid() || NetClient::IsValid())
			ExplosiveGame::GetInstance().Disconnect();

		if (multiplayer)
		{
			ExplosiveGame::GetInstance().RemoveServerFromMaster();
		}

		if (visibleMenu == Pause || visibleMenu == Victory)
		{
			// destroy network shit
			if (NetServer::IsValid())
			{
				NetServer::GetInstance().Shutdown();
				NetServer::Destroy();
			}

			if (NetClient::IsValid())
			{
				NetClient::GetInstance().Shutdown();
				NetClient::Destroy();
			}

			ExplosiveGame::GetInstance().EndGame();
		}
		else
		{
			shouldExit = true;
		}
	}

	if (child == btnBack)
	{
		if (multiplayer && (visibleMenu == PlayerSetup || visibleMenu == MapSelection || visibleMenu == GameOptions))
			DisplayMenu(LAN);
		else
			DisplayMenu(Main);
	}

	//
	// resume menu
	//
	if (child == btnContinue)
	{
		if (visibleMenu == Pause)
		{
			ExplosiveGame::GetInstance().SetResume();
		}
		else if (visibleMenu == Victory)
		{
			ExplosiveGame::GetInstance().SetRestart();
		}
	}

	if (child == btnRestart)
	{
		DisplayMenu(None);
		ExplosiveGame::GetInstance().SetRestart();
	}	

	if (child == btnStart)
	{
		DisplayMenu(None);
		Device::GetInstance().SetClearColour(0.0, 0.0, 0.0);
		ExplosiveGame::GetInstance().SetServerName(lipServerName.input->GetText());
		ExplosiveGame::GetInstance().SetClientName(lipClientName.input->GetText());

		if (multiplayer)
		{
			// create a server
			NetServer::Create();
			NetServer::GetInstance().Init(EXPLOSIVE_GAME_ID, 100, MASTER_SERVER_URL);
			NetServer::GetInstance().Listen(SERVER_PORT, NetworkBase::NF_ReliableOnly, ExplosiveGame::GetInstance().GetServerName());
			ExplosiveGame::GetInstance().GetMasterServer().StartNATPunchThroughCheck(&NetServer::GetInstance().GetRDPStream(), CLIENT_PORT, NAT_PUNCH_CHECK);
		}

		const char* level = enmLevels.GetElement(enmLevels.GetSelectedIndex());
		ExplosiveEnvironmentMgr::GetInstance().LoadMap(level);

		for (int i = 0; i < MAX_PLAYERS; ++i)
		{
			AddPlayerFromButton(i, players[i].button);
			/*
			if (stricmp(players[i].button->GetText(), "Left Keyboard") == 0)
			{
				ExplosiveGame::GetInstance().AddHumanCharacter(i, ExplosiveInputController::Left);
			}
			else if (stricmp(players[i].button->GetText(), "Right Keyboard") == 0)
			{
				ExplosiveGame::GetInstance().AddHumanCharacter(i, ExplosiveInputController::Right);
			}
			else if (stricmp(players[i].button->GetText(), "AI") == 0)
			{
				ExplosiveGame::GetInstance().AddAICharacter(i);
			}
			else
			{
				// check for joystick
				int joyCount = Input::GetInstance().GetJoystickCount();
				for (int j = 0; j < joyCount; ++j)
				{
					char buffer[256];
					sprintf(buffer, "Joystick %i", j);

					if (stricmp(players[i].button->GetText(), buffer) == 0)
					{
						ExplosiveGame::GetInstance().AddHumanCharacter(i, j);
					}
				}
			}*/

		}

		if (multiplayer)
			ExplosiveGame::GetInstance().AddServerToMaster();

		ExplosiveGame::GetInstance().Start();
	}

	//
	// Muliplayer
	//
	if (child == btnHost)
	{
		DisplayMenu(PlayerSetup);
		multiplayer = true;
	}

	if (child == btnMultiplayer)
	{
		multiplayer = true;
		DisplayMenu(LAN);
		UpdateLAN();
	}

	if (child == btnLAN)
	{
		DisplayMenu(LAN);
		UpdateLAN();
	}

	if (child == btnInternet)
	{
		DisplayMenu(Internet);
		UpdateInternet();
	}

	if (child == btnJoinIP)
	{
		DisplayMenu(JoinIP);
	}

	if (child == btnAbout)
	{
		DisplayMenu(About);
	}

	if (child == btnHelp)
	{
		DisplayMenu(Help);
	}

	//
	// Join
	//
	if (child == btnJoin)
	{
		ExplosiveGame::GetInstance().SetServerName(lipServerName.input->GetText());
		ExplosiveGame::GetInstance().SetClientName(lipClientName.input->GetText());

		const char* server = 0;
		if (visibleMenu == LAN)
		{
			server = lanServers[enmLAN.GetSelectedIndex()].ip.c_str();
		}
		else if (visibleMenu == Internet)
		{
			server = internetServers[enmInternet.GetSelectedIndex()].ip.c_str();

			MasterServer& masterServer = ExplosiveGame::GetInstance().GetMasterServer();
			masterServer.RequestJoin(server);
		}
		else if (visibleMenu == JoinIP)
		{
			server = ipIP->GetText();
		}

		if (!server || strlen(server) <= 0)
			return;

		NetClient::Create();
		NetClient::GetInstance().Init(EXPLOSIVE_GAME_ID);

		if (!NetClient::GetInstance().Connect(server, SERVER_PORT, CLIENT_PORT, NetworkBase::NF_ReliableOnly))
		{
			Log::Print("Failed to connect");
			NetClient::GetInstance().Shutdown();
			NetClient::Destroy();
			return;
		}

		DisplayMenu(None);
		Device::GetInstance().SetClearColour(0.0, 0.0, 0.0);

		ExplosiveGame::GetInstance().Start();
	}

	//
	// game options
	//
	if (child == lbpRoundsPerMap.button)
	{
		char buffer[256];
		strcpy(buffer, lbpRoundsPerMap.button->GetText());
		int count = 1;
		sscanf(buffer, "%i", &count);
		++count;
		count = count % 11;
		if (count == 0)
			count = 1;

		sprintf(buffer, "%i", count);
		child->SetText(buffer);

		ExplosiveGame::GetInstance().SetRoundsPerMap(count);
	}

	if (child == lbpTimeLimit.button)
	{
		char buffer[256];
		strcpy(buffer, lbpTimeLimit.button->GetText());
		int count = 1;
		sscanf(buffer, "%i", &count);
		++count;
		count = count % 11;
		if (count == 0)
			count = 1;

		sprintf(buffer, "%i", count);
		child->SetText(buffer);

		ExplosiveGame::GetInstance().SetTimeLimit(count);
	}

	if (child == lbpAIInStats.button)
	{
		bool aiInStats = true;
		if (strcmp(child->GetText(), "true") == 0)
			aiInStats = false;

		child->SetText(aiInStats ? "true" : "false");
		ExplosiveGame::GetInstance().SetAIInStats(aiInStats);
	}

	if (child == lbpAICanWin.button)
	{
		bool aiCanWin = true;
		if (strcmp(child->GetText(), "true") == 0)
			aiCanWin = false;

		child->SetText(aiCanWin ? "true" : "false");
		ExplosiveGame::GetInstance().SetAICanWin(aiCanWin);
	}

	if (child == lbpGameMode.button)
	{
		for (int i = 0; i < ExplosiveGame::GetInstance().GetNumGameModes(); ++i)
		{
			if (strcmp(ExplosiveGame::GetInstance().GetGameMode(i)->GetName(), child->GetText()) == 0)
			{
				++i;
				if (i >= ExplosiveGame::GetInstance().GetNumGameModes())
					i = 0;

				GameMode* gm = ExplosiveGame::GetInstance().GetGameMode(i);
				ExplosiveGame::GetInstance().SetActiveGameMode(gm );
				child->SetText(gm->GetName());
				break;
			}
		}
	}

	//
	// options
	//
	if (child == lbpFullscreen.button)
	{
		if (strcmp(child->GetText(), "fullscreen") == 0)
		{
			child->SetText("window");
			ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "fullscreen", false);
		}
		else
		{
			child->SetText("fullscreen");
			ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "fullscreen", true);
		}
	}

	if (child == lbpAntialiasing.button)
	{
		bool found = false;
		for (int i = 2; i <= 16; i *= 2)
		{
			char buffer[256];
			sprintf(buffer, "%ix", i);
			if (strcmp(buffer, child->GetText()) == 0)
			{
				i *= 2;
				if (i > 16)
				{
					child->SetText("off");
					ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "antialiasing", 0);
				}
				else
				{
					sprintf(buffer, "%ix", i);
					child->SetText(buffer);
					ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "antialiasing", i);
				}
				found = true;
				break;
			}
		}
		if (!found) // must be set to off
		{
			child->SetText("2x");
			ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "antialiasing", 2);
		}
	}

	if (child == lbpResolution.button)
	{
		if (strcmp(child->GetText(), "1600x1200") == 0)
		{
			child->SetText("800x600");
			ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "width", 800);
			ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "height", 600);
		}
		else if (strcmp(child->GetText(), "1280x1024") == 0)
		{
			if (Window::IsSupported(1600, 1200, 32))
			{
				child->SetText("1600x1200");
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "width", 1600);
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "height", 1200);
			}
			else
			{
				child->SetText("800x600");
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "width", 800);
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "height", 600);
			}
		}
		else if (strcmp(child->GetText(), "1024x768") == 0)
		{
			if (Window::IsSupported(1280, 1024, 32))
			{
				child->SetText("1280x1024");
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "width", 1280);
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "height", 1024);
			}
			else
			{
				child->SetText("800x600");
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "width", 800);
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "height", 600);
			}
		}
		else if (strcmp(child->GetText(), "800x600") == 0)
		{
			if (Window::IsSupported(1024, 768, 32))
			{
				child->SetText("1024x768");
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "width", 1024);
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "height", 768);
			}
			else
			{
				child->SetText("800x600");
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "width", 800);
				ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "height", 600);
			}
		}
		else
		{
			child->SetText("800x600");
			ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "width", 800);
			ExplosiveGame::GetInstance().GetIniFile().SetValue("video", "height", 600);
		}
	}

	//
	// player setup
	//
	if (child == btnMapSelection)
		DisplayMenu(MapSelection);

	for (int j = 0; j < MAX_PLAYERS; ++j)
	{
		if (child == players[j].button)
		{
			char strPlayer[256];
			sprintf(strPlayer, "%i", j);

			bool found = false;
			int joyCount = Input::GetInstance().GetJoystickCount();
			for (int i = 0; i < joyCount; ++i)
			{
				char buffer[256];
				sprintf(buffer, "Joystick %i", i);

				if (stricmp(child->GetText(), buffer) == 0)
				{
					if (i + 1 < joyCount)
					{
						sprintf(buffer, "Joystick %i", i + 1);
						child->SetText(buffer);
						ExplosiveGame::GetInstance().GetIniFile().SetValue("players", strPlayer, buffer);
						found = true;
						break;
					}
					else
					{
						child->SetText("AI");
						ExplosiveGame::GetInstance().GetIniFile().SetValue("players", strPlayer, "AI");
						found = true;
						break;
					}
				}
			}

			if (!found)
			{
			
				if (stricmp(child->GetText(), "Left Keyboard") == 0)
				{
					child->SetText("Right Keyboard");
					ExplosiveGame::GetInstance().GetIniFile().SetValue("players", strPlayer, "Right Keyboard");
				}
				else if (stricmp(child->GetText(), "Right Keyboard") == 0)
				{
					// if joystick, make it joystick 0
					int joyCount = Input::GetInstance().GetJoystickCount();
					if (joyCount)
					{
						child->SetText("Joystick 0");
						ExplosiveGame::GetInstance().GetIniFile().SetValue("players", strPlayer, "Joystick 0");
					}
					else
					{
						child->SetText("AI");
						ExplosiveGame::GetInstance().GetIniFile().SetValue("players", strPlayer, "AI");
					}
				}
				else if (stricmp(child->GetText(), "AI") == 0)
				{
					child->SetText("None");
					ExplosiveGame::GetInstance().GetIniFile().SetValue("players", strPlayer, "None");
				}
				else if (stricmp(child->GetText(), "None") == 0)
				{
					child->SetText("Left Keyboard");
					ExplosiveGame::GetInstance().GetIniFile().SetValue("players", strPlayer, "Left Keyboard");
				}
			}
		}
	}

	//
	// Multiplayer options
	//
	if (child == lipServerName.input)
	{
		lipClientName.input->SetInputFocus(false);
		lipServerName.input->SetInputFocus(true);
	}

	if (child == lipClientName.input)
	{
		lipClientName.input->SetInputFocus(true);
		lipServerName.input->SetInputFocus(false);
	}

	if (child == lbpControls.button)
	{
		bool found = false;
		int joyCount = Input::GetInstance().GetJoystickCount();
		for (int i = 0; i < joyCount; ++i)
		{
			char buffer[256];
			sprintf(buffer, "Joystick %i", i);

			if (stricmp(child->GetText(), buffer) == 0)
			{
				if (i + 1 < joyCount)
				{
					sprintf(buffer, "Joystick %i", i + 1);
					child->SetText(buffer);
					ExplosiveGame::GetInstance().GetIniFile().SetValue("network", "controls", buffer);
					found = true;
					break;
				}
				else
				{
					child->SetText("Right Keyboard");
					ExplosiveGame::GetInstance().GetIniFile().SetValue("network", "controls", "Right Keyboard");
					found = true;
					break;
				}
			}
		}

		if (!found)
		{
			if (stricmp(child->GetText(), "Right Keyboard") == 0)
			{
				child->SetText("Left Keyboard");
				ExplosiveGame::GetInstance().GetIniFile().SetValue("network", "controls", "Left Keyboard");
			}
			else if (stricmp(child->GetText(), "Left Keyboard") == 0)
			{
				// if joystick, make it joystick 0
				int joyCount = Input::GetInstance().GetJoystickCount();
				if (joyCount)
				{
					child->SetText("Joystick 0");
					ExplosiveGame::GetInstance().GetIniFile().SetValue("network", "controls", "Joystick 0");
				}
				else
				{
					child->SetText("Right Keyboard");
					ExplosiveGame::GetInstance().GetIniFile().SetValue("network", "controls", "Right Keyboard");
				}
			}
		}
	}


	// 
	// map selection
	//
	if (child == btnPlayerSetup)
		DisplayMenu(PlayerSetup);
}

void Menu::OnKeyPressed(GWindow* child, const std::string& pressedKey)
{
	if (child == ipIP || child == lipServerName.input || child == lipClientName.input)
	{
		std::string text = child->GetText();

		if (text.length() >= 255)
			return;

		if (pressedKey.compare("space") == 0)
		{
			text += " ";
			child->SetText(text.c_str());
			return;
		}
		else if (pressedKey.compare("return") == 0)
		{
			ExplosiveGame::GetInstance().SetServerName(lipServerName.input->GetText());
			ExplosiveGame::GetInstance().SetClientName(lipClientName.input->GetText());
			return;
		}
		else if (pressedKey.compare("backspace") == 0)
		{
			if (text.length() > 0)
			{
				text[text.length() - 1] = '\0';
				child->SetText(text.c_str());
			}
			return;
		}

		text += pressedKey;
		child->SetText(text.c_str());
	}

	if (child == ipChat)
	{
		std::string text = ipChat->GetText();

		if (pressedKey.compare("space") == 0)
		{
			text += " ";
			child->SetText(text.c_str());
			return;
		}
		else if (pressedKey.compare("return") == 0)
		{
			ExplosiveGame::GetInstance().SendChat(child->GetText());
			child->SetInputFocus(false);
			child->SetText("");
			return;
		}
		else if (pressedKey.compare("backspace") == 0)
		{
			if (text.length() > 0)
			{
				text[text.length() - 1] = '\0';
				child->SetText(text.c_str());
			}
			return;
		}

		text += pressedKey;
		child->SetText(text.c_str());
	}
}

void Menu::UpdateLAN()
{
	NetClient::Create();
	NetClient::GetInstance().Init(EXPLOSIVE_GAME_ID);

	lanServers.SetSize(0);
	NetClient::GetInstance().FindLANServers(lanServers);

	enmLAN.RemoveAllElements();

	for (int i = 0; i < lanServers.GetSize(); ++i)
	{
		char buffer[256];
		sprintf(buffer, "%s - %i", lanServers[i].name.c_str(), lanServers[i].numPlayers);
		enmLAN.AddElement(buffer);
	}

	NetClient::GetInstance().Shutdown();
	NetClient::GetInstance().Destroy();
}



void Menu::UpdateInternet()
{
	internetServers.SetSize(0);

	MasterServer& masterServer = ExplosiveGame::GetInstance().GetMasterServer();
	masterServer.Connect(MASTER_SERVER_URL, MASTER_SERVER_PATH, 80, EXPLOSIVE_GAME_ID);
	masterServer.GetServerList(internetServers);

	enmInternet.RemoveAllElements();

	for (int i = 0; i < internetServers.GetSize(); ++i)
	{
		char buffer[256];
		sprintf(buffer, "%s - %i", internetServers[i].name.c_str(), internetServers[i].numPlayers);
		enmInternet.AddElement(buffer);
	}
}

void Menu::OnMouseOver(GWindow* child)
{
	if (child == lblStats ||
		child == lblLevelDetails)
		return;

	child->SetFont(FontMgr::Load("five_cents_mouseover.fnt"));
}

void Menu::OnMouseOut(GWindow* child)
{
	if (child == lblStats ||
		child == lblLevelDetails)
		return;

	// if element of the enum control
	int idx = enmLevels.GetElementIndex(child);
	if (idx == enmLevels.GetSelectedIndex())
	{
		child->SetFont(FontMgr::Load("five_cents_mouseover.fnt"));
		return;
	}

	// if element of the enum control
	idx = enmLAN.GetElementIndex(child);
	if (idx == enmLAN.GetSelectedIndex())
	{
		child->SetFont(FontMgr::Load("five_cents_mouseover.fnt"));
		return;
	}

	// if element of the enum control
	idx = enmInternet.GetElementIndex(child);
	if (idx == enmInternet.GetSelectedIndex())
	{
		child->SetFont(FontMgr::Load("five_cents_mouseover.fnt"));
		return;
	}

	child->SetFont(FontMgr::Load("five_cents.fnt"));
}

void Menu::GenerateVictoryMenu()
{
	bool aiInStats = ExplosiveGame::GetInstance().GetAIInStats();
	std::string strWinner = "-";
	Array<Character*>& characters = ExplosiveGame::GetInstance().GetCharacters();

	// get the winner
	for (int i = 0; i < characters.GetSize(); ++i)
	{
		ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(characters[i]);
		ExplosiveController* control = static_cast<ExplosiveController*>(c->GetController());
		if (!c->IsDead())
		{
			// if this is an ai, and there are no ai in the stats, continue
			if (!aiInStats && !control->IsHuman())
				continue;

			if (ExplosiveGame::GetInstance().GetCurrentGameMode()->IsTeamGame())
			{
				strWinner = std::string(ExplosiveCharacter::GetPlayerName(c->GetTeamTag().teamIdx)) + std::string(" Team");
			}
			else
			{
				strWinner = c->GetName();
			}
			break;
		}
	}

	// most kills
	std::string strKills = "-";
	int mostKills = 0;
	for (int i = 0; i < characters.GetSize(); ++i)
	{
		ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(characters[i]);
		ExplosiveController* control = static_cast<ExplosiveController*>(c->GetController());

		Statistics& stats = c->GetStats();
		int kills = stats.GetKills();

		// if this is an ai, and there are no ai in the stats, continue
		if (!aiInStats && !control->IsHuman())
			continue;

		if (kills > mostKills)
		{
			mostKills = kills;
			strKills = c->GetName();
		}
	}

	// most pickups
	std::string strPickups = "-";
	int mostPickups = 0;
	for (int i = 0; i < characters.GetSize(); ++i)
	{
		ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(characters[i]);
		ExplosiveController* control = static_cast<ExplosiveController*>(c->GetController());

		Statistics& stats = c->GetStats();
		int pickups = stats.GetPickups();

		// if this is an ai, and there are no ai in the stats, continue
		if (!aiInStats && !control->IsHuman())
			continue;

		if (pickups > mostPickups)
		{
			mostPickups = pickups;
			strPickups = c->GetName();
		}
	}

	char buffer[1024];
	if (!ExplosiveGame::GetInstance().GetCurrentGameMode()->IsTeamGame())
	{
		sprintf(buffer, "Winner : %s\nKills : %s - %i\nMost Greedy : %s - %i", 
			strWinner.c_str(), strKills.c_str(), mostKills, strPickups.c_str(), mostPickups);
	}
	else
	{
		sprintf(buffer, "Winner : %s\nMost Greedy : %s - %i", 
			strWinner.c_str(), strPickups.c_str(), mostPickups);
	}

	lblStats->SetText(buffer);
}

void Menu::DisplayMenu(VisibleMenu menu)
{
	if (menu == Main)
		Device::GetInstance().SetClearColour(0.5, 0.5, 0.5);

	if (menu == Victory)
		GenerateVictoryMenu();

	visibleMenu = menu;
}

bool Menu::MenuDisplayed()
{
	return visibleMenu != None;
}

void Menu::Update()
{
	if (visibleMenu != None)
		cursor.Update();
	
	switch (visibleMenu)
	{
	case None:
		return;
	case Main:
		menuMain.Update(&cursor);
		break;
	case Options:
		menuOptions.Update(&cursor);
		break;
	case PlayerSetup:
		menuPlayerSetup.Update(&cursor);
		break;
	case MapSelection:
		menuMapSelection.Update(&cursor);
		break;
	case GameOptions:
		menuGameOptions.Update(&cursor);
		break;
	case Pause:
		menuPause.Update(&cursor);
		break;
	case Victory:
		menuVictory.Update(&cursor);
		break;
	case LAN:
		menuJoinLAN.Update(&cursor);
		break;
	case Internet:
		menuJoinInternet.Update(&cursor);
		break;
	case JoinIP:
		menuJoinIP.Update(&cursor);
		break;
	case Chat:
		menuChat.Update(&cursor);
		break;
	case About:
		menuAbout.Update(&cursor);
		break;
	case Help:
		menuHelp.Update(&cursor);
		break;
	case MultiplayerOptions:
		menuMultiplayerOptions.Update(&cursor);
		break;
	};
}

void Menu::Render()
{
	//if (visibleMenu == Pause)
	{
		glMatrixMode(GL_PROJECTION); // and initalize Ortho mode
  		glPushMatrix();
  		glLoadIdentity();
  		glOrtho(0.0f, 800, 600, 0.0f, -1.0f, 1.0f);

  		glMatrixMode(GL_MODELVIEW);
  		glPushMatrix();
		glLoadIdentity();

		glDisable(GL_DEPTH_TEST);
		glDepthFunc(GL_ALWAYS);
		glDepthMask(GL_FALSE);

		Texture::RestoreDefault();
		glColor4f(0.0f, 0.0f, 0.0f, 0.5f);

		GLint blendSrc;
		GLint blendDst;
		GLint blendOn;

		blendOn = glIsEnabled(GL_BLEND);
  		glGetIntegerv(GL_BLEND_SRC, &blendSrc);
		glGetIntegerv(GL_BLEND_DST, &blendDst);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBegin(GL_QUADS);
		{	
			glVertex2i(500, 0);
			glVertex2i(500, 600);
			glVertex2i(800, 600);	
			glVertex2i(800, 0);		
		}
		glEnd();

		if (blendOn)
			glEnable(GL_BLEND);
		else
			glDisable(GL_BLEND);

		glBlendFunc(blendSrc, blendDst);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glDepthMask(GL_TRUE);

		glMatrixMode(GL_PROJECTION);
  		glPopMatrix();
  		glMatrixMode(GL_MODELVIEW);
  		glPopMatrix();
	}

	switch (visibleMenu)
	{
	case None:
		return;
	case Main:
		menuMain.Render();
		break;
	case Options:
		menuOptions.Render();
		break;
	case PlayerSetup:
		menuPlayerSetup.Render();
		break;
	case MapSelection:
		menuMapSelection.Render();
		break;
	case GameOptions:
		menuGameOptions.Render();
		break;
	case Pause:
		menuPause.Render();
		break;
	case Victory:
		menuVictory.Render();
		break;
	case LAN:
		menuJoinLAN.Render();
		break;
	case Internet:
		menuJoinInternet.Render();
		break;
	case JoinIP:
		menuJoinIP.Render();
		break;
	case Chat:
		menuChat.Render();
		break;
	case About:
		menuAbout.Render();
		break;
	case Help:
		menuHelp.Render();
		break;
	case MultiplayerOptions:
		menuMultiplayerOptions.Render();
		break;
	};

	if (visibleMenu != None)
		cursor.Render();
}

void Menu::UpdateMapDetails()
{
	lblLevelDetails->SetBackgroundImage(NULL);
	lblLevelDetails->SetText("");

	const char* level = enmLevels.GetElement(enmLevels.GetSelectedIndex());

	INIFile file;
	if (!ExplosiveEnvironmentMgr::GetInstance().GetMap(level, file))
		return;

	std::string image = file.GetValue("info", "thumbnail");
	lblLevelImage->SetBackgroundImage(TextureMgr::LoadTex(image.c_str()));

	Array< Pair<std::string, std::string> >* descSection = file.GetSection("description");

	if (!descSection)
		return;

	char buffer[1024] = "\0";
	for (int i = 0; i < descSection->GetSize(); ++i)
	{
		std::string line = (*descSection)[i].second;

		if (i <= 0)
			sprintf(buffer, "%s", line.c_str());
		else
			sprintf(buffer, "%s\n%s", buffer, line.c_str());
	}

	lblLevelDetails->SetText(buffer);
}