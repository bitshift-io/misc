#include "ExplosiveAIController.h"
#include "ExplosiveCharacter.h"
#include "ExplosiveBomb.h"
#include "TileEnvironmentMgr.h"
#include "ExplosiveGame.h"
#include "ExplosiveEnvironmentMgr.h"
#include "DestructableBlock.h"
#include "ExplosivePickup.h"
#include "../Util/Log.h"

#define min(x, y) (x < y) ? x : y

ExplosiveAIController::ExplosiveAIController() : 
	state(Moving),
	target(0)
{
	stateMap.SetSize(5);
	stateMap[UP] = false;
	stateMap[DOWN] = false;
	stateMap[LEFT] = false;
	stateMap[RIGHT] = false;
	stateMap[FIRE] = false;
	randomizeCounter = 0;
	pickNewDirectionCounter = 0;
	rayDirectionPreference = rand() % 2;

	debuggerAttached = false;
}

bool ExplosiveAIController::KeyDown(Button button)
{
	return stateMap[button];
}

bool ExplosiveAIController::KeyPressed(Button button)
{
	return stateMap[button];
}

void ExplosiveAIController::Update(Actor* object)
{
	// clear the fire flag
	stateMap[FIRE] = false;

	ExplosiveCharacter* character = static_cast<ExplosiveCharacter*>(object);

	target = FindTarget(character);

	++randomizeCounter;
	if (randomizeCounter > 100)
	{
		rayDirectionPreference = rand() % 2;
		randomizeCounter = 0;
	}

	pickNewDirectionCounter += rand() % 3;
	if (pickNewDirectionCounter > 100)
	{
		state = Moving;
		pickNewDirectionCounter = 0;
		
	}
	
	if (ShouldChooseNewDirection(character))
		ChooseDirection(character);
}

bool ExplosiveAIController::ShouldChooseNewDirection(ExplosiveCharacter* character)
{
	if (character->nextTile == NULL)
		return true;

	// if not moving...
	int randNewDir = rand() % 100;
	int directionCount = 0;
	for (int i = 0; i < 5; ++i)
	{
		if (stateMap[i])
			++directionCount;
	}

	if (directionCount <= 0 && randNewDir <= 5)
		return true;

	// wait till we are totally on the current tile
	if (stateMap[UP] || stateMap[DOWN])
	{
		float direction = character->GetPosition()[Vector3::Z] - character->currentTile->GetPosition()[Vector3::Z];
		if (direction > character->speed || direction < -character->speed)
		{

			if (debuggerAttached)
				ExplosiveGame::GetInstance().Crash4("bomb.MDL");

			return false;
		}
	}

	// wait till we are totally on the current tile
	if (stateMap[LEFT] || stateMap[RIGHT])
	{
		float direction = character->GetPosition()[Vector3::X] - character->currentTile->GetPosition()[Vector3::X];
		if (direction > character->speed || direction < -character->speed)
			return false;
	}

	if (InBombRadius(character, character->currentTile))
	{
		debuggerAttached = ExplosiveGame::GetInstance().DebuggerCheck1();
		state = RunningFromBomb;
		return true;
	}

	if (character->nextTile && InBombRadius(character, character->nextTile))
	{
		state = RunningFromBomb;
		return true;
	}

	return false;
}

bool ExplosiveAIController::InBombRadius(ExplosiveCharacter* character, Tile* tile)
{
	// get the bombs
	const Array<Actor*>& actors = ActorMgr::GetInstance().GetActorGroup(1); // bombs are in group 1

	for (unsigned int i = 0; i < actors.GetSize(); ++i)
	{
		Actor* a = actors[i];
		Bomb* b = static_cast<Bomb*>(a);

		int radius = b->GetRadius();
		Tile* t = b->GetTile();

		if (t == tile)
			return true;

		if (t->GetX() == tile->GetX())
		{
			int dist = abs(t->GetZ() - tile->GetZ());
			if (dist <= radius)
			{
				// dir indicates which direction to ray trace
				int dir = 1;
				if ((t->GetZ() - tile->GetZ()) > 0) // cast from bomb to player
					dir = -1;

				for (int i = 0; i < dist; ++i)
				{
					Tile* rayTile = ExplosiveEnvironmentMgr::GetInstance().GetTile(t->GetX(), t->GetZ() + dir * (i + 1));
					TileType type = CheckTileType(rayTile, character);

					// if a bomb cant get through to our tile
					if (type == TypeBlocked || type == TypeDynamicBlocked)
						return false;
				}

				return true; // we should check to see if the bomb is blocked between us and the boom
			}
		}

		if (t->GetZ() == tile->GetZ())
		{
			int dist = abs(t->GetX() - tile->GetX());
			if (dist <= radius)
			{
				// dir indicates which direction to ray trace
				int dir = 1;
				if ((t->GetX() - tile->GetX()) > 0) // cast from bomb to player
					dir = -1;

				for (int i = 0; i < dist; ++i)
				{
					Tile* rayTile = ExplosiveEnvironmentMgr::GetInstance().GetTile(t->GetX() + dir * (i + 1), t->GetZ());
					TileType type = CheckTileType(rayTile, character);

					// if a bomb cant get through to our tile
					if (type == TypeBlocked || type == TypeDynamicBlocked)
						return false;
				}

				return true; // we should check to see if the bomb is blocked between us and the boom
			}
		}
	}

	return false;
}

bool ExplosiveAIController::CharacterInBombRadius(ExplosiveCharacter* character)
{
	Tile* thisTile = character->GetLocation();

	// get the bombs
	const Array<Actor*>& actors = ActorMgr::GetInstance().GetActorGroup(0); // characters are in group 0

	for (unsigned int i = 0; i < actors.GetSize(); ++i)
	{
		ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(actors[i]);

		// don't bomb them selves!
		if (c == character)
			continue;

		Tile* charTile = c->GetLocation();
		if (!charTile)
			continue;

		if (charTile->GetX() == thisTile->GetX())
		{
			int dist = thisTile->GetZ() - charTile->GetZ();
			if (abs(dist) < character->bombRadius)
				return true;
		}
		else if (charTile->GetZ() == thisTile->GetZ())
		{
			int dist = thisTile->GetX() - charTile->GetX();
			if (abs(dist) < character->bombRadius)
				return true;
		}
	}
	return false;
}

ExplosiveAIController::TileType ExplosiveAIController::CheckTileType(Tile* tile, ExplosiveCharacter* character)
{
	// this returns false if we cant explode further
	if (!tile)
		return TypeBlocked;

	for (unsigned int i = 0; i < tile->GetActors().GetSize(); ++i)
	{
		Actor* a = tile->GetActor(i);

		if (a->GetType() == Bomb::Type)
		{
			return TypeBomb;
		}
		else if (a->GetType() == DestructableBlock::Type)
		{
			return TypeDynamicBlocked;
		}
		else if (a != character && a->GetType() == ExplosiveCharacter::Type)
		{
			return TypeCharacter;
		}
		else if (a->GetType() == ExplosivePickup::Type || a->GetType() == SnailModifier::Type)
		{
			return TypePickup;
		}
	}

	return TypeEmpty;
}

void ExplosiveAIController::ChooseDirection(ExplosiveCharacter* character)
{
	// clear direction states
	for (int i = 0; i < 5; ++i)
		stateMap[i] = false;

	bool couldDropBomb = false;
	int directionCount = 0;
	bool direction[4] = { false, false, false, false };

	int bombCount = 0;
	bool bomb[4] = { false, false, false, false };

	TileType curTile = CheckTileType(character->GetLocation(), character);

	// left
	Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(character->GetLocation()->GetX() + 1, character->GetLocation()->GetZ());

	TileType type = CheckTileType(t, character);
	if (type == TypeEmpty || type == TypeCharacter || type == TypePickup)
	{
		direction[LEFT] = true;
	}
	else if (type == TypeDynamicBlocked)
	{
		couldDropBomb = true;
	}

	if (direction[LEFT] && InBombRadius(character, t))
	{
		bomb[LEFT] = true;
		++bombCount;
	}

	// right
	t = ExplosiveEnvironmentMgr::GetInstance().GetTile(character->GetLocation()->GetX() - 1, character->GetLocation()->GetZ());

	type = CheckTileType(t, character);
	if (type == TypeEmpty || type == TypeCharacter || type == TypePickup)
	{
		direction[RIGHT] = true;
	}
	else if (type == TypeDynamicBlocked)
	{
		couldDropBomb = true;
	}

	if (direction[RIGHT] && InBombRadius(character, t))
	{
		bomb[RIGHT] = true;
		++bombCount;
	}

	// up
	t = ExplosiveEnvironmentMgr::GetInstance().GetTile(character->GetLocation()->GetX(), character->GetLocation()->GetZ() + 1);

	type = CheckTileType(t, character);
	if (type == TypeEmpty || type == TypeCharacter || type == TypePickup)
	{
		direction[UP] = true;
	}
	else if (type == TypeDynamicBlocked)
	{
		couldDropBomb = true;
	}

	if (direction[UP] && InBombRadius(character, t))
	{
		bomb[UP] = true;
		++bombCount;
	}
	

	// down
	t = ExplosiveEnvironmentMgr::GetInstance().GetTile(character->GetLocation()->GetX(), character->GetLocation()->GetZ() - 1);

	type = CheckTileType(t, character);
	if (type == TypeEmpty || type == TypeCharacter || type == TypePickup)
	{
		direction[DOWN] = true;
	}
	else if (type == TypeDynamicBlocked)
	{
		couldDropBomb = true;
	}

	if (direction[DOWN] && InBombRadius(character, t))
	{
		bomb[DOWN] = true;
		++bombCount;
	}

	// if running for bomb, and there is a safe tile, go to the safe tile, just camp there till its all safe
	if (state == RunningFromBomb || state == SafeAndWatingForBombToBlow)
	{
		if (!InBombRadius(character, character->GetLocation()))
		{
			state = SafeAndWatingForBombToBlow;

			if (rand() % 50 == 0 || character->bombCount == 0)	
				state = Moving;
			else
				return; // no new direction, no firing, just camping
				
		}
		else
		{
			// move to a safe square if there are any around
			for (int i = 0; i < 4; ++i)
			{
				// we are next to a safe square
				if (bomb[i] == false && direction[i] == true)
				{
					stateMap[i] = true;
					return;
				}
			}

			// no safe squares near us, so run the longest direction
			int longestDirection = GetLongestDirection(character->GetLocation(), character);
			stateMap[longestDirection] = true;
/*
			switch (longestDirection)
			{
			case LEFT:
				Log::Print("left\n");
				break;
			case RIGHT:
				Log::Print("right\n");
				break;
			case UP:
				Log::Print("up\n");
				break;
			case DOWN:
				Log::Print("down\n");
				break;
			case FIRE:
				Log::Print("FIRE FIRE FIRE !!! NOOO!\n");
				break;
			};*/
			return;
		}
	}

	// dont drop a bomb if we are currently in threat of a bomb!
	// as bombs trigger other bombs!
	for (int i = 0; i < 4; ++i)
	{
		if (bomb[i])
		{
			// 20% chance we wont drop a bomb
			int dropBombRand = rand() % 100;
			if (dropBombRand > 20)
				couldDropBomb = false;

			break;
		}
	}

	// we should drop a bomb if a character is in radius
	bool shouldDropBomb = CharacterInBombRadius(character);
	if ((couldDropBomb || shouldDropBomb) && curTile != TypeBomb)
	{
		int randBombChance = (rand() % 100);	

		// fire the bomb (50% chance)
		if (randBombChance >= 50)
		{
			stateMap[FIRE] = true;
			//state = RunningFromBomb;

			// if we drop a bomb this frame, dont move as he may move off the tile then drop the bomb
			// in the character update
			for (int i = 0; i < 4; ++i)
			{
				direction[i] = false;
			}
		}
	}

	// calulate how many directions we have to choose from
	directionCount = 0;
	for (int i = 0; i < 4; ++i)
	{
		if (direction[i] == true)
			++directionCount;
	}

	// head towards player
	if (directionCount >= 2 && state == Moving)
	{
		HeadTowardsTarget(character, direction, directionCount);
	}

	// calulate how many directions we have to choose from
	directionCount = 0;
	for (int i = 0; i < 4; ++i)
	{
		if (direction[i] == true)
			++directionCount;
	}

	// if there are lots of directions we can move, then try to avoid back peddling
	if (directionCount >= 2 && state == Moving)
	{
		if (character->direction == LEFT)
			direction[RIGHT] = false;

		if (character->direction == RIGHT)
			direction[LEFT] = false;

		if (character->direction == UP)
			direction[DOWN] = false;

		if (character->direction == DOWN)
			direction[UP] = false;
	}

	// pick a direction!
	for (int i = 0; i < 20; ++i)
	{
		int randDirection = rand() % 4;
		if (direction[randDirection] == true)
		{
			stateMap[randDirection] = true;
/*
			switch (randDirection)
			{
			case LEFT:
				Log::Print("left\n");
				break;
			case RIGHT:
				Log::Print("right\n");
				break;
			case UP:
				Log::Print("up\n");
				break;
			case DOWN:
				Log::Print("down\n");
				break;
			case FIRE:
				Log::Print("FIRE FIRE FIRE !!! NOOO!\n");
				break;
			};*/
			break;
		}
	}
}

void ExplosiveAIController::HeadTowardsTarget(ExplosiveCharacter* character, bool* direction, int directionCount)
{
	if (!target)
		return;

	// which way to our target?
	Vector3 dir = (target->GetPosition() - character->GetPosition()).Normalize();
	float dotUp = dir.Dot(Vector3(0, 0, 1));
	float dotLeft = dir.Dot(Vector3(1, 0, 0));

	// top sector
	if (dotUp >= 0.707) // && absf(dotLeft) >= 0.707)
	{
		if (direction[DOWN])
			--directionCount;

		direction[DOWN] = false;
	}

	if (directionCount <= 1)
		return;

	// down sector
	if (dotUp <= -0.707)// && absf(dotLeft) >= 0.707)
	{
		if (direction[UP])
			--directionCount;

		direction[UP] = false;
	}

	if (directionCount <= 1)
		return;

	// left sector
	if (dotLeft >= 0.707) 
	{
		if (direction[RIGHT])
			--directionCount;

		direction[RIGHT] = false;
	}

	if (directionCount <= 1)
		return;

	// right sector
	if (dotLeft <= -0.707)
	{
		if (direction[LEFT])
			--directionCount;

		direction[LEFT] = false;
	}
/*
	// player is up, and right
	if (dotUp >= 0.0f && dotLeft <= 0.0f)
	{
		direction[DOWN] = false;
		direction[LEFT] = false;
	}
	
	// player is up and left
	if(dotUp >= 0.0f && dotLeft >= 0.0f)
	{
		direction[DOWN] = false;
		direction[RIGHT] = false;
	}

	// player is down, and right
	if (dotUp <= 0.0f && dotLeft <= 0.0f)
	{
		direction[UP] = false;
		direction[RIGHT] = false;
	}
	
	// player is down and left
	if(dotUp >= 0.0f && dotLeft >= 0.0f)
	{
		direction[UP] = false;
		direction[LEFT] = false;
	}*/
}

int ExplosiveAIController::GetLongestDirection(Tile* tile, ExplosiveCharacter* character)
{
	int longest = 0;
	int rayDistance = 0;
	Pair<int, int> rayDistances[4] = {Pair<int, int>(LEFT, 0), Pair<int, int>(RIGHT, 0), Pair<int, int>(UP, 0), Pair<int, int>(DOWN, 0)};

	// ray cast left
	int i = 1;
	bool continueCast = true;
	do
	{
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(tile->GetX() + i, tile->GetZ());

		TileType type = CheckTileType(t, character);
		if (type == TypeBomb || type == TypeDynamicBlocked || type == TypeBlocked)
		{
			continueCast = false;
		}

		++i;
	} while (continueCast);
	--i;

	for (int j = 0; j < 4; ++j)
	{
		if (rayDistances[j].first == LEFT)
			rayDistances[j].second = i;
	}

	if (i > rayDistance)
	{
		rayDistance = i;
		longest = LEFT;
	}

	// ray cast right
	i = 1;
	continueCast = true;
	do
	{
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(tile->GetX() - i, tile->GetZ());

		TileType type = CheckTileType(t, character);
		if (type == TypeBomb || type == TypeDynamicBlocked || type == TypeBlocked)
		{
			continueCast = false;
		}

		++i;
	} while (continueCast);
	--i;

	for (int j = 0; j < 4; ++j)
	{
		if (rayDistances[j].first == RIGHT)
			rayDistances[j].second = i;
	}

	if (i > rayDistance)
	{
		rayDistance = i;
		longest = RIGHT;
	}


	// ray cast up
	i = 1;
	continueCast = true;
	do
	{
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(tile->GetX(), tile->GetZ() + i);

		TileType type = CheckTileType(t, character);
		if (type == TypeBomb || type == TypeDynamicBlocked || type == TypeBlocked)
		{
			continueCast = false;
		}

		++i;
	} while (continueCast);
	--i;

	for (int j = 0; j < 4; ++j)
	{
		if (rayDistances[j].first == UP)
			rayDistances[j].second = i;
	}

	if (i > rayDistance)
	{
		rayDistance = i;
		longest = UP;
	}

	// ray cast down
	i = 1;
	continueCast = true;
	do
	{
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(tile->GetX(), tile->GetZ() - i);

		TileType type = CheckTileType(t, character);
		if (type == TypeBomb || type == TypeDynamicBlocked || type == TypeBlocked)
		{
			continueCast = false;
		}

		++i;
	} while (continueCast);
	--i;

	for (int j = 0; j < 4; ++j)
	{
		if (rayDistances[j].first == DOWN)
			rayDistances[j].second = i;
	}

	if (i > rayDistance)
	{
		rayDistance = i;
		longest = DOWN;
	}

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			Pair<int, int>& iPair = rayDistances[i];
			Pair<int, int>& jPair = rayDistances[j];

			if (iPair.second > jPair.second)
			{
				Pair<int, int> temp = iPair;
				iPair = jPair;
				jPair = temp;
			}
		}
	}

	// we now have sorted from highest to lowest
	for (int i = 3; i >= 0; --i)
	{
		Pair<int, int>& iPair = rayDistances[i];
		if (iPair.second > 2)
		{
			int idx = min(i, rayDirectionPreference); 
			Pair<int, int>& retPair = rayDistances[idx];
			return retPair.first;
		}
	}

	return rayDistances[0].first;
	//return longest;
}

ExplosiveCharacter* ExplosiveAIController::FindTarget(ExplosiveCharacter* character)
{
	if (target && !target->IsDead() && target->GetTeamTag().teamIdx != character->GetTeamTag().teamIdx)
		return target;

	float dist = 0.0f;
	ExplosiveCharacter* eTarget = 0;

	Array<Character*>& characters = ExplosiveGame::GetInstance().GetCharacters();
	for (int i = 0; i < characters.GetSize(); ++i)
	{
		ExplosiveCharacter* eChar = static_cast<ExplosiveCharacter*>(characters[i]);

		// if dead or on the same team... ignore
		if (eChar == character || eChar->IsDead() || eChar->GetTeamTag().teamIdx == character->GetTeamTag().teamIdx)
			continue;

		float distance = (character->GetPosition() - eChar->GetPosition()).MagnitudeSquared();
		if (distance < dist || eTarget == 0)
		{
			dist = distance;
			eTarget = eChar;
		}
	}

	return eTarget;
}
