#include "ExplosiveEnvironmentMgr.h"

#include "../Util/Log.h"
#include "../Util/Ini.h"
#include "../Util/Array.h"
#include "../Core/RenderMgr.h"
#include "../Util/Directory.h"
#include "ExplosivePickUp.h"
#include "ExplosiveCharacter.h"
#include "DestructableBlock.h"
#include "../Core/NetServer.h"
#include "../Core/NetClient.h"
#include "ExplosiveGame.h"

ExplosiveEnvironmentMgr::ExplosiveEnvironmentMgr() : staticLevel(NULL), valid(false)
{
	renderIdx = RenderMgr::GetInstance().AddRenderer(RenderMgr::GetInstance().GetDefaultCamera());
}

ExplosiveEnvironmentMgr::~ExplosiveEnvironmentMgr()
{
	UnloadMap();
}

Renderer* ExplosiveEnvironmentMgr::GetRenderer()
{
	return RenderMgr::GetInstance().GetRenderer(renderIdx);
}

void ExplosiveEnvironmentMgr::Reset()
{

}

void ExplosiveEnvironmentMgr::GetLevelList(Array<std::string>& levels, const char* dir)
{
#ifdef _DEMO
	levels.Insert("chess");
	levels.Insert("classic");
#else
	Directory::GetFileList(dir, levels);

	for (int i = 0; i < levels.GetSize(); ++i)
	{
		std::string& file = levels[i];
		int dotPos = file.rfind('.');

		// we only want ini files
		if (dotPos == -1 || strcmp(&file[dotPos], ".ini") != 0)
		{
			levels.Remove(i);
			--i;
		}
		else
		{
			file[dotPos] = '\0';		
		}
	}
#endif
}

void ExplosiveEnvironmentMgr::UnloadMap(bool restart)
{
	cout << "begin unload map" << endl;

	if (!restart)
	{
		if (staticLevel)
		{
			GetRenderer()->RemoveMesh(staticLevel);
			Mesh::ReleaseMesh(staticLevel, true);
			staticLevel = NULL;
		}
	}

	if (ExplosiveGame::GetInstance().DebuggerCheck2())
		ExplosiveGame::GetInstance().Crash4(level.c_str());

	spawn.SetSize(0);

	for (int i = 0; i < tiles.GetSize(); ++i)
	{
		Tile* tile = tiles[i];

		if (!tile)
			continue;

		while (tile->GetActors().GetSize())
		{
			Actor* a = tile->GetActor(0);

			if (a->GetType() == ExplosivePickup::Type)
				KillPickup(tile, a);
			else if (a->GetType() == DestructableBlock::Type)
				KillPickup(tile, a);
			else
				tile->GetActors().Remove(0); // this will be cleaned up by the actor mgr
		}

		delete tile;
		tiles[i] = 0;
	}
	tiles.SetSize(0);
	valid = false;

	VerifyAllTiles();
	cout << "end unload map" << endl;
}

bool ExplosiveEnvironmentMgr::LoadMapNetwork(const char* mapFileName, const char* tileList)
{	
	ExplosiveGame::GetInstance().PreRestartPlayers();

	bool restart = false;
	if (mapFileName != NULL)
	{
		//if (strcmp(level.c_str(), mapFileName) == 0)
		//	restart = true;

		level = mapFileName;
	}
	else
	{
		restart = true;
	}

	UnloadMap(restart);

	std::string path = "data/levels/" + std::string(level) + std::string(".ini");
	INIFile mapFile;

	if (!mapFile.Load(path.c_str()))
		return false;

	std::string level = mapFile.GetValue("info", "path");

	if (!restart)
	{
		staticLevel = Mesh::LoadMesh(level.c_str());
		GetRenderer()->InsertMesh(staticLevel);
	}

	// set clear colour
	for (int i = 0; i < 3; ++i)
		colour[i] = 0.0f;

	std::string strColour = mapFile.GetValue("info", "bgcolour");
	sscanf(strColour.c_str(), "%f %f %f", &colour[0], &colour[1], &colour[2]);
	SetClearColour();

	mapFile.GetValue("info", "width", width);
	mapFile.GetValue("info", "length", length);

	int size = width * length;
	tiles.SetSize(size);

	Array< Pair<std::string, std::string> >* data = mapFile.GetSection("data");

	float zPos = -(length / 2.0f * TILE_LENGTH);
	int z = 0;
	for (int r = data->GetSize() - 1; r >= 0; --r, ++z)
	{
		std::string row = (*data)[r].second;

		float xPos = -(width / 2.0f * TILE_WIDTH);
		int x = 0;
		for (int c = width - 1; c >= 0; --c, ++x)
		{
			char value = row[c];

			int idx = x + (width * z);
			int type = tileList[idx];
			switch (type)
			{
			case 0:
				tiles[idx] = NULL;
				break;
			case 1:
				tiles[idx] = new Tile(x, z, Vector3(xPos, 0, zPos));
				break;
			case 2:
				tiles[idx] = new Tile(x, z, Vector3(xPos, 0, zPos));
				DestructableBlock* block = new DestructableBlock(tiles[idx], "dynamic_woodbox.mdl");	
				tiles[idx]->GetActors().Insert(block);
				break;
			};

			if (value == '+') // spawn
				spawn.Insert(tiles[idx]);

			xPos += TILE_WIDTH;
		}
		zPos += TILE_LENGTH;
	}

	VerifyAllTiles();
	valid = true;
	return true;
}

bool ExplosiveEnvironmentMgr::GetMap(const char* mapFileName, INIFile& file)
{
	std::string path = "data/levels/" + std::string(mapFileName) + std::string(".ini");
	return file.Load(path.c_str());
}

bool ExplosiveEnvironmentMgr::LoadMap(const char* mapFileName)
{
	cout << "begin load map" << endl;
	ExplosiveGame::GetInstance().PreRestartPlayers();

	bool restart = false;
	if (mapFileName != NULL)
	{
		//if (strcmp(level.c_str(), mapFileName) == 0)
		//	restart = true;

		level = mapFileName;
	}
	else
	{
		restart = true;
	}

	UnloadMap(restart);

	std::string path = "data/levels/" + std::string(level) + std::string(".ini");
	INIFile mapFile;

	if (!mapFile.Load(path.c_str()))
		return false;

	std::string level = mapFile.GetValue("info", "path");

	if (!restart)
	{
		staticLevel = Mesh::LoadMesh(level.c_str());
		GetRenderer()->InsertMesh(staticLevel);
	}

	// set clear colour
	for (int i = 0; i < 3; ++i)
		colour[i] = 0.0f;

	std::string strColour = mapFile.GetValue("info", "bgcolour");
	sscanf(strColour.c_str(), "%f %f %f", &colour[0], &colour[1], &colour[2]);
	SetClearColour();

	mapFile.GetValue("info", "width", width);
	mapFile.GetValue("info", "length", length);

	// load percentages
	mapFile.GetValue("info", "pickuppcnt", pickupPcnt);
	
	int count = 0;
	int temp = 0;

	teleportPcnt.first = count;
	mapFile.GetValue("pickups", "teleport", temp);
	count += temp;
	teleportPcnt.second = count;

	kickPcnt.first = count;
	mapFile.GetValue("pickups", "kick", temp);
	count += temp;
	kickPcnt.second = count;

	bombPcnt.first = count;
	mapFile.GetValue("pickups", "bomb", temp);
	count += temp;
	bombPcnt.second = count;

	firePcnt.first = count;
	mapFile.GetValue("pickups", "fire", temp);
	count += temp;
	firePcnt.second = count;

	diseasePcnt.first = count;
	mapFile.GetValue("pickups", "disease", temp);
	count += temp;
	diseasePcnt.second = count;

	snailPcnt.first = count;
	mapFile.GetValue("pickups", "snail", temp);
	count += temp;
	snailPcnt.second = count;

	speedPcnt.first = count;
	mapFile.GetValue("pickups", "speed", temp);
	count += temp;
	speedPcnt.second = count;

	shieldPcnt.first = count;
	mapFile.GetValue("pickups", "shield", temp);
	count += temp;
	shieldPcnt.second = count;

	reversePcnt.first = count;
	mapFile.GetValue("pickups", "reverse", temp);
	count += temp;
	reversePcnt.second = count;

	randomPcnt.first = count;
	mapFile.GetValue("pickups", "random", temp);
	count += temp;
	randomPcnt.second = count;
	
	if (count > 100)
		Log::Print("Level warning: percentages add up to %i. Should be 100", count);

	Array< Pair<std::string, std::string> >* data = mapFile.GetSection("data");

	int size = width * length;
	tiles.SetCapacity(size);

	float zPos = -(length / 2.0f * TILE_LENGTH);
	int z = 0;
	for (int r = data->GetSize() - 1; r >= 0; --r, ++z)
	{
		std::string row = (*data)[r].second;

		float xPos = -(width / 2.0f * TILE_WIDTH);
		int x = 0;
		for (int c = width - 1; c >= 0; --c, ++x)
		{
			char value = row[c];

			switch (value)
			{
			case '0':	// Static, cant move on this tile, cant kill it
				{
					tiles.Insert(NULL);
				}
				break;
			case '+': // spawn
				{
					Tile* t = new Tile(x, z, Vector3(xPos, 0, zPos));
					spawn.Insert(t);
					tiles.Insert(t);
				}
				break;
			case 'd': // an empty tile
				{
					Tile* t = new Tile(x, z, Vector3(xPos, 0, zPos));
					tiles.Insert(t);
					DestructableBlock* block = new DestructableBlock(t, "dynamic_woodbox.mdl");	
					t->GetActors().Insert(block);
				}
				break;
			case 'r': // an empty tile
				{
					Tile* t = new Tile(x, z, Vector3(xPos, 0, zPos));

					int value = rand() % 4; // this random value should be from the ini file
					if (value)
					{
						DestructableBlock* block = new DestructableBlock(t, "dynamic_woodbox.mdl");	
						t->GetActors().Insert(block);
					}

					tiles.Insert(t);
				}
				break;
			case ' ': // an dynamic block
			default:
				{
					tiles.Insert(new Tile(x, z, Vector3(xPos, 0, zPos)));
				}
				break;
			}

			xPos += TILE_WIDTH;
		}
		zPos += TILE_LENGTH;
	}

	if (z != length)
		Log::Print("error with map length");

	RandomiseSpawns();

	SendMapInfo();

	valid = true;
	cout << "end load map" << endl;
	return true;
}

void ExplosiveEnvironmentMgr::SetClearColour() 
{ 
	Device::GetInstance().SetClearColour(colour[0], colour[1], colour[2]); 
}

void ExplosiveEnvironmentMgr::MoveToTile(Tile* tile, Character* character)
{
	VerifyAllTiles();

	if (!tile)
		return;

	for (int i = 0; i < tile->GetActors().GetSize(); ++i)
	{
		Actor* a = tile->GetActor(i);

		if (a->GetType() == ExplosivePickup::Type)
		{
			// check for pickups, and give to player
			ExplosivePickup* p = static_cast<ExplosivePickup*>(a);
			if (p->GiveToPlayer(character) == false)
				return;

			KillPickup(tile, a);
			break;
		}
	}	

	VerifyAllTiles();
}

void ExplosiveEnvironmentMgr::KillPickup(Tile* tile)
{
	VerifyAllTiles();

	if (tile == 0)
		return;

	for (int i = 0; i < tile->GetActors().GetSize(); ++i)
	{
		Actor* a = tile->GetActor(i);
		if (a->GetType() == ExplosivePickup::Type)
		{
			/*
			tile->GetActors().Remove(i);
			delete a;*/
			KillPickup(tile, a);
			--i;
		}
	}

	if (NetServer::IsValid())
	{
		int data[3];
		data[0] = NM_KillPickup;
		data[1] = tile->GetX();
		data[2] = tile->GetZ();
		Send(data, sizeof(int) * 3);
	}
	VerifyAllTiles();
}

void ExplosiveEnvironmentMgr::KillPickup(Tile* tile, Actor* actor)
{
	VerifyAllTiles();
	// remove and delete the dynamic tile
	cout << "KillPickup(Tile* tile, Actor* actor) deleting actor from tile: " << tile->GetX() << " " << tile->GetZ() << endl;
	tile->GetActors().Remove(actor);
	delete actor;
	VerifyAllTiles();
}

void ExplosiveEnvironmentMgr::KillDynamicTile(Tile* tile, int pickupType)
{
	VerifyAllTiles();

	// kill any dynamic tile on this square
	for (int i = 0; i < tile->GetActors().GetSize(); ++i)
	{
		Actor* a = tile->GetActor(i);
		if (a->GetType() == DestructableBlock::Type)
		{
			cout << "KillDynamicTile(Tile* tile, int pickupType) deleting destructable actor from tile: " << tile->GetX() << " " << tile->GetZ() << endl;
			tile->GetActors().Remove(i);
			delete a;
			--i;
		}
	}

	switch (pickupType)
	{
	case TeleportPickUp::PickupType:
		new TeleportPickUp(tile, "pickup.mdl", "pickup_teleport.mat");
		break;
	case BombKickPickUp::PickupType:
		new BombKickPickUp(tile, "pickup.mdl", "pickup_kick.mat");
		break;
	case BombCountPickUp::PickupType:
		new BombCountPickUp(tile, "pickup.mdl", "pickup_bomb.mat");
		break;
	case BombExpandPickUp::PickupType:
		new BombExpandPickUp(tile, "pickup.mdl", "pickup_fire.mat");
		break;
	case DiseasePickUp::PickupType:
		new DiseasePickUp(tile, "pickup.mdl", "pickup_disease.mat");
		break;
	case SnailPickUp::PickupType:
		new SnailPickUp(tile, "pickup.mdl", "pickup_snail.mat");
		break;
	case SpeedPickUp::PickupType:
		new SpeedPickUp(tile, "pickup.mdl", "pickup_speed.mat");
		break;
	case ShieldPickUp::PickupType:
		new ShieldPickUp(tile, "pickup.mdl", "pickup_shield.mat");
		break;
	case ReversePickUp::PickupType:
		new ReversePickUp(tile, "pickup.mdl", "pickup_reverse.mat");
		break;
	case RandomPickUp::PickupType:
		new RandomPickUp(tile, "pickup.mdl", "pickup_random.mat");
		break;
	}

	VerifyAllTiles();
}

void ExplosiveEnvironmentMgr::KillDynamicTile(Tile* tile, Actor* actor)
{
	VerifyAllTiles();

	int data[4];
	data[0] = NM_KillDynamicTile;
	data[1] = tile->GetX();
	data[2] = tile->GetZ();
	data[3] = -1;

	cout << "KillDynamicTile(Tile* tile, Actor* actor) deleting destructable actor from tile: " << tile->GetX() << " " << tile->GetZ() << endl;

	// remove and delete the dynamic tile
	tile->GetActors().Remove(actor);
	delete actor;
	
	// server will send this info
	if (NetClient::IsValid())
		return;

	// spawn a pickup
	int randPickupChangce = rand() % 100;
	if (randPickupChangce <= pickupPcnt)
	{
		int randPickup = rand() % 100;

		ExplosivePickup* pickUp = NULL;
		if (randPickup >= teleportPcnt.first && randPickup < teleportPcnt.second)
		{
			pickUp = new TeleportPickUp(tile, "pickup.mdl", "pickup_teleport.mat");
		}
		else if (randPickup >= kickPcnt.first && randPickup < kickPcnt.second)
		{
			pickUp = new BombKickPickUp(tile, "pickup.mdl", "pickup_kick.mat");
		}
		else if (randPickup >= bombPcnt.first && randPickup < bombPcnt.second)
		{
			pickUp = new BombCountPickUp(tile, "pickup.mdl", "pickup_bomb.mat");
		}
		else if (randPickup >= firePcnt.first && randPickup < firePcnt.second)
		{
			pickUp = new BombExpandPickUp(tile, "pickup.mdl", "pickup_fire.mat");
		}
		else if (randPickup >= diseasePcnt.first && randPickup < diseasePcnt.second)
		{
			pickUp = new DiseasePickUp(tile, "pickup.mdl", "pickup_disease.mat");
		}
		else if (randPickup >= snailPcnt.first && randPickup < snailPcnt.second)
		{
			pickUp = new SnailPickUp(tile, "pickup.mdl", "pickup_snail.mat");
		}
		else if (randPickup >= speedPcnt.first && randPickup < speedPcnt.second)
		{
			pickUp = new SpeedPickUp(tile, "pickup.mdl", "pickup_speed.mat");
		}
		else if (randPickup >= shieldPcnt.first && randPickup < shieldPcnt.second)
		{
			pickUp = new ShieldPickUp(tile, "pickup.mdl", "pickup_shield.mat");
		}
		else if (randPickup >= reversePcnt.first && randPickup < reversePcnt.second)
		{
			pickUp = new ReversePickUp(tile, "pickup.mdl", "pickup_reverse.mat");
		}
		else if (randPickup >= randomPcnt.first && randPickup < randomPcnt.second)
		{
			pickUp = new RandomPickUp(tile, "pickup.mdl", "pickup_random.mat");
		}

		if (pickUp)
			data[3] = pickUp->GetPickupType();
	}

	Send(data, sizeof(int) * 4);
	VerifyAllTiles();
}

void ExplosiveEnvironmentMgr::RandomiseSpawns()
{
	// do a random shuffle :)
	for (int i = 0; i < spawn.GetSize(); ++i)
	{
		int randIndex = rand() % spawn.GetSize();
		Tile* temp = spawn[i];
		spawn[i] = spawn[randIndex];
		spawn[randIndex] = temp;
	}

	if (!NetServer::IsValid())
		return;

	// send spawns
	int* intBuffer = new int[1 + spawn.GetSize() * 2];
	intBuffer[0] = NM_Spawns;
	for (int i = 0; i < spawn.GetSize(); ++i)
	{
		Tile* t = spawn[i];
		intBuffer[1 + i * 2] = t->GetX();
		intBuffer[1 + i * 2 + 1] = t->GetZ();
	}
	Send(intBuffer, sizeof(int) * (1 + spawn.GetSize() * 2));
	delete[] intBuffer;
}

Tile* ExplosiveEnvironmentMgr::GetSpawnPoint(int index)
{
	return spawn[index];
}

void ExplosiveEnvironmentMgr::DebugDraw()
{/*
	Box floorBox;
	floorBox.SetMin(Vector3(-TILE_WIDTH/2.0f, -TILE_FLOOR_HEIGHT, -TILE_LENGTH/2.0f));
	floorBox.SetMax(Vector3(TILE_WIDTH/2.0f, 0, TILE_LENGTH/2.0f));
	floorBox.SetTransform(Matrix4(Matrix4::Identity));

	Array<Tile*>& floor = tiles[0];
	for (int i = 0; i < floor.GetSize(); ++i)
	{
		MeshTile* tile = static_cast<MeshTile*>(floor[i]);
		if (!tile)
			continue;

		floorBox.SetTransform(tile->GetTransform());
		floorBox.Draw();
	}
*/
	/*
	// draw a red box in center of world
	Box brickBox;
	brickBox.SetMin(Vector3(-TILE_WIDTH/2.0f, 0, -TILE_LENGTH/2.0f));
	brickBox.SetMax(Vector3(TILE_WIDTH/2.0f, TILE_HEIGHT, TILE_LENGTH/2.0f));
	brickBox.SetTransform(Matrix4(Matrix4::Identity));
	//brickBox.Draw(Vector3(1,0,0));

	for (int i = 0; i < tiles.GetSize(); ++i)
	{
		const Tile& tile = tiles[i];
		const Vector3& pos = tile.GetPosition();

		for (int a = 0; a < tile.GetActors().GetSize(); ++a)
		{
			if (tile.GetActor(i).GetType() == DestructableBlock::GetType())
			{
				brickBox.SetTransform(tile->GetTransform());
				brickBox.Draw(Vector3(0,1,0));
			}
		}

		
	}
/*
	// draw static bricks
	Array<Tile*>& staticBricks = tiles[1];
	for (int i = 0; i < staticBricks.GetSize(); ++i)
	{
		MeshTile* tile = static_cast<MeshTile*>(staticBricks[i]);
		if (!tile)
			continue;

		brickBox.SetTransform(tile->GetTransform());
		brickBox.Draw(Vector3(0,1,0));
	}

	// draw dynamic bricks
	Array<Tile*>& dynamicBricks = tiles[2];
	for (int i = 0; i < dynamicBricks.GetSize(); ++i)
	{
		Tile* tile = dynamicBricks[i];
		if (!tile)
			continue;

		brickBox.SetTransform(tile->GetTransform());
		brickBox.Draw(Vector3(0,0,1));
	}
/*
	// draw spawn points
	for (int i = 0; i < 4; ++i)
	{
		MeshTile* tile = static_cast<MeshTile*>(GetSpawnPoint(i));

		if (!tile)
			continue;

		floorBox.SetTransform(tile->GetTransform());
		floorBox.Draw(Vector3(1,0,0));
	}*/
}

void ExplosiveEnvironmentMgr::Register()
{
	RegisterWithNetwork(false, false);
	SetNetworkId(5);
}

void ExplosiveEnvironmentMgr::ReceiveMessage(const void* message, int length, NetExtension::NetMessageTarget target)
{
	BlockSendBegin();

	int msg = *(const int*)message;

	switch (msg)
	{
	case NM_KillDynamicTile:
		{
			if (Valid())
			{
				//cout << "received message: NM_KillDynamicTile" << endl;
				const int* data = (const int*)message + 1;
				KillDynamicTile(GetTile(data[0], data[1]), data[2]);
			}
			else
			{
				int nothing = 0;
			}
		}
		break;
	case NM_KillPickup:
		{
			if (Valid())
			{
				//cout << "received message: NM_KillPickup" << endl;
				const int* data = (const int*)message + 1;
				KillPickup(GetTile(data[0], data[1]));
			}
			else
			{
				int nothing = 0;
			}
		}
		break;
	case NM_LoadMap:
		{
			const char* map = (const char*)message + sizeof(int);
			int strLen = strlen(map);
			const char* tileList = map + strLen + 1;
			if (!LoadMapNetwork(map, tileList))
				ExplosiveGame::GetInstance().GetMenu().OnMousePressed(ExplosiveGame::GetInstance().GetMenu().btnExit);
		}
		break;
	case NM_Spawns:
		{
			if (Valid())
			{
				const int* tiles = (const int*)message + 1;
				for (int i = 0; i < spawn.GetSize(); ++i)
				{
					int x = tiles[i * 2];
					int z = tiles[i * 2 + 1];
					spawn[i] = GetTile(x, z);
				}
			}
			else
			{
				int nothing = 0;
			}
		}
		break;
	};

	BlockSendEnd();
}

void ExplosiveEnvironmentMgr::SendMapInfo()
{
	// send the map
	int strLen = strlen(level.c_str()) + 1;
	char* buffer = new char[strLen + sizeof(int) + tiles.GetSize()];
	int msg = NM_LoadMap;
	memcpy(buffer, &msg, sizeof(int));
	memcpy(buffer + sizeof(int), level.c_str(), strLen);

	// send tiles
	for (int i = 0; i < tiles.GetSize(); ++i)
	{
		buffer[sizeof(int) + strLen + i] = 0;

		Tile* t = tiles[i];
		if (t)
		{
			buffer[sizeof(int) + strLen + i] = 1;

			for (int j = 0; j < t->GetActors().GetSize(); ++j)
			{
				Actor* a = t->GetActor(j);
				if (a->GetType() == DestructableBlock::Type)
				{
					buffer[sizeof(int) + strLen + i] = 2;
				}
			}
		}
	}

	Send(buffer, strLen + sizeof(int) + tiles.GetSize());
	delete[] buffer;

	// send pickups
	for (int i = 0; i < tiles.GetSize(); ++i)
	{
		Tile* t = tiles[i];
		if (t)
		{
			for (int j = 0; j < t->GetActors().GetSize(); ++j)
			{
				Actor* a = t->GetActor(j);
				if (a->GetType() == ExplosivePickup::Type)
				{
					ExplosivePickup* pickUp = static_cast<ExplosivePickup*>(a);
					int data[4];
					data[0] = NM_KillDynamicTile;
					data[1] = t->GetX();
					data[2] = t->GetZ();
					data[3] = pickUp->GetPickupType();				
					Send(data, sizeof(int) * 4);
				}
			}
		}
	}	

	// send spawns
	int* intBuffer = new int[1 + spawn.GetSize() * 2];
	intBuffer[0] = NM_Spawns;
	for (int i = 0; i < spawn.GetSize(); ++i)
	{
		Tile* t = spawn[i];
		intBuffer[1 + i * 2] = t->GetX();
		intBuffer[1 + i * 2 + 1] = t->GetZ();
	}
	Send(intBuffer, sizeof(int) * (1 + spawn.GetSize() * 2));
	delete[] intBuffer;
}

void ExplosiveEnvironmentMgr::AcceptConnection(int index, Connection& client)
{
	// send the map
	int strLen = strlen(level.c_str()) + 1;
	char* buffer = new char[strLen + sizeof(int) + tiles.GetSize()];
	int msg = NM_LoadMap;
	memcpy(buffer, &msg, sizeof(int));
	memcpy(buffer + sizeof(int), level.c_str(), strLen);

	// send tiles
	for (int i = 0; i < tiles.GetSize(); ++i)
	{
		buffer[sizeof(int) + strLen + i] = 0;

		Tile* t = tiles[i];
		if (t)
		{
			buffer[sizeof(int) + strLen + i] = 1;

			for (int j = 0; j < t->GetActors().GetSize(); ++j)
			{
				Actor* a = t->GetActor(j);
				int type = a->GetType();
				if (type == DestructableBlock::Type)
				{
					buffer[sizeof(int) + strLen + i] = 2;
				}
			}
		}
	}

	SendToClient(buffer, strLen + sizeof(int) + tiles.GetSize(), index);
	//NetServer::GetInstance().SendNetObjectMessage(index, GetNetworkId(), buffer, strLen + sizeof(int) + tiles.GetSize());
	delete[] buffer;

	// send pickups
	for (int i = 0; i < tiles.GetSize(); ++i)
	{
		Tile* t = tiles[i];
		if (t)
		{
			for (int j = 0; j < t->GetActors().GetSize(); ++j)
			{
				Actor* a = t->GetActor(j);
				if (a->GetType() == ExplosivePickup::Type)
				{
					ExplosivePickup* pickUp = static_cast<ExplosivePickup*>(a);
					int data[4];
					data[0] = NM_KillDynamicTile;
					data[1] = t->GetX();
					data[2] = t->GetZ();
					data[3] = pickUp->GetPickupType();	
					SendToClient(data, sizeof(int) * 4, index);
					//NetServer::GetInstance().SendNetObjectMessage(index, GetNetworkId(), data, sizeof(int) * 4);
				}
			}
		}
	}	

	// send spawns
	int* intBuffer = new int[1 + spawn.GetSize() * 2];
	intBuffer[0] = NM_Spawns;
	for (int i = 0; i < spawn.GetSize(); ++i)
	{
		Tile* t = spawn[i];
		intBuffer[1 + i * 2] = t->GetX();
		intBuffer[1 + i * 2 + 1] = t->GetZ();
	}
	SendToClient(intBuffer, sizeof(int) * (1 + spawn.GetSize() * 2), index);
	//NetServer::GetInstance().SendNetObjectMessage(index, GetNetworkId(), intBuffer, sizeof(int) * (1 + spawn.GetSize() * 2));
	delete[] intBuffer;
}

void ExplosiveEnvironmentMgr::VerifyAllTiles(bool empty)
{
#ifdef _DEBUG

	for (int i = 0; i < tiles.GetSize(); ++i)
	{
		Tile* t = tiles[i];
		if (!t)
			continue;

		if (empty && t->GetActors().GetSize() > 0)
		{
			Array<Actor*>& actors = t->GetActors();
			int nothing = 0;
		}

		for (int j = 0; j < t->GetActors().GetSize(); ++j)
		{
			Array<Actor*>& actors = t->GetActors();
			Actor* a = t->GetActor(j);
			if (a->GetType() == ExplosivePickup::Type ||
				a->GetType() == ExplosiveCharacter::Type ||
				a->GetType() == Bomb::Type)
			{
				int nothing = 0;
			}
			else if (a->GetType() == DestructableBlock::Type)
			{
				int nothing = 0;
			}
			else
			{
				Log::Print("Invalid tile found!\n");
			}
		}
	}	

#endif
}


void ExplosiveEnvironmentMgr::ClearAllTiles()
{
	for (int i = 0; i < tiles.GetSize(); ++i)
	{
		Tile* t = tiles[i];
		if (!t)
			continue;

		for (int j = 0; j < t->GetActors().GetSize(); ++j)
		{
			Actor* a = t->GetActor(j);
			if (a->GetType() != DestructableBlock::Type)
			{
				t->GetActors().Remove(j);
			}
		}
	}	
}

Tile* ExplosiveEnvironmentMgr::GetTile(int x, int y)
{
	if (!valid)
		return 0;

	VerifyAllTiles();

	return TileEnvironmentMgr::GetTile(x, y);
}