#ifndef _EXPLOSIVE_CONTROLLER_
#define _EXPLOSIVE_CONTROLLER_

#include "../Core/Character.h"

class ExplosiveController : public Controller
{
public:
	enum Button
	{
		UP,
		DOWN,
		LEFT,
		RIGHT,
		FIRE
	};

	virtual ~ExplosiveController() { }

	virtual bool IsHuman() = 0;
	virtual bool IsLocalHuman() { return false; }

	virtual bool KeyDown(Button button) = 0;
	virtual bool KeyPressed(Button button) = 0;
protected:

};

class ExplosiveInputController : public ExplosiveController
{
public:

	enum Keyboard
	{
		Left,
		Right
	};

	ExplosiveInputController(Keyboard keys);
	ExplosiveInputController(int joystickIdx);

	virtual ~ExplosiveInputController();

	virtual bool IsHuman() { return true; }
	virtual bool IsLocalHuman() { return true; }

	virtual void Update(Actor* object);

	virtual bool KeyDown(Button button);
	virtual bool KeyPressed(Button button);

	void SetClientId(int clientId) { this->clientId = clientId; }

protected:
	Array<int> keyMap;
	int joystick;

	int clientId;
	int controllerId;
	static int inputControllerCount;
};

/**
 * If a client joins the server you are in, you get an instance of this
 * this receives network commands from the ClientNetworkController
 */
class NetworkController : public ExplosiveController
{
public:
	NetworkController(int clientId);

	virtual bool KeyDown(Button button);
	virtual bool KeyPressed(Button button);
	virtual void Update(Actor* object);

	virtual bool IsHuman() { return true; }

	static int GetClientId();

protected:
	bool keys[5];
	int clientId;

	static int clientCount;
};

#endif