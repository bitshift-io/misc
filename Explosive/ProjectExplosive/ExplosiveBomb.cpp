#include "ExplosiveBomb.h"
#include "ExplosiveCharacter.h"
#include "ExplosiveEnvironmentMgr.h"
#include "ExplosivePickup.h"
#include "ExplosiveGame.h"
#include "TileEnvironmentMgr.h"
#include "DestructableBlock.h"
#include "../Engine/Renderer.h"
#include "../Core/NetServer.h"
#include "../Core/NetClient.h"

#ifdef WIN32
	#include "../Engine/Win32Time.h"
#else
	#include "../Engine/LinuxTime.h"
#endif

#define EXPLOSION_LIFE 8
#define DEATH_LIFE 50

struct BombLocation
{
	int		msg;
	float	x;
	float	z;
	int		tileX;
	int		tileZ;
};

struct ClientBombLocation
{
	float	x;
	float	z;
	int		tileX;
	int		tileZ;
};


void Bomb::PreCache()
{
	Mesh::LoadMesh("bomb.mdl");
	SoundMgr::Load("explode01.wav");
	SoundMgr::Load("explode02.wav");
	SoundMgr::Load("explode03.wav");
	Mesh::LoadMesh("explosion_ring.MDL");

	Mesh::LoadMesh("explosion_middle.MDL");	
	Mesh::LoadMesh("explosion_middle.MDL");
	Mesh::LoadMesh("explosion_center.MDL");
	Mesh::LoadMesh("explosion_end.MDL");
	Mesh::LoadMesh("explosion_end.MDL");

	Mesh::LoadMesh("fx_death.mdl");
}


Bomb::Bomb(ExplosiveCharacter* owner, Tile* tile, int life, int radius, unsigned int networkId, bool replicate) : 
	owner(owner), 
	life(life), 
	radius(radius),
	tile(tile),
	xDirection(0),
	zDirection(0),
	exploded(false),
	networkKick(false),
	kick(false),
	kickedBy(0)
{
	mesh = Mesh::LoadMesh("bomb.mdl"); //till we get things fixed
	mesh->SetBoundSphere(Sphere(30, Vector3(0,0,0)));
	Matrix4 transform(Matrix4::Identity);
	transform.SetTranslation(tile->GetPosition());
	mesh->SetTransform(transform);

	// add the actor to the cell
	tile->GetActors().Insert(this);

	// bombs and characters should go in thier own renderer as to speed up removal
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(mesh);

	scale = 1.0f;

	if (networkId != -1)
	{
		if (replicate)
			SetNetworkId(networkId);
		else
			NetObject::SetNetworkId(networkId);

		RegisterWithNetwork(false, false);
	}
	else
	{		
		RegisterWithNetwork(false, true);
	}
}

Bomb::~Bomb()
{
	// remove mesh
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(mesh);

	// release the mesh
//	Mesh::ReleaseMesh(mesh);

//	UnregisterWithNetwork();
}

void Bomb::SetNetworkId(unsigned int netId)
{
	//cout << "A bomb has been created with id: " << netId << endl;

	NetObject::SetNetworkId(netId);
	owner->BombReceiveNetworkId(netId, this);

	if (networkKick)
	{
		// send network goodies
		int data[5];
		data[0] = NM_Kick;
		data[1] = xDirection;
		data[2] = zDirection;
		data[3] = tile->GetX();
		data[4] = tile->GetZ();
		Send(data, sizeof(int) * 5, NetExtension::Server);
	}
}

void Bomb::DebugDraw()
{
	mesh->GetBoundSphere().Draw();
}

void Bomb::Kill(Explosion* detonatingExplosion, Array<Tile*>* hitTiles, list<Bomb*>* otherBombs, bool network)
{
	if (otherBombs && !network)
		otherBombs->push_back(this);

	Explode(detonatingExplosion, hitTiles, otherBombs, network);
	life = 0;
}

bool Bomb::Update()
{
	scale = 0.75f + sin(Time::GetInstance().GetTimeSeconds() * 6.0f) / 8.0f;

	// client cant do much once dead :P
	if (NetClient::IsValid() && life <= 0)
	{
		return false;
	}

	// only server can blow up bombs
	if (NetServer::IsValid() || (!NetClient::IsValid() && !NetServer::IsValid()))
		--life;

	if (life <= 0)
	{
		Explode();
		ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
		return false; // destroy this class
	}

	Matrix4 transform(mesh->GetTransform());
	transform.SetScale(scale, scale, scale);
	UpdateBombMovement(transform);
	mesh->SetTransform(transform);
/*
	// server is mother, server is father
	if (IsKicked() && NetServer::IsValid())
	{
		// send network goodies
		int data[5];
		data[0] = NM_MoveToTile;
		data[1] = tile->GetX();
		data[2] = tile->GetZ();
		Send(data, sizeof(int) * 3, NetExtension::All, SocketStream::UnReliable);
	}*/

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
	return true;
}

void Bomb::ReceiveMessage(const void* message, int length, NetExtension::NetMessageTarget target)
{
	BlockSendBegin();

	if (!ExplosiveEnvironmentMgr::GetInstance().Valid())
	{
		BlockSendEnd();
		return;
	}

	int msg = *static_cast<const int*>(message);

	switch (msg)
	{
	case NM_MoveToTile:
		{
			const ClientBombLocation* pData = (const ClientBombLocation*)((int*)message +1);

			float x = pData->x;
			float z = pData->z;

			int tileX = pData->tileX;
			int tileY = pData->tileZ;

			Matrix4 transform = mesh->GetTransform();
			transform.SetTranslation(Vector3(x, 0.f, z));
			mesh->SetTransform(transform);


			MoveToTile(ExplosiveEnvironmentMgr::GetInstance().GetTile(tileX, tileY));	

			// if we get this message, the server is now controlling the bomb
			//xDirection = 0;
			//zDirection = 0;
		}
		break;

	case NM_Kick:
		{
			kick = true;

			ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
			const int* data = static_cast<const int*>(message) + 1;
			xDirection = data[0];
			zDirection = data[1];

			Tile* nextTile = ExplosiveEnvironmentMgr::GetInstance().GetTile(data[2], data[3]);
			MoveToTile(nextTile);
			Log::Print("kicked: moving to a new tile (%i, %i)\n", nextTile->GetX(), nextTile->GetZ());
/*
			if (NetServer::IsValid())
			{
				BlockSendEnd();
				// send network goodies to all clients
				int data[5];
				data[0] = NM_Kick;
				data[1] = xDirection;
				data[2] = zDirection;
				data[3] = tile->GetX();
				data[4] = tile->GetZ();
				Send(data, sizeof(int) * 5);
				BlockSendBegin();
			}*/

			ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
		}
		break;

	case NM_Explode:
		{
			// may need a flag indicating we have already exploded...

			const int* data = static_cast<const int*>(message) + 1;

			Tile* nextTile = ExplosiveEnvironmentMgr::GetInstance().GetTile(data[0], data[1]);
			MoveToTile(nextTile);
			life = 0;

			int numOtherBombs = data[2];

			Log::Print("explode: moving to a new tile (%i, %i), other tiles: %i\n", nextTile->GetX(), nextTile->GetZ(), numOtherBombs);

			list<Bomb*> otherBombs;
			int idx = 3;
			for (int i = 0; i < numOtherBombs; ++i, idx += 3)
			{
				unsigned int uid = data[idx+0];

				Bomb* b = static_cast<Bomb*>(NetClient::GetInstance().GetNetObject(uid));
				if (!b)
					Log::Print("Failed to find bomb - this means games our out of sync as the client is missing a bomb!\n");

				Tile* tile = ExplosiveEnvironmentMgr::GetInstance().GetTile(data[idx+1], data[idx+2]);
				b->MoveToTile(tile);				

				Array<Actor*>& actors = tile->GetActors();
				for (int a = 0; a < actors.GetSize(); ++a)
				{
					Actor* c = actors[a];
					if (c->GetType() == Bomb::Type)
					{
						otherBombs.push_back(static_cast<Bomb*>(c));
					}						
				}
			}

			NetworkExplode(otherBombs);
		}
		break;
	};

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
	BlockSendEnd();
}

void Bomb::MoveToTile(Tile* nextTile)
{
	tile->GetActors().Remove(this);	
	tile = nextTile;
	if (tile)
		tile->GetActors().Insert(this);

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
}

bool Bomb::IsKicked()
{
	if (zDirection == 0 && xDirection == 0)
		return false;

	return true;
}

void Bomb::UpdateBombMovement(Matrix4& transform)
{
	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	// if not moving abort
	//if (!kick)
	//	return;

	if (!IsKicked())
		return;

	Vector3 position = transform.GetTranslation();
	Vector3 offset(xDirection * 0.5f, 0.0f, zDirection * 0.5f);

	// if no next tile, abort
	Tile* nextTile = ExplosiveEnvironmentMgr::GetInstance().GetTile(tile->GetX() + xDirection, tile->GetZ() + zDirection);
	if (!nextTile)
	{
		AdjustBombOffset(position, offset);
	}
	else
	{
		// if next tile is a blocked, abort
		for (int i = 0; i < nextTile->GetActors().GetSize(); ++i)
		{
			Actor* a = nextTile->GetActor(i);
			if (a->GetType() == Bomb::Type ||
				a->GetType() == DestructableBlock::Type ||
				a->GetType() == ExplosivePickup::Type ||
				a->GetType() == ExplosiveCharacter::Type)
			{
				AdjustBombOffset(position, offset);
				zDirection = 0;
				xDirection = 0;
				break;
			}
		}

		// if closer to next tile than this tile, change to next tile
		float distToNext = (position - nextTile->GetPosition()).MagnitudeSquared();
		float distToCurret = (position - tile->GetPosition()).MagnitudeSquared();
		if (distToNext <= distToCurret)
		{
			MoveToTile(nextTile);
			Log::Print("update: moving to a new tile (%i, %i)\n", nextTile->GetX(), nextTile->GetZ());
		}
	}

	// translate
	transform.SetTranslation(position + offset);

	if (NetServer::IsValid() && IsKicked())
	{
		Vector3 translation = transform.GetTranslation();

		BombLocation bombLoc;
		bombLoc.msg = NM_MoveToTile;
		bombLoc.x = translation[0];
		bombLoc.z = translation[2];		
		bombLoc.tileX = tile->GetX();
		bombLoc.tileZ = tile->GetZ();

		Send(&bombLoc, sizeof(BombLocation), NetExtension::All, SocketStream::UnReliable);
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
}

bool Bomb::Kick(int xDir, int zDir, ExplosiveCharacter* kickedBy)
{
	this->kickedBy = kickedBy;

	Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(tile->GetX() + xDir, tile->GetZ() + zDir);

	if (!t)
		return false;

	xDirection = xDir;
	zDirection = zDir;

	if (NetClient::IsValid())
	{
		// send network goodies
		int data[5];
		data[0] = NM_Kick;
		data[1] = xDirection;
		data[2] = zDirection;
		data[3] = tile->GetX();
		data[4] = tile->GetZ();
		Send(data, sizeof(int) * 5, NetExtension::Server);

		networkKick = true;
	}
	else if (NetServer::IsValid() || !NetClient::IsValid())
	{
		kick = true;

		ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

		for (int i = 0; i < t->GetActors().GetSize(); ++i)
		{
			Actor* a = t->GetActor(i);
			if (a->GetType() == Bomb::Type ||
					a->GetType() == DestructableBlock::Type ||
					a->GetType() == ExplosivePickup::Type ||
					a->GetType() == ExplosiveCharacter::Type)
				return false;
		}
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
	return true;
}

void Bomb::AdjustBombOffset(const Vector3& position, Vector3& offset)
{
	if (xDirection > 0) // left
	{
		if ((position[X] + offset[X]) > tile->GetPosition()[X])
			offset[X] = tile->GetPosition()[X] - position[X];
	}
	else if (xDirection < 0) // right
	{
		if ((position[X] + offset[X]) < tile->GetPosition()[X])
			offset[X] = tile->GetPosition()[X] - position[X];
	}

	if (zDirection > 0) // up
	{
		if ((position[Z] + offset[Z]) > tile->GetPosition()[Z])
			offset[Z] = tile->GetPosition()[Z] - position[Z];
	}
	else if (zDirection < 0) // down
	{
		if ((position[Z] + offset[Z]) < tile->GetPosition()[Z])
			offset[Z] = tile->GetPosition()[Z] - position[Z];
	}
}

void Bomb::NetworkExplode(list<Bomb*>& otherBombs)
{
	if (exploded)
		return;

	exploded = true;
	life = 0;
	owner->DestroyBomb(this);

	// remove actor from the tile
	for (int i = 0; i < tile->GetActors().GetSize(); ++i)
	{
		Actor* a = tile->GetActor(i);
		if (a == this)
		{
			tile->GetActors().Remove(i);
			break;
		}
	}

	Explosion* e = new Explosion(radius);
	e->SetColour(owner->GetColour());

	Array<Tile*> hitTiles;

	for (int x = tile->GetX(); x >= tile->GetX() - radius; --x)
	{	
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(x, tile->GetZ());

		Explosion::ExplosionType type = Explosion::XAxisMiddle;
		if (x == tile->GetX())
			type = Explosion::Centre;
		else if (x == (tile->GetX() - radius))
			type = Explosion::XAxisLeftEnd;
		
		if (!Explode(t, type, e, Explosion::XAxisLeftEnd, &hitTiles, &otherBombs, true))
			break;
	}

	for (int x = tile->GetX() + 1; x <= tile->GetX() + radius; ++x)
	{	
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(x, tile->GetZ());

		Explosion::ExplosionType type = Explosion::XAxisMiddle;
		if (x == (tile->GetX() + radius))
			type = Explosion::XAxisRightEnd;

		if (!Explode(t, type, e, Explosion::XAxisRightEnd, &hitTiles, &otherBombs, true))
			break;
	}

	for (int z = tile->GetZ() - 1; z >= tile->GetZ() - radius; --z)
	{	
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(tile->GetX(), z);

		Explosion::ExplosionType type = Explosion::ZAxisMiddle;
		if (z == (tile->GetZ() - radius))
			type = Explosion::ZAxisLeftEnd;

		if (!Explode(t, type, e, Explosion::ZAxisLeftEnd, &hitTiles, &otherBombs, true))
			break;
	}

	for (int z = tile->GetZ() + 1; z <= tile->GetZ() + radius; ++z)
	{	
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(tile->GetX(), z);

		Explosion::ExplosionType type = Explosion::ZAxisMiddle;
		if (z == (tile->GetZ() + radius))
			type = Explosion::ZAxisRightEnd;

		if (!Explode(t, type, e, Explosion::ZAxisRightEnd, &hitTiles, &otherBombs, true))
			break;
	}
/*
	// killed of dynamic tiles
	for (int t = 0; t < hitTiles.GetSize(); ++t)
	{
		Tile* tile = hitTiles[t];

		// check for everything else
		for (int i = 0; i < tile->GetActors().GetSize(); ++i)
		{
			Actor* a = tile->GetActor(i);

			if (a->GetType() == DestructableBlock::Type)
			{
				// kill the tile, and spawn an explosion
				ExplosiveEnvironmentMgr::GetInstance().KillDynamicTile(tile, a);
				break;
			}/*
			else if (a->GetType() == ExplosiveCharacter::Type)
			{
				// kill the player
				ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(a);
				if (ExplosiveGame::GetInstance().GetCurrentGameMode()->PlayerKilled(owner, c))
					--i;
			}* /
			else if (a->GetType() == ExplosivePickup::Type)
			{
				ExplosiveEnvironmentMgr::GetInstance().KillPickup(tile, a);
			}
		}
	}*/

	list<Bomb*>::iterator it;
	for (it = otherBombs.begin(); it != otherBombs.end(); ++it)
	{
		Bomb* b = *it;
		// notify owner that we go boom boom
		b->owner->DestroyBomb(b);

		// remove actor from the tile
		for (int i = 0; i < b->tile->GetActors().GetSize(); ++i)
		{
			Actor* a = b->tile->GetActor(i);
			if (a == b)
			{
				b->tile->GetActors().Remove(i);
				break;
			}
		}
	}

	ActorMgr::GetInstance().AddActor(e, 2);

	if (rand() % 3 == 0)
		ExplosiveGame::GetInstance().DoCameraShake(0.1, 5);

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
}

void Bomb::Explode(Explosion* detonatingExplosion, Array<Tile*>* hitTiles, list<Bomb*>* otherBombs, bool network)
{
	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	if (exploded)
		return;

	exploded = true;

	Array<Tile*> tiles;

	// remove actor from the tile
	for (int i = 0; i < tile->GetActors().GetSize(); ++i)
	{
		Actor* a = tile->GetActor(i);
		if (a == this)
		{
			tile->GetActors().Remove(i);
			break;
		}
	}

	// the first exploding bomb is the master bomb
	list<Bomb*> other;
	bool masterBomb = false;
	if (!hitTiles)
	{
		hitTiles = new Array<Tile*>(radius * 2);
		otherBombs = &other;
		masterBomb = true;
	}

	//
	// kill off any relevent characters/tiles etc.
	// and spawn an explosion effect
	//
	Explosion* e = detonatingExplosion ? detonatingExplosion : new Explosion(radius);
	e->SetColour(owner->GetColour());

	if (masterBomb)
		ActorMgr::GetInstance().AddActor(e, 2);

	for (int x = tile->GetX(); x >= tile->GetX() - radius; --x)
	{	
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(x, tile->GetZ());

		Explosion::ExplosionType type = Explosion::XAxisMiddle;
		if (x == tile->GetX())
			type = Explosion::Centre;
		else if (x == (tile->GetX() - radius))
			type = Explosion::XAxisLeftEnd;
		
		if (!Explode(t, type, e, Explosion::XAxisLeftEnd, hitTiles, otherBombs, network))
			break;
	}

	for (int x = tile->GetX() + 1; x <= tile->GetX() + radius; ++x)
	{	
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(x, tile->GetZ());

		Explosion::ExplosionType type = Explosion::XAxisMiddle;
		if (x == (tile->GetX() + radius))
			type = Explosion::XAxisRightEnd;

		if (!Explode(t, type, e, Explosion::XAxisRightEnd, hitTiles, otherBombs, network))
			break;
	}

	for (int z = tile->GetZ() - 1; z >= tile->GetZ() - radius; --z)
	{	
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(tile->GetX(), z);

		Explosion::ExplosionType type = Explosion::ZAxisMiddle;
		if (z == (tile->GetZ() - radius))
			type = Explosion::ZAxisLeftEnd;

		if (!Explode(t, type, e, Explosion::ZAxisLeftEnd, hitTiles, otherBombs, network))
			break;
	}

	for (int z = tile->GetZ() + 1; z <= tile->GetZ() + radius; ++z)
	{	
		Tile* t = ExplosiveEnvironmentMgr::GetInstance().GetTile(tile->GetX(), z);

		Explosion::ExplosionType type = Explosion::ZAxisMiddle;
		if (z == (tile->GetZ() + radius))
			type = Explosion::ZAxisRightEnd;

		if (!Explode(t, type, e, Explosion::ZAxisRightEnd, hitTiles, otherBombs, network))
			break;
	}

	if (!network)
	{
		// notify owner that we go boom boom
		owner->DestroyBomb(this);
	}

	// do the dirty work!
	if (masterBomb)
	{
		// only server can blow up bombs
		if (NetServer::IsValid() || (!NetClient::IsValid() && !NetServer::IsValid()))
		{
			// network shenanigans
			int msg[256];
			msg[0] = NM_Explode;
			msg[1] = tile->GetX();
			msg[2] = tile->GetZ();

			int numberOfOtherBombs = other.size();
			msg[3] = numberOfOtherBombs;
			list<Bomb*>::iterator it;
			int idx = 4;
			for (it = other.begin(); it != other.end(); ++it, idx += 3)
			{
				msg[idx + 0] = (*it)->GetNetworkId();
				msg[idx + 1] = (*it)->GetTile()->GetX();
				msg[idx + 2] = (*it)->GetTile()->GetZ();
			}

			Send(&msg, sizeof(int) * idx);
			//if (!IsBlocking())
			Log::Print("sending explode message to all tile(%i, %i) killing %i bombs\n", tile->GetX(), tile->GetZ(), other.size());
		}

		if (rand() % 3 == 0)
			ExplosiveGame::GetInstance().DoCameraShake(0.1, 5);

		for (int t = 0; t < hitTiles->GetSize(); ++t)
		{
			Tile* tile = (*hitTiles)[t];

			// check for everything else
			for (int i = 0; i < tile->GetActors().GetSize(); ++i)
			{
				Actor* a = tile->GetActor(i);

				if (a->GetType() == DestructableBlock::Type)
				{
					// kill the tile, and spawn an explosion
					ExplosiveEnvironmentMgr::GetInstance().KillDynamicTile(tile, a);
					break;
				}
				else if (a->GetType() == ExplosiveCharacter::Type)
				{
					// kill the player
					ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(a);
					if (ExplosiveGame::GetInstance().GetCurrentGameMode()->PlayerKilled(owner, c))
						--i;
				}
				else if (a->GetType() == ExplosivePickup::Type)
				{
					ExplosiveEnvironmentMgr::GetInstance().KillPickup(tile);
				}
			}
		}

		delete hitTiles;
	}

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
}

bool Bomb::Explode(Tile* tile, Explosion::ExplosionType type, Explosion* explosion, Explosion::ExplosionType endType, Array<Tile*>* hitTiles, list<Bomb*>* otherBombs, bool networkExplode)
{
	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();

	// this returns false if we cant explode further
	if (!tile)
		return false;

	if (ExplosiveGame::GetInstance().DebuggerCheck2())
		ExplosiveGame::GetInstance().Crash4("bomb.MDL");

	//if (detonatingExplosion)
	{
		bool explosionOnTile = explosion->ExplosionOnTile(tile);
		if (explosionOnTile)
			return false;
	}

	bool returnVal = true;
	bool addSegment = true;

	// check for bombs first pass
	for (int i = 0; i < tile->GetActors().GetSize(); ++i)
	{
		Actor* a = tile->GetActor(i);

		if (a->GetType() == Bomb::Type)
		{
			Bomb* b = static_cast<Bomb*>(a);

			// this bomb is only allowed to explode if the server tells us
			if (networkExplode)
			{
				list<Bomb*>::iterator it;
				for (it = otherBombs->begin(); it != otherBombs->end(); ++it)
				{
					if ((*it) == b)
					{
						b->Kill(explosion, hitTiles, otherBombs, networkExplode);
						addSegment = false;
						returnVal = false;
						break;
					}
				}
			}
			else
			{
				b->Kill(explosion, hitTiles, otherBombs, networkExplode);
				addSegment = false;
				returnVal = false;
			}			
		}
	}

	if (returnVal == false)
		return false;

	hitTiles->Insert(tile);

	// check for everything else
	for (int i = 0; i < tile->GetActors().GetSize(); ++i)
	{
		Actor* a = tile->GetActor(i);

		if (a->GetType() == DestructableBlock::Type)
		{
			// kill the tile, and spawn an explosion
			//ExplosiveEnvironmentMgr::GetInstance().KillDynamicTile(tile, a);
			explosion->AddExplosionSegment(tile, endType);
			return false;
		}
	/*	else if (a->GetType() == ExplosiveCharacter::Type)
		{
			// kill the player
			//ExplosiveCharacter* c = static_cast<ExplosiveCharacter*>(a);
			if (ExplosiveGame::GetInstance().GetCurrentGameMode()->PlayerKilled(owner, c))
				--i;

			/*
			if (c->Kill(owner))
			{
				owner->NotifyKilledPlayer(c);
				--i;
			}* /
		}
		else if (a->GetType() == ExplosivePickup::Type)
		{
			ExplosiveEnvironmentMgr::GetInstance().KillPickup(tile, a);
		}*/
	}

	if (returnVal == false)
		return false;

	if (addSegment)
		explosion->AddExplosionSegment(tile, type);

	ExplosiveEnvironmentMgr::GetInstance().VerifyAllTiles();
	return true;
}

Explosion::Explosion(int radius) : 
	explosionSnd(0), 
	ring(0)
{
	life = EXPLOSION_LIFE;
	speed = 1.0f * radius;
	scale = 1.0f;

	int r = rand() % 3;
	switch (r)
	{
	case 0:
		explosionSnd = SoundMgr::Load("explode01.wav");
		break;
	case 1:
		explosionSnd = SoundMgr::Load("explode02.wav");
		break;
	case 2:
		explosionSnd = SoundMgr::Load("explode03.wav");
		break;
	}

	if (explosionSnd)
		explosionSnd->Play();
}

Explosion::~Explosion()
{
	// remove meshes
	for (int i = 0; i < tileExplosion.GetSize(); ++i)
	{
		Mesh* mesh = tileExplosion[i].first;
		ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(mesh);
	}

	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(ring);

	Mesh::ReleaseMesh(ring);

	for (int i = 0; i < tileExplosion.GetSize(); ++i)
		Mesh::ReleaseMesh(tileExplosion[i].first);

	SoundMgr::Release(explosionSnd);
}

bool Explosion::ExplosionOnTile(Tile* tile)
{
	for (int i = 0; i < tileExplosion.GetSize(); ++i)
		if (tileExplosion[i].second == tile)
			return true;

	return false;
}

void Explosion::AddExplosionSegment(Tile* tile, ExplosionType type)
{
	// the first tile is the centre tile
	if (!ring)
	{
		ring = Mesh::LoadMesh("explosion_ring.MDL");
		ring->SetCallback(this);

		Matrix4 transform(Matrix4::Identity);
		transform.SetTranslation(tile->GetPosition());
		ring->SetTransform(transform);

		ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(ring);
	}

	// load appropriate mesh, and roaate it appropriately
	// and add to renderer
	Mesh* mesh = 0;
	float angle = 0.0f;

	switch (type)
	{
	case ZAxisMiddle:
		{
			mesh = Mesh::LoadMesh("explosion_middle.MDL");	
		}
		break;
	case XAxisMiddle:
		{
			mesh = Mesh::LoadMesh("explosion_middle.MDL");
			angle = Math::DtoR(-90.0f);
		}
		break;
	case Centre:
		{
			mesh = Mesh::LoadMesh("explosion_center.MDL");
		}
		break;
	case ZAxisLeftEnd:
		{
			mesh = Mesh::LoadMesh("explosion_end.MDL");
			angle = Math::DtoR(180.0f);
		}
		break;
	case ZAxisRightEnd:
		{
			mesh = Mesh::LoadMesh("explosion_end.MDL");
		}
		break;
	case XAxisLeftEnd:
		{
			mesh = Mesh::LoadMesh("explosion_end.MDL");
			angle = Math::DtoR(90.0f);
		}
		break;
	case XAxisRightEnd:
		{
			mesh = Mesh::LoadMesh("explosion_end.MDL");
			angle = Math::DtoR(-90.0f);
		}
		break;
	}

	if (!mesh)
		return;

	mesh->SetCallback(this);

	alphaParam = mesh->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("alpha");
	colourParam = mesh->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("colour");
	effect = mesh->GetMeshMat().material[0].material->GetEffect();

	if (alphaParam == 0)
	{
		alphaParam = mesh->GetMeshMat().material[1].material->GetEffect()->GetParameterByName("alpha");
		colourParam = mesh->GetMeshMat().material[1].material->GetEffect()->GetParameterByName("colour");
		effect = mesh->GetMeshMat().material[1].material->GetEffect();
	}

	Matrix4 transform(Matrix4::Identity);
	transform.RotateY(angle);
	transform.SetTranslation(tile->GetPosition());
	mesh->SetTransform(transform);

	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(mesh);

	tileExplosion.Insert(Pair<Mesh*, Tile*>(mesh, tile));
}

void Explosion::PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format)
{
	if (material->GetEffect() == effect)
	{
		effect->SetVector3(colourParam, colour);
	
		if (mesh == ring)
			effect->SetFloat(alphaParam, ringAlpha);
		else
			effect->SetFloat(alphaParam, alpha);
	}
}

bool Explosion::Update()
{
	int midPoint = EXPLOSION_LIFE / 2;
	int lifeUsed = EXPLOSION_LIFE - life;
	if (lifeUsed < midPoint)
		alpha = float(lifeUsed) / float(midPoint);
	else
		alpha = 1.0f - (float(lifeUsed - midPoint) / float(midPoint));

	alpha *= 2.0f;

	// scale ring according to life used
	ringAlpha = (float(life) / 2.0f) / float(EXPLOSION_LIFE) + 0.1f;
	speed /= 1.5f;
	scale += speed;
	Matrix4 transform = ring->GetTransform();
	transform.SetScale(scale);
	ring->SetTransform(transform);

	--life;	

	if (life <= 0)
	{
		if (!explosionSnd)
			return false;

		if (!explosionSnd->IsPlaying())
			return false;		
	}

	return true;
}


//*******************************************************************************************

DeathEffect::DeathEffect(Tile* tile, const Vector3& colour) :
	tile(tile),
	colour(colour),
	mesh(0),
	life(DEATH_LIFE),
	alpha(1.0f)
{
	mesh = Mesh::LoadMesh("fx_death.mdl");
	mesh->SetCallback(this);
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(mesh);

	Matrix4 transform(Matrix4::Identity);
	transform.SetTranslation(tile->GetPosition());
	mesh->SetTransform(transform);

	alphaParam = mesh->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("alpha");
	colourParam = mesh->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("colour");
	scrollParam = mesh->GetMeshMat().material[0].material->GetEffect()->GetParameterByName("texScroll");

	ActorMgr::GetInstance().AddActor(this, 2); // add to fx group
}

DeathEffect::~DeathEffect()
{
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(mesh);
	Mesh::ReleaseMesh(mesh);
}

bool DeathEffect::Update()
{
	alpha = float(life) / float(DEATH_LIFE) + 0.5f;

	--life;

	if (life <= 0)
		return false;

	return true;
}

void DeathEffect::PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format)
{
	material->GetEffect()->SetFloat(alphaParam, alpha);
	material->GetEffect()->SetVector3(colourParam, colour);

	float scrl[2] = {0.0f, -1.0f};
	material->GetEffect()->SetFloat2(scrollParam, scrl);
}