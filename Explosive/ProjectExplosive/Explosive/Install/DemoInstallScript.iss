; -- Explosive --
; Installer so um yeah!

[Setup]
AppName=Explosive Demo
AppVerName=Explosive Demo 1.1
DefaultDirName=C:\Games\ExplosiveDemo
DisableProgramGroupPage=true
UninstallDisplayIcon={app}\ExplosiveDemo.exe
OutputDir=D:\BlackCarbon\SiphonGL2\ProjectExplosive\Explosive\Install\Output
SourceDir=D:\BlackCarbon\SiphonGL2\ProjectExplosive\Explosive\Install
OutputBaseFilename=ExplosiveDemo
VersionInfoVersion=1.1
VersionInfoCompany=www.Blackcarbon.net
ShowLanguageDialog=auto
WizardImageFile=D:\BlackCarbon\SiphonGL2\ProjectExplosive\Explosive\Install\Resources\SideBar.bmp
WizardSmallImageFile=D:\BlackCarbon\SiphonGL2\ProjectExplosive\Explosive\Install\Resources\TopBar.bmp
InternalCompressLevel=ultra
MergeDuplicateFiles=true
Compression=lzma/ultra
DirExistsWarning=no
LicenseFile=D:\BlackCarbon\SiphonGL2\ProjectExplosive\Explosive\Install\Resources\Eula.txt
VersionInfoCopyright=2005-2009
SolidCompression=true
Encryption=false
Password=
SetupIconFile=D:\BlackCarbon\SiphonGL2\ProjectExplosive\Explosive\Install\Resources\Icon.ico
AppPublisher=Black Carbon
AppPublisherURL=www.blackcarbon.net
AppVersion=1.1
UninstallDisplayName=Explosive Demo

[Files]
Source: ExplosiveDemo\data\levels\chess.ini; DestDir: {app}\data\levels
Source: ExplosiveDemo\data\levels\Classic.ini; DestDir: {app}\data\levels
Source: ExplosiveDemo\data\arial.fnt; DestDir: {app}\data\
Source: ExplosiveDemo\data\arial.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\bannana.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\bannana.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\bannanaBark.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\bannanaBark.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\bannanaLeaf.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\bannanaLeaf.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\blackMarble.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\blackMarble.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\blackMarble2.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\blackMarble2.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\blob_shadow.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Blob_Shadow.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\Blob_Shadow.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\bomb.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\bomb.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\bomb.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\bonus.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\breadBoard.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\breadBoard.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\brick_rocky.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Brick_Rocky.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Button_Generic.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Button_Generic.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Button_Right.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\caustics.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\caustics.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\cavern.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\character.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\character.skl; DestDir: {app}\data\
Source: ExplosiveDemo\data\Character.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\character_skin.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\character_skin.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\chess.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\chess.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\chessClock.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\chessClock.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\chessClock2.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\chessClock2.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\chessCube.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\chessCube2.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Classic.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\classic.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\colour_texture_additive.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\colour_texture_additive_nodepth.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\colour_texture_additive_scrolling.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\concrete_tile.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Concrete_Tile.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\concrete_trim.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Concrete_Trim.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\drop.wav; DestDir: {app}\data\
Source: ExplosiveDemo\data\dynamic_woodbox.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\dynamic_woodbox.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\dynamic_woodbox.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\earth_clouds.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Earth_Clouds.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\earth_day_night.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\earth_day_night.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Explode01.wav; DestDir: {app}\data\
Source: ExplosiveDemo\data\Explode02.wav; DestDir: {app}\data\
Source: ExplosiveDemo\data\Explode03.wav; DestDir: {app}\data\
Source: ExplosiveDemo\data\explosion.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Explosion.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\explosion_center.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\explosion_end.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\explosion_middle.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\explosion_ring.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\explosion_ring.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\Explosion_Ring.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\explosion_ring_shadow.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Explosion_Ring_Shadow.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\explosion_shadow.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Explosion_Shadow.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Five_Cents.fnt; DestDir: {app}\data\
Source: ExplosiveDemo\data\Five_Cents.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Five_Cents2.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Five_Cents_MouseOver.fnt; DestDir: {app}\data\
Source: ExplosiveDemo\data\Five_Cents_MouseOver.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\fx_death.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\fx_death.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\FX_Death.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\fx_pickup.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\fx_shield.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\fx_shield.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\FX_Shield.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\fx_shield_inner.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\fx_shield_outter.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\fx_teleport.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\fx_teleport.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\fx_teleport.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\grassOverhang.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\grassOverhang.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\idle.anm; DestDir: {app}\data\
Source: ExplosiveDemo\data\island.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\kick.wav; DestDir: {app}\data\
Source: ExplosiveDemo\data\kitchen.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\kitchenCube01.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\lava.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Lava.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\LeftKeyboard.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\MarbleChecker.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\MarbleChecker.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\marbleTableTop.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\marbleTableTop.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\metal_01.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Metal_01.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Metal_Bumped.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\moon_craters.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\moon_craters.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Mouse_Pointer.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\ornament_01.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Ornament_01.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pea.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\pea.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup.wav; DestDir: {app}\data\
Source: ExplosiveDemo\data\Pickups1.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Pickups2.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_bomb.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_bomb.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_disease.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_disease.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_fire.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_fire.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_kick.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_kick.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_random.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_random.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_reverse.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_reverse.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_shield.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_shield.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_snail.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_snail.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_speed.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_speed.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_teleport.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\pickup_teleport.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\plain_texture.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\plain_texture_additive.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\plain_texture_additive_depth.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\plain_texture_additive_oscillating.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\plain_texture_additive_scrolling.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\plain_texture_clip.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\plain_texture_cube.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\plain_texture_oscillating.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\plain_texture_scrolling.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\planet_halo.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\planet_halo.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\polishedWood.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\polishedWood.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\RightKeyboard.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\rockHard.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\rockHard.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\RockyFace.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\RockyFace.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\rock_cracked.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Rock_Cracked.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\skin.fx; DestDir: {app}\data\
Source: ExplosiveDemo\data\sporkDecal.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\sporkDecal.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\stalagmite.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\stalagmite.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\stars_background.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\stars_background.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\steam_fine.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\steam_fine.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Stroke_Outline.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Stroke_Outline.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\teammarker.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\teamMarker.MDL; DestDir: {app}\data\
Source: ExplosiveDemo\data\TeamMarker.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\teleport.wav; DestDir: {app}\data\
Source: ExplosiveDemo\data\tomato.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\tomato.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\trim_metalicfrills.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Trim_MetalicFrills.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\Voice_EvilLaugh.wav; DestDir: {app}\data\
Source: ExplosiveDemo\data\Voice_InsaneLaugh.wav; DestDir: {app}\data\
Source: ExplosiveDemo\data\walk.anm; DestDir: {app}\data\
Source: ExplosiveDemo\data\WaterRipple.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\WaterRipple.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\whiteMarble.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\whiteMarble.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\whiteMarble2.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\whiteMarble2.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\woodenBevel.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\woodenBevel.tga; DestDir: {app}\data\
Source: ExplosiveDemo\data\wood_01.mat; DestDir: {app}\data\
Source: ExplosiveDemo\data\Wood_01.tga; DestDir: {app}\data\
Source: ExplosiveDemo\cg.dll; DestDir: {app}
Source: ExplosiveDemo\cgGL.dll; DestDir: {app}
Source: ExplosiveDemo\ExplosiveDemo.exe; DestDir: {app}
Source: ExplosiveDemo\OpenAL32.dll; DestDir: {app}
Source: ExplosiveDemo\settings.ini; DestDir: {app}
Source: ExplosiveDemo\system.log; DestDir: {app}
Source: ExplosiveDemo\wrap_oal.dll; DestDir: {app}

[Icons]
Name: {commonprograms}\Games\Explosive; Filename: {app}\ExplosiveDemo.exe; WorkingDir: {app}; IconIndex: 0
Name: {userdesktop}\Explosive; Filename: {app}\ExplosiveDemo.exe; WorkingDir: {app}; IconIndex: 0

[Dirs]
Name: {app}\data
Name: {app}\data\levels

[UninstallDelete]
Name: {app}; Type: filesandordirs
Name: {userdesktop}\Explosive; Type: files
Name: {commonprograms}\Games\Explosive; Type: files
