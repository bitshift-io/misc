//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4	worldViewProj	: WorldViewProjection;
float4x4	world			: World;
float4x4	worldIT			: WorldInverseTranspose;
float3		eyePosition		: EyePosition;

float		overbright = 2.0;

sampler2D diffuseTexture = sampler_state
{
    minFilter = NearestMipMapNearest;
    magFilter = Nearest;
};

samplerCUBE cubeTexture
<
	string ResourceType = "Cube";
> = sampler_state
{
    minFilter = NearestMipMapNearest;
    magFilter = Nearest;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
    float3 position 	: POSITION;
	float3 normal		: NORMAL;
	float2 uvCoord		: TEXCOORD0;
	float4 colour		: COLOR0;	
};

struct VS_OUTPUT
{
	float4 position		: POSITION;
	float2 uvCoord		: TEXCOORD0;
	float3 normal		: TEXCOORD1;
	float3 view			: TEXCOORD2;
	float4 colour		: COLOR0;	
};

struct PS_OUTPUT
{
	float4 colour		: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform float4x4 modelIT, 
			uniform float4x4 model, 
			uniform float3 eyePos)
{
	VS_OUTPUT OUT;

   	OUT.position = mul( modelViewProjection, float4(IN.position, 1.0) );
	OUT.uvCoord = IN.uvCoord;
	OUT.colour = IN.colour;
	OUT.normal = mul((float3x3) modelIT, IN.normal);
	
	float3 worldPos = mul( model, float4(IN.position, 1.0) ).xyz;
	OUT.view = eyePos - worldPos;

	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN, uniform sampler2D diffuse, uniform samplerCUBE cube)
{
	PS_OUTPUT OUT;

	float3 normal = normalize(IN.normal);
	float3 view = normalize(IN.view);
	float3 reflection = reflect(-view, normal);
	float4 cubeColour = texCUBE(cube, reflection);
	OUT.colour = (cubeColour + tex2D(diffuse, IN.uvCoord)) * float4(overbright * IN.colour.rgb, IN.colour.a);
	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		DepthTestEnable = true;
		DepthMask = true;
		DepthFunc = lequal;
		FrontFace = CW;
		CullFace = front;
		CullFaceEnable = true;
		BlendEnable = false;
		
		VertexProgram = compile arbvp1 myvs(worldViewProj, worldIT, world, eyePosition);
		FragmentProgram  = compile arbfp1 myps(diffuseTexture, cubeTexture);
    }
}
