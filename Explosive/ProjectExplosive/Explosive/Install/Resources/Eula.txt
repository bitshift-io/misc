Explosive - Conditions of use

This program is copyright � 2005 - 2009 Black Carbon. You are authorised to use the program for your own private use but you may not sell the program or remove any copyright information from it. The software may not be distributed without prior permission from Black Carbon.

You use the software at your own risk - the author accepts no responsibility for any problems or losses which might arise from its use, and support for the software is not guaranteed.