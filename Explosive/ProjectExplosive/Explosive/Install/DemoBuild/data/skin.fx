
//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4 	worldViewProj	: WorldViewProjection;
float4x4	bones[42];
float3		colour = float3(0.0, 1.0, 0.0);

sampler2D diffuseTexture = sampler_state
{
    minFilter = NearestMipMapNearest;
    magFilter = Nearest;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
	float3 position 	: POSITION;
	float3 normal 		: NORMAL;
	float4 weights		: TEXCOORD0;
	float4 bones		: TEXCOORD1;
	float2 uvCoord		: TEXCOORD2;
};

struct VS_OUTPUT
{
	float4 position 	: POSITION;
	float2 uvCoord		: TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 colour	: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs(	const VS_INPUT IN,
			uniform float4x4 modelViewProjection)
{
	VS_OUTPUT OUT;
	float4 pos = float4(0, 0, 0, 0);

	for (int i = 0; i < 4; ++i)	
	{
		int boneIdx = IN.bones[i];
		pos += mul(bones[boneIdx], float4(IN.position, 1.0)) * IN.weights[i];
		//pos += float4(IN.position, 1.0) * IN.weights[i];
	}
	
	OUT.uvCoord = IN.uvCoord;
	OUT.position = mul(modelViewProjection, pos);	
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN, uniform sampler2D diffuse )
{
	PS_OUTPUT OUT;
	float4 texCol = tex2D(diffuse, IN.uvCoord);

	if (texCol.a > 0)
		OUT.colour.rgb = colour.rgb * texCol.r; // texCol.a;
	else
		OUT.colour.rgb = texCol.rgb;
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		DepthTestEnable = true;
		DepthMask = true;
		DepthFunc = lequal;
		FrontFace = CW;
		CullFace = front;
		CullFaceEnable = true;
		BlendEnable = false;

		FragmentProgram = compile arbvp1 myvs(worldViewProj);
       	VertexProgram  = compile arbfp1 myps(diffuseTexture);
    }
}
