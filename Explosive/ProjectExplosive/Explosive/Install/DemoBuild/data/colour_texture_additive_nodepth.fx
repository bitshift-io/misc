//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4	worldViewProj	: WorldViewProjection;
float3	colour = float3(0.0, 1.0, 0.0);
float		alpha = 1.0;

sampler2D diffuseTexture = sampler_state
{
    minFilter = LinearMipMapLinear;
    magFilter = Linear;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
    float3 position 	: POSITION;
	float2 uvCoord		: TEXCOORD0;	
	float4 colour		: COLOR0;
};

struct VS_OUTPUT
{
	float4 position		: POSITION;
	float2 uvCoord		: TEXCOORD0;
	float4 colour		: COLOR0;	
};

struct PS_OUTPUT
{
	float4 colour		: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection)
{
    VS_OUTPUT OUT;

   	OUT.position = mul( modelViewProjection, float4(IN.position, 1.0) );
	OUT.uvCoord = IN.uvCoord;
	OUT.colour.rgb = colour.rgb;
	OUT.colour.a = alpha * IN.colour.a;
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN, uniform sampler2D diffuse)
{
	PS_OUTPUT OUT;
	
	float4 texCol = tex2D(diffuse, IN.uvCoord);
	OUT.colour.a = texCol.a * IN.colour.a;

	if (texCol.r > 0.5)	
	{
		float illum = ((texCol.r - 0.5) * 2.0);
		OUT.colour.rgb = ((1.0f - illum) * IN.colour.rgb) + (illum * float3(1,1,1));
	}	
	else
	{
		OUT.colour.rgb = (texCol.r * 2.0 * IN.colour.rgb);	
	}

	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		DepthTestEnable = false;
		DepthMask = false;
		DepthFunc = always;
		CullFaceEnable = false;
		
		BlendEnable = true;
		BlendEquation = FuncAdd;
		BlendFunc = int2(SrcAlpha, OneMinusSrcAlpha);
		
		VertexProgram = compile arbvp1 myvs(worldViewProj);
        FragmentProgram  = compile arbfp1 myps(diffuseTexture);
    }
}
