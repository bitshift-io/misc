//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4	worldViewProj	: WorldViewProjection;
float		time 			: Time;
float2	texScroll;
float		overbright = 2.0;

sampler2D diffuseTexture = sampler_state
{
    minFilter = NearestMipMapNearest;
    magFilter = Nearest;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
    	float3 position 		: POSITION;
	float2 uvCoord		: TEXCOORD0;	
	float4 colour		: COLOR0;
};

struct VS_OUTPUT
{
	float4 position		: POSITION;
	float2 uvCoord		: TEXCOORD0;
	float4 colour		: COLOR0;	
};

struct PS_OUTPUT
{
	float4 colour		: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform float2 ts,
			uniform float t)
{
    	VS_OUTPUT OUT;

   	OUT.position = mul( modelViewProjection, float4(IN.position, 1.0) );
	OUT.uvCoord = IN.uvCoord + float2(sin(t/5)+ts.x , cos(t/3)+ts.y) * 0.1; //bronson changed
	OUT.colour = IN.colour;
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN, uniform sampler2D diffuse)
{
	PS_OUTPUT OUT;
	OUT.colour = tex2D(diffuse, IN.uvCoord) * float4(overbright * IN.colour.rgb, IN.colour.a);
	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{
		DepthTestEnable = true;
		DepthMask = false;
		DepthFunc = lequal;
		CullFaceEnable = false;
		
		BlendEnable = true;
		BlendEquation = FuncAdd;
		BlendFunc = int2(SrcAlpha, OneMinusSrcAlpha);
		
		VertexProgram = compile arbvp1 myvs(worldViewProj, texScroll, time);
        	FragmentProgram  = compile arbfp1 myps(diffuseTexture);
    }
}


