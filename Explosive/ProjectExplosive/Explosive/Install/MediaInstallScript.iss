; -- Explosive --
; Installer so um yeah!

[Setup]
AppName=Explosive Demo
AppVerName=Explosive Demo 1.0
DefaultDirName=C:\Games\ExplosiveMediaDemo
DisableProgramGroupPage=true
UninstallDisplayIcon={app}\Explosive.exe
OutputDir=D:\SiphonGL2\ProjectExplosive\Explosive\Install\Output\
SourceDir=D:\SiphonGL2\ProjectExplosive\Explosive\Install\
OutputBaseFilename=ExplosiveMediaDemo
VersionInfoVersion=1.0
VersionInfoCompany=www.Blackcarbon.net
ShowLanguageDialog=auto
WizardImageFile=D:\SiphonGL2\ProjectExplosive\Explosive\Install\Resources\SideBar.bmp
WizardSmallImageFile=D:\SiphonGL2\ProjectExplosive\Explosive\Install\Resources\TopBar.bmp
InternalCompressLevel=ultra
MergeDuplicateFiles=true
Compression=lzma/ultra
DirExistsWarning=no
LicenseFile=D:\SiphonGL2\ProjectExplosive\Explosive\Install\Resources\Eula.txt
VersionInfoCopyright=2005-2009
SolidCompression=true
Encryption=false
Password=mediaonly
SetupIconFile=D:\SiphonGL2\ProjectExplosive\Explosive\Install\Resources\Icon.ico
AppPublisher=Black Carbon
AppPublisherURL=www.blackcarbon.net
AppVersion=1.0

[Files]
Source: RetailBuild\OpenAL32.dll; DestDir: {app}
Source: RetailBuild\settings.ini; DestDir: {app}
Source: RetailBuild\system.log; DestDir: {app}
Source: RetailBuild\wrap_oal.dll; DestDir: {app}
Source: RetailBuild\data\levels\bonus.ini; DestDir: {app}\data\levels
Source: RetailBuild\data\levels\Cavern.ini; DestDir: {app}\data\levels
Source: RetailBuild\data\levels\chess.ini; DestDir: {app}\data\levels
Source: RetailBuild\data\levels\Classic.ini; DestDir: {app}\data\levels
Source: RetailBuild\data\levels\Island.ini; DestDir: {app}\data\levels
Source: RetailBuild\data\levels\kitchen.ini; DestDir: {app}\data\levels
Source: RetailBuild\data\levels\SpaceMaze.ini; DestDir: {app}\data\levels
Source: RetailBuild\data\arial.fnt; DestDir: {app}\data\
Source: RetailBuild\data\arial.tga; DestDir: {app}\data\
Source: RetailBuild\data\bannana.mat; DestDir: {app}\data\
Source: RetailBuild\data\bannana.tga; DestDir: {app}\data\
Source: RetailBuild\data\bannanaBark.mat; DestDir: {app}\data\
Source: RetailBuild\data\bannanaBark.tga; DestDir: {app}\data\
Source: RetailBuild\data\bannanaLeaf.mat; DestDir: {app}\data\
Source: RetailBuild\data\bannanaLeaf.tga; DestDir: {app}\data\
Source: RetailBuild\data\blackMarble.mat; DestDir: {app}\data\
Source: RetailBuild\data\blackMarble.tga; DestDir: {app}\data\
Source: RetailBuild\data\blackMarble2.mat; DestDir: {app}\data\
Source: RetailBuild\data\blackMarble2.tga; DestDir: {app}\data\
Source: RetailBuild\data\blob_shadow.mat; DestDir: {app}\data\
Source: RetailBuild\data\Blob_Shadow.MDL; DestDir: {app}\data\
Source: RetailBuild\data\Blob_Shadow.tga; DestDir: {app}\data\
Source: RetailBuild\data\bomb.mat; DestDir: {app}\data\
Source: RetailBuild\data\bomb.MDL; DestDir: {app}\data\
Source: RetailBuild\data\bomb.tga; DestDir: {app}\data\
Source: RetailBuild\data\bonus.MDL; DestDir: {app}\data\
Source: RetailBuild\data\bonus.tga; DestDir: {app}\data\
Source: RetailBuild\data\breadBoard.mat; DestDir: {app}\data\
Source: RetailBuild\data\breadBoard.tga; DestDir: {app}\data\
Source: RetailBuild\data\brick_rocky.mat; DestDir: {app}\data\
Source: RetailBuild\data\Brick_Rocky.tga; DestDir: {app}\data\
Source: RetailBuild\data\Button_Generic.mat; DestDir: {app}\data\
Source: RetailBuild\data\Button_Generic.tga; DestDir: {app}\data\
Source: RetailBuild\data\Button_Right.tga; DestDir: {app}\data\
Source: RetailBuild\data\caustics.mat; DestDir: {app}\data\
Source: RetailBuild\data\caustics.tga; DestDir: {app}\data\
Source: RetailBuild\data\Cavern.MDL; DestDir: {app}\data\
Source: RetailBuild\data\cavern.tga; DestDir: {app}\data\
Source: RetailBuild\data\character.MDL; DestDir: {app}\data\
Source: RetailBuild\data\character.skl; DestDir: {app}\data\
Source: RetailBuild\data\Character.tga; DestDir: {app}\data\
Source: RetailBuild\data\character_skin.mat; DestDir: {app}\data\
Source: RetailBuild\data\character_skin.tga; DestDir: {app}\data\
Source: RetailBuild\data\chess.MDL; DestDir: {app}\data\
Source: RetailBuild\data\chess.tga; DestDir: {app}\data\
Source: RetailBuild\data\chessClock.mat; DestDir: {app}\data\
Source: RetailBuild\data\chessClock.tga; DestDir: {app}\data\
Source: RetailBuild\data\chessClock2.mat; DestDir: {app}\data\
Source: RetailBuild\data\chessClock2.tga; DestDir: {app}\data\
Source: RetailBuild\data\chessCube.tga; DestDir: {app}\data\
Source: RetailBuild\data\chessCube2.tga; DestDir: {app}\data\
Source: RetailBuild\data\Classic.MDL; DestDir: {app}\data\
Source: RetailBuild\data\classic.tga; DestDir: {app}\data\
Source: RetailBuild\data\colour_texture_additive.fx; DestDir: {app}\data\
Source: RetailBuild\data\colour_texture_additive_nodepth.fx; DestDir: {app}\data\
Source: RetailBuild\data\colour_texture_additive_scrolling.fx; DestDir: {app}\data\
Source: RetailBuild\data\concrete_tile.mat; DestDir: {app}\data\
Source: RetailBuild\data\Concrete_Tile.tga; DestDir: {app}\data\
Source: RetailBuild\data\concrete_trim.mat; DestDir: {app}\data\
Source: RetailBuild\data\Concrete_Trim.tga; DestDir: {app}\data\
Source: RetailBuild\data\drop.wav; DestDir: {app}\data\
Source: RetailBuild\data\dynamic_woodbox.mat; DestDir: {app}\data\
Source: RetailBuild\data\dynamic_woodbox.MDL; DestDir: {app}\data\
Source: RetailBuild\data\dynamic_woodbox.tga; DestDir: {app}\data\
Source: RetailBuild\data\earth_clouds.mat; DestDir: {app}\data\
Source: RetailBuild\data\Earth_Clouds.tga; DestDir: {app}\data\
Source: RetailBuild\data\earth_day_night.mat; DestDir: {app}\data\
Source: RetailBuild\data\earth_day_night.tga; DestDir: {app}\data\
Source: RetailBuild\data\Explode01.wav; DestDir: {app}\data\
Source: RetailBuild\data\Explode02.wav; DestDir: {app}\data\
Source: RetailBuild\data\Explode03.wav; DestDir: {app}\data\
Source: RetailBuild\data\explosion.mat; DestDir: {app}\data\
Source: RetailBuild\data\Explosion.tga; DestDir: {app}\data\
Source: RetailBuild\data\explosion_center.MDL; DestDir: {app}\data\
Source: RetailBuild\data\explosion_end.MDL; DestDir: {app}\data\
Source: RetailBuild\data\explosion_middle.MDL; DestDir: {app}\data\
Source: RetailBuild\data\explosion_ring.mat; DestDir: {app}\data\
Source: RetailBuild\data\explosion_ring.MDL; DestDir: {app}\data\
Source: RetailBuild\data\Explosion_Ring.tga; DestDir: {app}\data\
Source: RetailBuild\data\explosion_ring_shadow.mat; DestDir: {app}\data\
Source: RetailBuild\data\Explosion_Ring_Shadow.tga; DestDir: {app}\data\
Source: RetailBuild\data\explosion_shadow.mat; DestDir: {app}\data\
Source: RetailBuild\data\Explosion_Shadow.tga; DestDir: {app}\data\
Source: RetailBuild\data\Five_Cents.fnt; DestDir: {app}\data\
Source: RetailBuild\data\Five_Cents.tga; DestDir: {app}\data\
Source: RetailBuild\data\Five_Cents2.tga; DestDir: {app}\data\
Source: RetailBuild\data\Five_Cents_MouseOver.fnt; DestDir: {app}\data\
Source: RetailBuild\data\Five_Cents_MouseOver.tga; DestDir: {app}\data\
Source: RetailBuild\data\fx_death.mat; DestDir: {app}\data\
Source: RetailBuild\data\fx_death.MDL; DestDir: {app}\data\
Source: RetailBuild\data\FX_Death.tga; DestDir: {app}\data\
Source: RetailBuild\data\fx_pickup.MDL; DestDir: {app}\data\
Source: RetailBuild\data\fx_shield.mat; DestDir: {app}\data\
Source: RetailBuild\data\fx_shield.MDL; DestDir: {app}\data\
Source: RetailBuild\data\FX_Shield.tga; DestDir: {app}\data\
Source: RetailBuild\data\fx_shield_inner.MDL; DestDir: {app}\data\
Source: RetailBuild\data\fx_shield_outter.MDL; DestDir: {app}\data\
Source: RetailBuild\data\fx_teleport.mat; DestDir: {app}\data\
Source: RetailBuild\data\fx_teleport.MDL; DestDir: {app}\data\
Source: RetailBuild\data\fx_teleport.tga; DestDir: {app}\data\
Source: RetailBuild\data\grassOverhang.mat; DestDir: {app}\data\
Source: RetailBuild\data\grassOverhang.tga; DestDir: {app}\data\
Source: RetailBuild\data\idle.anm; DestDir: {app}\data\
Source: RetailBuild\data\Island.MDL; DestDir: {app}\data\
Source: RetailBuild\data\island.tga; DestDir: {app}\data\
Source: RetailBuild\data\kick.wav; DestDir: {app}\data\
Source: RetailBuild\data\kitchen.MDL; DestDir: {app}\data\
Source: RetailBuild\data\kitchen.tga; DestDir: {app}\data\
Source: RetailBuild\data\kitchenCube01.tga; DestDir: {app}\data\
Source: RetailBuild\data\lava.mat; DestDir: {app}\data\
Source: RetailBuild\data\Lava.tga; DestDir: {app}\data\
Source: RetailBuild\data\MarbleChecker.mat; DestDir: {app}\data\
Source: RetailBuild\data\MarbleChecker.tga; DestDir: {app}\data\
Source: RetailBuild\data\marbleTableTop.mat; DestDir: {app}\data\
Source: RetailBuild\data\marbleTableTop.tga; DestDir: {app}\data\
Source: RetailBuild\data\metal_01.mat; DestDir: {app}\data\
Source: RetailBuild\data\Metal_01.tga; DestDir: {app}\data\
Source: RetailBuild\data\Metal_Bumped.tga; DestDir: {app}\data\
Source: RetailBuild\data\moon_craters.mat; DestDir: {app}\data\
Source: RetailBuild\data\moon_craters.tga; DestDir: {app}\data\
Source: RetailBuild\data\Mouse_Pointer.tga; DestDir: {app}\data\
Source: RetailBuild\data\ornament_01.mat; DestDir: {app}\data\
Source: RetailBuild\data\Ornament_01.tga; DestDir: {app}\data\
Source: RetailBuild\data\pea.mat; DestDir: {app}\data\
Source: RetailBuild\data\pea.tga; DestDir: {app}\data\
Source: RetailBuild\data\pickup.MDL; DestDir: {app}\data\
Source: RetailBuild\data\pickup.wav; DestDir: {app}\data\
Source: RetailBuild\data\pickup_bomb.mat; DestDir: {app}\data\
Source: RetailBuild\data\pickup_bomb.tga; DestDir: {app}\data\
Source: RetailBuild\data\pickup_disease.mat; DestDir: {app}\data\
Source: RetailBuild\data\pickup_disease.tga; DestDir: {app}\data\
Source: RetailBuild\data\pickup_fire.mat; DestDir: {app}\data\
Source: RetailBuild\data\pickup_fire.tga; DestDir: {app}\data\
Source: RetailBuild\data\pickup_kick.mat; DestDir: {app}\data\
Source: RetailBuild\data\pickup_kick.tga; DestDir: {app}\data\
Source: RetailBuild\data\pickup_random.mat; DestDir: {app}\data\
Source: RetailBuild\data\pickup_random.tga; DestDir: {app}\data\
Source: RetailBuild\data\pickup_reverse.mat; DestDir: {app}\data\
Source: RetailBuild\data\pickup_reverse.tga; DestDir: {app}\data\
Source: RetailBuild\data\pickup_shield.mat; DestDir: {app}\data\
Source: RetailBuild\data\pickup_shield.tga; DestDir: {app}\data\
Source: RetailBuild\data\pickup_snail.mat; DestDir: {app}\data\
Source: RetailBuild\data\pickup_snail.tga; DestDir: {app}\data\
Source: RetailBuild\data\pickup_speed.mat; DestDir: {app}\data\
Source: RetailBuild\data\pickup_speed.tga; DestDir: {app}\data\
Source: RetailBuild\data\pickup_teleport.mat; DestDir: {app}\data\
Source: RetailBuild\data\pickup_teleport.tga; DestDir: {app}\data\
Source: RetailBuild\data\plain_texture.fx; DestDir: {app}\data\
Source: RetailBuild\data\plain_texture_additive.fx; DestDir: {app}\data\
Source: RetailBuild\data\plain_texture_additive_depth.fx; DestDir: {app}\data\
Source: RetailBuild\data\plain_texture_additive_oscillating.fx; DestDir: {app}\data\
Source: RetailBuild\data\plain_texture_additive_scrolling.fx; DestDir: {app}\data\
Source: RetailBuild\data\plain_texture_clip.fx; DestDir: {app}\data\
Source: RetailBuild\data\plain_texture_cube.fx; DestDir: {app}\data\
Source: RetailBuild\data\plain_texture_oscillating.fx; DestDir: {app}\data\
Source: RetailBuild\data\plain_texture_scrolling.fx; DestDir: {app}\data\
Source: RetailBuild\data\planet_halo.mat; DestDir: {app}\data\
Source: RetailBuild\data\planet_halo.tga; DestDir: {app}\data\
Source: RetailBuild\data\polishedWood.mat; DestDir: {app}\data\
Source: RetailBuild\data\polishedWood.tga; DestDir: {app}\data\
Source: RetailBuild\data\rockHard.mat; DestDir: {app}\data\
Source: RetailBuild\data\rockHard.tga; DestDir: {app}\data\
Source: RetailBuild\data\RockyFace.mat; DestDir: {app}\data\
Source: RetailBuild\data\RockyFace.tga; DestDir: {app}\data\
Source: RetailBuild\data\rock_cracked.mat; DestDir: {app}\data\
Source: RetailBuild\data\Rock_Cracked.tga; DestDir: {app}\data\
Source: RetailBuild\data\skin.fx; DestDir: {app}\data\
Source: RetailBuild\data\SpaceMaze.MDL; DestDir: {app}\data\
Source: RetailBuild\data\spacemaze.tga; DestDir: {app}\data\
Source: RetailBuild\data\sporkDecal.mat; DestDir: {app}\data\
Source: RetailBuild\data\sporkDecal.tga; DestDir: {app}\data\
Source: RetailBuild\data\stalagmite.mat; DestDir: {app}\data\
Source: RetailBuild\data\stalagmite.tga; DestDir: {app}\data\
Source: RetailBuild\data\stars_background.mat; DestDir: {app}\data\
Source: RetailBuild\data\stars_background.tga; DestDir: {app}\data\
Source: RetailBuild\data\steam_fine.mat; DestDir: {app}\data\
Source: RetailBuild\data\steam_fine.tga; DestDir: {app}\data\
Source: RetailBuild\data\Stroke_Outline.mat; DestDir: {app}\data\
Source: RetailBuild\data\Stroke_Outline.tga; DestDir: {app}\data\
Source: RetailBuild\data\teammarker.mat; DestDir: {app}\data\
Source: RetailBuild\data\teamMarker.MDL; DestDir: {app}\data\
Source: RetailBuild\data\TeamMarker.tga; DestDir: {app}\data\
Source: RetailBuild\data\teleport.wav; DestDir: {app}\data\
Source: RetailBuild\data\tomato.mat; DestDir: {app}\data\
Source: RetailBuild\data\tomato.tga; DestDir: {app}\data\
Source: RetailBuild\data\trim_metalicfrills.mat; DestDir: {app}\data\
Source: RetailBuild\data\Trim_MetalicFrills.tga; DestDir: {app}\data\
Source: RetailBuild\data\Voice_EvilLaugh.wav; DestDir: {app}\data\
Source: RetailBuild\data\Voice_InsaneLaugh.wav; DestDir: {app}\data\
Source: RetailBuild\data\walk.anm; DestDir: {app}\data\
Source: RetailBuild\data\WaterRipple.mat; DestDir: {app}\data\
Source: RetailBuild\data\WaterRipple.tga; DestDir: {app}\data\
Source: RetailBuild\data\whiteMarble.mat; DestDir: {app}\data\
Source: RetailBuild\data\whiteMarble.tga; DestDir: {app}\data\
Source: RetailBuild\data\whiteMarble2.mat; DestDir: {app}\data\
Source: RetailBuild\data\whiteMarble2.tga; DestDir: {app}\data\
Source: RetailBuild\data\woodenBevel.mat; DestDir: {app}\data\
Source: RetailBuild\data\woodenBevel.tga; DestDir: {app}\data\
Source: RetailBuild\data\wood_01.mat; DestDir: {app}\data\
Source: RetailBuild\data\Wood_01.tga; DestDir: {app}\data\
Source: RetailBuild\cg.dll; DestDir: {app}
Source: RetailBuild\cgGL.dll; DestDir: {app}
Source: RetailBuild\Explosive.exe; DestDir: {app}

[Icons]
Name: {commonprograms}\Games\Explosive; Filename: {app}\Explosive.exe; WorkingDir: {app}
Name: {userdesktop}\Explosive; Filename: {app}\Explosive.exe; WorkingDir: {app}

[Dirs]
Name: {app}\data
Name: {app}\data\levels

[UninstallDelete]
Name: {app}; Type: filesandordirs
Name: {userdesktop}\Explosive; Type: files
Name: {commonprograms}\Games\Explosive; Type: files
