#include "DestructableBlock.h"
#include "ExplosiveEnvironmentMgr.h"

DestructableBlock::DestructableBlock(const Tile* tile, const char* meshFile) :
	block(NULL),
	tile(tile)
{
	if (meshFile)
		block = Mesh::LoadMesh(meshFile);

	Matrix4 transform(Matrix4::Identity);

	// apply a random roation 
	int value = rand() % 4;
	transform.RotateY(Math::DtoR(90.0f * value));

	transform.SetTranslation(tile->GetPosition());
	block->SetTransform(transform);

	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->InsertMesh(block);
}

DestructableBlock::~DestructableBlock()
{
	ExplosiveEnvironmentMgr::GetInstance().GetRenderer()->RemoveMesh(block);
	Mesh::ReleaseMesh(block);
}