#ifndef _EXPLOSIVEGAME_
#define _EXPLOSIVEGAME_

#include "../RSA/MD5.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"
#include "../Engine/Font.h"
#include "../Engine/Camera.h"

#ifdef WIN32
	#include "../Engine/Win32Input.h"
	#include "../Engine/Win32Window.h"
	#include "../Engine/Win32Time.h"
#else
	#include "../Engine/LinuxInput.h"
	#include "../Engine/LinuxWindow.h"
	#include "../Engine/LinuxTime.h"
#endif

#include "../Engine/Renderer.h"
#include "../Engine/Material.h"
#include "../Engine/Sound.h"

#include "../Util/Singleton.h"
#include "../Util/Log.h"
#include "../Util/Macros.h"
#include "../Util/INI.h"

#include "../Core/ActorMgr.h"
#include "../Core/PickUp.h"
#include "../Core/Character.h"
#include "../Core/GameModeMgr.h"
#include "../Core/Team.h"
#include "../Core/RenderMgr.h"
#include "../Core/GameModeMgr.h"

#include "TileEnvironmentMgr.h"
#include "ExplosiveCharacter.h"
#include "ExplosiveAIController.h"
#include "ExplosivePickUp.h"
#include "ExplosiveBomb.h"
#include "ExplosiveGame.h"
#include "ExplosiveTagTeamGame.h"
#include "ExplosiveEnvironmentMgr.h"
#include "Menu.h"
#include "../Core/NetObject.h"
#include "../Core/NetServer.h"
#include "../Core/NetClient.h"
#include "../PHPMasterServer/MasterServer.h"
#include "Globals.h"

using namespace Siphon;

class ExplosiveCharacter;


class ExplosiveGame : public GameMgr<ExplosiveGame>, public NetObject, public NetServer::Callback, public NetClient::Callback
{
public:
	enum CharacterType
	{
		Human,
		AI,
		Network
	};

	enum NetworkMessage
	{
		NM_SetGameState,
		NM_AddCharacterNetwork,
		NM_AddCharacterSelf,
		NM_SetGameMode,
		NM_Bomb,
		NM_RemoveCharacterNetwork,
		NM_Join,
		NM_ServerInfo,
		NM_Chat,
		NM_Disconnect,
		NM_SetLevel,
		NM_TimeRemaning,
	};

	virtual bool Init(int argc, char **argv);
	virtual void Shutdown();

	ExplosiveCharacter* AddNetworkCharacter(int idx, int characterId = -1);
	ExplosiveCharacter* AddHumanCharacter(int idx, ExplosiveInputController::Keyboard keys, int characterId = -1);
	ExplosiveCharacter* AddHumanCharacter(int idx, int joystickIdx, int characterId = -1);
	ExplosiveCharacter* AddAICharacter(int idx, int characterId = -1);
	
	virtual void AddCharacter(Character* character);
	virtual void RemoveCharacter(Character* character);

	void PreRestartPlayers();

	virtual void Start();
	virtual void Restart();
	virtual void Pause();
	virtual void Resume();
	virtual void EndRound();
	virtual void EndGame();

	// safe to call from game code
	virtual void SetEndGame();
	virtual void SetRestart(bool viaNetwork = false);
	virtual void SetResume(bool viaNetwork = false);
	virtual void SetEndRound();

	virtual bool UpdateGame();
	virtual void Update(bool updateActors = true);
	virtual void Render();

	void DoCameraShake(float camShake = 0.8f, int time = 20);
	void UpdateCameraShake();

	void DebugDraw();

	INIFile& GetIniFile() { return iniSettings; }

	// game options
	virtual void SetActiveGameMode(GameMode* gameMode);

	void SetServerName(const char* serverName);
	const char* GetServerName();

	void SetClientName(const char* clientName);
	const char* GetClientName();

	void SetAIInStats(bool value);
	bool GetAIInStats() { return AIInStats; }

	void SetAICanWin(bool value);
	bool GetAICanWin() { return AICanWin; }

	void SetRoundsPerMap(int rounds);
	int GetRoundsPerMap() { return roundsPerMap; }

	void SetTimeLimit(int time);
	int GetTimeLimit() { return timeLimit; }

	// network shenanigens
	virtual unsigned int GetConstructorDataSize() { return 0; }
	virtual void GetConstructorData(char* data) { }
	virtual void ReceiveMessage(const void* message, int length, NetExtension::NetMessageTarget target);
	virtual NetType* GetNetType() { return 0; }

	virtual void AcceptConnection(int index, Connection& client);
	virtual void ClientDisconnect(int index, Connection& client);
	void DeleteClient(int index);

	virtual void AcceptConnection(int flags);
	virtual void ClientDisconnect();
	void CheckDisconnect();
	void PlayerConnected(int clientId);
	void RegisterCallbacks();

	MasterServer& GetMasterServer() { return masterServer; }

	void AddServerToMaster();
	void RemoveServerFromMaster();

	void SendChat(const char* msg);

	inline bool DebuggerCheck1();
	inline bool DebuggerCheck2();

	inline void Crash1();
	inline void Crash2();
	inline void Crash3();
	inline void Crash4(const char* file); // invalid the exe by modifying it so it dies

	inline bool IsDebuggerAttached() { return debuggerAttached; }
	inline bool MD5Check();

	void Disconnect();

	Menu& GetMenu() { return menu; }
	void	SentTimeRemaning();

protected:


	GameState				setGameState;

	ExplosiveTagTeamGame	tagTeamGame;
	DeathmatchGame			deathmatchGame;

	int	timeLimit;
	int roundsPerMap;
	int winsToEnd;
	bool AIInStats;
	bool AICanWin;

	Window*	window;
	Menu	menu;
	INIFile iniSettings;

	int		cameraShakeTime;
	float	cameraShake;
	Vector3 cameraOffset;

	bool	cameraDebug;
	bool	actorDebug;
	bool	fpsDebug;

	bool	disconnect;

	MasterServer	masterServer;
	std::string		serverName;
	std::string		clientName;
	unsigned long	masterServerPing;
	//unsigned long	natPunchCheckTime;

	bool			debuggerAttached;
	std::string		fileName;
	int				roundsRemaning;
	unsigned long	timeRemaning;
	unsigned long	lastTime;

	unsigned long	removeChatTimer;

	bool			debugCharacter;
};

inline bool ExplosiveGame::DebuggerCheck1()
{

#ifndef COPY_PROTECTION
	debuggerAttached = false;
	return false;
#endif


	// some lame ass copy protection
	__try
	{
		debuggerAttached = true;
		_asm
		{
			pushfd
			or dword ptr [esp], 0x100 //set the trap flag
			popfd
			nop
		}
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		debuggerAttached = false;
		return false;
	}

	if (debuggerAttached)
	{
		Crash1();
	}

	return true;
}

inline bool ExplosiveGame::DebuggerCheck2()
{

#ifndef COPY_PROTECTION
	debuggerAttached = false;
	return false;
#endif

	_asm
	{
		//Taken from Elad's book.
		mov eax,fs:[0x18]
		mov eax,[eax+0x30]
		cmp byte ptr [eax+0x2], 0
		je RunProgram
	}

	debuggerAttached = true;
	Crash2();
	//Crash4("data/bomb.MDL");
	return true;

RunProgram:
	debuggerAttached = false;
	return false;

	return false;
}

inline void ExplosiveGame::Crash4(const char* file)
{
	int r = rand() % 100;
	if (r < 5)
	{
		File f(file, "rb+");
		if (!f.Valid())
			return;

		int size = f.Size();
		r = rand() % size;
		f.Seek(r, SEEK_SET);
		int overwrite;
		f.Write(&overwrite, sizeof(int));
	}
}

inline void ExplosiveGame::Crash1()
{
	_asm
	{
		rdtsc
		push eax
		ret
	}
}

inline void ExplosiveGame::Crash2()
{
	// delete stack memory, owchies
	int nothing = 0;
	delete[] &nothing;
}

inline void ExplosiveGame::Crash3()
{
	int nothing = 10;
	nothing /= 0;
}

inline bool ExplosiveGame::MD5Check()
{/*
#ifndef COPY_PROTECTION
	return true;
#endif

#ifndef _DEMO
	MD5 md5;
	File f(fileName.c_str(), "r");
	md5.Update(&f);
	md5.Finalize();
/*
	char* digest = md5.HexDigest();
	if (strcmp(digest, MD5_CHECKSUM) == 0)
		return true;
* /
	return false;
#endif*/
	return true;
}

#endif