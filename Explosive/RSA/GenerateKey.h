#ifndef _GENERATEKEY_H_
#define _GENERATEKEY_H_

#include "Key.h"

// http://www.di-mgt.com.au/rsa_alg.html#keygen
class GenerateKeys
{
public:
	void	Generate(Key& publicKey, Key& privateKey);

protected:
	long	GeneratePrime(long ignorePrime = 1);
	long	SelectE(long undef);
	long	CalculateD(long undef, long e);
	bool	IsRelativePrime(float x, float y);
};

#endif