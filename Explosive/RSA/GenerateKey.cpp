#include "GenerateKey.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void GenerateKeys::Generate(Key& publicKey, Key& privateKey)
{
	// 1. Generating random prime numbers
	long prime1 = GeneratePrime();
	long prime2 = GeneratePrime(prime1);

	// 2. calculate n = p * q
	long n = prime1 * prime2;

	// 3. 0 = (p - 1)(q - 1)
	long undef = (prime1 - 1) * (prime2 - 1);

	// 4. Select e
	long e = SelectE(undef);

	// 5. Calculate d
	long d = CalculateD(undef, e);

	// ops, weve got bad values
	if (d == -1)
	{
		Generate(publicKey, privateKey);
		return;
	}

	// 6. the keys...
	publicKey.mPrimary = e;
	publicKey.mSecondary = n;

	privateKey.mPrimary = d;
	privateKey.mSecondary = n;
}

long GenerateKeys::SelectE(long undef)
{
	for (float i = 2.f; i < 100000.f; ++i)
	{
		if (IsRelativePrime((float)undef, (float)i))
			return i;
	}

	return -1;
}

long GenerateKeys::CalculateD(long undef, long e)
{
	float d;
	long dDash;

	for (float k = 1.f; k < 100000.f; ++k)
	{
		d = float(undef * k + 1) / float(e);
		dDash = (undef * k + 1) / e;

		if (long(d) == dDash)
			return dDash;
	}

	return -1;
}

bool GenerateKeys::IsRelativePrime(float x, float y)
{
	float r;

	//if x is smaller then swap the values
	if (x < y)
	{
		x = x + y;
		y = x - y;
		x = x - y;
	}

	while (1)
	{
		if (y == 0) 
			break;

		r = fmod(x, y);
		x = y;
		y = r;
	}

	if (x != 1) 
		return false;

	return true; //relatively prime
}


long GenerateKeys::GeneratePrime(long ignorePrime)
{
	long odd;
	long tmpOdd;
	double tmp;

	srand((unsigned)time(0));
	
	while(1)
	{
		odd = rand();
		if (odd % 2 == 0)
			odd++;

		if (ignorePrime == odd) 
			continue;

		// test whether 'odd' is prime number or not
		int i = 2;
		for (; i <= odd / 2; ++i)
		{
			tmpOdd = odd % i;

			if (tmpOdd == 0) 
			{
				--i;
				break;
			}
		}

		if (i != -1) 
			break;
	} 
	return odd;
}