#include "Key.h"

bool Key::Encrypt(void* inData, void* outData, unsigned long size)
{
	if ((size % sizeof(float)) != 0)
		return false;

	float* inDataFloat = static_cast<float*>(inData);
	float* outDataFloat = static_cast<float*>(outData);

	int floatSize = size / sizeof(float);

	char binaryStr[8];
	int binaryStrLen = ConvertToBinaryString(mPrimary, binaryStr);

	for (int i = 0; i < floatSize; ++i)
	{
		*outDataFloat = *inDataFloat;
/*
		float c;
		float d;

		for (i=k;i>=0;i--)
		{

		}
*/
		++inDataFloat;
		++outDataFloat;
	}
/*
	int NO_BITS=32;
	double c=0,d=1;
	char bits[100];
	double n=(double)m_n;
	unsigned char ch;
	double data;//19;
	long i,k=NO_BITS;
	CString str;
	int sizeof_d=sizeof(double);


	FILE *inFP, *outFP;
	inFP=fopen(infile,"rt");
	if(inFP==NULL)
	{
		str.Format("\nERROR: Couldn't open input file: %s\nAborting operation",infile);
		DumpNotes(str);
		return;
		
	}

	outFP=fopen(outfile,"wb");
	if(outFP==NULL)
	{
		str.Format("\nERROR: Couldn't create output file: %s\nAborting operation",outfile);
		DumpNotes(str);
		fcloseall();
		return;
		
	}
	

	//change integer 'm' to binary 
	D_to_B(m_e,32,bits);
	GetOnlyProperBits(bits);
	k=NO_BITS = strlen(bits)-1;

	while(1)
	{
		//read from ip file into 'data'
		if(fread(&ch,1,1,inFP)==0)
			break;
		data =(double)ch;

		//calculate ((data)^e mod n) .i.e the remainder
		c=0;d=1;
		for(i=k;i>=0;i--)
		{
			c=2*c;
			d=fmod(d*d,n);
	
			if(bits[NO_BITS-i] == '1')
			{
				c=c+1;
				d=fmod(data*d,n);
			}//end of IF		
		}//end of for loop 

		//write to op file from 'd'
		fwrite(&d,sizeof_d,1,outFP);
		
	}//end of while loop 
*/
	return true;
}

bool Key::Decrypt(void* inData, void* outData, unsigned long size)
{
	return true;
}

int Key::ConvertToBinaryString(long value, char* buffer)
{
	char temp[8];
	int last = 0;
	for (int i = 0; i < 8; ++i)
	{
		int remainder = value % 2;
		value = value / 2;
		temp[i] = (remainder == 0) ? '0' : '1';

		if (remainder == 1)
			last = i + 1;
	}

	//if (last > 8)
	//	last = 8;

	for (int j = last - 1; j >= 0; --j)
	{
		buffer[j] = temp[last - j];
	}

	return last;
}