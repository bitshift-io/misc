#ifndef _KEY_H_
#define _KEY_H_

class Key
{
public:
	bool Encrypt(void* inData, void* outData, unsigned long size);
	bool Decrypt(void* inData, void* outData, unsigned long size);

	long mPrimary;
	long mSecondary;

private:
	int ConvertToBinaryString(long value, char* buffer);
};

#endif