//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4 	worldViewProj 	: WorldViewProjection;

float4x4 	projTexture 	: ProjectorWorldViewProjectionTexture;
float4x4 	proj		 	: ProjectorWorldViewProjection;

float3   	lightPos 		: LightPosition;
float3   	eyePos 			: EyePosition;

sampler2D shadowTexture = sampler_state
{
    Texture = <shadowTexture>;
    minFilter = Linear;
    magFilter = Linear;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
    float3 position 	: POSITION;	
    float2 uvCoord 		: TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 position : POSITION;
	float4 uvCoord 	: TEXCOORD0; // should projectors use 0 - n? and lights?
	float4 zDepth	: TEXCOORD1;
	float4 posTest : TEXCOORD2;
};

struct PS_OUTPUT
{
	float4 colour	: COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs( const VS_INPUT IN,
			uniform float4x4 modelViewProjection,
			uniform float4x4 projection,
			uniform float4x4 projectionTexture)
{
    VS_OUTPUT OUT;
    
   	OUT.position = mul(modelViewProjection, float4(IN.position, 1.0));
	OUT.posTest = OUT.position;
							 
	float4 projSpacePos = mul(projectionTexture, float4(IN.position, 1.0));
	OUT.uvCoord = projSpacePos;
	
	float4 sceneDepth = mul(projection, float4(IN.position, 1.0));
	OUT.zDepth = sceneDepth / sceneDepth.w; //.z;
	//OUT.zDepth.z = OUT.position.z;
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

float Difference(uniform sampler2D shadow, float depth, float4 texCoord)
{
	float shadowDepth = tex2Dproj(shadow, texCoord).r;
	return (depth - 0.008) - shadowDepth;
}

float DepthCheck(uniform sampler2D shadow, float depth, float4 texCoord)
{
	if (Difference(shadow, depth, texCoord) > 0.0)
		return 0.1;
	else
		return 1.0;
}

float ShadowPcnt(uniform sampler2D shadow, float4 texCoord, float depth)
{
	float shadowPct = 0.0;
	
	float innerDist = 1.0 / 2048.0;
	float outterDist = innerDist * 2.0;
	
	//float d = max(0.001, Difference(shadow, depth, texCoord)) * 1000;//max(1.0, Difference(shadow, depth, texCoord));
	
	shadowPct += DepthCheck(shadow, depth, texCoord);
	
	int divCount = 1;
	float dist = innerDist;
	for (int i = 0; i < 4; ++i)
	{
		shadowPct += DepthCheck(shadow, depth, texCoord + float4(dist, dist, 0.0, 0.0));
		shadowPct += DepthCheck(shadow, depth, texCoord + float4(-dist, dist, 0.0, 0.0));
		shadowPct += DepthCheck(shadow, depth, texCoord + float4(-dist, -dist, 0.0, 0.0));
		shadowPct += DepthCheck(shadow, depth, texCoord + float4(dist, -dist, 0.0, 0.0));
		
		shadowPct += DepthCheck(shadow, depth, texCoord + float4(dist, 0.0, 0.0, 0.0));
		shadowPct += DepthCheck(shadow, depth, texCoord + float4(-dist, 0.0, 0.0, 0.0));
		shadowPct += DepthCheck(shadow, depth, texCoord + float4(0.0, dist, 0.0, 0.0));
		shadowPct += DepthCheck(shadow, depth, texCoord + float4(0.0, -dist, 0.0, 0.0));
		
		divCount += 8;
		dist += innerDist;// * d;
	}

	shadowPct /= divCount;
	return shadowPct;
}

PS_OUTPUT myps(VS_OUTPUT IN, uniform sampler2D shadow)
{
	PS_OUTPUT OUT;

	//OUT.colour = float4(1.0, 1.0, 1.0, 1.0) * ShadowPcnt(shadow, IN.uvCoord, IN.zDepth.z);
	 
	float4 depth = tex2Dproj(shadow, IN.uvCoord);	
	OUT.colour = depth.r < IN.zDepth.z - 0.005 ? float4(0.5, 0.5, 0.5, 1.0) : float4(1.0, 1.0, 1.0, 1.0);

	OUT.colour = float4(depth.rrr,1);
	//OUT.colour = float4(0.0, 0.0, 0.0, 1.0); //depth.r, depth.r, depth.r, 0.0);
	
	//OUT.colour += depth;
	
	//OUT.colour = float4(2.0 * (IN.zDepth.z/IN.zDepth.w - depth.r), 2.0 * (IN.zDepth.z/IN.zDepth.w - depth.r), 2.0 * (IN.zDepth.z/IN.zDepth.w - depth.r), 0.0f);
	
	OUT.colour = float4(IN.zDepth.z - IN.position.z, IN.zDepth.z - IN.position.z, IN.zDepth.z - IN.position.z, 0.0f);
	
	//float value = 20.0 * ((IN.posTest.z/IN.posTest.w)/50.0 - IN.position.z);
	//OUT.colour = float4(value, value, value, 0.0f);
	
	
	//OUT.colour = float4(2.0 * (IN.zDepth.z - depth.r), 2.0 * (IN.zDepth.z - depth.r), 2.0 * (IN.zDepth.z - depth.r), 0.0f);
	//OUT.colour += float4(IN.zDepth.z/IN.zDepth.w, IN.zDepth.z/IN.zDepth.w, IN.zDepth.z/IN.zDepth.w, 0.0f);
	
	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{		
		DepthTestEnable = true;
		DepthMask = true;
		DepthFunc = lequal;
		FrontFace = CW;
		CullFace = front;
		
		VertexProgram = compile arbvp1 myvs(worldViewProj, proj, projTexture);
        FragmentProgram  = compile arbfp1 myps(shadowTexture);
    }
}
