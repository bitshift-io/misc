
float4x4 worldViewProj : WorldViewProjection;

struct VS_INPUT
{
    float3 position 	: POSITION;
};

struct VS_OUTPUT
{
	float4 position 	: POSITION;
	float4 pos		 	: TEXCOORD0;
};

struct PS_OUTPUT
{
	float4 color : COLOR;
};

//-----------------------------------------------------------------------------
// Vertex Shader
//-----------------------------------------------------------------------------

VS_OUTPUT myvs(const VS_INPUT IN,
			uniform float4x4 modelViewProjection)
{
    VS_OUTPUT OUT;
    OUT.position = mul(modelViewProjection, float4(IN.position, 1.0));
    OUT.pos = OUT.position / OUT.position.w;
	return OUT;
}

//-----------------------------------------------------------------------------
// Pixel Shader
//-----------------------------------------------------------------------------

PS_OUTPUT myps( VS_OUTPUT IN )
{
	PS_OUTPUT OUT;
	OUT.color = float4(0,0,0,1); //IN.pos.zzzz;
	return OUT;
}

//-----------------------------------------------------------------------------
// Effect
//-----------------------------------------------------------------------------

technique Technique0
{
	pass Pass0
	{		
		DepthTestEnable = true;
		DepthMask = true;
		DepthFunc = lequal;
		FrontFace = CW;
		CullFace = front;
		
		VertexProgram = compile arbvp1 myvs(worldViewProj);
       	FragmentProgram  = compile arbfp1 myps();
    }
}
