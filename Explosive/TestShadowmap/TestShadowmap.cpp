#include "../Core/GameBase.h"

#include "../Engine/Win32Window.h"
#include "../Engine/Win32Input.h"
#include "../Engine/Win32Time.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"
#include "../Engine/Font.h"
#include "../Engine/Camera.h"
#include "../Engine/Renderer.h"
#include "../Engine/Material.h"
#include "../Engine/Projector.h"

#include "../Util/Log.h"
#include "../Util/Macros.h"

using namespace Siphon;

class TestShadowmap : public GameBase<TestShadowmap>
{
public:
	TestShadowmap() : angle(0)
	{

	}

	bool Init(int argc, char **argv)
	{
		GameBase::Init(argc, argv);

		lightDist = 3.0f;

		window = new Window();
		window->Create("TestShadowmap", 512, 512, 16, false);
		window->ShowWindow();
		Device::GetInstance().SetActiveWindow(window);

		Device::GetInstance().CreateDevice();
		Device::GetInstance().SetClearColour(0.5, 0.5, 0.5);

		Input::GetInstance().CreateInput(window);
		
		camera = new Camera();
		camera->SetLookAt( Vector3(0, 5, -10), Vector3(0, 0, 0) );
		camera->SetSpeed(0.1f);
		camera->SetFreeCam(true);
		camera->SetPerspective(90, 1.0, 10.0f, 0.5f);

		shadowCam = new Camera();
		shadowCam->SetPerspective(90, 1.0, 10.0f, 0.5f);
		shadowCam->SetFreeCam(false);
		shadowCam->SetLookAt( Vector3(0.0, 5.0, -5), Vector3(0, 0, 0) );
		shadowCam->Update();

		renderMgr.SetCamera(camera);

		// set up projector
		shadowProjector.SetCamera(shadowCam);
		shadowProjector.SetTexture(shadowTexture.GetDepthTexture()); //GetColourTexture()); //
		renderMgr.InsertProjector(&shadowProjector);
		shadowTexture.SetClearColour(1, 1, 1);
 
		skybox = Mesh::LoadMesh("skybox.MDL");
		renderMgr.InsertMesh(skybox);
 
		scene = Mesh::LoadMesh("scene.MDL");
		renderMgr.InsertMesh(scene);

		sceneLight = Mesh::LoadMesh("scene.MDL");
		for (int i = 0; i < sceneLight->GetMeshMat().material.GetSize(); ++i)
		{
			Material* mat = sceneLight->GetMeshMat().material[i].material;
			if (strcmpi(mat->GetName(), "data/diffuse.mat") == 0)
			{
				sceneLight->GetMeshMat().material[i].material = MaterialMgr::Load("light.mat");
			}
		}

		shadowRndr.SetCamera(shadowCam);
		shadowRndr.InsertMesh(sceneLight);

		shadowTexture.Create(2048, 2048, RenderTexture::TextureFlags(Texture::TF_FloatFormat | RenderTexture::TF_DepthTexture | RenderTexture::TF_Colour)); 
		shadowRndr.SetRenderTarget(&shadowTexture);

		font = FontMgr::Load("arial.fnt");

		// NVNOTE: comment out this for loop, and you can see it working
		// all be it shocking frame rate for some reason

		// assign render target texture to materials
		for (int i = 0; i < scene->GetMeshMat().material.GetSize(); ++i)
		{
			Material* mat = scene->GetMeshMat().material[i].material;

			Material::Param* param = mat->GetParameterByName("shadowTexture");
			if (!param || param->type != Material::Param::ParamTexture)
				continue;

			Material::TextureParam* texParam = (Material::TextureParam*)param;

			if (!texParam->texture)
				texParam->texture = shadowTexture.GetDepthTexture();//shadowTexture.GetColourTexture(); //shadowTexture.GetDepthTexture();
		}

		Device::GetInstance().CheckErrors();
		Log::Print("Loading complete.\n");
		return true;
	}

	virtual void Shutdown()
	{
		SAFE_DELETE(camera);
	}

	virtual bool UpdateGame()
	{
		Input::GetInstance().Update();

		if (window->ShouldExit() || window->HandleMessages())
			return false;

		if (gInput.KeyPressed(Input::KEY_F1))
			gDevice.SaveScreenShot();

		// update shadow cam animation
		angle += 0.3f;
		shadowCam->SetLookAt( Vector3(-lightDist * sin(Math::DtoR(angle)), 2.0f, -lightDist * cos(Math::DtoR(angle))), Vector3(0, 0, 0) );
		shadowCam->Update();

		//shadowCam->SetLookAt(*camera);
		//shadowCam->Update();

		camera->Update();

		renderMgr.SetTime(Time::GetInstance().GetTimeSeconds());
		
		if (Input::GetInstance().KeyPressed(Input::KEY_z))
			lightDist += 1.0f;

		if (Input::GetInstance().KeyPressed(Input::KEY_x))
			lightDist -= 1.0f;

		if (Input::GetInstance().KeyPressed(DIK_ESCAPE))
			return false;

		return true;
	}

	virtual void UpdatePhysics()
	{

	}

	void DrawDebug(Mesh* scene)
	{
		Matrix4 identity(Matrix4::Identity);
		identity.Draw();

		const Sphere& sphere = scene->GetBoundSphere();
		sphere.Draw();
	}

	virtual void Render()
	{
		// NVNOTE: this is causing shit house frame rates,
		// have a quick look at this to if your bored ;)
		shadowRndr.Render();

		Device::GetInstance().BeginRender();
		renderMgr.Render();

		unsigned int fps = Device::GetInstance().GetFPS();
		font->Update();
		font->Print(0.1f, 0.1f, 0.5f, "FPS: %i", fps);
		font->Render();

		shadowTexture.GetDepthTexture()->Draw(0, 0, 256);
		//shadowTexture.GetColourTexture()->Draw(0, 128, 128);
		Device::GetInstance().EndRender();
	}

protected:
	float			lightDist;
	Window*			window;
	float			angle;
	Font*			font;
	Camera*			camera;
	Mesh*			scene;
	Mesh*			sceneLight;
	Mesh*			skybox;
	Renderer		renderMgr;
	Renderer		shadowRndr;
	Camera*			shadowCam;
	RenderTexture	shadowTexture;
	Projector		shadowProjector;
};

MAIN(TestShadowmap)
