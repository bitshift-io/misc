#ifndef _SIPHON_EXPORTMODEL_
#define _SIPHON_EXPORTMODEL_

#include <vector>
#include <list>
#include <map>
#include <set>

#include "ExportBase.h"

class ExportModel : public ExportBase
{
public:
	virtual void Initilize( IGameScene* scene, TCHAR *name, Interface* ip );

	// clean up code
	virtual void Shutdown();

	// add this node to the scene
	virtual bool ProcessNode( IGameNode* pNode );

	// add a new mesh to the scene
	virtual bool ProcessMesh(IGameNode* node);

	// add a new skin to the scene
	virtual bool ProcessSkin(IGameNode* node);

	// add a new mesh reference
	virtual bool ProcessReference(IGameNode* node);

	// process any object we need to
	virtual bool ProcessScene();

	virtual bool SaveScene();

	// given a node, return a skeleton
	ExpSkeleton* GetSkeleton(IGameNode* node);

	virtual void InitDialog(HWND panel);
	virtual void UpdateDialog();

protected:
	ExpModel model;
	std::vector<ExpMesh*> meshes;
	std::vector<ExpSkeleton*> skeletons;
	std::vector<ExpObject*> objects;

	IGameScene* scene;
	FILE* pFile;
	TCHAR* fileName;
	Interface* ip;

	bool batchEnable;
	bool exportMaterials;
};

#endif