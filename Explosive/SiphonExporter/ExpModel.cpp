
#include "SiphonExporter.h"

ExpModel::ExpModel() :
	colours(false),
	uvCoords(false),
	uvCoords2(false),
	normals(false),
	binormals(false),
	tangents(false),
	bones(false),
	weights(false)
{
	meshMats.vertexBuffer = new VertexBuffer();
}

ExpModel::~ExpModel()
{
	SAFE_DELETE(meshMats.vertexBuffer);
}

bool ExpModel::AddMesh(IGameNode* pGameNode, int materialId, IGameMaterial* pGameMaterial)
{
	FaceMesh fMesh;
	fMesh.pMesh = (IGameMesh*)pGameNode->GetIGameObject();
	fMesh.materialId = materialId;
		
	meshes[pGameMaterial].push_back(fMesh);
	return true;
}

void ExpModel::Process()
{
	int c;

	//for each material....
	std::map<IGameMaterial*, std::vector<FaceMesh> >::iterator matIt; 
	for (matIt = meshes.begin(); matIt != meshes.end(); matIt++)
	{
		// create material table
		int index = meshMats.material.GetSize();
		meshMats.material.SetSize(index+1);
		Siphon::Mesh::MaterialTable& curMatTable = meshMats.material[index]; 
	    
		curMatTable.material = new ExpMaterial(matIt->first);
		curMatTable.startIndex = meshMats.vertexBuffer->indices.GetSize();
		curMatTable.numIndices = 0;

		// for each set of faces
		std::vector<FaceMesh>::iterator faceMeshIt;
		for (faceMeshIt = matIt->second.begin(); faceMeshIt != matIt->second.end(); faceMeshIt++)
		{
			//compare against existing vertices
			bool bUvs = faceMeshIt->pMesh->GetNumberOfTexVerts();
			bool bNormals = faceMeshIt->pMesh->GetNumberOfNormals();
			bool bColors = faceMeshIt->pMesh->GetNumberOfColorVerts();

			Tab<FaceEx*> faces = faceMeshIt->pMesh->GetFacesFromMatID(faceMeshIt->materialId);
			for (int f = 0; f < faces.Count(); ++f)
			{
				FaceEx* face = faces[f];
				for(int i = 0; i < 3; ++i)
				{
					Point2 uv;
					Point3 normal;
					Point3 color;
					Point3 position;

					Colour colColour;
					Vector3 vecNormal;
					UVCoord	uvUVCoord;
					Vector3 vecPosition;

					if (bUvs)
					{
						uv = faceMeshIt->pMesh->GetTexVertex(face->texCoord[i]);
						uvUVCoord = UVCoord(uv.x, uv.y);
					}

					if (bNormals)
					{
						normal = faceMeshIt->pMesh->GetNormal(face->norm[i]);
						vecNormal = Vector3(normal.x, normal.y, normal.z);
					}

					if (bColors)
					{
						color = faceMeshIt->pMesh->GetColorVertex(face->color[i]);
						colColour = Colour(color.x, color.y, color.z);
					}

					position = faceMeshIt->pMesh->GetVertex(face->vert[i]);
					vecPosition = Vector3(position.x, position.y, position.z);

					for (c = 0; c < meshMats.vertexBuffer->positions.GetSize(); c++)
					{
						// compare position
						if (vecPosition != meshMats.vertexBuffer->positions[c])
							continue;

						// compare uv's
						if (bUvs && uvUVCoord != meshMats.vertexBuffer->uvCoords[c])
							continue;

						// compare colours
						if (bColors && colColour != meshMats.vertexBuffer->colours[c])
							continue;

						// compare normals
						if (bNormals && vecNormal != meshMats.vertexBuffer->normals[c])
							continue;

						// add a new index
						++curMatTable.numIndices;
						unsigned int idx = c;
						meshMats.vertexBuffer->indices.Insert(idx);
						break;
					}

					// if we didnt find duplicate info... insert it
					if (c == meshMats.vertexBuffer->positions.GetSize())
					{
						++curMatTable.numIndices;
						unsigned int index = meshMats.vertexBuffer->positions.GetSize();
						meshMats.vertexBuffer->indices.Insert(index);
						meshMats.vertexBuffer->positions.Insert(vecPosition);

						if (bColors)
							meshMats.vertexBuffer->colours.Insert(colColour);

						if (bNormals)
							meshMats.vertexBuffer->normals.Insert(vecNormal);

						if (bUvs)
							meshMats.vertexBuffer->uvCoords.Insert(uvUVCoord);
					}
				}
			}
		}
		curMatTable.numPrimitives = curMatTable.numIndices / 3;
	}
}

bool ExpModel::AddMeshes(std::map<IGameMaterial*, std::vector<ExpMesh*> >& materialCache, bool batchEnable)
{
	std::map<IGameMaterial*, std::vector<ExpMesh*> >::iterator matIt;

	// calculate vertex format
	for (matIt = materialCache.begin(); matIt != materialCache.end(); matIt++)
		CalculateVertexFormat(matIt->second);

	for (matIt = materialCache.begin(); matIt != materialCache.end(); matIt++)
		AddMeshes(matIt->second, matIt->first, batchEnable);

	return true;
}

bool ExpModel::AddMeshes(std::vector<ExpMesh*>& meshes, IGameMaterial* pGameMaterial, bool batchEnable)
{
	CalculateVertexFormat(meshes);

	if (batchEnable)
		return AddBatched(meshes, pGameMaterial);
	else
		return AddNonBatched(meshes, pGameMaterial);
}

void ExpModel::CalculateVertexFormat(std::vector<ExpMesh*>& meshes)
{
	std::vector<ExpMesh*>::iterator meshIt;
	for (meshIt = meshes.begin(); meshIt != meshes.end(); meshIt++)
	{
		if ((*meshIt)->buffer.colours.GetSize())
			colours = true;

		if ((*meshIt)->buffer.uvCoords.GetSize())
			uvCoords = true;

		if ((*meshIt)->buffer.normals.GetSize())
			normals = true;

		if ((*meshIt)->buffer.uvCoords2.GetSize())
			uvCoords2 = true;

		if ((*meshIt)->buffer.binormals.GetSize())
			binormals = true;

		if ((*meshIt)->buffer.tangents.GetSize())
			tangents = true;

		if ((*meshIt)->buffer.bones.GetSize())
			bones = true;

		if ((*meshIt)->buffer.weights.GetSize())
			weights = true;

		if (!(*meshIt)->buffer.positions.GetSize())
			MessageBox(0, "Fuck were gonna die!", "Invalid mesh format", MB_OK);
	}
}

bool ExpModel::AddBatched(std::vector<ExpMesh*>& meshes, IGameMaterial* pGameMaterial)
{
	int index = meshMats.material.GetSize();
	meshMats.material.SetSize(index+1);
	Siphon::Mesh::MaterialTable& curMatTable = meshMats.material[index]; 
    
	curMatTable.material = new ExpMaterial(pGameMaterial);
	curMatTable.startIndex = meshMats.vertexBuffer->indices.GetSize();
	curMatTable.numIndices = 0;

	std::vector<ExpMesh*>::iterator meshIt;
	for (meshIt = meshes.begin(); meshIt != meshes.end(); meshIt++)
	{
		// TODO: we cant do a strait out copy,
		// we need to check vertices from same
		// material together (but its already done for same model)
		int vertexOffset = meshMats.vertexBuffer->positions.GetSize();
		curMatTable.numIndices += (*meshIt)->buffer.indices.GetSize();

		// all buffers should be the same size
		unsigned int newSize = (*meshIt)->buffer.positions.GetSize();
		unsigned int newTotalSize = meshMats.vertexBuffer->positions.GetSize() + newSize;
		unsigned int oldSize = meshMats.vertexBuffer->positions.GetSize();

		// copy positions
		meshMats.vertexBuffer->positions.SetCapacity(newTotalSize);
		meshMats.vertexBuffer->positions.SetSize(newTotalSize);

		memcpy(meshMats.vertexBuffer->positions.GetData() + vertexOffset, 
			(*meshIt)->buffer.positions.GetData(), 
			sizeof(Vector3) * newSize);

		// copy uvcoords
		if (uvCoords)
		{
			meshMats.vertexBuffer->uvCoords.SetCapacity(newTotalSize);
			meshMats.vertexBuffer->uvCoords.SetSize(newTotalSize);

			// input data, if no data exists, put in blank data
			if ((*meshIt)->buffer.uvCoords.GetSize())
			{
				memcpy(meshMats.vertexBuffer->uvCoords.GetData() + vertexOffset, 
					(*meshIt)->buffer.uvCoords.GetData(), 
					sizeof(UVCoord) * newSize);
			}
			else
			{
				for (int i = oldSize; i < newTotalSize; ++i)
					meshMats.vertexBuffer->uvCoords[i] = UVCoord(0,0);
			}
		}

		// copy uvcoords2
		if (uvCoords2)
		{
			meshMats.vertexBuffer->uvCoords2.SetCapacity(newTotalSize);
			meshMats.vertexBuffer->uvCoords2.SetSize(newTotalSize);

			// input data, if no data exists, put in blank data
			if ((*meshIt)->buffer.uvCoords2.GetSize())
			{
				memcpy(meshMats.vertexBuffer->uvCoords2.GetData() + vertexOffset, 
					(*meshIt)->buffer.uvCoords2.GetData(), 
					sizeof(UVCoord) * newSize);
			}
			else if (uvCoords && (*meshIt)->buffer.uvCoords.GetSize())
			{
				// here we have a second uv channel, in the scene, but this mesh doesnt,
				// so by default, duplicate the uvcoords to this second channel
				memcpy(meshMats.vertexBuffer->uvCoords2.GetData() + vertexOffset, 
					(*meshIt)->buffer.uvCoords.GetData(), 
					sizeof(UVCoord) * newSize);
			}
			else
			{
				for (int i = oldSize; i < newTotalSize; ++i)
					meshMats.vertexBuffer->uvCoords2[i] = UVCoord(0,0);
			}
		}

		// copy normals
		if (normals)
		{
			meshMats.vertexBuffer->normals.SetCapacity(newTotalSize);
			meshMats.vertexBuffer->normals.SetSize(newTotalSize);

			// input data, if no data exists, put in blank data
			if ((*meshIt)->buffer.normals.GetSize())
			{
				memcpy(meshMats.vertexBuffer->normals.GetData() + vertexOffset, 
					(*meshIt)->buffer.normals.GetData(), 
					sizeof(Vector3) * newSize);
			}
			else
			{
				for (int i = oldSize; i < newTotalSize; ++i)
					meshMats.vertexBuffer->normals[i] = Vector3(0,0,0);
			}
		}

		// copy binormals
		if (binormals)
		{
			meshMats.vertexBuffer->binormals.SetCapacity(newTotalSize);
			meshMats.vertexBuffer->binormals.SetSize(newTotalSize);

			// input data, if no data exists, put in blank data
			if ((*meshIt)->buffer.normals.GetSize())
			{
				memcpy(meshMats.vertexBuffer->binormals.GetData() + vertexOffset, 
					(*meshIt)->buffer.binormals.GetData(), 
					sizeof(Vector3) * newSize);
			}
			else
			{
				for (int i = oldSize; i < newTotalSize; ++i)
					meshMats.vertexBuffer->binormals[i] = Vector3(0,0,0);
			}
		}

		// copy tangents
		if (tangents)
		{
			meshMats.vertexBuffer->tangents.SetCapacity(newTotalSize);
			meshMats.vertexBuffer->tangents.SetSize(newTotalSize);

			// input data, if no data exists, put in blank data
			if ((*meshIt)->buffer.normals.GetSize())
			{
				memcpy(meshMats.vertexBuffer->tangents.GetData() + vertexOffset, 
					(*meshIt)->buffer.tangents.GetData(), 
					sizeof(Vector3) * newSize);
			}
			else
			{
				for (int i = oldSize; i < newTotalSize; ++i)
					meshMats.vertexBuffer->tangents[i] = Vector3(0,0,0);
			}
		}

		// copy colours
		if (colours)
		{
			meshMats.vertexBuffer->colours.SetCapacity(newTotalSize);
			meshMats.vertexBuffer->colours.SetSize(newTotalSize);

			// input data, if no data exists, put in blank data
			if ((*meshIt)->buffer.normals.GetSize())
			{
				memcpy(meshMats.vertexBuffer->colours.GetData() + vertexOffset, 
					(*meshIt)->buffer.colours.GetData(), 
					sizeof(Colour) * newSize);
			}
			else
			{
				for (int i = oldSize; i < newTotalSize; ++i)
					meshMats.vertexBuffer->colours[i] = Colour(0,0,0);
			}
		}

		// copy bone info
		if (bones)
		{
			meshMats.vertexBuffer->bones.SetCapacity(newTotalSize);
			meshMats.vertexBuffer->bones.SetSize(newTotalSize);

			// input data, if no data exists, put in blank data
			if ((*meshIt)->buffer.normals.GetSize())
			{
				memcpy(meshMats.vertexBuffer->bones.GetData() + vertexOffset, 
					(*meshIt)->buffer.bones.GetData(), 
					sizeof(Vector4) * newSize);
			}
			else
			{
				for (int i = oldSize; i < newTotalSize; ++i)
					meshMats.vertexBuffer->bones[i] = Vector4(0,0,0,0);
			}
		}

		// copy weight info
		if (weights)
		{
			meshMats.vertexBuffer->weights.SetCapacity(newTotalSize);
			meshMats.vertexBuffer->weights.SetSize(newTotalSize);

			// input data, if no data exists, put in blank data
			if ((*meshIt)->buffer.normals.GetSize())
			{
				memcpy(meshMats.vertexBuffer->weights.GetData() + vertexOffset, 
					(*meshIt)->buffer.weights.GetData(), 
					sizeof(Vector4) * newSize);
			}
			else
			{
				for (int i = oldSize; i < newTotalSize; ++i)
					meshMats.vertexBuffer->weights[i] = Vector4(0,0,0,0);
			}
		}

		//copy over indices with offset
		for (int i = 0; i < (*meshIt)->buffer.indices.GetSize(); i++)
		{
			unsigned int val = (*meshIt)->buffer.indices[i] + vertexOffset;
			meshMats.vertexBuffer->indices.Insert(val);
		}
	}
	curMatTable.numPrimitives = curMatTable.numIndices / 3;
	return true;
}
/*
bool ExpModel::Save(const char* filename, bool exportMaterials)
{
	if (exportMaterials)
	{
		//extract directory
		char buffer[256];	
		strcpy(buffer, filename);
		for (int i = strlen(buffer); i >= 0; --i)
		{
			if (buffer[i] == '\\' || buffer[i] == '/')
			{
				buffer[i+1] = '\0';
				break;
			}
		}

		for (int i = 0; i < meshMats.material.GetSize(); ++i)
		{
		//	meshMats.material[i].material->Save(buffer);
		}
	}

	bool val = Mesh::Save(filename);
	return val;
}
*/
bool ExpModel::AddNonBatched(std::vector<ExpMesh*>& meshes, IGameMaterial* pGameMaterial)
{
/*	int i;
	Array<IGameMaterial*>	material;
	for (i = 0; i < material.GetSize(); ++i)
	{
		material[i]
	}

	std::vector<ExpMesh*>::iterator meshIt;
	for (meshIt = meshes.begin(); meshIt != meshes.end(); meshIt++)
	{
		//create a material table
		sMaterialTable* curMatTable;
		materialTable.push_back(sMaterialTable());
		gameMaterials.push_back(pGameMaterial);

		curMatTable = &(materialTable[materialTable.size() - 1]);
		curMatTable->startIndex = indices.size();

		int vertexOffset = positions.size();
		curMatTable->numIndices += (*meshIt)->indices.size();
		curMatTable->numPrimitives = curMatTable->numIndices / 3;

		// copy vertex data strait over
		for (int v = 0; v < (*meshIt)->positions.size(); v++)
		{
			Point2 uv = (*meshIt)->uvCoords[v];
			positions.push_back((*meshIt)->positions[v]);

			if ((*meshIt)->uvCoords.size())
				uvCoords.push_back((*meshIt)->uvCoords[v]);

			if ((*meshIt)->normals.size())
				normals.push_back((*meshIt)->normals[v]);

			if ((*meshIt)->colors.size())
				colors.push_back((*meshIt)->colors[v]);
		}

		// copy over index data with an offset
		for (int i = 0; i < (*meshIt)->indices.size(); i++)
		{
			int index = (*meshIt)->indices[i] + vertexOffset;
			indices.push_back((*meshIt)->indices[i] + vertexOffset);
		}
	}*/
	return true;
}


/*
int ExpModel::GetMaterialCount()
{
	return materialTable.size();
}
/*
sMaterialTable* ExpModel::GetMaterialTable(int index)
{
	return &(materialTable[index]);
}

IGameMaterial* ExpModel::GetGameMaterial(int index)
{
	return meshes[index].first;
}*/