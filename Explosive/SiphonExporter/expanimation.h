#ifndef _SIPHON_EXPSKINANIMATION_
#define _SIPHON_EXPSKINANIMATION_

#include "..\Engine\Animation.h"

class Quaternion;

class ExpAnimation : public ExpObject, public Siphon::AnimationStream
{
public:
	ExpAnimation(IGameNode* gameNode);

	virtual void Update(AnimationStream* parentAnimation); // maybe the offset should be put in via a constructor?
	virtual void Update() {};

protected:

};

#endif
