#include "SiphonExporter.h"

void ExportWorld::Initilize( IGameScene* scene, TCHAR *name, Interface* ip ) 
{
	this->scene = scene; 
	this->ip = ip;
	pFile = fopen( name, "wb" );

	//temporarily make the map 3x3
	//renderSectors.resize(4);
}

void ExportWorld::Shutdown()
{
	ExportData( pFile, 0, 0, ENDOFFILE );
	fclose( pFile );
}

bool ExportWorld::ExportNode( IGameNode* pNode )
{
	TSTR key_collision = "collision";
	TSTR key_sky = "sky";
	TSTR key_spawn = "spawn";
	TSTR key_sound = "sound";
	TSTR str;

	if( pNode->GetMaxNode()->GetUserPropString( key_spawn, str ) )
	{
		ExportSpawn( pNode );
		return true;
	}

	switch( pNode->GetIGameObject()->GetIGameType() )
	{
	case IGameObject::IGAME_MESH:
		if( pNode->GetMaxNode()->GetUserPropString( key_collision, str ) )
		{
			//collisionShapes.push_back( pNode );
			ExportCollision( (IGameMesh*)pNode->GetIGameObject(), pNode );
		}
		else if( pNode->GetMaxNode()->GetUserPropString( key_sky, str ) )
		{
			ExportSkyBox( (IGameMesh*)pNode->GetIGameObject(), pNode );
		}
		else if( pNode->GetMaxNode()->GetUserPropString( key_sound, str ) )
		{
			ExportSound( pNode );
		}
		else
		{
			ExportMesh( (IGameMesh*)pNode->GetIGameObject(), pNode );
		}
		break;
	}

	//loop through children
	int noChildren = pNode->GetChildCount();

	for( int i = 0; i < noChildren; ++i )
	{
		ExportNode( pNode->GetNodeChild(i) );		
	}

	return true;
}

bool ExportWorld::ExportSound( IGameNode* pNode )
{
	TSTR key_sound = "sound";
	TSTR str;
	pNode->GetMaxNode()->GetUserPropString( key_sound, str ); 

	sSound sound;
	strcpy( (char*)&sound.sound, str );

	GMatrix matrix = pNode->GetWorldTM();
	for( int i = 0; i < 3; ++i )
		sound.position[i] = matrix[3][i];

	INode* maxNode = pNode->GetMaxNode();
	ObjectState os = maxNode->EvalWorldState( ip->GetTime() );

	if( os.obj->SuperClassID() == GEOMOBJECT_CLASS_ID ) 
	{
		Object* obj = os.obj;

		if( obj && obj->ClassID() == Class_ID(SPHERE_CLASS_ID, 0) )
		{
			IParamArray* array = obj->GetParamBlock();
			array->GetValue( obj->GetParamBlockIndex(SPHERE_RADIUS), 0, sound.radius, FOREVER);

			ExportData( pFile, &sound, sizeof(sSound), SOUND );
		}
	}

	return true;
}

bool ExportWorld::ExportSpawn( IGameNode* pNode )
{
	TSTR key_spawn = "spawn";
	TSTR str;
	pNode->GetMaxNode()->GetUserPropString( key_spawn, str );

	GMatrix matrix = pNode->GetWorldTM();

	//float heading = atan( matrix[0][1] / matrix[0][0] );
	//float bank = atan( matrix[1][2] / matrix[2][2] );
	//float alttitude = asin( -matrix[0][2] );

	sSpawn spawn;

	spawn.spawnId = atoi( str );

	//yaw pitch roll
	//spawn.rotation[0] = heading;
	//spawn.rotation[2] = bank;
	//spawn.rotation[1] = alttitude;

	for( int i = 0; i < 16; ++i )
		spawn.matrix[i] = matrix[ i / 4 ][ i % 4 ];

	//for( int i = 0; i < 3; ++i )
	//	spawn.position[i] = matrix[3][i];

	ExportData( pFile, &spawn, sizeof(sSpawn), SPAWN );

	return true;
}

bool ExportWorld::ExportSkyBox( IGameMesh* mesh, IGameNode* node )
{
	mesh->InitializeData();
	Tab<int> matIds = mesh->GetActiveMatIDs();

	//there should be 5 matid's....
	for( int i = 0; i < matIds.Count(); ++i )
	{
		Tab<FaceEx*> exFaces = mesh->GetFacesFromMatID( matIds[i] );
			
		if( exFaces.Count() )
		{
		//	mesh->GetMaterialFromFace( exFaces[0] )
		}
	}

	return true;
}

bool ExportWorld::QueODEShapes( IGameNode* pNode )
{
	std::list<IGameNode*>::iterator shapeIt;
	for( shapeIt = collisionShapes.begin(); shapeIt != collisionShapes.end(); ++shapeIt )
	{
		GMatrix matrix = (*shapeIt)->GetWorldTM();
		fprintf( pFile, "Position: %f %f %f\n", matrix[3][0], matrix[3][1], matrix[3][2] );
	}

	return true;
}
/*
bool Export::ExportCustomShape( IGameNode* pNode )
{
	INode* maxNode = pNode->GetMaxNode();
	ObjectState os = maxNode->EvalWorldState( ip->GetTime() );

	if( os.obj->SuperClassID() == GEOMOBJECT_CLASS_ID ) 
	{
		Object* obj = os.obj;

		GMatrix matrix = pNode->GetWorldTM();

		float heading = atan( matrix[0][1] / matrix[0][0] );
		float bank = atan( matrix[1][2] / matrix[2][2] );
		float alttitude = asin( -matrix[0][2] );

		fprintf( pFile, "Heading: %f Bank: %f Attitude: %f\n", heading, bank, alttitude );
		fprintf( pFile, "Position: %f %f %f\n", matrix[3][0], matrix[3][1], matrix[3][2] );

		if( obj && obj->ClassID() == Class_ID(BOXOBJ_CLASS_ID, 0) )
		{
			IParamArray* array = obj->GetParamBlock();

			odeBox box;
			
			array->GetValue( obj->GetParamBlockIndex(BOXOBJ_LENGTH), 0, box.length, FOREVER);
			array->GetValue( obj->GetParamBlockIndex(BOXOBJ_HEIGHT), 0, box.height, FOREVER);
			array->GetValue( obj->GetParamBlockIndex(BOXOBJ_WIDTH), 0, box.width, FOREVER);

			box.rotation[0] = heading;
			box.rotation[2] = bank;
			box.rotation[1] = alttitude;

			for( int i = 0; i < 3; ++i )
				box.position[i] = matrix[3][i];

			ExportData(pFile, &box, sizeof(odeBox), CUSTOM_CHUNK );
		}
/*		else if( obj && obj->ClassID() == Class_ID(SPHERE_CLASS_ID, 0) )
		{
			IParamArray* array = obj->GetParamBlock();
			float radius  = 0.0f;

			array->GetValue( obj->GetParamBlockIndex(SPHERE_RADIUS), 0, radius, FOREVER);

			fprintf( pFile, "RADISU: %f\n", radius);
		}
		else if( obj && obj->ClassID() == Class_ID(CYLINDER_CLASS_ID, 0) )
		{
			IParamArray* array = obj->GetParamBlock();
			float radius  = 0.0f;
			float height  = 0.0f;

			array->GetValue( obj->GetParamBlockIndex(CYLINDER_RADIUS), 0, radius, FOREVER);
			array->GetValue( obj->GetParamBlockIndex(CYLINDER_HEIGHT), 0, height, FOREVER);

			fprintf( pFile, "RADISU: %f, HEIGHT: %f\n", radius, height);			
		}* /
	}

	return true;
}*/

bool ExportWorld::ExportODEShape( IGameNode* pNode )
{/*
	INode* maxNode = pNode->GetMaxNode();
	ObjectState os = maxNode->EvalWorldState( ip->GetTime() );

	if( os.obj->SuperClassID() == GEOMOBJECT_CLASS_ID ) 
	{
		Object* obj = os.obj;

		GMatrix matrix = pNode->GetWorldTM();

		float heading = atan( matrix[0][1] / matrix[0][0] );
		float bank = atan( matrix[1][2] / matrix[2][2] );
		float attitude = asin( -matrix[0][2] );

		fprintf( pFile, "Heading: %f Bank: %f Attitude: %f\n", heading, bank, attitude );
		fprintf( pFile, "Position: %f %f %f\n", matrix[3][0], matrix[3][1], matrix[3][2] );

		if( obj && obj->ClassID() == Class_ID(BOXOBJ_CLASS_ID, 0) )
		{
			IParamArray* array = obj->GetParamBlock();
			float length = 0.0f;
			float height = 0.0f;
			float width = 0.0f;
			
			array->GetValue( obj->GetParamBlockIndex(BOXOBJ_LENGTH), 0, length, FOREVER);
			array->GetValue( obj->GetParamBlockIndex(BOXOBJ_HEIGHT), 0, height, FOREVER);
			array->GetValue( obj->GetParamBlockIndex(BOXOBJ_WIDTH), 0, width, FOREVER);
				
			fprintf( pFile, "LENGTH: %f, HEIGHT: %f WIDTH: %f\n", length, width, height);
		}
		else if( obj && obj->ClassID() == Class_ID(SPHERE_CLASS_ID, 0) )
		{
			IParamArray* array = obj->GetParamBlock();
			float radius  = 0.0f;

			array->GetValue( obj->GetParamBlockIndex(SPHERE_RADIUS), 0, radius, FOREVER);

			fprintf( pFile, "RADISU: %f\n", radius);
		}
		else if( obj && obj->ClassID() == Class_ID(CYLINDER_CLASS_ID, 0) )
		{
			IParamArray* array = obj->GetParamBlock();
			float radius  = 0.0f;
			float height  = 0.0f;

			array->GetValue( obj->GetParamBlockIndex(CYLINDER_RADIUS), 0, radius, FOREVER);
			array->GetValue( obj->GetParamBlockIndex(CYLINDER_HEIGHT), 0, height, FOREVER);

			fprintf( pFile, "RADISU: %f, HEIGHT: %f\n", radius, height);			
		}
		else
		{*/
			return ExportCollision( (IGameMesh*)pNode->GetIGameObject(), pNode );
/*		}
	}*/

	return false;
}

bool ExportWorld::ExportCollision( IGameMesh* mesh, IGameNode* node )
{
	mesh->InitializeData();

	//should we test if this vertex already exists?
	for( int i = 0; i < mesh->GetNumberOfFaces(); ++i )
	{
		FaceEx* face = mesh->GetFace(i);

		for( int p = 0; p < 3; ++p )
		{
			Point3 pnt = mesh->GetVertex( face->vert[p] );
			collisionMesh.indices.push_back( collisionMesh.positions.size() );
			collisionMesh.positions.push_back( pnt );

			if( -abs(pnt.x) < collisionSectorInfo.startValue )
			{
				collisionSectorInfo.startValue = -abs(pnt.x) - 20;
			}
			if( -abs(pnt.z) < collisionSectorInfo.startValue )
			{
				collisionSectorInfo.startValue = -abs(pnt.z) - 20;
			}
		}
	}

	return true;
}

bool ExportWorld::ExportVolume( IGameNode* pNode )
{
	INode* mNode = pNode->GetMaxNode();

	TSTR key = "volume";
	TSTR str;
	mNode->GetUserPropString( key, str );

	return true;
}

bool ExportWorld::Done()
{
	//fprintf(pFile, "PRE DONE\n");
	//fflush(pFile);

	ip->ProgressUpdate(0);

	//export mesh stuff
	std::list<snMesh*>::iterator meshIt;
	for( meshIt = meshes.begin(); meshIt != meshes.end(); ++meshIt )
	{
		/*
		int opacity = 0;
		TCHAR* tex;
		if( (*meshIt)->material->GetOpacityData()->GetPropertyValue(opacity) )
			fprintf( pFile, "WE HAVE OPACITY %f!!!\n", opacity );

		(*meshIt)->material->GetOpacityData()->GetPropertyValue(tex);
			fprintf( pFile, "TEXUTRE: %s\n", tex );
		*/
		//ExportData( pFile, 0, 0, MESH_HEAD );

		//ExportMaterial( (*meshIt)->material );

		//fprintf(pFile, "PRE STARTING A NEW MESH!!!\n");
		//fflush(pFile);

		DivideMesh( (*meshIt) );

		//fprintf(pFile, "POST END MESH!!!\n");
		//fflush(pFile);
		//ExportRawMesh( (*meshIt) );

		//delete (*meshIt);
		//(*meshIt) = 0;
	}

	//fprintf(pFile, "MESH DIVIDED\n");
	//fflush(pFile);


	//fprintf(pFile, "THE PROBLEM IS FUCKEN SOME HWERE ELSE!!!\n");
	//fflush(pFile);

	ip->ProgressUpdate(50);

	//now the map is divided (done by DivideMesh)
	//iterate through each rendercell,
	//and each mesh/material and export
	std::list<snMesh*>::iterator mIt; // meshes;
	std::vector< RenderSector >::iterator renderIt;// renderSectors;

	//tell the importer, what comes now is renderable, subdivisions
	ExportData( pFile, &renderSectorInfo, sizeof(sectorInfo), SECTOR_RENDER_INFO );

	for( renderIt = renderSectors.begin(); renderIt != renderSectors.end(); ++renderIt )
	{
		//fprintf(pFile, "NEW RENDER CELL\n");
		//fflush(pFile);
		//a new sector on the world
		ExportData( pFile, 0, 0, NEW_SECTOR_HEADER );
		for( mIt = renderIt->meshes.begin(); mIt != renderIt->meshes.end(); ++mIt )
		{
			//only export if some data is exported
			if( (*mIt)->indices.size() && (*mIt)->positions.size() )
			{
				ExportData( pFile, 0, 0, MESH_HEAD );				
				ExportRawMesh( (*mIt) );
				ExportMaterial( (*mIt)->material );
			}
		}
		//fprintf(pFile, "END RENDER CELL\n");
	}

	//fprintf(pFile, "THE PROBLEM IS FUCKEN DOWN!!!\n");
	//fflush(pFile);

	//export collision
	DivideCollisionMesh( &collisionMesh );

	//fprintf(pFile ,"start value: %f\n", collisionSectorInfo.startValue );
	ExportData( pFile, &collisionSectorInfo, sizeof(sectorInfo), SECTOR_COLLISION_INFO );

	std::vector< CollisionSector >::iterator collisionIt;
	for( collisionIt = collisionSectors.begin(); collisionIt != collisionSectors.end(); ++collisionIt )
	{
		ExportData( pFile, 0, 0, NEW_SECTOR_HEADER );

		//only export if some data is exported
		if( collisionIt->collisionMesh.indices.size() && collisionIt->collisionMesh.positions.size() )
		{
			ExportData( pFile, 0, 0, COLLISION_HEAD );
			ExportRawMesh( &(collisionIt->collisionMesh) );
		}
	}
	/*
	ExportData( pFile, 0, 0, COLLISION_HEAD );
	ExportRawMesh( &collisionMesh );
	*/

	ip->ProgressUpdate(100);

	return true;
}

bool ExportWorld::DivideCollisionMesh( snMesh* mesh )
{
	/*
	if( collisionSectorInfo.cols <= 1 )
	{
		collisionSectors[ 0 ].collisionMesh = mesh;
		return true;
	}*/

	float mapSize = abs( collisionSectorInfo.startValue ) * 2;
	collisionSectorInfo.cellGap = mapSize / collisionSectorInfo.cols;
/*
	fprintf(pFile, "cellgap: %f\n", collisionSectorInfo.cellGap );
	fprintf(pFile, "start val: %f\n", collisionSectorInfo.startValue );
	fprintf(pFile, "cols: %i\n", collisionSectorInfo.cols );
	fprintf(pFile, "size: %i\n", collisionSectors.size() );
*/
//	fprintf(pFile, "STARTING!!!\n");
//	fflush(pFile);
/*
	//loop through each face
	for( int f = 0; f < mesh->indices.size(); f += 3 )
	{
		//which cell is this vertex in?
		Point3 p[3];
		int faces[3];
		std::set<int> cells;

//		fprintf(pFile, "PRE HERE!!!\n");
//		fflush(pFile);
		for( int i = 0; i < 3; ++i )		
		{
			faces[i] = mesh->indices[f + i];
			p[i] = mesh->positions[ faces[i] ];

			int x = (p[i].x - collisionSectorInfo.startValue)/collisionSectorInfo.cellGap;
			int z = (p[i].z - collisionSectorInfo.startValue)/collisionSectorInfo.cellGap;

			cells.insert( ((x * collisionSectorInfo.cols)+z) );
		}

//		fprintf(pFile, "HERE!!!\n");
//		fflush(pFile);
		std::set<int>::iterator cIt;
		for( cIt = cells.begin(); cIt != cells.end(); cIt++ )
		{/*
			if( (*cIt) >= collisionSectors.size() )
			{
				fprintf(pFile, "out of bounds\n");
				fflush(pFile);
				//MessageBox(0, "ERROR","Outside of collision bounds", MB_OK);
				continue;
				//return false;
			}* /
/*
			fprintf(pFile, "cell: %i\n", (*cIt) );
			fflush( pFile );
			fprintf( pFile, "size: %i\n", collisionSectors[ (*cIt) ].collisionMesh.positions.size() );
* /
			for( int t = 0; t < 3; t++ )
			{
				//loop through each position
				//to reduce duplicate data
				bool bFound = false;

				for( int idx = 0; idx < collisionSectors[ (*cIt) ].collisionMesh.positions.size(); ++idx )
				{
					if( collisionSectors[ (*cIt) ].collisionMesh.positions[ idx ] == p[t] )
					{
						bFound = true;
						collisionSectors[ (*cIt) ].collisionMesh.indices.push_back( idx );
						break;
					}
				}
				if( !bFound )
				{
					//CollisionSector c;
					//collisionSectors[ (*cIt) ] = c;

					collisionSectors[ (*cIt) ].collisionMesh.indices.push_back( 
						collisionSectors[ (*cIt) ].collisionMesh.positions.size() );

					collisionSectors[ (*cIt) ].collisionMesh.positions.push_back( p[t] );
				}
				/*
				//now find which mesh to insert this in via texture
				std::list<snMesh*>::iterator meshIt;
				for( meshIt = collisionSectors[ (*cIt) ].begin(); meshIt != collisionSectors[ (*cIt) ].end(); ++meshIt )
				{
					//go through each face,
					// and determine if the position already exists
					std::vector<unsigned int>::iterator idxIt;
					bool bFound = false;
					for( int idx = 0; idx < (*meshIt)->positions.size(); ++idx )
					{
						if( (*meshIt)->positions[ idx ] == p[t] )
						{
							(*meshIt)->indices.push_back( idx );
						}
					}
					if( !bFound )
					{
						(*meshIt)->indices.push_back( (*meshIt)->positions.size() );
						(*meshIt)->positions.push_back( p[t] );
					}
					break;
				}
				if( meshIt == renderSectors[ (*cIt) ].end() )
				{
					snMesh* newMesh = 0;
					newMesh = new snMesh;

					if( newMesh == 0 )
					{
						fprintf(pFile,"Not enougn memory!!!!!\n");
						fclose(pFile);
						exit(0);
					}

					collisionSectors[ (*cIt) ].push_front( newMesh );
					
					unsigned int size = collisionSectors[ (*cIt) ].front()->positions.size();
					snMesh *m = collisionSectors[ (*cIt) ].front();

					collisionSectors[ (*cIt) ].front()->indices.push_back( size );
					collisionSectors[ (*cIt) ].front()->positions.push_back( p[t] );
				}* /
			}
		}
//		fprintf(pFile, "END HERE!!!\n");
//		fflush(pFile);
	}

//	fprintf(pFile, "HELLO!!!!\n");
//	fflush( pFile );
	*/
	return true;
}

bool ExportWorld::DivideMesh( snMesh* mesh )
{
	/*
	if( mesh->colors.size() && mesh->colors.size() != mesh->positions.size() )
		MessageBox(0, "Error", "colors != verts", MB_OK );

	if( mesh->uvCoords2.size() > 0 && mesh->uvCoords2.size() != mesh->uvCoords.size() )
	{
		MessageBox(0, "Error", "not the same!!!!", MB_OK );
	}
*/
/*	float mapSize = abs( renderSectorInfo.startValue ) * 2;
	renderSectorInfo.cellGap = mapSize / renderSectorInfo.cols;

	if( mesh->indices.size() % 3 != 0 )
	{
		MessageBox(0, "Error", "Incorrect number of indices", MB_OK );
	}

	//loop through each face
	for( int f = 0; f < mesh->indices.size(); f += 3 )
	{
		//which cell is this vertex in?
		Point3 p[3];
		int faces[3];
		std::set<int> cells;

		for( int i = 0; i < 3; ++i )		
		{
			faces[i] = mesh->indices[f + i];
			p[i] = mesh->positions[ faces[i] ];

			int x = (p[i].x - renderSectorInfo.startValue)/renderSectorInfo.cellGap;
			int z = (p[i].z - renderSectorInfo.startValue)/renderSectorInfo.cellGap;

			cells.insert( ((x * renderSectorInfo.cols)+z) );
		}

		std::set<int>::iterator cIt;
		for( cIt = cells.begin(); cIt != cells.end(); cIt++ )
		{
			for( int t = 0; t < 3; t++ )
			{
				//now find which mesh to insert this in via texture
				std::list<snMesh*>::iterator meshIt;
				for( meshIt = renderSectors[ (*cIt) ].meshes.begin(); meshIt != renderSectors[ (*cIt) ].meshes.end(); ++meshIt )
				{
					if( (*meshIt)->material == mesh->material )
					{
						//fprintf(pFile, "mesh found with approperiate material\n");
						//go through each face,
						// and determine if the position already exists
						std::vector<unsigned int>::iterator idxIt;
						bool bFound = false;
						for( int idx = 0; idx < (*meshIt)->positions.size(); ++idx )
						{
							if( (*meshIt)->positions[ idx ] == p[t] )
							{
								bFound = true;

								if( bFound && mesh->uvCoords.size() > 0 )
								{
									bFound = false;

									if( (*meshIt)->uvCoords[ idx ] == mesh->uvCoords[ faces[t] ] )
										bFound = true;
								}

								if( bFound && mesh->uvCoords2.size() > 0 )
								{
									bFound = false;
									
									if( (*meshIt)->uvCoords2[ idx ] == mesh->uvCoords2[ faces[t] ] )
										bFound = true;
								}

								if( bFound && mesh->colors.size() > 0 )
								{
									bFound = false;

									if( (*meshIt)->colors[ idx ] == mesh->colors[ faces[t] ] )
										bFound = true;
								}

								if( bFound )
								{
									(*meshIt)->indices.push_back( idx );
									break;
								}
							}
						}
						if( !bFound )
						{
							(*meshIt)->indices.push_back( (*meshIt)->positions.size() );

							if( mesh->uvCoords.size() > 0 )
								(*meshIt)->uvCoords.push_back( mesh->uvCoords[ faces[t] ] );

							if( mesh->uvCoords2.size() > 0 )
								(*meshIt)->uvCoords2.push_back( mesh->uvCoords2[ faces[t] ] );

							if( mesh->colors.size() > 0 )
								(*meshIt)->colors.push_back( mesh->colors[ faces[t] ] );

							(*meshIt)->positions.push_back( p[t] );
						}

						break;
					}
				}				
				if( meshIt == renderSectors[ (*cIt) ].meshes.end() )
				{					
					renderSectors[ (*cIt) ].meshes.push_front( new snMesh );
					renderSectors[ (*cIt) ].meshes.front()->material = mesh->material;

					unsigned int size = renderSectors[ (*cIt) ].meshes.front()->positions.size();
					renderSectors[ (*cIt) ].meshes.front()->indices.push_back( size );

					if( mesh->uvCoords.size() )
						renderSectors[ (*cIt) ].meshes.front()->uvCoords.push_back( mesh->uvCoords[ faces[t] ] );

					if( mesh->uvCoords2.size() )
						renderSectors[ (*cIt) ].meshes.front()->uvCoords2.push_back( mesh->uvCoords2[ faces[t] ] );

					if( mesh->colors.size() > 0 )
						renderSectors[ (*cIt) ].meshes.front()->colors.push_back( mesh->colors[ faces[t] ] );

					renderSectors[ (*cIt) ].meshes.front()->positions.push_back( p[t] );				
				}
			}
		}
	}
*/
	return true;
}

bool ExportWorld::AppendMesh( snMesh* sMesh, Tab<FaceEx*>& exFaces, IGameMesh* mesh )
{

	Point3 firstColor;
	Point3 badColor(-1, -1, -1);
	bool bBadColor = false;
	bool bDifferentColor = false;

	if( mesh->GetNumberOfColorVerts() )
		firstColor = mesh->GetColorVertex( exFaces[0]->color[0] );

	IGameUVGen* texTrans = 0;

	int noTex;
	if( mesh->GetMaterialFromFace( exFaces[0] ) && (noTex = mesh->GetMaterialFromFace( exFaces[0] )->GetNumberOfTextureMaps()) > 0 )
	{
		texTrans = mesh->GetMaterialFromFace( exFaces[0] )->GetIGameTextureMap(noTex-1)->GetIGameUVGen();
	}

	float uTile = 1, vTile = 1;
	if( texTrans )
	{		
		texTrans->GetUTilingData()->GetPropertyValue( uTile );
		texTrans->GetVTilingData()->GetPropertyValue( vTile );
	}

	for( int f = 0; f < exFaces.Count(); ++f )
	{						
		for( int k = 0; k < 3; ++k )
		{
			int j = 0;

			/*
			//does this vertex already exist in the list?
			bool bFound = false;

			for( j = 0; j < sMesh->positions.size(); ++j )
			{
				//we found the vertex
				if( sMesh->positions[j] == mesh->GetVertex( exFaces[f]->vert[k] ) )
				{
					bFound = true;

					//we have normal info
					if( bFound && mesh->GetNumberOfNormals() )
					{
						bFound = false;
						if( sMesh->normals[j] == mesh->GetNormal( exFaces[f]->norm[k] ) )
						{
							bFound = true;
						}
					}

					//we have tex coords
					if( bFound && mesh->GetNumberOfTexVerts() )
					{
						bFound = false;
						Point2 uv = mesh->GetTexVertex( exFaces[f]->texCoord[k] );
						uv.x *= uTile;
						uv.y *= vTile;
						if( sMesh->uvCoords[j] == uv )
						{
							bFound = true;
						}
					}

					//do we have a second channel? - lightmap
					if( mesh->GetActiveMapChannelNum().Count() > 1 && sMesh->material->GetIGameTextureMap(0)->GetStdMapSlot() == ID_AM )
					{
						bFound = false;
						Point2 uv2 = mesh->GetMapVertex( 1, exFaces[f]->texCoord[k] );
						if( sMesh->uvCoords2[j] == uv2 )
						{
							bFound = true;
						}
					}

					//we have color info
					if( bFound && mesh->GetNumberOfColorVerts() )
					{
						bFound = false;
						if( sMesh->colors[j] == mesh->GetColorVertex( exFaces[f]->color[k] ) )
						{
							bFound = true;
						}
					}

					if( bFound )
						break;
				}
			}

			if( bFound == false )
			{*/
				//static int go = 0;
				//fprintf(pFile, "this works: %i\n", ++go );
				sMesh->indices.push_back( sMesh->positions.size() );

				if( mesh->GetNumberOfVerts() )
				{
					//fprintf(pFile, "%f %f %f\n", mesh->GetVertex( exFaces[f]->vert[k] ).x,
					//	mesh->GetVertex( exFaces[f]->vert[k] ).y,
					//	mesh->GetVertex( exFaces[f]->vert[k] ).z );
					Point3 p = mesh->GetVertex( exFaces[f]->vert[k] );
					sMesh->positions.push_back( p );

					//get our start value here
					//need the 10 leway here 
					if( (-abs(p.x) - 10) < renderSectorInfo.startValue )
					{
						renderSectorInfo.startValue = -abs(p.x) - 10;
					}
					if( (-abs(p.z) - 10) < renderSectorInfo.startValue )
					{
						renderSectorInfo.startValue = -abs(p.z) - 10;
					}	
				}

				//we have normals
				if( mesh->GetNumberOfNormals() )
					sMesh->normals.push_back( mesh->GetNormal( exFaces[f]->norm[k] ) );


				//we have tex coords
				if( mesh->GetNumberOfTexVerts() )
				{
					//Point2 uv = mesh->GetMapVertex( 0, exFaces[f]->texCoord[k] );
					Point2 uv = mesh->GetTexVertex( exFaces[f]->texCoord[k] );
					//fprintf(pFile,"UV: %f %f\n", uv.x, uv.y );
					uv.x *= uTile;
					uv.y *= vTile;
					sMesh->uvCoords.push_back( uv );
				}

				//do we have a second channel? - lightmap
				if( mesh->GetActiveMapChannelNum().Count() > 1 && sMesh->material->GetIGameTextureMap(0)->GetStdMapSlot() == ID_AM )
				{
					//fprintf(pFile, "model has second uv's\n");

					int fi = 0;
					DWORD  v[3];
					//FaceEx* fex = mesh->GetFace( fi );;
					while( exFaces[f] != mesh->GetFace( fi ) && fi < mesh->GetNumberOfFaces() )
					{	
						++fi;
						//fex = mesh->GetFace( fi );
					}					

					mesh->GetMapFaceIndex( 2, fi, v );
					//fprintf(pFile, "face: %d %d %d\n",v[0],v[1],v[2]);

					Point2 uv2 = mesh->GetMapVertex( 2, v[k] );
					//fprintf(pFile,"%i UV2: %f %f\n", fi, uv2.x, uv2.y );
					
					//Point2 uv2 = mesh->GetMapVertex( 1, exFaces[f]->texCoord[k] );
					sMesh->uvCoords2.push_back( uv2 );

					//if( mesh->GetMapVertex( 1, exFaces[f]->texCoord[k] ) == mesh->GetMapVertex( 0, exFaces[f]->texCoord[k] ) )
					//	fprintf(pFile, "poofta!!!\n");
				}

				//we have vertex colors
				if( mesh->GetNumberOfColorVerts() || !bBadColor )
				{
					Point3 color = mesh->GetColorVertex( exFaces[f]->color[k] );				
					sMesh->colors.push_back( color );

					//if( firstColor != color )
					//	bDifferentColor = true;

					//if( color.x < 0 || color.y < 0 || color.z < 0 )
					//if( color == badColor )
					//{
						//fprintf("bad color: %f %f %f\n", );
					//	bBadColor = true;
					//}
				}
			/*}
			else //we found it already :)
			{
				sMesh->indices.push_back( j );
			}*/
		}
	}

	//now are all colors the same?
	//if( !bDifferentColor || bBadColor )
	//{
	//	sMesh->colors.clear();
	//	sMesh->colors.resize(0);
	//}


	return true;
}

bool ExportWorld::ExportMesh( IGameMesh* mesh, IGameNode* node )
{
	mesh->InitializeData();

	GMatrix tm = node->GetObjectTM();

	Tab<int> matIds = mesh->GetActiveMatIDs();
/*
	std::vector<sFace> faces;
	std::vector<unsigned int> indices;
	std::vector<Point3> positions;
	std::vector<Point2> uvCoords;
	std::vector<Point3> normals;
	std::vector<Point3> colors;
*/
	IGameUVGen* texTrans = 0;

	//compile a list of faces and vertices:
	for( int i = 0; i < matIds.Count(); ++i )
	{
		Tab<FaceEx*> exFaces = mesh->GetFacesFromMatID( matIds[i] );
			
		if( exFaces.Count() )
		{
			//now we have our material, find it in our list of meshes
			std::list<snMesh*>::iterator meshIt;
			for( meshIt = meshes.begin(); meshIt != meshes.end(); ++meshIt )
			{
				if( (*meshIt)->material == mesh->GetMaterialFromFace( exFaces[0] ) )
				{
					AppendMesh( (*meshIt), exFaces, mesh );	
					break;
				}
			}
			if( meshIt == meshes.end() )
			{
				snMesh* newMesh = new snMesh;
				newMesh->material = mesh->GetMaterialFromFace( exFaces[0] );

				//fprintf(pFile, "This material has: %i textures\n", newMesh->material[0]->GetNumberOfTextureMaps() ); 
				//newMesh->material[1] = 
				meshes.push_front( newMesh );
				AppendMesh( newMesh, exFaces, mesh );
			}
		}
	}
/*

		Tab<int> mapNums = mesh->GetActiveMapChannelNum();
		int mapCount = mapNums.Count();
		fprintf(pFile, "Num map channels: %d\n",mapCount );

		for( i = 0; i < mapCount; ++i )
		{

			fprintf( pFile, "%d\n",mapNums[i]);
			int vCount = mesh->GetNumberOfMapVerts(mapNums[i]);
			fprintf(pFile, "num verts in channel: %d\n",vCount);

			for(int j = 0 ; j < vCount; ++j )
			{
				fprintf(pFile, "%d\n",j);
				Point3 v;
				if( mesh->GetMapVertex(mapNums[i],j,v) )
				{
					fprintf( pFile, "vert: %f %f %f\n",v.x,v.y,v.z);
				}		
			}

			int fCount = mesh->GetNumberOfFaces();
			fprintf(pFile, "num faces in channel: %d\n",fCount);

			for( int k = 0; k < fCount; ++k )
			{
				fprintf(pFile, "%d\n",k);
				DWORD  v[3];
				mesh->GetMapFaceIndex(mapNums[i],k,v);
				fprintf(pFile, "face: %d %d %d\n",v[0],v[1],v[2]);			
			}
		}
*/

	return true;
}

bool ExportWorld::ExportRawMesh( snMesh* mesh )
{
	int i;

	//EXPORT FACES/INDICES
	if( mesh->indices.size() )
	{
		//fprintf(pFile, "THE PROBLEM IS LOWER\n");
		fflush(pFile);
		unsigned int* eIndices = new unsigned int[ mesh->indices.size() ];

		//fprintf(pFile, "THE PROBLEM IS LOWER STILL\n");
		fflush(pFile);
		for( int ei = 0; ei < mesh->indices.size(); ei += 3 )
		{
			eIndices[ ei + 0 ] = mesh->indices[ ei + 2 ];
			eIndices[ ei + 1 ] = mesh->indices[ ei + 1 ];
			eIndices[ ei + 2 ] = mesh->indices[ ei + 0 ];
		}	
		ExportData( pFile, eIndices, sizeof(unsigned int) * mesh->indices.size(), FACES, mesh->indices.size()/3 );
		delete[] eIndices;
	}
	else
	{
		return false;
	}

	//EXPORT VERTICES
	if( mesh->positions.size() )
	{
		sVertex* eVertices = new sVertex[ mesh->positions.size() ];

		std::vector<Point3>::iterator posIt;
		for (posIt = mesh->positions.begin(), i = 0; posIt != mesh->positions.end(); posIt++, i++)
		{
			eVertices[i].vert[0] = posIt->x;
			eVertices[i].vert[1] = posIt->y;
			eVertices[i].vert[2] = posIt->z;
		}
		/*
		for( int ev = 0; ev < mesh->positions.size(); ++ev )
		{
			//positions[ ev ] = positions[ ev ] * tm;
			eVertices[ ev ].vert[0] = mesh->positions[ ev ].x;
			eVertices[ ev ].vert[1] = mesh->positions[ ev ].y;
			eVertices[ ev ].vert[2] = mesh->positions[ ev ].z;
		}*/		
		ExportData( pFile, eVertices, sizeof(sVertex) * mesh->positions.size(), VERTICES, mesh->positions.size() );
		delete[] eVertices;
	}
	else
	{
		return false;
	}
	
	//EXPORT NORMALS
	if( mesh->normals.size() )
	{
		sVertex* eNormals = new sVertex[ mesh->normals.size() ];

		std::vector<Point3>::iterator nrmIt;
		for (nrmIt = mesh->normals.begin(), i = 0; nrmIt != mesh->normals.end(); nrmIt++, i++)
		{
			eNormals[i].vert[0] = nrmIt->x;
			eNormals[i].vert[1] = nrmIt->y;
			eNormals[i].vert[2] = nrmIt->z;
		}

		/*
		for( int en = 0; en < mesh->normals.size(); ++en )
		{
			//positions[ ev ] = positions[ ev ] * tm;
			eNormals[ en ].vert[0] = mesh->normals[ en ].x;
			eNormals[ en ].vert[1] = mesh->normals[ en ].y;
			eNormals[ en ].vert[2] = mesh->normals[ en ].z;
		}*/
		ExportData( pFile, eNormals, sizeof(sVertex) * mesh->normals.size(), NORMALS, mesh->normals.size() );
		delete[] eNormals;
	}

	//EXPORT COLORS
	if( mesh->colors.size() )
	{
		sVertex* eColors = new sVertex[ mesh->colors.size() ];
		/*
		for( int ec = 0; ec < mesh->colors.size(); ++ec )
		{
			eColors[ ec ].vert[0] = mesh->colors[ ec ].x;
			eColors[ ec ].vert[1] = mesh->colors[ ec ].y;
			eColors[ ec ].vert[2] = mesh->colors[ ec ].z;
		}*/

		std::vector<Point3>::iterator clrIt;
		for (clrIt = mesh->colors.begin(), i = 0; clrIt != mesh->colors.end(); clrIt++, i++)
		{
			eColors[i].vert[0] = clrIt->x;
			eColors[i].vert[1] = clrIt->y;
			eColors[i].vert[2] = clrIt->z;
		}

		ExportData( pFile, eColors, sizeof(sVertex) * mesh->colors.size(), COLORS, mesh->colors.size() );
		delete[] eColors;
	}

	//EXPORT UVCOORDS
	if( mesh->uvCoords.size() )
	{
		sUV* eTexCoords = new sUV[ mesh->uvCoords.size() ];
		/*
		for( int et = 0; et < mesh->uvCoords.size(); ++et )
		{
			eTexCoords[ et ].uv[0] = mesh->uvCoords[ et ].x;
			eTexCoords[ et ].uv[1] = mesh->uvCoords[ et ].y;
		}*/

		
		std::vector<Point2>::iterator uvIt;
		for (uvIt = mesh->uvCoords.begin(), i = 0; uvIt != mesh->uvCoords.end(); uvIt++, i++)
		{
			eTexCoords[i].uv[0] = uvIt->x;
			eTexCoords[i].uv[1] = uvIt->y;
		}

		ExportData( pFile, eTexCoords, sizeof(sUV) * mesh->uvCoords.size(), UVCOORDS, mesh->uvCoords.size() );
		delete[] eTexCoords;
	}

	//EXPORT UVCOORDS2
	if( mesh->uvCoords2.size() )
	{
		//fprintf(pFile, "START SECOND UVS:\n");
		sUV* eTexCoords2 = new sUV[ mesh->uvCoords2.size() ];
		/*
		for( int et = 0; et < mesh->uvCoords.size(); ++et )
		{
			eTexCoords2[ et ].uv[0] = mesh->uvCoords2[ et ].x;
			eTexCoords2[ et ].uv[1] = mesh->uvCoords2[ et ].y;
		}*/

		std::vector<Point2>::iterator uvIt;
		for (uvIt = mesh->uvCoords2.begin(), i = 0; uvIt != mesh->uvCoords2.end(); uvIt++, i++)
		{
			eTexCoords2[i].uv[0] = uvIt->x;
			eTexCoords2[i].uv[1] = uvIt->y;
		}

		ExportData( pFile, eTexCoords2, sizeof(sUV) * mesh->uvCoords2.size(), UVCOORDS, mesh->uvCoords2.size() );
		delete[] eTexCoords2;
		//fprintf(pFile, ":END\n");
	}

	return true;
}

bool ExportWorld::ExportData( FILE* pFile, void* data, long size, unsigned int type, unsigned int noElements )
{
	//firstly write out the header
	Head h;
	h.size = size;
	h.type = type;
	h.noElements = noElements;
	fwrite( &h, sizeof(Head), 1, pFile );

	//body info
	return fwrite( data, size, 1, pFile );
}

char* ExportWorld::FixPath( const char* const bmpName, char ext[3] )
{
	TCHAR* matName = "null";
	int size;
	bool bDot = false;
	int d = strlen( bmpName );
	for( int i = strlen( bmpName ); i >= 0; --i )
	{
		if( bmpName[i] == '\\' )
		{
			++i;
			break;
		}

		if( bmpName[d] == '.' )
			bDot = true;

		if( bDot == false )
			--d;
	}

	size = d - i;
	//now copy into matName
	matName = new TCHAR[ size + 5 ];
	int l = 0;
	for( int k = i; k < d; ++k, ++l )
	{
		matName[l] = bmpName[k];
	}
	//crop of the .bmp or what ever...
	matName[ size + 0 ] = '.';
	for( int e = 1; e < 4; ++e )
		matName[ size + e ] = ext[e-1];
/*
	matName[ size + 0 ] = '.';
	matName[ size + 1 ] = 'S';
	matName[ size + 2 ] = 'M';
	matName[ size + 3 ] = 'T';*/
	matName[ size + 4 ] = '\0';

	return matName;
}

bool ExportWorld::ExportMaterial( IGameMaterial* material )
{
	if( material == 0 )
		return false;

	bool bFree = false;
	TCHAR* matName = "null"; // = material->GetMaterialName();
	TCHAR* matTex = "null";

	//fprintf(pFile, "Num textures: %i\n", material->GetNumberOfTextureMaps() );
	//fflush(pFile);
	int size;
	if( material->GetNumberOfTextureMaps() > 1 )
	{
		//bFree = true;
		matName = FixPath( material->GetIGameTextureMap(1)->GetBitmapFileName(), "SMT" );
		matTex = FixPath( material->GetIGameTextureMap(1)->GetBitmapFileName(), "dds" );
	}
	else if( material->GetNumberOfTextureMaps() )
	{
		matName = FixPath( material->GetIGameTextureMap(0)->GetBitmapFileName(), "SMT" );
		matTex = FixPath( material->GetIGameTextureMap(0)->GetBitmapFileName(), "dds" );

/*
		bool bDot = false;
		int d = strlen( bmpName );
		for( int i = strlen( bmpName ); i >= 0; --i )
		{
			if( bmpName[i] == '\\' )
			{
				++i;
				break;
			}

			if( bmpName[d] == '.' )
				bDot = true;

			if( bDot == false )
				--d;
		}

		size = d - i;
		//now copy into matName
		matName = new TCHAR[ size + 5 ];
		int l = 0;
		for( int k = i; k < d; ++k, ++l )
		{
			matName[l] = bmpName[k];
		}
		//crop of the .bmp or what ever...
		matName[ size + 0 ] = '.';
		matName[ size + 1 ] = 'S';
		matName[ size + 2 ] = 'M';
		matName[ size + 3 ] = 'T';
		matName[ size + 4 ] = '\0';
		//matName = bmpName;*/
	}
	else
	{
		//deal with no texture here!
		//material->GetMaterialName()
		return false;
	}

	ExportData( pFile, matName, sizeof(char) * (strlen(matName) + 1), MATERIALTABLE, strlen(matName) + 1 );

	FILE* matFile = fopen( matName, "wb" );// matName

	if( matFile == 0 )
		return false;

	//matName = FixPath( matName, "dds" );
	/*
	matName[ size - 3 ] = 'd';
	matName[ size - 2 ] = 'd';
	matName[ size - 1 ] = 's';*/

	ExportData( matFile, matTex, sizeof(char) * (strlen(matTex) + 1), TEXTUREREF, strlen(matTex) + 1 );
	
	sMaterial exportMat;

	Point3 diffuse;
	material->GetDiffuseData()->GetPropertyValue( diffuse );
	exportMat.diffuse.vert[0] = diffuse.x;
	exportMat.diffuse.vert[1] = diffuse.y;
	exportMat.diffuse.vert[2] = diffuse.z;

	Point3 specular;
	material->GetSpecularData()->GetPropertyValue( specular );
	exportMat.specular.vert[0] = specular.x;
	exportMat.specular.vert[1] = specular.y;
	exportMat.specular.vert[2] = specular.z;

	material->GetGlossinessData()->GetPropertyValue( exportMat.shine );
	
	ExportData( matFile, &exportMat, sizeof(sMaterial), MATERIAL );



	
	/*
	struct sMaterial
{
	sVertex specular;
	sVertex diffuse;
	float shine;
};
/*
	if( material->GetNumberOfTextureMaps() )
	{
		matName = material->GetIGameTextureMap(0)->GetBitmapFileName();

		//make it a tga insterad of psd for example
		matName[ strlen( matName ) - 2 ] = 't';
		matName[ strlen( matName ) - 1 ] = 'g';
		matName[ strlen( matName ) - 0 ] = 'a';

		ExportData( matFile, matName, strlen(matName), TEXTUREREF, 0 );
	}
*/
	ExportData( matFile, 0, 0, ENDOFFILE );
	fclose( matFile );

	if( material->GetNumberOfTextureMaps() > 1 && material->GetIGameTextureMap(0)->GetStdMapSlot() == ID_AM )
	{		
		TCHAR* bmpLightMap = FixPath( material->GetIGameTextureMap(0)->GetBitmapFileName(), "dds" );
		ExportData( pFile, bmpLightMap, sizeof(char) * (strlen(bmpLightMap) + 1), LIGHT_MAP, sizeof(char) * (strlen(bmpLightMap) + 1) );
	}

	return true;
}

bool ExportWorld::ExportBasicShape( INode* maxNode )
{
	// parent: GEOMOBJECT_CLASS_ID
	//BOXOBJ_CLASS_ID
	//SPHERE_CLASS_ID
	//CYLINDER_CLASS_ID

	return true;
}

void ExportWorld::SetMapDivisions( int renderDivisions, int collisionDivisions )
{
	if( renderDivisions <= 0 )
		renderDivisions = 1;
	if( collisionDivisions <= 0 )
		collisionDivisions = 1;

	renderSectorInfo.cols = renderDivisions;
	collisionSectorInfo.cols = collisionDivisions;
/*
	std::vector< int > test;
	test.resize( 2 * 2 );

	fprintf(pFile, "renderdiviosn: %i\n", renderDivisions );
	fprintf(pFile, "collisionDivisions: %i\n", collisionDivisions );
*/
	renderSectors.resize( renderDivisions * renderDivisions );
	collisionSectors.resize( collisionDivisions * collisionDivisions );

	collisionSectorInfo.startValue = 0;
	renderSectorInfo.startValue = 0;
}