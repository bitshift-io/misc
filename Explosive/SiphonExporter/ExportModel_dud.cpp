#include "SiphonExporter.h"

void ExportModel::Initilize( IGameScene* scene, TCHAR *name, Interface* ip ) 
{
	this->scene = scene; 
	this->ip = ip;
	pFile = fopen( name, "wb" );

	//temporarily make the map 3x3
	//renderSectors.resize(4);
};

ExportModel::~ExportModel()
{
	ExportData( pFile, 0, 0, ENDOFFILE );
	fclose( pFile );
}

bool ExportModel::ExportNode( IGameNode* pNode )
{
	switch( pNode->GetIGameObject()->GetIGameType() )
	{
	case IGameObject::IGAME_MESH:
			ExportMesh( (IGameMesh*)pNode->GetIGameObject(), pNode );
		break;
	}

	//loop through children
	int noChildren = pNode->GetChildCount();
	for( int i = 0; i < noChildren; ++i )
	{
		ExportNode( pNode->GetNodeChild(i) );		
	}

	return true;
}

bool ExportModel::Done()
{/*
	//fprintf(pFile, "PRE DONE\n");
	//fflush(pFile);

	ip->ProgressUpdate(0);

	//export mesh stuff
	std::list<snMesh*>::iterator meshIt;
	for( meshIt = meshes.begin(); meshIt != meshes.end(); ++meshIt )
	{

		//ExportData( pFile, 0, 0, MESH_HEAD );

		//ExportMaterial( (*meshIt)->material );

		//fprintf(pFile, "PRE STARTING A NEW MESH!!!\n");
		//fflush(pFile);

		DivideMesh( (*meshIt) );

		//fprintf(pFile, "POST END MESH!!!\n");
		//fflush(pFile);
		//ExportRawMesh( (*meshIt) );

		//delete (*meshIt);
		//(*meshIt) = 0;
	}

	//fprintf(pFile, "MESH DIVIDED\n");
	//fflush(pFile);


	//fprintf(pFile, "THE PROBLEM IS FUCKEN SOME HWERE ELSE!!!\n");
	//fflush(pFile);

	ip->ProgressUpdate(50);

	//now the map is divided (done by DivideMesh)
	//iterate through each rendercell,
	//and each mesh/material and export
	std::list<snMesh*>::iterator mIt; // meshes;
	std::vector< RenderSector >::iterator renderIt;// renderSectors;

	//tell the importer, what comes now is renderable, subdivisions
	ExportData( pFile, &renderSectorInfo, sizeof(sectorInfo), SECTOR_RENDER_INFO );

	for( renderIt = renderSectors.begin(); renderIt != renderSectors.end(); ++renderIt )
	{
		//fprintf(pFile, "NEW RENDER CELL\n");
		//fflush(pFile);
		//a new sector on the world
		ExportData( pFile, 0, 0, NEW_SECTOR_HEADER );
		for( mIt = renderIt->meshes.begin(); mIt != renderIt->meshes.end(); ++mIt )
		{
			//only export if some data is exported
			if( (*mIt)->indices.size() && (*mIt)->positions.size() )
			{
				ExportData( pFile, 0, 0, MESH_HEAD );				
				ExportRawMesh( (*mIt) );
				ExportMaterial( (*mIt)->material );
			}
		}
		//fprintf(pFile, "END RENDER CELL\n");
	}

	//fprintf(pFile, "THE PROBLEM IS FUCKEN DOWN!!!\n");
	//fflush(pFile);

	//export collision
	DivideCollisionMesh( &collisionMesh );

	//fprintf(pFile ,"start value: %f\n", collisionSectorInfo.startValue );
	ExportData( pFile, &collisionSectorInfo, sizeof(sectorInfo), SECTOR_COLLISION_INFO );

	std::vector< CollisionSector >::iterator collisionIt;
	for( collisionIt = collisionSectors.begin(); collisionIt != collisionSectors.end(); ++collisionIt )
	{
		ExportData( pFile, 0, 0, NEW_SECTOR_HEADER );

		//only export if some data is exported
		if( collisionIt->collisionMesh.indices.size() && collisionIt->collisionMesh.positions.size() )
		{
			ExportData( pFile, 0, 0, COLLISION_HEAD );
			ExportRawMesh( &(collisionIt->collisionMesh) );
		}
	}
	/*
	ExportData( pFile, 0, 0, COLLISION_HEAD );
	ExportRawMesh( &collisionMesh );
	*/

	ExportData( pFile, 0, 0, MESH_HEAD );				
	ExportRawMesh( &(mesh) );
	ExportData( pFile, 0, 0, MESH_END );

	ip->ProgressUpdate(100);

	return true;
}

int ExportModel::ComparePoint3(Point3& left, Point3& right)
{
	return memcmp(&left, &right, sizeof(Point3));

	/*
	if (left.x > right.x)
		return 1;

	if (left.y > right.y)
		return 1;

	if (left.z > right.z)
		return 1;

	if (left == right)
		return 0;

	return -1;*/
}

bool ExportModel::AppendMesh( snMesh* sMesh, Tab<FaceEx*>& exFaces, IGameMesh* mesh, IGameMaterial* material )
{
	Point3 firstColor;
	Point3 badColor(-1, -1, -1);
	bool bBadColor = false;
	bool bDifferentColor = false;

	if( mesh->GetNumberOfColorVerts() )
		firstColor = mesh->GetColorVertex( exFaces[0]->color[0] );

	Tab<int> mapChannels = mesh->GetActiveMapChannelNum();

	IGameUVGen* texTrans = 0;

	//does this material already in our list?
	snMaterialTable* curTable = 0;

//	if( material != 0 )
//	{
		std::vector<snMaterialTable>::iterator matIt;
		for( matIt = this->mesh.materialTable.begin(); matIt != this->mesh.materialTable.end(); ++matIt )
		{
			if( matIt->material == material )
			{
				//fprintf(pFile, "material found, modfiying");
				curTable = &(*matIt);
				//curTable->matTable.numIndices += exFaces.Count() * 3;
				curTable->matTable.numPrimitives += exFaces.Count();
				break;
			}
		}
		if( matIt == this->mesh.materialTable.end() )
		{
			//fprintf(pFile, "material not, creating");
			snMaterialTable newTable;
			newTable.material = material;
			newTable.matTable.startIndex = sMesh->indices.size();
			newTable.matTable.numIndices = 0; //exFaces.Count() * 3;
			newTable.matTable.numPrimitives = exFaces.Count();
			ExportMaterial( material, newTable.matTable ); 
			this->mesh.materialTable.push_back( newTable );
			curTable = &(this->mesh.materialTable[ this->mesh.materialTable.size() - 1 ]);
		}
//	}


	int noTex;
	if( mesh->GetMaterialFromFace( exFaces[0] ) && (noTex = mesh->GetMaterialFromFace( exFaces[0] )->GetNumberOfTextureMaps()) > 0 )
	{
		texTrans = mesh->GetMaterialFromFace( exFaces[0] )->GetIGameTextureMap(noTex-1)->GetIGameUVGen();
	}

	float uTile = 1, vTile = 1;
	if( texTrans )
	{		
		texTrans->GetUTilingData()->GetPropertyValue( uTile );
		texTrans->GetVTilingData()->GetPropertyValue( vTile );
	}
	/*
	// the new way
	for( int f = 0; f < exFaces.Count(); ++f )
	{	
		//insert faces
		DWORD faceIdx[3];
		mesh->GetMapFaceIndex(1, f, faceIdx);

		for( int k = 0; k < 3; ++k )
		{
			Point3 position;
			int comparison = 1;
			bool bFound = false;

			std::list<Point3>::iterator posIt = this->mesh.positions.begin();
			int iSize = sMesh->positions.size();
			int size = sMesh->positions.size() / 2;

			if (sMesh->positions.size() && size < 1)
				size = 1;

			//if (sMesh->positions.size() == 0)
			//	sMesh->positions.push_front(mesh->GetVertex(exFaces[f]->vert[k]));

			while (size >= 1)
			{
				for (int l = 0; l < size; l++)
				{
					if (comparison > 0)
						posIt++;
					else if (comparison < 0)
						posIt--;
				}
				size /= 2;

				// compare vertex position
				position = mesh->GetVertex(exFaces[f]->vert[k]);
				comparison = ComparePoint3((*posIt), position);
				if (comparison == 0)
					bFound = true;
/*
				//we have normal info
				if( bFound && mesh->GetNumberOfNormals() )
				{
					bFound = false;
					if( sMesh->normals[j] == mesh->GetNormal( exFaces[f]->norm[k] ) )
					{
						bFound = true;
					}
				}

				//we have tex coords
				if( bFound && mesh->GetNumberOfTexVerts() )
				{
					bFound = false;
					Point2 uv = mesh->GetTexVertex( exFaces[f]->texCoord[k] );
					uv.x *= uTile;
					uv.y *= vTile;
					if( sMesh->uvCoords[j] == uv )
					{
						bFound = true;
					}
				}

				//we have color info
				if( bFound && mesh->GetNumberOfColorVerts() )
				{
					bFound = false;
					if( sMesh->colors[j] == mesh->GetColorVertex( exFaces[f]->color[k] ) )
					{
						bFound = true;
					}
				}* /
			}

			this->mesh.positions.insert(posIt, position);

		}
	}*/

	for( int f = 0; f < exFaces.Count(); ++f )
	{	
		//to get correct uvw's!!!
		DWORD faceIdx[3];
		mesh->GetMapFaceIndex(1, f, faceIdx);

		for( int k = 0; k < 3; ++k )
		{
			int j = 0;

			//does this vertex already exist in the list?
			bool bFound = false;
			for( j = 0; j < sMesh->positions.size(); ++j )
			{
				//we found the vertex

				if( sMesh->positions[j] == mesh->GetVertex( exFaces[f]->vert[k] ) )
				{
					bFound = true;

					//we have normal info
					if( bFound && mesh->GetNumberOfNormals() )
					{
						bFound = false;
						if( sMesh->normals[j] == mesh->GetNormal( exFaces[f]->norm[k] ) )
						{
							bFound = true;
						}
					}

					//we have tex coords
					if( bFound && mesh->GetNumberOfTexVerts() )
					{
						bFound = false;
						Point2 uv = mesh->GetTexVertex( exFaces[f]->texCoord[k] );
						uv.x *= uTile;
						uv.y *= vTile;
						if( sMesh->uvCoords[j] == uv )
						{
							bFound = true;
						}
					}
/*
					//do we have a second channel? - lightmap
					if( mesh->GetActiveMapChannelNum().Count() > 1 && sMesh->material->GetIGameTextureMap(0)->GetStdMapSlot() == ID_AM )
					{
						bFound = false;
						Point2 uv2 = mesh->GetMapVertex( 1, exFaces[f]->texCoord[k] );
						if( sMesh->uvCoords2[j] == uv2 )
						{
							bFound = true;
						}
					}*/

					//we have color info
					if( bFound && mesh->GetNumberOfColorVerts() )
					{
						bFound = false;
						if( sMesh->colors[j] == mesh->GetColorVertex( exFaces[f]->color[k] ) )
						{
							bFound = true;
						}
					}

					if( bFound )
					{
						bFound = true;

						sMesh->indices.insert( 
						(sMesh->indices.begin() + curTable->matTable.startIndex),// + curTable->matTable.numIndices), 
							j );

						++curTable->matTable.numIndices;

						//move those tables in front of us, up by 1 (startValue)
						std::vector<snMaterialTable>::iterator matIt;
						for( matIt = this->mesh.materialTable.begin(); matIt != this->mesh.materialTable.end(); ++matIt )
						{
							if( matIt->matTable.startIndex > curTable->matTable.startIndex )
							{
								++matIt->matTable.startIndex;
							}
						}

						break;
					}
				}
			}

			if( bFound == false )
			{
				//static int go = 0;
				//fprintf(pFile, "this works: %i\n", ++go );
				//sMesh->indices.push_back( sMesh->positions.size() );
			
				sMesh->indices.insert( 
					(sMesh->indices.begin() + curTable->matTable.startIndex),// + curTable->matTable.numIndices), 
						sMesh->positions.size() );


				++curTable->matTable.numIndices;

				//move those tables in front of us, up by 1 (startValue)
				std::vector<snMaterialTable>::iterator matIt;
				for( matIt = this->mesh.materialTable.begin(); matIt != this->mesh.materialTable.end(); ++matIt )
				{
					if( matIt->matTable.startIndex > curTable->matTable.startIndex )
					{
						++matIt->matTable.startIndex;
					}
				}

				if( mesh->GetNumberOfVerts() )
				{
					//fprintf(pFile, "%f %f %f\n", mesh->GetVertex( exFaces[f]->vert[k] ).x,
					//	mesh->GetVertex( exFaces[f]->vert[k] ).y,
					//	mesh->GetVertex( exFaces[f]->vert[k] ).z );
					Point3 p = mesh->GetVertex( exFaces[f]->vert[k] );
					sMesh->positions.push_back( p );
				}

				//we have normals
				if( mesh->GetNumberOfNormals() )
					sMesh->normals.push_back( mesh->GetNormal( exFaces[f]->norm[k] ) );

				//we have tex coords
				if( mesh->GetNumberOfTexVerts() )
				{
					//Point2 uv = mesh->GetMapVertex( 0, exFaces[f]->texCoord[k] );
					Point2 uv = mesh->GetTexVertex( exFaces[f]->texCoord[k] );
					//fprintf(pFile,"UV: %f %f\n", uv.x, uv.y );
					uv.x *= uTile;
					uv.y *= vTile;
					sMesh->uvCoords.push_back( uv );
				}
/*
				//do we have a second channel? - lightmap
				if( mesh->GetActiveMapChannelNum().Count() > 1 && sMesh->material->GetIGameTextureMap(0)->GetStdMapSlot() == ID_AM )
				{
					//fprintf(pFile, "model has second uv's\n");

					int fi = 0;
					DWORD  v[3];
					//FaceEx* fex = mesh->GetFace( fi );;
					while( exFaces[f] != mesh->GetFace( fi ) && fi < mesh->GetNumberOfFaces() )
					{	
						++fi;
						//fex = mesh->GetFace( fi );
					}					

					mesh->GetMapFaceIndex( 2, fi, v );
					//fprintf(pFile, "face: %d %d %d\n",v[0],v[1],v[2]);

					Point2 uv2 = mesh->GetMapVertex( 2, v[k] );
					//fprintf(pFile,"%i UV2: %f %f\n", fi, uv2.x, uv2.y );
					
					//Point2 uv2 = mesh->GetMapVertex( 1, exFaces[f]->texCoord[k] );
					sMesh->uvCoords2.push_back( uv2 );

					//if( mesh->GetMapVertex( 1, exFaces[f]->texCoord[k] ) == mesh->GetMapVertex( 0, exFaces[f]->texCoord[k] ) )
					//	fprintf(pFile, "poofta!!!\n");
				}*/

				//we have vertex colors
				if( mesh->GetNumberOfColorVerts() )// || !bBadColor )
				{
					Point3 color = mesh->GetColorVertex( exFaces[f]->color[k] );				
					sMesh->colors.push_back( color );

					//if( firstColor != color )
					//	bDifferentColor = true;

					//if( color.x < 0 || color.y < 0 || color.z < 0 )
					//if( color == badColor )
					//{
						//fprintf("bad color: %f %f %f\n", );
					//	bBadColor = true;
					//}
				}

			}
			/*else //we found it already :)
			{
				sMesh->indices.push_back( j );
			}*/
		}
	}

	//now are all colors the same?
	//if( !bDifferentColor || bBadColor )
	//{
	//	sMesh->colors.clear();
	//	sMesh->colors.resize(0);
	//}


	return true;
}

bool ExportModel::ExportMesh( IGameMesh* mesh, IGameNode* node )
{
	mesh->InitializeData();

	GMatrix tm = node->GetObjectTM();

	Tab<int> matIds = mesh->GetActiveMatIDs();

	IGameUVGen* texTrans = 0;

	//compile a list of faces and vertices:
	for( int i = 0; i < matIds.Count(); ++i )
	{
		Tab<FaceEx*> exFaces = mesh->GetFacesFromMatID( matIds[i] );
			
		if( exFaces.Count() )
		{
			AppendMesh( &(this->mesh), exFaces, mesh, mesh->GetMaterialFromFace( exFaces[0] ) );
		}
	}

	return true;
}

bool ExportModel::ExportRawMesh( snMesh* mesh )
{
	//EXPORT FACES/INDICES
	if( mesh->indices.size() )
	{
		//fprintf(pFile, "THE PROBLEM IS LOWER\n");
		fflush(pFile);
		unsigned int* eIndices = new unsigned int[ mesh->indices.size() ];

		//fprintf(pFile, "THE PROBLEM IS LOWER STILL\n");
		fflush(pFile);
		for( int ei = 0; ei < mesh->indices.size(); ei += 3 )
		{
			eIndices[ ei + 0 ] = mesh->indices[ ei + 2 ];
			eIndices[ ei + 1 ] = mesh->indices[ ei + 1 ];
			eIndices[ ei + 2 ] = mesh->indices[ ei + 0 ];
		}	
		ExportData( pFile, eIndices, sizeof(unsigned int) * mesh->indices.size(), FACES, mesh->indices.size()/3 );
		delete[] eIndices;
	}
	else
	{
		return false;
	}

	//EXPORT VERTICES
	if( mesh->positions.size() )
	{
		sVertex* eVertices = new sVertex[ mesh->positions.size() ];
		for( int ev = 0; ev < mesh->positions.size(); ++ev )
		{
			//positions[ ev ] = positions[ ev ] * tm;
			eVertices[ ev ].vert[0] = mesh->positions[ ev ].x;
			eVertices[ ev ].vert[1] = mesh->positions[ ev ].y;
			eVertices[ ev ].vert[2] = mesh->positions[ ev ].z;
		}		
		ExportData( pFile, eVertices, sizeof(sVertex) * mesh->positions.size(), VERTICES, mesh->positions.size() );
		delete[] eVertices;
	}
	else
	{
		return false;
	}
	
	//EXPORT NORMALS
	if( mesh->normals.size() && mesh->positions.size() == mesh->normals.size() )
	{
		sVertex* eNormals = new sVertex[ mesh->normals.size() ];
		for( int en = 0; en < mesh->normals.size(); ++en )
		{
			//positions[ ev ] = positions[ ev ] * tm;
			eNormals[ en ].vert[0] = mesh->normals[ en ].x;
			eNormals[ en ].vert[1] = mesh->normals[ en ].y;
			eNormals[ en ].vert[2] = mesh->normals[ en ].z;
		}
		ExportData( pFile, eNormals, sizeof(sVertex) * mesh->normals.size(), NORMALS, mesh->normals.size() );
		delete[] eNormals;
	}

	//EXPORT COLORS
	if( mesh->colors.size() && mesh->positions.size() == mesh->colors.size() )
	{
		sVertex* eColors = new sVertex[ mesh->colors.size() ];
		for( int ec = 0; ec < mesh->colors.size(); ++ec )
		{
			eColors[ ec ].vert[0] = mesh->colors[ ec ].x;
			eColors[ ec ].vert[1] = mesh->colors[ ec ].y;
			eColors[ ec ].vert[2] = mesh->colors[ ec ].z;
		}
		ExportData( pFile, eColors, sizeof(sVertex) * mesh->colors.size(), COLORS, mesh->colors.size() );
		delete[] eColors;
	}

	//EXPORT UVCOORDS
	if( mesh->uvCoords.size() )
	{
		sUV* eTexCoords = new sUV[ mesh->uvCoords.size() ];
		for( int et = 0; et < mesh->uvCoords.size(); ++et )
		{
			eTexCoords[ et ].uv[0] = mesh->uvCoords[ et ].x;
			eTexCoords[ et ].uv[1] = mesh->uvCoords[ et ].y;
		}
		ExportData( pFile, eTexCoords, sizeof(sUV) * mesh->uvCoords.size(), UVCOORDS, mesh->uvCoords.size() );
		delete[] eTexCoords;
	}

	//EXPORT MATERIAL TABLES
	if( mesh->materialTable.size() )
	{
		for( int em = 0; em < mesh->materialTable.size(); ++em )
		{
			ExportData( pFile, &(mesh->materialTable[em].matTable), sizeof(sMaterialTable), MATERIALTABLE, 1 );	
		}
	}

	return true;
}

bool ExportModel::ExportData( FILE* pFile, void* data, long size, unsigned int type, unsigned int noElements )
{
	//firstly write out the header
	Head h;
	h.size = size;
	h.type = type;
	h.noElements = noElements;
	fwrite( &h, sizeof(Head), 1, pFile );

	//body info
	return fwrite( data, size, 1, pFile );
}

char* ExportModel::FixPath( const char* const bmpName, char ext[3] )
{
	TCHAR* matName = "null";
	int size;
	bool bDot = false;
	int d = strlen( bmpName );
	for( int i = strlen( bmpName ); i >= 0; --i )
	{
		if( bmpName[i] == '\\' )
		{
			++i;
			break;
		}

		if( bmpName[d] == '.' )
			bDot = true;

		if( bDot == false )
			--d;
	}

	size = d - i;
	//now copy into matName
	matName = new TCHAR[ size + 5 ];
	int l = 0;
	for( int k = i; k < d; ++k, ++l )
	{
		matName[l] = bmpName[k];
	}
	//crop of the .bmp or what ever...
	matName[ size + 0 ] = '.';
	for( int e = 1; e < 4; ++e )
		matName[ size + e ] = ext[e-1];
/*
	matName[ size + 0 ] = '.';
	matName[ size + 1 ] = 'S';
	matName[ size + 2 ] = 'M';
	matName[ size + 3 ] = 'T';*/
	matName[ size + 4 ] = '\0';

	return matName;
}

bool ExportModel::ExportMaterial( IGameMaterial* material, sMaterialTable& matTable )
{
	if( material == 0 )
		return false;

	bool bFree = false;
	TCHAR* matName = "null"; // = material->GetMaterialName();
	TCHAR* matTex = "null";

	//fprintf(pFile, "Num textures: %i\n", material->GetNumberOfTextureMaps() );
	//fflush(pFile);
	int size;
	if( material->GetNumberOfTextureMaps() > 1 )
	{
		//bFree = true;
		matName = FixPath( material->GetIGameTextureMap(1)->GetBitmapFileName(), "SMT" );
		matTex = FixPath( material->GetIGameTextureMap(1)->GetBitmapFileName(), "dds" );
	}
	else if( material->GetNumberOfTextureMaps() )
	{
		matName = FixPath( material->GetIGameTextureMap(0)->GetBitmapFileName(), "SMT" );
		matTex = FixPath( material->GetIGameTextureMap(0)->GetBitmapFileName(), "dds" );

/*
		bool bDot = false;
		int d = strlen( bmpName );
		for( int i = strlen( bmpName ); i >= 0; --i )
		{
			if( bmpName[i] == '\\' )
			{
				++i;
				break;
			}

			if( bmpName[d] == '.' )
				bDot = true;

			if( bDot == false )
				--d;
		}

		size = d - i;
		//now copy into matName
		matName = new TCHAR[ size + 5 ];
		int l = 0;
		for( int k = i; k < d; ++k, ++l )
		{
			matName[l] = bmpName[k];
		}
		//crop of the .bmp or what ever...
		matName[ size + 0 ] = '.';
		matName[ size + 1 ] = 'S';
		matName[ size + 2 ] = 'M';
		matName[ size + 3 ] = 'T';
		matName[ size + 4 ] = '\0';
		//matName = bmpName;*/
	}
	else
	{
		//deal with no texture here!
		//material->GetMaterialName()
		return false;
	}

	strcpy( matTable.material, matName);

	//ExportData( pFile, matName, sizeof(char) * (strlen(matName) + 1), MATERIALTABLE, strlen(matName) + 1 );

	FILE* matFile = fopen( matName, "wb" );// matName

	if( matFile == 0 )
		return false;

	//matName = FixPath( matName, "dds" );
	/*
	matName[ size - 3 ] = 'd';
	matName[ size - 2 ] = 'd';
	matName[ size - 1 ] = 's';*/

	fprintf(matFile, "<texture> %s\n", matTex );

	//ExportData( matFile, matTex, sizeof(char) * (strlen(matTex) + 1), TEXTUREREF, strlen(matTex) + 1 );
	
	sMaterial exportMat;

	Point3 diffuse;
	material->GetDiffuseData()->GetPropertyValue( diffuse );
	exportMat.diffuse.vert[0] = diffuse.x;
	exportMat.diffuse.vert[1] = diffuse.y;
	exportMat.diffuse.vert[2] = diffuse.z;

	Point3 specular;
	material->GetSpecularData()->GetPropertyValue( specular );
	exportMat.specular.vert[0] = specular.x;
	exportMat.specular.vert[1] = specular.y;
	exportMat.specular.vert[2] = specular.z;

	material->GetGlossinessData()->GetPropertyValue( exportMat.shine );
	
	fprintf( matFile, "<property> %f %f %f\n", exportMat.diffuse.vert[0], exportMat.diffuse.vert[1], exportMat.diffuse.vert[1]);
	fprintf( matFile, "<property> %f %f %f\n", exportMat.specular.vert[0], exportMat.specular.vert[1], exportMat.specular.vert[2]);
	//ExportData( matFile, &exportMat, sizeof(sMaterial), MATERIAL );

	//ExportData( matFile, 0, 0, ENDOFFILE );
	fclose( matFile );
/*
	if( material->GetNumberOfTextureMaps() > 1 && material->GetIGameTextureMap(0)->GetStdMapSlot() == ID_AM )
	{		
		TCHAR* bmpLightMap = FixPath( material->GetIGameTextureMap(0)->GetBitmapFileName(), "dds" );
		ExportData( pFile, bmpLightMap, sizeof(char) * (strlen(bmpLightMap) + 1), LIGHT_MAP, sizeof(char) * (strlen(bmpLightMap) + 1) );
	}*/

	return true;
}
