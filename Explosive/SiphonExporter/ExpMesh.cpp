
#include "SiphonExporter.h"

#define MAX_TO_METERS 1.0f / 39.37f

using namespace Siphon;

ExpMesh::ExpMesh(IGameNode* pGameNode, std::vector<int>& matIds, IGameMaterial* pGameMaterial, ExpSkeleton* skel) : ExpObject(pGameNode),
matIds(matIds),
pGameMaterial(pGameMaterial),
skeleton(skel)
{
}


IGameMaterial* ExpMesh::GetGameMaterial() const
{
	return pGameMaterial;

}

void ExpMesh::Update()
{
	// go through all faces,
	// compile this into a model
	int c;
	IGameMesh* pMesh = (IGameMesh*)pGameObject;
	Matrix4 scaleMat(Matrix4::Identity);
	scaleMat.SetScale(MAX_TO_METERS, MAX_TO_METERS, MAX_TO_METERS);

	bool skinned = pGameObject->IsObjectSkinned();
	IGameSkin* pSkin = 0;

	if (skinned)
	{
		if (!skeleton)
			skinned = false;

		pSkin = pGameObject->GetIGameSkin();
	}

	bool bUvs = pMesh->GetNumberOfTexVerts() ? true : false;
	bool bUvs2 = pMesh->GetNumberOfMapVerts(2) ? true : false;
	bool bNormals = pMesh->GetNumberOfNormals() ? true : false;
	bool bExtraNormals = pMesh->GetNumberOfBinormals() && pMesh->GetNumberOfTangents() ? true : false;
	bool bColors = pMesh->GetNumberOfColorVerts() ? true : false;
	bool bAlpha = pMesh->GetNumberOfAlphaVerts() ? true : false;

	bool bTangentError = false;

	for (int m = 0; m < matIds.size(); ++m)
	{
		Tab<FaceEx*> faces = pMesh->GetFacesFromMatID(matIds[m]);
		for (int f = 0; f < faces.Count(); ++f)
		{
			FaceEx* face = faces[f];
			for(int i = 0; i < 3; ++i)
			{
				Point2 uv;
				Point3 uv2;
				Point3 normal;
				Point3 color;
				Point3 position;
				Point3 binormal;
				Point3 tangent;

				Colour colColour;
				Vector3 vecNormal;
				UVCoord	uvUVCoord;
				UVCoord	uvUVCoord2;
				Vector3 vecPosition;
				Vector4 weights(0.0f, 0.0f, 0.0f, 0.0f);
				Vector4 bones(-1, -1, -1, -1);
				Vector3 vecBinormal;
				Vector3 vecTangent;

				if (bUvs)
				{
					uv = pMesh->GetTexVertex(face->texCoord[i]);
					uvUVCoord = UVCoord(uv.x, uv.y);
				}

				if (bUvs2)
				{
					DWORD index[3];
					pMesh->GetMapFaceIndex(2, face->meshFaceIndex, index); 
					uv2 = pMesh->GetMapVertex(2, index[i]);
					uvUVCoord2 = UVCoord(uv2.x, uv2.y);
				}

				if (bNormals)
				{
					normal = pMesh->GetNormal(face->norm[i]);
					vecNormal = Vector3(normal.x, normal.y, normal.z);
				}

				if (bColors)
				{
					color = pMesh->GetColorVertex(face->color[i]);
					colColour = Colour(color.x, color.y, color.z);

					if (bAlpha)
						colColour[Colour::A] = pMesh->GetAlphaVertex(face->alpha[i]);
				}

				if (skinned)
				{
					int numBones = pSkin->GetNumberOfBones(face->vert[i]);

					if (numBones > 4)
						MessageBox(0, "A vertex is assigned to too many bones", "Error", MB_OK);

					float mag = 0.f;
					float totalWeight = 0.0f;
					for (int b = 0; b < numBones; ++b)
					{
						bones[b] = skeleton->GetBoneIdx(pSkin->GetIGameBone(face->vert[i], b));
						weights[b] = pSkin->GetWeight(face->vert[i], b);
						totalWeight += weights[b];
						mag += weights[b] * weights[b];
					}

					// normalize bone weights
					mag = sqrtf(mag);
					for (int b = 0; b < numBones; ++b)
					{
						weights[b] /= mag;
					}

					//if (totalWeight > 1.0f)
					//	MessageBox(0, "A vertex has weight greater than 1", "Error", MB_OK);

					//if (totalWeight < 1.0f)
					//	MessageBox(0, "A vertex has weight less than 1", "Error", MB_OK);

				}

				if (bExtraNormals)
				{
					tangent = pMesh->GetTangent(face->norm[i]);
					vecTangent = Vector3(tangent.x, tangent.y, tangent.z);

					// hrmm binormals == normals wtf?!
					binormal = pMesh->GetBinormal(face->norm[i]);
					//vecBinormal = Vector3(binormal.x, binormal.y, binormal.z);
					vecBinormal = vecTangent.CrossProduct(vecNormal);

					if (vecTangent.Magnitude() <= 0.5f)
					{
						bTangentError = true;
					}
				}

				position = pMesh->GetVertex(face->vert[i]);
				vecPosition = scaleMat * Vector3(position.x, position.y, position.z);

				for (c = 0; c < buffer.positions.GetSize(); c++)
				{
					// compare position
					if (vecPosition != buffer.positions[c])
						continue;

					// compare uv's
					if (bUvs && uvUVCoord != buffer.uvCoords[c])
						continue;

					// compare uv's
					if (bUvs2 && uvUVCoord2 != buffer.uvCoords2[c])
						continue;

					// compare colours
					if (bColors && colColour != buffer.colours[c])
						continue;

					// compare normals
					if (bNormals && vecNormal != buffer.normals[c])
						continue;

					if (skinned && weights != buffer.weights[c])
						continue;

					if (skinned && bones != buffer.bones[c])
						continue;

					if (bExtraNormals && vecBinormal != buffer.binormals[c])
						continue;

					if (bExtraNormals && vecTangent != buffer.tangents[c])
						continue;

					// add a new index
					unsigned int idx = c;
					buffer.indices.Insert(idx);
					break;
				}

				// if we didnt find duplicate info... insert it
				if (c == buffer.positions.GetSize())
				{
					unsigned int index = buffer.positions.GetSize();
					buffer.indices.Insert(index);
					buffer.positions.Insert(vecPosition);

					if (bColors)
						buffer.colours.Insert(colColour);

					if (bNormals)
						buffer.normals.Insert(vecNormal);

					if (bUvs)
						buffer.uvCoords.Insert(uvUVCoord);

					if (bUvs2)
						buffer.uvCoords2.Insert(uvUVCoord2);

					if (skinned)
					{
						buffer.weights.Insert(weights);
						buffer.bones.Insert(bones);
					}

					if (bExtraNormals)
					{
						buffer.binormals.Insert(vecBinormal);
						buffer.tangents.Insert(vecTangent);
					}
				}		
			}
		}
	}

	if (bTangentError)
	{
		MessageBox(0, "Error with tangents!", "Error", MB_OK);
	}

	// apply uv tiling from material to uv coords
}