
#include "SiphonExporter.h"

ExpObject::ExpObject(IGameNode* pGameNode) : pGameNode(0), pGameObject(0)
{
	if (pGameNode)
	{
		this->pGameNode = pGameNode;
		pGameObject = pGameNode->GetIGameObject();
	}
}

ExpObject::~ExpObject()
{
	if (pGameNode)
		pGameNode->ReleaseIGameObject();
}
