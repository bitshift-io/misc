#ifndef _SIPHON_MODEL_
#define _SIPHON_MODEL_

#include <map>
#include "SiphonExporter.h"

using namespace Siphon;

class ExpModel : public Siphon::Mesh
{
public:
	ExpModel();
	~ExpModel();

	bool AddMeshes(std::map<IGameMaterial*, std::vector<ExpMesh*> >& materialCache, bool batchEnable);

	bool AddMeshes(std::vector<ExpMesh*>& meshes, IGameMaterial* pGameMaterial, bool batchEnable);
	bool AddMesh(IGameNode* pGameNode, int materialId, IGameMaterial* pGameMaterial);
	void Process();

	void CalculateVertexFormat(std::vector<ExpMesh*>& meshes);

	/*
	 * This exports the given data is size size, and also adds in a header ot type
	 * type. Sub header info is info that goes after the header and before the data,
	 * this is used for example, to tell how many vertices there are.
	 */
	int GetMaterialCount();
	//sMaterialTable* GetMaterialTable(int index);
	IGameMaterial* GetGameMaterial(int index);

	//virtual bool Save(const char* filename, bool exportMaterials);

protected:
	bool AddBatched(std::vector<ExpMesh*>& meshes, IGameMaterial* pGameMaterial);
	bool AddNonBatched(std::vector<ExpMesh*>& meshes, IGameMaterial* pGameMaterial);

	Array<IGameMaterial*>	material;

	struct FaceMesh
	{
		IGameMesh*	pMesh;
		int			materialId;
	};
	std::map<IGameMaterial*, std::vector<FaceMesh> > meshes;

	// vertex format flags
	bool colours;
	bool uvCoords;
	bool uvCoords2;
	bool normals;
	bool binormals;
	bool tangents;
	bool bones;
	bool weights;
};

#endif