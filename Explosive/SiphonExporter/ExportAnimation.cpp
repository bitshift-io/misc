
#include "SiphonExporter.h"

void ExportAnimation::Initilize( IGameScene* scene, TCHAR *name, Interface* ip ) 
{
	this->scene = scene; 
	this->ip = ip;
//	fileName = name;
}

void ExportAnimation::Shutdown()
{

}

void ExportAnimation::InitDialog(HWND panel)
{
	this->panel = panel;
}

void ExportAnimation::UpdateDialog()
{

}

bool ExportAnimation::ProcessNode( IGameNode* pNode )
{
	bool bLoopThroughChildren = true;
/*	switch( pNode->GetIGameObject()->GetIGameType() )
	{
	case IGameObject::IGAME_MESH:
	case IGameObject::IGAME_HELPER:
		ProcessMesh(pNode);
		break;
	}
*/
	ProcessMesh(pNode);

	//loop through children
	if (bLoopThroughChildren)
	{
		int noChildren = pNode->GetChildCount();
		for( int i = 0; i < noChildren; ++i )
			ProcessNode( pNode->GetNodeChild(i) );
	}

	return true;
}

bool ExportAnimation::ProcessScene()
{/*
	float progressRate = 100.0f / objects.size();
	int progress = 0;

	std::vector<ExpObject*>::iterator objIt;
	for (objIt = objects.begin(); objIt != objects.end(); objIt++)
	{
		(*objIt)->Update();

		progress += progressRate;
		ip->ProgressUpdate(progress);
	}
*/
	ip->ProgressUpdate(100);
	return true;
}

bool ExportAnimation::ProcessMesh(IGameNode* node)
{
//	objects.push_back(new ExpAnimation(node));
	return true;
}

bool ExportAnimation::SaveScene()
{/*
	std::vector<ExpObject*>::iterator objIt;
	for (objIt = objects.begin(); objIt != objects.end(); objIt++)
	{
		ExpAnimation* anim  = static_cast<ExpAnimation*>(*objIt);
		anim->Save("test.anm"); //the animation should be compiled in to some sort of heirachy
	}
*/
	return true;
}

