
#include "SiphonExporter.h"

using namespace Siphon;

#define MAX_TO_METERS 1.0f / 39.37f

// this is the class for all biped controllers except the root and the footsteps
#define BIPSLAVE_CONTROL_CLASS_ID Class_ID(0x9154,0)

ExpAnimation::ExpAnimation(IGameNode* gameNode) : ExpObject(gameNode)
{

}

void ExpAnimation::Update(AnimationStream* parentAnimation)
{
	//http://nebuladevice.cubik.org/documentation/nebula2/nmaxexport__transformation_8cc-source.shtml
	// is it animated?
	IGameControl* control = pGameNode->GetIGameControl();

	IGameKeyTab posKeys;
	IGameKeyTab rotKeys;

	IGameControl::MaxControlType rotType = control->GetControlType(IGAME_ROT); 
	IGameControl::MaxControlType posType = control->GetControlType(IGAME_POS);

	const char* name = pGameNode->GetName();

	// bip01
	if (!parentAnimation)
	{
		GMatrix worldTrans = pGameNode->GetWorldTM();
		Point3 pos = worldTrans.Translation();

		PositionKey posKey;
		posKey.position = Vector3(pos[0], pos[1], pos[2]) * MAX_TO_METERS;
		posKey.time = 0;

		positions.Insert(posKey);		
		return;
	}

	if (posType == IGameControl::IGAME_BIPED)
	{
		Control* posControl = control->GetMaxControl(IGAME_POS);
		if (posControl->ClassID() == BIPSLAVE_CONTROL_CLASS_ID)
		{
			// Get the Biped Export Interface from the controller 
            //IBipedExport *BipIface = (IBipedExport *) c->GetInterface(I_BIPINTERFACE);

			Tab<TimeValue> times;
			posControl->GetKeyTimes(times, FOREVER, 0);

			for (int i = 0; i < times.Count(); ++i)
			{
				if (times[i] < GetIGameInterface()->GetSceneStartTime() || times[i] > GetIGameInterface()->GetSceneEndTime())
					continue;

				IGameNode* parentGNode = pGameNode->GetNodeParent();
				GMatrix gCurrentMat = pGameNode->GetWorldTM(times[i]);
				GMatrix gParentMat =  parentGNode->GetWorldTM(times[i]);
				GMatrix gRelativeMat = gCurrentMat * gParentMat.Inverse();

				Point3 bonePos = gRelativeMat.Translation();

				PositionKey posKey;
				posKey.position = Siphon::Vector3(bonePos.x, bonePos.y, bonePos.z) * MAX_TO_METERS;
				posKey.time = times[i];
				positions.Insert(posKey);
			}
		}
	}
	else
	{
		// positional keys
		if (!control->GetQuickSampledKeys(posKeys, IGAME_POS))
			control->GetFullSampledKeys(posKeys, 30, IGAME_POS);
		
		for (int i = 0; i < posKeys.Count(); ++i)
		{
			IGameKey& key = posKeys[i];
			IGameSampleKey& sampleKey = key.sampleKey;

			if (key.t < GetIGameInterface()->GetSceneStartTime() || key.t > GetIGameInterface()->GetSceneEndTime())
				continue;

			Point3 pos = sampleKey.pval;

			PositionKey posKey;
			posKey.position = Vector3(pos[0], pos[1], pos[2]) * MAX_TO_METERS;
			posKey.time = key.t;

			positions.Insert(posKey);
		}
	}

	if (rotType == IGameControl::IGAME_BIPED)
	{
		Control* rotControl = control->GetMaxControl(IGAME_ROT);
		IKeyControl* ikeys = GetKeyControlInterface(rotControl);

		if (rotControl->ClassID() == BIPSLAVE_CONTROL_CLASS_ID)
		{
			// Get the Biped Export Interface from the controller 
            //IBipedExport *BipIface = (IBipedExport *) c->GetInterface(I_BIPINTERFACE);

			Tab<TimeValue> times;
			rotControl->GetKeyTimes(times, FOREVER, 0);

			for (int i = 0; i < times.Count(); ++i)
			{
				if (times[i] < GetIGameInterface()->GetSceneStartTime() || times[i] > GetIGameInterface()->GetSceneEndTime())
					continue;

				IGameNode* parentGNode = pGameNode->GetNodeParent();
				GMatrix gCurrentMat = pGameNode->GetWorldTM(times[i]);
				GMatrix gParentMat =  parentGNode->GetWorldTM(times[i]);
				GMatrix gRelativeMat = gCurrentMat * gParentMat.Inverse();

				Quat boneRot = Quat(gRelativeMat.ExtractMatrix3());
				
				RotationKey rotKey;
				rotKey.rotation = Siphon::Quaternion(boneRot.x, boneRot.y, boneRot.z, -boneRot.w);

				rotKey.time = times[i];
				rotations.Insert(rotKey);
			}		
		}
	}
	else
	{
		// rotation keys
		if (!control->GetQuickSampledKeys(rotKeys, IGAME_ROT))
			control->GetFullSampledKeys(rotKeys, 30, IGAME_ROT);

		Matrix4 nodeTM(pGameNode->GetObjectTM());
		Siphon::Quaternion origonal;
		origonal.CreateFromMatrix(nodeTM);
		for (int i = 0; i < rotKeys.Count(); ++i)
		{
			IGameKey key = rotKeys[i];

			if (key.t < GetIGameInterface()->GetSceneStartTime() || key.t > GetIGameInterface()->GetSceneEndTime())
				continue;

			RotationKey rotKey;
			rotKey.rotation = Siphon::Quaternion(key.sampleKey.qval.x, key.sampleKey.qval.y, key.sampleKey.qval.z, -key.sampleKey.qval.w);
	
			rotKey.time = key.t;
			rotations.Insert(rotKey);
		}
	}
}
