#ifndef _SIPHON_OBJECT_
#define _SIPHON_OBJECT_

class ExpObject
{
public:
	ExpObject(IGameNode* pGameNode);
	~ExpObject();

	virtual void Update() = 0;
	virtual IGameObject::ObjectTypes GetGameType() const { return pGameObject->GetIGameType(); }
	virtual const char* GetName() const { return pGameNode->GetName(); }

protected:
	IGameNode* pGameNode;
	IGameObject* pGameObject;
};

#endif