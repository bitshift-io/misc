
#include "SiphonExporter.h"
#include "../Engine/Math3D.h"

#define MAX_TO_METERS 1.0f / 39.37f

#define BIPBONE_CONTROL_CLASS_ID    Class_ID(0x9125,0)

// this is the class for all biped controllers except the root and the footsteps
#define BIPSLAVE_CONTROL_CLASS_ID Class_ID(0x9154,0)

// this is the class for the center of mass, biped root controller ("Bip01")
#define BIPBODY_CONTROL_CLASS_ID  Class_ID(0x9156,0)

// this is the class for the biped footstep controller ("Bip01 Footsteps")
#define FOOTPRINT_CLASS_ID Class_ID(0x3011,0)  

ExpSkeleton::ExpSkeleton(IGameNode* gameNode) : ExpObject(gameNode)
{

}

void ExpSkeleton::Update()
{
	// http://cvs.sourceforge.net/viewcvs.py/neoengine/neoexporters/3DSMax/exporter.cpp?rev=1.6

	IGameSkin* pSkin = pGameObject->GetIGameSkin();

	std::set<IGameNode*> boneSet;

	// go through each vertex, compile a list of bones
	for (int v = 0; v < pSkin->GetNumOfSkinnedVerts(); ++v)
	{
		for (int b = 0; b < pSkin->GetNumberOfBones(v); ++b)
		{
			IGameNode* boneNode = pSkin->GetIGameBone(v, b);
			const char* boneName = boneNode->GetName();
			if (boneNode && !boneNode->IsNodeHidden())
				boneSet.insert(boneNode);
		}
	}

	// now we have a unique set of bones, we need to find the root bone
	std::set<IGameNode*>::iterator boneIt;
	std::set<IGameNode*>::iterator secBoneIt;

	if (boneSet.empty())
		return;

	IGameNode* root = (*boneSet.begin());

	IGameNode* parent = root->GetNodeParent();;
	
	bool rootFound = false;
	while (parent && !rootFound)
	{
		const char* parentName = root->GetName();

		if (!parent || !parent->GetIGameObject() || parent->IsNodeHidden())
		{
			rootFound = true;
		}
		else
		{
			root = parent;
			parent = root->GetNodeParent();
		}
	}
	
	/*
	bool child = true;
	while (child)
	{
		child = false;
		for (boneIt = boneSet.begin(); boneIt != boneSet.end(); boneIt++)
		{
			// is the root a child of this bone?
			for (int i = 0; i < (*boneIt)->GetChildCount(); ++i)
			{
				if ((*boneIt)->GetNodeChild(i) == root)
				{
					root = (*boneIt);
					child = true;
					break;
				}
			}
		}
	}*/
	const char* boneName = root->GetName();

	// create an animation group
	curAnim = new Animation();
	AddBone(root, 0, 0);
}

IGameSkin* ExpSkeleton::GetSkin()
{
	return pGameObject->GetIGameSkin();
}

Matrix4 ExpSkeleton::GetWorldToObjectTransform()
{
	// Fabian's essay on max matrices:
	// we are using IGameInterface, so ALL meshes
	// are in world space
	// we need to convert characters back to thier initial pose space
	//
	// MaxController::GetObjectTM will return a matrix to convert
	// from the hidden/invisible pivot point of the mesh (this can be seen
	// by clicking the hierachey tab, and clicking reset pivot) to the visible 
	// pivot point of the mesh (this is seen if you dont reset the pivot point)
	//
	// GetNodeTM will return a matrix, that converts from the visible pivot point
	// to world space.

	// note: max treats skin differently from physique
	// objectTM = parentTM * nodeTM * offsetTM
	// IPhysique->GetInitNodeTM(..) returns objectTM
	// while ISkin->GetInitNodeTM(..) returns nodeTM
	// this fixes the max inconsistency
	INode* node = pGameNode->GetMaxNode();
	Matrix4 worldToObj(Matrix4::Identity);

	if (GetSkin()->GetSkinType() == IGameSkin::IGAME_SKIN)
	{
		// convert FROM world TO visible pivot point
		::Matrix3 nodeTM = node->GetNodeTM(0); // pivot to node
		::Matrix3 worldToPivot = Inverse(nodeTM); // go from world to pivot space

		Point3 p = worldToPivot.GetRow(0);
		worldToObj[0] = p[0];
		worldToObj[1] = p[1];
		worldToObj[2] = p[2];

		p = worldToPivot.GetRow(1);
		worldToObj[5] = p[0];
		worldToObj[6] = p[1];
		worldToObj[7] = p[2];

		p = worldToPivot.GetRow(2);
		worldToObj[9] = p[0];
		worldToObj[10] = p[1];
		worldToObj[11] = p[2];

		p = worldToPivot.GetRow(3);
		worldToObj[13] = p[0];
		worldToObj[14] = p[1];
		worldToObj[15] = p[2];
/*
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				worldToObj[i*4 + j] = p[j];
			}
		}*/

		int nothing = 0;
		//MaxController::CopyMaxMatrix(worldToPivot, worldToObj);
	}
/*	else
	{
		// convert FROM world TO hidden pivot point
		Matrix3 objectTM = MaxController::GetObjectTM(node, 0);
		Matrix3 nodeTM = node->GetNodeTM(0); // pivot to node
		Matrix3 worldToPivot = Inverse(nodeTM * objectTM); // go from world to reset pivot space
		MaxController::CopyMaxMatrix(worldToPivot, worldToObj);
	}	*/
	
	return worldToObj;
}

void ExpSkeleton::AddBone(IGameNode* bone, IGameNode* parent, int parentIdx)
{
	if (!bone || bone->IsNodeHidden())
		return;

	const char* boneName = bone->GetName();

	Vector3 vScale(MAX_TO_METERS, MAX_TO_METERS, MAX_TO_METERS);//FMNOTE: this should be 0.01f;

	Bone b;
	b.parentIdx = parentIdx;
	b.SetName(boneName);

	IGameSkin* pSkin = pGameObject->GetIGameSkin();
	GMatrix mat, parentMat;
	pSkin->GetInitBoneTM(bone, mat);

	if (parent)
	{
		pSkin->GetInitBoneTM(parent, parentMat);
	}

	// inverse out the parent 
	b.defaultPose = mat * Inverse(parentMat.ExtractMatrix3());

	GMatrix initialSkinTM;
	pSkin->GetInitSkinTM(initialSkinTM);

	GMatrix boneSpace = GMatrix(Inverse(parentMat.ExtractMatrix3())) * mat;

	
/*
	GMatrix parentMatrix;
	//parentMatrix.SetIdentity();
	IGameNode* parentBone = bone->GetNodeParent();
	while (parentBone)
	{
		GMatrix parentMat;
		pSkin->GetInitBoneTM(parentBone, parentMat);
		parentMatrix = GMatrix(Inverse(parentMat.ExtractMatrix3())) * parentMatrix;

		parentBone = parentBone->GetNodeParent();
	}
	b.defaultPose = mat * parentMatrix;
*/

	Vector3 scaledTranslation = b.defaultPose.GetTranslation();
	scaledTranslation = scaledTranslation * vScale;
	b.defaultPose.Translate(scaledTranslation);


	
	b.skinToBone = mat; //b.defaultPose;

	// dont forget to scale position
	scaledTranslation = b.skinToBone.GetTranslation();
	scaledTranslation = scaledTranslation * vScale;
	b.skinToBone.Translate(scaledTranslation);

	b.skinToBone.Inverse();


//GMatrix(Inverse(boneSpace.ExtractMatrix3()));// * Inverse(initialSkinTM.ExtractMatrix3()); // this may need to be swapped around
//	b.skinToBone.Inverse(); // i think this is right


	// set up bone bounds
	Box3 bb;
	IGameObject* obj = bone->GetIGameObject();
	obj->GetBoundingBox(bb);
	Matrix4 invDefaultPose = b.defaultPose;
	invDefaultPose.Inverse();
	b.bounds.SetMin(invDefaultPose * (Vector3(bb.pmin.x, bb.pmin.y, bb.pmin.z) * MAX_TO_METERS));
	b.bounds.SetMax(invDefaultPose * (Vector3(bb.pmax.x, bb.pmax.y, bb.pmax.z) * MAX_TO_METERS));

	//mat = mat * Inverse(parentMat.ExtractMatrix3());
	Point4 parentTrans4 = parentMat.GetRow(3);
	Point4 trans4 = mat.GetRow(3) - parentTrans4;	
	Vector3 trans = Vector3(trans4[0], trans4[1], trans4[2]); // swap z and y
	b.offset = trans * MAX_TO_METERS; //we need to scale bones accordingly
/*
	// calculate angle between this bone and parent bone, which 
	// will be used to adjust the animation
	Siphon::Quaternion animOffset2(Vector3(0,0,0), 0);
	if (parent)
	{
		Vector3 parentDir = trans;
		parentDir.Normalize();
		Vector3 boneDir = Vector3(mat.GetRow(0).x, mat.GetRow(0).y, mat.GetRow(0).z); 
		boneDir.Normalize();

		float angle = boneDir.AngleBetween(parentDir);
		Vector3 axis = parentDir.CrossProduct(boneDir);

		animOffset2.CreateFromAxisAngle(axis, angle);
		animOffset2.Normalize();
	}	


	Siphon::Quaternion animOffset = CalculateParentOffset(pSkin, bone, parent);

	// for debug
	Matrix4 test = animOffset.GetRotationMatrix();
*/
	Array<AnimationStream>& animations = curAnim->GetStreams();
	ExpAnimation anim(bone);
	anim.Update(parent ? &(animations[b.parentIdx]) : 0);
	animations.Insert(anim);

	//make sure this bone hasnt been visted before
	for (int i = 0; i < iGameBones.GetSize(); ++i)
	{
		if (iGameBones[i] == bone)
			int nothing = 0;
	}

	int idx = bones.Insert(b);
	iGameBones.Insert(bone);

	// iterate through children bones
	int count = bone->GetChildCount();
	for (int i = 0; i < count; ++i)
	{
		AddBone(bone->GetNodeChild(i), bone, idx);
	}

	int nothing = 0;
}

Siphon::Quaternion ExpSkeleton::CalculateParentOffset(IGameSkin* pSkin, IGameNode* bone, IGameNode* parent)
{
	Siphon::Quaternion animOffset(Vector3(0,0,0), 0);
	//return animOffset;

	//if (!parent)
	//	return animOffset;

	GMatrix mat, parentMat;
	pSkin->GetInitBoneTM(bone, mat);

	if (parent)
		pSkin->GetInitBoneTM(parent, parentMat);
/*
	Point4 parentTrans4 = parentMat.GetRow(3);
	Point4 trans4 = mat.GetRow(3) - parentTrans4;	
	Vector3 trans = Vector3(trans4[0], trans4[1], trans4[2]); // swap z and y

	Vector3 parentDir = trans;
	parentDir.Normalize();
*/

/*
	Vector3 boneDir = Vector3(mat.GetRow(0).x, mat.GetRow(0).y, mat.GetRow(0).z); 
	boneDir.Normalize();

	Vector3 parentDir = Vector3(parentMat.GetRow(0).x, parentMat.GetRow(0).y, parentMat.GetRow(0).z);
	parentDir.Normalize();

	float angle = boneDir.AngleBetween(parentDir);
	
	// FMNOTE: i think what we need here, is to project a vector on to each axis
	// then calculate the axis/angle for each axis?

	// fab test 
	float gedAngle = Math::RtoD(angle);

	Vector3 axis = parentDir.CrossProduct(boneDir);

	// if angle is puny, cross product wont work!
	if (angle < Math::Epsilon)
		axis = Vector3(0,0,1);

	axis.Normalize();

	animOffset.CreateFromAxisAngle(axis, angle);
	animOffset.Normalize();

	return animOffset;
*/
	Matrix4 testMat(mat), testParent(parentMat);
	testParent.Inverse();
	testMat = testMat * testParent;

	Siphon::Quaternion ret;
	ret.CreateFromMatrix(testMat);
	return ret;
}

int ExpSkeleton::GetBoneIdx(IGameNode* bone)
{
	for (int i = 0; i < iGameBones.GetSize(); ++i)
	{
		if (bone == iGameBones[i])
			return i;
	}

	return -1;
}

bool ExpSkeleton::Save(const char* file)
{
	Skeleton::Save(file);

	char buffer[256];
	strcpy(buffer, file);
	char* ext = buffer + (strlen(buffer) - 3);
	strcpy(ext, "anm");

	return curAnim->Save(buffer);
}

bool ExpSkeleton::IsBone(IGameNode* bone)
{
	INode* node = bone->GetMaxNode();
	Object* obj = bone->GetIGameObject()->GetMaxObject();
	Control* ctrl = node->GetTMController(); 

    // check if this is part of the biped hierarchy
    if(ctrl)
    {
        Class_ID nodeControllerClassId = ctrl->ClassID();
        Class_ID objectClassId = obj->ClassID();

        // make sure its not a nub
        if (objectClassId == Class_ID(DUMMY_CLASS_ID, 0x0))
			return false;

        // this is a bone, we should not include it in the hierarchy
        // instead we allow the dexcharacter to process it         
        if ((objectClassId == BIPBONE_CONTROL_CLASS_ID) ||
            (objectClassId == Class_ID(BONE_CLASS_ID, 0)) ||
            (ctrl && nodeControllerClassId == BIPSLAVE_CONTROL_CLASS_ID))
                return true;
    }

	// check if bone properties are turned on
	if (node->GetBoneNodeOnOff())
		return true;

	return false;
}