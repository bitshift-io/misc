#ifndef _SIPHON_EXPSKELETON_
#define _SIPHON_EXPSKELETON_

#include "IGame\IGame.h"

#include "..\Engine\Animation.h"
#include "..\Util\Array.h"

class IGameNode;

class ExpSkeleton : public ExpObject, public Siphon::Skeleton
{
public:
	ExpSkeleton(IGameNode* gameNode);

	virtual void Update();

	virtual void AddBone(IGameNode* bone, IGameNode* parent, int parentIdx);

	virtual bool Save(const char* file);

	IGameSkin* GetSkin();

	// given a bone, we can calculate its index if this skeleton has been updated
	virtual int GetBoneIdx(IGameNode* bone);

	static bool IsBone(IGameNode* bone);

	Matrix4 GetWorldToObjectTransform();

protected:

	Siphon::Quaternion CalculateParentOffset(IGameSkin* pSkin, IGameNode* bone, IGameNode* parent);

	Array<IGameNode*> iGameBones;
	Animation* curAnim;
};

#endif
