
#include "SiphonExporter.h"

void ExportModel::Initilize( IGameScene* scene, TCHAR *name, Interface* ip ) 
{
	this->scene = scene; 
	this->ip = ip;
	fileName = name;
}

void ExportModel::Shutdown()
{

}

void ExportModel::InitDialog(HWND panel)
{
	this->panel = panel;
	SendMessage(GetDlgItem(panel, IDC_BATCH), BM_SETCHECK, BST_CHECKED, 0);
}

void ExportModel::UpdateDialog()
{
	batchEnable = SendMessage(GetDlgItem(panel, IDC_BATCH), BM_GETCHECK, 0, 0) == BST_CHECKED ? true : false;
	exportMaterials = SendMessage(GetDlgItem(panel, IDC_MATERIALS), BM_GETCHECK, 0, 0) == BST_CHECKED ? true : false;
}

bool ExportModel::ProcessNode( IGameNode* pNode )
{
	if (!pNode || pNode->IsNodeHidden())
		return true;

	const char* name = pNode->GetName();

	bool bLoopThroughChildren = true;
	switch( pNode->GetIGameObject()->GetIGameType() )
	{
	case IGameObject::IGAME_MESH:
		ProcessMesh(pNode);
		break;
	case IGameObject::IGAME_HELPER:
		ProcessReference(pNode);
//		bLoopThroughChildren = false;
		break;
	}

	//loop through children
	if (bLoopThroughChildren)
	{
		int noChildren = pNode->GetChildCount();
		for( int i = 0; i < noChildren; ++i )
			ProcessNode( pNode->GetNodeChild(i) );
	}

	return true;
}

bool ExportModel::ProcessScene()
{
	float progressRate = 100.0f / meshes.size();
	int progress = 0;

	// process meshes
	std::map<IGameMaterial*, std::vector<ExpMesh*> > materialCache;
	std::map<IGameMaterial*, std::vector<ExpMesh*> >::iterator matIt;

	std::vector<ExpMesh*>::iterator meshIt;
	for (meshIt = meshes.begin(); meshIt != meshes.end(); meshIt++)
	{
		ip->ProgressUpdate(progress);
		progress += (int)progressRate;

		(*meshIt)->Update();

		// while we iterate through meshes, lets compiles a material list
		IGameMaterial* meshMat = (*meshIt)->GetGameMaterial();
		if (!meshMat)
		{
			char error[256];
			sprintf(error, "Mesh %s has no material applied.", (*meshIt)->GetName());
            MessageBox(0, error, "Error - Export Aborting", MB_OK);
			return false;
		}
		materialCache[meshMat].push_back((*meshIt));
	}

	model.AddMeshes(materialCache, true);


//	model.Process();
/*
	// process any other objects
	for (int o = 0; o < objects.size(); o++)
	{
		objects[o]->Update();
	}*/

	ip->ProgressUpdate(100);
	return true;
}

bool ExportModel::ProcessSkin(IGameNode* node)
{
	// if not already existing, add it
	if (!GetSkeleton(node))
	{
		ExpSkeleton* skeleton = new ExpSkeleton(node);
		skeleton->Update();
		skeletons.push_back(skeleton);
	}

	return true;
}

ExpSkeleton* ExportModel::GetSkeleton(IGameNode* node)
{
	if (!node || !node->GetIGameObject() || !node->GetIGameObject()->IsObjectSkinned())
		return 0;

	IGameSkin* skin = node->GetIGameObject()->GetIGameSkin();

	std::vector<ExpSkeleton*>::iterator skelIt;
	for (skelIt = skeletons.begin(); skelIt != skeletons.end(); skelIt++)
	{
		if (skin == (*skelIt)->GetSkin())
			return (*skelIt);
	}

	return 0;
}

bool ExportModel::ProcessMesh(IGameNode* node)
{
	if (ExpSkeleton::IsBone(node))
		return true;

	const char* name = node->GetName();
	IGameMesh* pGameMesh = (IGameMesh*)node->GetIGameObject();
	pGameMesh->SetCreateOptimizedNormalList();
	pGameMesh->InitializeData();

	// if its skinned....
	if (node->GetIGameObject()->IsObjectSkinned())
	{
		ProcessSkin(node);
	}

	// okay the problem:
	// a mesh may have multiple mat id's but be using the same material!
	std::map<IGameMaterial*, std::vector<int> > matIdMap;
	std::map<IGameMaterial*, std::vector<int> >::iterator matIt;

	Tab<int> matIds = pGameMesh->GetActiveMatIDs();
	for( int i = 0; i < matIds.Count(); ++i )
	{
		Tab<FaceEx*> exFaces = pGameMesh->GetFacesFromMatID( matIds[i] );
		if (exFaces.Count())
			matIdMap[pGameMesh->GetMaterialFromFace(exFaces[0])].push_back(matIds[i]);
	}

	for (matIt = matIdMap.begin(); matIt != matIdMap.end(); matIt++)
	{
		if (matIt->second.size() > 0)
			meshes.push_back(new ExpMesh(node, matIt->second, matIt->first, GetSkeleton(node)));
	}

/*
		if( exFaces.Count() )
		{
			model.AddMesh(node, matIds[i], pGameMesh->GetMaterialFromFace(exFaces[0]));

			//meshes.push_back(new ExpMesh(node, matIds[i], pGameMesh->GetMaterialFromFace(exFaces[0])));
		}
	}*/
	
	return true;
}

bool ExportModel::ProcessReference(IGameNode* node)
{
	//objects.push_back(new ExpReference(node));
	return true;
}

bool ExportModel::SaveScene()
{
	model.Save(fileName);//, exportMaterials);

	char skelName[256];
	strcpy(skelName, fileName);

	char* ext = skelName + (strlen(skelName) - 3);
	strcpy(ext, "skl");

	std::vector<ExpSkeleton*>::iterator skelIt;
	for (skelIt = skeletons.begin(); skelIt != skeletons.end(); skelIt++)
	{
		(*skelIt)->Save(skelName);
	}
	

/*

	// save materials
	for (int m = 0; m < model.GetMaterialCount(); m++)
	{
		ExpMaterial material(model.GetGameMaterial(m));
		material.Save(buffer);
	}
/*
	// save any other objects
	for (int o = 0; o < objects.size(); o++)
	{
		objects[o]->Save(pFile);
	}

	fclose(pFile);
*/
	return true;
}

/*
bool ExportModel::ExportMaterial( IGameMaterial* material, sMaterialTable& matTable )
{
	if( material == 0 )
		return false;

	bool bFree = false;
	TCHAR* matName = "null"; // = material->GetMaterialName();
	TCHAR* matTex = "null";

	//fprintf(pFile, "Num textures: %i\n", material->GetNumberOfTextureMaps() );
	//fflush(pFile);
	int size;
	if( material->GetNumberOfTextureMaps() > 1 )
	{
		//bFree = true;
		matName = FixPath( material->GetIGameTextureMap(1)->GetBitmapFileName(), "SMT" );
		matTex = FixPath( material->GetIGameTextureMap(1)->GetBitmapFileName(), "dds" );
	}
	else if( material->GetNumberOfTextureMaps() )
	{
		matName = FixPath( material->GetIGameTextureMap(0)->GetBitmapFileName(), "SMT" );
		matTex = FixPath( material->GetIGameTextureMap(0)->GetBitmapFileName(), "dds" );

/*
		bool bDot = false;
		int d = strlen( bmpName );
		for( int i = strlen( bmpName ); i >= 0; --i )
		{
			if( bmpName[i] == '\\' )
			{
				++i;
				break;
			}

			if( bmpName[d] == '.' )
				bDot = true;

			if( bDot == false )
				--d;
		}

		size = d - i;
		//now copy into matName
		matName = new TCHAR[ size + 5 ];
		int l = 0;
		for( int k = i; k < d; ++k, ++l )
		{
			matName[l] = bmpName[k];
		}
		//crop of the .bmp or what ever...
		matName[ size + 0 ] = '.';
		matName[ size + 1 ] = 'S';
		matName[ size + 2 ] = 'M';
		matName[ size + 3 ] = 'T';
		matName[ size + 4 ] = '\0';
		//matName = bmpName;* /
	}
	else
	{
		//deal with no texture here!
		//material->GetMaterialName()
		return false;
	}

	strcpy( matTable.material, matName);

	//ExportData( pFile, matName, sizeof(char) * (strlen(matName) + 1), MATERIALTABLE, strlen(matName) + 1 );

	FILE* matFile = fopen( matName, "wb" );// matName

	if( matFile == 0 )
		return false;

	//matName = FixPath( matName, "dds" );
	/*
	matName[ size - 3 ] = 'd';
	matName[ size - 2 ] = 'd';
	matName[ size - 1 ] = 's';* /

	fprintf(matFile, "<texture> %s\n", matTex );

	//ExportData( matFile, matTex, sizeof(char) * (strlen(matTex) + 1), TEXTUREREF, strlen(matTex) + 1 );
	
	sMaterial exportMat;

	Point3 diffuse;
	material->GetDiffuseData()->GetPropertyValue( diffuse );
	exportMat.diffuse.vert[0] = diffuse.x;
	exportMat.diffuse.vert[1] = diffuse.y;
	exportMat.diffuse.vert[2] = diffuse.z;

	Point3 specular;
	material->GetSpecularData()->GetPropertyValue( specular );
	exportMat.specular.vert[0] = specular.x;
	exportMat.specular.vert[1] = specular.y;
	exportMat.specular.vert[2] = specular.z;

	material->GetGlossinessData()->GetPropertyValue( exportMat.shine );
	
	fprintf( matFile, "<property> %f %f %f\n", exportMat.diffuse.vert[0], exportMat.diffuse.vert[1], exportMat.diffuse.vert[1]);
	fprintf( matFile, "<property> %f %f %f\n", exportMat.specular.vert[0], exportMat.specular.vert[1], exportMat.specular.vert[2]);
	//ExportData( matFile, &exportMat, sizeof(sMaterial), MATERIAL );

	//ExportData( matFile, 0, 0, ENDOFFILE );
	fclose( matFile );
/*
	if( material->GetNumberOfTextureMaps() > 1 && material->GetIGameTextureMap(0)->GetStdMapSlot() == ID_AM )
	{		
		TCHAR* bmpLightMap = FixPath( material->GetIGameTextureMap(0)->GetBitmapFileName(), "dds" );
		ExportData( pFile, bmpLightMap, sizeof(char) * (strlen(bmpLightMap) + 1), LIGHT_MAP, sizeof(char) * (strlen(bmpLightMap) + 1) );
	}* /

	return true;
}*/
