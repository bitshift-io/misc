#ifndef _SIPHON_EXPORTBASE_
#define _SIPHON_EXPORTBASE_

class ExportBase
{
public:
	virtual void Initilize( IGameScene* scene, TCHAR *name, Interface* ip ) = 0;
	virtual void Shutdown() = 0;

	virtual bool ProcessNode( IGameNode* pNode ) = 0;
	virtual bool ProcessScene() = 0;

	virtual void InitDialog(HWND panel) = 0;
	virtual void UpdateDialog() = 0;

	virtual bool SaveScene() = 0;

protected:
	HWND panel;
};

#endif