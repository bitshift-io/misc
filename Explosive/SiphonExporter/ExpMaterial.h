#ifndef _SIPHON_EXPMATERIAL_
#define _SIPHON_EXPMATERIAL_

#include "SiphonExporter.h"
#include "..\Engine\Mesh.h"

using namespace Siphon;

class ExpMaterial : public Siphon::Material
{
public:
	
	ExpMaterial(IGameMaterial* pGameMaterial);
	
	virtual void Update() {}

	virtual bool Save(const char* file);
protected:
	char* FixPath(const char* const bmpName, char ext[3]);

	IGameMaterial* pGameMaterial;
};

#endif