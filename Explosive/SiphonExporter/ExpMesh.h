#ifndef _SIPHON_EXPMESH_
#define _SIPHON_EXPMESH_

#include <vector>
#include "SiphonExporter.h"

class ExpSkeleton;

class ExpMesh : public ExpObject
{
	friend class ExpModel;

public:

	ExpMesh(IGameNode* pGameNode, std::vector<int>& matIds, IGameMaterial* pGameMaterial, ExpSkeleton* skel);

	virtual bool Save(FILE* pFile) { return true; }
	virtual void Update();
	
	IGameMaterial* GetGameMaterial() const;
protected:
	std::vector<int> matIds;
	IGameMaterial* pGameMaterial;

	ExpSkeleton* skeleton;
	Siphon::VertexBuffer	buffer;
	std::vector<unsigned int> indices;
	std::vector<Point3> positions;
	std::vector<Point2> uvCoords;
	std::vector<Point3> normals;
	std::vector<Point3> colors;
};

#endif