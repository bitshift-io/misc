/**********************************************************************
 *<
	FILE: SiphonExporter.h

	DESCRIPTION:	Includes for Plugins

	CREATED BY:

	HISTORY:

 *>	Copyright (c) 2000, All Rights Reserved.
 **********************************************************************/

#ifndef __SIPHONEXPORTER__H
#define __SIPHONEXPORTER__H

#pragma comment(lib, "USER32.LIB")

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

//#define _SIPHON_EXPORT_

#include "IGame\IGame.h"
#include "IGame\IGameObject.h"
#include "IGame\IGameProperty.h"
#include "IGame\IGameControl.h"
#include "IGame\IGameModifier.h"
#include "IGame\IConversionManager.h"
#include "IGame\IGameError.h"

#include <windows.h>
#include <winuser.h>

#include "Math3D.h"
#include "Array.h"
#include "File.h"
#include "File.h"
#include "Log.h"
#include "Macros.h"

#include "../Util/Resource.h"
#include "../Engine/Mesh.h"
#include "../Engine/Animation.h"

#include "ExpObject.h"
#include "ExpAnimation.h"
#include "ExpMesh.h"
#include "ExpModel.h"
#include "ExpMaterial.h"
#include "ExpSkeleton.h"
#include "ExportBase.h"
//#include "ExportWorld.h"
#include "ExportModel.h"
#include "ExportAnimation.h"


extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

#endif // __SIPHONEXPORTER__H
