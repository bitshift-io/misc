/**********************************************************************
 *<
	FILE: SiphonExporter.cpp

	DESCRIPTION:	Appwizard generated plugin

	CREATED BY:

	HISTORY:

 *>	Copyright (c) 2000, All Rights Reserved.
 **********************************************************************/

#include <windows.h>
#include "SiphonExporter.h"

#define SIPHONEXPORTER_CLASS_ID	Class_ID(0x19c02651, 0x274632a6)

class SiphonExporter : public SceneExport
{
public:
		enum EXPORT_TYPE
		{
			EXPORT_MODEL = 0,
			EXPORT_ANIMATION = 1
			//EXPORT_MAP = 1
		};

		HWND propertySheet;
		ExportModel model;
		ExportAnimation animation;

		ExportBase* exportType;

		// should we export
		bool export;

		bool exportSelected;
		EXPORT_TYPE selectedPage;

		int renderDivisions;
		int collisionDivisions;
		int noSectors;
		LPTSTR temp;


		IGameScene* scene;


		static HWND hParams;


		int				ExtCount();					// Number of extensions supported
		const TCHAR *	Ext(int n);					// Extension #n (i.e. "3DS")
		const TCHAR *	LongDesc();					// Long ASCII description (i.e. "Autodesk 3D Studio File")
		const TCHAR *	ShortDesc();				// Short ASCII description (i.e. "3D Studio")
		const TCHAR *	AuthorName();				// ASCII Author name
		const TCHAR *	CopyrightMessage();			// ASCII Copyright message
		const TCHAR *	OtherMessage1();			// Other message #1
		const TCHAR *	OtherMessage2();			// Other message #2
		unsigned int	Version();					// Version number * 100 (i.e. v3.01 = 301)
		void			ShowAbout(HWND hWnd);		// Show DLL's "About..." box

		BOOL SupportsOptions(int ext, DWORD options);
		int				DoExport(const TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts=FALSE, DWORD options=0);


		bool ReadConfig();
		bool WriteConfig();
		TSTR GetCfgFilename();

		//Constructor/Destructor

		SiphonExporter();
		~SiphonExporter();

};

class SiphonExporterClassDesc:public ClassDesc2
{
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new SiphonExporter(); }
	const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); }
	SClass_ID		SuperClassID() { return SCENE_EXPORT_CLASS_ID; }
	Class_ID		ClassID() { return SIPHONEXPORTER_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("SiphonExporter"); }	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance() { return hInstance; }				// returns owning module handle
	virtual char* GetRsrcString( long t ){ return NULL; }
};

static SiphonExporter *sImp = NULL;
static SiphonExporterClassDesc SiphonExporterDesc;
ClassDesc2* GetSiphonExporterDesc() { return &SiphonExporterDesc; }

BOOL CALLBACK SiphonExporterOptionsDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam);

BOOL CALLBACK PanelDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) 
{
	if (sImp == NULL)
		return FALSE;

	switch(message)
	{
	case WM_INITDIALOG:
		{
			PROPSHEETPAGE* page = (PROPSHEETPAGE*)lParam;

			switch (page->lParam)
			{
			case SiphonExporter::EXPORT_MODEL:
				sImp->model.InitDialog(hWnd);
				break;
			case SiphonExporter::EXPORT_ANIMATION:
				sImp->animation.InitDialog(hWnd);
				break;
			}
		}
		break;
	case WM_NOTIFY:
		switch (((NMHDR FAR *) lParam)->code)
		{
		case PSN_SETACTIVE:
			{
				sImp->selectedPage = (SiphonExporter::EXPORT_TYPE)PropSheet_HwndToIndex(sImp->propertySheet, hWnd);
				switch (sImp->selectedPage)
				{
				case SiphonExporter::EXPORT_MODEL:
					sImp->exportType = &(sImp->model);
					break;
				case SiphonExporter::EXPORT_ANIMATION:
					sImp->exportType = &(sImp->animation);
					break;
				}
			}
			break;
		case PSN_APPLY:
			{
				sImp->exportType->UpdateDialog();
				sImp->export = true;
			}
			break;
		case PSN_QUERYCANCEL:
			{
				sImp->export = false;
			}
			break;
		}
		break;
	}
	return FALSE;
}

int CALLBACK PropSheetProc(HWND hwndDlg, UINT uMsg, LPARAM lParam)
{
	if (sImp == NULL)
		return FALSE;

	sImp->propertySheet = hwndDlg;
	return TRUE;
}

VOID DoPropertySheet(HWND hwndOwner)
{
	HINSTANCE g_hinst = (HINSTANCE)GetWindowLong(hwndOwner,GWL_HINSTANCE);

    PROPSHEETPAGE psp[2];
    PROPSHEETHEADER psh;

	/*
    psp[1].dwSize = sizeof(PROPSHEETPAGE);
    psp[1].dwFlags =  PSP_USETITLE;
    psp[1].hInstance = g_hinst;
    psp[1].pszTemplate = MAKEINTRESOURCE(DLG_SMAP);
    psp[1].pfnDlgProc = PanelDlgProc;
    psp[1].pszTitle = "Map";
    psp[1].lParam = 0;
    psp[1].pfnCallback = NULL;*/

    psp[0].dwSize = sizeof(PROPSHEETPAGE);
    psp[0].dwFlags = PSP_USEICONID | PSP_USETITLE;
    psp[0].hInstance = g_hinst;
    psp[0].pszTemplate = MAKEINTRESOURCE(DLG_SMODEL);
    psp[0].pfnDlgProc = PanelDlgProc;
    psp[0].pszTitle = "Model";
	psp[0].lParam = SiphonExporter::EXPORT_MODEL;
    psp[0].pfnCallback = NULL;

    psp[1].dwSize = sizeof(PROPSHEETPAGE);
    psp[1].dwFlags = PSP_USEICONID | PSP_USETITLE;
    psp[1].hInstance = g_hinst;
    psp[1].pszTemplate = MAKEINTRESOURCE(DLG_SANIMATION);
    psp[1].pfnDlgProc = PanelDlgProc;
    psp[1].pszTitle = "Animation";
	psp[1].lParam = SiphonExporter::EXPORT_ANIMATION;
    psp[1].pfnCallback = NULL;

    psh.dwSize = sizeof(PROPSHEETHEADER);
    psh.dwFlags = PSH_PROPSHEETPAGE | PSH_USECALLBACK | PSH_NOAPPLYNOW;
    psh.hwndParent = hwndOwner;
    psh.hInstance = g_hinst;

    psh.pszCaption = (LPSTR) "Siphon Exporter";
    psh.nPages = sizeof(psp) / sizeof(PROPSHEETPAGE);
    psh.nStartPage = 0;
    psh.ppsp = (LPCPROPSHEETPAGE) &psp;
	psh.pfnCallback = PropSheetProc;

    PropertySheet(&psh);
}

BOOL CALLBACK SiphonExporterOptionsDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	static SiphonExporter *imp = NULL;
	static HWND g_hwndTab;
	static HWND g_hwndDisplay;
	static HINSTANCE g_hinst = (HINSTANCE)GetWindowLong(hWnd,GWL_HINSTANCE);
	static bool bInit = false;

	switch(message)
	{
		case WM_INITDIALOG:
		{
			sImp = (SiphonExporter *)lParam;
			imp = (SiphonExporter *)lParam;
			CenterWindow(hWnd,GetParent(hWnd));

			DoPropertySheet(hWnd);
			EndDialog(hWnd,0);
			return TRUE;
		}
		case WM_COMMAND:
		{
			switch( LOWORD(wParam) )
			{
				case IDC_CANCEL:
					EndDialog(hWnd,1);
					return TRUE;
					break;
				case IDC_EXPORT:
					EndDialog(hWnd,0);
					break;
			}
		}
		break;
		case WM_CLOSE:
			EndDialog(hWnd, 1);
			return TRUE;
	}
	return FALSE;
}

//--- SiphonExporter -------------------------------------------------------
SiphonExporter::SiphonExporter()// : exportType(0)
{

}

SiphonExporter::~SiphonExporter()
{

}

int SiphonExporter::ExtCount()
{
	//TODO: Returns the number of file name extensions supported by the plug-in.
	return 3;
}

const TCHAR *SiphonExporter::Ext(int n)
{
	//TODO: Return the 'i-th' file name extension (i.e. "3DS").
	switch( n )
	{
	case 0:
		return _T("mdl");
		break;
	case 1:
		return _T("SST");
		break;
	case 2:
		return _T("SMP");
		break;
	}
	return _T("");
}

const TCHAR *SiphonExporter::LongDesc()
{
	//TODO: Return long ASCII description (i.e. "Targa 2.0 Image File")
	return _T("Siphon Exporter");
}

const TCHAR *SiphonExporter::ShortDesc()
{
	//TODO: Return short ASCII description (i.e. "Targa")
	return _T("Siphon");
}

const TCHAR *SiphonExporter::AuthorName()
{
	//TODO: Return ASCII Author name
	return _T("Fabian Mathews");
}

const TCHAR *SiphonExporter::CopyrightMessage()
{
	// Return ASCII Copyright message
	return _T("");
}

const TCHAR *SiphonExporter::OtherMessage1()
{
	//TODO: Return Other message #1 if any
	return _T("");
}

const TCHAR *SiphonExporter::OtherMessage2()
{
	//TODO: Return other message #2 in any
	return _T("");
}

unsigned int SiphonExporter::Version()
{
	//TODO: Return Version number * 100 (i.e. v3.01 = 301)
	return 100;
}

void SiphonExporter::ShowAbout(HWND hWnd)
{
	// Optional
}

BOOL SiphonExporter::SupportsOptions(int ext, DWORD options)
{
	// TODO Decide which options to support.  Simply return
	// true for each option supported by each Extension
	// the exporter supports.

	return TRUE;
}

TSTR SiphonExporter::GetCfgFilename()
{
	TSTR filename;

	filename += GetCOREInterface()->GetDir(APP_PLUGCFG_DIR);
	filename += "\\";
	filename += "SiphonExporter.cfg";

	return filename;
}

bool SiphonExporter::ReadConfig()
{
//	FILE* pFile = fopen( GetCfgFilename(), "rb" );

//	fclose( pFile );
	return true;
}

bool SiphonExporter::WriteConfig()
{
//	FILE* pFile = fopen( GetCfgFilename(), "wb" );

//	fclose( pFile );
	return true;
}

// Dummy function for progress bar
DWORD WINAPI fn(LPVOID arg)
{
	return(0);
}

int	SiphonExporter::DoExport(const TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts, DWORD options)
{
	export = true;
	Interface* ip = GetCOREInterface();

	UserCoord Whacky = {
		1,	//Right Handed
		1,	//X axis goes right
		4,	//Y Axis goes in
		3,	//Z Axis goes down.
		0,	//U Tex axis is left
		1,  //V Tex axis is Down
	};

	exportSelected = (options & SCENE_EXPORT_SELECTED) ? true : false;

	ReadConfig();

	if(!suppressPrompts)
	{
		if( DialogBoxParam(hInstance,
				MAKEINTRESOURCE(IDD_PANEL),
				GetActiveWindow(),
				SiphonExporterOptionsDlgProc, (LPARAM)this) )
				return TRUE;

		if (!export)
			return TRUE;
	}

	scene = GetIGameInterface();
	ip->ProgressStart(_T("Processing Nodes"), TRUE, fn, NULL);

	IGameConversionManager * cm = GetConversionManager();
	cm->SetUserCoordSystem(Whacky);
	cm->SetCoordSystem( IGameConversionManager::IGAME_OGL );
	scene->InitialiseIGame( exportSelected );

	if (exportType == 0)
	{
		MessageBox(0, "No exporter created", "Error", MB_OK);
		return FALSE;
	}

	exportType->Initilize(scene, (TCHAR*)name, ip);

	if (scene->GetTopLevelNodeCount() <= 0)
		return TRUE;

	int nodePct = 100 / scene->GetTopLevelNodeCount();
	int percent = 0;

	for( int loop = 0; loop < scene->GetTopLevelNodeCount();loop++ )
	{
		ip->ProgressUpdate(percent);
		percent += nodePct;

		IGameNode* pNode = scene->GetTopLevelNode(loop);
		exportType->ProcessNode(pNode);
	}
	ip->ProgressEnd();

	ip->ProgressStart(_T("Final Processing"), TRUE, fn, NULL);
	if (exportType->ProcessScene() == true)
	{
		ip->ProgressEnd();

		ip->ProgressStart(_T("Saving"), TRUE, fn, NULL);
		exportType->SaveScene();
	}
	ip->ProgressEnd();

	exportType->Shutdown();

	scene->ReleaseIGame();

	WriteConfig();
	return TRUE;
}


