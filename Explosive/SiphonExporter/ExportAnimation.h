#ifndef _SIPHON_EXPORTANIMATION_
#define _SIPHON_EXPORTANIMATION_

#include <vector>
#include <list>
#include <map>
#include <set>

#include "ExportBase.h"

class ExportAnimation : public ExportBase
{
public:
	virtual void Initilize( IGameScene* scene, TCHAR *name, Interface* ip );

	// clean up code
	virtual void Shutdown();

	// add this node to the scene
	virtual bool ProcessNode( IGameNode* pNode );

	// add a new mesh to the scene
	virtual bool ProcessMesh(IGameNode* node);

	// process any object we need to
	virtual bool ProcessScene();

	virtual bool SaveScene();

	virtual void InitDialog(HWND panel);
	virtual void UpdateDialog();
/*
protected:
//	ExpAnimation animation;
	std::vector<ExpObject*> objects;
*/
	IGameScene* scene;
	FILE* pFile;
	TCHAR* fileName;
	Interface* ip;
};

#endif