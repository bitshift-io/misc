
#include "SiphonExporter.h"

ExpMaterial::ExpMaterial(IGameMaterial* pGameMaterial) : Material(), pGameMaterial(pGameMaterial)
{
	TCHAR* matName = ""; 
	TCHAR* matTex = "";

	if (!pGameMaterial)
		return;

	if (pGameMaterial->GetNumberOfTextureMaps())
		matName = FixPath( pGameMaterial->GetIGameTextureMap(0)->GetBitmapFileName(), "mat" );
	else
		matName = pGameMaterial->GetMaterialName();

	name = std::string(matName);
	
	if (!pGameMaterial->GetNumberOfTextureMaps())
		name += ".mat";
}

bool ExpMaterial::Save(const char* file)
{
	if (!pGameMaterial)
		return false;
/*
	TCHAR* matName = "null.dds"; 
	TCHAR* matTex = "";

	char outpath[256];
	strcpy(outpath, file);
	strcat(outpath, this->name.c_str());
	
	CPass& pass = AddPass();
	
	for (int i = 0; i < pGameMaterial->GetNumberOfTextureMaps(); ++i)
	{
		Material::Pass::TextureParams& texParams = pass.AddTextureParams();
		texParams.texture = new Texture(FixPath( pGameMaterial->GetIGameTextureMap(i)->GetBitmapFileName(), "dds" ));
	}

	Point3 diffuse;
	pGameMaterial->GetDiffuseData()->GetPropertyValue(diffuse);
	pass.diffuse = Vector3(diffuse.x, diffuse.y, diffuse.z);

	Point3 specular;
	pGameMaterial->GetSpecularData()->GetPropertyValue(specular);
	pass.specular = Vector3(specular.x, specular.y, specular.z);

	float shine;
	pGameMaterial->GetGlossinessData()->GetPropertyValue(shine);
	pass.shine = shine;

	Point3 ambient;
	pGameMaterial->GetAmbientData()->GetPropertyValue(ambient);
	pass.ambient = Vector3(ambient.x, ambient.y, ambient.z);

	Material::Save(outpath);*/
	return true;
}

char* ExpMaterial::FixPath( const char* const bmpName, char ext[3] )
{
	TCHAR* matName = "null";
	int size;
	bool bDot = false;
	int d = strlen( bmpName );
	int i = 0;
	for( i = strlen( bmpName ); i >= 0; --i )
	{
		if( bmpName[i] == '\\' )
		{
			++i;
			break;
		}

		if( bmpName[d] == '.' )
			bDot = true;

		if( bDot == false )
			--d;
	}

	size = d - i;
	//now copy into matName
	matName = new TCHAR[ size + 5 ];
	int l = 0;
	for( int k = i; k < d; ++k, ++l )
	{
		matName[l] = bmpName[k];
	}

	//crop of the .bmp or what ever...
	matName[ size + 0 ] = '.';
	for( int e = 1; e < 4; ++e )
		matName[ size + e ] = ext[e-1];

	matName[ size + 4 ] = '\0';

	return matName;
}