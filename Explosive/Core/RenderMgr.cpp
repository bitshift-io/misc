#include "RenderMgr.h"
#include "Log.h"

#ifdef WIN32
	#include "Win32Time.h"
#else
	#include "LinuxTime.h"
#endif

using namespace Siphon;

RenderMgr::RenderMgr() : polys(0), pauseTime(false)
{
	font = FontMgr::Load("arial.fnt");
}

RenderMgr::~RenderMgr()
{

}

void RenderMgr::Update()
{
	camera.Update();
	font->Update();

	if (!pauseTime)
	{
		float time = Time::GetInstance().GetTimeSeconds();
		for (int i = 0; i < renderers.GetSize(); ++i)
		{
			Renderer* renderer = renderers[i];
			renderer->SetTime(time);
		}
	}
}

int RenderMgr::AddRenderer(Camera* camera)
{
	Renderer* renderer = new Renderer();
	renderer->SetCamera(camera);
	return renderers.Insert(renderer);
	return 0;
}

void RenderMgr::Render()
{
	polys = 0;

	for (int i = 0; i < renderers.GetSize(); ++i)
	{
		this->polys = renderers[i]->Render();
	}

	// render font
	font->Render();
}