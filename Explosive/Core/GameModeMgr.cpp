#include "GameModeMgr.h"

bool GameMode::PlayerKilled(Character* killer, Character* killed)
{
	return killed->Kill(killer);
}

/*
GameModeMgr::GameModeMgr() : currentGameMode(NULL)
{

}

GameMode* GameModeMgr::GetCurrentGameMode()
{
	return currentGameMode;
}

void GameModeMgr::SetActiveGameMode(GameMode* gameMode)
{
	currentGameMode = gameMode;
}

int GameModeMgr::GetNumGameModes()
{
	return gameModes.GetSize();
}

GameMode* GameModeMgr::GetGameMode(int index)
{
	return gameModes[index];
}

void GameModeMgr::AddGameMode(GameMode* gameMode)
{
	if (!currentGameMode)
		currentGameMode = gameMode;

	gameModes.Insert(gameMode);
}

void GameModeMgr::RemoveGameMode(GameMode* gameMode)
{
	// remove
}

void GameModeMgr::Update()
{
	currentGameMode->Update();
}*/