#ifndef _SIPHON_CHARACTER_
#define _SIPHON_CHARACTER_

#include "../Engine/Mesh.h"

#include "ActorMgr.h"

using namespace Siphon;

/**
 * Controller for an object
 * the idea is to have input, network, and ai controllers
 */
class Controller
{
public:
	virtual ~Controller() { }

	// pass in the object being controlled
	virtual void Update(Actor* object) = 0;

	// this allows for multiple controller types
	//virtual int GetContollerType() = 0;

	// this should be used to query the state
	// ie. state moving fowards
	// states players/ai/network uses have control over
	//virtual bool GetState(int state) = 0;
protected:

};

/**
 * Generic character base class
 */
class Character : public Actor
{
public:
	Character();
	Character(Controller* controller, const char* skinFile);
	virtual ~Character();

	virtual bool Update();

	virtual void TakeDamage(int damage, Actor* actor);
	virtual void Respawn() = 0;
	void IncrementScore(int score);
	virtual void CheckInput() = 0;
	virtual void Reset();
	bool IsDead();

	virtual bool Kill(Actor* actor);

	virtual void DebugDraw();

	Vector3 GetPosition() const { return skin->GetTransform().GetTranslation(); }

	Controller* GetController() { return controller; }
	void SetController(Controller* control) { controller = control; }

protected:
	int health;
	int score;

	Mesh* skin;
	Controller* controller;
};

#endif



