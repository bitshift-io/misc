#include "Team.h"

void Team::AddTeamMember(Character* member)
{
	members.Insert(member);
}

void Team::RemoveTeamMember(Character* member)
{
	// remove
}

Character* Team::GetMember(int index) const
{
	return members[index];
}

int Team::GetNumMembers() const
{
	return members.GetSize();
}
