#ifndef _SIPHON_PICKUP_
#define _SIPHON_PICKUP_

#include "../Engine/Mesh.h"

#include "ActorMgr.h"
#include "Character.h"

using namespace Siphon;

/**
 * Generic class for pickups to extend
 */
class PickUp : public Actor
{
public:
	PickUp(const char* meshFile);
	~PickUp();

	virtual bool GiveToPlayer(Character* player) = 0;
	virtual void TakeDamage(int damage, Actor* actor) { }

	Mesh* GetMesh() { return mesh; }
protected:
	Mesh* mesh;
};

#endif
