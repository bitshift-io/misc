#ifndef _SIPHON_NETSERVER_
#define _SIPHON_NETSERVER_

#include "../Engine/Server.h"
#include "../Util/Singleton.h"
#include "NetObject.h"

class NetServer : public ServerBase, public Siphon::Singleton<NetServer>, public NetExtension
{
public:
	class Callback
	{
	public:
		virtual void AcceptConnection(int index, Connection& client) = 0;
		virtual void ClientDisconnect(int index, Connection& client) = 0;
	};

	NetServer();
	virtual ~NetServer();

	virtual bool Listen(int port, int flags = 0, const char* name = 0);

	// set the first number available as a networkId,
	// use numbers efore this range as network singletons
	virtual bool Init(unsigned int gameId, unsigned int baseNetworkId, const char* masterServer);

	virtual unsigned int	RegisterNetObject(NetObject* obj, bool createOverNetwork, bool assignNetworkId);
	virtual void			UnregisterNetObject(NetObject* obj);

	// send a message from a network object on this machine to another machine
	virtual void SendNetObjectMessage(unsigned int networkId, const void* data, int size, NetMessageTarget target = All, SocketStream::Type type = SocketStream::Reliable);
	virtual void SendNetObjectMessage(int index, unsigned int networkId, const void* data, int size, SocketStream::Type type = SocketStream::Reliable);
	virtual void SendNetObjectMessageIgnoreClient(int index, unsigned int networkId, const void* data, int size, SocketStream::Type type = SocketStream::Reliable);

	virtual void Update();

	virtual void SetCallback(Callback* callback) { this->callback = callback; }

	virtual void CheckConnections();

	virtual void ClientDisconnect(int index, Connection& client);

	void		FlushUnreliable();

protected:

	virtual void AcceptConnection(int index, Connection& client);
	
	bool CompleteMessageInBuffer(int index, SocketStream::Type type);

	unsigned int	networkCounter;
	Callback*		callback;

	int masterServerPort;
	std::string masterServer;

	SocketStream	masterServerConnection;
};

#endif