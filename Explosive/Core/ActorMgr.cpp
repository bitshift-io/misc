#include "ActorMgr.h"

ActorMgr::ActorMgr() : locked(false)
{

}

void ActorMgr::Init(int numGroups)
{
	actors.SetCapacity(numGroups);

	for (int i = 0; i < numGroups; ++i)
	{
		actors.Insert(Array<Actor*>());
	}
}

void ActorMgr::AddActor(Actor* actor, int group)
{/*
	// if this group doesn't exist, create it
	if (group >= GetNumActorGroups())
	{
		// insert a new array
		actors.Insert(Array<Actor*>());
	}*/

	// check actor doesnt already exist

	// add actor
	Array<Actor*>& actorGroup = actors[group];
	actorGroup.Insert(actor);
}

void ActorMgr::RemoveActor(Actor* actor, int group, bool ignoreLock)
{
	if (!ignoreLock && locked)
		return;

	// remove actor
	Array<Actor*>& actorGroup = actors[group];
	for (int a = 0; a < actorGroup.GetSize(); ++a)
	{
		if (actorGroup[a] == actor)
		{
			// remove it, this should probably be a linked list :-/
			actorGroup.Remove(a);
		}
	}
}

void ActorMgr::Update()
{
	locked = true;

	for (int g = 0; g < GetNumActorGroups(); ++g)
	{
		Array<Actor*>& actorGroup = actors[g];

		for (int a = 0; a < actorGroup.GetSize(); ++a)
		{
			Actor* actor = actorGroup[a];
			if (actor->Update() == false)
			{
				actorGroup.Remove(a);
				delete actor;
				--a;
			}
		}
	}

	locked = false;
}

void ActorMgr::DebugDraw()
{
	for (int g = 0; g < GetNumActorGroups(); ++g)
	{
		Array<Actor*>& actorGroup = actors[g];

		for (int a = 0; a < actorGroup.GetSize(); ++a)
		{
			Actor* actor = actorGroup[a];
			actor->DebugDraw();
		}
	}
}

int ActorMgr::GetNumActorGroups()
{
	return actors.GetSize();
}

Array<Actor*>& ActorMgr::GetActorGroup(int group)
{
	return actors[group];
}