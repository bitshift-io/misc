#ifndef _SIPHON_RENDERMGR_
#define _SIPHON_RENDERMGR_

#include "../Util/Array.h"
#include "../Engine/Renderer.h"
#include "../Engine/Camera.h"
#include "../Engine/Font.h"

using namespace Siphon;

// this should be a template class
class RenderMgr : public Siphon::Singleton<RenderMgr>
{
public:
	RenderMgr();
	~RenderMgr();

	int AddRenderer(Camera* camera = 0);

	int GetNumRenderers() { return renderers.GetSize(); }
	Renderer* GetRenderer(int index) { return renderers[index]; }

	void Update();
	void Render();

	Camera* GetDefaultCamera() { return &camera; }
	Font* GetFont() { return font; }
	unsigned int GetPolyCount() { return polys; }

	void SetPauseTime(bool pauseTime) { this->pauseTime = pauseTime; }

protected:
	unsigned int polys;
	Font* font;
	Array<Renderer*> renderers;	
	Camera camera;
	bool pauseTime;
};

#endif