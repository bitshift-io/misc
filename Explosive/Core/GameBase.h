//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_GAMEBASE_
#define _SIPHON_GAMEBASE_

#include "../Util/Singleton.h"
#include "../Util/Log.h"
#include "../Engine/Device.h"
#include "../Engine/Texture.h"
#include "../Engine/Material.h"
#include "../Engine/Mesh.h"

#ifdef WIN32
    #include "../Engine/Win32Input.h"
    #include "../Engine/Win32Window.h"
	#include "../Engine/Win32Time.h"
#else
    #include "LinuxInput.h"
    #include "LinuxWindow.h"
	#include "LinuxTime.h"
#endif

#define MAIN(startClass)								\
int main(int argc, char **argv)							\
{														\
	startClass::Create();								\
														\
	if (startClass::GetInstance().Init(argc, argv))		\
		startClass::GetInstance().Run();				\
														\
	startClass::Destroy();								\
	return 0;											\
}

using namespace Siphon;

/*
 * Extend this class, to get
 * updates etcc for you
 */
template <class T>
class GameBase : public Siphon::Singleton<T>
{
public:
	GameBase();
	~GameBase();

	virtual bool Init(int argc, char **argv);
	virtual void Shutdown();

	virtual void SetBasePath(const char* path);

	// returning false ends the application
	virtual bool UpdateGame() { return true; }
	virtual void UpdatePhysics() { }

	virtual void Render() { }

	virtual void Run();
protected:

};

template <class T>
GameBase<T>::GameBase()
{

}

template <class T>
GameBase<T>::~GameBase()
{

}

template <class T>
void GameBase<T>::Shutdown()
{
    Siphon::Time::Destroy();
	Device::Destroy();
	Siphon::Time::Destroy();
	Log::Close();
}

template <class T>
bool GameBase<T>::Init(int argc, char **argv)
{
	char basePath[256] = "data/";
	if (argc >= 2)
	{
		sprintf(basePath, "%s/", argv[1]);

		TextureMgr::SetBasePath(basePath);
		MaterialMgr::SetBasePath(basePath);
		EffectMgr::SetBasePath(basePath);
		Mesh::SetBasePath(basePath);
	}

	Siphon::Time::Create();
	Input::Create();
	Device::Create();

	Siphon::Time::GetInstance().SetUpdateRate(Siphon::Time::UPDATE_PHYSICS, 60);
	return true;
}

template <class T>
void GameBase<T>::Run()
{
	Input::GetInstance().Update();
	Siphon::Time::GetInstance().Update();

	int i;
	bool playing = true;
	while (playing)
	{
		Siphon::Time::GetInstance().Update();

		for (i = 0; i < Siphon::Time::GetInstance().GetNumUpdates(Siphon::Time::UPDATE_GAME); i++)
		{
		    // Log::Print("updating game\n");
			playing = UpdateGame();

			if (playing == false)
                break;
		}

		for (i = 0; i < Siphon::Time::GetInstance().GetNumUpdates(Siphon::Time::UPDATE_PHYSICS); i++)
			UpdatePhysics();

		Render();
	}

	Shutdown();
}

template <class T>
void GameBase<T>::SetBasePath(const char* path)
{
	TextureMgr::SetBasePath(path);
	MaterialMgr::SetBasePath(path);
	EffectMgr::SetBasePath(path);
}

#endif
