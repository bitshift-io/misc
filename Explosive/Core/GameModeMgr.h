#ifndef _SIPHON_GAMEMODE_
#define _SIPHON_GAMEMODE_

#include "GameBase.h"
#include "Team.h"

class Objective
{
public:

protected:

};

class GameMode
{
public:
	// the name of the game mode
	virtual const char* GetName() = 0;

	// get the description
	virtual const char* GetDescription() = 0;

	virtual void Reset() = 0;
	virtual void Update() = 0;
	virtual bool PlayerKilled(Character* killer, Character* killed);

	virtual void AddPlayer(Character* player) { }
	virtual void RemovePlayer(Character* player) { }

	virtual bool IsTeamGame() = 0;

protected:
	Array<Team*> teams;
};

/**
 * you should extends this, and your game GUI
 * should access this class to set up the game
 */
template<class T>
class GameMgr : public GameBase<T>
{
public:
	enum GameState
	{
		GameNotStarted,
		GamePaused,
		GamePlaying,
		GameEndGame,		// in a state when the game is over, but actors are all valid
		GameRestart,
		GameEndRound,
		GameResume,
		GameUnknown
	};

	enum MenuState
	{
		Visible,
		Hidden
	};

	GameMgr() : currentGameMode(NULL), gameState(GameNotStarted) { }

	GameMode* GetCurrentGameMode()
	{
		return currentGameMode;
	}

	virtual void SetActiveGameMode(GameMode* gameMode)
	{
		currentGameMode = gameMode;
	}

	int GetNumGameModes()
	{
		return gameModes.GetSize();
	}

	GameMode* GetGameMode(int index)
	{
		return gameModes[index];
	}

	void AddGameMode(GameMode* gameMode)
	{
		if (!currentGameMode)
			currentGameMode = gameMode;

		gameModes.Insert(gameMode);
	}

	void RemoveGameMode(GameMode* gameMode)
	{
		// remove
	}

	//virtual void Render() = 0;

	//virtual void Update() = 0;
	/*{
		currentGameMode->Update();
	}*/

	virtual Array<Character*>& GetCharacters()
	{
		return players;
	}

	virtual void AddCharacter(Character* player)
	{
		players.Insert(player);
	}

	virtual void RemoveCharacter(Character* player)
	{
		players.Remove(player);
	}

	virtual void Start() = 0;
	virtual void Restart() = 0;
	virtual void Pause() = 0;
	virtual void Resume() = 0;
	virtual void EndGame() = 0;
	virtual void EndRound() = 0;

	GameState GetGameState() { return gameState; }

protected:

	void SetGameState(GameState state) { gameState = state; }

	GameState			gameState;
	GameMode*			currentGameMode;
	Array<GameMode*>	gameModes;
	Array<Character*>	players;
};

#endif