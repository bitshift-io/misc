#include "Character.h"

#define max(a, b) a > b ? a : b

Character::Character() : controller(NULL), skin(NULL)
{

}

Character::Character(Controller* controller, const char* skinFile) : controller(controller)
{
	skin = Mesh::LoadMesh(skinFile); // just till exporter works
	skin->SetBoundSphere(Sphere(50, Vector3(0,0,0)));
}

Character::~Character()
{
	Mesh::ReleaseMesh(skin);
}

void Character::Reset()
{
	health = 100;
}

bool Character::IsDead()
{
	return (health == 0) ? true : false;
}

void Character::DebugDraw()
{
	skin->GetBoundSphere().Draw();
}

bool Character::Update()
{
	if (IsDead())
		return true; // dont destroy this actor!

	controller->Update(this);
	CheckInput();
	return true;
}

void Character::TakeDamage(int damage, Actor* actor)
{
	health -= damage;
	
	if (health <= 0)
		Kill(actor);
}

bool Character::Kill(Actor* actor)
{
	health = 0;
	return true;
}

void Character::IncrementScore(int score)
{
	this->score += score;
}
