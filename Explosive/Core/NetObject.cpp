#include "NetObject.h"
#include "NetClient.h"
#include "NetServer.h"

Array<NetObject*> NetExtension::netObjects;

//********************************************************

NetExtension::~NetExtension()
{
	// clean up any network objects we didn't create
}

NetObject* NetExtension::GetNetObject(unsigned int uid)
{
	for (int i = 0; i < netObjects.GetSize(); ++i)
	{
		if (netObjects[i]->GetNetworkId() == uid)
			return netObjects[i];
	}

	return 0;
}

unsigned int NetExtension::RegisterNetObject(NetObject* obj, bool createOverNetwork, bool assignNetworkId)
{
	for (int i = 0; i < netObjects.GetSize(); ++i)
	{
		if (netObjects[i] == obj)
			return i;
	}

	return netObjects.Insert(obj);
}

void NetExtension::UnregisterNetObject(NetObject* obj)
{
	netObjects.Remove(obj);
}

void NetExtension::ReceiveNetObjectMessage(unsigned int networkId, const void* data, int size, NetMessageTarget target)
{
	// we could do a binary search
	for (int i = 0; i < netObjects.GetSize(); ++i)
	{
		NetObject* nObj = netObjects[i];
		if (nObj->GetNetworkId() == networkId)
		{
			nObj->ReceiveMessage(data, size, target);
			break;
		}
	}
}

void NetExtension::Clear()
{
	while (netObjects.GetSize())
		netObjects.Remove(0);
}

//********************************************************

NetObject::NetObject() :
	networkId(-1),
	enableSend(true)
{

}

NetObject::~NetObject()
{
	if (NetServer::IsValid())
		NetServer::GetInstance().UnregisterNetObject(this);
	else if (NetClient::IsValid())
		NetClient::GetInstance().UnregisterNetObject(this);
}

bool NetObject::FilterTarget(NetExtension::NetMessageTarget target)
{
	if (target == NetExtension::All)
		return false;

	if (target == NetExtension::Server)
	{
		if (NetServer::IsValid())
			return false;
		else
			return true;
	}

	if (target == NetExtension::Owner)
		return !IsOwner();

	return true;
}

void NetObject::SendToAllButClient(const void* data, unsigned int size, int clientIdx, SocketStream::Type type)
{
	if (networkId == -1 || !enableSend || !NetServer::IsValid())
		return;

	NetServer::GetInstance().SendNetObjectMessageIgnoreClient(clientIdx, GetNetworkId(), data, size, type);
}

void NetObject::SendToClient(const void* data, unsigned int size, int clientIdx, SocketStream::Type type)
{
	if (networkId == -1 || !enableSend || !NetServer::IsValid())
		return;

	NetServer::GetInstance().SendNetObjectMessage(clientIdx, GetNetworkId(), data, size, type);
}

void NetObject::Send(const void* data, unsigned int size, NetExtension::NetMessageTarget target, SocketStream::Type type)
{
	if (networkId == -1 || !enableSend)
		return;

	if (NetServer::IsValid())
		NetServer::GetInstance().SendNetObjectMessage(GetNetworkId(), data, size, target, type);
	else if (NetClient::IsValid())
		NetClient::GetInstance().SendNetObjectMessage(GetNetworkId(), data, size, target, type);
}

bool NetObject::IsOwner()
{
	if (NetServer::IsValid())
		return GetOwnerClientId() == NetServer::GetInstance().GetClientId();
	else if (NetClient::IsValid())
		return GetOwnerClientId() == NetClient::GetInstance().GetClientId();

	return false;
}

//********************************************************

Array<NetType*> NetFactory::netTypes(2);

unsigned int NetFactory::Register(NetType* netType) 
{ 
	return netTypes.Insert(netType); 
}

NetObject* NetFactory::Create(unsigned int ownerClientId, unsigned int type, unsigned int networkId, unsigned int size, char* data) 
{ 
	return netTypes[type]->Create(ownerClientId, networkId, size, data); 
}