#ifndef _SIPHON_TEAM_
#define _SIPHON_TEAM_

#include "Character.h"

class Team
{
public:
	void AddTeamMember(Character* member);
	void RemoveTeamMember(Character* member);

	Character* GetMember(int index) const;
	int GetNumMembers() const;

protected:
	Array<Character*> members;
};

#endif