#ifndef _SIPHON_ACTOR_
#define _SIPHON_ACTOR_
	
#include "../Util/Singleton.h"
#include "../Util/Array.h"

#include "../Engine/Math3D.h"

using namespace Siphon;

#define TYPE(t) enum { Type = t }; virtual int GetType() const { return Type; }
//#define TYPE(c, t) enum { c#Type = t }; virtual int Get#c#Type() const { return c#Type; }

class Actor
{
public:
	virtual ~Actor() { }

	// return false to destroy this class, and remove it from the mgr
	virtual bool Update() = 0;
	//const char* GetName() { return name; }
	virtual void DebugDraw() { }

	virtual int GetType() const { return -1; }

protected:
	//const char* name;
};

class ActorMgr : public Siphon::Singleton<ActorMgr>
{
public:

	ActorMgr();

	void Init(int numGroups);

	void DebugDraw();
	void Update();
	void GetActorsInRadius(Array<Actor*>& actors, Sphere& sphere, int group = 0);
	Actor* GetActorByName(const char* name, int group = 0);
	void AddActor(Actor* actor, int group = 0);
	void RemoveActor(Actor* actor, int group = 0, bool ignoreLock = false);

	int GetNumActorGroups();
	Array<Actor*>& GetActorGroup(int group = 0);

protected:
	Array<Array<Actor*> > actors; // arrays of actors (groups of them)
	bool	locked;
};

#endif