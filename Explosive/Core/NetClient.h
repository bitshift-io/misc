#ifndef _SIPHON_NETCLIENT_
#define _SIPHON_NETCLIENT_

#include "../Engine/Client.h"
#include "../Util/Singleton.h"
#include "NetObject.h"

class NetClient : public ClientBase, public Siphon::Singleton<NetClient>, public NetExtension
{
public:
	class Callback
	{
	public:
		virtual void AcceptConnection(int flags) = 0;
		virtual void ClientDisconnect() = 0;
	};

	NetClient() : callback(0) { }

	virtual bool Init(unsigned int gameId);

	virtual unsigned int	RegisterNetObject(NetObject* obj, bool createOverNetwork, bool assignNetworkId);
	virtual void			UnregisterNetObject(NetObject* obj);

	// send a message from a network object on this machine to another machine
	virtual void SendNetObjectMessage(unsigned int networkId, const void* data, int size, NetMessageTarget target = All, SocketStream::Type type = SocketStream::Reliable);

	virtual void Update();

	virtual void SetCallback(Callback* callback) { this->callback = callback; }

	void	FlushUnreliable();

protected:
	virtual void AcceptConnection(int flags);
	virtual void ClientDisconnect();

	bool CompleteMessageInBuffer(SocketStream::Type type);

	Callback*		callback;
};

#endif