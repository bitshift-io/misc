#ifndef _SIPHON_NETOBJECT_
#define _SIPHON_NETOBJECT_

#include "../Util/Array.h"
#include "NetworkBase.h"

using namespace Siphon;

class NetObject;

class NetExtension
{
public:
	virtual ~NetExtension();

	enum NetMessageTarget
	{
		Server,
		Owner,
		All
	};

#pragma pack(push,1)
	struct MessageHeader
	{
		unsigned char	messageType;
		unsigned int	size;		// size of the message
	};

	struct NetObjectHeader
	{
		unsigned int		clientId;	//who needs to receive the message
		unsigned int		networkId;	//which netobj is this meant for?
		NetMessageTarget	target;
	};

	struct InternalHeader
	{
		unsigned char message;
	};

	struct InternalCreateNetworkId
	{
		unsigned int	ownerClientId;
		unsigned int	clientId;
		unsigned int	netType;
		unsigned int	networkId;
		unsigned int	origonalIdx;
		unsigned int	dataSize;
		bool			createOverNetwork;
	};

	struct InternalDestroyNetworkId
	{
		unsigned int networkId;
	};
#pragma pack(pop)

	enum MessageType
	{
		MessageNetObject,
		MessageRaw,
		MessageInternal
	};

	enum InternalMessage
	{
		InternalMessageCreateNetworkId,
		InternalMessageDestroyNetworkId
	};

	virtual unsigned int	RegisterNetObject(NetObject* obj, bool createOverNetwork, bool assignNetworkId);
	virtual void			UnregisterNetObject(NetObject* obj);

	virtual void			Clear();

	// send a message from a network object on this machine to another machine
	virtual void SendNetObjectMessage(unsigned int networkId, const void* data, int size, NetMessageTarget target = All, SocketStream::Type type = SocketStream::Reliable) = 0;
	virtual void ReceiveNetObjectMessage(unsigned int networkId, const void* data, int size, NetMessageTarget target);

	
	NetObject*				GetNetObject(unsigned int uid);

	virtual void Update() = 0;

protected:
	static Array<NetObject*> netObjects;
};


class NetType;

class NetFactory
{
public:
	static unsigned int		Register(NetType* netType);
	static NetObject*		Create(unsigned int networkClientId, unsigned int type, unsigned int networkId, unsigned int size, char* data);

protected:
	static Array<NetType*> netTypes;
};

class NetType
{
public:
	NetType() { type = NetFactory::Register(this); }
	unsigned int GetType() { return type; }
	virtual NetObject* Create(unsigned int networkClientId, unsigned int networkId, unsigned int size, char* data) = 0;

protected:
	unsigned int type;
};

class NetObject
{
public:
	NetObject();
	virtual ~NetObject();

	// methods for sending constructor data
	virtual unsigned int GetConstructorDataSize() = 0;
	virtual void GetConstructorData(char* data) = 0;

	virtual void Send(const void* data, unsigned int size, NetExtension::NetMessageTarget target = NetExtension::All, SocketStream::Type type = SocketStream::Reliable);
	virtual void SendToClient(const void* data, unsigned int size, int clientIdx, SocketStream::Type type = SocketStream::Reliable);
	virtual void SendToAllButClient(const void* data, unsigned int size, int clientIdx, SocketStream::Type type = SocketStream::Reliable);

	virtual void ReceiveMessage(const void* message, int length, NetExtension::NetMessageTarget target) = 0;

	// determines if we should parse this message
	// if true this message is not for us
	bool FilterTarget(NetExtension::NetMessageTarget target);

	bool IsBlocking() { return !enableSend; }
	void BlockSendBegin() { enableSend = false; }
	void BlockSendEnd() { enableSend = true; }

	virtual void SetOwnerClientId(unsigned int clientId) { ownerClientId = clientId; }
	unsigned int GetOwnerClientId() { return ownerClientId; }

	virtual void SetNetworkId(unsigned int netId) { networkId = netId; }
	unsigned int GetNetworkId() { return networkId; }

	virtual NetType* GetNetType() = 0;

	bool IsValid() { return networkId != -1; }

	// do we own this network object?
	bool IsOwner();

	// used internally, by server and client
	virtual void SetAssignNetworkId(bool assignNetId) { assignNetworkId = assignNetId; }
	unsigned int GetAssignNetworkId() { return assignNetworkId; }

	virtual void SetCreateOverNetwork(bool createOverNet) { createOverNetwork = createOverNet; }
	unsigned int GetCreateOverNetwork() { return createOverNetwork; }

	unsigned int	tempIndex;

protected:
	unsigned int	ownerClientId;
	unsigned int	networkId;
	bool			enableSend;
	bool			assignNetworkId;
	bool			createOverNetwork;
};

// when you create an object, sometimes you dont want it to create it self
// you just want it to link up to an existing class, so pass false
#define RegisterWithNetwork(createOverNetwork, assignNetworkId)								\
if (NetServer::IsValid())																	\
	NetServer::GetInstance().RegisterNetObject(this, createOverNetwork, assignNetworkId);	\
else if (NetClient::IsValid())																\
	NetClient::GetInstance().RegisterNetObject(this, createOverNetwork, assignNetworkId);

#define UnregisterWithNetwork()																\
if (NetServer::IsValid())																	\
	NetServer::GetInstance().UnregisterNetObject(this);										\
else if (NetClient::IsValid())																\
	NetClient::GetInstance().UnregisterNetObject(this);

#endif