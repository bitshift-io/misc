#include "NetClient.h"
#include "../Network/Packet.h"

bool NetClient::Init(unsigned int gameId)
{
	Clear();
	return ClientBase::Init(gameId);
}

unsigned int NetClient::RegisterNetObject(NetObject* obj, bool createOverNetwork, bool assignNetworkId)
{
	unsigned int idx = NetExtension::RegisterNetObject(obj, createOverNetwork, assignNetworkId);

	obj->SetCreateOverNetwork(createOverNetwork);
	obj->SetAssignNetworkId(assignNetworkId);

	if (!assignNetworkId)
		return idx;

	unsigned int dataSize = obj->GetConstructorDataSize();

	MessageHeader messageHeader;
	messageHeader.messageType = MessageInternal;
	messageHeader.size = sizeof(InternalHeader) + sizeof(InternalCreateNetworkId) + dataSize;

	InternalHeader internalHeader;
	internalHeader.message = InternalMessageCreateNetworkId;

	obj->tempIndex = idx;

	InternalCreateNetworkId internalCreateNetworkId;
	internalCreateNetworkId.origonalIdx = idx;
	internalCreateNetworkId.clientId = GetClientId();
	internalCreateNetworkId.ownerClientId = GetClientId(); 
	internalCreateNetworkId.netType = obj->GetNetType() ? obj->GetNetType()->GetType() : -1;
	internalCreateNetworkId.dataSize = dataSize;
	internalCreateNetworkId.createOverNetwork = createOverNetwork;

	int finalSize = sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId) + dataSize;
	char* buffer = new char[finalSize];

	memcpy(buffer, &messageHeader, sizeof(MessageHeader));
	memcpy(buffer + sizeof(MessageHeader), &internalHeader, sizeof(InternalHeader));
	memcpy(buffer + sizeof(MessageHeader) + sizeof(InternalHeader), &internalCreateNetworkId, sizeof(InternalCreateNetworkId));
	obj->GetConstructorData(buffer + sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId));

	//cout << "Requesting a netobject id" << endl;

	Send((const char*)buffer, finalSize);
	delete[] buffer;

/*
	int finalSize = sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId) + (createOverNetwork ? dataSize : 0);
	char* buffer = new char[finalSize];

	memcpy(buffer, &messageHeader, sizeof(MessageHeader));
	memcpy(buffer + sizeof(MessageHeader), &internalHeader, sizeof(InternalHeader));
	memcpy(buffer + sizeof(MessageHeader) + sizeof(InternalHeader), internalCreateNetworkId, sizeof(InternalCreateNetworkId));
	
	if (createOverNetwork)
		obj->GetConstructorData(buffer + sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId));

	Send((const char*)buffer, finalSize);
	delete buffer;

	//Send((const char*)&messageHeader, sizeof(MessageHeader));
	//Send((const char*)&internalHeader, sizeof(InternalHeader));
	//Send((const char*)&internalCreateNetworkId, sizeof(InternalCreateNetworkId));
/*
	if (createOverNetwork)
	{
		// send constructor data
		char* buffer = new char[dataSize];
		obj->GetConstructorData(buffer);
		Send((const char*)buffer, dataSize);
		delete[] buffer;
	}*/

	return idx;
}

void NetClient::AcceptConnection(int flags)
{
	ClientBase::AcceptConnection(flags);

	// when we connect, we need to sync the game state, send all objects
	for (unsigned int i = 0; i < netObjects.GetSize(); ++i)
	{
		NetObject* nObj = netObjects[i];

		bool createOverNetwork = nObj->GetCreateOverNetwork();
		bool assignNetworkId = nObj->GetAssignNetworkId();

		if (!assignNetworkId)
			continue;

		unsigned int dataSize = nObj->GetConstructorDataSize();

		MessageHeader messageHeader;
		messageHeader.messageType = MessageInternal;
		messageHeader.size = sizeof(InternalHeader) + sizeof(InternalCreateNetworkId) + dataSize;

		InternalHeader internalHeader;
		internalHeader.message = InternalMessageCreateNetworkId;

		InternalCreateNetworkId internalCreateNetworkId;
		internalCreateNetworkId.origonalIdx = -1;
		internalCreateNetworkId.clientId = GetClientId();
		internalCreateNetworkId.ownerClientId = nObj->GetOwnerClientId(); 
		internalCreateNetworkId.netType = nObj->GetNetType() ? nObj->GetNetType()->GetType() : -1;
		internalCreateNetworkId.dataSize = dataSize;
		internalCreateNetworkId.createOverNetwork = createOverNetwork;


		int finalSize = sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId) + dataSize;
		char* buffer = new char[finalSize];

		memcpy(buffer, &messageHeader, sizeof(MessageHeader));
		memcpy(buffer + sizeof(MessageHeader), &internalHeader, sizeof(InternalHeader));
		memcpy(buffer + sizeof(MessageHeader) + sizeof(InternalHeader), &internalCreateNetworkId, sizeof(InternalCreateNetworkId));
		nObj->GetConstructorData(buffer + sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId));

		Send((const char*)buffer, finalSize);
		delete[] buffer;
/*

		Send(&messageHeader, sizeof(MessageHeader));
		Send(&internalHeader, sizeof(InternalHeader));
		Send(&internalCreateNetworkId, sizeof(InternalCreateNetworkId));

		// send constructor data
		char* buffer = new char[dataSize];
		nObj->GetConstructorData(buffer);
		Send((const char*)buffer, dataSize);
		delete[] buffer;
*/
		// we may need to call a NetObject->Sync method to make sure all net objects are in the same state
	}

	if (callback)
		callback->AcceptConnection(flags);
}

void NetClient::ClientDisconnect()
{
	if (callback)
		callback->ClientDisconnect();
}

void NetClient::UnregisterNetObject(NetObject* obj)
{
	NetExtension::UnregisterNetObject(obj);
}

void NetClient::SendNetObjectMessage(unsigned int networkId, const void* data, int size, NetMessageTarget target, SocketStream::Type type)
{
	MessageHeader messageHeader;
	messageHeader.messageType = MessageNetObject;
	messageHeader.size = sizeof(NetObjectHeader) + size;

	NetObjectHeader netObjectHeader;
	netObjectHeader.networkId = networkId;
	netObjectHeader.clientId = GetClientId();
	netObjectHeader.target = target;

	int finalSize = sizeof(MessageHeader) + sizeof(NetObjectHeader) + size;
	char* buffer = new char[finalSize];

	memcpy(buffer, &messageHeader, sizeof(MessageHeader));
	memcpy(buffer + sizeof(MessageHeader), &netObjectHeader, sizeof(NetObjectHeader));
	memcpy(buffer + sizeof(MessageHeader) + sizeof(NetObjectHeader), data, size);

	if (!Send(buffer, finalSize, type))
	{
		reliable.Close();
		unreliable.Close();
	}

	delete[] buffer;
/*
	Send(&messageHeader, sizeof(MessageHeader), type);
	Send(&netObjectHeader, sizeof(NetObjectHeader), type);
	Send(data, size, type);*/
	
}

bool NetClient::CompleteMessageInBuffer(SocketStream::Type type)
{
	return (stream.GetState(type == SocketStream::Reliable ? reliableHandle : unreliableHandle) & eRead) == eRead;
	/*
	int size = 0;
	switch (type)
	{
	case SocketStream::Reliable:
		size = reliable.GetBufferSize();
		break;
	case SocketStream::UnReliable:
		size = unreliable.GetBufferSize();
		break;
	}

	if (size > sizeof(MessageHeader))
	{
		MessageHeader messageHeader;
		PeekBufferData(&messageHeader, sizeof(MessageHeader), type);

		if (size >= sizeof(MessageHeader) + messageHeader.size && messageHeader.size > 0)
			return true;
		else
			return false;
	}

	return false;*/
}

void NetClient::Update()
{
	/*
	if (!reliable.IsValid())
	{
		ClientDisconnect();
		return;
	}
*/
	ClientBase::Update();

	for (int st = 0; st < 2; ++st)
	{
		SocketStream::Type type = (SocketStream::Type)st;

		while (CompleteMessageInBuffer(type))
		{
			//if (type == SocketStream::Reliable)
			//		cout << "received a reliable packet : ";

			Packet p;
			stream.Receive(&p, -1, type == SocketStream::Reliable ? reliableHandle : unreliableHandle);

			MessageHeader messageHeader;
			p.Read(&messageHeader, sizeof(MessageHeader));
			//memcpy(&messageHeader, p.GetBuffer(), sizeof(MessageHeader));
			//GetBufferData(&messageHeader, sizeof(MessageHeader), type);

			switch (messageHeader.messageType)
			{
			case MessageNetObject:
				{
					//if (type == SocketStream::Reliable)
					//		cout << "MessageNetObject" << endl;

					NetObjectHeader netObjectHeader;
					p.Read(&netObjectHeader, sizeof(NetObjectHeader));
					//GetBufferData(&netObjectHeader, sizeof(NetObjectHeader), type);

					int size = messageHeader.size - sizeof(NetObjectHeader);
					char* buffer = new char[size];
					//GetBufferData(buffer, size, type);
					p.Read(buffer, size);
					ReceiveNetObjectMessage(netObjectHeader.networkId, buffer, size, netObjectHeader.target);
					delete[] buffer;
				}
				break;
			case MessageRaw:
				{

				}
				break;
			case MessageInternal:
				{
					//if (type == SocketStream::Reliable)
					//		cout << "MessageInternal" << endl;

					InternalHeader internalHeader;
					p.Read(&internalHeader, sizeof(InternalHeader));
					//GetBufferData(&internalHeader, sizeof(InternalHeader));

					switch (internalHeader.message)
					{				
					case InternalMessageCreateNetworkId:
						{
							InternalCreateNetworkId internalCreateNetworkId;
							p.Read(&internalCreateNetworkId, sizeof(InternalCreateNetworkId));
							//GetBufferData(&internalCreateNetworkId, sizeof(InternalCreateNetworkId), type);

							char* buffer = new char[internalCreateNetworkId.dataSize];
							//GetBufferData(buffer, internalCreateNetworkId.dataSize, type);
							p.Read(buffer, internalCreateNetworkId.dataSize);

							if (internalCreateNetworkId.clientId == clientId)
							{
								// this should fix the problem where if we request an index
								// and then something gets deleted, the index returned from the server could be wrong
								for (int i = 0; i < netObjects.GetSize(); ++i)
								{
									NetObject* obj = netObjects[i];
									if (obj->tempIndex == internalCreateNetworkId.origonalIdx)
									{
										obj->SetNetworkId(internalCreateNetworkId.networkId);
										obj->SetOwnerClientId(clientId);
										obj->tempIndex = -1;
									}
								}

								//netObjects[internalCreateNetworkId.origonalIdx]->SetNetworkId(internalCreateNetworkId.networkId);
								//netObjects[internalCreateNetworkId.origonalIdx]->SetOwnerClientId(clientId);
							}
							else
							{
								// create, assign network id, and add to the list
								netObjects.Insert(NetFactory::Create(internalCreateNetworkId.ownerClientId, internalCreateNetworkId.netType, internalCreateNetworkId.networkId, internalCreateNetworkId.dataSize, buffer));
							}	
							delete[] buffer;
						}
						break;
					case InternalMessageDestroyNetworkId:
						{

						}
						break;
					}
				}
				break;
			}
		}
	}
}

void NetClient::FlushUnreliable()
{
	// lay waste to unreliable packets
	while (stream.GetState(unreliableHandle) & eRead)
	{
		Packet p;
		stream.Receive(&p, -1, unreliableHandle);
	}	
}