//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "GameBase.h"
/*
#include "macros.h"
#include "../Engine/time.h"
#include "../Engine/input.h"
#include "../Engine/device.h"

#include "../Engine/texture.h"
#include "../Engine/material.h"

#include "log.h"

using namespace Siphon;

template <class T>
GameBase<T>::GameBase()
{

}

template <class T>
GameBase<T>::~GameBase()
{
	Time::Destroy();
	Input::Destroy();
	Device::Destroy();
	Log::Close();
}

template <class T>
bool GameBase<T>::Init(int argc, char **argv)
{
	Time::Create();
	Input::Create();
	Device::Create();

	Time::GetInstance().SetUpdateRate(Time::UPDATE_PHYSICS, 60);
	return true;
}

template <class T>
void GameBase<T>::Run()
{
	Input::GetInstance().Update();
	Time::GetInstance().Update();

	int i;
	bool playing = true;
	while (playing)
	{
		Time::GetInstance().Update();
		Input::GetInstance().Update();

		for (i = 0; i < Time::GetInstance().GetNumUpdates(Time::UPDATE_GAME); i++)
			playing &= UpdateGame();

		for (i = 0; i < Time::GetInstance().GetNumUpdates(Time::UPDATE_PHYSICS); i++)
			UpdatePhysics();

		Render();
	}

	Shutdown();
}

template <class T>
void GameBase<T>::SetBasePath(const char* path)
{
	TextureMgr::SetBasePath(path);
	MaterialMgr::SetBasePath(path);
	EffectMgr::SetBasePath(path);
}
*/
