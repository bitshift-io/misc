#include "NetServer.h"
#include "../Network/Packet.h"

NetServer::NetServer() :
	networkCounter(0),
	masterServerConnection(SocketStream::Reliable)
{

}

NetServer::~NetServer()
{

}

bool NetServer::Listen(int port, int flags, const char* name)
{ 
	return ServerBase::Listen(port, flags, name);
}

bool NetServer::Init(unsigned int gameId, unsigned int baseNetworkId, const char* masterServer)
{
	Clear();

	this->masterServer = masterServer;

	networkCounter = baseNetworkId;
	return ServerBase::Init(gameId);
}

unsigned int NetServer::RegisterNetObject(NetObject* obj, bool createOverNetwork, bool assignNetworkId)
{
	unsigned int idx = NetExtension::RegisterNetObject(obj, createOverNetwork, assignNetworkId);

	obj->SetCreateOverNetwork(createOverNetwork);
	obj->SetAssignNetworkId(assignNetworkId);

	if (!assignNetworkId)
		return idx;

	unsigned int dataSize = obj->GetConstructorDataSize();

	MessageHeader messageHeader;
	messageHeader.messageType = MessageInternal;
	messageHeader.size = sizeof(InternalHeader) + sizeof(InternalCreateNetworkId) + dataSize;

	InternalHeader internalHeader;
	internalHeader.message = InternalMessageCreateNetworkId;

	InternalCreateNetworkId internalCreateNetworkId;
	internalCreateNetworkId.origonalIdx = idx;
	internalCreateNetworkId.networkId = ++networkCounter;
	internalCreateNetworkId.netType = obj->GetNetType() ? obj->GetNetType()->GetType() : -1;
	internalCreateNetworkId.dataSize = dataSize;
	internalCreateNetworkId.clientId = GetClientId();
	internalCreateNetworkId.ownerClientId = GetClientId(); 
	internalCreateNetworkId.createOverNetwork = createOverNetwork;

	if (createOverNetwork)
	{
		int finalSize = sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId) + dataSize;
		char* buffer = new char[finalSize];

		memcpy(buffer, &messageHeader, sizeof(MessageHeader));
		memcpy(buffer + sizeof(MessageHeader), &internalHeader, sizeof(InternalHeader));
		memcpy(buffer + sizeof(MessageHeader) + sizeof(InternalHeader), &internalCreateNetworkId, sizeof(InternalCreateNetworkId));
		obj->GetConstructorData(buffer + sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId));

		Send((const char*)buffer, finalSize);
		delete[] buffer;
/*
		Send((const char*)&messageHeader, sizeof(MessageHeader));
		Send((const char*)&internalHeader, sizeof(InternalHeader));
		Send((const char*)&internalCreateNetworkId, sizeof(InternalCreateNetworkId));

		// send constructor data
		char* buffer = new char[dataSize];
		obj->GetConstructorData(buffer);
		Send((const char*)buffer, dataSize);
		delete[] buffer;*/
	}

	// set up id's
	obj->SetNetworkId(internalCreateNetworkId.networkId);
	obj->SetOwnerClientId(GetClientId());

	return idx;
}

void NetServer::UnregisterNetObject(NetObject* obj)
{
	NetExtension::UnregisterNetObject(obj);
}

void NetServer::SendNetObjectMessage(unsigned int networkId, const void* data, int size, NetMessageTarget target, SocketStream::Type type)
{
	MessageHeader messageHeader;
	messageHeader.messageType = MessageNetObject;
	messageHeader.size = sizeof(NetObjectHeader) + size;

	NetObjectHeader netObjectHeader;
	netObjectHeader.networkId = networkId;
	netObjectHeader.clientId = GetClientId();
	netObjectHeader.target = target;

	int finalSize = sizeof(MessageHeader) + sizeof(NetObjectHeader) + size;
	char* buffer = new char[finalSize];

	memcpy(buffer, &messageHeader, sizeof(MessageHeader));
	memcpy(buffer + sizeof(MessageHeader), &netObjectHeader, sizeof(NetObjectHeader));
	memcpy(buffer + sizeof(MessageHeader) + sizeof(NetObjectHeader), data, size);

	Send(buffer, finalSize, type);
	delete[] buffer;

	/*
	Send(&messageHeader, sizeof(MessageHeader), type);
	Send(&netObjectHeader, sizeof(NetObjectHeader), type);
	Send(data, size, type);*/
}

void NetServer::SendNetObjectMessageIgnoreClient(int index, unsigned int networkId, const void* data, int size, SocketStream::Type type)
{
	MessageHeader messageHeader;
	messageHeader.messageType = MessageNetObject;
	messageHeader.size = sizeof(NetObjectHeader) + size;

	NetObjectHeader netObjectHeader;
	netObjectHeader.networkId = networkId;
	netObjectHeader.clientId = GetClientId();
	netObjectHeader.target = All;

	int finalSize = sizeof(MessageHeader) + sizeof(NetObjectHeader) + size;
	char* buffer = new char[finalSize];

	memcpy(buffer, &messageHeader, sizeof(MessageHeader));
	memcpy(buffer + sizeof(MessageHeader), &netObjectHeader, sizeof(NetObjectHeader));
	memcpy(buffer + sizeof(MessageHeader) + sizeof(NetObjectHeader), data, size);

	// send to all but the ignored client
	for (int i = 0; i < GetNumClients(); ++i)
	{
		Connection& cl = GetClientConnection(i);

		if (i == index)
			continue;

		if (!Send(i, buffer, finalSize, type))
		{
			Connection& client = GetClientConnection(index);
			client.reliable.Close();
			client.unreliable.Close();
		}
	}	

	delete[] buffer;
}

void NetServer::SendNetObjectMessage(int index, unsigned int networkId, const void* data, int size, SocketStream::Type type)
{
	MessageHeader messageHeader;
	messageHeader.messageType = MessageNetObject;
	messageHeader.size = sizeof(NetObjectHeader) + size;

	NetObjectHeader netObjectHeader;
	netObjectHeader.networkId = networkId;
	netObjectHeader.clientId = GetClientId();
	netObjectHeader.target = All;

	int finalSize = sizeof(MessageHeader) + sizeof(NetObjectHeader) + size;
	char* buffer = new char[finalSize];

	memcpy(buffer, &messageHeader, sizeof(MessageHeader));
	memcpy(buffer + sizeof(MessageHeader), &netObjectHeader, sizeof(NetObjectHeader));
	memcpy(buffer + sizeof(MessageHeader) + sizeof(NetObjectHeader), data, size);

	if (!Send(index, buffer, finalSize, type))
	{
		Connection& client = GetClientConnection(index);
		client.reliable.Close();
		client.unreliable.Close();
	}

	delete[] buffer;
/*
	Send(index, &messageHeader, sizeof(MessageHeader), type);
	Send(index, &netObjectHeader, sizeof(NetObjectHeader), type);
	Send(index, data, size, type);*/
}

bool NetServer::CompleteMessageInBuffer(int index, SocketStream::Type type)
{
	Connection& client = GetClientConnection(index);
	return (client.stream.GetState(type == SocketStream::Reliable ? client.reliableHandle : client.unreliableHandle) & eRead) == eRead;
	/*
	Connection& client = GetClientConnection(index);
	int size = 0;
	
	switch (type)
	{
	case SocketStream::Reliable:
		size = client.reliable.GetBufferSize();
		break;
	case SocketStream::UnReliable:
		size = client.unreliable.GetBufferSize();
		break;
	}
	
	if (size > sizeof(MessageHeader))
	{
		MessageHeader messageHeader;
		PeekBufferData(index, &messageHeader, sizeof(MessageHeader), type);
		if (size >= sizeof(MessageHeader) + messageHeader.size && messageHeader.size > 0)
			return true;
		else
			return false;
	}

	return false;*/
}

void NetServer::CheckConnections()
{
	int state = masterServerConnection.GetSocketState();

	// is stream is readable, accept a new client
	if (state & SocketStream::Read)
	{
		SocketStream r(SocketStream::Reliable);
		if (masterServerConnection.Accept(r))
		{
			// client wants to join, master server will simply send an ip
			Log::Print("Connection accepted from master server\n");
/*
			// this is just to open up our server to an NAT's
			SocketStream udpSocket(SocketStream::UnReliable);
			bool connected = udpSocket.Connect(server.c_str(), serverPort);
			udpSocket.Send("nothing", sizeof("nothing"));
			udpSocket.Close();
*/
			//AcceptConnection(idx, clients[idx]);
			//Log::Print("Connection accepted from %s:%i\n", inet_ntoa(clientRemote.sin_addr), ntohs(clientRemote.sin_port));
		}
	}

	if (state & SocketStream::Error)
	{
		int err;
		int errlen = sizeof(err);
		getsockopt(masterServerConnection.GetSocket(), SOL_SOCKET, SO_ERROR,
			(char*)&err, &errlen);

		Log::Print("Socket error: %ld\n", err);
	}
	
	ServerBase::CheckConnections();
}

void NetServer::Update()
{
	CheckConnections();
	ServerBase::Update();

	for (int i = 0; i < GetNumClients(); ++i)
	{
		Connection& client = GetClientConnection(i);

		//int st = 0;
		for (int st = 0; st < 2; ++st)
		{
			SocketStream::Type type = (SocketStream::Type)st;

			while (CompleteMessageInBuffer(i, type))
			{
				//if (type == SocketStream::Reliable)
				//	cout << "received a reliable packet : ";

				Packet p;
				client.stream.Receive(&p, -1, type == SocketStream::Reliable ? client.reliableHandle : client.unreliableHandle);

				MessageHeader messageHeader;
				//GetBufferData(i, &messageHeader, sizeof(MessageHeader), type);
				p.Read(&messageHeader, sizeof(MessageHeader));

				switch (messageHeader.messageType)
				{
				case MessageNetObject:
					{
						//if (type == SocketStream::Reliable)
						//	cout << "MessageNetObject" << endl;

						NetObjectHeader netObjectHeader;
						//GetBufferData(i, &netObjectHeader, sizeof(NetObjectHeader), type);
						p.Read(&netObjectHeader, sizeof(NetObjectHeader));

						int size = messageHeader.size - sizeof(NetObjectHeader);
						char* data = new char[size];
						//GetBufferData(i, data, size, type);
						p.Read(data, size);
						ReceiveNetObjectMessage(netObjectHeader.networkId, data, size, netObjectHeader.target);

						int finalSize = sizeof(MessageHeader) + sizeof(NetObjectHeader) + size;
						char* buffer = new char[finalSize];

						memcpy(buffer, &messageHeader, sizeof(MessageHeader));
						memcpy(buffer + sizeof(MessageHeader), &netObjectHeader, sizeof(NetObjectHeader));
						memcpy(buffer + sizeof(MessageHeader) + sizeof(NetObjectHeader), data, size);

						// notify all clients except the sending client
						for (int i = 0; i < GetNumClients(); ++i)
						{
							Connection& cl = GetClientConnection(i);
							if (cl.clientId != netObjectHeader.clientId)
							{
								Send(i, buffer, finalSize, type);
	/*
								// notify all clients of the new object
								Send(i, &messageHeader, sizeof(MessageHeader), type);
								Send(i, &netObjectHeader, sizeof(NetObjectHeader), type);
								Send(i, data, size, type);*/
							}
						}

						delete[] buffer;
						delete[] data;
					}
					break;
				case MessageRaw:
					{

					}
					break;
				case MessageInternal:
					{
						//if (type == SocketStream::Reliable)
						//	cout << "MessageInternal" << endl;

						InternalHeader internalHeader;
						//GetBufferData(i, &internalHeader, sizeof(InternalHeader), type);
						p.Read(&internalHeader, sizeof(InternalHeader));

						switch (internalHeader.message)
						{				
						case InternalMessageCreateNetworkId:
							{
								InternalCreateNetworkId internalCreateNetworkId;
								//GetBufferData(i, &internalCreateNetworkId, sizeof(InternalCreateNetworkId), type);
								p.Read(&internalCreateNetworkId, sizeof(InternalCreateNetworkId));

								char* buffer = new char[internalCreateNetworkId.dataSize];
								//GetBufferData(i, buffer, internalCreateNetworkId.dataSize);
								p.Read(buffer, internalCreateNetworkId.dataSize);

								internalCreateNetworkId.networkId = ++networkCounter;
								
								if (!internalCreateNetworkId.createOverNetwork)
								{
									// notify all clients of the new object
									int finalSize = sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId) + internalCreateNetworkId.dataSize;
									char* sendBuffer = new char[finalSize];

									memcpy(sendBuffer, &messageHeader, sizeof(MessageHeader));
									memcpy(sendBuffer + sizeof(MessageHeader), &internalHeader, sizeof(InternalHeader));
									memcpy(sendBuffer + sizeof(MessageHeader) + sizeof(InternalHeader), &internalCreateNetworkId, sizeof(InternalCreateNetworkId));
									memcpy(sendBuffer + sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId), buffer, internalCreateNetworkId.dataSize);
									
									bool sent = Send(i, (const char*)sendBuffer, finalSize, type);
									delete[] sendBuffer;

									/*
									Send(i, (const char*)&messageHeader, sizeof(MessageHeader), type);
									Send(i, (const char*)&internalHeader, sizeof(InternalHeader), type);
									Send(i, (const char*)&internalCreateNetworkId, sizeof(InternalCreateNetworkId), type);
									Send(i, (const char*)buffer, internalCreateNetworkId.dataSize, type);*/
								}
								else
								{
									// notify all clients of the new object
									int finalSize = sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId) + internalCreateNetworkId.dataSize;
									char* sendBuffer = new char[finalSize];

									memcpy(sendBuffer, &messageHeader, sizeof(MessageHeader));
									memcpy(sendBuffer + sizeof(MessageHeader), &internalHeader, sizeof(InternalHeader));
									memcpy(sendBuffer + sizeof(MessageHeader) + sizeof(InternalHeader), &internalCreateNetworkId, sizeof(InternalCreateNetworkId));
									memcpy(sendBuffer + sizeof(MessageHeader) + sizeof(InternalHeader) + sizeof(InternalCreateNetworkId), buffer, internalCreateNetworkId.dataSize);
									
									bool sent = Send((const char*)sendBuffer, finalSize, type);
									delete[] sendBuffer;

									/*
									Send((const char*)&messageHeader, sizeof(MessageHeader), type);
									Send((const char*)&internalHeader, sizeof(InternalHeader), type);
									Send((const char*)&internalCreateNetworkId, sizeof(InternalCreateNetworkId), type);
									Send((const char*)buffer, internalCreateNetworkId.dataSize, type);
									*/
									// create, asign network id, and add to the list
									netObjects.Insert(NetFactory::Create(internalCreateNetworkId.ownerClientId, internalCreateNetworkId.netType, internalCreateNetworkId.networkId, internalCreateNetworkId.dataSize, buffer));
								}

								delete[] buffer;
							}
							break;
						case InternalMessageDestroyNetworkId:
							{

							}
							break;
						}
					}
					break;
				}
			}
		}
	}
}

void NetServer::AcceptConnection(int index, Connection& client)
{/*
	// open a UDP port to master server, so we can get through the NAT
	SocketStream temp(SocketStream::UnReliable);
	temp.Connect(masterServer.c_str(), port + client.clientId);
	temp.Send(masterServer.c_str(), masterServer.length() + 1);
	temp.Close();
	temp.Connect(masterServer.c_str(), port + client.clientId + 1);
	temp.Send(masterServer.c_str(), masterServer.length() + 1);
	temp.Close();
*/
	ServerBase::AcceptConnection(index, client);

	// when a client connects, we need to sync the game state, send it all objects
	for (unsigned int i = 0; i < netObjects.GetSize(); ++i)
	{
		NetObject* nObj = netObjects[i];

		bool createOverNetwork = nObj->GetCreateOverNetwork();
		bool assignNetworkId = nObj->GetAssignNetworkId();

		if (!createOverNetwork) //assignNetworkId)
			continue;

		unsigned int dataSize = nObj->GetConstructorDataSize();

		MessageHeader messageHeader;
		messageHeader.messageType = MessageInternal;
		messageHeader.size = sizeof(InternalHeader) + sizeof(InternalCreateNetworkId) + dataSize;

		InternalHeader internalHeader;
		internalHeader.message = InternalMessageCreateNetworkId;

		InternalCreateNetworkId internalCreateNetworkId;
		internalCreateNetworkId.origonalIdx = -1;
		internalCreateNetworkId.clientId = GetClientId();
		internalCreateNetworkId.ownerClientId = nObj->GetOwnerClientId(); 
		internalCreateNetworkId.networkId = nObj->GetNetworkId();
		internalCreateNetworkId.netType = nObj->GetNetType() ? nObj->GetNetType()->GetType() : -1;
		internalCreateNetworkId.dataSize = dataSize;
		internalCreateNetworkId.createOverNetwork = createOverNetwork;

		Send(index, &messageHeader, sizeof(MessageHeader));
		Send(index, &internalHeader, sizeof(InternalHeader));
		Send(index, &internalCreateNetworkId, sizeof(InternalCreateNetworkId));

		// send constructor data
		char* buffer = new char[dataSize];
		nObj->GetConstructorData(buffer);
		Send((const char*)buffer, dataSize);
		delete[] buffer;

		// we may need to call a NetObject->Sync method to make sure all net objects are in the same state
	}

	if (callback)
		callback->AcceptConnection(index, client);
}

void NetServer::ClientDisconnect(int index, Connection& client)
{
	if (callback)
		callback->ClientDisconnect(index, client);

	/*
	// delete all netobjects from the owner
	for (unsigned int i = 0; i < netObjects.GetSize(); ++i)
	{
		NetObject* nObj = netObjects[i];
	}
*/
	ServerBase::ClientDisconnect(index, client);
}

void NetServer::FlushUnreliable()
{
	for (int i = 0; i < GetNumClients(); ++i)
	{
		Connection& con = GetClientConnection(i);

		// lay waste to unreliable packets
		while (con.stream.GetState(con.unreliableHandle) & eRead)
		{
			Packet p;
			con.stream.Receive(&p, -1, con.unreliableHandle);
		}
	}
}