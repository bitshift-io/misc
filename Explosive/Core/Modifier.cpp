#include "Modifier.h"

Modifier::Modifier() : owner(0)
{

}

Modifier::~Modifier()
{

}

Modifier::Modifier(Actor* owner) 
{
	SetOwner(owner);
}

void Modifier::SetOwner(Actor* owner)
{
	this->owner = owner;
}

Actor* Modifier::GetOwner()
{
	return owner;
}
