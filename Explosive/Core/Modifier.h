#ifndef _MODIFIER_
#define _MODIFIER_

#include "ActorMgr.h"

/**
 * Modifies an actor
 */
class Modifier
{
public:
	Modifier();
	virtual ~Modifier();

	Modifier(Actor* owner);

	void SetOwner(Actor* owner);
	Actor* GetOwner();

	virtual int GetType() const { return -1; }

	// return false indicated it should be destroyed
	virtual bool Update() = 0;

protected:
	Actor* owner;
};

#endif