#ifndef _SIPHON_GAMEMODE_
#define _SIPHON_GAMEMODE_

#include "Team.h"

class Objective
{
public:

protected:

};

class GameMode
{
public:
	virtual void Reset();
	virtual void Update() = 0;
	virtual void NotifyDead(Character* character) = 0;

protected:
	Array<Team*> teams;
};

#endif