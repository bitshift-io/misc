#include "PickUp.h"

PickUp::PickUp(const char* meshFile)
{
	mesh = Mesh::LoadMesh(meshFile);
}

PickUp::~PickUp()
{
	Mesh::ReleaseMesh(mesh);
}