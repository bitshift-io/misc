#include "../Core/GameBase.h"
#include "../Core/NetServer.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"
#include "../Engine/Font.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "../Engine/Renderer.h"
#include "../Engine/Time.h"
#include "../Engine/Material.h"
#include "../Engine/Animation.h"
#include "../Engine/Window.h"
#include "../Engine/Server.h"

#include "../Util/Log.h"
#include "../Util/Macros.h"

using namespace Siphon;

class TestNetworkServer : public GameBase<TestNetworkServer>
{
public:
	bool Init(int argc, char **argv) 
	{
		GameBase::Init(argc, argv);

		NetServer::Create();
		NetServer::GetInstance().Init();

		NetServer::GetInstance().Listen(1050);

		while (1)
		{
			NetServer::GetInstance().Update();
			/*
			NetServer::GetInstance().CheckConnections();

			for (int i = 0; i < NetServer::GetInstance().GetNumClients(); ++i)
			{
				Connection& client = NetServer::GetInstance().GetClientConnection(i);

				char buffer[1024];
				if (client.reliable.GetSocketState() & SocketStream::Read)
				{
					int numBytes = client.reliable.Receive(buffer, 1024);
					client.reliable.Send(buffer, numBytes);
				}
			}*/
		}

		NetServer::GetInstance().Shutdown();
		NetServer::Destroy();

		return false;
	}

	virtual void Shutdown()
	{
	}

	virtual bool UpdateGame()
	{
		return true;
	}

	virtual void UpdatePhysics()
	{

	}

	virtual void Render()
	{

	}

protected:

};

MAIN(TestNetworkServer)
