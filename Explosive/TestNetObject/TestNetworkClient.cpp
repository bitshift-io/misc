#include "../Core/GameBase.h"
#include "../Core/NetClient.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"
#include "../Engine/Font.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "../Engine/Renderer.h"
#include "../Engine/Time.h"
#include "../Engine/Material.h"
#include "../Engine/Animation.h"
#include "../Engine/Window.h"
#include "../Engine/Client.h"

#include "../Util/Log.h"
#include "../Util/Macros.h"

#include "TestNetObject.h"

using namespace Siphon;

class TestNetworkClient : public GameBase<TestNetworkClient>
{
public:
	bool Init(int argc, char **argv) 
	{
		GameBase::Init(argc, argv);

		NetClient::Create();
		NetClient::GetInstance().Init();

		if (NetClient::GetInstance().Connect("localhost", 1050))
		{
			TestNetObject* netObj = new TestNetObject();
			NetClient::GetInstance().Update();
			netObj->SomeFn();
			netObj->ParamFn("yay! i pwn joos all!");
			NetClient::GetInstance().Update();
		}

		NetClient::GetInstance().Shutdown();
		NetClient::Destroy();

		return false;
	}

	virtual void Shutdown()
	{
	}

	virtual bool UpdateGame()
	{
		return true;
	}

	virtual void UpdatePhysics()
	{

	}

	virtual void Render()
	{

	}

protected:

};

MAIN(TestNetworkClient)
