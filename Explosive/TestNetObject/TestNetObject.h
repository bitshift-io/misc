#ifndef _TEST_NETWORK_OBJECT_
#define _TEST_NETWORK_OBJECT_

#include "../Core/NetObject.h"
#include "../Core/NetServer.h"
#include "../Core/NetClient.h"
#include "../Util/Log.h"

using namespace Siphon;

#define SOME_FN 1
#define PARAM_FN 2

class TestNetObject : public NetObject
{
public:
	TestNetObject(unsigned int networkId) : NetObject(networkId) 
	{ 
		Log::Print("Constructing TestNetObject with network id: %i\n", networkId);
	}

	TestNetObject() : NetObject()
	{
		RegisterWithNetwork();
		Log::Print("Constructing owner TestNetObject with network id: %i\n", networkId);
	}

	void SomeFn()
	{
		unsigned int temp = SOME_FN;
		Send(&temp, sizeof(unsigned int));
		Log::Print("calling some fn\n");
	}

	void ParamFn(const char* str)
	{
		unsigned int temp = PARAM_FN;

		char* buffer = new char[strlen(str) + 1 + sizeof(unsigned int)];
		memcpy(buffer, &temp, sizeof(unsigned int));
		memcpy(buffer + sizeof(unsigned int), str, strlen(str) + 1);
		Send(buffer, strlen(str) + 1 + sizeof(unsigned int));
		Log::Print("calling ParamFn with parameter: %s\n", str);
	}

	virtual void ReceiveMessage(const void* message, int length)
	{
		BlockSendBegin();

		const char* data = (const char*)message;
		unsigned int fn = *data;

		switch (fn)
		{
		case SOME_FN:
			SomeFn();
			return;
		case PARAM_FN:
			ParamFn(data + sizeof(unsigned int));
			return;
		}

		BlockSendEnd();
	}

	virtual NetType* GetNetType();

protected:

};

class TestNetObjectType : public NetType
{
public:
	TestNetObjectType() : NetType() { }
	virtual NetObject* Create(unsigned int networkId) { return new TestNetObject(networkId); }

	static TestNetObjectType instance;
};


#endif
