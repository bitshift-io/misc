#include "../Memory/Memory.h"

int main(int argc, void* args[])
{
	{
		char* leak = new char[100];
	}

	{
		char* noLeak = new char[100];
		delete[] noLeak;
	}

	{
		char* leak = new char[100];
		delete leak;
	}

	{
		char* noLeak = (char*)malloc(100);
		free(noLeak);
	}

	{
		char* leak = (char*)malloc(100);;
	}

	MemTracker::Log();
	return 0;
}