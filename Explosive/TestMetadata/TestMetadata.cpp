#include "../Metadata/PrecompiledHeader.h"

// FUCKING FUCK! why cant we declare this in the metadata lib?
METADATA_BASICTYPE(int, "%i")
METADATA_BASICTYPE(float, "%f")
METADATA_BASICTYPE(char, "%c")

enum E
{
	eTest,
	eTest2,
};

METADATA_ENUM_START(E)
	METADATA_ENUM_VARIABLE(eTest)
	METADATA_ENUM_VARIABLE(eTest2)
METADATA_ENUM_END(E, false)

METADATA_CLASS_EXTERN(P)

class P
{
public:
	METADATA_DECLARE(P)

	int x;
	E	y;
};


METADATA_CLASS_START(P)
	METADATA_CLASS_VARIABLE_C(P, int, x, "health")
	METADATA_CLASS_VARIABLE_C(P, E, y, "some combo box")
METADATA_CLASS_END_C(P, "dummy")

int main(int argc, void* args[])
{
	P test;
	test.x = 5;
	test.y = eTest2;

	gMetadataFactory.SerializeTo("test.mda");
	test.SerializeTo("test.mdd");

	const Metadata* md = test.GetMetadata();
	return 0;
}