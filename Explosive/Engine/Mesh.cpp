//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------

#ifndef WIN32
	#define GL_GLEXT_PROTOTYPES

	#include <GL/gl.h>
	#include <GL/glu.h>
	#include <GL/glext.h>
#endif

#include "Mesh.h"
#include "Device.h"

#include "Macros.h"
#include "Log.h"

#include "Animation.h"

namespace Siphon
{

#define VERTEXBUFFER	30
#define MESH			31

//--------------------------------------------------------------------------------------
// VertexBuffer
//--------------------------------------------------------------------------------------
GLint VertexBuffer::weightHandle = -1;
GLint VertexBuffer::boneHandle = -1;
GLint VertexBuffer::binormalHandle = -1;
GLint VertexBuffer::tangentHandle = -1;

VertexBuffer::VertexBuffer() : flags(Static), renderType(GL_TRIANGLES)
{

}

VertexBuffer::~VertexBuffer()
{/*
	//release vbos?
	if (flags == STATIC)
	{
		glDeleteBuffersARB( 1, &vboPositions );
		glDeleteBuffersARB( 1, &vboTexCoords );
		glDeleteBuffersARB( 1, &vboIndices );
		glDeleteBuffersARB( 1, &vboNormals );
		glDeleteBuffersARB( 1, &vboColours );
	}*/
}

void VertexBuffer::SetShaderHandles(GLint weight, GLint bone, GLint binormal, GLint tangent)
{
	weightHandle = weight;
	boneHandle = bone;
	binormalHandle = binormal;
	tangentHandle = tangent;
}

void VertexBuffer::DebugRender(float scale)
{
	// draw tangents, normals and binormals
	// this will only work if the mesh is using identity matrix

	glBegin(GL_LINES);

	glColor4f(0.0f, 0.0f, 1.0f, 0.5f);
	for (int i = 0; i < positions.GetSize(); ++i)
	{
		Vector3 p = positions[i];
		Vector3 n = normals[i] * scale;

		glVertex3f(p[Vector3::X], p[Vector3::Y], p[Vector3::Z]);
		glVertex3f(p[Vector3::X] + n[Vector3::X], p[Vector3::Y] + n[Vector3::Y], p[Vector3::Z] + n[Vector3::Z]);
	}

	glColor4f(0.0f, 1.0f, 0.0f, 0.5f);
	for (int i = 0; i < positions.GetSize(); ++i)
	{
		Vector3 p = positions[i];
		Vector3 n = tangents[i] * scale;

		glVertex3f(p[Vector3::X], p[Vector3::Y], p[Vector3::Z]);
		glVertex3f(p[Vector3::X] + n[Vector3::X], p[Vector3::Y] + n[Vector3::Y], p[Vector3::Z] + n[Vector3::Z]);
	}

	glColor4f(1.0f, 0.0f, 0.0f, 0.5f);
	for (int i = 0; i < positions.GetSize(); ++i)
	{
		Vector3 p = positions[i];
		Vector3 n = binormals[i] * scale;

		glVertex3f(p[Vector3::X], p[Vector3::Y], p[Vector3::Z]);
		glVertex3f(p[Vector3::X] + n[Vector3::X], p[Vector3::Y] + n[Vector3::Y], p[Vector3::Z] + n[Vector3::Z]);
	}

	glEnd();
}

unsigned int VertexBuffer::Render(unsigned int startIndex, unsigned int numIndices, VertexFormat format)
{
#ifdef DEBUG
	char info[256];
	sprintf(info, "[START] VertexBuffer::Render(%i, %i, %i) - %s", startIndex, numIndices, flags, name.c_str());
	GDBSTRING(info);
#endif

	if (this->flags & Static)
	{
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_INDEX_ARRAY);

		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboPositions);
		glVertexPointer(3, GL_FLOAT, sizeof(Vector3), 0);

		if (format & VertexNormal && normals.GetSize())
		{
			glEnableClientState( GL_NORMAL_ARRAY );
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboNormals );
			glNormalPointer( GL_FLOAT, sizeof(Vector3), 0 );
		}
		else
		{
			glDisableClientState(GL_NORMAL_ARRAY);
		}

		if (format & VertexColour && colours.GetSize())
		{
			glEnableClientState(GL_COLOR_ARRAY);
			glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboColours);
			glColorPointer(4, GL_FLOAT, sizeof(Colour), 0);
		}
		else
		{
			glDisableClientState(GL_COLOR_ARRAY);
		}

		int uvIdx = 0;

		if (format & VertexTangent && tangents.GetSize())
		{
			glClientActiveTexture(GL_TEXTURE0_ARB + uvIdx);
			glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboTangents);
			glTexCoordPointer(3, GL_FLOAT, sizeof(Vector3), 0);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			++uvIdx;
		}

		if (format & VertexBinormal && binormals.GetSize())
		{
			glClientActiveTexture(GL_TEXTURE0_ARB + uvIdx);
			glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboBinormals);
			glTexCoordPointer(3, GL_FLOAT, sizeof(Vector3), 0);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			++uvIdx;
		}

		if (format & VertexWeights && weights.GetSize())
		{
			glClientActiveTexture(GL_TEXTURE0_ARB + uvIdx);
			glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboWeights);
			glTexCoordPointer(4, GL_FLOAT, sizeof(Vector4), 0);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			++uvIdx;
		}

		if (format & VertexBones && bones.GetSize())
		{
			glClientActiveTexture(GL_TEXTURE0_ARB + uvIdx);
			glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboBones);
			glTexCoordPointer(4, GL_FLOAT, sizeof(Vector4), 0);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			++uvIdx;
		}

		if (format & VertexUVCoord1 && uvCoords.GetSize())
		{
			glClientActiveTexture(GL_TEXTURE0_ARB + uvIdx);
			glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboTexCoords);
			glTexCoordPointer(2, GL_FLOAT, sizeof(UVCoord), 0);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			++uvIdx;
		}
/*
		if (format & VertexUVCoord2 && uvCoords2.GetSize())
		{
			glClientActiveTexture(uvIdx);
			glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboTexCoords2);
			glTexCoordPointer(2, GL_FLOAT, sizeof(UVCoord), 0);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			++uvIdx;
		}*/

		/*
		// disable any left over uv channels
		for (; uvIdx <= 4; ++uvIdx)
		{
			glClientActiveTexture(uvIdx);
			glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		}*/


/*
		if (uvCoords.GetSize())
		{
			int i;
			glGetIntegerv(GL_MAX_TEXTURE_UNITS_ARB, &i);
			--i;
			for (; i >= 0; --i)
			{
				if (uvCoords2.GetSize() && i != 0)
				{
					glClientActiveTexture(i);
					glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboTexCoords2);
					glTexCoordPointer(2, GL_FLOAT, sizeof(UVCoord), 0);
					glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				}
				else
				{
					glClientActiveTexture(i);
					glBindBufferARB(GL_ARRAY_BUFFER_ARB, vboTexCoords);
					glTexCoordPointer(2, GL_FLOAT, sizeof(UVCoord), 0);
					glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				}
			}
		}
		else
		{
			int i;
			glGetIntegerv(GL_MAX_TEXTURE_UNITS_ARB, &i);
			--i;
			for (; i >= 0; --i)
			{
				glClientActiveTexture(i);
				glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
				glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			}
		}

		if (weightHandle != -1)
		{
			if (weights.GetSize())
			{
				glEnableVertexAttribArrayARB(weightHandle);
				glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboWeights );
				glVertexAttribPointerARB(weightHandle, 4, GL_FLOAT, GL_FALSE, 0, 0);
			}
			else
			{
				glDisableVertexAttribArrayARB(weightHandle);
			}
		}

		if (boneHandle != -1)
		{
			if (bones.GetSize())
			{
				glEnableVertexAttribArrayARB(boneHandle);
				glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboBones );
				glVertexAttribPointerARB(boneHandle, 4, GL_FLOAT, GL_FALSE, 0, 0);
			}
			else
			{
				glDisableVertexAttribArrayARB(boneHandle);
			}
		}

		if (binormalHandle != -1)
		{
			if (binormals.GetSize())
			{
				glEnableVertexAttribArrayARB(binormalHandle);
				glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboBinormals );
				glVertexAttribPointerARB(binormalHandle, 3, GL_FLOAT, GL_FALSE, 0, 0);
			}
			else
			{
				glDisableVertexAttribArrayARB(binormalHandle);
			}
		}

		if (tangentHandle != -1)
		{
			if (tangents.GetSize())
			{
				glEnableVertexAttribArrayARB(tangentHandle);
				glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboTangents );
				glVertexAttribPointerARB(tangentHandle, 3, GL_FLOAT, GL_FALSE, 0, 0);
			}
			else
			{
				glDisableVertexAttribArrayARB(tangentHandle);
			}
		}*/

		glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, vboIndices);
		glDrawElements(renderType, numIndices,GL_UNSIGNED_INT,BUFFER_OFFSET(startIndex*sizeof(unsigned int)));

		//glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
		//glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
	}
	else if (this->flags & Dynamic)
	{
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_INDEX_ARRAY);

		glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
		glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);

		if (numIndices == 0)
			numIndices = indices.GetSize();

		glEnableClientState(GL_VERTEX_ARRAY);

		if (format & VertexNormal && normals.GetSize())
		{
			glEnableClientState(GL_NORMAL_ARRAY);
			glNormalPointer(GL_FLOAT, sizeof(Vector3), normals.GetData());
		}
		else
		{
			glDisableClientState(GL_NORMAL_ARRAY);
		}

		if (format & VertexColour && colours.GetSize())
		{
			glEnableClientState(GL_COLOR_ARRAY);
			glColorPointer(4, GL_FLOAT, sizeof(Colour), colours.GetData());
		}
		else
		{
			glDisableClientState(GL_COLOR_ARRAY);
		}

		int uvIdx = 0;

		if (format & VertexTangent && tangents.GetSize())
		{
			glClientActiveTexture(GL_TEXTURE0_ARB + uvIdx);
			glTexCoordPointer(3, GL_FLOAT, sizeof(Vector3), tangents.GetData());
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			++uvIdx;
		}

		if (format & VertexBinormal && binormals.GetSize())
		{
			glClientActiveTexture(GL_TEXTURE0_ARB + uvIdx);
			glTexCoordPointer(3, GL_FLOAT, sizeof(Vector3), binormals.GetData());
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			++uvIdx;
		}

		if (format & VertexWeights && weights.GetSize())
		{
			glClientActiveTexture(GL_TEXTURE0_ARB + uvIdx);
			glTexCoordPointer(4, GL_FLOAT, sizeof(Vector3), weights.GetData());
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			++uvIdx;
		}

		if (format & VertexBones && bones.GetSize())
		{
			glClientActiveTexture(GL_TEXTURE0_ARB + uvIdx);
			glTexCoordPointer(4, GL_FLOAT, sizeof(Vector3), bones.GetData());
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			++uvIdx;
		}

		if (format & VertexUVCoord1 && uvCoords.GetSize())
		{
			glClientActiveTexture(GL_TEXTURE0_ARB + uvIdx);
			glTexCoordPointer(2, GL_FLOAT, sizeof(UVCoord), uvCoords.GetData());
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			++uvIdx;
		}
		
		glVertexPointer(3, GL_FLOAT, sizeof(Vector3), positions.GetData());

		//if (numIndices)
			glDrawElements(renderType, numIndices, GL_UNSIGNED_INT, indices.GetData() + startIndex);
		//else
		//	glDrawArrays(renderType, startIndex, );
		//glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indices.GetData() + (startIndex*sizeof(unsigned int)));
	}

#ifdef DEBUG
	sprintf(info, "[END] VertexBuffer::Render - %s", name.c_str());
	GDBSTRING(info);
#endif

	return numIndices/3;
}

bool VertexBuffer::Build()
{
	if (flags & Dynamic)
		return true;

	if (!Device::GetInstance().VBOSupported())
	{
		SetFlags(Dynamic);
		return true;
	}

	SetFlags(Static);

	glGenBuffersARB( 1, &vboPositions );
	glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboPositions );
	glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Vector3) * positions.GetSize(), positions.GetData(), GL_STATIC_DRAW_ARB );

	if (normals.GetSize())
	{
		glGenBuffersARB( 1, &vboNormals );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboNormals );
		glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Vector3) * normals.GetSize(), normals.GetData(), GL_STATIC_DRAW_ARB );
	}

	if (uvCoords.GetSize())
	{
		glGenBuffersARB( 1, &vboTexCoords );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboTexCoords );
		glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(UVCoord) * uvCoords.GetSize(), uvCoords.GetData(), GL_STATIC_DRAW_ARB );
	}

	if (uvCoords2.GetSize())
	{
		glGenBuffersARB( 1, &vboTexCoords2 );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboTexCoords2 );
		glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(UVCoord) * uvCoords2.GetSize(), uvCoords2.GetData(), GL_STATIC_DRAW_ARB );
	}

	if (colours.GetSize())
	{
		glGenBuffersARB( 1, &vboColours );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboColours );
		glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Colour) * colours.GetSize(), colours.GetData(), GL_STATIC_DRAW_ARB );
	}

	if (weights.GetSize())
	{
		glGenBuffersARB( 1, &vboWeights );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboWeights );
		glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Vector4) * weights.GetSize(), weights.GetData(), GL_STATIC_DRAW_ARB );
	}

	if (bones.GetSize())
	{
		glGenBuffersARB( 1, &vboBones );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboBones );
		glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Vector4) * bones.GetSize(), bones.GetData(), GL_STATIC_DRAW_ARB );
	}

	if (tangents.GetSize())
	{
		glGenBuffersARB( 1, &vboTangents );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboTangents );
		glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Vector3) * tangents.GetSize(), tangents.GetData(), GL_STATIC_DRAW_ARB );
	}

	if (binormals.GetSize())
	{
		glGenBuffersARB( 1, &vboBinormals );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboBinormals );
		glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Vector3) * binormals.GetSize(), binormals.GetData(), GL_STATIC_DRAW_ARB );
	}

	glGenBuffersARB( 1, &vboIndices );
	glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, vboIndices );
	glBufferDataARB( GL_ELEMENT_ARRAY_BUFFER_ARB, sizeof(unsigned int) * indices.GetSize(), indices.GetData(), GL_STATIC_DRAW_ARB );

	glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
	glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0 );

	glDisable(GL_COLOR_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_INDEX_ARRAY);

	BuildBounds();

	return true;
}

void VertexBuffer::BuildBounds()
{
	boundSphere = Sphere(0, Vector3(0,0,0));

	for (unsigned int i = 0; i < positions.GetSize(); ++i)
	{
		float size = positions[i].Magnitude();
		if (size > boundSphere.GetRadius())
			boundSphere.SetRadius(size);
	}
}

void VertexBuffer::SetFlags(unsigned int flags)
{
	this->flags = flags;
}

unsigned int VertexBuffer::GetFlags()
{
	return flags;
}

bool VertexBuffer::Load(File& file)
{
	//name = file.GetName();

	// write out vertex buffer header
	Chunk vBufChunk;
	vBufChunk.ReadHeader(file);

	// read flags
	file.Read(&flags, sizeof(unsigned int));

	// write out individual settings
	indices.Load(file);
	positions.Load(file);
	uvCoords.Load(file);
	uvCoords2.Load(file);
	normals.Load(file);
	colours.Load(file);
	weights.Load(file);
	bones.Load(file);

	// HACK
	for (int i = 0; i < bones.GetSize(); ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			if (bones[i][j] == -1.0f)
				bones[i][j] = 0.0f;
		}
	}

	tangents.Load(file);
	binormals.Load(file);

	Build();
	return true;
}

bool VertexBuffer::Save(File& file)
{
	// write out vertex buffer header
	Chunk vBufChunk;
	vBufChunk.WriteHead(file, VERTEXBUFFER);

	// write flags
	file.Write(&flags, sizeof(unsigned int));

	// write out individual settings
	indices.Save(file);
	positions.Save(file);
	uvCoords.Save(file);
	uvCoords2.Save(file);
	normals.Save(file);
	colours.Save(file);
	weights.Save(file);
	bones.Save(file);
	tangents.Save(file);
	binormals.Save(file);

	vBufChunk.WriteFoot(file);
	return true;
}

void VertexBuffer::operator=(const VertexBuffer& rhs)
{
	boundSphere = rhs.boundSphere;
	flags = rhs.flags;
	renderType = rhs.renderType;
	name = rhs.name + std::string("#clone");

	colours.SetSize(rhs.colours.GetSize());
	memcpy(colours.GetData(), rhs.colours.GetData(), sizeof(Colour) * rhs.colours.GetSize());

	positions.SetSize(rhs.positions.GetSize());
	memcpy(positions.GetData(), rhs.positions.GetData(), sizeof(Vector3) * rhs.positions.GetSize());

	normals.SetSize(rhs.normals.GetSize());
	memcpy(normals.GetData(), rhs.normals.GetData(), sizeof(Vector3) * rhs.normals.GetSize());

	uvCoords.SetSize(rhs.uvCoords.GetSize());
	memcpy(uvCoords.GetData(), rhs.uvCoords.GetData(), sizeof(UVCoord) * rhs.uvCoords.GetSize());

	uvCoords2.SetSize(rhs.uvCoords2.GetSize());
	memcpy(uvCoords.GetData(), rhs.uvCoords2.GetData(), sizeof(UVCoord) * rhs.uvCoords2.GetSize());

	indices.SetSize(rhs.indices.GetSize());
	memcpy(indices.GetData(), rhs.indices.GetData(), sizeof(unsigned int) * rhs.indices.GetSize());

	weights.SetSize(rhs.weights.GetSize());
	memcpy(weights.GetData(), rhs.weights.GetData(), sizeof(Vector4) * rhs.weights.GetSize());

	bones.SetSize(rhs.bones.GetSize());
	memcpy(bones.GetData(), rhs.bones.GetData(), sizeof(Vector4) * rhs.bones.GetSize());

	binormals.SetSize(rhs.binormals.GetSize());
	memcpy(binormals.GetData(), rhs.binormals.GetData(), sizeof(Vector3) * rhs.binormals.GetSize());

	tangents.SetSize(rhs.tangents.GetSize());
	memcpy(tangents.GetData(), rhs.tangents.GetData(), sizeof(Vector3) * rhs.tangents.GetSize());
}

//--------------------------------------------------------------------------------------
// Class Mesh
//--------------------------------------------------------------------------------------
Array<Mesh*> Mesh::resources;
std::string Mesh::basePath("data/");

Mesh* Mesh::LoadMesh(const char* file)
{
	// memory leak... need a way to release all meshes....
	// maybe Mesh::Shutdown() or Mesh::ReleaseMeshes();

	// step 1. find the resource
	std::string path = basePath + std::string(file);
	for (unsigned int i = 0; i < resources.GetSize(); ++i)
	{
		if (strcmp(resources[i]->GetName(), path.c_str()) == 0)
		{
			Mesh* clone = resources[i]->Clone();
			//resources.Insert(clone); // we only need the primray one in the list
			return clone;
		}
	}

	// step 2. create new resource if not existing
	Mesh* mesh = new Mesh;
	if (!mesh->Load(path.c_str()))
	{
		SAFE_DELETE(mesh);
		return 0;
	}

	resources.Insert(mesh);
	return mesh;
}

void Mesh::ReleaseMesh(Mesh* mesh, bool removeFromMem)
{
	// step 1. find the resource, see
	for (unsigned int i = 0; i < resources.GetSize(); ++i)
	{
		// okay then its a primary resource!
		if (mesh == resources[i])
		{
			if (removeFromMem)
			{
				resources.Remove(i);
				delete mesh->GetMeshMat().vertexBuffer;
			}
			return;
		}
	}

	delete mesh;
}

void Mesh::SetBasePath(const char* path)
{
	basePath = std::string(path);
}

void Mesh::SetCallback(Callback* callback)
{
	callbacks = callback;
}

void Mesh::ReleaseInternal()
{
	delete meshMats.vertexBuffer;
	meshMats.vertexBuffer = 0;

	SAFE_DELETE(skinRenderBuffer);
	SAFE_DELETE(boneMatrices);

	// TODO: delete materials
}

void Mesh::DeleteResources()
{
	for (unsigned int i = 0; i < resources.GetSize(); ++i)
	{
		resources[i]->ReleaseInternal();
		delete resources[i];
	}
}

const char* Mesh::GetName()
{
	return meshMats.vertexBuffer->name.c_str();
}

bool Mesh::Load(const char* file)
{
	unsigned int i;

	// write out vertex buffer header
	File in(file, "rb");

	if (!in.Valid())
	{
		Log::Print("Failed to open mesh file: %s", file);
		return false;
	}

	Chunk meshChunk;
	meshChunk.ReadHeader(in);

	unsigned int size;
	in.Read(&size, sizeof(unsigned int));
	meshMats.material.SetSize(size);
	for (i = 0; i < meshMats.material.GetSize(); ++i)
	{
		// read in each table
		in.Read(&meshMats.material[i].startIndex, sizeof(unsigned int));
		in.Read(&meshMats.material[i].numIndices, sizeof(unsigned int));
		in.Read(&meshMats.material[i].numPrimitives, sizeof(unsigned int));

		// read material name
		char matName[256];
		int length = 0;
		meshMats.material[i].material = 0;
		in.Read(&length, sizeof(int));
		if (length)
		{
			in.Read(&matName, length);
			meshMats.material[i].material = MaterialMgr::Load(matName);
		}
	}

	meshMats.vertexBuffer = new VertexBuffer();
	meshMats.vertexBuffer->Load(in);
	meshMats.vertexBuffer->name = std::string(file);

	bounds = meshMats.vertexBuffer->boundSphere;

	return true;
}

bool Mesh::Save(const char* file)
{
	unsigned int i;

	File out(file, "wb");

	Chunk meshChunk;
	meshChunk.WriteHead(out, MESH);

	unsigned int size;
	size = meshMats.material.GetSize();
	out.Write(&size, sizeof(unsigned int));
	for (i = 0; i < meshMats.material.GetSize(); ++i)
	{
		// write out each table (should this be mattable.Save?
		out.Write(&meshMats.material[i].startIndex, sizeof(unsigned int));
		out.Write(&meshMats.material[i].numIndices, sizeof(unsigned int));
		out.Write(&meshMats.material[i].numPrimitives, sizeof(unsigned int));

		// write material name
		int length = 0;
		if (meshMats.material[i].material)
		{
			length = strlen(meshMats.material[i].material->GetName()) + 1;
			out.Write(&length, sizeof(int));
			out.Write(meshMats.material[i].material->GetName(), length);
		}
		else
		{
			out.Write(&length, sizeof(int));
		}
	}

	meshMats.vertexBuffer->Save(out);

	meshChunk.WriteFoot(out);
	return true;
}

Mesh* Mesh::Clone()
{
	Mesh* clone = new Mesh();
	clone->meshMats = meshMats;
	//clone->transform = transform;
	clone->bounds.SetRadius(bounds.GetRadius());
	return clone;
}

Mesh::Mesh() :
	cullState(Frustum::Inside),
	transform(Matrix4::Identity),
	skeletonAnimation(0),
	skinRenderBuffer(0),
	boneMatrices(0),
	callbacks(0)
{

}

Mesh::Mesh(VertexBuffer* vb, MeshMat& meshMat) :
	cullState(Frustum::Inside),
	transform(Matrix4::Identity),
	skeletonAnimation(0),
	skinRenderBuffer(0),
	boneMatrices(0),
	callbacks(0)
{
	this->meshMats = meshMat;
}

void Mesh::SetSkeletonAnimation(SkeletonAnimation* skeletonAnimation)
{
	bool hardwareSkin = false;
	for (int m = 0; m < meshMats.material.GetSize(); ++m)
		hardwareSkin |= meshMats.material[m].material->UsesSkinningParameters();

	SAFE_DELETE(skinRenderBuffer);
	SAFE_DELETE(boneMatrices);

	Skeleton* skeleton = skeletonAnimation->GetSkeleton();
	boneMatrices = new Matrix4[skeleton->GetBoneCount()];

	this->skeletonAnimation = skeletonAnimation;

	if (hardwareSkin)
	{	
		Update();
		return;
	}

	VertexBuffer& origBuff = (*meshMats.vertexBuffer);

	skinRenderBuffer = new VertexBuffer();
	skinRenderBuffer->SetFlags(Dynamic);
	skinRenderBuffer->positions.SetCapacity(origBuff.positions.GetSize());
	skinRenderBuffer->normals.SetCapacity(origBuff.normals.GetSize());

	// copy indices
	skinRenderBuffer->indices.SetSize(origBuff.indices.GetSize());
	memcpy(skinRenderBuffer->indices.GetData(), origBuff.indices.GetData(), sizeof(unsigned int) * origBuff.indices.GetSize());

	// copy over uv coords
	skinRenderBuffer->uvCoords.SetSize(origBuff.uvCoords.GetSize());
	memcpy(skinRenderBuffer->uvCoords.GetData(), origBuff.uvCoords.GetData(), sizeof(UVCoord) * origBuff.uvCoords.GetSize());

	Update();
}

SkeletonAnimation* Mesh::GetSkeletonAnimation()
{
	return skeletonAnimation;
}

unsigned int Mesh::Render(Material* material, int materialIdx, VertexFormat format)
{
	if (callbacks)
		callbacks->PreRender(this, material, materialIdx, format);

	int startIdx = meshMats.material[materialIdx].startIndex;
	int numIndices = meshMats.material[materialIdx].numIndices;

	if (skeletonAnimation)
	{
		Skeleton* skeleton = skeletonAnimation->GetSkeleton();

		if (!material->UsesSkinningParameters())
		{
			skinRenderBuffer->Render(startIdx, numIndices, format);
			return meshMats.material[materialIdx].numIndices/3;
		}
		else
		{
			material->SetSkinningParameters(boneMatrices, skeleton->GetBoneCount());
		}
	}

	meshMats.vertexBuffer->Render(startIdx, numIndices, format);
	return meshMats.material[materialIdx].numIndices/3;
}

unsigned int Mesh::SoftwareSkin(Matrix4* boneMatrices)
{
	VertexBuffer& origBuff = (*meshMats.vertexBuffer);

	// iterate over all positions
	for (int p = 0; p < origBuff.positions.GetSize(); ++p)
	{
		Vector3 position(0,0,0);
		Vector3 normal(0,0,0);

		// get weight and apply to the position
		for (int b = 0; b < 4; ++b)
		{
			int bone = (int)origBuff.bones[p][b];
			if (bone == -1)
				break;

			position += (boneMatrices[bone] * origBuff.positions[p]) * origBuff.weights[p][b];
			normal += (boneMatrices[bone] * origBuff.normals[p]) * origBuff.weights[p][b];
		}

		skinRenderBuffer->positions[p] = position;

		normal.Normalize();
		skinRenderBuffer->normals[p] = normal;
	}
	
	return 0;
}

void Mesh::Update()
{
	if (skeletonAnimation)
	{
		Matrix4* matrices = skeletonAnimation->GetMatrices();
		Skeleton* skeleton = skeletonAnimation->GetSkeleton();

		// convert the skin from mesh space to bone space, then to the animation
		for (int i = 0; i < skeleton->GetBoneCount(); ++i)
			boneMatrices[i] = skeleton->GetBone(i).skinToBone * matrices[i];

		for (int i = 0; i < meshMats.material.GetSize(); ++i)
		{
			if (meshMats.material[i].material->UsesSkinningParameters())
				return;
		}

		SoftwareSkin(boneMatrices);
	}
}

}