#pragma comment(lib, "User32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "OpenGL32.lib")

#include "LinuxWindow.h"
#include "LinuxInput.h"
#include "../Util/Log.h"

#include <X11/Xatom.h>

namespace Siphon
{

/* attributes for a single buffered visual in RGBA format with at least
 * 4 bits per color and a 16 bit depth buffer */
static int attrListSgl[] = {GLX_RGBA, GLX_RED_SIZE, 4,
    GLX_GREEN_SIZE, 4,
    GLX_BLUE_SIZE, 4,
    GLX_DEPTH_SIZE, 16,
    None};

/* attributes for a double buffered visual in RGBA format with at least
 * 4 bits per color and a 16 bit depth buffer */
static int attrListDbl[] = { GLX_RGBA, GLX_DOUBLEBUFFER,
    GLX_RED_SIZE, 4,
    GLX_GREEN_SIZE, 4,
    GLX_BLUE_SIZE, 4,
    GLX_DEPTH_SIZE, 16,
    None };

Window::Window() :
    shouldExit(false),
    input(0)
{

}

Window::Window(char* title, int width, int height, int bits, bool fullscreen)
{
	Create(title, width, height, bits, fullscreen);
}

Window::~Window()
{
	Destroy();
}

bool Window::ShouldExit()
{
    return shouldExit;
}

bool Window::Attach(int hWnd)
{
	return true;
}

void Window::ShowWindow()
{

}

bool Window::Create(char* title, int width, int height, int bits, bool fullscreen)
{
    XVisualInfo *vi;
    Colormap cmap;
    int dpyWidth, dpyHeight;
    int i;
    int glxMajorVersion, glxMinorVersion;
    int vidModeMajorVersion, vidModeMinorVersion;
    XF86VidModeModeInfo **modes;
    int modeNum;
    int bestMode;
    Atom wmDelete;
    ::Window winDummy;
    unsigned int borderDummy;

    this->fullscreen = fullscreen;
    this->width = width;
    this->height = height;
    /* set best mode to current */
    bestMode = 0;
    /* get a connection */
    dpy = XOpenDisplay(0);
    screen = DefaultScreen(dpy);
    //XF86VidModeQueryVersion(dpy, &vidModeMajorVersion, &vidModeMinorVersion);
    //Log::Print("XF86VidModeExtension-Version %d.%d\n", vidModeMajorVersion, vidModeMinorVersion);
    XF86VidModeGetAllModeLines(dpy, screen, &modeNum, &modes);
    /* save desktop-resolution before switching modes */
    deskMode = *modes[0];
    /* look for mode with requested resolution */
    for (i = 0; i < modeNum; i++)
    {
        if ((modes[i]->hdisplay == width) && (modes[i]->vdisplay == height))
        {
            bestMode = i;
        }
    }
    /* get an appropriate visual */
    vi = glXChooseVisual(dpy, screen, attrListDbl);
    if (vi == NULL)
    {
        vi = glXChooseVisual(dpy, screen, attrListSgl);
        doubleBuffered = False;
        Log::Print("Only Singlebuffered Visual!\n");
    }
    else
    {
        doubleBuffered = True;
        Log::Print("Got Doublebuffered Visual!\n");
    }
    //glXQueryVersion(dpy, &glxMajorVersion, &glxMinorVersion);
    //Log::Print("glX-Version %d.%d\n", glxMajorVersion, glxMinorVersion);
    /* create a GLX context */
    ctx = glXCreateContext(dpy, vi, 0, GL_TRUE);
    /* create a color map */
    cmap = XCreateColormap(dpy, RootWindow(dpy, vi->screen),
        vi->visual, AllocNone);
    attr.colormap = cmap;
    attr.border_pixel = 0;

    if (fullscreen)
    {
        XF86VidModeSwitchToMode(dpy, screen, modes[bestMode]);
        XF86VidModeSetViewPort(dpy, screen, 0, 0);
        dpyWidth = modes[bestMode]->hdisplay;
        dpyHeight = modes[bestMode]->vdisplay;
        Log::Print("Resolution %dx%d\n", dpyWidth, dpyHeight);
        XFree(modes);

        /* create a fullscreen window */
        attr.override_redirect = True;
        attr.event_mask = ExposureMask | KeyPressMask | ButtonPressMask |
            StructureNotifyMask;
        win = XCreateWindow(dpy, RootWindow(dpy, vi->screen),
            0, 0, dpyWidth, dpyHeight, 0, vi->depth, InputOutput, vi->visual,
            CWBorderPixel | CWColormap | CWEventMask | CWOverrideRedirect,
            &attr);
        XWarpPointer(dpy, None, win, 0, 0, 0, 0, 0, 0);
		XMapRaised(dpy, win);
        XGrabKeyboard(dpy, win, True, GrabModeAsync,
            GrabModeAsync, CurrentTime);
        XGrabPointer(dpy, win, True, ButtonPressMask,
            GrabModeAsync, GrabModeAsync, win, None, CurrentTime);
    }
    else
    {
        /* create a window in window mode*/
        attr.event_mask = ExposureMask | KeyPressMask | ButtonPressMask |
            StructureNotifyMask;
        win = XCreateWindow(dpy, RootWindow(dpy, vi->screen),
            0, 0, width, height, 0, vi->depth, InputOutput, vi->visual,
            CWBorderPixel | CWColormap | CWEventMask, &attr);
        /* only set window title and handle wm_delete_events if in windowed mode */
        wmDelete = XInternAtom(dpy, "WM_DELETE_WINDOW", True);
        XSetWMProtocols(dpy, win, &wmDelete, 1);
        XSetStandardProperties(dpy, win, title,
            title, None, NULL, 0, NULL);
        XMapRaised(dpy, win);
    }
    /* connect the glx-context to the window */
    glXMakeCurrent(dpy, win, ctx);

    XGetGeometry(dpy, win, &winDummy, &x, &y,
        &this->width, &this->height, &borderDummy, &depth);

    Log::Print("Depth %d\n", depth);
    if (glXIsDirect(dpy, ctx))
        Log::Print("Congrats, you have Direct Rendering!\n");
    else
        Log::Print("Sorry, no Direct Rendering possible!\n");

	return true;
}

void Window::Destroy()								// Properly Kill The Window
{
    Log::Print("destroy window.\n");

    if (ctx)
    {
        if (!glXMakeCurrent(dpy, None, NULL))
        {
            Log::Print("Could not release drawing context.\n");
        }
        glXDestroyContext(dpy, ctx);
        ctx = NULL;
    }
    /* switch back to original desktop resolution if we were in fs */
    if (fullscreen)
    {
        XF86VidModeSwitchToMode(dpy, screen, &deskMode);
        XF86VidModeSetViewPort(dpy, screen, 0, 0);
    }
    XCloseDisplay(dpy);
}

bool Window::HandleMessages()
{
    XEvent event;
    Atom wm_protocols = XInternAtom(dpy, "WM_PROTOCOLS", False);

    while (XPending(dpy) > 0)
    {
        XNextEvent(dpy, &event);

        switch (event.type)
        {
        case ConfigureNotify:
            {
                if ((event.xconfigure.width != width) ||
                    (event.xconfigure.height != height))
                {
                    width = event.xconfigure.width;
                    height = event.xconfigure.height;
                }
            }
            break;
        case ButtonPress: // mouse butons
            {

            }
            break;
         case KeyPress:
            {
                if (input)
                    input->NotifyKeyState(event.xkey.keycode, true);
            }
            break;
        case KeyRelease:
            {
                if (input)
                    input->NotifyKeyState(event.xkey.keycode, true);
            }
            break;
        case ClientMessage:
            {
                if (dpy, event.xclient.message_type == wm_protocols)
                    shouldExit = true;
            }
            break;
        default:
            break;
        }
    }

    return shouldExit;
}

void Window::Display()
{
     glXSwapBuffers(dpy, win);
}

void* Window::GetExtensionAddress(const char* extension)
{
#ifdef GLX_GLXEXT_LEGACY
    return (void*)glXGetProcAddressARB((const GLubyte*)extension);
#else
	return (void*)glXGetProcAddress((const GLubyte*)extension);
#endif
}

int Window::GetHandle()
{

}

bool Window::GetCursorPosition(int& x, int& y)
{
	return true;
}

int Window::SetCursorVisibility(bool visible)
{
	return true;
}

bool Window::IsCursorVisible()
{
	return true;
}

}

