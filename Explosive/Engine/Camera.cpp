#include "Camera.h"

#ifdef WIN32
    #include "Win32Input.h"
#else
    #include "LinuxInput.h"
#endif

#include "Device.h"

namespace Siphon
{

Camera::Camera() :
	//position(0,0,0),
	//view(0,0,1.0),
	//right(1.0,0,0),
	//up(0,1,0),
	rotation(0,0,0),
	bFreeCam(true),
	transform(Matrix4::Identity),
	world(Matrix4::Identity),
	camSpeed(0.025f),
	mouseSensitivity(5.0f)
{
	SetPerspective();
}

void Camera::SetOrthographic(int width, int height)
{
//	projection.
}

void Camera::SetPerspective(float fovy, float aspect, float zfar, float znear)
{
	farPlane = zfar;
	nearPlane = znear;
	fov = fov;

	projection.SetPerspective(fovy, aspect, znear, zfar);
	frustum.SetUpFromProjection(projection);
}
/*
void Camera::Rotate( float angle, float x, float y, float z )
{
	view.Rotate(angle, x, y, z);
	view.Normalize();
}*/

void Camera::Update()
{
	if( bFreeCam )
		FreeCam();

	Transform();
}

void Camera::SetFreeCam( bool bFreeCam )
{
	this->bFreeCam = bFreeCam;
}

void Camera::SetPosition( float x, float y, float z )
{
	world.SetTranslation(x,y,z);
	/*
	position[Vector3::X] = x;
	position[Vector3::Y] = y;
	position[Vector3::Z] = z;*/
}

void Camera::SetPosition(const Vector3& position)
{
	//this->position = position;
	world.SetTranslation(position);
}

void Camera::FreeCam()
{
	int new_x = 0, new_y = 0;
	Input::GetInstance().GetMouseState( new_x, new_y );

	rotation[Vector3::YAW] += (float)new_x;

	//if( rotation[PITCH] + new_y > 90 && rotation[PITCH] + new_y < 270 )
		rotation[Vector3::PITCH] += (float)new_y;

	float invSensitivity = 1000.0f / mouseSensitivity;
	Matrix4 m(Matrix4::Identity),m2(Matrix4::Identity);
	m.RotateY( rotation[Vector3::YAW] / invSensitivity );
	m2.RotateX( rotation[Vector3::PITCH] / invSensitivity );
	m = m2 * m;

	Vector3 up = Vector3( m[4], m[5], m[6] );
	Vector3 view = Vector3( m[8], m[9], m[10] );
	Vector3 right = Vector3( m[0], m[1], m[2] );
	Vector3 position = GetPosition();

	if (Input::GetInstance().KeyDown(Input::KEY_s))
	{
		position =  (view * camSpeed) + position;
	}

	if (Input::GetInstance().KeyDown(Input::KEY_w))
	{
		position =  position - (view * camSpeed);
	}

	if (Input::GetInstance().KeyDown(Input::KEY_a))
	{
		position = position - (right * camSpeed);
	}

	if (Input::GetInstance().KeyDown(Input::KEY_d))
	{
		position = position + (right * camSpeed);
	}

	if (Input::GetInstance().KeyDown(Input::KEY_Space))
	{
		position[Vector3::Y] += camSpeed;
	}

	if (Input::GetInstance().KeyDown(Input::KEY_LControl))
	{
		position[Vector3::Y] -= camSpeed;
	}

	SetLookAt(view, up, right);
	SetPosition(position);
}

/**
 * 4x4 matrix
 *
 * m[0]  m[4]  m[ 8]  m[12]
 * m[1]  m[5]  m[ 9]  m[13]
 * m[2]  m[6]  m[10]  m[14]
 * m[3]  m[7]  m[11]  m[15]
 *
 *   x axis - column 0
 *  y axis - column 1
 *   z axis - column 3
 */
void Camera::Transform()
{
	Vector3 right = GetRight();
	Vector3 up = GetUp();
	Vector3 view = GetView();
	Vector3 position = GetPosition();

	// set the camera view matrix
	transform[ 0] = right[Vector3::X];
	transform[ 4] = right[Vector3::Y];
	transform[ 8] = right[Vector3::Z];

	transform[ 1] = up[Vector3::X];
	transform[ 5] = up[Vector3::Y];
	transform[ 9] = up[Vector3::Z];

	transform[ 2] = view[Vector3::X];
	transform[ 6] = view[Vector3::Y];
	transform[10] = view[Vector3::Z];

	transform[12] = -position.Dot(right);
	transform[13] = -position.Dot(up);
	transform[14] = -position.Dot(view);
/*
	//set world matrix also
	world[ 0] = right[Vector3::X];
	world[ 1] = right[Vector3::Y];
	world[ 2] = right[Vector3::Z];

	world[ 4] = up[Vector3::X];
	world[ 5] = up[Vector3::Y];
	world[ 6] = up[Vector3::Z];

	world[ 8] = view[Vector3::X];
	world[ 9] = view[Vector3::Y];
	world[10] = view[Vector3::Z];
	world.SetTranslation( position[Vector3::X], position[Vector3::Y], position[Vector3::Z] );
*/
	UpdateFrustrum();
}
/*
BOUNDS Camera::GetTransformedBounds()
{
	BOUNDS worldBounds;

	for( int i = 0 ; i < 8; ++i )
	{
		worldBounds.points[i] = world * frustum.points[i];
	}

	return worldBounds;
}*/

void Camera::LookAtM()
{
	transform.SetM();
}

void Camera::LookAt()
{
	Device::GetInstance().SetMatrix(projection, Device::Projection);
	Device::GetInstance().SetMatrix(transform, Device::ModelView);
}

Vector3& Camera::GetRotation()
{
	return rotation;
}

void Camera::SetWorld(Matrix4& world)
{
	this->world = world;
}

const Matrix4& Camera::GetWorld() const
{
	return world;
}

void Camera::SetTransform(Matrix4& transform)
{
	this->transform = transform;
}

Matrix4& Camera::GetTransform()
{
	return transform;
}

void Camera::SetLookAt(const Camera& cam)
{
	world = cam.GetWorld();
}

void Camera::SetLookAt(const Matrix4& worldMatrix)
{
	world = worldMatrix;

	//SetLookAt(worldMatrix.GetColumn(Z), worldMatrix.GetColumn(Y), worldMatrix.GetColumn(X));
	//SetPosition(worldMatrix.GetTranslation());
}

void Camera::SetLookAt(const Vector3& view, const Vector3& up, const Vector3& right)
{
	world.SetColumn(right, 0);
	world.SetColumn(up, 1);
	world.SetColumn(view, 2);

	//this->view = view;
	//this->up = up;
	//this->right = right;
}

void Camera::SetLookAt(const Vector3& pos, const Vector3& target)
{
	Vector3 up = Vector3(0, 1, 0);
	Vector3 view = pos - target;
	view.Normalize();

	Vector3 right = view.CrossProduct(up);
	right.Normalize();

	up = right.CrossProduct(view);
	up.Normalize();

	world.SetColumn(right, 0);
	world.SetColumn(up, 1);
	world.SetColumn(view, 2);
	world.SetTranslation(pos);
	//position = pos;
}

Matrix4& Camera::GetProjection()
{
	return projection;
}

void Camera::UpdateFrustrum()
{
	// get frustrum in to world space
	Matrix4 matModelView = GetTransform();

	Vector3 view = GetView();
	Vector3 right = GetRight();
	Vector3 up = GetUp();
	Vector3 position = GetPosition();

	Vector3 negView(-view[Vector3::X], -view[Vector3::Y], -view[Vector3::Z]);
	matModelView[ 2] = negView[Vector3::X];
	matModelView[ 6] = negView[Vector3::Y];
	matModelView[10] = negView[Vector3::Z];

	matModelView[12] = position.Dot(right);
	matModelView[13] = position.Dot(up);
	matModelView[14] = position.Dot(negView);

	Matrix4 matModelViewProj = matModelView * projection;
	frustum.SetUpFromProjection(matModelViewProj);
}

void Camera::SetSpeed(float speed)
{
	this->camSpeed = speed;
}

}
