//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------

#include "stdlib.h"
#include <string.h>

#include "Log.h"
#include "Device.h"
#include "TGA.h"
#include "File.h"

#ifdef WIN32
    #pragma comment(lib, "OpenGL32.lib")
    #pragma comment(lib, "GLu32.lib")
    //#pragma comment(lib, "GLaux.lib")

    #include "Win32Window.h"
    #include "Win32Time.h"
#else
    #include "LinuxWindow.h"
    #include "LinuxTime.h"
#endif


//--------------------------------------------------------------------------------------

#ifdef WIN32

	PFNGLSAMPLECOVERAGEARBPROC glSampleCoverageARB = 0;

	PFNGLPOINTPARAMETERFARBPROC  glPointParameterfARB = 0;
	PFNGLPOINTPARAMETERFVARBPROC glPointParameterfvARB = 0;

	PFNGLACTIVETEXTUREARBPROC glActiveTextureARB = 0;
	PFNGLCLIENTACTIVETEXTUREARBPROC glClientActiveTexture = 0;

	PFNGLBINDBUFFERARBPROC glBindBufferARB = 0;
	PFNGLGENBUFFERSARBPROC glGenBuffersARB = 0;
	PFNGLBUFFERDATAARBPROC glBufferDataARB = 0;
	PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB = 0;

	PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElements = 0;

	PFNGLCREATESHADEROBJECTARBPROC glCreateShaderObjectARB = 0;
	PFNGLUSEPROGRAMOBJECTARBPROC glUseProgramObjectARB = 0;

	PFNGLSHADERSOURCEARBPROC glShaderSourceARB = 0;
	PFNGLCOMPILESHADERARBPROC glCompileShaderARB = 0;
	PFNGLGETOBJECTPARAMETERIVARBPROC glGetObjectParameterivARB = 0;
	PFNGLATTACHOBJECTARBPROC glAttachObjectARB = 0;
	PFNGLCREATEPROGRAMOBJECTARBPROC glCreateProgramObjectARB = 0;
	PFNGLGETINFOLOGARBPROC glGetInfoLogARB = 0;
	PFNGLLINKPROGRAMARBPROC glLinkProgramARB = 0;
	PFNGLDELETEOBJECTARBPROC glDeleteObjectARB = 0;
	PFNGLGETUNIFORMLOCATIONARBPROC glGetUniformLocationARB = 0;
	PFNGLUNIFORM1IARBPROC glUniform1iARB = 0;
	PFNGLUNIFORM1FARBPROC glUniform1fARB = 0;

	PFNGLVERTEXATTRIBPOINTERARBPROC glVertexAttribPointerARB = 0;
	PFNGLENABLEVERTEXATTRIBARRAYARBPROC glEnableVertexAttribArrayARB = 0;
	PFNGLDISABLEVERTEXATTRIBARRAYARBPROC glDisableVertexAttribArrayARB = 0;

	PFNGLBINDATTRIBLOCATIONARBPROC glBindAttribLocationARB = 0;
	PFNGLGETACTIVEATTRIBARBPROC glGetActiveAttribARB = 0;
	PFNGLGETATTRIBLOCATIONARBPROC glGetAttribLocationARB = 0;

	PFNGLUNIFORMMATRIX2FVARBPROC glUniformMatrix2fvARB = 0;
	PFNGLUNIFORMMATRIX3FVARBPROC glUniformMatrix3fvARB = 0;
	PFNGLUNIFORMMATRIX4FVARBPROC glUniformMatrix4fvARB = 0;

	PFNGLSTRINGMARKERGREMEDYPROC glStringMarkerGREMEDY = 0;

	// FBO
	PFNGLBINDFRAMEBUFFEREXTPROC glBindFramebufferEXT = 0;
	PFNGLBINDRENDERBUFFEREXTPROC glBindRenderbufferEXT = 0;
	PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC glCheckFramebufferStatusEXT = 0;
	PFNGLDELETEFRAMEBUFFERSEXTPROC glDeleteFramebuffersEXT = 0;
	PFNGLDELETERENDERBUFFERSEXTPROC glDeleteRenderbuffersEXT = 0;
	PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC glFramebufferRenderbufferEXT = 0;
	PFNGLFRAMEBUFFERTEXTURE1DEXTPROC glFramebufferTexture1DEXT = 0;
	PFNGLFRAMEBUFFERTEXTURE2DEXTPROC glFramebufferTexture2DEXT = 0;
	PFNGLFRAMEBUFFERTEXTURE3DEXTPROC glFramebufferTexture3DEXT = 0;
	PFNGLGENFRAMEBUFFERSEXTPROC glGenFramebuffersEXT = 0;
	PFNGLGENRENDERBUFFERSEXTPROC glGenRenderbuffersEXT = 0;
	PFNGLGENERATEMIPMAPEXTPROC glGenerateMipmapEXT = 0;
	PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC glGetFramebufferAttachmentParameterivEXT = 0;
	PFNGLGETRENDERBUFFERPARAMETERIVEXTPROC glGetRenderbufferParameterivEXT = 0;
	PFNGLISFRAMEBUFFEREXTPROC glIsFramebufferEXT = 0;
	PFNGLISRENDERBUFFEREXTPROC glIsRenderbufferEXT = 0;
	PFNGLRENDERBUFFERSTORAGEEXTPROC glRenderbufferStorageEXT = 0;

#endif

//--------------------------------------------------------------------------------------
#define LoadGLExtension(f)									\
{				                                            \
	void **p = reinterpret_cast<void**>(&f);				\
	*p = activeWindow->GetExtensionAddress(#f);				\
}

namespace Siphon
{

Device::Device() :
	framesThisSecond(0),
	fps(0),
	ticks(0),
	activeWindow(0)
{

}

Device::~Device()
{

}

void Device::SetClearColour(float r, float g, float b, float a)
{
	clearColour[0] = r;
	clearColour[1] = g;
	clearColour[2] = b;
	clearColour[3] = a;

	glClearColor(r, g, b, a);
}

void Device::SetMatrix(const Matrix4& matrix, MatrixMode mode)
{
	int glMode;
	switch (mode)
	{
	case ModelView:
		glMode = GL_MODELVIEW;
		break;
	case Projection:
		glMode = GL_PROJECTION;
		break;
	case Texture:
		glMode = GL_TEXTURE;
		break;
	}

	glMatrixMode(glMode);
	glLoadMatrixf(matrix.GetData());
}

void Device::DestroyDevice()
{

}

bool Device::CreateDevice(Window* wnd)
{
	if (wnd)
		SetActiveWindow(wnd);

	bool destroyWindow = false;
	if (!activeWindow)
	{
		destroyWindow = true;
		activeWindow = new Window();
		activeWindow->Create("initialising opengl", 100, 100, 32, false);
	}

	SetClearColour(0,0,0);
	glClear( GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); //GL_COLOR_BUFFER_BIT
	EndRender();

	glShadeModel(GL_SMOOTH);
	glEnable(GL_TEXTURE_2D);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST); //GL_NICEST
	glColor3f(1.0f,1.0f,1.0f);
/* //is the lighting issue here?
	glFrontFace(GL_CW);
	glCullFace(GL_BACK);*/
	glEnable(GL_CULL_FACE);

	Log::Print("----------------------------------------\n");
	Log::Print("\tOpenGL Extensions\n");
	Log::Print("----------------------------------------\n");

    //PrintExtensions();
#ifdef WIN32

#ifdef DEBUG
	if ( ExtensionSupported("GL_GREMEDY_string_marker") )
	{
	    LoadGLExtension(glStringMarkerGREMEDY);

        Log::Print("GREMEDY String Marker\t\t\t[OK]\n");
	}
#endif
/*
	if ( ExtensionSupported("GL_ARB_point_parameters") )
	{
	    LoadGLExtension(glPointParameterfARB);
	    LoadGLExtension(glPointParameterfvARB);

		Log::Print("Point Sprites\t\t\t[OK]\n");
	}
*/

	if (ExtensionSupported("GL_ARB_multitexture"))
	{
	    LoadGLExtension(glActiveTextureARB);

		glActiveTextureARB( GL_TEXTURE1_ARB );
		glEnable(GL_TEXTURE_2D);

		glActiveTextureARB( GL_TEXTURE0_ARB );
		glEnable(GL_TEXTURE_2D);

		Log::Print("Multitexture\t\t\t[OK]\n");
	}
	else
	{
		//we NEED multitexture
		Log::Print("Multitexture\t\t\t[FAIL]\n");
		return false;
	}

	if( ExtensionSupported("GL_EXT_draw_range_elements") )
	{
	    LoadGLExtension(glDrawRangeElements);

		Log::Print("Draw Range Element\t\t[OK]\n");
	}
	else
	{
		//required extension
		Log::Print("Draw Range Element\t\t[FAIL]\n");
		return false;
	}

	if( ExtensionSupported("GL_ARB_vertex_buffer_object") )
	{
		bVBOSupported = true;

        LoadGLExtension(glClientActiveTexture);
        LoadGLExtension(glBindBufferARB);
        LoadGLExtension(glGenBuffersARB);
        LoadGLExtension(glBufferDataARB);
        LoadGLExtension(glCreateShaderObjectARB);
        LoadGLExtension(glDeleteBuffersARB);

		Log::Print("Vertex Buffer Objects\t\t[OK]\n");
	}
	else
	{
		Log::Print("Vertex Buffer Objects\t\t[FAIL]\n");
		bVBOSupported = false;
	}
/*
	if( ExtensionSupported("GL_ARB_fragment_shader") &&
		ExtensionSupported("GL_ARB_vertex_shader") &&
		ExtensionSupported("GL_ARB_shader_objects") )
	{
		bShadersSupported = true;

        LoadGLExtension(glBindBufferARB);
        LoadGLExtension(glGenBuffersARB);
        LoadGLExtension(glBufferDataARB);
        LoadGLExtension(glUseProgramObjectARB);
        LoadGLExtension(glShaderSourceARB);
        LoadGLExtension(glCompileShaderARB);
        LoadGLExtension(glGetObjectParameterivARB);
        LoadGLExtension(glAttachObjectARB);
        LoadGLExtension(glCreateProgramObjectARB);
        LoadGLExtension(glGetInfoLogARB);
        LoadGLExtension(glLinkProgramARB);
        LoadGLExtension(glDeleteObjectARB);
        LoadGLExtension(glGetUniformLocationARB);
        LoadGLExtension(glGetUniformLocationARB);

		Log::Print("Vertex/Pixel Shaders\t\t[OK]\n");
	}
	else
	{
		bShadersSupported = false;
		Log::Print("Vertex/Pixel Shaders\t\t[FAIL]\n");
	}
*/

	if (ExtensionSupported("GL_EXT_framebuffer_object"))
	{
	    LoadGLExtension(glBindFramebufferEXT);
	    LoadGLExtension(glBindRenderbufferEXT);
	    LoadGLExtension(glCheckFramebufferStatusEXT);

        LoadGLExtension(glDeleteFramebuffersEXT);
	    LoadGLExtension(glDeleteRenderbuffersEXT);
	    LoadGLExtension(glFramebufferRenderbufferEXT);

	    LoadGLExtension(glFramebufferTexture1DEXT);
	    LoadGLExtension(glFramebufferTexture2DEXT);
	    LoadGLExtension(glFramebufferTexture3DEXT);

	    LoadGLExtension(glGenFramebuffersEXT);
	    LoadGLExtension(glGenRenderbuffersEXT);
	    LoadGLExtension(glGenerateMipmapEXT);

	    LoadGLExtension(glGetFramebufferAttachmentParameterivEXT);
	    LoadGLExtension(glGetRenderbufferParameterivEXT);
	    LoadGLExtension(glIsFramebufferEXT);

	    LoadGLExtension(glIsRenderbufferEXT);
	    LoadGLExtension(glRenderbufferStorageEXT);

		Log::Print("Frame Buffer Object\t\t[OK]\n");
	}
	else
	{
		Log::Print("Frame Buffer Object\t\t[FAIL]\n");
	}

	if (ExtensionSupported("GL_ARB_multisample"))
	{
		bMultisampleSupported = true;
		LoadGLExtension(glSampleCoverageARB);
		Log::Print("Multisample\t\t\t[OK]\n");
	}
	else
	{
		bMultisampleSupported = false;
		Log::Print("Multisample\t\t\t[FAIL]\n");
	}

#endif
/*
	// set up default point sprite settings
	float quadratic[] = {0.0f, 0.01f, 0.0f};
	glTexEnvf(GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_TRUE);
	glEnable(GL_POINT_SPRITE_ARB);

	float maxSize = 0.0f;
    glGetFloatv(GL_POINT_SIZE_MAX_ARB, &maxSize);
	glPointSize(maxSize);

	glPointParameterfARB(GL_POINT_SIZE_MAX_ARB, maxSize);
    glPointParameterfARB(GL_POINT_SIZE_MIN_ARB, 1.0f);
    glTexEnvf(GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_TRUE);
	glPointParameterfvARB(GL_POINT_DISTANCE_ATTENUATION_ARB, quadratic);
*/
	// set projective texture settings
	GLfloat eyePlaneS[] = { 1.0, 0.0, 0.0, 0.0 };
    GLfloat eyePlaneT[] = { 0.0, 1.0, 0.0, 0.0 };
    GLfloat eyePlaneR[] = { 0.0, 0.0, 1.0, 0.0 };
    GLfloat eyePlaneQ[] = { 0.0, 0.0, 0.0, 1.0 };

	glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
    glTexGenfv(GL_S, GL_EYE_PLANE, eyePlaneS);

    glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
    glTexGenfv(GL_T, GL_EYE_PLANE, eyePlaneT);

    glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
    glTexGenfv(GL_R, GL_EYE_PLANE, eyePlaneR);

    glTexGeni(GL_Q, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
    glTexGenfv(GL_Q, GL_EYE_PLANE, eyePlaneQ);

	// set line width
	glLineWidth(2.0);

	// set default texture combiners
	glActiveTextureARB(GL_TEXTURE0_ARB);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT);
	glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_MODULATE);
	glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_MODULATE);
	glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_TEXTURE);
	glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_PREVIOUS_EXT);
	glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_CONSTANT_EXT);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA);
	glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_TEXTURE);
	glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_PREVIOUS_EXT);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA);
	glTexEnvf(GL_TEXTURE_ENV, GL_RGB_SCALE_EXT, 1.0f);
	glTexEnvf(GL_TEXTURE_ENV, GL_ALPHA_SCALE, 1.0f);

	glActiveTextureARB(GL_TEXTURE1_ARB);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT);
	glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_MODULATE);
	glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_MODULATE);
	glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_TEXTURE);
	glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_PREVIOUS_EXT);
	glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_CONSTANT_EXT);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA);
	glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_TEXTURE);
	glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_PREVIOUS_EXT);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA);
	glTexEnvf(GL_TEXTURE_ENV, GL_RGB_SCALE_EXT, 1.0f);
	glTexEnvf(GL_TEXTURE_ENV, GL_ALPHA_SCALE, 1.0f);

	Log::Print("----------------------------------------\n\n");

	if (destroyWindow)
		delete activeWindow;

	return true;
}

void Device::PrintExtensions()
{
    const GLubyte *extensions = glGetString(GL_EXTENSIONS);
    if (!extensions)
		return Log::Print("No available extensions!\n");

    Log::Print("%s\n", extensions);
}

int Device::ExtensionSupported(const char* extension)
{
	const GLubyte *extensions = glGetString(GL_EXTENSIONS);
	if (!extensions)
	{
		Log::Print("No available extensions!\n");
		return 0;
    }

	return FindExtension(extension, (const char*)extensions);
}

int Device::FindExtension(const char* extension, const char* extensions)
{
	char *where, *terminator;

	// Extension names should not have spaces.
	where = (char*)strchr(extension, ' ');
	if (where || *extension == '\0')
	{
		return 0;
	}

	// It takes a bit of care to be fool-proof about parsing the
	// OpenGL extensions string. Don't be fooled by sub-strings
	const char *start = extensions;
	while(1)
	{
		where = (char*) strstr((const char *) start, extension);
		if (!where)
		{
			break;
		}

		terminator = where + strlen(extension);
		if (where == start || *(where - 1) == ' ')
		{
			if (*terminator == ' ' || *terminator == '\0')
			{
				return 1;
			}
		}

		start = terminator;
	}

	return 0;
}

void Device::BeginRender(int flags)
{
#ifdef DEBUG
	GDBSTRING("Device::BeginRender");
#endif

	if (Time::IsValid())
	{
		if (Time::GetInstance().GetTicks() >= ticks )
		{
			fps = framesThisSecond;
			framesThisSecond = 0;
			ticks = Time::GetInstance().GetTicks() + 1000;
		}
		else
		{
			++framesThisSecond;
		}
	}

	SetClearColour(clearColour[0], clearColour[1], clearColour[2], clearColour[3]);

	//glStencilFunc(GL_ALWAYS, 0, ~0 );
	glClear(flags);
}

void Device::EndRender()
{
#ifdef DEBUG
	GDBSTRING("Device::EndRender");
#endif
	activeWindow->Display();
}

void Device::SetActiveWindow(Window* wnd)
{
	activeWindow = wnd;
	ResetDevice(activeWindow->GetWidth(), activeWindow->GetHeight());
}

void Device::ResetDevice(int width, int height)
{
    // should this go in the camera code also?
	glViewport(0, 0, width, height);
}

unsigned int Device::GetFPS()
{
	return fps;
}

int Device::CheckErrors()
{
	int num = 0;
#ifdef WIN32
    GLenum error;
    while ((error = glGetError()) != GL_NO_ERROR)
	{
		Log::Print("Error: %i - %s\n", error, (char*)gluErrorString(error));
		++num;
    }
#endif
	return num;

}

void Device::CheckFramebufferStatus()
{
    GLenum status;
    status = (GLenum) glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
    switch(status) {
        case GL_FRAMEBUFFER_COMPLETE_EXT:
			Log::Print("Framebuffer complete\n");
            break;
        case GL_FRAMEBUFFER_UNSUPPORTED_EXT:
            Log::Print("Unsupported framebuffer format\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
            Log::Print("Framebuffer incomplete, missing attachment\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT_EXT:
            Log::Print("Framebuffer incomplete, duplicate attachment\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
            Log::Print("Framebuffer incomplete, attached images must have same dimensions\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
            Log::Print("Framebuffer incomplete, attached images must have same format\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
            Log::Print("Framebuffer incomplete, missing draw buffer\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
            Log::Print("Framebuffer incomplete, missing read buffer\n");
            break;
        default:
            Log::Print("Uh oh!");
    }
}

void Device::EnableMulisample(bool enable)
{
	if (enable)
		glEnable(GL_MULTISAMPLE_ARB);
	else
		glDisable(GL_MULTISAMPLE_ARB);
}

void Device::SaveScreenShot()
{
	int count = activeWindow->GetWidth() * activeWindow->GetHeight();
	char* data = new char[count * 4];
	glReadPixels(0, 0, activeWindow->GetWidth(), activeWindow->GetHeight(), GL_RGBA, GL_UNSIGNED_BYTE, data);

	CheckErrors();

	if (!data)
	{
		delete[] data;
		return;
	}

	char pixel[4];
	for (int i = 0; i < count; ++i)
	{
		memcpy(pixel, &(data[i * 4]), 4);
		char temp = pixel[0];
		pixel[0] = pixel[2];
		pixel[2] = temp;
		memcpy(&(data[i * 4]), pixel, 4);
	}

	char buffer[256];
	int i = 0;
	while (1)
	{
		sprintf(buffer, "screen%i.tga", i);
		++i;

		File temp(buffer, "rb");
		if (temp.Valid())
			continue;

		TGA tga;
		tga.Save(buffer, data, activeWindow->GetWidth(), activeWindow->GetHeight(), 32);
		break;
	}

	delete[] data;
}

}
