/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 1.3.25
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


public enum MeshFlags {
  MNone = 0x0,
  Dynamic = 0x1<<1,
  Static = 0x1<<2,
  Decal = 0x1<<3,
  NoShadow = 0x1<<4
}
