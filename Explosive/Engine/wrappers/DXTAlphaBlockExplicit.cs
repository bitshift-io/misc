/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 1.3.25
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class DXTAlphaBlockExplicit : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal DXTAlphaBlockExplicit(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(DXTAlphaBlockExplicit obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~DXTAlphaBlockExplicit() {
    Dispose();
  }

  public virtual void Dispose() {
    if(swigCPtr.Handle != IntPtr.Zero && swigCMemOwn) {
      swigCMemOwn = false;
      SiphonGL2PINVOKE.delete_DXTAlphaBlockExplicit(swigCPtr);
    }
    swigCPtr = new HandleRef(null, IntPtr.Zero);
    GC.SuppressFinalize(this);
  }

  public SWIGTYPE_p_short row {
    set {
      SiphonGL2PINVOKE.set_DXTAlphaBlockExplicit_row(swigCPtr, SWIGTYPE_p_short.getCPtr(value));
    } 
    get {
      IntPtr cPtr = SiphonGL2PINVOKE.get_DXTAlphaBlockExplicit_row(swigCPtr);
      SWIGTYPE_p_short ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_short(cPtr, false);
      return ret;
    } 
  }

  public DXTAlphaBlockExplicit() : this(SiphonGL2PINVOKE.new_DXTAlphaBlockExplicit(), true) {
  }

}
