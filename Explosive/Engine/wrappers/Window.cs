/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 1.3.25
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class Window : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal Window(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(Window obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~Window() {
    Dispose();
  }

  public virtual void Dispose() {
    if(swigCPtr.Handle != IntPtr.Zero && swigCMemOwn) {
      swigCMemOwn = false;
      SiphonGL2PINVOKE.delete_Window(swigCPtr);
    }
    swigCPtr = new HandleRef(null, IntPtr.Zero);
    GC.SuppressFinalize(this);
  }

  public Window() : this(SiphonGL2PINVOKE.new_Window(), true) {
  }

  public bool Attach(int hWnd) {
    bool ret = SiphonGL2PINVOKE.Window_Attach(swigCPtr, hWnd);
    return ret;
  }

  public bool Create(string title, int width, int height, int bits, bool fullscreen) {
    bool ret = SiphonGL2PINVOKE.Window_Create(swigCPtr, title, width, height, bits, fullscreen);
    return ret;
  }

  public void Destroy() {
    SiphonGL2PINVOKE.Window_Destroy(swigCPtr);
  }

  public void ShowWindow() {
    SiphonGL2PINVOKE.Window_ShowWindow(swigCPtr);
  }

  public void Display() {
    SiphonGL2PINVOKE.Window_Display(swigCPtr);
  }

  public bool HandleMessages() {
    bool ret = SiphonGL2PINVOKE.Window_HandleMessages(swigCPtr);
    return ret;
  }

  public SWIGTYPE_p_void GetExtensionAddress(string extension) {
    IntPtr cPtr = SiphonGL2PINVOKE.Window_GetExtensionAddress(swigCPtr, extension);
    SWIGTYPE_p_void ret = (cPtr == IntPtr.Zero) ? null : new SWIGTYPE_p_void(cPtr, false);
    return ret;
  }

  public int GetWidth() {
    int ret = SiphonGL2PINVOKE.Window_GetWidth(swigCPtr);
    return ret;
  }

  public int GetHeight() {
    int ret = SiphonGL2PINVOKE.Window_GetHeight(swigCPtr);
    return ret;
  }

  public int GetHandle() {
    int ret = SiphonGL2PINVOKE.Window_GetHandle(swigCPtr);
    return ret;
  }

}
