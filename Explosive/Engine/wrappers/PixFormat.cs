/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 1.3.25
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


public enum PixFormat {
  PF_ARGB,
  PF_RGB,
  PF_DXT1,
  PF_DXT2,
  PF_DXT3,
  PF_DXT4,
  PF_DXT5,
  PF_UNKNOWN
}
