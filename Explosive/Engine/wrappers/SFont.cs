/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 1.3.25
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class SFont : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal SFont(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(SFont obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~SFont() {
    Dispose();
  }

  public virtual void Dispose() {
    if(swigCPtr.Handle != IntPtr.Zero && swigCMemOwn) {
      swigCMemOwn = false;
      SiphonGL2PINVOKE.delete_SFont(swigCPtr);
    }
    swigCPtr = new HandleRef(null, IntPtr.Zero);
    GC.SuppressFinalize(this);
  }

  public bool CreateFont(string file) {
    bool ret = SiphonGL2PINVOKE.SFont_CreateFont(swigCPtr, file);
    return ret;
  }

  public bool DrawText(float x, float y, string text) {
    bool ret = SiphonGL2PINVOKE.SFont_DrawText(swigCPtr, x, y, text);
    return ret;
  }

  public bool Print(float x, float y, string text) {
    bool ret = SiphonGL2PINVOKE.SFont_Print(swigCPtr, x, y, text);
    return ret;
  }

  public void Render() {
    SiphonGL2PINVOKE.SFont_Render(swigCPtr);
  }

  public void Update() {
    SiphonGL2PINVOKE.SFont_Update(swigCPtr);
  }

  public SFont() : this(SiphonGL2PINVOKE.new_SFont(), true) {
  }

}
