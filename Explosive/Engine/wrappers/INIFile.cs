/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 1.3.25
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

public class INIFile : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal INIFile(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(INIFile obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~INIFile() {
    Dispose();
  }

  public virtual void Dispose() {
    if(swigCPtr.Handle != IntPtr.Zero && swigCMemOwn) {
      swigCMemOwn = false;
      SiphonGL2PINVOKE.delete_INIFile(swigCPtr);
    }
    swigCPtr = new HandleRef(null, IntPtr.Zero);
    GC.SuppressFinalize(this);
  }

  public bool Load(string path) {
    bool ret = SiphonGL2PINVOKE.INIFile_Load(swigCPtr, path);
    return ret;
  }

  public SWIGTYPE_p_std__string GetValue(string section, string key) {
    SWIGTYPE_p_std__string ret = new SWIGTYPE_p_std__string(SiphonGL2PINVOKE.INIFile_GetValue__SWIG_0(swigCPtr, section, key), true);
    return ret;
  }

  public void GetValue(string section, string key, SWIGTYPE_p_int value) {
    SiphonGL2PINVOKE.INIFile_GetValue__SWIG_1(swigCPtr, section, key, SWIGTYPE_p_int.getCPtr(value));
    if (SiphonGL2PINVOKE.SWIGPendingException.Pending) throw SiphonGL2PINVOKE.SWIGPendingException.Retrieve();
  }

  public void GetValue(string section, string key, SWIGTYPE_p_bool value) {
    SiphonGL2PINVOKE.INIFile_GetValue__SWIG_2(swigCPtr, section, key, SWIGTYPE_p_bool.getCPtr(value));
    if (SiphonGL2PINVOKE.SWIGPendingException.Pending) throw SiphonGL2PINVOKE.SWIGPendingException.Retrieve();
  }

  public void SetValue(string section, string key, SWIGTYPE_p_std__string value) {
    SiphonGL2PINVOKE.INIFile_SetValue(swigCPtr, section, key, SWIGTYPE_p_std__string.getCPtr(value));
    if (SiphonGL2PINVOKE.SWIGPendingException.Pending) throw SiphonGL2PINVOKE.SWIGPendingException.Retrieve();
  }

  public INIFile() : this(SiphonGL2PINVOKE.new_INIFile(), true) {
  }

}
