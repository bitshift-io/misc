#ifndef _SIPHON_WINDOW_
#define _SIPHON_WINDOW_

#define GLX_GLXEXT_LEGACY

#include <GL/glx.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <X11/extensions/xf86vmode.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <stdio.h>

namespace Siphon
{

class Input;

class Window
{
	friend class Input;

public:

	Window();
	Window(char* title, int width, int height, int bits, bool fullscreen);
	~Window();

	bool Attach(int hWnd);
	bool Create(char* title, int width, int height, int bits, bool fullscreen);
	void Destroy();

	void ShowWindow();

	void Display();

	// retuens true if quit message recived
	bool HandleMessages();

	void* GetExtensionAddress(const char* extension);

	int GetWidth() { return width; }
	int GetHeight() { return height; }

	// returns cursor position relative to client area
	// returns true if in the client area
	bool GetCursorPosition(int& x, int& y);
	int SetCursorVisibility(bool visible);
	bool IsCursorVisible();

	int GetHandle();
	bool HasFocus() {  }

	bool ShouldExit();

protected:
    ::Display *dpy;
    int screen;
    ::Window win;
    GLXContext ctx;
    XSetWindowAttributes attr;
    bool fullscreen;
    bool doubleBuffered;
    XF86VidModeModeInfo deskMode;
    int x;
    int y;
    unsigned int width;
    unsigned int height;
    unsigned int depth;
    bool shouldExit;
    Input* input;
};

}

#endif
