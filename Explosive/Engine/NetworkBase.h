#ifndef _SIPHON_NETWORKBASE_
#define _SIPHON_NETWORKBASE_

#pragma comment(lib, "Ws2_32.lib")

#include <winsock.h>
#include "Singleton.h"
#include "Log.h"
#include "Array.h"

namespace Siphon
{

struct ClientInfo
{
	std::string ip;
};

struct ServerInfo
{
	std::string ip;
	std::string name;
	int			numPlayers;
};

// message used to find games
struct BroadcastMsg
{
	unsigned int	id;
	unsigned long	addr;
	int				numPlayers;
	char			name[256];
};

class SocketStream
{
public:
	enum Type
	{
		Reliable,
		UnReliable
	};

	enum State
	{
		Read	= 0x1 << 0,
		Write	= 0x1 << 1,
		Error	= 0x1 << 2
	};

	SocketStream(Type type) : type(type), sock(0), buffer(0), bufferSize(0) { }

	bool Close();

	// for connecting to a server (sendPort only used for UDP)
	// pass NULL for addr to connect on broadcast address (UDP only)
	bool Connect(const char* addr, int recvPort, int sendPort = -1);

	bool UDPConnect(int port);
	bool UDPSendTo(const void* data, int size, const char* address, int port);

	// set up socket to listen for a connection (sendPort only used for UDP)
	// pass NULL for addr to listen on braodcast address (UDP only)
	bool Listen(const char* addr, int recvPort, int sendPort = -1);
	
	bool Accept(SocketStream& stream);

	// get socket address
	unsigned long GetAddress();

	// are we connected?
	bool IsValid();

	// sned and receive directly to the socket
	bool Send(const void* data, int size);

	// last arg is for UDP, getting a list of ip's we received data from
	// may contain duplicates
	int Receive(void* data, unsigned int size, Array<ServerInfo>* recvList = NULL);

	// check for readability/writeability
	// readable:
	// is there data to be read from the stream?
	// OR will accept call complete with out blocking
	// writeable:
	// send is garanted not to block
	// see: State enum above
	int GetSocketState();

	// move data from socket stream to buffer
	// last arg is for UDP, getting a list of ip's we received data from
	// may contain duplicates
	int Update(Array<ServerInfo>* recvList = NULL);

	// how much data is left in the buffer
	int GetBufferSize() { return bufferSize; }

	// retreive data from the buffer, this removes the data from the buffer
	int GetBufferData(void* data, int size);

	// peek in to the buffer, with out removing the data from the buffer
	int PeekBufferData(void* data, int size);

	/**  
	 * Given an address string, determine if it's a dotted-quad IP address
	 * or a domain address.  If the latter, ask DNS to resolve it.  In
	 * either case, return resolved IP address.  If we fail, we return
	 * INADDR_NONE.
	 */
	unsigned long LookupAddress(const char* hostAddr);

	void ConvertToString(unsigned long addr, std::string& str);

	/**
	 * get local host ip
	 */
	unsigned long GetLocalAddress();

	SOCKET& GetSocket() { return sock; }

protected:
	SOCKET CreateSocket();

	Type	type;
	SOCKET	sock;
	char*	buffer;
	int		bufferSize;

	sockaddr_in sendAddr;
	sockaddr_in recvAddr;
};

class NetworkBase
{
public:
	enum NetworkFlags
	{
		NF_ReliableOnly		= 0x1 << 0,	// TCP only, no UDP
		NF_ConnectExternal	= 0x1 << 1,	// want to connect to a non siphon application (eg. website)
	};

	NetworkBase() : clientId(-1) { }
	virtual ~NetworkBase() { }

	virtual bool Init()
	{/*
		// start winsock
		WSAData wsaData;
		int nCode;
		if ((nCode = WSAStartup(MAKEWORD(2, 2), &wsaData)) != 0) 
		{
			Log::Print("WSAStartup returned error code %i\n", nCode);
			return false;
		}
*/
		return true;
	}

	virtual void Shutdown()
	{/*
		// shutdown winsock
		WSACleanup();*/
	}

	static bool InitSocket()
	{
		// start winsock
		WSAData wsaData;
		int nCode;
		if ((nCode = WSAStartup(MAKEWORD(2, 2), &wsaData)) != 0) 
		{
			Log::Print("WSAStartup returned error code %i\n", nCode);
			return false;
		}

		return true;
	}

	static void ShutdownSocket()
	{
		// shutdown winsock
		WSACleanup();
	}

	virtual unsigned long GetLocalAddress() = 0;

	unsigned int GetClientId() { return clientId; }

	//void	OutputDebug(RDPStream* stream);

protected:
	unsigned int clientId;
};

}

#endif