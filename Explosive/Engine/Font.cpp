//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "Font.h"
#include "Log.h"

namespace Siphon
{

//--------------------------------------------------------------------------------------

bool Font::Load(const char* name)
{
	this->name = name;

	// crop off up to the slash
	const char* slash = strchr(name, '/');
	if (!slash)
		slash = strchr(name, '\\');

	if (!slash)
		slash = name;
	else
		++slash;

	// crop off the .fnt and make it .tga
	char textureName[256];
	strcpy(textureName, slash);
	strcpy(&textureName[strlen(textureName) - 3], "tga");

	// load the tga
	texture = TextureMgr::LoadTex(textureName);
	if (!texture)
	{
		Log::Print("Failed to load font texture: %s\n", textureName);
		return false;
	}

	File fntFile(name, "rt");
	if (!fntFile.Valid())
	{
		Log::Print("Failed to load font: %s\n", name);
		return false;
	}

	//
	// read the head:
	//
	// info face="Arial" size=60 bold=0 italic=0 charset="ANSI" stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=1,1
	// common lineHeight=60 base=48 scaleW=256 scaleH=256 pages=1

	char fntName[256];
	int size;
	int bold;
	int italic;
	char charSet[256];
	int stretchH;
	int smooth;
	int aa;
	int padding[4];
	int spacing[2];

	// convert space in between " marks
	// to underscores for easy parsing
	char buffer[256];
	fntFile.GetStr(buffer, 256);
	int lineLen = strlen(buffer);
	bool firstSpeechMark = false;
	for (int i = 0; i < lineLen; ++i)
	{
		if (buffer[i] == '\"')
			firstSpeechMark = !firstSpeechMark;

		if (firstSpeechMark && buffer[i] == ' ')
			buffer[i] = '_';
	}

	sscanf(buffer, "info face=%s size=%i bold=%i italic=%i charset=%s stretchH=%i smooth=%i aa=%i padding=%i,%i,%i,%i spacing=%i,%i", 
		&fntName, &size, &bold, &italic, &charSet, &stretchH, &smooth, &aa, &padding[0], &padding[1], &padding[2], &padding[3], &spacing[0], &spacing[1]);

	int lineHeight;
	int base;
	int scaleW;
	int scaleH;
	int pages;

	// have to do a funky here o.0
	fntFile.ReadString("%s", &buffer);
	fntFile.ReadString(" lineHeight=%i base=%i scaleW=%i scaleH=%i pages=%i", 
		&lineHeight, &base, &scaleW, &scaleH, &pages);

	this->lineHeight = lineHeight + spacing[1];
	texWidth = scaleW;
	texHeight = scaleH;

	while (!fntFile.EndOfFile())
	{
		char lineType[256];
		fntFile.ReadString("%s", lineType);

		// read in the char description
		if (strcmp(lineType, "char") == 0)
		{
			//  id=32   x=0     y=0     width=1     height=0     xoffset=0     yoffset=60    xadvance=14    page=0
			int id;
			int x;
			int y;
			int width;
			int height;
			int xOffset;
			int yOffset;
			int xAdvance;
			int page;

			char buffer[256];
			fntFile.GetStr(buffer, 256);
			int count = sscanf(buffer, " id=%i   x=%i     y=%i     width=%i     height=%i     xoffset=%i     yoffset=%i    xadvance=%i    page=%i", 
				&id, &x, &y, &width, &height, &xOffset, &yOffset, &xAdvance, &page);

			if (count != 9)
			{
				Log::Print("Failed to parse font character: %s\n", name);
				return false;
			}

			characters[id].x = x; 
			characters[id].y = y; 
			characters[id].width = width; 
			characters[id].height = height; 
			characters[id].xOffset = xOffset; 
			characters[id].yOffset = yOffset; 
			characters[id].xAdvance = xAdvance + spacing[0]; 

			GenerateRenderList(id);
		}
	}

	return true;
}

void Font::GenerateRenderList(int index)
{
	CharDesc& charDesc = characters[index];
	charDesc.renderList = glGenLists(1);

	glColor3f(1.0f,1.0f,1.0f);

	glNewList(charDesc.renderList, GL_COMPILE);
	{
		glBegin(GL_QUADS); 

		// NOTE: invert the y axis of uv coords as tga is assumed to be upside down

		// Vertex Coord (Top Left)
		glTexCoord2f(float(charDesc.x) / float(texWidth), 1.0f - float(charDesc.y) / float(texHeight));
		glVertex2i(charDesc.xOffset, charDesc.yOffset);                           

		// Vertex Coord (Bottom Left)
		glTexCoord2f(float(charDesc.x) / float(texWidth), 1.0f - float(charDesc.y + charDesc.height) / float(texHeight));
		glVertex2d(charDesc.xOffset, (charDesc.yOffset + charDesc.height));  

		// Vertex Coord (Bottom Right)
		glTexCoord2f(float(charDesc.x + charDesc.width) / float(texWidth), 1.0f - float(charDesc.y + charDesc.height) / float(texHeight));
		glVertex2i((charDesc.xOffset + charDesc.width), (charDesc.yOffset + charDesc.height));
		
		// Vertex Coord (Top Right)
		glTexCoord2f(float(charDesc.x + charDesc.width) / float(texWidth), 1.0f - float(charDesc.y) / float(texHeight));
		glVertex2i((charDesc.xOffset + charDesc.width), charDesc.yOffset);                          

		glEnd(); 
		glTranslated(charDesc.xAdvance, 0, 0);                      
	}
	glEndList(); 
}

bool Font::Print(float x, float y, float scale, const char* text, ...)
{
	va_list ap;
	char newText[1024];
 	if( text == 0 )
    	return false;

  	va_start(ap, text);
  	vsprintf(newText, text, ap);
  	va_end(ap);

	this->text.Insert(Text(newText, x, y, scale));
	return true;
}

bool Font::RenderText(float x, float y, float scale, const char* text, ...)
{
	RenderBegin();

	va_list ap;
	char newText[1024];
 	if( text == 0 )
    	return false;

  	va_start(ap, text);
  	vsprintf(newText, text, ap);
  	va_end(ap);

	RenderTextInternal(x, y, scale, newText);
	RenderEnd();
	return true;
}

void Font::RenderBegin()
{
	blendOn = glIsEnabled(GL_BLEND);
  	glGetIntegerv(GL_BLEND_SRC, &blendSrc);
	glGetIntegerv(GL_BLEND_DST, &blendDst);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glMatrixMode(GL_PROJECTION); // and initalize Ortho mode
  	glPushMatrix();
  	glLoadIdentity();
  	glOrtho(0.0f, 800,600, 0.0f,-1.0f,1.0f);

  	glMatrixMode(GL_MODELVIEW);
  	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(GL_FALSE);

	texture->Bind();
	glDisable(GL_LIGHTING);
	glColor3f(1.0f,1.0f,1.0f);
}

void Font::RenderEnd()
{
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);

	glMatrixMode(GL_PROJECTION);
  	glPopMatrix();
  	glMatrixMode(GL_MODELVIEW);
  	glPopMatrix();

	if (blendOn)
		glEnable(GL_BLEND);
	else
		glDisable(GL_BLEND);

	glBlendFunc(blendSrc, blendDst);
	Texture::RestoreDefault();
}

bool Font::RenderTextInternal(float x, float y, float scale, const char* text)
{
	glLoadIdentity();
  	glTranslated(x, y, 0);
	glScalef(scale, scale, 1.0f);

	for (unsigned int i = 0 ; i < strlen(text); ++i)
	{
		if (text[i] == '\n')
		{
			y += lineHeight * scale;
			glLoadIdentity();
			glTranslated(x, y, 0);
			glScalef(scale, scale, 1.0f);
		}
		else
		{
			glCallList(characters[text[i]].renderList);
		}
	}

	return true;
}

void Font::Render()
{
	RenderBegin();

	for (int i = 0; i < text.GetSize(); ++i)
	{
		Text& txt = text[i];
		RenderTextInternal(txt.x, txt.y, txt.scale, txt.text);
	}

	RenderEnd();
}

void Font::Update()
{
	text.SetSize(0);
}

}
