#include "Renderer.h"
#include "Macros.h"

namespace Siphon
{

Renderer::Renderer() : material(0), lightNodes(0), camera(0), renderTarget(0), time(0), numClipPlanes(0)
{

}

Renderer::~Renderer()
{
	// clean up meshes and materials
	MaterialNode* matIt = material;
	MaterialNode* matLast = 0;

	MeshNode* meshIt;
	MeshNode* meshLast;

	while (matIt)
	{
		matLast = matIt;
		matIt = matIt->pNext;

		meshIt = matLast->meshList;
		while (meshIt)
		{
			meshLast = meshIt;
			meshIt = meshIt->pNext;
			SAFE_DELETE(meshLast);
		}
		SAFE_DELETE(matLast);
	}

	// delete light nodes
	LightNode* lightIt = lightNodes;
	LightNode* lightLast;
	while(lightIt)
	{
		lightLast = lightIt;
		lightIt = lightIt->pNext;

		SAFE_DELETE(lightLast);
	}
}

void Renderer::CullMeshes(Camera* camera)
{
	//camera->GetFrustum().Draw();

	// iterate through meshes checking for visibility
	// this should be done in update!
	for (unsigned int i = 0; i < meshes.GetSize(); ++i)
	{
		// todo: transform frustrum to world space
		Frustum::State state = camera->GetFrustum().CullCheck(meshes[i]->GetBoundSphere());
		meshes[i]->SetCullState(state);
	}
}

unsigned int Renderer::Render()
{
	if (!camera)
		return 0;

	unsigned int polys = 0;
	MaterialNode* matIt = material;
	MeshNode* meshIt = 0;
	unsigned int pass = 0;

	int faces = 1;
	if (renderTarget)
		faces = renderTarget->Lock();

	for (int i = 0; i < numClipPlanes; ++i)
		clipPlanes[i].BindClipPlane(i);

	for (int i = 0; i < faces; ++i)
	{
		if (renderTarget)
			renderTarget->BindTarget(i, true);

		for (unsigned int p = 0; p < postEffects.GetSize(); ++p)
			postEffects[p]->PreRender();

		while (matIt)
		{
			// can now bind for each pass elegantly here :)
			if (matIt->material)
			{
				int pass = 0;
				while ((pass = matIt->material->DoPass()) != -1)
				{
					// this binds textures, only needs to be done once per pass
					matIt->material->SetPassParameters(pass);

					//if (matIt->material->UsesTimeParameter())
					{
						//Log::Print("time: %f\n", time);
						matIt->material->SetTimeParameter(time);
					}

					matIt->material->GetEffect()->BindParameters();

					meshIt = matIt->meshList;
					while(meshIt)
					{
						if (meshIt->mesh->IsVisible())
						{
							// this needs to be done for each sub mesh unfortunately
							matIt->material->SetMatrixParameters(camera->GetProjection(), camera->GetTransform(), meshIt->mesh->GetTransform(), camera->GetPosition());

							// bind lights only if lights are used
							if (matIt->material->UsesLightParameters())
								matIt->material->SetLightParameters(lights, GetClosestLights(meshIt->mesh->GetBoundSphere(), lights, 8));

							// bind projectors
							if (matIt->material->UsesProjectorParameters() && projectors.GetSize())
								matIt->material->SetProjectorParameters(meshIt->mesh->GetTransform(), projectors.GetData(), projectors.GetSize());

							// render
							polys += meshIt->mesh->Render(matIt->material, meshIt->materialIdx, matIt->material->GetVertexFormat(pass));
						}
						meshIt = meshIt->pNext;
					}

					// this causes significat loss of FPS
					if (matIt->pNext == 0)
						matIt->material->ClearParameters();
				}
			}

			matIt = matIt->pNext;
		}

		for (unsigned int p = 0; p < postEffects.GetSize(); ++p)
			postEffects[p]->PostRender();
	}

	Plane::RestoreDefaultClipPlanes();

	if (renderTarget)
		renderTarget->UnLock();

	return polys;
}

bool Renderer::RemoveMesh(Mesh* mesh)
{
	if (!mesh)
		return false;

	// remove from mesh list
	bool found = false;
	for (int i = 0; i < meshes.GetSize(); ++i)
	{
		if (meshes[i] == mesh)
		{
			meshes.Remove(i);
			found = true;
			break;
		}
	}

	// if its not even in the renderer, return true
	if (!found)
		return true;

	int matCount = 0;

	// remove from render list
	MaterialNode* matIt = material;
	MaterialNode* prevMatIt = matIt;
	MeshNode* meshIt;
	MeshNode* prevMeshIt;

	// iterate over materials
	while (matIt)
	{
		if (matIt->material)
		{
			// iterate over meshes
			meshIt = matIt->meshList;
			prevMeshIt = meshIt;

			// if only a single mesh...
			if (meshIt && meshIt->mesh == mesh && meshIt->pNext == 0)
			{
				// removing the head node!
				if (matIt == material)
				{
					material = matIt->pNext;
					prevMatIt = material;

					delete meshIt;
					delete matIt;

					matIt = material;
				}
				else
				{
					// remove material
					prevMatIt->pNext = matIt->pNext;

					delete meshIt;
					delete matIt;

					matIt = prevMatIt->pNext;
				}

				++matCount;

				if (matCount >= mesh->meshMats.material.GetSize())
					return true;

				continue;
			}

			while(meshIt)
			{
				if (meshIt->mesh == mesh)
				{
					// TODO: check to see if there is only 1 mesh, if so,
					// remove the material
					if (prevMeshIt == meshIt) // deleting the head mesh node
						matIt->meshList = meshIt->pNext;
					else
						prevMeshIt->pNext = meshIt->pNext;

					delete meshIt;
					++matCount;

					if (matCount >= mesh->meshMats.material.GetSize())
						return true;

					meshIt = 0;
					continue;
				}

				prevMeshIt = meshIt;
				meshIt = meshIt->pNext;
			}

		}
		prevMatIt = matIt;
		matIt = matIt->pNext;
	}

	if (matCount >= mesh->meshMats.material.GetSize())
		return true;

	return false;
}

void Renderer::InsertMesh(Mesh* mesh)
{
	if (!mesh)
		return;

	meshes.Insert(mesh);

	unsigned int i;
	for (i = 0; i < mesh->meshMats.material.GetSize(); ++i)
	{
		MeshNode* front = new MeshNode;
		front->pNext = 0;
		front->mesh = mesh;
		front->materialIdx = i;

		//see if material exists
		MaterialNode* iterator = material;
		if (!mesh->meshMats.material[i].material)
		{
			delete front;
			continue;
		}

		while (iterator)
		{
			if (mesh->meshMats.material[i].material == iterator->material)
			{
				front->pNext = iterator->meshList;
				iterator->meshList = front;
				break;
			}

			iterator = iterator->pNext;
		}

		// material not found!
		if (iterator == 0)
		{
			MaterialNode* newMatNode = InsertMaterial(mesh->meshMats.material[i].material);
			newMatNode->meshList = front;
		}
	}
}

Renderer::MaterialNode* Renderer::InsertMaterial(Material* mat)
{
	MaterialNode* insertLocation = 0;

	MaterialNode* iterator = material;
	MaterialNode* prev = 0;

	while (iterator)
	{
		if ((*mat) < (*iterator->material))
		{
			MaterialNode* next = new MaterialNode;
			next->material = mat;
			next->meshList = 0;
			next->pNext = iterator;
			if (prev)
			{
				prev->pNext = next;
			}
			else
			{
				material = next;
			}

			return next;
		}
		else if (iterator->pNext == 0)
		{
			MaterialNode* next = new MaterialNode;
			next->material = mat;
			next->meshList = 0;
			next->pNext = 0;
			iterator->pNext = next;
			return next;
		}
		prev = iterator;
		iterator = iterator->pNext;
	}
	if (!iterator)
	{
		MaterialNode* next = new MaterialNode;
		next->meshList = 0;
		next->material = mat;
		next->pNext = 0;

		material = next;

		return next;
	}

	return 0;
}

void Renderer::InsertLight(Light* light)
{
	LightNode* front = new LightNode;
	front->pNext = lightNodes;
	front->light = light;
	lightNodes = front;
}

void Renderer::RemoveLight(Light* light)
{
	LightNode* curNode = lightNodes;
	LightNode* prevNode = lightNodes;

	while (curNode)
	{
		if (curNode->light == light)
		{
			prevNode->pNext = curNode->pNext;
			break;
		}

		prevNode = curNode;
		curNode = curNode->pNext;
	}
}

void Renderer::InsertPostEffect(PostEffect* pEffect)
{
	postEffects.Insert(pEffect);
}

void Renderer::InsertProjector(Projector* projector)
{
	projectors.Insert(projector);
}

void Renderer::SetClipPlane(const Plane& plane, int idx)
{
	clipPlanes[idx] = plane;
	numClipPlanes = max(numClipPlanes, idx + 1);
}

}
