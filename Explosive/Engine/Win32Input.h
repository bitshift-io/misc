#ifndef _SIPHONWIN32INPUT_
#define _SIPHONWIN32INPUT_

#include <string>

#include "Array.h"
#include "Singleton.h"
#include "dx9/dinput.h"

namespace Siphon
{

class Window;

#define gInput Input::GetInstance()

class Input : public /*Siphon::*/Singleton<Input>
{
	friend class Window;

public:
	enum JoystickAxis
	{
		X,
		Y,
		Z
	};

	enum Key
	{
		KEY_Space = DIK_SPACE,

		// key pad
		KEY_KP0 = DIK_NUMPAD0,
		KEY_KP1 = DIK_NUMPAD1,
		KEY_KP2 = DIK_NUMPAD2,
		KEY_KP3 = DIK_NUMPAD3,
		KEY_KP4 = DIK_NUMPAD4,
		KEY_KP5 = DIK_NUMPAD5,
		KEY_KP6 = DIK_NUMPAD6,
		KEY_KP7 = DIK_NUMPAD7,
		KEY_KP8 = DIK_NUMPAD8,
		KEY_KP9 = DIK_NUMPAD9,

		KEY_0 = DIK_0,
		KEY_1 = DIK_1,
		KEY_2 = DIK_2,
		KEY_3 = DIK_3,
		KEY_4 = DIK_4,
		KEY_5 = DIK_5,
		KEY_6 = DIK_6,
		KEY_7 = DIK_7,
		KEY_8 = DIK_8,
		KEY_9 = DIK_9,

		KEY_a = DIK_A,
		KEY_b = DIK_B,
		KEY_c = DIK_C,
		KEY_d = DIK_D,
		KEY_e = DIK_E,
		KEY_f = DIK_F,
		KEY_g = DIK_G,
		KEY_h = DIK_H,
		KEY_i = DIK_I,
		KEY_j = DIK_J,
		KEY_k = DIK_K,
		KEY_l = DIK_L,
		KEY_m = DIK_M,
		KEY_n = DIK_N,
		KEY_o = DIK_O,
		KEY_p = DIK_P,
		KEY_q = DIK_Q,
		KEY_r = DIK_R,
		KEY_s = DIK_S,
		KEY_t = DIK_T,
		KEY_u = DIK_U,
		KEY_v = DIK_V,
		KEY_w = DIK_W,
		KEY_x = DIK_X,
		KEY_y = DIK_Y,
		KEY_z = DIK_Z,

		KEY_F1 = DIK_F1,
		KEY_F2 = DIK_F2,
		KEY_F3 = DIK_F3,
		KEY_F4 = DIK_F4,
		KEY_F5 = DIK_F5,
		KEY_F6 = DIK_F6,
		KEY_F7 = DIK_F7,
		KEY_F8 = DIK_F8,
		KEY_F9 = DIK_F9,
		KEY_F10 = DIK_F10,
		KEY_F11 = DIK_F11,
		KEY_F12 = DIK_F12,

		KEY_Esc = DIK_ESCAPE,
		KEY_Enter = DIK_RETURN,
		KEY_BackSpace = DIK_BACKSPACE,
		KEY_Tab = DIK_TAB,
		KEY_Pause = DIK_PAUSE,
		KEY_ScrollLock = DIK_SCROLL,
		KEY_PrintScreen = DIK_SYSRQ,
		KEY_Delete = DIK_DELETE,

		KEY_LShift = DIK_LSHIFT,
		KEY_RShift = DIK_RSHIFT,
		KEY_LControl = DIK_LCONTROL,
		KEY_RControl = DIK_RCONTROL,
		KEY_CapsLock = DIK_CAPSLOCK,
		KEY_LAlt = DIK_LALT,
		KEY_RAlt = DIK_RALT,

		KEY_Home = DIK_HOME,
		KEY_Left = DIK_LEFT,
		KEY_Up = DIK_UP,
		KEY_Down = DIK_DOWN,
		KEY_Right = DIK_RIGHT,
		KEY_PageUp = DIK_PRIOR,
		KEY_PageDown = DIK_NEXT,
		KEY_End = DIK_END,
		KEY_Begin = DIK_HOME,
	};

	Input();
	~Input();

	bool	CreateInput(Window* window, bool background = false);
	void	DestroyInput();

	void	Update();

	/**
	 * Determines if they key is currently down
	 */
	bool	KeyDown(int key);

	/**
	 * determines if the key was pressed
	 */
	bool	KeyPressed(int key);

	/**
	 * looks for any pressed keys, and returns it
	 * as a string, but is alphaNumeric is set to true
	 * then it will only return a value if a-z or 0-9 is pressed
	 */
	std::string GetPressedKey(bool alphaNumeric = false);

	/**
	 * give this a string like "a" or "shift"
	 * and this function will return the
	 * dx key code eg. DIK_A or DIK_SHIFT
	 */
	int		GetKeyCode(std::string str);

	/**
	 * give this something like DIK_A
	 * and it will return "a"
	 */
	std::string GetAsciiCode(int keyCode, bool alphaNumeric = false);

	/**
	 * Return the state of the mouse
	 */
	void	GetMouseState( int& x, int& y );

	/**
	 * Determines if they mouse button is currently down
	 */
	bool	MouseDown(int button);

	/**
	 * determines if the mouse button was pressed
	 */
	bool	MousePressed(int button);

	// use these for alt-tab
	void	AcquireMouse();
	void	UnAquireMouse();

	void	SetMouseVisibility(bool visible);
	void	SetMouseWarp(bool warp);

	void	SetAltTabStyleMouse(bool doAltTab);

	int		GetJoystickCount() { return joysticks.GetSize(); }
	bool	JoystickButtonDown(int joystick, int button);
	bool	JoystickButtonPressed(int joystick, int button);
	long int	JoystickAxisState(int joystick, JoystickAxis axis);

protected:

	struct Joystick
	{
		LPDIRECTINPUTDEVICE8	pDevice;
		DIDEVCAPS				caps;
		DIJOYSTATE2				state;
		bool					buttonLock[128];
	};

	void NotifyActive(bool active);

	// called for each joystick
	static BOOL CALLBACK EnumJoystickCallback(const DIDEVICEINSTANCE* pdidInstance, VOID* pContext);

	LPDIRECTINPUTDEVICE8	pMouseDevice;
	LPDIRECTINPUTDEVICE8	pKeyboardDevice;
	LPDIRECTINPUT8			pDI;

	Array<Joystick>	joysticks;

	// keyboard
	char keys[256];
	bool keyLock[256];

	// mouse
	DIMOUSESTATE2 mouse;
	bool mouseButtonLock[8];

	bool background;
	bool mouseWarp;
	bool mouseVisibility;
	bool altTabStyleMouse;

	Window* window; // what window is giving us input
};

}

#endif
