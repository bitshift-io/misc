//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_MATERIALDEFS_
#define _SIPHON_MATERIALDEFS_

namespace Siphon 
{

enum VertexFormat
{
	VertexAll			= 0xff,
	VertexPosition		= 0x1 << 0,
	VertexNormal		= 0x1 << 1,
	VertexColour		= 0x1 << 2,
	VertexBinormal		= 0x1 << 3,
	VertexTangent		= 0x1 << 4,
	VertexWeights		= 0x1 << 5,
	VertexBones			= 0x1 << 6,
	VertexUVCoord1		= 0x1 << 7,
	VertexUVCoord2		= 0x1 << 8,
};

class Colour
{
public:
	enum COLOUR
	{
		R = 0,
		G = 1,
		B = 2,
		A = 3
	};

	Colour()
	{

	}

	Colour( float r, float g, float b, float a = 1.0f )
	{
		colour[0] = r;
		colour[1] = g;
		colour[2] = b;
		colour[3] = a;
	}

	bool operator==(const Colour& rhs)
	{
		if( rhs.colour[0] == colour[0] && 
			rhs.colour[1] == colour[1] &&
			rhs.colour[2] == colour[2] &&
			rhs.colour[3] == colour[3])
			return true;

		return false;
	}

	bool operator!=(const Colour& rhs)
	{
		return !operator==(rhs);
	}

	float& operator[]( const int index )
	{
		return colour[index];
	}

	float* GetPointer()
	{
		return colour;
	}

protected:
	float colour[4];
};

class UVCoord
{
public:
	UVCoord()
	{
	}

	UVCoord(float u, float v)
	{
		uvCoord[0] = u;
		uvCoord[1] = v;
	}

	enum UVCOORDINATE
	{
		U = 0,
		V = 1
	};

	bool operator==(const UVCoord& rhs)
	{
		if( rhs.uvCoord[0] == uvCoord[0] && 
			rhs.uvCoord[1] == uvCoord[1])
			return true;

		return false;
	}

	bool operator!=(const UVCoord& rhs)
	{
		return !operator==(rhs);
	}

	inline float& operator[]( const int index )
	{
		return uvCoord[index];
	}

protected:
	float uvCoord[2];
};

}
#endif
