#ifndef _SIPHON_HDR_
#define _SIPHON_HDR_

#include <stdio.h>

#include "TextureFile.h"

namespace Siphon 
{

typedef struct rgbe_header_info
{
	int valid;            /* indicate which fields are valid */
	char programtype[16]; /* listed at beginning of file to identify it 
                         * after "#?".  defaults to "RGBE" */ 
	float gamma;          /* image has already been gamma corrected with 
                         * given gamma.  defaults to 1.0 (no correction) */
	float exposure;       /* a value of 1.0 in an image corresponds to
			 * <exposure> watts/steradian/m^2. 
			 * defaults to 1.0 */
};

/* flags indicating which fields in an rgbe_header_info are valid */
#define RGBE_VALID_PROGRAMTYPE 0x01
#define RGBE_VALID_GAMMA       0x02
#define RGBE_VALID_EXPOSURE    0x04

class HDR : public TextureFile
{
public:
	HDR();
	~HDR();

	bool Load(const char* file);

	virtual unsigned char* GetPixels();
	virtual int GetWidth();
	virtual int GetHeight();
	virtual int GetBPP();

	void ConvertToFloat32();

protected:

	/* read or write headers */
	/* you may set rgbe_header_info to null if you want to */
	bool WriteHeader(FILE *fp, int width, int height, rgbe_header_info *info);

	/* minimal header reading.  modify if you want to parse more information */
	int ReadHeader(FILE *fp, int *width, int *height, rgbe_header_info *info);

	/* read or write pixels */
	/* can read or write pixels in chunks of any size including single pixels*/
	int WritePixels(FILE *fp, float *data, int numpixels);

	/* simple read routine.  will not correctly handle run length encoding */
	int ReadPixels(FILE *fp, float *data, int numpixels);

	/* read or write run length encoded files */
	/* must be called to read or write whole scanlines */
	int WritePixels_RLE(FILE *fp, float *data, int scanline_width,
				int num_scanlines);
	int ReadPixels_RLE(FILE *fp, float *data, int scanline_width,
				int num_scanlines);

	int ReadPixels_Raw_RLE(FILE *fp, unsigned char *data, int scanline_width,
				int num_scanlines);


	/* standard conversion from float pixels to rgbe pixels */
	/* note: you can remove the "inline"s if your compiler complains about it */
	void Float2rgbe(unsigned char rgbe[4], float red, float green, float blue);

	/* standard conversion from rgbe to float pixels */
	/* note: Ward uses ldexp(col+0.5,exp-(128+8)).  However we wanted pixels */
	/*       in the range [0,1] to map back into the range [0,1].            */
	void Rgbe2float(float *red, float *green, float *blue, unsigned char rgbe[4]);

	static bool RgbeError(int rgbe_error_code, char *msg);

	/* The code below is only needed for the run-length encoded files. */
	/* Run length encoding adds considerable complexity but does */
	/* save some space.  For each scanline, each channel (r,g,b,e) is */
	/* encoded separately for better compression. */
	static int WriteBytes_RLE(FILE *fp, unsigned char *data, int numbytes);

	static int HDR::ReadPixels_Raw(FILE *fp, unsigned char *data, int numpixels);

	int width;
	int height;
	unsigned char* image;
};

}
#endif

