//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_OGLDEVICE_
#define _SIPHON_OGLDEVICE_

#ifndef WIN32
#define GL_GLEXT_PROTOTYPES
#endif

#ifdef WIN32
    #include <windows.h>
	#include <windef.h>
#endif

#include <GL/gl.h>
#include <GL/glu.h>

#ifndef WIN32
	#include <GL/glext.h>
#endif

#include "Util/Singleton.h"

#include "Math3D.h"

//--------------------------------------------------------------------------------------


#define BUFFER_OFFSET(i) ((char *)0 + (i))

#ifdef WIN32

	// Multisample
	#define GL_MULTISAMPLE_ARB                0x809D
	#define GL_SAMPLE_ALPHA_TO_COVERAGE_ARB   0x809E
	#define GL_SAMPLE_ALPHA_TO_ONE_ARB        0x809F
	#define GL_SAMPLE_COVERAGE_ARB            0x80A0
	#define GL_SAMPLE_BUFFERS_ARB             0x80A8
	#define GL_SAMPLES_ARB                    0x80A9
	#define GL_SAMPLE_COVERAGE_VALUE_ARB      0x80AA
	#define GL_SAMPLE_COVERAGE_INVERT_ARB     0x80AB
	#define GL_MULTISAMPLE_BIT_ARB            0x20000000

	typedef void (APIENTRY* PFNGLSAMPLECOVERAGEARBPROC) (GLclampf value, GLboolean invert);

	extern PFNGLSAMPLECOVERAGEARBPROC glSampleCoverageARB;


	// TextureParameterName
	#define GL_TEXTURE_MAG_FILTER             0x2800
	#define GL_TEXTURE_MIN_FILTER             0x2801
	#define GL_TEXTURE_WRAP_S                 0x2802
	#define GL_TEXTURE_WRAP_T                 0x2803
	#define GL_TEXTURE_WRAP_R                 0x8072

	#define GL_CLAMP_TO_EDGE                  0x812F
	#define GL_CLAMP_TO_EDGE_SGIS             0x812F
	#define GL_GENERATE_MIPMAP_SGIS           0x8191
	#define GL_GENERATE_MIPMAP_HINT_SGIS      0x8192

	// GL_NV_float_buffer
	#define GL_FLOAT_R_NV                     0x8880
	#define GL_FLOAT_RG_NV                    0x8881
	#define GL_FLOAT_RGB_NV                   0x8882
	#define GL_FLOAT_RGBA_NV                  0x8883
	#define GL_FLOAT_R16_NV                   0x8884
	#define GL_FLOAT_R32_NV                   0x8885
	#define GL_FLOAT_RG16_NV                  0x8886
	#define GL_FLOAT_RG32_NV                  0x8887
	#define GL_FLOAT_RGB16_NV                 0x8888
	#define GL_FLOAT_RGB32_NV                 0x8889
	#define GL_FLOAT_RGBA16_NV                0x888A
	#define GL_FLOAT_RGBA32_NV                0x888B
	#define GL_TEXTURE_FLOAT_COMPONENTS_NV    0x888C
	#define GL_FLOAT_CLEAR_COLOR_VALUE_NV     0x888D
	#define GL_FLOAT_RGBA_MODE_NV             0x888E

	// GL_NV_texture_rectangle
	#define GL_TEXTURE_RECTANGLE_NV           0x84F5
	#define GL_TEXTURE_BINDING_RECTANGLE_NV   0x84F6
	#define GL_PROXY_TEXTURE_RECTANGLE_NV     0x84F7
	#define GL_MAX_RECTANGLE_TEXTURE_SIZE_NV  0x84F8

	// GL_ATI_texture_float
	#define GL_RGBA_FLOAT32_ATI               0x8814
	#define GL_RGB_FLOAT32_ATI                0x8815
	#define GL_ALPHA_FLOAT32_ATI              0x8816
	#define GL_INTENSITY_FLOAT32_ATI          0x8817
	#define GL_LUMINANCE_FLOAT32_ATI          0x8818
	#define GL_LUMINANCE_ALPHA_FLOAT32_ATI    0x8819
	#define GL_RGBA_FLOAT16_ATI               0x881A
	#define GL_RGB_FLOAT16_ATI                0x881B
	#define GL_ALPHA_FLOAT16_ATI              0x881C
	#define GL_INTENSITY_FLOAT16_ATI          0x881D
	#define GL_LUMINANCE_FLOAT16_ATI          0x881E
	#define GL_LUMINANCE_ALPHA_FLOAT16_ATI    0x881F

	// float textures
	#define GL_RGBA32F_ARB 0x8814
	#define GL_RGB32F_ARB 0x8815
	#define GL_ALPHA32F_ARB 0x8816
	#define GL_INTENSITY32F_ARB 0x8817
	#define GL_LUMINANCE32F_ARB 0x8818
	#define GL_LUMINANCE_ALPHA32F_ARB 0x8819
	#define GL_RGBA16F_ARB 0x881A
	#define GL_RGB16F_ARB 0x881B
	#define GL_ALPHA16F_ARB 0x881C
	#define GL_INTENSITY16F_ARB 0x881D
	#define GL_LUMINANCE16F_ARB 0x881E
	#define GL_LUMINANCE_ALPHA16F_ARB 0x881F
	#define GL_TEXTURE_RED_TYPE_ARB 0x8C10
	#define GL_TEXTURE_GREEN_TYPE_ARB 0x8C11
	#define GL_TEXTURE_BLUE_TYPE_ARB 0x8C12
	#define GL_TEXTURE_ALPHA_TYPE_ARB 0x8C13
	#define GL_TEXTURE_LUMINANCE_TYPE_ARB 0x8C14
	#define GL_TEXTURE_INTENSITY_TYPE_ARB 0x8C15
	#define GL_TEXTURE_DEPTH_TYPE_ARB 0x8C16
	#define GL_UNSIGNED_NORMALIZED_ARB 0x8C17

	// GL_EXT_texture_env_combine
	#define GL_COMBINE_EXT                    0x8570
	#define GL_COMBINE_RGB_EXT                0x8571
	#define GL_COMBINE_ALPHA_EXT              0x8572
	#define GL_RGB_SCALE_EXT                  0x8573
	#define GL_ADD_SIGNED_EXT                 0x8574
	#define GL_INTERPOLATE_EXT                0x8575
	#define GL_CONSTANT_EXT                   0x8576
	#define GL_PRIMARY_COLOR_EXT              0x8577
	#define GL_PREVIOUS_EXT                   0x8578
	#define GL_SOURCE0_RGB_EXT                0x8580
	#define GL_SOURCE1_RGB_EXT                0x8581
	#define GL_SOURCE2_RGB_EXT                0x8582
	#define GL_SOURCE0_ALPHA_EXT              0x8588
	#define GL_SOURCE1_ALPHA_EXT              0x8589
	#define GL_SOURCE2_ALPHA_EXT              0x858A
	#define GL_OPERAND0_RGB_EXT               0x8590
	#define GL_OPERAND1_RGB_EXT               0x8591
	#define GL_OPERAND2_RGB_EXT               0x8592
	#define GL_OPERAND0_ALPHA_EXT             0x8598
	#define GL_OPERAND1_ALPHA_EXT             0x8599
	#define GL_OPERAND2_ALPHA_EXT             0x859A

	// hdr
	#define GL_RGB_FLOAT16_ATI                0x881B
	#define GL_RGB_FLOAT32_ATI                0x8815

	#define GL_HILO_NV                        0x86F4
	#define GL_HILO16_NV                      0x86F8

	#define GL_DEPTH_COMPONENT16		0x81A5
	#define GL_DEPTH_COMPONENT24		0x81A6
	#define GL_DEPTH_COMPONENT32		0x81A7

	// shaders
	#define GL_FRAGMENT_PROGRAM_ARB           0x8804
	#define GL_VERTEX_PROGRAM_ARB             0x8620

	// FBO
	#define GL_FRAMEBUFFER_EXT                     0x8D40
	#define GL_RENDERBUFFER_EXT                    0x8D41
	#define GL_STENCIL_INDEX_EXT                   0x8D45
	#define GL_STENCIL_INDEX1_EXT                  0x8D46
	#define GL_STENCIL_INDEX4_EXT                  0x8D47
	#define GL_STENCIL_INDEX8_EXT                  0x8D48
	#define GL_STENCIL_INDEX16_EXT                 0x8D49

	#define GL_RENDERBUFFER_WIDTH_EXT              0x8D42
	#define GL_RENDERBUFFER_HEIGHT_EXT             0x8D43
	#define GL_RENDERBUFFER_INTERNAL_FORMAT_EXT    0x8D44

	#define GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_EXT            0x8CD0
	#define GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_EXT            0x8CD1
	#define GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL_EXT          0x8CD2
	#define GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE_EXT  0x8CD3
	#define GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_3D_ZOFFSET_EXT     0x8CD4

	#define GL_COLOR_ATTACHMENT0_EXT                0x8CE0
	#define GL_COLOR_ATTACHMENT1_EXT                0x8CE1
	#define GL_COLOR_ATTACHMENT2_EXT                0x8CE2
	#define GL_COLOR_ATTACHMENT3_EXT                0x8CE3
	#define GL_COLOR_ATTACHMENT4_EXT                0x8CE4
	#define GL_COLOR_ATTACHMENT5_EXT                0x8CE5
	#define GL_COLOR_ATTACHMENT6_EXT                0x8CE6
	#define GL_COLOR_ATTACHMENT7_EXT                0x8CE7
	#define GL_COLOR_ATTACHMENT8_EXT                0x8CE8
	#define GL_COLOR_ATTACHMENT9_EXT                0x8CE9
	#define GL_COLOR_ATTACHMENT10_EXT               0x8CEA
	#define GL_COLOR_ATTACHMENT11_EXT               0x8CEB
	#define GL_COLOR_ATTACHMENT12_EXT               0x8CEC
	#define GL_COLOR_ATTACHMENT13_EXT               0x8CED
	#define GL_COLOR_ATTACHMENT14_EXT               0x8CEE
	#define GL_COLOR_ATTACHMENT15_EXT               0x8CEF
	#define GL_DEPTH_ATTACHMENT_EXT                 0x8D00
	#define GL_STENCIL_ATTACHMENT_EXT               0x8D20

	#define GL_FRAMEBUFFER_COMPLETE_EXT                          0x8CD5
	#define GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT             0x8CD6
	#define GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT     0x8CD7
	#define GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT_EXT   0x8CD8
	#define GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT             0x8CD9
	#define GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT                0x8CDA
	#define GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT            0x8CDB
	#define GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT            0x8CDC
	#define GL_FRAMEBUFFER_UNSUPPORTED_EXT                       0x8CDD
	#define GL_FRAMEBUFFER_STATUS_ERROR_EXT                      0x8CDE

	#define GL_FRAMEBUFFER_BINDING_EXT             0x8CA6
	#define GL_RENDERBUFFER_BINDING_EXT            0x8CA7
	#define GL_MAX_COLOR_ATTACHMENTS_EXT           0x8CDF
	#define GL_MAX_RENDERBUFFER_SIZE_EXT           0x84E8
	#define GL_INVALID_FRAMEBUFFER_OPERATION_EXT   0x0506

	typedef void (APIENTRY* PFNGLBINDFRAMEBUFFEREXTPROC) (GLenum target, GLuint framebuffer);
	typedef void (APIENTRY* PFNGLBINDRENDERBUFFEREXTPROC) (GLenum target, GLuint renderbuffer);
	typedef GLenum (APIENTRY* PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC) (GLenum target);
	typedef void (APIENTRY* PFNGLDELETEFRAMEBUFFERSEXTPROC) (GLsizei n, const GLuint* framebuffers);
	typedef void (APIENTRY* PFNGLDELETERENDERBUFFERSEXTPROC) (GLsizei n, const GLuint* renderbuffers);
	typedef void (APIENTRY* PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC) (GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);
	typedef void (APIENTRY* PFNGLFRAMEBUFFERTEXTURE1DEXTPROC) (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
	typedef void (APIENTRY* PFNGLFRAMEBUFFERTEXTURE2DEXTPROC) (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
	typedef void (APIENTRY* PFNGLFRAMEBUFFERTEXTURE3DEXTPROC) (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLint zoffset);
	typedef void (APIENTRY* PFNGLGENFRAMEBUFFERSEXTPROC) (GLsizei n, GLuint* framebuffers);
	typedef void (APIENTRY* PFNGLGENRENDERBUFFERSEXTPROC) (GLsizei n, GLuint* renderbuffers);
	typedef void (APIENTRY* PFNGLGENERATEMIPMAPEXTPROC) (GLenum target);
	typedef void (APIENTRY* PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC) (GLenum target, GLenum attachment, GLenum pname, GLint* params);
	typedef void (APIENTRY* PFNGLGETRENDERBUFFERPARAMETERIVEXTPROC) (GLenum target, GLenum pname, GLint* params);
	typedef GLboolean (APIENTRY* PFNGLISFRAMEBUFFEREXTPROC) (GLuint framebuffer);
	typedef GLboolean (APIENTRY* PFNGLISRENDERBUFFEREXTPROC) (GLuint renderbuffer);
	typedef void (APIENTRY* PFNGLRENDERBUFFERSTORAGEEXTPROC) (GLenum target, GLenum internalformat, GLsizei width, GLsizei height);

	extern PFNGLBINDFRAMEBUFFEREXTPROC glBindFramebufferEXT;
	extern PFNGLBINDRENDERBUFFEREXTPROC glBindRenderbufferEXT;
	extern PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC glCheckFramebufferStatusEXT;
	extern PFNGLDELETEFRAMEBUFFERSEXTPROC glDeleteFramebuffersEXT;
	extern PFNGLDELETERENDERBUFFERSEXTPROC glDeleteRenderbuffersEXT;
	extern PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC glFramebufferRenderbufferEXT;
	extern PFNGLFRAMEBUFFERTEXTURE1DEXTPROC glFramebufferTexture1DEXT;
	extern PFNGLFRAMEBUFFERTEXTURE2DEXTPROC glFramebufferTexture2DEXT;
	extern PFNGLFRAMEBUFFERTEXTURE3DEXTPROC glFramebufferTexture3DEXT;
	extern PFNGLGENFRAMEBUFFERSEXTPROC glGenFramebuffersEXT;
	extern PFNGLGENRENDERBUFFERSEXTPROC glGenRenderbuffersEXT;
	extern PFNGLGENERATEMIPMAPEXTPROC glGenerateMipmapEXT;
	extern PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC glGetFramebufferAttachmentParameterivEXT;
	extern PFNGLGETRENDERBUFFERPARAMETERIVEXTPROC glGetRenderbufferParameterivEXT;
	extern PFNGLISFRAMEBUFFEREXTPROC glIsFramebufferEXT;
	extern PFNGLISRENDERBUFFEREXTPROC glIsRenderbufferEXT;
	extern PFNGLRENDERBUFFERSTORAGEEXTPROC glRenderbufferStorageEXT;

	// NFI
	#define GL_CLAMP_TO_BORDER                0x812D

	// TEXTURE LOD
	#define GL_TEXTURE_LOD_BIAS_EXT             0x8501
	#define GL_MAX_TEXTURE_LOD_BIAS_EXT         0x84FD
	#define GL_TEXTURE_FILTER_CONTROL_EXT       0x8500

	#define GL_TEXTURE_MIN_LOD_SGIS				0x813A
	#define GL_TEXTURE_MAX_LOD_SGIS				0x813B
	#define GL_TEXTURE_BASE_LEVEL_SGIS			0x813C
	#define GL_TEXTURE_MAX_LEVEL_SGIS			0x813D

	// CUBE MAPPING
	#define GL_NORMAL_MAP                     0x8511
	#define GL_REFLECTION_MAP                 0x8512
	#define GL_TEXTURE_CUBE_MAP               0x8513
	#define GL_TEXTURE_CUBE_MAP_POSITIVE_X    0x8515
	#define GL_TEXTURE_CUBE_MAP_NEGATIVE_X    0x8516
	#define GL_TEXTURE_CUBE_MAP_POSITIVE_Y    0x8517
	#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Y    0x8518
	#define GL_TEXTURE_CUBE_MAP_POSITIVE_Z    0x8519
	#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Z    0x851A
	#define GL_PROXY_TEXTURE_CUBE_MAP_EXT     0x851B
	#define GL_MAX_CUBE_MAP_TEXTURE_SIZE_EXT  0x851C

	// BUMP MAPPING
	#define GL_DOT3_RGB_EXT                   0x8740
	#define GL_DOT3_RGBA_EXT                  0x8741

	// DEPTH TEXTURE
	#define GL_DEPTH_COMPONENT16_ARB          0x81A5
	#define GL_DEPTH_COMPONENT24_ARB          0x81A6
	#define GL_DEPTH_COMPONENT32_ARB          0x81A7
	#define GL_TEXTURE_DEPTH_SIZE_ARB         0x884A
	#define GL_DEPTH_TEXTURE_MODE_ARB         0x884B

	// SHADOW //GL_ARB_shadow
	#define GL_TEXTURE_COMPARE_MODE_ARB       0x884C
	#define GL_TEXTURE_COMPARE_FUNC_ARB       0x884D
	#define GL_COMPARE_R_TO_TEXTURE_ARB       0x884E

	// POINT SPRITES
	#define GL_POINT_DISTANCE_ATTENUATION     0x8129

	#define GL_POINT_SIZE_MIN_ARB             0x8126
	#define GL_POINT_SIZE_MAX_ARB             0x8127
	#define GL_POINT_FADE_THRESHOLD_SIZE_ARB  0x8128
	#define GL_POINT_DISTANCE_ATTENUATION_ARB 0x8129

	#define GL_POINT_SPRITE_ARB               0x8861
	#define GL_COORD_REPLACE_ARB              0x8862

	typedef void (APIENTRY* PFNGLPOINTPARAMETERFARBPROC) (GLenum pname, GLfloat param);
	typedef void (APIENTRY* PFNGLPOINTPARAMETERFVARBPROC) (GLenum pname, const GLfloat *params);

	extern PFNGLPOINTPARAMETERFARBPROC  glPointParameterfARB;
	extern PFNGLPOINTPARAMETERFVARBPROC glPointParameterfvARB;


	#define GL_POINT_SPRITE_ARB               0x8861
	#define GL_COORD_REPLACE_ARB              0x8862

	//MULTI TEXTURE
	#define GL_ACTIVE_TEXTURE_ARB               0x84E0
	#define GL_CLIENT_ACTIVE_TEXTURE_ARB        0x84E1
	#define GL_MAX_TEXTURE_UNITS_ARB            0x84E2
	#define GL_TEXTURE0_ARB                     0x84C0
	#define GL_TEXTURE1_ARB                     0x84C1
	#define GL_TEXTURE2_ARB                     0x84C2
	#define GL_TEXTURE3_ARB                     0x84C3
	#define GL_TEXTURE4_ARB                     0x84C4
	#define GL_TEXTURE5_ARB                     0x84C5
	#define GL_TEXTURE6_ARB                     0x84C6
	#define GL_TEXTURE7_ARB                     0x84C7
	#define GL_TEXTURE8_ARB                     0x84C8
	#define GL_TEXTURE9_ARB                     0x84C9
	#define GL_TEXTURE10_ARB                    0x84CA
	#define GL_TEXTURE11_ARB                    0x84CB
	#define GL_TEXTURE12_ARB                    0x84CC
	#define GL_TEXTURE13_ARB                    0x84CD
	#define GL_TEXTURE14_ARB                    0x84CE
	#define GL_TEXTURE15_ARB                    0x84CF
	#define GL_TEXTURE16_ARB                    0x84D0
	#define GL_TEXTURE17_ARB                    0x84D1
	#define GL_TEXTURE18_ARB                    0x84D2
	#define GL_TEXTURE19_ARB                    0x84D3
	#define GL_TEXTURE20_ARB                    0x84D4
	#define GL_TEXTURE21_ARB                    0x84D5
	#define GL_TEXTURE22_ARB                    0x84D6
	#define GL_TEXTURE23_ARB                    0x84D7
	#define GL_TEXTURE24_ARB                    0x84D8
	#define GL_TEXTURE25_ARB                    0x84D9
	#define GL_TEXTURE26_ARB                    0x84DA
	#define GL_TEXTURE27_ARB                    0x84DB
	#define GL_TEXTURE28_ARB                    0x84DC
	#define GL_TEXTURE29_ARB                    0x84DD
	#define GL_TEXTURE30_ARB                    0x84DE
	#define GL_TEXTURE31_ARB                    0x84DF

	typedef void (APIENTRY * PFNGLACTIVETEXTUREARBPROC) (GLenum target);
	typedef void (APIENTRY * PFNGLCLIENTACTIVETEXTUREARBPROC) (GLenum target);

	extern PFNGLACTIVETEXTUREARBPROC glActiveTextureARB;
	extern PFNGLCLIENTACTIVETEXTUREARBPROC glClientActiveTexture;

	//DRAW RANGE ELEMENTS
	typedef void (APIENTRY * PFNGLDRAWRANGEELEMENTSPROC) ( GLenum mode, GLuint start,
 		GLuint end, GLsizei count, GLenum type, const GLvoid *indices );

	extern PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElements;

	//VERTEX BUFFERS
	#define GL_ARRAY_BUFFER_ARB 0x8892
	#define GL_STATIC_DRAW_ARB 0x88E4
	#define GL_ELEMENT_ARRAY_BUFFER_ARB 0x8893

	typedef void (APIENTRY * PFNGLBINDBUFFERARBPROC) (GLenum target, GLuint buffer);
	typedef void (APIENTRY * PFNGLDELETEBUFFERSARBPROC) (GLsizei n, const GLuint *buffers);
	typedef void (APIENTRY * PFNGLGENBUFFERSARBPROC) (GLsizei n, GLuint *buffers);
	typedef void (APIENTRY * PFNGLBUFFERDATAARBPROC) (GLenum target, int size, const GLvoid *data, GLenum usage);
	typedef void (APIENTRY * PFNGLDELETEBUFFERSARBPROC) (GLsizei n, const GLuint *buffers);

	extern PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB;
	extern PFNGLBINDBUFFERARBPROC glBindBufferARB;
	extern PFNGLGENBUFFERSARBPROC glGenBuffersARB;
	extern PFNGLBUFFERDATAARBPROC glBufferDataARB;

	//SHADERS

	typedef char GLcharARB;
	typedef unsigned int GLhandleARB;

	#define FRAGMENT_SHADER_ARB 0x8B30
	#define MAX_FRAGMENT_UNIFORM_COMPONENTS_ARB 0x8B49
	#define MAX_TEXTURE_COORDS_ARB 0x8871
	#define MAX_TEXTURE_IMAGE_UNITS_ARB 0x8872
	#define OBJECT_TYPE_ARB	0x8B4E
	#define OBJECT_SUBTYPE_ARB 0x8B4F
	#define SHADER_OBJECT_ARB 0x8B48

	#define GL_PROGRAM_OBJECT_ARB             0x8B40
	#define GL_SHADER_OBJECT_ARB              0x8B48
	#define GL_OBJECT_TYPE_ARB                0x8B4E
	#define GL_OBJECT_SUBTYPE_ARB             0x8B4F
	#define GL_FLOAT_VEC2_ARB                 0x8B50
	#define GL_FLOAT_VEC3_ARB                 0x8B51
	#define GL_FLOAT_VEC4_ARB                 0x8B52
	#define GL_INT_VEC2_ARB                   0x8B53
	#define GL_INT_VEC3_ARB                   0x8B54
	#define GL_INT_VEC4_ARB                   0x8B55
	#define GL_BOOL_ARB                       0x8B56
	#define GL_BOOL_VEC2_ARB                  0x8B57
	#define GL_BOOL_VEC3_ARB                  0x8B58
	#define GL_BOOL_VEC4_ARB                  0x8B59
	#define GL_FLOAT_MAT2_ARB                 0x8B5A
	#define GL_FLOAT_MAT3_ARB                 0x8B5B
	#define GL_FLOAT_MAT4_ARB                 0x8B5C
	#define GL_OBJECT_DELETE_STATUS_ARB       0x8B80
	#define GL_OBJECT_COMPILE_STATUS_ARB      0x8B81
	#define GL_OBJECT_LINK_STATUS_ARB         0x8B82
	#define GL_OBJECT_VALIDATE_STATUS_ARB     0x8B83
	#define GL_OBJECT_INFO_LOG_LENGTH_ARB     0x8B84
	#define GL_OBJECT_ATTACHED_OBJECTS_ARB    0x8B85
	#define GL_OBJECT_ACTIVE_UNIFORMS_ARB     0x8B86
	#define GL_OBJECT_ACTIVE_UNIFORM_MAX_LENGTH_ARB 0x8B87
	#define GL_OBJECT_SHADER_SOURCE_LENGTH_ARB 0x8B88

	#define GL_VERTEX_SHADER_ARB              0x8B31
	#define GL_FRAGMENT_SHADER_ARB            0x8B30

	typedef GLhandleARB (APIENTRY * PFNGLCREATESHADEROBJECTARBPROC) (GLenum shaderType);
	typedef void (APIENTRY * PFNGLUSEPROGRAMOBJECTARBPROC) (GLhandleARB programObj);
	typedef void (APIENTRY * PFNGLSHADERSOURCEARBPROC) (GLhandleARB, GLsizei, const GLcharARB* *, const GLint *);
	typedef void (APIENTRY * PFNGLCOMPILESHADERARBPROC) (GLhandleARB);
	typedef void (APIENTRY * PFNGLGETOBJECTPARAMETERIVARBPROC) (GLhandleARB, GLenum, GLint *);
	typedef void (APIENTRY * PFNGLATTACHOBJECTARBPROC) (GLhandleARB, GLhandleARB);
	typedef void (APIENTRY * PFNGLATTACHOBJECTARBPROC) (GLhandleARB, GLhandleARB);
	typedef GLhandleARB (APIENTRY * PFNGLCREATEPROGRAMOBJECTARBPROC) (void);
	typedef void (APIENTRY * PFNGLGETINFOLOGARBPROC) (GLhandleARB, GLsizei, GLsizei *, GLcharARB *);
	typedef void (APIENTRY * PFNGLLINKPROGRAMARBPROC) (GLhandleARB);
	typedef void (APIENTRY * PFNGLDELETEOBJECTARBPROC) (GLhandleARB);
	typedef GLint (APIENTRY * PFNGLGETUNIFORMLOCATIONARBPROC) (GLhandleARB programObj, const GLcharARB *name);
	typedef void (APIENTRY * PFNGLUNIFORM1IARBPROC) (GLint location, GLint v0);
	typedef void (APIENTRY * PFNGLUNIFORM1FARBPROC) (GLint location, GLfloat v0);
	typedef void (APIENTRY * PFNGLVERTEXATTRIBPOINTERARBPROC) (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer);
	typedef void (APIENTRY * PFNGLENABLEVERTEXATTRIBARRAYARBPROC) (GLuint index);
	typedef void (APIENTRY * PFNGLDISABLEVERTEXATTRIBARRAYARBPROC) (GLuint index);
	typedef void (APIENTRY * PFNGLBINDATTRIBLOCATIONARBPROC) (GLhandleARB programObj, GLuint index, const GLcharARB *name);
	typedef void (APIENTRY * PFNGLGETACTIVEATTRIBARBPROC) (GLhandleARB programObj, GLuint index, GLsizei maxLength, GLsizei *length, GLint *size, GLenum *type, GLcharARB *name);
	typedef GLint (APIENTRY * PFNGLGETATTRIBLOCATIONARBPROC) (GLhandleARB programObj, const GLcharARB *name);
	typedef void (APIENTRY * PFNGLUNIFORMMATRIX2FVARBPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
	typedef void (APIENTRY * PFNGLUNIFORMMATRIX3FVARBPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
	typedef void (APIENTRY * PFNGLUNIFORMMATRIX4FVARBPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

	extern PFNGLCREATESHADEROBJECTARBPROC glCreateShaderObjectARB;
	extern PFNGLUSEPROGRAMOBJECTARBPROC glUseProgramObjectARB;
	extern PFNGLSHADERSOURCEARBPROC glShaderSourceARB;
	extern PFNGLCOMPILESHADERARBPROC glCompileShaderARB;
	extern PFNGLGETOBJECTPARAMETERIVARBPROC glGetObjectParameterivARB;
	extern PFNGLATTACHOBJECTARBPROC glAttachObjectARB;
	extern PFNGLCREATEPROGRAMOBJECTARBPROC glCreateProgramObjectARB;
	extern PFNGLGETINFOLOGARBPROC glGetInfoLogARB;
	extern PFNGLLINKPROGRAMARBPROC glLinkProgramARB;
	extern PFNGLDELETEOBJECTARBPROC glDeleteObjectARB;
	extern PFNGLGETUNIFORMLOCATIONARBPROC glGetUniformLocationARB;
	extern PFNGLUNIFORM1IARBPROC glUniform1iARB;
	extern PFNGLUNIFORM1FARBPROC glUniform1fARB;
	extern PFNGLVERTEXATTRIBPOINTERARBPROC glVertexAttribPointerARB;
	extern PFNGLENABLEVERTEXATTRIBARRAYARBPROC glEnableVertexAttribArrayARB;
	extern PFNGLDISABLEVERTEXATTRIBARRAYARBPROC glDisableVertexAttribArrayARB;
	extern PFNGLBINDATTRIBLOCATIONARBPROC glBindAttribLocationARB;
	extern PFNGLGETACTIVEATTRIBARBPROC glGetActiveAttribARB;
	extern PFNGLGETATTRIBLOCATIONARBPROC glGetAttribLocationARB;
	extern PFNGLUNIFORMMATRIX2FVARBPROC glUniformMatrix2fvARB;
	extern PFNGLUNIFORMMATRIX3FVARBPROC glUniformMatrix3fvARB;
	extern PFNGLUNIFORMMATRIX4FVARBPROC glUniformMatrix4fvARB;

	// gedebugger extension
	typedef void (APIENTRY * PFNGLSTRINGMARKERGREMEDYPROC) (GLsizei len, const GLvoid *string);

	extern PFNGLSTRINGMARKERGREMEDYPROC glStringMarkerGREMEDY;


	#define GDBSTRING(str)						\
	if (glStringMarkerGREMEDY)					\
		glStringMarkerGREMEDY(strlen(str), str);\

#endif
//--------------------------------------------------------------------------------------

namespace Siphon
{

class Window;

#define gDevice Device::GetInstance()

/**
 * This is the OpenGL device,
 * this class is responsible for
 * device related specifics,
 */
class Device : public /*Siphon::*/Singleton<Device>
{
public:

	enum MatrixMode
	{
		ModelView,
		Projection,
		Texture
	};

	Device();
	~Device();

	void DestroyDevice();
	bool CreateDevice(Window* wnd = 0);

	void SetMatrix(const Matrix4& matrix, MatrixMode mode);

	int CheckErrors();

    static void PrintExtensions();
	static int ExtensionSupported(const char* extension);
	static int FindExtension(const char* extension, const char* extensions);

	void BeginRender(int flags = GL_STENCIL_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	void EndRender();

	inline bool VBOSupported();
	inline bool ShaderSupported();

	unsigned int GetFPS();

	void SetClearColour(float r, float g, float b, float a = 1.0f);

	void CheckFramebufferStatus();

	void SetActiveWindow(Window* wnd);

	void ResetDevice(int width, int height);

	// enable/disable multi sampling
	void EnableMulisample(bool enable);
	inline bool MultisampleSupported();

	void SaveScreenShot();

protected:
	Window* activeWindow;
	float	clearColour[4];

	unsigned int framesThisSecond;
	unsigned int fps;
	unsigned int ticks;

	bool bVBOSupported;
	bool bShadersSupported;
	bool bMultisampleSupported;
};

inline bool Device::VBOSupported()
{
	return bVBOSupported;
}

inline bool Device::ShaderSupported()
{
	return bShadersSupported;
}

inline bool Device::MultisampleSupported()
{
	return bMultisampleSupported;
}

}
#endif
