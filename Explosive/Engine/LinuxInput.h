#ifndef _SIPHON_LINUXINPUT_
#define _SIPHON_LINUXINPUT_

#include <string>
#include <X11/keysym.h>

#include "../Util/Array.h"
#include "Singleton.h"

namespace Siphon
{

class Window;

#define gInput Input::GetInstance()

class Input : public Singleton<Input>
{
	friend class Window;

public:
	enum JoystickAxis
	{
		X,
		Y,
		Z
	};





/* International & multi-key character composition */

#define XK_Multi_key                     0xff20  /* Multi-key character compose */
#define XK_Codeinput                     0xff37
#define XK_SingleCandidate               0xff3c
#define XK_MultipleCandidate             0xff3d
#define XK_PreviousCandidate             0xff3e

/* Japanese keyboard support */

#define XK_Kanji                         0xff21  /* Kanji, Kanji convert */
#define XK_Muhenkan                      0xff22  /* Cancel Conversion */
#define XK_Henkan_Mode                   0xff23  /* Start/Stop Conversion */
#define XK_Henkan                        0xff23  /* Alias for Henkan_Mode */
#define XK_Romaji                        0xff24  /* to Romaji */
#define XK_Hiragana                      0xff25  /* to Hiragana */
#define XK_Katakana                      0xff26  /* to Katakana */
#define XK_Hiragana_Katakana             0xff27  /* Hiragana/Katakana toggle */
#define XK_Zenkaku                       0xff28  /* to Zenkaku */
#define XK_Hankaku                       0xff29  /* to Hankaku */
#define XK_Zenkaku_Hankaku               0xff2a  /* Zenkaku/Hankaku toggle */
#define XK_Touroku                       0xff2b  /* Add to Dictionary */
#define XK_Massyo                        0xff2c  /* Delete from Dictionary */
#define XK_Kana_Lock                     0xff2d  /* Kana Lock */
#define XK_Kana_Shift                    0xff2e  /* Kana Shift */
#define XK_Eisu_Shift                    0xff2f  /* Alphanumeric Shift */
#define XK_Eisu_toggle                   0xff30  /* Alphanumeric toggle */
#define XK_Kanji_Bangou                  0xff37  /* Codeinput */
#define XK_Zen_Koho                      0xff3d  /* Multiple/All Candidate(s) */
#define XK_Mae_Koho                      0xff3e  /* Previous Candidate */

/* 0xff31 thru 0xff3f are under XK_KOREAN */

/* Cursor control & motion */




/* Misc functions */

#define XK_Select                        0xff60  /* Select, mark */
#define XK_Print                         0xff61
#define XK_Execute                       0xff62  /* Execute, run, do */
#define XK_Insert                        0xff63  /* Insert, insert here */
#define XK_Undo                          0xff65
#define XK_Redo                          0xff66  /* Redo, again */
#define XK_Menu                          0xff67
#define XK_Find                          0xff68  /* Find, search */
#define XK_Cancel                        0xff69  /* Cancel, stop, abort, exit */
#define XK_Help                          0xff6a  /* Help */
#define XK_Break                         0xff6b
#define XK_Mode_switch                   0xff7e  /* Character set switch */
#define XK_script_switch                 0xff7e  /* Alias for mode_switch */
#define XK_Num_Lock                      0xff7f

/* Keypad functions, keypad numbers cleverly chosen to map to ASCII */

#define XK_KP_Space                      0xff80  /* Space */
#define XK_KP_Tab                        0xff89
#define XK_KP_Enter                      0xff8d  /* Enter */
#define XK_KP_F1                         0xff91  /* PF1, KP_A, ... */
#define XK_KP_F2                         0xff92
#define XK_KP_F3                         0xff93
#define XK_KP_F4                         0xff94
#define XK_KP_Home                       0xff95
#define XK_KP_Left                       0xff96
#define XK_KP_Up                         0xff97
#define XK_KP_Right                      0xff98
#define XK_KP_Down                       0xff99
#define XK_KP_Prior                      0xff9a
#define XK_KP_Page_Up                    0xff9a
#define XK_KP_Next                       0xff9b
#define XK_KP_Page_Down                  0xff9b
#define XK_KP_End                        0xff9c
#define XK_KP_Begin                      0xff9d
#define XK_KP_Insert                     0xff9e
#define XK_KP_Delete                     0xff9f
#define XK_KP_Equal                      0xffbd  /* Equals */
#define XK_KP_Multiply                   0xffaa
#define XK_KP_Add                        0xffab
#define XK_KP_Separator                  0xffac  /* Separator, often comma */
#define XK_KP_Subtract                   0xffad
#define XK_KP_Decimal                    0xffae
#define XK_KP_Divide                     0xffaf

    enum Key
    {
        KEY_Space = XK_KP_Space,

        // key pad
        KEY_KP0 = XK_KP_0,
        KEY_KP1 = XK_KP_1,
        KEY_KP2 = XK_KP_2,
        KEY_KP3 = XK_KP_3,
        KEY_KP4 = XK_KP_4,
        KEY_KP5 = XK_KP_5,
        KEY_KP6 = XK_KP_6,
        KEY_KP7 = XK_KP_7,
        KEY_KP8 = XK_KP_8,
        KEY_KP9 = XK_KP_9,

        KEY_0 = XK_0,
        KEY_1 = XK_1,
        KEY_2 = XK_2,
        KEY_3 = XK_3,
        KEY_4 = XK_4,
        KEY_5 = XK_5,
        KEY_6 = XK_6,
        KEY_7 = XK_7,
        KEY_8 = XK_8,
        KEY_9 = XK_9,

        KEY_a = XK_a,
        KEY_b = XK_b,
        KEY_c = XK_c,
        KEY_d = XK_d,
        KEY_e = XK_e,
        KEY_f = XK_f,
        KEY_g = XK_g,
        KEY_h = XK_h,
        KEY_i = XK_i,
        KEY_j = XK_j,
        KEY_k = XK_k,
        KEY_l = XK_l,
        KEY_m = XK_m,
        KEY_n = XK_n,
        KEY_o = XK_o,
        KEY_p = XK_p,
        KEY_q = XK_q,
        KEY_r = XK_r,
        KEY_s = XK_s,
        KEY_t = XK_t,
        KEY_u = XK_u,
        KEY_v = XK_v,
        KEY_w = XK_w,
        KEY_x = XK_x,
        KEY_y = XK_y,
        KEY_z = XK_z,

        KEY_F1 = XK_F1,
        KEY_F2 = XK_F2,
        KEY_F3 = XK_F3,
        KEY_F4 = XK_F4,
        KEY_F5 = XK_F5,
        KEY_F6 = XK_F6,
        KEY_F7 = XK_F7,
        KEY_F8 = XK_F8,
        KEY_F9 = XK_F9,
        KEY_F10 = XK_F10,
        KEY_F11 = XK_F11,
        KEY_F12 = XK_F12,

        KEY_Esc = XK_Escape,
        KEY_Enter = XK_Return,
        KEY_BackSpace = XK_BackSpace,
        KEY_Tab = XK_Tab,
        KEY_Pause = XK_Pause,
        KEY_ScrollLock = XK_Scroll_Lock,
        KEY_PrintScreen = XK_Sys_Req,
        KEY_Delete = XK_Delete,

        KEY_LShift = XK_Shift_L,
        KEY_RShift = XK_Shift_R,
        KEY_LControl = XK_Control_L,
        KEY_RControl = XK_Control_R,
        KEY_CapsLock = XK_Caps_Lock,
        KEY_LAlt = XK_Alt_L,
        KEY_RAlt = XK_Alt_R,

        KEY_Home = XK_Home,
        KEY_Left = XK_Left,
        KEY_Up = XK_Up,
        KEY_Down = XK_Down,
        KEY_Right = XK_Right,
        KEY_PageUp = XK_Page_Up,
        KEY_PageDown = XK_Page_Down,
        KEY_End = XK_End,
        KEY_Begin = XK_Begin,
    };


	Input() { }
	~Input() { }

	bool	CreateInput(Window* window, bool background = false);
	void	DestroyInput() {}

	void	Update();

	/**
	 * Determines if they key is currently down
	 */
	bool	KeyDown(int key);

	/**
	 * determines if the key was pressed
	 */
	bool	KeyPressed(int key);

	/**
	 * looks for any pressed keys, and returns it
	 * as a string, but is alphaNumeric is set to true
	 * then it will only return a value if a-z or 0-9 is pressed
	 */
	std::string GetPressedKey(bool alphaNumeric = false) {return std::string("");}

	/**
	 * give this a string like "a" or "shift"
	 * and this function will return the
	 * dx key code eg. DIK_A or DIK_SHIFT
	 */
	int		GetKeyCode(std::string str) {return 0;}

	/**
	 * give this something like DIK_A
	 * and it will return "a"
	 */
	std::string GetAsciiCode(int keyCode, bool alphaNumeric = false) {return std::string("");}

	/**
	 * Return the state of the mouse
	 */
	void	GetMouseState( int& x, int& y ) {}

	/**
	 * Determines if they mouse button is currently down
	 */
	bool	MouseDown(int button) { return true;}

	/**
	 * determines if the mouse button was pressed
	 */
	bool	MousePressed(int button) { return true;}


	void	SetMouseVisibility(bool visible) {}
	void	SetMouseWarp(bool warp) {}

	void	SetAltTabStyleMouse(bool doAltTab) {}

	int		GetJoystickCount() { return 0; };
	bool	JoystickButtonDown(int joystick, int button) { return true; };
	bool	JoystickButtonPressed(int joystick, int button) { return true; };
	long int	JoystickAxisState(int joystick, JoystickAxis axis) { return 0; };

protected:

    void NotifyKeyState(int key, bool state);

    Window* window;

    // keyboard
	bool keys[256];
	bool keyLock[256];
};

}

#endif
