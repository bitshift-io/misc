//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_LIGHT_
#define _SIPHON_LIGHT_

#include "Math3D.h"

namespace Siphon
{

class Light : public Sphere
{
public:
	Light();

	static void Enable();
	static void Disable();

	void Draw(float length = 1.0f) const;

	void Bind(int lightNum = 0);

	void SetAmbient(const Vector3& ambient);
	void SetSpecular(const Vector3& specular);
	void SetDiffuse(const Vector3& diffuse);

	void SetRange();

protected:

	Vector3 ambient;
	Vector3 specular;
	Vector3 diffuse;
};

}
#endif
