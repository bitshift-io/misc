#ifndef _SIPHONWIN32WINDOW_
#define _SIPHONWIN32WINDOW_

#include <windows.h>		// Header File For Windows

namespace Siphon
{

class Window
{
	friend LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	friend class Input;

public:
	Window();
	Window(char* title, int width, int height, int bits, bool fullscreen);
	~Window();	

	bool Attach(int hWnd);
	bool Create(char* title, int width, int height, int bits, bool fullscreen, int multisample = 4);
	void Destroy();

	void ShowWindow();

	static bool	IsSupported(int width, int height, int bits);

	void Display();

	// retuens true if quit message recived
	bool HandleMessages();

	void* GetExtensionAddress(const char* extension);

	int GetWidth() { return width; }
	int GetHeight() { return height; }

	// returns cursor position relative to client area
	// returns true if in the client area
	bool GetCursorPosition(int& x, int& y);
	int SetCursorVisibility(bool visible);
	bool IsCursorVisible();

	int GetHandle();

	bool HasFocus() { return focus; }

	bool ShouldExit();

protected:
	LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	int GetMultisamplePixelFormat(HDC hDC, int multisample);
	bool CreateInternal(char* title, int width, int height, int bits, bool fullscreen, int pixelFormat = -1);

	HDC			hDC;		// Private GDI Device Context
	HGLRC		hRC;		// Permanent Rendering Context
	HWND		hWnd;		// Holds Our Window Handle
	HINSTANCE	hInstance;		// Holds The Instance Of The Application
	bool		fullscreen;
	bool		focus;
	bool		shouldExit;
	int			width;
	int			height;
};

}


#endif