//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------

#include "Math3D.h"

namespace Siphon
{

const float Math::Epsilon = 0.00000001f;
const float Math::Pi = 3.1415f;


Math::ENUM_SSE Math::SupportsSSE()
{
	//does OS/CPU support SSE/SSE2/MMX instructions?
	// http://osdev.berlios.de/cpuid.html

	ENUM_SSE support = NONE;
/*
	char vendorSign[13];
	unsigned long maxEax;

	__asm
	{
		mov eax, 0x0
		cpuid
		mov	dword ptr[vendorSign], ebx
		mov	dword ptr[vendorSign+4], edx
		mov	dword ptr[vendorSign+8], ecx

		mov dword ptr[maxEax], eax
	}

	vendorSign[12]=0;
	//Engine::GetInstance()->Log("CPU Vendor: %s\n", vendorSign);

	if (maxEax < 1)
		return NONE;

	unsigned long regEdx;

	__asm
	{
		mov eax, 0x1
		cpuid
		mov regEdx, edx
	}

	if (regEdx & (0x1 << 23))
	{
		//Engine::GetInstance()->Log("MMX Supported\n");
		support = ENUM_SSE(support | MMX);
	}

	if (regEdx & (0x1 << 25))
	{
		//Engine::GetInstance()->Log("SSE Supported\n");
		support = ENUM_SSE(support | SSE);
	}

	if (regEdx & (0x1 << 26))
	{
		//Engine::GetInstance()->Log("SSE2 Supported\n");
		support = ENUM_SSE(support | SSE2);
	}

	/*
	if (strcmp(vendorSign, "AuthenticAMD") == 0)
	{
		if (regEdx & (0x1 << 30))
			Engine::GetInstance()->Log("3DNowExt Supported\n");

		if (regEdx & (0x1 << 31))
			Engine::GetInstance()->Log("3DNow Supported\n");
	}*/

	return support;
}

}
