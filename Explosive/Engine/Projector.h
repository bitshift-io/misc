#ifndef _SIPHON_PROJECTOR_
#define _SIPHON_PROJECTOR_

//#include "Camera.h"
//#include "Texture.h"

namespace Siphon
{

class Camera;
class Texture;

class Projector
{
public:
	Projector() : camera(0), texture(0) {}
	Projector(Camera* camera, Texture* texture) : camera(camera), texture(texture) {}
	void SetTexture(Texture* texture) { this->texture = texture; }
	void SetCamera(Camera* camera) { this->camera = camera; }

	Camera* GetCamera() { return camera; }
	Texture* GetTexture() { return texture; }

protected:
	Camera* camera;
	Texture* texture;
};

}

#endif
