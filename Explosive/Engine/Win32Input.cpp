#pragma comment(lib, "dinput.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

#include "Win32Input.h"
#include "Win32Window.h"
#include "../Util/Log.h"

namespace Siphon
{

Input::Input() : 
	pKeyboardDevice(0), 
	pDI(0), 
	pMouseDevice(0), 
	mouseVisibility(true), 
	window(0), 
	altTabStyleMouse(false),
	mouseWarp(0)
{
	memset(&keys, 0x0, 256);
	memset(&keyLock, 0x0, 256);
	memset(&mouseButtonLock, 0x0, 8);
	memset(&mouse, 0x0, sizeof(DIMOUSESTATE2));
}

Input::~Input()
{
	DestroyInput();
}

bool Input::CreateInput(Window* window, bool background)
{
	Log::Print("----------------------------------------\n");
	Log::Print("\tInput\n");
	Log::Print("----------------------------------------\n");

	this->window = window;
	this->background = background;
#ifdef DEBUG
	// enable background while in debug mode
	//background = true;
#endif

	// Set the cooperative level 
	DWORD flags = background ? DISCL_BACKGROUND : DISCL_FOREGROUND;
	flags |= window->fullscreen ? DISCL_EXCLUSIVE : DISCL_NONEXCLUSIVE; //(background ? DISCL_NONEXCLUSIVE : DISCL_EXCLUSIVE);

	HRESULT hr; 
 
	hr = DirectInput8Create(window->hInstance, DIRECTINPUT_VERSION, 
			IID_IDirectInput8, (void**)&pDI, NULL); 
	if FAILED(hr) 
	{ 
		// DirectInput not available; take appropriate action 
		Log::Print("Input\t\t\t[FAIL]\n");
		return false; 
	} 

	// 
	// Keyboard
	//
	hr = pDI->CreateDevice(GUID_SysKeyboard, &pKeyboardDevice, NULL); 
	if FAILED(hr) 
	{ 
		Log::Print("Keyboard\t\t\t[FAIL]\n");
		return false; 
	} 

	hr = pKeyboardDevice->SetDataFormat(&c_dfDIKeyboard); 
	if FAILED(hr) 
	{ 
		Log::Print("Keyboard\t\t\t[FAIL]\n");
		return false; 
	}

	hr = pKeyboardDevice->SetCooperativeLevel(window->hWnd, 
			flags); 
	if FAILED(hr) 
	{ 
		Log::Print("Keyboard\t\t\t[FAIL]\n");
		return false; 
	}

	pKeyboardDevice->Acquire(); 
	Log::Print("Keyboard\t\t\t[OK]\n");

	//
	// Mouse
	// 
	hr = pDI->CreateDevice(GUID_SysMouse, &pMouseDevice, NULL); 
	if FAILED(hr)
	{
		Log::Print("Mouse\t\t\t\t[FAIL]\n");
		return false; 
	}

	hr = pMouseDevice->SetDataFormat(&c_dfDIMouse2); 
	if FAILED(hr) 
	{  
		Log::Print("Mouse\t\t\t\t[FAIL]\n");
		return false; 
	}

	hr = pMouseDevice->SetCooperativeLevel(window->hWnd, 
			flags); 
	if FAILED(hr) 
	{ 
		Log::Print("Mouse\t\t\t\t[FAIL]\n");
		return false; 
	}

	pMouseDevice->Acquire(); 
	Log::Print("Mouse\t\t\t\t[OK]\n");

	//
	// Joysticks
	//
	pDI->EnumDevices(DI8DEVCLASS_GAMECTRL, Input::EnumJoystickCallback,
		this, DIEDFL_ATTACHEDONLY);

	Log::Print("----------------------------------------\n");

	return true;
}

BOOL CALLBACK Input::EnumJoystickCallback(const DIDEVICEINSTANCE* pdidInstance, VOID* pContext)
{
	Input* pInput = (Input*)pContext;
	HRESULT hr;

	Joystick joystick;

	// Obtain an interface to the enumerated joystick.
	hr = pInput->pDI->CreateDevice(pdidInstance->guidInstance,  
		&joystick.pDevice, NULL);
	if (FAILED(hr))
	{
		Log::Print("Joystick %i\t\t\t[FAIL]\n", pInput->joysticks.GetSize());
		return DIENUM_CONTINUE;
	}

	if (FAILED(hr = joystick.pDevice->SetDataFormat(&c_dfDIJoystick2)))
	{
		Log::Print("Joystick %i\t\t\t[FAIL]\n", pInput->joysticks.GetSize());
		return DIENUM_CONTINUE;
	}

	// Set the cooperative level 
	DWORD flags = pInput->background ? DISCL_BACKGROUND : DISCL_FOREGROUND;
	flags |= pInput->window->fullscreen ? DISCL_EXCLUSIVE : DISCL_NONEXCLUSIVE; //(background ? DISCL_NONEXCLUSIVE : DISCL_EXCLUSIVE);

	if (FAILED(hr = joystick.pDevice->SetCooperativeLevel(pInput->window->hWnd, 
		DISCL_EXCLUSIVE | DISCL_FOREGROUND)))
	{
		Log::Print("Joystick %i\t\t\t[FAIL]\n", pInput->joysticks.GetSize());
		return hr;
	}

	// get caps
	DIDEVCAPS diDevCaps;
	diDevCaps.dwSize = sizeof(DIDEVCAPS);
	if (FAILED(hr = joystick.pDevice->GetCapabilities(&diDevCaps)))
	{
		Log::Print("Joystick %i\t\t\t[FAIL]\n", pInput->joysticks.GetSize());
		return hr;
	}

	Log::Print("Joystick %i\t\t\t[OK]\n", pInput->joysticks.GetSize());

	joystick.caps = diDevCaps;
	pInput->joysticks.Insert(joystick);
	
	return DIENUM_CONTINUE;
}

void Input::DestroyInput()
{
	if (pKeyboardDevice)
	{
		pKeyboardDevice->Unacquire();
		pKeyboardDevice->Release();
		pKeyboardDevice = 0;
	}

	if (pMouseDevice)
	{
		pMouseDevice->Unacquire();
		pMouseDevice->Release();
		pMouseDevice = 0;
	}

	if (pDI)
	{
		pDI->Release();
		pDI = 0;
	}
}

void Input::Update()
{ 
	if (!window)
		return;

	HRESULT  hr; 

	//
	// Keyboard
	//
	memset(&keys, 0x0, 256);
    hr = pKeyboardDevice->GetDeviceState(256,(LPVOID)&keys); 
	if FAILED(hr) 
	{ 
		// If input is lost then acquire and keep trying 
        hr = pKeyboardDevice->Acquire();
        while (hr == DIERR_INPUTLOST) 
            hr = pKeyboardDevice->Acquire();

		memset(&keys, 0x0, 256);
	}

	//
	// Mouse
	//
	memset(&mouse, 0x0, sizeof(DIMOUSESTATE2));
	hr = pMouseDevice->GetDeviceState(sizeof(DIMOUSESTATE2), &mouse);
	if FAILED(hr) 
	{ 
		// If input is lost then acquire and keep trying 
        hr = pMouseDevice->Acquire();
        while (hr == DIERR_INPUTLOST) 
            hr = pMouseDevice->Acquire();

		memset(&mouse, 0x0, sizeof(DIMOUSESTATE2));
	}

	if (mouseWarp)
	{
		RECT rect;
		GetWindowRect(window->hWnd, &rect); //GetWindowRect GetClientRect
		SetCursorPos((rect.bottom + rect.top) / 2, (rect.right + rect.left) / 2);
	}

	//
	// Joystick
	//
	for (unsigned int i = 0; i < joysticks.GetSize(); ++i)
	{	
		Joystick& joystick = joysticks[i];
		LPDIRECTINPUTDEVICE8& joystickDevice = joystick.pDevice;

		hr = joystickDevice->Poll(); 
		if (FAILED(hr)) 
		{
			// If input is lost then acquire and keep trying 
			hr = joystickDevice->Acquire();
			while(hr == DIERR_INPUTLOST) 
				hr = joystickDevice->Acquire();
		}

		hr = joystickDevice->GetDeviceState(sizeof(DIJOYSTATE2), &joystick.state);
		/*if (FAILED(hr))
		{
			Log::Print("\tInput error 2\n");

			// If input is lost then acquire and keep trying 
			hr = joystickDevice->Acquire();
			while(hr == DIERR_INPUTLOST) 
				hr = joystickDevice->Acquire();
		}*/
	}
}

void Input::NotifyActive(bool active)
{
	if (altTabStyleMouse)
	{
		SetMouseVisibility(!mouseWarp);
		SetMouseWarp(!mouseWarp);
	}
}

void Input::SetAltTabStyleMouse(bool doAltTab)
{
	altTabStyleMouse = doAltTab;

	if (window->focus)
	{
		SetMouseWarp(true);
		SetMouseVisibility(false);
	}
}

void Input::GetMouseState( int& x, int& y )
{
	x = mouse.lX;
	y = mouse.lY;
}

bool Input::MouseDown(int button)
{
	return mouse.rgbButtons[button] & 0x80 ? true : false;
}

bool Input::MousePressed(int button)
{
	if (MouseDown(button) && mouseButtonLock[button] == false)
	{
		mouseButtonLock[button] = true;
		return true;
	}

	if (!MouseDown(button))
	{
		mouseButtonLock[button] = false;
	}

	return false;
}

long int Input::JoystickAxisState(int joystick, JoystickAxis axis)
{
	Joystick& joy = joysticks[joystick];

	switch (axis)
	{
	case X:
		return joy.state.lX - 32767;
	case Y:
		return joy.state.lY - 32767;
	case Z:
		return joy.state.lZ - 32767;
	};

	return 0;
}

bool Input::JoystickButtonDown(int joystick, int button)
{
	return joysticks[joystick].state.rgbButtons[button] & 0x80 ? true : false;
}

bool Input::JoystickButtonPressed(int joystick, int button)
{
	Joystick& joy = joysticks[joystick];

	if (JoystickButtonDown(joystick, button) && joy.buttonLock[button] == false)
	{
		joy.buttonLock[button] = true;
		return true;
	}

	if (!JoystickButtonDown(joystick, button))
	{
		joy.buttonLock[button] = false;
	}

	return false;
}

bool Input::KeyDown(int key)
{
	return keys[key] & 0x80 ? true : false;
}

bool Input::KeyPressed(int key)
{
	if (KeyDown(key) && keyLock[key] == false)
	{
		keyLock[key] = true;
		return true;
	}
	if (!KeyDown(key))
	{
		keyLock[key] = false;
	}

	return false;
}

std::string Input::GetPressedKey(bool alphaNumeric)
{
	for(int i=0; i < 256; i++)
	{
		if( KeyPressed(i) )
		{
			return std::string( GetAsciiCode(i, alphaNumeric) );
		}
	}

	return std::string("");
}

std::string Input::GetAsciiCode(int keyCode, bool alphaNumeric)
{
	//alpha numeric keys
	switch( keyCode )
	{
	case DIK_A:
		return std::string("a");
	case DIK_B:
		return std::string("b");
	case DIK_C:
		return std::string("c");
	case DIK_D:
		return std::string("d");
	case DIK_E:
		return std::string("e");
	case DIK_F:
		return std::string("f");
	case DIK_G:
		return std::string("g");
	case DIK_H:
		return std::string("h");
	case DIK_I:
		return std::string("i");
	case DIK_J:
		return std::string("j");
	case DIK_K:
		return std::string("k");
	case DIK_L:
		return std::string("l");
	case DIK_M:
		return std::string("m");
	case DIK_N:
		return std::string("n");
	case DIK_O:
		return std::string("o");
	case DIK_P:
		return std::string("p");
	case DIK_Q:
		return std::string("q");
	case DIK_R:
		return std::string("r");
	case DIK_S:
		return std::string("s");
	case DIK_T:
		return std::string("t");
	case DIK_U:
		return std::string("u");
	case DIK_V:
		return std::string("v");
	case DIK_W:
		return std::string("w");
	case DIK_X:
		return std::string("x");
	case DIK_Y:
		return std::string("y");
	case DIK_Z:
		return std::string("z");

	case DIK_0:
		return std::string("0");
	case DIK_1:
		return std::string("1");
	case DIK_2:
		return std::string("2");
	case DIK_3:
		return std::string("3");
	case DIK_4:
		return std::string("4");
	case DIK_5:
		return std::string("5");
	case DIK_6:
		return std::string("6");
	case DIK_7:
		return std::string("7");
	case DIK_8:
		return std::string("8");
	case DIK_9:
		return std::string("9");
/*
	case DIK_SPACE:
		return std::string(" ");
	case DIK_RETURN:
		return std::string("\n");*/
	case DIK_PERIOD:
		return std::string(".");
	}

	if( alphaNumeric )
		return std::string("");

	//special keys
	switch( keyCode )
	{
	case DIK_SPACE:
		return std::string("space");
	case DIK_RETURN:
		return std::string("return");
	case DIK_BACK:
		return std::string("backspace");
	}
	//TODO: finish adding special keys

	return std::string("");
}

int Input::GetKeyCode(std::string str)
{
	if( str == "a" )
		return DIK_A;
	else if( str == "b" )
		return DIK_B;
	else if( str == "c" )
		return DIK_C;
	else if( str == "d" )
		return DIK_D;
	else if( str == "e" )
		return DIK_E;
	else if( str == "f" )
		return DIK_F;
	else if( str == "g" )
		return DIK_G;
	else if( str == "h" )
		return DIK_H;
	else if( str == "i" )
		return DIK_I;
	else if( str == "j" )
		return DIK_J;
	else if( str == "k" )
		return DIK_K;
	else if( str == "l" )
		return DIK_L;
	else if( str == "m" )
		return DIK_M;
	else if( str == "n" )
		return DIK_N;
	else if( str == "o" )
		return DIK_O;
	else if( str == "p" )
		return DIK_P;
	else if( str == "q" )
		return DIK_Q;
	else if( str == "r" )
		return DIK_R;
	else if( str == "s" )
		return DIK_S;
	else if( str == "t" )
		return DIK_T;
	else if( str == "u" )
		return DIK_U;
	else if( str == "v" )
		return DIK_V;
	else if( str == "w" )
		return DIK_W;
	else if( str == "x" )
		return DIK_X;
	else if( str == "y" )
		return DIK_Y;
	else if( str == "z" )
		return DIK_Z;

	if( str == "space" )
		return DIK_SPACE;
	//TODO: finish adding special keys

	return 0;
}

void Input::AcquireMouse()
{
	pMouseDevice->Acquire();
}

void Input::UnAquireMouse()
{
	pMouseDevice->Unacquire();
}

void Input::SetMouseVisibility(bool visible)
{
	mouseVisibility = visible;
	ShowCursor(mouseVisibility);
}

void Input::SetMouseWarp(bool warp)
{
	mouseWarp = warp;
}

}
