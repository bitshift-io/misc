//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------

#include "Light.h"

#define max(a, b) a > b ? a : b

namespace Siphon
{
//--------------------------------------------------------------------------------------

Light::Light() :
	Sphere(0.0f, Vector3(0.0f,0.0f,0.0f)),
	diffuse(0.5f, 0.5f, 0.5f),
	ambient(0.5f, 0.5f, 0.5f),
	specular(0.3f, 0.3f, 0.3f)
{
/*
	glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.0f );
	glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.0 );
	glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.0 );
	*/

	SetRange();
}

void Light::SetRange()
{
	// range = 1.0f / (kc + kld + kqd2);
	//float kc = 1.0f, kl = 0.0f, kq = 0.0f;
	float kc = 0.0f, kl = 0.0f, kq = 0.4f;
/*	glGetLightfv(0, GL_CONSTANT_ATTENUATION, &kc);
	glGetLightfv(0, GL_LINEAR_ATTENUATION, &kl);
	glGetLightfv(0, GL_QUADRATIC_ATTENUATION, &kq);
*/

	// 0.1f = 1.0f / (kc + kld + kqd2);
	// (kc + kld + kqd2) = 10
	// kqd2 + kld + (kc - 10) = 0
	//
	// d = -kl +/- sqrt( kl^2 - 4 * kq * (kc - 10) )
	//     -------------------------------------------
	//           2 * kq
	//
	// http://mathworld.wolfram.com/QuadraticEquation.html

	if (kq == 0.0f)
		kq = 0.01f;

	float a = (-kl + sqrtf(powf(kl,2.0f) - (4.0f * kq * (kc - 10)))) / (2.0f * kq);
	float b = (-kl - sqrtf(powf(kl,2.0f) - (4.0f * kq * (kc - 10)))) / (2.0f * kq);

	radius = max(a,b);
}

void Light::Enable()
{
	glEnable(GL_LIGHTING);
}

void Light::Disable()
{
	glDisable(GL_LIGHTING);
}

void Light::Bind(int lightNum)
{
	glEnable(GL_LIGHT0 + lightNum);
	glMatrixMode(GL_MODELVIEW);
	//Engine::GetInstance()->GetRenderMgr()->GetActiveCamera()->LookAt();

	float pos[4] = { position[0], position[1], position[2], 1 };
	glLightfv(GL_LIGHT0 + lightNum, GL_POSITION, pos);

	float amb[4] = { ambient[0], ambient[1], ambient[2], 1 };
	glLightfv(GL_LIGHT0 + lightNum, GL_AMBIENT, amb);

	float diff[4] = { diffuse[0], diffuse[1], diffuse[2], 1 };
	glLightfv(GL_LIGHT0 + lightNum, GL_DIFFUSE, diff);

	float spec[4] = { specular[0], specular[1], specular[2], 1 };
	glLightfv(GL_LIGHT0 + lightNum, GL_SPECULAR, spec);
}

void Light::SetAmbient(const Vector3& ambient)
{
	this->ambient = ambient;
}

void Light::SetSpecular(const Vector3& specular)
{
	this->specular = specular;
}

void Light::SetDiffuse(const Vector3& diffuse)
{
	this->diffuse = diffuse;
}

void Light::Draw(float length) const
{
	Matrix4 trans(Matrix4::Identity);
	trans.SetTranslation(position[0], position[1], position[2]);
	trans.Draw(length);
	Sphere::Draw();
}

}
