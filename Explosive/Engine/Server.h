#ifndef _SIPHON_SERVER_
#define _SIPHON_SERVER_

#include "Util/Singleton.h"
#include "NetworkBase.h"
#include "Array.h"
#include "RDP/RDPStream.h"
#include <vector>


namespace Siphon
{

using namespace std;

class Connection
{
public:
	Connection() : reliable(SocketStream::Reliable), unreliable(SocketStream::UnReliable)  
	{ 
	}

	RDPStream		stream;
	RDPChannelHandle reliableHandle;
	RDPChannelHandle unreliableHandle;

	SocketStream	reliable;
	SocketStream	unreliable;
	unsigned int	clientId;
};

class ServerBase : public NetworkBase
{
public:
	ServerBase();
	virtual ~ServerBase() { }

	virtual bool Init(unsigned int gameId);
	virtual void Shutdown();

	virtual bool Listen(int port, int flags = 0, const char* name = 0);
	
	// accept connections
	virtual void CheckConnections();

	// send to all
	bool Send(const void* data, int size, SocketStream::Type type = SocketStream::Reliable);

	// send to a particular client
	bool Send(int index, const void* data, int size, SocketStream::Type type = SocketStream::Reliable);

	// receive data from a client
	// dont use this unless you know what your doing!
	int Receive(int index, void* data, unsigned int size, SocketStream::Type type = SocketStream::Reliable);

	// if you use 'Update' use these to get buffered data
	// this is the recommended way
	int GetBufferData(int index, void* data, int size, SocketStream::Type type = SocketStream::Reliable);
	int PeekBufferData(int index, void* data, int size, SocketStream::Type type = SocketStream::Reliable);

	virtual void Update();

	int GetNumClients() { return clients.size(); }
	Connection& GetClientConnection(int idx) { return clients[idx]; }

	virtual unsigned long GetLocalAddress();

	void SetNumPlayers(int numPlayers) { this->numPlayers = numPlayers; }

	RDPStream& GetRDPStream()			{ return stream; }

protected:
	virtual void AcceptConnection(int index, Connection& client);
	virtual void ClientDisconnect(int index, Connection& client);

	// check broadcast stream for messages
	virtual void CheckBroadcast();

	RDPStream stream;
	SocketStream reliable;
	SocketStream broadcast; // for broadcasts

	vector<Connection> clients;
	unsigned int clientCounter;
	int flags;
	int port;
	std::string addr;

	unsigned int	gameId;
	std::string		name;		// name of the server
	int				numPlayers;
};

class Server : public ServerBase, public Singleton<Server>
{
public:
	Server() : ServerBase() { }
	virtual ~Server() { }

protected:

};

}

#endif