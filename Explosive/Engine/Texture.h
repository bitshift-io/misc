//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_TEXTURE_
#define _SIPHON_TEXTURE_

#include "MaterialDefs.h"
#include "Array.h"
#include "Device.h"

namespace Siphon
{

class TextureFile;

/**
 * This hold the opengl texture,
 * plus extra parameters on the texture,
 * blend operations, cube map etc...
 */
class Texture
{
	friend class TextureMgr;
	friend class RenderTexture;

public:

	enum Defines
	{
		Unknown = -1
	};

	enum TextureFlags
	{
		TF_None					= 0x0,
		TF_KeepCopyInMemory		= 0x1 << 0,
		TF_FloatFormat			= 0x1 << 1, 
		TF_Nearest				= 0x1 << 2,
		TF_NoMipMaps			= 0x1 << 3,
		TF_VertexTexture		= 0x1 << 4, // for vertex texture fetches
	};

	Texture();
	Texture(const char* name);

	~Texture();

	/**
	 * bind a texture to a texture unit,
	 * must supply which unit!
	 */
	virtual void Bind(int texUnit = 0);

	void Draw(int x = 0, int y = 0, int size = 180);
	void Draw(int x1, int y1, int x2, int y2);

	const char* GetName();

	static void RestoreDefault();


	//void RenderQuad( int lx, int ty, int rx, int by );


	//static Texture* Load(const char* file, TextureFlags creationFlags = None);

	inline GLuint GetHandle() { return texture; }

	unsigned char* GetPixels();
	int GetWidth();
	virtual int GetHeight();
	virtual int GetBPP();

protected:

	bool GenerateTexture(char* image, int width, int height, GLint internalFormat, GLint format, GLenum type = GL_UNSIGNED_BYTE, GLint target = GL_TEXTURE_2D, int flags = 0);
	virtual bool Load(const char* name, int flags = TF_None);

	GLint			target;
	GLuint			texture;
	std::string		file;
	TextureFile*	textureFile;
};

// FMNOTE: this shouldnt texture, it should return a pointer to a number of textures,
// that way we render to a number of textures, and can bind any texture we need.
class RenderTexture// : public Texture
{
public:

	enum TextureFlags
	{
		TF_None	= 0x0,
		TF_Depth	= 0x1 << 1,
		TF_CubeMap = 0x1 << 2,
		TF_Stencil = 0x1 << 3,
		TF_DepthTexture = 0x1 << 4,
		TF_Colour	= 0x1 << 5
	};

	RenderTexture();
	~RenderTexture();

	bool Create(int width = 512, int height = 512, int flags = TextureFlags(TF_Depth | TF_Colour));

	// used when rendering too
	int Lock();
	void UnLock();

	void BindTarget(int index, bool clear = true);

	void SetClearColour(float r, float g, float b, float a = 1.0f);

	// we should save flags and only return is its a valid bindable texture!
	Texture* GetColourTexture() { return texture; }
	Texture* GetDepthTexture() { return depth; }
	Texture* GetStencilTexture() { return stencil; }

//protected:
	Texture* texture;
	Texture* depth;
	Texture* stencil;

	GLuint fb;  // color render target
	//GLuint depth; // depth render target
	//GLuint stencil; // depth render target

	int width;
	int height;

	float	clearColour[4];
	GLenum target;
};

class TextureCube : public Texture
{
	friend class TextureMgr;

public:
	//static Texture* Load(const char* name, char* file[], TextureFlags creationFlags = None);

	virtual void Bind(int texUnit = 0);

protected:
	virtual bool Load(char* name[]);
	bool LoadFace(int face, const char* file);
	void GenerateTextureFace(int face, char* image, int width, int height, GLint format, GLenum type = GL_UNSIGNED_BYTE);

	//Texture* textures[6];
};

class TextureMgr
{
public:
	static Texture* LoadTex(const char* name, int flags = Texture::TF_None)
	{
		std::string path = basePath + std::string(name);
		for (unsigned int i = 0; i < tex.GetSize(); ++i)
		{
			Texture* res = tex[i];
			if (strcmp(res->GetName(), path.c_str()) == 0)
				return res;
		}

		Texture* res = new Texture();
		if (!res->Load(path.c_str(), flags))
		{
			delete res;
			return 0;
		}

		tex.Insert(res);
		return res;
	}

	static TextureCube* LoadTexCube(char* name[])
	{
		char* buffer[6];
		for (unsigned int i = 0; i < 6; ++i)
		{
			buffer[i] = new char[256];
			sprintf(buffer[i], "%s%s", basePath.c_str(), name[i]);
		}

		std::string cubeName = std::string(buffer[0]);
		for (unsigned int i = 1; i < 6; ++i)
			cubeName += std::string("#") + std::string(buffer[i]);

		for (unsigned int i = 0; i < texCube.GetSize(); ++i)
		{
			TextureCube* res = texCube[i];
			if (strcmp(res->GetName(), cubeName.c_str()) == 0)
				return res;
		}

		TextureCube* res = new TextureCube();
		if (!res->Load(buffer))
		{
			for (unsigned int i = 1; i < 6; ++i)
				delete[] buffer[i];

			delete res;
			return 0;
		}

		for (unsigned int i = 1; i < 6; ++i)
				delete[] buffer[i];

		texCube.Insert(res);
		return res;
	}

	static void SetBasePath(const char* path)
	{
		basePath = std::string(path);
	}

protected:
	static Array<Texture*> tex;
	static Array<TextureCube*> texCube;
	static std::string basePath;
};

}
#endif
