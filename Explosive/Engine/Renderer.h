#ifndef _SIPHON_RENDERER_
#define _SIPHON_RENDERER_

#include "Device.h"
#include "MaterialDefs.h"
#include "Mesh.h"
#include "Light.h"
#include "Camera.h"
#include "PostEffect.h"
#include "Projector.h"

#include "List.h"

namespace Siphon
{

class Renderer
{
public:
	struct MeshNode : public List<MeshNode>
	{
		Mesh*	mesh;
		int		materialIdx;
	};

	struct MaterialNode : public List<MaterialNode>
	{
		Material*		material;
		MeshNode*		meshList;
	};

	struct LightNode : public List<LightNode>
	{
		Light*			light;
	};

	Renderer();
	~Renderer();

	unsigned int Render();

	void SetCamera(Camera* camera) { this->camera = camera; }
	Camera* GetCamera() { return camera; }


	bool RemoveMesh(Mesh* mesh);


	void InsertMesh(Mesh* mesh);
	MaterialNode* InsertMaterial(Material* material);

	// light methods
	void InsertLight(Light* light);
	void RemoveLight(Light* light);

	void InsertProjector(Projector* projector);

	// get the maxLights closest lights to this position
	inline int GetClosestLights(const Sphere& bounds, Light* lightList[], int maxLights);
	inline void BindLights(Light* lightList[], int numLights);

	void CullMeshes(Camera* camera);

	void SetRenderTarget(RenderTexture* target) { renderTarget = target; }
	RenderTexture* GetRenderTarget() { return renderTarget; }

	void SetTime(float t) { time = t; }

	void InsertPostEffect(PostEffect* pEffect);

	void SetClipPlane(const Plane& plane, int idx = 0);

protected:
	MaterialNode*	material;
	LightNode*		lightNodes; // this should be an array
	Array<Mesh*>	meshes; //unique array of meshes
	Array<PostEffect*> postEffects;
	Light*			lights[8]; // this should not be fixed to 8 (but because we are using ff we are)

	Plane			clipPlanes[6];
	int				numClipPlanes;

	Array<Projector*>	projectors;
	Camera*			camera;
	RenderTexture*	renderTarget;
	float			time;
};

void Renderer::BindLights(Light* lightList[], int numLights)
{
	for (int i = 0; i < numLights; ++i)
		lightList[i]->Bind(i);
}

int Renderer::GetClosestLights(const Sphere& bounds, Light* lightList[], int maxLights)
{
	int idx = 0;
	LightNode* lightIt = lightNodes;

	// add the first maxLights lights
	while(lightIt && idx < maxLights)
	{
		// check to make sure this light is in range
		float dist = bounds.Intersect(*(lightIt->light));
		if (dist != -1) //lightIt in range of position)
		{
			lightList[idx] = lightIt->light;
			++idx;
		}

		lightIt = lightIt->pNext;
	}

	while (lightIt)
	{
		//if (lightIt in range of position)
		{
			int i = 0;
			for (; i < idx; ++i)
			{
				float dist = bounds.Intersect(*(lightIt->light));
				if (dist != -1)
				{

				}
				// if (lightIt closer to position than this light)
				//	break;
			}
			if (i < idx)
			{
				lightList[i] = lightIt->light;
			}
			else if (i < maxLights)
			{
				idx = i;
				lightList[i] = lightIt->light;
			}
		}

		lightIt = lightIt->pNext;
	}

	return idx; // if less than maxLights exist return how many we added
}

}
#endif
