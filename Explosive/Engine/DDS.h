#ifndef _SIPHON_DDS_
#define _SIPHON_DDS_

#include <stdlib.h>
#include <stdio.h>

#include "TextureFile.h"

namespace Siphon 
{

//this code has mostly been taken from the devIL
// image library,
// I extracted it so its more
// OO and its the only
// file format i want to supply
// so no use having the
// other code in there

#pragma pack(push,1)
struct DDSHEAD
{
	char	Signature[4];
    
	unsigned int	Size1;				// size of the structure (minus MagicNum)
    unsigned int	Flags1;				// determines what fields are valid
    unsigned int	Height;				// height of surface to be created
    unsigned int	Width;				// width of input surface
    unsigned int	LinearSize;			// Formless late-allocated optimized surface size
    unsigned int	Depth;				// Depth if a volume texture
    unsigned int	MipMapCount;		// number of mip-map levels requested
    unsigned int	AlphaBitDepth;		// depth of alpha buffer requested

	unsigned int	NotUsed[10];

	unsigned int	Size2;				// size of structure
	unsigned int	Flags2;				// pixel format flags
	unsigned int	FourCC;				// (FOURCC code)
	unsigned int	RGBBitCount;		// how many bits per pixel
	unsigned int	RBitMask;			// mask for red bit
	unsigned int	GBitMask;			// mask for green bits
	unsigned int	BBitMask;			// mask for blue bits
	unsigned int	RGBAlphaBitMask;	// mask for alpha channel
	
    unsigned int	ddsCaps1, ddsCaps2, ddsCaps3, ddsCaps4; // direct draw surface capabilities
	unsigned int	TextureStage;
};
#pragma pack(pop)

// use cast to struct instead of RGBA_MAKE as struct is
//  much
typedef struct Color8888
{
	unsigned char r;		// change the order of names to change the 
	unsigned char g;		//  order of the output ARGB or BGRA, etc...
	unsigned char b;		//  Last one is MSB, 1st is LSB.
	unsigned char a;
} Color8888;


typedef struct Color888
{
	unsigned char r;		// change the order of names to change the 
	unsigned char g;		//  order of the output ARGB or BGRA, etc...
	unsigned char b;		//  Last one is MSB, 1st is LSB.
} Color888;


typedef struct Color565
{
	unsigned nBlue  : 5;		// order of names changes
	unsigned nGreen : 6;		//	byte order of output to 32 bit
	unsigned nRed	: 5;
} Color565;


typedef struct DXTColBlock
{
	short col0;
	short col1;

	// no bit fields - use bytes
	char row[4];
} DXTColBlock;

typedef struct DXTAlphaBlockExplicit
{
	short row[4];
} DXTAlphaBlockExplicit;

typedef struct DXTAlphaBlock3BitLinear
{
	char alpha0;
	char alpha1;

	char stuff[6];
} DXTAlphaBlock3BitLinear;


// Defines
#define DDS_ALPHAPIXELS			0x00000001l
#define DDS_ALPHA				0x00000002l
#define DDS_FOURCC				0x00000004l
#define DDS_PITCH				0x00000008l
#define DDS_COMPLEX				0x00000008l
#define DDS_TEXTURE				0x00001000l
#define DDS_MIPMAPCOUNT			0x00020000l
#define DDS_LINEARSIZE			0x00080000l
#define DDS_VOLUME				0x00200000l
#define DDS_MIPMAP				0x00400000l
#define DDS_DEPTH				0x00800000l

#define DDS_CUBEMAP				0x00000200L
#define DDS_CUBEMAP_POSITIVEX	0x00000400L
#define DDS_CUBEMAP_NEGATIVEX	0x00000800L
#define DDS_CUBEMAP_POSITIVEY	0x00001000L
#define DDS_CUBEMAP_NEGATIVEY	0x00002000L
#define DDS_CUBEMAP_POSITIVEZ	0x00004000L
#define DDS_CUBEMAP_NEGATIVEZ	0x00008000L


#define IL_MAKEFOURCC(ch0, ch1, ch2, ch3)                              \
            ((int)(char)(ch0) | ((int)(char)(ch1) << 8) |   \
            ((int)(char)(ch2) << 16) | ((int)(char)(ch3) << 24 ))

enum PixFormat
{
	PF_ARGB,
	PF_RGB,
	PF_DXT1,
	PF_DXT2,
	PF_DXT3,
	PF_DXT4,
	PF_DXT5,
	PF_UNKNOWN
};

// Global variables


#define CUBEMAP_SIDES 6

class DDS : public TextureFile
{
public:
	DDS();
	~DDS();

	bool Load(const char* file);

	virtual unsigned char* GetPixels();
	virtual int GetWidth();
	virtual int GetHeight();

	//bytes per pixel
	virtual int GetBPP();

protected:

	/**
	 * determines the compression type
	 */
	void DecodePixelFormat();

	/**
	 * Read in the core data from the dds
	 */
	bool ReadInData( FILE* pFile );

	/**
	 * creates the image according to dds info
	 */
	bool CreateImage();

	/**
	 * determine which sort of compression
	 * and call the appropriate funciton
	 */
	bool Decompress();

	/**
	 * decompressions routines
	 */
	bool DecompressARGB();
	bool DecompressDXT1();
	bool DecompressDXT2();
	bool DecompressDXT3();
	bool DecompressDXT4();
	bool DecompressDXT5();

	void GetBitsFromMask( unsigned int Mask, unsigned int *ShiftLeft, unsigned int *ShiftRight);
	void CorrectPreMult();

	bool FlipImage();

	DDSHEAD	Head;			// Image header
	unsigned char	*CompData;		// Compressed data
	unsigned int	CompSize;		// Compressed size
	unsigned int	CompLineSize;		// Compressed line size
	unsigned int	CompFormat;		// Compressed format
	unsigned char* Image;
	int	Width, Height, Depth;
	unsigned int	BlockSize;

/*
Image->Width = Width == 0 ? 1 : Width;
	Image->Height = Height == 0 ? 1 : Height;
	Image->Depth = Depth == 0 ? 1 : Depth;
	Image->Bpp = Bpp;
	Image->Bpc = BppType;
	Image->Bps = Width * Bpp * Image->Bpc;
	Image->SizeOfPlane = Image->Bps * Height;
	Image->SizeOfData = Image->SizeOfPlane * Depth;
	Image->Format = Format;
	Image->Type*/

	unsigned int SizeOfData;
	unsigned int SizeOfPlane;
	int Bpc;
	int Bpp;
	int Bps;
};

}
#endif

