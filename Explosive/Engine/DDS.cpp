#include "DDS.h"
#include "Log.h"

namespace Siphon
{

DDS::DDS() : CompData(0), Image(0)
{

}

DDS::~DDS()
{
	free(Image);
}

bool DDS::Load( const char* file)
{
	FILE* pFile = 0;
	pFile = fopen( file, "rb" );

	if( pFile == 0 )
		return false;

	Log::Print("Loading texture: %s\n", file);

	//read header
	if( fread( &Head, sizeof(DDSHEAD), 1, pFile ) != 1 )
	{
		fclose( pFile );
		return false;
	}

	if( Head.Depth == 0 )
		Head.Depth = 1;

	DecodePixelFormat();

	if( CompFormat == PF_UNKNOWN )
	{
		fclose( pFile );
		Log::Print("\tunknown format\n");
		return false;
	}

	// Microsoft bug, they're not following their own documentation.
	if( !( Head.Flags1 & (DDS_LINEARSIZE | DDS_PITCH) ) )
	{
		Head.Flags1 |= DDS_LINEARSIZE;
		Head.LinearSize = BlockSize;
	}

	//something here

	Width = Head.Width;
	Height = Head.Height;
	Depth = Head.Depth;

	if( ReadInData( pFile ) == false )
	{
		fclose( pFile );
		Log::Print("\tread error\n");
		return false;
	}

	if( CreateImage() == false )
	{
		fclose( pFile );
		Log::Print("\terror creating image\n");
		return false;
	}

	if( Decompress() == false )
	{
		fclose( pFile );
		Log::Print("\tdecompress error\n");
		return false;
	}

	free( CompData );
	fclose( pFile );

	return true;
}

unsigned char* DDS::GetPixels()
{
	return (unsigned char*)Image;
}

int DDS::GetWidth()
{
	return Width;
}

int DDS::GetHeight()
{
	return Height;
}

int DDS::GetBPP()
{
	return Bpp;
}

bool DDS::Decompress()
{
	switch( CompFormat )
	{
	case PF_ARGB:
		return DecompressARGB();
	case PF_RGB:
		return DecompressARGB();
	case PF_DXT1:
		return DecompressDXT1();
	case PF_DXT2:
		return DecompressDXT2();
	case PF_DXT3:
		return DecompressDXT3();
	case PF_DXT4:
		return DecompressDXT4();
	case PF_DXT5:
		return DecompressDXT5();
	case PF_UNKNOWN:
		return false;
	}

	//return false;
	return true;
}

bool DDS::ReadInData( FILE* pFile )
{
	unsigned int	Bps;
	int	y, z;
	unsigned char	*Temp;
	unsigned int	Bpp;

	if( CompFormat == PF_RGB )
	{
		Bpp = 3;
	}
	else
	{
		Bpp = 4;
	}
/*
	if( CompData )
	{
		free( CompData );
		CompData = NULL;
	}*/

	if( Head.Flags1 & DDS_LINEARSIZE )
	{
		//Head.LinearSize = Head.LinearSize * Depth;

		CompData = (unsigned char*)malloc( Head.LinearSize );
		if( CompData == NULL )
		{
			return false;
		}

		if( fread( CompData, 1, Head.LinearSize, pFile ) != (unsigned int)Head.LinearSize )
		{
			free( CompData );
			return false;
		}
	}
	else
	{
		Bps = Width * Head.RGBBitCount / 8;
		CompSize = Bps * Height * Depth;
		CompLineSize = Bps;

		CompData = (unsigned char*)malloc( CompSize );
		if( CompData == NULL )
		{
			return false;
		}

		Temp = CompData;
		for (z = 0; z < Depth; z++)
		{
			for (y = 0; y < Height; y++)
			{
				if( fread( Temp, 1, Bps, pFile ) != Bps)
				{
					free( CompData );
					return false;
				}
				Temp += Bps;
			}
		}
	}

	return true;

}

void DDS::DecodePixelFormat()
{
	if(Head.Flags2 & DDS_FOURCC )
	{
		BlockSize = ((Head.Width + 3)/4) * ((Head.Height + 3)/4) * ((Head.Depth + 3)/4);
		switch( Head.FourCC )
		{
			case IL_MAKEFOURCC('D','X','T','1'):
				CompFormat = PF_DXT1;
				BlockSize *= 8;
				//printf("DXT1\n");
				break;

			case IL_MAKEFOURCC('D','X','T','2'):
				CompFormat = PF_DXT2;
				BlockSize *= 16;
				//printf("DXT2\n");
				break;

			case IL_MAKEFOURCC('D','X','T','3'):
				CompFormat = PF_DXT3;
				BlockSize *= 16;
				//printf("DXT3\n");
				break;

			case IL_MAKEFOURCC('D','X','T','4'):
				CompFormat = PF_DXT4;
				BlockSize *= 16;
				//printf("DXT4\n");
				break;

			case IL_MAKEFOURCC('D','X','T','5'):
				CompFormat = PF_DXT5;
				BlockSize *= 16;
				//printf("DXT5\n");
				break;

			default:
				CompFormat = PF_UNKNOWN;
				BlockSize *= 16;
				//printf("UNKNOWN\n");
				break;
		}
	}
	else
	{
		// This dds texture isn't compressed so write out ARGB format
		if (Head.Flags2 & DDS_ALPHAPIXELS)
		{
			CompFormat = PF_ARGB;
		}
		else
		{
			CompFormat = PF_RGB;
		}
		BlockSize = (Head.Width * Head.Height * Head.Depth * (Head.RGBBitCount >> 3));
	}
	return;
}

bool DDS::CreateImage()
{
	Bpc = sizeof(unsigned char);
	switch( CompFormat )
	{
	case PF_RGB:
		Bpp = 3;
		break;
	case PF_ARGB:
		Bpp = 4;
		break;
	default:
		Bpp = 4;
		break;
	};

	SizeOfData = Width * Height * Bpp * Depth;
	Bps = Width * Bpp * Bpc;
	SizeOfPlane = Bps * Height;
	SizeOfData = SizeOfPlane * Depth;

	Image = (unsigned char*)malloc( SizeOfData );
	return true;
}

bool DDS::DecompressDXT1()
{
	int			x, y, z, i, j, k, Select;
	unsigned char		*Temp;
	Color565	*color_0, *color_1;
	Color8888	colours[4], *col;
	unsigned int		bitmask, Offset;

	Temp = CompData;
	for (z = 0; z < Depth; z++)
	{
		int oy = Height - 1;
		for (y = 0; y < Height; y += 4, oy -= 4)
		{
			int ox = Width - 1;
			for (x = 0; x < Width; x += 4, ox -= 4 )
			{

				color_0 = ((Color565*)Temp);
				color_1 = ((Color565*)(Temp+2));
				bitmask = ((unsigned int*)Temp)[1];
				Temp += 8;

				colours[0].r = color_0->nRed << 3;
				colours[0].g = color_0->nGreen << 2;
				colours[0].b = color_0->nBlue << 3;
				colours[0].a = 0xFF;

				colours[1].r = color_1->nRed << 3;
				colours[1].g = color_1->nGreen << 2;
				colours[1].b = color_1->nBlue << 3;
				colours[1].a = 0xFF;


				if (*((unsigned short*)color_0) > *((unsigned short*)color_1))
				{
					// Four-color block: derive the other two colors.
					// 00 = color_0, 01 = color_1, 10 = color_2, 11 = color_3
					// These 2-bit codes correspond to the 2-bit fields
					// stored in the 64-bit block.
					colours[2].b = (2 * colours[0].b + colours[1].b + 1) / 3;
					colours[2].g = (2 * colours[0].g + colours[1].g + 1) / 3;
					colours[2].r = (2 * colours[0].r + colours[1].r + 1) / 3;
					colours[2].a = 0xFF;

					colours[3].b = (colours[0].b + 2 * colours[1].b + 1) / 3;
					colours[3].g = (colours[0].g + 2 * colours[1].g + 1) / 3;
					colours[3].r = (colours[0].r + 2 * colours[1].r + 1) / 3;
					colours[3].a = 0xFF;
				}
				else
				{
					// Three-color block: derive the other color.
					// 00 = color_0,  01 = color_1,  10 = color_2,
					// 11 = transparent.
					// These 2-bit codes correspond to the 2-bit fields
					// stored in the 64-bit block.
					colours[2].b = (colours[0].b + colours[1].b) / 2;
					colours[2].g = (colours[0].g + colours[1].g) / 2;
					colours[2].r = (colours[0].r + colours[1].r) / 2;
					colours[2].a = 0xFF;

					colours[3].b = (colours[0].b + 2 * colours[1].b + 1) / 3;
					colours[3].g = (colours[0].g + 2 * colours[1].g + 1) / 3;
					colours[3].r = (colours[0].r + 2 * colours[1].r + 1) / 3;
					colours[3].a = 0x00;
				}

				for (j = 0, k = 0; j < 4; j++)
				{
					for (i = 0; i < 4; i++, k++)
					{

						Select = (bitmask & (0x03 << k*2)) >> k*2;
						col = &colours[Select];

						if (((x + i) < Width) && ((y + j) < Height))
						{
							//Offset = z * SizeOfPlane + (y + j) * Bps + (x + i) * Bpp;
							//Offset = z * SizeOfPlane + (y + j) * Bps + (x + i) * Bpp;
							Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + (x + i) * Bpp;

							//Offset = SizeOfPlane - Offset;
							Image[Offset + 0] = col->r;
							Image[Offset + 1] = col->g;
							Image[Offset + 2] = col->b;
							Image[Offset + 3] = col->a;
						}
					}
				}
			}
		}
	}

	return true;
}

bool DDS::DecompressDXT2()
{
	// Can do color & alpha same as dxt3, but color is pre-multiplied
	//   so the result will be wrong unless corrected.
	if( !DecompressDXT3() )
		return false;

	CorrectPreMult();

	return true;
}

bool DDS::DecompressDXT3()
{
	int			x, y, z, i, j, k, Select;
	unsigned char		*Temp;
	Color565	*color_0, *color_1;
	Color8888	colours[4], *col;
	unsigned int		bitmask, Offset;
	unsigned short	word;
	DXTAlphaBlockExplicit *alpha;

	Temp = CompData;
	for (z = 0; z < Depth; z++)
	{
		for (y = 0; y < Height; y += 4)
		{
			for (x = 0; x < Width; x += 4)
			{
				alpha = (DXTAlphaBlockExplicit*)Temp;
				Temp += 8;
				color_0 = ((Color565*)Temp);
				color_1 = ((Color565*)(Temp+2));
				bitmask = ((unsigned int*)Temp)[1];
				Temp += 8;

				colours[0].r = color_0->nRed << 3;
				colours[0].g = color_0->nGreen << 2;
				colours[0].b = color_0->nBlue << 3;
				colours[0].a = 0xFF;

				colours[1].r = color_1->nRed << 3;
				colours[1].g = color_1->nGreen << 2;
				colours[1].b = color_1->nBlue << 3;
				colours[1].a = 0xFF;

				// Four-color block: derive the other two colors.
				// 00 = color_0, 01 = color_1, 10 = color_2, 11 = color_3
				// These 2-bit codes correspond to the 2-bit fields
				// stored in the 64-bit block.
				colours[2].b = (2 * colours[0].b + colours[1].b + 1) / 3;
				colours[2].g = (2 * colours[0].g + colours[1].g + 1) / 3;
				colours[2].r = (2 * colours[0].r + colours[1].r + 1) / 3;
				colours[2].a = 0xFF;

				colours[3].b = (colours[0].b + 2 * colours[1].b + 1) / 3;
				colours[3].g = (colours[0].g + 2 * colours[1].g + 1) / 3;
				colours[3].r = (colours[0].r + 2 * colours[1].r + 1) / 3;
				colours[3].a = 0xFF;

				k = 0;
				for (j = 0; j < 4; j++)
				{
					for (i = 0; i < 4; i++, k++)
					{

						Select = (bitmask & (0x03 << k*2)) >> k*2;
						col = &colours[Select];

						if (((x + i) < Width) && ((y + j) < Height))
						{
							Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + (x + i) * Bpp;
							Image[Offset + 0] = col->r;
							Image[Offset + 1] = col->g;
							Image[Offset + 2] = col->b;
						}
					}
				}

				for (j = 0; j < 4; j++)
				{
					word = alpha->row[j];
					for (i = 0; i < 4; i++)
					{
						if (((x + i) < Width) && ((y + j) < Height))
						{
							Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + (x + i) * Bpp + 3;
							Image[Offset] = word & 0x0F;
							Image[Offset] = Image[Offset] | (Image[Offset] << 4);
						}
						word >>= 4;
					}
				}

			}
		}
	}

	return true;
}

bool DDS::DecompressDXT4()
{
	// Can do color & alpha same as dxt5, but color is pre-multiplied
	//   so the result will be wrong unless corrected.
	if( !DecompressDXT5() )
		return false;

	CorrectPreMult();

	return true;
}

void DDS::CorrectPreMult()
{
	unsigned int i;

	for (i = 0; i < SizeOfData; i += 4)
	{
		if( Image[i+3] != 0 )
		{  // Cannot divide by 0.
			Image[i]   = (unsigned char)(((unsigned int)Image[i]   << 8) / Image[i+3]);
			Image[i+1] = (unsigned char)(((unsigned int)Image[i+1] << 8) / Image[i+3]);
			Image[i+2] = (unsigned char)(((unsigned int)Image[i+2] << 8) / Image[i+3]);
		}
	}

	return;
}

bool DDS::DecompressDXT5()
{
	int			x, y, z, i, j, k, Select;
	unsigned char		*Temp;
	Color565	*color_0, *color_1;
	Color8888	colours[4], *col;
	unsigned int		bitmask, Offset;
	unsigned char		alphas[8], *alphamask;
	unsigned int		bits;

	Temp = CompData;
	for (z = 0; z < Depth; z++)
	{
		for (y = 0; y < Height; y += 4)
		{
			for (x = 0; x < Width; x += 4)
			{
				if (y >= Height || x >= Width)
					break;

				alphas[0] = Temp[0];
				alphas[1] = Temp[1];
				alphamask = Temp + 2;
				Temp += 8;
				color_0 = ((Color565*)Temp);
				color_1 = ((Color565*)(Temp+2));
				bitmask = ((unsigned int*)Temp)[1];
				Temp += 8;

				colours[0].r = color_0->nRed << 3;
				colours[0].g = color_0->nGreen << 2;
				colours[0].b = color_0->nBlue << 3;
				colours[0].a = 0xFF;

				colours[1].r = color_1->nRed << 3;
				colours[1].g = color_1->nGreen << 2;
				colours[1].b = color_1->nBlue << 3;
				colours[1].a = 0xFF;

				// Four-color block: derive the other two colors.
				// 00 = color_0, 01 = color_1, 10 = color_2, 11 = color_3
				// These 2-bit codes correspond to the 2-bit fields
				// stored in the 64-bit block.
				colours[2].b = (2 * colours[0].b + colours[1].b + 1) / 3;
				colours[2].g = (2 * colours[0].g + colours[1].g + 1) / 3;
				colours[2].r = (2 * colours[0].r + colours[1].r + 1) / 3;
				colours[2].a = 0xFF;

				colours[3].b = (colours[0].b + 2 * colours[1].b + 1) / 3;
				colours[3].g = (colours[0].g + 2 * colours[1].g + 1) / 3;
				colours[3].r = (colours[0].r + 2 * colours[1].r + 1) / 3;
				colours[3].a = 0xFF;

				k = 0;
				for (j = 0; j < 4; j++)
				{
					for (i = 0; i < 4; i++, k++)
					{

						Select = (bitmask & (0x03 << k*2)) >> k*2;
						col = &colours[Select];

						// only put pixels out < width or height
						if(((x + i) < Width) && ((y + j) < Height))
						{
						//	Offset = z * SizeOfPlane + (y + j) * Bps + (x + i) * Bpp;
						//	Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + ((Width - x) + i) * Bpp;
							Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + (x + i) * Bpp;
						//	Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + ((Width - 1) - (x + i)) * Bpp;
							Image[Offset + 0] = col->r;
							Image[Offset + 1] = col->g;
							Image[Offset + 2] = col->b;
						}
					}
				}

				// 8-alpha or 6-alpha block?
				if (alphas[0] > alphas[1])
				{
					// 8-alpha block:  derive the other six alphas.
					// Bit code 000 = alpha_0, 001 = alpha_1, others are interpolated.
					alphas[2] = (6 * alphas[0] + 1 * alphas[1] + 3) / 7;	// bit code 010
					alphas[3] = (5 * alphas[0] + 2 * alphas[1] + 3) / 7;	// bit code 011
					alphas[4] = (4 * alphas[0] + 3 * alphas[1] + 3) / 7;	// bit code 100
					alphas[5] = (3 * alphas[0] + 4 * alphas[1] + 3) / 7;	// bit code 101
					alphas[6] = (2 * alphas[0] + 5 * alphas[1] + 3) / 7;	// bit code 110
					alphas[7] = (1 * alphas[0] + 6 * alphas[1] + 3) / 7;	// bit code 111
				}
				else
				{
					// 6-alpha block.
					// Bit code 000 = alpha_0, 001 = alpha_1, others are interpolated.
					alphas[2] = (4 * alphas[0] + 1 * alphas[1] + 2) / 5;	// Bit code 010
					alphas[3] = (3 * alphas[0] + 2 * alphas[1] + 2) / 5;	// Bit code 011
					alphas[4] = (2 * alphas[0] + 3 * alphas[1] + 2) / 5;	// Bit code 100
					alphas[5] = (1 * alphas[0] + 4 * alphas[1] + 2) / 5;	// Bit code 101
					alphas[6] = 0x00;										// Bit code 110
					alphas[7] = 0xFF;										// Bit code 111
				}

				// Note: Have to separate the next two loops,
				//	it operates on a 6-byte system.

				// First three bytes
				bits = *((int*)alphamask);
				for (j = 0; j < 2; j++)
				{
					for (i = 0; i < 4; i++)
					{
						// only put pixels out < width or height
						if (((x + i) < Width) && ((y + j) < Height))
						{
							//Offset = z * SizeOfPlane + (y + j) * Bps + (x + i) * Bpp + 3;
							//Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + ((Width - x) + i) * Bpp;// - 3;
							Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + (x + i) * Bpp + 3;
							//Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + ((Width - 1) - (x + i)) * Bpp + 3;
							Image[Offset] = alphas[bits & 0x07];
						}
						bits >>= 3;
					}
				}

				// Last three bytes
				bits = *((int*)&alphamask[3]);
				for (j = 2; j < 4; j++)
				{
					for (i = 0; i < 4; i++)
					{
						// only put pixels out < width or height
						if (((x + i) < Width) && ((y + j) < Height))
						{
							//Offset = z * SizeOfPlane + (y + j) * Bps + (x + i) * Bpp + 3;
							//Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + ((Width - x) + i) * Bpp - 3;
							Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + (x + i) * Bpp + 3;
							//Offset = z * SizeOfPlane + ((Height - 1) - (y + j)) * Bps + ((Width - 1) - (x + i)) * Bpp + 3;
							Image[Offset] = alphas[bits & 0x07];
						}
						bits >>= 3;
					}
				}
			}
		}
	}

	return true;
}

bool DDS::DecompressARGB()
{
	unsigned int	i, ReadI, RedL, RedR, GreenL, GreenR, BlueL, BlueR, AlphaL, AlphaR;
	unsigned char	*Temp;

	GetBitsFromMask(Head.RBitMask, &RedL, &RedR);
	GetBitsFromMask(Head.GBitMask, &GreenL, &GreenR);
	GetBitsFromMask(Head.BBitMask, &BlueL, &BlueR);
	GetBitsFromMask(Head.RGBAlphaBitMask, &AlphaL, &AlphaR);
	Temp = CompData;

	unsigned int pos = SizeOfData + Width*Bpp;

	for( i = 0; i < SizeOfData; i += Bpp )
	{
		ReadI = *((unsigned int*)Temp);
		Temp += (Head.RGBBitCount / 8);

		if( (i/4) % Width == 0 )
		{
			pos -= Width * Bpp * 2;
		}

		Image[pos]   = ((ReadI & Head.RBitMask) >> RedR) << RedL;
		Image[pos+1] = ((ReadI & Head.GBitMask) >> GreenR) << GreenL;
		Image[pos+2] = ((ReadI & Head.BBitMask) >> BlueR) << BlueL;

		if( Bpp == 4 )
		{
			Image[pos+3] = ((ReadI & Head.RGBAlphaBitMask) >> AlphaR) << AlphaL;
			if (AlphaL >= 7)
			{
				Image[pos+3] = Image[pos+3] ? 0xFF : 0x00;
			}
			else if (AlphaL >= 4)
			{
				Image[pos+3] = Image[pos+3] | (Image[pos+3] >> 4);
			}
		}
		pos += Bpp;
	}

	return true;
}

void DDS::GetBitsFromMask( unsigned int Mask, unsigned int *ShiftLeft, unsigned int *ShiftRight)
{
	unsigned int Temp, i;

	if( Mask == 0 )
	{
		*ShiftLeft = *ShiftRight = 0;
		return;
	}

	Temp = Mask;
	for(i = 0; i < 32; i++, Temp >>= 1)
	{
		if( Temp & 1 )
			break;
	}
	*ShiftRight = i;

	// Temp is preserved, so use it again:
	for( i = 0; i < 8; i++, Temp >>= 1 )
	{
		if (!(Temp & 1))
			break;
	}
	*ShiftLeft = 8 - i;

	return;
}

}
