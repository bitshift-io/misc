//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "Texture.h"

#include "DDS.h"
#include "HDR.h"
#include "TGA.h"
#include "Device.h"
#include "Math3D.h"
#include "Macros.h"
#include "TextureFile.h"

#include "Log.h"

#ifndef WIN32
    #define stricmp strcasecmp
#endif

namespace Siphon
{

Array<Texture*> TextureMgr::tex;
Array<TextureCube*> TextureMgr::texCube;
std::string TextureMgr::basePath("data/");

bool Texture::Load(const char* name, int flags)
{
	file = std::string(name);
	const char* extension = name + (strlen(name) - 3);

	Log::Print("Loading texture: %s\n", name);

	GLint internalFormat = GL_RGB;
	GLint format = GL_RGB;
	GLint type = GL_UNSIGNED_BYTE;

	if (stricmp(extension, "dds") == 0)
	{
		textureFile = new DDS();

		if(!textureFile->Load(name))
		{
			Log::Print("Failed to load texture: %s\n", name);
			SAFE_DELETE(textureFile);
			return false;
		}

		if (textureFile->GetBPP() == 4)
		{
			internalFormat = GL_RGBA;
			format = GL_RGBA;
		}

		GenerateTexture((char*)textureFile->GetPixels(), textureFile->GetWidth(), textureFile->GetHeight(), internalFormat, format, type, GL_TEXTURE_2D, flags);

	}
	else if (stricmp(extension, "tga") == 0)
	{
		textureFile = new TGA();

		if(!textureFile->Load(name))
		{
			Log::Print("Failed to load texture: %s\n", name);
			SAFE_DELETE(textureFile);
			return false;
		}

		if (textureFile->GetBPP() == 4)
		{
			internalFormat = GL_RGBA;
			format = GL_RGBA;
		}

		if (flags & TF_VertexTexture)
		{
			format = GL_LUMINANCE;
			internalFormat = GL_RGBA_FLOAT32_ATI;
			((TGA*)textureFile)->ConvertToGreyScale();
		}

		GenerateTexture((char*)textureFile->GetPixels(), textureFile->GetWidth(), textureFile->GetHeight(), internalFormat, format, type, GL_TEXTURE_2D, flags);
	}
	else
	{
		textureFile = new HDR();

		if(!textureFile->Load(name))
		{
			Log::Print("Failed to load texture: %s\n", name);
			SAFE_DELETE(textureFile);
			return false;
		}



		//GenerateTexture((char*)textureFile->GetPixels(), textureFile->GetWidth(), textureFile->GetHeight(),
		//	GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, GL_TEXTURE_RECTANGLE_NV, Nearest | NoMipMaps);

		((HDR*)textureFile)->ConvertToFloat32();

		GenerateTexture((char*)textureFile->GetPixels(), textureFile->GetWidth(), textureFile->GetHeight(),
			GL_FLOAT_RGB32_NV, GL_RGB, GL_FLOAT, GL_TEXTURE_RECTANGLE_NV, TF_Nearest | TF_NoMipMaps);

	}

/*
	if(!textureFile->Load(name))
	{
		Log::Print("Failed to load texture: %s\n", name);
		SAFE_DELETE(textureFile);
		return false;
	}
/*
	if (flags & FloatFormat)
	{
		//GL_RGBA_FLOAT32_ATI
		//GL_LUMINANCE_FLOAT32_ATI
		if (textureFile->GetBPP() == 3)
			format = GL_RGB_FLOAT32_ATI; //GL_RGB32F_ARB;
		else if (textureFile->GetBPP() == 4)
			format = GL_RGBA_FLOAT32_ATI; //GL_RGBA32F_ARB;

		format = GL_RGBA_FLOAT32_ATI;
//		type = GL_FLOAT;
	}
*/

/*
	if (textureFile->GetBPP() == 3)
	{
		GenerateTexture((char*)textureFile->GetPixels(), textureFile->GetWidth(), textureFile->GetHeight(), GL_RGB);
	}
	else if (textureFile->GetBPP() == 4)
	{

	}*/



	//glTexImage2D(texType, 0, GL_RGBA_FLOAT32_ATI/*format*/, width, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE/*type*/, image);

	//GenerateTexture((char*)hdr.GetPixels(), hdr.GetWidth(), hdr.GetHeight(), GL_RGB, GL_FLOAT);

	if (!(flags & TF_KeepCopyInMemory))
		SAFE_DELETE(textureFile);

	return true;
}

bool Texture::GenerateTexture(char* image, int width, int height, GLint internalFormat, GLint format, GLenum type, GLint target, int flags)
{
	glGenTextures(1, &texture);
	glBindTexture(target, texture);

	if (flags & TF_FloatFormat || flags & TF_Nearest || flags & TF_VertexTexture)
	{
		glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
	else
	{
		glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

	if (!(flags & TF_NoMipMaps))
		glTexParameteri(target, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);

	glTexImage2D(target, 0, internalFormat, width, height, 0, format, type, image);
	Device::GetInstance().CheckErrors();

	glBindTexture(target, 0);
	this->target = target;
	return true;
}

Texture::Texture() :
	textureFile(0)
{

}

Texture::Texture(const char* name) :
	file(name),
	textureFile(0)
{

}

Texture::~Texture()
{

}

unsigned char* Texture::GetPixels()
{
	if (textureFile)
		return textureFile->GetPixels();

	return (unsigned char*)Unknown;
}

int Texture::GetWidth()
{
	if (textureFile)
		return textureFile->GetWidth();

	return (int)Unknown;
}

int Texture::GetHeight()
{
	if (textureFile)
		return textureFile->GetHeight();

	return (int)Unknown;
}

int Texture::GetBPP()
{
	if (textureFile)
		return textureFile->GetBPP();

	return (int)Unknown;
}

void Texture::RestoreDefault()
{
	for (int i = 7; i >= 0; --i)
	{
		glActiveTextureARB(GL_TEXTURE0_ARB + i);

		glDisable(GL_TEXTURE_2D);
		glDisable(GL_TEXTURE_CUBE_MAP);

		glDisable(GL_TEXTURE_GEN_S);
		glDisable(GL_TEXTURE_GEN_T);
		glDisable(GL_TEXTURE_GEN_R);
		glDisable(GL_TEXTURE_GEN_Q);

		glMatrixMode(GL_TEXTURE);
		Matrix4 identity(Matrix4::Identity);
		identity.Set();
		glMatrixMode(GL_MODELVIEW);
	}
	glMatrixMode(GL_MODELVIEW);
}

void Texture::Bind( int texUnit )
{
#ifdef DEBUG
	char info[256];
	sprintf(info, "Texture::Bind(%i) - %s", texUnit, file.c_str());
	GDBSTRING(info);
#endif

	glActiveTextureARB(GL_TEXTURE0_ARB + texUnit);
	glEnable(target);
	glBindTexture(target, texture);
}

const char* Texture::GetName()
{
	return file.c_str();
}

void Texture::Draw(int x1, int y1, int x2, int y2)
{
	glMatrixMode(GL_PROJECTION); // and initalize Ortho mode
  	glPushMatrix();
  	glLoadIdentity();
  	glOrtho(0.0f, 800, 600, 0.0f, -1.0f, 1.0f);

  	glMatrixMode(GL_MODELVIEW);
  	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(GL_FALSE);

	Bind(0);
	glColor3f(1, 1, 1);

	glBegin(GL_QUADS);
	{
		glTexCoord2f(0, 1);
		glVertex2i(x1, y1);

		glTexCoord2f(0, 0);
		glVertex2i(x1, y2);

		glTexCoord2f(1, 0);
		glVertex2i(x2, y2);

		glTexCoord2f(1, 1);
		glVertex2i(x2, y1);
	}
	glEnd();

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);

	glMatrixMode(GL_PROJECTION);
  	glPopMatrix();
  	glMatrixMode(GL_MODELVIEW);
  	glPopMatrix();
}

void Texture::Draw(int x, int y, int size)
{
	glMatrixMode(GL_PROJECTION); // and initalize Ortho mode
  	glPushMatrix();
  	glLoadIdentity();
  	glOrtho(0.0f, 800,600, 0.0f,-1.0f,1.0f);

  	glMatrixMode(GL_MODELVIEW);
  	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(GL_FALSE);

	Bind(0);
	glColor3f(1, 1, 1);

	glBegin(GL_QUADS);
	{
		glTexCoord2f(0, 1);
		glVertex2i(0 + x, 0 + y);

		glTexCoord2f(0, 0);
		glVertex2i(0 + x, size + y);

		glTexCoord2f(1, 0);
		glVertex2i(int(size * (800.0f/600.0f) + x), size + y);

		glTexCoord2f(1, 1);
		glVertex2i(int(size * (800.0f/600.0f) + x), 0 + y);
	}
	glEnd();

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);

	glMatrixMode(GL_PROJECTION);
  	glPopMatrix();
  	glMatrixMode(GL_MODELVIEW);
  	glPopMatrix();
}

//--------------------------------------------------------------------------------------
// Rneder Target
//--------------------------------------------------------------------------------------
RenderTexture::RenderTexture() : depth(0), stencil(0), texture(0)
{

}

RenderTexture::~RenderTexture()
{
	SAFE_DELETE(texture);
	SAFE_DELETE(depth);
	SAFE_DELETE(stencil);
}

bool RenderTexture::Create(int width, int height, int flags)
{
	// http://opengl.org/documentation/extensions/EXT_framebuffer_object.txt
	// http://oss.sgi.com/projects/ogl-sample/registry/ARB/texture_float.txt

	this->width = width;
	this->height = height;

	SAFE_DELETE(texture);
	SAFE_DELETE(depth);
	SAFE_DELETE(stencil);

	Device::GetInstance().CheckErrors();

	glGenFramebuffersEXT(1, &fb);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb);

	// initialize texture
	if (flags & TF_CubeMap)
	{
		texture = new TextureCube();
		glGenTextures(1, &texture->texture);

		target = GL_TEXTURE_CUBE_MAP;
		texture->target = target;

		glEnable(target);
		glBindTexture(target, texture->texture);

		for (int i = 0; i < 6; ++i)
		{
			GLenum face = GL_TEXTURE_CUBE_MAP_POSITIVE_X + i;

			glTexImage2D(face, 0, GL_RGBA8, width, height, 0,
				GL_RGBA, GL_UNSIGNED_BYTE, NULL);

			glTexParameterf(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameterf(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterf(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
				GL_COLOR_ATTACHMENT0_EXT, face, texture->texture, 0);
		}
	}
	else if (flags & TF_Colour)
	{
		texture = new Texture();
		glGenTextures(1, &texture->texture);

		target = GL_TEXTURE_2D;
		texture->target = target;

		glEnable(target);
		glBindTexture(target, texture->texture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0,
			GL_RGBA, GL_UNSIGNED_BYTE, NULL);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
			GL_COLOR_ATTACHMENT0_EXT,
			GL_TEXTURE_2D, texture->texture, 0);


		/*
		texture = new Texture();
		glGenTextures(1, &texture->texture);

		target = GL_TEXTURE_2D; //GL_TEXTURE_RECTANGLE_NV; //GL_TEXTURE_2D;
		texture->target = GL_TEXTURE_RECTANGLE_NV;//target;

		glEnable(target);
		glBindTexture(target, texture->texture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glTexImage2D(GL_TEXTURE_RECTANGLE_NV, 0, GL_FLOAT_RGB32_NV, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
			GL_COLOR_ATTACHMENT0_EXT,
			GL_TEXTURE_RECTANGLE_NV, texture->texture, 0);*/
	}
	else
	{
		// No color buffer to draw to or read from
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
	}

	// initialize depth renderbuffer
	if (flags & TF_DepthTexture)
	{
		depth = new Texture();
		glGenTextures(1, &depth->texture);

		depth->target = GL_TEXTURE_2D;

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, depth->texture);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE_ARB, GL_LUMINANCE);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0,
			GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);

		Device::GetInstance().CheckErrors();

		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
			GL_DEPTH_ATTACHMENT_EXT,
			GL_TEXTURE_2D, depth->texture, 0);
	}
	else if (flags & TF_Depth)
	{
		depth = new Texture();
		glGenRenderbuffersEXT(1, &depth->texture);

		depth->target = GL_TEXTURE_2D;

		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depth->texture);

		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,
			GL_DEPTH_COMPONENT24, width, height);

		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,
			GL_DEPTH_ATTACHMENT_EXT,
			GL_RENDERBUFFER_EXT, depth->texture);
	}

	// initialize stencil renderbuffer
	if (flags & TF_Stencil)
	{
		stencil = new Texture();
		glGenRenderbuffersEXT(1, &stencil->texture);

		stencil->target = GL_TEXTURE_2D;

		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, stencil->texture);

		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,
			GL_STENCIL_INDEX, width, height);

		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,
			GL_STENCIL_ATTACHMENT_EXT,
			GL_RENDERBUFFER_EXT, stencil->texture);
	}

	Device::GetInstance().CheckFramebufferStatus();

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

    return true;
}

int RenderTexture::Lock()
{
	Texture::RestoreDefault();

	glEnable(target);
	glBindTexture(target, 0);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb);

	glPushAttrib(GL_VIEWPORT_BIT);
	
	glClearColor(clearColour[0], clearColour[1], clearColour[2], clearColour[3]);

	return 1; //(target == GL_TEXTURE_2D) ? 1 : 6;
}

void RenderTexture::BindTarget(int index, bool clear)
{
	if (target != GL_TEXTURE_2D)
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_CUBE_MAP_POSITIVE_X+index, texture->texture, 0);
	//else
	//	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_NV, texture->texture, 0);

	//Device::GetInstance().CheckFramebufferStatus();

	glViewport(0, 0, width, height);
	//glClearColor(1, 0, 0, 0);

	if (clear)
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RenderTexture::UnLock()
{
	glPopAttrib();

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	//Device::GetInstance().CheckFramebufferStatus();
}

void RenderTexture::SetClearColour(float r, float g, float b, float a)
{
	clearColour[0] = r;
	clearColour[1] = g;
	clearColour[2] = b;
	clearColour[3] = a;
}
/*
void RenderTexture::SetClearColour(float r, float g, float b, float a)
{
	int faces = Lock();
	for (int i = 0; i < faces; ++i)
	{
		BindTarget(i);
		glClearColor(r, g, b, a);
	}
	UnLock();
}
*/
//--------------------------------------------------------------------------------------
// TextureCube
//--------------------------------------------------------------------------------------

bool TextureCube::Load(char* name[])
{
	file = std::string(name[0]);
	for (int i = 1; i < 6; ++i)
		file += std::string("#") + std::string(name[i]);

	Log::Print("Loading cube texture: %s\n", file.c_str());

	glGenTextures(1, &texture);
	glEnable(GL_TEXTURE_CUBE_MAP);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);

	for (int i = 0; i < 6; ++i)
		LoadFace(i, name[i]);

	glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP);
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP);
	glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP);

	return true;
}

bool TextureCube::LoadFace(int face, const char* file)
{
	const char* extension = file + (strlen(file) - 3);

	if (stricmp(extension, "dds") == 0)
	{
		DDS dds;
		if (!dds.Load(file))
			return false;

		if (dds.GetBPP() == 3)
			GenerateTextureFace(face, (char*)dds.GetPixels(), dds.GetWidth(), dds.GetHeight(), GL_RGB);
		else if (dds.GetBPP() == 4)
			GenerateTextureFace(face, (char*)dds.GetPixels(), dds.GetWidth(), dds.GetHeight(), GL_RGBA);
	}
	if (stricmp(extension, "tga") == 0)
	{
		TGA tga;
		if (!tga.Load(file))
			return false;

		if (tga.GetBPP() == 3)
			GenerateTextureFace(face, (char*)tga.GetPixels(), tga.GetWidth(), tga.GetHeight(), GL_RGB);
		else if (tga.GetBPP() == 4)
			GenerateTextureFace(face, (char*)tga.GetPixels(), tga.GetWidth(), tga.GetHeight(), GL_RGBA);
	}
	else
	{
		HDR hdr;
		if(!hdr.Load(file))
		{
			Log::Print("Failed to load texture: %s\n", file);
			return false;
		}

		hdr.ConvertToFloat32();
		GenerateTextureFace(face, (char*)hdr.GetPixels(), hdr.GetWidth(), hdr.GetHeight(), GL_RGB, GL_FLOAT);
	}

	return true;
}

void TextureCube::GenerateTextureFace(int face, char* image, int width, int height, GLint format, GLenum type)
{
	GLenum target = GL_TEXTURE_CUBE_MAP_POSITIVE_X + face;

	GLint internalFormat = (type == GL_UNSIGNED_BYTE) ? format : GL_RGB16;
	glTexImage2D(target, 0, internalFormat, width, height, 0, format, type, image);
}

void TextureCube::Bind(int texUnit)
{
#ifdef DEBUG
	char info[256];
	sprintf(info, "TextureCube::Bind(%i) - %s", texUnit, file.c_str());
	GDBSTRING(info);
#endif

	glActiveTextureARB(GL_TEXTURE0_ARB + texUnit);

	glEnable(GL_TEXTURE_CUBE_MAP);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

	glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP); // GL_NORMAL_MAP GL_REFLECTION_MAP
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP); //  GL_NORMAL_MAP
	glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP); //  GL_NORMAL_MAP

	glEnable(GL_TEXTURE_GEN_S);
	glEnable(GL_TEXTURE_GEN_T);
	glEnable(GL_TEXTURE_GEN_R);
	glEnable(GL_NORMALIZE);
}

}
