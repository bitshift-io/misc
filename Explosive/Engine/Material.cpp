//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------

#include "Material.h"
#include "Log.h"
#include "Camera.h"
#include <string.h>

namespace Siphon
{

#ifndef WIN32
    #define stricmp strcasecmp
#endif

CGcontext Effect::context = NULL;

static void CgErrorCallback()
{
    Log::Print("%s\n", cgGetErrorString(cgGetError()));
}

Effect::Effect() :
	effect(0),
	light(false),
	validTechnique(-1),
	projector(false),
	pass(0),
	skinning(false)
{

}

Effect::~Effect()
{

}

bool Effect::Load(const char* name)
{
	this->name = std::string(name);
	Log::Print("Loading effect: %s\n", name);

	if (!context)
	{
		context = cgCreateContext();

		cgSetErrorCallback(CgErrorCallback);
		cgGLRegisterStates(context);
		
		cgGLSetOptimalOptions(CG_PROFILE_ARBVP1);
		cgGLSetOptimalOptions(CG_PROFILE_ARBFP1);
	}

	effect = cgCreateEffectFromFile(context, name, NULL);
    if (!effect)
	{
		const char* listing = cgGetLastListing(context);
		Log::Print("Call to cgCreateEffectFromFile failed\n%s", listing);
        return false;
    }

	// find a valid technique
	technique = cgGetFirstTechnique(effect);
    while (technique)
	{
        if (cgValidateTechnique(technique) == CG_FALSE)
			Log::Print("Technique %s did not validate.\n", cgGetTechniqueName(technique));

        technique = cgGetNextTechnique(technique);
    }

	technique = cgGetFirstTechnique(effect);
    while (technique && !cgIsTechniqueValidated(technique))
		technique = cgGetNextTechnique(technique);

    if (!technique)
	{
		Log::Print("No valid techniques found.\n");
        return false;
    }

	ProcessParameters();
    return true;
}

int Effect::DoPass()
{
	if (pass == 0)
	{
		pass = cgGetFirstPass(technique);
		curPass = 0;
	}
	else
	{
		pass = cgGetNextPass(pass);
		++curPass;

		if (pass == 0)
			curPass = -1;
	}

#ifdef DEBUG
	char info[256];
	sprintf(info, "Effect::DoPass %i - %s", curPass, GetName());
	GDBSTRING(info);
#endif

	return curPass;
}

void Effect::BindParameters()
{
#ifdef DEBUG
	GDBSTRING("[START] Effect::BindParameters");
#endif

	cgSetPassState(pass);

#ifdef DEBUG
	GDBSTRING("[END] Effect::BindParameters");
#endif
}

void Effect::ClearParameters()
{
#ifdef DEBUG
	GDBSTRING("[START] Effect::ClearParameters");
#endif

	cgResetPassState(pass);

#ifdef DEBUG
	GDBSTRING("[END] Effect::ClearParameters");
#endif
}

void Effect::ProcessParameters()
{
	CGparameter param = cgGetFirstEffectParameter(effect);
    while (param)
	{
		const char* semantic = cgGetParameterSemantic(param);
		const char* paramName = cgGetParameterName(param);

		// process the parameter
        if (semantic == NULL/* || strlen(semantic) <= 0*/)
		{
			param = cgGetNextParameter(param);
			continue;
		}

		if (stricmp(semantic, "World") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(WorldMatrix, param));
		else if (stricmp(semantic, "WorldInverseTranspose") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(WorldITMatrix, param));
		else if (stricmp(semantic, "WorldView") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(WorldViewMatrix, param));
		else if (stricmp(semantic, "WorldViewInverseTranspose") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(WorldViewITMatrix, param));
		else if (stricmp(semantic, "Projection") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectionMatrix, param));
		else if (stricmp(semantic, "WorldViewProjection") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(WorldViewProjectionMatrix, param));
		else if (stricmp(semantic, "WorldViewProjectionInverseTranspose") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(WorldViewProjectionITMatrix, param));
		else if (stricmp(semantic, "View") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(ViewMatrix, param));
		else if (stricmp(semantic, "ViewInverseTranspose") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(ViewITMatrix, param));
		else if (stricmp(semantic, "ViewInverse") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(ViewInvMatrix, param));
		else if (stricmp(semantic, "WorldViewInverse") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(WorldViewInvMatrix, param));
		else if (stricmp(semantic, "WorldInverse") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(WorldInvMatrix, param));

		// projectors
		else if (stricmp(semantic, "ProjectorProjection") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorProjectionMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorView") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorViewMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorViewProjection") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorViewProjectionMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorViewProjectionIT") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorViewProjectionITMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorViewIT") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorViewITMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorViewInv") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorViewInvMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorWorldViewProjection") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorWorldViewProjectionMatrix, param));
			projector = true;
		}

		else if (stricmp(semantic, "ProjectorProjectionTexture") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorProjectionTextureMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorViewTexture") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorViewTextureMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorViewProjectionTexture") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorViewProjectionTextureMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorViewProjectionITTexture") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorViewProjectionITTextureMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorViewITTexture") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorViewITTextureMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorViewInvTexture") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorViewInvTextureMatrix, param));
			projector = true;
		}
		else if (stricmp(semantic, "ProjectorWorldViewProjectionTexture") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(ProjectorWorldViewProjectionTextureMatrix, param));
			projector = true;
		}

		// bones
		// HACK till cg is fixed
		else if (stricmp(semantic, "Bones") == 0 || stricmp(paramName, "bones") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(BonesMatrix, param));
			skinning = true;
		}

		else if (stricmp(semantic, "EyePosition") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(EyePosition, param));
		else if (stricmp(semantic, "EyeDirection") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(EyeDirection, param));
		else if (stricmp(semantic, "LightPosition") == 0)
		{
			parameters.Insert(Pair<ParamID, CGparameter>(LightPosition, param));
			light = true;
		}
		else if (stricmp(semantic, "AmbientColour") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(AmbientColour, param));
		else if (stricmp(semantic, "DiffuseColour") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(DiffuseColour, param));
		else if (stricmp(semantic, "SpecularColour") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(SpecularColour, param));
		else if (stricmp(semantic, "SpecularPower") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(SpecularPower, param));
		else if (stricmp(semantic, "Time") == 0)
			parameters.Insert(Pair<ParamID, CGparameter>(Time, param));

		param = cgGetNextParameter(param);
	}
}

void Effect::SetLightParameters(Light* lights[], unsigned int numLights)
{
	unsigned int posCount = 0;

	for (unsigned int p = 0; p < parameters.GetSize(); ++p)
	{
		Pair<ParamID, CGparameter>& pair = parameters[p];

		switch (pair.first)
		{
		case LightPosition: // this should be in material->SetLights(Light* lights, numlights);?
			{
				if (posCount >= numLights)
					break;

				SetVector3(pair.second, lights[posCount]->GetPosition());
				++posCount;
			}
			break;
		default:
			break;
		}
	}
}

void Effect::SetTimeParameter(float t)
{
	for (unsigned int p = 0; p < parameters.GetSize(); ++p)
	{
		Pair<ParamID, CGparameter>& pair = parameters[p];
		if (pair.first == Time)
		{
			//Log::Print("time: %f\n", time);
			//t += 0.0001f; // just get it working!
			SetFloat(pair.second, t);
		}
/*
		switch (pair.first)
		{
		case Time:
			{
				Log::Print("time: %f\n", time);
				SetFloat(pair.second, time);
			}
			break;

		default:
			break;
		}*/
	}
}

void Effect::SetProjectorParameters(const Matrix4& model, Projector* projectors[], unsigned int numProjectors)
{
	int curProjection = 0;
	int curView = 0;
	int curViewProjection = 0;
	int curViewProjectionIT = 0;
	int curViewIT = 0;
	int curViewInv = 0;
	int curWorldViewProjection = 0;

	int curProjectionTexture = 0;
	int curViewTexture = 0;
	int curViewProjectionTexture = 0;
	int curViewProjectionITTexture = 0;
	int curViewITTexture = 0;
	int curViewInvTexture = 0;
	int curWorldViewProjectionTexture = 0;

	Matrix4 textureMat( 0.5f, 0.0f, 0.0f, 0.5f,
						0.0f, 0.5f, 0.0f, 0.5f,
						0.0f, 0.0f, 0.0f, 0.0f,
						0.0f, 0.0f, 0.0f, 1.0f);

	// will need some static offset matrix here

	for (unsigned int p = 0; p < parameters.GetSize(); ++p)
	{
		Pair<ParamID, CGparameter>& pair = parameters[p];

		switch (pair.first)
		{
		case ProjectorProjectionMatrix:
			{
				Camera* proj = projectors[curProjection]->GetCamera();
				++curProjection;
			}
			break;
		case ProjectorViewMatrix:
			{
				Camera* proj = projectors[curView]->GetCamera();
				++curView;
			}
			break;
		case ProjectorViewProjectionMatrix:
			{
				Camera* proj = projectors[curViewProjection]->GetCamera();
				++curViewProjection;

				Matrix4 viewProj = proj->GetTransform() * proj->GetProjection() * textureMat;
				SetMatrix4(pair.second, viewProj);
			}
			break;
		case ProjectorViewProjectionITMatrix:
			{
				Camera* proj = projectors[curViewProjectionIT]->GetCamera();
				++curViewProjectionIT;
			}
			break;
		case ProjectorViewITMatrix:
			{
				Camera* proj = projectors[curViewIT]->GetCamera();
				++curViewIT;
			}
			break;
		case ProjectorViewInvMatrix:
			{
				Camera* proj = projectors[curViewInv]->GetCamera();
				++curViewInv;
			}
			break;
		case ProjectorWorldViewProjectionMatrix:
			{
				Camera* proj = projectors[curWorldViewProjection]->GetCamera();
				++curWorldViewProjection;

				Matrix4 modelViewProj = proj->GetTransform() * proj->GetProjection();
				SetMatrix4(pair.second, modelViewProj);
			}
			break;

		case ProjectorProjectionTextureMatrix:
			{
				Camera* proj = projectors[curProjectionTexture]->GetCamera();
				++curProjectionTexture;
			}
			break;
		case ProjectorViewTextureMatrix:
			{
				Camera* proj = projectors[curViewTexture]->GetCamera();
				++curViewTexture;
			}
			break;
		case ProjectorViewProjectionTextureMatrix:
			{
				Camera* proj = projectors[curViewProjectionTexture]->GetCamera();
				++curViewProjectionTexture;

				Matrix4 viewProj = proj->GetTransform() * proj->GetProjection() * textureMat;
				SetMatrix4(pair.second, viewProj);
			}
			break;
		case ProjectorViewProjectionITTextureMatrix:
			{
				Camera* proj = projectors[curViewProjectionITTexture]->GetCamera();
				++curViewProjectionITTexture;
			}
			break;
		case ProjectorViewITTextureMatrix:
			{
				Camera* proj = projectors[curViewITTexture]->GetCamera();
				++curViewITTexture;
			}
			break;
		case ProjectorViewInvTextureMatrix:
			{
				Camera* proj = projectors[curViewInvTexture]->GetCamera();
				++curViewInvTexture;
			}
			break;
		case ProjectorWorldViewProjectionTextureMatrix:
			{
				Camera* proj = projectors[curWorldViewProjectionTexture]->GetCamera();
				++curWorldViewProjectionTexture;

				Matrix4 modelViewProj = proj->GetTransform() * proj->GetProjection() * textureMat;
				SetMatrix4(pair.second, modelViewProj);
			}
			break;
		default:
			break;
		}
	}
}

void Effect::SetSkinningParameters(const Matrix4 bones[], unsigned int numBones)
{
	for (unsigned int p = 0; p < parameters.GetSize(); ++p)
	{
		Pair<ParamID, CGparameter>& pair = parameters[p];

		switch (pair.first)
		{
		case BonesMatrix:
			{
				SetMatrix4Array(pair.second, bones, numBones);
			}
			break;
		}
	}
}

void Effect::SetMatrixParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& model, const Vector3& eyePosition)
{
#ifdef DEBUG
	GDBSTRING("[START] Effect::SetMatrixParameters");
#endif

	// http://www.gignews.com/realtime020100.htm
	for (unsigned int p = 0; p < parameters.GetSize(); ++p)
	{
		Pair<ParamID, CGparameter>& pair = parameters[p];

		switch (pair.first)
		{
		case EyePosition:
			{
				// can we extract this from the view matrix cheaply?
				SetVector3(pair.second, const_cast<Vector3&>(eyePosition));
			}
			break;
		case EyeDirection:
			{
				Vector3 eyeDir = -view.GetRow(2);
				SetVector3(pair.second, eyeDir);
			}
			break;
		case ViewMatrix:
			{
				SetMatrix4(pair.second, view);
			}
			break;
		case ViewITMatrix:
			{
				Matrix4 viewIT = view;
				viewIT.Inverse();
				viewIT.Transpose();
				SetMatrix4(pair.second, viewIT);
			}
			break;
		case ViewInvMatrix:
			{
				Matrix4 viewInv = view;
				viewInv.Inverse();
				SetMatrix4(pair.second, viewInv);
			}
			break;
		case WorldMatrix:
			{
				SetMatrix4(pair.second, model);
			}
			break;
		case WorldITMatrix:
			{
				Matrix4 worldIT = model;
				worldIT.Inverse();
				worldIT.Transpose();
				SetMatrix4(pair.second, worldIT);
			}
			break;
		case WorldViewMatrix:
			{
				Matrix4 worldView = model * view;
				SetMatrix4(pair.second, worldView);
			}
			break;
		case WorldInvMatrix:
			{
				Matrix4 world = model;
				world.Inverse();
				SetMatrix4(pair.second, world);
			}
			break;
		case WorldViewITMatrix:
			{
				Matrix4 worldView = model * view;
				worldView.Inverse();
				worldView.Transpose();
				SetMatrix4(pair.second, worldView);
			}
			break;
		case WorldViewInvMatrix:
			{
				Matrix4 worldViewInv = model * view;
				worldViewInv.Inverse();
				SetMatrix4(pair.second, worldViewInv);
			}
			break;
		case ProjectionMatrix:
			{
				SetMatrix4(pair.second, projection);
			}
			break;
		case WorldViewProjectionMatrix:
			{
				Matrix4 worldViewProj = model * view * projection;
				SetMatrix4(pair.second, worldViewProj);
			}
			break;
		case WorldViewProjectionITMatrix:
			{
				Matrix4 worldViewProj = model * view * projection;
				worldViewProj.Inverse();
				worldViewProj.Transpose();
				SetMatrix4(pair.second, worldViewProj);
			}
			break;
		default:
			break;
		}
	}

#ifdef DEBUG
	GDBSTRING("[END] Effect::SetMatrixParameters");
#endif
}

void Effect::SetMatrix4(CGparameter param, const Matrix4& matrix)
{
	// change c to r is this doenst work
	cgSetMatrixParameterfc(param, matrix.GetData());
}

void Effect::SetMatrix4Array(CGparameter param, const Matrix4* matrix, int count)
{/*
	for (int i = 0; i < count; ++i)
	{
		CGparameter p = cgGetArrayParameter(param, i);
		SetMatrix4(p, matrix[i]);
	}*/

	cgGLSetMatrixParameterArrayfc(param, 0, count, (const float*)matrix);
}

//******************************************

#define START_BRACE		"{"
#define END_BRACE		"}"
#define STR_PASS		"pass"
#define RENDERORDER		"renderorder"
#define TEXTURE			"texture"
#define EFFECT			"effect"
#define VERTEXFORMAT	"vertexformat"

Effect Material::nullEffect;

void Material::Disable()
{/*
	if (!nullEffect.IsValid())
		nullEffect.Load("null.fx");

	while (nullEffect.DoPass() != -1)
		nullEffect.ClearParameters();*/
}

Material::Material() : renderOrder(0), effect(0)
{

}

void Material::ClearParameters()
{
	GetEffect()->ClearParameters();
}

void Material::SetPassParameters(int passNum)
{
#ifdef DEBUG
	char info[256];
	sprintf(info, "[START] Effect::SetPassParameters %i", passNum);
	GDBSTRING(info);
#endif

	CPass& pass = passes[passNum];

	// bind textures
	int texCount = 0;
	for (unsigned int p = 0; p < pass.parameters.GetSize(); ++p)
	{
		Param* param = pass.parameters[p];

		switch (param->type)
		{
		case Param::ParamTexture:
			{
				TextureParam* texParam = static_cast<TextureParam*>(param);

				// REMOVE THIS WHEN DONE
				if (texParam->texture == NULL)
					continue;
				// END

				#ifdef DEBUG
					char info[256];
					sprintf(info, "Effect::SetTexture - %s", texParam->texture->GetName());
					GDBSTRING(info);
				#endif

				effect->SetTexture(texParam->param, texParam->texture);

				//texParam->texture->Bind(texCount);
				++texCount;
			}
			break;
		case Param::ParamVector4:
			{
				Vector4Param* vec4Param = static_cast<Vector4Param*>(param);
				effect->SetVector4(vec4Param->param, vec4Param->data);
			}
			break;
		case Param::ParamVector3:
			{
				Vector3Param* vec3Param = static_cast<Vector3Param*>(param);
				effect->SetVector3(vec3Param->param, vec3Param->data);
			}
			break;
		case Param::ParamFloat:
			{
				FloatParam* floatParam = static_cast<FloatParam*>(param);
				effect->SetFloat(floatParam->param, floatParam->data);
			}
			break;
		case Param::ParamFloat2:
			{
				Float2Param* float2Param = static_cast<Float2Param*>(param);
				effect->SetFloat2(float2Param->param, float2Param->data);
			}
			break;
		}
	}

#ifdef DEBUG
	sprintf(info, "[END] Effect::SetPassParameters %i", passNum);
	GDBSTRING(info);
#endif
}

bool Material::Load(const char* name)
{
	this->name = std::string(name);

	File file(name, "r");

	if (!file.Valid())
	{
		Log::Print("ERROR: Failed to load material %s\n", name);
		return false;
	}

	printf("Loading material: %s\n", name);

	char buffer[256];
	do
	{
		if (!ReadString(file, buffer))
			return true; // reached EOF

		if (strcmp(buffer, STR_PASS) == 0)
		{
			ReadString(file, buffer);
			if (strcmp(buffer, START_BRACE) != 0)
			{
				Log::Print("ERROR: expected %s after %s in material %s not found\n", START_BRACE, STR_PASS, name);
				return false;
			}

			if (ReadPass(file) == false)
				return false;
		}
		else if (strcmp(buffer, EFFECT) == 0)
		{
			ReadString(file, buffer);
			effect = EffectMgr::Load(buffer);

			if (!effect)
				return false;
		}
		else if (strcmp(buffer, RENDERORDER) == 0)
		{
			ReadInt(file, renderOrder);
		}
		else
		{
			Log::Print("ERROR: expected %s in material %s not found. Found '%s'.\n", STR_PASS, name, buffer);
			return false;
		}

		buffer[0] = '\0';
	} while (!file.EndOfFile());

	return true;
}

bool Material::ReadPass(File& file)
{
	char buffer[256];
	CPass pass;
	do
	{
		ReadString(file, buffer);

		if (strcmp(buffer, END_BRACE) == 0)
		{
			passes.Insert(pass);
			return true;
		}
		else if (strcmp(buffer, VERTEXFORMAT) == 0)
		{
			ReadVertexFormat(file, pass.format);
		}
		else
		{
			// what about texture matrices?
			CGparameter cgParam = effect->GetParameterByName(buffer);
			if (!cgParam)
			{
				Log::Print("WARNING: unexpected parameter %s in material %s\n", buffer, name.c_str());
				continue;
			}

			CGtype type = effect->GetParameterType(cgParam);

			Param* param = 0;
			switch (type)
			{
			case CG_SAMPLER2D:
			case CG_SAMPLERRECT:
			case CG_SAMPLERCUBE:
				{
					CGannotation resType = effect->GetAnnotationByName(cgParam, "ResourceType");

					const char* string = resType ? cgGetStringAnnotationValue(resType) : 0;

					param = new TextureParam(cgParam);
					TextureParam* texParam = static_cast<TextureParam*>(param);

					ReadString(file, buffer);

					if (stricmp(buffer, "<rendertarget>") == 0)
						break;

					if (string && stricmp("Cube", string) == 0) // cube map
					{
						char* cBuffer[6];
						char name[1024] = "\0";

						for (int i = 0; i < 6; ++i)
						{
							cBuffer[i] = new char[256];

							if (i == 0)
								strcpy(cBuffer[i], buffer);
							else
								ReadString(file, cBuffer[i]);

							strcat(name, cBuffer[i]);
						}

						texParam->texture = TextureMgr::LoadTexCube(cBuffer);

						for (int i = 0; i < 6; ++i)
							delete[] cBuffer[i];
					}
					else // normal texture
					{
						texParam->texture = TextureMgr::LoadTex(buffer);
					}

					if (!texParam->texture)
					{
						delete param;
						continue;
					}
				}
				break;
			case CG_FLOAT:
				{
					param = new FloatParam(cgParam);
					FloatParam* floatParam = static_cast<FloatParam*>(param);
					ReadFloat(file, floatParam->data);
				}
				break;
			case CG_FLOAT2:
				{
					param = new Float2Param(cgParam);
					Float2Param* float2Param = static_cast<Float2Param*>(param);
					ReadFloat2(file, float2Param->data);
				}
				break;
			case CG_FLOAT3:
				{
					param = new Vector3Param(cgParam);
					Vector3Param* vec3Param = static_cast<Vector3Param*>(param);
					ReadVector3(file, vec3Param->data);
				}
				break;
			case CG_FLOAT4:
				{
					param = new Vector4Param(cgParam);
					Vector4Param* vec4Param = static_cast<Vector4Param*>(param);
					ReadVector4(file, vec4Param->data);
				}
				break;
			}

			pass.parameters.Insert(param);
		}

		buffer[0] = '\0';
	} while (!file.EndOfFile());

	Log::Print("ERROR: expected %s in material %s not found\n", END_BRACE, name.c_str());
	return false;
}

bool Material::ReadVertexFormat(File& file, VertexFormat& value)
{
	char buffer[256];

	// read till we reach newline
	file.GetStr(buffer, 256);

	int iVal = 0;
	if (strstr(buffer, "position"))
		iVal |= (int)VertexPosition;
	if (strstr(buffer, "normal"))
		iVal |= (int)VertexNormal;
	if (strstr(buffer, "colour"))
		iVal |= (int)VertexColour;
	if (strstr(buffer, "binormal"))
		iVal |= (int)VertexBinormal;
	if (strstr(buffer, "tangent"))
		iVal |= (int)VertexTangent;
	if (strstr(buffer, "uvcoord1"))
		iVal |= (int)VertexUVCoord1;
	if (strstr(buffer, "uvcoord2"))
		iVal |= (int)VertexUVCoord2;
	if (strstr(buffer, "weights"))
		iVal |= (int)VertexWeights;
	if (strstr(buffer, "bones"))
		iVal |= (int)VertexBones;

	value = VertexFormat(iVal);
	return true;
}

bool Material::ReadString(File& file, char* value)
{
	value[0] = '\0';
	file.ReadString("%s", value);

	if (file.EndOfFile())
		return false;

	// we came across a comment
	if (value[0] == ';')
	{
		char discard[256];
		file.GetStr(discard, 256);

		return ReadString(file, value);
	}

	return true;
}

bool Material::ReadVector4(File& file, Vector4& value)
{
	file.ReadString("%f %f %f %f", &value[0], &value[1], &value[2], &value[3]);
	return true;
}

bool Material::ReadVector3(File& file, Vector3& value)
{
	file.ReadString("%f %f %f", &value[0], &value[1], &value[2]);
	return true;
}

bool Material::ReadFloat(File& file, float& value)
{
	file.ReadString("%f", &value);
	return true;
}

bool Material::ReadFloat2(File& file, float* value)
{
	file.ReadString("%f %f", &(value[0]), &(value[1]));
	return true;
}

bool Material::ReadInt(File& file, int& value)
{
	file.ReadString("%i", &value);
	return true;
}

Material::Param* Material::GetParameterByName(const char* name)
{
	CGparameter cgParam = effect->GetParameterByName(name);

	for (unsigned int p = 0; p < passes.GetSize(); ++p)
	{
		for (unsigned int i = 0; i < passes[p].parameters.GetSize(); ++i)
		{
			Param* param = passes[p].parameters[i];

			if (param->param == cgParam)
				return param;
		}
	}

	return 0;
}

}
