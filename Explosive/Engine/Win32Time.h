//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_TIME_
#define _SIPHON_TIME_

#ifdef WIN32
    #include <windows.h>
#else
   #include <time.h>
#endif

#include "Singleton.h"

namespace Siphon
{

#define gTime Time::GetInstance()

/**
 * A time class for the engine,
 * set the game, physcs etc update rate
 * here, and each update, query how mny times
 * it should be done
 */
class Time : public Siphon::Singleton<Time>
{
public:

	enum UpdateRate
	{
		UPDATE_GAME		= 0,
		UPDATE_PHYSICS	= 1,
	};

	Time();

	// Get Ticks returns time program has been running
	// in ms
	long GetTicks();

	float GetTimeSeconds();

	void SetUpdateRate(UpdateRate rateType, long time);
	long GetUpdateRate(UpdateRate rateType);

	// returns the number of times a loop should be called
	// to catch up the physics or game
	long GetNumUpdates(UpdateRate rateType);

	// needs to be called each frame
	void Update();

protected:

	long updateRate[2];
	long numUpdates[2];
	long numUpdatesThisFrame[2];
	long lastUpdateTime[2];

	static int numRates;

#ifdef WIN32
	LARGE_INTEGER timerFrequency;
	LARGE_INTEGER startTime;
#else
    timespec startTime;
#endif
};

}
#endif
