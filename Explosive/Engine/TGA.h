#ifndef _SIPHON_TGA_
#define _SIPHON_TGA_

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "TextureFile.h"

namespace Siphon 
{

class TGA : public TextureFile
{
public:

	TGA();
	~TGA();
	bool Load(const char* file);

	virtual unsigned char* GetPixels();
	virtual int GetWidth();
	virtual int GetHeight();

	//bytes per pixel
	virtual int GetBPP();

	bool ConvertToGreyScale();

	bool Save(const char* file, const void* data, int width, int height, int bpp);

protected:

	#pragma pack(push,1)
	struct TGAHead
    {
		char identsize;
		char colorType;
		char imageType; //compressed if 10, uncom if 2
		short mapstart;
		short length;
		char mapBits;
		short xstart;
		short ystart;
		short width;
		short height;
		char bpp;

		  /* Image descriptor.
			 3-0: attribute bpp
			 4:   left-to-right ordering
			 5:   top-to-bottom ordering
			 7-6: zero
			*/
		char descritor;
    };
	#pragma pack(pop)

	TGAHead header;
	char* image;

	bool LoadUncompresed( FILE* pFile, TGAHead& header );
	bool LoadCompresed( FILE* pFile, TGAHead& header );
};

}

#endif
