//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "Animation.h"

namespace Siphon
{

void AnimationStream::GetRotation(Quaternion& rotation, unsigned int time, int idx) const
{
	if (rotations.GetSize() == 0)
	{
		rotation = Quaternion(Vector3(0,0,0),0);
		return;
	}

	if (rotations.GetSize() == 1)
	{
		rotation = rotations[0].rotation;
		return;
	}

	// wrap time around
	int maxTime = rotations[rotations.GetSize() - 1].time;
	time = time % maxTime;

	for (unsigned int i = idx; i < rotations.GetSize(); ++i)
	{
		RotationKey& current = rotations[i];
		if (current.time == time)
		{
			rotation = current.rotation;
			break;
		}
		else if (current.time > time)
		{
			RotationKey& previous = rotations[i - 1];

			// we now have 2 keys, we need to interpolate between them according to time difference
			int timeDiff = current.time - previous.time;
			float weight = float(time - previous.time) / float(timeDiff);

			Quaternion interp = previous.rotation;
			rotation = interp.Interpolate(current.rotation, weight);
			rotation.Normalize();
			break;
		}
	}
}

void AnimationStream::GetPosition(Vector3& position, unsigned int time, int idx) const
{
	if (positions.GetSize() == 0)
	{
		position = Vector3(0,0,0);
		return;
	}

	if (positions.GetSize() == 1)
	{
		position = positions[0].position;
		return;
	}

	// wrap time around
	int maxTime = positions[positions.GetSize() - 1].time;
	time = time % maxTime;

	for (unsigned int i = 0; i < positions.GetSize(); ++i)
	{
		PositionKey& current = positions[i];
		if (current.time == time)
		{
			position = current.position;
			break;
		}
		else if (current.time > time)
		{
			PositionKey& previous = positions[i - 1];

			// we now have 2 keys, we need to interpolate between them according to time difference
			int timeDiff = current.time - previous.time;
			float weight = float(time - previous.time) / float(timeDiff);

			Vector3 interp = previous.position;
			position = (interp * (1.0f - weight)) + (current.position * weight);
			break;
		}
	}
}

int AnimationStream::GetTransform(Matrix4& matrix, unsigned int time, int idx) const
{
	// this hsould use GetRotation and GetPosition and make a matrix from it
	/*
	if (rotations.GetSize() == 0)
	{
		matrix.LoadIdentity();
		return 0;
	}

	// what we need to do is find 2 keyframes, with time in the middle
	// maybe we should save a pointer to the last location for optimisation,
	// or at least return it so it can be passed in later so we can share an animation
	unsigned int i;

	// wrap time around
	int maxTime = rotations[rotations.GetSize() - 1].time;

	if (maxTime != 0)
		time = time % maxTime;
	else
		time = 0;

	if (rotations.GetSize() == 1)
	{
		matrix = rotations[0].rotation.GetRotationMatrix();
	}
	else
	{
		for (i = idx; i < rotations.GetSize(); ++i)
		{
			RotationKey& current = rotations[i];
			if (current.time == time)
			{
				matrix = current.rotation.GetRotationMatrix();
				break;
			}
			else if (current.time > time)
			{
				RotationKey& previous = rotations[i - 1];

				// we now have 2 keys, we need to interpolate between them according to time difference
				int timeDiff = current.time - previous.time;
				float weight = float(time - previous.time) / float(timeDiff);

				Quaternion interp = previous.rotation;
				interp = interp.Interpolate(current.rotation, weight);
				interp.Normalize();

				matrix = interp.GetRotationMatrix();
				break;
			}
		}
	}

	if (positions.GetSize() == 1)
	{
		matrix.SetTranslation(positions[0].position);
	}
	else
	{
		for (i = 0; i < positions.GetSize(); ++i)
		{
			PositionKey& current = positions[i];
			if (current.time == time)
			{
				matrix.SetTranslation(current.position);
				break;
			}
			else if (current.time > time)
			{
				PositionKey& previous = positions[i - 1];

				// we now have 2 keys, we need to interpolate between them according to time difference
				int timeDiff = current.time - previous.time;
				float weight = float(time - previous.time) / float(timeDiff);

				Vector3 interp = previous.position;
				interp = (interp * (1.0f - weight)) + (current.position * weight);

				matrix.SetTranslation(interp);
				break;
			}
		}
	}
*/
	return 0;//i;
}

bool AnimationStream::Save(File& file)
{
	rotations.Save(file);
	positions.Save(file);
	return true;
}

bool AnimationStream::Load(File& file)
{
	rotations.Load(file);
	positions.Load(file);
	return true;
}

//--------------------------------------------------------------------------------------

bool Animation::Save(const char* file)
{
	File out(file, "wb");

	int size = streams.GetSize();
	out.Write(&size, sizeof(int));

	for (int i = 0; i < size; ++i)
	{
		streams[i].Save(out);
	}

	return true;
}

bool Animation::Load(const char* file)
{
	File in(file, "rb");
	name = file;

	if (!in.Valid())
		return false;

	int size;
	in.Read(&size, sizeof(int));

	streams.SetCapacity(size);
	for (int i = 0; i < size; ++i)
	{
		AnimationStream a;
		a.Load(in);
		streams.Insert(a);
	}

	return true;
}

unsigned int Animation::GetLength() const
{
	// root bone only has 1 key at the moment
	if (streams.GetSize() <= 1)
		return 0;

	const AnimationStream& stream = GetStream(1);
	if (stream.rotations.GetSize())
		return stream.rotations[stream.rotations.GetSize() - 1].time;

	if (stream.positions.GetSize())
		return stream.positions[stream.positions.GetSize() - 1].time;

	return 0;
}

const AnimationStream& Animation::GetStream(int idx) const
{
	return streams[idx];
}

Array<AnimationStream>& Animation::GetStreams()
{
	return streams;
}

//--------------------------------------------------------------------------------------

void Bone::operator=(const Bone& rhs)
{
	bounds = rhs.bounds;
	parentIdx = rhs.parentIdx;
	offset = rhs.offset;
	defaultPose = rhs.defaultPose;
	skinToBone = rhs.skinToBone;
	SetName(rhs.name);
}

void Bone::Draw()
{
	Matrix4 ident(Matrix4::Identity);
	bounds.SetTransform(ident); //ident); //defaultPose);
	bounds.Draw(Vector3(0.0f, 1.0f, 0.0f));
}

void Bone::Draw(Matrix4& bone)
{
	bounds.SetTransform(bone);
	bounds.Draw();
}

bool Bone::Save(File& file)
{
	// write out bone name
	int len = strlen(name) + 1;
	file.Write(&len, sizeof(int));
	file.Write(name, len);

	file.Write(&parentIdx, sizeof(int));
	file.Write(&offset, sizeof(Vector3));
	file.Write(&bounds, sizeof(Box));
	file.Write(&defaultPose, sizeof(Matrix4));
	file.Write(&skinToBone, sizeof(Matrix4));

	return true;
}

bool Bone::Load(File& file)
{
	// load bone name
	int len = 0;
	file.Read(&len, sizeof(int));
	char buffer[256];
	file.Read(buffer, len);
	SetName(buffer);

	file.Read(&parentIdx, sizeof(int));
	file.Read(&offset, sizeof(Vector3));
	file.Read(&bounds, sizeof(Box));
	file.Read(&defaultPose, sizeof(Matrix4));
	file.Read(&skinToBone, sizeof(Matrix4));

	return true;
}

//--------------------------------------------------------------------------------------
Skeleton::Skeleton()
{
}

bool Skeleton::Save(const char* file)
{
	File out(file, "wb");

	int size = bones.GetSize();
	out.Write(&size, sizeof(int));

	for (int i = 0; i < size; ++i)
	{
		bones[i].Save(out);
	}

	return true;
}

bool Skeleton::Load(const char* file)
{
	File in(file, "rb");

	if (!in.Valid())
		return false;

	int size;
	in.Read(&size, sizeof(int));

	bones.SetCapacity(size);
	for (int i = 0; i < size; ++i)
	{
		Bone b;
		b.Load(in);
		bones.Insert(b);
	}

	return true;
}

void Skeleton::Draw(const Matrix4& transform, Matrix4* matrix)
{
	Box joint;
	joint.SetMin(Vector3(-0.1f,-0.1f,-0.1f));
	joint.SetMax(Vector3(0.1f,0.1f,0.1f));

	Box box;
	box.SetMin(Vector3(0.0f,-0.005f,-0.005f));
	box.SetMax(Vector3(0.10f,0.005f,0.005f));

	for (unsigned int i = 0; i < bones.GetSize(); ++i)
	{/*
		box.SetTransform(matrix[i]);
		box.Draw();
/*
		// draw bones default pose
		Matrix4 test = bones[i].defaultPose;
		test.Inverse();
		box.SetTransform(test);
		box.Draw(Vector3(0.0, 1.0, 0));
*/
		Matrix4 final = matrix[i] * transform;
		joint.SetTransform(final);
		joint.Draw(Vector3(1.0, 0, 0));
	}
}

const Bone& Skeleton::GetBone(unsigned int idx)
{
	return bones[idx];
}

//--------------------------------------------------------------------------------------

SkeletonAnimation::SkeletonAnimation() : matrices(0)
{

}

SkeletonAnimation::~SkeletonAnimation()
{
	SAFE_DELETE(matrices);
}

void SkeletonAnimation::Draw(const Matrix4& transform)
{
	if (!skeleton)
		return;

	skeleton->Draw(transform, matrices);
}

void SkeletonAnimation::SetSkeleton(Skeleton* skeleton)
{
	this->skeleton = skeleton;
	SAFE_DELETE(matrices);
	matrices = new Matrix4[GetMatrixCount()];

	for (int i = 0; i < GetMatrixCount(); ++i)
		matrices[i].LoadIdentity();
}

Skeleton* SkeletonAnimation::GetSkeleton()
{
	return skeleton;
}

Matrix4* SkeletonAnimation::GetMatrices()
{
	return matrices;
}

int SkeletonAnimation::GetMatrixCount()
{
	return skeleton ? skeleton->GetBoneCount() : 0;
}

int SkeletonAnimation::AddAnimation(Animation* animation, float weight, int time)
{
	AnimationInst animInst;
	animInst.animation = animation;
	animInst.weight = weight;
	animInst.time = time;
	return animations.Insert(animInst);
}

SkeletonAnimation::AnimationInst& SkeletonAnimation::GetAnimationInst(int idx)
{
	return animations[idx];
}

void SkeletonAnimation::RemoveAnimation(Animation* animation)
{
	for (unsigned int i = 0; i < animations.GetSize(); ++i)
	{
		if (animations[i].animation == animation)
		{
			animations.Remove(i);
			break;
		}
	}
}

void SkeletonAnimation::Evaluate(int deltaTime)
{
	if (!skeleton)
		return;

	Quaternion* boneSpaceRotations = new Quaternion[skeleton->GetBoneCount()];
	Vector3*	boneSpacePositions = new Vector3[skeleton->GetBoneCount()];

	// for each bone ....
	for (unsigned int i = 0; i < skeleton->GetBoneCount(); ++i)
	{
		// for each animation ...
		Quaternion	rotation;
		Vector3		position;

		for (unsigned int a = 0; a < animations.GetSize(); ++a)
		{
			const AnimationStream& stream = animations[a].animation->GetStream(i);
			animations[a].time += deltaTime;
			int time = (int)animations[a].time;
			float weight = animations[0].weight;

			Quaternion	streamRotation;
			Vector3		streamPosition;

			stream.GetRotation(streamRotation, time);
			stream.GetPosition(streamPosition, time);

			if (a == 0)
			{
				rotation = streamRotation;
				position = streamPosition;
			}
			else
			{
				rotation = streamRotation.Interpolate(rotation, weight);
				position = streamPosition * (1.0f - weight) + position * weight;
			}
		}

		boneSpaceRotations[i] = rotation;
		boneSpacePositions[i] = position;
	}

	// now make all bones from object space to local (mesh) space
	for (unsigned int i = 0; i < skeleton->GetBoneCount(); ++i)
	{
		const Bone& bone = skeleton->GetBone(i);
		int parent = bone.parentIdx;

		matrices[i].LoadIdentity();
		matrices[i] = boneSpaceRotations[i].GetRotationMatrix();
		matrices[i].SetTranslation(boneSpacePositions[i]);

		if (parent != i)
			matrices[i] = matrices[i] * matrices[parent];
	}

	SAFE_DELETE_ARRAY(boneSpaceRotations);
	SAFE_DELETE_ARRAY(boneSpacePositions);
}

}
