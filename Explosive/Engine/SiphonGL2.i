/* File : mesh.i */
%module SiphonGL2

%{
#include "../util/singleton.h"
#include "../util/resource.h"
#include "../util/macros.h"
#include "../util/array.h"
#include "../util/list.h"
#include "../util/file.h"
#include "../util/INI.h"
#include "../util/log.h"

#include "math3d.h"
#include "camera.h"
#include "materialdefs.h"
#include "animation.h"
#include "mesh.h"
#include "material.h"
#include "dds.h"
#include "hdr.h"
#include "texture.h"
#include "font.h"
#include "posteffect.h"
#include "projector.h"
#include "light.h"
#include "time.h"
#include "device.h"
#include "renderer.h"
#include "input.h"
#include "window.h"
%}


/* Let's just grab the original header file here */
%include "../util/singleton.h"
%include "../util/resource.h"
%include "../util/macros.h"
%include "../util/array.h"
%include "../util/list.h"
%include "../util/file.h"
%include "../util/INI.h"
%include "../util/log.h"


/* Now instantiate some specific template declarations */
namespace Siphon
{
	%template(TimeSingleton)		Singleton<Siphon::Time >;
	%template(InputSingleton)		Singleton<Siphon::Input >;
	%template(DeviceSingleton)		Singleton<Siphon::Device >;
	%template(EffectResourceMgr)	ResourceMgr<Siphon::Effect >;
	%template(MaterialResourceMgr)	ResourceMgr<Siphon::Material >;
	%template(SkeletonResourceMgr)	ResourceMgr<Siphon::Skeleton >;
	%template(AnimationResourceMgr)	ResourceMgr<Siphon::Animation >;
}

%include "math3d.h"
%include "camera.h"
%include "materialdefs.h"
%include "animation.h"
%include "mesh.h"
%include "material.h"
%include "dds.h"
%include "hdr.h"
%include "texture.h"
%include "font.h"
%include "posteffect.h"
%include "projector.h"
%include "light.h"
%include "time.h"
%include "device.h"
%include "renderer.h"
%include "input.h"
%include "window.h"

