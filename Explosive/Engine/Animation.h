//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_ANIMATION_
#define _SIPHON_ANIMATION_

#include "Math3D.h"
#include "../Util/Macros.h"
#include "../Util/File.h"
#include "../Util/Array.h"

#include "../Util/Resource.h"

namespace Siphon
{

/**
 * Loading saving of animations, playing
 */
class AnimationStream
{
public:

	struct RotationKey
	{
		Quaternion rotation;
		unsigned int time;
	};

	struct PositionKey
	{
		Vector3 position;
		unsigned int time;
	};

	// this should take and return a pair of ints
	int GetTransform(Matrix4& matrix, unsigned int time, int idx = 0) const;

	void GetRotation(Quaternion& rotation, unsigned int time, int idx = 0) const;
	void GetPosition(Vector3& position, unsigned int time, int idx = 0) const;

	bool Save(File& file);
	bool Load(File& file);

//protected:
	Array<RotationKey> rotations;
	Array<PositionKey> positions;
};

/*
 * Okay, the problem:
 * loading animations on objects requires each object have a pointer to an
 * anim, but we want to share/only load an animation once.
 * so an AnimationGroup simply represents an array of animations
 * anm files are just an array of Animations
 * this way, when we change animation we supply a AnimationGroup
 * and use the aray indexes to assign to objects.
 */
class Animation : public Resource
{
public:
	//void SetNumberAnimations(unsigned int numAnims);
	//unsigned int GetNumAnimations();

	unsigned int GetLength() const;

	bool Save(const char* file);
	virtual bool Load(const char* file);

	const AnimationStream& GetStream(int idx) const;
	Array<AnimationStream>& GetStreams();

protected:
	Array<AnimationStream> streams;
	std::string file;
};

ResourceManager(AnimationMgr, Animation)

class Bone
{
public:
	Bone() : name(0) { }
	~Bone() { /*SAFE_DELETE(name);*/ }

	void operator=(const Bone& rhs);

	bool Save(File& file);
	bool Load(File& file);

	void SetName(const char* name) { this->name = strdup(name); }

	void Draw();
	void Draw(Matrix4& bone);
//protected:
	int			parentIdx;
	Vector4		offset;
	Box			bounds;
	Matrix4		defaultPose;
	Matrix4		skinToBone;
	const char* name;
};

class Skeleton : public Resource
{
public:
	Skeleton();

	bool Save(const char* file);
	virtual bool Load(const char* file);

	inline unsigned int GetBoneCount();
	const Bone& GetBone(unsigned int idx);

	// draw using the specified matrices
	void Draw(const Matrix4& transform, Matrix4* matrix);

protected:
	Array<Bone> bones;
};

inline unsigned int Skeleton::GetBoneCount()
{
	return bones.GetSize();
}

ResourceManager(SkeletonMgr, Skeleton)

/**
 * This class holds a pointer to a skeleton and animation(s)
 * and is responsible for generating the matrices
 */
class SkeletonAnimation
{
public:

	class AnimationInst
	{
	public:
		Animation*	animation;
		float		weight;
		float		time;
	};

	SkeletonAnimation();
	~SkeletonAnimation();

	void		SetSkeleton(Skeleton* skeleton);
	Skeleton*	GetSkeleton();

	Matrix4*	GetMatrices();
	int			GetMatrixCount();

	int			AddAnimation(Animation* animation, float weight = 1.0f, int time = 0);
	void		RemoveAnimation(Animation* animation);

	AnimationInst& GetAnimationInst(int idx);

	void		Evaluate(int deltaTime);

	void		Draw(const Matrix4& transform);
protected:
	Matrix4* matrices;
	Skeleton* skeleton;
	Array<AnimationInst> animations;
};

}
#endif
