//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "Win32Time.h"

namespace Siphon
{

int Time::numRates = 2;

//--------------------------------------------------------------------------------------

Time::Time()
{
	SetThreadAffinityMask(GetCurrentThread(), 1);

	for (int i = 0; i < numRates; ++i)
	{
		updateRate[i] = 30;
		lastUpdateTime[i] = 0;
		numUpdates[i] = 0;
		numUpdatesThisFrame[i] = 0;
	}

#ifdef WIN32
	QueryPerformanceFrequency(&timerFrequency);
	QueryPerformanceCounter(&startTime);
#else
    clock_gettime(CLOCK_REALTIME, &startTime);
#endif
}

//--------------------------------------------------------------------------------------

long Time::GetTicks()
{
#ifdef WIN32
	LARGE_INTEGER currentTicks;

	QueryPerformanceCounter(&currentTicks);

	currentTicks.QuadPart -= startTime.QuadPart;
	currentTicks.QuadPart *= 1000;
	currentTicks.QuadPart /= timerFrequency.QuadPart;

	return (DWORD)currentTicks.QuadPart;
#else
    struct timespec tp;
    clock_gettime(CLOCK_REALTIME, &tp);

    long seconds = tp.tv_sec - startTime.tv_sec;
    long milliseconds = (tp.tv_nsec / 1000000l) + 1000 - (startTime.tv_nsec / 1000000l);

    long appMilliseconds = seconds * 1000l + milliseconds;
    return appMilliseconds;
#endif
}

float Time::GetTimeSeconds()
{
	return float(GetTicks()) / 1000.0f;
}

//--------------------------------------------------------------------------------------

void Time::SetUpdateRate(UpdateRate rateType, long time)
{
	updateRate[rateType] = time;
}

//--------------------------------------------------------------------------------------

long Time::GetUpdateRate(UpdateRate rateType)
{
	return updateRate[rateType];
}

//--------------------------------------------------------------------------------------

long Time::GetNumUpdates(UpdateRate rateType)
{
	return numUpdatesThisFrame[rateType];
}

//--------------------------------------------------------------------------------------

void Time::Update()
{
	long deltaTime = GetTicks();
	for (int i = 0; i < numRates; ++i)
	{
		long lastUpdates = numUpdates[i];
		numUpdates[i] = long( float(deltaTime) / (1000.0f / float(updateRate[i]) ) );
		numUpdatesThisFrame[i] = numUpdates[i] - lastUpdates;
	}
}

}
