#include "LinuxInput.h"
#include "LinuxWindow.h"
#include "../Util/Log.h"

namespace Siphon
{

// must be the same order as the enum
static int inputLookup[] =
{
    XK_KP_Space,

    // key pad
    XK_KP_0,
    XK_KP_1,
    XK_KP_2,
    XK_KP_3,
    XK_KP_4,
    XK_KP_5,
    XK_KP_6,
    XK_KP_7,
    XK_KP_8,
    XK_KP_9,

    XK_0,
    XK_1,
    XK_2,
    XK_3,
    XK_4,
    XK_5,
    XK_6,
    XK_7,
    XK_8,
    XK_9,

    XK_a,
    XK_b,
    XK_c,
    XK_d,
    XK_e,
    XK_f,
    XK_g,
    XK_h,
    XK_i,
    XK_j,
    XK_k,
    XK_l,
    XK_m,
    XK_n,
    XK_o,
    XK_p,
    XK_q,
    XK_r,
    XK_s,
    XK_t,
    XK_u,
    XK_v,
    XK_w,
    XK_x,
    XK_y,
    XK_z,

    XK_F1,
    XK_F2,
    XK_F3,
    XK_F4,
    XK_F5,
    XK_F6,
    XK_F7,
    XK_F8,
    XK_F9,
    XK_F10,
    XK_F11,
    XK_F12,

    XK_Escape,
    XK_Return,
    XK_BackSpace,
    XK_Tab,
    XK_Pause,
    XK_Scroll_Lock,
    XK_Sys_Req,
    XK_Delete,

    XK_Shift_L,
    XK_Shift_R,
    XK_Control_L,
    XK_Control_R,
    XK_Caps_Lock,
    XK_Alt_L,
    XK_Alt_R,

    XK_Home,
    XK_Left,
    XK_Up,
    XK_Down,
    XK_Right,
    XK_Page_Up,
    XK_Page_Down,
    XK_End,
    XK_Begin,
};

static int secondaryLookup[256];

bool Input::CreateInput(Window* window, bool background)
{
    this->window = window;
    window->input = this;

    // create the secondary lookup table
    for (int i = 0; i < 256; ++i)
        secondaryLookup[i] = XKeycodeToKeysym(window->dpy, i, 0);

    return true;
}

void Input::Update()
{

}

bool Input::KeyDown(int key)
{
    return keys[key];
}

bool Input::KeyPressed(int key)
{
	if (KeyDown(key) && keyLock[key] == false)
	{
		keyLock[key] = true;
		return true;
	}
	if (!KeyDown(key))
	{
		keyLock[key] = false;
	}

	return false;
}

void Input::NotifyKeyState(int key, bool state)
{
    keys[secondaryLookup[key]] = state;

    //if (XK_Escape == XKeycodeToKeysym(window->dpy, key, 0))
    //    Log::Print("escape pressed!\n");

    //keys[key] = state;
}


}
