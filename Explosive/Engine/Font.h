//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_FONT_
#define _SIPHON_FONT_

#include "../Util/Resource.h"
#include "Texture.h"
#include <list>

namespace Siphon
{

class Font : public Resource
{
public:

	virtual bool Load(const char* name);

	bool Print(float x, float y, float scale, const char* text, ...);
	bool RenderText(float x, float y, float scale, const char* text, ...);

	int GetLineHeight() { return lineHeight; }

	void Render();
	void Update();

protected:

	struct Text
	{
		char text[256];
		float x, y;
		float scale;

		Text() { }
		Text(const char* text, float x, float y, float scale) : x(x), y(y), scale(scale)
			{ strcpy(this->text, text);	}
	};

	struct CharDesc
	{
		unsigned short x;
		unsigned short y;

		unsigned short width;
		unsigned short height;

		unsigned short xOffset;
		unsigned short yOffset;

		unsigned short xAdvance;

		GLuint renderList;

		CharDesc() : width(0), height(0), xOffset(0), yOffset(0), xAdvance(0) { }
	};

	void RenderBegin();
	void RenderEnd();

	bool RenderTextInternal(float x, float y, float scale, const char* text);
	void GenerateRenderList(int index);

	Texture*	texture;
	Array<Text> text;
	CharDesc	characters[256];

	int			lineHeight;
	int			texWidth;
	int			texHeight;

	// back ups
	GLint blendSrc;
	GLint blendDst;
	GLint blendOn;
};

ResourceManager(FontMgr, Font);

}
#endif

