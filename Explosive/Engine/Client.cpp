#include "../Util/Log.h"
#include "Client.h"
#include "../Network/Packet.h"

namespace Siphon
{

ClientBase::ClientBase() :
	reliable(SocketStream::Reliable),
	unreliable(SocketStream::UnReliable),
	broadcast(SocketStream::UnReliable)
{
	reliableHandle = stream.CreateChannel(CT_ReliableOrdered);
	unreliableHandle = stream.CreateChannel(CT_UnreliableOrdered);
}

bool ClientBase::Init(unsigned int gameId)
{
	bool ret = NetworkBase::Init();

	broadcast.Connect(NULL, 6455, 6454);
	this->gameId = gameId;
	return ret;
}

void ClientBase::Shutdown()
{
	reliable.Close();
	broadcast.Close();
}

int	ClientBase::FindLANServers(Array<ServerInfo>& servers)
{
	BroadcastMsg msg;
	msg.id = gameId;
	msg.addr = 0;
	msg.name[0] = '\0';

	for (int i = 0; i < 10; ++i)
	{
		broadcast.Send(&msg, sizeof(BroadcastMsg));
		//int bufferSize = broadcast.Receive(&msg, sizeof(BroadcastMsg));

		Sleep(200); // sleep a few hundred ms

		Array<ServerInfo> recvList(10);
		broadcast.Update(&recvList);

		for (int j = 0; j < recvList.GetSize(); ++j)
		{
			// convert to a string
			//std::string server;
			//broadcast.ConvertToString(recvList[j].addr, server);

			// make sure its not already in the list
			int s = 0;
			for (; s < servers.GetSize(); ++s)
			{
				if (strcmp(servers[s].ip.c_str(), recvList[j].ip.c_str()) == 0)
					break;
			}
			if (s >= servers.GetSize())
				servers.Insert(recvList[j]);
		}
	}

	return servers.GetSize();
}

bool ClientBase::Connect(const char* addr, int port, int localPort, int flags)
{
	this->addr = addr;
	this->port = port;
	this->flags = flags;

	if (!stream.Bind(gNetwork.GetAnyAddress(localPort)))
	{
		int lastErr = gNetwork.GetLastError();
		return false;
	}

	bool ret = stream.Connect(gNetwork.GetAddress(addr, port));

	//bool ret = reliable.Connect(addr, port);
	if (ret == false)
		return false;

	if ((flags & NF_ConnectExternal) == 0)
	{/*
		while (!(reliable.GetSocketState() & SocketStream::Read))
		{
		}*/

		StreamState state;
		while ((state = stream.GetState(reliableHandle)) == eNone)
		{
		}

		AcceptConnection(flags);
	}
	return true;
}

void ClientBase::AcceptConnection(int flags)
{
	Receive((char*)&clientId, sizeof(unsigned int));

	// connect to cerver on port + client id
	//if ((flags & NF_ConnectExternal) == 0 && (flags & NF_ReliableOnly) == 0)
	//	unreliable.Connect(addr.c_str(), port + clientId + 1, port + clientId);
}

void ClientBase::Disconnect()
{

}

bool ClientBase::StreamValid(SocketStream::Type type)
{
	switch (type)
	{
	case SocketStream::Reliable:
		return reliable.IsValid();
	case SocketStream::UnReliable:
		return unreliable.IsValid();
	}

	return false;
}

bool ClientBase::Send(const void* data, int size, SocketStream::Type type)
{
	//switch (type)
	{
	//case SocketStream::Reliable:
		{
			Packet p;
			p.Append(data, size);
			return stream.Send(&p, type == SocketStream::Reliable ? reliableHandle : unreliableHandle);
			//return reliable.Send(data, size);
		}
	//case SocketStream::UnReliable:
	//	return unreliable.Send(data, size);
	}

	return false;
}

int ClientBase::Receive(void* data, unsigned int size, SocketStream::Type type)
{
	//switch (type)
	{
	//case SocketStream::Reliable:
		{
			Packet p;
			stream.Receive(&p, -1, type == SocketStream::Reliable ? reliableHandle : unreliableHandle);
			int actualSize = min(size, p.GetSize());
			memcpy(data, p.GetBuffer(), actualSize);
			return actualSize;

			//return reliable.Receive(data, size);
		}
	//case SocketStream::UnReliable:
	//	return unreliable.Receive(data, size);
	}

	return false;
}

int ClientBase::GetBufferData(void* data, int size, SocketStream::Type type)
{
	switch (type)
	{
	case SocketStream::Reliable:
		return reliable.GetBufferData(data, size);
	case SocketStream::UnReliable:
		return unreliable.GetBufferData(data, size);
	}

	return 0;
}

int ClientBase::PeekBufferData(void* data, int size, SocketStream::Type type)
{
	switch (type)
	{
	case SocketStream::Reliable:
		return reliable.PeekBufferData(data, size);
	case SocketStream::UnReliable:
		return unreliable.PeekBufferData(data, size);
	}

	return 0;
}

void ClientBase::Update()
{
	//if (!reliable.IsValid())
	//	return;

	CheckBroadcast();
/*
	if (unreliable.Update() == SOCKET_ERROR)
		Log::Print("Failed at unreliable.Update: %ld\n", WSAGetLastError());

	if (reliable.Update() == SOCKET_ERROR)
		Log::Print("Failed at reliable.Update: %ld\n", WSAGetLastError());*/
}

void ClientBase::CheckBroadcast()
{

}

unsigned long ClientBase::GetLocalAddress()
{
	return reliable.GetLocalAddress();
}

}

