//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_MATH3D_
#define _SIPHON_MATH3D_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef WIN32
    #include <windows.h>
#endif
#include <GL/gl.h>

namespace Siphon
{

#define _11 0
#define _21 1
#define _31 2
#define _41 3
#define _12 4
#define _22 5
#define _32 6
#define _42 7
#define _13 8
#define _23 9
#define _33 10
#define _43 11
#define _14 12
#define _24 13
#define _34 14
#define _44 15

/*
__inline int quick_ftol(float f)
{
	__asm(
		"fld DWORD PTR %f\nfistp DWORD PTR %dlong"
		);
	return dlong.i[0];
}*/

/**
 * global math functions
 */
class Math
{
public:

	enum ENUM_SSE
	{
		NONE = 0x0,
		SSE = 0x1 << 0,
		SSE2 = 0x1 << 1,
		MMX = 0x1 << 2,
	};
	static ENUM_SSE SupportsSSE();

	static inline float DtoR( float degrees )
	{
		return (float)degrees * 0.01745f; //(PI / 180 )
	}

	static inline float RtoD( float radians )
	{
		return (float)radians * 57.29577f;//( 180 / PI )
	}

	static const float Epsilon;
	static const float Pi;
};

enum POSTION
{
	X = 0,
	Y = 1,
	Z = 2
};

enum ROTATION
{
	YAW = 0,
	PITCH = 1,
	ROLL = 2
};

/**
 * a vector in 3d space
 */
class Vector3
{
public:
	enum POSTION
	{
		X = 0,
		Y = 1,
		Z = 2
	};

	enum ROTATION
	{
		YAW = 0,
		PITCH = 1,
		ROLL = 2
	};

	Vector3()
	{
	}

	Vector3(float x, float y, float z)
	{
		v[X] = x;
		v[Y] = y;
		v[Z] = z;
	}

	inline float& operator[]( const int index )
	{
		return v[ index ];
	}

	inline const float& operator[]( const int index ) const
	{
		return v[ index ];
	}

	void operator=( const Vector3& rhs )
	{
		for( int i = 0; i < 3; ++i )
			v[i] = rhs.v[i];
	}

	bool operator==( const Vector3& rhs )
	{
		if( rhs.v[0] == v[0] &&
			rhs.v[1] == v[1] &&
			rhs.v[2] == v[2] )
			return true;

		return false;
	}

	bool operator!=( const Vector3& rhs )
	{
		return !operator==(rhs);
	}

	void Print()
	{
		printf("X: %f Y: %f Z: %f\n", v[X], v[Y], v[Z] );
	}

	Vector3 operator-( const Vector3& rhs ) const
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] - rhs.v[i];

		return ret;
	}

	Vector3 operator-( const float& rhs ) const
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] - rhs;

		return ret;
	}

	Vector3 operator-() const
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = -v[i];

		return ret;
	}

	Vector3 operator+=(const Vector3& rhs)
	{
		for( int i = 0; i < 3; ++i )
			v[i] += rhs.v[i];

		return *this;
	}

	Vector3 operator*( const float rhs ) const
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] * rhs;

		return ret;
	}

	Vector3 operator*( const Vector3& rhs ) const
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] * rhs[i];

		return ret;
	}

	Vector3 operator/( const float rhs )
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] / rhs;

		return ret;
	}

	Vector3 operator+( const Vector3& rhs ) const
	{
		return Vector3(v[0] + rhs.v[0], v[1] + rhs.v[1], v[2] + rhs.v[2]);
	}

	const float* GetPointer()
	{
		return v;
	}

	/*
	 * returns a new vector perp to both vectors
	 */
	Vector3 CrossProduct(const Vector3& other) const
	{
		Vector3 vNormal;

		// Calculate the cross product with the non communitive equation
		vNormal[X] = ((other[Y] * v[Z]) - (other[Z] * v[Y]));
		vNormal[Y] = ((other[Z] * v[X]) - (other[X] * v[Z]));
		vNormal[Z] = ((other[X] * v[Y]) - (other[Y] * v[X]));

		return vNormal;
	}

	/*
	 * gives some idea of angle between vectors
	 */
	inline float Dot(const Vector3& other) const
	{
		float dot = 0;

		for( int i = 0; i < 3; ++i )
			dot += v[i] * other[i];

		return dot;
	}

	// make sure both vectors are normalised
	// returns radians
	float AngleBetween( Vector3& other )
	{
		float dot = Dot(other);
		return acosf(dot);
	}

	Vector3 Refract(const Vector3& normal, float refractionIdx) const
	{
		// snells law:
		// http://en.wikipedia.org/wiki/Law_of_Refraction

		float cosIncidence = Dot(normal);
		//cosIncidence = cos(cosIncidence);
		float cosRefraction = -sqrt(1.0f - (refractionIdx * refractionIdx) * (1.0f - cosIncidence * cosIncidence));
		Vector3 refraction = (*this) * refractionIdx + normal * (cosRefraction - refractionIdx * cosIncidence);
		refraction.Normalize();
		return refraction;
	}

	float Magnitude() const
	{
		// Here is the equation:  magnitude = sqrt(V.x^2 + V.y^2 + V.z^2) : Where V is the vector
		return (float)sqrt( (v[X] * v[X]) +
				(v[Y] * v[Y]) +	(v[Z] * v[Z]) );
	}

	float MagnitudeSquared() const
	{
		// Here is the equation:  magnitude squared = (V.x^2 + V.y^2 + V.z^2) : Where V is the vector
		return (float)((v[X] * v[X]) +
				(v[Y] * v[Y]) +	(v[Z] * v[Z]));
	}

	Vector3 Normalize()
	{
		// Get the magnitude of our normal
		float magnitude = Magnitude();

		// Now that we have the magnitude, we can divide our vector by that magnitude.
		// That will make our vector a total length of 1.
		for( int i = 0; i < 3; ++i )
			v[i] /= magnitude;

		return (*this);
	}

	void Rotate( float angle, float x, float y, float z )
	{
		// Calculate the sine and cosine of the angle once
		float cosTheta = (float)cos(angle);
		float sinTheta = (float)sin(angle);

		Vector3 old = *this;
		// Find the new x position for the new rotated point
		v[X]  = (cosTheta + (1 - cosTheta) * x * x) * old[X];
		v[X] += ((1 - cosTheta) * x * y - z * sinTheta)	* old[Y];
		v[X] += ((1 - cosTheta) * x * z + y * sinTheta)	* old[Z];

		// Find the new y position for the new rotated point
		v[Y]  = ((1 - cosTheta) * x * y + z * sinTheta)	* old[X];
		v[Y] += (cosTheta + (1 - cosTheta) * y * y) * old[Y];
		v[Y] += ((1 - cosTheta) * y * z - x * sinTheta)	* old[Z];

		// Find the new z position for the new rotated point
		v[Z]  = ((1 - cosTheta) * x * z - y * sinTheta)	* old[X];
		v[Z] += ((1 - cosTheta) * y * z + x * sinTheta)	* old[Y];
		v[Z] += (cosTheta + (1 - cosTheta) * z * z) * old[Z];
	}

	void Rotate( float angle, Vector3& axis )
	{
		// Calculate the sine and cosine of the angle once
		float cosTheta = (float)cos(angle);
		float sinTheta = (float)sin(angle);

		Vector3 old = *this;
		// Find the new x position for the new rotated point
		v[X]  = (cosTheta + (1 - cosTheta) * axis[X] * axis[X]) * old[X];
		v[X] += ((1 - cosTheta) * axis[X] * axis[Y] - axis[Z] * sinTheta)	* old[Y];
		v[X] += ((1 - cosTheta) * axis[X] * axis[Z] + axis[Y] * sinTheta)	* old[Z];

		// Find the new y position for the new rotated point
		v[Y]  = ((1 - cosTheta) * axis[X] * axis[Y] + axis[Z] * sinTheta)	* old[X];
		v[Y] += (cosTheta + (1 - cosTheta) * axis[Y] * axis[Y]) * old[Y];
		v[Y] += ((1 - cosTheta) * axis[Y] * axis[Z] - axis[X] * sinTheta)	* old[Z];

		// Find the new z position for the new rotated point
		v[Z]  = ((1 - cosTheta) * axis[X] * axis[Z] - axis[Y] * sinTheta)	* old[X];
		v[Z] += ((1 - cosTheta) * axis[Y] * axis[Z] + axis[X] * sinTheta)	* old[Y];
		v[Z] += (cosTheta + (1 - cosTheta) * axis[Z] * axis[Z]) * old[Z];
	}

	// Debug drawing of axis
	void Draw(const Vector3& startPos = Vector3(0,0,0), const Vector3& colour = Vector3(1,0,0)) const
	{
		glBegin(GL_LINES);
		{
			glColor4f(colour[Vector3::X], colour[Vector3::Y], colour[Vector3::Z], 0.5f);
			glVertex3f(startPos[Vector3::X], startPos[Vector3::Y], startPos[Vector3::Z]);
			glVertex3f(v[Vector3::X], v[Vector3::Y], v[Vector3::Z]);
		}
		glEnd();

		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	}

	float GetX()
	{
		return v[X];
	}

	float GetY()
	{
		return v[Y];
	}

	float GetZ()
	{
		return v[Z];
	}

	float* GetFloats()
	{
		return v;
	}

	const float* GetConstFloats() const
	{
		return v;
	}

protected:
	float v[3];
};
/*
class Int4
{
public:

	Int4( float a = 0, float b = 0, float c = 0, float d = 0 )
	{
		i[0] = a;
		i[1] = b;
		i[2] = c;
		i[3] = d;
	}

	bool operator==( const Int4& rhs )
	{
		if( rhs.i[0] == i[0] &&
			rhs.i[1] == i[1] &&
			rhs.i[2] == i[2] &&
			rhs.i[3] == i[3] )
			return true;

		return false;
	}

	bool operator!=( const Int4& rhs )
	{
		return !operator==(rhs);
	}

	inline int& operator[]( const int index )
	{
		return i[ index ];
	}

	int* GetInts()
	{
		return i;
	}
protected:
	int i[4];
};
*/
class Vector4
{
public:
	enum POSTION
	{
		X = 0,
		Y = 1,
		Z = 2,
		W = 3
	};

	Vector4( float x = 0, float y = 0, float z = 0, float w = 0 )
	{
		v[X] = x;
		v[Y] = y;
		v[Z] = z;
		v[W] = w;
	}

	bool operator==( const Vector4& rhs )
	{
		if( rhs.v[0] == v[0] &&
			rhs.v[1] == v[1] &&
			rhs.v[2] == v[2] &&
			rhs.v[3] == v[3] )
			return true;

		return false;
	}

	bool operator!=( const Vector4& rhs )
	{
		return !operator==(rhs);
	}

	inline float& operator[]( const int index )
	{
		return v[ index ];
	}

	float* GetFloats()
	{
		return v;
	}

protected:
	float v[4];
};

class Matrix3
{
public:
	Matrix3()
	{
	}

	Matrix3( float mat0, float mat1, float mat2, float mat3, float mat4, float mat5,
		float mat6, float mat7, float mat8)
	{
		m[0] = mat0;
		m[1] = mat1;
		m[2] = mat2;
		m[3] = mat3;
		m[4] = mat4;
		m[5] = mat5;
		m[6] = mat6;
		m[7] = mat7;
		m[8] = mat8;
	}

	inline float& operator[]( const int index )
	{
		return m[ index ];
	}

	float Determinant()
	{
		return	(m[0] * (m[4] * m[8] - m[5] * m[7])) -
				(m[3] * (m[1] * m[8] - m[2] * m[7])) +
				(m[6] * (m[1] * m[5] - m[4] * m[2]));
	}

	/*
			| A B C |
		M = | D E F |
			| G H I |

	then the determinant is calculated as follows:
		det M = A * (EI - HF) - B * (DI - GF) + C * (DH - GE)

		Providing that the determinant is non-zero, then the inverse is
	calculated as:

		-1     1      |   EI-FH  -(BI-HC)   BF-EC  |
		M   = ----- . | -(DI-FG)   AI-GC  -(AF-DC) |
			det M     |   DH-GE  -(AH-GB)   AE-BD  |
	 */
	void Inverse()
	{
		float det = 1.0f / Determinant();

		// copy this it a temp matrix
		Matrix3 t = *this;

		m[0] = (t[4]*t[8] - t[7]*t[5]) * det;
		m[1] = -(t[1]*t[8] - t[7]*t[2]) * det;
		m[2] = (t[1]*t[5] - t[4]*t[2]) * det;

		m[3] = -(t[3]*t[8] - t[6]*t[5]) * det;
		m[4] = (t[0]*t[8] - t[2]*t[6]) * det;
		m[5] = -(t[0]*t[5] - t[3]*t[2]) * det;

		m[6] = (t[3]*t[7] - t[6]*t[4]) * det;
		m[7] = -(t[0]*t[7] - t[1]*t[6]) * det;
		m[8] = (t[0]*t[4] - t[3]*t[1]) * det;
	}

protected:
	float m[9];
};

/**
 * 4x4 matrix
 *
 * m[0]  m[4]  m[ 8]  m[12]
 * m[1]  m[5]  m[ 9]  m[13]
 * m[2]  m[6]  m[10]  m[14]
 * m[3]  m[7]  m[11]  m[15]
 *
 *   x axis - column 0
 *  y axis - column 1
 *   z axis - column 3
 */
class Matrix4
{
public:
	enum MatrixType
	{
		Identity,
		Zero,
		Mirror
	};

	inline Matrix4()
	{

	}

	Matrix4( float mat[16] )
	{
		for( int i = 0; i < 16; ++i )
			m[i] = mat[i];
	}

	Matrix4( const Matrix4& mat )
	{
		for( int i = 0; i < 16; ++i )
			m[i] = mat.m[i];
	}

	Matrix4( MatrixType type )
	{
		if (type == Identity)
			LoadIdentity();
		else if (type == Zero)
			LoadZero();
		else
			LoadMirror();
	}

	Matrix4(	const Vector3& xAxis,
				const Vector3& yAxis,
				const Vector3& zAxis,
				const Vector3& translation)
	{
		SetColumn(xAxis, X);
		SetColumn(yAxis, Y);
		SetColumn(zAxis, Z);
		SetTranslation(translation);
		
		m[3] = m[7] = m[11] = 0.0f;
		m[15] = 1.0f;
	}

	Matrix4(	float mat0, float mat4, float mat8,  float mat12, 
				float mat1, float mat5, float mat9,  float mat13,
				float mat2, float mat6, float mat10, float mat14,
				float mat3, float mat7, float mat11, float mat15)
	{
		m[0] = mat0;
		m[1] = mat1;
		m[2] = mat2;
		m[3] = mat3;
		m[4] = mat4;
		m[5] = mat5;
		m[6] = mat6;
		m[7] = mat7;
		m[8] = mat8;
		m[9] = mat9;
		m[10] = mat10;
		m[11] = mat11;
		m[12] = mat12;
		m[13] = mat13;
		m[14] = mat14;
		m[15] = mat15;
	}

	void SetScale(float scale)
	{
		m[0] = scale;
		m[5] = scale;
		m[10] = scale;
	}

	void SetScale(float x, float y, float z)
	{
		m[0] = x;
		m[5] = y;
		m[10] = z;
	}

	void LoadIdentity()
	{
		m[0] = m[5]  = m[10] = m[15] = 1.0f;
		m[1] = m[2]  = m[3]  = m[4]  = 0.0f;
		m[6] = m[7]  = m[8]  = m[9]  = 0.0f;
		m[11]= m[12] = m[13] = m[14] = 0.0f;
	}

	void LoadZero()
	{
		m[0] = m[5]  = m[10] = m[15] = 0.0f;
		m[1] = m[2]  = m[3]  = m[4]  = 0.0f;
		m[6] = m[7]  = m[8]  = m[9]  = 0.0f;
		m[11]= m[12] = m[13] = m[14] = 0.0f;
	}

	void LoadMirror()
	{
		m[5] = -1.0f;
		m[0] = m[10] = m[15] = 1.0f;
		m[1] = m[2]  = m[3]  = m[4]  = 0.0f;
		m[6] = m[7]  = m[8]  = m[9]  = 0.0f;
		m[11]= m[12] = m[13] = m[14] = 0.0f;
	}

	void SetOrthographic(float width, float height, float far, float near)
	{/*
		LoadIdentity();

		float right = width / 2.0f;
		float left = -right;

		float top = height / 2.0f;
		float bottom = -top;

		m[0]  = 2.0f / (right - left);
		m[5]  = 2.0f / (top - bottom);
		m[10] = 2.0f / (far - near);

		m[12] = (right + left) / (right - left);
		m[13] = (top + bottom) / (top - bottom);
		m[14] = (far + near) / (far - near);*/
	}

	void SetPerspective(float fovy, float aspect, float znear, float zfar)
	{
		LoadZero();

		float f = 1.0f/tan(Math::DtoR(fovy/2.0f));

		m[0]  = f / aspect;
		m[5]  = f;
		m[10] = (zfar + znear) / (znear - zfar);

		m[11] = -1.0f;
		m[14] = (2.0f * zfar * znear) / (znear - zfar);
	}

	inline float& operator[](const int index)
	{
		return m[index];
	}

	inline float operator[](const int index) const
	{
		return m[index];
	}

	void Translate(float x, float y, float z)
	{
		Matrix4 n(Matrix4::Identity);

		n[12] = x;
		n[13] = y;
		n[14] = z;

		(*this) = (*this) * n;
	}

	void Translate(const Vector3& translate)
	{
		Matrix4 n(Matrix4::Identity);

		n[12] = translate[Vector3::X];
		n[13] = translate[Vector3::Y];
		n[14] = translate[Vector3::Z];

		(*this) = (*this) * n;
	}

	void SetTranslation( float x, float y, float z )
	{
		m[12] = x;
		m[13] = y;
		m[14] = z;
	}

	void SetTranslation(const Vector3& translate)
	{
		m[12] = translate[Vector3::X];
		m[13] = translate[Vector3::Y];
		m[14] = translate[Vector3::Z];
	}

	void Print()
	{
		for( int i = 0; i < 16; ++i )
		{
			printf("m[%i]  =  %f\n", i, m[i] );
		}
	}

	void RotateY( float angle )
	{
		Matrix4 n(Matrix4::Identity);

		float c = (float)cos( angle );
		float s = (float)sin( angle );

		n.m[0] = c;
		n.m[2] = s;
		n.m[8] = -s;
		n.m[10] = c;

		(*this) = (*this) * n;
	}

	void RotateX( float angle )
	{
		Matrix4 n(Matrix4::Identity);

		float c = (float)cos( angle );
		float s = (float)sin( angle );

		n.m[5] = c;
		n.m[6] = -s;
		n.m[9] = s;
		n.m[10] = c;

		(*this) = (*this) * n;
	}

	void RotateZ( float angle )
	{
		Matrix4 n(Matrix4::Identity);

		float c = (float)cos( angle );
		float s = (float)sin( angle );

		n.m[0] = c;
		n.m[1] = -s;
		n.m[4] = s;
		n.m[5] = c;

		(*this) = (*this) * n;
	}

	void RotateAxis( float angle, Vector3 axis )
	{
		float c = (float)cos(angle);
		float s = (float)sin(angle);

		m[0] = c + ((axis[Vector3::X] * axis[Vector3::X]) * (1 - c));
		m[1] = (axis[Vector3::X] * axis[Vector3::Y] * (1 - c)) + (axis[Vector3::Z] * s);
		m[2] = (axis[Vector3::X] * axis[Vector3::Z] * (1 - c)) - (axis[Vector3::Y] * s);

		m[4] = (axis[Vector3::X] * axis[Vector3::Y] * (1 - c)) - (axis[Vector3::Z] * s);
		m[5] = c + (axis[Vector3::Y] * axis[Vector3::Y] * (1 - c));
		m[6] = (axis[Vector3::Y] * axis[Vector3::Z] * (1 - c)) + (axis[Vector3::X] * s);

		m[8] = (axis[Vector3::X] * axis[Vector3::Z] * (1 - c)) + (axis[Vector3::Y] * s);
		m[9] = (axis[Vector3::Y] * axis[Vector3::Z] * (1 - c)) - (axis[Vector3::X] * s);
		m[10]= c + (axis[Vector3::Z] * axis[Vector3::Z] * (1 - c));
	}

	inline Vector3 operator*(const Vector3& vector) const
	{
		return Transform(vector);
	}

	inline Vector3 Transform(const Vector3& vector) const
	{/*
		float vec[4], vecCur[4];
		int i;

		for( i = 0; i < 4; ++i )
		{
			vec[i] = 0;

			if( i < 3 )
				vecCur[i] = vector[i];
			else
				vecCur[i] = 1;
		}

		//multiply matrix by vector.. arg
		for( i = 0; i < 4; ++i )
		{
			for( int j = 0; j < 4; ++j )
				vec[i] += (m[ i + (4 * j) ] * vecCur[j]);
		}

		return Vector3( vec[0], vec[1], vec[2] );*/

		Vector3 ret;

		ret[0] = (m[0] * vector[0]) + (m[4] * vector[1]) + (m[8] * vector[2]) + m[12];
		ret[1] = (m[1] * vector[0]) + (m[5] * vector[1]) + (m[9] * vector[2]) + m[13];
		ret[2] = (m[2] * vector[0]) + (m[6] * vector[1]) + (m[10] * vector[2]) + m[14];

		return ret;
	}

	inline Matrix4 operator*(const Matrix4& rhs) const
	{
		return Multiply(rhs);
	}

	inline Matrix4 operator*(const float rhs) const
	{
		Matrix4 ret;

		ret.m[0] = m[0] * rhs;
		ret.m[1] = m[1] * rhs;
		ret.m[2] = m[2] * rhs;
		ret.m[3] = m[3] * rhs;

		ret.m[4] = m[4] * rhs;
		ret.m[5] = m[5] * rhs;
		ret.m[6] = m[6] * rhs;
		ret.m[7] = m[7] * rhs;

		ret.m[8] = m[8] * rhs;
		ret.m[9] = m[9] * rhs;
		ret.m[10] = m[10] * rhs;
		ret.m[11] = m[11] * rhs;

		ret.m[12] = m[12] * rhs;
		ret.m[13] = m[13] * rhs;
		ret.m[14] = m[14] * rhs;
		ret.m[15] = m[15] * rhs;

		return ret;
	}

	inline Matrix4 MirrorMatrix(const Matrix4& matrixToMirror)
	{
		// NOTE: try changing mirroring around y to mirroring around z!
		Matrix4 mirror(Matrix4::Mirror);
		Matrix4 temp = (mirror * (*this));
		Matrix4 temp2 = matrixToMirror * temp; // * temp;

		return temp2;
	}

	inline Matrix4 Multiply(const Matrix4& rhs) const
	{
		Matrix4 ret;

		ret.m[0] = (rhs.m[0] * m[0]) + (rhs.m[4] * m[1]) + (rhs.m[8] * m[2]) + (rhs.m[12] * m[3]);
		ret.m[1] = (rhs.m[1] * m[0]) + (rhs.m[5] * m[1]) + (rhs.m[9] * m[2]) + (rhs.m[13] * m[3]);
		ret.m[2] = (rhs.m[2] * m[0]) + (rhs.m[6] * m[1]) + (rhs.m[10] * m[2]) + (rhs.m[14] * m[3]);
		ret.m[3] = (rhs.m[3] * m[0]) + (rhs.m[7] * m[1]) + (rhs.m[11] * m[2]) + (rhs.m[15] * m[3]);

		ret.m[4] = (rhs.m[0] * m[4]) + (rhs.m[4] * m[5]) + (rhs.m[8] * m[6]) + (rhs.m[12] * m[7]);
		ret.m[5] = (rhs.m[1] * m[4]) + (rhs.m[5] * m[5]) + (rhs.m[9] * m[6]) + (rhs.m[13] * m[7]);
		ret.m[6] = (rhs.m[2] * m[4]) + (rhs.m[6] * m[5]) + (rhs.m[10] * m[6]) + (rhs.m[14] * m[7]);
		ret.m[7] = (rhs.m[3] * m[4]) + (rhs.m[7] * m[5]) + (rhs.m[11] * m[6]) + (rhs.m[15] * m[7]);

		ret.m[8] = (rhs.m[0] * m[8]) + (rhs.m[4] * m[9]) + (rhs.m[8] * m[10]) + (rhs.m[12] * m[11]);
		ret.m[9] = (rhs.m[1] * m[8]) + (rhs.m[5] * m[9]) + (rhs.m[9] * m[10]) + (rhs.m[13] * m[11]);
		ret.m[10] = (rhs.m[2] * m[8]) + (rhs.m[6] * m[9]) + (rhs.m[10] * m[10]) + (rhs.m[14] * m[11]);
		ret.m[11] = (rhs.m[3] * m[8]) + (rhs.m[7] * m[9]) + (rhs.m[11] * m[10]) + (rhs.m[15] * m[11]);

		ret.m[12] = (rhs.m[0] * m[12]) + (rhs.m[4] * m[13]) + (rhs.m[8] * m[14]) + (rhs.m[12] * m[15]);
		ret.m[13] = (rhs.m[1] * m[12]) + (rhs.m[5] * m[13]) + (rhs.m[9] * m[14]) + (rhs.m[13] * m[15]);
		ret.m[14] = (rhs.m[2] * m[12]) + (rhs.m[6] * m[13]) + (rhs.m[10] * m[14]) + (rhs.m[14] * m[15]);
		ret.m[15] = (rhs.m[3] * m[12]) + (rhs.m[7] * m[13]) + (rhs.m[11] * m[14]) + (rhs.m[15] * m[15]);

		/*
		for( int k = 0; k < 4; ++k )
		{
			for( int i = 0; i < 4; ++i )
			{
				for( int j = 0; j < 4; ++j )
				{
					ret.m[ 4 * k + i] += m[ 4 * k + j] * rhs.m[ 4 * j + i];
				}
			}
		}*/

		return ret;
	}

	void operator=( const Matrix4& rhs )
	{
		for( int i = 0; i < 16; ++i )
			m[i] = rhs.m[i];
	}

#ifdef _SIPHON_EXPORT_
	Matrix4(GMatrix& mat)
	{
		m[0] = mat[0][0];
		m[1] = mat[0][1];
		m[2] = mat[0][2];
		m[3] = mat[0][3];

		m[4] = mat[1][0];
		m[5] = mat[1][1];
		m[6] = mat[1][2];
		m[7] = mat[1][3];

		m[8] = mat[2][0];
		m[9] = mat[2][1];
		m[10] = mat[2][2];
		m[11] = mat[2][3];

		m[12] = mat[3][0];
		m[13] = mat[3][1];
		m[14] = mat[3][2];
		m[15] = mat[3][3];
	}
#endif

#ifndef _SIPHON_EXPORT_

	/**
	 * Loads current modelview matrix
	 */
	void LoadCurrent( GLint type = GL_MODELVIEW_MATRIX )
	{
		glGetFloatv( type, m );
	}

	// Debug drawing of axis
	void Draw(float length = 1.0f) const
	{
		Vector3 axis[4] = { Vector3(0,0,0),
							Vector3(length,0,0),
							Vector3(0,length,0),
							Vector3(0,0,length) };

		for (int i = 0; i < 4; ++i)
			axis[i] = operator*(axis[i]);

		glBegin(GL_LINES);
		{
			glColor4f(1.0f, 0.0f, 0.0f, 0.5f);
			glVertex3f(axis[0][Vector3::X], axis[0][Vector3::Y], axis[0][Vector3::Z]);
			glVertex3f(axis[1][Vector3::X], axis[1][Vector3::Y], axis[1][Vector3::Z]);

			glColor4f(0.0f, 1.0f, 0.0f, 0.5f);
			glVertex3f(axis[0][Vector3::X], axis[0][Vector3::Y], axis[0][Vector3::Z]);
			glVertex3f(axis[2][Vector3::X], axis[2][Vector3::Y], axis[2][Vector3::Z]);

			glColor4f(0.0f, 0.0f, 1.0f, 0.5f);
			glVertex3f(axis[0][Vector3::X], axis[0][Vector3::Y], axis[0][Vector3::Z]);
			glVertex3f(axis[3][Vector3::X], axis[3][Vector3::Y], axis[3][Vector3::Z]);
		}
		glEnd();

		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	}

	/**
	 * set this as current matrix
	 */
	void Set()
	{
		glLoadMatrixf(m);
	}

	/**
	 * set this as current matrix
	 * but multiplies with the current gl matrix
	 */
	void SetM() const
	{
		glMultMatrixf(m);
	}

#endif

	/**
	 * load a 4x3 rotation matrix OVER this rotation matrix
	 */
	void LoadRotation4x3( float* rotation )
	{
		for( int i = 0; i < 12; ++i )
		{
			m[i] = rotation[i];
		}
	}

	/**
	 * loads an ODE rotation,
	 * ODE uses different coord system,
	 * so needs to be transposed in
	 */
	void ODELoadRotation4x3( float* rotation )
	{
		m[0] = rotation[0];
		m[1] = rotation[4];
		m[2] = rotation[8];
		m[4] = rotation[1];
		m[5] = rotation[5];
		m[6] = rotation[9];
		m[8] = rotation[2];
		m[9] = rotation[6];
		m[10] = rotation[10];
		/*
		m[0] = rotation[0];
		m[1] = rotation[1];
		m[2] = rotation[2];
		m[4] = rotation[4];
		m[5] = rotation[5];
		m[6] = rotation[6];
		m[8] = rotation[8];
		m[9] = rotation[9];
		m[10] = rotation[10];*/
	}

	void Load( float* fm )
	{
		for( int i = 0; i < 16; ++i )
		{
			m[i] = fm[i];
		}
	}

	void Transpose()
	{
		Matrix4 temp = (*this);
		m[1] = temp[4];
		m[4] = temp[1];

		m[2] = temp[8];
		m[8] = temp[2];

		m[6] = temp[9];
		m[9] = temp[6];


		m[3] = temp[12];
		m[12] = temp[3];

		m[7] = temp[13];
		m[13] = temp[7];

		m[11] = temp[14];
		m[14] = temp[11];
	}

	/**
	 * This will return a 3x3 sub matrix of this matrix,
	 * it defaults to return the roational part of the matrix
	 */
	inline Matrix3 GetMatrix3(int row = 3, int col = 3)
	{
		Matrix3 mat;

		for (int c = 0, i = 0; c < 4; ++c, ++i)
		{
			if (c == col)
			{
				++c;

				if (c >= 4)
					break;
			}

			for (int r = 0, j = 0; r < 4; ++r, ++j)
			{
				if (r == row)
				{
					++r;

					if (r >= 4)
						break;
				}

				mat[i * 3 + j] = m[c * 4 + r];
			}
		}

		return mat;
	}

	/**
	 * copys a matrix3 into this matrix4, skipping row row, and col col
	 */
	inline void SetMatrix3(Matrix3 mat, int row = 3, int col = 3)
	{
		for (int c = 0, i = 0; c < 4; ++c, ++i)
		{
			if (c == col)
			{
				++c;

				if (c >= 4)
					break;
			}

			for (int r = 0, j = 0; r < 4; ++r, ++j)
			{
				if (r == row)
				{
					++r;

					if (r >= 4)
						break;
				}

				m[c * 4 + r] = mat[i * 3 + j];
			}
		}
	}

	inline float Determinant()
	{
		// http://www.cs.virginia.edu/~cs551dl/assignment3/matinv.c

		Matrix3 a = GetMatrix3(0, 0);
		Matrix3 b = GetMatrix3(0, 1);
		Matrix3 c = GetMatrix3(0, 2);
		Matrix3 d = GetMatrix3(0, 3);

		return (m[0] * a.Determinant()) - (m[4] * b.Determinant()) + (m[8] * c.Determinant()) - (m[12] * d.Determinant());
	}

	/*
	 *   adjoint()
	 *
	 *     calculate the adjoint of a 4x4 matrix
	 *
	 *      Let  a   denote the minor determinant of matrix A obtained by
	 *           ij
	 *
	 *      deleting the ith row and jth column from A.
	 *
	 *                    i+j
	 *     Let  b   = (-1)    a
	 *          ij            ji
	 *
	 *    The matrix B = (b  ) is the adjoint of A
	 *                     ij
	 */
	inline Matrix4 GetAdjoint()
	{
		// http://www.cs.virginia.edu/~cs551dl/assignment3/matinv.c

		Matrix4 ret;
		Matrix3 mat3;

		// we are effectivly doing a transpose here as well

		// column 0
		mat3 = GetMatrix3(0, 0);
		ret[0] = mat3.Determinant();

		mat3 = GetMatrix3(1, 0);
		ret[4] = -mat3.Determinant();

		mat3 = GetMatrix3(2, 0);
		ret[8] = mat3.Determinant();

		mat3 = GetMatrix3(3, 0);
		ret[12] = -mat3.Determinant();

		// column 1
		mat3 = GetMatrix3(0, 1);
		ret[1] = -mat3.Determinant();

		mat3 = GetMatrix3(1, 1);
		ret[5] = mat3.Determinant();

		mat3 = GetMatrix3(2, 1);
		ret[9] = -mat3.Determinant();

		mat3 = GetMatrix3(3, 1);
		ret[13] = mat3.Determinant();

		// column 2
		mat3 = GetMatrix3(0, 2);
		ret[2] = mat3.Determinant();

		mat3 = GetMatrix3(1, 2);
		ret[6] = -mat3.Determinant();

		mat3 = GetMatrix3(2, 2);
		ret[10] = mat3.Determinant();

		mat3 = GetMatrix3(3, 2);
		ret[14] = -mat3.Determinant();

		// column 3
		mat3 = GetMatrix3(0, 3);
		ret[3] = -mat3.Determinant();

		mat3 = GetMatrix3(1, 3);
		ret[7] = mat3.Determinant();

		mat3 = GetMatrix3(2, 3);
		ret[11] = -mat3.Determinant();

		mat3 = GetMatrix3(3, 3);
		ret[15] = mat3.Determinant();

		return ret;
	}

	void Inverse()
	{
		/*
		// http://www.j3d.org/matrix_faq/matrfaq_latest.html#Q24

		Matrix3 mat = GetMatrix3(3, 3);
		mat.Inverse();
		SetMatrix3(mat, 3, 3);
		*/

		// Inverse3x3();

		// http://www.cs.virginia.edu/~cs551dl/assignment3/matinv.c

		Matrix4 adjoint = GetAdjoint();
		float det = 1.0f / Determinant();

		for (int i = 0; i < 16; ++i)
			m[i] = adjoint[i] * det;
	}
/*
	// perform determinant on rotational part of matrix
	float Determinant3x3(int row = 0, int col = 0)
	{
		return	(m[0] * (m[5] * m[10] - m[6] * m[9])) -
				(m[4] * (m[1] * m[10] - m[2] * m[9])) +
				(m[8] * (m[1] * m[6 ] - m[5] * m[2]));
	}

	void Inverse3x3(int row = 0, int col = 0)
	{
		float det = 1.0f / Determinant3x3(row, col);

		// copy this it a temp matrix
		Matrix4 t = *this;

		m[0] = (t[5]*t[10] - t[9]*t[6]) * det;
		m[1] = (t[9]*t[2] - t[1]*t[10]) * det;
		m[2] = (t[1]*t[6] - t[5]*t[2]) * det;

		m[4] = (t[8]*t[6] - t[4]*t[10]) * det;
		m[5] = (t[0]*t[10] - t[8]*t[2]) * det;
		m[6] = (t[4]*t[2] - t[0]*t[6]) * det;

		m[8] = (t[4]*t[9] - t[8]*t[5]) * det;
		m[9] = (t[8]*t[1] - t[0]*t[9]) * det;
		m[10] = (t[0]*t[5] - t[4]*t[1]) * det;
	}
*/
	void QuickInverse()
	{
		Vector3 position = Vector3( m[12], m[13], m[14] );

		m[12] = -( (position[Vector3::X] * m[0]) + (position[Vector3::Y] * m[4]) + (position[Vector3::Z] * m[8]) );
		m[13] = -( (position[Vector3::X] * m[1]) + (position[Vector3::Y] * m[5]) + (position[Vector3::Z] * m[9]) );
		m[14] = -( (position[Vector3::X] * m[2]) + (position[Vector3::Y] * m[6]) + (position[Vector3::Z] * m[10]) );

		Transpose();
	}

	/*
	 * Returns a row of the matrix as a vector
	 */
	Vector3 GetRow( int row ) const
	{
		return Vector3( m[row], m[row + 4], m[row + 8] );
	}

	void SetColumn(const Vector3& col, int colIdx)
	{
		m[0 + (colIdx * 4)] = col[0];
		m[1 + (colIdx * 4)] = col[1];
		m[2 + (colIdx * 4)] = col[2];
	}

	Vector3 GetColumn(int colIdx) const
	{
		return Vector3(	m[0 + (colIdx * 4)],
						m[1 + (colIdx * 4)],
						m[2 + (colIdx * 4)]);
	}

	inline Vector3 GetTranslation() const
	{
		return Vector3(m[12], m[13], m[14]);
	}

	inline const float* GetData() const
	{
		return m;
	}

protected:
	float m[16];
};

/**
 * 4x4 matrix
 *
 * m[0]  m[4]  m[ 8]  m[12]
 * m[1]  m[5]  m[ 9]  m[13]
 * m[2]  m[6]  m[10]  m[14]
 * m[3]  m[7]  m[11]  m[15]
 *
 *   x axis - column 0
 *  y axis - column 1
 *   z axis - column 3
 */
class Quaternion
{
public:
	enum POSTION
	{
		X = 0,
		Y = 1,
		Z = 2,
		W = 3
	};

	Quaternion()
	{
	}

	Quaternion(float x, float y, float z, float w)
	{
		q[0] = x;
		q[1] = y;
		q[2] = z;
		q[3] = w;
	}

	Quaternion( const Vector3& axis, const float angle )
	{
		CreateFromAxisAngle(axis, angle);
	}

	void Conjugate()
	{
		q[X] = -q[X];
		q[Y] = -q[Y];
		q[Z] = -q[Z];
	}

	void Inverse()
	{
		Conjugate();
	}

	Vector3 operator*(const Vector3& t) const
	{
		Quaternion conj = *this;
		conj.Conjugate();

		Quaternion vec(t[X], t[Y], t[Z], 0.0f);

		Quaternion result;
		Quaternion temp;

		temp[X] = q[W] * vec[X] + q[Y] * vec[Z] - q[Z] * vec[Y];
        temp[Y] = q[W] * vec[Y] + q[Z] * vec[X] - q[X] * vec[Z];
        temp[Z] = q[W] * vec[Z] + q[X] * vec[Y] - q[Y] * vec[X];
		temp[W] = -q[X] * vec[X] - q[Y] * vec[Y] - q[Z] * vec[Z];

		result[X] = temp[W] * conj[X] + temp[X] * conj[W] + temp[Y] * conj[Z] - temp[Z] * conj[Y];
        result[Y] = temp[W] * conj[Y] + temp[Y] * conj[W] + temp[Z] * conj[X] - temp[X] * conj[Z];
        result[Z] = temp[W] * conj[Z] + temp[Z] * conj[W] + temp[X] * conj[Y] - temp[Y] * conj[X];

		return Vector3(result[X], result[Y], result[Z]);
	}

	void CreateFromMatrix( Matrix4& mat )
	{
		// http://skal.planet-d.net/demo/matrixfaq.htm#Q55
		float t = 1 + mat[0] + mat[5] + mat[10];

		if (t > Math::Epsilon) // should this be less than epsilon also?
		{
			float s = sqrtf(t) * 2;
			q[X] = (mat[9] - mat[6]) / s;
			q[Y] = (mat[2] - mat[8]) / s;
			q[Z] = (mat[4] - mat[1]) / s;
			q[W] = 0.25f * s;
		}
		else // if t == 0
		{
			if (mat[0] > mat[5] && mat[0] > mat[10]) // col1
			{
				float s = sqrtf(1.0f + mat[0] - mat[5] - mat[10]) * 2;
				q[X] = 0.25f * s;
				q[Y] = (mat[4] + mat[1]) / s;
				q[Z] = (mat[2] + mat[8]) / s;
				q[X] = (mat[9] - mat[6]) / s;
			}
			else if (mat[5] > mat[10]) // col2
			{
				float s = sqrtf(1.0f + mat[5] - mat[0] - mat[10]) * 2;
				q[X] = (mat[4] + mat[1]) / s;
				q[Y] = 0.25f * s;
				q[Z] = (mat[9] + mat[6]) / s;
				q[W] = (mat[2] - mat[8]) / s;
			}
			else //col3
			{
				float s = sqrtf(1.0f + mat[10] - mat[0] - mat[5]) * 2;
				q[X] = (mat[2] - mat[8]) / s;
				q[Y] = (mat[9] - mat[6]) / s;
				q[Z] = 0.25f * s;
				q[W] = (mat[4] - mat[1]) / s;
			}
		}
	}

	void CreateFromAxisAngle( const Vector3& axis, const float angle )
	{
		Vector3 temp = axis;
		temp = temp * (float)sin(angle/2.0f);

		q[0] = temp[0];
		q[1] = temp[1];
		q[2] = temp[2];

		q[3] = (float)cos(angle/2.0f);
	}

	// you should normalize before caling this?
	void GetAxisAngle(Vector3& axis, float& angle)
	{
		float cosAng = q[W];
		angle = acosf(cosAng) * 2;

		float sinAng = sqrtf(1.0f - cosAng * cosAng);
		if (fabsf( sinAng ) < 0.0005f)
			sinAng = 1;

		axis[Vector3::X] = q[X] / sinAng;
		axis[Vector3::Y] = q[Y] / sinAng;
		axis[Vector3::Z] = q[Z] / sinAng;
	}

	Quaternion operator*( const Quaternion& rhs ) const
	{
		Quaternion ret;

		ret[W] = (q[W] * rhs.q[W]) - (q[X] * rhs.q[X]) - (q[Y] * rhs.q[Y]) - (q[Z] * rhs.q[Z]);
		ret[X] = (q[W] * rhs.q[X]) + (q[X] * rhs.q[W]) + (q[Y] * rhs.q[Z]) - (q[Z] * rhs.q[Y]);
		ret[Y] = (q[W] * rhs.q[Y]) + (q[Y] * rhs.q[W]) + (q[Z] * rhs.q[X]) - (q[X] * rhs.q[Z]);
		ret[Z] = (q[W] * rhs.q[Z]) + (q[Z] * rhs.q[W]) + (q[X] * rhs.q[Y]) - (q[Y] * rhs.q[X]);

		return ret;
	}

	Quaternion operator*( const float& rhs )
	{
		Quaternion ret;

		for (int i = 0; i < 4; ++i)
			ret[i] = q[i] * rhs;

		return ret;
	}

	void operator=( const Quaternion& rhs )
	{
		for( int i = 0; i < 4; ++i )
			q[i] = rhs.q[i];
	}

	inline float& operator[]( const int index )
	{
		return q[index];
	}

	Quaternion operator+( const Quaternion& rhs )
	{
		return Quaternion(q[0] + rhs.q[0], q[1] + rhs.q[1], q[2] + rhs.q[2], q[3] + rhs.q[3]);
	}

	Quaternion operator-( const Quaternion& rhs )
	{
		return Quaternion(q[0] - rhs.q[0], q[1] - rhs.q[1], q[2] - rhs.q[2], q[3] - rhs.q[3]);
	}

	Matrix4 GetRotationMatrix()
	{
		Matrix4 m(Matrix4::Identity);
		float y2 = q[Y] * q[Y];
		float x2 = q[X] * q[X];
		float z2 = q[Z] * q[Z];

		m[0] = 1.0f - (2 * y2) - (2 * z2);
		m[1] = (2.0f * q[X] * q[Y]) + (2 * q[W] * q[Z]);
		m[2] = (2.0f * q[X] * q[Z]) - (2 * q[W] * q[Y]);

		m[4] = (2.0f * q[X] * q[Y]) - (2 * q[W] * q[Z]);
		m[5] = 1.0f - (2.0f * x2) - (2 * z2);
		m[6] = (2.0f * q[Y] * q[Z]) + (2 * q[W] * q[X]);

		m[8] = (2.0f * q[X] * q[Z]) + (2 * q[W] * q[Y]);
		m[9] = (2.0f * q[Y] * q[Z]) - (2 * q[W] * q[X]);
		m[10]= 1.0f - (2.0f * x2) - (2 * y2);

		return m;
	}

	float Magnitude()
	{
		return sqrtf(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
	}

	void Normalize()
	{
		float mag = Magnitude();
		q[0] /= mag;
		q[1] /= mag;
		q[2] /= mag;
		q[3] /= mag;
	}

	float Dot(Quaternion& rhs)
	{
		float dot = 0;

		for( int i = 0; i < 4; ++i )
			dot += q[i] * rhs[i];

		return dot;
	}

	float AngleBetween(Quaternion& rhs)
	{
		float dot = Dot(rhs);
		return acosf(dot);
	}

	Quaternion Interpolate(Quaternion& to, float weight)
	{
		// http://www.gamasutra.com/features/19980703/quaternions_01.htm

		Quaternion temp;
        float omega, cosom, sinom, scale0, scale1;

        // calc cosine
        cosom = Dot(to);

        // adjust signs (if necessary)
        if (cosom < 0.0)
		{
			cosom = -cosom;
			temp[X] = -to[X];
			temp[Y] = -to[Y];
			temp[Z] = -to[Z];
			temp[W] = -to[W];
        }
		else
		{
			temp[X] = to[X];
			temp[Y] = to[Y];
			temp[Z] = to[Z];
			temp[W] = to[W];
        }

        // calculate coefficients
       if ((1.0f - cosom) > 0.0001f)
	   {
            // standard case (slerp)
            omega = acosf(cosom);
            sinom = sinf(omega);
            scale0 = sinf((1.0f - weight) * omega) / sinom;
            scale1 = sinf(weight * omega) / sinom;
       }
	   else
	   {
			// "from" and "to" quaternions are very close
			//  ... so we can do a linear interpolation
            scale0 = 1.0f - weight;
            scale1 = weight;
        }

		Quaternion res;

		// calculate final values
		res[X] = scale0 * q[X] + scale1 * temp[X];
		res[Y] = scale0 * q[Y] + scale1 * temp[Y];
		res[Z] = scale0 * q[Z] + scale1 * temp[Z];
		res[W] = scale0 * q[W] + scale1 * temp[W];

		return res;
	}

protected:
	//x, y, z, w
	float q[4];
};


/**
 * a plane in 3d space
 * Ax + By + Cz + D = 0
 * normal = (a,b,c)
 * d = distance from origin
 *
 * Ax + By + Cz + D = 0
 * (x, y, z) are the coordinates of a point on the plane, and the coefficients A, B, C, and D 
 * are constants describe the spatial properties of the plane. A, B, and C can also represent 
 * a vector (a, b, c) which is a normal of the plane. Some plane equations show D to be negative 
 * instead of positive in this example.
 *
 * Once again, this can be thought of as taking two sides of a triangle, and then trying to find 
 * the third side (the resultant vector). 
 */
class Plane
{
public:
	enum PLANE
	{
		ON = 0,
		BACK = -1,
		FRONT = 1
	};

	float& operator[]( const int index )
	{
		return p[ index ];
	}

#ifndef _SIPHON_EXPORT_

	void BindClipPlane(int planeNum = 0)
	{
		glEnable(GL_CLIP_PLANE0 + planeNum);
		double ptemp[4] =  {p[0], p[1], p[2], p[3]};
		glClipPlane(GL_CLIP_PLANE0 + planeNum, ptemp);
	}

	static void RestoreDefaultClipPlanes()
	{
		for (int i = 0; i < 6; ++i)
		{
			glDisable(GL_CLIP_PLANE0 + i);
		}
	}

#endif

	void Print()
	{
		printf("N(%f %f %f) D(%f)\n", p[0], p[1], p[2], p[3]);
	}

	Plane Transform(const Matrix4& transform) const
	{
		Plane ret;

		ret[0] = (transform[0] * p[0]) + (transform[4] * p[1]) + (transform[8] * p[2])  + (transform[12] * p[3]);
		ret[1] = (transform[1] * p[0]) + (transform[5] * p[1]) + (transform[9] * p[2])  + (transform[13] * p[3]);
		ret[2] = (transform[2] * p[0]) + (transform[6] * p[1]) + (transform[10] * p[2]) + (transform[14] * p[3]);
		ret[3] = (transform[3] * p[0]) + (transform[7] * p[1]) + (transform[11] * p[2]) + (transform[15] * p[3]);

		return ret;
	}

	int SideOfPlane(const Vector3& pnt) const
	{
		//plug point into plane equation
		//ax + by + cz = d
		float value = p[0] * pnt[Vector3::X] + p[1] * pnt[Vector3::Y] + p[2] * pnt[Vector3::Z] - p[3];

		if( value > 0 )
			return FRONT;
		if( value == 0 )
			return ON;
		if( value < 0 )
			return BACK;
	}

	inline Matrix4 Mirror(const Matrix4& origonal) const
	{
		// http://www.euclideanspace.com/maths/geometry/affine/reflection/

		float pXsquared = p[0] * p[0];
		float pYsquared = p[1] * p[1];
		float pZsquared = p[2] * p[2];

		float val = 1.0f / (pXsquared + pYsquared + pZsquared);

		Matrix4 m(Matrix4::Identity);

		m[0] = (-pXsquared + pZsquared + pYsquared);
		m[1] = (-2 * p[Y] * p[X]);
		m[2] = (-2 * p[Z] * p[X]);

		m[4] = (-2 * p[X] * p[Y]);
		m[5] = (-pYsquared + pXsquared + pZsquared);
		m[6] = (-2 * p[Z] * p[Y]);

		m[8] = (-2 * p[X] * p[Y]);
		m[9] = (-2 * p[Z] * p[Y]);
		m[10] = (-pZsquared + pYsquared + pXsquared);

		return origonal * (m * val);
	}

	
	inline Matrix4 Refract(const Matrix4& incidence, float refractionIdx) const
	{
		// snells law:
		// http://en.wikipedia.org/wiki/Law_of_Refraction

		Vector3 incidenceVec = incidence.GetColumn(Z);
		Vector3 refractVec = incidenceVec.Refract(GetNormal(), refractionIdx);

		Vector3 up = incidence.GetColumn(X).CrossProduct(refractVec);
		up.Normalize();

		Vector3 right = refractVec.CrossProduct(up);
		right.Normalize();

		// get distance from matrix to plane intersection point
		Vector3 translation = incidence.GetTranslation();
		float distFromOrigin = translation.Magnitude();
		Vector3 planeIntersection; 
		bool intersects = RayIntersect(translation, incidenceVec, planeIntersection);
		if (!intersects)
			return Matrix4(Matrix4::Identity);

		float planeDistance = (translation - planeIntersection).Magnitude();
		translation = planeIntersection - refractVec * planeDistance;

		// temp testing
		//translation = incidence.GetTranslation();
/*
	glColor3f(1, 1, 1);
	glBegin(GL_QUADS);
	{
		glVertex3f(planeIntersection[X], planeIntersection[Y], planeIntersection[Z]);
		glVertex3f(translation[X], translation[Y], translation[Z]);
	}
	glEnd();
*/
		Matrix4 refractMat(right, up, refractVec, translation);
		return refractMat;
	}

	bool RayIntersect(const Vector3& rayPosition, const Vector3& rayDirection, Vector3& intersectPoint) const
	{
		float intersectLength;
		bool intersect = RayIntersect(rayPosition, rayDirection, intersectLength);
		if (!intersect)
			return false;

		intersectPoint = rayPosition + rayDirection * intersectLength;
		return true;
	}

	bool RayIntersect(const Vector3& rayPosition, const Vector3& rayDirection, float& intersectLength) const
	{
		// ray intersects plane when 
		// ray * time = plane
		// Ax + By + Cz + D = 0

		Vector3 normal = GetNormal();

		// if angle between normal and ray are at 90 degrees, ray wont intersect plane
		// or it lies on the plane
		float cosAngle = rayDirection.Dot(normal);
		if (cosAngle == 0.0f)
		{
			if (SideOfPlane(rayPosition) == ON)
				return true;

			return false;
		}

		/*
		// plug ray and t(time) in to the equation
		// t(p[0] * ray[X] + p[1] * ray[Y] + p[2] * ray[Z]) - p[3] = 0;
		// t(p[0] * ray[X] + p[1] * ray[Y] + p[2] * ray[Z]) = p[3]
		// t = p[3] / (p[0] * ray[X] + p[1] * ray[Y] + p[2] * ray[Z])
		float t = p[3] / (p[0] * ray[X] + p[1] * ray[Y] + p[2] * ray[Z]);
		intersectPoint = ray * t;*/

		// http://www.cs.montana.edu/~charon/thesis/tutorials/collision.php
		// find distance to closest point on the plane
		// we now have the angle between the normal and the ray
		// we also have the distance to the plane along the normal
		// use trig to calulate the distance to the plane along the hypotenus
		// sin = O / H
		// cos = A / H
		// tan = O/H / A/H = sin / cos
		//
		// H = A / cos
		float distance = DistanceToPoint(rayPosition);
		intersectLength = distance / cosAngle;
		return true;
	}

	void SetNormal(Vector3& normal)
	{
		p[0] = normal[0];
		p[1] = normal[1];
		p[2] = normal[2];
	}

	void SetDistanceFromOrigin(float d)
	{
		p[3] = d;
	}

	Vector3 Intersect3( Plane& p1, Plane& p2 )
	{
		Vector3 n1 = Vector3( p[0], p[1], p[2] );
		Vector3 n2 = Vector3( p1[0], p1[1], p1[2] );
		Vector3 n3 = Vector3( p2[0], p2[1], p2[2] );
		float d1 = p[3];
		float d2 = p1[3];
		float d3 = p2[3];

		//http://astronomy.swin.edu.au/~pbourke/geometry/3planes/
		Vector3 point = ((n2.CrossProduct( n3 ) * d1) + (n1.CrossProduct( n3 ) * d2) + (n1.CrossProduct( n2 ) * d3))
			/ n1.Dot( n2.CrossProduct( n3 ) );

		return point;
	}

	void CreateFromFace( Vector3& p1, Vector3& p2, Vector3& p3 )
	{
		//http://easyweb.easynet.co.uk/~mrmeanie/plane/planes.htm

		Vector3 vec1 = p1 - p2;
		Vector3 vec2 = p1 - p3;
		Vector3 normal = vec1.CrossProduct( vec2 );

		p[0] = normal[0];
		p[1] = normal[1];
		p[2] = normal[2];
		p[3] = p1.Dot( normal ); //p[0] * p1[X] + p[1] * p1[Y] + p[2] * p1[Z];
	}

	// the following are plane extractions from matrices
	// http://www2.ravensoft.com/users/ggribb/plane%20extraction.pdf
	void ExtractLeft(Matrix4& transform)
	{
		p[0] = transform[3] + transform[0];
		p[1] = transform[7] + transform[4];
		p[2] = transform[11] + transform[8];
		p[3] = transform[15] + transform[12];
	}

	void ExtractRight(Matrix4& transform)
	{
		p[0] = transform[3] - transform[0];
		p[1] = transform[7] - transform[4];
		p[2] = transform[11] - transform[8];
		p[3] = transform[15] - transform[12];
	}

	void ExtractTop(Matrix4& transform)
	{
		p[0] = transform[3] - transform[1];
		p[1] = transform[7] - transform[5];
		p[2] = transform[11] - transform[9];
		p[3] = transform[15] - transform[13];
	}

	void ExtractBottom(Matrix4& transform)
	{
		p[0] = transform[3] + transform[1];
		p[1] = transform[7] + transform[5];
		p[2] = transform[11] + transform[9];
		p[3] = transform[15] + transform[13];
	}

	void ExtractNear(Matrix4& transform)
	{
		p[0] = transform[3] + transform[2];
		p[1] = transform[7] + transform[6];
		p[2] = transform[11] + transform[10];
		p[3] = transform[15] + transform[14];
	}

	void ExtractFar(Matrix4& transform)
	{
		p[0] = transform[3] - transform[2];
		p[1] = transform[7] - transform[6];
		p[2] = transform[11] - transform[10];
		p[3] = transform[15] - transform[14];
	}

	void Normalize()
	{
		float mag;
		mag = sqrtf(p[0]*p[0] + p[1]*p[1] + p[2]*p[2]); // + p[3]*p[3]);

		p[0] /= mag;
		p[1] /= mag;
		p[2] /= mag;
		p[3] /= mag;
	}

	inline Vector3 GetNormal() const
	{
		return Vector3(p[0], p[1], p[2]);
	}

	inline float DistanceToPoint(Vector3 point) const
	{
		Vector3 n = GetNormal();
		return n.Dot(point) - GetDistance();
	}

	inline float GetDistance() const
	{
		return p[3];
	}

	float* GetFloats()
	{
		return p;
	}

protected:
	float p[4];
};

class Sphere
{
public:
	Sphere() {}

	Sphere(float radius, const Vector3& position) : radius(radius), position(position) {}

	inline void SetRadius(float radius) { this->radius = radius; }
	inline void SetPosition(const Vector3& position) { this->position = position; }

	inline const float& GetRadius() const { return radius; }
	inline const Vector3& GetPosition() const { return position; }

	// returns distance between spheres or -1 if no intersection
	inline float Intersect(const Sphere& other) const
	{
		Vector3 diff = other.position - position;
		float totalRange = radius + other.radius;
		totalRange *= totalRange;

		float diffSize = (diff[Vector3::X] * diff[Vector3::X]) + (diff[Vector3::Y] * diff[Vector3::Y]) +
			(diff[Vector3::Z] * diff[Vector3::Z]);

		if (diffSize < totalRange)
			return sqrtf(diffSize);

		return -1;
	}

#ifndef _SIPHON_EXPORT_

	void Draw(const Vector3& colour = Vector3(1.0, 1.0, 1.0)) const
	{
		glColor4f(colour[Vector3::X], colour[Vector3::Y], colour[Vector3::Z], 1.0f);

		glBegin(GL_LINES);
		{
			Vector3 cur;
			Vector3 last(sinf(0), cosf(0), 0);
			last = position + (last * radius);

			for (float a = 0.1f; a <= (Math::Pi * 2) + 0.1f; a += 0.1f)
			{
				cur = position + Vector3(sinf(a), cosf(a), 0) * radius;

				glVertex3f(last[Vector3::X], last[Vector3::Y], last[Vector3::Z]);
				glVertex3f(cur[Vector3::X], cur[Vector3::Y], cur[Vector3::Z]);

				last = cur;
			}

			last = position + (Vector3(sinf(0), 0, cosf(0)) * radius);
			for (float a = 0.1f; a <= (Math::Pi * 2) + 0.1f; a += 0.1f)
			{
				cur = position + Vector3(sinf(a), 0, cosf(a)) * radius;

				glVertex3f(last[Vector3::X], last[Vector3::Y], last[Vector3::Z]);
				glVertex3f(cur[Vector3::X], cur[Vector3::Y], cur[Vector3::Z]);

				last = cur;
			}

			last = position + (Vector3(0, sinf(0), cosf(0)) * radius);
			for (float a = 0.1f; a <= (Math::Pi * 2) + 0.1f; a += 0.1f)
			{
				cur = position + Vector3(0, sinf(a), cosf(a)) * radius;

				glVertex3f(last[Vector3::X], last[Vector3::Y], last[Vector3::Z]);
				glVertex3f(cur[Vector3::X], cur[Vector3::Y], cur[Vector3::Z]);

				last = cur;
			}
		}
		glEnd();

		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	}

#endif

protected:
	float	radius;
	Vector3 position;
};

class AABox
{
public:
	AABox()
	{
	}

	void SetMin(const Vector3& min)
	{
		this->min = min;
	}

	void SetMax(const Vector3& max)
	{
		this->max = max;
	}

	const Vector3& GetMin()
	{
		return min;
	}

	const Vector3& GetMax()
	{
		return max;
	}
/*
	AABox(Vector3 minimum, Vector3& maximum) //: min(minimum)//, max(maximum)
	{
		min = minimum;
	}
*/
protected:
	Vector3 min;
	Vector3 max;
};

class Box : public AABox
{
public:
	Box() : transform(Matrix4::Identity)
	{
	}
/*
	Box(Vector3& min, Vector3& max) : AABox(min, max)
	{
	}*/

#ifndef _SIPHON_EXPORT_

	void Draw(const Vector3& colour = Vector3(1.0, 1.0, 1.0)) const
	{
		Vector3 box[8] = {	Vector3(min[Vector3::X], min[Vector3::Y], min[Vector3::Z]),
							Vector3(min[Vector3::X], min[Vector3::Y], max[Vector3::Z]),
							Vector3(min[Vector3::X], max[Vector3::Y], max[Vector3::Z]),
							Vector3(max[Vector3::X], max[Vector3::Y], max[Vector3::Z]),
							Vector3(max[Vector3::X], max[Vector3::Y], min[Vector3::Z]),
							Vector3(max[Vector3::X], min[Vector3::Y], min[Vector3::Z]),
							Vector3(min[Vector3::X], max[Vector3::Y], min[Vector3::Z]),
							Vector3(max[Vector3::X], min[Vector3::Y], max[Vector3::Z])};

		for (int i = 0; i < 8; ++i)
			box[i] = transform * box[i];

		glColor4f(colour[Vector3::X], colour[Vector3::Y], colour[Vector3::Z], 1.0f);

		glBegin(GL_LINES);
		{
			Vector3 temp1 = box[0];
			Vector3 temp2 = box[1];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = box[1];
			temp2 = box[2];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = box[2];
			temp2 = box[3];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = box[3];
			temp2 = box[4];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = box[4];
			temp2 = box[5];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = box[5];
			temp2 = box[0];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = box[6];
			temp2 = box[0];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = box[6];
			temp2 = box[2];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = box[6];
			temp2 = box[4];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = box[7];
			temp2 = box[3];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = box[7];
			temp2 = box[5];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = box[7];
			temp2 = box[1];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);
		}
		glEnd();

		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	}

#endif

	inline void SetTransform(const Matrix4& transform)
	{
		this->transform = transform;
	}

	inline const Matrix4& GetTransform() const
	{
		return transform;
	}

protected:
	Matrix4 transform;
};

class Frustum
{
public:
	enum Sides
	{
		Top,
		Bottom,
		Left,
		Right,
		Near,
		Far
	};

	enum Points
	{
		TopLeftNear,
		TopLeftFar,
		TopRightNear,
		TopRightFar,

		BottomLeftNear,
		BottomLeftFar,
		BottomRightNear,
		BottomRightFar
	};

	enum State
	{
		Inside,
		Intersect,
		Outside
	};

	inline State CullCheck(const Sphere& cullSphere) const
	{
		if (sphere.Intersect(cullSphere) == -1)
			return Outside;

		float dist;

		// calculate our distances to each of the planes
		for(int i = 0; i < 6; ++i)
		{
			// find the distance to this plane
			dist = -sides[i].DistanceToPoint(cullSphere.GetPosition());

			// if this distance is < -sphere.radius, we are outside
			if (dist < -cullSphere.GetRadius())
				return Outside;

			// else if the distance is between +- radius, then we intersect
			//if (fabsf(dist) < sphere.GetRadius())
			//	return Intersect;
		}

		// otherwise we are fully in view
		return Inside;
	}

	State QuickCullCheck(const Sphere& sphere) const
	{/*
		Vector3 diff = sphere - position;

		// check to see if sphere out of range
		if (diff.Magnitude() > (length + sphere.GetRadius()))
			return Outside;

		// check to see if sphere intersects the cone
		diff.Normalize();
		float angle = diff.Dot(foward);

		if (angle >)
		*/

		// otherwise we are fully in view
		return Inside;
	}

	void SetUpFromProjection(Matrix4& projection)
	{
		sides[Top].ExtractTop(projection);
		sides[Bottom].ExtractBottom(projection);
		sides[Left].ExtractLeft(projection);
		sides[Right].ExtractRight(projection);
		sides[Near].ExtractNear(projection);
		sides[Far].ExtractFar(projection);

		for (int i = 0; i < 6; ++i)
			sides[i].Normalize();

		//find intersection of planes
		points[TopLeftNear] = sides[Top].Intersect3( sides[Left], sides[Near] );
		points[TopRightNear] = sides[Top].Intersect3( sides[Right], sides[Near] );
		points[BottomLeftNear] = sides[Bottom].Intersect3( sides[Left], sides[Near] );
		points[BottomRightNear] = sides[Bottom].Intersect3( sides[Right], sides[Near] );
		points[TopLeftFar] = sides[Top].Intersect3( sides[Left], sides[Far] );
		points[TopRightFar] = sides[Top].Intersect3( sides[Right], sides[Far] );
		points[BottomLeftFar] = sides[Bottom].Intersect3( sides[Left], sides[Far] );
		points[BottomRightFar] = sides[Bottom].Intersect3( sides[Right], sides[Far] );

		// set up sphere
		Vector3 total(0,0,0);
		for (int i = 0; i < 8; ++i)
			total = total + points[i];

		total = total / 8;
		sphere.SetPosition(total);

		for (int i = 0; i < 8; ++i)
		{
			Vector3 diff = total - points[i];
			float dist = diff.Magnitude();
			if (dist > sphere.GetRadius())
				sphere.SetRadius(dist);
		}
		/*
		// extract conical frustum info
		//length = sides[Far].DistanceToPoint(points[TopLeftFar][Vector3::Z]);

		// calculate angle....
		foward = Vector3(0.0f, 0.0f, 1.0f);
		Vector3 endPoint = points[TopLeftFar];
		endPoint.Normalize();
		position = Vector3(0,0,0);

		angle = foward.Dot(endPoint);*/
	}

	void Transform(Matrix4& transform)
	{
		// transform planes
		//for (int i = 0; i < 6; ++i)
		//	sides[i] = transform * sides[i];

		// transform points
		for (int i = 0; i < 8; ++i)
			points[i] = transform * points[i];

		// transform conical cone stuffs
		foward = transform * Vector3(0.0f, 0.0f, 1.0f);
		foward.Normalize();

		position = transform.GetTranslation();
	}

#ifndef _SIPHON_EXPORT_

	void Draw(const Vector3& colour = Vector3(1.0, 1.0, 1.0)) const
	{
		glColor4f(colour[Vector3::X], colour[Vector3::Y], colour[Vector3::Z], 1.0f);

		glBegin(GL_LINES);
		{
			Vector3 temp1 = points[TopLeftNear];
			Vector3 temp2 = points[TopLeftFar];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = points[TopRightNear];
			temp2 = points[TopRightFar];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = points[BottomLeftNear];
			temp2 = points[BottomLeftFar];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = points[BottomRightNear];
			temp2 = points[BottomRightFar];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);


			temp1 = points[TopLeftNear];
			temp2 = points[BottomLeftNear];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = points[TopRightNear];
			temp2 = points[BottomRightNear];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = points[BottomLeftFar];
			temp2 = points[TopLeftFar];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = points[BottomRightFar];
			temp2 = points[TopRightFar];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);


			temp1 = points[TopLeftNear];
			temp2 = points[TopRightNear];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = points[TopRightFar];
			temp2 = points[TopLeftFar];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = points[BottomLeftFar];
			temp2 = points[BottomRightFar];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);

			temp1 = points[BottomRightNear];
			temp2 = points[BottomLeftNear];
			glVertex3f(temp1[Vector3::X], temp1[Vector3::Y], temp1[Vector3::Z]);
			glVertex3f(temp2[Vector3::X], temp2[Vector3::Y], temp2[Vector3::Z]);
		}
		glEnd();

		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	}

#endif

protected:
	Sphere sphere;

	Plane	sides[6];
	Vector3 points[8];

	float angle;
	float length;
	Vector3 foward;
	Vector3 position;

	// should we also keep conical frustrum info for quick culling?
};

}
#endif

