#ifndef _SIPHON_CLIENT_
#define _SIPHON_CLIENT_

#include "Util/Singleton.h"
#include "NetworkBase.h"
#include "RDP/RDPStream.h"

namespace Siphon
{

#define gClient Client::GetInstance()

class ClientBase : public NetworkBase
{
public:
	ClientBase();
	virtual ~ClientBase() { }

	virtual bool Init(unsigned int gameId);
	virtual void Shutdown();

	int	FindLANServers(Array<ServerInfo>& servers);

	bool Connect(const char* addr, int port, int localPort, int flags = 0); // last param = NetworkFlags
	bool StreamValid(SocketStream::Type type);
	bool Send(const void* data, int size, SocketStream::Type type = SocketStream::Reliable);

	// use this to receive data from the raw socket
	// dont use unless you know what your doing!
	int Receive(void* data, unsigned int size, SocketStream::Type type = SocketStream::Reliable);

	// if you use 'Update' use these to get buffered data
	// this is the recommended way
	int GetBufferData(void* data, int size, SocketStream::Type type = SocketStream::Reliable);
	int PeekBufferData(void* data, int size, SocketStream::Type type = SocketStream::Reliable);

	virtual void Update();

	virtual unsigned long GetLocalAddress();

protected:
	// give us a chance to get our client id
	virtual void AcceptConnection(int flags);
	virtual void Disconnect();

	// check broadcast stream for messages
	virtual void CheckBroadcast();

	RDPStream stream;
	RDPChannelHandle reliableHandle;
	RDPChannelHandle unreliableHandle;

	SocketStream reliable;
	SocketStream unreliable;
	SocketStream broadcast; // for broadcasts

	std::string addr;
	int port;
	int flags;

	unsigned int gameId;
};

/**
 * This is done like this so we can extend the client base 
 */
class Client : public ClientBase, public Singleton<Client>
{
public:
	Client() : ClientBase() { }
	virtual ~Client() { }

protected:

};

}

#endif