#ifndef _SIPHON_CAMERA_
#define _SIPHON_CAMERA_

#include "Math3D.h"

namespace Siphon
{
/*
 * OGL camera uses right handed matrices,
 * also not the camera looks down the -z axis
 * so the world matrix will probably not be the matrix you'd expect
 */
class Camera
{
public:
	Camera();

	virtual void Update();

	virtual void FreeCam();

	//void Rotate( float angle, float x, float y, float z );
	void LookAt();
	void LookAtM();

	Vector3& GetRotation();

	void SetPosition(const Vector3& position);
	void SetPosition(float x, float y, float z);

	void SetFreeCam( bool bFreeCam );

	void SetLookAt(const Vector3& view, const Vector3& up, const Vector3& right);
	void SetLookAt(const Matrix4& worldMatrix);
	void SetLookAt(const Vector3& position, const Vector3& target);
	void SetLookAt(const Camera& cam);

	virtual void Transform();

	// the cameras world matrix (where it is in the world)
	void SetWorld(Matrix4& world);
	const Matrix4& GetWorld() const;

	// the cameras view matrix This should be SetViewMatrix and GetViewMatrix
	void SetTransform(Matrix4& transform);
	Matrix4& GetTransform();

	Matrix4& GetProjection();
	void SetProjection(const Matrix4& projection);

	//Vector3 Plane3Intersect( Plane& p1, Plane& p2, Plane& p3 );

	/**
	 * get the transformed version of the cameras
	 * frustrum
	 */
	//BOUNDS GetTransformedBounds();

	inline const Frustum& GetFrustum();

	float GetSpeed() { return camSpeed; }

	void SetSpeed(float speed);

	void SetMouseSensitivity(float sensitivity) { mouseSensitivity = sensitivity; }
	float GetMouseSensitivity() { return mouseSensitivity; }

	void UpdateFrustrum();

	Vector3 GetView() { return world.GetColumn(2); }//view; }
	Vector3 GetUp() { return world.GetColumn(1); }//up; }
	Vector3 GetRight() { return world.GetColumn(0); }//right; }
	Vector3 GetPosition() { return world.GetTranslation(); }

	void SetOrthographic(int width = 800, int height = 600);
	void SetPerspective(float fovy = 90.0f, float aspect = 1.33f, float zfar = 1000.0f, float znear = 0.01f);

protected:
	Vector3		rotation;
	//Vector3 position;
	//Vector3 view;
	//Vector3 up;
	//Vector3 right;

	Frustum		frustum;

	Matrix4		projection;
	Matrix4		world;
	Matrix4		transform;

	bool	bFreeCam;
	float	camSpeed;
	float	mouseSensitivity;

	float	farPlane;
	float	nearPlane;
	float	fov;
};

inline const Frustum& Camera::GetFrustum()
{
	return frustum;
}

}
#endif

