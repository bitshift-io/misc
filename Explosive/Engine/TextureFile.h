#ifndef _SIPHON_TEXTUREFILE_
#define _SIPHON_TEXTUREFILE_

namespace Siphon
{

class TextureFile
{
public:
	virtual bool			Load(const char* file) = 0;

	virtual unsigned char*	GetPixels() = 0;
	virtual int				GetWidth() = 0;
	virtual int				GetHeight() = 0;
	virtual int				GetBPP() = 0;
};

}

#endif
