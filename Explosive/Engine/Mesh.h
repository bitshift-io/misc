//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_MESH_
#define _SIPHON_MESH_

#include "Array.h"
#include "MaterialDefs.h"
#include "Math3D.h"
#include "Material.h"

namespace Siphon
{

class Skeleton;
class SkeletonAnimation;

enum MeshFlags
{
	MNone	= 0x0,
	Dynamic = 0x1 << 1,
	Static	= 0x1 << 2,
	Decal	= 0x1 << 3,
	NoShadow = 0x1 << 4,
};


/**
 * the core of a mesh,
 * vertices indices ,uv's
 * these are fixed,
 * this is unique!
 */
class VertexBuffer
{
	friend class Mesh;

public:

	VertexBuffer();
	~VertexBuffer();

	/**
	 * Compile this mesh's VBO's
	 */
	bool Build();
	void BuildBounds();

	void operator=(const VertexBuffer& rhs);

	/**
	 * renders a part of this mesh,
	 */
	unsigned int Render(unsigned int startIndex = 0, unsigned int numIndices = 0, VertexFormat format = VertexAll);

	/**
	 * Render normals, binormals and tangents
	 */
	void DebugRender(float scale = 0.001f);

	/**
	 * use this is we need to supply extra vertex attributes. ie. only for shaders
	 */
	//unsigned int Render(Shader::Param* weight, Shader::Param* bone, Shader::Param* binormal, Shader::Param* tangent, unsigned int startIndex = 0, unsigned int numIndices = 0, RenderFlags flags = None);

	void SetRenderType(unsigned int renderType) { this->renderType = renderType; }
	void SetFlags(unsigned int flags);
	unsigned int GetFlags();

	Array<Colour>		colours;
	Array<Vector3>		positions;
	Array<Vector3>		normals;
	Array<UVCoord>		uvCoords;
	Array<UVCoord>		uvCoords2;
	Array<unsigned int>	indices;
	Array<Vector4>		weights;
	Array<Vector4>		bones;
	Array<Vector3>		binormals;
	Array<Vector3>		tangents;

	bool Load(File& file);
	bool Save(File& file);

	static void SetShaderHandles(GLint weight, GLint bone, GLint binormal, GLint tangent);

//protected:
	GLuint			vboPositions;
	GLuint			vboTexCoords;
	GLuint			vboTexCoords2;
	GLuint			vboNormals;
	GLuint			vboColours;
	GLuint			vboIndices;
	GLuint			vboBones;
	GLuint			vboWeights;
	GLuint			vboBinormals;
	GLuint			vboTangents;

	unsigned int			flags;
	int						renderType;

	// handles for binding custom vertex attributes
	static GLint weightHandle;
	static GLint boneHandle;
	static GLint binormalHandle;
	static GLint tangentHandle;

	Sphere		boundSphere;
	std::string name;
};

/**
 * The extra info,
 * vertex buffer, with material info
 * and transform,
 * this means the same model can have
 * different materials
 * this is not unique!
 */
class Mesh
{
	friend class Renderer;

public:

	class Callback
	{
	public:
		virtual void PreRender(Mesh* mesh, Material* material, int materialIdx, VertexFormat format) = 0;
	};

	//our table of material to index count
	struct MaterialTable
	{
		unsigned int	startIndex;
		unsigned int	numIndices;
		unsigned int	numPrimitives;
		Material*		material;
	};

	struct MeshMat
	{
		VertexBuffer*			vertexBuffer;
		Array<MaterialTable>	material;
	};

	Mesh();

	// constructore if making your own mesh
	Mesh(VertexBuffer* vb, MeshMat& meshMat);

	static Mesh* LoadMesh(const char* file);
	static void ReleaseMesh(Mesh* mesh, bool removeFromMem = false); // safely release the mesh
	static void DeleteResources();
	static void SetBasePath(const char* path);

	virtual unsigned int Render(Material* material, int materialIdx, VertexFormat format = VertexAll);

	inline MeshMat& GetMeshMat();

	// uses the Set shader to get a pointer to the vertex attributes.
	//void SetShaderVertexAttributes(const char* binormalAttrib, const char* tangentAttrib);

	const char* GetName();

	inline const Sphere& GetBoundSphere();
	inline const void SetBoundSphere(Sphere& sphere) { bounds = sphere; } // temporary

	inline void SetTransform(const Matrix4& transform);
	inline const Matrix4& GetTransform();

	Frustum::State GetCullState();
	inline void SetCullState(Frustum::State state);
	inline bool IsVisible();

	virtual bool Load(const char* file);
	virtual bool Save(const char* file);

	// delete the materials and vertexbuffer
	void ReleaseInternal();

	// software skinning test
	unsigned int SoftwareSkin(Matrix4* boneMatrices);

	void SetSkeletonAnimation(SkeletonAnimation* skeletonAnimation);
	SkeletonAnimation* GetSkeletonAnimation();

	void Update(); // update software skinned mesh

	void SetCallback(Callback* callback);

protected:

	virtual Mesh* Clone();

	MeshMat		meshMats;
	Matrix4		transform;

	Frustum::State	cullState;
	Sphere			bounds;

	// for skinning
	SkeletonAnimation*	skeletonAnimation;
	VertexBuffer*		skinRenderBuffer;
	Matrix4*			boneMatrices;

	// callbacks
	Callback*			callbacks;

	static Array<Mesh*> resources;
	static std::string	basePath;
};

inline void Mesh::SetCullState(Frustum::State state)
{
	this->cullState = state;
}

inline Frustum::State Mesh::GetCullState()
{
	return cullState;
}

inline bool Mesh::IsVisible()
{
	return cullState == Frustum::Outside ? false : true;
}

inline const Matrix4& Mesh::GetTransform()
{
	return transform;
}

inline void Mesh::SetTransform(const Matrix4& transform)
{
	this->transform = transform;
	bounds.SetPosition(transform.GetTranslation());
}

inline const Sphere& Mesh::GetBoundSphere()
{
	return bounds;
}

inline Mesh::MeshMat& Mesh::GetMeshMat()
{
	return meshMats;
}

}
#endif
