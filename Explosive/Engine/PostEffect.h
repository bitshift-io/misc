//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_POSTEFFECT_
#define _SIPHON_POSTEFFECT_

#include "Material.h"

namespace Siphon
{

/**
 * Post effect interface
 */
class PostEffect
{
public:
	PostEffect() : enabled(true) {}

	virtual void PreRender() = 0;
	virtual void PostRender() = 0;

	virtual bool Enabled() { return enabled; }
	virtual void SetEnable(bool enabled) { this->enabled = enabled; }

protected:
	bool enabled;
};

class MatQuad : public PostEffect
{
public:
	MatQuad(Material* material);
	virtual void PostRender();

protected:
	Material* material;
};

class Bloom : public PostEffect
{
public:
	Bloom();
	~Bloom();

	virtual void PreRender() {}
	virtual void PostRender();

	// set the number of blending levels
	virtual void SetLevels(int mipClose, int mipFar);

	// sets how much the bloom is blended with the frame buffer
	// it also affects the blending between each level
	virtual void SetBlendRatio(float blendRatio);

protected:
	GLuint screenTexture;
	int textureSize;
	int mipClose;
	int mipFar;
	float blendRatio;
};

/**
 * this requires depth information,
 * so must be applied first,
 * alternatively, we could have a few statis in our PstEffect class
 * that automatically recive depth info and copys of screen....
 */
/*
class DepthOfField : public PostEffect
{
public:
	DepthOfField();
	~DepthOfField();

	virtual void PreRender() {}
	virtual void PostRender();

protected:
	GLuint screenTexture;
	GLuint depthTexture;
	int textureSize;
	Shader dof;

	Shader::Param* paramNear;
	Shader::Param* paramFar;
};*/

/**
 * wireframe
 */
class WireFrame : public PostEffect
{
public:
	virtual void PreRender() { glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); }
	virtual void PostRender() { glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); }

protected:
};

/**
 * fog
 */
class Fog : public PostEffect
{
public:
	enum FogMode
	{
		FogLinear,
		FogExp,
		FogExp2
	};

	Fog();

	virtual void PreRender();
	virtual void PostRender();

	void SetFogMode(FogMode mode) { this->mode = mode; }
	void SetDensity(float density) { this->density = density; }
	void SetStart(float start) { this->start = start; }
	void SetEnd(float end) { this->end = end; }

protected:
	FogMode mode;
	float density;
	float start;
	float end;
	Colour colour;
};

}
#endif
