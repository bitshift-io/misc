namespace std
{
%callback(1) wendl;
%callback(1) wends;
%callback(1) wflush;
}

%include <std/std_wiostream.i>
