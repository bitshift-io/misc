import sys
import std_containers


cube = (((1, 2), (3, 4)), ((5, 6), (7, 8)))


if cube != std_containers.cident(cube):
  raise RuntimeError, "bad cident"


p = (1,2)
if p != std_containers.pident(p):
  raise RuntimeError, "bad pident"

v = (1,2,3,4,5,6)

if v != std_containers.vident(v):
  raise RuntimeError, "bad pident"


if v != std_containers.videntu(v):
  raise RuntimeError, "bad videntu"

vu = std_containers.vector_ui(v)
if vu[2] != std_containers.videntu(vu)[2]:
  raise RuntimeError, "bad videntu"
  

if v[0:3][1] != vu[0:3][1]:
  print v[0:3][1], vu[0:3][1]
  raise RuntimeError, "bad getslice"
  

m = ((1,2,3),(2,3),(3,4))
if m != std_containers.midenti(m):
  raise RuntimeError, "bad getslice"

mb = ((1,0,1),(1,1),(1,1))
if mb != std_containers.midentb(mb):
  raise RuntimeError, "bad getslice"


mi = std_containers.imatrix(m)
mc = std_containers.cmatrix(m)
if mi[0][1] != mc[0][1]:
  raise RuntimeError, "bad matrix"


map ={}
map['hello'] = 1
map['hi'] = 2
map['3'] = 2

if map != std_containers.mapident(map):
  raise RuntimeError, "bad map"


mapc ={}
c1 = std_containers.C()
c2 = std_containers.C()
mapc[1] = c1.this
mapc[2] = c2

std_containers.mapidentc(mapc)


vi = std_containers.vector_i((2,2,3,4))


v = (1,2)
v1 = std_containers.v_inout(vi)
vi[1], v1[1]

v1,v2 = ((1,2),(3,4))
v1,v2 = std_containers.v_inout2(v1,v2)

a1 = std_containers.A(1)
a2 = std_containers.A(2)

p1 = (1,a1)
p2 = (2,a2)
v = (p1,p2)
v2= std_containers.pia_vident(v)

v2[0][1].a
v2[1][1].a

v3 = std_containers.vector_piA(v2)

v3[0][1].a
v3[1][1].a


