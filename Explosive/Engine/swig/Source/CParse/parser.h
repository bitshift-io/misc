/* A Bison parser, made by GNU Bison 1.875.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     ID = 258,
     HBLOCK = 259,
     POUND = 260,
     STRING = 261,
     INCLUDE = 262,
     IMPORT = 263,
     INSERT = 264,
     CHARCONST = 265,
     NUM_INT = 266,
     NUM_FLOAT = 267,
     NUM_UNSIGNED = 268,
     NUM_LONG = 269,
     NUM_ULONG = 270,
     NUM_LONGLONG = 271,
     NUM_ULONGLONG = 272,
     TYPEDEF = 273,
     TYPE_INT = 274,
     TYPE_UNSIGNED = 275,
     TYPE_SHORT = 276,
     TYPE_LONG = 277,
     TYPE_FLOAT = 278,
     TYPE_DOUBLE = 279,
     TYPE_CHAR = 280,
     TYPE_VOID = 281,
     TYPE_SIGNED = 282,
     TYPE_BOOL = 283,
     TYPE_COMPLEX = 284,
     TYPE_TYPEDEF = 285,
     TYPE_RAW = 286,
     LPAREN = 287,
     RPAREN = 288,
     COMMA = 289,
     SEMI = 290,
     EXTERN = 291,
     INIT = 292,
     LBRACE = 293,
     RBRACE = 294,
     PERIOD = 295,
     CONST_QUAL = 296,
     VOLATILE = 297,
     REGISTER = 298,
     STRUCT = 299,
     UNION = 300,
     EQUAL = 301,
     SIZEOF = 302,
     MODULE = 303,
     LBRACKET = 304,
     RBRACKET = 305,
     ILLEGAL = 306,
     CONSTANT = 307,
     NAME = 308,
     RENAME = 309,
     NAMEWARN = 310,
     EXTEND = 311,
     PRAGMA = 312,
     FEATURE = 313,
     VARARGS = 314,
     ENUM = 315,
     CLASS = 316,
     TYPENAME = 317,
     PRIVATE = 318,
     PUBLIC = 319,
     PROTECTED = 320,
     COLON = 321,
     STATIC = 322,
     VIRTUAL = 323,
     FRIEND = 324,
     THROW = 325,
     CATCH = 326,
     USING = 327,
     NAMESPACE = 328,
     NATIVE = 329,
     INLINE = 330,
     TYPEMAP = 331,
     EXCEPT = 332,
     ECHO = 333,
     APPLY = 334,
     CLEAR = 335,
     SWIGTEMPLATE = 336,
     FRAGMENT = 337,
     WARN = 338,
     LESSTHAN = 339,
     GREATERTHAN = 340,
     MODULO = 341,
     DELETE_KW = 342,
     TYPES = 343,
     PARMS = 344,
     NONID = 345,
     DSTAR = 346,
     DCNOT = 347,
     TEMPLATE = 348,
     OPERATOR = 349,
     COPERATOR = 350,
     PARSETYPE = 351,
     PARSEPARM = 352,
     CAST = 353,
     LOR = 354,
     LAND = 355,
     OR = 356,
     XOR = 357,
     AND = 358,
     RSHIFT = 359,
     LSHIFT = 360,
     MINUS = 361,
     PLUS = 362,
     SLASH = 363,
     STAR = 364,
     LNOT = 365,
     NOT = 366,
     UMINUS = 367,
     DCOLON = 368
   };
#endif
#define ID 258
#define HBLOCK 259
#define POUND 260
#define STRING 261
#define INCLUDE 262
#define IMPORT 263
#define INSERT 264
#define CHARCONST 265
#define NUM_INT 266
#define NUM_FLOAT 267
#define NUM_UNSIGNED 268
#define NUM_LONG 269
#define NUM_ULONG 270
#define NUM_LONGLONG 271
#define NUM_ULONGLONG 272
#define TYPEDEF 273
#define TYPE_INT 274
#define TYPE_UNSIGNED 275
#define TYPE_SHORT 276
#define TYPE_LONG 277
#define TYPE_FLOAT 278
#define TYPE_DOUBLE 279
#define TYPE_CHAR 280
#define TYPE_VOID 281
#define TYPE_SIGNED 282
#define TYPE_BOOL 283
#define TYPE_COMPLEX 284
#define TYPE_TYPEDEF 285
#define TYPE_RAW 286
#define LPAREN 287
#define RPAREN 288
#define COMMA 289
#define SEMI 290
#define EXTERN 291
#define INIT 292
#define LBRACE 293
#define RBRACE 294
#define PERIOD 295
#define CONST_QUAL 296
#define VOLATILE 297
#define REGISTER 298
#define STRUCT 299
#define UNION 300
#define EQUAL 301
#define SIZEOF 302
#define MODULE 303
#define LBRACKET 304
#define RBRACKET 305
#define ILLEGAL 306
#define CONSTANT 307
#define NAME 308
#define RENAME 309
#define NAMEWARN 310
#define EXTEND 311
#define PRAGMA 312
#define FEATURE 313
#define VARARGS 314
#define ENUM 315
#define CLASS 316
#define TYPENAME 317
#define PRIVATE 318
#define PUBLIC 319
#define PROTECTED 320
#define COLON 321
#define STATIC 322
#define VIRTUAL 323
#define FRIEND 324
#define THROW 325
#define CATCH 326
#define USING 327
#define NAMESPACE 328
#define NATIVE 329
#define INLINE 330
#define TYPEMAP 331
#define EXCEPT 332
#define ECHO 333
#define APPLY 334
#define CLEAR 335
#define SWIGTEMPLATE 336
#define FRAGMENT 337
#define WARN 338
#define LESSTHAN 339
#define GREATERTHAN 340
#define MODULO 341
#define DELETE_KW 342
#define TYPES 343
#define PARMS 344
#define NONID 345
#define DSTAR 346
#define DCNOT 347
#define TEMPLATE 348
#define OPERATOR 349
#define COPERATOR 350
#define PARSETYPE 351
#define PARSEPARM 352
#define CAST 353
#define LOR 354
#define LAND 355
#define OR 356
#define XOR 357
#define AND 358
#define RSHIFT 359
#define LSHIFT 360
#define MINUS 361
#define PLUS 362
#define SLASH 363
#define STAR 364
#define LNOT 365
#define NOT 366
#define UMINUS 367
#define DCOLON 368




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 1240 "parser.y"
typedef union YYSTYPE {
  char  *id;
  List  *bases;
  struct Define {
    String *val;
    String *rawval;
    int     type;
    String *qualifier;
    String *bitfield;
    Parm   *throws;
    String *throw;
  } dtype;
  struct {
    char *type;
    char *filename;
    int   line;
  } loc;
  struct {
    char      *id;
    SwigType  *type;
    String    *defarg;
    ParmList  *parms;
    short      have_parms;
    ParmList  *throws;
    String    *throw;
  } decl;
  Parm         *tparms;
  struct {
    String     *op;
    Hash       *kwargs;
  } tmap;
  struct {
    String     *type;
    String     *us;
  } ptype;
  SwigType     *type;
  String       *str;
  Parm         *p;
  ParmList     *pl;
  int           ivalue;
  Node         *node;
} YYSTYPE;
/* Line 1249 of yacc.c.  */
#line 305 "y.tab.h"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;



