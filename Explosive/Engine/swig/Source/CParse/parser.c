/* A Bison parser, made by GNU Bison 1.875.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     ID = 258,
     HBLOCK = 259,
     POUND = 260,
     STRING = 261,
     INCLUDE = 262,
     IMPORT = 263,
     INSERT = 264,
     CHARCONST = 265,
     NUM_INT = 266,
     NUM_FLOAT = 267,
     NUM_UNSIGNED = 268,
     NUM_LONG = 269,
     NUM_ULONG = 270,
     NUM_LONGLONG = 271,
     NUM_ULONGLONG = 272,
     TYPEDEF = 273,
     TYPE_INT = 274,
     TYPE_UNSIGNED = 275,
     TYPE_SHORT = 276,
     TYPE_LONG = 277,
     TYPE_FLOAT = 278,
     TYPE_DOUBLE = 279,
     TYPE_CHAR = 280,
     TYPE_VOID = 281,
     TYPE_SIGNED = 282,
     TYPE_BOOL = 283,
     TYPE_COMPLEX = 284,
     TYPE_TYPEDEF = 285,
     TYPE_RAW = 286,
     LPAREN = 287,
     RPAREN = 288,
     COMMA = 289,
     SEMI = 290,
     EXTERN = 291,
     INIT = 292,
     LBRACE = 293,
     RBRACE = 294,
     PERIOD = 295,
     CONST_QUAL = 296,
     VOLATILE = 297,
     REGISTER = 298,
     STRUCT = 299,
     UNION = 300,
     EQUAL = 301,
     SIZEOF = 302,
     MODULE = 303,
     LBRACKET = 304,
     RBRACKET = 305,
     ILLEGAL = 306,
     CONSTANT = 307,
     NAME = 308,
     RENAME = 309,
     NAMEWARN = 310,
     EXTEND = 311,
     PRAGMA = 312,
     FEATURE = 313,
     VARARGS = 314,
     ENUM = 315,
     CLASS = 316,
     TYPENAME = 317,
     PRIVATE = 318,
     PUBLIC = 319,
     PROTECTED = 320,
     COLON = 321,
     STATIC = 322,
     VIRTUAL = 323,
     FRIEND = 324,
     THROW = 325,
     CATCH = 326,
     USING = 327,
     NAMESPACE = 328,
     NATIVE = 329,
     INLINE = 330,
     TYPEMAP = 331,
     EXCEPT = 332,
     ECHO = 333,
     APPLY = 334,
     CLEAR = 335,
     SWIGTEMPLATE = 336,
     FRAGMENT = 337,
     WARN = 338,
     LESSTHAN = 339,
     GREATERTHAN = 340,
     MODULO = 341,
     DELETE_KW = 342,
     TYPES = 343,
     PARMS = 344,
     NONID = 345,
     DSTAR = 346,
     DCNOT = 347,
     TEMPLATE = 348,
     OPERATOR = 349,
     COPERATOR = 350,
     PARSETYPE = 351,
     PARSEPARM = 352,
     CAST = 353,
     LOR = 354,
     LAND = 355,
     OR = 356,
     XOR = 357,
     AND = 358,
     RSHIFT = 359,
     LSHIFT = 360,
     MINUS = 361,
     PLUS = 362,
     SLASH = 363,
     STAR = 364,
     LNOT = 365,
     NOT = 366,
     UMINUS = 367,
     DCOLON = 368
   };
#endif
#define ID 258
#define HBLOCK 259
#define POUND 260
#define STRING 261
#define INCLUDE 262
#define IMPORT 263
#define INSERT 264
#define CHARCONST 265
#define NUM_INT 266
#define NUM_FLOAT 267
#define NUM_UNSIGNED 268
#define NUM_LONG 269
#define NUM_ULONG 270
#define NUM_LONGLONG 271
#define NUM_ULONGLONG 272
#define TYPEDEF 273
#define TYPE_INT 274
#define TYPE_UNSIGNED 275
#define TYPE_SHORT 276
#define TYPE_LONG 277
#define TYPE_FLOAT 278
#define TYPE_DOUBLE 279
#define TYPE_CHAR 280
#define TYPE_VOID 281
#define TYPE_SIGNED 282
#define TYPE_BOOL 283
#define TYPE_COMPLEX 284
#define TYPE_TYPEDEF 285
#define TYPE_RAW 286
#define LPAREN 287
#define RPAREN 288
#define COMMA 289
#define SEMI 290
#define EXTERN 291
#define INIT 292
#define LBRACE 293
#define RBRACE 294
#define PERIOD 295
#define CONST_QUAL 296
#define VOLATILE 297
#define REGISTER 298
#define STRUCT 299
#define UNION 300
#define EQUAL 301
#define SIZEOF 302
#define MODULE 303
#define LBRACKET 304
#define RBRACKET 305
#define ILLEGAL 306
#define CONSTANT 307
#define NAME 308
#define RENAME 309
#define NAMEWARN 310
#define EXTEND 311
#define PRAGMA 312
#define FEATURE 313
#define VARARGS 314
#define ENUM 315
#define CLASS 316
#define TYPENAME 317
#define PRIVATE 318
#define PUBLIC 319
#define PROTECTED 320
#define COLON 321
#define STATIC 322
#define VIRTUAL 323
#define FRIEND 324
#define THROW 325
#define CATCH 326
#define USING 327
#define NAMESPACE 328
#define NATIVE 329
#define INLINE 330
#define TYPEMAP 331
#define EXCEPT 332
#define ECHO 333
#define APPLY 334
#define CLEAR 335
#define SWIGTEMPLATE 336
#define FRAGMENT 337
#define WARN 338
#define LESSTHAN 339
#define GREATERTHAN 340
#define MODULO 341
#define DELETE_KW 342
#define TYPES 343
#define PARMS 344
#define NONID 345
#define DSTAR 346
#define DCNOT 347
#define TEMPLATE 348
#define OPERATOR 349
#define COPERATOR 350
#define PARSETYPE 351
#define PARSEPARM 352
#define CAST 353
#define LOR 354
#define LAND 355
#define OR 356
#define XOR 357
#define AND 358
#define RSHIFT 359
#define LSHIFT 360
#define MINUS 361
#define PLUS 362
#define SLASH 363
#define STAR 364
#define LNOT 365
#define NOT 366
#define UMINUS 367
#define DCOLON 368




/* Copy the first part of user declarations.  */
#line 1 "parser.y"

/* -----------------------------------------------------------------------------
 * parser.y
 *
 *     YACC parser for SWIG.   The grammar is a somewhat broken subset of C/C++.
 *     This file is a bit of a mess and probably needs to be rewritten at
 *     some point.  Beware.
 *
 * Author(s) : David Beazley (beazley@cs.uchicago.edu)
 *
 * Copyright (C) 1998-2001.  The University of Chicago
 * Copyright (C) 1995-1998.  The University of Utah and The Regents of the
 *                           University of California.
 *
 * See the file LICENSE for information on usage and redistribution.
 * ----------------------------------------------------------------------------- */

#define yylex yylex

char cvsroot_parser_y[] = "$Header: /cvsroot/swig/SWIG/Source/CParse/parser.y,v 1.112 2005/04/01 19:45:21 wsfulton Exp $";

#include "swig.h"
#include "cparse.h"
#include "preprocessor.h"
#include <ctype.h>

/* We do this for portability */
#undef alloca
#define alloca malloc

/* -----------------------------------------------------------------------------
 *                               Externals
 * ----------------------------------------------------------------------------- */

int  yyparse();

/* NEW Variables */

static Node    *top = 0;      /* Top of the generated parse tree */
static int      unnamed = 0;  /* Unnamed datatype counter */
static Hash    *extendhash = 0;     /* Hash table of added methods */
static Hash    *classes = 0;        /* Hash table of classes */
static Symtab  *prev_symtab = 0;
static Node    *current_class = 0;
       String  *ModuleName = 0;
static Node    *module_node = 0;
static String  *Classprefix = 0;  
static String  *Namespaceprefix = 0;
static int      inclass = 0;
static char    *last_cpptype = 0;
static int      inherit_list = 0;
static Parm    *template_parameters = 0;
static int      extendmode   = 0;
static int      dirprot_mode  = 0;
static int      compact_default_args = 0;
static int      template_reduce = 0;
static int      cparse_externc = 0;

/* -----------------------------------------------------------------------------
 *                            Assist Functions
 * ----------------------------------------------------------------------------- */

#define SWIG_WARN_NODE_BEGIN(Node) \
 { \
  String *wrnfilter = Node ? Getattr(Node,"feature:warnfilter") : 0; \
  if (wrnfilter) Swig_warnfilter(wrnfilter,1) 

#define SWIG_WARN_NODE_END(Node) \
  if (wrnfilter) Swig_warnfilter(wrnfilter,0); \
 }

 
/* Called by the parser (yyparse) when an error is found.*/
static void yyerror (const char *e) {
  (void)e;
}

static Node *new_node(const String_or_char *tag) {
  Node *n = NewHash();
  set_nodeType(n,tag);
  Setfile(n,cparse_file);
  Setline(n,cparse_line);
  return n;
}

/* Copies a node.  Does not copy tree links or symbol table data (except for
   sym:name) */

static Node *copy_node(Node *n) {
  Node *nn;
  String *key;
  Iterator k;
  nn = NewHash();
  Setfile(nn,Getfile(n));
  Setline(nn,Getline(n));
  for (k = First(n); k.key; k = Next(k)) {
    key = k.key;
    if ((Strcmp(key,"nextSibling") == 0) ||
	(Strcmp(key,"previousSibling") == 0) ||
	(Strcmp(key,"parentNode") == 0) ||
	(Strcmp(key,"lastChild") == 0)) {
      continue;
    }
    if (Strncmp(key,"csym:",5) == 0) continue;
    /* We do copy sym:name.  For templates */
    if ((Strcmp(key,"sym:name") == 0) || 
	(Strcmp(key,"sym:weak") == 0) ||
	(Strcmp(key,"sym:typename") == 0)) {
      Setattr(nn,key, Copy(k.item));
      continue;
    }
    if (Strcmp(key,"sym:symtab") == 0) {
      Setattr(nn,"sym:needs_symtab", "1");
    }
    /* We don't copy any other symbol table attributes */
    if (Strncmp(key,"sym:",4) == 0) {
      continue;
    }
    /* If children.  We copy them recursively using this function */
    if (Strcmp(key,"firstChild") == 0) {
      /* Copy children */
      Node *cn = k.item;
      while (cn) {
	appendChild(nn,copy_node(cn));
	cn = nextSibling(cn);
      }
      continue;
    }
    /* We don't copy the symbol table.  But we drop an attribute 
       requires_symtab so that functions know it needs to be built */

    if (Strcmp(key,"symtab") == 0) {
      /* Node defined a symbol table. */
      Setattr(nn,"requires_symtab","1");
      continue;
    }
    /* Can't copy nodes */
    if (Strcmp(key,"node") == 0) {
      continue;
    }
    if ((Strcmp(key,"parms") == 0) || (Strcmp(key,"pattern") == 0) || (Strcmp(key,"throws") == 0)
	|| (Strcmp(key,"kwargs") == 0)) {
      ParmList *pl = CopyParmList(k.item);
      Setattr(nn,key,pl);
      Delete(pl);
      continue;
    }
    /* Looks okay.  Just copy the data using Copy */
    Setattr(nn, key, Copy(k.item));
  }
  return nn;
}

/* -----------------------------------------------------------------------------
 *                              Variables
 * ----------------------------------------------------------------------------- */

static char  *typemap_lang = 0;    /* Current language setting */

static int cplus_mode  = 0;
static String  *class_rename = 0;

/* C++ modes */

#define  CPLUS_PUBLIC    1
#define  CPLUS_PRIVATE   2
#define  CPLUS_PROTECTED 3

void SWIG_typemap_lang(const char *tm_lang) {
  typemap_lang = Swig_copy_string(tm_lang);
}

void SWIG_cparse_set_compact_default_args(int defargs) {
  compact_default_args = defargs;
}

void SWIG_cparse_template_reduce(int treduce) {
  template_reduce = treduce;
}

/* -----------------------------------------------------------------------------
 *                           Assist functions
 * ----------------------------------------------------------------------------- */

/* Perform type-promotion for binary operators */
static int promote(int t1, int t2) {
  return t1 > t2 ? t1 : t2;
}

static String *yyrename = 0;

/* Forward renaming operator */
static Hash   *rename_hash = 0;
static Hash   *namewarn_hash = 0;
static Hash   *features_hash = 0;

Hash *Swig_cparse_features() {
  if (!features_hash) features_hash = NewHash();
  return features_hash;
}

static String *feature_identifier_fix(String *s) {
  if (SwigType_istemplate(s)) {
    String *tp, *ts, *ta, *tq;
    tp = SwigType_templateprefix(s);
    ts = SwigType_templatesuffix(s);
    ta = SwigType_templateargs(s);
    tq = Swig_symbol_type_qualify(ta,0);
    Append(tp,tq);
    Append(tp,ts);
    Delete(ts);
    Delete(ta);
    Delete(tq);
    return tp;
  } else {
    return NewString(s);
  }
}

static void single_rename_add(const char *name, SwigType *decl, const char *newname) {
  String *nname;
  if (!rename_hash) rename_hash = NewHash();
  if (Namespaceprefix) {
    nname = NewStringf("%s::%s",Namespaceprefix, name);
  } else {
    nname = NewString(name);
  }
  Swig_name_object_set(rename_hash,nname,decl,NewString(newname));
  Delete(nname);
}

/* Add a new rename. Works much like new_feature including default argument handling. */
static void rename_add(const char *name, SwigType *decl, const char *newname, ParmList *declaratorparms) {

  ParmList *declparms = declaratorparms;

  /* Add the name */
  single_rename_add(name, decl, newname);

  /* Add extra names if there are default parameters in the parameter list */
  if (decl) {
    int constqualifier = SwigType_isconst(decl);
    while (declparms) {
      if (ParmList_has_defaultargs(declparms)) {

        /* Create a parameter list for the new rename by copying all
           but the last (defaulted) parameter */
        ParmList* newparms = ParmList_copy_all_except_last_parm(declparms);

        /* Create new declaration - with the last parameter removed */
        SwigType *newdecl = Copy(decl);
        Delete(SwigType_pop_function(newdecl)); /* remove the old parameter list from newdecl */
        SwigType_add_function(newdecl,newparms);
        if (constqualifier)
          SwigType_add_qualifier(newdecl,"const");

        single_rename_add(name, newdecl, newname);
        declparms = newparms;
        Delete(newdecl);
      } else {
        declparms = 0;
      }
    }
  }
}

static void namewarn_add(const char *name, SwigType *decl, const char *warning) {
  String *nname;
  if (!namewarn_hash) namewarn_hash = NewHash();
  if (Namespaceprefix) {
    nname = NewStringf("%s::%s",Namespaceprefix, name);
  } else {
    nname = NewString(name);
  }

  Swig_name_object_set(namewarn_hash,nname,decl,NewString(warning));
  Delete(nname);
}

static void rename_inherit(String *base, String *derived) {
  /*  Printf(stdout,"base = '%s', derived = '%s'\n", base, derived); */
  Swig_name_object_inherit(rename_hash,base,derived);
  Swig_name_object_inherit(namewarn_hash,base,derived);
  Swig_name_object_inherit(features_hash,base,derived);
}

/* Generate the symbol table name for an object */
/* This is a bit of a mess. Need to clean up */
static String *add_oldname = 0;

static String *make_name(String *name,SwigType *decl) {
  String *rn = 0;
  String *origname = name;
  int     destructor = 0;

  if (name && (*(Char(name)) == '~')) {
    destructor = 1;
  }
  if (yyrename) {
    String *s = yyrename;
    yyrename = 0;
    if (destructor) {
      Insert(s,0,"~");
    }
    return s;
  }

  if (!name) return 0;
  /* Check to see if the name is in the hash */
  if (!rename_hash) {
    if (add_oldname) return Copy(add_oldname);
    return origname;
  }
  rn = Swig_name_object_get(rename_hash, Namespaceprefix, name, decl);
  if (!rn) {
    if (add_oldname) return Copy(add_oldname);
    return name;
  }
  if (destructor) {
    if (Strcmp(rn,"$ignore") != 0) {
      String *s = NewStringf("~%s", rn);
      return s;
    }
  }
  return Copy(rn);
}

/* Generate an unnamed identifier */
static String *make_unnamed() {
  unnamed++;
  return NewStringf("$unnamed%d$",unnamed);
}

/* Return the node name when it requires to emit a name warning */
static String *name_warning(Node *n,String *name,SwigType *decl) {
  /* Return in the obvious cases */
  if (!namewarn_hash || !name || !need_name_warning(n)) {
    return 0;
  } else {
    String *access = Getattr(n,"access");	
    int is_public = !access || (Strcmp(access,"public") == 0);
    if (!is_public && !need_protected(n,dirprot_mode)) {
      return 0;
    }
  }
  
  /* Check to see if the name is in the hash */
  return Swig_name_object_get(namewarn_hash,Namespaceprefix,name,decl);
}

/* Return if the node is a friend declaration */
static int is_friend(Node *n) {
 return Cmp(Getattr(n,"storage"),"friend") == 0;
}


/* Add declaration list to symbol table */
static int  add_only_one = 0;

static void add_symbols(Node *n) {
  String *decl;
  String *wrn = 0;
  if (inclass && n) {
    cparse_normalize_void(n);
  }
  while (n) {
    String *symname;
    /* for friends, we need to pop the scope once */
    int isfriend = is_friend(n);
    Symtab *class_scope = isfriend ? Swig_symbol_popscope() : 0;

    if (!isfriend && inclass && (cplus_mode != CPLUS_PUBLIC)) {
      int only_csymbol = 1;
      if (cplus_mode == CPLUS_PROTECTED) {
	Setattr(n,"access", "protected");
	only_csymbol = !need_protected(n, dirprot_mode);
      } else {
	/* private are needed only when they are pure virtuals */
	Setattr(n,"access", "private");
	if ((Cmp(Getattr(n,"storage"),"virtual") == 0) 
	    && (Cmp(Getattr(n,"value"),"0") == 0)) {
	  only_csymbol = !need_protected(n, dirprot_mode);
	}    
      }
      if (only_csymbol) {
	/* Only add to C symbol table and continue */
	Swig_symbol_add(0, n); 
	if (add_only_one) break;
	n = nextSibling(n);
	continue;
      }
    }
    if (Getattr(n,"sym:name")) {
      n = nextSibling(n);
      continue;
    }
    decl = Getattr(n,"decl");
    if (!SwigType_isfunction(decl)) {
      String *makename = Getattr(n,"parser:makename");
      if (makename) {
        Delattr(n,"parser:makename"); /* temporary information, don't leave it hanging around */
      } else {
        makename = Getattr(n,"name");
      }
      
      symname = make_name(makename,0);
      if (!symname) {
	symname = Getattr(n,"unnamed");
      }
      if (symname) {
	wrn = name_warning(n,symname,0);
	Swig_features_get(features_hash, Namespaceprefix, Getattr(n,"name"), 0, n);
      }
    } else {
      SwigType *fdecl = Copy(decl);
      SwigType *fun = SwigType_pop_function(fdecl);

      /* for friends, we need to disable the class prefix */
      String* class_prefix = isfriend ? Namespaceprefix : 0;
      if (isfriend) Namespaceprefix = 0;

      symname = make_name(Getattr(n,"name"),fun);
      wrn = name_warning(n,symname,fun);
      
      Swig_features_get(features_hash,Namespaceprefix,Getattr(n,"name"),fun,n);
      Delete(fdecl);
      Delete(fun);
      
      /* restore the class prefix if needed */
      if (isfriend) Namespaceprefix = class_prefix;
    }
    if (!symname) {
      n = nextSibling(n);
      continue;
    }
    if (strncmp(Char(symname),"$ignore",7) == 0) {
      char *c = Char(symname)+7;
      Setattr(n,"feature:ignore","1");
      if (strlen(c)) {
	SWIG_WARN_NODE_BEGIN(n);
	Swig_warning(0,Getfile(n), Getline(n), "%s\n",c+1);
	SWIG_WARN_NODE_END(n);
      }
      Swig_symbol_add(0, n);
    } else {
      Node *c;
      if ((wrn) && (Len(wrn))) {
	String *metaname = symname;
	if (!Getmeta(metaname,"already_warned")) {
	  SWIG_WARN_NODE_BEGIN(n);
	  Swig_warning(0,Getfile(n),Getline(n), "%s\n", wrn);
	  SWIG_WARN_NODE_END(n);
	  Setmeta(metaname,"already_warned","1");
	}
      }
      c = Swig_symbol_add(symname,n);

      if (c != n) {
        /* symbol conflict attempting to add in the new symbol */
        if (Getattr(n,"sym:weak")) {
          Setattr(n,"sym:name",symname);
        } else {
          String *e = NewString("");
          String *en = NewString("");
          String *ec = NewString("");
          int redefined = need_redefined_warn(n,c,inclass);
          if (redefined) {
            Printf(en,"Identifier '%s' redefined (ignored)",symname);
            Printf(ec,"previous definition of '%s'",symname);
          } else {
            Printf(en,"Redundant redeclaration of '%s'",symname);
            Printf(ec,"previous declaration of '%s'",symname);
          }
          if (Cmp(symname,Getattr(n,"name"))) {
            Printf(en," (Renamed from '%s')", SwigType_namestr(Getattr(n,"name")));
          }
          Printf(en,",");
          if (Cmp(symname,Getattr(c,"name"))) {
            Printf(ec," (Renamed from '%s')", SwigType_namestr(Getattr(c,"name")));
          }
          Printf(ec,".");
	  SWIG_WARN_NODE_BEGIN(n);
          if (redefined) {
            Swig_warning(WARN_PARSE_REDEFINED,Getfile(n),Getline(n),"%s\n",en);
            Swig_warning(WARN_PARSE_REDEFINED,Getfile(c),Getline(c),"%s\n",ec);
          } else if (!is_friend(n) && !is_friend(c)) {
            Swig_warning(WARN_PARSE_REDUNDANT,Getfile(n),Getline(n),"%s\n",en);
            Swig_warning(WARN_PARSE_REDUNDANT,Getfile(c),Getline(c),"%s\n",ec);
          }
	  SWIG_WARN_NODE_END(n);
          Printf(e,"%s:%d:%s\n%s:%d:%s\n",Getfile(n),Getline(n),en,
                 Getfile(c),Getline(c),ec);
          Setattr(n,"error",e);
          Delete(en);
          Delete(ec);
        }
      }
    }
    /* restore the class scope if needed */
    if (isfriend) Swig_symbol_setscope(class_scope);

    if (add_only_one) return;
    n = nextSibling(n);
  }
}


/* add symbols a parse tree node copy */

static void add_symbols_copy(Node *n) {
  String *name;
  int    emode = 0;

  while (n) {

    if (Strcmp(nodeType(n),"access") == 0) {
      String *kind = Getattr(n,"kind");
      if (Strcmp(kind,"public") == 0) {
	cplus_mode = CPLUS_PUBLIC;
      } else if (Strcmp(kind,"private") == 0) {
	cplus_mode = CPLUS_PRIVATE;
      } else if (Strcmp(kind,"protected") == 0) {
	cplus_mode = CPLUS_PROTECTED;
      }
      n = nextSibling(n);
      continue;
    }

    add_oldname = Getattr(n,"sym:name");
    if ((add_oldname) || (Getattr(n,"sym:needs_symtab"))) {
      if (add_oldname) {
	DohIncref(add_oldname);
	/* If already renamed, we used that name */
	if (Strcmp(add_oldname, Getattr(n,"name")) != 0) {
	  yyrename = add_oldname;
	}
      }
      Delattr(n,"sym:needs_symtab");
      Delattr(n,"sym:name");

      add_only_one = 1;
      add_symbols(n);

      if (Getattr(n,"partialargs")) {
	Swig_symbol_cadd(Getattr(n,"partialargs"),n);
      }
      add_only_one = 0;
      name = Getattr(n,"name");
      if (Getattr(n,"requires_symtab")) {
	Swig_symbol_newscope();
	Swig_symbol_setscopename(name);
	Namespaceprefix = Swig_symbol_qualifiedscopename(0);
      }
      if (Strcmp(nodeType(n),"class") == 0) {
	inclass = 1;
	if (Strcmp(Getattr(n,"kind"),"class") == 0) {
	  cplus_mode = CPLUS_PRIVATE;
	} else {
	  cplus_mode = CPLUS_PUBLIC;
	}
      }
      if (Strcmp(nodeType(n),"extend") == 0) {
	emode = cplus_mode;
	cplus_mode = CPLUS_PUBLIC;
      }
      add_symbols_copy(firstChild(n));
      if (Strcmp(nodeType(n),"extend") == 0) {
	cplus_mode = emode;
      }
      if (Getattr(n,"requires_symtab")) {
	Setattr(n,"symtab", Swig_symbol_popscope());
	Delattr(n,"requires_symtab");
	Namespaceprefix = Swig_symbol_qualifiedscopename(0);
      }
      if (add_oldname) {
	Delete(add_oldname);
      }
      if (Strcmp(nodeType(n),"class") == 0) {
	inclass = 0;
      }
      add_oldname = 0;
    } else {
      if (Strcmp(nodeType(n),"extend") == 0) {
	emode = cplus_mode;
	cplus_mode = CPLUS_PUBLIC;
      }
      add_symbols_copy(firstChild(n));
      if (Strcmp(nodeType(n),"extend") == 0) {
	cplus_mode = emode;
      }
    }
    n = nextSibling(n);
  }
}

/* Extension merge.  This function is used to handle the %extend directive
   when it appears before a class definition.   To handle this, the %extend
   actually needs to take precedence.  Therefore, we will selectively nuke symbols
   from the current symbol table, replacing them with the added methods */

static void merge_extensions(Node *cls, Node *am) {
  Node *n;
  Node *csym;

  n = firstChild(am);
  while (n) {
    String *symname;
    if (Strcmp(nodeType(n),"constructor") == 0) {
      symname = Getattr(n,"sym:name");
      if (symname) {
	if (Strcmp(symname,Getattr(n,"name")) == 0) {
	  /* If the name and the sym:name of a constructor are the same,
             then it hasn't been renamed.  However---the name of the class
             itself might have been renamed so we need to do a consistency
             check here */
	  if (Getattr(cls,"sym:name")) {
	    Setattr(n,"sym:name", Getattr(cls,"sym:name"));
	  }
	}
      } 
    }

    symname = Getattr(n,"sym:name");
    DohIncref(symname);
    if ((symname) && (!Getattr(n,"error"))) {
      /* Remove node from its symbol table */
      Swig_symbol_remove(n);
      csym = Swig_symbol_add(symname,n);
      if (csym != n) {
	/* Conflict with previous definition.  Nuke previous definition */
	String *e = NewString("");
	String *en = NewString("");
	String *ec = NewString("");
	Printf(ec,"Identifier '%s' redefined by %%extend (ignored),",symname);
	Printf(en,"%%extend definition of '%s'.",symname);
	SWIG_WARN_NODE_BEGIN(n);
	Swig_warning(WARN_PARSE_REDEFINED,Getfile(csym),Getline(csym),"%s\n",ec);
	Swig_warning(WARN_PARSE_REDEFINED,Getfile(n),Getline(n),"%s\n",en);
	SWIG_WARN_NODE_END(n);
	Printf(e,"%s:%d:%s\n%s:%d:%s\n",Getfile(csym),Getline(csym),ec, 
	       Getfile(n),Getline(n),en);
	Setattr(csym,"error",e);
	Delete(en);
	Delete(ec);
	Swig_symbol_remove(csym);              /* Remove class definition */
	Swig_symbol_add(symname,n);            /* Insert extend definition */
      }
    }
    n = nextSibling(n);
  }
}

/* Check for unused %extend.  Special case, don't report unused
   extensions for templates */
 
 static void check_extensions() {
   Iterator ki;

   if (!extendhash) return;
   for (ki = First(extendhash); ki.key; ki = Next(ki)) {
     if (!Strstr(ki.key,"<")) {
       SWIG_WARN_NODE_BEGIN(ki.item);
       Swig_warning(WARN_PARSE_EXTEND_UNDEF,Getfile(ki.item), Getline(ki.item), "%%extend defined for an undeclared class %s.\n", ki.key);
       SWIG_WARN_NODE_END(ki.item);
     }
   }
 }

/* Check a set of declarations to see if any are pure-abstract */

 static List *pure_abstract(Node *n) {
   List *abs = 0;
   while (n) {
     if (Cmp(nodeType(n),"cdecl") == 0) {
       String *decl = Getattr(n,"decl");
       if (SwigType_isfunction(decl)) {
	 String *init = Getattr(n,"value");
	 if (Cmp(init,"0") == 0) {
	   if (!abs) {
	     abs = NewList();
	   }
	   Append(abs,n);
	   Setattr(n,"abstract","1");
	 }
       }
     } else if (Cmp(nodeType(n),"destructor") == 0) {
       if (Cmp(Getattr(n,"value"),"0") == 0) {
	 if (!abs) {
	   abs = NewList();
	 }
	 Append(abs,n);
	 Setattr(n,"abstract","1");
       }
     }
     n = nextSibling(n);
   }
   return abs;
 }

 /* Make a classname */

 static String *make_class_name(String *name) {
   String *nname = 0;
   if (Namespaceprefix) {
     nname= NewStringf("%s::%s", Namespaceprefix, name);
   } else {
     nname = NewString(name);
   }
   if (SwigType_istemplate(nname)) {
     String *prefix, *args, *qargs;
     prefix = SwigType_templateprefix(nname);
     args   = SwigType_templateargs(nname);
     qargs  = Swig_symbol_type_qualify(args,0);
     Append(prefix,qargs);
     Delete(nname);
     nname = prefix;
   }
   return nname;
 }

 static List *make_inherit_list(String *clsname, List *names) {
   int i;
   String *derived;
   List *bases = NewList();

   if (Namespaceprefix) derived = NewStringf("%s::%s", Namespaceprefix,clsname);
   else derived = NewString(clsname);

   for (i = 0; i < Len(names); i++) {
     Node *s;
     String *base;
     String *n = Getitem(names,i);
     /* Try to figure out where this symbol is */
     s = Swig_symbol_clookup(n,0);
     if (s) {
       while (s && (Strcmp(nodeType(s),"class") != 0)) {
	 /* Not a class.  Could be a typedef though. */
	 String *storage = Getattr(s,"storage");
	 if (storage && (Strcmp(storage,"typedef") == 0)) {
	   String *nn = Getattr(s,"type");
	   s = Swig_symbol_clookup(nn,Getattr(s,"sym:symtab"));
	 } else {
	   break;
	 }
       }
       if (s && ((Strcmp(nodeType(s),"class") == 0) || (Strcmp(nodeType(s),"template") == 0))) {
	 String *q = Swig_symbol_qualified(s);
	 Append(bases,s);
	 if (q) {
	   base = NewStringf("%s::%s", q, Getattr(s,"name"));
	 } else {
	   base = NewString(Getattr(s,"name"));
	 }
       } else {
	 base = NewString(n);
       }
     } else {
       base = NewString(n);
     }
     if (base) {
       rename_inherit(base,derived);
       Delete(base);
     }
   }
   return bases;
 }

/* If the class name is qualified.  We need to create or lookup namespace entries */

static Symtab *get_global_scope() {
  Symtab *symtab = Swig_symbol_current();
  Node   *pn = parentNode(symtab);
  while (pn) {
    symtab = pn;
    pn = parentNode(symtab);
    if (!pn) break;
  }
  Swig_symbol_setscope(symtab);
  return symtab;
}
 

static Node *nscope = 0;
static Node *nscope_inner = 0;
static String *resolve_node_scope(String *cname) {
  Symtab *gscope = 0;
  nscope = 0;
  nscope_inner = 0;
  if (Swig_scopename_check(cname)) {
    Node   *ns;
    String *prefix = Swig_scopename_prefix(cname);
    String *base = Swig_scopename_last(cname);
    if (prefix && (Strncmp(prefix,"::",2) == 0)) {
      /* Use the global scope */
      String *nprefix = NewString(Char(prefix)+2);
      Delete(prefix);
      prefix= nprefix;
      gscope = get_global_scope();
    }    
    if (!prefix || (Len(prefix) == 0)) {
      /* Use the global scope, but we need to add a 'global' namespace.  */
      if (!gscope) gscope = get_global_scope();
      /* note that this namespace is not the "unnamed" one,
	 and we don't use Setattr(nscope,"name", ""),
	 because the unnamed namespace is private */
      nscope = new_node("namespace");
      Setattr(nscope,"symtab", gscope);;
      nscope_inner = nscope;
      return base;
    }
    /* Try to locate the scope */
    ns = Swig_symbol_clookup(prefix,0);
    if (!ns) {
      Swig_error(cparse_file,cparse_line,"Undefined scope '%s'\n", prefix);
    } else {
      Symtab *nstab = Getattr(ns,"symtab");
      if (!nstab) {
	Swig_error(cparse_file,cparse_line,
		   "'%s' is not defined as a valid scope.\n", prefix);
	ns = 0;
      } else {
	/* Check if the node scope is the current scope */
	String *tname = Swig_symbol_qualifiedscopename(0);
	String *nname = Swig_symbol_qualifiedscopename(nstab);
	if (tname && (Strcmp(tname,nname) == 0)) {
	  ns = 0;
	  cname = base;
	}
	Delete(tname);
	Delete(nname);
      }
      if (ns) {
	/* we will to try to create a new node using the namespaces we
	   can find in the scope name */
	List *scopes;
	String *sname;
	Iterator si;
	String *name = NewString(prefix);
	scopes = NewList();
	while (name) {
	  String *base = Swig_scopename_last(name);
	  String *tprefix = Swig_scopename_prefix(name);
	  Insert(scopes,0,base);
	  Delete(name);
	  name = tprefix;
	}
	for (si = First(scopes); si.item; si = Next(si)) {
	  Node *ns1,*ns2;
	  sname = si.item;
	  ns1 = Swig_symbol_clookup(sname,0);
	  assert(ns1);
	  if (Strcmp(nodeType(ns1),"namespace") == 0) {
	    if (Getattr(ns1,"alias")) {
	      ns1 = Getattr(ns1,"namespace");
	    }
	  } else {
	    /* now this last part is a class */
	    si = Next(si);
	    ns1 = Swig_symbol_clookup(sname,0);
	    /*  or a nested class tree, which is unrolled here */
	    for (; si.item; si = Next(si)) {
	      if (si.item) {
		Printf(sname,"::%s",si.item);
	      }
	    }
	    /* we get the 'inner' class */
	    nscope_inner = Swig_symbol_clookup(sname,0);
	    /* set the scope to the inner class */
	    Swig_symbol_setscope(Getattr(nscope_inner,"symtab"));
	    /* save the last namespace prefix */
	    Namespaceprefix = Swig_symbol_qualifiedscopename(0);
	    /* and return the node name, including the inner class prefix */
	    break;
	  }
	  /* here we just populate the namespace tree as usual */
	  ns2 = new_node("namespace");
	  Setattr(ns2,"name",sname);
	  Setattr(ns2,"symtab", Getattr(ns1,"symtab"));
	  add_symbols(ns2);
	  Swig_symbol_setscope(Getattr(ns1,"symtab"));
	  Namespaceprefix = Swig_symbol_qualifiedscopename(0);
	  if (nscope_inner) {
	    if (Getattr(nscope_inner,"symtab") != Getattr(ns2,"symtab")) {
	      appendChild(nscope_inner,ns2);
	    }
	  }
	  nscope_inner = ns2;
	  if (!nscope) nscope = ns2;
	}
	cname = base;
      }
    }
    Delete(prefix);
  }
  return cname;
}
 




/* Structures for handling code fragments built for nested classes */

typedef struct Nested {
  String   *code;        /* Associated code fragment */
  int      line;         /* line number where it starts */
  char     *name;        /* Name associated with this nested class */
  char     *kind;        /* Kind of class */
  SwigType *type;        /* Datatype associated with the name */
  struct Nested   *next;        /* Next code fragment in list */
} Nested;

/* Some internal variables for saving nested class information */

static Nested      *nested_list = 0;

/* Add a function to the nested list */

static void add_nested(Nested *n) {
  Nested *n1;
  if (!nested_list) nested_list = n;
  else {
    n1 = nested_list;
    while (n1->next) n1 = n1->next;
    n1->next = n;
  }
}

/* Dump all of the nested class declarations to the inline processor
 * However.  We need to do a few name replacements and other munging
 * first.  This function must be called before closing a class! */

static Node *dump_nested(const char *parent) {
  Nested *n,*n1;
  Node *ret = 0;
  n = nested_list;
  if (!parent) {
    nested_list = 0;
    return 0;
  }
  while (n) {
    char temp[256];
    Node *retx;
    /* Token replace the name of the parent class */
    Replace(n->code, "$classname", parent, DOH_REPLACE_ANY);
    /* Fix up the name of the datatype (for building typedefs and other stuff) */
    sprintf(temp,"%s_%s", parent,n->name);

    Append(n->type,parent);
    Append(n->type,"_");
    Append(n->type,n->name);

    /* Add the appropriate declaration to the C++ processor */
    retx = new_node("cdecl");
    Setattr(retx,"name",n->name);
    Setattr(retx,"type",Copy(n->type));
    Setattr(retx,"nested",parent);
    add_symbols(retx);
    if (ret) {
      set_nextSibling(retx,ret);
    }
    ret = retx;

    /* Insert a forward class declaration */
    /* Disabled: [ 597599 ] union in class: incorrect scope 
    retx = new_node("classforward");
    Setattr(retx,"kind",n->kind);
    Setattr(retx,"name",Copy(n->type));
    Setattr(retx,"sym:name", make_name(n->type,0));
    set_nextSibling(retx,ret);
    ret = retx; 
    */

    /* Make all SWIG created typedef structs/unions/classes unnamed else 
       redefinition errors occur - nasty hack alert.*/

    {
      const char* types_array[3] = {"struct", "union", "class"};
      int i;
      for (i=0; i<3; i++) {
	char* code_ptr = Char(n->code);
      while (code_ptr) {
        /* Replace struct name (as in 'struct name {' ) with whitespace
           name will be between struct and { */
	
        code_ptr = strstr(code_ptr, types_array[i]);
        if (code_ptr) {
	  char *open_bracket_pos;
          code_ptr += strlen(types_array[i]);
          open_bracket_pos = strstr(code_ptr, "{");
          if (open_bracket_pos) { 
            /* Make sure we don't have something like struct A a; */
            char* semi_colon_pos = strstr(code_ptr, ";");
            if (!(semi_colon_pos && (semi_colon_pos < open_bracket_pos)))
              while (code_ptr < open_bracket_pos)
                *code_ptr++ = ' ';
          }
        }
      }
      }
    }
    
    {
      /* Remove SWIG directive %constant which may be left in the SWIG created typedefs */
      char* code_ptr = Char(n->code);
      while (code_ptr) {
	code_ptr = strstr(code_ptr, "%constant");
	if (code_ptr) {
	  char* directive_end_pos = strstr(code_ptr, ";");
	  if (directive_end_pos) { 
            while (code_ptr <= directive_end_pos)
              *code_ptr++ = ' ';
	  }
	}
      }
    }
    {
      Node *head;
      head = new_node("insert");
      Setattr(head,"code",NewStringf("\n%s\n",n->code));
      set_nextSibling(head,ret);
      ret = head;
    }
      
    /* Dump the code to the scanner */
    start_inline(Char(n->code),n->line);

    n1 = n->next;
    Delete(n->code);
    free(n);
    n = n1;
  }
  nested_list = 0;
  return ret;
}

Node *Swig_cparse(File *f) {
  scanner_file(f);
  top = 0;
  yyparse();
  return top;
}

static void single_new_feature(const char *featurename, String *val, Hash *featureattribs, char *declaratorid, SwigType *type, ParmList *declaratorparms, String *qualifier) {
  String *fname;
  String *name;
  String *fixname;
  SwigType *t = Copy(type);

  /* Printf(stdout, "single_new_feature: [%s] [%s] [%s] [%s] [%s] [%s]\n", featurename, val, declaratorid, t, ParmList_str_defaultargs(declaratorparms), qualifier); */

  if (!features_hash) features_hash = NewHash();
  fname = NewStringf("feature:%s",featurename);
  if (declaratorid) {
    fixname = feature_identifier_fix(declaratorid);
  } else {
    fixname = NewString("");
  }
  if (Namespaceprefix) {
   name = NewStringf("%s::%s",Namespaceprefix, fixname);
  } else {
   name = fixname;
  }

  if (declaratorparms) Setmeta(val,"parms",declaratorparms);
  if (!Len(t)) t = 0;
  if (t) {
   if (qualifier) SwigType_push(t,qualifier);
   if (SwigType_isfunction(t)) {
     SwigType *decl = SwigType_pop_function(t);
     if (SwigType_ispointer(t)) {
       String *nname = NewStringf("*%s",name);
       Swig_feature_set(features_hash, nname, decl, fname, val, featureattribs);
       Delete(nname);
     } else {
       Swig_feature_set(features_hash, name, decl, fname, val, featureattribs);
     }
     Delete(decl);
   } else if (SwigType_ispointer(t)) {
     String *nname = NewStringf("*%s",name);
     Swig_feature_set(features_hash,nname,0,fname,val, featureattribs);
     Delete(nname);
   }
  } else {
   /* Global feature, that is, feature not associated with any particular symbol */
   Swig_feature_set(features_hash,name,0,fname,val, featureattribs);
  }
  Delete(fname);
  Delete(name);
}

/* Add a new feature to the Hash. Additional features are added if the feature has a parameter list (declaratorparms)
 * and one or more of the parameters have a default argument. An extra feature is added for each defaulted parameter,
 * simulating the equivalent overloaded method. */
static void new_feature(const char *featurename, String *val, Hash *featureattribs, char *declaratorid, SwigType *type, ParmList *declaratorparms, String *qualifier) {

  ParmList *declparms = declaratorparms;

  /* Add the feature */
  single_new_feature(featurename, val, featureattribs, declaratorid, type, declaratorparms, qualifier);

  /* Add extra features if there are default parameters in the parameter list */
  if (type) {
    while (declparms) {
      if (ParmList_has_defaultargs(declparms)) {

        /* Create a parameter list for the new feature by copying all
           but the last (defaulted) parameter */
        ParmList* newparms = ParmList_copy_all_except_last_parm(declparms);

        /* Create new declaration - with the last parameter removed */
        SwigType *newtype = Copy(type);
        Delete(SwigType_pop_function(newtype)); /* remove the old parameter list from newtype */
        SwigType_add_function(newtype,newparms);

        single_new_feature(featurename, Copy(val), featureattribs, declaratorid, newtype, newparms, qualifier);
        declparms = newparms;
      } else {
        declparms = 0;
      }
    }
  }
}

/* check if a function declaration is a plain C object */
static int is_cfunction(Node *n) {
  if (!cparse_cplusplus || cparse_externc) return 1;
  if (Cmp(Getattr(n,"storage"),"externc") == 0) {
    return 1;
  }
  return 0;
}

/* If the Node is a function with parameters, check to see if any of the parameters
 * have default arguments. If so create a new function for each defaulted argument. 
 * The additional functions form a linked list of nodes with the head being the original Node n. */
static void default_arguments(Node *n) {
  Node *function = n;

  if (function) {
    ParmList *varargs = Getattr(function,"feature:varargs");
    if (varargs) {
      /* Handles the %varargs directive by looking for "feature:varargs" and 
       * substituting ... with an alternative set of arguments.  */
      Parm     *p = Getattr(function,"parms");
      Parm     *pp = 0;
      while (p) {
	SwigType *t = Getattr(p,"type");
	if (Strcmp(t,"v(...)") == 0) {
	  if (pp) {
	    set_nextSibling(pp,Copy(varargs));
	  } else {
	    Setattr(function,"parms", Copy(varargs));
	  }
	  break;
	}
	pp = p;
	p = nextSibling(p);
      }
    }

    /* Do not add in functions if kwargs is being used or if user wants old default argument wrapping
      (one wrapped method per function irrespective of number of default arguments) */
    if (compact_default_args 
	|| is_cfunction(function) 
	|| Getattr(function,"feature:compactdefaultargs") 
	|| Getattr(function,"feature:kwargs")) {
      ParmList *p = Getattr(function,"parms");
      if (p) 
        Setattr(p,"compactdefargs", "1"); /* mark parameters for special handling */
      function = 0; /* don't add in extra methods */
    }
  }

  while (function) {
    ParmList *parms = Getattr(function,"parms");
    if (ParmList_has_defaultargs(parms)) {

      /* Create a parameter list for the new function by copying all
         but the last (defaulted) parameter */
      ParmList* newparms = ParmList_copy_all_except_last_parm(parms);

      /* Create new function and add to symbol table */
      {
        Node *new_function = new_node(Copy(nodeType(function)));
        SwigType *decl = Copy(Getattr(function,"decl"));
        int constqualifier = SwigType_isconst(decl);

        Delete(SwigType_pop_function(decl)); /* remove the old parameter list from decl */
        SwigType_add_function(decl,newparms);
        if (constqualifier)
          SwigType_add_qualifier(decl,"const");

        Setattr(new_function,"name",Getattr(function,"name"));
        Setattr(new_function,"code",Copy(Getattr(function,"code")));
        Setattr(new_function,"decl", decl);
        Setattr(new_function,"parms",newparms);
        Setattr(new_function,"storage",Copy(Getattr(function,"storage")));
        Setattr(new_function,"type",Copy(Getattr(function,"type")));
        Setattr(new_function,"throw",Copy(Getattr(function,"throw")));

        {
          Node *throws = Getattr(function,"throws");
	  ParmList *pl = CopyParmList(throws);
          if (throws) Setattr(new_function,"throws",pl);
	  Delete(pl);
        }

        /* copy specific attributes for global (or in a namespace) template functions - these are not templated class methods */
        if (Strcmp(nodeType(function),"template") == 0) {
          Node *templatetype = Getattr(function,"templatetype");
          Node *symtypename = Getattr(function,"sym:typename");
          Parm *templateparms = Getattr(function,"templateparms");
          if (templatetype) Setattr(new_function,"templatetype",Copy(templatetype));
          if (symtypename) Setattr(new_function,"sym:typename",Copy(symtypename));
          if (templateparms) Setattr(new_function,"templateparms",CopyParmList(templateparms));
        } else if (Strcmp(nodeType(function),"constructor") == 0) {
          /* only copied for constructors as this is not a user defined feature - it is hard coded in the parser */
          Node *featurenew = Getattr(function,"feature:new");
          if (featurenew) Setattr(new_function,"feature:new",Copy(featurenew));
        }

        add_symbols(new_function);
        /* mark added functions as ones with overloaded parameters and point to the parsed method */
        Setattr(new_function,"defaultargs", n);

        /* Point to the new function, extending the linked list */
        set_nextSibling(function, new_function);

        function = new_function;
      }
    } else {
      function = 0;
    }
  }
}



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 1240 "parser.y"
typedef union YYSTYPE {
  char  *id;
  List  *bases;
  struct Define {
    String *val;
    String *rawval;
    int     type;
    String *qualifier;
    String *bitfield;
    Parm   *throws;
    String *throw;
  } dtype;
  struct {
    char *type;
    char *filename;
    int   line;
  } loc;
  struct {
    char      *id;
    SwigType  *type;
    String    *defarg;
    ParmList  *parms;
    short      have_parms;
    ParmList  *throws;
    String    *throw;
  } decl;
  Parm         *tparms;
  struct {
    String     *op;
    Hash       *kwargs;
  } tmap;
  struct {
    String     *type;
    String     *us;
  } ptype;
  SwigType     *type;
  String       *str;
  Parm         *p;
  ParmList     *pl;
  int           ivalue;
  Node         *node;
} YYSTYPE;
/* Line 191 of yacc.c.  */
#line 1583 "y.tab.c"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 214 of yacc.c.  */
#line 1595 "y.tab.c"

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# if YYSTACK_USE_ALLOCA
#  define YYSTACK_ALLOC alloca
# else
#  ifndef YYSTACK_USE_ALLOCA
#   if defined (alloca) || defined (_ALLOCA_H)
#    define YYSTACK_ALLOC alloca
#   else
#    ifdef __GNUC__
#     define YYSTACK_ALLOC __builtin_alloca
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC malloc
#  define YYSTACK_FREE free
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  46
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   3354

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  114
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  143
/* YYNRULES -- Number of rules. */
#define YYNRULES  442
/* YYNRULES -- Number of states. */
#define YYNSTATES  856

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   368

#define YYTRANSLATE(YYX) 						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short yyprhs[] =
{
       0,     0,     3,     5,     9,    12,    16,    19,    22,    24,
      26,    28,    30,    32,    34,    36,    39,    41,    43,    45,
      47,    49,    51,    53,    55,    57,    59,    61,    63,    65,
      67,    69,    71,    73,    75,    77,    79,    81,    82,    90,
      96,   100,   106,   112,   116,   119,   122,   128,   131,   137,
     140,   145,   147,   149,   157,   165,   171,   172,   180,   182,
     184,   187,   190,   192,   198,   204,   210,   214,   219,   223,
     231,   240,   246,   250,   252,   254,   258,   260,   265,   273,
     280,   282,   284,   292,   302,   311,   322,   328,   336,   343,
     352,   354,   356,   362,   367,   373,   381,   383,   387,   394,
     401,   410,   412,   415,   419,   421,   424,   428,   435,   441,
     451,   454,   456,   458,   460,   461,   468,   474,   476,   481,
     483,   485,   488,   494,   501,   506,   514,   523,   530,   532,
     534,   536,   538,   540,   542,   543,   553,   554,   563,   565,
     568,   573,   574,   581,   585,   587,   589,   591,   593,   595,
     597,   601,   606,   607,   614,   615,   621,   627,   630,   631,
     638,   640,   641,   645,   647,   649,   651,   653,   655,   657,
     659,   661,   665,   667,   669,   671,   673,   675,   677,   679,
     681,   683,   690,   697,   705,   714,   723,   731,   737,   740,
     743,   746,   747,   755,   756,   763,   764,   773,   775,   777,
     779,   781,   783,   785,   787,   789,   791,   793,   795,   797,
     799,   802,   805,   808,   813,   816,   822,   824,   827,   829,
     831,   833,   835,   837,   839,   842,   844,   848,   850,   853,
     860,   864,   866,   869,   871,   875,   877,   879,   881,   883,
     886,   890,   893,   896,   898,   901,   904,   906,   908,   910,
     912,   915,   919,   921,   924,   928,   933,   939,   944,   946,
     949,   953,   958,   964,   968,   973,   978,   980,   983,   988,
     993,   999,  1003,  1008,  1013,  1015,  1018,  1021,  1025,  1027,
    1030,  1032,  1035,  1039,  1044,  1048,  1053,  1056,  1060,  1064,
    1069,  1073,  1077,  1080,  1083,  1085,  1087,  1090,  1092,  1094,
    1096,  1098,  1101,  1103,  1105,  1107,  1109,  1112,  1115,  1117,
    1120,  1122,  1125,  1127,  1129,  1132,  1134,  1136,  1138,  1140,
    1142,  1144,  1146,  1148,  1150,  1151,  1154,  1156,  1158,  1160,
    1164,  1166,  1168,  1172,  1174,  1176,  1178,  1180,  1182,  1188,
    1190,  1192,  1196,  1201,  1207,  1213,  1220,  1222,  1224,  1226,
    1228,  1230,  1232,  1234,  1238,  1242,  1246,  1250,  1254,  1258,
    1262,  1266,  1270,  1274,  1278,  1281,  1284,  1287,  1290,  1293,
    1295,  1296,  1300,  1302,  1304,  1308,  1311,  1316,  1318,  1320,
    1322,  1324,  1326,  1328,  1330,  1332,  1334,  1336,  1341,  1347,
    1349,  1353,  1357,  1362,  1367,  1371,  1374,  1376,  1378,  1382,
    1385,  1389,  1391,  1393,  1395,  1397,  1399,  1402,  1407,  1409,
    1413,  1415,  1419,  1423,  1426,  1429,  1432,  1435,  1438,  1443,
    1445,  1449,  1451,  1455,  1459,  1462,  1465,  1468,  1471,  1473,
    1475,  1477,  1479,  1483,  1485,  1489,  1495,  1497,  1501,  1505,
    1511,  1513,  1515
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const short yyrhs[] =
{
     115,     0,    -1,   116,    -1,    96,   199,    35,    -1,    96,
       1,    -1,    97,   199,    35,    -1,    97,     1,    -1,   116,
     117,    -1,   256,    -1,   118,    -1,   155,    -1,   163,    -1,
      35,    -1,     1,    -1,   162,    -1,     1,    95,    -1,   119,
      -1,   121,    -1,   122,    -1,   123,    -1,   124,    -1,   125,
      -1,   128,    -1,   129,    -1,   132,    -1,   133,    -1,   134,
      -1,   135,    -1,   136,    -1,   137,    -1,   140,    -1,   142,
      -1,   145,    -1,   147,    -1,   152,    -1,   153,    -1,   154,
      -1,    -1,    56,   253,   246,    38,   120,   177,    39,    -1,
      79,   151,    38,   149,    39,    -1,    80,   149,    35,    -1,
      52,     3,    46,   221,    35,    -1,    52,   215,   207,   204,
      35,    -1,    52,     1,    35,    -1,    78,     4,    -1,    78,
     251,    -1,    77,    32,     3,    33,    38,    -1,    77,    38,
      -1,    77,    32,     3,    33,    35,    -1,    77,    35,    -1,
     251,    38,   199,    39,    -1,   251,    -1,   126,    -1,    82,
      32,   127,    34,   254,    33,     4,    -1,    82,    32,   127,
      34,   254,    33,    38,    -1,    82,    32,   127,    33,    35,
      -1,    -1,   131,   253,   251,    49,   130,   116,    50,    -1,
       7,    -1,     8,    -1,    75,     4,    -1,    75,    38,    -1,
       4,    -1,     9,    32,   244,    33,   251,    -1,     9,    32,
     244,    33,     4,    -1,     9,    32,   244,    33,    38,    -1,
      48,   253,   244,    -1,    53,    32,   244,    33,    -1,    53,
      32,    33,    -1,    74,    32,     3,    33,   195,     3,    35,
      -1,    74,    32,     3,    33,   195,   215,   207,    35,    -1,
      57,   139,     3,    46,   138,    -1,    57,   139,     3,    -1,
     251,    -1,     4,    -1,    32,     3,    33,    -1,   256,    -1,
     141,   207,   244,    35,    -1,   141,    32,   244,    33,   207,
     238,    35,    -1,   141,    32,   244,    33,   251,    35,    -1,
      54,    -1,    55,    -1,    58,    32,   244,    33,   207,   238,
     143,    -1,    58,    32,   244,    34,   255,    33,   207,   238,
      35,    -1,    58,    32,   244,   144,    33,   207,   238,   143,
      -1,    58,    32,   244,    34,   255,   144,    33,   207,   238,
      35,    -1,    58,    32,   244,    33,   143,    -1,    58,    32,
     244,    34,   255,    33,    35,    -1,    58,    32,   244,   144,
      33,   143,    -1,    58,    32,   244,    34,   255,   144,    33,
      35,    -1,   252,    -1,    35,    -1,    89,    32,   196,    33,
      35,    -1,    34,   244,    46,   255,    -1,    34,   244,    46,
     255,   144,    -1,    59,    32,   146,    33,   207,   238,    35,
      -1,   196,    -1,    11,    34,   199,    -1,    76,    32,   148,
      33,   149,   252,    -1,    76,    32,   148,    33,   149,    35,
      -1,    76,    32,   148,    33,   149,    46,   151,    35,    -1,
     254,    -1,   151,   150,    -1,    34,   151,   150,    -1,   256,
      -1,   215,   206,    -1,    32,   196,    33,    -1,    32,   196,
      33,    32,   196,    33,    -1,    88,    32,   196,    33,    35,
      -1,    81,    32,   245,    33,   249,    84,   200,    85,    35,
      -1,    83,   251,    -1,   157,    -1,   161,    -1,   160,    -1,
      -1,    36,   251,    38,   156,   116,    39,    -1,   195,   215,
     207,   159,   158,    -1,    35,    -1,    34,   207,   159,   158,
      -1,    38,    -1,   204,    -1,   213,   204,    -1,    70,    32,
     196,    33,   204,    -1,   213,    70,    32,   196,    33,   204,
      -1,   195,    60,     3,    35,    -1,   195,    60,   223,    38,
     224,    39,    35,    -1,   195,    60,   223,    38,   224,    39,
     207,   158,    -1,   195,   215,    32,   196,    33,   239,    -1,
     164,    -1,   168,    -1,   169,    -1,   173,    -1,   174,    -1,
     184,    -1,    -1,   195,   236,   246,   230,    38,   165,   177,
      39,   167,    -1,    -1,   195,   236,    38,   166,   177,    39,
     207,   158,    -1,    35,    -1,   207,   158,    -1,   195,   236,
     246,    35,    -1,    -1,    93,    84,   172,    85,   170,   171,
      -1,    93,   236,   246,    -1,   157,    -1,   164,    -1,   181,
      -1,   169,    -1,   168,    -1,   197,    -1,    72,   246,    35,
      -1,    72,    73,   246,    35,    -1,    -1,    73,   246,    38,
     175,   116,    39,    -1,    -1,    73,    38,   176,   116,    39,
      -1,    73,     3,    46,   246,    35,    -1,   180,   177,    -1,
      -1,    56,    38,   178,   177,    39,   177,    -1,   256,    -1,
      -1,     1,   179,   177,    -1,   155,    -1,   181,    -1,   182,
      -1,   185,    -1,   191,    -1,   183,    -1,   168,    -1,   186,
      -1,   195,   246,    35,    -1,   173,    -1,   169,    -1,   184,
      -1,   153,    -1,   154,    -1,   194,    -1,   128,    -1,   152,
      -1,    35,    -1,   195,   215,    32,   196,    33,   239,    -1,
     111,   248,    32,   196,    33,   192,    -1,    68,   111,   248,
      32,   196,    33,   193,    -1,   195,    95,   215,   212,    32,
     196,    33,   193,    -1,   195,    95,   215,   103,    32,   196,
      33,   193,    -1,   195,    95,   215,    32,   196,    33,   193,
      -1,    71,    32,   196,    33,    38,    -1,    64,    66,    -1,
      63,    66,    -1,    65,    66,    -1,    -1,   195,   236,     3,
      38,   187,   190,    35,    -1,    -1,   195,   236,    38,   188,
     190,    35,    -1,    -1,   195,   236,   246,    66,   233,    38,
     189,    35,    -1,   207,    -1,   256,    -1,   137,    -1,   123,
      -1,   135,    -1,   140,    -1,   142,    -1,   145,    -1,   133,
      -1,   147,    -1,   121,    -1,   122,    -1,   124,    -1,   238,
      35,    -1,   238,    38,    -1,   238,    35,    -1,   238,    46,
     221,    35,    -1,   238,    38,    -1,   195,   215,    66,   227,
      35,    -1,    36,    -1,    36,   251,    -1,    67,    -1,    18,
      -1,    68,    -1,    69,    -1,   256,    -1,   197,    -1,   199,
     198,    -1,   256,    -1,    34,   199,   198,    -1,   256,    -1,
     216,   205,    -1,    93,    84,   236,    85,   236,   246,    -1,
      40,    40,    40,    -1,   201,    -1,   203,   202,    -1,   256,
      -1,    34,   203,   202,    -1,   256,    -1,   199,    -1,   228,
      -1,     6,    -1,    46,   221,    -1,    46,   103,   207,    -1,
      46,    38,    -1,    66,   227,    -1,   256,    -1,   207,   204,
      -1,   210,   204,    -1,   204,    -1,   207,    -1,   210,    -1,
     256,    -1,   212,   208,    -1,   212,   103,   208,    -1,   209,
      -1,   103,   208,    -1,   246,    91,   208,    -1,   212,   246,
      91,   208,    -1,   212,   246,    91,   103,   208,    -1,   246,
      91,   103,   208,    -1,   246,    -1,   111,   246,    -1,    32,
     246,    33,    -1,    32,   212,   208,    33,    -1,    32,   246,
      91,   208,    33,    -1,   208,    49,    50,    -1,   208,    49,
     227,    50,    -1,   208,    32,   196,    33,    -1,   246,    -1,
     111,   246,    -1,    32,   212,   209,    33,    -1,    32,   103,
     209,    33,    -1,    32,   246,    91,   209,    33,    -1,   209,
      49,    50,    -1,   209,    49,   227,    50,    -1,   209,    32,
     196,    33,    -1,   212,    -1,   212,   211,    -1,   212,   103,
      -1,   212,   103,   211,    -1,   211,    -1,   103,   211,    -1,
     103,    -1,   246,    91,    -1,   212,   246,    91,    -1,   212,
     246,    91,   211,    -1,   211,    49,    50,    -1,   211,    49,
     227,    50,    -1,    49,    50,    -1,    49,   227,    50,    -1,
      32,   210,    33,    -1,   211,    32,   196,    33,    -1,    32,
     196,    33,    -1,   109,   213,   212,    -1,   109,   212,    -1,
     109,   213,    -1,   109,    -1,   214,    -1,   214,   213,    -1,
      41,    -1,    42,    -1,    43,    -1,   216,    -1,   213,   217,
      -1,   217,    -1,   218,    -1,    28,    -1,    26,    -1,    30,
     243,    -1,    60,   246,    -1,    31,    -1,   217,   213,    -1,
     246,    -1,   236,   246,    -1,   219,    -1,   220,    -1,   220,
     219,    -1,    19,    -1,    21,    -1,    22,    -1,    25,    -1,
      23,    -1,    24,    -1,    27,    -1,    20,    -1,    29,    -1,
      -1,   222,   227,    -1,    10,    -1,     3,    -1,   256,    -1,
     224,    34,   225,    -1,   225,    -1,     3,    -1,     3,    46,
     226,    -1,   256,    -1,   227,    -1,    10,    -1,   228,    -1,
     251,    -1,    47,    32,   215,   205,    33,    -1,   229,    -1,
     215,    -1,    32,   227,    33,    -1,    32,   227,    33,   227,
      -1,    32,   227,   212,    33,   227,    -1,    32,   227,   103,
      33,   227,    -1,    32,   227,   212,   103,    33,   227,    -1,
      11,    -1,    12,    -1,    13,    -1,    14,    -1,    15,    -1,
      16,    -1,    17,    -1,   227,   107,   227,    -1,   227,   106,
     227,    -1,   227,   109,   227,    -1,   227,   108,   227,    -1,
     227,   103,   227,    -1,   227,   101,   227,    -1,   227,   102,
     227,    -1,   227,   105,   227,    -1,   227,   104,   227,    -1,
     227,   100,   227,    -1,   227,    99,   227,    -1,   106,   227,
      -1,   107,   227,    -1,   111,   227,    -1,   110,   227,    -1,
     215,    32,    -1,   231,    -1,    -1,    66,   232,   233,    -1,
     256,    -1,   234,    -1,   233,    34,   234,    -1,   237,   246,
      -1,   237,   235,   237,   246,    -1,    64,    -1,    63,    -1,
      65,    -1,    61,    -1,    44,    -1,    45,    -1,    62,    -1,
      68,    -1,   256,    -1,   213,    -1,    70,    32,   196,    33,
      -1,   213,    70,    32,   196,    33,    -1,   256,    -1,   238,
     240,    35,    -1,   238,   240,    38,    -1,    32,   196,    33,
      35,    -1,    32,   196,    33,    38,    -1,    46,   221,    35,
      -1,    66,   241,    -1,   256,    -1,   242,    -1,   241,    34,
     242,    -1,   246,    32,    -1,    84,   200,    85,    -1,   256,
      -1,     3,    -1,   251,    -1,   244,    -1,   256,    -1,   248,
     247,    -1,    90,   113,   248,   247,    -1,   248,    -1,    90,
     113,   248,    -1,    94,    -1,    90,   113,    94,    -1,   113,
     248,   247,    -1,   113,   248,    -1,   113,    94,    -1,    92,
     248,    -1,     3,   243,    -1,     3,   250,    -1,    90,   113,
       3,   250,    -1,     3,    -1,    90,   113,     3,    -1,    94,
      -1,    90,   113,    94,    -1,   113,     3,   250,    -1,   113,
       3,    -1,   113,    94,    -1,    92,     3,    -1,   251,     6,
      -1,     6,    -1,   251,    -1,    38,    -1,     4,    -1,    32,
     254,    33,    -1,   256,    -1,   244,    46,   255,    -1,   244,
      46,   255,    34,   254,    -1,   244,    -1,   244,    34,   254,
      -1,   244,    46,   126,    -1,   244,    46,   126,    34,   254,
      -1,   251,    -1,   228,    -1,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short yyrline[] =
{
       0,  1386,  1386,  1398,  1401,  1404,  1407,  1412,  1417,  1422,
    1423,  1424,  1425,  1426,  1438,  1454,  1464,  1465,  1466,  1467,
    1468,  1469,  1470,  1471,  1472,  1473,  1474,  1475,  1476,  1477,
    1478,  1479,  1480,  1481,  1482,  1483,  1484,  1491,  1491,  1563,
    1573,  1584,  1603,  1625,  1636,  1645,  1664,  1670,  1676,  1681,
    1692,  1699,  1703,  1708,  1717,  1729,  1742,  1742,  1769,  1770,
    1777,  1797,  1824,  1828,  1838,  1843,  1858,  1885,  1890,  1903,
    1909,  1935,  1941,  1948,  1949,  1952,  1953,  1961,  1972,  2017,
    2028,  2031,  2058,  2063,  2068,  2073,  2080,  2085,  2090,  2095,
    2102,  2103,  2104,  2107,  2112,  2122,  2159,  2160,  2189,  2201,
    2209,  2222,  2244,  2250,  2254,  2257,  2265,  2270,  2282,  2292,
    2532,  2542,  2549,  2550,  2554,  2554,  2585,  2639,  2643,  2663,
    2669,  2675,  2681,  2687,  2700,  2715,  2726,  2795,  2843,  2844,
    2845,  2846,  2847,  2848,  2854,  2854,  3047,  3047,  3142,  3143,
    3155,  3175,  3175,  3405,  3411,  3414,  3417,  3420,  3423,  3428,
    3460,  3467,  3494,  3494,  3521,  3521,  3542,  3569,  3584,  3584,
    3594,  3595,  3595,  3615,  3616,  3631,  3632,  3633,  3634,  3635,
    3636,  3637,  3638,  3639,  3640,  3641,  3642,  3643,  3644,  3645,
    3646,  3655,  3677,  3695,  3730,  3744,  3761,  3779,  3786,  3793,
    3801,  3824,  3824,  3852,  3852,  3882,  3882,  3900,  3901,  3907,
    3910,  3914,  3917,  3918,  3919,  3920,  3921,  3922,  3923,  3924,
    3927,  3932,  3939,  3947,  3955,  3966,  3972,  3973,  3981,  3982,
    3983,  3984,  3985,  3992,  4003,  4011,  4014,  4018,  4022,  4032,
    4037,  4045,  4058,  4066,  4069,  4073,  4077,  4103,  4109,  4117,
    4128,  4149,  4158,  4166,  4176,  4180,  4184,  4191,  4208,  4225,
    4233,  4241,  4250,  4254,  4263,  4274,  4286,  4296,  4309,  4316,
    4324,  4340,  4348,  4359,  4370,  4381,  4400,  4408,  4425,  4433,
    4440,  4451,  4462,  4473,  4492,  4498,  4504,  4511,  4520,  4523,
    4532,  4539,  4546,  4556,  4567,  4578,  4589,  4596,  4603,  4606,
    4623,  4633,  4640,  4646,  4651,  4657,  4661,  4667,  4668,  4669,
    4675,  4681,  4685,  4688,  4691,  4692,  4693,  4694,  4695,  4696,
    4701,  4704,  4709,  4734,  4737,  4791,  4795,  4799,  4803,  4807,
    4811,  4815,  4819,  4823,  4829,  4829,  4850,  4868,  4869,  4872,
    4885,  4893,  4899,  4912,  4915,  4924,  4935,  4936,  4940,  4945,
    4946,  4965,  4972,  4978,  4985,  4992,  5002,  5003,  5004,  5005,
    5006,  5007,  5008,  5011,  5015,  5019,  5023,  5027,  5031,  5035,
    5039,  5043,  5047,  5051,  5055,  5059,  5063,  5067,  5071,  5083,
    5088,  5088,  5089,  5092,  5103,  5112,  5125,  5138,  5139,  5140,
    5144,  5148,  5152,  5156,  5162,  5163,  5166,  5171,  5176,  5181,
    5188,  5195,  5202,  5210,  5218,  5226,  5227,  5230,  5231,  5234,
    5240,  5246,  5249,  5250,  5253,  5254,  5257,  5262,  5266,  5269,
    5272,  5275,  5280,  5284,  5287,  5294,  5300,  5309,  5314,  5318,
    5321,  5324,  5327,  5332,  5336,  5339,  5342,  5348,  5353,  5356,
    5359,  5363,  5368,  5381,  5385,  5390,  5396,  5400,  5405,  5409,
    5416,  5419,  5424
};
#endif

#if YYDEBUG || YYERROR_VERBOSE
/* YYTNME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "ID", "HBLOCK", "POUND", "STRING", 
  "INCLUDE", "IMPORT", "INSERT", "CHARCONST", "NUM_INT", "NUM_FLOAT", 
  "NUM_UNSIGNED", "NUM_LONG", "NUM_ULONG", "NUM_LONGLONG", 
  "NUM_ULONGLONG", "TYPEDEF", "TYPE_INT", "TYPE_UNSIGNED", "TYPE_SHORT", 
  "TYPE_LONG", "TYPE_FLOAT", "TYPE_DOUBLE", "TYPE_CHAR", "TYPE_VOID", 
  "TYPE_SIGNED", "TYPE_BOOL", "TYPE_COMPLEX", "TYPE_TYPEDEF", "TYPE_RAW", 
  "LPAREN", "RPAREN", "COMMA", "SEMI", "EXTERN", "INIT", "LBRACE", 
  "RBRACE", "PERIOD", "CONST_QUAL", "VOLATILE", "REGISTER", "STRUCT", 
  "UNION", "EQUAL", "SIZEOF", "MODULE", "LBRACKET", "RBRACKET", "ILLEGAL", 
  "CONSTANT", "NAME", "RENAME", "NAMEWARN", "EXTEND", "PRAGMA", "FEATURE", 
  "VARARGS", "ENUM", "CLASS", "TYPENAME", "PRIVATE", "PUBLIC", 
  "PROTECTED", "COLON", "STATIC", "VIRTUAL", "FRIEND", "THROW", "CATCH", 
  "USING", "NAMESPACE", "NATIVE", "INLINE", "TYPEMAP", "EXCEPT", "ECHO", 
  "APPLY", "CLEAR", "SWIGTEMPLATE", "FRAGMENT", "WARN", "LESSTHAN", 
  "GREATERTHAN", "MODULO", "DELETE_KW", "TYPES", "PARMS", "NONID", 
  "DSTAR", "DCNOT", "TEMPLATE", "OPERATOR", "COPERATOR", "PARSETYPE", 
  "PARSEPARM", "CAST", "LOR", "LAND", "OR", "XOR", "AND", "RSHIFT", 
  "LSHIFT", "MINUS", "PLUS", "SLASH", "STAR", "LNOT", "NOT", "UMINUS", 
  "DCOLON", "$accept", "program", "interface", "declaration", 
  "swig_directive", "extend_directive", "@1", "apply_directive", 
  "clear_directive", "constant_directive", "echo_directive", 
  "except_directive", "stringtype", "fname", "fragment_directive", 
  "include_directive", "@2", "includetype", "inline_directive", 
  "insert_directive", "module_directive", "name_directive", 
  "native_directive", "pragma_directive", "pragma_arg", "pragma_lang", 
  "rename_directive", "rename_namewarn", "feature_directive", 
  "stringbracesemi", "featattr", "varargs_directive", "varargs_parms", 
  "typemap_directive", "typemap_type", "tm_list", "tm_tail", 
  "typemap_parm", "types_directive", "template_directive", 
  "warn_directive", "c_declaration", "@3", "c_decl", "c_decl_tail", 
  "initializer", "c_enum_forward_decl", "c_enum_decl", 
  "c_constructor_decl", "cpp_declaration", "cpp_class_decl", "@4", "@5", 
  "cpp_opt_declarators", "cpp_forward_class_decl", "cpp_template_decl", 
  "@6", "cpp_temp_possible", "template_parms", "cpp_using_decl", 
  "cpp_namespace_decl", "@7", "@8", "cpp_members", "@9", "@10", 
  "cpp_member", "cpp_constructor_decl", "cpp_destructor_decl", 
  "cpp_conversion_operator", "cpp_catch_decl", "cpp_protection_decl", 
  "cpp_nested", "@11", "@12", "@13", "nested_decl", "cpp_swig_directive", 
  "cpp_end", "cpp_vend", "anonymous_bitfield", "storage_class", "parms", 
  "rawparms", "ptail", "parm", "valparms", "rawvalparms", "valptail", 
  "valparm", "def_args", "parameter_declarator", 
  "typemap_parameter_declarator", "declarator", "notso_direct_declarator", 
  "direct_declarator", "abstract_declarator", 
  "direct_abstract_declarator", "pointer", "type_qualifier", 
  "type_qualifier_raw", "type", "rawtype", "type_right", "primitive_type", 
  "primitive_type_list", "type_specifier", "definetype", "@14", "ename", 
  "enumlist", "edecl", "etype", "expr", "exprnum", "exprcompound", 
  "inherit", "raw_inherit", "@15", "base_list", "base_specifier", 
  "access_specifier", "cpptype", "opt_virtual", "cpp_const", "ctor_end", 
  "ctor_initializer", "mem_initializer_list", "mem_initializer", 
  "template_decl", "idstring", "idstringopt", "idcolon", "idcolontail", 
  "idtemplate", "idcolonnt", "idcolontailnt", "string", "stringbrace", 
  "options", "kwargs", "stringnum", "empty", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned short yyr1[] =
{
       0,   114,   115,   115,   115,   115,   115,   116,   116,   117,
     117,   117,   117,   117,   117,   117,   118,   118,   118,   118,
     118,   118,   118,   118,   118,   118,   118,   118,   118,   118,
     118,   118,   118,   118,   118,   118,   118,   120,   119,   121,
     122,   123,   123,   123,   124,   124,   125,   125,   125,   125,
     126,   127,   127,   128,   128,   128,   130,   129,   131,   131,
     132,   132,   133,   133,   133,   133,   134,   135,   135,   136,
     136,   137,   137,   138,   138,   139,   139,   140,   140,   140,
     141,   141,   142,   142,   142,   142,   142,   142,   142,   142,
     143,   143,   143,   144,   144,   145,   146,   146,   147,   147,
     147,   148,   149,   150,   150,   151,   151,   151,   152,   153,
     154,   155,   155,   155,   156,   155,   157,   158,   158,   158,
     159,   159,   159,   159,   160,   161,   161,   162,   163,   163,
     163,   163,   163,   163,   165,   164,   166,   164,   167,   167,
     168,   170,   169,   169,   171,   171,   171,   171,   171,   172,
     173,   173,   175,   174,   176,   174,   174,   177,   178,   177,
     177,   179,   177,   180,   180,   180,   180,   180,   180,   180,
     180,   180,   180,   180,   180,   180,   180,   180,   180,   180,
     180,   181,   182,   182,   183,   183,   183,   184,   185,   185,
     185,   187,   186,   188,   186,   189,   186,   190,   190,   191,
     191,   191,   191,   191,   191,   191,   191,   191,   191,   191,
     192,   192,   193,   193,   193,   194,   195,   195,   195,   195,
     195,   195,   195,   196,   197,   197,   198,   198,   199,   199,
     199,   200,   201,   201,   202,   202,   203,   203,   203,   204,
     204,   204,   204,   204,   205,   205,   205,   206,   206,   206,
     207,   207,   207,   207,   207,   207,   207,   207,   208,   208,
     208,   208,   208,   208,   208,   208,   209,   209,   209,   209,
     209,   209,   209,   209,   210,   210,   210,   210,   210,   210,
     210,   210,   210,   210,   211,   211,   211,   211,   211,   211,
     211,   212,   212,   212,   212,   213,   213,   214,   214,   214,
     215,   216,   216,   217,   217,   217,   217,   217,   217,   217,
     217,   217,   218,   219,   219,   220,   220,   220,   220,   220,
     220,   220,   220,   220,   222,   221,   221,   223,   223,   224,
     224,   225,   225,   225,   226,   226,   227,   227,   227,   227,
     227,   227,   227,   227,   227,   227,   228,   228,   228,   228,
     228,   228,   228,   229,   229,   229,   229,   229,   229,   229,
     229,   229,   229,   229,   229,   229,   229,   229,   229,   230,
     232,   231,   231,   233,   233,   234,   234,   235,   235,   235,
     236,   236,   236,   236,   237,   237,   238,   238,   238,   238,
     239,   239,   239,   239,   239,   240,   240,   241,   241,   242,
     243,   243,   244,   244,   245,   245,   246,   246,   246,   246,
     246,   246,   247,   247,   247,   247,   248,   249,   249,   249,
     249,   249,   249,   250,   250,   250,   250,   251,   251,   252,
     252,   252,   253,   253,   254,   254,   254,   254,   254,   254,
     255,   255,   256
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     3,     2,     3,     2,     2,     1,     1,
       1,     1,     1,     1,     1,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     0,     7,     5,
       3,     5,     5,     3,     2,     2,     5,     2,     5,     2,
       4,     1,     1,     7,     7,     5,     0,     7,     1,     1,
       2,     2,     1,     5,     5,     5,     3,     4,     3,     7,
       8,     5,     3,     1,     1,     3,     1,     4,     7,     6,
       1,     1,     7,     9,     8,    10,     5,     7,     6,     8,
       1,     1,     5,     4,     5,     7,     1,     3,     6,     6,
       8,     1,     2,     3,     1,     2,     3,     6,     5,     9,
       2,     1,     1,     1,     0,     6,     5,     1,     4,     1,
       1,     2,     5,     6,     4,     7,     8,     6,     1,     1,
       1,     1,     1,     1,     0,     9,     0,     8,     1,     2,
       4,     0,     6,     3,     1,     1,     1,     1,     1,     1,
       3,     4,     0,     6,     0,     5,     5,     2,     0,     6,
       1,     0,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     6,     6,     7,     8,     8,     7,     5,     2,     2,
       2,     0,     7,     0,     6,     0,     8,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       2,     2,     2,     4,     2,     5,     1,     2,     1,     1,
       1,     1,     1,     1,     2,     1,     3,     1,     2,     6,
       3,     1,     2,     1,     3,     1,     1,     1,     1,     2,
       3,     2,     2,     1,     2,     2,     1,     1,     1,     1,
       2,     3,     1,     2,     3,     4,     5,     4,     1,     2,
       3,     4,     5,     3,     4,     4,     1,     2,     4,     4,
       5,     3,     4,     4,     1,     2,     2,     3,     1,     2,
       1,     2,     3,     4,     3,     4,     2,     3,     3,     4,
       3,     3,     2,     2,     1,     1,     2,     1,     1,     1,
       1,     2,     1,     1,     1,     1,     2,     2,     1,     2,
       1,     2,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     0,     2,     1,     1,     1,     3,
       1,     1,     3,     1,     1,     1,     1,     1,     5,     1,
       1,     3,     4,     5,     5,     6,     1,     1,     1,     1,
       1,     1,     1,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     2,     2,     2,     2,     2,     1,
       0,     3,     1,     1,     3,     2,     4,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     4,     5,     1,
       3,     3,     4,     4,     3,     2,     1,     1,     3,     2,
       3,     1,     1,     1,     1,     1,     2,     4,     1,     3,
       1,     3,     3,     2,     2,     2,     2,     2,     4,     1,
       3,     1,     3,     3,     2,     2,     2,     2,     1,     1,
       1,     1,     3,     1,     3,     5,     1,     3,     3,     5,
       1,     1,     0
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned short yydefact[] =
{
     442,     0,     0,     0,     0,     8,     4,   442,   315,   322,
     316,   317,   319,   320,   318,   305,   321,   304,   323,   442,
     308,     0,   297,   298,   299,   381,   382,     0,   380,   383,
       0,     0,   410,     0,     0,   295,   442,   302,   303,   312,
     313,     0,   310,   408,     6,     0,     1,    13,    62,    58,
      59,     0,   219,    12,   216,   442,     0,     0,    80,    81,
     442,   442,     0,     0,   218,   220,   221,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     7,     9,    16,    17,    18,    19,    20,    21,
      22,    23,   442,    24,    25,    26,    27,    28,    29,    30,
       0,    31,    32,    33,    34,    35,    36,    10,   111,   113,
     112,    14,    11,   128,   129,   130,   131,   132,   133,     0,
     222,   442,   416,   401,   306,     0,   307,     0,     0,     3,
     301,   296,   442,   324,     0,     0,   280,   294,     0,   246,
     228,   442,   252,   442,   278,   274,   266,   243,   309,   314,
     311,     0,     0,   406,     5,    15,     0,   428,   217,     0,
       0,   433,     0,   442,     0,   300,     0,     0,     0,     0,
      76,     0,   442,   442,     0,     0,   442,   154,     0,     0,
      60,    61,     0,     0,    49,    47,    44,    45,   442,     0,
     442,     0,   442,   442,     0,   110,   442,   442,     0,     0,
       0,     0,     0,     0,   266,   442,     0,     0,   238,   346,
     347,   348,   349,   350,   351,   352,   236,     0,   231,   442,
     237,   233,   230,   411,   409,     0,   442,   280,     0,   223,
     442,     0,   274,   310,   225,   326,   241,     0,   239,     0,
       0,     0,   286,     0,     0,     0,     0,   340,     0,   336,
     339,   337,   242,   442,     0,   253,   279,   258,   292,   293,
     267,   244,   442,     0,   245,   442,     0,   276,   250,   275,
     258,   281,   415,   414,   413,   402,     0,   403,   427,   114,
     436,     0,    66,    43,   324,     0,   442,    68,     0,     0,
       0,    72,     0,     0,     0,    96,     0,     0,   150,     0,
     442,   152,     0,     0,   101,     0,     0,     0,   105,   247,
     248,   249,    40,     0,   102,   104,   404,     0,   405,    52,
       0,    51,     0,     0,   149,   143,     0,   442,     0,     0,
       0,     0,     0,     0,     0,   258,     0,   442,     0,   328,
     442,   442,   136,   311,   400,     0,   232,   235,   407,     0,
     280,   274,   310,     0,   266,   290,     0,   224,   227,   288,
     276,     0,   266,   281,   240,   325,     0,     0,   364,   365,
     367,   366,   368,   287,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   274,   310,   259,   442,     0,
     291,     0,   271,     0,     0,   284,     0,   251,   277,   282,
       0,   254,   412,     0,   442,     0,     0,   432,     0,     0,
      67,    37,    75,     0,     0,     0,     0,     0,     0,     0,
     151,     0,     0,   442,   442,     0,     0,   106,     0,   442,
       0,     0,     0,     0,     0,   141,    56,     0,     0,     0,
       0,    77,     0,   124,   442,     0,   310,     0,     0,   120,
     442,     0,   140,   370,     0,   369,   372,   442,     0,     0,
     281,   269,   442,   268,   282,     0,   341,     0,   294,     0,
     442,   363,   362,   358,   359,   357,   361,   360,   354,   353,
     356,   355,     0,   258,   260,   281,     0,   263,     0,   273,
     272,   289,   285,     0,   255,   283,   257,    64,    65,    63,
       0,   437,   438,   441,   440,   434,    41,    42,     0,    74,
      71,    73,   431,    91,   430,     0,    86,   442,   429,    90,
       0,   440,     0,     0,    97,   442,   187,   156,   155,     0,
     216,     0,     0,    48,    46,   442,    39,   103,   419,     0,
     421,     0,    55,     0,     0,   108,   442,   442,   442,     0,
       0,   331,     0,   330,   333,   442,   442,     0,   117,   119,
     116,     0,   121,   161,   180,     0,     0,     0,     0,   220,
       0,   207,   208,   200,   209,   178,   205,   201,   199,   202,
     203,   204,   206,   179,   175,   176,   163,   169,   173,   172,
       0,     0,   164,   165,   168,   174,   166,   170,   167,   177,
       0,   222,   442,   134,   234,   229,   226,   270,   342,     0,
     293,     0,     0,     0,   261,     0,   265,   264,   256,   115,
       0,     0,     0,   442,     0,   386,     0,   389,     0,     0,
       0,     0,    88,   442,     0,   153,   217,   442,     0,    99,
       0,    98,     0,     0,     0,   417,     0,   442,     0,    50,
     144,   145,   148,   147,   142,   146,     0,     0,     0,    79,
       0,   442,     0,   442,   324,   442,   127,     0,   442,   442,
       0,   158,   189,   188,   190,     0,     0,     0,   157,     0,
       0,     0,   310,   384,   371,   373,     0,   385,     0,   344,
     343,     0,   338,   262,   439,   435,    38,     0,   442,     0,
      82,   440,    93,    87,   442,     0,     0,    95,    69,     0,
       0,   107,   426,   424,   425,   420,   422,     0,    53,    54,
       0,    57,    78,   335,   332,   334,   329,   125,     0,     0,
       0,     0,     0,   396,   442,     0,     0,   162,     0,     0,
     442,     0,     0,   442,     0,   442,   193,   311,   171,   442,
     378,   377,   379,   442,   375,     0,   345,     0,     0,   442,
      94,     0,    89,   442,    84,    70,   100,   423,   418,     0,
     126,     0,   394,   395,   397,     0,   390,   391,   122,   118,
     442,     0,   442,     0,   137,   442,     0,     0,     0,     0,
     191,   442,   442,   374,     0,     0,    92,   387,     0,    83,
       0,   109,   392,   393,     0,   399,   123,     0,     0,   442,
       0,   442,   442,   442,   215,   442,     0,   197,   198,     0,
     376,   138,   135,     0,   388,    85,   398,   159,   442,   182,
       0,   442,     0,     0,   181,     0,   194,   195,   139,   183,
       0,   210,   211,   186,   442,   442,   192,     0,   212,   214,
     324,   185,   184,   196,     0,   213
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short yydefgoto[] =
{
      -1,     3,     4,    82,    83,    84,   508,   571,   572,   573,
     574,    89,   319,   320,   575,    91,   547,    92,    93,   576,
      95,   577,    97,   578,   510,   169,   579,   100,   580,   516,
     416,   581,   294,   582,   303,   191,   314,   192,   583,   584,
     585,   586,   404,   108,   560,   448,   109,   110,   111,   112,
     113,   688,   451,   822,   587,   588,   546,   654,   323,   589,
     117,   423,   300,   590,   738,   670,   591,   592,   593,   594,
     595,   596,   597,   815,   791,   847,   816,   598,   829,   839,
     599,   600,   228,   229,   357,   230,   217,   218,   346,   219,
     139,   140,   308,   341,   255,   142,   231,   144,   203,    34,
      35,   247,   165,    37,    38,    39,    40,   238,   239,   338,
     552,   553,   724,   475,   249,   250,   454,   455,   602,   684,
     685,   753,    41,   686,   840,   666,   732,   773,   774,   122,
     280,   317,    42,   153,    43,   541,   645,   251,   519,   160,
     281,   505,   234
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -697
static const short yypact[] =
{
     331,  2714,  2769,    62,  2279,  -697,  -697,   -11,  -697,  -697,
    -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,   -11,
    -697,   124,  -697,  -697,  -697,  -697,  -697,   148,  -697,  -697,
     -37,   129,  -697,   169,  3260,   538,   877,   538,  -697,  -697,
    1648,   148,  -697,   -41,  -697,   192,  -697,   160,  -697,  -697,
    -697,    52,  -697,  -697,   276,   234,  2823,   274,  -697,  -697,
     234,   304,   347,   359,  -697,  -697,  -697,   367,    38,    80,
     375,   127,   383,   480,   233,  3084,  3084,   388,   404,   276,
     408,   381,  -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,
    -697,  -697,   234,  -697,  -697,  -697,  -697,  -697,  -697,  -697,
     952,  -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,
    -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,  3128,
    -697,  2952,  -697,  -697,  -697,   407,  -697,    35,   427,  -697,
     538,  -697,  2052,    19,  1122,  1997,   729,   287,   148,  -697,
    -697,   133,   215,   133,   263,   262,   362,  -697,  -697,  -697,
    -697,   457,    56,  -697,  -697,  -697,   471,  -697,   102,   471,
     471,  -697,   441,    29,  1099,  -697,   405,   148,   496,   500,
    -697,   471,  2996,  3040,   148,   478,    89,  -697,   493,   535,
    -697,  -697,   471,   542,  -697,  -697,  -697,   543,  3040,   519,
     229,   531,   554,   471,   276,   543,  3040,  3040,   148,   276,
      43,   197,   471,   294,   510,   282,  1153,   120,  -697,  -697,
    -697,  -697,  -697,  -697,  -697,  -697,  -697,   523,  -697,   586,
    -697,  -697,  -697,  -697,   -41,   540,  2107,   902,   594,  -697,
     589,   599,   871,   557,  -697,  -697,  -697,  1099,  -697,  1997,
    1997,   613,  -697,  1997,  1997,  1997,  1997,   618,  1557,  -697,
    -697,   543,  1462,  2107,   148,   293,   263,  -697,  -697,   560,
    -697,  -697,  3040,  1385,  -697,  3040,  1487,   729,   293,   263,
     575,   687,  -697,  -697,   -41,  -697,   639,   543,  -697,  -697,
      20,   643,  -697,  -697,   672,    66,   133,  -697,   650,   647,
     653,   645,   585,   658,   662,  -697,   667,   671,  -697,   148,
    -697,  -697,   669,   674,  -697,   681,   690,  3084,  -697,  -697,
    -697,  -697,  -697,  3084,  -697,  -697,  -697,   691,  -697,  -697,
     597,   183,   696,   648,  -697,  -697,    71,   -21,   213,   213,
     697,   644,    77,   704,   197,   652,   687,    55,   708,  -697,
    2151,  1193,  -697,   238,  -697,  2952,  -697,  -697,  -697,   427,
     312,   260,   656,   360,  -697,  -697,  3040,  -697,  -697,  -697,
     312,   435,   659,   213,  -697,  1462,   970,  3172,  -697,  -697,
    -697,  -697,  -697,  -697,  1997,  1997,  1997,  1997,  1997,  1997,
    1997,  1997,  1997,  1997,  1997,  1188,     4,  -697,  3040,  1589,
    -697,   719,  -697,  1640,   726,  -697,  1659,   293,   263,  1189,
     197,   293,  -697,   202,  -697,   471,  1255,  -697,   734,   735,
    -697,  -697,  -697,   590,   843,  1447,   731,  3040,  1099,   733,
    -697,   738,  2367,  -697,  1183,  3084,   255,   743,   737,   554,
     379,   744,   471,  3040,   750,  -697,  -697,   866,   213,   197,
       9,  -697,   762,  -697,   790,   766,   644,   763,   283,  -697,
     348,  1292,  -697,  -697,   764,  -697,  -697,   586,   148,   659,
    -697,  -697,   589,  -697,   312,   447,  1997,  1691,  1793,    -9,
     877,  1482,  1335,  1316,  1181,  1090,   603,   603,   544,   544,
    -697,  -697,   502,   659,  -697,   197,   772,  -697,  1742,  -697,
    -697,  -697,  -697,   197,   293,   263,   293,  -697,  -697,   543,
    2455,  -697,   776,  -697,   183,   780,  -697,  -697,  1292,  -697,
    -697,   543,  -697,  -697,  -697,   774,  -697,   292,   543,  -697,
     770,    92,   630,   843,  -697,   292,  -697,  -697,  -697,  2543,
     276,  3216,   878,  -697,  -697,  3040,  -697,  -697,   -14,   711,
    -697,   741,  -697,   796,   799,  -697,   606,  -697,   292,   174,
     197,   793,   400,  -697,  -697,  1011,  3040,  1099,  -697,  -697,
    -697,   809,  -697,  -697,  -697,   804,   784,   787,   788,   746,
     457,  -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,
    -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,
     816,  1292,  -697,  -697,  -697,  -697,  -697,  -697,  -697,  -697,
    2867,   819,   792,  -697,  -697,  -697,  -697,  -697,  1462,  1997,
    2202,  1997,   828,   829,  -697,   509,  -697,  -697,   293,  -697,
     471,   471,   825,  3040,   834,   797,   275,  -697,  1255,   882,
     471,   835,  -697,   292,   848,  -697,   543,    21,  1099,  -697,
    3084,  -697,   853,   884,    67,  -697,    78,  2952,   144,  -697,
    -697,  -697,  -697,  -697,  -697,  -697,  3172,  2631,   855,  -697,
    1895,   790,   907,  3040,   672,   830,  -697,   867,  1193,  3040,
    1292,  -697,  -697,  -697,  -697,   457,   869,  1099,  -697,  3172,
     955,   163,   872,  -697,   868,  -697,   430,  -697,  1292,  1462,
    1462,  1997,  -697,  -697,  -697,  -697,  -697,   873,  3040,   876,
    -697,   543,   870,  -697,   292,  1009,   275,  -697,  -697,   883,
     886,  -697,  -697,   -14,  -697,   -14,  -697,   826,  -697,  -697,
    1362,  -697,  -697,  -697,  -697,  1462,  -697,  -697,   283,   889,
     890,   148,   443,  -697,   133,   283,   894,  -697,  1292,   887,
    3040,   283,     3,  2151,  1997,    -5,  -697,   162,  -697,   792,
    -697,  -697,  -697,   792,  -697,   891,  1462,   893,   896,  3040,
    -697,   900,  -697,   292,  -697,  -697,  -697,  -697,  -697,   901,
    -697,   466,  -697,   904,  -697,   908,  -697,  -697,  -697,  -697,
     133,   905,  3040,   912,  -697,  3040,   909,   918,   924,  1451,
    -697,  1099,   792,  -697,   148,  1065,  -697,  -697,   926,  -697,
     929,  -697,  -697,  -697,   148,  -697,  -697,  1292,   933,   292,
     935,  3040,  3040,  1011,  -697,  1099,   943,  -697,  -697,   164,
    -697,  -697,  -697,   283,  -697,  -697,  -697,  -697,   292,  -697,
     518,   292,   946,   948,  -697,   954,  -697,  -697,  -697,  -697,
     339,  -697,  -697,  -697,   292,   292,  -697,   959,  -697,  -697,
     672,  -697,  -697,  -697,   960,  -697
};

/* YYPGOTO[NTERM-NUM].  */
static const short yypgoto[] =
{
    -697,  -697,  -278,  -697,  -697,  -697,  -697,    30,    39,    44,
      60,  -697,   577,  -697,    61,  -697,  -697,  -697,  -697,    63,
    -697,    64,  -697,    70,  -697,  -697,    83,  -697,    84,  -510,
    -517,    85,  -697,    87,  -697,  -281,   561,   -54,    97,   100,
     105,   110,  -697,   453,  -696,   332,  -697,  -697,  -697,  -697,
     458,  -697,  -697,  -697,    23,    36,  -697,  -697,  -697,   111,
    -697,  -697,  -697,  -437,  -697,  -697,  -697,   460,  -697,  -697,
     115,  -697,  -697,  -697,  -697,  -697,   193,  -697,  -697,  -444,
    -697,    -3,   463,   810,   547,    13,   364,  -697,   558,   675,
     -91,   552,  -697,   158,   563,  -204,   -33,  -125,    46,   -27,
    -697,   291,    15,   -32,  -697,   974,  -697,  -275,  -697,  -697,
    -697,   356,  -697,   649,   -25,  -697,  -697,  -697,  -697,   235,
     285,  -697,    49,   284,  -142,   222,  -697,  -697,   232,  1020,
      96,  -697,    17,  -177,  -121,  -697,  -106,   677,   508,   223,
    -157,  -397,     0
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -443
static const short yytable[] =
{
       5,   119,   130,   143,   120,   631,   224,   123,   131,   408,
     148,   256,  -402,   632,    33,    45,    36,    36,   522,   123,
     269,   189,   422,   353,   611,   304,   428,   114,   361,   235,
     272,   274,   770,   790,    85,   785,   147,   484,     7,   779,
     115,     7,   484,    86,   126,   784,   327,   348,    87,   157,
     261,   151,   264,   146,   405,   161,   708,   236,   150,     7,
     161,   170,    46,   121,    88,    90,   406,    94,    96,     7,
     713,   622,   152,   121,    98,   284,   127,   278,   643,   121,
       7,   715,   145,   176,   156,   175,   178,    99,   101,   102,
     443,   103,   161,  -327,   612,   485,   220,   402,   278,   644,
     550,   104,   256,   148,   105,   121,   786,   269,   278,   106,
     259,   174,   137,   121,   107,   116,   700,   204,   177,   118,
     436,   221,   237,     7,   353,   361,   500,   838,    30,   223,
     198,   180,    32,    30,   216,   299,    36,    32,  -403,   121,
     279,   147,   398,   147,   532,   529,   328,    36,   718,   233,
     273,     7,   137,   257,   678,   260,    30,   310,   342,   465,
      32,   714,   270,   123,   125,   181,   745,    30,   207,   328,
      30,    32,   716,   121,    32,   137,   123,   225,   232,   133,
     278,   204,   719,   258,   289,   760,   137,    36,    36,   278,
     311,   297,   315,   318,   141,   409,   764,   452,   749,   135,
       7,   746,   837,    36,   129,   339,   497,   146,   157,   659,
      30,    36,    36,   128,    32,   325,     7,   331,   257,   347,
     335,   433,   126,   204,   343,   256,   269,   154,   792,   332,
     358,   702,     7,   737,   465,   398,   145,   186,    30,   157,
     498,    36,    32,   352,   354,   285,   329,   262,   501,   362,
     449,   755,   276,    30,   204,   155,   282,    32,   202,   429,
     269,   132,   288,     7,   263,     7,   159,   292,    36,   657,
     386,   387,   351,   452,   495,   543,  -442,    36,   134,   512,
      36,   157,   157,   167,   257,   337,   147,    30,   257,   316,
     533,    32,   226,   534,   253,   265,   330,     7,   333,   385,
       5,   781,   331,    30,   453,   390,   166,    32,   254,   134,
     513,   134,   266,   514,   450,   199,   421,   557,   558,    30,
     220,   559,   286,    32,   138,   388,   332,   123,    22,    23,
      24,   329,   136,    22,    23,    24,   168,   123,   137,   495,
     138,   147,   389,   456,   226,   354,   354,   164,   309,   440,
      30,   257,    30,   257,    32,    36,    32,   446,   216,   562,
      36,   134,   624,   360,   515,   267,   190,   190,   459,   462,
     827,    36,    30,   254,   848,   626,    32,   849,   439,   171,
     354,   503,   538,   634,    30,   850,   329,   843,    32,   730,
     503,   172,   262,   461,   133,   364,   137,   334,   458,   173,
     851,   852,   483,    36,     5,   254,   658,   179,   275,   263,
     206,   157,   469,   665,   135,   182,   257,   257,   561,   119,
     193,   531,   120,     5,   120,    25,    26,     1,     2,   315,
     524,   204,    36,     7,   661,   204,   194,   143,   287,   662,
     196,   610,    28,    29,   554,   114,   544,   222,    36,   676,
     147,   601,    85,   271,   204,   354,   257,   347,   115,   257,
       7,    86,   358,   694,   695,   197,    87,   262,   463,   539,
     147,    25,    26,   540,   275,   605,   283,   157,   776,   262,
     607,   777,    88,    90,   263,    94,    96,   146,    28,    29,
     625,   706,    98,   750,   751,   752,   263,   119,   625,   290,
     120,   802,   257,   291,   803,    99,   101,   102,   601,   103,
     257,   520,   183,   298,   258,   184,   145,   627,   185,   104,
      30,   625,   105,   114,    32,   627,   119,   106,   625,   120,
      85,   301,   107,   116,   388,   614,   115,   118,   302,    86,
     204,   388,   693,   656,    87,   305,   120,     5,   627,   278,
      36,   389,   114,   841,   739,   627,   842,   307,   389,    85,
      88,    90,   761,    94,    96,   115,   312,   257,    86,   652,
      98,    36,   517,    87,   204,   854,   525,   449,   130,    22,
      23,    24,   653,    99,   101,   102,   710,   103,   313,    88,
      90,   601,    94,    96,   509,   548,   157,   104,   190,    98,
     105,   336,   687,   503,   190,   106,   625,   767,   344,   768,
     107,   116,    99,   101,   102,   118,   103,   682,   414,   415,
     345,   800,   220,   356,    52,   349,   104,   355,   141,   105,
     431,   432,   359,   627,   106,   295,   296,   123,    36,   107,
     116,   450,   530,   778,   118,   367,   204,   221,   363,   681,
     372,   306,   383,   384,   119,   204,   390,   120,   470,   322,
     216,   554,    36,   629,   630,   733,   399,   830,   147,   137,
     601,   665,   403,    64,    65,    66,   407,   625,    36,   204,
     114,   633,   235,   410,    36,   411,   412,    85,   601,   806,
       7,   413,   417,   115,   204,   418,    86,   204,   747,    81,
     419,    87,   424,   754,   627,   207,   420,   425,   268,   381,
     382,   383,   384,    36,   426,   668,   190,    88,    90,   332,
      94,    96,   204,   427,   430,   391,   520,    98,   394,   434,
     437,   158,     7,   435,   147,   438,   625,   204,   601,   441,
      99,   101,   102,   442,   103,   123,   444,   460,   775,   687,
     464,   187,   489,   687,   104,    36,   195,   105,    36,   491,
     446,   253,   106,   627,   523,     7,   268,   107,   116,   506,
     507,   526,   118,   527,    36,   535,   536,    30,   134,   542,
     147,    32,   625,   248,   252,   545,   625,   704,   787,   329,
     400,   818,   687,   551,   332,   556,   709,    36,   254,   555,
      36,   625,   603,   445,   625,   616,   623,   601,   204,   627,
     620,   820,   204,   627,   621,   818,   628,   625,   625,    30,
     728,   775,   638,    32,   646,   647,    36,    36,   627,   648,
     397,   627,   204,   277,   401,   741,   277,   277,   649,   660,
     254,   669,   671,   277,   627,   627,     7,   512,   277,   157,
     672,   486,    30,   673,   674,   677,    32,   675,  -160,   277,
     683,   691,   692,   763,   696,   493,   698,   699,   705,     7,
     277,   321,   157,   254,     7,   285,   326,   277,   513,   277,
       7,   514,   512,   707,   157,     7,   711,   712,   365,   366,
     722,   680,   368,   369,   370,   371,   731,   397,   285,   401,
     734,   740,   749,   132,   630,     7,   757,   748,   759,   132,
       7,   769,   393,   639,   285,   396,   514,   703,   765,   782,
     134,   766,   771,   133,   640,   772,   134,   780,   796,   797,
     795,   190,   515,    30,   132,   799,   801,    32,   804,   285,
     805,   811,   727,   135,   807,   809,   201,   720,   482,   817,
     812,   134,   137,   823,   138,     7,    30,   813,     7,   824,
      32,    30,   494,   496,   825,    32,   828,    30,   831,   201,
     742,    32,    30,   817,   360,   137,    32,   138,   836,   844,
     136,   845,   138,   502,   200,   201,   137,   743,   138,   846,
     537,   137,    30,   138,   853,   855,    32,    30,   642,   650,
     735,    32,   482,   466,   651,   494,   655,   324,   835,   606,
     201,   717,     7,   138,   149,   604,   137,   726,   138,   667,
     457,   744,   613,   471,   472,   473,   474,   819,   476,   477,
     478,   479,   480,   481,   793,   834,   826,   794,   488,   124,
     641,   285,    30,   663,   762,    30,    32,     0,   615,    32,
       0,     0,    22,    23,    24,   201,   618,   664,   201,     0,
       0,   137,     0,   138,   137,     0,   138,     0,     7,   374,
     375,   376,   377,   467,   379,   380,   381,   382,   383,   468,
     499,   624,   277,   504,     0,     0,   697,     0,     0,     0,
     511,   518,   521,     0,     0,     0,     0,   285,     0,    30,
     821,     0,     7,    32,     0,     0,     0,     0,     0,   277,
       0,     0,   201,   615,   549,   608,     0,   481,   137,     0,
     138,     0,     0,     0,     0,     7,   729,     0,   157,     0,
       0,   285,   736,   209,   210,   211,   212,   213,   214,   215,
       0,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,   240,    30,     7,     0,     0,    32,
       0,   758,     0,    22,    23,    24,    25,    26,   201,   241,
       0,     0,   242,     0,   137,     0,   138,     0,     0,     0,
       0,     0,    27,    28,    29,   340,     0,     0,     0,    30,
       0,     7,     7,    32,   379,   380,   381,   382,   383,   384,
     518,    52,   201,   783,     0,     0,   788,   636,   137,   518,
     138,     0,    30,     0,     0,     0,    32,     0,     0,   530,
     253,   253,   798,     0,     0,     0,     0,     0,   243,   244,
       0,     0,   245,   246,    22,    23,    24,   134,   134,   133,
       0,     0,     0,    30,     0,   808,     0,    32,   810,     0,
      64,    65,    66,     0,     0,     0,   201,     0,   689,   135,
     690,   157,   137,   447,   138,     0,   209,   210,   211,   212,
     213,   214,   215,     0,   832,   833,     0,     0,    30,    30,
       0,     0,    32,    32,   378,   379,   380,   381,   382,   383,
     384,   360,   493,   563,     0,  -442,    48,   277,   277,   254,
     254,    51,     0,   518,     0,   701,     0,   277,     0,   725,
      52,  -442,  -442,  -442,  -442,  -442,  -442,  -442,  -442,  -442,
    -442,  -442,  -442,  -442,     0,     0,     0,   564,    54,     0,
       0,  -442,     0,  -442,  -442,  -442,  -442,  -442,     0,     0,
     756,     0,     0,     0,    56,    57,    58,    59,   565,    61,
      62,    63,  -442,  -442,  -442,   566,   567,   568,     0,    64,
     569,    66,     0,    67,    68,     7,     0,     0,    72,     0,
      74,    75,    76,    77,    78,    79,     0,     0,     0,     0,
      80,     0,  -442,   518,     0,    81,  -442,  -442,     7,     0,
       0,   157,     0,   789,   743,     0,   209,   210,   211,   212,
     213,   214,   215,   570,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,   240,   377,   378,
     379,   380,   381,   382,   383,   384,    22,    23,    24,    25,
      26,     0,   241,     0,     0,   392,   376,   377,   378,   379,
     380,   381,   382,   383,   384,    27,    28,    29,     0,     0,
     275,     0,    30,   157,     0,     0,    32,     0,   209,   210,
     211,   212,   213,   214,   215,   201,     0,     0,     0,     0,
       0,   137,     0,   138,     0,    30,     0,     0,     0,    32,
       0,     0,     0,     0,     0,     0,   814,     0,     0,     0,
       7,   243,   244,   157,     0,   245,   246,     0,   209,   210,
     211,   212,   213,   214,   215,     0,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,   240,
       0,     0,     0,     0,     0,     0,     0,     0,    22,    23,
      24,    25,    26,     0,   241,     0,     0,   395,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    27,    28,    29,
     374,   375,   376,   377,   378,   379,   380,   381,   382,   383,
     384,   374,   375,   376,   377,   378,   379,   380,   381,   382,
     383,   384,     0,     0,     0,     0,     0,    30,     0,     0,
       0,    32,   375,   376,   377,   378,   379,   380,   381,   382,
     383,   384,     7,   243,   244,   157,     0,   245,   246,     0,
     209,   210,   211,   212,   213,   214,   215,   373,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,   240,     0,     0,     0,     0,     0,     0,     0,     0,
      22,    23,    24,    25,    26,     0,   241,     0,     0,   487,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    27,
      28,    29,     0,     0,     0,     0,   374,   375,   376,   377,
     378,   379,   380,   381,   382,   383,   384,     8,     9,    10,
      11,    12,    13,    14,     0,    16,     0,    18,     0,    30,
       0,     0,     0,    32,     0,     0,     0,     0,     0,     0,
     490,     0,     0,     0,     7,   243,   244,   157,     0,   245,
     246,     0,   209,   210,   211,   212,   213,   214,   215,   492,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,   240,   609,     0,     0,     0,     0,     0,
       0,     0,    22,    23,    24,    25,    26,     0,   241,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
       0,    27,    28,    29,     0,     0,     0,     0,   374,   375,
     376,   377,   378,   379,   380,   381,   382,   383,   384,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    30,     0,     0,     0,    32,     0,     0,     0,     0,
       0,     0,   617,     0,     0,     0,     7,   243,   244,   157,
       0,   245,   246,     0,   209,   210,   211,   212,   213,   214,
     215,     0,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,   240,     0,     0,     0,     0,
       0,     0,     0,     0,    22,    23,    24,    25,    26,     0,
     241,   374,   375,   376,   377,   378,   379,   380,   381,   382,
     383,   384,     0,    27,    28,    29,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    30,     0,     0,     0,    32,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     7,   243,
     244,   157,   137,   245,   246,   723,   209,   210,   211,   212,
     213,   214,   215,     0,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,   240,     0,     0,
       0,     0,     0,     0,     0,     0,    22,    23,    24,    25,
      26,     0,   241,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    27,    28,    29,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    30,     0,     0,     0,    32,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       7,   243,   244,   157,     0,   245,   246,     0,   209,   210,
     211,   212,   213,   214,   215,     0,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,   240,
       0,     0,     0,     0,     0,     0,     0,     0,    22,    23,
      24,    25,    26,     0,   241,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     7,     0,    27,    28,    29,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,   226,     0,     0,    30,     0,     0,
       0,    32,    21,    22,    23,    24,    25,    26,     0,     0,
       0,   134,     0,   243,   244,     0,     0,   245,   246,     0,
       7,     0,    27,    28,    29,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,   226,
       0,     0,    30,     0,     0,    31,    32,    21,    22,    23,
      24,    25,    26,     0,     7,   227,   134,     0,     0,     0,
       0,   137,     0,     0,     0,     0,     0,    27,    28,    29,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,     0,     0,     0,     0,     0,     0,     0,
       0,    21,    22,    23,    24,    25,    26,    30,     0,     0,
      31,    32,     0,     0,     0,     7,     0,     0,     0,     0,
     350,    27,    28,    29,     0,     0,   137,     0,     0,     0,
       0,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,     0,     0,     0,     0,     0,     0,
       0,    30,     0,     0,    31,    32,    25,    26,     0,     0,
       0,     0,     0,     0,   328,     0,     0,     0,     0,     0,
     137,     0,    27,    28,    29,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    -2,
      47,     0,  -442,    48,     0,     0,    49,    50,    51,     0,
       0,     0,    30,     0,     0,     0,    32,    52,  -442,  -442,
    -442,  -442,  -442,  -442,  -442,  -442,  -442,  -442,  -442,  -442,
    -442,   137,     0,     0,    53,    54,     0,     0,     0,     0,
    -442,  -442,  -442,  -442,  -442,     0,     0,    55,     0,     0,
       0,    56,    57,    58,    59,    60,    61,    62,    63,  -442,
    -442,  -442,     0,     0,     0,     0,    64,    65,    66,     0,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,     0,     0,     0,     0,    80,    47,  -442,
    -442,    48,    81,  -442,    49,    50,    51,     0,     0,     0,
       0,     0,     0,     0,     0,    52,  -442,  -442,  -442,  -442,
    -442,  -442,  -442,  -442,  -442,  -442,  -442,  -442,  -442,     0,
       0,     0,    53,    54,     0,     0,   528,     0,  -442,  -442,
    -442,  -442,  -442,     0,     0,    55,     0,     0,     0,    56,
      57,    58,    59,    60,    61,    62,    63,  -442,  -442,  -442,
       0,     0,     0,     0,    64,    65,    66,     0,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,     0,     0,     0,     0,    80,    47,  -442,  -442,    48,
      81,  -442,    49,    50,    51,     0,     0,     0,     0,     0,
       0,     0,     0,    52,  -442,  -442,  -442,  -442,  -442,  -442,
    -442,  -442,  -442,  -442,  -442,  -442,  -442,     0,     0,     0,
      53,    54,     0,     0,   619,     0,  -442,  -442,  -442,  -442,
    -442,     0,     0,    55,     0,     0,     0,    56,    57,    58,
      59,    60,    61,    62,    63,  -442,  -442,  -442,     0,     0,
       0,     0,    64,    65,    66,     0,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,     0,
       0,     0,     0,    80,    47,  -442,  -442,    48,    81,  -442,
      49,    50,    51,     0,     0,     0,     0,     0,     0,     0,
       0,    52,  -442,  -442,  -442,  -442,  -442,  -442,  -442,  -442,
    -442,  -442,  -442,  -442,  -442,     0,     0,     0,    53,    54,
       0,     0,   635,     0,  -442,  -442,  -442,  -442,  -442,     0,
       0,    55,     0,     0,     0,    56,    57,    58,    59,    60,
      61,    62,    63,  -442,  -442,  -442,     0,     0,     0,     0,
      64,    65,    66,     0,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,     0,     0,     0,
       0,    80,    47,  -442,  -442,    48,    81,  -442,    49,    50,
      51,     0,     0,     0,     0,     0,     0,     0,     0,    52,
    -442,  -442,  -442,  -442,  -442,  -442,  -442,  -442,  -442,  -442,
    -442,  -442,  -442,     0,     0,     0,    53,    54,     0,     0,
       0,     0,  -442,  -442,  -442,  -442,  -442,     0,     0,    55,
       0,   721,     0,    56,    57,    58,    59,    60,    61,    62,
      63,  -442,  -442,  -442,     0,     0,     0,     0,    64,    65,
      66,     0,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,     6,     0,     7,     0,    80,
       0,  -442,     0,     0,    81,  -442,     0,     0,     0,     0,
       0,     0,     0,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,     0,     0,     0,     0,
       0,     0,     0,     0,    21,    22,    23,    24,    25,    26,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      44,     0,     7,     0,    27,    28,    29,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,     0,     0,     0,    30,     0,     0,    31,    32,    21,
      22,    23,    24,    25,    26,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   162,     0,   163,     0,     0,    27,
      28,    29,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,     0,     0,     0,     0,    30,
       0,     0,    31,    32,    22,    23,    24,    25,    26,     0,
       7,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    27,    28,    29,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    22,    23,
      24,    25,    26,    30,     0,     0,     0,    32,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   205,    28,    29,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     7,     0,    30,   208,     0,
       0,    32,   679,   209,   210,   211,   212,   213,   214,   215,
       0,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,     0,     0,     0,     0,     0,     0,
       0,     0,    21,    22,    23,    24,    25,    26,     0,     7,
       0,     0,     0,     0,     0,     0,     0,   293,     0,     0,
       0,     0,    27,    28,    29,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,     0,     0,
       0,     0,     0,     0,     0,     0,    21,    22,    23,    24,
      25,    26,    30,     7,     0,    31,    32,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    27,    28,    29,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,     0,     0,     0,     0,     0,     0,     0,     0,
      21,    22,    23,    24,    25,    26,    30,     7,     0,    31,
      32,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      27,    28,    29,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,   188,     0,     0,     0,
       0,     0,     0,     0,     0,    22,    23,    24,    25,    26,
      30,     7,     0,    31,    32,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    27,    28,    29,     8,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    22,
      23,    24,    25,    26,    30,     7,     0,     0,    32,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   205,    28,
      29,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    22,    23,    24,    25,    26,    30,   637,
       0,     0,    32,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    27,    28,    29,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    22,    23,    24,
      25,    26,    30,     7,     0,     0,    32,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    27,    28,    29,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    25,    26,    30,     0,     0,     0,
      32,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      27,    28,    29,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      30,     0,     0,     0,    32
};

static const short yycheck[] =
{
       0,     4,    34,    36,     4,   522,   127,     7,    35,   284,
      37,   136,    33,   523,     1,     2,     1,     2,   415,    19,
     145,    75,   300,   227,    33,   182,   307,     4,   232,    10,
     151,   152,   728,    38,     4,    32,    36,    33,     3,   735,
       4,     3,    33,     4,    27,   741,     3,   224,     4,     6,
     141,    92,   143,    36,    34,    55,    35,    38,    41,     3,
      60,    61,     0,    84,     4,     4,    46,     4,     4,     3,
       3,   508,   113,    84,     4,    46,   113,     6,    92,    84,
       3,     3,    36,     3,    32,    68,    69,     4,     4,     4,
      35,     4,    92,    38,   103,    91,   121,   274,     6,   113,
      91,     4,   227,   130,     4,    84,   103,   232,     6,     4,
     137,    73,   109,    84,     4,     4,   626,   100,    38,     4,
      49,   121,   103,     3,   328,   329,   404,   823,    90,    94,
      81,     4,    94,    90,   121,    46,   121,    94,    46,    84,
      38,   141,   267,   143,   425,   423,   103,   132,     4,   132,
      94,     3,   109,   136,   591,   138,    90,   190,    38,   363,
      94,    94,   145,   163,    40,    38,     3,    90,   119,   103,
      90,    94,    94,    84,    94,   109,   176,   128,   132,    46,
       6,   164,    38,   137,   167,   702,   109,   172,   173,     6,
     190,   174,   192,   193,    36,   286,   706,    35,    34,    66,
       3,    38,    38,   188,    35,   205,     4,   190,     6,    35,
      90,   196,   197,    84,    94,   198,     3,   200,   201,   219,
     203,    38,   205,   206,   207,   350,   351,    35,    66,    32,
     230,   628,     3,   670,   438,   360,   190,     4,    90,     6,
      38,   226,    94,   226,   227,    32,   200,    32,   405,   232,
     341,   688,   156,    90,   237,    95,   160,    94,   100,   313,
     385,    32,   166,     3,    49,     3,    32,   171,   253,   547,
     253,   254,   226,    35,   399,   432,    38,   262,    49,     4,
     265,     6,     6,    60,   267,     3,   286,    90,   271,   193,
      35,    94,    32,    38,    32,    32,   200,     3,   202,   253,
     300,   738,   285,    90,    66,   259,    32,    94,   111,    49,
      35,    49,    49,    38,   341,    92,   299,    34,    35,    90,
     345,    38,   164,    94,   111,    32,    32,   327,    41,    42,
      43,   285,   103,    41,    42,    43,    32,   337,   109,   464,
     111,   341,    49,   343,    32,   328,   329,    56,   190,   332,
      90,   334,    90,   336,    94,   340,    94,   340,   345,   450,
     345,    49,    70,   103,    89,   103,    75,    76,   351,   356,
     807,   356,    90,   111,    35,   517,    94,    38,   332,    32,
     363,   406,     3,   525,    90,    46,   340,   831,    94,   664,
     415,    32,    32,    33,    46,   237,   109,   103,   349,    32,
     844,   845,   385,   388,   404,   111,   548,    32,     3,    49,
     119,     6,   366,   555,    66,    32,   399,   400,    70,   422,
      32,   424,   422,   423,   424,    44,    45,    96,    97,   429,
     417,   414,   417,     3,    34,   418,    32,   470,    33,    39,
      32,   468,    61,    62,   444,   422,   433,    40,   433,   570,
     450,   451,   422,    91,   437,   438,   439,   457,   422,   442,
       3,   422,   462,   620,   621,    84,   422,    32,    33,    90,
     470,    44,    45,    94,     3,   458,    35,     6,    35,    32,
      33,    38,   422,   422,    49,   422,   422,   470,    61,    62,
     517,   633,   422,    63,    64,    65,    49,   500,   525,     3,
     500,    35,   485,     3,    38,   422,   422,   422,   508,   422,
     493,   415,    32,    35,   468,    35,   470,   517,    38,   422,
      90,   548,   422,   500,    94,   525,   529,   422,   555,   529,
     500,    38,   422,   422,    32,    33,   500,   422,     3,   500,
     523,    32,    33,   546,   500,     3,   546,   547,   548,     6,
     535,    49,   529,    35,   675,   555,    38,    38,    49,   529,
     500,   500,   704,   500,   500,   529,    35,   550,   529,   546,
     500,   556,   414,   529,   557,   850,   418,   668,   610,    41,
      42,    43,   546,   500,   500,   500,   640,   500,    34,   529,
     529,   591,   529,   529,     4,   437,     6,   500,   307,   529,
     500,    91,   602,   628,   313,   500,   633,   713,    85,   715,
     500,   500,   529,   529,   529,   500,   529,   600,    33,    34,
      34,   763,   647,    34,    18,    85,   529,    33,   470,   529,
      33,    34,    33,   633,   529,   172,   173,   637,   623,   529,
     529,   668,    36,   734,   529,    32,   629,   647,    91,   600,
      32,   188,   108,   109,   657,   638,   610,   657,   367,   196,
     647,   661,   647,    33,    34,   665,    91,   809,   668,   109,
     670,   813,    33,    67,    68,    69,    33,   704,   663,   662,
     657,   523,    10,    33,   669,    38,    33,   657,   688,   780,
       3,    46,    34,   657,   677,    33,   657,   680,   681,    93,
      33,   657,    33,   686,   704,   656,    35,    33,   145,   106,
     107,   108,   109,   698,    33,   557,   425,   657,   657,    32,
     657,   657,   705,    33,    33,   262,   630,   657,   265,    33,
      33,    54,     3,    85,   734,    91,   763,   720,   738,    35,
     657,   657,   657,    91,   657,   745,    38,    91,   731,   749,
      91,    74,    33,   753,   657,   740,    79,   657,   743,    33,
     743,    32,   657,   763,    33,     3,   203,   657,   657,    35,
      35,    38,   657,    35,   759,    32,    39,    90,    49,    35,
     780,    94,   809,   134,   135,    35,   813,   629,   742,   743,
     103,   791,   792,     3,    32,    32,   638,   782,   111,    33,
     785,   828,    38,   340,   831,    33,    32,   807,   791,   809,
      34,   794,   795,   813,    34,   815,    46,   844,   845,    90,
     662,   804,   531,    94,   113,    84,   811,   812,   828,    33,
     267,   831,   815,   156,   271,   677,   159,   160,    39,    46,
     111,    32,    38,   166,   844,   845,     3,     4,   171,     6,
      66,   388,    90,    66,    66,    39,    94,   111,    39,   182,
      68,    33,    33,   705,    39,   103,    32,    70,    33,     3,
     193,   194,     6,   111,     3,    32,   199,   200,    35,   202,
       3,    38,     4,    35,     6,     3,    33,     3,   239,   240,
      35,   600,   243,   244,   245,   246,    66,   334,    32,   336,
      33,    32,    34,    32,    34,     3,    33,    35,    32,    32,
       3,    85,   263,    35,    32,   266,    38,    35,    35,    32,
      49,    35,    33,    46,    46,    35,    49,    33,    35,    33,
      39,   640,    89,    90,    32,    35,    35,    94,    34,    32,
      32,    32,    35,    66,    39,    33,   103,   656,   385,   791,
      32,    49,   109,   795,   111,     3,    90,    33,     3,    33,
      94,    90,   399,   400,    35,    94,    33,    90,    33,   103,
     679,    94,    90,   815,   103,   109,    94,   111,    35,    33,
     103,    33,   111,   406,    32,   103,   109,    32,   111,    35,
     429,   109,    90,   111,    35,    35,    94,    90,   535,   546,
     668,    94,   439,    33,   546,   442,   546,   197,   815,   462,
     103,   647,     3,   111,    40,   457,   109,   661,   111,   556,
     345,    66,   470,   374,   375,   376,   377,   792,   379,   380,
     381,   382,   383,   384,   749,   813,   804,   753,   389,    19,
     532,    32,    90,    32,    35,    90,    94,    -1,   485,    94,
      -1,    -1,    41,    42,    43,   103,   493,    46,   103,    -1,
      -1,   109,    -1,   111,   109,    -1,   111,    -1,     3,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     403,    70,   405,   406,    -1,    -1,   623,    -1,    -1,    -1,
     413,   414,   415,    -1,    -1,    -1,    -1,    32,    -1,    90,
      35,    -1,     3,    94,    -1,    -1,    -1,    -1,    -1,   432,
      -1,    -1,   103,   550,   437,   466,    -1,   468,   109,    -1,
     111,    -1,    -1,    -1,    -1,     3,   663,    -1,     6,    -1,
      -1,    32,   669,    11,    12,    13,    14,    15,    16,    17,
      -1,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    90,     3,    -1,    -1,    94,
      -1,   698,    -1,    41,    42,    43,    44,    45,   103,    47,
      -1,    -1,    50,    -1,   109,    -1,   111,    -1,    -1,    -1,
      -1,    -1,    60,    61,    62,    32,    -1,    -1,    -1,    90,
      -1,     3,     3,    94,   104,   105,   106,   107,   108,   109,
     523,    18,   103,   740,    -1,    -1,   743,   530,   109,   532,
     111,    -1,    90,    -1,    -1,    -1,    94,    -1,    -1,    36,
      32,    32,   759,    -1,    -1,    -1,    -1,    -1,   106,   107,
      -1,    -1,   110,   111,    41,    42,    43,    49,    49,    46,
      -1,    -1,    -1,    90,    -1,   782,    -1,    94,   785,    -1,
      67,    68,    69,    -1,    -1,    -1,   103,    -1,   609,    66,
     611,     6,   109,    70,   111,    -1,    11,    12,    13,    14,
      15,    16,    17,    -1,   811,   812,    -1,    -1,    90,    90,
      -1,    -1,    94,    94,   103,   104,   105,   106,   107,   108,
     109,   103,   103,     1,    -1,     3,     4,   620,   621,   111,
     111,     9,    -1,   626,    -1,   628,    -1,   630,    -1,   660,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    -1,    -1,    -1,    35,    36,    -1,
      -1,    39,    -1,    41,    42,    43,    44,    45,    -1,    -1,
     691,    -1,    -1,    -1,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    -1,    67,
      68,    69,    -1,    71,    72,     3,    -1,    -1,    76,    -1,
      78,    79,    80,    81,    82,    83,    -1,    -1,    -1,    -1,
      88,    -1,    90,   706,    -1,    93,    94,    95,     3,    -1,
      -1,     6,    -1,   744,    32,    -1,    11,    12,    13,    14,
      15,    16,    17,   111,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,   102,   103,
     104,   105,   106,   107,   108,   109,    41,    42,    43,    44,
      45,    -1,    47,    -1,    -1,    50,   101,   102,   103,   104,
     105,   106,   107,   108,   109,    60,    61,    62,    -1,    -1,
       3,    -1,    90,     6,    -1,    -1,    94,    -1,    11,    12,
      13,    14,    15,    16,    17,   103,    -1,    -1,    -1,    -1,
      -1,   109,    -1,   111,    -1,    90,    -1,    -1,    -1,    94,
      -1,    -1,    -1,    -1,    -1,    -1,    35,    -1,    -1,    -1,
       3,   106,   107,     6,    -1,   110,   111,    -1,    11,    12,
      13,    14,    15,    16,    17,    -1,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    41,    42,
      43,    44,    45,    -1,    47,    -1,    -1,    50,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    60,    61,    62,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,    -1,    -1,    -1,    -1,    -1,    90,    -1,    -1,
      -1,    94,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,     3,   106,   107,     6,    -1,   110,   111,    -1,
      11,    12,    13,    14,    15,    16,    17,    50,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      41,    42,    43,    44,    45,    -1,    47,    -1,    -1,    50,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    60,
      61,    62,    -1,    -1,    -1,    -1,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,    19,    20,    21,
      22,    23,    24,    25,    -1,    27,    -1,    29,    -1,    90,
      -1,    -1,    -1,    94,    -1,    -1,    -1,    -1,    -1,    -1,
      50,    -1,    -1,    -1,     3,   106,   107,     6,    -1,   110,
     111,    -1,    11,    12,    13,    14,    15,    16,    17,    50,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    41,    42,    43,    44,    45,    -1,    47,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
      -1,    60,    61,    62,    -1,    -1,    -1,    -1,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    90,    -1,    -1,    -1,    94,    -1,    -1,    -1,    -1,
      -1,    -1,    50,    -1,    -1,    -1,     3,   106,   107,     6,
      -1,   110,   111,    -1,    11,    12,    13,    14,    15,    16,
      17,    -1,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    41,    42,    43,    44,    45,    -1,
      47,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,    -1,    60,    61,    62,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    90,    -1,    -1,    -1,    94,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     3,   106,
     107,     6,   109,   110,   111,    10,    11,    12,    13,    14,
      15,    16,    17,    -1,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    41,    42,    43,    44,
      45,    -1,    47,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    60,    61,    62,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    90,    -1,    -1,    -1,    94,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       3,   106,   107,     6,    -1,   110,   111,    -1,    11,    12,
      13,    14,    15,    16,    17,    -1,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    41,    42,
      43,    44,    45,    -1,    47,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     3,    -1,    60,    61,    62,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    -1,    -1,    90,    -1,    -1,
      -1,    94,    40,    41,    42,    43,    44,    45,    -1,    -1,
      -1,    49,    -1,   106,   107,    -1,    -1,   110,   111,    -1,
       3,    -1,    60,    61,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      -1,    -1,    90,    -1,    -1,    93,    94,    40,    41,    42,
      43,    44,    45,    -1,     3,   103,    49,    -1,    -1,    -1,
      -1,   109,    -1,    -1,    -1,    -1,    -1,    60,    61,    62,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    40,    41,    42,    43,    44,    45,    90,    -1,    -1,
      93,    94,    -1,    -1,    -1,     3,    -1,    -1,    -1,    -1,
     103,    60,    61,    62,    -1,    -1,   109,    -1,    -1,    -1,
      -1,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    90,    -1,    -1,    93,    94,    44,    45,    -1,    -1,
      -1,    -1,    -1,    -1,   103,    -1,    -1,    -1,    -1,    -1,
     109,    -1,    60,    61,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     0,
       1,    -1,     3,     4,    -1,    -1,     7,     8,     9,    -1,
      -1,    -1,    90,    -1,    -1,    -1,    94,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,   109,    -1,    -1,    35,    36,    -1,    -1,    -1,    -1,
      41,    42,    43,    44,    45,    -1,    -1,    48,    -1,    -1,
      -1,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    -1,    -1,    -1,    -1,    67,    68,    69,    -1,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    -1,    -1,    -1,    -1,    88,     1,    90,
       3,     4,    93,    94,     7,     8,     9,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    -1,
      -1,    -1,    35,    36,    -1,    -1,    39,    -1,    41,    42,
      43,    44,    45,    -1,    -1,    48,    -1,    -1,    -1,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      -1,    -1,    -1,    -1,    67,    68,    69,    -1,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    -1,    -1,    -1,    -1,    88,     1,    90,     3,     4,
      93,    94,     7,     8,     9,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    -1,    -1,    -1,
      35,    36,    -1,    -1,    39,    -1,    41,    42,    43,    44,
      45,    -1,    -1,    48,    -1,    -1,    -1,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    -1,    -1,
      -1,    -1,    67,    68,    69,    -1,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    -1,
      -1,    -1,    -1,    88,     1,    90,     3,     4,    93,    94,
       7,     8,     9,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    -1,    -1,    -1,    35,    36,
      -1,    -1,    39,    -1,    41,    42,    43,    44,    45,    -1,
      -1,    48,    -1,    -1,    -1,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    -1,    -1,    -1,    -1,
      67,    68,    69,    -1,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    -1,    -1,    -1,
      -1,    88,     1,    90,     3,     4,    93,    94,     7,     8,
       9,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    -1,    -1,    -1,    35,    36,    -1,    -1,
      -1,    -1,    41,    42,    43,    44,    45,    -1,    -1,    48,
      -1,    50,    -1,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    -1,    -1,    -1,    -1,    67,    68,
      69,    -1,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,     1,    -1,     3,    -1,    88,
      -1,    90,    -1,    -1,    93,    94,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    40,    41,    42,    43,    44,    45,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       1,    -1,     3,    -1,    60,    61,    62,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    -1,    -1,    -1,    90,    -1,    -1,    93,    94,    40,
      41,    42,    43,    44,    45,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     1,    -1,     3,    -1,    -1,    60,
      61,    62,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    -1,    -1,    -1,    -1,    90,
      -1,    -1,    93,    94,    41,    42,    43,    44,    45,    -1,
       3,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    60,    61,    62,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    41,    42,
      43,    44,    45,    90,    -1,    -1,    -1,    94,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    60,    61,    62,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     3,    -1,    90,     6,    -1,
      -1,    94,    95,    11,    12,    13,    14,    15,    16,    17,
      -1,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    40,    41,    42,    43,    44,    45,    -1,     3,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    11,    -1,    -1,
      -1,    -1,    60,    61,    62,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    40,    41,    42,    43,
      44,    45,    90,     3,    -1,    93,    94,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    60,    61,    62,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      40,    41,    42,    43,    44,    45,    90,     3,    -1,    93,
      94,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      60,    61,    62,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    41,    42,    43,    44,    45,
      90,     3,    -1,    93,    94,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    60,    61,    62,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    41,
      42,    43,    44,    45,    90,     3,    -1,    -1,    94,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    60,    61,
      62,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    41,    42,    43,    44,    45,    90,     3,
      -1,    -1,    94,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    60,    61,    62,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    41,    42,    43,
      44,    45,    90,     3,    -1,    -1,    94,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    60,    61,    62,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    44,    45,    90,    -1,    -1,    -1,
      94,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      60,    61,    62,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      90,    -1,    -1,    -1,    94
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned short yystos[] =
{
       0,    96,    97,   115,   116,   256,     1,     3,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    40,    41,    42,    43,    44,    45,    60,    61,    62,
      90,    93,    94,   199,   213,   214,   216,   217,   218,   219,
     220,   236,   246,   248,     1,   199,     0,     1,     4,     7,
       8,     9,    18,    35,    36,    48,    52,    53,    54,    55,
      56,    57,    58,    59,    67,    68,    69,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      88,    93,   117,   118,   119,   121,   122,   123,   124,   125,
     128,   129,   131,   132,   133,   134,   135,   136,   137,   140,
     141,   142,   145,   147,   152,   153,   154,   155,   157,   160,
     161,   162,   163,   164,   168,   169,   173,   174,   184,   195,
     256,    84,   243,   256,   243,    40,   246,   113,    84,    35,
     217,   213,    32,    46,    49,    66,   103,   109,   111,   204,
     205,   207,   209,   210,   211,   212,   246,   256,   213,   219,
     246,    92,   113,   247,    35,    95,    32,     6,   251,    32,
     253,   256,     1,     3,   215,   216,    32,   253,    32,   139,
     256,    32,    32,    32,    73,   246,     3,    38,   246,    32,
       4,    38,    32,    32,    35,    38,     4,   251,    32,   151,
     215,   149,   151,    32,    32,   251,    32,    84,   236,   253,
      32,   103,   207,   212,   246,    60,   215,   236,     6,    11,
      12,    13,    14,    15,    16,    17,   199,   200,   201,   203,
     228,   256,    40,    94,   248,   236,    32,   103,   196,   197,
     199,   210,   212,   246,   256,    10,    38,   103,   221,   222,
      32,    47,    50,   106,   107,   110,   111,   215,   227,   228,
     229,   251,   227,    32,   111,   208,   211,   246,   212,   213,
     246,   204,    32,    49,   204,    32,    49,   103,   208,   211,
     246,    91,   248,    94,   248,     3,   244,   251,     6,    38,
     244,   254,   244,    35,    46,    32,   207,    33,   244,   246,
       3,     3,   244,    11,   146,   196,   196,   246,    35,    46,
     176,    38,     3,   148,   254,     3,   196,    38,   206,   207,
     210,   256,    35,    34,   150,   256,   244,   245,   256,   126,
     127,   251,   196,   172,   197,   246,   251,     3,   103,   212,
     244,   246,    32,   244,   103,   246,    91,     3,   223,   256,
      32,   207,    38,   246,    85,    34,   202,   256,   247,    85,
     103,   212,   246,   209,   246,    33,    34,   198,   256,    33,
     103,   209,   246,    91,   207,   227,   227,    32,   227,   227,
     227,   227,    32,    50,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   212,   246,   246,    32,    49,
     212,   196,    50,   227,   196,    50,   227,   208,   211,    91,
     103,   208,   247,    33,   156,    34,    46,    33,   221,   204,
      33,    38,    33,    46,    33,    34,   144,    34,    33,    33,
      35,   246,   116,   175,    33,    33,    33,    33,   149,   151,
      33,    33,    34,    38,    33,    85,    49,    33,    91,   212,
     246,    35,    91,    35,    38,   196,   246,    70,   159,   204,
     213,   166,    35,    66,   230,   231,   256,   203,   236,   246,
      91,    33,   199,    33,    91,   209,    33,   103,   109,   212,
     215,   227,   227,   227,   227,   227,   227,   227,   227,   227,
     227,   227,   208,   246,    33,    91,   196,    50,   227,    33,
      50,    33,    50,   103,   208,   211,   208,     4,    38,   251,
     116,   254,   126,   228,   251,   255,    35,    35,   120,     4,
     138,   251,     4,    35,    38,    89,   143,   207,   251,   252,
     244,   251,   255,    33,   199,   207,    38,    35,    39,   116,
      36,   195,   149,    35,    38,    32,    39,   150,     3,    90,
      94,   249,    35,   254,   199,    35,   170,   130,   207,   251,
      91,     3,   224,   225,   256,    33,    32,    34,    35,    38,
     158,    70,   204,     1,    35,    56,    63,    64,    65,    68,
     111,   121,   122,   123,   124,   128,   133,   135,   137,   140,
     142,   145,   147,   152,   153,   154,   155,   168,   169,   173,
     177,   180,   181,   182,   183,   184,   185,   186,   191,   194,
     195,   256,   232,    38,   202,   246,   198,    33,   227,    33,
     213,    33,   103,   205,    33,   208,    33,    50,   208,    39,
      34,    34,   177,    32,    70,   213,   238,   256,    46,    33,
      34,   144,   143,   207,   238,    39,   251,     3,   215,    35,
      46,   252,   196,    92,   113,   250,   113,    84,    33,    39,
     157,   164,   168,   169,   171,   181,   195,   116,   238,    35,
      46,    34,    39,    32,    46,   238,   239,   196,   207,    32,
     179,    38,    66,    66,    66,   111,   248,    39,   177,    95,
     215,   236,   246,    68,   233,   234,   237,   256,   165,   227,
     227,    33,    33,    33,   254,   254,    39,   196,    32,    70,
     143,   251,   255,    35,   207,    33,   238,    35,    35,   207,
     151,    33,     3,     3,    94,     3,    94,   200,     4,    38,
     215,    50,    35,    10,   226,   227,   225,    35,   207,   196,
     221,    66,   240,   256,    33,   159,   196,   177,   178,   248,
      32,   207,   215,    32,    66,     3,    38,   246,    35,    34,
      63,    64,    65,   235,   246,   177,   227,    33,   196,    32,
     144,   238,    35,   207,   143,    35,    35,   250,   250,    85,
     158,    33,    35,   241,   242,   246,    35,    38,   204,   158,
      33,   177,    32,   196,   158,    32,   103,   212,   196,   227,
      38,   188,    66,   234,   237,    39,    35,    33,   196,    35,
     238,    35,    35,    38,    34,    32,   204,    39,   196,    33,
     196,    32,    32,    33,    35,   187,   190,   207,   256,   233,
     246,    35,   167,   207,    33,    35,   242,   177,    33,   192,
     238,    33,   196,   196,   239,   190,    35,    38,   158,   193,
     238,    35,    38,   193,    33,    33,    35,   189,    35,    38,
      46,   193,   193,    35,   221,    35
};

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrlab1

/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)         \
  Current.first_line   = Rhs[1].first_line;      \
  Current.first_column = Rhs[1].first_column;    \
  Current.last_line    = Rhs[N].last_line;       \
  Current.last_column  = Rhs[N].last_column;
#endif

/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YYDSYMPRINT(Args)			\
do {						\
  if (yydebug)					\
    yysymprint Args;				\
} while (0)

# define YYDSYMPRINTF(Title, Token, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr, 					\
                  Token, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (cinluded).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short *bottom, short *top)
#else
static void
yy_stack_print (bottom, top)
    short *bottom;
    short *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned int yylineno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %u), ",
             yyrule - 1, yylineno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname [yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname [yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YYDSYMPRINT(Args)
# define YYDSYMPRINTF(Title, Token, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

#endif /* !YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    {
      YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
# ifdef YYPRINT
      YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
    }
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yytype, yyvaluep)
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YYDSYMPRINTF ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %s, ", yytname[yytoken]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 1386 "parser.y"
    {
		   Setattr(yyvsp[0].node,"classes",classes); 
		   Setattr(yyvsp[0].node,"name",ModuleName);
		   
		   if ((!module_node) && ModuleName) {
		     module_node = new_node("module");
		     Setattr(module_node,"name",ModuleName);
		   }
		   Setattr(yyvsp[0].node,"module",module_node);
		   check_extensions();
	           top = yyvsp[0].node;
               }
    break;

  case 3:
#line 1398 "parser.y"
    {
                 top = Getattr(yyvsp[-1].p,"type");
               }
    break;

  case 4:
#line 1401 "parser.y"
    {
                 top = 0;
               }
    break;

  case 5:
#line 1404 "parser.y"
    {
                 top = yyvsp[-1].p;
               }
    break;

  case 6:
#line 1407 "parser.y"
    {
                 top = 0;
               }
    break;

  case 7:
#line 1412 "parser.y"
    {  
                   /* add declaration to end of linked list (the declaration isn't always a single declaration, sometimes it is a linked list itself) */
                   appendChild(yyvsp[-1].node,yyvsp[0].node);
                   yyval.node = yyvsp[-1].node;
               }
    break;

  case 8:
#line 1417 "parser.y"
    {
                   yyval.node = new_node("top");
               }
    break;

  case 9:
#line 1422 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 10:
#line 1423 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 11:
#line 1424 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 12:
#line 1425 "parser.y"
    { yyval.node = 0; }
    break;

  case 13:
#line 1426 "parser.y"
    {
                  yyval.node = 0;
		  if (!Swig_error_count()) {
		    static int last_error_line = -1;
		    if (last_error_line != cparse_line) {
		      Swig_error(cparse_file, cparse_line,"Syntax error in input.\n");
		      last_error_line = cparse_line;
		      skip_decl();
		    }
		  }
               }
    break;

  case 14:
#line 1438 "parser.y"
    { 
                  if (yyval.node) {
   		      add_symbols(yyval.node);
                  }
                  yyval.node = yyvsp[0].node; 
	       }
    break;

  case 15:
#line 1454 "parser.y"
    {
                  yyval.node = 0;
                  skip_decl();
               }
    break;

  case 16:
#line 1464 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 17:
#line 1465 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 18:
#line 1466 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 19:
#line 1467 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 20:
#line 1468 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 21:
#line 1469 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 22:
#line 1470 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 23:
#line 1471 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 24:
#line 1472 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 25:
#line 1473 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 26:
#line 1474 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 27:
#line 1475 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 28:
#line 1476 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 29:
#line 1477 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 30:
#line 1478 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 31:
#line 1479 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 32:
#line 1480 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 33:
#line 1481 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 34:
#line 1482 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 35:
#line 1483 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 36:
#line 1484 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 37:
#line 1491 "parser.y"
    {
               Node *cls;
	       String *clsname;
	       cplus_mode = CPLUS_PUBLIC;
	       if (!classes) classes = NewHash();
	       if (!extendhash) extendhash = NewHash();
	       clsname = make_class_name(yyvsp[-1].str);
	       cls = Getattr(classes,clsname);
	       if (!cls) {
		 /* No previous definition. Create a new scope */
		 Node *am = Getattr(extendhash,clsname);
		 if (!am) {
		   Swig_symbol_newscope();
		   Swig_symbol_setscopename(yyvsp[-1].str);
		   prev_symtab = 0;
		 } else {
		   prev_symtab = Swig_symbol_setscope(Getattr(am,"symtab"));
		 }
		 current_class = 0;
	       } else {
		 /* Previous class definition.  Use its symbol table */
		 prev_symtab = Swig_symbol_setscope(Getattr(cls,"symtab"));
		 current_class = cls;
		 extendmode = 1;
	       }
	       Classprefix = NewString(yyvsp[-1].str);
	       Namespaceprefix= Swig_symbol_qualifiedscopename(0);
	       Delete(clsname);
	     }
    break;

  case 38:
#line 1519 "parser.y"
    {
               String *clsname;
	       extendmode = 0;
               yyval.node = new_node("extend");
	       Setattr(yyval.node,"symtab",Swig_symbol_popscope());
	       if (prev_symtab) {
		 Swig_symbol_setscope(prev_symtab);
	       }
	       Namespaceprefix = Swig_symbol_qualifiedscopename(0);
               clsname = make_class_name(yyvsp[-4].str);
	       Setattr(yyval.node,"name",clsname);

	       /* Mark members as extend */

	       Swig_tag_nodes(yyvsp[-1].node,"feature:extend",(char*) "1");
	       if (current_class) {
		 /* We add the extension to the previously defined class */
		 appendChild(yyval.node,yyvsp[-1].node);
		 appendChild(current_class,yyval.node);
	       } else {
		 /* We store the extensions in the extensions hash */
		 Node *am = Getattr(extendhash,clsname);
		 if (am) {
		   /* Append the members to the previous extend methods */
		   appendChild(am,yyvsp[-1].node);
		 } else {
		   appendChild(yyval.node,yyvsp[-1].node);
		   Setattr(extendhash,clsname,yyval.node);
		 }
	       }
	       current_class = 0;
	       Delete(Classprefix);
	       Delete(clsname);
	       Classprefix = 0;
	       prev_symtab = 0;
	       yyval.node = 0;

	     }
    break;

  case 39:
#line 1563 "parser.y"
    {
                    yyval.node = new_node("apply");
                    Setattr(yyval.node,"pattern",Getattr(yyvsp[-3].p,"pattern"));
		    appendChild(yyval.node,yyvsp[-1].p);
               }
    break;

  case 40:
#line 1573 "parser.y"
    {
		 yyval.node = new_node("clear");
		 appendChild(yyval.node,yyvsp[-1].p);
               }
    break;

  case 41:
#line 1584 "parser.y"
    {
		   if ((yyvsp[-1].dtype.type != T_ERROR) && (yyvsp[-1].dtype.type != T_SYMBOL)) {
		     yyval.node = new_node("constant");
		     Setattr(yyval.node,"name",yyvsp[-3].id);
		     Setattr(yyval.node,"type",NewSwigType(yyvsp[-1].dtype.type));
		     Setattr(yyval.node,"value",yyvsp[-1].dtype.val);
		     if (yyvsp[-1].dtype.rawval) Setattr(yyval.node,"rawval", yyvsp[-1].dtype.rawval);
		     Setattr(yyval.node,"storage","%constant");
		     Setattr(yyval.node,"feature:immutable","1");
		     add_symbols(yyval.node);
		   } else {
		     if (yyvsp[-1].dtype.type == T_ERROR) {
		       Swig_warning(WARN_PARSE_UNSUPPORTED_VALUE,cparse_file,cparse_line,"Unsupported constant value (ignored)\n");
		     }
		     yyval.node = 0;
		   }

	       }
    break;

  case 42:
#line 1603 "parser.y"
    {
		 if ((yyvsp[-1].dtype.type != T_ERROR) && (yyvsp[-1].dtype.type != T_SYMBOL)) {
		   SwigType_push(yyvsp[-3].type,yyvsp[-2].decl.type);
		   /* Sneaky callback function trick */
		   if (SwigType_isfunction(yyvsp[-3].type)) {
		     SwigType_add_pointer(yyvsp[-3].type);
		   }
		   yyval.node = new_node("constant");
		   Setattr(yyval.node,"name",yyvsp[-2].decl.id);
		   Setattr(yyval.node,"type",yyvsp[-3].type);
		   Setattr(yyval.node,"value",yyvsp[-1].dtype.val);
		   if (yyvsp[-1].dtype.rawval) Setattr(yyval.node,"rawval", yyvsp[-1].dtype.rawval);
		   Setattr(yyval.node,"storage","%constant");
		   Setattr(yyval.node,"feature:immutable","1");
		   add_symbols(yyval.node);
		 } else {
		     if (yyvsp[-1].dtype.type == T_ERROR) {
		       Swig_warning(WARN_PARSE_UNSUPPORTED_VALUE,cparse_file,cparse_line,"Unsupported constant value\n");
		     }
		   yyval.node = 0;
		 }
               }
    break;

  case 43:
#line 1625 "parser.y"
    {
		 Swig_warning(WARN_PARSE_BAD_VALUE,cparse_file,cparse_line,"Bad constant value (ignored).\n");
		 yyval.node = 0;
	       }
    break;

  case 44:
#line 1636 "parser.y"
    {
		 char temp[64];
		 Replace(yyvsp[0].str,"$file",cparse_file, DOH_REPLACE_ANY);
		 sprintf(temp,"%d", cparse_line);
		 Replace(yyvsp[0].str,"$line",temp,DOH_REPLACE_ANY);
		 Printf(stderr,"%s\n", yyvsp[0].str);
		 Delete(yyvsp[0].str);
                 yyval.node = 0;
	       }
    break;

  case 45:
#line 1645 "parser.y"
    {
		 char temp[64];
		 String *s = NewString(yyvsp[0].id);
		 Replace(s,"$file",cparse_file, DOH_REPLACE_ANY);
		 sprintf(temp,"%d", cparse_line);
		 Replace(s,"$line",temp,DOH_REPLACE_ANY);
		 Printf(stderr,"%s\n", s);
		 Delete(s);
                 yyval.node = 0;
               }
    break;

  case 46:
#line 1664 "parser.y"
    {
                    skip_balanced('{','}');
		    yyval.node = 0;
		    Swig_warning(WARN_DEPRECATED_EXCEPT,cparse_file, cparse_line, "%%except is deprecated.  Use %%exception instead.\n");
	       }
    break;

  case 47:
#line 1670 "parser.y"
    {
                    skip_balanced('{','}');
		    yyval.node = 0;
		    Swig_warning(WARN_DEPRECATED_EXCEPT,cparse_file, cparse_line, "%%except is deprecated.  Use %%exception instead.\n");
               }
    break;

  case 48:
#line 1676 "parser.y"
    {
		 yyval.node = 0;
		 Swig_warning(WARN_DEPRECATED_EXCEPT,cparse_file, cparse_line, "%%except is deprecated.  Use %%exception instead.\n");
               }
    break;

  case 49:
#line 1681 "parser.y"
    {
		 yyval.node = 0;
		 Swig_warning(WARN_DEPRECATED_EXCEPT,cparse_file, cparse_line, "%%except is deprecated.  Use %%exception instead.\n");
	       }
    break;

  case 50:
#line 1692 "parser.y"
    {		 
                 yyval.node = NewHash();
                 Setattr(yyval.node,"value",yyvsp[-3].id);
		 Setattr(yyval.node,"type",Getattr(yyvsp[-1].p,"type"));
               }
    break;

  case 51:
#line 1699 "parser.y"
    {
                 yyval.node = NewHash();
                 Setattr(yyval.node,"value",yyvsp[0].id);
              }
    break;

  case 52:
#line 1703 "parser.y"
    {
                yyval.node = yyvsp[0].node;
              }
    break;

  case 53:
#line 1708 "parser.y"
    {
                   Hash *p = yyvsp[-2].node;
		   yyval.node = new_node("fragment");
		   Setattr(yyval.node,"value",Getattr(yyvsp[-4].node,"value"));
		   Setattr(yyval.node,"type",Getattr(yyvsp[-4].node,"type"));
		   Setattr(yyval.node,"section",Getattr(p,"name"));
		   Setattr(yyval.node,"kwargs",nextSibling(p));
		   Setattr(yyval.node,"code",yyvsp[0].str);
                 }
    break;

  case 54:
#line 1717 "parser.y"
    {
		   Hash *p = yyvsp[-2].node;
                   skip_balanced('{','}');
		   yyval.node = new_node("fragment");
		   Setattr(yyval.node,"value",Getattr(yyvsp[-4].node,"value"));
		   Setattr(yyval.node,"type",Getattr(yyvsp[-4].node,"type"));
		   Setattr(yyval.node,"section",Getattr(p,"name"));
		   Setattr(yyval.node,"kwargs",nextSibling(p));
		   Delitem(scanner_ccode,0);
		   Delitem(scanner_ccode,DOH_END);
		   Setattr(yyval.node,"code",Copy(scanner_ccode));
                 }
    break;

  case 55:
#line 1729 "parser.y"
    {
		   yyval.node = new_node("fragment");
		   Setattr(yyval.node,"value",Getattr(yyvsp[-2].node,"value"));
		   Setattr(yyval.node,"type",Getattr(yyvsp[-2].node,"type"));
		   Setattr(yyval.node,"emitonly","1");
		 }
    break;

  case 56:
#line 1742 "parser.y"
    {
                     yyvsp[-3].loc.filename = Swig_copy_string(cparse_file);
		     yyvsp[-3].loc.line = cparse_line;
		     cparse_file = Swig_copy_string(yyvsp[-1].id);
		     cparse_line = 0;
               }
    break;

  case 57:
#line 1747 "parser.y"
    {
                     yyval.node = yyvsp[-1].node;
		     cparse_file = yyvsp[-6].loc.filename;
		     cparse_line = yyvsp[-6].loc.line;
		     if (strcmp(yyvsp[-6].loc.type,"include") == 0) set_nodeType(yyval.node,"include");
		     if (strcmp(yyvsp[-6].loc.type,"import") == 0) set_nodeType(yyval.node,"import");
		     Setattr(yyval.node,"name",yyvsp[-4].id);
		     /* Search for the module (if any) */
		     {
			 Node *n = firstChild(yyval.node);
			 while (n) {
			     if (Strcmp(nodeType(n),"module") == 0) {
				 Setattr(yyval.node,"module",Getattr(n,"name"));
				 break;
			     }
			     n = nextSibling(n);
			 }
		     }
		     Setattr(yyval.node,"options",yyvsp[-5].node);
               }
    break;

  case 58:
#line 1769 "parser.y"
    { yyval.loc.type = (char *) "include"; }
    break;

  case 59:
#line 1770 "parser.y"
    { yyval.loc.type = (char *) "import"; }
    break;

  case 60:
#line 1777 "parser.y"
    {
                 String *cpps;
		 if (Namespaceprefix) {
		   Swig_error(cparse_file, cparse_start_line, "%%inline directive inside a namespace is disallowed.\n");

		   yyval.node = 0;
		 } else {
		   yyval.node = new_node("insert");
		   Setattr(yyval.node,"code",yyvsp[0].str);
		   /* Need to run through the preprocessor */
		   Setline(yyvsp[0].str,cparse_start_line);
		   Setfile(yyvsp[0].str,cparse_file);
		   Seek(yyvsp[0].str,0,SEEK_SET);
		   cpps = Preprocessor_parse(yyvsp[0].str);
		   start_inline(Char(cpps), cparse_start_line);
		   Delete(yyvsp[0].str);
		   Delete(cpps);
		 }
		 
	       }
    break;

  case 61:
#line 1797 "parser.y"
    {
                 String *cpps;
		 skip_balanced('{','}');
		 if (Namespaceprefix) {
		   Swig_error(cparse_file, cparse_start_line, "%%inline directive inside a namespace is disallowed.\n");
		   
		   yyval.node = 0;
		 } else {
                   yyval.node = new_node("insert");
		   Delitem(scanner_ccode,0);
		   Delitem(scanner_ccode,DOH_END);
		   Setattr(yyval.node,"code", Copy(scanner_ccode));
		   cpps=Copy(scanner_ccode);
		   start_inline(Char(cpps), cparse_start_line);
		   Delete(cpps);
		 }
               }
    break;

  case 62:
#line 1824 "parser.y"
    {
                 yyval.node = new_node("insert");
		 Setattr(yyval.node,"code",yyvsp[0].str);
	       }
    break;

  case 63:
#line 1828 "parser.y"
    {
		 String *code = NewString("");
		 yyval.node = new_node("insert");
		 Setattr(yyval.node,"section",yyvsp[-2].id);
		 Setattr(yyval.node,"code",code);
		 if (Swig_insert_file(yyvsp[0].id,code) < 0) {
		   Swig_error(cparse_file, cparse_line, "Couldn't find '%s'.\n", yyvsp[0].id);
		   yyval.node = 0;
		 } 
               }
    break;

  case 64:
#line 1838 "parser.y"
    {
		 yyval.node = new_node("insert");
		 Setattr(yyval.node,"section",yyvsp[-2].id);
		 Setattr(yyval.node,"code",yyvsp[0].str);
               }
    break;

  case 65:
#line 1843 "parser.y"
    {
                 skip_balanced('{','}');
		 yyval.node = new_node("insert");
		 Setattr(yyval.node,"section",yyvsp[-2].id);
		 Delitem(scanner_ccode,0);
		 Delitem(scanner_ccode,DOH_END);
		 Setattr(yyval.node,"code", Copy(scanner_ccode));
	       }
    break;

  case 66:
#line 1858 "parser.y"
    {
                 yyval.node = new_node("module");
		 Setattr(yyval.node,"name",yyvsp[0].id);
		 if (yyvsp[-1].node) {
		   Setattr(yyval.node,"options",yyvsp[-1].node);
		   if (Getattr(yyvsp[-1].node,"directors")) {
		     /*
		       we set dirprot_mode here to 1, just to save the
		       symbols. Later, the language module must decide
		       what to do with them.
		     */
		     dirprot_mode = 1;
		   } 
		   if (Getattr(yyvsp[-1].node,"templatereduce")) {
		     template_reduce = 1;
		   }
		 }
		 if (!ModuleName) ModuleName = NewString(yyvsp[0].id);
		 if (!module_node) module_node = yyval.node;
	       }
    break;

  case 67:
#line 1885 "parser.y"
    {
                 Swig_warning(WARN_DEPRECATED_NAME,cparse_file,cparse_line, "%%name is deprecated.  Use %%rename instead.\n");
                 yyrename = NewString(yyvsp[-1].id);
		 yyval.node = 0;
               }
    break;

  case 68:
#line 1890 "parser.y"
    {
		 Swig_warning(WARN_DEPRECATED_NAME,cparse_file,cparse_line, "%%name is deprecated.  Use %%rename instead.\n");
		 yyval.node = 0;
		 Swig_error(cparse_file,cparse_line,"Missing argument to %%name directive.\n");
	       }
    break;

  case 69:
#line 1903 "parser.y"
    {
                 yyval.node = new_node("native");
		 Setattr(yyval.node,"name",yyvsp[-4].id);
		 Setattr(yyval.node,"wrap:name",yyvsp[-1].id);
	         add_symbols(yyval.node);
	       }
    break;

  case 70:
#line 1909 "parser.y"
    {
		 if (!SwigType_isfunction(yyvsp[-1].decl.type)) {
		   Swig_error(cparse_file,cparse_line,"%%native declaration '%s' is not a function.\n", yyvsp[-1].decl.id);
		   yyval.node = 0;
		 } else {
		     Delete(SwigType_pop_function(yyvsp[-1].decl.type));
		     /* Need check for function here */
		     SwigType_push(yyvsp[-2].type,yyvsp[-1].decl.type);
		     yyval.node = new_node("native");
	             Setattr(yyval.node,"name",yyvsp[-5].id);
		     Setattr(yyval.node,"wrap:name",yyvsp[-1].decl.id);
		     Setattr(yyval.node,"type",yyvsp[-2].type);
		     Setattr(yyval.node,"parms",yyvsp[-1].decl.parms);
		     Setattr(yyval.node,"decl",yyvsp[-1].decl.type);
		 }
	         add_symbols(yyval.node);
	       }
    break;

  case 71:
#line 1935 "parser.y"
    {
                 yyval.node = new_node("pragma");
		 Setattr(yyval.node,"lang",yyvsp[-3].id);
		 Setattr(yyval.node,"name",yyvsp[-2].id);
		 Setattr(yyval.node,"value",yyvsp[0].str);
	       }
    break;

  case 72:
#line 1941 "parser.y"
    {
		yyval.node = new_node("pragma");
		Setattr(yyval.node,"lang",yyvsp[-1].id);
		Setattr(yyval.node,"name",yyvsp[0].id);
	      }
    break;

  case 73:
#line 1948 "parser.y"
    { yyval.str = NewString(yyvsp[0].id); }
    break;

  case 74:
#line 1949 "parser.y"
    { yyval.str = yyvsp[0].str; }
    break;

  case 75:
#line 1952 "parser.y"
    { yyval.id = yyvsp[-1].id; }
    break;

  case 76:
#line 1953 "parser.y"
    { yyval.id = (char *) "swig"; }
    break;

  case 77:
#line 1961 "parser.y"
    {
                    SwigType *t = yyvsp[-2].decl.type;
		    if (!Len(t)) t = 0;
		    if (yyvsp[-3].ivalue) {
		      rename_add(yyvsp[-2].decl.id,t,yyvsp[-1].id,yyvsp[-2].decl.parms);
		    } else {
		      namewarn_add(yyvsp[-2].decl.id,t,yyvsp[-1].id);
		    }
		    yyval.node = 0;
		    scanner_clear_rename();
              }
    break;

  case 78:
#line 1972 "parser.y"
    {
		String *fixname;
		SwigType *t = yyvsp[-2].decl.type;
		fixname = feature_identifier_fix(yyvsp[-2].decl.id);
		if (!Len(t)) t = 0;
		/* Special declarator check */
		if (t) {
		  if (yyvsp[-1].dtype.qualifier) SwigType_push(t,yyvsp[-1].dtype.qualifier);
		  if (SwigType_isfunction(t)) {
		    SwigType *decl = SwigType_pop_function(t);
		    if (SwigType_ispointer(t)) {
		      String *nname = NewStringf("*%s",fixname);
		      if (yyvsp[-6].ivalue) {
			rename_add(Char(nname),decl,yyvsp[-4].id,yyvsp[-2].decl.parms);
		      } else {
			namewarn_add(Char(nname),decl,yyvsp[-4].id);
		      }
		      Delete(nname);
		    } else {
		      if (yyvsp[-6].ivalue) {
			rename_add(Char(fixname),decl,yyvsp[-4].id,yyvsp[-2].decl.parms);
		      } else {
			namewarn_add(Char(fixname),decl,yyvsp[-4].id);
		      }
		    }
		    Delete(decl);
		  } else if (SwigType_ispointer(t)) {
		    String *nname = NewStringf("*%s",fixname);
		    if (yyvsp[-6].ivalue) {
		      rename_add(Char(nname),0,yyvsp[-4].id,yyvsp[-2].decl.parms);
		    } else {
		      namewarn_add(Char(nname),0,yyvsp[-4].id);
		    }
		    Delete(nname);
		  }
		} else {
		  if (yyvsp[-6].ivalue) {
		    rename_add(Char(fixname),0,yyvsp[-4].id,yyvsp[-2].decl.parms);
		  } else {
		    namewarn_add(Char(fixname),0,yyvsp[-4].id);
		  }
		}
                yyval.node = 0;
		scanner_clear_rename();
              }
    break;

  case 79:
#line 2017 "parser.y"
    {
		if (yyvsp[-5].ivalue) {
		  rename_add(yyvsp[-1].id,0,yyvsp[-3].id,0);
		} else {
		  namewarn_add(yyvsp[-1].id,0,yyvsp[-3].id);
		}
		yyval.node = 0;
		scanner_clear_rename();
              }
    break;

  case 80:
#line 2028 "parser.y"
    {
		    yyval.ivalue = 1;
                }
    break;

  case 81:
#line 2031 "parser.y"
    {
                    yyval.ivalue = 0;
                }
    break;

  case 82:
#line 2058 "parser.y"
    {
                    String *val = yyvsp[0].str ? NewString(yyvsp[0].str) : NewString("1");
                    new_feature(yyvsp[-4].id, val, 0, yyvsp[-2].decl.id, yyvsp[-2].decl.type, yyvsp[-2].decl.parms, yyvsp[-1].dtype.qualifier);
                    yyval.node = 0;
                  }
    break;

  case 83:
#line 2063 "parser.y"
    {
                    String *val = Len(yyvsp[-4].id) ? NewString(yyvsp[-4].id) : 0;
                    new_feature(yyvsp[-6].id, val, 0, yyvsp[-2].decl.id, yyvsp[-2].decl.type, yyvsp[-2].decl.parms, yyvsp[-1].dtype.qualifier);
                    yyval.node = 0;
                  }
    break;

  case 84:
#line 2068 "parser.y"
    {
                    String *val = yyvsp[0].str ? NewString(yyvsp[0].str) : NewString("1");
                    new_feature(yyvsp[-5].id, val, yyvsp[-4].node, yyvsp[-2].decl.id, yyvsp[-2].decl.type, yyvsp[-2].decl.parms, yyvsp[-1].dtype.qualifier);
                    yyval.node = 0;
                  }
    break;

  case 85:
#line 2073 "parser.y"
    {
                    String *val = Len(yyvsp[-5].id) ? NewString(yyvsp[-5].id) : 0;
                    new_feature(yyvsp[-7].id, val, yyvsp[-4].node, yyvsp[-2].decl.id, yyvsp[-2].decl.type, yyvsp[-2].decl.parms, yyvsp[-1].dtype.qualifier);
                    yyval.node = 0;
                  }
    break;

  case 86:
#line 2080 "parser.y"
    {
                    String *val = yyvsp[0].str ? NewString(yyvsp[0].str) : NewString("1");
                    new_feature(yyvsp[-2].id, val, 0, 0, 0, 0, 0);
                    yyval.node = 0;
                  }
    break;

  case 87:
#line 2085 "parser.y"
    {
                    String *val = Len(yyvsp[-2].id) ? NewString(yyvsp[-2].id) : 0;
                    new_feature(yyvsp[-4].id, val, 0, 0, 0, 0, 0);
                    yyval.node = 0;
                  }
    break;

  case 88:
#line 2090 "parser.y"
    {
                    String *val = yyvsp[0].str ? NewString(yyvsp[0].str) : NewString("1");
                    new_feature(yyvsp[-3].id, val, yyvsp[-2].node, 0, 0, 0, 0);
                    yyval.node = 0;
                  }
    break;

  case 89:
#line 2095 "parser.y"
    {
                    String *val = Len(yyvsp[-3].id) ? NewString(yyvsp[-3].id) : 0;
                    new_feature(yyvsp[-5].id, val, yyvsp[-2].node, 0, 0, 0, 0);
                    yyval.node = 0;
                  }
    break;

  case 90:
#line 2102 "parser.y"
    { yyval.str = yyvsp[0].str; }
    break;

  case 91:
#line 2103 "parser.y"
    { yyval.str = 0; }
    break;

  case 92:
#line 2104 "parser.y"
    { yyval.str = yyvsp[-2].pl; }
    break;

  case 93:
#line 2107 "parser.y"
    {
		  yyval.node = NewHash();
		  Setattr(yyval.node,"name",yyvsp[-2].id);
		  Setattr(yyval.node,"value",yyvsp[0].id);
                }
    break;

  case 94:
#line 2112 "parser.y"
    {
		  yyval.node = NewHash();
		  Setattr(yyval.node,"name",yyvsp[-3].id);
		  Setattr(yyval.node,"value",yyvsp[-1].id);
                  set_nextSibling(yyval.node,yyvsp[0].node);
                }
    break;

  case 95:
#line 2122 "parser.y"
    {
                 Parm *val;
		 String *name;
		 SwigType *t;
                 if (!features_hash) features_hash = NewHash();
		 if (Namespaceprefix) name = NewStringf("%s::%s", Namespaceprefix, yyvsp[-2].decl.id);
		 else name = NewString(yyvsp[-2].decl.id);
		 val = yyvsp[-4].pl;
		 if (yyvsp[-2].decl.parms) {
		   Setmeta(val,"parms",yyvsp[-2].decl.parms);
		 }
		 t = yyvsp[-2].decl.type;
		 if (!Len(t)) t = 0;
		 if (t) {
		   if (yyvsp[-1].dtype.qualifier) SwigType_push(t,yyvsp[-1].dtype.qualifier);
		   if (SwigType_isfunction(t)) {
		     SwigType *decl = SwigType_pop_function(t);
		     if (SwigType_ispointer(t)) {
		       String *nname = NewStringf("*%s",name);
		       Swig_feature_set(features_hash, nname, decl, "feature:varargs", val, 0);
		       Delete(nname);
		     } else {
		       Swig_feature_set(features_hash, name, decl, "feature:varargs", val, 0);
		     }
		     Delete(decl);
		   } else if (SwigType_ispointer(t)) {
		     String *nname = NewStringf("*%s",name);
		     Swig_feature_set(features_hash,nname,0,"feature:varargs",val, 0);
		     Delete(nname);
		   }
		 } else {
		   Swig_feature_set(features_hash,name,0,"feature:varargs",val, 0);
		 }
		 Delete(name);
		 yyval.node = 0;
              }
    break;

  case 96:
#line 2159 "parser.y"
    { yyval.pl = yyvsp[0].pl; }
    break;

  case 97:
#line 2160 "parser.y"
    { 
		  int i;
		  int n;
		  Parm *p;
		  n = atoi(Char(yyvsp[-2].dtype.val));
		  if (n <= 0) {
		    Swig_error(cparse_file, cparse_line,"Argument count in %%varargs must be positive.\n");
		    yyval.pl = 0;
		  } else {
		    yyval.pl = Copy(yyvsp[0].p);
		    Setattr(yyval.pl,"name","VARARGS_SENTINEL");
		    for (i = 0; i < n; i++) {
		      p = Copy(yyvsp[0].p);
		      set_nextSibling(p,yyval.pl);
		      yyval.pl = p;
		    }
		  }
                }
    break;

  case 98:
#line 2189 "parser.y"
    {
		   yyval.node = 0;
		   if (yyvsp[-3].tmap.op) {
		     yyval.node = new_node("typemap");
		     Setattr(yyval.node,"method",yyvsp[-3].tmap.op);
		     Setattr(yyval.node,"code",NewString(yyvsp[0].str));
		     if (yyvsp[-3].tmap.kwargs) {
		       Setattr(yyval.node,"kwargs", yyvsp[-3].tmap.kwargs);
		     }
		     appendChild(yyval.node,yyvsp[-1].p);
		   }
	       }
    break;

  case 99:
#line 2201 "parser.y"
    {
		 yyval.node = 0;
		 if (yyvsp[-3].tmap.op) {
		   yyval.node = new_node("typemap");
		   Setattr(yyval.node,"method",yyvsp[-3].tmap.op);
		   appendChild(yyval.node,yyvsp[-1].p);
		 }
	       }
    break;

  case 100:
#line 2209 "parser.y"
    {
		   yyval.node = 0;
		   if (yyvsp[-5].tmap.op) {
		     yyval.node = new_node("typemapcopy");
		     Setattr(yyval.node,"method",yyvsp[-5].tmap.op);
		     Setattr(yyval.node,"pattern", Getattr(yyvsp[-1].p,"pattern"));
		     appendChild(yyval.node,yyvsp[-3].p);
		   }
	       }
    break;

  case 101:
#line 2222 "parser.y"
    {
		 Hash *p;
		 String *name;
		 p = nextSibling(yyvsp[0].node);
		 if (p && (!Getattr(p,"value"))) {
		   /* two argument typemap form */
		   name = Getattr(yyvsp[0].node,"name");
		   if (!name || (Strcmp(name,typemap_lang))) {
		     yyval.tmap.op = 0;
		     yyval.tmap.kwargs = 0;
		   } else {
		     yyval.tmap.op = Getattr(p,"name");
		     yyval.tmap.kwargs = nextSibling(p);
		   }
		 } else {
		   /* one-argument typemap-form */
		   yyval.tmap.op = Getattr(yyvsp[0].node,"name");
		   yyval.tmap.kwargs = p;
		 }
                }
    break;

  case 102:
#line 2244 "parser.y"
    {
                 yyval.p = yyvsp[-1].p;
		 set_nextSibling(yyval.p,yyvsp[0].p);
		}
    break;

  case 103:
#line 2250 "parser.y"
    {
                 yyval.p = yyvsp[-1].p;
		 set_nextSibling(yyval.p,yyvsp[0].p);
                }
    break;

  case 104:
#line 2254 "parser.y"
    { yyval.p = 0;}
    break;

  case 105:
#line 2257 "parser.y"
    {
		  SwigType_push(yyvsp[-1].type,yyvsp[0].decl.type);
		  yyval.p = new_node("typemapitem");
		  Setattr(yyval.p,"pattern",NewParm(yyvsp[-1].type,yyvsp[0].decl.id));
		  Setattr(yyval.p,"parms", yyvsp[0].decl.parms);
		  /*		  $$ = NewParm($1,$2.id);
				  Setattr($$,"parms",$2.parms); */
                }
    break;

  case 106:
#line 2265 "parser.y"
    {
                  yyval.p = new_node("typemapitem");
		  Setattr(yyval.p,"pattern",yyvsp[-1].pl);
		  /*		  Setattr($$,"multitype",$2); */
               }
    break;

  case 107:
#line 2270 "parser.y"
    {
		 yyval.p = new_node("typemapitem");
		 Setattr(yyval.p,"pattern", yyvsp[-4].pl);
		 /*                 Setattr($$,"multitype",$2); */
		 Setattr(yyval.p,"parms",yyvsp[-1].pl);
               }
    break;

  case 108:
#line 2282 "parser.y"
    {
                   yyval.node = new_node("types");
		   Setattr(yyval.node,"parms",yyvsp[-2].pl);
               }
    break;

  case 109:
#line 2292 "parser.y"
    {
                  Parm *p, *tp;
		  Node *n;
		  Node *tnode = 0;
		  Symtab *tscope = 0;
		  int     specialized = 0;
		  
		  yyval.node = 0;

		  tscope = Swig_symbol_current();          /* Get the current scope */

		  /* If the class name is qualified.  We need to create or lookup namespace entries */
		  yyvsp[-4].str = resolve_node_scope(yyvsp[-4].str);

		  /*
		    we use the new namespace entry 'nscope' only to
		    emit the template node. The template parameters are
		    resolved in the current 'tscope'.
		    
		    this is closer to the C++ (typedef) behavior.
		  */
		  n = Swig_cparse_template_locate(yyvsp[-4].str,yyvsp[-2].p,tscope);

		  /* Patch the argument types to respect namespaces */
		  p = yyvsp[-2].p;
		  while (p) {
		    SwigType *value = Getattr(p,"value");
		    if (!value) {
		      SwigType *ty = Getattr(p,"type");
		      if (ty) {
			if (template_reduce) {
			  SwigType *rty = Swig_symbol_typedef_reduce(ty,tscope);
			  ty = Swig_symbol_type_qualify(rty,tscope);
			  Setattr(p,"type",ty);
			  Delete(rty);
			} else {
			  ty = Swig_symbol_type_qualify(ty,tscope);
			  Setattr(p,"type",ty);
			}
		      }
		    } else {
		      value = Swig_symbol_type_qualify(value,tscope);
		      Setattr(p,"value",value);
		    }
		    
		    p = nextSibling(p);
		  }

		  /* Look for the template */
		  {
                    Node *nn = n;
                    Node *linklistend = 0;
                    while (nn) {
                      Node *templnode = 0;
                      if (Strcmp(nodeType(nn),"template") == 0) {
                        int nnisclass = (Strcmp(Getattr(nn,"templatetype"),"class") == 0); /* if not a templated class it is a templated function */
                        Parm *tparms = Getattr(nn,"templateparms");
                        if (!tparms) {
                          specialized = 1;
                        }
                        if (nnisclass && !specialized && ((ParmList_len(yyvsp[-2].p) > ParmList_len(tparms)))) {
                          Swig_error(cparse_file, cparse_line, "Too many template parameters. Maximum of %d.\n", ParmList_len(tparms));
                        } else if (nnisclass && !specialized && ((ParmList_len(yyvsp[-2].p) < ParmList_numrequired(tparms)))) {
                          Swig_error(cparse_file, cparse_line, "Not enough template parameters specified. %d required.\n", ParmList_numrequired(tparms));
                        } else if (!nnisclass && ((ParmList_len(yyvsp[-2].p) != ParmList_len(tparms)))) {
                          /* must be an overloaded templated method - ignore it as it is overloaded with a different number of template parameters */
                          nn = Getattr(nn,"sym:nextSibling"); /* repeat for overloaded templated functions */
                          continue;
                        } else {
                          int  def_supplied = 0;
                          /* Expand the template */
			  Node *templ = Swig_symbol_clookup(yyvsp[-4].str,0);
			  Parm *targs = templ ? Getattr(templ,"templateparms") : 0;

                          ParmList *temparms;
                          if (specialized) temparms = CopyParmList(yyvsp[-2].p);
                          else temparms = CopyParmList(tparms);

                          /* Create typedef's and arguments */
                          p = yyvsp[-2].p;
                          tp = temparms;
                          while (p) {
                            String *value = Getattr(p,"value");
                            if (def_supplied) {
                              Setattr(p,"default","1");
                            }
                            if (value) {
                              Setattr(tp,"value",value);
                            } else {
                              SwigType *ty = Getattr(p,"type");
                              if (ty) {
                                Setattr(tp,"type",ty);
                              }
                              Delattr(tp,"value");
                            }
			    /* fix default arg values */
			    if (targs) {
			      Parm *pi = temparms;
			      Parm *ti = targs;
			      String *tv = Getattr(tp,"value");
			      if (!tv) tv = Getattr(tp,"type");
			      while(pi != tp) {
				String *name = Getattr(ti,"name");
				String *value = Getattr(pi,"value");
				if (!value) value = Getattr(pi,"type");
				Replaceid(tv, name, value);
				pi = nextSibling(pi);
				ti = nextSibling(ti);
			      }
			    }
                            p = nextSibling(p);
                            tp = nextSibling(tp);
                            if (!p && tp) {
                              p = tp;
                              def_supplied = 1;
                            }
                          }

                          templnode = copy_node(nn);
                          /* We need to set the node name based on name used to instantiate */
                          Setattr(templnode,"name",Copy(yyvsp[-4].str));
                          if (!specialized) {
                            Delattr(templnode,"sym:typename");
                          } else {
                            Setattr(templnode,"sym:typename","1");
                          }
                          if (yyvsp[-6].id) {
                            Swig_cparse_template_expand(templnode,yyvsp[-6].id,temparms,tscope);
                            Setattr(templnode,"sym:name",yyvsp[-6].id);
                          } else {
                            static int cnt = 0;
                            String *nname = NewStringf("__dummy_%d__", cnt++);
                            Swig_cparse_template_expand(templnode,nname,temparms,tscope);
                            Setattr(templnode,"sym:name",nname);
                            Setattr(templnode,"feature:onlychildren",
                                    "typemap,typemapitem,typemapcopy,typedef,types,fragment");
                          }
                          Delattr(templnode,"templatetype");
                          Setattr(templnode,"template",nn);
                          tnode = templnode;
                          Setfile(templnode,cparse_file);
                          Setline(templnode,cparse_line);
                          Delete(temparms);
                          
                          add_symbols_copy(templnode);

                          if (Strcmp(nodeType(templnode),"class") == 0) {

                            /* Identify pure abstract methods */
                            Setattr(templnode,"abstract", pure_abstract(firstChild(templnode)));
                            
                            /* Set up inheritance in symbol table */
                            {
                              Symtab  *csyms;
                              List *baselist = Getattr(templnode,"baselist");
                              csyms = Swig_symbol_current();
                              Swig_symbol_setscope(Getattr(templnode,"symtab"));
                              if (baselist) {
                                List *bases = make_inherit_list(Getattr(templnode,"name"),baselist);
                                if (bases) {
                                  Iterator s;
                                  for (s = First(bases); s.item; s = Next(s)) {
                                    Symtab *st = Getattr(s.item,"symtab");
                                    if (st) {
                                      Swig_symbol_inherit(st);
                                    }
                                  }
                                }
                              }
                              Swig_symbol_setscope(csyms);
                            }

                            /* Merge in addmethods for this class */
                            
                            /* !!! This may be broken.  We may have to  add the addmethods at the beginning of
                               the class */
                            
                            if (extendhash) {
                              String *clsname;
                              Node *am;
                              if (Namespaceprefix) {
                                clsname = NewStringf("%s::%s", Namespaceprefix, Getattr(templnode,"name"));
                              } else {
                                clsname = Getattr(templnode,"name");
                              }
                              am = Getattr(extendhash,clsname);
                              if (am) {
                                Symtab *st = Swig_symbol_current();
                                Swig_symbol_setscope(Getattr(templnode,"symtab"));
                                /*			    Printf(stdout,"%s: %s %x %x\n", Getattr(templnode,"name"), clsname, Swig_symbol_current(), Getattr(templnode,"symtab")); */
                                merge_extensions(templnode,am);
                                Swig_symbol_setscope(st);
                                appendChild(templnode,am);
                                Delattr(extendhash,clsname);
                              }
                            }
                            /* Add to classes hash */
                            if (!classes) classes = NewHash();

                            {
                              if (Namespaceprefix) {
                                String *temp = NewStringf("%s::%s", Namespaceprefix, Getattr(templnode,"name"));
                                Setattr(classes,temp,templnode);
                              } else {
                                Setattr(classes,Swig_symbol_qualifiedscopename(templnode),templnode);
                              }
                            }
                          }
                        }

                        /* all the overloaded templated functions are added into a linked list */
                        if (nscope_inner) {
                          /* non-global namespace */
                          if (templnode) {
                            appendChild(nscope_inner,templnode);
                            if (nscope) yyval.node = nscope;
                          }
                        } else {
                          /* global namespace */
                          if (!linklistend) {
                            yyval.node = templnode;
                          } else {
                            set_nextSibling(linklistend,templnode);
                          }
                          linklistend = templnode;
                        }
                      }
                      nn = Getattr(nn,"sym:nextSibling"); /* repeat for overloaded templated functions. If a templated class there will never be a sibling. */
                    }
		  }
   	          Swig_symbol_setscope(tscope);
		  Namespaceprefix = Swig_symbol_qualifiedscopename(0);
                }
    break;

  case 110:
#line 2532 "parser.y"
    {
		  Swig_warning(0,cparse_file, cparse_line,"%s\n", yyvsp[0].id);
		  yyval.node = 0;
               }
    break;

  case 111:
#line 2542 "parser.y"
    {
                    yyval.node = yyvsp[0].node; 
                    if (yyval.node) {
   		      add_symbols(yyval.node);
                      default_arguments(yyval.node);
   	            }
                }
    break;

  case 112:
#line 2549 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 113:
#line 2550 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 114:
#line 2554 "parser.y"
    {
		  if (Strcmp(yyvsp[-1].id,"C") == 0) {
		    cparse_externc = 1;
		  }
		}
    break;

  case 115:
#line 2558 "parser.y"
    {
		  cparse_externc = 0;
		  if (Strcmp(yyvsp[-4].id,"C") == 0) {
		    Node *n = firstChild(yyvsp[-1].node);
		    yyval.node = new_node("extern");
		    Setattr(yyval.node,"name",yyvsp[-4].id);
		    appendChild(yyval.node,n);
		    while (n) {
		      SwigType *decl = Getattr(n,"decl");
		      if (SwigType_isfunction(decl)) {
			Setattr(n,"storage","externc");
		      }
		      n = nextSibling(n);
		    }
		  } else {
		     Swig_warning(WARN_PARSE_UNDEFINED_EXTERN,cparse_file, cparse_line,"Unrecognized extern type \"%s\".\n", yyvsp[-4].id);
		    yyval.node = new_node("extern");
		    Setattr(yyval.node,"name",yyvsp[-4].id);
		    appendChild(yyval.node,firstChild(yyvsp[-1].node));
		  }
                }
    break;

  case 116:
#line 2585 "parser.y"
    {
              yyval.node = new_node("cdecl");
	      if (yyvsp[-1].dtype.qualifier) SwigType_push(yyvsp[-2].decl.type,yyvsp[-1].dtype.qualifier);
	      Setattr(yyval.node,"type",yyvsp[-3].type);
	      Setattr(yyval.node,"storage",yyvsp[-4].id);
	      Setattr(yyval.node,"name",yyvsp[-2].decl.id);
	      Setattr(yyval.node,"decl",yyvsp[-2].decl.type);
	      Setattr(yyval.node,"parms",yyvsp[-2].decl.parms);
	      Setattr(yyval.node,"value",yyvsp[-1].dtype.val);
	      Setattr(yyval.node,"throws",yyvsp[-1].dtype.throws);
	      Setattr(yyval.node,"throw",yyvsp[-1].dtype.throw);
	      if (!yyvsp[0].node) {
		if (Len(scanner_ccode)) {
		  Setattr(yyval.node,"code",Copy(scanner_ccode));
		}
	      } else {
		Node *n = yyvsp[0].node;
		/* Inherit attributes */
		while (n) {
		  Setattr(n,"type",Copy(yyvsp[-3].type));
		  Setattr(n,"storage",yyvsp[-4].id);
		  n = nextSibling(n);
		}
	      }
	      if (yyvsp[-1].dtype.bitfield) {
		Setattr(yyval.node,"bitfield", yyvsp[-1].dtype.bitfield);
	      }

	      /* Look for "::" declarations (ignored) */
	      if (Strstr(yyvsp[-2].decl.id,"::")) {
                /* This is a special case. If the scope name of the declaration exactly
                   matches that of the declaration, then we will allow it. Otherwise, delete. */
                String *p = Swig_scopename_prefix(yyvsp[-2].decl.id);
		if (p && Namespaceprefix) {
		  if (Strcmp(p,Namespaceprefix) == 0) {
		    Setattr(yyval.node,"name",Swig_scopename_last(yyvsp[-2].decl.id));
		    set_nextSibling(yyval.node,yyvsp[0].node);
		  } else {
		    Delete(yyval.node);
		    yyval.node = yyvsp[0].node;
		  }
		  Delete(p);
		} else {
		  Delete(yyval.node);
		  yyval.node = yyvsp[0].node;
		}
	      } else {
		set_nextSibling(yyval.node,yyvsp[0].node);
	      }
           }
    break;

  case 117:
#line 2639 "parser.y"
    { 
                   yyval.node = 0;
                   Clear(scanner_ccode); 
               }
    break;

  case 118:
#line 2643 "parser.y"
    {
		 yyval.node = new_node("cdecl");
		 if (yyvsp[-1].dtype.qualifier) SwigType_push(yyvsp[-2].decl.type,yyvsp[-1].dtype.qualifier);
		 Setattr(yyval.node,"name",yyvsp[-2].decl.id);
		 Setattr(yyval.node,"decl",yyvsp[-2].decl.type);
		 Setattr(yyval.node,"parms",yyvsp[-2].decl.parms);
		 Setattr(yyval.node,"value",yyvsp[-1].dtype.val);
		 Setattr(yyval.node,"throws",yyvsp[-1].dtype.throws);
		 Setattr(yyval.node,"throw",yyvsp[-1].dtype.throw);
		 if (yyvsp[-1].dtype.bitfield) {
		   Setattr(yyval.node,"bitfield", yyvsp[-1].dtype.bitfield);
		 }
		 if (!yyvsp[0].node) {
		   if (Len(scanner_ccode)) {
		     Setattr(yyval.node,"code",Copy(scanner_ccode));
		   }
		 } else {
		   set_nextSibling(yyval.node,yyvsp[0].node);
		 }
	       }
    break;

  case 119:
#line 2663 "parser.y"
    { 
                   skip_balanced('{','}');
                   yyval.node = 0;
               }
    break;

  case 120:
#line 2669 "parser.y"
    { 
                   yyval.dtype = yyvsp[0].dtype; 
                   yyval.dtype.qualifier = 0;
		   yyval.dtype.throws = 0;
		   yyval.dtype.throw = 0;
              }
    break;

  case 121:
#line 2675 "parser.y"
    { 
                   yyval.dtype = yyvsp[0].dtype; 
		   yyval.dtype.qualifier = yyvsp[-1].str;
		   yyval.dtype.throws = 0;
		   yyval.dtype.throw = 0;
	      }
    break;

  case 122:
#line 2681 "parser.y"
    { 
		   yyval.dtype = yyvsp[0].dtype; 
                   yyval.dtype.qualifier = 0;
		   yyval.dtype.throws = yyvsp[-2].pl;
		   yyval.dtype.throw = NewString("1");
              }
    break;

  case 123:
#line 2687 "parser.y"
    { 
                   yyval.dtype = yyvsp[0].dtype; 
                   yyval.dtype.qualifier = yyvsp[-5].str;
		   yyval.dtype.throws = yyvsp[-2].pl;
		   yyval.dtype.throw = NewString("1");
              }
    break;

  case 124:
#line 2700 "parser.y"
    {
		   SwigType *ty = 0;
		   yyval.node = new_node("enumforward");
		   ty = NewStringf("enum %s", yyvsp[-1].id);
		   Setattr(yyval.node,"name",yyvsp[-1].id);
		   Setattr(yyval.node,"type",ty);
		   Setattr(yyval.node,"sym:weak", "1");
		   add_symbols(yyval.node);
	      }
    break;

  case 125:
#line 2715 "parser.y"
    {
		  SwigType *ty = 0;
                  yyval.node = new_node("enum");
		  ty = NewStringf("enum %s", yyvsp[-4].id);
		  Setattr(yyval.node,"name",yyvsp[-4].id);
		  Setattr(yyval.node,"type",ty);
		  appendChild(yyval.node,yyvsp[-2].node);
		  add_symbols(yyval.node);       /* Add to tag space */
		  add_symbols(yyvsp[-2].node);       /* Add enum values to id space */
	       }
    break;

  case 126:
#line 2726 "parser.y"
    {
		 Node *n;
		 SwigType *ty = 0;
		 String   *unnamed = 0;
		 int       unnamedinstance = 0;

		 yyval.node = new_node("enum");
		 if (yyvsp[-5].id) {
		   Setattr(yyval.node,"name",yyvsp[-5].id);
		   ty = NewStringf("enum %s", yyvsp[-5].id);
		 } else if (yyvsp[-1].decl.id) {
		   unnamed = make_unnamed();
		   ty = NewStringf("enum %s", unnamed);
		   Setattr(yyval.node,"unnamed",unnamed);
                   /* name is not set for unnamed enum instances, e.g. enum { foo } Instance; */
		   if (yyvsp[-7].id && Cmp(yyvsp[-7].id,"typedef") == 0) {
		     Setattr(yyval.node,"name",yyvsp[-1].decl.id);
                   } else {
                     unnamedinstance = 1;
                   }
		   Setattr(yyval.node,"storage",yyvsp[-7].id);
		 }
		 if (yyvsp[-1].decl.id && Cmp(yyvsp[-7].id,"typedef") == 0) {
		   Setattr(yyval.node,"tdname",yyvsp[-1].decl.id);
                   Setattr(yyval.node,"allows_typedef","1");
                 }
		 appendChild(yyval.node,yyvsp[-3].node);
		 n = new_node("cdecl");
		 Setattr(n,"type",ty);
		 Setattr(n,"name",yyvsp[-1].decl.id);
		 Setattr(n,"storage",yyvsp[-7].id);
		 Setattr(n,"decl",yyvsp[-1].decl.type);
		 Setattr(n,"parms",yyvsp[-1].decl.parms);
		 Setattr(n,"unnamed",unnamed);

                 if (unnamedinstance) {
		   Setattr(yyval.node,"type",NewString("enum "));
		   Setattr(yyval.node,"unnamedinstance","1");
		   Setattr(n,"unnamedinstance","1");
                 }
		 if (yyvsp[0].node) {
		   Node *p = yyvsp[0].node;
		   set_nextSibling(n,p);
		   while (p) {
		     Setattr(p,"type",Copy(ty));
		     Setattr(p,"unnamed",unnamed);
		     Setattr(p,"storage",yyvsp[-7].id);
		     p = nextSibling(p);
		   }
		 } else {
		   if (Len(scanner_ccode)) {
		     Setattr(n,"code",Copy(scanner_ccode));
		   }
		 }

                 /* Ensure that typedef enum ABC {foo} XYZ; uses XYZ for sym:name, like structs.
                  * Note that class_rename/yyrename are bit of a mess so used this simple approach to change the name. */
                 if (yyvsp[-1].decl.id && yyvsp[-5].id && Cmp(yyvsp[-7].id,"typedef") == 0) {
                   Setattr(yyval.node, "parser:makename", NewString(yyvsp[-1].decl.id));
                 }

		 add_symbols(yyval.node);       /* Add enum to tag space */
		 set_nextSibling(yyval.node,n);

		 add_symbols(yyvsp[-3].node);       /* Add enum values to id space */
	         add_symbols(n);
	       }
    break;

  case 127:
#line 2795 "parser.y"
    {
                   /* This is a sick hack.  If the ctor_end has parameters,
                      and the parms paremeter only has 1 parameter, this
                      could be a declaration of the form:

                         type (id)(parms)

			 Otherwise it's an error. */
                    int err = 0;
                    yyval.node = 0;

		    if ((ParmList_len(yyvsp[-2].pl) == 1) && (!Swig_scopename_check(yyvsp[-4].type))) {
		      SwigType *ty = Getattr(yyvsp[-2].pl,"type");
		      String *name = Getattr(yyvsp[-2].pl,"name");
		      err = 1;
		      if (!name) {
			yyval.node = new_node("cdecl");
			Setattr(yyval.node,"type",yyvsp[-4].type);
			Setattr(yyval.node,"storage",yyvsp[-5].id);
			Setattr(yyval.node,"name",ty);

			if (yyvsp[0].decl.have_parms) {
			  SwigType *decl = NewString("");
			  SwigType_add_function(decl,yyvsp[0].decl.parms);
			  Setattr(yyval.node,"decl",decl);
			  Setattr(yyval.node,"parms",yyvsp[0].decl.parms);
			  if (Len(scanner_ccode)) {
			    Setattr(yyval.node,"code",Copy(scanner_ccode));
			  }
			}
			if (yyvsp[0].decl.defarg) {
			  Setattr(yyval.node,"value",yyvsp[0].decl.defarg);
			}
			Setattr(yyval.node,"throws",yyvsp[0].decl.throws);
			Setattr(yyval.node,"throw",yyvsp[0].decl.throw);
			err = 0;
		      }
		    }
		    if (err) {
		      Swig_error(cparse_file,cparse_line,"Syntax error in input.\n");
		    }
                }
    break;

  case 128:
#line 2843 "parser.y"
    {  yyval.node = yyvsp[0].node; }
    break;

  case 129:
#line 2844 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 130:
#line 2845 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 131:
#line 2846 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 132:
#line 2847 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 133:
#line 2848 "parser.y"
    { yyval.node = 0; }
    break;

  case 134:
#line 2854 "parser.y"
    {
                   List *bases = 0;

		   /* preserve the current scope */
		   prev_symtab = Swig_symbol_current();
		  
		   /* If the class name is qualified.  We need to create or lookup namespace/scope entries */
		   yyvsp[-2].str = resolve_node_scope(yyvsp[-2].str);
		   
		   /* support for old nested classes "pseudo" support, such as:

		         %rename(Ala__Ola) Ala::Ola;
			class Ala::Ola {
			public:
			    Ola() {}
		         };

		      this should dissapear with a proper implementation is added.
		   */
		   if (nscope_inner && Strcmp(nodeType(nscope_inner),"namespace") != 0) {
		     if (Namespaceprefix) {
		       String *name = NewStringf("%s::%s", Namespaceprefix, yyvsp[-2].str);		       
		       yyvsp[-2].str = name;
		       Namespaceprefix = 0;
		       nscope_inner = 0;
		     }
		   }

                   class_rename = make_name(yyvsp[-2].str,0);
		   Classprefix = NewString(yyvsp[-2].str);
		   /* Deal with inheritance  */
		   if (yyvsp[-1].bases) {
		     bases = make_inherit_list(yyvsp[-2].str,Getattr(yyvsp[-1].bases,"public"));
		   }
		   if (SwigType_istemplate(yyvsp[-2].str)) {
		     String *fbase, *tbase, *prefix;
		     prefix = SwigType_templateprefix(yyvsp[-2].str);
		     if (Namespaceprefix) {
		       fbase = NewStringf("%s::%s", Namespaceprefix,yyvsp[-2].str);
		       tbase = NewStringf("%s::%s", Namespaceprefix, prefix);
		     } else {
		       fbase = Copy(yyvsp[-2].str);
		       tbase = Copy(prefix);
		     }
		     rename_inherit(tbase,fbase);
		     Delete(fbase);
		     Delete(tbase);
		     Delete(prefix);
		   }
                   if (strcmp(yyvsp[-3].id,"class") == 0) {
		     cplus_mode = CPLUS_PRIVATE;
		   } else {
		     cplus_mode = CPLUS_PUBLIC;
		   }
		   Swig_symbol_newscope();
		   Swig_symbol_setscopename(yyvsp[-2].str);
		   if (bases) {
		     Iterator s;
		     for (s = First(bases); s.item; s = Next(s)) {
		       Symtab *st = Getattr(s.item,"symtab");
		       if (st) {
			 Swig_symbol_inherit(st); 
		       }
		     }
		   }
		   Namespaceprefix = Swig_symbol_qualifiedscopename(0);
		   cparse_start_line = cparse_line;

		   /* If there are active template parameters, we need to make sure they are
                      placed in the class symbol table so we can catch shadows */

		   if (template_parameters) {
		     Parm *tp = template_parameters;
		     while(tp) {
		       Node *tn = new_node("templateparm");
		       Setattr(tn,"name",Getattr(tp,"name"));
		       Swig_symbol_cadd(Copy(Getattr(tp,"name")),tn);
		       tp = nextSibling(tp);
		     }
		   }
		   inclass = 1;
               }
    break;

  case 135:
#line 2935 "parser.y"
    {
		 Node *p;
		 SwigType *ty;
		 Symtab *cscope = prev_symtab;
		 Node *am = 0;
		 inclass = 0;
		 yyval.node = new_node("class");
		 Setline(yyval.node,cparse_start_line);
		 Setattr(yyval.node,"name",yyvsp[-6].str);
		 Setattr(yyval.node,"kind",yyvsp[-7].id);
		 if (yyvsp[-5].bases) {
		   Setattr(yyval.node,"baselist", Getattr(yyvsp[-5].bases,"public"));
		   Setattr(yyval.node,"protectedbaselist", Getattr(yyvsp[-5].bases,"protected"));
		   Setattr(yyval.node,"privatebaselist", Getattr(yyvsp[-5].bases,"private"));
		 }
		 Setattr(yyval.node,"allows_typedef","1");
		 /* Check for pure-abstract class */
		 Setattr(yyval.node,"abstract", pure_abstract(yyvsp[-2].node));
		 
		 /* This bit of code merges in a previously defined %extend directive (if any) */
		 
		 if (extendhash) {
		   String *clsname = Swig_symbol_qualifiedscopename(0);
		   am = Getattr(extendhash,clsname);
		   if (am) {
		     merge_extensions(yyval.node,am);
		     Delattr(extendhash,clsname);
		   }
		   Delete(clsname);
		 }
		 if (!classes) classes = NewHash();
		 Setattr(classes,Swig_symbol_qualifiedscopename(0),yyval.node);

		 appendChild(yyval.node,yyvsp[-2].node);
		 if (am) appendChild(yyval.node,am);

		 p = yyvsp[0].node;
		 if (p) {
		   set_nextSibling(yyval.node,p);
		 }
		 
		 if (cparse_cplusplus && !cparse_externc) {
		   ty = NewString(yyvsp[-6].str);
		 } else {
		   ty = NewStringf("%s %s", yyvsp[-7].id,yyvsp[-6].str);
		 }
		 while (p) {
		   Setattr(p,"storage",yyvsp[-8].id);
		   Setattr(p,"type",ty);
		   p = nextSibling(p);
		 }
		 /* Dump nested classes */
		 {
		   String *name = yyvsp[-6].str;
		   if (yyvsp[0].node) {
		     SwigType *decltype = Getattr(yyvsp[0].node,"decl");
		     if (Cmp(yyvsp[-8].id,"typedef") == 0) {
		       if (!decltype || !Len(decltype)) {
			 name = Getattr(yyvsp[0].node,"name");
			 Setattr(yyval.node,"tdname",Copy(name));

			 /* Use typedef name as class name */
			 if (class_rename && (Strcmp(class_rename,yyvsp[-6].str) == 0)) {
			   class_rename = NewString(name);
			 }
			 if (!Getattr(classes,name)) {
			   Setattr(classes,name,yyval.node);
			 }
			 Setattr(yyval.node,"decl",decltype);
		       }
		     }
		   }
		   appendChild(yyval.node,dump_nested(Char(name)));
		 }

		 if (cplus_mode != CPLUS_PUBLIC) {
		 /* we 'open' the class at the end, to allow %template
		    to add new members */
		   Node *pa = new_node("access");
		   Setattr(pa,"kind","public");
		   cplus_mode = CPLUS_PUBLIC;
		   appendChild(yyval.node,pa);
		 }

		 Setattr(yyval.node,"symtab",Swig_symbol_popscope());

		 Classprefix = 0;
		 if (nscope_inner) {
		   /* this is tricky */
		   /* we add the declaration in the original namespace */
		   appendChild(nscope_inner,yyval.node);
		   Swig_symbol_setscope(Getattr(nscope_inner,"symtab"));
		   Namespaceprefix = Swig_symbol_qualifiedscopename(0);
		   add_symbols(yyval.node);
		   if (nscope) yyval.node = nscope;
		   /* but the variable definition in the current scope */
		   Swig_symbol_setscope(cscope);
		   Namespaceprefix = Swig_symbol_qualifiedscopename(0);
		   add_symbols(yyvsp[0].node);
		 } else {
		   yyrename = NewString(class_rename);
		   Namespaceprefix = Swig_symbol_qualifiedscopename(0);

		   add_symbols(yyval.node);
		   add_symbols(yyvsp[0].node);
		 }
		 Swig_symbol_setscope(cscope);
		 Namespaceprefix = Swig_symbol_qualifiedscopename(0);
	       }
    break;

  case 136:
#line 3047 "parser.y"
    {
	       class_rename = make_name(0,0);
	       if (strcmp(yyvsp[-1].id,"class") == 0) {
		 cplus_mode = CPLUS_PRIVATE;
	       } else {
		 cplus_mode = CPLUS_PUBLIC;
	       }
	       Swig_symbol_newscope();
	       cparse_start_line = cparse_line;
	       inclass = 1;
	       Classprefix = NewString("");
	       Namespaceprefix = Swig_symbol_qualifiedscopename(0);
             }
    break;

  case 137:
#line 3059 "parser.y"
    {
	       String *unnamed;
	       Node *n;
	       Classprefix = 0;
	       inclass = 0;
	       unnamed = make_unnamed();
	       yyval.node = new_node("class");
	       Setline(yyval.node,cparse_start_line);
	       Setattr(yyval.node,"kind",yyvsp[-6].id);
	       Setattr(yyval.node,"storage",yyvsp[-7].id);
	       Setattr(yyval.node,"unnamed",unnamed);
	       Setattr(yyval.node,"allows_typedef","1");

	       /* Check for pure-abstract class */
	       Setattr(yyval.node,"abstract", pure_abstract(yyvsp[-3].node));

	       n = new_node("cdecl");
	       Setattr(n,"name",yyvsp[-1].decl.id);
	       Setattr(n,"unnamed",unnamed);
	       Setattr(n,"type",unnamed);
	       Setattr(n,"decl",yyvsp[-1].decl.type);
	       Setattr(n,"parms",yyvsp[-1].decl.parms);
	       Setattr(n,"storage",yyvsp[-7].id);
	       if (yyvsp[0].node) {
		 Node *p = yyvsp[0].node;
		 set_nextSibling(n,p);
		 while (p) {
		   Setattr(p,"unnamed",unnamed);
		   Setattr(p,"type",Copy(unnamed));
		   Setattr(p,"storage",yyvsp[-7].id);
		   p = nextSibling(p);
		 }
	       }
	       set_nextSibling(yyval.node,n);
	       {
		 /* If a proper typedef name was given, we'll use it to set the scope name */
		 String *name = 0;
		 if (yyvsp[-7].id && (strcmp(yyvsp[-7].id,"typedef") == 0)) {
		   if (!Len(yyvsp[-1].decl.type)) {	
		     name = yyvsp[-1].decl.id;
		     Setattr(yyval.node,"tdname",name);
		     Setattr(yyval.node,"name",name);
		     Swig_symbol_setscopename(name);

		     /* If a proper name given, we use that as the typedef, not unnamed */
		     Clear(unnamed);
		     Append(unnamed, name);
		     
		     n = nextSibling(n);
		     set_nextSibling(yyval.node,n);

		     /* Check for previous extensions */
		     if (extendhash) {
		       String *clsname = Swig_symbol_qualifiedscopename(0);
		       Node *am = Getattr(extendhash,clsname);
		       if (am) {
			 /* Merge the extension into the symbol table */
			 merge_extensions(yyval.node,am);
			 appendChild(yyval.node,am);
			 Delattr(extendhash,clsname);
		       }
		       Delete(clsname);
		     }
		     if (!classes) classes = NewHash();
		     Setattr(classes,Swig_symbol_qualifiedscopename(0),yyval.node);
		   } else {
		     Swig_symbol_setscopename((char*)"<unnamed>");
		   }
		 }
		 appendChild(yyval.node,yyvsp[-3].node);
		 appendChild(yyval.node,dump_nested(Char(name)));
	       }
	       /* Pop the scope */
	       Setattr(yyval.node,"symtab",Swig_symbol_popscope());
	       if (class_rename) {
		 yyrename = NewString(class_rename);
	       }
	       Namespaceprefix = Swig_symbol_qualifiedscopename(0);
	       add_symbols(yyval.node);
	       add_symbols(n);
              }
    break;

  case 138:
#line 3142 "parser.y"
    { yyval.node = 0; }
    break;

  case 139:
#line 3143 "parser.y"
    {
                        yyval.node = new_node("cdecl");
                        Setattr(yyval.node,"name",yyvsp[-1].decl.id);
                        Setattr(yyval.node,"decl",yyvsp[-1].decl.type);
                        Setattr(yyval.node,"parms",yyvsp[-1].decl.parms);
			set_nextSibling(yyval.node,yyvsp[0].node);
                    }
    break;

  case 140:
#line 3155 "parser.y"
    {
              if (yyvsp[-3].id && (Strcmp(yyvsp[-3].id,"friend") == 0)) {
		/* Ignore */
                yyval.node = 0; 
	      } else {
		yyval.node = new_node("classforward");
		Setfile(yyval.node,cparse_file);
		Setline(yyval.node,cparse_line);
		Setattr(yyval.node,"kind",yyvsp[-2].id);
		Setattr(yyval.node,"name",yyvsp[-1].str);
		Setattr(yyval.node,"sym:weak", "1");
		add_symbols(yyval.node);
	      }
             }
    break;

  case 141:
#line 3175 "parser.y"
    { template_parameters = yyvsp[-1].tparms; }
    break;

  case 142:
#line 3175 "parser.y"
    {
		      String *tname = 0;
		      int     error = 0;

		      /* check if we get a namespace node with a class declaration, and retrieve the class */
		      Symtab *cscope = Swig_symbol_current();
		      Symtab *sti = 0;
		      Node *ntop = yyvsp[0].node;
		      Node *ni = ntop;
		      SwigType *ntype = ni ? nodeType(ni) : 0;
		      while (ni && Strcmp(ntype,"namespace") == 0) {
			sti = Getattr(ni,"symtab");
			ni = firstChild(ni);
			ntype = nodeType(ni);
		      }
		      if (sti) {
			Swig_symbol_setscope(sti);
			Namespaceprefix = Swig_symbol_qualifiedscopename(0);
			yyvsp[0].node = ni;
		      }

                      template_parameters = 0;
                      yyval.node = yyvsp[0].node;
		      if (yyval.node) tname = Getattr(yyval.node,"name");
		      
		      /* Check if the class is a template specialization */
		      if ((yyval.node) && (Strstr(tname,"<")) && (Strncmp(tname,"operator ",9) != 0)) {
			/* If a specialization.  Check if defined. */
			Node *tempn = 0;
			{
			  String *tbase = SwigType_templateprefix(tname);
			  tempn = Swig_symbol_clookup_local(tbase,0);
			  if (!tempn || (Strcmp(nodeType(tempn),"template") != 0)) {
			    SWIG_WARN_NODE_BEGIN(tempn);
			    Swig_warning(WARN_PARSE_TEMPLATE_SP_UNDEF, Getfile(yyval.node),Getline(yyval.node),"Specialization of non-template '%s'.\n", tbase);
			    SWIG_WARN_NODE_END(tempn);
			    tempn = 0;
			    error = 1;
			  }
			  Delete(tbase);
			}
			Setattr(yyval.node,"specialization","1");
			Setattr(yyval.node,"templatetype",nodeType(yyval.node));
			set_nodeType(yyval.node,"template");
			/* Template partial specialization */
			if (tempn && (yyvsp[-3].tparms) && (yyvsp[0].node)) {
			  List   *tlist;
			  String *targs = SwigType_templateargs(tname);
			  tlist = SwigType_parmlist(targs);
			  /*			  Printf(stdout,"targs = '%s' %s\n", targs, tlist); */
			  if (!Getattr(yyval.node,"sym:weak")) {
			    Setattr(yyval.node,"sym:typename","1");
			  }
			  
			  if (Len(tlist) != ParmList_len(Getattr(tempn,"templateparms"))) {
			    Swig_error(Getfile(yyval.node),Getline(yyval.node),"Inconsistent argument count in template partial specialization. %d %d\n", Len(tlist), ParmList_len(Getattr(tempn,"templateparms")));
			    
			  } else {

			  /* This code builds the argument list for the partial template
                             specialization.  This is a little hairy, but the idea is as
                             follows:

                             $3 contains a list of arguments supplied for the template.
                             For example template<class T>.

                             tlist is a list of the specialization arguments--which may be
                             different.  For example class<int,T>.

                             tp is a copy of the arguments in the original template definition.
     
                             The patching algorithm walks through the list of supplied
                             arguments ($3), finds the position in the specialization arguments
                             (tlist), and then patches the name in the argument list of the
                             original template.
			  */

			  {
			    String *pn;
			    Parm *p, *p1;
			    int i, nargs;
			    Parm *tp = CopyParmList(Getattr(tempn,"templateparms"));
			    nargs = Len(tlist);
			    p = yyvsp[-3].tparms;
			    while (p) {
			      for (i = 0; i < nargs; i++){
				pn = Getattr(p,"name");
				if (Strcmp(pn,SwigType_base(Getitem(tlist,i))) == 0) {
				  int j;
				  Parm *p1 = tp;
				  for (j = 0; j < i; j++) {
				    p1 = nextSibling(p1);
				  }
				  Setattr(p1,"name",pn);
				  Setattr(p1,"partialarg","1");
				}
			      }
			      p = nextSibling(p);
			    }
			    p1 = tp;
			    i = 0;
			    while (p1) {
			      if (!Getattr(p1,"partialarg")) {
				Delattr(p1,"name");
				Setattr(p1,"type", Getitem(tlist,i));
			      } 
			      i++;
			      p1 = nextSibling(p1);
			    }
			    Setattr(yyval.node,"templateparms",tp);
			    Delete(tp);
			  }
#if 0
			  /* Patch the parameter list */
			  if (tempn) {
			    Parm *p,*p1;
			    ParmList *tp = CopyParmList(Getattr(tempn,"templateparms"));
			    p = yyvsp[-3].tparms;
			    p1 = tp;
			    while (p && p1) {
			      String *pn = Getattr(p,"name");
			      Printf(stdout,"pn = '%s'\n", pn);
			      if (pn) Setattr(p1,"name",pn);
			      else Delattr(p1,"name");
			      pn = Getattr(p,"type");
			      if (pn) Setattr(p1,"type",pn);
			      p = nextSibling(p);
			      p1 = nextSibling(p1);
			    }
			    Setattr(yyval.node,"templateparms",tp);
			    Delete(tp);
			  } else {
			    Setattr(yyval.node,"templateparms",yyvsp[-3].tparms);
			  }
#endif
			  Delattr(yyval.node,"specialization");
			  Setattr(yyval.node,"partialspecialization","1");
			  /* Create a specialized name for matching */
			  {
			    Parm *p = yyvsp[-3].tparms;
			    String *fname = NewString(Getattr(yyval.node,"name"));
			    String *ffname = 0;

			    char   tmp[32];
			    int    i;
			    while (p) {
			      String *n = Getattr(p,"name");
			      if (!n) {
				p = nextSibling(p);
				continue;
			      }
			      for (i = 0; i < Len(tlist); i++) {
				if (Strstr(Getitem(tlist,i),n)) {
				  sprintf(tmp,"$%d",i+1);
				  Replaceid(fname,n,tmp);
				}
			      }
			      p = nextSibling(p);
			    }
			    /* Patch argument names with typedef */
			    {
			      Iterator tt;
			      List *tparms = SwigType_parmlist(fname);
			      ffname = SwigType_templateprefix(fname);
			      Append(ffname,"<(");
			      for (tt = First(tparms); tt.item; ) {
				SwigType *rtt = Swig_symbol_typedef_reduce(tt.item,0);
				SwigType *ttr = Swig_symbol_type_qualify(rtt,0);
				Append(ffname,ttr);
				tt = Next(tt);
				if (tt.item) Putc(',',ffname);
				Delete(rtt);
				Delete(ttr);
			      }
			      Append(ffname,")>");
			    }
			    {
			      String *partials = Getattr(tempn,"partials");
			      if (!partials) {
				partials = NewList();
				Setattr(tempn,"partials",partials);
			      }
			      /*			      Printf(stdout,"partial: fname = '%s', '%s'\n", fname, Swig_symbol_typedef_reduce(fname,0)); */
			      Append(partials,ffname);
			    }
			    Setattr(yyval.node,"partialargs",ffname);
			    Swig_symbol_cadd(ffname,yyval.node);
			  }
			  }
			  Delete(tlist);
			  Delete(targs);
			} else {
			  /* Need to resolve exact specialization name */
			  /* add default args from generic template */
			  String *ty = Swig_symbol_template_deftype(tname,0);
			  String *fname = Swig_symbol_type_qualify(ty,0);
			  Swig_symbol_cadd(fname,yyval.node);
			  Delete(ty);
			  Delete(fname);
			}
		      }  else if (yyval.node) {
			Setattr(yyval.node,"templatetype",nodeType(yyvsp[0].node));
			set_nodeType(yyval.node,"template");
			Setattr(yyval.node,"templateparms", yyvsp[-3].tparms);
			if (!Getattr(yyval.node,"sym:weak")) {
			  Setattr(yyval.node,"sym:typename","1");
			}
			add_symbols(yyval.node);
                        default_arguments(yyval.node);
			/* We also place a fully parameterized version in the symbol table */
			{
			  Parm *p;
			  String *fname = NewStringf("%s<(",Getattr(yyval.node,"name"));
			  p = yyvsp[-3].tparms;
			  while (p) {
			    String *n = Getattr(p,"name");
			    if (!n) n = Getattr(p,"type");
			    Printf(fname,"%s", n);
			    p = nextSibling(p);
			    if (p) Putc(',',fname);
			  }
			  Printf(fname,")>");
			  Swig_symbol_cadd(fname,yyval.node);
			}
		      }
		      yyval.node = ntop;
		      Swig_symbol_setscope(cscope);
		      Namespaceprefix = Swig_symbol_qualifiedscopename(0);
		      if (error) yyval.node = 0;
                  }
    break;

  case 143:
#line 3405 "parser.y"
    {
		  Swig_warning(WARN_PARSE_EXPLICIT_TEMPLATE, cparse_file, cparse_line, "Explicit template instantiation ignored.\n");
                   yyval.node = 0; 
                }
    break;

  case 144:
#line 3411 "parser.y"
    {
		  yyval.node = yyvsp[0].node;
                }
    break;

  case 145:
#line 3414 "parser.y"
    {
                   yyval.node = yyvsp[0].node;
                }
    break;

  case 146:
#line 3417 "parser.y"
    {
                   yyval.node = yyvsp[0].node;
                }
    break;

  case 147:
#line 3420 "parser.y"
    {
		  yyval.node = 0;
                }
    break;

  case 148:
#line 3423 "parser.y"
    {
                  yyval.node = yyvsp[0].node;
                }
    break;

  case 149:
#line 3428 "parser.y"
    {
		   /* Rip out the parameter names */
		  Parm *p = yyvsp[0].pl;
		  yyval.tparms = yyvsp[0].pl;

		  while (p) {
		    String *name = Getattr(p,"name");
		    if (!name) {
		      /* Hmmm. Maybe it's a 'class T' parameter */
		      char *type = Char(Getattr(p,"type"));
		      /* Template template parameter */
		      if (strncmp(type,"template<class> ",16) == 0) {
			type += 16;
		      }
		      if ((strncmp(type,"class ",6) == 0) || (strncmp(type,"typename ", 9) == 0)) {
			char *t = strchr(type,' ');
			Setattr(p,"name", t+1);
		      } else {
			/*
			 Swig_error(cparse_file, cparse_line, "Missing template parameter name\n");
			 $$.rparms = 0;
			 $$.parms = 0;
			 break; */
		      }
		    }
		    p = nextSibling(p);
		  }
                 }
    break;

  case 150:
#line 3460 "parser.y"
    {
                  String *uname = Swig_symbol_type_qualify(yyvsp[-1].str,0);
                  yyval.node = new_node("using");
		  Setattr(yyval.node,"uname",uname);
		  Setattr(yyval.node,"name", Swig_scopename_last(yyvsp[-1].str));
		  add_symbols(yyval.node);
             }
    break;

  case 151:
#line 3467 "parser.y"
    {
	       Node *n = Swig_symbol_clookup(yyvsp[-1].str,0);
	       if (!n) {
		 Swig_error(cparse_file, cparse_line, "Nothing known about namespace '%s'\n", yyvsp[-1].str);
		 yyval.node = 0;
	       } else {

		 while (Strcmp(nodeType(n),"using") == 0) {
		   n = Getattr(n,"node");
		 }
		 if (n) {
		   if (Strcmp(nodeType(n),"namespace") == 0) {
		     yyval.node = new_node("using");
		     Setattr(yyval.node,"node",n);
		     Setattr(yyval.node,"namespace", yyvsp[-1].str);
		     Swig_symbol_inherit(Getattr(n,"symtab"));
		   } else {
		     Swig_error(cparse_file, cparse_line, "'%s' is not a namespace.\n", yyvsp[-1].str);
		     yyval.node = 0;
		   }
		 } else {
		   yyval.node = 0;
		 }
	       }
             }
    break;

  case 152:
#line 3494 "parser.y"
    { 
                Hash *h;
                yyvsp[-2].node = Swig_symbol_current();
		h = Swig_symbol_clookup(yyvsp[-1].str,0);
		if (h && (Strcmp(nodeType(h),"namespace") == 0)) {
		  if (Getattr(h,"alias")) {
		    h = Getattr(h,"namespace");
		    Swig_warning(WARN_PARSE_NAMESPACE_ALIAS, cparse_file, cparse_line, "Namespace alias '%s' not allowed here. Assuming '%s'\n",
				 yyvsp[-1].str, Getattr(h,"name"));
		    yyvsp[-1].str = Getattr(h,"name");
		  }
		  Swig_symbol_setscope(Getattr(h,"symtab"));
		} else {
		  Swig_symbol_newscope();
		  Swig_symbol_setscopename(yyvsp[-1].str);
		}
		Namespaceprefix = Swig_symbol_qualifiedscopename(0);
             }
    break;

  case 153:
#line 3511 "parser.y"
    {
                Node *n = yyvsp[-1].node;
		set_nodeType(n,"namespace");
		Setattr(n,"name",yyvsp[-4].str);
                Setattr(n,"symtab", Swig_symbol_popscope());
		Swig_symbol_setscope(yyvsp[-5].node);
		yyval.node = n;
		Namespaceprefix = Swig_symbol_qualifiedscopename(0);
		add_symbols(yyval.node);
             }
    break;

  case 154:
#line 3521 "parser.y"
    {
	       Hash *h;
	       yyvsp[-1].node = Swig_symbol_current();
	       h = Swig_symbol_clookup((char *)"    ",0);
	       if (h && (Strcmp(nodeType(h),"namespace") == 0)) {
		 Swig_symbol_setscope(Getattr(h,"symtab"));
	       } else {
		 Swig_symbol_newscope();
		 /* we don't use "__unnamed__", but a long 'empty' name */
		 Swig_symbol_setscopename("    ");
	       }
	       Namespaceprefix = 0;
             }
    break;

  case 155:
#line 3533 "parser.y"
    {
	       yyval.node = yyvsp[-1].node;
	       set_nodeType(yyval.node,"namespace");
	       Setattr(yyval.node,"unnamed","1");
	       Setattr(yyval.node,"symtab", Swig_symbol_popscope());
	       Swig_symbol_setscope(yyvsp[-4].node);
	       Namespaceprefix = Swig_symbol_qualifiedscopename(0);
	       add_symbols(yyval.node);
             }
    break;

  case 156:
#line 3542 "parser.y"
    {
	       /* Namespace alias */
	       Node *n;
	       yyval.node = new_node("namespace");
	       Setattr(yyval.node,"name",yyvsp[-3].id);
	       Setattr(yyval.node,"alias",yyvsp[-1].str);
	       n = Swig_symbol_clookup(yyvsp[-1].str,0);
	       if (!n) {
		 Swig_error(cparse_file, cparse_line, "Unknown namespace '%s'\n", yyvsp[-1].str);
		 yyval.node = 0;
	       } else {
		 if (Strcmp(nodeType(n),"namespace") != 0) {
		   Swig_error(cparse_file, cparse_line, "'%s' is not a namespace\n",yyvsp[-1].str);
		   yyval.node = 0;
		 } else {
		   while (Getattr(n,"alias")) {
		     n = Getattr(n,"namespace");
		   }
		   Setattr(yyval.node,"namespace",n);
		   add_symbols(yyval.node);
		   /* Set up a scope alias */
		   Swig_symbol_alias(yyvsp[-3].id,Getattr(n,"symtab"));
		 }
	       }
             }
    break;

  case 157:
#line 3569 "parser.y"
    {
                   yyval.node = yyvsp[-1].node;
                   /* Insert cpp_member (including any siblings) to the front of the cpp_members linked list */
		   if (yyval.node) {
		     Node *p = yyval.node;
		     Node *pp =0;
		     while (p) {
		       pp = p;
		       p = nextSibling(p);
		     }
		     set_nextSibling(pp,yyvsp[0].node);
		   } else {
		     yyval.node = yyvsp[0].node;
		   }
             }
    break;

  case 158:
#line 3584 "parser.y"
    { 
                  if (cplus_mode != CPLUS_PUBLIC) {
		     Swig_error(cparse_file,cparse_line,"%%extend can only be used in a public section\n");
		  }
             }
    break;

  case 159:
#line 3588 "parser.y"
    {
	       yyval.node = new_node("extend");
	       Swig_tag_nodes(yyvsp[-2].node,"feature:extend",(char*) "1");
	       appendChild(yyval.node,yyvsp[-2].node);
	       set_nextSibling(yyval.node,yyvsp[0].node);
	     }
    break;

  case 160:
#line 3594 "parser.y"
    { yyval.node = 0;}
    break;

  case 161:
#line 3595 "parser.y"
    {
	       skip_decl();
		   {
		     static int last_error_line = -1;
		     if (last_error_line != cparse_line) {
		       Swig_error(cparse_file, cparse_line,"Syntax error in input.\n");
		       last_error_line = cparse_line;
		     }
		   }
	     }
    break;

  case 162:
#line 3604 "parser.y"
    { 
                yyval.node = yyvsp[0].node;
             }
    break;

  case 163:
#line 3615 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 164:
#line 3616 "parser.y"
    { 
                 yyval.node = yyvsp[0].node; 
		 if (extendmode) {
		   String *symname;
		   symname= make_name(Getattr(yyval.node,"name"), Getattr(yyval.node,"decl"));
		   if (Strcmp(symname,Getattr(yyval.node,"name")) == 0) {
		     /* No renaming operation.  Set name to class name */
		     yyrename = NewString(Getattr(current_class,"sym:name"));
		   } else {
		     yyrename = symname;
		   }
		 }
		 add_symbols(yyval.node);
                 default_arguments(yyval.node);
             }
    break;

  case 165:
#line 3631 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 166:
#line 3632 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 167:
#line 3633 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 168:
#line 3634 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 169:
#line 3635 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 170:
#line 3636 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 171:
#line 3637 "parser.y"
    { yyval.node = 0; }
    break;

  case 172:
#line 3638 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 173:
#line 3639 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 174:
#line 3640 "parser.y"
    { yyval.node = 0; }
    break;

  case 175:
#line 3641 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 176:
#line 3642 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 177:
#line 3643 "parser.y"
    { yyval.node = 0; }
    break;

  case 178:
#line 3644 "parser.y"
    {yyval.node = yyvsp[0].node; }
    break;

  case 179:
#line 3645 "parser.y"
    {yyval.node = yyvsp[0].node; }
    break;

  case 180:
#line 3646 "parser.y"
    { yyval.node = 0; }
    break;

  case 181:
#line 3655 "parser.y"
    {
              if (Classprefix) {
		 SwigType *decl = NewString("");
		 yyval.node = new_node("constructor");
		 Setattr(yyval.node,"name",yyvsp[-4].type);
		 Setattr(yyval.node,"parms",yyvsp[-2].pl);
		 SwigType_add_function(decl,yyvsp[-2].pl);
		 Setattr(yyval.node,"decl",decl);
		 Setattr(yyval.node,"throws",yyvsp[0].decl.throws);
		 Setattr(yyval.node,"throw",yyvsp[0].decl.throw);
		 if (Len(scanner_ccode)) {
		   Setattr(yyval.node,"code",Copy(scanner_ccode));
		 }
		 Setattr(yyval.node,"feature:new","1");
	      } else {
		yyval.node = 0;
              }
              }
    break;

  case 182:
#line 3677 "parser.y"
    {
               yyval.node = new_node("destructor");
	       Setattr(yyval.node,"name",NewStringf("~%s",yyvsp[-4].str));
	       if (Len(scanner_ccode)) {
		 Setattr(yyval.node,"code",Copy(scanner_ccode));
	       }
	       {
		 String *decl = NewString("");
		 SwigType_add_function(decl,yyvsp[-2].pl);
		 Setattr(yyval.node,"decl",decl);
	       }
	       Setattr(yyval.node,"throws",yyvsp[0].dtype.throws);
	       Setattr(yyval.node,"throw",yyvsp[0].dtype.throw);
	       add_symbols(yyval.node);
	      }
    break;

  case 183:
#line 3695 "parser.y"
    {
		yyval.node = new_node("destructor");
	       /* Check for template names.  If the class is a template
		  and the constructor is missing the template part, we
		  add it */
	       {
		 char *c = Strstr(Classprefix,"<");
		 if (c) {
		   if (!Strstr(yyvsp[-4].str,"<")) {
		     yyvsp[-4].str = NewStringf("%s%s",yyvsp[-4].str,c);
		   }
		 }
	       }
		Setattr(yyval.node,"storage","virtual");
		Setattr(yyval.node,"name",NewStringf("~%s",yyvsp[-4].str));
		Setattr(yyval.node,"throws",yyvsp[0].dtype.throws);
		Setattr(yyval.node,"throw",yyvsp[0].dtype.throw);
		if (yyvsp[0].dtype.val) {
		  Setattr(yyval.node,"value","0");
		}
		if (Len(scanner_ccode)) {
		  Setattr(yyval.node,"code",Copy(scanner_ccode));
		}
		{
		  String *decl = NewString("");
		  SwigType_add_function(decl,yyvsp[-2].pl);
		  Setattr(yyval.node,"decl",decl);
		}

		add_symbols(yyval.node);
	      }
    break;

  case 184:
#line 3730 "parser.y"
    {
                 yyval.node = new_node("cdecl");
                 Setattr(yyval.node,"type",yyvsp[-5].type);
		 Setattr(yyval.node,"name",yyvsp[-6].str);

		 SwigType_add_function(yyvsp[-4].type,yyvsp[-2].pl);
		 if (yyvsp[0].dtype.qualifier) {
		   SwigType_push(yyvsp[-4].type,yyvsp[0].dtype.qualifier);
		 }
		 Setattr(yyval.node,"decl",yyvsp[-4].type);
		 Setattr(yyval.node,"parms",yyvsp[-2].pl);
		 Setattr(yyval.node,"conversion_operator","1");
		 add_symbols(yyval.node);
              }
    break;

  case 185:
#line 3744 "parser.y"
    {
		 SwigType *decl;
                 yyval.node = new_node("cdecl");
                 Setattr(yyval.node,"type",yyvsp[-5].type);
		 Setattr(yyval.node,"name",yyvsp[-6].str);
		 decl = NewString("");
		 SwigType_add_reference(decl);
		 SwigType_add_function(decl,yyvsp[-2].pl);
		 if (yyvsp[0].dtype.qualifier) {
		   SwigType_push(decl,yyvsp[0].dtype.qualifier);
		 }
		 Setattr(yyval.node,"decl",decl);
		 Setattr(yyval.node,"parms",yyvsp[-2].pl);
		 Setattr(yyval.node,"conversion_operator","1");
		 add_symbols(yyval.node);
	       }
    break;

  case 186:
#line 3761 "parser.y"
    {
		String *t = NewString("");
		yyval.node = new_node("cdecl");
		Setattr(yyval.node,"type",yyvsp[-4].type);
		Setattr(yyval.node,"name",yyvsp[-5].str);
		SwigType_add_function(t,yyvsp[-2].pl);
		if (yyvsp[0].dtype.qualifier) {
		  SwigType_push(t,yyvsp[0].dtype.qualifier);
		}
		Setattr(yyval.node,"decl",t);
		Setattr(yyval.node,"parms",yyvsp[-2].pl);
		Setattr(yyval.node,"conversion_operator","1");
		add_symbols(yyval.node);
              }
    break;

  case 187:
#line 3779 "parser.y"
    {
                 skip_balanced('{','}');
                 yyval.node = 0;
               }
    break;

  case 188:
#line 3786 "parser.y"
    { 
                yyval.node = new_node("access");
		Setattr(yyval.node,"kind","public");
                cplus_mode = CPLUS_PUBLIC;
              }
    break;

  case 189:
#line 3793 "parser.y"
    { 
                yyval.node = new_node("access");
                Setattr(yyval.node,"kind","private");
		cplus_mode = CPLUS_PRIVATE;
	      }
    break;

  case 190:
#line 3801 "parser.y"
    { 
		yyval.node = new_node("access");
		Setattr(yyval.node,"kind","protected");
		cplus_mode = CPLUS_PROTECTED;
	      }
    break;

  case 191:
#line 3824 "parser.y"
    { cparse_start_line = cparse_line; skip_balanced('{','}');
	      }
    break;

  case 192:
#line 3825 "parser.y"
    {
	        yyval.node = 0;
		if (cplus_mode == CPLUS_PUBLIC) {
		  if (yyvsp[-1].decl.id) {
		    if (strcmp(yyvsp[-5].id,"class") == 0) {
		      Swig_warning(WARN_PARSE_NESTED_CLASS, cparse_file, cparse_line, "Nested classes not currently supported (ignored).\n");
		      /* Generate some code for a new class */
		    } else {
		      Nested *n = (Nested *) malloc(sizeof(Nested));
		      n->code = NewString("");
		      Printv(n->code, "typedef ", yyvsp[-5].id, " ",
			     Char(scanner_ccode), " $classname_", yyvsp[-1].decl.id, ";\n", NIL);

		      n->name = Swig_copy_string(yyvsp[-1].decl.id);
		      n->line = cparse_start_line;
		      n->type = NewString("");
		      n->kind = yyvsp[-5].id;
		      SwigType_push(n->type, yyvsp[-1].decl.type);
		      n->next = 0;
		      add_nested(n);
		    }
		  } else {
		    Swig_warning(WARN_PARSE_NESTED_CLASS, cparse_file, cparse_line, "Nested %s not currently supported (ignored).\n", yyvsp[-5].id);
		  }
		}
	      }
    break;

  case 193:
#line 3852 "parser.y"
    { cparse_start_line = cparse_line; skip_balanced('{','}');
              }
    break;

  case 194:
#line 3853 "parser.y"
    {
	        yyval.node = 0;
		if (cplus_mode == CPLUS_PUBLIC) {
		  if (strcmp(yyvsp[-4].id,"class") == 0) {
		    Swig_warning(WARN_PARSE_NESTED_CLASS,cparse_file, cparse_line,"Nested class not currently supported (ignored)\n");
		    /* Generate some code for a new class */
		  } else if (yyvsp[-1].decl.id) {
		    /* Generate some code for a new class */
		    Nested *n = (Nested *) malloc(sizeof(Nested));
		    n->code = NewString("");
		    Printv(n->code, "typedef ", yyvsp[-4].id, " " ,
			    Char(scanner_ccode), " $classname_", yyvsp[-1].decl.id, ";\n",NIL);
		    n->name = Swig_copy_string(yyvsp[-1].decl.id);
		    n->line = cparse_start_line;
		    n->type = NewString("");
		    n->kind = yyvsp[-4].id;
		    SwigType_push(n->type,yyvsp[-1].decl.type);
		    n->next = 0;
		    add_nested(n);
		  } else {
		    Swig_warning(WARN_PARSE_NESTED_CLASS, cparse_file, cparse_line, "Nested %s not currently supported (ignored).\n", yyvsp[-4].id);
		  }
		}
	      }
    break;

  case 195:
#line 3882 "parser.y"
    { cparse_start_line = cparse_line; skip_balanced('{','}');
              }
    break;

  case 196:
#line 3883 "parser.y"
    {
	        yyval.node = 0;
		if (cplus_mode == CPLUS_PUBLIC) {
		  Swig_warning(WARN_PARSE_NESTED_CLASS,cparse_file, cparse_line,"Nested class not currently supported (ignored)\n");
		}
	      }
    break;

  case 197:
#line 3900 "parser.y"
    { yyval.decl = yyvsp[0].decl;}
    break;

  case 198:
#line 3901 "parser.y"
    { yyval.decl.id = 0; }
    break;

  case 199:
#line 3907 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 200:
#line 3910 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 201:
#line 3914 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 202:
#line 3917 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 203:
#line 3918 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 204:
#line 3919 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 205:
#line 3920 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 206:
#line 3921 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 207:
#line 3922 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 208:
#line 3923 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 209:
#line 3924 "parser.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 210:
#line 3927 "parser.y"
    {
	            Clear(scanner_ccode);
		    yyval.dtype.throws = yyvsp[-1].dtype.throws;
		    yyval.dtype.throw = yyvsp[-1].dtype.throw;
               }
    break;

  case 211:
#line 3932 "parser.y"
    { 
		    skip_balanced('{','}'); 
		    yyval.dtype.throws = yyvsp[-1].dtype.throws;
		    yyval.dtype.throw = yyvsp[-1].dtype.throw;
	       }
    break;

  case 212:
#line 3939 "parser.y"
    { 
                     Clear(scanner_ccode);
                     yyval.dtype.val = 0;
                     yyval.dtype.qualifier = yyvsp[-1].dtype.qualifier;
                     yyval.dtype.bitfield = 0;
                     yyval.dtype.throws = yyvsp[-1].dtype.throws;
                     yyval.dtype.throw = yyvsp[-1].dtype.throw;
                }
    break;

  case 213:
#line 3947 "parser.y"
    { 
                     Clear(scanner_ccode);
                     yyval.dtype.val = yyvsp[-1].dtype.val;
                     yyval.dtype.qualifier = yyvsp[-3].dtype.qualifier;
                     yyval.dtype.bitfield = 0;
                     yyval.dtype.throws = yyvsp[-3].dtype.throws; 
                     yyval.dtype.throw = yyvsp[-3].dtype.throw; 
               }
    break;

  case 214:
#line 3955 "parser.y"
    { 
                     skip_balanced('{','}');
                     yyval.dtype.val = 0;
                     yyval.dtype.qualifier = yyvsp[-1].dtype.qualifier;
                     yyval.dtype.bitfield = 0;
                     yyval.dtype.throws = yyvsp[-1].dtype.throws; 
                     yyval.dtype.throw = yyvsp[-1].dtype.throw; 
               }
    break;

  case 215:
#line 3966 "parser.y"
    { }
    break;

  case 216:
#line 3972 "parser.y"
    { yyval.id = "extern"; }
    break;

  case 217:
#line 3973 "parser.y"
    { 
                   if (strcmp(yyvsp[0].id,"C") == 0) {
		     yyval.id = "externc";
		   } else {
		     Swig_warning(WARN_PARSE_UNDEFINED_EXTERN,cparse_file, cparse_line,"Unrecognized extern type \"%s\".\n", yyvsp[0].id);
		     yyval.id = 0;
		   }
               }
    break;

  case 218:
#line 3981 "parser.y"
    { yyval.id = "static"; }
    break;

  case 219:
#line 3982 "parser.y"
    { yyval.id = "typedef"; }
    break;

  case 220:
#line 3983 "parser.y"
    { yyval.id = "virtual"; }
    break;

  case 221:
#line 3984 "parser.y"
    { yyval.id = "friend"; }
    break;

  case 222:
#line 3985 "parser.y"
    { yyval.id = 0; }
    break;

  case 223:
#line 3992 "parser.y"
    {
                 Parm *p;
		 yyval.pl = yyvsp[0].pl;
		 p = yyvsp[0].pl;
                 while (p) {
		   Replace(Getattr(p,"type"),"typename ", "", DOH_REPLACE_ANY);
		   p = nextSibling(p);
                 }
               }
    break;

  case 224:
#line 4003 "parser.y"
    {
		  if (1) { 
		    set_nextSibling(yyvsp[-1].p,yyvsp[0].pl);
		    yyval.pl = yyvsp[-1].p;
		  } else {
		    yyval.pl = yyvsp[0].pl;
		  }
		}
    break;

  case 225:
#line 4011 "parser.y"
    { yyval.pl = 0; }
    break;

  case 226:
#line 4014 "parser.y"
    {
                 set_nextSibling(yyvsp[-1].p,yyvsp[0].pl);
		 yyval.pl = yyvsp[-1].p;
                }
    break;

  case 227:
#line 4018 "parser.y"
    { yyval.pl = 0; }
    break;

  case 228:
#line 4022 "parser.y"
    {
                   SwigType_push(yyvsp[-1].type,yyvsp[0].decl.type);
		   yyval.p = NewParm(yyvsp[-1].type,yyvsp[0].decl.id);
		   Setfile(yyval.p,cparse_file);
		   Setline(yyval.p,cparse_line);
		   if (yyvsp[0].decl.defarg) {
		     Setattr(yyval.p,"value",yyvsp[0].decl.defarg);
		   }
		}
    break;

  case 229:
#line 4032 "parser.y"
    {
                  yyval.p = NewParm(NewStringf("template<class> %s %s", yyvsp[-1].id,yyvsp[0].str), 0);
		  Setfile(yyval.p,cparse_file);
		  Setline(yyval.p,cparse_line);
                }
    break;

  case 230:
#line 4037 "parser.y"
    {
		  SwigType *t = NewString("v(...)");
		  yyval.p = NewParm(t, 0);
		  Setfile(yyval.p,cparse_file);
		  Setline(yyval.p,cparse_line);
		}
    break;

  case 231:
#line 4045 "parser.y"
    {
                 Parm *p;
		 yyval.p = yyvsp[0].p;
		 p = yyvsp[0].p;
                 while (p) {
		   if (Getattr(p,"type")) {
		     Replace(Getattr(p,"type"),"typename ", "", DOH_REPLACE_ANY);
		   }
		   p = nextSibling(p);
                 }
               }
    break;

  case 232:
#line 4058 "parser.y"
    {
		  if (1) { 
		    set_nextSibling(yyvsp[-1].p,yyvsp[0].p);
		    yyval.p = yyvsp[-1].p;
		  } else {
		    yyval.p = yyvsp[0].p;
		  }
		}
    break;

  case 233:
#line 4066 "parser.y"
    { yyval.p = 0; }
    break;

  case 234:
#line 4069 "parser.y"
    {
                 set_nextSibling(yyvsp[-1].p,yyvsp[0].p);
		 yyval.p = yyvsp[-1].p;
                }
    break;

  case 235:
#line 4073 "parser.y"
    { yyval.p = 0; }
    break;

  case 236:
#line 4077 "parser.y"
    {
		  yyval.p = yyvsp[0].p;
		  {
		    /* We need to make a possible adjustment for integer parameters. */
		    SwigType *type;
		    Node     *n = 0;

		    while (!n) {
		      type = Getattr(yyvsp[0].p,"type");
		      n = Swig_symbol_clookup(type,0);     /* See if we can find a node that matches the typename */
		      if ((n) && (Strcmp(nodeType(n),"cdecl") == 0)) {
			SwigType *decl = Getattr(n,"decl");
			if (!SwigType_isfunction(decl)) {
			  String *value = Getattr(n,"value");
			  if (value) {
			    Setattr(yyvsp[0].p,"type",Copy(value));
			    n = 0;
			  }
			}
		      } else {
			break;
		      }
		    }
		  }
		  
               }
    break;

  case 237:
#line 4103 "parser.y"
    {
                  yyval.p = NewParm(0,0);
                  Setfile(yyval.p,cparse_file);
		  Setline(yyval.p,cparse_line);
		  Setattr(yyval.p,"value",yyvsp[0].dtype.val);
               }
    break;

  case 238:
#line 4109 "parser.y"
    {
                  yyval.p = NewParm(0,0);
                  Setfile(yyval.p,cparse_file);
		  Setline(yyval.p,cparse_line);
		  Setattr(yyval.p,"value",NewString(yyvsp[0].id));
               }
    break;

  case 239:
#line 4117 "parser.y"
    { 
                  yyval.dtype = yyvsp[0].dtype; 
		  if (yyvsp[0].dtype.type == T_ERROR) {
		    Swig_warning(WARN_PARSE_BAD_DEFAULT,cparse_file, cparse_line, "Can't set default argument (ignored)\n");
		    yyval.dtype.val = 0;
		    yyval.dtype.rawval = 0;
		    yyval.dtype.bitfield = 0;
		    yyval.dtype.throws = 0;
		    yyval.dtype.throw = 0;
		  }
               }
    break;

  case 240:
#line 4128 "parser.y"
    {
		 Node *n = Swig_symbol_clookup(yyvsp[0].decl.id,0);
		 if (n) {
		   String *q = Swig_symbol_qualified(n);
                   if (q) {
                     String *temp = NewStringf("%s::%s", q, Getattr(n,"name"));
                     yyval.dtype.val = NewStringf("&%s", SwigType_str(yyvsp[0].decl.type,temp));
                     Delete(q);
                     Delete(temp);
                   } else {
                     yyval.dtype.val = NewStringf("&%s", SwigType_str(yyvsp[0].decl.type,yyvsp[0].decl.id));
                   }
		 } else {
		   yyval.dtype.val = NewStringf("&%s",SwigType_str(yyvsp[0].decl.type,yyvsp[0].decl.id));
		 }
		 yyval.dtype.rawval = 0;
		 yyval.dtype.type = T_USER;
		 yyval.dtype.bitfield = 0;
		 yyval.dtype.throws = 0;
		 yyval.dtype.throw = 0;
	       }
    break;

  case 241:
#line 4149 "parser.y"
    {
		 skip_balanced('{','}');
		 yyval.dtype.val = 0;
		 yyval.dtype.rawval = 0;
                 yyval.dtype.type = T_INT;
		 yyval.dtype.bitfield = 0;
		 yyval.dtype.throws = 0;
		 yyval.dtype.throw = 0;
	       }
    break;

  case 242:
#line 4158 "parser.y"
    { 
		 yyval.dtype.val = 0;
		 yyval.dtype.rawval = 0;
		 yyval.dtype.type = 0;
		 yyval.dtype.bitfield = yyvsp[0].dtype.val;
		 yyval.dtype.throws = 0;
		 yyval.dtype.throw = 0;
	       }
    break;

  case 243:
#line 4166 "parser.y"
    {
                 yyval.dtype.val = 0;
                 yyval.dtype.rawval = 0;
                 yyval.dtype.type = T_INT;
		 yyval.dtype.bitfield = 0;
		 yyval.dtype.throws = 0;
		 yyval.dtype.throw = 0;
               }
    break;

  case 244:
#line 4176 "parser.y"
    {
                 yyval.decl = yyvsp[-1].decl;
		 yyval.decl.defarg = yyvsp[0].dtype.rawval ? yyvsp[0].dtype.rawval : yyvsp[0].dtype.val;
            }
    break;

  case 245:
#line 4180 "parser.y"
    {
              yyval.decl = yyvsp[-1].decl;
	      yyval.decl.defarg = yyvsp[0].dtype.rawval ? yyvsp[0].dtype.rawval : yyvsp[0].dtype.val;
            }
    break;

  case 246:
#line 4184 "parser.y"
    {
   	      yyval.decl.type = 0;
              yyval.decl.id = 0;
	      yyval.decl.defarg = yyvsp[0].dtype.rawval ? yyvsp[0].dtype.rawval : yyvsp[0].dtype.val;
            }
    break;

  case 247:
#line 4191 "parser.y"
    {
                 yyval.decl = yyvsp[0].decl;
		 if (SwigType_isfunction(yyvsp[0].decl.type)) {
		   Delete(SwigType_pop_function(yyvsp[0].decl.type));
		 } else if (SwigType_isarray(yyvsp[0].decl.type)) {
		   SwigType *ta = SwigType_pop_arrays(yyvsp[0].decl.type);
		   if (SwigType_isfunction(yyvsp[0].decl.type)) {
		     Delete(SwigType_pop_function(yyvsp[0].decl.type));
		   } else {
		     yyval.decl.parms = 0;
		   }
		   SwigType_push(yyvsp[0].decl.type,ta);
		   Delete(ta);
		 } else {
		   yyval.decl.parms = 0;
		 }
            }
    break;

  case 248:
#line 4208 "parser.y"
    {
              yyval.decl = yyvsp[0].decl;
	      if (SwigType_isfunction(yyvsp[0].decl.type)) {
		Delete(SwigType_pop_function(yyvsp[0].decl.type));
	      } else if (SwigType_isarray(yyvsp[0].decl.type)) {
		SwigType *ta = SwigType_pop_arrays(yyvsp[0].decl.type);
		if (SwigType_isfunction(yyvsp[0].decl.type)) {
		  Delete(SwigType_pop_function(yyvsp[0].decl.type));
		} else {
		  yyval.decl.parms = 0;
		}
		SwigType_push(yyvsp[0].decl.type,ta);
		Delete(ta);
	      } else {
		yyval.decl.parms = 0;
	      }
            }
    break;

  case 249:
#line 4225 "parser.y"
    {
   	      yyval.decl.type = 0;
              yyval.decl.id = 0;
	      yyval.decl.parms = 0;
	      }
    break;

  case 250:
#line 4233 "parser.y"
    {
              yyval.decl = yyvsp[0].decl;
	      if (yyval.decl.type) {
		SwigType_push(yyvsp[-1].type,yyval.decl.type);
		Delete(yyval.decl.type);
	      }
	      yyval.decl.type = yyvsp[-1].type;
           }
    break;

  case 251:
#line 4241 "parser.y"
    {
              yyval.decl = yyvsp[0].decl;
	      SwigType_add_reference(yyvsp[-2].type);
              if (yyval.decl.type) {
		SwigType_push(yyvsp[-2].type,yyval.decl.type);
		Delete(yyval.decl.type);
	      }
	      yyval.decl.type = yyvsp[-2].type;
           }
    break;

  case 252:
#line 4250 "parser.y"
    {
              yyval.decl = yyvsp[0].decl;
	      if (!yyval.decl.type) yyval.decl.type = NewString("");
           }
    break;

  case 253:
#line 4254 "parser.y"
    { 
	     yyval.decl = yyvsp[0].decl;
	     yyval.decl.type = NewString("");
	     SwigType_add_reference(yyval.decl.type);
	     if (yyvsp[0].decl.type) {
	       SwigType_push(yyval.decl.type,yyvsp[0].decl.type);
	       Delete(yyvsp[0].decl.type);
	     }
           }
    break;

  case 254:
#line 4263 "parser.y"
    { 
	     SwigType *t = NewString("");

	     yyval.decl = yyvsp[0].decl;
	     SwigType_add_memberpointer(t,yyvsp[-2].str);
	     if (yyval.decl.type) {
	       SwigType_push(t,yyval.decl.type);
	       Delete(yyval.decl.type);
	     }
	     yyval.decl.type = t;
	     }
    break;

  case 255:
#line 4274 "parser.y"
    { 
	     SwigType *t = NewString("");
	     yyval.decl = yyvsp[0].decl;
	     SwigType_add_memberpointer(t,yyvsp[-2].str);
	     SwigType_push(yyvsp[-3].type,t);
	     if (yyval.decl.type) {
	       SwigType_push(yyvsp[-3].type,yyval.decl.type);
	       Delete(yyval.decl.type);
	     }
	     yyval.decl.type = yyvsp[-3].type;
	     Delete(t);
	   }
    break;

  case 256:
#line 4286 "parser.y"
    { 
	     yyval.decl = yyvsp[0].decl;
	     SwigType_add_memberpointer(yyvsp[-4].type,yyvsp[-3].str);
	     SwigType_add_reference(yyvsp[-4].type);
	     if (yyval.decl.type) {
	       SwigType_push(yyvsp[-4].type,yyval.decl.type);
	       Delete(yyval.decl.type);
	     }
	     yyval.decl.type = yyvsp[-4].type;
	   }
    break;

  case 257:
#line 4296 "parser.y"
    { 
	     SwigType *t = NewString("");
	     yyval.decl = yyvsp[0].decl;
	     SwigType_add_memberpointer(t,yyvsp[-3].str);
	     SwigType_add_reference(t);
	     if (yyval.decl.type) {
	       SwigType_push(t,yyval.decl.type);
	       Delete(yyval.decl.type);
	     } 
	     yyval.decl.type = t;
	   }
    break;

  case 258:
#line 4309 "parser.y"
    {
                /* Note: This is non-standard C.  Template declarator is allowed to follow an identifier */
                 yyval.decl.id = Char(yyvsp[0].str);
		 yyval.decl.type = 0;
		 yyval.decl.parms = 0;
		 yyval.decl.have_parms = 0;
                  }
    break;

  case 259:
#line 4316 "parser.y"
    {
                  yyval.decl.id = Char(NewStringf("~%s",yyvsp[0].str));
                  yyval.decl.type = 0;
                  yyval.decl.parms = 0;
                  yyval.decl.have_parms = 0;
                  }
    break;

  case 260:
#line 4324 "parser.y"
    {
                  yyval.decl.id = Char(yyvsp[-1].str);
                  yyval.decl.type = 0;
                  yyval.decl.parms = 0;
                  yyval.decl.have_parms = 0;
                  }
    break;

  case 261:
#line 4340 "parser.y"
    {
		    yyval.decl = yyvsp[-1].decl;
		    if (yyval.decl.type) {
		      SwigType_push(yyvsp[-2].type,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = yyvsp[-2].type;
                  }
    break;

  case 262:
#line 4348 "parser.y"
    {
		    SwigType *t;
		    yyval.decl = yyvsp[-1].decl;
		    t = NewString("");
		    SwigType_add_memberpointer(t,yyvsp[-3].str);
		    if (yyval.decl.type) {
		      SwigType_push(t,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = t;
		    }
    break;

  case 263:
#line 4359 "parser.y"
    { 
		    SwigType *t;
		    yyval.decl = yyvsp[-2].decl;
		    t = NewString("");
		    SwigType_add_array(t,(char*)"");
		    if (yyval.decl.type) {
		      SwigType_push(t,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = t;
                  }
    break;

  case 264:
#line 4370 "parser.y"
    { 
		    SwigType *t;
		    yyval.decl = yyvsp[-3].decl;
		    t = NewString("");
		    SwigType_add_array(t,yyvsp[-1].dtype.val);
		    if (yyval.decl.type) {
		      SwigType_push(t,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = t;
                  }
    break;

  case 265:
#line 4381 "parser.y"
    {
		    SwigType *t;
                    yyval.decl = yyvsp[-3].decl;
		    t = NewString("");
		    SwigType_add_function(t,yyvsp[-1].pl);
		    if (!yyval.decl.have_parms) {
		      yyval.decl.parms = yyvsp[-1].pl;
		      yyval.decl.have_parms = 1;
		    }
		    if (!yyval.decl.type) {
		      yyval.decl.type = t;
		    } else {
		      SwigType_push(t, yyval.decl.type);
		      Delete(yyval.decl.type);
		      yyval.decl.type = t;
		    }
		  }
    break;

  case 266:
#line 4400 "parser.y"
    {
                /* Note: This is non-standard C.  Template declarator is allowed to follow an identifier */
                 yyval.decl.id = Char(yyvsp[0].str);
		 yyval.decl.type = 0;
		 yyval.decl.parms = 0;
		 yyval.decl.have_parms = 0;
                  }
    break;

  case 267:
#line 4408 "parser.y"
    {
                  yyval.decl.id = Char(NewStringf("~%s",yyvsp[0].str));
                  yyval.decl.type = 0;
                  yyval.decl.parms = 0;
                  yyval.decl.have_parms = 0;
                  }
    break;

  case 268:
#line 4425 "parser.y"
    {
		    yyval.decl = yyvsp[-1].decl;
		    if (yyval.decl.type) {
		      SwigType_push(yyvsp[-2].type,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = yyvsp[-2].type;
                  }
    break;

  case 269:
#line 4433 "parser.y"
    {
                    yyval.decl = yyvsp[-1].decl;
		    if (!yyval.decl.type) {
		      yyval.decl.type = NewString("");
		    }
		    SwigType_add_reference(yyval.decl.type);
                  }
    break;

  case 270:
#line 4440 "parser.y"
    {
		    SwigType *t;
		    yyval.decl = yyvsp[-1].decl;
		    t = NewString("");
		    SwigType_add_memberpointer(t,yyvsp[-3].str);
		    if (yyval.decl.type) {
		      SwigType_push(t,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = t;
		    }
    break;

  case 271:
#line 4451 "parser.y"
    { 
		    SwigType *t;
		    yyval.decl = yyvsp[-2].decl;
		    t = NewString("");
		    SwigType_add_array(t,(char*)"");
		    if (yyval.decl.type) {
		      SwigType_push(t,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = t;
                  }
    break;

  case 272:
#line 4462 "parser.y"
    { 
		    SwigType *t;
		    yyval.decl = yyvsp[-3].decl;
		    t = NewString("");
		    SwigType_add_array(t,yyvsp[-1].dtype.val);
		    if (yyval.decl.type) {
		      SwigType_push(t,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = t;
                  }
    break;

  case 273:
#line 4473 "parser.y"
    {
		    SwigType *t;
                    yyval.decl = yyvsp[-3].decl;
		    t = NewString("");
		    SwigType_add_function(t,yyvsp[-1].pl);
		    if (!yyval.decl.have_parms) {
		      yyval.decl.parms = yyvsp[-1].pl;
		      yyval.decl.have_parms = 1;
		    }
		    if (!yyval.decl.type) {
		      yyval.decl.type = t;
		    } else {
		      SwigType_push(t, yyval.decl.type);
		      Delete(yyval.decl.type);
		      yyval.decl.type = t;
		    }
		  }
    break;

  case 274:
#line 4492 "parser.y"
    {
		    yyval.decl.type = yyvsp[0].type;
                    yyval.decl.id = 0;
		    yyval.decl.parms = 0;
		    yyval.decl.have_parms = 0;
                  }
    break;

  case 275:
#line 4498 "parser.y"
    { 
                     yyval.decl = yyvsp[0].decl;
                     SwigType_push(yyvsp[-1].type,yyvsp[0].decl.type);
		     yyval.decl.type = yyvsp[-1].type;
		     Delete(yyvsp[0].decl.type);
                  }
    break;

  case 276:
#line 4504 "parser.y"
    {
		    yyval.decl.type = yyvsp[-1].type;
		    SwigType_add_reference(yyval.decl.type);
		    yyval.decl.id = 0;
		    yyval.decl.parms = 0;
		    yyval.decl.have_parms = 0;
		  }
    break;

  case 277:
#line 4511 "parser.y"
    {
		    yyval.decl = yyvsp[0].decl;
		    SwigType_add_reference(yyvsp[-2].type);
		    if (yyval.decl.type) {
		      SwigType_push(yyvsp[-2].type,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = yyvsp[-2].type;
                  }
    break;

  case 278:
#line 4520 "parser.y"
    {
		    yyval.decl = yyvsp[0].decl;
                  }
    break;

  case 279:
#line 4523 "parser.y"
    {
		    yyval.decl = yyvsp[0].decl;
		    yyval.decl.type = NewString("");
		    SwigType_add_reference(yyval.decl.type);
		    if (yyvsp[0].decl.type) {
		      SwigType_push(yyval.decl.type,yyvsp[0].decl.type);
		      Delete(yyvsp[0].decl.type);
		    }
                  }
    break;

  case 280:
#line 4532 "parser.y"
    { 
                    yyval.decl.id = 0;
                    yyval.decl.parms = 0;
		    yyval.decl.have_parms = 0;
                    yyval.decl.type = NewString("");
		    SwigType_add_reference(yyval.decl.type);
                  }
    break;

  case 281:
#line 4539 "parser.y"
    { 
		    yyval.decl.type = NewString("");
                    SwigType_add_memberpointer(yyval.decl.type,yyvsp[-1].str);
                    yyval.decl.id = 0;
                    yyval.decl.parms = 0;
		    yyval.decl.have_parms = 0;
      	          }
    break;

  case 282:
#line 4546 "parser.y"
    { 
		    SwigType *t = NewString("");
                    yyval.decl.type = yyvsp[-2].type;
		    yyval.decl.id = 0;
		    yyval.decl.parms = 0;
		    yyval.decl.have_parms = 0;
		    SwigType_add_memberpointer(t,yyvsp[-1].str);
		    SwigType_push(yyval.decl.type,t);
		    Delete(t);
                  }
    break;

  case 283:
#line 4556 "parser.y"
    { 
		    yyval.decl = yyvsp[0].decl;
		    SwigType_add_memberpointer(yyvsp[-3].type,yyvsp[-2].str);
		    if (yyval.decl.type) {
		      SwigType_push(yyvsp[-3].type,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = yyvsp[-3].type;
                  }
    break;

  case 284:
#line 4567 "parser.y"
    { 
		    SwigType *t;
		    yyval.decl = yyvsp[-2].decl;
		    t = NewString("");
		    SwigType_add_array(t,(char*)"");
		    if (yyval.decl.type) {
		      SwigType_push(t,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = t;
                  }
    break;

  case 285:
#line 4578 "parser.y"
    { 
		    SwigType *t;
		    yyval.decl = yyvsp[-3].decl;
		    t = NewString("");
		    SwigType_add_array(t,yyvsp[-1].dtype.val);
		    if (yyval.decl.type) {
		      SwigType_push(t,yyval.decl.type);
		      Delete(yyval.decl.type);
		    }
		    yyval.decl.type = t;
                  }
    break;

  case 286:
#line 4589 "parser.y"
    { 
		    yyval.decl.type = NewString("");
		    yyval.decl.id = 0;
		    yyval.decl.parms = 0;
		    yyval.decl.have_parms = 0;
		    SwigType_add_array(yyval.decl.type,(char*)"");
                  }
    break;

  case 287:
#line 4596 "parser.y"
    { 
		    yyval.decl.type = NewString("");
		    yyval.decl.id = 0;
		    yyval.decl.parms = 0;
		    yyval.decl.have_parms = 0;
		    SwigType_add_array(yyval.decl.type,yyvsp[-1].dtype.val);
		  }
    break;

  case 288:
#line 4603 "parser.y"
    {
                    yyval.decl = yyvsp[-1].decl;
		  }
    break;

  case 289:
#line 4606 "parser.y"
    {
		    SwigType *t;
                    yyval.decl = yyvsp[-3].decl;
		    t = NewString("");
                    SwigType_add_function(t,yyvsp[-1].pl);
		    if (!yyval.decl.type) {
		      yyval.decl.type = t;
		    } else {
		      SwigType_push(t,yyval.decl.type);
		      Delete(yyval.decl.type);
		      yyval.decl.type = t;
		    }
		    if (!yyval.decl.have_parms) {
		      yyval.decl.parms = yyvsp[-1].pl;
		      yyval.decl.have_parms = 1;
		    }
		  }
    break;

  case 290:
#line 4623 "parser.y"
    {
                    yyval.decl.type = NewString("");
                    SwigType_add_function(yyval.decl.type,yyvsp[-1].pl);
		    yyval.decl.parms = yyvsp[-1].pl;
		    yyval.decl.have_parms = 1;
		    yyval.decl.id = 0;
                  }
    break;

  case 291:
#line 4633 "parser.y"
    { 
               yyval.type = NewString("");
               SwigType_add_pointer(yyval.type);
	       SwigType_push(yyval.type,yyvsp[-1].str);
	       SwigType_push(yyval.type,yyvsp[0].type);
	       Delete(yyvsp[0].type);
           }
    break;

  case 292:
#line 4640 "parser.y"
    {
	     yyval.type = NewString("");
	     SwigType_add_pointer(yyval.type);
	     SwigType_push(yyval.type,yyvsp[0].type);
	     Delete(yyvsp[0].type);
	     }
    break;

  case 293:
#line 4646 "parser.y"
    { 
	     	yyval.type = NewString("");	
		SwigType_add_pointer(yyval.type);
	        SwigType_push(yyval.type,yyvsp[0].str);
           }
    break;

  case 294:
#line 4651 "parser.y"
    {
	      yyval.type = NewString("");
	      SwigType_add_pointer(yyval.type);
           }
    break;

  case 295:
#line 4657 "parser.y"
    { 
	          yyval.str = NewString("");
	          if (yyvsp[0].id) SwigType_add_qualifier(yyval.str,yyvsp[0].id);
               }
    break;

  case 296:
#line 4661 "parser.y"
    { 
		  yyval.str = yyvsp[0].str; 
	          if (yyvsp[-1].id) SwigType_add_qualifier(yyval.str,yyvsp[-1].id);
               }
    break;

  case 297:
#line 4667 "parser.y"
    { yyval.id = "const"; }
    break;

  case 298:
#line 4668 "parser.y"
    { yyval.id = "volatile"; }
    break;

  case 299:
#line 4669 "parser.y"
    { yyval.id = 0; }
    break;

  case 300:
#line 4675 "parser.y"
    {
                   yyval.type = yyvsp[0].type;
                   Replace(yyval.type,"typename ","", DOH_REPLACE_ANY);
                }
    break;

  case 301:
#line 4681 "parser.y"
    {
                   yyval.type = yyvsp[0].type;
	           SwigType_push(yyval.type,yyvsp[-1].str);
               }
    break;

  case 302:
#line 4685 "parser.y"
    { yyval.type = yyvsp[0].type; }
    break;

  case 303:
#line 4688 "parser.y"
    { yyval.type = yyvsp[0].type;
                  /* Printf(stdout,"primitive = '%s'\n", $$);*/
                }
    break;

  case 304:
#line 4691 "parser.y"
    { yyval.type = yyvsp[0].type; }
    break;

  case 305:
#line 4692 "parser.y"
    { yyval.type = yyvsp[0].type; }
    break;

  case 306:
#line 4693 "parser.y"
    { yyval.type = NewStringf("%s%s",yyvsp[-1].type,yyvsp[0].id); }
    break;

  case 307:
#line 4694 "parser.y"
    { yyval.type = NewStringf("enum %s", yyvsp[0].str); }
    break;

  case 308:
#line 4695 "parser.y"
    { yyval.type = yyvsp[0].type; }
    break;

  case 309:
#line 4696 "parser.y"
    {
		  yyval.type = yyvsp[-1].type;
	          SwigType_push(yyval.type,yyvsp[0].str);
     	       }
    break;

  case 310:
#line 4701 "parser.y"
    {
		  yyval.type = yyvsp[0].str;
               }
    break;

  case 311:
#line 4704 "parser.y"
    { 
		 yyval.type = NewStringf("%s %s", yyvsp[-1].id, yyvsp[0].str);
               }
    break;

  case 312:
#line 4709 "parser.y"
    {
		 if (!yyvsp[0].ptype.type) yyvsp[0].ptype.type = NewString("int");
		 if (yyvsp[0].ptype.us) {
		   yyval.type = NewStringf("%s %s", yyvsp[0].ptype.us, yyvsp[0].ptype.type);
		   Delete(yyvsp[0].ptype.us);
                   Delete(yyvsp[0].ptype.type);
		 } else {
                   yyval.type = yyvsp[0].ptype.type;
		 }
		 if (Cmp(yyval.type,"signed int") == 0) {
		   Delete(yyval.type);
		   yyval.type = NewString("int");
                 } else if (Cmp(yyval.type,"signed long") == 0) {
		   Delete(yyval.type);
                   yyval.type = NewString("long");
                 } else if (Cmp(yyval.type,"signed short") == 0) {
		   Delete(yyval.type);
		   yyval.type = NewString("short");
		 } else if (Cmp(yyval.type,"signed long long") == 0) {
		   Delete(yyval.type);
		   yyval.type = NewString("long long");
		 }
               }
    break;

  case 313:
#line 4734 "parser.y"
    { 
                 yyval.ptype = yyvsp[0].ptype;
               }
    break;

  case 314:
#line 4737 "parser.y"
    {
                    if (yyvsp[-1].ptype.us && yyvsp[0].ptype.us) {
		      Swig_error(cparse_file, cparse_line, "Extra %s specifier.\n", yyvsp[0].ptype.us);
		    }
                    yyval.ptype = yyvsp[0].ptype;
                    if (yyvsp[-1].ptype.us) yyval.ptype.us = yyvsp[-1].ptype.us;
		    if (yyvsp[-1].ptype.type) {
		      if (!yyvsp[0].ptype.type) yyval.ptype.type = yyvsp[-1].ptype.type;
		      else {
			int err = 0;
			if ((Cmp(yyvsp[-1].ptype.type,"long") == 0)) {
			  if ((Cmp(yyvsp[0].ptype.type,"long") == 0) || (Strncmp(yyvsp[0].ptype.type,"double",6) == 0)) {
			    yyval.ptype.type = NewStringf("long %s", yyvsp[0].ptype.type);
			  } else if (Cmp(yyvsp[0].ptype.type,"int") == 0) {
			    yyval.ptype.type = yyvsp[-1].ptype.type;
			  } else {
			    err = 1;
			  }
			} else if ((Cmp(yyvsp[-1].ptype.type,"short")) == 0) {
			  if (Cmp(yyvsp[0].ptype.type,"int") == 0) {
			    yyval.ptype.type = yyvsp[-1].ptype.type;
			  } else {
			    err = 1;
			  }
			} else if (Cmp(yyvsp[-1].ptype.type,"int") == 0) {
			  yyval.ptype.type = yyvsp[0].ptype.type;
			} else if (Cmp(yyvsp[-1].ptype.type,"double") == 0) {
			  if (Cmp(yyvsp[0].ptype.type,"long") == 0) {
			    yyval.ptype.type = NewString("long double");
			  } else if (Cmp(yyvsp[0].ptype.type,"complex") == 0) {
			    yyval.ptype.type = NewString("double complex");
			  } else {
			    err = 1;
			  }
			} else if (Cmp(yyvsp[-1].ptype.type,"float") == 0) {
			  if (Cmp(yyvsp[0].ptype.type,"complex") == 0) {
			    yyval.ptype.type = NewString("float complex");
			  } else {
			    err = 1;
			  }
			} else if (Cmp(yyvsp[-1].ptype.type,"complex") == 0) {
			  yyval.ptype.type = NewStringf("%s complex", yyvsp[0].ptype.type);
			} else {
			  err = 1;
			}
			if (err) {
			  Swig_error(cparse_file, cparse_line, "Extra %s specifier.\n", yyvsp[-1].ptype.type);
			}
		      }
		    }
               }
    break;

  case 315:
#line 4791 "parser.y"
    { 
		    yyval.ptype.type = NewString("int");
                    yyval.ptype.us = 0;
               }
    break;

  case 316:
#line 4795 "parser.y"
    { 
                    yyval.ptype.type = NewString("short");
                    yyval.ptype.us = 0;
                }
    break;

  case 317:
#line 4799 "parser.y"
    { 
                    yyval.ptype.type = NewString("long");
                    yyval.ptype.us = 0;
                }
    break;

  case 318:
#line 4803 "parser.y"
    { 
                    yyval.ptype.type = NewString("char");
                    yyval.ptype.us = 0;
                }
    break;

  case 319:
#line 4807 "parser.y"
    { 
                    yyval.ptype.type = NewString("float");
                    yyval.ptype.us = 0;
                }
    break;

  case 320:
#line 4811 "parser.y"
    { 
                    yyval.ptype.type = NewString("double");
                    yyval.ptype.us = 0;
                }
    break;

  case 321:
#line 4815 "parser.y"
    { 
                    yyval.ptype.us = NewString("signed");
                    yyval.ptype.type = 0;
                }
    break;

  case 322:
#line 4819 "parser.y"
    { 
                    yyval.ptype.us = NewString("unsigned");
                    yyval.ptype.type = 0;
                }
    break;

  case 323:
#line 4823 "parser.y"
    { 
                    yyval.ptype.type = NewString("complex");
                    yyval.ptype.us = 0;
                }
    break;

  case 324:
#line 4829 "parser.y"
    { /* scanner_check_typedef(); */ }
    break;

  case 325:
#line 4829 "parser.y"
    {
                   yyval.dtype = yyvsp[0].dtype;
		   if (yyval.dtype.type == T_STRING) {
		     yyval.dtype.rawval = NewStringf("\"%(escape)s\"",yyval.dtype.val);
		   } else {
		     yyval.dtype.rawval = 0;
		   }
		   yyval.dtype.bitfield = 0;
		   yyval.dtype.throws = 0;
		   yyval.dtype.throw = 0;
		   scanner_ignore_typedef();
                }
    break;

  case 326:
#line 4850 "parser.y"
    {
                   yyval.dtype.val = NewString(yyvsp[0].str);
		   /*		   $$.rawval = NewStringf("\'%(escape)s\'",$$.val); */
		   /*		   Printf(stdout,"rawval = '%s'\n", $$.rawval); */
		   if (Len(yyval.dtype.val)) {
		     yyval.dtype.rawval = NewStringf("\'%(escape)s\'", yyval.dtype.val);
		   } else {
		     yyval.dtype.rawval = NewString("\'\\0'");
		   }
		   yyval.dtype.type = T_CHAR;
		   yyval.dtype.bitfield = 0;
		   yyval.dtype.throws = 0;
		   yyval.dtype.throw = 0;
		 }
    break;

  case 327:
#line 4868 "parser.y"
    { yyval.id = yyvsp[0].id; }
    break;

  case 328:
#line 4869 "parser.y"
    { yyval.id = (char *) 0;}
    break;

  case 329:
#line 4872 "parser.y"
    { 

                  /* Ignore if there is a trailing comma in the enum list */
                  if (yyvsp[0].node) {
                    Node *leftSibling = Getattr(yyvsp[-2].node,"_last");
                    if (!leftSibling) {
                      leftSibling=yyvsp[-2].node;
                    }
                    set_nextSibling(leftSibling,yyvsp[0].node);
                    Setattr(yyvsp[-2].node,"_last",yyvsp[0].node);
                  }
		  yyval.node = yyvsp[-2].node;
               }
    break;

  case 330:
#line 4885 "parser.y"
    { 
                   yyval.node = yyvsp[0].node; 
                   if (yyvsp[0].node) {
                     Setattr(yyvsp[0].node,"_last",yyvsp[0].node);
                   }
               }
    break;

  case 331:
#line 4893 "parser.y"
    {
		   yyval.node = new_node("enumitem");
		   Setattr(yyval.node,"name",yyvsp[0].id);
		   Setattr(yyval.node,"type",NewSwigType(T_INT));
		   Setattr(yyval.node,"feature:immutable","1");
		 }
    break;

  case 332:
#line 4899 "parser.y"
    {
		   yyval.node = new_node("enumitem");
		   Setattr(yyval.node,"name",yyvsp[-2].id);
		   Setattr(yyval.node,"enumvalue", yyvsp[0].dtype.val);
	           if (yyvsp[0].dtype.type == T_CHAR) {
		     Setattr(yyval.node,"value",yyvsp[0].dtype.val);
		     Setattr(yyval.node,"type",NewSwigType(T_CHAR));
		   } else {
		     Setattr(yyval.node,"value",yyvsp[-2].id);
		     Setattr(yyval.node,"type",NewSwigType(T_INT));
		   }
		   Setattr(yyval.node,"feature:immutable","1");
                 }
    break;

  case 333:
#line 4912 "parser.y"
    { yyval.node = 0; }
    break;

  case 334:
#line 4915 "parser.y"
    {
                   yyval.dtype = yyvsp[0].dtype;
		   if ((yyval.dtype.type != T_INT) && (yyval.dtype.type != T_UINT) &&
		       (yyval.dtype.type != T_LONG) && (yyval.dtype.type != T_ULONG) &&
		       (yyval.dtype.type != T_SHORT) && (yyval.dtype.type != T_USHORT) &&
		       (yyval.dtype.type != T_SCHAR) && (yyval.dtype.type != T_UCHAR)) {
		     Swig_error(cparse_file,cparse_line,"Type error. Expecting an int\n");
		   }
                }
    break;

  case 335:
#line 4924 "parser.y"
    {
                   yyval.dtype.val  = NewString(yyvsp[0].str);
		   yyval.dtype.type = T_INT;
		 }
    break;

  case 336:
#line 4935 "parser.y"
    { yyval.dtype = yyvsp[0].dtype; }
    break;

  case 337:
#line 4936 "parser.y"
    { 
		    yyval.dtype.val = NewString(yyvsp[0].id); 
                    yyval.dtype.type = T_STRING; 
               }
    break;

  case 338:
#line 4940 "parser.y"
    {
  		  SwigType_push(yyvsp[-2].type,yyvsp[-1].decl.type);
		  yyval.dtype.val = NewStringf("sizeof(%s)",SwigType_str(yyvsp[-2].type,0));
		  yyval.dtype.type = T_INT;
               }
    break;

  case 339:
#line 4945 "parser.y"
    { yyval.dtype = yyvsp[0].dtype; }
    break;

  case 340:
#line 4946 "parser.y"
    {
		 Node *n;
		 yyval.dtype.val = yyvsp[0].type;
		 yyval.dtype.type = T_INT;
		 /* Check if value is in scope */
		 n = Swig_symbol_clookup(yyvsp[0].type,0);
		 if (n) {
                   /* A band-aid for enum values used in expressions. */
                   if (Strcmp(nodeType(n),"enumitem") == 0) {
                     String *q = Swig_symbol_qualified(n);
                     if (q) {
                       yyval.dtype.val = NewStringf("%s::%s", q, Getattr(n,"name"));
                       Delete(q);
                     }
                   }
		 }
               }
    break;

  case 341:
#line 4965 "parser.y"
    {
   	            yyval.dtype.val = NewStringf("(%s)",yyvsp[-1].dtype.val);
		    yyval.dtype.type = yyvsp[-1].dtype.type;
   	       }
    break;

  case 342:
#line 4972 "parser.y"
    {
                 yyval.dtype = yyvsp[0].dtype;
		 if (yyvsp[0].dtype.type != T_STRING) {
		   yyval.dtype.val = NewStringf("(%s) %s", SwigType_str(yyvsp[-2].dtype.val,0), yyvsp[0].dtype.val);
		 }
 	       }
    break;

  case 343:
#line 4978 "parser.y"
    {
                 yyval.dtype = yyvsp[0].dtype;
		 if (yyvsp[0].dtype.type != T_STRING) {
		   SwigType_push(yyvsp[-3].dtype.val,yyvsp[-2].type);
		   yyval.dtype.val = NewStringf("(%s) %s", SwigType_str(yyvsp[-3].dtype.val,0), yyvsp[0].dtype.val);
		 }
 	       }
    break;

  case 344:
#line 4985 "parser.y"
    {
                 yyval.dtype = yyvsp[0].dtype;
		 if (yyvsp[0].dtype.type != T_STRING) {
		   SwigType_add_reference(yyvsp[-3].dtype.val);
		   yyval.dtype.val = NewStringf("(%s) %s", SwigType_str(yyvsp[-3].dtype.val,0), yyvsp[0].dtype.val);
		 }
 	       }
    break;

  case 345:
#line 4992 "parser.y"
    {
                 yyval.dtype = yyvsp[0].dtype;
		 if (yyvsp[0].dtype.type != T_STRING) {
		   SwigType_push(yyvsp[-4].dtype.val,yyvsp[-3].type);
		   SwigType_add_reference(yyvsp[-4].dtype.val);
		   yyval.dtype.val = NewStringf("(%s) %s", SwigType_str(yyvsp[-4].dtype.val,0), yyvsp[0].dtype.val);
		 }
 	       }
    break;

  case 346:
#line 5002 "parser.y"
    { yyval.dtype = yyvsp[0].dtype; }
    break;

  case 347:
#line 5003 "parser.y"
    { yyval.dtype = yyvsp[0].dtype; }
    break;

  case 348:
#line 5004 "parser.y"
    { yyval.dtype = yyvsp[0].dtype; }
    break;

  case 349:
#line 5005 "parser.y"
    { yyval.dtype = yyvsp[0].dtype; }
    break;

  case 350:
#line 5006 "parser.y"
    { yyval.dtype = yyvsp[0].dtype; }
    break;

  case 351:
#line 5007 "parser.y"
    { yyval.dtype = yyvsp[0].dtype; }
    break;

  case 352:
#line 5008 "parser.y"
    { yyval.dtype = yyvsp[0].dtype; }
    break;

  case 353:
#line 5011 "parser.y"
    {
		 yyval.dtype.val = NewStringf("%s+%s",yyvsp[-2].dtype.val,yyvsp[0].dtype.val);
		 yyval.dtype.type = promote(yyvsp[-2].dtype.type,yyvsp[0].dtype.type);
	       }
    break;

  case 354:
#line 5015 "parser.y"
    {
		 yyval.dtype.val = NewStringf("%s-%s",yyvsp[-2].dtype.val,yyvsp[0].dtype.val);
		 yyval.dtype.type = promote(yyvsp[-2].dtype.type,yyvsp[0].dtype.type);
	       }
    break;

  case 355:
#line 5019 "parser.y"
    {
		 yyval.dtype.val = NewStringf("%s*%s",yyvsp[-2].dtype.val,yyvsp[0].dtype.val);
		 yyval.dtype.type = promote(yyvsp[-2].dtype.type,yyvsp[0].dtype.type);
	       }
    break;

  case 356:
#line 5023 "parser.y"
    {
		 yyval.dtype.val = NewStringf("%s/%s",yyvsp[-2].dtype.val,yyvsp[0].dtype.val);
		 yyval.dtype.type = promote(yyvsp[-2].dtype.type,yyvsp[0].dtype.type);
	       }
    break;

  case 357:
#line 5027 "parser.y"
    {
		 yyval.dtype.val = NewStringf("%s&%s",yyvsp[-2].dtype.val,yyvsp[0].dtype.val);
		 yyval.dtype.type = promote(yyvsp[-2].dtype.type,yyvsp[0].dtype.type);
	       }
    break;

  case 358:
#line 5031 "parser.y"
    {
		 yyval.dtype.val = NewStringf("%s|%s",yyvsp[-2].dtype.val,yyvsp[0].dtype.val);
		 yyval.dtype.type = promote(yyvsp[-2].dtype.type,yyvsp[0].dtype.type);
	       }
    break;

  case 359:
#line 5035 "parser.y"
    {
		 yyval.dtype.val = NewStringf("%s^%s",yyvsp[-2].dtype.val,yyvsp[0].dtype.val);
		 yyval.dtype.type = promote(yyvsp[-2].dtype.type,yyvsp[0].dtype.type);
	       }
    break;

  case 360:
#line 5039 "parser.y"
    {
		 yyval.dtype.val = NewStringf("%s<<%s",yyvsp[-2].dtype.val,yyvsp[0].dtype.val);
		 yyval.dtype.type = promote(yyvsp[-2].dtype.type,yyvsp[0].dtype.type);
	       }
    break;

  case 361:
#line 5043 "parser.y"
    {
		 yyval.dtype.val = NewStringf("%s>>%s",yyvsp[-2].dtype.val,yyvsp[0].dtype.val);
		 yyval.dtype.type = promote(yyvsp[-2].dtype.type,yyvsp[0].dtype.type);
	       }
    break;

  case 362:
#line 5047 "parser.y"
    {
		 yyval.dtype.val = NewStringf("%s&&%s",yyvsp[-2].dtype.val,yyvsp[0].dtype.val);
		 yyval.dtype.type = T_INT;
	       }
    break;

  case 363:
#line 5051 "parser.y"
    {
		 yyval.dtype.val = NewStringf("%s||%s",yyvsp[-2].dtype.val,yyvsp[0].dtype.val);
		 yyval.dtype.type = T_INT;
	       }
    break;

  case 364:
#line 5055 "parser.y"
    {
		 yyval.dtype.val = NewStringf("-%s",yyvsp[0].dtype.val);
		 yyval.dtype.type = yyvsp[0].dtype.type;
	       }
    break;

  case 365:
#line 5059 "parser.y"
    {
                 yyval.dtype.val = NewStringf("+%s",yyvsp[0].dtype.val);
		 yyval.dtype.type = yyvsp[0].dtype.type;
  	       }
    break;

  case 366:
#line 5063 "parser.y"
    {
		 yyval.dtype.val = NewStringf("~%s",yyvsp[0].dtype.val);
		 yyval.dtype.type = yyvsp[0].dtype.type;
	       }
    break;

  case 367:
#line 5067 "parser.y"
    {
                 yyval.dtype.val = NewStringf("!%s",yyvsp[0].dtype.val);
		 yyval.dtype.type = T_INT;
	       }
    break;

  case 368:
#line 5071 "parser.y"
    {
                 skip_balanced('(',')');
		 yyvsp[-1].type = Swig_symbol_type_qualify(yyvsp[-1].type,0);
		 if (SwigType_istemplate(yyvsp[-1].type)) {
		   yyvsp[-1].type = SwigType_namestr(yyvsp[-1].type);
		 }
		 yyval.dtype.val = NewStringf("%s%s",yyvsp[-1].type,scanner_ccode);
		 Clear(scanner_ccode);
		 yyval.dtype.type = T_INT;
               }
    break;

  case 369:
#line 5083 "parser.y"
    {
		 yyval.bases = yyvsp[0].bases;
               }
    break;

  case 370:
#line 5088 "parser.y"
    { inherit_list = 1; }
    break;

  case 371:
#line 5088 "parser.y"
    { yyval.bases = yyvsp[0].bases; inherit_list = 0; }
    break;

  case 372:
#line 5089 "parser.y"
    { yyval.bases = 0; }
    break;

  case 373:
#line 5092 "parser.y"
    {
		   Hash *list = NewHash();
		   Node *base = yyvsp[0].node;
		   Node *name = Getattr(base,"name");
		   Setattr(list,"public",NewList());
		   Setattr(list,"protected",NewList());
		   Setattr(list,"private",NewList());
		   Append(Getattr(list,Getattr(base,"access")),name);
	           yyval.bases = list;
               }
    break;

  case 374:
#line 5103 "parser.y"
    {
		   Hash *list = yyvsp[-2].bases;
		   Node *base = yyvsp[0].node;
		   Node *name = Getattr(base,"name");
		   Append(Getattr(list,Getattr(base,"access")),name);
                   yyval.bases = list;
               }
    break;

  case 375:
#line 5112 "parser.y"
    {
		 yyval.node = NewHash();
		 Setfile(yyval.node,cparse_file);
		 Setline(yyval.node,cparse_line);
		 Setattr(yyval.node,"name",yyvsp[0].str);
                 if (last_cpptype && (Strcmp(last_cpptype,"struct") != 0)) {
		   Setattr(yyval.node,"access","private");
		   Swig_warning(WARN_PARSE_NO_ACCESS,cparse_file,cparse_line,
				"No access specifier given for base class %s (ignored).\n",yyvsp[0].str);
                 } else {
		   Setattr(yyval.node,"access","public");
		 }
               }
    break;

  case 376:
#line 5125 "parser.y"
    {
		 yyval.node = NewHash();
		 Setfile(yyval.node,cparse_file);
		 Setline(yyval.node,cparse_line);
		 Setattr(yyval.node,"name",yyvsp[0].str);
		 Setattr(yyval.node,"access",yyvsp[-2].id);
	         if (Strcmp(yyvsp[-2].id,"public") != 0) {
		   Swig_warning(WARN_PARSE_PRIVATE_INHERIT, cparse_file, 
				cparse_line,"%s inheritance ignored.\n", yyvsp[-2].id);
		 }
               }
    break;

  case 377:
#line 5138 "parser.y"
    { yyval.id = (char*)"public"; }
    break;

  case 378:
#line 5139 "parser.y"
    { yyval.id = (char*)"private"; }
    break;

  case 379:
#line 5140 "parser.y"
    { yyval.id = (char*)"protected"; }
    break;

  case 380:
#line 5144 "parser.y"
    { 
                   yyval.id = (char*)"class"; 
		   if (!inherit_list) last_cpptype = yyval.id;
               }
    break;

  case 381:
#line 5148 "parser.y"
    { 
                   yyval.id = (char*)"struct"; 
		   if (!inherit_list) last_cpptype = yyval.id;
               }
    break;

  case 382:
#line 5152 "parser.y"
    {
                   yyval.id = (char*)"union"; 
		   if (!inherit_list) last_cpptype = yyval.id;
               }
    break;

  case 383:
#line 5156 "parser.y"
    { 
                   yyval.id = (char *)"typename"; 
		   if (!inherit_list) last_cpptype = yyval.id;
               }
    break;

  case 386:
#line 5166 "parser.y"
    {
                    yyval.dtype.qualifier = yyvsp[0].str;
                    yyval.dtype.throws = 0;
                    yyval.dtype.throw = 0;
               }
    break;

  case 387:
#line 5171 "parser.y"
    {
                    yyval.dtype.qualifier = 0;
                    yyval.dtype.throws = yyvsp[-1].pl;
                    yyval.dtype.throw = NewString("1");
               }
    break;

  case 388:
#line 5176 "parser.y"
    {
                    yyval.dtype.qualifier = yyvsp[-4].str;
                    yyval.dtype.throws = yyvsp[-1].pl;
                    yyval.dtype.throw = NewString("1");
               }
    break;

  case 389:
#line 5181 "parser.y"
    { 
                    yyval.dtype.qualifier = 0; 
                    yyval.dtype.throws = 0;
                    yyval.dtype.throw = 0;
               }
    break;

  case 390:
#line 5188 "parser.y"
    { 
                    Clear(scanner_ccode); 
                    yyval.decl.have_parms = 0; 
                    yyval.decl.defarg = 0; 
		    yyval.decl.throws = yyvsp[-2].dtype.throws;
		    yyval.decl.throw = yyvsp[-2].dtype.throw;
               }
    break;

  case 391:
#line 5195 "parser.y"
    { 
                    skip_balanced('{','}'); 
                    yyval.decl.have_parms = 0; 
                    yyval.decl.defarg = 0; 
                    yyval.decl.throws = yyvsp[-2].dtype.throws;
                    yyval.decl.throw = yyvsp[-2].dtype.throw;
               }
    break;

  case 392:
#line 5202 "parser.y"
    { 
                    Clear(scanner_ccode); 
                    yyval.decl.parms = yyvsp[-2].pl; 
                    yyval.decl.have_parms = 1; 
                    yyval.decl.defarg = 0; 
		    yyval.decl.throws = 0;
		    yyval.decl.throw = 0;
               }
    break;

  case 393:
#line 5210 "parser.y"
    {
                    skip_balanced('{','}'); 
                    yyval.decl.parms = yyvsp[-2].pl; 
                    yyval.decl.have_parms = 1; 
                    yyval.decl.defarg = 0; 
                    yyval.decl.throws = 0;
                    yyval.decl.throw = 0;
               }
    break;

  case 394:
#line 5218 "parser.y"
    { 
                    yyval.decl.have_parms = 0; 
                    yyval.decl.defarg = yyvsp[-1].dtype.val; 
                    yyval.decl.throws = 0;
                    yyval.decl.throw = 0;
               }
    break;

  case 399:
#line 5234 "parser.y"
    {
	            skip_balanced('(',')');
                    Clear(scanner_ccode);
            	}
    break;

  case 400:
#line 5240 "parser.y"
    { 
                     String *s = NewString("");
                     SwigType_add_template(s,yyvsp[-1].p);
                     yyval.id = Char(s);
		     scanner_last_id(1);
                 }
    break;

  case 401:
#line 5246 "parser.y"
    { yyval.id = (char*)"";  }
    break;

  case 402:
#line 5249 "parser.y"
    { yyval.id = yyvsp[0].id; }
    break;

  case 403:
#line 5250 "parser.y"
    { yyval.id = yyvsp[0].id; }
    break;

  case 404:
#line 5253 "parser.y"
    { yyval.id = yyvsp[0].id; }
    break;

  case 405:
#line 5254 "parser.y"
    { yyval.id = 0; }
    break;

  case 406:
#line 5257 "parser.y"
    { 
                  yyval.str = 0;
		  if (!yyval.str) yyval.str = NewStringf("%s%s", yyvsp[-1].str,yyvsp[0].str);
      	          Delete(yyvsp[0].str);
               }
    break;

  case 407:
#line 5262 "parser.y"
    { 
		 yyval.str = NewStringf("::%s%s",yyvsp[-1].str,yyvsp[0].str);
                 Delete(yyvsp[0].str);
               }
    break;

  case 408:
#line 5266 "parser.y"
    {
		 yyval.str = NewString(yyvsp[0].str);
   	       }
    break;

  case 409:
#line 5269 "parser.y"
    {
		 yyval.str = NewStringf("::%s",yyvsp[0].str);
               }
    break;

  case 410:
#line 5272 "parser.y"
    {
                 yyval.str = NewString(yyvsp[0].str);
	       }
    break;

  case 411:
#line 5275 "parser.y"
    {
                 yyval.str = NewStringf("::%s",yyvsp[0].str);
               }
    break;

  case 412:
#line 5280 "parser.y"
    {
                   yyval.str = NewStringf("::%s%s",yyvsp[-1].str,yyvsp[0].str);
		   Delete(yyvsp[0].str);
               }
    break;

  case 413:
#line 5284 "parser.y"
    {
                   yyval.str = NewStringf("::%s",yyvsp[0].str);
               }
    break;

  case 414:
#line 5287 "parser.y"
    {
                   yyval.str = NewStringf("::%s",yyvsp[0].str);
               }
    break;

  case 415:
#line 5294 "parser.y"
    {
		 yyval.str = NewStringf("::~%s",yyvsp[0].str);
               }
    break;

  case 416:
#line 5300 "parser.y"
    {
                  yyval.str = NewStringf("%s%s",yyvsp[-1].id,yyvsp[0].id);
		  /*		  if (Len($2)) {
		    scanner_last_id(1);
		    } */
              }
    break;

  case 417:
#line 5309 "parser.y"
    { 
                  yyval.str = 0;
		  if (!yyval.str) yyval.str = NewStringf("%s%s", yyvsp[-1].id,yyvsp[0].str);
      	          Delete(yyvsp[0].str);
               }
    break;

  case 418:
#line 5314 "parser.y"
    { 
		 yyval.str = NewStringf("::%s%s",yyvsp[-1].id,yyvsp[0].str);
                 Delete(yyvsp[0].str);
               }
    break;

  case 419:
#line 5318 "parser.y"
    {
		 yyval.str = NewString(yyvsp[0].id);
   	       }
    break;

  case 420:
#line 5321 "parser.y"
    {
		 yyval.str = NewStringf("::%s",yyvsp[0].id);
               }
    break;

  case 421:
#line 5324 "parser.y"
    {
                 yyval.str = NewString(yyvsp[0].str);
	       }
    break;

  case 422:
#line 5327 "parser.y"
    {
                 yyval.str = NewStringf("::%s",yyvsp[0].str);
               }
    break;

  case 423:
#line 5332 "parser.y"
    {
                   yyval.str = NewStringf("::%s%s",yyvsp[-1].id,yyvsp[0].str);
		   Delete(yyvsp[0].str);
               }
    break;

  case 424:
#line 5336 "parser.y"
    {
                   yyval.str = NewStringf("::%s",yyvsp[0].id);
               }
    break;

  case 425:
#line 5339 "parser.y"
    {
                   yyval.str = NewStringf("::%s",yyvsp[0].str);
               }
    break;

  case 426:
#line 5342 "parser.y"
    {
		 yyval.str = NewStringf("::~%s",yyvsp[0].id);
               }
    break;

  case 427:
#line 5348 "parser.y"
    { 
                   yyval.id = (char *) malloc(strlen(yyvsp[-1].id)+strlen(yyvsp[0].id)+1);
                   strcpy(yyval.id,yyvsp[-1].id);
                   strcat(yyval.id,yyvsp[0].id);
               }
    break;

  case 428:
#line 5353 "parser.y"
    { yyval.id = yyvsp[0].id;}
    break;

  case 429:
#line 5356 "parser.y"
    {
		 yyval.str = NewString(yyvsp[0].id);
               }
    break;

  case 430:
#line 5359 "parser.y"
    {
                  skip_balanced('{','}');
		  yyval.str = NewString(scanner_ccode);
               }
    break;

  case 431:
#line 5363 "parser.y"
    {
		 yyval.str = yyvsp[0].str;
              }
    break;

  case 432:
#line 5368 "parser.y"
    {
                  Hash *n;
                  yyval.node = NewHash();
                  n = yyvsp[-1].node;
                  while(n) {
                     String *name, *value;
                     name = Getattr(n,"name");
                     value = Getattr(n,"value");
		     if (!value) value = (String *) "1";
                     Setattr(yyval.node,name, value);
		     n = nextSibling(n);
		  }
               }
    break;

  case 433:
#line 5381 "parser.y"
    { yyval.node = 0; }
    break;

  case 434:
#line 5385 "parser.y"
    {
		 yyval.node = NewHash();
		 Setattr(yyval.node,"name",yyvsp[-2].id);
		 Setattr(yyval.node,"value",yyvsp[0].id);
               }
    break;

  case 435:
#line 5390 "parser.y"
    {
		 yyval.node = NewHash();
		 Setattr(yyval.node,"name",yyvsp[-4].id);
		 Setattr(yyval.node,"value",yyvsp[-2].id);
		 set_nextSibling(yyval.node,yyvsp[0].node);
               }
    break;

  case 436:
#line 5396 "parser.y"
    {
                 yyval.node = NewHash();
                 Setattr(yyval.node,"name",yyvsp[0].id);
	       }
    break;

  case 437:
#line 5400 "parser.y"
    {
                 yyval.node = NewHash();
                 Setattr(yyval.node,"name",yyvsp[-2].id);
                 set_nextSibling(yyval.node,yyvsp[0].node);
               }
    break;

  case 438:
#line 5405 "parser.y"
    {
                 yyval.node = yyvsp[0].node;
		 Setattr(yyval.node,"name",yyvsp[-2].id);
               }
    break;

  case 439:
#line 5409 "parser.y"
    {
                 yyval.node = yyvsp[-2].node;
		 Setattr(yyval.node,"name",yyvsp[-4].id);
		 set_nextSibling(yyval.node,yyvsp[0].node);
               }
    break;

  case 440:
#line 5416 "parser.y"
    {
		 yyval.id = yyvsp[0].id;
               }
    break;

  case 441:
#line 5419 "parser.y"
    {
                 yyval.id = Char(yyvsp[0].dtype.val);
               }
    break;


    }

/* Line 991 of yacc.c.  */
#line 9009 "y.tab.c"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  int yytype = YYTRANSLATE (yychar);
	  char *yymsg;
	  int yyx, yycount;

	  yycount = 0;
	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  for (yyx = yyn < 0 ? -yyn : 0;
	       yyx < (int) (sizeof (yytname) / sizeof (char *)); yyx++)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      yysize += yystrlen (yytname[yyx]) + 15, yycount++;
	  yysize += yystrlen ("syntax error, unexpected ") + 1;
	  yysize += yystrlen (yytname[yytype]);
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "syntax error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[yytype]);

	      if (yycount < 5)
		{
		  yycount = 0;
		  for (yyx = yyn < 0 ? -yyn : 0;
		       yyx < (int) (sizeof (yytname) / sizeof (char *));
		       yyx++)
		    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
		      {
			const char *yyq = ! yycount ? ", expecting " : " or ";
			yyp = yystpcpy (yyp, yyq);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yycount++;
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("syntax error; also virtual memory exhausted");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror ("syntax error");
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* Return failure if at end of input.  */
      if (yychar == YYEOF)
        {
	  /* Pop the error token.  */
          YYPOPSTACK;
	  /* Pop the rest of the stack.  */
	  while (yyss < yyssp)
	    {
	      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
	      yydestruct (yystos[*yyssp], yyvsp);
	      YYPOPSTACK;
	    }
	  YYABORT;
        }

      YYDSYMPRINTF ("Error: discarding", yytoken, &yylval, &yylloc);
      yydestruct (yytoken, &yylval);
      yychar = YYEMPTY;

    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab2;


/*----------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action.  |
`----------------------------------------------------*/
yyerrlab1:

  /* Suppress GCC warning that yyerrlab1 is unused when no action
     invokes YYERROR.  */
#if defined (__GNUC_MINOR__) && 2093 <= (__GNUC__ * 1000 + __GNUC_MINOR__) \
    && !defined __cplusplus
  __attribute__ ((__unused__))
#endif


  goto yyerrlab2;


/*---------------------------------------------------------------.
| yyerrlab2 -- pop states until the error token can be shifted.  |
`---------------------------------------------------------------*/
yyerrlab2:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
      yydestruct (yystos[yystate], yyvsp);
      yyvsp--;
      yystate = *--yyssp;

      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;


  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*----------------------------------------------.
| yyoverflowlab -- parser overflow comes here.  |
`----------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 5426 "parser.y"


SwigType *Swig_cparse_type(String *s) {
   String *ns;
   ns = NewStringf("%s;",s);
   Seek(ns,0,SEEK_SET);
   scanner_file(ns);
   top = 0;
   scanner_next_token(PARSETYPE);
   yyparse();
   /*   Printf(stdout,"typeparse: '%s' ---> '%s'\n", s, top); */
   return top;
}


Parm *Swig_cparse_parm(String *s) {
   String *ns;
   ns = NewStringf("%s;",s);
   Seek(ns,0,SEEK_SET);
   scanner_file(ns);
   top = 0;
   scanner_next_token(PARSEPARM);
   yyparse();
   /*   Printf(stdout,"typeparse: '%s' ---> '%s'\n", s, top); */
   return top;
}










