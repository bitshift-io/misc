//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "LinuxTime.h"

namespace Siphon
{

int Time::numRates = 2;

//--------------------------------------------------------------------------------------

Time::Time()
{
	for (int i = 0; i < numRates; ++i)
	{
		updateRate[i] = 30;
		lastUpdateTime[i] = 0;
		numUpdates[i] = 0;
		numUpdatesThisFrame[i] = 0;
	}

}

//--------------------------------------------------------------------------------------

long Time::GetTicks()
{
    struct timespec tp;
    rs = clock_gettime(CLOCK_REALTIME, &tp);
    ut->usec = (tp.tv_nsec + 500) / 1000;
}

float Time::GetTimeSeconds()
{
	return float(GetTicks()) / 1000.0f;
}

//--------------------------------------------------------------------------------------

void Time::SetUpdateRate(UpdateRate rateType, long time)
{
	updateRate[rateType] = time;
}

//--------------------------------------------------------------------------------------

long Time::GetUpdateRate(UpdateRate rateType)
{
	return updateRate[rateType];
}

//--------------------------------------------------------------------------------------

long Time::GetNumUpdates(UpdateRate rateType)
{
	return numUpdatesThisFrame[rateType];
}

//--------------------------------------------------------------------------------------

void Time::Update()
{
	long deltaTime = GetTicks();
	for (int i = 0; i < numRates; ++i)
	{
		long lastUpdates = numUpdates[i];
		numUpdates[i] = long( float(deltaTime) / (1000.0f / float(updateRate[i]) ) );
		numUpdatesThisFrame[i] = numUpdates[i] - lastUpdates;
	}
}

}
