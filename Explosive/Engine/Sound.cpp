//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "Sound.h"
#include "Log.h"

namespace Siphon
{

#ifndef WIN32
    #define stricmp strcasecmp
#endif

//#define DISABLE_SOUND 1

#define BUFFER_SIZE     32768       // 32 KB buffers

std::string SoundMgr::basePath("data/");

SoundMgr::SoundMgr()
{
#ifndef DISABLE_SOUND

	//Open device
	device = alcOpenDevice(NULL);
	if (!device)
	{
		//we failed to initialize the device
		Log::Print("Failed to get OpenAL device\n");
		return;
	}

	//Create context(s)
	context = alcCreateContext(device, NULL);

	//Set active context
	alcMakeContextCurrent(context);

//	alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);

	// Clear Error Code
	ALenum error;
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		Log::Print("OpenAL Error: %s\n", alGetString(error));
	}

	// Get the device specifier.
    const char* deviceSpecifier = alcGetString(device, ALC_DEVICE_SPECIFIER);
	Log::Print("OpenAL Initilised using: %s\n", deviceSpecifier);

	// generate sources until we cant get more
	sources.SetCapacity(16);
	SoundSource* src = 0;
	int i = 0;
	do
	{
		Log::Print("new SoundSource\t");
		src = new SoundSource();

		if (!src->IsValid())
		{
			Log::Print("[FAIL]\n");
			delete src;
			src = 0;
		}
		else
		{
			Log::Print("[OK]\n");
			sources.Insert(src);
			++i;
		}
	} while (src && i < 16);

#endif
}

SoundMgr::~SoundMgr()
{
	while (sources.GetSize())
	{
		delete sources[0];
		sources.Remove(0);
	}

	while (buffers.GetSize())
	{
		delete buffers[0];
		buffers.Remove(0);
	}

#ifndef DISABLE_SOUND

	//Get active context
	context = alcGetCurrentContext();

	//Get device for active context
	device = alcGetContextsDevice(context);

	//Disable context
	alcMakeContextCurrent(NULL);

	//Release context(s)
	alcDestroyContext(context);

	//Close device
	alcCloseDevice(device);

#endif
}

void SoundMgr::SetBasePath(const char* path) 
{
	basePath = std::string(path);
}

Sound* SoundMgr::Load(const char* name)
{
	std::string path = basePath + std::string(name);
	Sound* snd = new Sound();
	if (!snd->Load(path.c_str()))
	{
		delete snd;
		return 0;
	}

	return snd;
}

void SoundMgr::Release(Sound* resource)
{
	delete resource;
}

void SoundMgr::Update()
{
	for (int i = 0; i < sources.GetSize(); ++i)
	{
		SoundSource* src = sources[i];
		if (!src->IsPlaying())
			src->SetInUse(false);
	}
}

void SoundMgr::SetListenerPosition(float x, float y, float z)
{
	//set the position using 3 seperate floats
	alListener3f(AL_POSITION, x,y,z);
}

void SoundMgr::SetListenerOrientation(float fx, float fy, float fz, float ux, float uy, float uz)
{
	//set the orientation using an array of floats
	float vec[6];
	vec[0] = fx;
	vec[1] = fy;
	vec[2] = fz;
	vec[3] = ux;
	vec[4] = uy;
	vec[5] = uz;
	alListenerfv(AL_ORIENTATION, vec);
}

SoundSource* SoundMgr::GetSource()
{
	for (int i = 0; i < sources.GetSize(); ++i)
	{
		SoundSource* src = sources[i];
		if (!src->InUse())
		{
			src->SetInUse(true);
			return src;
		}
	}

	return 0;
}

void SoundMgr::ReleaseSource(SoundSource* source)
{
	if (source)
		source->SetInUse(false);
}

SoundBuffer* SoundMgr::GetBuffer(const char* name)
{
	for (int i = 0; i < buffers.GetSize(); ++i)
	{
		SoundBuffer* buffer = buffers[i];
		if (strcmp(buffer->GetName(), name) == 0)
			return buffer;
	}

	SoundBuffer* buffer = new SoundBuffer();
	if (!buffer->Load(name))
	{
		delete buffer;
		return 0;
	}

	buffers.Insert(buffer);
	return buffers.Last();
}

//--------------------------------------------------------------------------------------

Sound::Sound() :
	source(0),
	buffer(0),
	radius(0.f),
	position(0.f, 0.f, 0.f),
	velocity(0.f, 0.f, 0.f),
	looping(false),
	pitch(1.f),
	gain(1.f),
	relative(false)
{	

}

Sound::~Sound()
{
	Stop();
}

void Sound::SetRadius(float radius)
{
	this->radius = radius;

	if (source)
		return source->SetRadius(radius);
}

void Sound::SetPosition(const Vector3& position)
{
	this->position = position;

	if (source)
		return source->SetPosition(position);
}

void Sound::SetVelocity(const Vector3& velocity)
{
	this->velocity = velocity;

	if (source)
		return source->SetVelocity(velocity);
}

void Sound::SetSourceRelative(bool relative)
{
	this->relative = relative;

	if (source)
		return source->SetSourceRelative(relative);
}	

void Sound::SetLooping(bool looping)
{
	this->looping = looping;

	if (source)
		return source->SetLooping(looping);
}

void Sound::SetPitch(float pitch)
{
	this->pitch = pitch;

	if (source)
		return source->SetPitch(pitch);
}

void Sound::SetGain(float gain)
{
	this->gain = gain;

	if (source)
		return source->SetGain(gain);
}

bool Sound::Load(const char* name)
{
	buffer = gSoundMgr.GetBuffer(name);
	return buffer;
}

void Sound::Play()
{
	// obtain a new source, notify soundmgr
	source = gSoundMgr.GetSource(); 
	if (source)
	{
		source->SetBuffer(buffer);
		source->SetLooping(looping);
		source->SetPitch(pitch);
		source->SetRadius(radius);
		source->SetSourceRelative(relative);
		source->SetPosition(position);
		source->SetVelocity(velocity);
		source->Play();
	}
}

void Sound::Stop()
{
	// release source, notify soundmgr
	gSoundMgr.ReleaseSource(source);
	source = 0;
}

bool Sound::IsPlaying()
{
	if (source)
		return source->IsPlaying();

	return false;
}

//--------------------------------------------------------------------------------------

SoundSource::SoundSource() :
	inUse(false),
	alSource(0)
{
	alGenSources(1, &alSource);
	ALenum error = alGetError();
	if (error != AL_NO_ERROR)
		Log::Print("OpenAL Error: %s\n", alGetString(error));
}

SoundSource::~SoundSource()
{
	alDeleteSources(1, &alSource);
}

void SoundSource::SetRadius( float radius )
{
	alSourcef(alSource, AL_REFERENCE_DISTANCE, radius);
	alSourcef(alSource, AL_ROLLOFF_FACTOR, radius / 4);
}

void SoundSource::SetPitch(float pitch)
{
	alSourcef( alSource, AL_PITCH, pitch );
}

void SoundSource::SetGain(float gain)
{
	alSourcef( alSource, AL_GAIN, 1.0f);
}

void SoundSource::SetLooping( bool state )
{
	alSourcei( alSource, AL_LOOPING, state );
}

void SoundSource::SetPosition(const Vector3& position)
{
	alSource3f(alSource,AL_POSITION, position[X], position[Y], position[Z]);
}

void SoundSource::SetVelocity(const Vector3& velocity)
{
	alSource3f(alSource,AL_VELOCITY, velocity[X], velocity[Y], velocity[Z]);
}

void SoundSource::SetBuffer(SoundBuffer* buffer)
{
	alSourcei(alSource, AL_BUFFER, buffer->alSampleSet);
	ALenum error = alGetError();
	if (error != AL_NO_ERROR)
		Log::Print("OpenAL Error: %s\n", alGetString(error));
}
/*
Resource* SoundSource::Clone()
{
#ifdef DISABLE_SOUND
	return this;
#endif

	ALenum error;

	Sound* clone = new Sound();
	clone->name = name;
	clone->alSampleSet = alSampleSet;

	ALint alBuffer;
	alGetSourcei(alSource, AL_BUFFER, &alBuffer);

	if ((error = alGetError()) != AL_NO_ERROR)
	{
		Log::Print("OpenAL Error: %s, with sound: %s\n", alGetString(error), name.c_str());
		return NULL;
	}

    alGenSources(1, &clone->alSource);
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		Log::Print("OpenAL Error: %s, with sound: %s\n", alGetString(error), name.c_str());
		return NULL;
	}
	/*
    if (alGetError() != AL_NO_ERROR)
    {
		Log::Print("Error generating audio source for: %s", name.c_str());
       // return NULL;
    }* /

    alSourcei(clone->alSource, AL_BUFFER, alBuffer);
	return clone;
}*/

void SoundSource::SetSourceRelative(bool relative)
{
	alSourcei(alSource,AL_SOURCE_RELATIVE, relative ? AL_TRUE : AL_FALSE);
}

void SoundSource::Play()
{
	if (!IsPlaying())
		alSourcePlay(alSource);

	ALenum error = alGetError();
	if (error != AL_NO_ERROR)
		Log::Print("OpenAL Error: %s\n", alGetString(error));
}

bool SoundSource::IsPlaying()
{
	ALint state;
	alGetSourcei(alSource, AL_SOURCE_STATE, &state);
	return state == AL_PLAYING;
}

void SoundSource::Stop()
{
	alSourceStop(alSource);
}

//--------------------------------------------------------------------------------------

SoundBuffer::SoundBuffer() :
	alFormatBuffer(0),
	alFreqBuffer(0),
	alBufferLen(0),
	alSampleSet(0)
{

}

void SoundBuffer::Destroy()
{
	alDeleteBuffers(1, &alSampleSet);
}

bool SoundBuffer::Load(const char* name)
{
#ifdef DISABLE_SOUND
	return false;
#endif

	this->name = name;

	//load our sound
	Log::Print("Loading sound: %s\n", name);

	const char* extension = name + (strlen(name) - 3);

	ALint error;
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		Log::Print("Unknown openal error: %s\n", alGetString(error));
	}

	char* alBuffer = 0;
	if (stricmp(extension, "wav") == 0)
	{
		ALboolean loop;
		alutLoadWAVFile(name, &alFormatBuffer, (void**)&alBuffer, &alBufferLen, &alFreqBuffer, &loop);

		if ((error = alGetError()) != AL_NO_ERROR)
		{
			Log::Print("Failed to load WAV: %s\n", alGetString(error));
			return false;
		}
	}
	else if (stricmp(extension, "ogg") == 0)
	{
		ALboolean loop;
		LoadOGGFile(name, &alFormatBuffer, (void**)&alBuffer, &alBufferLen, &alFreqBuffer, &loop);
	}
	else
	{
		Log::Print("Unsupported file extension\n");
		return false;
	}

	alGenBuffers(1, &alSampleSet);
	alBufferData(alSampleSet, alFormatBuffer, alBuffer, alBufferLen, alFreqBuffer);

	// unload the data we dont need anymore
	if (stricmp(extension, "wav") == 0)
		alutUnloadWAV(alFormatBuffer, alBuffer, alBufferLen, alFreqBuffer);
	else
		delete[] alBuffer;

	error = alGetError();
	if (error != AL_NO_ERROR)
		Log::Print("OpenAL Error: %s\n", alGetString(error));

	return true;
}

void SoundBuffer::LoadOGGFile(const ALbyte *file, ALenum *format, ALvoid **data, ALsizei *size, ALsizei *freq, ALboolean *loop)
{
	int endian = 0;                         // 0 for Little-Endian, 1 for Big-Endian
	int bitStream;
	long bytes;
	char buffer[BUFFER_SIZE];                // Local fixed size array

	FILE* f = fopen(file, "rb");
	if (f == NULL)
		return;

	vorbis_info *pInfo;
	OggVorbis_File oggFile;

	// Try opening the given file
	if (ov_open(f, &oggFile, NULL, 0) != 0)
		return;

	// Get some information about the OGG file
	pInfo = ov_info(&oggFile, -1);

	// Check the number of channels... always use 16-bit samples
	if (pInfo->channels == 1)
		*format = AL_FORMAT_MONO16;
	else
		*format = AL_FORMAT_STEREO16;

	// The frequency of the sampling rate
	*freq = pInfo->rate;

	char* soundBuffer = 0;
	int bufferSize = 0;

	// Keep reading until all is read
	do
	{
		// Read up to a buffer's worth of decoded sound data
		bytes = ov_read(&oggFile, buffer, BUFFER_SIZE, endian, 2, 1, &bitStream);

		if (bytes < 0)
		{
			ov_clear(&oggFile);
			Log::Print("Error decoding file");
			return;
		}

		// allocate a new block of memroy the right size, delete the old block
		char* oldBuffer = soundBuffer;

		int oldBufferSize = bufferSize;
		bufferSize += bytes;
		soundBuffer = new char[bufferSize];
		memcpy(soundBuffer, oldBuffer, oldBufferSize);
		memcpy(soundBuffer + oldBufferSize, buffer, bytes);

		delete[] oldBuffer;

	} while (bytes > 0);

	ov_clear(&oggFile);
	*size = bufferSize;
	*data = soundBuffer;
}

}
