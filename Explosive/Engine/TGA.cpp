#include "TGA.h"
#include "../Util/File.h"

namespace Siphon
{

#define DESC_ABITS      0x0f
#define DESC_HORIZONTAL 0x10
#define DESC_VERTICAL   0x20
	
TGA::TGA() : image(0)
{

}

TGA::~TGA()
{
	free(image);
}

unsigned char* TGA::GetPixels()
{
	return (unsigned char*)image;
}

int TGA::GetWidth()
{
	return header.width;
}

int TGA::GetHeight()
{
	return header.height;
}

int TGA::GetBPP()
{
	return header.bpp / 8;
}

bool TGA::Save(const char* file, const void* data, int width, int height, int bpp)
{
	TGAHead header;
	memset(&header, 0x0, sizeof(TGAHead));
	header.width = width;
	header.height = height;
	header.bpp = bpp;
	header.imageType = 2; // uncompressed

	File out(file, "wb");
	if (!out.Valid())
		return false;

	out.Write(&header, sizeof(TGAHead));
	out.Write(data, width * height * header.bpp / 8);

	return true;
}

bool TGA::LoadUncompresed( FILE* pFile, TGAHead& header )
{
	if( header.width <= 0 || header.height <= 0 || header.bpp < 24 )
	{
		printf("something wrong bpp: %i width: %i height: %i\n", header.bpp, header.width, header.height);
		return false;
	}

	unsigned int bytesPerPixel = header.bpp / 8;

	image = new char[ header.width * header.height * bytesPerPixel ];
	if (!image)
	{
		printf("failed to alloc mem of size: %i bytes\n", (header.width * header.height * bytesPerPixel));
		return false;
	}

	int curByte = 0;
	int pixelsRead = 0;
	int noPixels = header.width * header.height;
	char pixel[4];

	do
	{
		//Read 1 Pixel
        if( fread(pixel, 1, bytesPerPixel, pFile) != bytesPerPixel)
		{
			printf( "failed to from file (pixel)\n");
                       	return false;
		}

		image[curByte + 0] = pixel[2];
		image[curByte + 1] = pixel[1];
		image[curByte + 2] = pixel[0];

		if( bytesPerPixel >= 4 )
		{
			image[curByte + 3] = pixel[3];
		}

		curByte += bytesPerPixel;
		++pixelsRead;

	} while(pixelsRead < noPixels);

	return true;
}

bool TGA::LoadCompresed( FILE* pFile, TGAHead& header )
{
	if( header.width <= 0 || header.height <= 0 || header.bpp < 24 )
	{
		printf("something wrong bpp: %i width: %i height: %i\n", header.bpp, header.width, header.height);
		return false;
	}

	unsigned int bytesPerPixel = header.bpp / 8;

	image = (char*)malloc( header.width * header.height * bytesPerPixel );
	if( !image )
	{
		printf( "failed to alloc mem\n");
		return false;
	}

	unsigned int curByte = 0;
	unsigned int pixelsRead = 0;
	int noPixels = header.width * header.height;
	char* pixel = (char*)malloc( bytesPerPixel );
	unsigned char chunkHead;

	do
	{
		if( fread( &chunkHead, sizeof(char), 1, pFile ) == 0 )
		{
			printf( "failed to from file (chunkHead)\n");
			return false;
		}

		//SPEED UP LOAD TIME BY READING A CHUNK AT A TIME!!!
		if( chunkHead < 128 ) //RAW - not compressesd
		{
			++chunkHead; //increase by one to determine how many pixels follow
			pixelsRead += chunkHead;
			
/*
			if( !fread( &image[ curByte ], bytesPerPixel, chunkHead, pFile) )
			{
				printf( "failed to from file (pixel RAW)\n");

				if( feof( pFile ) )
					printf("END OF FILE!!\n");

                return false;
			}
/*
			char temp;
			for( int i = 0; i < chunkHead; ++i )
			{
				curByte += bytesPerPixel;

				temp = image[ curByte + 0 ];
				image[ curByte + 0 ] = image[ curByte + 2 ];
				image[ curByte + 2 ] = temp;

				if( bytesPerPixel >= 4 )
				{
					//if this alpha pixel is not 0 or 255, then we are laoding a blend
					//texture
					if( pixel[3] > 0 && pixel[3] < 255 )
							blendType = BLEND;
				}
			}*/

			//curByte += bytesPerPixel * chunkHead;

			for(short counter = 0; counter < chunkHead; ++counter, curByte += bytesPerPixel )
       		{
            	//Read 1 Pixel
            	if( fread( pixel, 1, bytesPerPixel, pFile) != bytesPerPixel )
				{
					printf( "failed to from file (pixel RAW)\n");

					if( feof( pFile ) )
						printf("END OF FILE!!\n");
                    return false;
				}

				image[ curByte + 0 ] = pixel[2];
				image[ curByte + 1 ] = pixel[1];
				image[ curByte + 2 ] = pixel[0];

				//if this alpha pixel is not 0 or 255, then we are laoding a blend
				//texture
				//if( pixel[3] != 0 && pixel[3] != 255 )
				//{
					//blendType = BLEND;
				//	printf("alpha value: %i\n", pixel[3] );
				//}

				if( bytesPerPixel >= 4 )
				{
					image[ curByte + 3 ] = pixel[3];
				}

				if(pixelsRead > noPixels)											// Make sure we havent read too many pixels
				{
					printf("ERROR! READ TO MANY PIXELS!\n");
				}
			}
		}
		else //RLE - compressed
		{
			chunkHead -= 127;
			pixelsRead += chunkHead;

			//Read 1 Pixel
            if( fread( pixel, 1, bytesPerPixel, pFile) != bytesPerPixel )
			{
				printf( "failed to from file (pixel RLE)\n");

				if( feof( pFile ) )
						printf("END OF FILE!!\n");
                return false;
			}

			for(short counter = 0; counter < chunkHead; ++counter, curByte += bytesPerPixel)
       		{
				image[ curByte + 0 ] = pixel[2];
				image[ curByte + 1 ] = pixel[1];
				image[ curByte + 2 ] = pixel[0];

				if( bytesPerPixel >= 4 )
					image[ curByte + 3 ] = pixel[3];
			}
		}
	} while( pixelsRead < noPixels );

	free(pixel);

	return true;
}

bool TGA::Load( const char* file)
{
	FILE* pFile = 0;
	pFile = fopen( file, "rb" );

	if( pFile == 0 )
		return false;

	if( !fread( &header, sizeof( TGAHead ), 1, pFile ) )
		return false;

	bool ret = false;
	if( header.imageType == 10 )
		ret = LoadCompresed( pFile, header );
	else //if( header.imageType == 2 )
		ret = LoadUncompresed( pFile, header );

	bool flipHorizontal = header.descritor & DESC_HORIZONTAL;
	bool flipVertical = header.descritor & DESC_VERTICAL;
	char alphaBits = header.descritor & DESC_ABITS;

	if (flipVertical)
	{
		unsigned int bytesPerPixel = header.bpp / 8;
		int lineLength = header.width * bytesPerPixel;
		char* oldImage = image;
		image = new char[header.width * header.height * bytesPerPixel];

		int hNew = header.height - 1;
		for (int hOld = 0; hOld < header.height; ++hOld, --hNew)
			memcpy(&image[hNew * lineLength], &oldImage[hOld * lineLength], lineLength);
		
		delete[] oldImage;
	}

	fclose(pFile);
	return ret;
}

bool TGA::ConvertToGreyScale()
{
	int width = GetWidth();
	int height = GetHeight();
	int bpp = GetBPP();
	char* temp = image;

	image = (char*)new unsigned int[width * height];

	unsigned int* intPtr = (unsigned int*)image;
	for (int i = 0; i < width * height; ++i)
		intPtr[i] = 1.0f;

	#define RES 256
	float frequency = 20.0f;
	unsigned char *data = new unsigned char[RES*RES];
	unsigned char *dp = data;
	for (int i = 0; i < RES; ++i) {
		for (int j = 0; j < RES; ++j) {
			float u = float(i) / float(RES);
			float v = float(j) / float(RES);
			u *= frequency;
			v *= frequency;
			if ((int(u) + int(v)) & 1) {
				*dp++ = 1.0f;
				//*dp++ = color0[1];
				//*dp++ = color0[2];
			}
			else {
				*dp++ = 0.0f;
				//*dp++ = color1[1];
				//*dp++ = color1[2];
			}
			//*dp++ = 1.;
		}
	}

	image = (char*)data;

/*
	for (int w = 0; w < width; ++w)
	{
		for (int h = 0; h < height; ++h)
		{
			image[w + h * width] = temp[(w + h * width) * bpp];
		}
	}*/

	delete[] temp;
	return true;
}

}
