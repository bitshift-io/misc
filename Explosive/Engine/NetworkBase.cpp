#include "NetworkBase.h"
#include "Log.h"
#include "Server.h"
#include "Client.h"

namespace Siphon
{

bool SocketStream::Close()
{
	if (!IsValid())
		return false;

	if (closesocket(sock) != 0)
	{
		Log::Print("Failed at socket: %ld\n", WSAGetLastError());
		sock = 0;
		return false;
	}

	sock = 0;
	return true;
}

int SocketStream::GetSocketState()
{
	if (!IsValid())
		return 0;

	int state = 0;

	fd_set readFDs, writeFDs, exceptFDs;
	FD_ZERO(&readFDs);
	FD_ZERO(&writeFDs);
	FD_ZERO(&exceptFDs);

	FD_SET(sock, &readFDs);
	FD_SET(sock, &exceptFDs);
	FD_SET(sock, &writeFDs);

	timeval val;
	val.tv_sec = 0;
	val.tv_usec = 0;

	int value = select(0, &readFDs, &writeFDs, &exceptFDs, &val);
	if (value == SOCKET_ERROR)
	{
		Log::Print("Failed at socket: %ld\n", WSAGetLastError());
		return 0;
	}

	if (value > 0)
	{
		if (FD_ISSET(sock, &readFDs)) 
			state |= Read;
		
		if (FD_ISSET(sock, &exceptFDs)) 
			state |= Error;
		
		if (FD_ISSET(sock, &writeFDs)) 
			state |= Write;
	}

	return state;
}

int SocketStream::Update(Array<ServerInfo>* recvList)
{
	if (type == UnReliable)
	{
		delete[] buffer;
		buffer = 0;
		bufferSize = 0;
	}

	int totalReceived = 0;
	char temp[32768]; // 32K for UDP, no message can be over 32K
	while (GetSocketState() & Read)
	{
		if (type == UnReliable)
		{
			int nothing = 0;
		}

		int size = Receive(temp, 32768, recvList);
		totalReceived += size;

		// resize buffer
		int oldBufferSize = bufferSize;
		bufferSize += size;
		char* newBuffer = new char[bufferSize];
		memcpy(newBuffer, buffer, oldBufferSize);
		memcpy(newBuffer + oldBufferSize, temp, size);
		delete[] buffer;
		buffer = newBuffer;
	}

	return totalReceived;
}

unsigned long SocketStream::GetAddress()
{
	sockaddr_in sockAddr;
    int len = sizeof(sockaddr_in);
    getsockname(sock, (sockaddr*)&sockAddr, &len);
	return ntohl(sockAddr.sin_addr.s_addr);
}

int SocketStream::GetBufferData(void* data, int size)
{
	int s = PeekBufferData(data, size);

	// shrink the buffer
	bufferSize -= s;

	if (bufferSize <= 0)
	{
		delete[] buffer;
		buffer = 0;
	}
	else
	{
		char* newBuffer = new char[bufferSize];
		memcpy(newBuffer, buffer + s, bufferSize);
		delete[] buffer;
		buffer = newBuffer;
	}

	return s;
}

int SocketStream::PeekBufferData(void* data, int size)
{
	int s = min(size, bufferSize);
	memcpy(data, buffer, s);
	return s;
}

unsigned long SocketStream::LookupAddress(const char* hostAddr)
{
	unsigned long remoteAddr = inet_addr(hostAddr);
	if (remoteAddr == INADDR_NONE) 
	{
		// pcHost isn't a dotted IP, so resolve it through DNS
		hostent* he = gethostbyname(hostAddr);
		if (he == 0) 
			return INADDR_NONE;

		remoteAddr = *((unsigned long*)he->h_addr_list[0]);
	}

	return remoteAddr;
}

void SocketStream::ConvertToString(unsigned long addr, std::string& str)
{
	unsigned char* ip = (unsigned char*)&addr;
	int intIp[4];

	for (int i = 0; i < 4; ++i)
		intIp[i] = ip[i];

	char strIp[256];
	sprintf(strIp, "%i.%i.%i.%i", intIp[3], intIp[2], intIp[1], intIp[0]);
	str = strIp;
}

unsigned long SocketStream::GetLocalAddress()
{
	char hostName[256];
	if (gethostname(hostName, sizeof(hostName)) == SOCKET_ERROR)
		return INADDR_NONE;

	hostent* he = gethostbyname(hostName);
	if (he == 0) 
		return INADDR_NONE;

	return *((unsigned long*)he->h_addr_list[0]);
}

SOCKET SocketStream::CreateSocket()
{
	return socket(AF_INET, type == Reliable ? SOCK_STREAM : SOCK_DGRAM, type == Reliable ? IPPROTO_TCP : IPPROTO_UDP);
}

bool SocketStream::Connect(const char* addr, int recvPort, int sendPort)
{
	// can only broadcast over UDP
	if (type == Reliable && addr == NULL)
		return false;

	sock = CreateSocket();
	if (sock == SOCKET_ERROR)
	{
		Log::Print("Failed at socket: %ld\n", WSAGetLastError());
		return false;
	}
/*
	int err = setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(yes));
	if (err != 0)
	{
		Log::Print("Failed at setsockopt (SO_REUSEPORT): %ld\n", WSAGetLastError());
		closesocket(sock);
		return false;
	}
	*/
/*
	// no delay to reduce lag
	bool noDelay = true;
	int err = setsockopt(sock, SOL_SOCKET, TCP_NODELAY, (char*)noDelay, sizeof(bool));
	if (err == SOCKET_ERROR)
		Log::Print("Failed at setsockopt (nodelay): %ld\n", WSAGetLastError());
*/
	if (addr == NULL)
	{
		bool broadcast = true;
		int err = setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (char*)&broadcast, sizeof(bool));

		if (err == SOCKET_ERROR)
		{
			Log::Print("Failed at setsockopt: %ld\n", WSAGetLastError());
			closesocket(sock);
			return false;
		}
	}

	// connect to server
	sockaddr_in clientService;
	clientService.sin_family = AF_INET;


	//if (type == Reliable)
	//	clientService.sin_addr.s_addr = htonl(INADDR_ANY);
	//else
	clientService.sin_addr.s_addr = addr ? LookupAddress(addr) : htonl(INADDR_ANY);

	//if (type == UnReliable)
	//	clientService.sin_addr.s_addr = htonl(INADDR_ANY);

	clientService.sin_port = htons(recvPort);
/*
	recvAddr = clientService;
	sendAddr = clientService;
	sendAddr.sin_port = htons(sendPort);
*/
	if (type == Reliable)
	{
		if (connect(sock, (SOCKADDR*)&clientService, sizeof(clientService)) == SOCKET_ERROR) 
		{
			Log::Print("Failed to connect: %ld\n", WSAGetLastError());
			return false;
		}
	}
	else // unreliable (udp)
	{	
		recvAddr = clientService;
		sendAddr = clientService;
		sendAddr.sin_port = htons(sendPort);

		if (addr == NULL)
			sendAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);

		// bind socket
		if (bind(sock, (SOCKADDR*)&clientService, sizeof(clientService)) == SOCKET_ERROR) 
		{
			Log::Print("Failed at bind: %ld\n", WSAGetLastError());
			closesocket(sock);
			return false;
		}

		/*
		Listen(addr, recvPort, sendPort);
			/*
		// bind socket
		if (bind(sock, (SOCKADDR*)&clientService, sizeof(clientService)) == SOCKET_ERROR) 
		{
			Log::Print("Failed at bind: %ld\n", WSAGetLastError());
			closesocket(sock);
			return false;
		}*/
	}

	//Log::Print("Connected to server: %s:%i\n", addr, recvPort);
	return true;
}

bool SocketStream::UDPConnect(int port)
{
	sock = CreateSocket();
	if (sock == SOCKET_ERROR)
	{
		Log::Print("Failed at socket: %ld\n", WSAGetLastError());
		return false;
	}

	// connect to server
	sockaddr_in clientService;
	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = htonl(INADDR_ANY);
	clientService.sin_port = htons(port);

	// bind socket
	if (bind(sock, (SOCKADDR*)&clientService, sizeof(clientService)) == SOCKET_ERROR) 
	{
		Log::Print("Failed at bind: %ld\n", WSAGetLastError());
		closesocket(sock);
		return false;
	}

	//Log::Print("Connected to server: %s:%i\n", addr, recvPort);
	return true;
}

bool SocketStream::UDPSendTo(const void* data, int size, const char* address, int port)
{
	sockaddr_in sendAddr;
	memset(&sendAddr, 0, sizeof(sockaddr_in));
	sendAddr.sin_addr.s_addr = LookupAddress(address);
	sendAddr.sin_port = htons(port);
	sendAddr.sin_family = AF_INET;

	return sendto(sock, (const char*)data, size, 0, (SOCKADDR*)&sendAddr, sizeof(sendAddr)) != SOCKET_ERROR;
}

bool SocketStream::Listen(const char* addr, int recvPort, int sendPort)
{
	// can only broadcast over UDP
	if (type == Reliable && addr == NULL)
		return false;

	sock = CreateSocket();
	if (sock == SOCKET_ERROR)
	{
		Log::Print("Failed at socket: %ld\n", WSAGetLastError());
		return false;
	}

	if (addr == NULL)
	{
		bool broadcast = true;
		int err = setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (char*)&broadcast, sizeof(bool));

		if (err == SOCKET_ERROR)
		{
			Log::Print("Failed at setsockopt: %ld\n", WSAGetLastError());
			closesocket(sock);
			return false;
		}
	}

	// connect to server
	sockaddr_in serverService;
	serverService.sin_family = AF_INET;
	serverService.sin_port = htons(recvPort);

	if (type == Reliable)
		serverService.sin_addr.s_addr = htonl(INADDR_ANY); //addr ? LookupAddress(addr) : htonl(INADDR_ANY);
	else
		serverService.sin_addr.s_addr = addr ? LookupAddress(addr) : htonl(INADDR_ANY);

	if (type == UnReliable)
	{
		recvAddr = serverService;
		sendAddr = serverService;
		sendAddr.sin_port = htons(sendPort);

		if (addr == NULL)
			sendAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	}

	// bind socket
	if (bind(sock, (SOCKADDR*)&serverService, sizeof(serverService)) == SOCKET_ERROR) 
	{
		Log::Print("Failed at bind: %ld\n", WSAGetLastError());
		closesocket(sock);
		return false;
	}

	if (type == Reliable)
	{
		// listen
		if (listen(sock, 1) == SOCKET_ERROR) 
		{
			Log::Print("Failed at listen: %ld\n", WSAGetLastError());
			closesocket(sock);
			return false;
		}
	}

	return true;
}

bool SocketStream::Accept(SocketStream& stream)
{
	sockaddr_in clientRemote;
	int addrSize = sizeof(clientRemote);

	stream.sock = accept(sock, (sockaddr*)&clientRemote, &addrSize);
	return stream.sock != SOCKET_ERROR;
}

bool SocketStream::IsValid()
{
	return sock != 0;
}

bool SocketStream::Send(const void* data, int size)
{
	switch (type)
	{
	case Reliable:
		return send(sock, (const char*)data, size, 0) != SOCKET_ERROR;
	case UnReliable:
		return sendto(sock, (const char*)data, size, 0, (SOCKADDR*)&sendAddr, sizeof(sendAddr)) != SOCKET_ERROR;
	}

	return false;
}

int SocketStream::Receive(void* data, unsigned int size, Array<ServerInfo>* recvList)
{
	int numBytes = 0;

	switch (type)
	{
	case Reliable:
		{
			numBytes = recv(sock, (char*)data, size, 0);
		}
		break;
	case UnReliable:
		{
			sockaddr_in recvAddr;
			int recvAddrSize = sizeof(recvAddr);
			numBytes = recvfrom(sock, (char*)data, size, 0, (SOCKADDR*)&recvAddr, &recvAddrSize);

			if (recvList)
			{
				int noMsgs = numBytes / sizeof(BroadcastMsg);
				for (int i = 0; i < noMsgs; ++i)
				{
					BroadcastMsg msg;
					memcpy(&msg, (char*)data + sizeof(BroadcastMsg) * i, sizeof(BroadcastMsg));
					msg.addr = ntohl(recvAddr.sin_addr.s_addr);

					ServerInfo info;
					info.name = msg.name;
					info.numPlayers = msg.numPlayers;
					ConvertToString(msg.addr, info.ip);
					recvList->Insert(info);
				}
/*
				if (numBytes >= sizeof(BroadcastMsg))
				{
					BroadcastMsg msg;
					msg.addr = recvAddr.sin_addr.s_addr;
					

					//recvList->Insert(ntohl(recvAddr.sin_addr.s_addr));
					recvList->Insert(msg);
				}*/
			}
		}
		break;
	};

	if (numBytes == 0 || numBytes == SOCKET_ERROR || numBytes == WSAECONNRESET)
	{
		//Log::Print("Connection closed by peer\n");
		Close();
		return 0;
	}

	return numBytes;
}

}