//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_SOUND_
#define _SIPHON_SOUND_

#pragma comment(lib, "alut.lib")
#pragma comment(lib, "openal32.lib")
#pragma comment(lib, "ogg.lib")
#pragma comment(lib, "vorbis.lib")
#pragma comment(lib, "vorbisfile.lib")

#include "AL/al.h"
#include "AL/alc.h"
#include "AL/alut.h"
#include "vorbis/vorbisfile.h"

#include <string>

#include "Singleton.h"
#include "Resource.h"
#include "Math3D.h"

namespace Siphon 
{

#define gSoundMgr SoundMgr::GetInstance()

/**
 * sound buffer, one only exists for each sound loaded from disk
 */
class SoundBuffer
{
public:
	SoundBuffer();

	void Destroy();

	bool Load(const char* name);

	// filename, format, data, size, freqency, looping
	static void LoadOGGFile(const ALbyte *file, ALenum *format, ALvoid **data, ALsizei *size, ALsizei *freq, ALboolean *loop);

	const char* GetName() { return name.c_str(); }

	ALenum			alFormatBuffer;
	ALsizei			alFreqBuffer;
	ALsizei			alBufferLen;
	unsigned int	alSampleSet;
	std::string		name;
};

/**
 * sound instance, never make these directly unless you know what your doing!
 */
class SoundSource
{
	friend class SoundMgr;
public:
	SoundSource();
	~SoundSource();

	bool IsValid() { return alSource != 0; }
	void SetBuffer(SoundBuffer* buffer);

	void SetRadius(float radius);
	void SetPosition(const Vector3& position);
	void SetVelocity(const Vector3& velocity);
	void SetSourceRelative(bool relative);
	void Play();
	void Stop();
	bool IsPlaying();
	void SetLooping(bool state);
	void SetPitch(float pitch);
	void SetGain(float gain);

protected:
	bool InUse() { return inUse; }
	void SetInUse(bool inUse) { this->inUse = inUse; }

	unsigned int	alSource;
	bool			inUse;
};

/**
* There are only so many sounds sources that can play at once,
* once a sound source has stopped playing, it will be released
* and can be reassigned to another sound. This class handles this transparently
*/
class Sound
{
public:
	Sound();
	virtual ~Sound();

	virtual bool Load(const char* name);

	void Play();
	void Stop();
	bool IsPlaying();

	void Destroy();

	void SetRadius(float radius);
	void SetPosition(const Vector3& position);
	void SetVelocity(const Vector3& velocity);

	//this function makes a sound source relative so all direction and velocity
	//parameters become relative to the source rather than the listener
	//useful for background music that you want to stay constant relative to the listener
	//no matter where they go
	void SetSourceRelative(bool relative);

	void SetLooping(bool looping);
	void SetPitch(float pitch);
	void SetGain(float gain);

protected:
	SoundSource*	source;
	SoundBuffer*	buffer; // a pointer to the buffer, when we obtain a sound, we set it to use this buffer

	float			radius;
	Vector3			position;
	Vector3			velocity;
	bool			looping;
	float			pitch;
	float			gain;
	bool			relative;
};

/**
 * you need an instance of this class to load/close openal, then make any number of sound and play them
 */
class SoundMgr : public Siphon::Singleton<SoundMgr>
{
	friend class Sound;
public:
	SoundMgr();
	~SoundMgr();

	void SetListenerPosition(float x, float y, float z);
	void SetListenerOrientation(float fx, float fy, float fz, float ux, float uy, float uz);

	void Update();

	static void SetBasePath(const char* path);

	static Sound* Load(const char* name);
	static void Release(Sound* resource);

protected:
	SoundSource* GetSource();
	void ReleaseSource(SoundSource* source);

	SoundBuffer* GetBuffer(const char* name);

	ALCcontext* context;
	ALCdevice*	device;

	Array<SoundSource*> sources;
	Array<SoundBuffer*> buffers;

	static std::string	basePath;
};

}
#endif
