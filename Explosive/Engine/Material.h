//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_CGMATERIAL_
#define _SIPHON_CGMATERIAL_

#pragma comment(lib, "cg.lib")
#pragma comment(lib, "cgGL.lib")

#include "MaterialDefs.h"
#include "Cg/cg.h"
#include "Cg/cgGL.h"
#include "../Util/Resource.h"
#include "Math3D.h"
#include "Texture.h"
#include "Light.h"
#include "Device.h"
#include "Projector.h"

namespace Siphon
{


/**
 * Whats an effect?
 * a bunch of states and passes
 */
class Effect : public Resource
{
public:
	enum ParamID
	{
		//IT = Inverse Transpose
		ProjectionMatrix,
		WorldMatrix,
		WorldInvMatrix,
		WorldITMatrix,
		ViewMatrix,
		ViewITMatrix,
		ViewInvMatrix,
		WorldViewMatrix,
		WorldViewITMatrix,
		WorldViewInvMatrix,
		WorldViewProjectionMatrix,
		WorldViewProjectionITMatrix,

		// for projectors
		ProjectorProjectionMatrix,
		ProjectorViewMatrix,
		ProjectorViewProjectionMatrix,
		ProjectorViewProjectionITMatrix,
		ProjectorViewITMatrix,
		ProjectorViewInvMatrix,
		ProjectorWorldViewProjectionMatrix,

		ProjectorProjectionTextureMatrix,
		ProjectorViewTextureMatrix,
		ProjectorViewProjectionTextureMatrix,
		ProjectorViewProjectionITTextureMatrix,
		ProjectorViewITTextureMatrix,
		ProjectorViewInvTextureMatrix,
		ProjectorWorldViewProjectionTextureMatrix,

		// skinning
		BonesMatrix,

		EyePosition,	// these are in world space
		EyeDirection,
		LightPosition,
		AmbientColour,
		DiffuseColour,
		SpecularColour,
		SpecularPower,

		Time
	};

	Effect();
	~Effect();

	// bind parameters
	void SetMatrixParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& model, const Vector3& eyePosition);

	// bind lights
	void SetLightParameters(Light* lights[], unsigned int numLights);

	// bind projectors
	void SetProjectorParameters(const Matrix4& model, Projector* projectors[], unsigned int numProjectors);

	// bind bone matrices
	void SetSkinningParameters(const Matrix4 bones[], unsigned int numBones);

	// bind time
	void SetTimeParameter(float time);

	virtual bool Load(const char* name);

	int DoPass();

	CGparameter GetParameterByName(const char* name) const { return cgGetNamedEffectParameter(effect, name); }
	CGtype GetParameterType(CGparameter param) const { return cgGetParameterType(param); }
	CGannotation GetAnnotationByName(CGparameter param, const char* name) const { return cgGetNamedParameterAnnotation(param, name); }

	void SetInt(CGparameter param, const int& value) { cgSetParameter1i(param, value); }
	void SetTexture(CGparameter param, Texture* texture) { cgGLSetupSampler(param, texture->GetHandle()); }
	void SetFloat(CGparameter param, const float& value) { cgSetParameter1f(param, value); }
	void SetFloat2(CGparameter param, const float* value) { cgSetParameter2fv(param, value); }
	void SetVector3(CGparameter param, const Vector3& value) { cgSetParameter3fv(param, value.GetConstFloats()); }
	void SetVector4(CGparameter param, Vector4& value) { cgSetParameter4fv(param, value.GetFloats()); }
	void SetMatrix4(CGparameter param, const Matrix4& matrix);
	void SetMatrix4Array(CGparameter param, const Matrix4* matrix, int count);

	CGeffect GetEffect() const { return effect; }

	inline bool UsesLightParameters() { return light; }
	inline bool UsesTimeParameter() { return time; }
	inline bool UsesProjectorParameters() { return projector; }
	inline bool UsesSkinningParameters() { return skinning; }

	bool IsValid() { return (validTechnique == -1) ? false : true; }

	void BindParameters();
	void ClearParameters();

protected:

	// this will compile a list of parameters from the shader, that we know how to set
	void ProcessParameters();

	// these should be sorted by ParamID for binary searching.
	Array< Pair<ParamID, CGparameter> > parameters;

	int curPass;
	CGpass pass;
	CGtechnique technique;
	static CGcontext context;

	CGeffect effect;
	int validTechnique;

	// we really should just have an array for lights, and for time as they are special cases
	bool light; // should we bind lights (or should we bother finding the lights to set)?
	bool time; // this shader wants the time
	bool projector; // should we bind projector params
	bool skinning; // is this a skinning shader
};

ResourceManager(EffectMgr, Effect)

/**
 * Whats a material?
 * a combination of textures and effect/shaders
 * and is applied to a model/mesh
 */
class Material : public Resource
{
public:
	class Param
	{
	public:
		enum ParamType
		{
			ParamTexture,
			ParamFloat,
			ParamFloat2,
			ParamVector3,
			ParamVector4,
			ParamInt,
			ParamMatrix
		};

		Param(CGparameter param, ParamType type) : param(param), type(type) {}

		CGparameter param;
		ParamType	type;
	};

	class TextureParam : public Param
	{
	public:
		TextureParam(CGparameter param) : Param(param, ParamTexture), texture(0)  { }
		Texture* texture;
	};

	class FloatParam : public Param
	{
	public:
		FloatParam(CGparameter param) : Param(param, ParamFloat) { }
		float data;
	};

	class Float2Param : public Param
	{
	public:
		Float2Param(CGparameter param) : Param(param, ParamFloat2) { }
		float data[2];
	};

	class Vector3Param : public Param
	{
	public:
		Vector3Param(CGparameter param) : Param(param, ParamVector3) { }
		Vector3 data;
	};

	class Vector4Param : public Param
	{
	public:
		Vector4Param(CGparameter param) : Param(param, ParamVector4) { }
		Vector4 data;
	};

	class CPass
	{
	public:
		CPass() : format(VertexAll) { }
		~CPass() { }

		VertexFormat	format;
		Array<Param*>	parameters;
	};

	Material();

	virtual bool Load(const char* name);

	inline int DoPass() { return effect->DoPass(); }

	// this binds textures etc...
	void SetPassParameters(int passNum);

	inline void SetMatrixParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& model, const Vector3& eyePosition)
		{ effect->SetMatrixParameters(projection, view, model, eyePosition); }

	inline void SetLightParameters(Light* lights[], unsigned int numLights)
		{ effect->SetLightParameters(lights, numLights); }

	inline void SetProjectorParameters(const Matrix4& model, Projector* projectors[], unsigned int numProjectors)
		{ effect->SetProjectorParameters(model, projectors, numProjectors); }

	inline void SetSkinningParameters(const Matrix4 bones[], unsigned int numBones)
		{ effect->SetSkinningParameters(bones, numBones); }

	inline void SetTimeParameter(float time)
		{ effect->SetTimeParameter(time);}

	Effect* GetEffect() const { return effect; }

	bool operator<(Material& rhs)
	{
		return renderOrder <= rhs.renderOrder;
	}

	inline bool UsesLightParameters() { return effect->UsesLightParameters(); }
	inline bool UsesTimeParameter() { return effect->UsesTimeParameter(); }
	inline bool UsesProjectorParameters() { return effect->UsesProjectorParameters(); }
	inline bool UsesSkinningParameters() { return effect->UsesSkinningParameters(); }

	inline VertexFormat GetVertexFormat(int passNum) { return passes[passNum].format; }

	Material::Param* GetParameterByName(const char* name);

	void ClearParameters();

	static void Disable();

protected:

	bool ReadPass(File& file);
	bool ReadString(File& file, char* value);
	bool ReadVector4(File& file, Vector4& value);
	bool ReadVector3(File& file, Vector3& value);
	bool ReadFloat(File& file, float& value);
	bool ReadFloat2(File& file, float* value);
	bool ReadInt(File& file, int& value);
	bool ReadVertexFormat(File& file, VertexFormat& value);

	Effect* effect;
	Array<CPass> passes;
	int renderOrder;

	static Effect nullEffect;
};

ResourceManager(MaterialMgr, Material)

}
#endif
