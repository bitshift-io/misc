#include "Server.h"
#include "Log.h"
#include "../Network/Packet.h"

namespace Siphon
{

ServerBase::ServerBase() :
	reliable(SocketStream::Reliable),
	broadcast(SocketStream::UnReliable),
	clientCounter(2),
	numPlayers(0)
{
	clientId = 0;
}

bool ServerBase::Init(unsigned int gameId)
{
	this->gameId = gameId;
	return NetworkBase::Init();
}

void ServerBase::Shutdown()
{
	reliable.Close();
	broadcast.Close();

	for (int i = 0; i < GetNumClients(); ++i)
	{
		Connection& client = GetClientConnection(i);
		client.reliable.Close();
		client.unreliable.Close();
	}
}

bool ServerBase::Listen(int port, int flags, const char* name)
{
	this->port = port;
	//this->addr = std::string(addr);
	this->flags = flags;
	this->name = name;

	if (!stream.Bind(gNetwork.GetAnyAddress(port)))
		return false;

	if (!stream.Listen())
		return false;

	//reliable.Listen(addr, port);

	broadcast.Listen(NULL, 6454, 6455); 
//	if ((flags & NF_ReliableOnly) == 0 && (flags & NF_ConnectExternal) == 0)
//		unreliable.Listen(addr, port - 1);

	return true;
}

void ServerBase::CheckConnections()
{

	int state = stream.GetState();
	if (state & eRead)
	{
		RDPStream r;
		if (stream.Accept(r))
		{
			Log::Print("Connection accepted client\n");
			Connection connection;
			connection.stream = r;
			connection.reliableHandle = connection.stream.CreateChannel(CT_ReliableOrdered);
			connection.unreliableHandle = connection.stream.CreateChannel(CT_UnreliableOrdered);
			int idx = clients.size();
			clients.push_back(connection);

			AcceptConnection(idx, clients[idx]);
		}
	}
	
	if (state == -1)
	{
		int err = gNetwork.GetLastError();
		Log::Print("Socket error: %ld\n", err);
	}

	// check for closed connections
	for (int i = 0; i < GetNumClients(); )
	{
		Connection& connection = GetClientConnection(i);
		if (!connection.stream.IsValid())
		{
			ClientDisconnect(i, connection);
		}
		else
		{
			++i;
		}
	}


/*
	int state = reliable.GetSocketState();

	// is stream is readable, accept a new client
	if (state & SocketStream::Read)
	{
		SocketStream r(SocketStream::Reliable);
		if (reliable.Accept(r))
		{
			Log::Print("Connection accepted client\n");
			Connection connection;
			connection.reliable = r;
			int idx = clients.Insert(connection);

			AcceptConnection(idx, clients[idx]);
			//Log::Print("Connection accepted from %s:%i\n", inet_ntoa(clientRemote.sin_addr), ntohs(clientRemote.sin_port));
		}
	}

	if (state & SocketStream::Err-or)
	{
		int err;
		int errlen = sizeof(err);
		getsockopt(reliable.GetSocket(), SOL_SOCKET, SO_ERROR,
			(char*)&err, &errlen);

		Log::Print("Socket error: %ld\n", err);
	}*/

/*
	// check for any closed connections
	for (int i = 0; i < GetNumClients(); ++i)
	{
		Connection& client = GetClientConnection(i);
		if (!client.reliable.IsValid())
		{
			ClientDisconnect(i, client);
		}
		//client.unreliable.Close();
	}*/
}

bool ServerBase::Send(const void* data, int size, SocketStream::Type type)
{
	for (int i = 0; i < GetNumClients(); ++i)
	{
		if (Send(i, data, size, type) == SOCKET_ERROR)
			Log::Print("Failed at Send: %ld\n", WSAGetLastError());
	}

	return true;
}

bool ServerBase::Send(int index, const void* data, int size, SocketStream::Type type)
{
	Connection& client = GetClientConnection(index);

	//switch (type)
	{
	//case SocketStream::Reliable:
		{
			Packet p;
			p.Append(data, size);
			return client.stream.Send(&p, type == SocketStream::Reliable ? client.reliableHandle : client.unreliableHandle);
			//return client.reliable.Send(data, size);
		}
	//case SocketStream::UnReliable:
	//	return client.unreliable.Send(data, size);
	}

	return false;
}

int ServerBase::Receive(int index, void* data, unsigned int size, SocketStream::Type type)
{
	Connection& client = GetClientConnection(index);

	//switch (type)
	{
	//case SocketStream::Reliable:
		{
			Packet p;
			client.stream.Receive(&p, -1, type == SocketStream::Reliable ? client.reliableHandle : client.unreliableHandle);
			int actualSize = min(size, p.GetSize());
			memcpy(data, p.GetBuffer(), actualSize);
			return actualSize;
			//return client.reliable.Receive(data, size);
		}
	//case SocketStream::UnReliable:
	//	return client.unreliable.Receive(data, size);
	}

	return 0;
}

void ServerBase::AcceptConnection(int index, Connection& client)
{
	// send the client a clientId
	client.clientId = clientCounter;
	Send(index, (const char*)&client.clientId, sizeof(unsigned int));
	clientCounter += 2; // inrecment by 2, just cuz UDP uses 2 ports
/*
	// open up a UDP stream on port + clientid
	if ((flags & NF_ReliableOnly) == 0)
	{
		client.reliable.ConvertToString(client.reliable.GetAddress(), addr);
		client.unreliable.Listen(addr.c_str(), port + client.clientId, port + client.clientId + 1);
		Log::Print("Opening UDP port for %s, on port %i, for client %i\n", addr.c_str(), port + client.clientId, client.clientId);
	}*/
}

void ServerBase::ClientDisconnect(int index, Connection& client)
{
	vector<Connection>::iterator it = clients.begin();
	for (int i = 0; i < index; ++i)
		++it;

	it->stream.Close();
	clients.erase(it);
/*
	clients[index].stream.Close();
	clients[index] = clients[clients.size() - 1];
	clients.resize(clients.size() - 1);*/
}

int ServerBase::GetBufferData(int index, void* data, int size, SocketStream::Type type)
{
	Connection& client = GetClientConnection(index);

	switch (type)
	{
	case SocketStream::Reliable:
		return client.reliable.GetBufferData(data, size);
	case SocketStream::UnReliable:
		return client.unreliable.GetBufferData(data, size);
	}

	return 0;
}

int ServerBase::PeekBufferData(int index, void* data, int size, SocketStream::Type type)
{
	Connection& client = GetClientConnection(index);

	switch (type)
	{
	case SocketStream::Reliable:
		return client.reliable.PeekBufferData(data, size);
	case SocketStream::UnReliable:
		return client.unreliable.PeekBufferData(data, size);
	}

	return 0;
}

void ServerBase::Update()
{
	CheckBroadcast();

	for (int i = 0; i < GetNumClients(); ++i)
	{
		Connection& client = GetClientConnection(i);

		if (client.reliable.Update() == SOCKET_ERROR)
			Log::Print("Failed at client.reliable.Update: %ld\n", WSAGetLastError());

		if (client.unreliable.Update() == SOCKET_ERROR)
			Log::Print("Failed at client.unreliable.Update: %ld\n", WSAGetLastError());
	}
}

void ServerBase::CheckBroadcast()
{
	broadcast.Update();

	int bufferSize = broadcast.GetBufferSize();
	if (bufferSize >= sizeof(BroadcastMsg))
	{
		BroadcastMsg msg;
		broadcast.GetBufferData(&msg, sizeof(BroadcastMsg));

		// if broadcast message meant for us....
		if (msg.id == gameId)
		{
			// send around my ip
			msg.numPlayers = numPlayers;
			msg.addr = broadcast.LookupAddress(addr.c_str());
			memcpy(msg.name, name.c_str(), min(name.length() + 1, 256));

			for (int i = 0; i < 10; ++i)
			{
				bool sent = broadcast.Send(&msg, sizeof(BroadcastMsg) + name.length() + 1);
				int nothing = 0;
			}
		}
	}
}

unsigned long ServerBase::GetLocalAddress()
{
	return reliable.GetLocalAddress();
}

}