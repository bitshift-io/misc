//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "PostEffect.h"

namespace Siphon
{

MatQuad::MatQuad(Material* material) : PostEffect(), material(material)
{
}

void MatQuad::PostRender()
{/*
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	Device::GetInstance().SetOrtho();
	glLoadIdentity();

	unsigned int passes = material->Begin();
	for (unsigned int i = 0; i < passes; ++i)
	{
		material->Pass(i);

		glDisable(GL_CULL_FACE);

		glBegin(GL_QUADS);
		{
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f,1.0f, 0.5f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.5f);

			glTexCoord2f(0.0f,0.0f);
			glVertex3f(-1.0f, -1.0f, 0.5f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 0.5f);
		}
		glEnd();
	}

	material->End();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();*/
}

Bloom::Bloom() : PostEffect(), mipClose(4), mipFar(8), blendRatio(0.7f)
{/*
	// full size texture
	glGenTextures(1, &screenTexture);
	glBindTexture(GL_TEXTURE_2D, screenTexture);

	int width, height;
	Device::GetInstance().GetDeviceSize(width, height);

	// get next power of 2 larger than screen res.
	textureSize = 256;
	while (textureSize < width)
		textureSize *= 2;

	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureSize, textureSize, 0,
				 GL_RGB, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); */
}

Bloom::~Bloom()
{
	// release texture
}

void Bloom::SetLevels(int mipClose, int mipFar)
{
	this->mipClose = mipClose;
	this->mipFar = mipFar;
}

void Bloom::SetBlendRatio(float blendRatio)
{
	this->blendRatio = blendRatio;
}

void Bloom::PostRender()
{/*
	// clear accum buf
	glClear(GL_ACCUM_BUFFER_BIT);

	glActiveTextureARB(GL_TEXTURE0_ARB);
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, screenTexture);

	int width, height;
	Device::GetInstance().GetDeviceSize(width, height);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, width, height);

	// then render it back
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	Device::GetInstance().SetOrtho();
	glLoadIdentity();

	glAccum(GL_ACCUM, blendRatio);

	float ratioX = float(textureSize) / float(width);
	float ratioY = float(textureSize) / float(height);

	float x = ratioX * 2.0f - 1.0f;
	float y = ratioY * 2.0f - 1.0f;

	float remainderRatio = (1.0f - blendRatio) / (mipFar - mipClose);

	for (int i = mipClose; i < mipFar; ++i)
	{
		glTexEnvi(GL_TEXTURE_FILTER_CONTROL_EXT, GL_TEXTURE_LOD_BIAS_EXT, i);
		glClear(GL_DEPTH_BUFFER_BIT);

		glBegin(GL_QUADS);
		{
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(x, y, 0.5f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(x, -1.0f, 0.5f);

			glTexCoord2f(0.0f,0.0f);
			glVertex3f(-1.0f, -1.0f, 0.5f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, y, 0.5f);
		}
		glEnd();

		glAccum(GL_ACCUM, remainderRatio); //(1.0f / 2.0f) / 3.0f
	}

	glTexEnvi(GL_TEXTURE_FILTER_CONTROL_EXT, GL_TEXTURE_LOD_BIAS_EXT, 0);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glAccum(GL_RETURN, 1.0f);*/
}

//-------------------------------------------------------------------------

Fog::Fog() :
	mode(FogExp2),
	density(0.02f),
	start(0.98f),
	end(1.0f),
	colour(0.2f,0.2f,0.21f)
{

}

void Fog::PreRender()
{
	glEnable(GL_FOG);

	switch (mode)
	{
	case FogLinear:
		glFogi(GL_FOG_MODE, GL_LINEAR);
		break;
	case FogExp:
		glFogi(GL_FOG_MODE, GL_EXP);
		break;
	case FogExp2:
		glFogi(GL_FOG_MODE, GL_EXP2);
		break;
	}

	glFogf(GL_FOG_DENSITY, density);
	glFogf(GL_FOG_START, start);
	glFogf(GL_FOG_END, end);
	glFogfv(GL_FOG_COLOR, colour.GetPointer());
}
void Fog::PostRender()
{
	glDisable(GL_FOG);
}

//-------------------------------------------------------------------------
/*
DepthOfField::DepthOfField() : PostEffect()
{
	// full size texture
	glGenTextures(1, &screenTexture);
	glBindTexture(GL_TEXTURE_2D, screenTexture);

	int width, height;
	Engine::GetInstance()->GetDevice()->GetDeviceSize(width, height);

	// get next power of 2 larger than screen res.
	textureSize = 256;
	while (textureSize < width)
		textureSize *= 2;

	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureSize, textureSize, 0,
				 GL_RGB, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	// set up depth texture
	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, textureSize, textureSize, 0,
					GL_DEPTH_COMPONENT, GL_FLOAT, NULL); //GL_UNSIGNED_BYTE

	// we have no mipmaps
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	dof.Load("depthofview.vert", "depthofview.frag");

	paramNear = dof.GetParameterByName("near");
	paramFar = dof.GetParameterByName("far");
}

DepthOfField::~DepthOfField()
{
	// release texture
}

void DepthOfField::PostRender()
{
	// the idea here:
	// create a blurred image of the scene,
	// and use a shader to use z-depth to determine blending

	// clear accum buf
	glClear(GL_ACCUM_BUFFER_BIT);

	glActiveTextureARB(GL_TEXTURE0_ARB);
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, screenTexture);

	int width, height;
	Engine::GetInstance()->GetDevice()->GetDeviceSize(width, height);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, width, height);

	//copy to depth
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, width, height);

	glBindTexture(GL_TEXTURE_2D, screenTexture);

	// then render it back
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	Engine::GetInstance()->GetDevice()->SetOrtho();
	glLoadIdentity();

	float ratioX = float(textureSize) / float(width);
	float ratioY = float(textureSize) / float(height);

	float x = ratioX * 2.0f - 1.0f;
	float y = ratioY * 2.0f - 1.0f;

	for (int i = 0; i < 8; ++i)
	{
		glTexEnvi(GL_TEXTURE_FILTER_CONTROL_EXT, GL_TEXTURE_LOD_BIAS_EXT, i);
		glClear(GL_DEPTH_BUFFER_BIT);

		glBegin(GL_QUADS);
		{
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(x, y, 0.5f);

			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(x, -1.0f, 0.5f);

			glTexCoord2f(0.0f,0.0f);
			glVertex3f(-1.0f, -1.0f, 0.5f);

			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, y, 0.5f);
		}
		glEnd();

		glAccum(GL_ACCUM, 0.125f);
	}

	glTexEnvi(GL_TEXTURE_FILTER_CONTROL_EXT, GL_TEXTURE_LOD_BIAS_EXT, 0);


	glAccum(GL_RETURN, 1.0f);

	glActiveTextureARB(GL_TEXTURE1_ARB);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, depthTexture);

	// at this stage we have our screen blurred...
	// but in our level 0 mipmap we have the screen texture!
	// so we can use this to restore

	dof.Bind();
	dof.SetParameter(paramNear->handle, 0.1f);
	dof.SetParameter(paramFar->handle, 1.0f);

	//glDisable(GL_BLEND);
	glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);
	//glAlphaFunc(GL_GREATER, 0.0f);

	glBegin(GL_QUADS);
	{
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(x, y, 0.5f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(x, -1.0f, 0.5f);

		glTexCoord2f(0.0f,0.0f);
		glVertex3f(-1.0f, -1.0f, 0.5f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-1.0f, y, 0.5f);
	}
	glEnd();

	// restore origonal states
	glDisable(GL_BLEND);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	dof.Disable();

	glActiveTextureARB(GL_TEXTURE1_ARB);
	glDisable(GL_TEXTURE_2D);

	glActiveTextureARB(GL_TEXTURE0_ARB);
	glDisable(GL_TEXTURE_2D);
}
*/
}
