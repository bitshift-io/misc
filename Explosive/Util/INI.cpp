#include "INI.h"
#include "Log.h"
#include "File.h"

namespace Siphon
{

bool INIFile::Valid()
{
	return data.GetSize();
}

bool INIFile::Save(const char* path)
{
	this->path = std::string(path);

	File file(path, "wt");
	if (!file.Valid())
	{
		Log::Print("Failed to open ini file %s\n", path);
		return false;
	}

	for (int i = 0; i < data.GetSize(); ++i)
	{
		Pair<std::string, Array< Pair<std::string, std::string> > >& section = data[i];
		file.WriteString("[%s]\n", section.first.c_str());

		// write out keys
		for (int k = 0; k < section.second.GetSize(); ++k)
		{
			Pair<std::string, std::string>& key = section.second[k];
			file.WriteString("%s = %s\n", key.first.c_str(), key.second.c_str());
		}

		file.WriteString("\n");
	}

	return true;
}

bool INIFile::Load(const char* path)
{
	this->path = std::string(path);

	File file(path, "r");
	if (!file.Valid())
	{
		Log::Print("Failed to open ini file %s\n", path);
		return false;
	}

	//read in data
	char strSection[255] = {0};
	char strKey[255] = {0};
	char strValue[255] = {0};
	char strTemp[255] = {0};
	int idx = 0;

	while (!file.EndOfFile())
	{
		strTemp[0] = '\0';
		file.GetStr(strTemp, 255);

		// crop of any end of line terminators
		int i = strlen(strTemp);
		while (strTemp[i] == '\n' || strTemp[i] == '\r' || strTemp[i] == '\0')
			--i;
		++i;
		if (i < strlen(strTemp))
			strTemp[i] = '\0';

		if (strTemp[0] == '[')
		{
			idx = 0;
			strcpy(strSection, (strTemp + 1));

			int i = strlen(strSection);
			while (strSection[i] != ']' && i > 0)
				--i;

			strSection[i] = '\0';
		}
		else if (strTemp[0] != '\0' && strTemp[0] != '\n')
		{
			const char* equals = strchr(strTemp, '=');

			//int count = sscanf(strTemp, "%s = %s", &strKey, &strValue);

			// if there is no equals sign, then we set the key as a number
			// and the value is the line from the ini
			if (equals == 0)
			{
				strcpy(strValue, strTemp);
				sprintf(strKey, "%i", idx);
				++idx;

				SetValue(strSection, strKey, strValue);
				strValue[0] = '\0';
				continue;
			}

			int equalsPos = equals - strTemp;

			strcpy(strKey, strTemp);
			strKey[equalsPos - 1] = '\0';
			strcpy(strValue, strTemp + equalsPos + 2);

			SetValue(strSection, strKey, strValue);
			strValue[0] = '\0';
		}
	}

	return true;
}

Array< Pair<std::string, std::string> >* INIFile::GetSection(const char* section)
{
	for (unsigned int i = 0; i < data.GetSize(); ++i)
	{
		Pair<std::string, Array<Pair<std::string, std::string> > >& pair = data[i];
		if (pair.first.compare(section) == 0)
		{
			return &(pair.second);
		}
	}

	return NULL;
}

std::string INIFile::GetValue(const char* section, const char* key )
{
	for (unsigned int i = 0; i < data.GetSize(); ++i)
	{
		Pair<std::string, Array<Pair<std::string, std::string> > >& pair = data[i];
		if (pair.first.compare(section) == 0)
		{
			for (unsigned int k = 0; k < pair.second.GetSize(); ++k)
			{
				Pair<std::string, std::string>& innerPair = pair.second[k];
				if (innerPair.first.compare(key) == 0)
				{
					return innerPair.second;
				}
			}

			return std::string("");
		}
	}

	return std::string("");
}

void INIFile::SetValue(const char* section, const char* key, const char* value)
{
	for (unsigned int i = 0; i < data.GetSize(); ++i)
	{
		Pair<std::string, Array<Pair<std::string, std::string> > >& pair = data[i];
		if (pair.first.compare(section) == 0)
		{
			for (unsigned int k = 0; k < pair.second.GetSize(); ++k)
			{
				Pair<std::string, std::string>& innerPair = pair.second[k];
				if (innerPair.first.compare(key) == 0)
				{
					innerPair.second = value;
					return;
				}
			}

			// add the key
			pair.second.Insert(Pair<std::string, std::string>(key, value));
			return;
		}
	}

	Pair<std::string, Array<Pair<std::string, std::string> > > pair;
	pair.first = section;
	pair.second.Insert(Pair<std::string, std::string>(key, value));
	data.Insert(pair);
}

void INIFile::SetValue(const char* section, const char* key, float value)
{
	char buffer[256];
	sprintf(buffer, "%f", value);
	SetValue(section, key, buffer);
}

void INIFile::SetValue(const char* section, const char* key, int value)
{
	char buffer[256];
	sprintf(buffer, "%i", value);
	SetValue(section, key, buffer);
}

void INIFile::SetValue(const char* section, const char* key, bool value)
{
	char buffer[256];
	sprintf(buffer, "%i", value);
	SetValue(section, key, buffer);
}

/*

void INIFile::GetValue( std::string section, std::string key, std::string& value )
{
	value = std::string( data[section][key] );
}

void INIFile::GetValue( std::string section, std::string key, float& value  )
{
	sscanf( data[section][key].c_str(), "%f", &value);
}
*/
bool INIFile::GetValue(const char* section,const char* key, int& value  )
{
	std::string val = GetValue(section, key);
	value = atoi(val.c_str());
	return true;
}

bool INIFile::GetValue(const char* section, const char* key, bool& value)
{
	std::string val = GetValue(section, key);
	if (val == "true" || val == "TRUE" || val == "1")
		value = true;
	else
		value = false;

	return true;
}
/*
void INIFile::GetValue( std::string section, std::string key, bool& value  )
{
	sscanf( data[section][key].c_str(), "%b", &value);
}*/
/*
std::map<std::string, std::string>& INIFile::GetSection(const char* section)
{
	return data[section];
}*/

};
