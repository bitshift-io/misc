#ifndef _SIPHON_RESOURCE_
#define _SIPHON_RESOURCE_

#include <string>
#include "Array.h"

namespace Siphon
{

class Resource
{
public:
	virtual bool		Load(const char* name) = 0;
	virtual Resource*	Clone() { return this; }
	const char*			GetName() const { return name.c_str(); }
	virtual void		RemoveFromMemory() { }

protected:
	std::string name;
};

// should the macro, register with this somehow so we can autoclean up?
template<class T>
class ResourceMgr
{
public:

	static int GetIndex(const char* name, bool loadIfNotExist = true)
	{
		std::string path = basePath + std::string(name);
		for (unsigned int i = 0; i < resources.GetSize(); ++i)
		{
			T* res = resources[i];
			if (strcmp(res->GetName(), path.c_str()) == 0)
				return i;
		}

		// if not loaded
		if (loadIfNotExist)
		{
			// load it, if we loaded it, return its index, but dont load it again!
			if (Load(name))
				return GetIndex(name, false);
		}

		return -1;
	}

	static T* GetResourceClone(int index)
	{
		T* res = resources[index];
		return (T*)res->Clone();
	}

	static T* Load(const char* name)
	{
		std::string path = basePath + std::string(name);
		for (unsigned int i = 0; i < resources.GetSize(); ++i)
		{
			T* res = resources[i];
			if (strcmp(res->GetName(), path.c_str()) == 0)
				return (T*)res->Clone();
		}

		T* res = new T();
		if (!res->Load(path.c_str()))
		{
			delete res;
			return 0;
		}

		resources.Insert(res);
		return res;
	}

	static void SetBasePath(const char* path)
	{
		basePath = std::string(path);
	}

	static void Release(T* resource, bool removeFromMem = false)
	{
		if (!resource)
			return;

		if (resources.Remove(resource) != -1)
		{
			// this gives us a chance to release anything that belongs to the first instance
			if (removeFromMem)
				resource->RemoveFromMemory();

			delete resource;
		}
	}

protected:
	static Array<T*>	resources;
	static std::string	basePath;
};

template<class T>
Array<T*> ResourceMgr<T>::resources;

template<class T>
std::string ResourceMgr<T>::basePath("data/");

#ifdef WIN32
    #define ResourceManager(mgrName, resName) \
    class mgrName : public ResourceMgr<resName> { }; \
    Array<resName*> mgrName::resources;
#else
    #define ResourceManager(mgrName, resName) \
    class mgrName : public ResourceMgr<resName> { };
#endif

}
#endif
