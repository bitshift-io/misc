#include <stdio.h>
#include <stdarg.h>
#include <stdarg.h>
#include <memory.h>
#include <malloc.h>

#include "File.h"

namespace Siphon
{

//-----------------------------------------------------------------

#ifdef WIN32
int vfscanf(FILE* file, const char *format, va_list argPtr)
{
	// http://www.codeguru.com/Cpp/Cpp/string/comments.php/c5631/?thread=61724

	// Get an upper bound for the # of args
	size_t count = 0;
	const char* p = format;

	while(1)
	{
		char c = *(p++);
		if (c == 0)
			break;

		if (c == '%' && (p[0] != '*' && p[0] != '%'))
			++count;
	}

	if (count <= 0)
		return 0;

	int result;

	// copy stack pointer
	_asm
	{
		mov esi, esp;
	}

	// push variable parameters pointers on stack
	for (int i = count - 1; i >= 0; --i)
	{
		_asm
		{
			mov eax, dword ptr[i];
			mov ecx, dword ptr [argPtr];
			mov edx, dword ptr [ecx+eax*4];
			push edx;
		}
	}

	int stackAdvance = (2 + count) * 4;

	_asm
	{
		// now push on the fixed params
		mov eax, dword ptr [format];
		push eax;
		mov eax, dword ptr [file];
		push eax;

		// call fscanf, and more the result in to result
		call dword ptr [fscanf];
		mov result, eax;

		// restore stack pointer
		mov eax, dword ptr[stackAdvance];
		add esp, eax;
		//mov esp, esi;
	}
	return result;
}
#endif

//-------------------------------------------------------------------

File::File() : file(0)//, buffer(1024)
{
}

File::File(const char* file, const char* access)
{
	Open(file, access);
}

File::~File()
{
	Close();
}

void File::Close()
{
	if (file)
	{
		fclose(file);
		file = 0;
	}
}

bool File::Open(const char* file, const char* access)
{
	if (!file)
		return false;

	name = std::string(file);
	this->file = fopen(file, access);
	return this->file ? true : false;
}

bool File::EndOfFile()
{
	return feof(file) ? true : false;
}

bool File::Valid()
{
	return file ? true : false;
}

unsigned int File::Write(const void* data, unsigned int size)
{
	return fwrite(data, size, 1, file);
}

unsigned int File::Read(void* data, unsigned int size)
{
	return fread(data, 1, size, file);
}

unsigned int File::WriteString(const char* str, ...)
{
	va_list ap;
	char newText[1024];
  	va_start(ap, str);
  	unsigned int count = vsprintf(newText, str, ap);
  	va_end(ap);

	Write(newText, strlen(newText));

	return count;
}

unsigned int File::ReadString(const char* text, ...)
{
 	if( text == 0 )
    	return 0;

	va_list ap;
  	va_start(ap, text);
	int result = vfscanf(file, text, ap);
  	va_end(ap);

	return result;
}

int File::Seek(long offset, int type)
{
	return fseek(file, offset, type);
}

int File::GetPosition()
{
	return ftell(file);
}

char* File::GetStr(char* buffer, unsigned int size)
{
	return fgets(buffer, size, file);
}

int	File::Size()
{
	int curPos = GetPosition();
	Seek(0, SEEK_END);
	int size = GetPosition();
	Seek(curPos, SEEK_SET);
	return size;
}

//--------------------------------------------------------------------------------------
// Chunk functions below
//--------------------------------------------------------------------------------------

void Chunk::WriteHead(File& file, unsigned int id)
{
	file.Write(&id, sizeof(unsigned int));
	headOffset = file.GetPosition();
	file.Write(&headOffset, sizeof(int));
}

void Chunk::WriteFoot(File& file)
{
	int end = file.GetPosition();
	int size = end - headOffset;

	file.Seek(headOffset, SEEK_SET);
	file.Write(&headOffset, sizeof(int));
	file.Seek(end, SEEK_SET);
}

void Chunk::ReadHeader(File& file)
{
	file.Read(&id, sizeof(unsigned int));
	file.Read(&size, sizeof(int));
}

void Chunk::ReadFooter(File& file)
{

}

unsigned int Chunk::GetID()
{
	return id;
}

int Chunk::GetSize()
{
	return size;
}

}
