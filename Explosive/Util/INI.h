#ifndef _SIPHON_INI_
#define _SIPHON_INI_

#include <string>

#include "Array.h"

namespace Siphon
{

class INIFile
{
public:
	bool Load(const char* path);
	bool Save(const char* path);

	bool Valid();

	//std::map<std::string, std::string>& GetSection(const char* section);

	Array< Pair<std::string, std::string> >* GetSection(const char* section);

	std::string GetValue(const char* section, const char* key);

	//overloaded stuffs
	/*
	void GetValue( std::string section, std::string key, std::string& value );
	void GetValue( std::string section, std::string key, float& value  );*/
	bool GetValue(const char* section, const char* key, int& value);
	bool GetValue(const char* section, const char* key, bool& value);

	void SetValue(const char* section, const char* key, const char* value);
	void SetValue(const char* section, const char* key, float value);
	void SetValue(const char* section, const char* key, int value);
	void SetValue(const char* section, const char* key, bool value);

protected:
	Array< Pair<std::string, Array< Pair<std::string, std::string> > > > data;
	std::string path;
};

};

#endif
