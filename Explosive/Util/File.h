#ifndef _SIPHON_FILE_
#define _SIPHON_FILE_

#include <stdio.h>
#include <string>

namespace Siphon
{

class File
{
public:
	File();
	File(const char* file, const char* access);
	~File();

	bool Open(const char* file, const char* access);
	void Close();
	bool EndOfFile();

	bool Valid();

	unsigned int Write(const void* data, unsigned int size);
	unsigned int Read(void* data, unsigned int size);

	unsigned int WriteString(const char* str, ...);
	unsigned int ReadString(const char* str, ...);

	int Seek(long offset, int type);
	int GetPosition();

	int	Size();

	const char* GetName() { return name.c_str(); }

	// fgets
	char* GetStr(char* buffer, unsigned int size);

protected:
//	Array<unsigned char> buffer;
	FILE*	file;
	std::string name;
};

class Chunk
{
public:
	void WriteHead(File& file, unsigned int id);
	void WriteFoot(File& file);

	void ReadHeader(File& file);
	void ReadFooter(File& file);

	unsigned int GetID();
	int GetSize();
protected:
	int headOffset;
	int size;
	unsigned int id;
};

}
#endif
