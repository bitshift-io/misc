#ifndef _SIPHON_LIST_
#define _SIPHON_LIST_

namespace Siphon
{

template<class T>
class List
{
public:
	List() : pNext(0)
	{
	}

	void InsertFront(T* const obj)
	{
		obj->pNext = pNext;
		pNext = obj;
	}

	T*	pNext;
};

}
#endif
