#ifndef _SIPHON_SORT_
#define _SIPHON_SORT_

#include "Array.h"

namespace Siphon
{

template <class T>
class Sorter
{
public:
	typedef bool(*Compare)(T& x, T& y);

	void Sort(Array<T>& array, Compare cmpFn)
	{
		n = array.GetSize();

		if (n <= 0)
			return;

		//unsigned int temp = array.
		DoSort(array, cmpFn);
	}

protected:
	unsigned int n;

	void Swap(T& x, T& y)
	{
		T temp = x;
		x = y;
		y = temp;
	}

	virtual void DoSort(Array<T>& array, Compare cmpFn) = 0;
};

// http://www.brpreiss.com/books/opus4/html/page491.html
// inplace binary sorting
template <class T>
class BinarySort : public Sorter<T>
{
protected:
	void DoSort(Array<T>& array, Compare cmpFn)
	{
		for (unsigned int i = 1; i < n; ++i)
		{
			T& temp = array[i];
			unsigned int left = 0;
			unsigned int right = i;

			while (left < right)
			{
				unsigned int const middle = (left + right) / 2;

				if (cmpFn(temp, array[middle]))
					left = middle + 1;
				else
					right = middle;
			}

			for (unsigned int j = i; j > left; --j)
				Swap(array[j - 1], array[j]);
		}
	}
};

}

#endif