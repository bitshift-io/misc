#ifndef _SIPHON_ARRAY_
#define _SIPHON_ARRAY_

#include "File.h"

namespace Siphon
{

template<class T, class U>
class Pair
{
public:
	Pair()
	{
	}

	Pair(const T& f, const U& s) : first(f), second(s)
	{
	}

	~Pair()
	{
	}

	T first;
	U second;
};

template<class T>
class Array
{
public:
	Array(unsigned int capacity = 2) : size(0), array(0), capacity(0)
	{
		SetCapacity(capacity);
	}

	Array(const Array<T>& copy)
	{
		operator=(copy);
	}

	void operator=(const Array<T>& rhs)
	{
		if (array)
		{
			delete[] array;
			//free(array);
			array = 0;
			capacity = 0;
			size = 0;
		}

		SetCapacity(rhs.capacity);

		//capacity = rhs.capacity;
		//array = (T*)malloc(sizeof(T) * capacity);
		//memcpy(array, rhs.array, size * sizeof(T));

		for (int i = 0; i < rhs.size; ++i)
			Insert(rhs[i]);

		size = rhs.size;
	}

	~Array()
	{
		if (array)
		{
			delete[] array;
			//free(array);
			array = 0;
		}
	}

	inline bool SetCapacity(unsigned int capacity)
	{
		if (capacity <= this->capacity)
			return false;

		this->capacity = capacity;

		T* temp = array;
		//array = (T*)malloc(sizeof(T) * capacity);
		array = new T[capacity];
		if (!array)
			return false;

		// zero memory
		//#ifdef DEBUG
			//memset(newArray, 0, sizeof(T) * capacity);
		//	memset(array, 0, sizeof(T) * capacity);
		//#endif

		if (array)
		{
			for (int i = 0; i < size; ++i)
			{
				//array[i] = temp[i];
				new(&array[i]) T(temp[i]);
				//new(&array[index]) T(array[size - 1]);
				//operator[](i) = array[i];
			}
		}
		//	memcpy(newArray, array, sizeof(T) * this->size);

		//array = newArray;

		if (temp)
		{
			delete[] temp;
			//free(temp);
		}

		return true;
	}

	inline unsigned int GetSize() const
	{
		return size;
	}

	inline void SetSize(unsigned int size)
	{
		if (size > capacity)
			SetCapacity(size);

		this->size = size;
	}

	inline unsigned int GetCapacity() const
	{
		return capacity;
	}

	inline T& operator[](unsigned int index) const
	{
		return array[index];
	}

	inline T& First() const
	{
		return array[0];
	}

	inline T& Last() const
	{
		return array[size - 1];
	}

	inline T* GetData() const
	{
		return array;
	}

	/*
	 * This will save out the size of the array to file
	 * and dump the buffer to file
	 * NOTE: only use this on simple types:
	 * eg. int arrays or vector arrays, on more complex data structures
	 * you will have to manually iterate over items to save!
	 */
	bool Save(File& file)
	{
		file.Write(&size, sizeof(unsigned int));
		file.Write(array, sizeof(T) * size);
		return true;
	}

	bool Load(File& file)
	{
		unsigned int size;
		file.Read(&size, sizeof(unsigned int));
		SetSize(size);
		file.Read(array, sizeof(T) * size);
		return true;
	}

	inline unsigned int Insert(const T& object)
	{
		if (size >= capacity)
			SetCapacity(capacity > 0 ? capacity * 2 : 2);

		//memcpy(&array[size], &object, sizeof(T));
		//array[size] = object;
		new(&array[size]) T(object);

		++size;
		return (size - 1);
	}
/*
	// this is slow, it shifts elements fowards
	inline unsigned int Insert(const T& object, int idx)
	{
		if (idx == size)
			return Insert(object);

		if (size + 1 >= capacity)
			SetCapacity(capacity * 2);

		for (int i = size + 1; i > idx; --i)
			array[i] = array[i-1];

		array[idx] = object;
		++size;

		return (size - 1);
	}
*/
	inline int Remove(const T& object)
	{
		for (int i = 0; i < GetSize(); ++i)
		{
			if (object == array[i])
			{
				Remove(i);
				return i;
			}
		}

		return -1;
	}

	inline void Remove(int index)
	{
		// move the last element to this element if array is
		// big enough, then reduce the size
		if (size >= 2 && index != (size - 1))
		{
			array[index] = array[size - 1];

			// for debugging
			memset(&array[size - 1], 0, sizeof(T)); 
			/*
			array[index].~T();
			new(&array[index]) T(array[size - 1]);
			array[size - 1].~T();
			//array[index] = array[size - 1];*/
		}

		--size;
	}

	inline void RemoveShift(int index)
	{
		// removes the element and shifts all down
		if (size <= 0)
			return;
		
		for (int i = index; i < size - 1; ++i)
		{
			array[i] = array[i + 1];
		}

		--size;
	}

//protected:
	T* array;
	unsigned int size;
	unsigned int capacity;
};

}
#endif
