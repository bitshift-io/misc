#define SAFE_DELETE(p){ if(p){delete p; p = 0;} }
#define SAFE_DELETE_ARRAY(p){ if(p){delete[] p; p = 0;} }
#define SUPER_CLASS(type) typedef Super type
