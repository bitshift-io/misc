#ifndef _SIPHON_SINGLETON_
#define _SIPHON_SINGLETON_

namespace Siphon
{

template<class T>
class Singleton
{
public:
	static void Create() { m_Instance = new T(); }
	static void Destroy() { if (IsValid()) { delete m_Instance; m_Instance = 0; } }

	static void SetInstance(T* instance) { m_Instance = instance;}

	static inline T& GetInstance() { return *m_Instance; }

	static bool IsValid() { return m_Instance ? true : false; }
//protected:

	static T* m_Instance;
};

template<class T>
T* Singleton<T>::m_Instance = 0;

//Engine::Render()
//#define SingletonInterface(t) { #define t { t::GetInstance() } }
//#define SingletonInterface(t) { typedef t::GetInstance() t }
//#define SingletonInterface(t) { typedef t::GetInstance() t:: }

}
#endif
