#ifndef _SIPHON_QUEUE_
#define _SIPHON_QUEUE_

#include "File.h"

namespace Siphon
{

template<class T>
class Queue
{
public:
	
	Queue(unsigned int capacity = 2) : size(0), queue(0), capacity(0), head(0), tail(0)
	{
		SetCapacity(capacity);
	}

	Queue(const Queue<T>& copy)
	{
		operator=(copy);
	}

	void operator=(const Queue<T>& rhs)
	{
		if (queue)
		{
			delete[] queue;
			queue = 0;
			size = 0;
			capacity = 0;
		}

		SetCapacity(rhs.capacity);

		for (int i = 0; i < rhs.size; ++i)
			Insert(rhs[i]);

		//capacity = copy.capacity;
		head = rhs.head;
		tail = rhs.tail;
		size = rhs.size;
		//queue = (T*)malloc(sizeof(T) * capacity);
		//memcpy(queue, copy.queue, size * sizeof(T));
	}

	~Queue()
	{
		if (queue)
		{
			//free(queue);
			delete[] queue;
			queue = 0;
		}
	}

	inline bool SetCapacity(unsigned int capacity)
	{
		if (capacity <= this->capacity)
			return false;

		this->capacity = capacity;

		T* newQueue = new T[capacity]; //(T*)malloc(sizeof(T) * capacity);
		if (!newQueue)
			return false;

		// zero memory
		//#ifdef DEBUG
		//	memset(newQueue, 0, sizeof(T) * capacity);
		//#endif

		if (queue)
		{
			Unwrap(newQueue);
		}

		queue = newQueue;
		return true;
	}

	//
	// Unwrap the queue
	//
	void Unwrap(T* newQueue = 0)
	{
		if (size <= 0)
			return;

		if (newQueue == 0)
			newQueue = new T[capacity]; //(T*)malloc(sizeof(T) * capacity);

		if (head > tail)
		{
			for (int i = 0, j = tail; i < size; ++i, ++j)
				new(&newQueue[i]) T(queue[j]);
			//memcpy(newQueue, queue + tail, sizeof(T) * size);
		}
		else 
		{
			int tailToSize = size - tail;
			for (int i = 0, j = tail; i < tailToSize; ++i, ++j)
				new(&newQueue[i]) T(queue[j]);

			//memcpy(newQueue, queue + tail, sizeof(T) * tailToSize);

			if (tailToSize != size)
			{
				for (int i = tailToSize, j = 0; j < head; ++i, ++j)
					new(&newQueue[i]) T(queue[j]);
				//memcpy(newQueue + tailToSize, queue, sizeof(T) * head);
			}
		}

		delete[] queue; //free(queue);
		queue = newQueue;

		tail = 0;
		head = size;		
	}

	inline unsigned int GetSize() const
	{
		return size;
	}

	inline void SetSize(unsigned int size)
	{
		if (size > capacity)
			SetCapacity(size);
	}

	inline unsigned int GetCapacity() const
	{
		return capacity;
	}
/*
	inline T& operator[](unsigned int index) const
	{
		return queue[index];
	}

	inline T& First() const
	{
		return queue[0];
	}

	inline T& Last() const
	{
		return queue[size - 1];
	}

	inline T& Head() const
	{
		int h = head - 1;
		if (h < 0)
			h = size;

		return queue[head];
	}
*/
	inline T& Tail() const
	{
		return queue[tail];
	}

	inline T* GetData() const
	{
		return queue;
	}

	//
	// This will save out the size of the array to file
	// and dump the buffer to file
	// NOTE: only use this on simple types:
	// eg. int arrays or vector arrays, on more complex data structures
	// you will have to manually iterate over items to save!
	//
	bool Save(File& file)
	{
		Unwrap();
		file.Write(&size, sizeof(unsigned int));
		file.Write(queue, sizeof(T) * size);
		return true;
	}

	bool Load(File& file)
	{
		unsigned int size;
		file.Read(&size, sizeof(unsigned int));
		SetSize(size);
		file.Read(queue, sizeof(T) * size);
		tail = 0;
		head = size;
		return true;
	}

	inline unsigned int Insert(const T& object)
	{
		if (size >= capacity)
			SetCapacity(capacity > 0 ? capacity * 2 : 2);

		//memcpy(&array[size], &object, sizeof(T));
		//array[size] = object;
		new(&queue[head]) T(object);

		int ret = head;

		++size;
		++head;
		if (head >= capacity)
			head = 0;

		return ret;
	}
/*
	// this is slow, it shifts elements fowards
	inline unsigned int Insert(const T& object, int idx)
	{
		if (idx == size)
			return Insert(object);

		if (size + 1 >= capacity)
			SetCapacity(capacity * 2);

		for (int i = size + 1; i > idx; --i)
			queue[i] = queue[i-1];

		queue[idx] = object;
		++size;

		return (size - 1);
	}

	inline int Remove(const T& object)
	{
		for (int i = 0; i < GetSize(); ++i)
		{
			if (object == queue[i])
			{
				Remove(i);
				return i;
			}
		}

		return -1;
	}

	inline void Remove(int index)
	{
		// slow! moves everything foward, back one
		if (tail < head)
		{
			for (int i = 0; i < head; ++i)
			{
				queue[i] = queue[i + 1];
			}
		}
		else
		{
			for (int i = 0; i < capacity; ++i)
			{
				queue[i] = queue[i + 1];
			}

			for (int i = 0; i < head; ++i)
			{
				queue[i] = queue[i + 1];
			}
		}
	}
*/
	inline void Remove()
	{
		if (size <= 0)
			return;

		queue[tail].~T();

		++tail;
		if (tail >= capacity)
			tail = 0;

		--size;
	}

protected:
	T* queue;
	unsigned int head;
	unsigned int tail;
	unsigned int size;
	unsigned int capacity;
};

}
#endif
