// metadatatest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>

using namespace std;

class MetaClassBase
{
public:
	MetaClassBase(const char* name, const char* code)
	{
		mName = name;
		mCode = code;
	}

	virtual void* Create() = 0;
	virtual void  Destroy(void* instance) = 0;

	const char* mName;
	const char* mCode;
};

template<class T>
class MetaClass : public MetaClassBase
{
public:
	MetaClass(const char* name, const char* code) : MetaClassBase(name, code)
	{
	}

	virtual void* Create()
	{
		return new T();
	}

	virtual void Destroy(void* instance)
	{
		delete instance;
	}
};

// META_BEGIN/END blocks cannot be nested
// there can only be one class/struct in a META_BEGIN/END block
// inheritance !!!

#define META_BEGIN(c, p) p MetaClass<c> s##c(#c, #p);
#define META_END
#define PERSIST	// a marker used by meta data to find variables to persist

#define ENUM(c) c,	// commas are bad! so we have to wrap enums :-/


META_BEGIN(MyStructure,

class MyStructure
{
	friend class MetaData;
public:

	enum MyEnum
	{
		ENUM(Test = 1 << 0)
		ENUM(Test2 = 1 << 1)
	};
	
	virtual void SetNothing(int nothing = 0)
	{
		nothing = 0;
	}

	PERSIST MyEnum		enm;

protected:
	PERSIST int			nothing;
	PERSIST vector<int> someArray;

	void* dontPersisteMe;
};

)META_END


int _tmain(int argc, _TCHAR* argv[])
{
	MyStructure myStruct;
	myStruct.enm = MyStructure::Test;
	printf(sMyStructure.mCode);

	return 0;
}

