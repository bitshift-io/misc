#include "../Multithread/pch.h"
#include "../Memory/Memory.h"
#include "../Util/Log.h"
#include "../Util/Queue.h"
#include "../Util/Array.h"
#include <string>

int main(int argc, void* args[])
{
	Array<std::string> array2;

	{
		Array<std::string> array1;
		array1.Insert(std::string("temp1"));
		array1.Insert(std::string("temp2"));
		array1.Insert(std::string("temp3"));

		array2 = array1;
	}


	return 0;
}