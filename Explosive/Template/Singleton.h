#ifndef _SINGLETON_H_H
#define _SINGLETON_H_H

#include "Destructor.h"


//
// always good to a #define gMySingleton MySingleton::GetInstance()
// for ease of use
//
template<class T>
class SingletonBC
{
public:
	static void Create() 
	{ 
		SetInstance(new T());
	}

	static void Destroy() 
	{ 
		if (IsValid()) 
		{ 
			delete mInstance; 
			mInstance = 0; 
		} 
	}

	static void SetInstance(T* instance) 
	{ 
		mInstance = instance; 
		mDestructor.SetFnPtr(SingletonBC<T>::Destroy); // TODO
	}

	static bool IsValid() 
	{ 
		return mInstance ? true : false; 
	}

	static inline T& GetInstance() 
	{ 
		if (!IsValid())
			Create();

		return *mInstance; 
	}

protected:

	static FunctionDestructor	mDestructor;
	static T*					mInstance;
};

template<class T>
T* SingletonBC<T>::mInstance = 0;

template<class T>
FunctionDestructor SingletonBC<T>::mDestructor;


#endif
