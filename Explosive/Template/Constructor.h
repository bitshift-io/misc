#ifndef _CONSTRUCTOR_H_
#define _CONSTRUCTOR_H_

//
// Creates an object when this function is called
//
typedef void* (*FnCreate)();

template <class T>
void* Creator()
{
	return new T();
}

#endif