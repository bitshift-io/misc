#ifndef _DESTRUCTOR_H_
#define _DESTRUCTOR_H_

//
// Cleans up an object when this class goes out of scope
//
class ObjectDestructor
{
public:

	ObjectDestructor(void* obj) : mPtr(obj)
	{

	}

	~ObjectDestructor()
	{
		delete mPtr;
	}

protected:

	void* mPtr;
};

//
// Calls a function when this object goes out of scope
//
class FunctionDestructor
{
public:

	typedef void (*FnDestructor)();

	~FunctionDestructor()
	{
		if (mPtr)
			mPtr();
	}

	void SetFnPtr(FnDestructor ptr)
	{
		mPtr = ptr;
	}

protected:

	FnDestructor mPtr;
};

#endif