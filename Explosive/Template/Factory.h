#ifndef _FACTORY_H_
#define _FACTORY_H_

#include "Singleton.h"

#define REGISTER_FACTORYOBJECT(c, f)	\
c::GetInstance().Register(c::mInstance);


template<class T>
class FactoryObject
{
public:
	virtual T* Create();

protected:
	static T mInstance;
};

template<class T>
class Factory : public StackSingleton<T>
{
public:
	

protected:
	void Register(FactoryObject* object);

	vector<FactoryObject*>	mObject;
};

#endif