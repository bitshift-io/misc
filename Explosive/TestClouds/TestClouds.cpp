#include "../Core/GameBase.h"

#include "../Engine/Mesh.h"
#include "../Engine/Device.h"
#include "../Engine/Font.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "../Engine/Renderer.h"
#include "../Engine/Time.h"
#include "../Engine/Material.h"
#include "../Engine/Window.h"

#include "../Util/Log.h"
#include "../Util/Macros.h"

#include "Clouds.h"

using namespace Siphon;

class TestClouds : public GameBase<TestClouds>
{
public:
	bool Init(int argc, char **argv)
	{
		GameBase::Init(argc, argv);
		SetBasePath("data/");
	
		Device::GetInstance().CreateDevice();
		
		window = new Window();
		window->Create("TestClouds", 800, 600, 16, false);
		window->ShowWindow();
		Device::GetInstance().SetActiveWindow(window);
		Device::GetInstance().SetClearColour(0.0, 0.0, 0.5);

		Input::GetInstance().CreateInput(window);
		Input::GetInstance().SetAltTabStyleMouse(true);
		
		camera = new Camera();
		camera->SetLookAt(Vector3(0, 0.001, -0.5), Vector3(0, 0, 0));
		camera->SetSpeed(0.05f);
		camera->SetPerspective(80, 1.33, 100.0f, 0.0001f);

		lightTexture = new RenderTexture();
		lightTexture->Create();
		lightRenderer.SetRenderTarget(lightTexture);

		renderer.SetCamera(camera);
		lightRenderer.SetCamera(camera);

		camera->Update();
		clouds.Load(camera);
		renderer.InsertMesh(clouds.GetMesh());
		lightRenderer.InsertMesh(clouds.GetMesh());

		font = FontMgr::Load("arial.fnt");

		Device::GetInstance().CheckErrors();
		Log::Print("Loading complete.\n");
		return true;
	}

	virtual void Shutdown()
	{
		SAFE_DELETE(camera);
		SAFE_DELETE(window);
	}

	virtual bool UpdateGame()
	{
		Input::GetInstance().Update();

		window->HandleMessages();
		camera->Update();

		clouds.Update(camera);

		renderer.SetTime(Time::GetInstance().GetTimeSeconds());

		if (Input::GetInstance().KeyPressed(DIK_ESCAPE))
			return false;

		return true;
	}

	virtual void UpdatePhysics()
	{

	}

	virtual void Render()
	{
		Device::GetInstance().BeginRender();

		renderer.Render();
		lightRenderer.Render();

		camera->LookAt();
		lightTexture->GetColourTexture()->Draw();
		//clouds.Debug();

		unsigned int fps = Device::GetInstance().GetFPS();
		font->Update();
		font->Print(0.1f, 0.1f, 0.5f, "FPS: %i", fps);
		font->Render();

		Device::GetInstance().EndRender();
	}

protected:
	Window*			window;
	Font*			font;
	Camera*			camera;
	Mesh*			scene;
	Renderer		renderer;
	Renderer		lightRenderer;
	RenderTexture*	lightTexture;
	Clouds			clouds;
};

MAIN(TestClouds);
