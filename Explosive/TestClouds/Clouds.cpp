#include "../Util/Array.h"
#include "../Util/Sort.h"
#include "Clouds.h"

void Clouds::Load(Camera* camera)
{
	mRenderMesh = Mesh::LoadMesh("clouds.MDL");

	VertexBuffer* vBuff = mRenderMesh->GetMeshMat().vertexBuffer;

	// generate a unique list of vertices
	Array<Vector3> positions;
	positions.SetCapacity(vBuff->positions.GetSize());
	for (int i = 0; i < vBuff->positions.GetSize(); ++i)
	{
		int j = 0;
		for (; j < positions.GetSize(); ++j)
		{
			if (positions[j] == vBuff->positions[i])
				break;
		}

		if (j >= positions.GetSize())
			positions.Insert(vBuff->positions[i]);
	}

	// clean up the old buffer and make a new one for particles
	delete vBuff;
	vBuff = new VertexBuffer();
	vBuff->SetFlags(Dynamic);
	mRenderMesh->GetMeshMat().vertexBuffer = vBuff;

	// create particles
	int size = positions.GetSize();
	mParticles.SetCapacity(size);

	vBuff->positions.SetSize(size * 4);
	memset(vBuff->positions.GetData(), 0, sizeof(Vector3) * size * 4);

	mSortedCamera.SetSize(size);
	memset(mSortedCamera.GetData(), 0, sizeof(Particle*) * size);

	mSortedLight.SetSize(size);
	memset(mSortedLight.GetData(), 0, sizeof(Particle*) * size);

	vBuff->uvCoords.SetCapacity(size * 4);

	vBuff->indices.SetCapacity(size * 6);
	for (int i = 0; i < size; ++i)
	{
		Particle p;
		p.mPosition = positions[i];
		p.mSize = 0.5f;
		mParticles.Insert(p);

		int pIdx = i * 4;

		vBuff->uvCoords.Insert(UVCoord(0.0f, 0.0f));
		vBuff->uvCoords.Insert(UVCoord(0.0f, 1.0f));
		vBuff->uvCoords.Insert(UVCoord(1.0f, 1.0f));
		vBuff->uvCoords.Insert(UVCoord(1.0f, 0.0f));

		vBuff->indices.Insert(pIdx);
		vBuff->indices.Insert(pIdx + 1);
		vBuff->indices.Insert(pIdx + 2);

		vBuff->indices.Insert(pIdx);
		vBuff->indices.Insert(pIdx + 2);
		vBuff->indices.Insert(pIdx + 3);
	}

	// fix the mesh mats
	mRenderMesh->GetMeshMat().material[0].numIndices = size * 6;
	mRenderMesh->GetMeshMat().material[0].numPrimitives = size * 2;
	mRenderMesh->GetMeshMat().material[0].startIndex = 0;

	Update(camera);
	//vBuff->Build();
}

void Clouds::Debug()
{
	mRenderMesh->GetBoundSphere().Draw();
}

void Clouds::Update(Camera* camera)
{
	//
	// sort from dist to camera and light
	//

	const Vector3& position = camera->GetPosition();
	for (int i = 0; i < mParticles.GetSize(); ++i)
	{
		const Vector3& pos = mParticles[i].mPosition;
		mParticles[i].mDistToCam = (position - pos).MagnitudeSquared();

		mSortedCamera[i] = &mParticles[i];
	}

	BinarySort<Particle*> sorter;
	sorter.Sort(mSortedCamera, Particle::CompareCam);

	//
	// update render buffer
	//

	const Vector3& up = camera->GetUp();
	const Vector3& right = camera->GetRight();

	VertexBuffer* vBuff = mRenderMesh->GetMeshMat().vertexBuffer;
	for (int i = 0; i < mSortedCamera.GetSize(); ++i)
	{
		int pIdx = i * 4;
		float size = mSortedCamera[i]->mSize;
		const Vector3& pos = mSortedCamera[i]->mPosition;

		Vector3 localUp = up * size;
		Vector3 localRight = right * size;

		vBuff->positions[pIdx + 0] = pos + localUp - localRight;
		vBuff->positions[pIdx + 1] = pos - localUp - localRight;
		vBuff->positions[pIdx + 2] = pos - localUp + localRight;
		vBuff->positions[pIdx + 3] = pos + localUp + localRight;
	}
}