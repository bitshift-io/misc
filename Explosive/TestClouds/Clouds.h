#include "../Engine/Renderer.h"

using namespace Siphon;

class Particle
{
public:

	Vector3 mPosition;
	float	mSize;
	float	mDistToCam;
	float	mDistToLight;

	static bool CompareCam(Particle*& x, Particle*& y)
	{
		return x->mDistToCam < y->mDistToCam;
	}
};

class Clouds
{
public:
	void Load(Camera* camera);
	void Update(Camera* camera);
	void Debug();

	Mesh* GetMesh() { return mRenderMesh; }

protected:
	Array<Particle>		mParticles;
	Array<Particle*>	mSortedLight;
	Array<Particle*>	mSortedCamera;
	Mesh*				mRenderMesh;
};