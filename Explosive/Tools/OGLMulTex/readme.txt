

OGLMulTex - An interactive illustration for learning and testing
            Rage128 multitexture support in OpenGL using the
            ARB_Multitexture and EXT_texture_env_combine extensions


     OGLMulTex is designed to help OpenGL application developers
understand the multiple texture support provided by ARB_multitexture
and EXT_texture_env_combine on the Rage128.  Due to their extreme
flexibility, these extensions can initially appear complex and
confusing.  OGLMulTex lays out the Rage128's abstracted texture
blending units graphically in order to allow developers to
experiment with the units' functionality and see rendered results
interactively.
 
     The OGLMulTex interface consists of three windows: A window containing
an interactive illustration of the OpenGL abstracted texture blending
units, a window showing the OpenGL code necessary to put the blending
units in the corresponding states (close by default), and a window which
shows rendered results (if the two extensions are supported).  This last window,
the output window, shows the RGB and A channels separately.  The texture environment
states shown in the Interactive Illustration Window can be modified by context-sensitive
pop-ups and the Code and output windows will be updated accordingly.

  How to use:  Clicking on any of the light gray boxes in the main window
    will bring up a pop-up menu of appropriate settings.  These settings are
    hardcoded to match the Rage128's capabilities.  (Any code generated with
    OGLMulTex will be hardware accelerated by the Rage128.) Through these pop-ups
    texture environment states can be set and abstract "connections" from
    relevant sources will be drawn.  The user can also open TGA files to use as
    textures in the app.  Several sample TGAs are included.  When an
    interesting set of render states is found, the code used to select
    those render states can be copied from the Code Window and pasted
    into a developer's own application.


Bug Reports and Feedback
------------------------

Send bugs and feedback to MulTex@atitech.com

