Visual Studio.Net 2003 Solution Cleanup
---------------------------------------

This utility cleans up Solution directories, for all solutions created with
Visual Studio.Net 2003.

It can:

- Remove links to source code control (Visual SourceSafe, SourceGear Vault, ...)
- Remove bin, obj, Debug, Release directories
- Remove *.user files (which can cause file reference problems)
- Clear the Visual Studio.Net Web Cache (which can also cause Web project compilation problems)

Simply copy the program to your hard disk and run it. Check the options you want
and click the "Clean up" button. Make sure that Visual Studio.Net is closed and that
all files and directories you wish to delete are not read-only. If necessary, use
right-click/Properties on the directory containing your solution to turn read-only mode
off.

This utility is freeware; it is provided at no cost and with no guarantees
whatsoever. Use at your own risk! Only for Visual Studio.Net 2003 solutions. This
utility does not remove source code control links from Visual Studio.Net 2002 or
Visual Studio 2005 projects.

Written by Roy Dictus (roy.dictus@gmail.com); see also my blog on .Net development at
http://www.dotnetjunkies.com/weblog/roydictus. Feedback is always welcome!


This utility may be included in CD-ROM utility compilations if I get a copy of the CD. Please
send CDs to:

Roy Dictus
James Ensorlaan 2/B
B-2630 Aartselaar
Belgium

Many thanks and have fun with Cleanup!


TROUBLESHOOTING
---------------
- If you get a PolicyException when running the utility, very probably you're trying
to run it from a mounted volume -- i.e., from a non-local disk. Copy the utility
to your disk to use it.