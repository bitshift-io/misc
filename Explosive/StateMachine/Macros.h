#ifndef _MACROS_H_
#define _MACROS_H_

// should this be part of the metadata class?
#define DECLARE_CLASS(c)	\
	virtual const char* GetTypeName() { return "##c"; }

#endif