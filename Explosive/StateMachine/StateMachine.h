#ifndef _STATEMACHINE_H_
#define _STATEMACHINE_H_



class StateMachine
{
public:
	void SetState();

protected:
	Vector<State>	mState;
};

#endif