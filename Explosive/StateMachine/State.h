#ifndef _STATE_H_
#define _STATE_H_

#include <vector>
#include "../Metadata/PrecompiledHeader.h"
#include "Action.h"

using namespace std;

class State
{
public:
	//METADATA_DECLARE(State)
	DECLARE_CLASS(State);

	virtual void Enter(State* previous);
	virtual void Update();
	virtual void Exit();

protected:
	vector<Action*>	mAction;
};
/*
METADATA_CLASS_START(State)
	METADATA_CLASS_VARIABLE_STL(vector, Action*, mAction)
METADATA_CLASS_END(State)
*/
#endif