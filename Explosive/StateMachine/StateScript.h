#ifndef _STATESCRIPT_H_
#define _STATESCRIPT_H_

//
// Loads a state machine + states + actions
// prime candidate for metadata?!
// if it were in a script format
//
// state Fire
// {
//		animation = Fire
//
//		action Timmer
//		{
//			state = Idle
//			time = 10.0f
//		}
//
//		action ButtonPress
//		{
//			state = Reload
//		}
// }
// 
// state Reload
// {
//		animation = Reload
//
//		action AnimationEnd
//		{
//			state = Idle
//		}
// }
//
class StateScript
{
public:

protected:

	Vector<State>	mStates;
};


#endif