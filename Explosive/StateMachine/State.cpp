#include "State.h"

void State::Enter(State* previous)
{
	vector<Action*>::iterator itAction;
	for (itAction = mAction.begin(); itAction != mAction.end(); ++itAction)
		(*itAction)->Reset();
}

void State::Update()
{
	vector<Action*>::iterator itAction;
	for (itAction = mAction.begin(); itAction != mAction.end(); ++itAction)
		(*itAction)->Update();
}

void State::Exit()
{

}