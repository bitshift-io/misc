#ifndef _ACTION_H_
#define _ACTION_H_

#include "../Metadata/PrecompiledHeader.h"
#include "Macros.h"

// 
// Action triggers a state change
//
class Action
{
public:
	virtual void Reset() = 0;
	virtual unsigned int Update() = 0;
	//virtual const char* GetName() = 0;

protected:
	unsigned int mState;
};
/*
//
// Button press
//
class ButtonPressAction : public Action
{
public:
	bool DoAction();
};

//
// Animation end
//
class AnimationEndAction : public Action
{
public:
};
*/

METADATA_CLASS_EXTERN(TimmerAction)

//
// A timmer, when it reaches zero, returns the next state
//
class TimmerAction : public Action
{
public:
	METADATA_DECLARE(TimmerAction)
	DECLARE_CLASS(TimmerAction)

	virtual void Reset();
	unsigned unsigned int Update();

//protected:
	float	mTime;
	float	mInitialTime;
};




#endif