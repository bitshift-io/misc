#include "Action.h"

METADATA_CLASS_START(TimmerAction)
	METADATA_CLASS_VARIABLE(TimmerAction, float, mTime)
	METADATA_CLASS_VARIABLE(TimmerAction, float, mInitialTime)
METADATA_CLASS_END(TimmerAction)

void TimmerAction::Reset()
{
	mTime = mInitialTime;
}