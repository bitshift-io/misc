#ifndef _SIPHON_MASTERSERVER_
#define _SIPHON_MASTERSERVER_

#include "Array.h"
#include "Client.h"
#include <string>
#include "RDP/RDPStream.h"
#include "Thread.h"

namespace Siphon
{

class MasterServer
{
public:
	MasterServer();
	~MasterServer();

	bool Connect(const char* serverURL, const char* masterServerPath, int port/* = 80*/, int gameId);
	void AddServer(const char* name, int numPlayers);
	void RemoveServer();
	void GetServerList(Array<ServerInfo>& servers);
	void RequestJoin(const char* ip);

	void StartNATPunchThroughCheck(RDPStream* stream, int clientPort, unsigned long updateRate);
	void StopNATPunchThroughCheck();

protected:

	static unsigned long UpdateNATPunchThrough(void* data, Thread* thread);

	int ReadLine(const char* buffer, char* line);
	bool ReadServerInfo(const char* line, ServerInfo& info);
	bool ReadClientInfo(const char* line, ClientInfo& info);
	bool IsIPAddress(const char* buffer);

	std::string masterServerPath;
	std::string serverURL;

	int gameId;
	int port;
	int gamePort;
	SocketStream	socket;

	Thread thread;
	RDPStream* stream;
	unsigned long updateRate;
	int clientPort;

	bool	enabled;

	Mutex mutex;
};

}

#endif