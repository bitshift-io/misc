#include "MasterServer.h"
#include "Win32Time.h"
#include "Singleton.h"
#include "RDP/RDPStreamMgr.h"

namespace Siphon
{

MasterServer::MasterServer() : socket(SocketStream::Reliable), enabled(true)
{

}

MasterServer::~MasterServer()
{
	StopNATPunchThroughCheck();
}

bool MasterServer::Connect(const char* serverURL, const char* masterServerPath, int port, int gameId)
{
	this->serverURL = serverURL;
	this->masterServerPath = masterServerPath;
	this->port = port;
	this->gameId = gameId;

	return true;
}

void MasterServer::StartNATPunchThroughCheck(RDPStream* stream, int clientPort, unsigned long updateRate)
{
	this->updateRate = updateRate;
	this->clientPort = clientPort;
	this->stream = stream;

	thread.Start(&Task(UpdateNATPunchThrough, this));
}

void MasterServer::StopNATPunchThroughCheck()
{
	thread.Stop();
	thread.Wait();
}

unsigned long MasterServer::UpdateNATPunchThrough(void* data, Thread* thread)
{
	MasterServer* ms = static_cast<MasterServer*>(data);

	if (!ms->enabled)
		return 0;

	while (thread->IsRunning())
	{
		Sleep(ms->updateRate);

		ms->mutex.Lock();

		// get requested clients and attempt nat punch through
		ms->enabled = ms->socket.Connect(ms->serverURL.c_str(), ms->port);
		if (!ms->enabled)
		{
			Log::Print("Failed to connect to master server\n");
		}
		else
		{
			char httpGetRequest[256];
			sprintf(httpGetRequest, "GET http://%s/%s/client.php?operation=get HTTP/1.0\n\n", ms->serverURL.c_str(), ms->masterServerPath.c_str());
			bool send = ms->socket.Send(httpGetRequest, strlen(httpGetRequest));

			char line[256];
			char buffer[2048];
			memset(buffer, 0, 2048);

			// read the first chunk so we can strip
			// off the header
			int bytes = 0;
			int lineSize = 0;

			while (bytes = ms->socket.Receive(buffer, 2048))
			{
				char* bufferPtr = buffer;
				while (lineSize = ms->ReadLine(bufferPtr, line))
				{
					bufferPtr += lineSize;

					ClientInfo info;
					if (ms->ReadClientInfo(line, info))
					{
						SocketAddr sockAddr = gNetwork.GetAddress(info.ip.c_str(), ms->clientPort);
						if (!gRDPStreamMgr.GetRDPStream(sockAddr))
						{
							bool connecting = ms->stream->Connect(sockAddr, false);
							cout << "Attempting to punch through NAT for connecting client: " << info.ip.c_str() << endl;
						}
					}
				}
			}

			ms->socket.Close();
		}

		ms->mutex.Unlock();
	}

	return 0;
}

void MasterServer::RequestJoin(const char* ip)
{
	if (!enabled)
		return;

	mutex.Lock();

	enabled = socket.Connect(serverURL.c_str(), port);
	if (!enabled)
	{
		Log::Print("Failed to connect to master server\n");
	}
	else
	{
		char httpGetRequest[256];
		sprintf(httpGetRequest, "GET http://%s/%s/client.php?operation=join&ip=%s HTTP/1.0\n\n", serverURL.c_str(), masterServerPath.c_str(), ip);
		bool send = socket.Send(httpGetRequest, strlen(httpGetRequest));

		// flush buffer
		char buffer[2048];
		int bytes = 0;
		while (bytes = socket.Receive(buffer, 2048))
		{
		}

		if (bytes == -1)
			Log::Print("Failed to contact master server\n");

		socket.Close();
	}

	mutex.Unlock();
}

void MasterServer::AddServer(const char* name, int numPlayers)
{
	if (!enabled)
		return;

	mutex.Lock();

	enabled = socket.Connect(serverURL.c_str(), port);
	if (!enabled)
	{
		Log::Print("Failed to connect to master server\n");
	}
	else
	{
		char formattedName[256];
		int j = 0;
		for (int i = 0; i < strlen(name); ++i)
		{
			if (name[i] == ' ')
			{
				memcpy(formattedName + j, "%20", sizeof(char) * 3);
				j += 3;
			}
			else
			{
				formattedName[j] = name[i];
				++j;
			}
		}
		formattedName[j] = '\0';


		char httpGetRequest[256];
		sprintf(httpGetRequest, "GET http://%s/%s/server.php?operation=add&name=%s&players=%i&gameId=%i HTTP/1.0\n\n", serverURL.c_str(), masterServerPath.c_str(), formattedName, numPlayers, gameId);
		bool send = socket.Send(httpGetRequest, strlen(httpGetRequest));

		// flush buffer
		char buffer[2048];
		while (socket.Receive(buffer, 2048))
		{
		}

		socket.Close();
	}

	mutex.Unlock();
}

void MasterServer::RemoveServer()
{
	if (!enabled)
		return;

	mutex.Lock();

	enabled = socket.Connect(serverURL.c_str(), port);
	if (!enabled)
	{
		Log::Print("Failed to connect to master server\n");
	}
	else
	{
		char httpGetRequest[256];
		sprintf(httpGetRequest, "GET http://%s/%s/server.php?operation=remove HTTP/1.0\n\n", serverURL.c_str(), masterServerPath.c_str());
		bool send = socket.Send(httpGetRequest, strlen(httpGetRequest));

		// flush buffer
		char buffer[2048];
		while (socket.Receive(buffer, 2048))
		{
		}

		socket.Close();
	}

	mutex.Unlock();
}

void MasterServer::GetServerList(Array<ServerInfo>& servers)
{
	if (!enabled)
		return;

	mutex.Lock();

	enabled = socket.Connect(serverURL.c_str(), port);
	if (!enabled)
	{
		Log::Print("Failed to connect to master server\n");
	}
	else
	{
		char httpGetRequest[256];
		sprintf(httpGetRequest, "GET http://%s/%s/server.php?operation=get HTTP/1.0\n\n", serverURL.c_str(), masterServerPath.c_str());
		bool send = socket.Send(httpGetRequest, strlen(httpGetRequest));

		char line[256];
		char buffer[2048];
		memset(buffer, 0, 2048);

		// read the first chunk so we can strip
		// off the header
		int bytes = 0;
		int lineSize = 0;

		while (bytes = socket.Receive(buffer, 2048))
		{
			char* bufferPtr = buffer;
			while (lineSize = ReadLine(bufferPtr, line))
			{
				bufferPtr += lineSize;

				ServerInfo info;
				if (ReadServerInfo(line, info))
					servers.Insert(info);
			}
		}

		socket.Close();
	}

	mutex.Unlock();
}

bool MasterServer::ReadServerInfo(const char* line, ServerInfo& info)
{
	char ip[256];
	char name[256];
	int	players;
	int gameId = -1;

	if (sscanf(line, "%s %i %i", &ip, &players, &gameId) != 3)
		return false;

	if (gameId != this->gameId)
		return false;

	int begin = false;
	int j = 0;
	for (int i = 0; i < strlen(line); ++i)
	{
		if (begin && line[i] == '\"')
			break;

		if (begin)
		{
			name[j] = line[i];
			++j;
		}

		if (line[i] == '\"')
			begin = true;
	}
	name[j] = '\0';

	if (!begin)
		return false;

	if (!IsIPAddress(ip))
		return false;

	info.ip = ip;
	info.name = name;
	info.numPlayers = players;
	return true;
}

bool MasterServer::ReadClientInfo(const char* line, ClientInfo& info)
{
	char ip[256];

	if (sscanf(line, "%s", &ip) != 1)
		return false;

	if (!IsIPAddress(ip))
		return false;

	info.ip = ip;
	return true;
}

bool MasterServer::IsIPAddress(const char* buffer)
{
	int ip[4];
	int count = sscanf(buffer, "%i.%i.%i.%i", &ip[0], &ip[1], &ip[2], &ip[3]);
	if (count == 4)
	{
		for (int i = 0; i < 4; ++i)
		{
			if (ip[i] < 0 || ip[i] > 255)
				return false;
		}

		return true;
	}

	return false;
}

int MasterServer::ReadLine(const char* buffer, char* line)
{
	line[0] = '\0';
	int len = strlen(buffer);
	if (len <= 0)
		return 0;

	// strip any new line chars from the start
	int start = 0;
	while ((buffer[start] == '\n' || buffer[start] == '\r') && start < len)
		++start;

	int i = 0;
	while (buffer[start + i] != '\n' && buffer[start + i] != '\r' && (start + i) < len)
	{
		line[i] = buffer[start + i];
		++i;
	}

	line[i] = '\0';
	return (start + i);
}

}