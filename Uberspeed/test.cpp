#include "Engine/SiphonGl.h"

#include "hud.h"
#include "car.h"
#include "checkpoint.h"
#include "racingworld.h"


//#include "SDL.h"
//#include <SDL/SDL_thread.h>

//Camera camera;
SkyBox sky;
SFont fnt;
Model newModel; 

//Client client;
//Server server;
FileMgr fileMgr;
Car myCar;

int fps;
unsigned int t;
int fps_last;

unsigned int tick = 0;
unsigned int ttnt = 0;
unsigned int ttnpt = 0;

bool bServer = false;

Vector3 positions[500];
bool done = false;
bool lockRender = false;

int polys = 0;

#define PHYSICS_UPDATE 10
#define GAME_UPDATE		20
#define NETWORK_UPDATE	10000

Sound fsnd;

class Chat : public NetObj//, virtual public Obj
{
public:
	Chat()
	{
		//notify obj manager
		if( ObjMgr::RegisterObject( this ) )
		{
			//notify client...
			Client::GetInstance().RegisterNetObject( this );
		}

		printf("CREATING A NEW CHAT\n");
		bFocus = false;
	};

	virtual void Update( unsigned long tick )
	{
		if( ownerId == -1 ) //we are owner
		{

			std::string temp = Input::GetInstance().GetPressedKey(true);

			if( !bFocus && temp == "t" )
			{
				bFocus = true;
				return;
			}
			else if( !bFocus )
				return;
			
			if( temp == "\n" )
			{
				printf("chat: sending %s\n", newMsg.c_str() );
				Send( (void*)newMsg.c_str(), strlen(newMsg.c_str()) + 1 );

				newMsg = "";
				bFocus = false;
			}
			else
				newMsg += temp;

			fnt.Print( 10, 100, "%s", newMsg.c_str() );
		}
		fnt.Print( 10, 150, "%s", last.c_str() );
	}

	bool Recive( void* message, int length )
	{
		printf("chat: packet recived: %s\n", message);
		last = std::string( (char*)message );
		//fnt.DrawText(10, 100, "%s", (char*)message );
		return true;
	}


	bool Send( void* message, int length )
	{
		Client::GetInstance().SendReliable( message, length, this, TO_ALL );
		return true;
	};
/*
	bool Recive( void* message, int length )
	{/*
		printf("I HAVE RECIVED A MESSAGE :)\n");
		last = std::string( (char*)message );
		//exit(0);* /
		return true;
	};*/

	virtual void AnotherFn()
	{
		printf("Another fn called from Chat\n");
	}
/*
	Obj* Create()
	{
		return new Chat;
	}
*/
	virtual char* GetClassId()
	{
		printf("returning 'chat' as classid\n");
		return "chat";
	}

	virtual bool SharedNetObj()
	{
		return true;
	}
//protected:
	//static ObjReg<Chat> regReg; //("chat");
	std::string last;
	std::string newMsg;
	bool bFocus;
};

//Temp<Chat> empty;
//ObjReg<Chat> regReg("chat");
ObjReg<Car> regCar("car");

//Chat myChat;



void DrawGLScene()
{
	//printf("new frame\n");
	polys = 0;
	static float f = 0;

	GLDevice::GetInstance().BeginRender();

	//printf("hello?\m");
	glLoadIdentity(); // Reset the view
	RenderMgr::GetInstance().GetCamera().LookAt();

	sky.Render( RenderMgr::GetInstance().GetCamera() );
	polys += World::GetInstance().Render( RenderMgr::GetInstance().GetCamera() );


 /* 

  for( int i = 0; i < 100; ++i )
  {
	  newModel.SetPosition( positions[i][X], positions[i][Y], positions[i][Z]);
	  polys += newModel.Render( camera );
  }
*/
//	polys += myCar.Render( camera );

	RenderMgr::GetInstance().Render();



    if( SDL_GetTicks() > t )
	{
		fps_last = fps;
		fps = 0;
		t = SDL_GetTicks() + 1000;
		//exit(0);
	}
	++fps;

	polys += ShadowMgr::GetInstance().Render( RenderMgr::GetInstance().GetCamera() );

	fnt.Render();

	GLDevice::GetInstance().EndRender();
}

int WorldUpdate( void* info )
{
	long time = SDL_GetTicks();
	long timeLast = time;
/*
	long physDeltaTime;
	long gameDeltaTime;

	long physStep;
	long gameStep;

	long lastPhysTime = time;
	long lastGameTime = time;

	long physReminder = 0;
	long gameRemainder = 0;
*/
	long testTime= SDL_GetTicks() + 1000; //1 seconds
	long testTicks = 0;

	long startTime = SDL_GetTicks();
	long deltaTime = 0;

	long curPhysSteps = 0;
	long completePhysSteps = 0;

	long curGameSteps = 0;
	long completeGameSteps = 0;

	long curNetworkSteps = 0;
	long completeNetworkSteps = 0;

	while( !done )
	{
		time = SDL_GetTicks();

		deltaTime = time - startTime;
		curPhysSteps = deltaTime / PHYSICS_UPDATE;
		curGameSteps = deltaTime / GAME_UPDATE;
		curNetworkSteps = deltaTime / NETWORK_UPDATE;
	
		/*
		if( time > testTime )
		{
			done = true;
			printf("number of ticks: %u\n", completePhysSteps );
		}*/

		//bool bRun = false;
		for( int n = 0; n < curNetworkSteps - completeNetworkSteps; ++n )
		{
			
			  //server needs to update faster
			if( bServer )
				Server::GetInstance().Update(0);

			//else
			//for( int i =0 ; i < 2; ++i )
			Client::GetInstance().Update(0);
		}
		completeNetworkSteps = curNetworkSteps;

		for( int p = 0; p < curPhysSteps - completePhysSteps; ++p )
		{
		//	bRun = true;			
			
				PhysicsMgr::GetInstance().Update(0);
			//myCar.Update(0);

			
		}
		completePhysSteps = curPhysSteps;


		//printf("$phys remainder: %i, steps: %i deltaT: %i\n", physReminder,  physStep, physDeltaTime);

		for( int g = 0; g < curGameSteps - completeGameSteps; ++g )
		{
			//lockRender = true;
			SDL_ActiveEvent actEvent;
    		SDL_Event event;
    		while ( SDL_PollEvent(&event) )
			{
			//	SDL_PollEvent(&actEvent);
      			switch (event.type)
				{
        			// If a quit event was recieved
        			case SDL_QUIT:
         					done=true;				
        			 break;
					case SDL_ACTIVEEVENT:
					 if( event.active.state != SDL_APPMOUSEFOCUS )
					 {
							 Input::GetInstance().SetFocus( event.active.gain );
					 }
					 break;
      			}
			}

			Input::GetInstance().Update(0);
			RenderMgr::GetInstance().Update(0);
			//RenderMgr::GetInstance().GetCamera().Update( 0 );			

			fnt.Update(0);
			fnt.Print( 10, 10, "FPS: %i\nPolys: %i", fps_last, polys);			

			//myChat.Update(0);

			ShadowMgr::GetInstance().Update();
			World::GetInstance().Update(0);
			  //update remote objects and stuff
			 ObjMgr::Update(1);
			 SoundListener::GetInstance().Update(0);


    		// and check if ESCAPE has been pressed. If so then quit
    		if( Input::GetInstance().KeyDown(SDLK_ESCAPE) ) 
				done = true;

			if( Input::GetInstance().KeyDown(SDLK_F12) )
				GLDevice::GetInstance().ToggleFullScreen();


			if( Input::GetInstance().KeyDown(SDLK_n) )
				glPolygonMode(GL_FRONT, GL_LINE);

			if( Input::GetInstance().KeyDown(SDLK_m) )
				glPolygonMode(GL_FRONT, GL_FILL);

			
		}
		completeGameSteps = curGameSteps;

		

		DrawGLScene();
	}

	return true;
}

void LoadScreen( int percentage, DDS& loadTexture )
{
	GLDevice::GetInstance().BeginRender();
	glOrtho( 0, 512, 512, 0,0.0f,0.5f);

	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);

	//loadTexture.SetTexture();
	glBindTexture( GL_TEXTURE_2D, loadTexture.GetTexture() );

	glBegin(GL_QUADS);
		
		glColor4f(1,1,1,0);

		glTexCoord2f(0.0f, 1.0f);
		glVertex2i( -100, 0 );

		glTexCoord2f(1.0f, 1.0f);
		glVertex2i( 612, 0 );

		glTexCoord2f(1.0f, 0.0f);
		glVertex2i( 612, 512 );

		glTexCoord2f(0.0f, 0.0f);
		glVertex2i( -100, 512 );
	glEnd();

	int posx = 0;
	int posy = 0;
	int width= 512;
	int height = 512;
	int pc = width * (percentage / 100);
	glBegin(GL_QUADS);		
		glColor4f( 1,1,1,1 );

		//glTexCoord2f(0.0f, 1.0f);
		glVertex2i( posx, posy );		

		//glTexCoord2f(1.0f, 1.0f);
		glVertex2i( posx + pc, posy );

		//glTexCoord2f(1.0f, 0.0f);
		glVertex2i( posx + pc, posy + height );

		//glTexCoord2f(0.0f, 0.0f);
		glVertex2i( posx, posy + height );
	glEnd();

	GLDevice::GetInstance().EndRender();
}

int main(int argc, char **argv)
{
	World::SetInstance( new RacingWorld() ); //create world;


//	PhysicsMgr::GetInstance().Initialize();
	INI systemIni("config/system.ini");
/*
	//lets see if we want a server or client
	if( argc >= 3 && strcmp( argv[1], "client" ) == 0 )
	{
		//printf("CREATING CLIENT\n");
		//client.Connect( argv[2] );
		Client::GetInstance().Connect( argv[2] );
	}
	else if( argc >= 2 && strcmp( argv[1], "server" ) == 0 )
	{
		bServer = true;
		//printf("CREATING SERVER\n");
		//server.Create();
		Server::GetInstance().Create();
		//connect to self
		//printf("CREATING CLIENT AND CONNECTING TO SELF\n");
		//client.Connect( "localhost" );
		Client::GetInstance().Connect( "localhost" );
	}*/

	if( GLDevice::GetInstance().CreateDeviceWindow( systemIni ) == false )
		exit(0);

	//amke sure opanal is intit
	SoundListener::GetInstance().SetListenerPosition(0,0,0);

	DDS loadScreen;
	loadScreen.Load( "data/splash.dds"); //, false );
	LoadScreen( 20, loadScreen );

	fnt.CreateFont("data/font.tga");

	sky.Create( "data/top.dds", "data/left.dds", "data/right.dds",
		"data/front.dds", "data/back.dds" );

	World::GetInstance().Load("data/map.SML");
	ShadowMgr::GetInstance().BuildStaticShadows();


//	LoadScreen( 40, loadScreen );



//	LoadScreen( 80, loadScreen );

//	myCar.Initialize();

//	LoadScreen( 90, loadScreen );



	//SDL_Thread* renderThrd = SDL_CreateThread(Render, 0);
	//SDL_Thread* updateThrd = SDL_CreateThread(WorldUpdate, 0);
/*
	//amke sure opanal is intit
	SoundListener::GetInstance().SetListenerPosition(0,0,0);

	fsnd.LoadSound("data/audio/car_moving.wav", true );
	fsnd.SetProperties(0,0,0,0,0,0);
	fsnd.PlaySound();
*/
//	LoadScreen( 100, loadScreen );
//	printf("LOADING COMPLETE\n");

	WorldUpdate(0);


	//GLDevice::GetInstance().DestroyWindow();
	FileMgr::Destroy();
	//GLDevice::Destory();
	//ObjMgr::Shutdown();

	return 0;
}
