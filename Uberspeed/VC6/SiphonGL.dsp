# Microsoft Developer Studio Project File - Name="SiphonGL" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=SiphonGL - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "SiphonGL.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "SiphonGL.mak" CFG="SiphonGL - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "SiphonGL - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "SiphonGL - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "SiphonGL - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0xc09 /d "NDEBUG"
# ADD RSC /l 0xc09 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386 /out:"../SiphonGL.exe"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "SiphonGL - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /w /W0 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0xc09 /d "_DEBUG"
# ADD RSC /l 0xc09 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /nodefaultlib:"libcp.lib" /nodefaultlib:"MSVCP60D.dll" /out:"../SiphonGL.exe" /pdbtype:sept
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "SiphonGL - Win32 Release"
# Name "SiphonGL - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\Engine\Camera.cpp
# End Source File
# Begin Source File

SOURCE=..\car.cpp
# End Source File
# Begin Source File

SOURCE=..\checkpoint.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\Client.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\DDS.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\FileMgr.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\Font.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\GLDevice.cpp
# End Source File
# Begin Source File

SOURCE=..\hud.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\ImportMaterial.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\ImportModel.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\ImportStatic.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\INI.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\Input.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\Light.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\Material.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\Math3D.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\Model.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\NetObj.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\ObjMgr.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\PhysicsMgr.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\PhysicsObj.cpp
# End Source File
# Begin Source File

SOURCE=..\racingworld.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\RenderMgr.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\Server.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\ShadowMgr.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\Sound.cpp
# End Source File
# Begin Source File

SOURCE=..\test.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\TGA.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\World.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine\WorldExtras.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\Engine\Camera.h
# End Source File
# Begin Source File

SOURCE=..\car.h
# End Source File
# Begin Source File

SOURCE=..\checkpoint.h
# End Source File
# Begin Source File

SOURCE=..\Engine\Client.h
# End Source File
# Begin Source File

SOURCE=..\Engine\DDS.h
# End Source File
# Begin Source File

SOURCE=..\Engine\FileMgr.h
# End Source File
# Begin Source File

SOURCE=..\Engine\Font.h
# End Source File
# Begin Source File

SOURCE=..\Engine\GLDevice.h
# End Source File
# Begin Source File

SOURCE=..\hud.h
# End Source File
# Begin Source File

SOURCE=..\Engine\ImportDefs.h
# End Source File
# Begin Source File

SOURCE=..\Engine\ImportMaterial.h
# End Source File
# Begin Source File

SOURCE=..\Engine\ImportModel.h
# End Source File
# Begin Source File

SOURCE=..\Engine\ImportStatic.h
# End Source File
# Begin Source File

SOURCE=..\Engine\INI.h
# End Source File
# Begin Source File

SOURCE=..\Engine\Input.h
# End Source File
# Begin Source File

SOURCE=..\Engine\Light.h
# End Source File
# Begin Source File

SOURCE=..\Engine\Material.h
# End Source File
# Begin Source File

SOURCE=..\Engine\Math3D.h
# End Source File
# Begin Source File

SOURCE=..\Engine\Model.h
# End Source File
# Begin Source File

SOURCE=..\Engine\NetObj.h
# End Source File
# Begin Source File

SOURCE=..\Engine\ObjMgr.h
# End Source File
# Begin Source File

SOURCE=..\Engine\PhysicsMgr.h
# End Source File
# Begin Source File

SOURCE=..\Engine\PhysicsObj.h
# End Source File
# Begin Source File

SOURCE=..\racingworld.h
# End Source File
# Begin Source File

SOURCE=..\Engine\RenderMgr.h
# End Source File
# Begin Source File

SOURCE=..\Engine\Server.h
# End Source File
# Begin Source File

SOURCE=..\Engine\ShadowMgr.h
# End Source File
# Begin Source File

SOURCE=..\Engine\Singleton.h
# End Source File
# Begin Source File

SOURCE=..\Engine\SiphonGL.h
# End Source File
# Begin Source File

SOURCE=..\Engine\Sound.h
# End Source File
# Begin Source File

SOURCE=..\Engine\TGA.h
# End Source File
# Begin Source File

SOURCE=..\Engine\World.h
# End Source File
# Begin Source File

SOURCE=..\Engine\WorldExtras.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
