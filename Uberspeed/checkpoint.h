#ifndef CHECKPOINT_C
#define CHECKPOINT_C

#include "Engine/SiphonGL.h"
//#include "Engine/ImportDefs.h"
//#include "Engine/Math3D.h"

#define CHECKPOINT				27

struct sCheckpoint : public odeBox
{
	int checkpointId;	
};

class Checkpoint
{
public:
	Checkpoint( sCheckpoint checkpoint );

	/**
	 * Convert position to checkpoint local space
	 * then do simple axis aligned box check 
	 */
	bool PointInBox( Vector3& position );

	int GetID();

	virtual int Render( Camera& camera );

protected:
	Matrix4 world;
	Matrix4 worldTranspose;

	Vector3 lines[10];

	//Vector3 position, rotation;
	float length, width, height;
	int id;
};

#endif