#ifndef HUD
#define HUD

#include "Engine/SiphonGL.h"

/**
 * hud for player
 */
class Hud
{
public:
	void Initialize();

	void SetSpeed( float speed );
	void SetLapTime( long lapTime );
	void EndLap();

	void Update( unsigned tick );
	virtual int Render( Camera & camera );

protected:
	long bestLapTime;
	long lapTime;
	long lap;
	float speed;

	char sLap[100];
	char sBestLapTime[100];
	char sLapTime[100];

	SFont font;
};

#endif