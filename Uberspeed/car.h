#ifndef CAR
#define CAR

#include "engine/SiphonGL.h"
#include "hud.h"

class Car : public NetObj, virtual public Model //, virtual public NetObj, virtual public Obj
{
public:
	Car();
	bool Initialize();
	virtual int Render( Camera& camera );

	virtual void Update( unsigned long tick );

	virtual void SetCheckPoint( int checkpoint, int noPoints );

	virtual Vector3 GetPosition();
protected:
	enum VIEW
	{
		FIRSTPERSON,
		THIRDPERSON,
		TOPDOWN
	};

	PhysicsObj phys;
	Camera* camera;

	Matrix4 transform;

	Matrix4 nm; //camera matrix

	Matrix4 model;

	dBodyID body[6];
	dJointID joint[5];	// joint[0] is the front wheel
	//dGeomID geom_group;
	dGeomID box[1];
	dGeomID sphere[4];

	dSpaceID* space;

	Vector3 prevPos; //previous position
	Vector3 lastCamPos; //previsou camera position, fdor smoothing

	float speed;
	float steer;
	float cameraSteer; //similar to steer, but this controls the camera

	Sound motor;
	Sound brake;
	Sound horn;
	Sound idle;
	Sound lapEnd;
	Sound badLap;

	int cameraView; //the view

	virtual bool Recive( void* message, int length );
	virtual bool Send();
	virtual bool SharedNetObj();

	virtual char* GetClassId();

	//DDS test;

	Model mdlCar;
	//Model wheels[4];

	//check point related stuffs
	int lastCheckPoint;
	long lapTime;
	long lastCheckPointTime;

	Hud hud;

	bool bJustCreated;

	Light light; //tmeporary for casting shadow
};



#endif
