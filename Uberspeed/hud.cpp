#include "hud.h"

void Hud::Initialize()
{
	font.CreateFont( "data/font.tga" );
	sLapTime[0] = '\0';
	lap = 1;
	lapTime = 0;
	sprintf( sLap, "Lap: %i", lap );
	sprintf( sBestLapTime, "Best Lap: n/a");
}

void Hud::SetSpeed( float speed )
{
	this->speed = speed;
}

void Hud::SetLapTime( long lapTime )
{
	this->lapTime = lapTime;
}

void Hud::EndLap()
{
	if( lapTime < bestLapTime || bestLapTime == 0 )
		bestLapTime = lapTime;

	lap++;

	printf("lap time: %f lapnumber: %i\n", bestLapTime / 1000.0f, lap );
	sprintf( sBestLapTime, "Best Lap: %.2f sec", bestLapTime / 1000.0f );
	sprintf( sLap, "Lap: %i", lap );

	lapTime = 0;	
}

void Hud::Update( unsigned tick )
{
	lapTime += tick * 20;
	sprintf( sLapTime, "Lap Time: %.2f sec", lapTime / 1000.0f );
	//SetLapTime( lapTime + 20 );
}

int Hud::Render( Camera& camera )
{
	//draw all stuffs here in 2D!
	font.DrawText( 10, 550, sLap );
	font.DrawText( 10, 565, sLapTime );
	font.DrawText( 10, 580, sBestLapTime );

//	font.DrawText( 50, 50, "HEYYAAAA THERE SLACKERS!!!!" );

	return 0;
}
