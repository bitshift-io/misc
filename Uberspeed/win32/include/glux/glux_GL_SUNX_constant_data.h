
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:43 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_SUNX_constant_data__
#define __GLUX_GL_SUNX_constant_data__

GLUX_NEW_PLUGIN(GL_SUNX_constant_data);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_UNPACK_CONSTANT_DATA_SUNX 0x81D5
#define GL_TEXTURE_CONSTANT_DATA_SUNX 0x81D6
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glFinishTextureSUNX
typedef void (APIENTRY * PFNGLUXFINISHTEXTURESUNXPROC) (void);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glFinishTextureSUNX
extern PFNGLUXFINISHTEXTURESUNXPROC glFinishTextureSUNX;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
