
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:16 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_HP_texture_lighting__
#define __GLUX_GL_HP_texture_lighting__

GLUX_NEW_PLUGIN(GL_HP_texture_lighting);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_TEXTURE_LIGHTING_MODE_HP 0x8167
#define GL_TEXTURE_POST_SPECULAR_HP 0x8168
#define GL_TEXTURE_PRE_SPECULAR_HP 0x8169
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
// --------------------------------------------------------
#endif
// --------------------------------------------------------
