// --------------------------------------------------------
#ifdef WIN32

// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:40 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_WGL_ARB_pbuffer__
#define __GLUX_WGL_ARB_pbuffer__

GLUX_NEW_PLUGIN(WGL_ARB_pbuffer);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define WGL_DRAW_TO_PBUFFER_ARB 0x202D
#define WGL_MAX_PBUFFER_PIXELS_ARB 0x202E
#define WGL_MAX_PBUFFER_WIDTH_ARB 0x202F
#define WGL_MAX_PBUFFER_HEIGHT_ARB 0x2030
#define WGL_PBUFFER_LARGEST_ARB 0x2033
#define WGL_PBUFFER_WIDTH_ARB 0x2034
#define WGL_PBUFFER_HEIGHT_ARB 0x2035
#define WGL_PBUFFER_LOST_ARB 0x2036
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_wglCreatePbufferARB
typedef HPBUFFERARB (WINAPI * PFNWGLUXCREATEPBUFFERARBPROC) (HDC hDC, int iPixelFormat, int iWidth, int iHeight, const int *piAttribList);
#endif
#ifndef __GLUX__GLFCT_wglGetPbufferDCARB
typedef HDC (WINAPI * PFNWGLUXGETPBUFFERDCARBPROC) (HPBUFFERARB hPbuffer);
#endif
#ifndef __GLUX__GLFCT_wglReleasePbufferDCARB
typedef int (WINAPI * PFNWGLUXRELEASEPBUFFERDCARBPROC) (HPBUFFERARB hPbuffer, HDC hDC);
#endif
#ifndef __GLUX__GLFCT_wglDestroyPbufferARB
typedef BOOL (WINAPI * PFNWGLUXDESTROYPBUFFERARBPROC) (HPBUFFERARB hPbuffer);
#endif
#ifndef __GLUX__GLFCT_wglQueryPbufferARB
typedef BOOL (WINAPI * PFNWGLUXQUERYPBUFFERARBPROC) (HPBUFFERARB hPbuffer, int iAttribute, int *piValue);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_wglCreatePbufferARB
extern PFNWGLUXCREATEPBUFFERARBPROC wglCreatePbufferARB;
#endif
#ifndef __GLUX__GLFCT_wglGetPbufferDCARB
extern PFNWGLUXGETPBUFFERDCARBPROC wglGetPbufferDCARB;
#endif
#ifndef __GLUX__GLFCT_wglReleasePbufferDCARB
extern PFNWGLUXRELEASEPBUFFERDCARBPROC wglReleasePbufferDCARB;
#endif
#ifndef __GLUX__GLFCT_wglDestroyPbufferARB
extern PFNWGLUXDESTROYPBUFFERARBPROC wglDestroyPbufferARB;
#endif
#ifndef __GLUX__GLFCT_wglQueryPbufferARB
extern PFNWGLUXQUERYPBUFFERARBPROC wglQueryPbufferARB;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
