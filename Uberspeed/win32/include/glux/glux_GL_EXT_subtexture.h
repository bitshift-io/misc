
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:42 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_EXT_subtexture__
#define __GLUX_GL_EXT_subtexture__

GLUX_NEW_PLUGIN(GL_EXT_subtexture);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glTexSubImage1DEXT
typedef void (APIENTRY * PFNGLUXTEXSUBIMAGE1DEXTPROC) (GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, const GLvoid *pixels);
#endif
#ifndef __GLUX__GLFCT_glTexSubImage2DEXT
typedef void (APIENTRY * PFNGLUXTEXSUBIMAGE2DEXTPROC) (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glTexSubImage1DEXT
extern PFNGLUXTEXSUBIMAGE1DEXTPROC glTexSubImage1DEXT;
#endif
#ifndef __GLUX__GLFCT_glTexSubImage2DEXT
extern PFNGLUXTEXSUBIMAGE2DEXTPROC glTexSubImage2DEXT;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
