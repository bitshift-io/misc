// --------------------------------------------------------
#ifndef WIN32

// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:30 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GLX_MESA_pixmap_colormap__
#define __GLUX_GLX_MESA_pixmap_colormap__

GLUX_NEW_PLUGIN(GLX_MESA_pixmap_colormap);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glXCreateGLXPixmapMESA
typedef GLXPixmap ( * PFNXGLUXCREATEGLXPIXMAPMESAPROC) (Display *dpy, XVisualInfo *visual, Pixmap pixmap, Colormap cmap);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glXCreateGLXPixmapMESA
extern PFNXGLUXCREATEGLXPIXMAPMESAPROC glXCreateGLXPixmapMESA;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
