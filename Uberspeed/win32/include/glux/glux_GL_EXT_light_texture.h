
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:18 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_EXT_light_texture__
#define __GLUX_GL_EXT_light_texture__

GLUX_NEW_PLUGIN(GL_EXT_light_texture);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_FRAGMENT_MATERIAL_EXT 0x8349
#define GL_FRAGMENT_NORMAL_EXT 0x834A
#define GL_FRAGMENT_COLOR_EXT 0x834C
#define GL_ATTENUATION_EXT 0x834D
#define GL_SHADOW_ATTENUATION_EXT 0x834E
#define GL_TEXTURE_APPLICATION_MODE_EXT 0x834F
#define GL_TEXTURE_LIGHT_EXT 0x8350
#define GL_TEXTURE_MATERIAL_FACE_EXT 0x8351
#define GL_TEXTURE_MATERIAL_PARAMETER_EXT 0x8352
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glApplyTextureEXT
typedef void (APIENTRY * PFNGLUXAPPLYTEXTUREEXTPROC) (GLenum mode);
#endif
#ifndef __GLUX__GLFCT_glTextureLightEXT
typedef void (APIENTRY * PFNGLUXTEXTURELIGHTEXTPROC) (GLenum pname);
#endif
#ifndef __GLUX__GLFCT_glTextureMaterialEXT
typedef void (APIENTRY * PFNGLUXTEXTUREMATERIALEXTPROC) (GLenum face, GLenum mode);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glApplyTextureEXT
extern PFNGLUXAPPLYTEXTUREEXTPROC glApplyTextureEXT;
#endif
#ifndef __GLUX__GLFCT_glTextureLightEXT
extern PFNGLUXTEXTURELIGHTEXTPROC glTextureLightEXT;
#endif
#ifndef __GLUX__GLFCT_glTextureMaterialEXT
extern PFNGLUXTEXTUREMATERIALEXTPROC glTextureMaterialEXT;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
