
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:26 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_INTEL_parallel_arrays__
#define __GLUX_GL_INTEL_parallel_arrays__

GLUX_NEW_PLUGIN(GL_INTEL_parallel_arrays);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_PARALLEL_ARRAYS_INTEL 0x83F4
#define GL_VERTEX_ARRAY_PARALLEL_POINTERS_INTEL 0x83F5
#define GL_NORMAL_ARRAY_PARALLEL_POINTERS_INTEL 0x83F6
#define GL_COLOR_ARRAY_PARALLEL_POINTERS_INTEL 0x83F7
#define GL_TEXTURE_COORD_ARRAY_PARALLEL_POINTERS_INTEL 0x83F8
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glVertexPointervINTEL
typedef void (APIENTRY * PFNGLUXVERTEXPOINTERVINTELPROC) (GLint size, GLenum type, const GLvoid* *pointer);
#endif
#ifndef __GLUX__GLFCT_glNormalPointervINTEL
typedef void (APIENTRY * PFNGLUXNORMALPOINTERVINTELPROC) (GLenum type, const GLvoid* *pointer);
#endif
#ifndef __GLUX__GLFCT_glColorPointervINTEL
typedef void (APIENTRY * PFNGLUXCOLORPOINTERVINTELPROC) (GLint size, GLenum type, const GLvoid* *pointer);
#endif
#ifndef __GLUX__GLFCT_glTexCoordPointervINTEL
typedef void (APIENTRY * PFNGLUXTEXCOORDPOINTERVINTELPROC) (GLint size, GLenum type, const GLvoid* *pointer);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glVertexPointervINTEL
extern PFNGLUXVERTEXPOINTERVINTELPROC glVertexPointervINTEL;
#endif
#ifndef __GLUX__GLFCT_glNormalPointervINTEL
extern PFNGLUXNORMALPOINTERVINTELPROC glNormalPointervINTEL;
#endif
#ifndef __GLUX__GLFCT_glColorPointervINTEL
extern PFNGLUXCOLORPOINTERVINTELPROC glColorPointervINTEL;
#endif
#ifndef __GLUX__GLFCT_glTexCoordPointervINTEL
extern PFNGLUXTEXCOORDPOINTERVINTELPROC glTexCoordPointervINTEL;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
