// --------------------------------------------------------
#ifndef WIN32

// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:29 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GLX_MESA_release_buffers__
#define __GLUX_GLX_MESA_release_buffers__

GLUX_NEW_PLUGIN(GLX_MESA_release_buffers);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glXReleaseBuffersMESA
typedef Bool ( * PFNXGLUXRELEASEBUFFERSMESAPROC) (Display *dpy, GLXDrawable drawable);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glXReleaseBuffersMESA
extern PFNXGLUXRELEASEBUFFERSMESAPROC glXReleaseBuffersMESA;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
