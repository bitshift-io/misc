
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:34 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_EXT_index_array_formats__
#define __GLUX_GL_EXT_index_array_formats__

GLUX_NEW_PLUGIN(GL_EXT_index_array_formats);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_IUI_V2F_EXT 0x81AD
#define GL_IUI_V3F_EXT 0x81AE
#define GL_IUI_N3F_V2F_EXT 0x81AF
#define GL_IUI_N3F_V3F_EXT 0x81B0
#define GL_T2F_IUI_V2F_EXT 0x81B1
#define GL_T2F_IUI_V3F_EXT 0x81B2
#define GL_T2F_IUI_N3F_V2F_EXT 0x81B3
#define GL_T2F_IUI_N3F_V3F_EXT 0x81B4
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
// --------------------------------------------------------
#endif
// --------------------------------------------------------
