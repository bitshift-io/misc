// --------------------------------------------------------
// Generated by glux perl script ()
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#ifndef __GLUX_EXT_DEFS__
#define __GLUX_EXT_DEFS__
// --------------------------------------------------------
#ifdef WIN32
#include <windows.h>
#else

#endif
// --------------------------------------------------------
using namespace glux;
// --------------------------------------------------------
// Extensions custom declarations
// --------------------------------------------------------
#ifndef GL_EXT_light_texture
  /* reuse GL_FRAGMENT_DEPTH_EXT */
#endif
#ifndef GL_ARB_fragment_program
  /* All ARB_fragment_program entry points are shared with ARB_vertex_program. */
#endif
#ifndef GL_NV_half_float
  /* GL type for representing NVIDIA "half" floating point type in host memory */
  /* Only used by this extension for now; later needs to be moved earlier in glext.h */
  typedef unsigned short GLhalfNV;
#endif
#ifndef GL_NV_fragment_program
  /* Some NV_fragment_program entry points are shared with ARB_vertex_program. */
#endif
#ifndef GLX_SGIX_fbconfig
# ifndef WIN32
  /* reuse GLX_SCREEN_EXT */
  typedef XID GLXFBConfigIDSGIX;
  typedef struct __GLXFBConfigRec *GLXFBConfigSGIX;
# endif
#endif
#ifndef GL_NV_register_combiners
  /* reuse GL_TEXTURE0_ARB */
  /* reuse GL_TEXTURE1_ARB */
  /* reuse GL_ZERO */
  /* reuse GL_NONE */
  /* reuse GL_FOG */
#endif
#ifndef WGL_EXT_pbuffer
# ifdef WIN32
  DECLARE_HANDLE(HPBUFFEREXT);
# endif
#endif
#ifndef GLX_OML_sync_control
# ifndef WIN32
  #if defined(__STDC_VERSION__)
  #if __STDC_VERSION__ >= 199901L
  /* Include ISO C99 integer types for OML_sync_control; need a better test */
  #include <inttypes.h>
  
  #endif
  
  #endif /* C99 version test */
# endif
#endif
#ifndef GLX_SGIX_video_source
# ifndef WIN32
  typedef XID GLXVideoSourceSGIX;
  #ifdef _VL_H
  #endif
  
# endif
#endif
#ifndef GLX_EXT_visual_rating
# ifndef WIN32
  /* reuse GLX_NONE_EXT */
# endif
#endif
#ifndef GLX_SGIX_pbuffer
# ifndef WIN32
  typedef XID GLXPbufferSGIX;
  typedef struct {
      int type;
      unsigned long serial;	  /* # of last request processed by server */
      Bool send_event;		  /* true if this came for SendEvent request */
      Display *display;		  /* display the event was read from */
      GLXDrawable drawable;	  /* i.d. of Drawable */
      int event_type;		  /* GLX_DAMAGED_SGIX or GLX_SAVED_SGIX */
      int draw_type;		  /* GLX_WINDOW_SGIX or GLX_PBUFFER_SGIX */
      unsigned int mask;	  /* mask indicating which buffers are affected*/
      int x, y;
      int width, height;
      int count;		  /* if nonzero, at least this many more */
  } GLXBufferClobberEventSGIX;
# endif
#endif
#ifndef GL_NV_fog_distance
  /* reuse GL_EYE_PLANE */
#endif
#ifndef GL_ARB_vertex_buffer_object
  /* GL types for handling large vertex buffer objects */
  /* Only used by this extension for now; later needs to be moved earlier in glext.h */
  #include <stddef.h>
  typedef ptrdiff_t GLintptrARB;
  typedef ptrdiff_t GLsizeiptrARB;
#endif
#ifndef WGL_ARB_make_current_read
# ifdef WIN32
  #define ERROR_INVALID_PIXEL_TYPE_ARB   0x2043
  #define ERROR_INCOMPATIBLE_DEVICE_CONTEXTS_ARB 0x2054
# endif
#endif
#ifndef WGL_ARB_pbuffer
# ifdef WIN32
  DECLARE_HANDLE(HPBUFFERARB);
# endif
#endif
#ifndef GLX_SGIX_dmbuffer
# ifndef WIN32
  #ifdef _DM_BUFFER_H_
  #endif
  
# endif
#endif
#ifndef GLX_ARB_get_proc_address
# ifndef WIN32
  typedef void (*__GLXextFuncPtr)(void);
# endif
#endif
#ifndef WGL_EXT_make_current_read
# ifdef WIN32
  #define ERROR_INVALID_PIXEL_TYPE_EXT   0x2043
# endif
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
