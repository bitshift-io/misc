
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:23 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_ARB_transpose_matrix__
#define __GLUX_GL_ARB_transpose_matrix__

GLUX_NEW_PLUGIN(GL_ARB_transpose_matrix);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_TRANSPOSE_MODELVIEW_MATRIX_ARB 0x84E3
#define GL_TRANSPOSE_PROJECTION_MATRIX_ARB 0x84E4
#define GL_TRANSPOSE_TEXTURE_MATRIX_ARB 0x84E5
#define GL_TRANSPOSE_COLOR_MATRIX_ARB 0x84E6
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glLoadTransposeMatrixfARB
typedef void (APIENTRY * PFNGLUXLOADTRANSPOSEMATRIXFARBPROC) (const GLfloat *m);
#endif
#ifndef __GLUX__GLFCT_glLoadTransposeMatrixdARB
typedef void (APIENTRY * PFNGLUXLOADTRANSPOSEMATRIXDARBPROC) (const GLdouble *m);
#endif
#ifndef __GLUX__GLFCT_glMultTransposeMatrixfARB
typedef void (APIENTRY * PFNGLUXMULTTRANSPOSEMATRIXFARBPROC) (const GLfloat *m);
#endif
#ifndef __GLUX__GLFCT_glMultTransposeMatrixdARB
typedef void (APIENTRY * PFNGLUXMULTTRANSPOSEMATRIXDARBPROC) (const GLdouble *m);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glLoadTransposeMatrixfARB
extern PFNGLUXLOADTRANSPOSEMATRIXFARBPROC glLoadTransposeMatrixfARB;
#endif
#ifndef __GLUX__GLFCT_glLoadTransposeMatrixdARB
extern PFNGLUXLOADTRANSPOSEMATRIXDARBPROC glLoadTransposeMatrixdARB;
#endif
#ifndef __GLUX__GLFCT_glMultTransposeMatrixfARB
extern PFNGLUXMULTTRANSPOSEMATRIXFARBPROC glMultTransposeMatrixfARB;
#endif
#ifndef __GLUX__GLFCT_glMultTransposeMatrixdARB
extern PFNGLUXMULTTRANSPOSEMATRIXDARBPROC glMultTransposeMatrixdARB;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
