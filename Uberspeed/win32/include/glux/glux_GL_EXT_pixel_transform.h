
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:20 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_EXT_pixel_transform__
#define __GLUX_GL_EXT_pixel_transform__

GLUX_NEW_PLUGIN(GL_EXT_pixel_transform);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_PIXEL_TRANSFORM_2D_EXT 0x8330
#define GL_PIXEL_MAG_FILTER_EXT 0x8331
#define GL_PIXEL_MIN_FILTER_EXT 0x8332
#define GL_PIXEL_CUBIC_WEIGHT_EXT 0x8333
#define GL_CUBIC_EXT 0x8334
#define GL_AVERAGE_EXT 0x8335
#define GL_PIXEL_TRANSFORM_2D_STACK_DEPTH_EXT 0x8336
#define GL_MAX_PIXEL_TRANSFORM_2D_STACK_DEPTH_EXT 0x8337
#define GL_PIXEL_TRANSFORM_2D_MATRIX_EXT 0x8338
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glPixelTransformParameteriEXT
typedef void (APIENTRY * PFNGLUXPIXELTRANSFORMPARAMETERIEXTPROC) (GLenum target, GLenum pname, GLint param);
#endif
#ifndef __GLUX__GLFCT_glPixelTransformParameterfEXT
typedef void (APIENTRY * PFNGLUXPIXELTRANSFORMPARAMETERFEXTPROC) (GLenum target, GLenum pname, GLfloat param);
#endif
#ifndef __GLUX__GLFCT_glPixelTransformParameterivEXT
typedef void (APIENTRY * PFNGLUXPIXELTRANSFORMPARAMETERIVEXTPROC) (GLenum target, GLenum pname, const GLint *params);
#endif
#ifndef __GLUX__GLFCT_glPixelTransformParameterfvEXT
typedef void (APIENTRY * PFNGLUXPIXELTRANSFORMPARAMETERFVEXTPROC) (GLenum target, GLenum pname, const GLfloat *params);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glPixelTransformParameteriEXT
extern PFNGLUXPIXELTRANSFORMPARAMETERIEXTPROC glPixelTransformParameteriEXT;
#endif
#ifndef __GLUX__GLFCT_glPixelTransformParameterfEXT
extern PFNGLUXPIXELTRANSFORMPARAMETERFEXTPROC glPixelTransformParameterfEXT;
#endif
#ifndef __GLUX__GLFCT_glPixelTransformParameterivEXT
extern PFNGLUXPIXELTRANSFORMPARAMETERIVEXTPROC glPixelTransformParameterivEXT;
#endif
#ifndef __GLUX__GLFCT_glPixelTransformParameterfvEXT
extern PFNGLUXPIXELTRANSFORMPARAMETERFVEXTPROC glPixelTransformParameterfvEXT;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
