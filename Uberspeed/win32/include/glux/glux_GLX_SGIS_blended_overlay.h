// --------------------------------------------------------
#ifndef WIN32

// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:29 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GLX_SGIS_blended_overlay__
#define __GLUX_GLX_SGIS_blended_overlay__

GLUX_NEW_PLUGIN(GLX_SGIS_blended_overlay);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GLX_BLENDED_RGBA_SGIS 0x8025
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
// --------------------------------------------------------
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
