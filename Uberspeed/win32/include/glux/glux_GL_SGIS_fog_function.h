
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:43 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_SGIS_fog_function__
#define __GLUX_GL_SGIS_fog_function__

GLUX_NEW_PLUGIN(GL_SGIS_fog_function);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_FOG_FUNC_SGIS 0x812A
#define GL_FOG_FUNC_POINTS_SGIS 0x812B
#define GL_MAX_FOG_FUNC_POINTS_SGIS 0x812C
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glFogFuncSGIS
typedef void (APIENTRY * PFNGLUXFOGFUNCSGISPROC) (GLsizei n, const GLfloat *points);
#endif
#ifndef __GLUX__GLFCT_glGetFogFuncSGIS
typedef void (APIENTRY * PFNGLUXGETFOGFUNCSGISPROC) (GLfloat *points);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glFogFuncSGIS
extern PFNGLUXFOGFUNCSGISPROC glFogFuncSGIS;
#endif
#ifndef __GLUX__GLFCT_glGetFogFuncSGIS
extern PFNGLUXGETFOGFUNCSGISPROC glGetFogFuncSGIS;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
