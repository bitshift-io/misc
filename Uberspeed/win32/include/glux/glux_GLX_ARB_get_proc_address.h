// --------------------------------------------------------
#ifndef WIN32

// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:43 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GLX_ARB_get_proc_address__
#define __GLUX_GLX_ARB_get_proc_address__

GLUX_NEW_PLUGIN(GLX_ARB_get_proc_address);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glXGetProcAddressARB
typedef __GLXextFuncPtr ( * PFNXGLUXGETPROCADDRESSARBPROC) (const GLubyte *procName);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glXGetProcAddressARB
extern PFNXGLUXGETPROCADDRESSARBPROC glXGetProcAddressARB;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
