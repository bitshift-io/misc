// --------------------------------------------------------
#ifndef WIN32

// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:20 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GLX_SGIX_fbconfig__
#define __GLUX_GLX_SGIX_fbconfig__

GLUX_NEW_PLUGIN(GLX_SGIX_fbconfig);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GLX_WINDOW_BIT_SGIX 0x00000001
#define GLX_PIXMAP_BIT_SGIX 0x00000002
#define GLX_RGBA_BIT_SGIX 0x00000001
#define GLX_COLOR_INDEX_BIT_SGIX 0x00000002
#define GLX_DRAWABLE_TYPE_SGIX 0x8010
#define GLX_RENDER_TYPE_SGIX 0x8011
#define GLX_X_RENDERABLE_SGIX 0x8012
#define GLX_FBCONFIG_ID_SGIX 0x8013
#define GLX_RGBA_TYPE_SGIX 0x8014
#define GLX_COLOR_INDEX_TYPE_SGIX 0x8015
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glXGetFBConfigAttribSGIX
typedef int ( * PFNXGLUXGETFBCONFIGATTRIBSGIXPROC) (Display *dpy, GLXFBConfigSGIX config, int attribute, int *value);
#endif
#ifndef __GLUX__GLFCT_glXChooseFBConfigSGIX
typedef GLXFBConfigSGIX * ( * PFNXGLUXCHOOSEFBCONFIGSGIXPROC) (Display *dpy, int screen, int *attrib_list, int *nelements);
#endif
#ifndef __GLUX__GLFCT_glXCreateGLXPixmapWithConfigSGIX
typedef GLXPixmap ( * PFNXGLUXCREATEGLXPIXMAPWITHCONFIGSGIXPROC) (Display *dpy, GLXFBConfigSGIX config, Pixmap pixmap);
#endif
#ifndef __GLUX__GLFCT_glXCreateContextWithConfigSGIX
typedef GLXContext ( * PFNXGLUXCREATECONTEXTWITHCONFIGSGIXPROC) (Display *dpy, GLXFBConfigSGIX config, int render_type, GLXContext share_list, Bool direct);
#endif
#ifndef __GLUX__GLFCT_glXGetVisualFromFBConfigSGIX
typedef XVisualInfo * ( * PFNXGLUXGETVISUALFROMFBCONFIGSGIXPROC) (Display *dpy, GLXFBConfigSGIX config);
#endif
#ifndef __GLUX__GLFCT_glXGetFBConfigFromVisualSGIX
typedef GLXFBConfigSGIX ( * PFNXGLUXGETFBCONFIGFROMVISUALSGIXPROC) (Display *dpy, XVisualInfo *vis);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glXGetFBConfigAttribSGIX
extern PFNXGLUXGETFBCONFIGATTRIBSGIXPROC glXGetFBConfigAttribSGIX;
#endif
#ifndef __GLUX__GLFCT_glXChooseFBConfigSGIX
extern PFNXGLUXCHOOSEFBCONFIGSGIXPROC glXChooseFBConfigSGIX;
#endif
#ifndef __GLUX__GLFCT_glXCreateGLXPixmapWithConfigSGIX
extern PFNXGLUXCREATEGLXPIXMAPWITHCONFIGSGIXPROC glXCreateGLXPixmapWithConfigSGIX;
#endif
#ifndef __GLUX__GLFCT_glXCreateContextWithConfigSGIX
extern PFNXGLUXCREATECONTEXTWITHCONFIGSGIXPROC glXCreateContextWithConfigSGIX;
#endif
#ifndef __GLUX__GLFCT_glXGetVisualFromFBConfigSGIX
extern PFNXGLUXGETVISUALFROMFBCONFIGSGIXPROC glXGetVisualFromFBConfigSGIX;
#endif
#ifndef __GLUX__GLFCT_glXGetFBConfigFromVisualSGIX
extern PFNXGLUXGETFBCONFIGFROMVISUALSGIXPROC glXGetFBConfigFromVisualSGIX;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
