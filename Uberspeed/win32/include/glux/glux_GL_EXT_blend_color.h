
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:36 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_EXT_blend_color__
#define __GLUX_GL_EXT_blend_color__

GLUX_NEW_PLUGIN(GL_EXT_blend_color);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_CONSTANT_COLOR_EXT 0x8001
#define GL_ONE_MINUS_CONSTANT_COLOR_EXT 0x8002
#define GL_CONSTANT_ALPHA_EXT 0x8003
#define GL_ONE_MINUS_CONSTANT_ALPHA_EXT 0x8004
#define GL_BLEND_COLOR_EXT 0x8005
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glBlendColorEXT
typedef void (APIENTRY * PFNGLUXBLENDCOLOREXTPROC) (GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glBlendColorEXT
extern PFNGLUXBLENDCOLOREXTPROC glBlendColorEXT;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
