
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:16 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_ATI_vertex_streams__
#define __GLUX_GL_ATI_vertex_streams__

GLUX_NEW_PLUGIN(GL_ATI_vertex_streams);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_MAX_VERTEX_STREAMS_ATI 0x876B
#define GL_VERTEX_STREAM0_ATI 0x876C
#define GL_VERTEX_STREAM1_ATI 0x876D
#define GL_VERTEX_STREAM2_ATI 0x876E
#define GL_VERTEX_STREAM3_ATI 0x876F
#define GL_VERTEX_STREAM4_ATI 0x8770
#define GL_VERTEX_STREAM5_ATI 0x8771
#define GL_VERTEX_STREAM6_ATI 0x8772
#define GL_VERTEX_STREAM7_ATI 0x8773
#define GL_VERTEX_SOURCE_ATI 0x8774
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glVertexStream1sATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM1SATIPROC) (GLenum stream, GLshort x);
#endif
#ifndef __GLUX__GLFCT_glVertexStream1svATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM1SVATIPROC) (GLenum stream, const GLshort *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream1iATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM1IATIPROC) (GLenum stream, GLint x);
#endif
#ifndef __GLUX__GLFCT_glVertexStream1ivATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM1IVATIPROC) (GLenum stream, const GLint *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream1fATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM1FATIPROC) (GLenum stream, GLfloat x);
#endif
#ifndef __GLUX__GLFCT_glVertexStream1fvATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM1FVATIPROC) (GLenum stream, const GLfloat *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream1dATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM1DATIPROC) (GLenum stream, GLdouble x);
#endif
#ifndef __GLUX__GLFCT_glVertexStream1dvATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM1DVATIPROC) (GLenum stream, const GLdouble *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream2sATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM2SATIPROC) (GLenum stream, GLshort x, GLshort y);
#endif
#ifndef __GLUX__GLFCT_glVertexStream2svATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM2SVATIPROC) (GLenum stream, const GLshort *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream2iATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM2IATIPROC) (GLenum stream, GLint x, GLint y);
#endif
#ifndef __GLUX__GLFCT_glVertexStream2ivATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM2IVATIPROC) (GLenum stream, const GLint *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream2fATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM2FATIPROC) (GLenum stream, GLfloat x, GLfloat y);
#endif
#ifndef __GLUX__GLFCT_glVertexStream2fvATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM2FVATIPROC) (GLenum stream, const GLfloat *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream2dATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM2DATIPROC) (GLenum stream, GLdouble x, GLdouble y);
#endif
#ifndef __GLUX__GLFCT_glVertexStream2dvATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM2DVATIPROC) (GLenum stream, const GLdouble *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream3sATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM3SATIPROC) (GLenum stream, GLshort x, GLshort y, GLshort z);
#endif
#ifndef __GLUX__GLFCT_glVertexStream3svATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM3SVATIPROC) (GLenum stream, const GLshort *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream3iATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM3IATIPROC) (GLenum stream, GLint x, GLint y, GLint z);
#endif
#ifndef __GLUX__GLFCT_glVertexStream3ivATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM3IVATIPROC) (GLenum stream, const GLint *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream3fATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM3FATIPROC) (GLenum stream, GLfloat x, GLfloat y, GLfloat z);
#endif
#ifndef __GLUX__GLFCT_glVertexStream3fvATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM3FVATIPROC) (GLenum stream, const GLfloat *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream3dATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM3DATIPROC) (GLenum stream, GLdouble x, GLdouble y, GLdouble z);
#endif
#ifndef __GLUX__GLFCT_glVertexStream3dvATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM3DVATIPROC) (GLenum stream, const GLdouble *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream4sATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM4SATIPROC) (GLenum stream, GLshort x, GLshort y, GLshort z, GLshort w);
#endif
#ifndef __GLUX__GLFCT_glVertexStream4svATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM4SVATIPROC) (GLenum stream, const GLshort *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream4iATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM4IATIPROC) (GLenum stream, GLint x, GLint y, GLint z, GLint w);
#endif
#ifndef __GLUX__GLFCT_glVertexStream4ivATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM4IVATIPROC) (GLenum stream, const GLint *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream4fATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM4FATIPROC) (GLenum stream, GLfloat x, GLfloat y, GLfloat z, GLfloat w);
#endif
#ifndef __GLUX__GLFCT_glVertexStream4fvATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM4FVATIPROC) (GLenum stream, const GLfloat *coords);
#endif
#ifndef __GLUX__GLFCT_glVertexStream4dATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM4DATIPROC) (GLenum stream, GLdouble x, GLdouble y, GLdouble z, GLdouble w);
#endif
#ifndef __GLUX__GLFCT_glVertexStream4dvATI
typedef void (APIENTRY * PFNGLUXVERTEXSTREAM4DVATIPROC) (GLenum stream, const GLdouble *coords);
#endif
#ifndef __GLUX__GLFCT_glNormalStream3bATI
typedef void (APIENTRY * PFNGLUXNORMALSTREAM3BATIPROC) (GLenum stream, GLbyte nx, GLbyte ny, GLbyte nz);
#endif
#ifndef __GLUX__GLFCT_glNormalStream3bvATI
typedef void (APIENTRY * PFNGLUXNORMALSTREAM3BVATIPROC) (GLenum stream, const GLbyte *coords);
#endif
#ifndef __GLUX__GLFCT_glNormalStream3sATI
typedef void (APIENTRY * PFNGLUXNORMALSTREAM3SATIPROC) (GLenum stream, GLshort nx, GLshort ny, GLshort nz);
#endif
#ifndef __GLUX__GLFCT_glNormalStream3svATI
typedef void (APIENTRY * PFNGLUXNORMALSTREAM3SVATIPROC) (GLenum stream, const GLshort *coords);
#endif
#ifndef __GLUX__GLFCT_glNormalStream3iATI
typedef void (APIENTRY * PFNGLUXNORMALSTREAM3IATIPROC) (GLenum stream, GLint nx, GLint ny, GLint nz);
#endif
#ifndef __GLUX__GLFCT_glNormalStream3ivATI
typedef void (APIENTRY * PFNGLUXNORMALSTREAM3IVATIPROC) (GLenum stream, const GLint *coords);
#endif
#ifndef __GLUX__GLFCT_glNormalStream3fATI
typedef void (APIENTRY * PFNGLUXNORMALSTREAM3FATIPROC) (GLenum stream, GLfloat nx, GLfloat ny, GLfloat nz);
#endif
#ifndef __GLUX__GLFCT_glNormalStream3fvATI
typedef void (APIENTRY * PFNGLUXNORMALSTREAM3FVATIPROC) (GLenum stream, const GLfloat *coords);
#endif
#ifndef __GLUX__GLFCT_glNormalStream3dATI
typedef void (APIENTRY * PFNGLUXNORMALSTREAM3DATIPROC) (GLenum stream, GLdouble nx, GLdouble ny, GLdouble nz);
#endif
#ifndef __GLUX__GLFCT_glNormalStream3dvATI
typedef void (APIENTRY * PFNGLUXNORMALSTREAM3DVATIPROC) (GLenum stream, const GLdouble *coords);
#endif
#ifndef __GLUX__GLFCT_glClientActiveVertexStreamATI
typedef void (APIENTRY * PFNGLUXCLIENTACTIVEVERTEXSTREAMATIPROC) (GLenum stream);
#endif
#ifndef __GLUX__GLFCT_glVertexBlendEnviATI
typedef void (APIENTRY * PFNGLUXVERTEXBLENDENVIATIPROC) (GLenum pname, GLint param);
#endif
#ifndef __GLUX__GLFCT_glVertexBlendEnvfATI
typedef void (APIENTRY * PFNGLUXVERTEXBLENDENVFATIPROC) (GLenum pname, GLfloat param);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glVertexStream1sATI
extern PFNGLUXVERTEXSTREAM1SATIPROC glVertexStream1sATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream1svATI
extern PFNGLUXVERTEXSTREAM1SVATIPROC glVertexStream1svATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream1iATI
extern PFNGLUXVERTEXSTREAM1IATIPROC glVertexStream1iATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream1ivATI
extern PFNGLUXVERTEXSTREAM1IVATIPROC glVertexStream1ivATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream1fATI
extern PFNGLUXVERTEXSTREAM1FATIPROC glVertexStream1fATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream1fvATI
extern PFNGLUXVERTEXSTREAM1FVATIPROC glVertexStream1fvATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream1dATI
extern PFNGLUXVERTEXSTREAM1DATIPROC glVertexStream1dATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream1dvATI
extern PFNGLUXVERTEXSTREAM1DVATIPROC glVertexStream1dvATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream2sATI
extern PFNGLUXVERTEXSTREAM2SATIPROC glVertexStream2sATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream2svATI
extern PFNGLUXVERTEXSTREAM2SVATIPROC glVertexStream2svATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream2iATI
extern PFNGLUXVERTEXSTREAM2IATIPROC glVertexStream2iATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream2ivATI
extern PFNGLUXVERTEXSTREAM2IVATIPROC glVertexStream2ivATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream2fATI
extern PFNGLUXVERTEXSTREAM2FATIPROC glVertexStream2fATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream2fvATI
extern PFNGLUXVERTEXSTREAM2FVATIPROC glVertexStream2fvATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream2dATI
extern PFNGLUXVERTEXSTREAM2DATIPROC glVertexStream2dATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream2dvATI
extern PFNGLUXVERTEXSTREAM2DVATIPROC glVertexStream2dvATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream3sATI
extern PFNGLUXVERTEXSTREAM3SATIPROC glVertexStream3sATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream3svATI
extern PFNGLUXVERTEXSTREAM3SVATIPROC glVertexStream3svATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream3iATI
extern PFNGLUXVERTEXSTREAM3IATIPROC glVertexStream3iATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream3ivATI
extern PFNGLUXVERTEXSTREAM3IVATIPROC glVertexStream3ivATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream3fATI
extern PFNGLUXVERTEXSTREAM3FATIPROC glVertexStream3fATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream3fvATI
extern PFNGLUXVERTEXSTREAM3FVATIPROC glVertexStream3fvATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream3dATI
extern PFNGLUXVERTEXSTREAM3DATIPROC glVertexStream3dATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream3dvATI
extern PFNGLUXVERTEXSTREAM3DVATIPROC glVertexStream3dvATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream4sATI
extern PFNGLUXVERTEXSTREAM4SATIPROC glVertexStream4sATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream4svATI
extern PFNGLUXVERTEXSTREAM4SVATIPROC glVertexStream4svATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream4iATI
extern PFNGLUXVERTEXSTREAM4IATIPROC glVertexStream4iATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream4ivATI
extern PFNGLUXVERTEXSTREAM4IVATIPROC glVertexStream4ivATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream4fATI
extern PFNGLUXVERTEXSTREAM4FATIPROC glVertexStream4fATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream4fvATI
extern PFNGLUXVERTEXSTREAM4FVATIPROC glVertexStream4fvATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream4dATI
extern PFNGLUXVERTEXSTREAM4DATIPROC glVertexStream4dATI;
#endif
#ifndef __GLUX__GLFCT_glVertexStream4dvATI
extern PFNGLUXVERTEXSTREAM4DVATIPROC glVertexStream4dvATI;
#endif
#ifndef __GLUX__GLFCT_glNormalStream3bATI
extern PFNGLUXNORMALSTREAM3BATIPROC glNormalStream3bATI;
#endif
#ifndef __GLUX__GLFCT_glNormalStream3bvATI
extern PFNGLUXNORMALSTREAM3BVATIPROC glNormalStream3bvATI;
#endif
#ifndef __GLUX__GLFCT_glNormalStream3sATI
extern PFNGLUXNORMALSTREAM3SATIPROC glNormalStream3sATI;
#endif
#ifndef __GLUX__GLFCT_glNormalStream3svATI
extern PFNGLUXNORMALSTREAM3SVATIPROC glNormalStream3svATI;
#endif
#ifndef __GLUX__GLFCT_glNormalStream3iATI
extern PFNGLUXNORMALSTREAM3IATIPROC glNormalStream3iATI;
#endif
#ifndef __GLUX__GLFCT_glNormalStream3ivATI
extern PFNGLUXNORMALSTREAM3IVATIPROC glNormalStream3ivATI;
#endif
#ifndef __GLUX__GLFCT_glNormalStream3fATI
extern PFNGLUXNORMALSTREAM3FATIPROC glNormalStream3fATI;
#endif
#ifndef __GLUX__GLFCT_glNormalStream3fvATI
extern PFNGLUXNORMALSTREAM3FVATIPROC glNormalStream3fvATI;
#endif
#ifndef __GLUX__GLFCT_glNormalStream3dATI
extern PFNGLUXNORMALSTREAM3DATIPROC glNormalStream3dATI;
#endif
#ifndef __GLUX__GLFCT_glNormalStream3dvATI
extern PFNGLUXNORMALSTREAM3DVATIPROC glNormalStream3dvATI;
#endif
#ifndef __GLUX__GLFCT_glClientActiveVertexStreamATI
extern PFNGLUXCLIENTACTIVEVERTEXSTREAMATIPROC glClientActiveVertexStreamATI;
#endif
#ifndef __GLUX__GLFCT_glVertexBlendEnviATI
extern PFNGLUXVERTEXBLENDENVIATIPROC glVertexBlendEnviATI;
#endif
#ifndef __GLUX__GLFCT_glVertexBlendEnvfATI
extern PFNGLUXVERTEXBLENDENVFATIPROC glVertexBlendEnvfATI;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
