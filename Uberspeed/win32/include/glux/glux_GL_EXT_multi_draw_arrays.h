
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:20 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_EXT_multi_draw_arrays__
#define __GLUX_GL_EXT_multi_draw_arrays__

GLUX_NEW_PLUGIN(GL_EXT_multi_draw_arrays);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glMultiDrawArraysEXT
typedef void (APIENTRY * PFNGLUXMULTIDRAWARRAYSEXTPROC) (GLenum mode, GLint *first, GLsizei *count, GLsizei primcount);
#endif
#ifndef __GLUX__GLFCT_glMultiDrawElementsEXT
typedef void (APIENTRY * PFNGLUXMULTIDRAWELEMENTSEXTPROC) (GLenum mode, const GLsizei *count, GLenum type, const GLvoid* *indices, GLsizei primcount);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glMultiDrawArraysEXT
extern PFNGLUXMULTIDRAWARRAYSEXTPROC glMultiDrawArraysEXT;
#endif
#ifndef __GLUX__GLFCT_glMultiDrawElementsEXT
extern PFNGLUXMULTIDRAWELEMENTSEXTPROC glMultiDrawElementsEXT;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
