
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:26 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_NV_register_combiners__
#define __GLUX_GL_NV_register_combiners__

GLUX_NEW_PLUGIN(GL_NV_register_combiners);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_REGISTER_COMBINERS_NV 0x8522
#define GL_VARIABLE_A_NV 0x8523
#define GL_VARIABLE_B_NV 0x8524
#define GL_VARIABLE_C_NV 0x8525
#define GL_VARIABLE_D_NV 0x8526
#define GL_VARIABLE_E_NV 0x8527
#define GL_VARIABLE_F_NV 0x8528
#define GL_VARIABLE_G_NV 0x8529
#define GL_CONSTANT_COLOR0_NV 0x852A
#define GL_CONSTANT_COLOR1_NV 0x852B
#define GL_PRIMARY_COLOR_NV 0x852C
#define GL_SECONDARY_COLOR_NV 0x852D
#define GL_SPARE0_NV 0x852E
#define GL_SPARE1_NV 0x852F
#define GL_DISCARD_NV 0x8530
#define GL_E_TIMES_F_NV 0x8531
#define GL_SPARE0_PLUS_SECONDARY_COLOR_NV 0x8532
#define GL_UNSIGNED_IDENTITY_NV 0x8536
#define GL_UNSIGNED_INVERT_NV 0x8537
#define GL_EXPAND_NORMAL_NV 0x8538
#define GL_EXPAND_NEGATE_NV 0x8539
#define GL_HALF_BIAS_NORMAL_NV 0x853A
#define GL_HALF_BIAS_NEGATE_NV 0x853B
#define GL_SIGNED_IDENTITY_NV 0x853C
#define GL_SIGNED_NEGATE_NV 0x853D
#define GL_SCALE_BY_TWO_NV 0x853E
#define GL_SCALE_BY_FOUR_NV 0x853F
#define GL_SCALE_BY_ONE_HALF_NV 0x8540
#define GL_BIAS_BY_NEGATIVE_ONE_HALF_NV 0x8541
#define GL_COMBINER_INPUT_NV 0x8542
#define GL_COMBINER_MAPPING_NV 0x8543
#define GL_COMBINER_COMPONENT_USAGE_NV 0x8544
#define GL_COMBINER_AB_DOT_PRODUCT_NV 0x8545
#define GL_COMBINER_CD_DOT_PRODUCT_NV 0x8546
#define GL_COMBINER_MUX_SUM_NV 0x8547
#define GL_COMBINER_SCALE_NV 0x8548
#define GL_COMBINER_BIAS_NV 0x8549
#define GL_COMBINER_AB_OUTPUT_NV 0x854A
#define GL_COMBINER_CD_OUTPUT_NV 0x854B
#define GL_COMBINER_SUM_OUTPUT_NV 0x854C
#define GL_MAX_GENERAL_COMBINERS_NV 0x854D
#define GL_NUM_GENERAL_COMBINERS_NV 0x854E
#define GL_COLOR_SUM_CLAMP_NV 0x854F
#define GL_COMBINER0_NV 0x8550
#define GL_COMBINER1_NV 0x8551
#define GL_COMBINER2_NV 0x8552
#define GL_COMBINER3_NV 0x8553
#define GL_COMBINER4_NV 0x8554
#define GL_COMBINER5_NV 0x8555
#define GL_COMBINER6_NV 0x8556
#define GL_COMBINER7_NV 0x8557
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glCombinerParameterfvNV
typedef void (APIENTRY * PFNGLUXCOMBINERPARAMETERFVNVPROC) (GLenum pname, const GLfloat *params);
#endif
#ifndef __GLUX__GLFCT_glCombinerParameterfNV
typedef void (APIENTRY * PFNGLUXCOMBINERPARAMETERFNVPROC) (GLenum pname, GLfloat param);
#endif
#ifndef __GLUX__GLFCT_glCombinerParameterivNV
typedef void (APIENTRY * PFNGLUXCOMBINERPARAMETERIVNVPROC) (GLenum pname, const GLint *params);
#endif
#ifndef __GLUX__GLFCT_glCombinerParameteriNV
typedef void (APIENTRY * PFNGLUXCOMBINERPARAMETERINVPROC) (GLenum pname, GLint param);
#endif
#ifndef __GLUX__GLFCT_glCombinerInputNV
typedef void (APIENTRY * PFNGLUXCOMBINERINPUTNVPROC) (GLenum stage, GLenum portion, GLenum variable, GLenum input, GLenum mapping, GLenum componentUsage);
#endif
#ifndef __GLUX__GLFCT_glCombinerOutputNV
typedef void (APIENTRY * PFNGLUXCOMBINEROUTPUTNVPROC) (GLenum stage, GLenum portion, GLenum abOutput, GLenum cdOutput, GLenum sumOutput, GLenum scale, GLenum bias, GLboolean abDotProduct, GLboolean cdDotProduct, GLboolean muxSum);
#endif
#ifndef __GLUX__GLFCT_glFinalCombinerInputNV
typedef void (APIENTRY * PFNGLUXFINALCOMBINERINPUTNVPROC) (GLenum variable, GLenum input, GLenum mapping, GLenum componentUsage);
#endif
#ifndef __GLUX__GLFCT_glGetCombinerInputParameterfvNV
typedef void (APIENTRY * PFNGLUXGETCOMBINERINPUTPARAMETERFVNVPROC) (GLenum stage, GLenum portion, GLenum variable, GLenum pname, GLfloat *params);
#endif
#ifndef __GLUX__GLFCT_glGetCombinerInputParameterivNV
typedef void (APIENTRY * PFNGLUXGETCOMBINERINPUTPARAMETERIVNVPROC) (GLenum stage, GLenum portion, GLenum variable, GLenum pname, GLint *params);
#endif
#ifndef __GLUX__GLFCT_glGetCombinerOutputParameterfvNV
typedef void (APIENTRY * PFNGLUXGETCOMBINEROUTPUTPARAMETERFVNVPROC) (GLenum stage, GLenum portion, GLenum pname, GLfloat *params);
#endif
#ifndef __GLUX__GLFCT_glGetCombinerOutputParameterivNV
typedef void (APIENTRY * PFNGLUXGETCOMBINEROUTPUTPARAMETERIVNVPROC) (GLenum stage, GLenum portion, GLenum pname, GLint *params);
#endif
#ifndef __GLUX__GLFCT_glGetFinalCombinerInputParameterfvNV
typedef void (APIENTRY * PFNGLUXGETFINALCOMBINERINPUTPARAMETERFVNVPROC) (GLenum variable, GLenum pname, GLfloat *params);
#endif
#ifndef __GLUX__GLFCT_glGetFinalCombinerInputParameterivNV
typedef void (APIENTRY * PFNGLUXGETFINALCOMBINERINPUTPARAMETERIVNVPROC) (GLenum variable, GLenum pname, GLint *params);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glCombinerParameterfvNV
extern PFNGLUXCOMBINERPARAMETERFVNVPROC glCombinerParameterfvNV;
#endif
#ifndef __GLUX__GLFCT_glCombinerParameterfNV
extern PFNGLUXCOMBINERPARAMETERFNVPROC glCombinerParameterfNV;
#endif
#ifndef __GLUX__GLFCT_glCombinerParameterivNV
extern PFNGLUXCOMBINERPARAMETERIVNVPROC glCombinerParameterivNV;
#endif
#ifndef __GLUX__GLFCT_glCombinerParameteriNV
extern PFNGLUXCOMBINERPARAMETERINVPROC glCombinerParameteriNV;
#endif
#ifndef __GLUX__GLFCT_glCombinerInputNV
extern PFNGLUXCOMBINERINPUTNVPROC glCombinerInputNV;
#endif
#ifndef __GLUX__GLFCT_glCombinerOutputNV
extern PFNGLUXCOMBINEROUTPUTNVPROC glCombinerOutputNV;
#endif
#ifndef __GLUX__GLFCT_glFinalCombinerInputNV
extern PFNGLUXFINALCOMBINERINPUTNVPROC glFinalCombinerInputNV;
#endif
#ifndef __GLUX__GLFCT_glGetCombinerInputParameterfvNV
extern PFNGLUXGETCOMBINERINPUTPARAMETERFVNVPROC glGetCombinerInputParameterfvNV;
#endif
#ifndef __GLUX__GLFCT_glGetCombinerInputParameterivNV
extern PFNGLUXGETCOMBINERINPUTPARAMETERIVNVPROC glGetCombinerInputParameterivNV;
#endif
#ifndef __GLUX__GLFCT_glGetCombinerOutputParameterfvNV
extern PFNGLUXGETCOMBINEROUTPUTPARAMETERFVNVPROC glGetCombinerOutputParameterfvNV;
#endif
#ifndef __GLUX__GLFCT_glGetCombinerOutputParameterivNV
extern PFNGLUXGETCOMBINEROUTPUTPARAMETERIVNVPROC glGetCombinerOutputParameterivNV;
#endif
#ifndef __GLUX__GLFCT_glGetFinalCombinerInputParameterfvNV
extern PFNGLUXGETFINALCOMBINERINPUTPARAMETERFVNVPROC glGetFinalCombinerInputParameterfvNV;
#endif
#ifndef __GLUX__GLFCT_glGetFinalCombinerInputParameterivNV
extern PFNGLUXGETFINALCOMBINERINPUTPARAMETERIVNVPROC glGetFinalCombinerInputParameterivNV;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
