
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:30 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_APPLE_element_array__
#define __GLUX_GL_APPLE_element_array__

GLUX_NEW_PLUGIN(GL_APPLE_element_array);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_ELEMENT_ARRAY_APPLE 0x8768
#define GL_ELEMENT_ARRAY_TYPE_APPLE 0x8769
#define GL_ELEMENT_ARRAY_POINTER_APPLE 0x876A
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glElementPointerAPPLE
typedef void (APIENTRY * PFNGLUXELEMENTPOINTERAPPLEPROC) (GLenum type, const GLvoid *pointer);
#endif
#ifndef __GLUX__GLFCT_glDrawElementArrayAPPLE
typedef void (APIENTRY * PFNGLUXDRAWELEMENTARRAYAPPLEPROC) (GLenum mode, GLint first, GLsizei count);
#endif
#ifndef __GLUX__GLFCT_glDrawRangeElementArrayAPPLE
typedef void (APIENTRY * PFNGLUXDRAWRANGEELEMENTARRAYAPPLEPROC) (GLenum mode, GLuint start, GLuint end, GLint first, GLsizei count);
#endif
#ifndef __GLUX__GLFCT_glMultiDrawElementArrayAPPLE
typedef void (APIENTRY * PFNGLUXMULTIDRAWELEMENTARRAYAPPLEPROC) (GLenum mode, const GLint *first, const GLsizei *count, GLsizei primcount);
#endif
#ifndef __GLUX__GLFCT_glMultiDrawRangeElementArrayAPPLE
typedef void (APIENTRY * PFNGLUXMULTIDRAWRANGEELEMENTARRAYAPPLEPROC) (GLenum mode, GLuint start, GLuint end, const GLint *first, const GLsizei *count, GLsizei primcount);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glElementPointerAPPLE
extern PFNGLUXELEMENTPOINTERAPPLEPROC glElementPointerAPPLE;
#endif
#ifndef __GLUX__GLFCT_glDrawElementArrayAPPLE
extern PFNGLUXDRAWELEMENTARRAYAPPLEPROC glDrawElementArrayAPPLE;
#endif
#ifndef __GLUX__GLFCT_glDrawRangeElementArrayAPPLE
extern PFNGLUXDRAWRANGEELEMENTARRAYAPPLEPROC glDrawRangeElementArrayAPPLE;
#endif
#ifndef __GLUX__GLFCT_glMultiDrawElementArrayAPPLE
extern PFNGLUXMULTIDRAWELEMENTARRAYAPPLEPROC glMultiDrawElementArrayAPPLE;
#endif
#ifndef __GLUX__GLFCT_glMultiDrawRangeElementArrayAPPLE
extern PFNGLUXMULTIDRAWRANGEELEMENTARRAYAPPLEPROC glMultiDrawRangeElementArrayAPPLE;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
