
// --------------------------------------------------------
// Generated by glux perl script (Wed Jul 23 12:25:37 2003)
// 
// Sylvain Lefebvre - 2002 - Sylvain.Lefebvre@imag.fr
// --------------------------------------------------------
#include "glux_no_redefine.h"
#include "gluxPlugin.h"
#include "glux_ext_defs.h"
// --------------------------------------------------------
//         Plugin creation
// --------------------------------------------------------

#ifndef __GLUX_GL_ARB_point_parameters__
#define __GLUX_GL_ARB_point_parameters__

GLUX_NEW_PLUGIN(GL_ARB_point_parameters);
// --------------------------------------------------------
//           Extension defines
// --------------------------------------------------------
#define GL_POINT_SIZE_MIN_ARB 0x8126
#define GL_POINT_SIZE_MAX_ARB 0x8127
#define GL_POINT_FADE_THRESHOLD_SIZE_ARB 0x8128
#define GL_POINT_DISTANCE_ATTENUATION_ARB 0x8129
// --------------------------------------------------------
//           Extension gl function typedefs
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glPointParameterfARB
typedef void (APIENTRY * PFNGLUXPOINTPARAMETERFARBPROC) (GLenum pname, GLfloat param);
#endif
#ifndef __GLUX__GLFCT_glPointParameterfvARB
typedef void (APIENTRY * PFNGLUXPOINTPARAMETERFVARBPROC) (GLenum pname, const GLfloat *params);
#endif
// --------------------------------------------------------
//           Extension gl functions
// --------------------------------------------------------
#ifndef __GLUX__GLFCT_glPointParameterfARB
extern PFNGLUXPOINTPARAMETERFARBPROC glPointParameterfARB;
#endif
#ifndef __GLUX__GLFCT_glPointParameterfvARB
extern PFNGLUXPOINTPARAMETERFVARBPROC glPointParameterfvARB;
#endif
// --------------------------------------------------------
#endif
// --------------------------------------------------------
