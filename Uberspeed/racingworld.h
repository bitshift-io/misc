#ifndef RACINGWORLD
#define RACINGWORLD

#include "Engine/SiphonGL.h"
#include "checkpoint.h"
#include "car.h"
#include <vector>
//#include "Engine/World.h"

class RacingWorld : public World
{
public:
	virtual bool Load( char* file );
	virtual bool ReadUnknown( Head& header );
	virtual void Update( unsigned int tick );
	virtual void AddPlayer( Car* player );
	virtual int Render( Camera& camera );

protected:
	std::vector< Checkpoint > checkPoints;
	std::vector< Car* > players;
};

#endif