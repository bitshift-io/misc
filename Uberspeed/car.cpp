#include "car.h"
#include "racingworld.h"

#define LENGTH 12	// chassis length
#define WIDTH 9	// chassis width
#define HEIGHT 3	// chassis height

#define CLENGTH 12	//chasis height
#define CWIDTH 10	//chasis height
#define CHEIGHT 3	//chasis height

#define RADIUS 2	// wheel radius
#define STARTZ 8	// starting height of chassis
#define CMASS 1		// chassis mass
#define BMASS 1		// body mass
#define WMASS 20	// wheel mass

Car::Car() : bJustCreated(true)
{
	space = 0;
	lastCheckPoint = 0;
	printf("CREATING A CAR\n");

	//notify obj manager
	if( ObjMgr::RegisterObject( this ) )
	{
		//notify client...
		Client::GetInstance().RegisterNetObject( this );
	}
	//Model::Model();
	
	bJustCreated = true;
}

Vector3 Car::GetPosition()
{
	return mdlCar.position;
}

void Car::SetCheckPoint( int checkpoint, int noPoints )
{
	//is this a new check point?
	//printf("check point id: %i, last: %i\n", checkpoint, (lastCheckPoint + 1) );
	if( checkpoint == (lastCheckPoint + 1) )
	{
		//good!
		//printf("checkpnt!");
		lastCheckPoint = checkpoint;
		lastCheckPointTime = 0;
	}
	else if( checkpoint == lastCheckPoint ) 
	{
		//still inside check point, no problems
	}
	else if( checkpoint == 1 && lastCheckPoint == noPoints )
	{
		//good! we completed a lap!!!
		//printf("lap complete!\n");
		hud.EndLap();
		lapEnd.PlaySound();

		lastCheckPoint = checkpoint;
		lastCheckPointTime = 0;
	}
	
	else
	{
		//printf("checkpnt missed!");
		badLap.PlaySound();
		//bad we missed a check point!
		//printf("BAD BOY!!! You missed a check point!\n");
	}

	//printf("check!");
	
}

bool Car::Initialize()
{
	((RacingWorld*)World::GetPointer())->AddPlayer( this );

	light.SetPosition( Vector3( 0, 400, 0 ) );
	//light.Enable();

	hud.Initialize();

	cameraView = THIRDPERSON;
	camera = &RenderMgr::GetInstance().GetCamera();
	this->camera = camera;
	camera->SetFreeCam( false );

	//max is weird!
	Matrix4 spawn = World::GetInstance().GetSpawn( World::GetInstance().GetRandomSpawn() );
	Vector3 p = Vector3( spawn[13], spawn[14], spawn[15] );

	mdlCar.position = p;

	//mdlCar.position = Vector3( 0, 0, 0);
	prevPos = Vector3( 0, 0, 1);

	mdlCar.SetMesh( FileMgr::GetInstance().LoadMesh("data/car.SML") );
	//FileMgr::GetInstance().LoadMesh("data/car.SML")->BuildShadow();
	mdlCar.mesh->BuildShadow();


/*
	for( int w = 0; w < 4; ++w )
	{
		wheels[w].SetMesh( FileMgr::GetInstance().LoadMesh("data/wheel.SML") );
	}
*/


	dMass m;

	// chassis body
	space = PhysicsMgr::GetInstance().GetSpace();
	body[0] = dBodyCreate( PhysicsMgr::GetInstance().GetWorld() );
	dBodySetPosition( body[0], p[0] + 0, p[1] + STARTZ + CHEIGHT, p[2] + 0 );
	dMassSetBox( &m, 1, LENGTH, WIDTH, HEIGHT );
	dMassAdjust( &m, CMASS );
	dBodySetMass( body[0], &m );
	//box[0] = dCreateSphere( PhysicsMgr::GetInstance().GetSpace(), 6);
	box[0] = dCreateBox( *space, CLENGTH, CWIDTH, CHEIGHT );
	dGeomSetBody( box[0], body[0] );
	

/*
	//body (above the chassis)
	body[5] = dBodyCreate( PhysicsMgr::GetInstance().GetWorld() );
	dBodySetPosition( body[5], 0, 3, 0 );
	dMassSetBox( &m, 1, LENGTH, WIDTH, HEIGHT );
	dMassAdjust( &m, BMASS );
	dBodySetMass( body[5], &m );
	box[0] = dCreateBox( PhysicsMgr::GetInstance().GetSpace(), LENGTH, WIDTH, HEIGHT );
	dGeomSetBody( box[0], body[5] );

	joint[5] = dJointCreateSlider( PhysicsMgr::GetInstance().GetWorld(), 0);
	dJointAttach( joint[5], body[0], body[5] );
	dJointSetSliderAxis (joint[5], 0, 1, 0);*/

	// wheel bodies
	int i;
	for( i=1; i<=4; i++)
	{
		body[i] = dBodyCreate( PhysicsMgr::GetInstance().GetWorld() );
		dQuaternion q;
		dQFromAxisAndAngle(q,1,0,0,M_PI*0.5);
		dBodySetQuaternion( body[i], q );
		dMassSetSphere( &m, 1, RADIUS );
		dMassAdjust( &m, WMASS );
		dBodySetMass( body[i], &m );
		sphere[i-1] = dCreateSphere( *space, RADIUS );
		dGeomSetBody( sphere[i-1],body[i] );
	}
	dBodySetPosition( body[1], p[0] +  0.5*LENGTH, p[1] + STARTZ-HEIGHT*0.5, p[2] + WIDTH*0.5 ); //
	dBodySetPosition( body[2], p[0] +   -0.5*LENGTH, p[1] +STARTZ-HEIGHT*0.5, p[2] +WIDTH*0.5 ); //-HEIGHT*0.5
	dBodySetPosition( body[3], p[0] +   -0.5*LENGTH, p[1] +STARTZ-HEIGHT*0.5, p[2] + -WIDTH*0.5 ); //-HEIGHT*0.5
	dBodySetPosition( body[4], p[0] +  0.5*LENGTH, p[1] + STARTZ-HEIGHT*0.5, p[2] + -WIDTH*0.5 ); //-HEIGHT*0.5

	// front and back wheel hinges
	for( i = 0; i < 4; ++i ) 
	{
		joint[i] = dJointCreateHinge2( PhysicsMgr::GetInstance().GetWorld(), 0 );
		dJointAttach( joint[i], body[0], body[i+1] );
		const dReal *a = dBodyGetPosition( body[i+1] );
		dJointSetHinge2Anchor( joint[i], a[0], a[1], a[2] );
		dJointSetHinge2Axis1( joint[i], 0, 1, 0 );
		dJointSetHinge2Axis2( joint[i], 0, 0, 1 );
	}

	// set joint suspension
	for (i=0; i<4; i++) 
	{
		
		//dJointSetHinge2Param( joint[i], dParamBounce, 0.0002 );
		dJointSetHinge2Param( joint[i], dParamSuspensionERP, 0.4 );
		dJointSetHinge2Param( joint[i], dParamSuspensionCFM, 0.002 );
	}

	// lock back wheels along the steering axis
	for (i=1; i<3; i++)  //for (i=0; i<4; i++) //
	{
		// set stops to make sure wheels always stay in alignment
		dJointSetHinge2Param( joint[i], dParamLoStop, 0 );
		dJointSetHinge2Param( joint[i], dParamHiStop, 0 );


		// the following alternative method is no good as the wheels may get out
		// of alignment:
		//   dJointSetHinge2Param (joint[i],dParamVel,0);
		//   dJointSetHinge2Param (joint[i],dParamFMax,dInfinity);
	}

	// create geometry group and add it to the space
//	geom_group = dCreateGeomGroup( PhysicsMgr::GetInstance().GetSpace() );  
//	dGeomGroupAdd( geom_group, box[0] );
/*	dGeomGroupAdd( geom_group, sphere[0] );
	dGeomGroupAdd( geom_group, sphere[1] );
	dGeomGroupAdd( geom_group, sphere[2] );
*/

	//amke sure opanal is intit
	SoundListener::GetInstance().SetListenerPosition(0,0,0);

	motor.LoadSound("data/audio/car_moving.wav", true);
	motor.PlaySound();
	motor.SetProperties(0, 0, 0, 0, 0, 0);

	horn.LoadSound("data/audio/horn_03.wav", false);
	brake.LoadSound("data/audio/car_skid.wav", true);
	idle.LoadSound("data/audio/car_idle.wav", true);
	lapEnd.LoadSound("data/audio/ojective_checkpoint.wav", false );
	badLap.LoadSound("data/audio/objective_wrongway.WAV", false );

	//test.Load("data/grass.dds");

	return true;
}

void Car::Update( unsigned long tick )
{
	if( bJustCreated )
	{
		bJustCreated = false;
		Initialize();
	}
	//return;
	Vector3 prev;

	if( ownerId == -1 )
	{
		hud.Update( tick );
	}

	Matrix4 worldInverse = model;
	worldInverse.Inverse();
	Vector3 carLocal = worldInverse * light.GetPosition();
	mdlCar.mesh->CreateShadow( carLocal );

	//lastCheckPointTime += tick * 20; //GAME_UPDATE;
	//lapTime += tick * 20;
	//printf("LAP TIME: %i\n", lapTime );

	//if we are owner
	if( ownerId == -1 )
	{

		if( Input::GetInstance().KeyPressed(SDLK_v) || Input::GetInstance().KeyPressed(SDLK_c) )
		{
			++cameraView;

			if( cameraView > 1 )
				cameraView = 0;
		}

		bool accelerate = false;
		if( Input::GetInstance().KeyDown(SDLK_r) )
		{
			//reset
			const dReal* mpos = dBodyGetPosition( body[0] );

			//for( int k = 0; k < 5; k++ )
			//{
				dBodySetPosition(body[0], mpos[0], mpos[1] + 2, mpos[2] );
			//}
			
			dQuaternion q;
			dQFromAxisAndAngle(q,1,0,0, 0); //M_PI*0.5
			dBodySetQuaternion( body[0], q );
		}

		if( Input::GetInstance().KeyDown(SDLK_UP) )
		{	
			accelerate = true;
			motor.PlaySound();
			idle.StopSound();

			if( speed <= 15 )
				speed += 1;
		}
		else if( Input::GetInstance().KeyDown(SDLK_DOWN) )
		{
			accelerate = true;
			motor.PlaySound();
			idle.StopSound();

			if( speed >= -15 )
				speed -= 1;
		}
		else
		{
			motor.StopSound();
			idle.PlaySound();

			if( speed > 0 )
				speed -= 1;
			else if( speed < 0 )
				speed += 1;
		}

		if( Input::GetInstance().KeyDown(SDLK_RIGHT) )
		{/*
			if( steer <= 0.3 )
			{
				steer = 0.3;
			}*/
			if( steer <= 0.4 )
			{
				steer += 0.1;
				cameraSteer += 0.1;

				//reduce speed while turning
				if( speed >= 5)
					speed -= 1;
			}
			else
			{
				steer = 0.4;
				cameraSteer = 0.4;
			}


			/*
			if( steer < 0.9 )
				if( steer <= 0.5 )
				{
					steer += 0.2;
				}
				else
				{
					steer = 0.5;
				}*/
		}
		else if( Input::GetInstance().KeyDown(SDLK_LEFT) )
		{/*
			if( steer >= -0.3 )
			{
				steer = -0.3;
			}*/
			if( steer >= -0.4 )
			{
				steer -= 0.1;
				cameraSteer -= 0.1;
				
				//reduce speed while turning
				if( speed >= 5)
					speed -= 1;
			}
			else
			{
				steer = -0.4;
				cameraSteer = -0.4;
			}

			//steer = -0.8;
			/*
			if( steer > -0.9 )
				if( steer >= -0.5 )
				{	
					steer -= 0.2;
				}
				else
				{
					steer = -0.5;
				}*/
		}
		else
		{
			if( cameraSteer > 0.1 )
				cameraSteer -= 0.1;
			else if(cameraSteer < -0.1 )
				cameraSteer += 0.1;

			steer = 0;
			brake.StopSound();
		}

		if( Input::GetInstance().KeyDown(SDLK_RETURN) )
		{
			horn.SetProperties( mdlCar.position[X], mdlCar.position[Y], mdlCar.position[Z], 0, 0, 0);
			horn.PlaySound();	
		}

		if( Input::GetInstance().KeyDown(SDLK_SPACE) )
		{
			speed = 0;

			brake.SetProperties( mdlCar.position[X], mdlCar.position[Y], mdlCar.position[Z], 0, 0, 0);
			brake.PlaySound();		
		}
		else
		{
			brake.StopSound();
		}

		// motor
		for( int k = 0; k < 4; k++ )
		{
			dJointSetHinge2Param(joint[k],dParamVel2,speed );
			dJointSetHinge2Param(joint[k],dParamFMax2, 180.0);
		}

		// steering
		dReal v = steer;// - dJointGetHinge2Angle1(joint[0]);
		v *= 10.0;

		dJointSetHinge2Param(joint[1],dParamVel, -dJointGetHinge2Angle1(joint[1]) );
		dJointSetHinge2Param(joint[1],dParamFMax,100);
		dJointSetHinge2Param(joint[2],dParamVel, -dJointGetHinge2Angle1(joint[2]) );
		dJointSetHinge2Param(joint[2],dParamFMax,100);

		//printf("speed: %f steer: %f\n", speed, v );
		dJointSetHinge2Param(joint[0],dParamVel, steer-dJointGetHinge2Angle1(joint[0]) );
		dJointSetHinge2Param(joint[0],dParamFMax,100);

		dJointSetHinge2Param(joint[3],dParamVel, steer-dJointGetHinge2Angle1(joint[3]) );
		dJointSetHinge2Param(joint[3],dParamFMax,100);

		prev = mdlCar.position;

	}



	//if( ownerId == -1 )
	//{
		const dReal* pos = dBodyGetPosition( body[0] );
		mdlCar.position = Vector3( pos[0], pos[1], pos[2] );

		//printf("car position: %f %f %f\n", pos[0], pos[1], pos[2] );
/*
	if( ownerId == -1 )
	{
		//char cPos[256];
		//sprintf(cPos, "%i %i %i", (int)pos[0], (int)pos[1], (int)pos[2] );
		Send( &mdlCar.position, sizeof(float) * 3 );
		//printf("message sending: %s\n", cPos );
	}*/
	

		motor.SetProperties( mdlCar.position[0], mdlCar.position[1], mdlCar.position[2], 0, 0, 0);
		idle.SetProperties( mdlCar.position[0], mdlCar.position[1], mdlCar.position[2], 0, 0, 0);
		horn.SetProperties( mdlCar.position[0], mdlCar.position[1], mdlCar.position[2], 0, 0, 0);
		brake.SetProperties( mdlCar.position[0], mdlCar.position[1], mdlCar.position[2], 0, 0, 0);

		lapEnd.SetProperties( this->camera->position[0], this->camera->position[1], this->camera->position[2], 0, 0, 0);
		badLap.SetProperties( this->camera->position[0], this->camera->position[1], this->camera->position[2], 0, 0, 0);


	//}


	if( ownerId == -1 )
	{
		//transform our lil car
		model.LoadIdentity();
		model.Translate( mdlCar.position[X], mdlCar.position[Y] - 5, mdlCar.position[Z] );
		float* f = (float*)dBodyGetRotation(body[0]);
		model.ODELoadRotation4x3( f );

	//	printf("Car: %f %f %f\n", pos[0], pos[1], pos[2]);
		
		switch( cameraView )
		{
		case THIRDPERSON:
			{				
				//if we are turning left or right, let camera swing left or right
				//according to steer

				//prev
				// - (speed/2);

				//this->camera->up = Vector3( model[1], model[5], model[9] );
				//this->camera->right = Vector3( model[2], model[6], model[10] );
				//this->camera->view = Vector3( model[0], -model[4], -model[8] );

				this->camera->view = Vector3( -model[0], -model[1], -model[2] );
				this->camera->up = Vector3( 0, 1, 0 ); //Vector3( model[4], model[5], model[6] );
				this->camera->right = this->camera->view.CrossProduct( this->camera->up ); //Vector3( model[8], model[9], model[10] );
				this->camera->right.Normalize();
				
				this->camera->view = this->camera->up.CrossProduct( this->camera->right );
				this->camera->view.Normalize();

				this->camera->position = mdlCar.position + (this->camera->view * 35);// - (Vector3( model[0], model[4], model[8] ) * 30);// * (30 + (speed/2));
				this->camera->position[Y] += 20;

				
				/*
				this->camera->right = Vector3( model[2], model[6], model[10] );
				//right.Normalize();
				//this->camera->position = this->camera->position + (right * (cameraSteer * 10));

				Vector3 v = mdlCar.position - this->camera->position;
				v.Normalize();
				v[0] = -v[0];
				v[1] = -v[1];
				v[2] = -v[1];

				this->camera->view = v;//Vector3( model[0], model[1], model[2] );*/

			}
			break;


		case FIRSTPERSON:
			{
				this->camera->view = Vector3( -model[0], -model[1], -model[2] );
				this->camera->up = Vector3( model[4], model[5], model[6] );
				this->camera->right = Vector3( model[8], model[9], model[10] );

				this->camera->position = mdlCar.position;
				this->camera->position = this->camera->position + (this->camera->up * 4); //raise camera a bit
				
			}
			break;
		}
		RenderMgr::GetInstance().GetCamera().Transform();

		SoundListener::GetInstance().SetListenerPosition( this->camera->position[0],
					this->camera->position[1], this->camera->position[2] );
		SoundListener::GetInstance().SetListenerOrientation( 
			this->camera->view[0],this->camera->view[1],this->camera->view[2],0,1,0);
	}

/*
	//remove from old space
	dSpaceRemove( *space, box[0] );
	for( int i = 0; i < 4; ++i )
		dSpaceRemove( *space, sphere[i] );
*/
	//add car to spaces
	dSpaceID* temp = World::GetInstance().AddToSpace( mdlCar.position );
	if( space != temp && temp != 0 )
	{
		if( space != 0 )
		{
			//remove from old space
			dSpaceRemove( *space, box[0] );
			for( int i = 0; i < 4; ++i )
				dSpaceRemove( *space, sphere[i] );
		}

		space = temp;
		dSpaceAdd( *space, box[0] );
		for(int  i = 0; i < 4; ++i )
			dSpaceAdd( *space, sphere[i] );
	}

	RenderMgr::GetInstance().Que( this );

	//printf("pos: %f\n", mdlCar.position[Y] );
}

int Car::Render( Camera& camera )
{


	if( ownerId == -1 )
	{
		if( cameraView == FIRSTPERSON )
		{
			hud.Render( camera );
			return 0;
		}

	//	glLoadIdentity();
		

		glLoadIdentity();
		camera.LookAt();
		Matrix4 cam;
		cam.LoadCurrent();

		Matrix4 trans = model * cam;
	//	glLoadIdentity();
		trans.Set();
	}
	else
	{
		glLoadIdentity();
		RenderMgr::GetInstance().GetCamera().LookAt();
		glPushMatrix();
		model.SetM();	
	}

/*
	if( mdlCar.mesh->defaultMaterial != 0 )
	{
		mdlCar.mesh->defaultMaterial->SetMaterial();
	}*/

	//glBindTexture( GL_TEXTURE_2D, test.GetTexture() );


	mdlCar.mesh->Render();

	if( ownerId != -1 )
	{
		glPopMatrix();			
	}
	else
	{
		hud.Render( camera );
	}
	

//	Model::Re
	return 0;
}

//---------------net code--------------------


bool Car::Recive( void* message, int length )
{

		/**
 * 4x4 matrix 
 *
 * m[0]  m[4]  m[ 8]  m[12]
 * m[1]  m[5]  m[ 9]  m[13]
 * m[2]  m[6]  m[10]  m[14]
 * m[3]  m[7]  m[11]  m[15]
 *
 *   x axis - column 0
 *  y axis - column 1
 *   z axis - column 3
 */
	model.Load( (float*)message );
	dBodySetPosition(body[0], model[12], model[13], model[14] );

	/*
	//mdlCar.position[0] = info[0];
	
	dBodySetPosition(body[0], info[0], info[1], info[2] );
	mdlCar.position[0] = info[0];
	mdlCar.position[1] = info[1];
	mdlCar.position[2] = info[2];

	printf("setting car position to: %f %f %f\n", info[0], info[1], info[2]);
	//sscanf( (char*)message, "%f %f %f", mdlCar.position[0], mdlCar.position[1], mdlCar.position[1] );
	//dBodySetPosition( body[0], mdlCar.position[0], mdlCar.position[1], mdlCar.position[2] );
*/
	//printf("Recived a new position: %f %f %f", mdlCar.position[0], mdlCar.position[1], mdlCar.position[1] );
	return true;
}

bool Car::Send()
{
	if( ownerId == -1 )
	{
		//send cars position,
		//and the positions/rotation of the 
		Client::GetInstance().SendReliable( &model.m, sizeof(float) * 16, this, TO_ALL );
	}
	
	return true;
}

char* Car::GetClassId()
{
	printf("returning 'chat' as classid\n");
	return "car";
}

bool Car::SharedNetObj()
{
	return false;
}
