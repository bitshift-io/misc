#ifndef _SIPHON_NETOBJ_
#define _SIPHON_NETOBJ_

class Client;

class NetObj
{
public:
	virtual bool Send( void* message, int length ) = 0;
	virtual bool Recive( void* message, int length ) = 0;
	virtual char* GetClassId() = 0;
	/*{
		printf("this will return netobj, calling getclassid\n");
		return "netobj";
	}*/

	void SetNetId( int netId )
	{
		this->netId = netId;
	}

	bool IsNetObject()
	{
		printf("this netobj check has been called\n");
		return true;
	}

	virtual void AnotherFn()
	{
		printf("Another fn called from NetObj\n");
	}
//protected:
	friend class Client;

	int ownerId; // to identify who is in control of this obj
	int netId; //to identify this object on netowkred pc's
};

#endif

