#include "WorldExtras.h"

bool SkyBox::Create( int size, char* texTop, char* texBottom, char* texLeft,
	char* texRight, char* texFront, char* texBack )
{
	glColor3f(1.0f,1.0f,1.0f);

	textures[BACK].Load( texBack );
	textures[RIGHT].Load( texRight );
	textures[FRONT].Load( texFront );
	textures[TOP].Load( texTop );
	textures[BOTTOM].Load( texBottom );
	textures[LEFT].Load( texLeft );

	model = glGenLists(1);
	glNewList( model, GL_COMPILE );

	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);

	// BACK ( -Z )
	textures[BACK].SetTexture();

	glBegin(GL_QUADS);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( size, size, -size );

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( size, -size, -size );

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f( -size, -size, -size );

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f( -size, size, -size );
	glEnd();

	// FRONT ( +Z )
	textures[FRONT].SetTexture();

	glBegin(GL_QUADS);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( -size, size, size );

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( -size, -size, size );

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f( size, -size, size );

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f( size, size, size );
	glEnd();

	// TOP ( +Y )
	textures[TOP].SetTexture();

	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f( size, size, size );

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f( size, size, -size );

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( -size, size, -size );

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( -size, size, size );
	glEnd();

	// BOTTOM ( -Y )
	textures[BOTTOM].SetTexture();

	glBegin(GL_QUADS);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( -size, -size, size );

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( -size, -size, -size );

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f( size, -size, -size );

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f( size, -size, size );
	glEnd();

	// LEFT ( -X );
	textures[LEFT].SetTexture();

	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f( -size, size, size );

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( -size, size, -size );

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( -size, -size, -size );

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f( -size, -size, size );
	glEnd();

	// RIGHT ( +X )
	textures[RIGHT].SetTexture();

	glBegin(GL_QUADS);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( size, -size, size );

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f( size, -size, -size );

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f( size, size, -size );

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( size, size, size );
	glEnd();

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glEndList();

	faces = 6 * 2; //6 quads, of 2 polys

	return true;
}

int SkyBox::Render()
{
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	//glLoadIdentity();

	glTranslatef( position[X], position[Y], position[Z] );
	glRotatef( rotation[PITCH], 1, 0, 0 );
	glRotatef( rotation[YAW], 0, 1, 0 );
	glRotatef( rotation[ROLL], 0, 0, 1);

	glCallList( model );

	glPopMatrix();
	return faces;
}
