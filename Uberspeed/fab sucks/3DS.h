#ifndef _3DS_H
#define _3DS_H

// Primary Chunk, at the beginning of each file
#define PRIMARY       0x4D4D

// Main Chunks
#define OBJECTINFO    0x3D3D				 
#define VERSION       0x0002				
#define EDITKEYFRAME  0xB000				

// Sub defines of OBJECTINFO
#define MATERIAL	  0xAFFF				
#define OBJECT		  0x4000				

// Sub defines of MATERIAL
#define MATNAME       0xA000				
#define MATDIFFUSE    0xA020				
#define MATMAP        0xA200
#define MATMAPFILE    0xA300				

#define OBJECT_MESH   0x4100				

// Sub defines of OBJECT_MESH
#define OBJECT_VERTICES     0x4110			
#define OBJECT_FACES		0x4120			
#define OBJECT_MATERIAL		0x4130			
#define OBJECT_UV			0x4140			

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>

#include <vector>
#include "Math3D.h"

//////////////////////////////////////
//The tFace Struct
//////////////////////////////////////
struct tFace
{
	int vertIndex[3];
	int coordIndex[3];
};

#pragma pack(push,1)
struct tFacePlain
{
	unsigned int vertIndex[3];
};

struct uvCoord
{
	float vertIndex[2];
};
#pragma pop

//////////////////////////////////////
//The tMaterialInfo Struct
//////////////////////////////////////
struct tMaterialInfo
{
	char  strName[255];							
	char  strFile[255];							
	char  color[3];
	int   texureId;								
	float uTile;								
	float vTile;								
	float uOffset;								
	float vOffset;
};

//////////////////////////////////////
//The t3DObject Struct
//////////////////////////////////////
class t3DObject
{
public:
	int  numOfVerts;
	int  numOfFaces;
	int  numTexVertex;
	int  materialID;
	bool bHasTexture;
	char strName[255];			
	tFacePlain      *pIndices;
	Vector3  *pVerts;
	uvCoord *pUVCoords;

	Vector3  *pNormals;
	Vector3  *pTexVerts;
	tFace *pFaces;

	t3DObject()
	{
		numOfVerts = 0;
		numOfFaces = 0;
		numTexVertex = 0;
		materialID = 0;
		bHasTexture = true;
		pIndices = 0;
		pVerts = 0;
		pNormals = 0;
		pTexVerts = 0;
		pFaces  = 0;
	}


};

//////////////////////////////////////
//The t3DModel Struct
//////////////////////////////////////
class t3DModel
{
public:
	GLuint model;

	void ProcessDiplayList()
	{
		model = glGenLists(1);
		glNewList( model, GL_COMPILE );

		std::vector<t3DObject>::iterator ObjIt;

		//for(int i = 0; i < numOfObjects; i++)
		for( ObjIt = pObject.begin(); ObjIt != pObject.end(); ++ObjIt )
		{

			//if(pObject.size() <= 0) break;

			//t3DObject *pObject = &pObject[i];

			if(ObjIt->bHasTexture)
			{/*
				glEnable(GL_TEXTURE_2D);

				glColor3ub(255, 255, 255);

				glBindTexture(GL_TEXTURE_2D, TextureArray3ds[pObject->materialID]);
				*/
			}
			else
			{
				//glDisable(GL_TEXTURE_2D);

				//glColor3ub(255, 255, 255);
			}

			//should be done with vertex array!
			glBegin(GL_TRIANGLES);

			//polys += ObjIt->numOfFaces;
			//glVertexPointer(3, GL_FLOAT, sizeof(Vector3), ObjIt->pVerts);
			//glTexCoordPointer(2, GL_FLOAT, sizeof(Vector3), ObjIt->pTexVerts);
		        //glDrawElements(GL_TRIANGLES, ObjIt->numOfFaces, GL_UNSIGNED_INT, ObjIt->pIndices);

			for(int j = 0; j < ObjIt->numOfFaces; j++)
			{

				for(int whichVertex = 0; whichVertex < 3; whichVertex++)
				{

					int index = ObjIt->pFaces[j].vertIndex[whichVertex];
/*
					glNormal3f(ObjIt->pNormals[ index ][X], ObjIt->pNormals[ index ][Y], ObjIt->pNormals[ index ][Z]);
*/

					if(ObjIt->bHasTexture)
					{

						if(ObjIt->pTexVerts) {
							glTexCoord2f(ObjIt->pTexVerts[ index ][X], ObjIt->pTexVerts[ index ][Y]);
						}
					}
					 else
					 {

						if(pMaterials.size() < ObjIt->materialID)
						{
							char *pColor = pMaterials[ObjIt->materialID].color;

							glColor3ub(pColor[0], pColor[1], pColor[2]);
						}
					}

					glVertex3f(ObjIt->pVerts[ index ][X], ObjIt->pVerts[ index ][Y], ObjIt->pVerts[ index ][Z]);
				}
			}

			glEnd();
		}

		glEndList();
	}

	void ProcessFaces()
	{
		//im lazy ;p
		//ProcessDiplayList();

		std::vector<t3DObject>::iterator ObjIt;

		for( ObjIt = pObject.begin(); ObjIt != pObject.end(); ++ObjIt )
		{
			printf("no faces: %i\n",ObjIt->numOfFaces);
			ObjIt->pIndices = new tFacePlain[ ObjIt->numOfFaces ];

			for( int i = 0; i < (ObjIt->numOfFaces); ++i )
			{
				for( int j = 0; j < 3; ++j )
					ObjIt->pIndices[i].vertIndex[j] = (unsigned int)ObjIt->pFaces[i].vertIndex[j];
			}

			ObjIt->pUVCoords = new uvCoord[ ObjIt->numOfVerts ];

			for( int k = 0; k < ObjIt->numOfFaces; ++k )
			{

				for( int j = 0; j < 2; ++j )
				{
					int idx = ObjIt->pFaces[k].vertIndex[j];
					int idx2 = ObjIt->pIndices[k].vertIndex[j];
					ObjIt->pUVCoords[ idx2 ].vertIndex[j] = ObjIt->pTexVerts[ idx ][j];
				}
			}
		}
	}

	int Render()
	{
		//glCallList( model );

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		//glEnableClientState(GL_VERTEX_ARRAY);

		int polys = 0;
		std::vector<t3DObject>::iterator ObjIt;

		//for(int i = 0; i < numOfObjects; i++)
		for( ObjIt = pObject.begin(); ObjIt != pObject.end(); ++ObjIt )
		{

			//if(pObject.size() <= 0) break;

			//t3DObject *pObject = &pObject[i];

			if(ObjIt->bHasTexture)
			{/*
				glEnable(GL_TEXTURE_2D);

				glColor3ub(255, 255, 255);

				glBindTexture(GL_TEXTURE_2D, TextureArray3ds[pObject->materialID]);
				*/
			}
			else
			{
				//glDisable(GL_TEXTURE_2D);

				//glColor3ub(255, 255, 255);
			}

			//should be done with vertex array!
			//glBegin(GL_TRIANGLES);

			polys += ObjIt->numOfFaces;
			glVertexPointer(3, GL_FLOAT, sizeof(Vector3), ObjIt->pVerts);
			glTexCoordPointer(2, GL_FLOAT, sizeof(Vector3), ObjIt->pTexVerts);
		        glDrawElements(GL_TRIANGLES, ObjIt->numOfFaces, GL_UNSIGNED_INT, ObjIt->pIndices);
/*
			for(int j = 0; j < ObjIt->numOfFaces; j++)
			{

				for(int whichVertex = 0; whichVertex < 3; whichVertex++)
				{

					int index = ObjIt->pFaces[j].vertIndex[whichVertex];
/*
					glNormal3f(ObjIt->pNormals[ index ][X], ObjIt->pNormals[ index ][Y], ObjIt->pNormals[ index ][Z]);
* /

					if(ObjIt->bHasTexture)
					{

						if(ObjIt->pTexVerts) {
							glTexCoord2f(ObjIt->pTexVerts[ index ][X], ObjIt->pTexVerts[ index ][Y]);
						}
					}
					 else
					 {

						if(pMaterials.size() < ObjIt->materialID)
						{
							char *pColor = pMaterials[ObjIt->materialID].color;

							glColor3ub(pColor[0], pColor[1], pColor[2]);
						}
					}

					glVertex3f(ObjIt->pVerts[ index ][X], ObjIt->pVerts[ index ][Y], ObjIt->pVerts[ index ][Z]);
				}
			}*/

			//glEnd();
		}


		return polys;
	}

	int numOfObjects;							
	int numOfMaterials;							
	std::vector<tMaterialInfo> pMaterials;
	std::vector<t3DObject> pObject;

};





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Here is our structure for our 3DS indicies (since .3DS stores 4 unsigned shorts)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct tIndices {							

	unsigned short a, b, c, bVisible;		
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This holds the chunk info
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct tChunk
{
	unsigned short int ID;					
	unsigned int length;					
	unsigned int bytesRead;					
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This class handles all of the loading code
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class CLoad3DS
{
public:
	CLoad3DS();

	bool Import3DS(t3DModel *pModel, char *strFileName);

private:
	int GetString(char *);

	void ReadChunk(tChunk *);

	void ProcessNextChunk(t3DModel *pModel, tChunk *);

	void ProcessNextObjectChunk(t3DModel *pModel, t3DObject *pObject, tChunk *);

	void ProcessNextMaterialChunk(t3DModel *pModel, tChunk *);

	void ReadColorChunk(tMaterialInfo *pMaterial, tChunk *pChunk);

	void ReadVertices(t3DObject *pObject, tChunk *);

	void ReadVertexIndices(t3DObject *pObject, tChunk *);

	void ReadUVCoordinates(t3DObject *pObject, tChunk *);

	void ReadObjectMaterial(t3DModel *pModel, t3DObject *pObject, tChunk *pPreviousChunk);
	
	void ComputeNormals(t3DModel *pModel);

	void CleanUp();
	
	FILE *m_FilePointer;
	
	tChunk *m_CurrentChunk;
	tChunk *m_TempChunk;
};


#endif



