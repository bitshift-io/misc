/*************************************************
 * SiphonGL v3.0
 *
 * Fabian Mathews
 * 2004 Copyright
 * supagu64@yahoo.com
 **************************************************/

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <GL/glut.h>
#include <string>


#define OPENGL_WIDTH 24
#define OPENGL_HEIGHT 13

bool InString(char *string, const char *search)
{
	int pos=0;
	int maxpos=strlen(search)-1;
	int len=strlen(string);
	char *other;
	for (int i=0; i<len; i++) {
		if ((i==0) || ((i>1) && string[i-1]=='\n')) {				// New Extension Begins Here!
			other=&string[i];
			pos=0;													// Begin New Search
			while (string[i]!='\n') {								// Search Whole Extension-String
				if (string[i]==search[pos]) pos++;					// Next Position
				if ((pos>maxpos) && string[i+1]=='\n') return true; // We Have A Winner!
				i++;
			}
		}
	}
	return false;													// Sorry, Not Found!
}

class TGALoader
{
public:
	#pragma pack(push,1)
	typedef struct TGAHead
        {
		char identsize;
		char colorType;
		char imageType; //compressed if 10, uncom if 2
		short mapstart;
		short length;
		char mapBits;
		short xstart;
		short ystart;
		short width;
		short height;
		char bpp;
		char descritor;
        } TGAHead;
	#pragma pop

protected:
	TGAHead header;
	GLuint texture;

	bool LoadUncompresed( FILE* pFile, TGAHead& header )
	{
		if( header.width <= 0 || header.height <= 0 || header.bpp < 24 )
		{
			printf("something wrong\n");
			return false;
		}

		char* image = 0;
		unsigned int bytesPerPixel = header.bpp / 8;

		image = (char*)malloc( header.width * header.height * bytesPerPixel );
		if( !image )
		{
			printf( "failed to alloc mem\n");
			return false;
		}

		int curByte = 0;
		int pixelsRead = 0;
		int noPixels = header.width * header.height;
		char* pixel = (char*)malloc( bytesPerPixel );

		do
		{
			//Read 1 Pixel
                	if( fread( pixel, 1, bytesPerPixel, pFile) != bytesPerPixel )
			{
				printf( "failed to from file (pixel)\n");
                        	return false;
			}

			image[ curByte + 0 ] = pixel[2];
			image[ curByte + 1 ] = pixel[1];
			image[ curByte + 2 ] = pixel[0];

			if( bytesPerPixel >= 4 )
				image[ curByte + 3 ] = pixel[3];

			curByte += bytesPerPixel;
			++pixelsRead;

		} while( pixelsRead < noPixels );

		if( header.bpp == 32 )
			GenerateTexture( image, header.width, header.height, GL_RGBA );
		else
			GenerateTexture( image, header.width, header.height, GL_RGB );

		free(image);

		return true;
	}

	bool LoadCompresed( FILE* pFile, TGAHead& header )
	{
		if( header.width <= 0 || header.height <= 0 || header.bpp < 24 )
		{
			printf("something wrong\n");
			return false;
		}

		char* image = 0;
		unsigned int bytesPerPixel = header.bpp / 8;

		image = (char*)malloc( header.width * header.height * bytesPerPixel );
		if( !image )
		{
			printf( "failed to alloc mem\n");
			return false;
		}

		int curByte = 0;
		int pixelsRead = 0;
		int noPixels = header.width * header.height;
		char* pixel = (char*)malloc( bytesPerPixel );
		char chunkHead;

		do
		{
			if( fread( &chunkHead, sizeof(char), 1, pFile ) == 0 )
			{
				printf( "failed to from file (chunkHead)\n");
				return false;
			}

			if( (chunkHead >> 7) == 0x0 ) //RAW - not compressesd
			{
				++chunkHead; //increase by one to determine how many pixels follow

				for(short counter = 0; counter < chunkHead; ++counter, curByte += bytesPerPixel, 					++pixelsRead )
        			{
                			//Read 1 Pixel
                			if( fread( pixel, 1, bytesPerPixel, pFile) != bytesPerPixel )
					{
						printf( "failed to from file (pixel RAW)\n");
                        			return false;
					}

					image[ curByte + 0 ] = pixel[2];
					image[ curByte + 1 ] = pixel[1];
					image[ curByte + 2 ] = pixel[0];

					if( bytesPerPixel >= 4 )
						image[ curByte + 3 ] = pixel[3];
				}
			}
			else //RLE - compressed
			{
				chunkHead = chunkHead ^ 128; //drop the 1 at start
				++chunkHead; //decrease by 127 to find out how many pixels follow

				//Read 1 Pixel
                		if( fread( pixel, 1, bytesPerPixel, pFile) != bytesPerPixel )
				{
					printf( "failed to from file (pixel RLE)\n");
                        		return false;
				}

				pixelsRead += (unsigned int)chunkHead;

				for(short counter = 0; counter < chunkHead; ++counter, curByte += bytesPerPixel,
					++pixelsRead )
        			{
					image[ curByte + 0 ] = pixel[2];
					image[ curByte + 1 ] = pixel[1];
					image[ curByte + 2 ] = pixel[0];

					if( bytesPerPixel >= 4 )
						image[ curByte + 3 ] = pixel[3];
				}
			}
		} while( pixelsRead < noPixels );


		if( header.bpp == 32 )
			GenerateTexture( image, header.width, header.height, GL_RGBA );
		else
			GenerateTexture( image, header.width, header.height, GL_RGB );

		free(image);
		free(pixel);

		return true;
	}

	bool GenerateTexture( char* image, int width, int height, GLint type = GL_RGB )
	{
		glGenTextures(1, &texture);
		glBindTexture( GL_TEXTURE_2D, texture );

		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, width, height, 0, type, GL_UNSIGNED_BYTE, image);
/*
		char* error = (char*)glGetString( glGetError() );
		printf("Erros reported: %s", error );*/

		return true;
	}

public:
	TGALoader()
	{

	}

	bool LoadImage( const char* file )
	{
		FILE* pFile = 0;
		pFile = fopen( file, "rb" );

		if( pFile == 0 )
			return false;

		if( !fread( &header, sizeof( TGAHead ), 1, pFile ) )
			return false;

		if( header.imageType == 10 )
		{
			printf("compressed\n");
			return LoadCompresed( pFile, header );
		}
		else if( header.imageType == 2 )
		{
			printf("uncompressed\n");
			return LoadUncompresed( pFile, header );
		}

		fclose( pFile );

		return true;
	}

	void GetInfo( int& width, int& height, int bpp, int type )
	{
		width = header.width;
		height = header.height;
		bpp = header.bpp;

		//if( header.bpp == 32 )
			//type = ;
	}

	void SetTexture()
	{
		glBindTexture( GL_TEXTURE_2D, texture );
	}

	GLuint GetTexture()
	{
		return texture;
	}
};

enum COLOR
{
	R = 0,
	G = 1,
	B = 2,
	A = 3
};

class Color
{
public:
	Color()
	{

	}

	Color( float r, float g, float b, float a = 1.0f )
	{
		color[0] = r;
		color[1] = g;
		color[2] = b;
		color[3] = a;
	}

	float operator[]( const int index )
	{
		return color[index];
	}

	float* GetRaw()
	{
		return color;
	}

protected:
	float color[4];
};

enum POSTION
{
	X = 0,
	Y = 1,
	Z = 2
};

enum ROTATION
{
	YAW = 0,
	PITCH = 1,
	ROLL = 2
};

enum UVCOORDINATE
{
	U = 0,
	V = 1
};

class Vector3
{
public:
	float operator[]( const int index )
	{
		return v[ index ];
	}

protected:
	float v[3];
};

class UVCoord
{
public:
	float operator[]( const int index )
	{
		return uv[ index ];
	}

protected:
	float uv[2];
};

class Light
{
protected:
	//int lightNo;
	//static int noLights;

public:
	Light()
	{
		//lightNo = 0x4000 + noLights;
		//noLights++;
	}

	void SetAmbient( Color value )
	{
		glLightfv( GL_LIGHT1, GL_AMBIENT, value.GetRaw() );
	}

	void SetDiffuse( Color value )
	{
		glLightfv( GL_LIGHT1, GL_DIFFUSE, value.GetRaw() );
	}

	void SetPosition( float x, float y, float z )
	{
		float position[4] = { x, y, z, 1.0f};
		glLightfv( GL_LIGHT1, GL_POSITION, position );
	}

	void SetPosition( Vector3 position )
	{
		float pos[4] = { position[X], position[Y], position[Z], 1.0f};
		glLightfv( GL_LIGHT1, GL_POSITION, pos );
	}

	void Enable()
	{
		glEnable( GL_LIGHT1 );
	}

	void Disable()
	{
		glDisable( GL_LIGHT1 );
	}
};

//int Light::noLights = 0;

class GLDevice
{
public:
	GLDevice() : multiTexture(false), noTextureChannels(1)
	{

	}

	bool CreateDevice()
	{
		glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);
		glEnable(GL_TEXTURE_2D);
		glShadeModel(GL_SMOOTH);
		glDepthFunc(GL_LESS);
		glEnable(GL_DEPTH_TEST);
		glShadeModel(GL_SMOOTH);
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		glFrontFace(GL_CW);
		glCullFace(GL_BACK);
		glEnable(GL_CULL_FACE);

		glEnable(GL_LIGHTING);

		float lmodel_ambient[] = {0.5, 0.5, 0.5, 1.0};
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
    		glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

		InitMultiTexture();
		SetDefaultMaterial();
		EnableFog();

		return true;
	}

	bool InitMultiTexture()
	{
		char *extensions;
		extensions = strdup((char *) glGetString(GL_EXTENSIONS));

		//bool multiTexture = InString( extensions, "GL_ARB_multitexture" );
		/*
		char* test = strtok( extensions, " " );
		while( test[0] != '\0')
		{
			test = strtok( extensions, " " );
TGA
			if( strcmp( test, "GL_ARB_multitexture" ) == 0 )
				exit(0);
		}*/

		return true;
	}

	void SetDefaultMaterial()
	{
		float front_mat_shininess[] = {30.0};
		float front_mat_specular[] = {0.5, 0.5, 0.5, 1.0};
		float front_mat_diffuse[] = {1.0, 1.0, 1.0, 1.0};
		float back_mat_shininess[] = {50.0};
		float back_mat_specular[] = {0.0, 0.0, 1.0, 1.0};
		float back_mat_diffuse[] = {1.0, 0.0, 0.0, 1.0};

		glMaterialfv(GL_FRONT, GL_SHININESS, front_mat_shininess);
    		glMaterialfv(GL_FRONT, GL_SPECULAR, front_mat_specular);
    		glMaterialfv(GL_FRONT, GL_DIFFUSE, front_mat_diffuse);
    		glMaterialfv(GL_BACK, GL_SHININESS, back_mat_shininess);
    		glMaterialfv(GL_BACK, GL_SPECULAR, back_mat_specular);
   		glMaterialfv(GL_BACK, GL_DIFFUSE, back_mat_diffuse);
	}

	void EnableFog()
	{
		float fog_color[] = {0.8, 0.8, 0.8, 1.0};
		float fogDensity = 0.02;

		glEnable(GL_FOG);
		glFogi(GL_FOG_MODE, GL_EXP);
    		glFogf(GL_FOG_DENSITY, fogDensity);

		glFogfv(GL_FOG_COLOR, fog_color);
		glClearColor(0.8, 0.8, 0.8, 1.0);
	}

protected:
	GLuint textureFiltering;

	//multitexturing variables
	bool multiTexture;
	GLint noTextureChannels;

	PFNGLMULTITEXCOORD1FARBPROC		glMultiTexCoord1fARB;
	PFNGLMULTITEXCOORD2FARBPROC		glMultiTexCoord2fARB;
	PFNGLMULTITEXCOORD3FARBPROC		glMultiTexCoord3fARB;
	PFNGLMULTITEXCOORD4FARBPROC		glMultiTexCoord4fARB;
	PFNGLACTIVETEXTUREARBPROC		glActiveTextureARB;
	PFNGLCLIENTACTIVETEXTUREARBPROC		glClientActiveTextureARB;
};

void Resize( int width, int height )
{
	float ratio = 1.0f * width / height;
	glViewport(0, 0, (GLint)width, (GLint)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45,ratio,1,1000);

	glMatrixMode(GL_MODELVIEW);
}

class Platform
{
public:
	bool CreateWindow( int width, int height, std::string title, bool fullscreen = false )
	{
		this->width = width;
		this->height = height;
		this->fullscreen = fullscreen;

		if( fullscreen && glutGameModeGet(GLUT_GAME_MODE_POSSIBLE) )
		{
			char string[255];
			sprintf( string, "%ix%i:32", width, height );

			glutGameModeString( string );
			glutEnterGameMode();
		}
		else
		{
			glutInitWindowPosition(10, 10);
			glutInitWindowSize( width, height);

			if (glutCreateWindow( title.c_str() ) == GL_FALSE)
				return false;

			if( fullscreen )
				glutFullScreen();

			glutReshapeFunc(Resize);
		}

		glutSetKeyRepeat( GLUT_KEY_REPEAT_ON );

		return true;
	}
protected:
	int width;
	int height;
	bool fullscreen;
};

class Geometry
{
public:
	Geometry() : compiled(false)
	{

	}

	bool Render()
	{
		//glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		if( compiled )
		{
			glCallList( meshList );
		}
		else
		{
			glBegin(GL_QUADS);
                	glVertex3f(-100.0f, 0.0f, -100.0f);
                	glVertex3f(-100.0f, 0.0f,  100.0f);
                	glVertex3f( 100.0f, 0.0f,  100.0f);
                	glVertex3f( 100.0f, 0.0f, -100.0f);
        		glEnd();
		}

		return true;
	}

	/*
	 * Compile a mesh from the info
	 */
	void CreateList()
	{
		meshList = glGenLists(1);
        	glNewList(meshList,GL_COMPILE);

		//compile channels here!
	        glColor3f(1.0f, 1.0f, 1.0f);

		// Draw Body
        	glTranslatef(0.0f ,0.75f, 0.0f);
        	glutSolidSphere(0.75f,20,20);

		// Draw Head
        	glTranslatef(0.0f, 1.0f, 0.0f);
        	glutSolidSphere(0.25f,20,20);

		// Draw Eyes
        	glPushMatrix();
        	glColor3f(0.0f,0.0f,0.0f);
        	glTranslatef(0.05f, 0.10f, 0.18f);
        	glutSolidSphere(0.05f,10,10);
        	glTranslatef(-0.1f, 0.0f, 0.0f);
        	glutSolidSphere(0.05f,10,10);
        	glPopMatrix();

		// Draw Nose
        	glColor3f(1.0f, 0.5f , 0.5f);
	        glRotatef(0.0f,1.0f, 0.0f, 0.0f);
        	glutSolidCone(0.08f,0.5f,10,2);

        	glEndList();

		compiled = true;
	}
protected:
	Vector3 v3Channels;
	UVCoord uvChannels;

	GLuint meshList;
	bool compiled;
};

enum BLENDTYPE
{
	COLORKEY = 0,
	BLEND = 1,
	OPAQUE = 2
};

class Texture
{
public:
	Texture() : blendType( OPAQUE )
	{

	}

	bool Load( char* file )
	{
		name = std::string( file );
		TGALoader tga;
		if( tga.LoadImage( file ) )
		{
			texture = tga.GetTexture();
			tga.GetInfo( width, height, bpp, blendType );

			return true;
		}
		else
		{
			return false;
		}
	}

	void SetTexture( int channel = 0 )
	{
		glBindTexture( GL_TEXTURE_2D, texture );
	}

protected:
	int width;
	int height;
	int bpp;
	int blendType;
	GLuint texture;
	std::string name;
};

class Material
{
public:
	Material() : front_specular( 0.5, 0.5, 0.5 ), front_diffuse( 1.0, 1.0, 1.0 ),
		back_specular( 0.0, 0.0, 1.0 ), back_diffuse( 1.0, 0.0, 0.0 ),
		front_shininess(30.0), back_shininess(50.0)
	{

	}

	bool SetMaterial()
	{
		glMaterialf(GL_FRONT, GL_SHININESS, front_shininess);

    		glMaterialfv(GL_FRONT, GL_SPECULAR, front_specular.GetRaw() );
    		glMaterialfv(GL_FRONT, GL_DIFFUSE, front_diffuse.GetRaw() );
    		glMaterialf(GL_BACK, GL_SHININESS, back_shininess );
    		glMaterialfv(GL_BACK, GL_SPECULAR, back_specular.GetRaw() );
   		glMaterialfv(GL_BACK, GL_DIFFUSE, back_diffuse.GetRaw() );

		return true;
	}

protected:

	float front_shininess;
	Color front_specular;
	Color front_diffuse;

	float back_shininess;
	Color back_specular;
	Color back_diffuse;
};

static float angle=0.0 ;
static float x=0.0f,y=1.75f,z=5.0f;
static float lx=0.0f,ly=0.0f,lz=-1.0f;

Geometry snowMan;

class Font
{
public:
	bool CreateFont( char* file )
	{
		bool ret = true;

		TGALoader tga;
		ret = tga.LoadImage( file );
		GLuint font = tga.GetTexture();
		size = 10;

		font = glGenLists(256);

		for( int u = 0; u < 16; ++u )
		{
			for( int v = 0; v < 16; ++v )
			{
				glNewList( fnt[ u*v ], GL_COMPILE );

				glBegin(GL_QUADS);
                		glTexCoord2f( u + 0, v + 0 );
                		glVertex3f(0.0f, 0.0f, 0.0f);

				glTexCoord2f( u + 0, v + 16 );
                		glVertex3f( 0, size,  0.0f);

				glTexCoord2f( u + 16, v + 16 );
                		glVertex3f( size, size,  0.0f);

				glTexCoord2f( u + 16, v + 0);
                		glVertex3f( size, 0, 0.0f);

        			glEnd();

				glEndList();
			}
		}

		return ret;
	}

	bool Draw( char* string, float x, float y )
	{
		//glMatrixMode(GL_PROJECTION);
		//glLoadIdentity();
		//gluOrtho2D(45,ratio,1,1000);

		//glMatrixMode(GL_MODELVIEW);
		//glCallLists( strlen(string), string, fnt );

		//glMatrixMode(GL_PROJECTION);
		//glPushMatrix();
		//glOrtho(-400.0, 400.0, -200.0, 200.0, -400.0, 400.0);

		//glMatrixMode(GL_MODELVIEW);
		//glPushMatrix();
    		//glRasterPos2f(-390.5, 0.5);

		glTranslatef( x, y, 2.0f );

		int len = strlen( string );
		for( int i = 0; i < len; ++i )
		{
			//glTranslatef( x + (i*16), y, 0.0f );
			glCallList( string[i] );
		}

		snowMan.Render();

		//glPopMatrix();

		//glMatrixMode(GL_PROJECTION);
		//glPopMatrix();

		return true;
	}


protected:
	int size;
	GLuint fnt[256];
};


TGALoader tga;
Light light;
Font font;

GLuint snowManDL;

GLint windW, windH;
GLenum rgb, doubleBuffer, windType;
GLint objectIndex = 0;
GLuint bases[20];
float angleX = 0.0, angleY = 0.0, angleZ = 0.0;
float scaleX = 1.0, scaleY = 1.0, scaleZ = 1.0;
float shiftX = 0.0, shiftY = 0.0, shiftZ = 0.0;


typedef struct CAMERA
{
	float position[3];
	float rotation[3];
} CAMERA;

CAMERA myCamera;

static void Init(void)
{
    int i;
/*
    glutFullScreen();
*/
   // glClearColor(0.0, 0.0, 0.0, 0.0);
    glClearIndex(0.0);


    for( i = 0; i < 3; i++ )
    {
    	myCamera.position[i] = 0;
	myCamera.rotation[i] = 0;
    }
    myCamera.position[Z] = -20;

}





void orientMe(float ang) {

        lx = sin(ang);
        lz = -cos(ang);
        glLoadIdentity();
        gluLookAt(x, y, z,
                      x + lx,y + ly,z + lz,
                          0.0f,1.0f,0.0f);
}

void moveMeFlat(int direction) {
        x = x + direction*(lx)*0.1;
        z = z + direction*(lz)*0.1;
        glLoadIdentity();
        gluLookAt(x, y, z, 
                      x + lx,y + ly,z + lz,
                          0.0f,1.0f,0.0f);
}


static void Key2(int key, int x, int y)
{

switch (key) {
                case GLUT_KEY_LEFT :
                        angle -= 0.01f;
                        orientMe(angle);break;
                case GLUT_KEY_RIGHT :
                        angle +=0.01f;
                        orientMe(angle);break;
                case GLUT_KEY_UP :
                        moveMeFlat(1);break;
                case GLUT_KEY_DOWN :
                        moveMeFlat(-1);break;
        }

    glutPostRedisplay();
}

static void Key(unsigned char key, int x, int y)
{

    switch (key) {
      case 27:
        exit(0);

   }

    glutPostRedisplay();
}

static void Draw(void)
{
int i;
int j;


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

     /*   // Draw ground*/

     	tga.SetTexture();

        glColor3f(0.1f, 0.1f, 0.1f);
        glBegin(GL_QUADS);
		//glTexCoord2f(0,0);
                glVertex3f(-100.0f, 0.0f, -100.0f);

		//glTexCoord2f(0,1);
                glVertex3f(-100.0f, 0.0f,  100.0f);

		//glTexCoord2f(1,1);
                glVertex3f( 100.0f, 0.0f,  100.0f);

		//glTexCoord2f(1,0);
                glVertex3f( 100.0f, 0.0f, -100.0f);
        glEnd();

      /*  // Draw 36 SnowMen*/

        for( i = -3; i < 3; i++)
                for(j=-3; j < 3; j++) {
                        glPushMatrix();
                        glTranslatef(i*10.0,0,j * 10.0);
                        //glCallList(snowManDL);
			snowMan.Render();

                        glPopMatrix();
                }

	//gluLookAt(0, 0, 0,
        //              0,0,1,
        //                  0.0f,1.0f,0.0f);
	font.Draw( "I ROCK!", 10,10);

glFlush();

    if (doubleBuffer) {
	glutSwapBuffers();
    }
}

static GLenum Args(int argc, char **argv)
{
    GLint i;

    rgb = GL_TRUE;
    doubleBuffer = GL_FALSE;

    for (i = 1; i < argc; i++) {
	if (strcmp(argv[i], "-ci") == 0) {
	    rgb = GL_FALSE;
	} else if (strcmp(argv[i], "-rgb") == 0) {
	    rgb = GL_TRUE;
	} else if (strcmp(argv[i], "-sb") == 0) {
	    doubleBuffer = GL_FALSE;
	} else if (strcmp(argv[i], "-db") == 0) {
	    doubleBuffer = GL_TRUE;
	} else {
	    printf("%s (Bad option).\n", argv[i]);
	    return GL_FALSE;
	}
    }
    return GL_TRUE;
}

void processMouse(int button, int state, int x, int y) {

/*
        specialKey = glutGetModifiers();
        // if both a mouse button, and the ALT key, are pressed  then
        if ((state == GLUT_DOWN) && 
                        (specialKey == GLUT_ACTIVE_ALT)) {

                // set the color to pure red for the left button
                if (button == GLUT_LEFT_BUTTON) {
                        red = 1.0; green = 0.0; blue = 0.0;
                }
                // set the color to pure green for the middle button
                else if (button == GLUT_MIDDLE_BUTTON) {
                        red = 0.0; green = 1.0; blue = 0.0;
                }
                // set the color to pure blue for the right button
                else {
                        red = 0.0; green = 0.0; blue = 1.0;
                }
        }*/
}

void processMouseActiveMotion(int x, int y) {

	angle += x;
	/*

switch (key) {
                case GLUT_KEY_LEFT :
                        angle -= 0.01f;
                        orientMe(angle);break;
                case GLUT_KEY_RIGHT :
                        angle +=0.01f;
                        orientMe(angle);break;
                case GLUT_KEY_UP :
                        moveMeFlat(1);break;
                case GLUT_KEY_DOWN :
                        moveMeFlat(-1);break;
        }*/

    glutPostRedisplay();

/*
        // the ALT key was used in the previous function
        if (specialKey != GLUT_ACTIVE_ALT) {
                // setting red to be relative to the mouse 
                // position inside the window
                if (x < 0)
                        red = 0.0;
                else if (x > width)
                        red = 1.0;
                else
                        red = ((float) x)/height;
                // setting green to be relative to the mouse 
                // position inside the window
                if (y < 0)
                        green = 0.0;
                else if (y > width)
                        green = 1.0;
                else
                        green = ((float) y)/height;
                // removing the blue component.
                blue = 0.0;
        }*/
}

void processMousePassiveMotion(int x, int y) {

	angle += (float)(x/100);
	orientMe(angle);

	    glutPostRedisplay();
/*
        // User must press the SHIFT key to change the 
        // rotation in the X axis
        if (specialKey != GLUT_ACTIVE_SHIFT) {

                // setting the angle to be relative to the mouse
                // position inside the window
                if (x < 0)
                        angleX = 0.0;
                else if (x > width)
                        angleX = 180.0;
                else
                        angleX = 180.0 * ((float) x)/height;
        }*/
}


int main(int argc, char **argv)
{

    glutInit(&argc, argv);
/*
    if (Args(argc, argv) == GL_FALSE) {
	exit(1);
    }*/



    Platform p;
    p.CreateWindow( 640, 480, "Siphon Engine v3.0", false );

    GLDevice d;
    d.CreateDevice();

    tga.LoadImage("menu.tga");
    light.SetPosition(0, 5, 0);
    Color c(0.5f,0.5f,0.5f);
    light.SetAmbient( c );
    light.SetDiffuse( c );
    light.Enable();
    font.CreateFont("menu.tga");

    snowMan.CreateList();

    Init();

    //glutReshapeFunc(Reshape);
    glutKeyboardFunc(Key);
    glutSpecialFunc(Key2);
    glutDisplayFunc(Draw);


    glutMouseFunc(processMouse);
		glutMotionFunc(processMouseActiveMotion);
        glutPassiveMotionFunc(processMousePassiveMotion);

    glutMainLoop();


    printf("0 & 0 = %i\n", 0 & 0);
    printf("1 & 0 = %i\n", 1 & 0);
    printf("0 & 1 = %i\n", 0 & 1);
    printf("1 & 1 = %i\n\n", 1 & 1);

    printf("0 | 0 = %i\n", 0 | 0);
    printf("1 | 0 = %i\n", 1 | 0);
    printf("0 | 1 = %i\n", 0 | 1);
    printf("1 | 1 = %i\n\n", 1 | 1);

    printf("0 ^ 0 = %i\n", 0 ^ 0);
    printf("1 ^ 0 = %i\n", 1 ^ 0);
    printf("0 ^ 1 = %i\n", 0 ^ 1);
    printf("1 ^ 1 = %i\n\n", 1 ^ 1);

    printf("254 ^ 128 = %i\n", 254 ^ 128);

        printf("size of short: %i\n", sizeof(short) );
    printf("size of byte: %i\n", sizeof(char));
    printf("size of long: %i\n\n", sizeof(long));

    printf( "sizeof header: %i\n", sizeof( TGALoader::TGAHead ) );
    printf( "proper size: %i\n", sizeof( char ) * 6 + sizeof( short ) * 6 );

	return 0;
}
