//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <SDL/SDL.h>

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "TGA.h"

class Font
{
public:
	bool CreateFont( char* file );
	bool DrawText( float x, float y, char* text, ... );

protected:
	TGA font;
	GLuint chars;
};

