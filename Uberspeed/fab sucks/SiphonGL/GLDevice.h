#ifndef _SIPHON_GLDEVICE_
#define _SIPHON_GLDEVICE_

//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <SDL/SDL.h>
#include "string.h"

/*
#if defined(_WIN32)
	glActiveTextureARB = (PFNGLACTIVETEXTUREARBPROC)wglGetProcAddress("glActiveTextureARB");
	if( !glActiveTextureARB )
	{
		printf("could not get pointer to glActiveTextureARB routine\n");
	}
#endif
*/

/*
 * Device and platform stuff
 */
class GLDevice
{
public:
	void ResizeDevice( int width, int height );
	bool CreateWindow( int width, int height, char* title, int bits = 32, bool fullscreen = false );
	void DestroyWindow();
	bool CreateDevice();
	void SetDefaultMaterial();
	void EnableFog();
	void ToggleFullScreen();
	int ExtensionSupported( const char* extension );

protected:
	SDL_Surface* windowHandle;
};

#endif
