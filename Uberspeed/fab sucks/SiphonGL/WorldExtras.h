#ifndef _SIPHON_WORLDEXTRAS_
#define _SIPHON_WORLDEXTRAS_

//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <SDL/SDL.h>

#include "Math3D.h"
#include "Material.h"

class SkyBox
{
public:
	bool Create( int size, char* texTop, char* texBottom, char* texLeft,
	char* texRight, char* texFront, char* texBack );
	int Render();

protected:
	enum SIDE
	{
		TOP = 0,
		LEFT = 1,
		RIGHT = 2,
		BOTTOM = 3,
		FRONT = 4,
		BACK = 5
	};

	int faces;
	GLuint model;
	Texture textures[6];

	Vector3 position;
	Vector3 rotation;

};

#endif

