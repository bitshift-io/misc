#include "Server.h"
#include "Client.h"

Server::Server() : bServer(false), noClients(0), noNetObjs(1)
{

}

bool Server::Create( int port )
{
	reliableClientsSet = SDLNet_AllocSocketSet( MAX_CONNECTIONS );
	if( !reliableClientsSet )
	{
    		printf("SDLNet_AllocSocketSet: %s\n", SDLNet_GetError());
		return false;
	}

	if(SDLNet_ResolveHost( &ip, NULL, port ) == -1 )
	{
    		printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
		return false;
	}

	return OpenReliable( port );
}

Server::~Server()
{
	SDLNet_FreeSocketSet( reliableClientsSet );
	reliableClientsSet = NULL;
}

bool Server::OpenReliable( int port )
{
	serverReliable = SDLNet_TCP_Open(&ip);
	if( !serverReliable )
	{
    		printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
		return false;
	}
/*
	//add to server socket to set
	int noUsed = SDLNet_TCP_AddSocket( reliableClients, serverReliable );
	if( noUsed == -1 )
	{
    		printf("SDLNet_AddSocket: %s\n", SDLNet_GetError());
		return false;
	}*/

	bServer = true;

	return true;
}

bool Server::AcceptReliable()
{
	TCPsocket newReliableClient;

	newReliableClient = SDLNet_TCP_Accept( serverReliable );
	if( !newReliableClient )
	{
    		//printf("SDLNet_TCP_Accept: %s\n", SDLNet_GetError());
		return false;
	}
	else
	{
		ClientInfo newClient;
		newClient.socket = newReliableClient;
		newClient.clientId = noClients;
		++noClients;
		clients.push_back( newClient );
		SendClientId( newClient );

		//add to socket set
		int noUsed = SDLNet_TCP_AddSocket( reliableClientsSet, newReliableClient );
		if( noUsed == -1 )
		{
    			printf("SDLNet_AddSocket: %s\n", SDLNet_GetError());
			return false;
		}
	}

	printf("Client Connected.\n");
	return true;
}

bool Server::SendClientId( ClientInfo& client )
{
	//lets create our message:
	char message[256];
	sprintf( message, "%i %i", CLIENTID, client.clientId );

	//create and send packet header
	PacketHead header;
	header.size = strlen(message);
	header.to = TO_CLIENT;
	//header.from = FROM_SERVER;
	SDLNet_TCP_Send( client.socket, &header, sizeof(PacketHead) );

	//then send message
	return SDLNet_TCP_Send( client.socket, message, strlen(message) );
}

bool Server::IsServer()
{
	return bServer;
}

void Server::Update( unsigned int tick )
{
	if( !IsServer() )
		return;

	//check for incomming connections
	AcceptReliable();

	//check for incomming info and send to all other clients
	CheckReciveReliable();
}

bool Server::RecivePacket( ClientInfo* from )
{
	//read header
	PacketHead header;
	SDLNet_TCP_Recv( from->socket, &header, sizeof(PacketHead) );

	//now read the body of the packet
	void* body = malloc( header.size );
	SDLNet_TCP_Recv( from->socket, body, header.size );

	bool ret;
	if( header.to == TO_SERVER )
	{
		//IS this packet for us?
		// ie. TO_SERVER
		// if so, its probably a create or destory message

		int message;
		sscanf( (char*)body, "%i", &message );
		if( message == CREATE )
		{
			//CREATE - this means we need to send every one
			// a packet telling them to create a new object...
			char classId[256];
			int clientId;
			sscanf( (char*)body, "%i %s %i", &message, &classId, &clientId );

			printf("Create message recived for '%s' from %i.\n", classId, clientId );

			//now we need to create a message for every one...
			// TO_ALL
			// the ID for this object, its class, its owner
			char newMessage[256];

			sprintf(newMessage, "%i %s %i %i", CREATE, &classId, clientId, noNetObjs );
			++noNetObjs;

			header.size = strlen( newMessage );
			header.to = TO_CLIENT;
			ret = SendReliable( newMessage, header, 0 );

			if( ret == false )
				printf("failed to send...\n");
			else
				printf("mesage sent to client: %s\n", newMessage );
		}
	}
	else
	{
		ret = SendReliable( body, header, from );
		printf("SERVER RECIVED HEADER: %i\n", header.netObjId );
	}

	free( body );

	return ret;
}

bool Server::SendReliable( void* data, PacketHead& header, ClientInfo* from )
{
	bool ret = true;

	if( header.to == TO_OWNER )
	{

	}
	else if( header.to == TO_ALL || header.to == TO_CLIENT )
	{
		std::vector< ClientInfo >::iterator clientIt;
		for( clientIt = clients.begin(); clientIt != clients.end(); ++clientIt )
		{
			if( &(*clientIt) != from )
			{
				if( !SDLNet_TCP_Send( clientIt->socket, &header, sizeof(PacketHead) ) )
					ret = false;

				//then send message
				if( !SDLNet_TCP_Send( clientIt->socket, data, header.size ) )
					ret = false;
			}
		}
	}

	return ret;
}

void Server::CheckReciveReliable()
{
	int noReady;

	noReady = SDLNet_CheckSockets( reliableClientsSet, 0 );
	if( noReady == -1 )
	{
		//has a client disconnected?
    		printf("SDLNet_CheckSockets: %s\n", SDLNet_GetError());
	}
	else if( noReady )
	{
		std::vector< ClientInfo >::iterator clientIt;
		for( clientIt = clients.begin(); clientIt != clients.end(); ++clientIt )
		{
			//does this socket have a message?
			//if so read it and send to every one else
			if( SDLNet_SocketReady( clientIt->socket ) )
			{
				RecivePacket( &(*clientIt) );
			}
		}
	}
}
