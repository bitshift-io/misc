#ifndef _SIPHON_MATERIAL_
#define _SIPHON_MATERIAL_

//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <SDL/SDL.h>
#include <string>

#include "TGA.h"

enum BLENDTYPE
{
	COLORKEY = 0,
	BLEND = 1,
	OPAQUE = 2
};


enum COLOR
{
	R = 0,
	G = 1,
	B = 2,
	A = 3
};

class Color
{
public:
	Color()
	{

	}

	Color( float r, float g, float b, float a = 1.0f )
	{
		color[0] = r;
		color[1] = g;
		color[2] = b;
		color[3] = a;
	}

	float& operator[]( const int index )
	{
		return color[index];
	}

	float* GetPointer()
	{
		return color;
	}

protected:
	float color[4];
};

class Texture
{
public:
	Texture();
	bool Load( char* file );
	void SetTexture( int channel = 0 );

protected:
	int width;
	int height;
	int bpp;
	int blendType;
	GLuint texture;
	std::string name;
};

class Material
{
public:
	Material();
	bool SetMaterial();

protected:

	float front_shininess;
	Color front_specular;
	Color front_diffuse;

	float back_shininess;
	Color back_specular;
	Color back_diffuse;
};

#endif
