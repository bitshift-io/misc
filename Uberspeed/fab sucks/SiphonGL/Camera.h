#ifndef _SIPHON_CAMERA_
#define _SIPHON_CAMERA_

#include "Math3D.h"

//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <SDL/SDL.h>

class Camera
{
public:
	Camera();

	void Update( unsigned long tick );

	void FreeCam();

	void Rotate( float angle, float x, float y, float z );
	void LookAt();

	Vector3& GetPosition();
	Vector3& GetRotation();

	void Translate( float x, float y, float z );

	void SetView( GLdouble planex, GLdouble planey, GLdouble planez,
	       GLdouble yaw, GLdouble pitch, GLdouble roll );
protected:
	Vector3 rotation;
	Vector3 position;
	Vector3 view;
	Vector3 up;
	Vector3 right;
};

#endif

