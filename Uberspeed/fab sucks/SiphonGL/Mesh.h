#ifndef _SIPHON_MESH_
#define _SIPHON_MESH_

//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <SDL/SDL.h>
#include "string.h"

#include "Material.h"
#include "Math3D.h"

class Geometry
{
public:
	Geometry() : compiled(false)
	{

	}

	bool Render()
	{
		//glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		if( compiled )
		{
			glCallList( meshList );
		}
		else
		{
			glBegin(GL_QUADS);
                	glVertex3f(-100.0f, 0.0f, -100.0f);
                	glVertex3f(-100.0f, 0.0f,  100.0f);
                	glVertex3f( 100.0f, 0.0f,  100.0f);
                	glVertex3f( 100.0f, 0.0f, -100.0f);
        		glEnd();
		}

		return true;
	}

	/*
	 * Compile a mesh from the info
	 */
	void CreateList()
	{/*
		meshList = glGenLists(1);
        	glNewList(meshList,GL_COMPILE);

		//compile channels here!
	        glColor3f(1.0f, 1.0f, 1.0f);

		// Draw Body
        	glTranslatef(0.0f ,0.75f, 0.0f);
        	glutSolidSphere(0.75f,20,20);

		// Draw Head
        	glTranslatef(0.0f, 1.0f, 0.0f);
        	glutSolidSphere(0.25f,20,20);

		// Draw Eyes
        	glPushMatrix();
        	glColor3f(0.0f,0.0f,0.0f);
        	glTranslatef(0.05f, 0.10f, 0.18f);
        	glutSolidSphere(0.05f,10,10);
        	glTranslatef(-0.1f, 0.0f, 0.0f);
        	glutSolidSphere(0.05f,10,10);
        	glPopMatrix();

		// Draw Nose
        	glColor3f(1.0f, 0.5f , 0.5f);
	        glRotatef(0.0f,1.0f, 0.0f, 0.0f);
        	glutSolidCone(0.08f,0.5f,10,2);

        	glEndList();
*/
		compiled = true;
	}
protected:
	int idxList;
	Vector3 v3Channels;
	UVCoord uvChannels;

	GLuint meshList;
	bool compiled;
};

class Mesh
{
public:

protected:
	Geometry geom; //the vertices and polys
	Material mat; //the material
};

#endif
