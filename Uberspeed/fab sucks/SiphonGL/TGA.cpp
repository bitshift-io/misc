#include "TGA.h"

bool TGA::LoadUncompresed( FILE* pFile, TGAHead& header )
{
	if( header.width <= 0 || header.height <= 0 || header.bpp < 24 )
	{
		printf("something wrong bpp: %i width: %i height: %i\n", header.bpp, header.width, header.height);
		return false;
	}

	char* image = 0;
	unsigned int bytesPerPixel = header.bpp / 8;

	image = (char*)malloc( header.width * header.height * bytesPerPixel );
	if( !image )
	{
		printf( "failed to alloc mem\n");
		return false;
	}

	int curByte = 0;
	int pixelsRead = 0;
	int noPixels = header.width * header.height;
	char* pixel = (char*)malloc( bytesPerPixel );

	do
	{
		//Read 1 Pixel
               	if( fread( pixel, 1, bytesPerPixel, pFile) != bytesPerPixel )
		{
			printf( "failed to from file (pixel)\n");
                       	return false;
		}

		image[ curByte + 0 ] = pixel[2];
		image[ curByte + 1 ] = pixel[1];
		image[ curByte + 2 ] = pixel[0];

		if( bytesPerPixel >= 4 )
			image[ curByte + 3 ] = pixel[3];

		curByte += bytesPerPixel;
		++pixelsRead;

	} while( pixelsRead < noPixels );

	if( header.bpp == 32 )
		GenerateTexture( image, header.width, header.height, GL_RGBA );
	else
		GenerateTexture( image, header.width, header.height, GL_RGB );

	free(image);

	return true;
}

bool TGA::LoadCompresed( FILE* pFile, TGAHead& header )
{
	if( header.width <= 0 || header.height <= 0 || header.bpp < 24 )
	{
		printf("something wrong bpp: %i width: %i height: %i\n", header.bpp, header.width, header.height);
		return false;
	}

	char* image = 0;
	unsigned int bytesPerPixel = header.bpp / 8;

	image = (char*)malloc( header.width * header.height * bytesPerPixel );
	if( !image )
	{
		printf( "failed to alloc mem\n");
		return false;
	}

	int curByte = 0;
	int pixelsRead = 0;
	int noPixels = header.width * header.height;
	char* pixel = (char*)malloc( bytesPerPixel );
	char chunkHead;

	do
	{
		if( fread( &chunkHead, sizeof(char), 1, pFile ) == 0 )
		{
			printf( "failed to from file (chunkHead)\n");
			return false;
		}

		if( (chunkHead >> 7) == 0x0 ) //RAW - not compressesd
		{
			++chunkHead; //increase by one to determine how many pixels follow

			for(short counter = 0; counter < chunkHead; ++counter, curByte += bytesPerPixel, 					++pixelsRead )
       			{
               			//Read 1 Pixel
               			if( fread( pixel, 1, bytesPerPixel, pFile) != bytesPerPixel )
				{
					printf( "failed to from file (pixel RAW)\n");
                       			return false;
				}

				image[ curByte + 0 ] = pixel[2];
				image[ curByte + 1 ] = pixel[1];
				image[ curByte + 2 ] = pixel[0];

				if( bytesPerPixel >= 4 )
					image[ curByte + 3 ] = pixel[3];
			}
		}
		else //RLE - compressed
		{
			chunkHead = chunkHead ^ 128; //drop the 1 at start
			++chunkHead;

			//Read 1 Pixel
               		if( fread( pixel, 1, bytesPerPixel, pFile) != bytesPerPixel )
			{
				printf( "failed to from file (pixel RLE)\n");
                       		return false;
			}

			pixelsRead += (unsigned int)chunkHead;

			for(short counter = 0; counter < chunkHead; ++counter, curByte += bytesPerPixel,
				++pixelsRead )
       			{
				image[ curByte + 0 ] = pixel[2];
				image[ curByte + 1 ] = pixel[1];
				image[ curByte + 2 ] = pixel[0];

				if( bytesPerPixel >= 4 )
					image[ curByte + 3 ] = pixel[3];
			}
		}
	} while( pixelsRead < noPixels );


	if( header.bpp == 32 )
		GenerateTexture( image, header.width, header.height, GL_RGBA );
	else
		GenerateTexture( image, header.width, header.height, GL_RGB );

	free(image);
	free(pixel);

	return true;
}

bool TGA::GenerateTexture( char* image, int width, int height, GLint type )
{
	glGenTextures(1, &texture);
	glBindTexture( GL_TEXTURE_2D, texture );

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);

	gluBuild2DMipmaps( GL_TEXTURE_2D, GL_RGBA, 32, 32, GL_RGBA, GL_UNSIGNED_BYTE, image );

	//glTexImage2D(GL_TEXTURE_2D, 0, 3, width, height, 0, type, GL_UNSIGNED_BYTE, image);
	if( type == GL_RGBA )
		glTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGBA_ARB, width, height, 0, type, GL_UNSIGNED_BYTE, image);
	else
		glTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGB_ARB, width, height, 0, type, GL_UNSIGNED_BYTE, image);

	return true;
}


TGA::TGA()
{

}

bool TGA::LoadImage( const char* file )
{
	FILE* pFile = 0;
	pFile = fopen( file, "rb" );

	if( pFile == 0 )
		return false;

	printf("Loading Texture: %s\n", file );

	if( !fread( &header, sizeof( TGAHead ), 1, pFile ) )
		return false;

	if( header.imageType == 10 )
	{
		return LoadCompresed( pFile, header );
	}
	else //if( header.imageType == 2 )
	{
		printf("\tWARNING: Loading uncompressed texture: %s\n", file );
		return LoadUncompresed( pFile, header );
	}
	/*else
	{
		printf("\tERROR: Unknown texture format: %s\n", file );
		return false;
	}*/

	fclose( pFile );

	return true;
}

void TGA::GetInfo( int& width, int& height, int bpp, int type )
{
	width = header.width;
	height = header.height;
	bpp = header.bpp;

	//if( header.bpp == 32 )
		//type = ;
}

void TGA::SetTexture()
{
	glBindTexture( GL_TEXTURE_2D, texture );
}

GLuint TGA::GetTexture()
{
	return texture;
}
