#ifndef _SIPHON_SERVER_
#define _SIPHON_SERVER_

#include <vector>
#include <stdlib.h>
//#include <SDL/SDL.h>
//#include <SDL/SDL_net.h>
//#include "Client.h"

#define MAX_CONNECTIONS 32

class PacketHead;

struct NetObjInfo
{
	int netId; //its unique id
	int classId; //which class is this obj
	int ownerId; //who owns it
};

struct ClientInfo
{
	TCPsocket socket;
	int clientId; //this users id
};

class Server
{
public:
	~Server();
	Server();

	bool Create( int port = 1555 );
	bool AcceptReliable();
	bool OpenReliable( int port = 1555 );

	bool IsServer();

	void Update( unsigned int tick );

	void CheckReciveReliable();

	bool SendClientId( ClientInfo& client );

	bool RecivePacket( ClientInfo* from );
	bool SendReliable( void* data, PacketHead& header, ClientInfo* from );

protected:
	std::vector< ClientInfo > clients;
	SDLNet_SocketSet reliableClientsSet;
	IPaddress ip;
	TCPsocket serverReliable;
	bool bServer; //is this the server? that has been created
	int noClients; //used to give clients a unique id
	int noNetObjs; //used to create netobjs
};

#endif
