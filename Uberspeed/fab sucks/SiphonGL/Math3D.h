#ifndef _SIPHON_MATH3D_
#define _SIPHON_MATH3D_

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

enum POSTION
{
	X = 0,
	Y = 1,
	Z = 2
};

enum ROTATION
{
	YAW = 0,
	PITCH = 1,
	ROLL = 2
};

enum UVCOORDINATE
{
	U = 0,
	V = 1
};

class Vector3
{
public:
	Vector3( float x = 0, float y = 0, float z = 0 )
	{
		v[X] = x;
		v[Y] = y;
		v[Z] = z;
	}

	float& operator[]( const int index )
	{
		return v[ index ];
	}

	void operator=( const Vector3& rhs )
	{
		for( int i = 0; i < 3; ++i )
			v[i] = rhs.v[i];
	}

	Vector3 operator-( const Vector3& rhs )
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] - rhs.v[i];

		return ret;
	}

	Vector3 operator+( const Vector3& rhs )
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] + rhs.v[i];

		return ret;
	}

	Vector3 operator*( const float rhs )
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] * rhs;

		return ret;
	}

	float* GetPointer()
	{
		return v;
	}

	Vector3 CrossProduct( Vector3& other )
	{
		Vector3 vNormal;

		// Calculate the cross product with the non communitive equation
		vNormal[X] = ((other[Y] * v[Z]) - (other[Z] * v[Y]));
		vNormal[Y] = ((other[Z] * v[X]) - (other[X] * v[Z]));
		vNormal[Z] = ((other[X] * v[Y]) - (other[Y] * v[X]));

		return vNormal;
	}

	float Magnitude()
	{
		// Here is the equation:  magnitude = sqrt(V.x^2 + V.y^2 + V.z^2) : Where V is the vector
		return (float)sqrt( (v[X] * v[X]) +
				(v[Y] * v[Y]) +	(v[Z] * v[Z]) );
	}

	void Normalize()
	{
		// Get the magnitude of our normal
		float magnitude = Magnitude();

		// Now that we have the magnitude, we can divide our vector by that magnitude.
		// That will make our vector a total length of 1.
		for( int i = 0; i < 3; ++i )
			v[i] /= magnitude;
	}

	void Rotate( float angle, float x, float y, float z )
	{
		// Calculate the sine and cosine of the angle once
		float cosTheta = (float)cos(angle);
		float sinTheta = (float)sin(angle);

		Vector3 old = *this;
		// Find the new x position for the new rotated point
		v[X]  = (cosTheta + (1 - cosTheta) * x * x) * old[X];
		v[X] += ((1 - cosTheta) * x * y - z * sinTheta)	* old[Y];
		v[X] += ((1 - cosTheta) * x * z + y * sinTheta)	* old[Z];

		// Find the new y position for the new rotated point
		v[Y]  = ((1 - cosTheta) * x * y + z * sinTheta)	* old[X];
		v[Y] += (cosTheta + (1 - cosTheta) * y * y) * old[Y];
		v[Y] += ((1 - cosTheta) * y * z - x * sinTheta)	* old[Z];

		// Find the new z position for the new rotated point
		v[Z]  = ((1 - cosTheta) * x * z - y * sinTheta)	* old[X];
		v[Z] += ((1 - cosTheta) * y * z + x * sinTheta)	* old[Y];
		v[Z] += (cosTheta + (1 - cosTheta) * z * z) * old[Z];
	}

protected:
	float v[3];
};

class Matrix
{
public:
	Matrix()
	{
		LoadIdentity();
	}

	void LoadIdentity()
	{
		for( int i = 0; i < 4; ++i )
		{
			for( int j = 0; j < 4; ++j )
			{
				if( i == j )
					m[i][j] = 1;
				else
					m[i][j] = 0;
			}
		}
	}

	float* operator[]( const int index )
	{
		return m[ index ];
	}

	void Rotate( float yaw, float pitch, float roll )
	{
		float cosYaw = cos( yaw );
		float sinYaw = sin( yaw );
		float cosPitch = cos( pitch );
		float sinPitch = sin( pitch );
		float cosRoll = cos( roll );
		float sinRoll = sin( roll );

		m[0][0] = cosYaw * cosPitch;
		m[0][1] = cosYaw * sinPitch * sinRoll - sinYaw * cosRoll;
		m[0][2] = cosYaw * sinPitch * cosRoll + sinYaw * sinRoll;

		m[1][0] = sinYaw * cosPitch;
		m[1][1] = sinYaw * sinPitch * sinRoll + cosYaw * cosRoll;
		m[1][2] = sinYaw * sinPitch * cosRoll - cosYaw * sinRoll;

		m[2][0] = -sinPitch;
		m[2][1] = cosPitch * sinRoll;
		m[2][3] = cosPitch * cosRoll;
	}

	/** rotate about a line centred on A */
	void SetRotate( float angle,  double x, double y, double z )
	{
		float ca = cos( angle );
		float sa = sin( angle );
		m[0][0] = ca + x*x * ( 1 - ca );
		m[0][1] = x*y * ( 1 - ca ) - z * sa;
		m[0][2] = x*z * ( 1 - ca ) + y * sa;

		m[1][0] = x*y * ( 1 - ca ) + z * sa;
		m[1][2] = ca + y*y * ( 1 - ca );
		m[1][3] = y*z * ( 1 - ca ) - x * sa;

		m[2][0] = x*z * ( 1 - ca ) - y * sa;
		m[2][1] = y*z * ( 1 - ca ) + x * sa;
		m[2][3] = ca + z*z * ( 1 - ca );
	}

	void Translate( float x, float y, float z )
	{
		m[0][3] = x;
		m[1][3] = y;
		m[2][3] = z;
	}

	Vector3 operator*( Vector3& vector )
	{
		float vec[4];
		for( int i = 0; i < 4; ++i )
			vec[i] = 0;

		//multiply matrix by vector.. arg
		for( i = 0; i < 4; ++i )
		{
			for( int j = 0; j < 4; ++j )
				vec[i] += (m[i][j] * vector[j]);
		}

		//if( vec[4] != 1 )
		//	printf("ops... w=%f\n", vec[4] );

		return Vector3( vec[0], vec[1], vec[2] );
	}

	void RotateY( float angle )
	{
		Matrix n;

		float c = cos( angle );
		float s = sin( angle );

		n.m[0][0] = c;
		n.m[0][2] = s;
		n.m[2][0] = -s;
		n.m[2][2] = c;

		(*this) = (*this) * n;
	}

	void RotateX( float angle )
	{
		Matrix n;

		float c = cos( angle );
		float s = sin( angle );

		n.m[1][1] = c;
		n.m[1][2] = -s;
		n.m[2][1] = s;
		n.m[2][2] = c;

		(*this) = (*this) * n;
	}

	void RotateZ( float angle ) //doesnt work :-/ hrmm
	{
		Matrix n;

		float c = cos( angle );
		float s = sin( angle );

		n.m[0][0] = c;
		n.m[0][1] = -s;
		n.m[1][0] = s;
		n.m[1][1] = c;

		(*this) = (*this) * n;
	}

	Matrix operator*( Matrix& rhs )
	{
		Matrix ret;
		ret.m[0][0] = 0;
		ret.m[1][1] = 0;
		ret.m[2][2] = 0;
		ret.m[3][3] = 0;

		for( int k = 0; k < 4; k++ )
		{
			for( int i = 0; i < 4; ++i )
			{
				for( int j = 0; j < 4; ++j )
				{
					ret.m[k][i] += m[k][j] * rhs.m[j][i];
				}
			}
		}


		return ret;
	}

protected:
	float m[4][4];
};


class UVCoord
{
public:
	float& operator[]( const int index )
	{
		return uv[ index ];
	}

	float* GetPointer()
	{
		return uv;
	}

protected:
	float uv[2];
};

#endif

