#include "Camera.h"

Camera::Camera() : position(0,0,0), view(0,0,-1), right(1,0,0), up(0,1,0), rotation()
{
	//view.Rotate(0.05, 0, 1, 0);
}

void Camera::Rotate( float angle, float x, float y, float z )
{
	view.Rotate(angle, x, y, z);
	view.Normalize();
}

void Camera::Update( unsigned long tick )
{
	FreeCam();
}

void Camera::FreeCam()
{
	int x = 0, y = 0;
	static int last_x = 0, last_y = 0;
	int new_x = 0, new_y = 0;
	static float f = 0;
	Uint8* keys;

	SDL_GetMouseState( &x, &y );

	new_x = x - last_x;
	new_y = y - last_y;
	last_x = x;
	last_y = y;

	rotation[YAW] -= new_x;
	rotation[PITCH] += new_y;

	//printf( "rotation: %f\t", f);



	keys = SDL_GetKeyState(NULL);

    	// and check if ESCAPE has been pressed. If so then quit

	//Vector3 normal = rotation;
	//normal.Normalize();
	//printf("pos: %f %f %f\n", position[0], position[1], position[2] );
	//printf("normal: %f %f %f\n\n", normal[0], normal[1], normal[2] );

	Matrix m;
	//m.Rotate( 0, rotation[YAW]/10, 0 );
	m.RotateY( rotation[YAW] / 50 );
	m.RotateX( rotation[PITCH] / 50 );
	//m.SetRotate(rotation[YAW] / 10, 0, 0, 1);
	Vector3 v( 0.0f, 0.0f, -1.0f );
	view = m * v;
	LookAt();

	if(keys[SDLK_w])
	{
		position =  (view * 0.01) + position;
	}

	if(keys[SDLK_s])
	{
		position =  position - (view * 0.01);
	}

	if( keys[SDLK_a] )
	{
		Vector3 right( m[0][0], m[1][0], m[2][0] );
		position = position - (right * 0.01);
	}

	if( keys[SDLK_d] )
	{
		Vector3 right( m[0][0], m[1][0], m[2][0] );
		position = position + (right * 0.01);
	}

	if( keys[SDLK_SPACE] )
	{
		position[Y] += 0.01;
	}

	if( keys[SDLK_LCTRL] )
	{
		position[Y] -= 0.01;
	}

	return;

	/*


		//v.Rotate(rotation[YAW], 0, 1, 0);
		Matrix m1, m2, m3, m4;
		//m.Rotate( rotation[YAW], rotation[PITCH], rotation[ROLL]);
		m1.SetRotate( rotation[ROLL], 0, 0, 1);
		m2.SetRotate( rotation[PITCH], 1, 0, 0);
		m3.SetRotate( rotation[YAW], 0, 1, 0);

		m4 = m1 * m2 * m3;


		//m.Rotate( rotation[YAW], rotation[PITCH], rotation[ROLL] );
		v = m4.Transform( v );

	GLfloat matrix[16];
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	glRotated( rotation[ROLL], 0.0, 0.0, 1.0);
	glRotated( rotation[PITCH], 1.0, 0.0, 0.0);
 	glRotated( rotation[YAW], 0.0, 1.0, 0.0);

	glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
	Vector3 look( matrix[8], matrix[9], matrix[10] );
*/
	/*
	 GLfloat Matrix[16];

	glRotatef( rotation[YAW], 0.0f, 1.0f, 0.0f);
	glRotatef( rotation[PITCH], 1.0f, 0.0f, 0.0f);

	glGetFloatv(GL_MODELVIEW_MATRIX, Matrix);

	Vector3 m_DirectionVector;
	m_DirectionVector[0] = Matrix[8];
	m_DirectionVector[1] = Matrix[10];

	glLoadIdentity();

	glRotatef( rotation[PITCH], 1.0f, 0.0f, 0.0f);

	glGetFloatv(GL_MODELVIEW_MATRIX, Matrix);
	m_DirectionVector[2] = Matrix[9];

	glRotatef( rotation[YAW], 0.0f, 1.0f, 0.0f);

	// Scale the direction by our speed.
	m_DirectionVector = m_DirectionVector * 0.01;
*/

/*


	//rotation.Normalize();
    	if(keys[SDLK_w])
	{
			// Increment our position by the vector
		//position[X] += m_DirectionVector[0];
		//position[Y] += m_DirectionVector[1];
		//position[Z] += m_DirectionVector[2];
		position = position - (look * 0.1);
	}
	if(keys[SDLK_s])
	{
		// Increment our position by the vector
		//position[X] -= m_DirectionVector[0];
		//position[Y] -= m_DirectionVector[1];
		//position[Z] -= m_DirectionVector[2];
		position = position + (look * 0.1);
	}

	if( keys[SDLK_SPACE] )
	{
		position[Y] += 0.01;
	}

	if( keys[SDLK_LCTRL] )
	{
		position[Y] -= 0.01;
	}

	// Translate to our new position.
	//glTranslatef( -position[X], -position[Y], position[Z] );

	//SetView( position[X], position[Y], position[Z],
	 //      rotation[YAW], rotation[PITCH], 0 );

		//printf("x: %i y:%i", new_x, new_y);
	//camera.GetRotation().Rotate( -(new_x/30), 0, 1, 0);
	*/
}

void Camera::LookAt()
{
	Vector3 lookAt = position + view;

	// Give openGL our camera position, then camera view, then camera up vector
	gluLookAt( position[X], position[Y], position[Z],
		   lookAt[X],	lookAt[Y],     lookAt[Z],
		   up[X],       up[Y],       up[Z] );
}

void Camera::SetView( GLdouble planex, GLdouble planey, GLdouble planez,
	       GLdouble yaw, GLdouble pitch, GLdouble roll )
{
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	glRotated(roll, 0.0, 0.0, 1.0);
	glRotated(pitch, 1.0, 0.0, 0.0);
	glRotated(yaw, 0.0, 1.0, 0.0);
	//glTranslatef( -planex, -planey, -planez );



	//printf("b4: %f %f %f, after: %f %f %f\n", 0.0, 0.0, -1.0, v[0], v[1], v[2] );
	//r.Rtoate();
	//Matrix m;
	//m.Rotate( yaw, pitch, roll );
	//m.Translate( -planex, -planey, -planez );
	//Vector3 trans = m.Transform( Vector3( 0.0f, 0.0f, -1.0f ) );
	//printf("Transform from: %f %f %f, to: %f %f %f\n", 0, 0, -1, trans[0], trans[1], trans[2] );
	//glTranslated( trans[0], trans[1], trans[2] );


	glTranslated( -planex, -planey, -planez );
	//glTranslated( position[X], position[Y], position[Z] );
}

Vector3& Camera::GetPosition()
{
	return position;
}

Vector3& Camera::GetRotation()
{
	return view;
}

void Camera::Translate( float x, float y, float z )
{
	//translate along camera view.... z is view, x is strafe, y is up and down
	if( z != 0 )
	{
		view.Normalize();
		position = position + (view * z);
	}
	else if( x != 0 )
	{
		right = view.CrossProduct( up );
		//up = view.CrossProduct( right );
		position = position + (right * x);
	}
}
