#ifdef WIN32
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLu32.lib")
#pragma comment(lib, "GLaux.lib")

#pragma comment(lib, "SDL.lib")
#pragma comment(lib, "SDLmain.lib")

#include <SDL.h>
#include <windows.h>

#endif

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glaux.h>

//#include <SDL/SDL.h>

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>



//#include "stdlib.h"
//#include "stdio.h"



#include "3DS.h"
#include "Material.h"
#include "Light.h"
#include "Mesh.h"
#include "ObjMgr.h"
#include "NetObj.h"
#include "Client.h"
#include "Server.h"
#include "Math3D.h"
#include "GLDevice.h"
#include "TGA.h"
#include "Font.h"
#include "Camera.h"
#include "WorldExtras.h"

