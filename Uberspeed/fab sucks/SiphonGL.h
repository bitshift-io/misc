//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <SDL/SDL.h>

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#ifdef WIN32
#pragma comment(lib, "SDL.lib")
#pragma comment(lib, "SDLmain.lib")
#include <SDL.h>
#endif

//#include "stdlib.h"
//#include "stdio.h"



#include "3DS.h"
#include "Material.h"
#include "Light.h"
#include "Mesh.h"
#include "ObjMgr.h"
#include "NetObj.h"
#include "Client.h"
#include "Server.h"
#include "Math3D.h"
#include "GLDevice.h"
#include "TGA.h"
#include "Font.h"
#include "Camera.h"
#include "WorldExtras.h"

