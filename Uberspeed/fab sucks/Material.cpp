#include "Material.h"

Texture::Texture() : blendType( OPAQUE )
{

}

bool Texture::Load( char* file )
{
	name = std::string( file );
	TGA tga;
	if( tga.LoadImage( file ) )
	{
		texture = tga.GetTexture();
		tga.GetInfo( width, height, bpp, blendType );

		return true;
	}
	else
	{
		return false;
	}
}

void Texture::SetTexture( int channel )
{
	glBindTexture( GL_TEXTURE_2D, texture );
}

Material::Material()/* : front_specular( 0.5, 0.5, 0.5 ), front_diffuse( 1.0, 1.0, 1.0 ),
	back_specular( 0.0, 0.0, 1.0 ), back_diffuse( 1.0, 0.0, 0.0 ),
	front_shininess(30.0), back_shininess(50.0)*/
{

}

bool Material::SetMaterial()
{
/*
	glMaterialf(GL_FRONT, GL_SHININESS, front_shininess);

	glMaterialfv(GL_FRONT, GL_SPECULAR, front_specular.GetRaw() );
	glMaterialfv(GL_FRONT, GL_DIFFUSE, front_diffuse.GetRaw() );
	glMaterialf(GL_BACK, GL_SHININESS, back_shininess );
	glMaterialfv(GL_BACK, GL_SPECULAR, back_specular.GetRaw() );
	glMaterialfv(GL_BACK, GL_DIFFUSE, back_diffuse.GetRaw() );
*/
	return true;
}
