#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <string>

#include <time.h>

#include "SiphonGL.h"
//#include "TGA.h"
//#include "Font.h"

Client client;
Server server;
SkyBox sky;
Camera camera;
TGA tex;
Font fnt;


class Chat : virtual public Obj, virtual public NetObj
{
public:
	Chat()
	{
		//notify obj manager
		if( ObjMgr::RegisterObject( this ) )
		{
			//notify client...
			client.RegisterNetObject( this );
		}

		printf("CREATING A NEW CHAT\n");
	};

	virtual void Update( unsigned long tick )
	{/*
		Uint8* keys;
    		keys = SDL_GetKeyState(NULL);

		if( keys[SDLK_RETURN] )
		{
			Send( (void*)"hello", strlen("hello") );
		}

		fnt.DrawText( 10, 10, (char*)last.c_str() );*/
	}

	bool Send( void* message, int length )
	{/*
		client.SendReliable( message, length, this, TO_ALL );
		last = std::string( "hello!" );
		return true;*/
	};

	bool Recive( void* message, int length )
	{/*
		printf("I HAVE RECIVED A MESSAGE :)\n");
		last = std::string( (char*)message );
		//exit(0);
		return true;*/
	};

	virtual void AnotherFn()
	{
		printf("Another fn called from Chat\n");
	}

	Obj* Create()
	{
		return new Chat;
	}

	virtual char* GetClassId()
	{
		printf("returning 'chat' as classid\n");
		return "chat";
	}
//protected:
	//static ObjReg<Chat> regReg; //("chat");
	std::string last;
};

ObjReg<Chat> regReg("chat");

Chat* chat; //create a chat, this should register it self with the net, and objmgr :)
t3DModel model;





int fps;
clock_t t;
int fps_last;
float rot = 0;
bool EvenFlag = true;

void Draw3DSGrid()
{
	// This function was added to give a better feeling of moving around.
	// A black background just doesn't give it to ya :)  We just draw 100
	// green lines vertical and horizontal along the X and Z axis.

	// Turn the lines GREEN
	//glColor3ub(0, 255, 0);

	// Draw a 1x1 grid along the X and Z axis'
	for(float i = -50; i <= 50; i += 1)
	{
		// Start drawing some lines
		glBegin(GL_LINES);

			// Do the horizontal lines (along the X)
			glVertex3f(-50, 0, i);
			glVertex3f(50, 0, i);

			// Do the vertical lines (along the Z)
			glVertex3f(i, 0, -50);
			glVertex3f(i, 0, 50);

		// Stop drawing lines
		glEnd();
	}
}

void DrawGLScene()
{
int polys = 0;
	static float f = 0;
	//tex.SetTexture();

	//gluLookAt(5, 5, 5,
        //              0,0,0,
         //                 0.0f,1.0f,0.0f);

	// camera.Update( 0 );

/*
	if (EvenFlag) {
            glDepthFunc( GL_LESS );
            glDepthRange( 0.0, 0.5 );
        }
        else {
            glDepthFunc( GL_GREATER );
            glDepthRange( 1.0, 0.5 );
        }
        EvenFlag = !EvenFlag;
*/
  glClear(GL_DEPTH_BUFFER_BIT); // GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT
  glLoadIdentity(); // Reset the view

 //camera.LookAt();


 	f += 0.05;
	camera.FreeCam();
  glColor3f(1.0f,1.0f,1.0f);
	 	sky.Render();
/*
	camera.SetView( 0, 0, f,
	       0, 0, 0 );*/

  glColor3f(1.0f,1.0f,1.0f);

  Draw3DSGrid();

  for( int i  = 0; i < 20; ++i)
  	polys += model.Render();

  //tex.SetTexture();
  //tex.SetTexture();


  	if( clock() > t )
	{
		fps_last = fps;
		fps = 0;
		t = clock() + 1000000;
		//exit(0);
	}
	++fps;




	//tex.SetTexture();
        glBegin(GL_TRIANGLES);

		glTexCoord2f(0,0);
		glColor3f(1.0f, 0.0f, 0.0f);
                glVertex3f(-10.0f, -10.0f, -10.0f);

		glTexCoord2f(0,1);
		glColor3f(0.0f, 0.0f, 1.0f);
                glVertex3f(-10.0f, 10.0f,  -10.0f);

		glTexCoord2f(1,1);
		glColor3f(0.0f, 1.0f, 0.0f);
                glVertex3f( 10.0f, 10.0f,  -10.0f);

		glTexCoord2f(1,1);
		glColor3f(1.0f, 0.0f, 0.0f);
                glVertex3f(10.0f, 10.0f, -10.0f);

		glTexCoord2f(1,0);
		glColor3f(0.0f, 0.0f, 1.0f);
                glVertex3f(10.0f, -10.0f,  -10.0f);

		glTexCoord2f(0,0);
		glColor3f(0.0f, 1.0f, 0.0f);
                glVertex3f( -10.0f, -10.0f,  -10.0f);

        glEnd();

	fnt.DrawText( 10, 10, "Did i say fab rocks?\nargh...looks like quake 3!\nFPS: %i\nPolys: %i", fps_last, polys);
	
	//this should not be here.. but its just a test ;p
	ObjMgr::Update(0);



  SDL_GL_SwapBuffers(); // Swap the buffers
}

int main(int argc, char* argv[] )
{
	CLoad3DS modell;

	//lets see if we want a server or client
	if( argc >= 3 && strcmp( argv[1], "client" ) == 0 )
	{
		printf("CREATING CLIENT\n");
		client.Connect( argv[2] );
	}
	else if( argc >= 2 && strcmp( argv[1], "server" ) == 0 )
	{
		printf("CREATING SERVER\n");
		server.Create();
		//connect to self
		client.Connect( "localhost" );
	}

	//Obj* obj = Chat::regReg.Create();
	chat = new Chat();

	bool bTyping = false;
	bool bPressed = false;
	std::string str = "";

	int last_x, last_y;
	Uint8* keys; // This variable will be used in the keyboard routine

	GLDevice device;
	device.CreateWindow(640,480, "fab rocks!", 32 );

	tex.LoadImage("menu.tga");
	fnt.CreateFont("font.tga");

	sky.Create( 20, "data/top.tga", "data/bottom.tga", "data/left.tga", "data/right.tga",
		"data/front.tga", "data/back.tga" );

	modell.Import3DS( &model, "data/face.3ds");

	// Hide the mouse cursor
  	SDL_ShowCursor(0);

	t = clock() + 1000000;
	fps = 0;

	bool done = false;
	while( !done )
	{
 		// Draw the scene
    		DrawGLScene();
    		// And poll for events
    		SDL_Event event;
    		while ( SDL_PollEvent(&event) )
		{
      			switch (event.type)
			{
        			// If a quit event was recieved
        			case SDL_QUIT:
         			 // then we're done and we'll end this program
          			done=true;
         			 break;
        			default:
         			 break;
      			}
		}

		int x, y;
		float new_x, new_y;
		SDL_GetMouseState( &x, &y);

		new_x = x - last_x;
		new_y = y - last_y;
		last_x = x;
		last_y = y;

		//printf("x: %i y:%i", new_x, new_y);
		//camera.GetRotation().Rotate( -(new_x/30), 0, 1, 0);
		//camera.GetRotation().Rotate( (new_y/20), 1, 0, 0);
		//SDL_WarpMouse( 0, 0);


		    // Get the state of the keyboard keys
    		keys = SDL_GetKeyState(NULL);

    		// and check if ESCAPE has been pressed. If so then quit
    		if(keys[SDLK_ESCAPE]) done=true;

		if( keys[SDLK_RETURN] && !bPressed )
		{
			bPressed = true;
			SDL_EnableKeyRepeat(500, 30);
			bTyping = true;
		}

		if( !keys[SDLK_RETURN] )
			bPressed = false;

		if( bTyping )
		{
			if( keys[SDLK_RETURN] )
			{
				bPressed = true;
				SDL_EnableKeyRepeat(0, 0);
				bTyping = false;
				//char buf = "hellooo!!!!";
				char* buf = "hellooo!!!!"; //(char*)str.c_str();
				if( !client.SendReliable( buf, strlen( buf ) ) )
				{
					printf("failed to send\n");
				}
				//break;
			}

			/*
			for( int i = 0; i < 256; ++i )
			{
				if( keys[i] )
					str.append( SDL_GetKeyName(i) );
			}*/
		}




/*
		void SetView( GLdouble planex, GLdouble planey,
               GLdouble planez, GLdouble roll,
               GLdouble pitch, GLdouble yaw );*/


		/*
		if( keys[SDLK_LCTRL] )
			camera.GetPosition()[Y] -= 0.1;

		if( keys[SDLK_SPACE] )
			camera.GetPosition()[Y] += 0.1;

		if( keys[SDLK_a] )
		{
			camera.Translate( 0.1, 0, 0 );
			//camera.GetPosition()[X] -= 0.03;
		}

		if( keys[SDLK_d] )
		{
			camera.Translate( -0.1, 0, 0 );
			//camera.GetPosition()[X] += 0.03;
		}

		if( keys[SDLK_w] )
		{
			//camera.GetPosition()[Z] -= 0.03;
			camera.Translate( 0, 0, 0.1 );
		}

		if( keys[SDLK_s] )
		{
			//camera.GetPosition()[Z] += 0.03;
			camera.Translate( 0, 0, -0.1 );
		}*/

		//do netowkr stuff
		client.Update(0);
		server.Update(0);

	}

	device.DestroyWindow();

	ObjMgr::Shutdown();

	return 0;
}
