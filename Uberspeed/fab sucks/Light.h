#ifndef _SIPHON_LIGHT_
#define _SIPHON_LIGHT_

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>

#include "Math3D.h"
#include "Material.h"

class Light
{
protected:
	//int lightNo;
	//static int noLights;

public:
	Light()
	{
		//lightNo = 0x4000 + noLights;
		//noLights++;
	}

	void SetAmbient( Color value )
	{
		glLightfv( GL_LIGHT1, GL_AMBIENT, value.GetPointer() );
	}

	void SetDiffuse( Color value )
	{
		glLightfv( GL_LIGHT1, GL_DIFFUSE, value.GetPointer() );
	}

	void SetPosition( float x, float y, float z )
	{
		float position[4] = { x, y, z, 1.0f};
		glLightfv( GL_LIGHT1, GL_POSITION, position );
	}

	void SetPosition( Vector3 position )
	{
		float pos[4] = { position[X], position[Y], position[Z], 1.0f};
		glLightfv( GL_LIGHT1, GL_POSITION, pos );
	}

	void Enable()
	{
		glEnable( GL_LIGHT1 );
	}

	void Disable()
	{
		glDisable( GL_LIGHT1 );
	}
};

#endif

