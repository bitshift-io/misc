#include "GLDevice.h"

bool SaveScreenshot()
{/*
	unsigned char *image = NULL;
	bool ret = 0;

	// reserve some mem
	image = (unsigned char*)malloc(sizeof(unsigned char)*width*height*3);
	if( !image )
	{
		// unable to allocate image buffer
		ret = -1;
	}
	else
	{
		glReadPixels(x,y,width,height, GL_BGR_EXT, GL_UNSIGNED_BYTE, image);
		//ret = write_screenshot_bmp(fname, image, width, height);
	}

	if (image)
	{
		free(image);
	}

	return ret;*/
}

void GLDevice::ResizeDevice( int width, int height )
{
	if( height <= 0 )
		height=1;

	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	// Calculate The Aspect Ratio Of The Window
	gluPerspective( 90.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
/*
	glClearDepth( 1.0 );
        glClear( GL_DEPTH_BUFFER_BIT );
        glDepthRange( 0.0, 0.5 );
        glDepthFunc( GL_LESS );*/
}

bool GLDevice::CreateWindow( int width, int height, char* title, int bits, bool fullscreen )
{
	int flags;
	int size;

	/* Initialize SDL */
	if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		fprintf(stderr, "Couldn't init SDL: %s\n", SDL_GetError());
		return false;
	}

	flags = SDL_OPENGL;
	if ( fullscreen )
	{
		flags |= SDL_FULLSCREEN;
	}
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 1 );
	if( (windowHandle = SDL_SetVideoMode(width, height, 0, flags)) == NULL )
	{
		return false;
	}
	SDL_GL_GetAttribute( SDL_GL_STENCIL_SIZE, &size);

	ResizeDevice(width, height);

	if (!CreateDevice())
	{
		DestroyWindow();
		return false;
	}

	SDL_WM_SetCaption( title, title );

	return true;
}

int GLDevice::ExtensionSupported( const char* extension )
{
	const GLubyte *extensions = NULL;
	const GLubyte *start;
	GLubyte *where, *terminator;

	/* Extension names should not have spaces. */
	where = (GLubyte *) strchr(extension, ' ');
	if( where || *extension == '\0' )
	{
		return 0;
	}

	extensions = glGetString(GL_EXTENSIONS);
	/* It takes a bit of care to be fool-proof about parsing the
	OpenGL extensions string. Don't be fooled by sub-strings,
	etc. */
	start = extensions;
	while(1)
	{
		where = (GLubyte *) strstr((const char *) start, extension);
		if( !where )
		{
			break;
		}

		terminator = where + strlen(extension);
		if( where == start || *(where - 1) == ' ' )
		{
			if( *terminator == ' ' || *terminator == '\0' )
			{
				return 1;
			}
		}

		start = terminator;
	}

	return 0;
}

void GLDevice::ToggleFullScreen()
{
	if(SDL_WM_ToggleFullScreen( windowHandle ) == 0)
	{
		printf("Failed to Toggle Fullscreen mode : %s\n", SDL_GetError() );
	}
}

void GLDevice::DestroyWindow()
{
	SDL_Quit();
}

bool GLDevice::CreateDevice()
{
	glEnable(GL_TEXTURE_2D);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_TEXTURE_2D);
	glClearColor(0.8f, 0.8f, 0.8f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); //GL_FASTEST
	glColor3f(1.0f,1.0f,1.0f);

	glFrontFace(GL_CW);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);

	//GetExtensions();

/*
	glEnable(GL_LIGHTING);

	float lmodel_ambient[] = {0.8, 0.8, 0.8, 1.0};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
    	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);


	SetDefaultMaterial();*/
	//EnableFog();
/*
	glClearDepth( 1.0 );
        glClear( GL_DEPTH_BUFFER_BIT );
        glDepthRange( 0.0, 0.5 );
        glDepthFunc( GL_LESS );*/

	if( ExtensionSupported("GL_ARB_multitexture") )
		printf("MULTITEXTURE SUPPORTED!\n");

	return true;
}

void GLDevice::SetDefaultMaterial()
{
	float front_mat_shininess[] = {30.0};
	float front_mat_specular[] = {0.5, 0.5, 0.5, 1.0};
	float front_mat_diffuse[] = {0.5, 0.5, 0.5, 1.0};
	float back_mat_shininess[] = {50.0};
	float back_mat_specular[] = {0.5, 0.5, 0.5, 1.0};
	float back_mat_diffuse[] = {0.5, 0.5, 0.5, 1.0};

	glMaterialfv(GL_FRONT, GL_SHININESS, front_mat_shininess);
	glMaterialfv(GL_FRONT, GL_SPECULAR, front_mat_specular);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, front_mat_diffuse);
	glMaterialfv(GL_BACK, GL_SHININESS, back_mat_shininess);
	glMaterialfv(GL_BACK, GL_SPECULAR, back_mat_specular);
	glMaterialfv(GL_BACK, GL_DIFFUSE, back_mat_diffuse);
}

void GLDevice::EnableFog()
{
	float fog_color[] = {0.8, 0.8, 0.8, 1.0};
	float fogDensity = 0.02;

	glEnable(GL_FOG);
	glFogi(GL_FOG_MODE, GL_EXP);
	glFogf(GL_FOG_DENSITY, fogDensity);

	glFogfv(GL_FOG_COLOR, fog_color);
	glClearColor(0.8, 0.8, 0.8, 1.0);
}
