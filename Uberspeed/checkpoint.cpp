#include "checkpoint.h"

Checkpoint::Checkpoint( sCheckpoint checkpoint )
{
	id = checkpoint.checkpointId;
	height = checkpoint.height;
	width = checkpoint.width;
	length = checkpoint.length;

	worldTranspose = Matrix4( checkpoint.matrix );

	lines[0] = Vector3( width/2, height/2, length/2 );
	lines[1] = Vector3( width/2, -height/2, length/2 );
	lines[2] = Vector3( -width/2, -height/2, length/2 );
	lines[3] = Vector3( -width/2, height/2, length/2 );
	lines[4] = Vector3( width/2, height/2, length/2 );

	lines[5] = Vector3( width/2, height/2, -length/2 );
	lines[6] = Vector3( width/2, -height/2, -length/2 );
	lines[7] = Vector3( -width/2, -height/2, -length/2 );
	lines[8] = Vector3( -width/2, height/2, -length/2 );
	lines[9] = Vector3( width/2, height/2, -length/2 );

	world = worldTranspose;
	worldTranspose.Inverse();

	for( int i = 0; i < 10; ++i )
		lines[i] = world * lines[i];
}

bool Checkpoint::PointInBox( Vector3& position )
{
	Vector3 local = worldTranspose * position;

	if( local[Z] <= length/2 && local[Z] >= -length/2 &&
		 local[Y] <= height/2 && local[Y] >= -height/2 &&
		  local[X] <= width/2 && local[X] >= -width/2 )
		  return true;

	return false;
}

int Checkpoint::GetID()
{
	return id;
}

int Checkpoint::Render( Camera& camera )
{
	glColor3f(1.0, 1.0, 0.0);
	glLineWidth(10.0);

	glBegin(GL_LINE_STRIP);

		for( int i = 0; i < 5; ++i )
			glVertex3f(lines[i][X], lines[i][Y], lines[i][Z]);

	glEnd();

	glBegin(GL_LINE_STRIP);

		for( i = 5; i < 10; ++i )
				glVertex3f(lines[i][X], lines[i][Y], lines[i][Z]);

	glEnd();

	return 0;
}