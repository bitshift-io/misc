#ifndef _SIPHON_STATICEXPORT_
#define _SIPHON_STATICEXPORT_

#include <vector>
#include <list>
#include <set>

#define VERTICES		0
#define UVCOORDS		1
#define FACES			2
#define NORMALS			3
#define MATERIALREF		4
#define COLORS			5
#define WEIGHTS			6
#define ENDOFFILE		7
#define MESH_HEAD		8
#define MESH_END		9
#define TEXTUREREF		10
#define MATERIAL		11
#define VOLUME			12
#define COLLISION_HEAD	13
#define COLLISION_END	14
#define ODE_BOX			15
#define ODE_CYLINDER	16
#define ODE_SPHERE		17
#define SECTOR_RENDER_INFO		18
#define NEW_SECTOR_HEADER		19	
#define SECTOR_COLLISION_INFO	20
#define LIGHT_MAP				21
#define CUSTOM_HEAD				22
#define CUSTOM_END				23
#define CUSTOM_CHUNK			24
#define SPAWN					25
#define SOUND					26
#define CHECKPOINT				27
/*
enum EVOLUME
{
	SOUND,
//	SPAWN,
};*/

#pragma pack(push,1)
struct Head
{
	unsigned int type; //the type of the body
	long size; //the size of the body
	unsigned int noElements;   //the number of elements eg. 400 vertices
};

struct sectorInfo
{
	float startValue;
	float cellGap;
	int cols;
};

struct odeSphere
{
	float radius;
	float position[3];
};

struct odeCylinder
{
	float radis;
	float length;
	float position[3];
	float rotation[3];
};

struct odeBox
{
	float width;
	float length;
	float height;
	float matrix[16];
	//float position[3];
	//float rotation[9]; //3x3 rotational matrix
};

struct sPlane
{
	float p[4];
};
/*
struct sBox
{
	sPlane plane[6];

	//we use a sphere to check if player is near checkpoint
	float radius;
	float position[3];
};
*/
struct sFace
{
	unsigned int idx[3];
};

struct sVertex
{
	float vert[3];
};

struct sUV
{
	float uv[2];
};

struct sMaterial
{
	sVertex specular;
	sVertex diffuse;
	float shine;
};

struct sVolume
{
	
};

struct sSound
{
	char sound[256];
	float position[3];
	float radius;
};

struct sSpawn 
{
	int spawnId;
	//float position[3];
	//float rotation[3];
	float matrix[16];
};

struct sCheckpoint : public odeBox
{
	int checkpointId;	
};

#pragma pop

struct snMesh
{
	IGameMaterial* material;

	std::vector<sFace> faces;
	std::vector<unsigned int> indices;
	std::vector<Point3> positions;
	std::vector<Point2> uvCoords;
	std::vector<Point2> uvCoords2; //light maps or what ever
	std::vector<Point3> normals;
	std::vector<Point3> colors;
};

class Export
{
public:
	Export( IGameScene* scene, TCHAR *name, Interface* ip );
	~Export();

	virtual void SetMapDivisions( int renderDivisions, int collisionDivisions );

	virtual bool Done();

	virtual bool ExportNode( IGameNode* pNode );
	virtual bool ExportMesh( IGameMesh* mesh, IGameNode* node );
	virtual bool AppendMesh( snMesh* sMesh, Tab<FaceEx*>& exFaces, IGameMesh* mesh );

	virtual bool DivideMesh( snMesh* mesh );
	virtual bool DivideCollisionMesh( snMesh* mesh );


	virtual bool ExportSkyBox( IGameMesh* mesh, IGameNode* node );
	virtual bool ExportRawMesh( snMesh* mesh );

	virtual bool ExportVolume( IGameNode* pNode );

	/**
	 * Export spawn point...
	 * everything needs a spawn point, even if just for camera
	 */
	virtual bool ExportSpawn( IGameNode* pNode );

	/**
	 * Export sound!
	 * this must be a sphere to get radius
	 */
	virtual bool ExportSound( IGameNode* pNode );

	/**
	 * called if user has custom data to export
	 */
	virtual bool ExportCheckPoint( IGameMesh* mesh, IGameNode* pNode );



	/**
	 * Ques the ODE shapes into appropriate collision
	 * sectors
	 */
	virtual bool QueODEShapes( IGameNode* pNode );

	/**
	 * actually exports the ODE shapes to file
	 */
	virtual bool ExportODEShape( IGameNode* pNode );
	virtual bool ExportCollision( IGameMesh* mesh, IGameNode* node );

	virtual bool ExportBasicShape( INode* maxNode );

	virtual bool ExportMaterial( IGameMaterial* material ); 

	virtual char* FixPath( const char* const bmpName, char ext[3]  );
	//virtual bool ExportMaterial();

	/*
	 * This exports the given data is size size, and also adds in a header ot type
	 * type. Sub header info is info that goes after the header and before the data,
	 * this is used for example, to tell how many vertices there are.
	 */
	virtual bool ExportData( FILE* pFile, void* data, long size, unsigned int type, unsigned int noElements = 0 );

protected:
	IGameScene* scene;
	FILE* pFile;
	Interface* ip;

	struct RenderSector
	{
		std::list<snMesh*> meshes;
	};

	struct CollisionSector
	{
		snMesh collisionMesh;
		//a list of ODE shapes also?
		//std::list<IGameNode*> collisionShapes;
	};

	std::list<IGameNode*> collisionShapes;

	int renderSectorDivision;
	sectorInfo renderSectorInfo;
	std::vector< RenderSector > renderSectors;

	sectorInfo collisionSectorInfo;
	std::vector< CollisionSector > collisionSectors;

	std::list<snMesh*> meshes;
	snMesh collisionMesh;
};

class ExportStatic : public Export
{
public:
	ExportStatic( IGameScene* scene, TCHAR *name, Interface* ip ) : Export( scene, name, ip ){}
};

#endif