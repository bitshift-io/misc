/**********************************************************************
 *<
	FILE: SiphonExporter.h

	DESCRIPTION:	Includes for Plugins

	CREATED BY:

	HISTORY:

 *>	Copyright (c) 2000, All Rights Reserved.
 **********************************************************************/

#ifndef __SIPHONEXPORTER__H
#define __SIPHONEXPORTER__H


#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

/*
#include "MaxScrpt/MaxScrpt.h"
#include "istdplug.h"
#include "stdmat.h"
#include "decomp.h"
#include "shape.h"
#include "interpik.h"
#include "modstack.h"
#include "iskin.h"
#include "utilapi.h"
#include "ikctrl.h"
#include "IIKSys.h"
#include "IKSolver.h"
#include "IKHierarchy.h"
#include "bipexp.h"
//geomimp.lib edmodel.lib comctl32.lib delayimp.lib maxscrpt.lib geom.lib 
*/

//#include "Max_Mem.h" 

#include "IGame\IGame.h"
#include "IGame\IGameObject.h"
#include "IGame\IGameProperty.h"
#include "IGame\IGameControl.h"
#include "IGame\IGameModifier.h"
#include "IGame\IConversionManager.h"
#include "IGame\IGameError.h"

#include "ExportStatic.h"
//igame.lib

#include "Math3D.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

#endif // __SIPHONEXPORTER__H
