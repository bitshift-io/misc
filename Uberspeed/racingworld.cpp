#include "checkpoint.h"
#include "racingworld.h"

bool RacingWorld::Load( char* file )
{
	bool ret = World::Load( file );

	//make a shadow for each mesh >-)

	std::vector<Mesh*>::iterator meshIt;
	std::list<Mesh*>::iterator renderIt;
	std::map<Material*, int>::iterator matIt;
/*
	for( int i = 0; i < renderSectors.size(); ++i )
	{
		for( meshIt = renderSectors[i].meshes.begin();
							meshIt != renderSectors[i].meshes.end(); ++meshIt )
		{
			//(*meshIt)->BuildShadow();
			(*meshIt)->BuildStaticShadow( Vector3(0,200,0) );
			printf(".");
		}
	}

	printf("what? all done?!\n");*/

	return false;
}

bool RacingWorld::ReadUnknown( Head& header )
{
	switch( header.type )
	{
	case CHECKPOINT:
		{
			sCheckpoint chkpnt;
			fread( &chkpnt, header.size, 1, pFile );
			checkPoints.push_back( Checkpoint( chkpnt ) );
		}
		break;
	default:
		fseek( pFile, header.size, SEEK_CUR );
		break;
	}
	
	return true; 
}

void RacingWorld::AddPlayer( Car* player )
{
	players.push_back( player );
	printf("Adding a player!!!\n");
}

void RacingWorld::Update( unsigned int tick )
{
	std::vector< Checkpoint >::iterator ckIt;
	for( ckIt = checkPoints.begin(); ckIt != checkPoints.end(); ++ckIt )
	{
		//is player in check point?
		std::vector< Car* >::iterator playerIt;
		for( playerIt = players.begin(); playerIt != players.end(); ++playerIt )
		{
			if( ckIt->PointInBox( (*playerIt)->GetPosition() ) )
			{
				(*playerIt)->SetCheckPoint( ckIt->GetID(), checkPoints.size() );
				//printf("INSIDE CHECK POINT!!!\n");
			}
		}

		/*
		//just use camera for time being...
		if( ckIt->PointInBox( RenderMgr::GetInstance().GetCamera().GetPosition() ) )
		{
			printf("INSIDE CHECK POINT!!!\n");
		}*/
	}

	World::Update( tick );
/*
	Matrix4 worldInverse;
	worldInverse.Translate(0,0,0);
	worldInverse.RotateX(0);
	worldInverse.RotateY(0);
	worldInverse.RotateZ(0);
	worldInverse.Inverse();
	Vector3 local = worldInverse * Vector3(0,400,0);

	std::map< Material*,  std::list<Mesh*> >::iterator renderIt;
	std::list<Mesh*>::iterator meshIt;
	for( renderIt = renderQue.begin(); renderIt != renderQue.end(); ++renderIt )
	{
		for( meshIt = renderIt->second.begin(); meshIt != renderIt->second.end(); ++meshIt )
		{
			//polys += (*meshIt)->Render( renderIt->first );

			(*meshIt)->BuildStaticShadow( Vector3& lightPos );
			(*meshIt)->CreateShadow( local );
		}
	}*/
}

int RacingWorld::Render( Camera& camera )
{
	int polys = World::Render( camera );
/*
	std::vector< Checkpoint >::iterator ckIt;
	for( ckIt = checkPoints.begin(); ckIt != checkPoints.end(); ++ckIt )
	{
		ckIt->Render( camera );
	}*/

	return polys;
}