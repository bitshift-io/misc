#ifndef _SIPHON_MATERIAL_
#define _SIPHON_MATERIAL_
#include <glee\glee.h>
//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <SDL/SDL.h>
#include <string>

#include "FileMgr.h"

//#include "TGA.h"

enum BLENDTYPE
{
	MASK,
	BLEND,
	SOLID
};

enum TEXTURE_TYPE
{
	TEX_DETAIL,
	TEX_BASE,
	TEX_LIGHTMAP
};


enum COLOR
{
	R = 0,
	G = 1,
	B = 2,
	A = 3
};

class Color
{
public:
	Color()
	{

	}

	Color( float r, float g, float b, float a = 1.0f )
	{
		color[0] = r;
		color[1] = g;
		color[2] = b;
		color[3] = a;
	}

	float& operator[]( const int index )
	{
		return color[index];
	}

	float* GetPointer()
	{
		return color;
	}

protected:
	float color[4];
};

enum UVCOORDINATE
{
	U = 0,
	V = 1
};


class UVCoord
{
public:
	float& operator[]( const int index )
	{
		return uvCoord[index];
	}

protected:
	float uvCoord[2];
};

class Texture : public Resource
{
public:
	Texture();
	bool Load( char* file, bool bMipMap = true );

	/**
	 * TGA's are used for 2D stuffs, eg: font
	 * which haave fine details
	 * and need losless compression
	 */
	bool LoadTGA( char* file );
	void SetTexture( int channel = 0 );

	int GetBlendType();

//protected:
	int width;
	int height;
	int bpp;
	int blendType;
	GLuint texture;
	std::string name;
};

class Material : public Resource
{
public:
	Material();
	bool SetMaterial( int channel = 0 );
	Texture* GetTexture();
	void SetTexture( Texture* texture );

//protected:

	float front_shininess;
	Color front_specular;
	Color front_diffuse;

	float back_shininess;
	Color back_specular;
	Color back_diffuse;

	Texture* texture;
	Texture* texture2;

	static Material* setMaterial;
};

#endif
