#ifndef _SIPHON_RENDERMGR_
#define _SIPHON_RENDERMGR_

#include <list>

class Model;

class RenderMgr : public Singleton<RenderMgr>
{
public:

	/**
	 * clear render que
	 * for this update
	 */
	void Update( unsigned int tick );

	/**
	 * Rendder our models :)
	 */
	void Render();

	/**
	 * add a model to the que for rendering
	 */
	void Que( Model* model );

	Camera& GetCamera();

protected:
	friend class Singleton<RenderMgr>;

	std::list< Model* > renderQue;
//	std::list< Light* > lights;
	Camera camera;
};

inline Camera& RenderMgr::GetCamera()
{
	return camera;
}

#endif