#ifndef _SIPHON_IMPORTMODEL_
#define _SIPHON_IMPORTMODEL_
/*
#define VERTICES		0
#define UVCOORDS		1
#define FACES			2
#define NORMALS			3
#define MATERIALREF		4
#define COLORS			5
#define WEIGHTS			6
#define ENDOFFILE		7
#define MESH_HEAD		8
#define MESH_END		9
#define TEXTUREREF		10
#define MATERIAL		11
#define VOLUME			12
#define COLLISION_HEAD	13
#define COLLISION_END	14
#define ODE_RECTANGLE	15
#define ODE_CYLINDER	16
#define ODE_SPHERE		17
#define SECTOR_RENDER_INFO		18
#define NEW_SECTOR_HEADER		19	
#define SECTOR_COLLISION_INFO	20
#define LIGHT_MAP				21

#pragma pack(push,1)
struct Head
{
	unsigned int type; //the type of the body
	long size; //the size of the body
	unsigned int noElements;   //the number of elements eg. 400 vertices
};

struct sectorInfo
{
	float startValue;
	float cellGap;
	int cols;
};

struct odeSphere
{
	float radius;
	float center[3];
};

struct odeCylinder
{
	float radis;
	float length;
	float center[3];
	float rotation[3];
};

struct odeRectangle
{
	float width;
	float length;
	float height;
	float center[3];
	float rotation[3];
};

struct sFace
{
	unsigned int idx[3];
};

struct sVertex
{
	float vert[3];
};

struct sUV
{
	float uv[2];
};

struct sMaterial
{
	sVertex specular;
	sVertex diffuse;
	float shine;
};
#pragma pop


class CollisionSector : public ColTriMesh
{
public:
	/**
	 * build collision info
	 * /
	bool Build();

	dSpaceID space;
	//a list of ODE shapes also?
};
*/
class ImportModel
{
public:
	bool Load( char* file );

	/*
	 * read in a chunk of info and then determine the function 
	 * thats should be processesd
	 */
	bool ReadChunk();

	//unsigned int GetNumVertices( int matId );
//protected:
	bool ReadVertices( Head& header, Vector3*& positions, unsigned int& noVertices );
	bool ReadFaces( Head& header, unsigned int*& indices, unsigned int& noIndices, unsigned int& noPolys );
	bool ReadUVCoords( Head& header );
	bool ReadNormals( Head& header );
	bool ReadColors( Head& header );

	bool ReadLightMap( Head& header );

	bool ReadSectorHeader( sectorInfo& sctrInfo, Head& header );
	/**
	 * This function is called on unknown chunks
	 * so if you extend this class
	 * you dont have to copy lots of code from here :)
	 */
	virtual bool ReadUnknown( Head& header );

	/*
	 * read the header, the type is the type of chunk
	 * the size is the size of the following data.
	 */
	bool ReadHead( Head& header );
	bool ReadMaterial( Head& header );

	FILE* pFile;

	std::list<Mesh*> meshes;

	sectorInfo collisionSectorInfo;
	//std::vector<ColTriMesh> collisionSectors;
	std::vector<CollisionSector> collisionSectors;
	ColTriMesh collisionMesh;


	int curHeadType; //the current header type: eg HEAD_MESH or HEAD_COLLISION

	struct RenderSector
	{
		std::vector<Mesh*> meshes;
		bool bVisible; 
	};

	unsigned int curSector;
	sectorInfo renderSectorInfo;
	std::vector< RenderSector > renderSectors;
};

#endif
