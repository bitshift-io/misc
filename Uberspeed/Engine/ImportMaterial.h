#ifndef _SIPHON_IMPORTMATERIAL_
#define _SIPHON_IMPORTMATERIAL_

class ImportMaterial
{
public:
	bool Load( char* file, Material* mat );

//protected:

	/*
	 * read in a chunk of info and then determine the function 
	 * thats should be processesd
	 */
	bool ReadChunk();

	bool ReadHead( Head& header );
	bool ReadTexture( Head& header );
	bool ReadMaterial( Head& header );

	FILE* pFile;
	Material* material; //material to load info into
};

#endif
