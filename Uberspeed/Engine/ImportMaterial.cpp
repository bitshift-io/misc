#include "SiphonGL.h"

Material* Material::setMaterial = 0;

bool ImportMaterial::Load( char* file, Material* mat )
{
	material = mat;

	pFile = 0;
	pFile = fopen( file, "rb" );

	if( pFile == 0 )
		return false;

	if( !ReadChunk() )
	{
		printf("ERROR:Read chunk failed\n");
		fclose( pFile );	
		return false;
	}

	fclose( pFile );
	return true;
}

bool ImportMaterial::ReadHead( Head& header )
{
    fread( &header, sizeof(Head), 1, pFile );
	return true;
}

bool ImportMaterial::ReadChunk()
{
	Head header;
	if( !ReadHead( header ) )
	{
		printf("ERROR:Read head failed\n");
		return false;
	}

	switch( header.type )
	{
	case TEXTUREREF:
		ReadTexture( header );
		break;
	case MATERIAL:
		ReadMaterial( header );
		break;
	case ENDOFFILE:
		return true;
		break;
	default:
		fseek( pFile, header.size, SEEK_CUR ); 
		break;
	};

	if( !feof( pFile ) )
		ReadChunk();

	return true;
}

bool ImportMaterial::ReadTexture( Head& header )
{
	char* texture = new char[ header.noElements ];
	fread( texture, header.size, 1, pFile );

	material->SetTexture( FileMgr::GetInstance().LoadTexture( texture ) );
	return true;
}

bool ImportMaterial::ReadMaterial( Head& header )
{
	sMaterial mat;
	fread( &mat, header.size, 1, pFile );

	material->front_shininess = mat.shine;

	for( int d = 0; d < 3; ++d )
		material->front_diffuse[d] = mat.diffuse.vert[d];

	for( int s = 0; s < 3; ++s )
		material->front_specular[s] = mat.specular.vert[s];

	return true;
}

