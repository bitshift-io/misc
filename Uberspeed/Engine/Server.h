#ifndef _SIPHON_SERVER_
#define _SIPHON_SERVER_

#include <string>
#include <vector>
#include <list>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_net.h>
#include "Singleton.h"
//#include "Client.h"

#define MAX_CONNECTIONS 32

struct PacketHead;


/**
 * this structure contains info on live objects,
 * so when a client joins,
 * they get a list of these objects
 */
struct NetObjInfo
{
	int netId; //its unique id
	std::string classId; //which class is this obj
	int ownerId; //who owns it
	std::string message; //the packet, saves reprocessing
};

struct ClientInfo
{
	TCPsocket socket;
	int clientId; //this users id
	bool bReady; //ready to transmit update infos
};

class Server : public Singleton<Server>
{
public:

	bool Create( int port = 1555 );
	bool AcceptReliable();
	bool OpenReliable( int port = 1555 );

	bool IsServer();

	void Update( unsigned int tick );

	void CheckReciveReliable();

	bool SendClientId( ClientInfo& client );

	/**
	 * This function send all obj info when they join
	 */
	bool SendObjectList( ClientInfo& client );

	bool RecivePacket( ClientInfo* from );
	bool SendReliable( void* data, PacketHead& header, ClientInfo* from );

protected:
	friend class Singleton<Server>;

	~Server();
	Server();

	std::list< NetObjInfo > gameObjects;

	std::vector< ClientInfo > clients;
	SDLNet_SocketSet reliableClientsSet;
	IPaddress ip;
	TCPsocket serverReliable;
	bool bServer; //is this the server? that has been created
	int noClients; //used to give clients a unique id
	int noNetObjs; //used to create netobjs
};

#endif
