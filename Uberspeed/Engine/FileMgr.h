#ifndef _SIPHON_FILEMGR_
#define _SIPHON_FILEMGR_

#include <vector>

class Mesh;
class Material;
class Texture;

class Resource
{
public:
	char* file;
protected:

};

class FileMgr : public Singleton<FileMgr>
{
public:
	FileMgr();
	~FileMgr();

	/*
	 * Load a mesh. If it exits return the pointer
	 * else load a new model, then return the pointer
	 */
	Mesh* LoadMesh( char* file );

	Texture* LoadTexture( char* file );

	/**
	 * Loads a sound, and returns a copy
	 */
	Sound LoadSound( char* file );

	/*
	 * Load a material. If it exits return the pointer
	 * else load a new material, then return the pointer
	 */
	Material* LoadMaterial( char* file );
protected:
	std::vector<Mesh*> meshes;
	std::vector<Texture*> textures;
	std::vector<Material*> materials;
	std::vector<Sound*> sounds;
};

#endif

