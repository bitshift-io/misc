#include "SiphonGL.h"

FileMgr::FileMgr()
{

}

FileMgr::~FileMgr()
{
	printf("CLEANING UP!!!!\n");

	//free meshes
	std::vector<Mesh*>::iterator meshIt; 
	for( meshIt = meshes.begin(); meshIt != meshes.end(); meshIt++ )
	{
		delete (*meshIt);
		(*meshIt) = 0;
	}

	//free textures
	std::vector<Texture*>::iterator texIt;
	for( texIt = textures.begin(); texIt != textures.end(); texIt++ )
	{
		delete (*texIt);
		(*texIt) = 0;
	}

	//free materials
	std::vector<Material*>::iterator matIt; 
	for( matIt = materials.begin(); matIt != materials.end(); matIt++ )
	{
		delete (*matIt);
		(*matIt) = 0;
	}
}

Mesh* FileMgr::LoadMesh( char* file )
{	/*
	std::vector<Mesh*>::iterator meshIt; 
	for( meshIt = meshes.begin(); meshIt != meshes.end(); meshIt++ )
	{
		if( strcmp( (*meshIt)->file,  file ) == 0 )
		{
			return (*meshIt);
		}
	}*/

	Mesh* m = new Mesh();
//	m->file = (char*)malloc( sizeof(char) * (strlen(file) + 1) );
//	memcpy( m->file, file, sizeof(char) * (strlen(file) + 1) );
	ImportModel s;
	s.Load( file );

	memcpy( m, s.meshes.front(), sizeof(Mesh) );

	meshes.push_back( m );


	return m;
}

Sound FileMgr::LoadSound( char* file )
{
	std::vector<Sound*>::iterator sndIt; 
	for( sndIt = sounds.begin(); sndIt != sounds.end(); sndIt++ )
	{
		if( strcmp( (*sndIt)->file,  file ) == 0 )
		{
			return *(*sndIt);
		}
	}

	std::string sFile = std::string("data/") + std::string( file );

	Sound* s = new Sound();
	s->file = file;
	if( !s->LoadSound( (char*)sFile.c_str() ) )
	{
		delete s;
		//return 0;
	}
	
	sounds.push_back( s ); 

	return *s;
}

Texture* FileMgr::LoadTexture( char* file )
{	
	std::vector<Texture*>::iterator texIt; 
	for( texIt = textures.begin(); texIt != textures.end(); texIt++ )
	{
		if( strcmp( (*texIt)->file,  file ) == 0 )
		{
			return (*texIt);
		}
	}

	std::string sFile = std::string("data/") + std::string( file );

	Texture* t = new Texture();
	t->file = file;
	if( !t->Load( (char*)sFile.c_str() ) )
	{
		delete t;
		return 0;
	}
	
	textures.push_back( t ); 

	return t;
}

Material* FileMgr::LoadMaterial( char* file )
{
	std::vector<Material*>::iterator matIt; 
	for( matIt = materials.begin(); matIt != materials.end(); matIt++ )
	{
		if( strcmp( (*matIt)->file,  file ) == 0 )
		{
			return (*matIt);
		}
	}

	std::string sFile = std::string("data/") + std::string( file );

	ImportMaterial im;
	Material* m = new Material();
	m->file = file;
	if( !im.Load( (char*)sFile.c_str(), m ) )
	{
		delete m;
		printf("ERROR: Failed to load material: %s\n", sFile.c_str() );
		return 0;
	}

	materials.push_back( m );

	return m;
}
