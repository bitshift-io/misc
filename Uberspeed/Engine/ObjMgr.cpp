#include "ObjMgr.h"

std::map<  const char*, RegBase* > ObjMgr::classes;
std::list< Obj* > ObjMgr::objects;

void ObjMgr::Shutdown()
{
	std::list< Obj* >::iterator objIt; // ObjMgr::objects

	for( objIt = objects.begin(); objIt != objects.end(); ++objIt )
	{
		delete (*objIt);
		(*objIt) = 0;
	}
}

void ObjMgr::Update( unsigned long tick )
{
	std::list< Obj* >::iterator objIt; // ObjMgr::objects

	for( objIt = objects.begin(); objIt != objects.end(); ++objIt )
	{
		(*objIt)->Update( tick );
	}
}

void ObjMgr::Register( const char* str, RegBase* obj )
{
	classes[ str ] = obj;
	//classes.insert( 4, obj );
	printf("a new object has been registered: %s\n", str);
}

Obj* ObjMgr::Create( const char* classId )
{
	printf("> Request to create: %s\nsize: %i", classId,  classes.size() );
	std::map< const char*, RegBase* >::iterator classIt = classes.find( classId );


	if( classIt != classes.end() )
	{
		return (Obj*)classIt->second->Create();
	}

	for( classIt = classes.begin(); classIt != classes.end(); ++classIt )
	{
		//printf( "comapring '%s' to '%s'\n", classIt->first, classId);
		if( strcmp( classIt->first, classId ) == 0 )
		{
			printf("\ncreating: %s\n", classIt->first );
			return (Obj*)classIt->second->Create();
		}
	}

	return 0;
}

bool ObjMgr::RegisterObject( Obj* obj )
{

	printf("object registered\n");
	objects.push_back( obj );

	return true;
}


//------------------------------------------------------------------------
/*
ObjReg<Obj> regReg("object");
*/
