#ifndef SIPHON_SINGLETON
#define SIPHON_SINGLETON

/*! \class Singleton
 *	\brief singletong template
 *
 * This template provides the creation of
 * only one single instance of a class.
 */
template<class T> class Singleton
{
public:
	/**
	 * A static verable that returns the only instance
	 * of the class
	 * \return
	 * The only instance of the class
	 */
	inline static T& GetInstance()
	{
		if( !instance )
			instance = new T();

		return *instance;
	}

	/**
	 * Returns a pointer and not a reference
	 */
	inline static T* GetPointer()
	{
		if( !instance )
			instance = new T();

		return instance;
	}

	/**
	 * Used to set the pointer to the instance
	 * used for overwritting classes
	 */
	static void SetInstance( T* newInstance )
	{
		if( !instance )
			delete instance;

		instance = newInstance;
	}

	/**
	 * Deletes any existing instance of the class.
	 * This should be called when cleaning up
	 * or exiting the application
	 */
	static void Destroy()
	{
		delete instance;
		instance = 0;
	}

protected:
	Singleton()
	{
	};

	virtual ~Singleton()
	{
		//delete instance;
		//instance = 0;
	}

	static T* instance;
};

template<class T> T* Singleton<T>::instance = 0;

#endif

