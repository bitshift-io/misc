#include "SiphonGL.h"

bool SFont::CreateFont( char* file )
{
	if( !font.LoadImage( file, false ) )
		return false;

	//font2.Load( file, false );

	glBindTexture( GL_TEXTURE_2D, font.GetTexture() );

	//chars
	chars = glGenLists(256);

	glColor3f(1.0f,1.0f,1.0f);

	int sizeX = 14, sizeY = 16;
	float width = (1.0f / 16.0f);
	for( int i = 0; i < 16; ++i )              // Loop Through All 256 Lists
  	{
		for( int j = 0; j < 16; ++j)
		{
    			//int cx=(float)(i%16);                  // X Position Of Current Character
    			//int cy=(float)(i/16);                  // Y Position Of Current Character

    			glNewList( chars+((j*16)+i), GL_COMPILE );            // Start Building A List

			//printf("CREATING CHAR: %i ( %c ) row:%i col:%i\n", ((i*16)+j), ((i*16)+j) + 32 , i, j);
			//printf("UV coords: %f %f - %f %f\n", width*i, width*j, width*i + width, width*j + width);

			glBindTexture( GL_TEXTURE_2D, font.GetTexture() );
			glColor3f(1.0f,1.0f,1.0f);

    			glBegin( GL_QUADS );                           // Use A Quad For Each Character

			//glTexCoord2f( cx+0.0625f,1.0f-cy-0.001f );   // Texture Coord (Top Right)
			glTexCoord2f( width*i + width, 1 - (width*j) );
      			glVertex2i( sizeX,0 );                          // Vertex Coord (Top Right)

			//glTexCoord2f( cx+0.0625f,1.0f-cy-0.0625f );  // Texture Coord (Bottom Right)
			glTexCoord2f( width*i + width, 1 - (width*j + width) );
      			glVertex2i( sizeX,sizeY );                         // Vertex Coord (Bottom Right)

			//glTexCoord2f( cx,1.0f-cy-0.0625f );          // Texture Coord (Bottom Left)
			glTexCoord2f( width*i, 1 - (width*j + width) );
      			glVertex2d( 0,sizeY );                          // Vertex Coord (Bottom Left)

			//glTexCoord2f( cx,1.0f-cy-0.001f );           // Texture Coord (Top Left)
			glTexCoord2f( width*i, 1 - (width*j) );
      			glVertex2i( 0,0 );                           // Vertex Coord (Top Left)


			glEnd();                                     // Done Building Our Quad (Character)
    			glTranslated(sizeX,0,0);                        // Move To The Right Of The Character

    			glEndList();                                 // Done Building The Display List
		}
  	}

	return true;
}

bool SFont::Print( float x, float y, char* text, ... )
{
	va_list ap;                                    // Pointer To List Of Arguments
	char newText[1024];

 	if (text == NULL)                               // If There's No Text
    		return false;                                      // Do Nothing

  	va_start(ap, text);                             // Parses The String For Variables
  	vsprintf(newText, text, ap);                       // And Converts Symbols To Actual Numbers
  	va_end(ap);

	TextInfo newTextInf;
	newTextInf.str = std::string( newText );
	newTextInf.x = x;
	newTextInf.y = y;
	this->text.push_front( newTextInf );
}

bool SFont::DrawText( float x, float y, char* text )
{
	GLint blendSrc,blendDst, blendOn;
	blendOn = glIsEnabled(GL_BLEND);
  	glGetIntegerv(GL_BLEND_SRC, &blendSrc);
	glGetIntegerv(GL_BLEND_DST, &blendDst);

	glEnable(GL_TEXTURE_2D);
	glEnable( GL_BLEND );
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	glMatrixMode(GL_PROJECTION); // and initalize Ortho mode
  	glPushMatrix();
  	glLoadIdentity();
  	glOrtho(0.0f, 800,600, 0.0f,-1.0f,1.0f);

  	glMatrixMode(GL_MODELVIEW);
  	glPushMatrix();

	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(GL_FALSE);

	//glColor3f(1.0f,1.0f,1.0f);
	//font.SetTexture();

	glLoadIdentity();
  	glTranslated( x, y, 0 );
	//glScalef( scalex,sizey,1.0f);
  	glListBase( chars-32 );

	glBindTexture( GL_TEXTURE_2D, font.GetTexture() );

	for( int i  = 0 ; i < strlen(text); i++ )
	{
		//printf("CHAR: %c, INT: %i SHOULD BE: %i\n", text[i], text[i], (text[i] - 32) );
		if( text[i] == '\n' )
		{
			y += 16;
			glLoadIdentity();
			glTranslated( x, y, 0 );
		}
		else
		{
			glCallList( (chars + (text[i] - 32))  );
		}
	}

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);

	glMatrixMode(GL_PROJECTION);
  	glPopMatrix();
  	glMatrixMode(GL_MODELVIEW);
  	glPopMatrix();

	glBlendFunc(blendSrc, blendDst);

	return true;
}

void SFont::Render()
{
	std::list<TextInfo>::iterator txtIt;
	for( txtIt = text.begin(); txtIt != text.end(); ++txtIt )
	{
		DrawText( txtIt->x, txtIt->y, (char*)txtIt->str.c_str() );	
	}
}

void SFont::Update( unsigned int tick )
{
	text.clear();
}
