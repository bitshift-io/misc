#include "SiphonGL.h"
#include <glee\glee.h>

Texture::Texture() : blendType( SOLID )
{

}

bool Texture::Load( char* file, bool bMipMap )
{
	name = std::string( file );

	DDS dds;
	if( dds.Load( file, bMipMap ) )
	{
		texture = dds.GetTexture();
		dds.GetInfo( width, height, bpp, blendType );

		return true;
	}
	else
	{
		return false;
	}
	/*
	TGA tga;
	if( tga.LoadImage( file, bMipMap ) )
	{
		texture = tga.GetTexture();
		tga.GetInfo( width, height, bpp, blendType );

		return true;
	}
	else
	{
		return false;
	}*/
}

void Texture::SetTexture( int channel )
{
	if( blendType == BLEND )
	{
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //sorting required
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE); //no sorting required :)
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glAlphaFunc(GL_GREATER, 0.0);
		
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);
		//glDepthMask(GL_FALSE);
	}
	else if( blendType == MASK )
	{
	//	glDisable(GL_BLEND);
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GREATER, 0.5);

	//	glFrontFace(GL_CW);
	//	glCullFace(GL_BACK);
		glDisable(GL_CULL_FACE);
	}
	else
	{
		glDisable(GL_BLEND);
		glEnable(GL_CULL_FACE);
		glDisable(GL_ALPHA_TEST);
	}

//	glDisable(GL_TEXTURE_2D);

//	if( gluxIsAvailable("GL_ARB_multitexture") == GLUX_AVAILABLE  )
/*	if( channel >= 0 && channel < 32 )
	{
		glActiveTextureARB( GL_TEXTURE0_ARB + channel );
		/*
		0x84C0
		switch( channel )
		{
		case 0:
			glActiveTextureARB( GL_TEXTURE0_ARB );
			break;
		case 1:
			glActiveTextureARB( GL_TEXTURE0_ARB );
			break;
		default:
			glActiveTextureARB( GL_TEXTURE0_ARB );
		}* /
	}
	else
	{
		glActiveTextureARB( GL_TEXTURE0_ARB );
	}*/

	glBindTexture( GL_TEXTURE_2D, texture );

	

	//printf("Set Texture: %s\n", file );
}

bool Texture::LoadTGA( char* file )
{
	name = std::string( file );

	TGA tga;
	if( tga.LoadImage( file, false ) )
	{
		texture = tga.GetTexture();
		tga.GetInfo( width, height, bpp, blendType );
		return true;
	}
	else
	{
		return false;
	}
}

int Texture::GetBlendType()
{
	return blendType;
}

Material::Material()/* : front_specular( 0.5, 0.5, 0.5 ), front_diffuse( 1.0, 1.0, 1.0 ),
	back_specular( 0.0, 0.0, 1.0 ), back_diffuse( 1.0, 0.0, 0.0 ),
	front_shininess(30.0), back_shininess(50.0)*/
{

}

bool Material::SetMaterial( int channel )
{

//	if( setMaterial == this )
//		return true;

//	setMaterial = this;

	texture->SetTexture( channel );

/*
	glMaterialf(GL_FRONT, GL_SHININESS, front_shininess);

	glMaterialfv(GL_FRONT, GL_SPECULAR, front_specular.GetRaw() );
	glMaterialfv(GL_FRONT, GL_DIFFUSE, front_diffuse.GetRaw() );
	glMaterialf(GL_BACK, GL_SHININESS, back_shininess );
	glMaterialfv(GL_BACK, GL_SPECULAR, back_specular.GetRaw() );
	glMaterialfv(GL_BACK, GL_DIFFUSE, back_diffuse.GetRaw() );
*/
	return true;
}

Texture* Material::GetTexture()
{
	return texture;
}

void Material::SetTexture( Texture* texture )
{
	this->texture = texture;
}
