//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <SDL/SDL.h>

//#include <stdarg.h>
//#include <stdio.h>
//#include <string.h>
//#include <stdlib.h>

#include "DDS.h"
#include <list>
/**
 * when draw string is called, the info is qued
 * and it is rendered last
 */
struct TextInfo
{
	std::string str;
	float x, y;
};

class SFont
{
public:
	bool CreateFont( char* file );
	bool DrawText( float x, float y, char* text );
	bool Print( float x, float y, char* text, ... );

	void Render();

	void Update( unsigned int tick );

protected:
	TGA font;
	DDS font2;
	//Material font;
	GLuint chars;

	std::list<TextInfo> text;
};

