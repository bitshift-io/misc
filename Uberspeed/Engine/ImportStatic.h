#ifndef _SIPHON_IMPORTSTATIC_
#define _SIPHON_IMPORTSTATIC_

#include "ImportDefs.h"

class CollisionSector : public ColTriMesh
{
public:
	/**
	 * build collision info
	 */
	bool Build();

	dSpaceID space;
	//a list of ODE shapes also?
};

class ImportStatic
{
public:
	bool Load( char* file );

	/*
	 * read in a chunk of info and then determine the function 
	 * thats should be processesd
	 */
	bool ReadChunk();

	//unsigned int GetNumVertices( int matId );
//protected:
	bool ReadVertices( Head& header, Vector3*& positions, unsigned int& noVertices );
	bool ReadFaces( Head& header, unsigned int*& indices, unsigned int& noIndices, unsigned int& noPolys );
	bool ReadUVCoords( Head& header );
	bool ReadNormals( Head& header );
	bool ReadColors( Head& header );

	bool ReadLightMap( Head& header );

	bool ReadSpawn( Head& header );
	bool ReadSound( Head& header );

	/**
	 * read the custom string related to this custom info
	 */
	bool ReadCustomString( Head& header );

	bool ReadSectorHeader( sectorInfo& sctrInfo, Head& header );
	/**
	 * This function is called on unknown chunks
	 * so if you extend this class
	 * you dont have to copy lots of code from here :)
	 */
	virtual bool ReadUnknown( Head& header );

	/*
	 * read the header, the type is the type of chunk
	 * the size is the size of the following data.
	 */
	bool ReadHead( Head& header );
	bool ReadMaterial( Head& header );

	FILE* pFile;

	std::list<Mesh*> meshes;

	sectorInfo collisionSectorInfo;
	//std::vector<ColTriMesh> collisionSectors;
	std::vector<CollisionSector> collisionSectors;
	ColTriMesh collisionMesh;


	int curHeadType; //the current header type: eg HEAD_MESH or HEAD_COLLISION or HEAD_CUSTOM

	struct RenderSector
	{
		std::vector<Mesh*> meshes;
		bool bVisible; 
	};

	unsigned int curSector;
	sectorInfo renderSectorInfo;
	std::vector< RenderSector > renderSectors;

	std::list< sCustData > customData;
	std::vector< sSpawn > spawnPoints;
//	std::vector< sSound > sounds;
	std::vector< Sound > sounds;
};

#endif
