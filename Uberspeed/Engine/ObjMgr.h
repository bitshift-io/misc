#ifndef _SIPHON_OBJMGR_
#define _SIPHON_OBJMGR_

#include <map>
#include <string>
#include <list>

class Obj;
class RegBase;

class ObjMgr
{
public:
	static void Register(  const char* str, RegBase* obj );
	static Obj* Create( const char* classId );

	/*
	 * called by an objects constructor
	 * if it returns false, it means it wasnt added
	 * which also means a net object shouldnt be registered!
	 *
	 */
	static bool RegisterObject( Obj* obj );
;
	/*
	 * called to update all objects
	 */
	static void Update( unsigned long tick );

	/*
	 * delete all objects
	 */
	static void Shutdown();
	//static void Update( unsigned int tick );

protected:
	static std::map<  const char*, RegBase* > classes; //a list of classes that can be created
	static std::list< Obj* > objects; //a list of all game objects
};

class RegBase
{
public:

	virtual void* Create()
	{
		return 0;
	}
};

template<class T> class ObjReg : public RegBase
{
public:
	ObjReg( const char* classId )
	{
		ObjMgr::Register( classId, (RegBase*)this );
	}

	//virtual Obj* Create() = 0;

	void* Create()
	{
		printf("Creating the class complete@!!!\n");
		return (void*)new T;
		//return 0;
	}

protected:

};

class Obj
{
public:
	//virtual Obj* Create() = 0;

	//virtual Obj* Create() = 0;

	virtual void Update( unsigned long tick )
	{

	}

protected:

};



// SAMPLE FACTORY REGISTRATION:
// make sure this goes in your cpp file!
//ObjRegister<Obj> selfReg("object");

/*
class Register
{
	Register( std::string sClass )
	{  
		ObjMgr::Register( this, sClass );
	}
	
	virtual Obj* Create() = 0;
};

class ChatRegister : public Register
{  
	ChatRegister( std::string sClass )
	{  
		Register::Register("chat");
	}  

	Obj* Create()
	{  
		return new Chat();
	}  
	
	static ChatRegister self; //created at load time to register self}
};
chatRegister ChatRegister::self("chat");
*/

#endif

