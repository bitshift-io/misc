#ifndef _SIPHON_PHYSICSOBJ_
#define _SIPHON_PHYSICSOBJ_

#define GRAD_PI 565.48 //180.0 / 3.1415926535897932384626433832795;

class PhysicsObj// : public Object
{
public:
	PhysicsObj();

	//virtual void Update( long tick );

	dBodyID GetBody();
	dGeomID GetGeomID();
	dSpaceID GetSpace();
	void SetSpace( dSpaceID space );

	void GetPosition( Vector3& position );
	void GetRotation( Vector3& rotation );

	void QuaternionToEuler( const dQuaternion quaternion, Vector3& euler );
	void MatrixToEuler( dReal matrix[12], Vector3& euler );

protected:
	dBodyID body;
	dGeomID geom;
	dSpaceID space;
};

inline void PhysicsObj::GetRotation( Vector3& rotation )
{
	dQuaternion quat;
	dGeomGetQuaternion( geom, quat );
	QuaternionToEuler( quat, rotation );

//	dReal* matrix = (dReal*)dBodyGetRotation( body );
//	MatrixToEuler( matrix, rotation );
}

inline void PhysicsObj::MatrixToEuler( dReal matrix[12], Vector3& euler )
{
	euler[YAW] = asin( matrix[2] );
	float C = cos( euler[YAW] );
}

inline void PhysicsObj::QuaternionToEuler( const dQuaternion quaternion, Vector3& euler )
{
  dReal w,x,y,z;

  w=quaternion[0];
  x=quaternion[1];
  y=quaternion[2];
  z=quaternion[3];

  double sqw = w*w;    
  double sqx = x*x;    
  double sqy = y*y;    
  double sqz = z*z; 
  
  euler[YAW] = RtoD( atan2(2.0 * (x*y + z*w),(sqx - sqy - sqz + sqw)) );
  euler[PITCH] = RtoD( atan2(2.0 * (y*z + x*w),(-sqx - sqy + sqz + sqw)) );  
  euler[ROLL] = RtoD( asin(-2.0 * (x*z - y*w)) );
}

inline void PhysicsObj::GetPosition( Vector3& position )
{
	const dReal* pos = dBodyGetPosition( GetBody() );
	position = Vector3( pos[0], pos[1], pos[2] );
	//printf("POs: %f %f %f\n", pos[0], pos[1], pos[2]);
}

inline dBodyID PhysicsObj::GetBody()
{
	return body;
}

inline dGeomID PhysicsObj::GetGeomID()
{
	return geom;
}

inline dSpaceID PhysicsObj::GetSpace()
{
	return space;
}

inline void PhysicsObj::SetSpace( dSpaceID space )
{
	this->space = space;
}

#endif
