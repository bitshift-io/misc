#include "SiphonGL.h"

Camera::Camera() : position(0,0,0), view(0,0,1), right(1,0,0), up(0,1,0), rotation(0,180,0), bFreeCam(true)
{
	LookAt();

	SetupFrustum();
}

void Camera::SetupFrustum()
{/*
	int width, height;
	GLDevice::GetInstance().GetDeviceSize( width, height );
	//glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	// Calculate The Aspect Ratio Of The Window
	gluPerspective( 90.0f, (GLfloat)width/(GLfloat)height, 0.1f, 1000.0f );
	
*/
	//the proper way:
	Matrix4 proj;
	proj.LoadCurrent( GL_PROJECTION_MATRIX );
	projection.LoadCurrent( GL_PROJECTION_MATRIX );
	proj.Transpose();
/*
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
*/
	//http://www2.ravensoft.com/users/ggribb/plane%20extraction.pdf

	//left plane
	frustum.planes[0][0] = proj[12] + proj[0];
	frustum.planes[0][1] = proj[13] + proj[1];
	frustum.planes[0][2] = proj[14] + proj[2];
	frustum.planes[0][3] = proj[15] + proj[3];

	//right plane
	frustum.planes[1][0] = proj[12] - proj[0];
	frustum.planes[1][1] = proj[13] - proj[1];
	frustum.planes[1][2] = proj[14] - proj[2];
	frustum.planes[1][3] = proj[15] - proj[3];

	//top plane
	frustum.planes[2][0] = proj[12] - proj[4];
	frustum.planes[2][1] = proj[13] - proj[5];
	frustum.planes[2][2] = proj[14] - proj[6];
	frustum.planes[2][3] = proj[15] - proj[7];

	//bottom plane
	frustum.planes[3][0] = proj[12] + proj[4];
	frustum.planes[3][1] = proj[13] + proj[5];
	frustum.planes[3][2] = proj[14] + proj[6];
	frustum.planes[3][3] = proj[15] + proj[7];

	//near plane
	frustum.planes[4][0] = proj[12] + proj[8];
	frustum.planes[4][1] = proj[13] + proj[9];
	frustum.planes[4][2] = proj[14] + proj[10];
	frustum.planes[4][3] = proj[15] + proj[11];

	//far plane
	frustum.planes[5][0] = proj[12] - proj[8];
	frustum.planes[5][1] = proj[13] - proj[9];
	frustum.planes[5][2] = proj[14] - proj[10];
	frustum.planes[5][3] = proj[15] - proj[11];

	//find intersection of planes
	//left top near
	frustum.points[0] = frustum.planes[0].Intersect3( frustum.planes[2], frustum.planes[4] ); 
	//	Plane3Intersect( frustum.planes[0], frustum.planes[2], frustum.planes[4]); 
	//printf("POINT: %f %f %f\n", frustum.points[0][X], frustum.points[0][Y], frustum.points[0][Z]);

	//right top near
	frustum.points[1] = frustum.planes[1].Intersect3( frustum.planes[2], frustum.planes[4] );
		// Plane3Intersect( frustum.planes[1], frustum.planes[2], frustum.planes[4]); 
	//printf("POINT: %f %f %f\n", frustum.points[1][X], frustum.points[1][Y], frustum.points[1][Z]);

	//left bottom near
	frustum.points[2] = frustum.planes[0].Intersect3( frustum.planes[3], frustum.planes[4] );
		//Plane3Intersect( frustum.planes[0], frustum.planes[3], frustum.planes[4]); 
	//printf("POINT: %f %f %f\n", frustum.points[2][X], frustum.points[2][Y], frustum.points[2][Z]);

	//right bottom near
	frustum.points[3] = frustum.planes[1].Intersect3( frustum.planes[3], frustum.planes[4] );
		//Plane3Intersect( frustum.planes[1], frustum.planes[3], frustum.planes[4]); 
	//printf("POINT: %f %f %f\n", frustum.points[3][X], frustum.points[3][Y], frustum.points[3][Z]);

	//left top far
	frustum.points[4] = frustum.planes[0].Intersect3( frustum.planes[2], frustum.planes[5] );
		//Plane3Intersect( frustum.planes[0], frustum.planes[2], frustum.planes[5]); 
	//printf("POINT: %f %f %f\n", frustum.points[4][X], frustum.points[4][Y], frustum.points[4][Z]);

	//right top far
	frustum.points[5] = frustum.planes[1].Intersect3( frustum.planes[2], frustum.planes[5] );
		//Plane3Intersect( frustum.planes[1], frustum.planes[2], frustum.planes[5]); 
	//printf("POINT: %f %f %f\n", frustum.points[5][X], frustum.points[5][Y], frustum.points[5][Z]);

	//left bottom far
	frustum.points[6] = frustum.planes[0].Intersect3( frustum.planes[3], frustum.planes[5] );
		//Plane3Intersect( frustum.planes[0], frustum.planes[3], frustum.planes[5]); 
	//printf("POINT: %f %f %f\n", frustum.points[6][X], frustum.points[6][Y], frustum.points[6][Z]);

	//right bottom far
	frustum.points[7] = frustum.planes[1].Intersect3( frustum.planes[3], frustum.planes[5] );
		//Plane3Intersect( frustum.planes[1], frustum.planes[3], frustum.planes[5]); 
	//printf("POINT: %f %f %f\n", frustum.points[7][X], frustum.points[7][Y], frustum.points[7][Z]);
}

/*
Vector3 Camera::Plane3Intersect( Plane& p1, Plane& p2, Plane& p3 )
{
	Vector3 n1 = Vector3( p1.a, p1.b, p1.c );
	Vector3 n2 = Vector3( p2.a, p2.b, p2.c );
	Vector3 n3 = Vector3( p3.a, p3.b, p3.c );
	float d1 = p1.d;
	float d2 = p2.d;
	float d3 = p3.d;

	//http://astronomy.swin.edu.au/~pbourke/geometry/3planes/
	Vector3 point = ((n2.CrossProduct( n3 ) * d1) + (n1.CrossProduct( n3 ) * d2) + (n1.CrossProduct( n2 ) * d3))
		/ n1.Dot( n2.CrossProduct( n3 ) );

	return point;
}*/

void Camera::Rotate( float angle, float x, float y, float z )
{
	view.Rotate(angle, x, y, z);
	view.Normalize();
}

void Camera::Update( unsigned long tick )
{
	if( bFreeCam )
		FreeCam();

	Transform();
	SoundListener::GetInstance().SetListenerPosition(position[0],position[1],position[2]);
}

void Camera::SetFreeCam( bool bFreeCam )
{
	this->bFreeCam = bFreeCam;
}

void Camera::SetPosition( float x, float y, float z )
{
	position[X] = x;
	position[Y] = y;
	position[Z] = z;
}

void Camera::FreeCam()
{
	int new_x = 0, new_y = 0;
	Input::GetInstance().GetMouseState( new_x, new_y );

	rotation[YAW] -= (float)new_x;

	//if( rotation[PITCH] + new_y > 90 && rotation[PITCH] + new_y < 270 )
		rotation[PITCH] += (float)new_y;

	Matrix4 m;
	m.RotateY( rotation[YAW] / 100.0f );
	m.RotateX( rotation[PITCH] / 100.0f );
/*
	up = Vector3( m[0][1], m[1][1], m[2][1] );
	view = Vector3( m[0][2], m[1][2], m[2][2] );
	right = Vector3( m[0][0], m[1][0], m[2][0] );*/

	/**
 * 4x4 matrix 
 *
 * m[0]  m[4]  m[ 8]  m[12]
 * m[1]  m[5]  m[ 9]  m[13]
 * m[2]  m[6]  m[10]  m[14]
 * m[3]  m[7]  m[11]  m[15]
 *
 *   x axis - column 0
 *  y axis - column 1
 *   z axis - column 3
 */

	up = Vector3( m[4], m[5], m[6] );
	view = Vector3( m[8], m[9], m[10] );
	right = Vector3( m[0], m[4], m[8] );

	if( Input::GetInstance().KeyDown(SDLK_s) )
	{
		position =  (view * 1.0) + position;
	}

	if( Input::GetInstance().KeyDown(SDLK_w) )
	{
		position =  position - (view * 1.0);
	}

	if( Input::GetInstance().KeyDown(SDLK_a)  )
	{
		//Vector3 right( m[0][0], m[1][0], m[2][0] );
		position = position - (right * 1.0);
	}

	if( Input::GetInstance().KeyDown(SDLK_d) )
	{
		//Vector3 right( m[0][0], m[1][0], m[2][0] );
		position = position + (right * 1.0);
	}

	if( Input::GetInstance().KeyDown(SDLK_SPACE) )
	{
		position[Y] += 1.0;
	}

	if( Input::GetInstance().KeyDown(SDLK_LCTRL) )
	{
		position[Y] -= 1.0;
	}

	return;
}

/**
 * 4x4 matrix 
 *
 * m[0]  m[4]  m[ 8]  m[12]
 * m[1]  m[5]  m[ 9]  m[13]
 * m[2]  m[6]  m[10]  m[14]
 * m[3]  m[7]  m[11]  m[15]
 *
 *   x axis - column 0
 *  y axis - column 1
 *   z axis - column 3
 */
void Camera::Transform()
{
	transform[ 0] = right[X];
	transform[ 4] = right[Y];
	transform[ 8] = right[Z];

	transform[ 1] = up[X];
	transform[ 5] = up[Y];
	transform[ 9] = up[Z];

	transform[ 2] = view[X];
	transform[ 6] = view[Y];
	transform[10] = view[Z];

	transform[12] = -( (position[X] * right[X]) + (position[Y] * right[Y]) + (position[Z] * right[Z]) );
	transform[13] = -( (position[X] * up[X]) + (position[Y] * up[Y]) + (position[Z] * up[Z]) );
	transform[14] = -( (position[X] * view[X]) + (position[Y] * view[Y]) + (position[Z] * view[Z]) );

	//set world matrix also
	world[ 0] = -right[X];
	world[ 1] = -right[Y];
	world[ 2] = -right[Z];

	world[ 4] = -up[X];
	world[ 5] = -up[Y];
	world[ 6] = -up[Z];

	world[ 8] = -view[X];
	world[ 9] = -view[Y];
	world[10] = -view[Z];
	world.Translate( position[X], position[Y], position[Z] );
}

BOUNDS Camera::GetTransformedBounds()
{
	BOUNDS worldBounds;

	for( int i = 0 ; i < 8; ++i )
	{
		worldBounds.points[i] = world * frustum.points[i];
	}

	return worldBounds;
}

void Camera::LookAt()
{
	transform.Set();
}

Vector3& Camera::GetPosition()
{
	return position;
}

Vector3& Camera::GetRotation()
{
	return view;
}

