#include "SiphonGL.h"

ShadowMgr::ShadowMgr()
{
	indices = new unsigned int[ 65536 ];
	vertices = new Vector3[ 65536 ];

	noIndices = 0;
	noVertices = 0;
	noPolys = 0;

	bCompiled = false;

	staticShadowVerts = 0;
	staticShadowIndxs = 0;

	shadowPolys = 0;

	printf("CREATING SHADOW MANAGER\n");
}

ShadowMgr::~ShadowMgr()
{
	SAFE_DELETE_ARRAY(indices);
	SAFE_DELETE_ARRAY(vertices);
}

void ShadowMgr::Update()
{
	noIndices = 0;
	noVertices = 0;
	noPolys = 0;
}

bool ShadowMgr::BuildStaticShadows()
{
	noStaticVertices = shadowVerts.size();
	noStaticIndices = shadowIndxs.size();

	staticShadowVerts = new Vector3[ noStaticVertices ];
	staticShadowIndxs = new unsigned int[ noStaticIndices ];

	std::list< Vector3 >::iterator vertIt;// shadowVerts;
	std::list< unsigned int >::iterator idxIt; // shadowIndxs;
	//for( int v = 0; v < noStaticVertices; ++v )

	int i = 0;
	for( vertIt = shadowVerts.begin(); vertIt != shadowVerts.end(); ++vertIt, ++i )
		staticShadowVerts[i] = (*vertIt);

	i = 0;
	//for( int i = 0; i < noStaticIndices; ++i )
	for( idxIt = shadowIndxs.begin(); idxIt != shadowIndxs.end(); ++idxIt, ++i )
		staticShadowIndxs[i] = (*idxIt);

	if( __GLEE_GL_ARB_vertex_buffer_object && GLDevice::GetInstance().UseVBO() )
	{	
		glGenBuffersARB( 1, &vboVertices );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboVertices );
		glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Vector3) * noStaticVertices, staticShadowVerts, GL_STATIC_DRAW_ARB );

		glGenBuffersARB( 1, &vboIndices );
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, vboIndices );
		glBufferDataARB( GL_ELEMENT_ARRAY_BUFFER_ARB, sizeof(unsigned int) * noStaticIndices, staticShadowIndxs, GL_STATIC_DRAW_ARB );

		glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0 );

		bCompiled = true;
	}

	shadowVerts.clear();
	shadowIndxs.clear();

	return true;
}

int ShadowMgr::Render( Camera& camera )
{
	glPushMatrix();
	glLoadIdentity();
	camera.LookAt();

	//printf("polys: %i, idx: %i vert: %i\n", noPolys, noIndices, noVertices );

	glDisable(GL_LIGHTING);
	glDepthMask(GL_FALSE);
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_STENCIL_TEST);
	glColorMask(0, 0, 0, 0);
	glStencilFunc(GL_ALWAYS, 1, 0xffffffff);

	// first pass, stencil operation decreases stencil value
	glFrontFace(GL_CCW);
	glStencilOp(GL_KEEP, GL_KEEP, GL_INCR);
/*
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer( 3, GL_FLOAT, sizeof(Vector3), vertices );
	glDrawElements( GL_TRIANGLES, noPolys*3, GL_UNSIGNED_INT, indices );
*/

	if( bCompiled )
	{
		glEnableClientState( GL_VERTEX_ARRAY );
		glEnableClientState( GL_INDEX_ARRAY );

		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboVertices );
		glVertexPointer( 3, GL_FLOAT, sizeof(Vector3), 0 );	
			
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, vboIndices );
		glIndexPointer( GL_UNSIGNED_INT, sizeof(unsigned int), 0 );

		glDrawElements( GL_TRIANGLES, shadowPolys*3, GL_UNSIGNED_INT, 0);
	}
	else
	{
		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer( 3, GL_FLOAT, sizeof(Vector3), staticShadowVerts );
		glDrawElements( GL_TRIANGLES, shadowPolys*3, GL_UNSIGNED_INT, staticShadowIndxs );
	}


	// second pass, stencil operation increases stencil value
	glFrontFace(GL_CW);
	glStencilOp(GL_KEEP, GL_KEEP, GL_DECR);
/*
	glDrawElements( GL_TRIANGLES, noPolys*3, GL_UNSIGNED_INT, indices );
	glDisableClientState(GL_VERTEX_ARRAY);
*/

	if( bCompiled )
	{
		glDrawElements( GL_TRIANGLES, shadowPolys*3, GL_UNSIGNED_INT, 0);
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0 );
	}
	else
	{
		glDrawElements( GL_TRIANGLES, shadowPolys*3, GL_UNSIGNED_INT, staticShadowIndxs );
		glDisableClientState(GL_VERTEX_ARRAY);
	}

	glColorMask(1, 1, 1, 1);
	glFrontFace(GL_CW);

		//glEnable(GL_DEPTH_TEST);
		//glDepthFunc(GL_LEQUAL);
		//glDepthMask(GL_TRUE);

		//glDepthFunc(GL_LEQUAL);
		//glDepthMask(GL_TRUE);
		//glEnable(GL_LIGHTING);
		//glDisable(GL_STENCIL_TEST);
		//glShadeModel(GL_SMOOTH);

	//glDisable(GL_LIGHTING);
	//glDepthMask(GL_FALSE);
	//glDepthFunc(GL_LEQUAL);
	//glEnable(GL_STENCIL_TEST);

	glMatrixMode(GL_PROJECTION); // and initalize Ortho mode
  	glPushMatrix();
  	glLoadIdentity();
  	glOrtho(0.0f, 800,600, 0.0f,-1.0f,1.0f);

  	glMatrixMode(GL_MODELVIEW);
  	glPushMatrix();

	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(GL_FALSE);

	//draw a quad over screen bringing stencil operations into site ;p
	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(GL_FALSE);

	glColor4f(0.0f, 0.0f, 0.0f, 0.4f);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glStencilFunc(GL_NOTEQUAL, 0, 0xffffffff);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	glLoadIdentity();

	glBegin(GL_TRIANGLE_STRIP);

		glVertex2f( 0, 600 );
		glVertex2f( 0, 0 );
		glVertex2f( 800, 600 );
		glVertex2f( 800, 0 );		

	glEnd();

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);

	glMatrixMode(GL_PROJECTION);
  	glPopMatrix();
  	glMatrixMode(GL_MODELVIEW);
  	glPopMatrix();

	glFrontFace(GL_CW);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	//glEnable(GL_LIGHTING);
	glDisable(GL_STENCIL_TEST);
	//glShadeModel(GL_SMOOTH);
	glDisable(GL_BLEND);

	glPopMatrix();
	return noPolys;
}