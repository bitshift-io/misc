#include "SiphonGL.h"

/*
bool CollisionSector::Build()
{
	space = dHashSpaceCreate(0);

	if( noVertices == 0 || noIndices == 0 || positions == 0 || indices == 0 )
		return false;

	printf("Building collision mesh! novert: %i noin: %i\n", noVertices, noIndices);
	meshData = dGeomTriMeshDataCreate();

	dVector3* newVerts = new dVector3[noVertices];
	int* newIdxs = new int[noIndices];

	for( int i = 0; i < noIndices; i+=3 )
	{
		newIdxs[i + 0] = indices[i + 2];
		newIdxs[i + 1] = indices[i + 1];
		newIdxs[i + 2] = indices[i + 0];
	}

	for( int v = 0; v < noVertices; ++v )
	{
		newVerts[v][0]= positions[v][0];
		newVerts[v][1]= positions[v][1];
		newVerts[v][2]= positions[v][2];
	}

#ifdef WIN32
	dGeomTriMeshDataBuildSimple( meshData, (dReal*)newVerts, noVertices,
			    newIdxs, noIndices );

#else
	dGeomTriMeshDataBuildSimple( meshData, newVerts, noVertices,
			    newIdxs, noIndices );
#endif

	geom = dCreateTriMesh( space, meshData, 0, 0, 0 );

	return true;
}
*/
bool ImportModel::Load( char* file )
{
	pFile = 0;
	pFile = fopen( file, "rb" );

	if( pFile == 0 )
		return false;

	printf("Loading model: %s\n", file );

	curSector = 1;
	renderSectors.resize( 1 );

	if( !ReadChunk() )
	{
		printf("ERROR:Read chunk failed\n");
		fclose( pFile );	
		return false;
	}
	printf("DONE\n");
/*
	//BUILD COLLISION MESHES
	std::vector<CollisionSector>::iterator collisionIt;
	for( collisionIt = collisionSectors.begin(); collisionIt != collisionSectors.end();
		++collisionIt )
	{
		collisionIt->Build();
	}
*/
	fclose( pFile );
	printf("CLOSED\n");
	return true;
}

bool ImportModel::ReadChunk()
{
	Head header;
	if( !ReadHead( header ) )
	{
		printf("ERROR:Read head failed\n");
		return false;
	}

	switch( header.type )
	{
/*	case SECTOR_RENDER_INFO:
		{
			curHeadType = MESH_HEAD;
			ReadSectorHeader( renderSectorInfo, header );
			curSector = 0;
			renderSectors.resize( renderSectorInfo.cols * renderSectorInfo.cols );
		}
		break;
	case SECTOR_COLLISION_INFO:
		{
			curHeadType = COLLISION_HEAD;
			ReadSectorHeader( collisionSectorInfo, header );
			curSector = 0;
			collisionSectors.resize( collisionSectorInfo.cols * collisionSectorInfo.cols );
		}
		break;
	case NEW_SECTOR_HEADER:
		++curSector;
		break;
	case COLLISION_HEAD:
		{
			//printf("COLLISION\n");
			//indicate that we are now reading collision info
			curHeadType = COLLISION_HEAD;
		}
		break;*/
	case MESH_HEAD:
		{
			//printf("HEAD\n");
			curHeadType = MESH_HEAD;

			//build front before pushing on a new mesh
			if( meshes.size() >= 1 )
				meshes.front()->Build();


			meshes.push_front( new Mesh );
			//printf("Loading new model into sector: %i\n", curSector - 1 );
			//renderSectors[ curSector - 1 ].meshes.push_back( &meshes.front() );
		}
		break;
	case FACES:
		{
			//if( curHeadType == MESH_HEAD )
				ReadFaces( header, meshes.front()->indices, meshes.front()->noIndices, meshes.front()->noPolys );	
		/*	else
			{
				//printf("COLLISION FACE\n");
				ReadFaces( header, collisionSectors[ curSector - 1 ].indices, 
					collisionSectors[ curSector - 1 ].noIndices, 
					collisionSectors[ curSector - 1 ].noPolys );	
			}*/
		}
		break;
	case VERTICES:
		{
		//	if( curHeadType == MESH_HEAD )
				ReadVertices( header, meshes.front()->positions, meshes.front()->noVertices );
		/*	else
			{
				//printf("COLLISION VERT\n");
				ReadVertices( header, collisionSectors[ curSector - 1 ].positions, 
					collisionSectors[ curSector - 1 ].noVertices );
			}*/
		}
		break;
	case NORMALS:
		ReadNormals( header );
		break;
	case UVCOORDS:
		//printf("uv's found!\n");
		ReadUVCoords( header );
		break;
/*	case COLORS:
		ReadColors( header );
		break;*/
	case MATERIALREF:
		ReadMaterial( header );
		break;
/*	case LIGHT_MAP:
		ReadLightMap( header );
		break;*/
	case ENDOFFILE:
		{
			if( meshes.size() >= 1 )
					meshes.front()->Build();

			//collisionMesh.Build();

			return true;
		}
		break;
	default:
		ReadUnknown( header );
		break;
	};

	if( !feof( pFile ) )
		ReadChunk();

	return true;
}

bool ImportModel::ReadUnknown( Head& header )
{
	fseek( pFile, header.size, SEEK_CUR );
	return true; 
}

bool ImportModel::ReadSectorHeader( sectorInfo& sctrInfo, Head& header )
{
	fread( &sctrInfo, sizeof(Head), 1, pFile );

//	printf("SECTOR INFO:\n");
//	printf("cellGap: %f\n", sctrInfo.cellGap);
//	printf("cols: %i\n", sctrInfo.cols);
//	printf("startValue: %f\n", sctrInfo.startValue);

	return true;
}

bool ImportModel::ReadHead( Head& header )
{
    fread( &header, sizeof(Head), 1, pFile );
	return true;
}

bool ImportModel::ReadFaces( Head& header, unsigned int*& indices, unsigned int& noIndices, unsigned int& noPolys )
{
	sFace* faces = new sFace[ header.noElements ];
	bool ret = fread( faces, header.size, 1, pFile );

	noIndices = header.noElements * 3;
	noPolys = header.noElements;
	indices = new unsigned int[ header.noElements * 3 ];

	int k = 0;
	for( int i = 0; i < header.noElements; ++i, k += 3 )
	{
		indices[k + 0] = faces[i].idx[0];
		indices[k + 1] = faces[i].idx[1];
		indices[k + 2] = faces[i].idx[2];
	}

	delete[] faces;

	return ret;
}

bool ImportModel::ReadVertices( Head& header, Vector3*& positions, unsigned int& noVertices )
{
	sVertex* vertices = new sVertex[ header.noElements ];
	bool ret = fread( vertices, header.size, 1, pFile );

	noVertices = header.noElements;
	positions = new Vector3[ header.noElements ];
	for( int i = 0; i < header.noElements; ++i )
	{
		for( int j = 0; j < 3; ++j )
		{
			positions[i][j] = vertices[i].vert[j];
		}
	}

	delete[] vertices;

	return ret;
}

bool ImportModel::ReadUVCoords( Head& header )
{
	sUV* uvs = new sUV[ header.noElements ];
	bool ret = fread( uvs, header.size, 1, pFile );

	Mesh& m = *meshes.front();

	if( m.uvCoords == 0 )
	{
		m.uvCoords = new UVCoord[ header.noElements ];
		for( int i = 0; i < header.noElements; ++i )
		{
			for( int j = 0; j < 2; ++j )
			{
				m.uvCoords[i][j] = uvs[i].uv[j];
				//printf("%f ", m.uvCoords[i][j] );
			}
		}		
	}
	else
	{
		m.uvCoords2 = new UVCoord[ header.noElements ];
		for( int i = 0; i < header.noElements; ++i )
		{
			for( int j = 0; j < 2; ++j )
			{
				m.uvCoords2[i][j] = uvs[i].uv[j];
				//printf("%f ", m.uvCoords2[i][j] );
			}
		}
	}

	delete[] uvs;
	return ret;
}

bool ImportModel::ReadNormals( Head& header )
{
	sVertex* normals = new sVertex[ header.noElements ];
	bool ret = fread( normals, header.size, 1, pFile );

	Mesh& m = *meshes.front();
	m.normals = new Vector3[ header.noElements ];
	for( int i = 0; i < header.noElements; ++i )
	{
		for( int j = 0; j < 3; ++j )
		{
			m.normals[i][j] = normals[i].vert[j];
		}
	}

	delete[] normals;
	return ret;
}

bool ImportModel::ReadMaterial( Head& header )
{
	char* material = new char[ header.noElements ];
	fread( material, header.size, 1, pFile );
	meshes.front()->defaultMaterial = FileMgr::GetInstance().LoadMaterial( material );

	printf("reading material: %s\n", material);

	return true;
}

bool ImportModel::ReadLightMap( Head& header )
{
	if( meshes.front()->defaultMaterial == 0 )
	{
		printf("ERROR: Lightmap being read before material created!!!\n");
		return false;
	}

	char* lightMap = new char[ header.noElements ];
	fread( lightMap, header.size, 1, pFile );
	meshes.front()->defaultMaterial->texture2 = FileMgr::GetInstance().LoadTexture( lightMap );

	return true;
}

bool ImportModel::ReadColors( Head& header )
{
	sVertex* colors = new sVertex[ header.noElements ];
	bool ret = fread( colors, header.size, 1, pFile );

	Mesh& m = *meshes.front();
	m.colors = new Color[ header.noElements ];
	for( int i = 0; i < header.noElements; ++i )
	{
		for( int j = 0; j < 3; ++j )
		{
			m.colors[i][j] = colors[i].vert[j];
			if( m.colors[i][j] < 0 )
				printf("bad color!!!!\n");
		}
		m.colors[i][3] = 1.0;
	}

	delete[] colors;
	return ret;
}
