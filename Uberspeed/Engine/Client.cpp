#include "Client.h"
#include "SDL_thread.h"

Client::Client() : bBlockNext(false), bConnected(false), bConnecting(false)
{

}

Client::~Client()
{
	SDLNet_Quit();
}

bool Client::Connect( char* address, int port )
{
	IPaddress ip;

	if( SDLNet_Init()==-1 )
	{
    		printf("SDLNet_Init: %s\n", SDLNet_GetError());
	}

	SDLNet_ResolveHost( &ip, address, port );
	if ( ip.host == INADDR_NONE )
	{
		printf("Couldn't resolve hostname\n");
		return false;
	}

	reliableSet = SDLNet_AllocSocketSet( 1 );
	if( !reliableSet )
	{
    		printf("SDLNet_AllocSocketSet: %s\n", SDLNet_GetError());
		return false;
	}

	reliableSocket = SDLNet_TCP_Open( &ip );
	if ( reliableSocket == NULL )
	{
		printf("SDLNet_TCP_Open: Connect failed\n");
		return false;
	}

	int noUsed = SDLNet_TCP_AddSocket( reliableSet, reliableSocket );
	if( noUsed == -1 )
	{
    		printf("SDLNet_AddSocket: %s\n", SDLNet_GetError());
		return false;
	}

	printf("Connected.\n");
	bConnecting = true;

	return true;
}

void Client::Disconnect()
{
	SDLNet_TCP_Close( reliableSocket );
	bConnected = false;
}

bool Client::IsConnected()
{
	return bConnected;
}

bool Client::SendReliable( void* data, int length, NetObj* obj, unsigned char to )
{
	if( !IsConnected() )
		return false;

	//create and send packet header
	PacketHead header;
	header.size = length;
	
	if( obj )
	{
		header.netObjId = obj->netId;
	//	header.netObjId = obj->netId;
		printf("Client: ID ASSIGNED: %i SENDING MESSAGE\n", header.netObjId );
	}
	else
		header.netObjId = 0;

	header.to = to;
	SDLNet_TCP_Send( reliableSocket, &header, sizeof(PacketHead) );

	//then send message
	int bytes = SDLNet_TCP_Send( reliableSocket, data, length );
	printf("Client: bytes sent to server: %i\n", bytes );
	return bytes;
}

void Client::RecivePacket()
{
	//read header
	PacketHead header;
	SDLNet_TCP_Recv( reliableSocket, &header, sizeof(PacketHead) );

	//now read the body of the packet
	void* body = malloc( header.size );
	SDLNet_TCP_Recv( reliableSocket, body, header.size );

	printf("Packet recived: %i\n", header.size );

	//is this packet for us to deal with right here?
	// TO_CLIENT means is prolly sent by server
	if( header.to == TO_CLIENT )
	{
		int message;
		sscanf( (char*)body, "%i", &message );
		if( message == CLIENTID )
		{
			//if we already have our client id,
			// return saying the server we can now 
			// recive general update info
			if( bConnected )
			{
				//create message and send to server
				//char message[256];
				//sprintf( message, "%i %s %i", CREATE, obj->GetClassId(), clientId ); //create class myid (im the owner!)

				//printf("Client: Send request: %s\n", message );

				//create and send packet header
				//PacketHead header;
				//header.size = strlen(message) + 1; //dont for get the \0
				header.to = TO_SERVER;

				SDLNet_TCP_Send( reliableSocket, &header, sizeof(PacketHead) );
				SDLNet_TCP_Send( reliableSocket, body, header.size );

				free(body);
				return;
			}

			bConnected = true;

			//CLIENTID - this means we now have our ID
			// lets spam the server with OUR netobjects
			sscanf( (char*)body, "%i %i", &message, &clientId );

			printf( "Client: We have recived our client id: %i\n", clientId );

			std::list< NetObj* >::iterator objIt;
			for( objIt = quedNetworkObjects.begin(); objIt != quedNetworkObjects.end(); ++objIt )
			{
				RequestCreate( (*objIt) );
			}
		}
		else if( message == CREATE )
		{
			//extract the class, and the owner...
			char classId[256];
			int ownerId;
			int netId;
			sscanf( (char*)body, "%i %s %i %i", &message, &classId, &ownerId, &netId );

			printf("Client: We having been instructed to create '%s', owner: %i, id: %i.\n", classId, ownerId, netId);
			//are we the owner?

			if( ownerId == clientId )
			{
				printf("Client: WE ARE OWNER!\n");
				//yep, assign it to one of our obj's of type class
				std::list< NetObj* >::iterator objIt;
				for( objIt = quedNetworkObjects.begin(); objIt != quedNetworkObjects.end(); ++objIt )
				{
					if( strcmp( (*objIt)->GetClassId(), classId ) == 0 )
					{
						break;
					}
				}

				if( objIt != quedNetworkObjects.end() )
				{
					//remove from list, and move into the networkObjects
					// list
					//(*objIt)->netId = netId;
					//(*objIt)->ownerId = ownerId;
					NetObj* obj = (*objIt);
					obj->netId = netId;
					obj->ownerId = -1; //ownerId;
					networkObjects[ netId ] = (*objIt);
					//networkObjects[ netId ]->SetNetId( netId );
					quedNetworkObjects.erase( objIt );

					printf("Client: so we are here? list size: %i NET ID ASSIGNED: %i\n", quedNetworkObjects.size(), netId );
				}
				else
				{
					printf("Client: we request a creation for no object?\n");
					printf("Client: list size: %i\n", quedNetworkObjects.size() );
				}
			}
			else
			{
				printf("Client: GONNA CALL CREATE!!! ID ASSIGNED: %i, '%s'\n", netId, classId );
				bBlockNext = true;
				NetObj* newObj = (NetObj*)ObjMgr::Create( classId );
				if( newObj )
				{
					newObj->netId = netId;
					newObj->ownerId = ownerId;
					if( newObj->IsNetObject() )
						printf("Client: this should be a net object: id: %i, owner: %i\n", netId, ownerId );

					//newObj->AnotherFn();

					//char* string = newObj->GetClassId();
					networkObjects[ netId ] = newObj;
					//printf("Client: adding class '%s' to location: %i\n", newObj->GetClassId(), netId );
				}
				else
				{
					printf("Client: A error has occured, no obj returned.\n");
				}
			}
		}
	}
	else
	{
		printf("Client: Attempting to send message to appropriate class.\n");
		//TODO: I AM HERE!!!
		//find the netobj this is associated with:
		//std::map< int, NetObj* > networkObjects
		NetObj* obj = networkObjects[ header.netObjId ];

		if( obj )
		{
			//printf("Client: sending message to '%s'\n", body );
			if( !obj->Recive( body, header.size ) )
			{
				printf("Client: Failed to recive\n");
			}
		}
		else
		{
			printf("Client: Failed %i not found\n", header.netObjId );
			printf("Client: size: %i\n", networkObjects.size() );
		}

	}

	free(body);
	printf("done!\n");
}

void Client::Update( unsigned int tick )
{
	if( !bConnecting )
		return;

	//tell objects to send thier info!
	std::map< int, NetObj* >::iterator netIt;
	for( netIt = networkObjects.begin(); netIt != networkObjects.end(); ++netIt )
	{
		netIt->second->Send();
	}

	//for( int i = 0; i < 10; ++i )
	//{
		//see if any incomming info
		CheckReciveReliable();
	//}
}

SDLNet_SocketSet Client::CreateSocketSet()
{
	static SDLNet_SocketSet set=NULL;
	int i;

	if( set )
		SDLNet_FreeSocketSet(set);

	set = SDLNet_AllocSocketSet( 1 );
	if( !set )
	{
		printf("SDLNet_AllocSocketSet: %s\n", SDLNet_GetError());
		exit(1); //most of the time this is a major error, but do what you want.
	}
	SDLNet_TCP_AddSocket( set, reliableSocket );

	return set;
}

void Client::CheckReciveReliable()
{
	int noReady;

	//SDLNet_SocketSet set = Client::CreateSocketSet();
	noReady = SDLNet_CheckSockets( reliableSet, 0 );
	//noReady = SDLNet_CheckSockets( set, 0 );
	if( noReady == -1 )
	{
		//has a client disconnected?
    		printf("SDLNet_CheckSockets: %s\n", SDLNet_GetError());
		return;
	}

	if( noReady == 1 )
	{
		//while( SDLNet_SocketReady( reliableSocket ) )
		//{
			//RecivePacket( &(*clientIt) );
			RecivePacket();
		//}

		
	}

	//if( SDLNet_SocketReady( reliableSocket ) )
	//{

	//}
}

bool Client::RegisterNetObject( NetObj* obj )
{
	if( bBlockNext )
	{
		bBlockNext = false;
		printf("hooked!\n");
		return false;
	}
	printf("Client: NET object registered\n");

	printf("setting ownerid to -1\n");
	obj->ownerId = -1; //indicate we are owner


	//send a create request to server with my
	//client id,
	//then the server will send a message back
	// with a create, and my client id,
	// and a id for my netobject
	// this tells me i can use this id
	// on ANY object as long as its the same class
	quedNetworkObjects.push_back( obj );

	printf("Client: CUR SIZE: %i\n", quedNetworkObjects.size() );

	return RequestCreate( obj );
}

bool Client::RequestCreate( NetObj* obj )
{
	if( !IsConnected() )
	{
		printf("REQUEST BLOCKED, NOT CONNECTED, WILL SEND WHEN CONNECTED\n");
		return false;
	}

	//create message and send to server
	char message[256];
	sprintf( message, "%i %s %i", CREATE, obj->GetClassId(), clientId ); //create class myid (im the owner!)

	printf("Client: Send request: %s\n", message );

	//create and send packet header
	PacketHead header;
	header.size = strlen(message) + 1; //dont for get the \0
	header.to = TO_SERVER;

	SDLNet_TCP_Send( reliableSocket, &header, sizeof(PacketHead) );

	//then send message - and the \0!!!!!
	return SDLNet_TCP_Send( reliableSocket, message, (strlen(message) + 1 * sizeof(char)) );
}
