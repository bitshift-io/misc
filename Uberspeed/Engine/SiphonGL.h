#ifndef _SIPHON_SIPHONGL_
#define _SIPHON_SIPHONGL_

#define SAFE_DELETE(p){ if(p){delete p; p = 0;} }
#define SAFE_DELETE_ARRAY(p){ if(p){delete[] p; p = 0;} }

#ifdef WIN32

#pragma comment(lib, "GLee.lib")
#include <glee\glee.h>

#pragma comment(lib, "ALut.lib")
#pragma comment(lib, "OpenAL32.lib")

//#pragma comment(lib, "glux-mt.lib")
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLu32.lib")
#pragma comment(lib, "GLaux.lib")

#pragma comment(lib, "SDL.lib")
#pragma comment(lib, "SDLmain.lib")

#pragma comment(lib, "SDL_net.lib")

#pragma comment(lib, "ode.lib")
#pragma comment(lib, "opcode.lib")

//#pragma comment(lib, "fmodvc.lib")

//#pragma comment(lib, "opcode_d.lib")

#include <windows.h>
#include <GL/glaux.h>

#endif
/*
#include "fmod/fmod.h"
#include "fmod/fmod_errors.h"
*/
//#include <glux/glux.h>
//#include <GL/gl.h>
//#include <GL/glu.h>	
//#include <GL/glext.h>

#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>

#ifdef WIN32
#include <alc.h>
#include <al.h>
#include <alut.h>
#endif

#ifdef LINUX
#include <al/alc.h>
#include <al/al.h>
#include <al/alut.h>
#endif

#include <ode/ode.h>
//#include <opcode.h>

#include <set>
#include <string>
#include <map>
#include <vector>
#include <list>

#include "stdlib.h"
#include "stdio.h"

#include "INI.h"
#include "Singleton.h"
#include "Input.h"
#include "PhysicsMgr.h"
#include "Sound.h"

#include "DDS.h"
#include "FileMgr.h"
#include "TGA.h"
#include "Font.h"
#include "GLDevice.h"
#include "Math3D.h"
#include "PhysicsObj.h"

#include "Camera.h"
#include "RenderMgr.h"
#include "Material.h"
#include "Light.h"
#include "WorldExtras.h"
#include "ObjMgr.h"
#include "NetObj.h"
#include "Client.h"
#include "Server.h"
#include "Model.h"
#include "ShadowMgr.h"
#include "ImportStatic.h"
#include "ImportMaterial.h"
#include "ImportModel.h"
#include "World.h"

#endif


