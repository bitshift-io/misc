#ifndef _SIPHON_PHYSICSMGR_
#define _SIPHON_PHYSICSMGR_

/**
 * This class deals with collision and physics
 * simulations
 */
class PhysicsMgr : public Singleton<PhysicsMgr>
{
public:
	void NearCollisionCallback(void *data, dGeomID o1, dGeomID o2);

	virtual void Update( unsigned long tick );

	dWorldID GetWorld();
	dSpaceID* GetSpace();

	void Initialize();

//protected:
	friend class Singleton<PhysicsMgr>;
	PhysicsMgr();


	dJointGroupID contactgroup;
	dWorldID world;
	dSpaceID space;
};

inline dWorldID PhysicsMgr::GetWorld()
{
	return world;
}

inline dSpaceID* PhysicsMgr::GetSpace()
{
	return &space;
}

#endif
