#include "SiphonGL.h"

SoundListener::SoundListener()
{
/*
	if( !FSOUND_Init(44100, 32, 0) )
    {
        printf("Init FMOD Failed:\n\t%s\n", FMOD_ErrorString(FSOUND_GetError()));
        return;
    }
	*/

	//Open device
	device = alcOpenDevice((ALubyte*)"DirectSound3D");

	if( device == NULL )
	{
		//we failed to initialize the device
		printf("Failed to Get openAL device\n");
		exit(-1);
	}

	//Create context(s)
	context = alcCreateContext( device, NULL );
	//Set active context
	alcMakeContextCurrent( context );

	alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);

	// Clear Error Code
	alGetError();
}

SoundListener::~SoundListener()
{
	/*
	FSOUND_Close();
	*/

	//Get active context
	context=alcGetCurrentContext();
	//Get device for active context
	device=alcGetContextsDevice(context);
	//Disable context
	alcMakeContextCurrent(NULL);
	//Release context(s)
	alcDestroyContext(context);
	//Close device
	alcCloseDevice(device);
}

void SoundListener::Update( unsigned long tick )
{/*
	FSOUND_Update();*/
}

void SoundListener::SetListenerPosition(float x, float y, float z)
{
	//set the position using 3 seperate floats
	alListener3f(AL_POSITION, x,y,z);
/*
	float pos[3] = {x,y,z};
	float vel[3] = {0,0,0};
	FSOUND_3D_Listener_SetAttributes(pos, vel, 0, 0, -1, 0, 1, 0);*/
}

void SoundListener::SetListenerOrientation(float fx, float fy, float fz, float ux, float uy, float uz)
{
	//set the orientation using an array of floats
	float vec[6];
	vec[0] = fx;
	vec[1] = fy;
	vec[2] = fz;
	vec[3] = ux;
	vec[4] = uy;
	vec[5] = uz;
	alListenerfv(AL_ORIENTATION, vec);
}

bool Sound::LoadSound(char fname[40], bool looping)
{/*
	fSound = 0;

//	if( looping )
		fSound = FSOUND_Sample_Load(FSOUND_FREE, fname, FSOUND_HW3D | FSOUND_LOOP_NORMAL, 0, 0);
//	else
//		FSOUND_Sample_Load(FSOUND_FREE, "engine.wav", FSOUND_HW3D | FSOUND_LOOP_NORMAL, 0, 0);

	if( !fSound )
	{
		printf("Load sound failed: %s\n\t%s\n", fname, FMOD_ErrorString(FSOUND_GetError()));
		return;
	}*/

	//load our sound
	ALboolean loop;
	loop = looping;
	alutLoadWAVFile(fname,&alFormatBuffer, (void **) &alBuffer,(unsigned int *)&alBufferLen, &alFreqBuffer, &loop);

	alGenSources(1, &alSource);
	alGenBuffers(1, &alSampleSet);
	alBufferData(alSampleSet, alFormatBuffer, alBuffer, alBufferLen, alFreqBuffer);
	alSourcei(alSource, AL_BUFFER, alSampleSet);

	alutUnloadWAV(alFormatBuffer, alBuffer, alBufferLen, alFreqBuffer);			

	//set the pitch
	alSourcef(alSource,AL_PITCH,1.0f);
	//set the gain
	alSourcef(alSource,AL_GAIN,0.5f);
	//set looping to true
	if( looping )
	 alSourcei(alSource,AL_LOOPING,AL_TRUE);

	alSourcef(alSource, AL_REFERENCE_DISTANCE, 20.0);

	return true;
}

void Sound::SetRadius( float radius )
{
	alSourcef(alSource, AL_REFERENCE_DISTANCE, radius );
	alSourcef(alSource, AL_ROLLOFF_FACTOR, radius / 4 );
}

void Sound::SetLooping( bool state )
{
	alSourcei( alSource, AL_LOOPING, state );
}

void Sound::SetProperties(float x, float y, float z, float vx, float vy, float vz)
{/*
	float pos[3] = { x, y, z };
    float vel[3] = { vx, vy, vz };

	//start sound paused
	fChannel = FSOUND_PlaySoundEx(FSOUND_FREE, fSound, NULL, TRUE);
	FSOUND_3D_SetAttributes(fChannel, pos, vel);*/

	//set the sounds position and velocity
	alSource3f(alSource,AL_POSITION,x,y,z);
	alSource3f(alSource,AL_VELOCITY,vx,vy,vz);
}

//this function makes a sound source relative so all direction and velocity
//parameters become relative to the source rather than the listener
//useful for background music that you want to stay constant relative to the listener
//no matter where they go
void Sound::SetSourceRelative()
{
	alSourcei(alSource,AL_SOURCE_RELATIVE,AL_TRUE);
}

void Sound::PlaySound()
{
/*	
    //fChannel = FSOUND_PlaySoundEx(FSOUND_FREE, fSound, NULL, TRUE);

	if( !FSOUND_SetPaused( fChannel, FALSE ) )
    {
		printf("Failed to play sound:\n\t%s\n", FMOD_ErrorString(FSOUND_GetError()));
    }
   */ 

	ALint state;
	alGetSourcei(alSource, AL_SOURCE_STATE, &state);
	if( state != AL_PLAYING )
	{
		alSourcePlay(alSource);
	}
}

void Sound::StopSound()
{/*
	if( !FSOUND_SetPaused( fChannel, TRUE ) )
    {
		printf("Failed to stop sound:\n\t%s\n", FMOD_ErrorString(FSOUND_GetError()));
    }*/

	alSourceStop(alSource);
}

void Sound::DestroySound()
{/*
	FSOUND_Sample_Free( fSound );*/

	alDeleteSources(1,&alSource);
	alDeleteBuffers(1,&alSampleSet);
}
