#include "SiphonGL.h"

bool SkyBox::Create( char* texTop, char* texLeft,
	char* texRight, char* texFront, char* texBack )
{
	int size = 20;

	glColor3f(1.0f,1.0f,1.0f);

	textures[BACK].Load( texBack, false );
	//glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	//glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );

	textures[RIGHT].Load( texRight, false );
	//glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	//glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );

	textures[FRONT].Load( texFront, false );
	//glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	//glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );

	textures[TOP].Load( texTop, false );
	//glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	//glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );

	textures[LEFT].Load( texLeft, false );
	//glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	//glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );

	model = glGenLists(1);
	glNewList( model, GL_COMPILE );

	glDepthMask(GL_FALSE); 

	glActiveTextureARB( GL_TEXTURE0_ARB );
	glEnable(GL_TEXTURE_2D);

	// BACK ( -Z )
	textures[BACK].SetTexture();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBegin(GL_QUADS);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( size, size, -size );

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( size, -size, -size );

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f( -size, -size, -size );

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f( -size, size, -size );
	glEnd();

	// FRONT ( +Z )
	textures[FRONT].SetTexture();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBegin(GL_QUADS);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( -size, size, size );

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( -size, -size, size );

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f( size, -size, size );

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f( size, size, size );
	glEnd();

	// TOP ( +Y )
	textures[TOP].SetTexture();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f( size, size, size );

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f( size, size, -size );

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( -size, size, -size );

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( -size, size, size );
	glEnd();

	// LEFT ( -X );
	textures[LEFT].SetTexture();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f( -size, size, size );

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( -size, size, -size );

		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( -size, -size, -size );

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f( -size, -size, size );
	glEnd();

	// RIGHT ( +X )
	textures[RIGHT].SetTexture();
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBegin(GL_QUADS);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f( size, -size, size );

		glTexCoord2f(0.0f, 0.0f);
		glVertex3f( size, -size, -size );

		glTexCoord2f(0.0f, 1.0f);
		glVertex3f( size, size, -size );

		glTexCoord2f(1.0f, 1.0f);
		glVertex3f( size, size, size );
	glEnd();

	glDepthMask(GL_TRUE);

	glEndList();

	faces = 6 * 2; //6 quads, of 2 polys

	return true;
}

int SkyBox::Render( Camera& camera )
{
	//glLoadIdentity();
	//camera.LookAt();

	glPushMatrix();

	Vector3& pos = camera.GetPosition();
	glTranslatef( pos[X], pos[Y], pos[Z] ); 
	glCallList( model );

	glPopMatrix();

	return faces;
}
