#include "SiphonGL.h"

ColTriMesh::ColTriMesh() : positions(0), indices(0), noPolys(0), noVertices(0), noIndices(0)
{

}

void ColTriMesh::Render( Camera& cam )
{
		glEnableClientState(GL_VERTEX_ARRAY);

		glVertexPointer( 3, GL_FLOAT, sizeof(Vector3), positions );

		glDrawElements( GL_TRIANGLES, noPolys*3, GL_UNSIGNED_INT, indices);
}

bool ColTriMesh::Build()
{
	if( noVertices == 0 || noIndices == 0 || positions == 0 || indices == 0 )
		return false;

	printf("Building collision mesh! novert: %i noin: %i\n", noVertices, noIndices);
	meshData = dGeomTriMeshDataCreate();

	dVector3* newVerts = new dVector3[noVertices];
	int* newIdxs = new int[noIndices];

	for( int i = 0; i < noIndices; i+=3 )
	{
		newIdxs[i + 0] = indices[i + 2];
		newIdxs[i + 1] = indices[i + 1];
		newIdxs[i + 2] = indices[i + 0];
	}

	for( int v = 0; v < noVertices; ++v )
	{
		newVerts[v][0]= positions[v][0];
		newVerts[v][1]= positions[v][1];
		newVerts[v][2]= positions[v][2];
	}

#ifdef WIN32
	dGeomTriMeshDataBuildSimple( meshData, (dReal*)newVerts, noVertices,
			    newIdxs, noIndices );

#else
	dGeomTriMeshDataBuildSimple( meshData, newVerts, noVertices,
			    newIdxs, noIndices );
#endif

	geom = dCreateTriMesh( *PhysicsMgr::GetInstance().GetSpace(), meshData, 0, 0, 0 );

	return true;
}

Mesh::Mesh() : positions(0), colors(0), uvCoords(0), normals(0), indices(0), noPolys(0), bCompiled(false),
		noVertices(0), noIndices(0), uvCoords2(0), bMultitexture(false), faces(0), noFaces(0), shadowPolys(0)
{

}

Mesh::~Mesh()
{

	//uber clean up!!!!
/*	SAFE_DELETE( faces );
	SAFE_DELETE( positions );
	SAFE_DELETE( colors );
	SAFE_DELETE( normals );
	SAFE_DELETE( uvCoords );
	SAFE_DELETE( uvCoords2 );
	SAFE_DELETE_ARRAY( indices );*/
}

bool Mesh::Build( bool bShadow )
{
//	if( gluxIsAvailable("GL_ARB_vertex_buffer_object") == GLUX_AVAILABLE && GLDevice::GetInstance().UseVBO() )
	if( __GLEE_GL_ARB_vertex_buffer_object && GLDevice::GetInstance().UseVBO() )
	{	
		bCompiled = true;

		glGenBuffersARB( 1, &vboPositions );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboPositions );
		glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Vector3) * noVertices, positions, GL_STATIC_DRAW_ARB );

		if( uvCoords != 0 )
		{
			glGenBuffersARB( 1, &vboTexCoords );
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboTexCoords );
			glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(UVCoord) * noVertices, uvCoords, GL_STATIC_DRAW_ARB );
		}

		if( uvCoords2 != 0 )
		{
			glGenBuffersARB( 1, &vboTexCoords2 );
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboTexCoords2 );
			glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(UVCoord) * noVertices, uvCoords2, GL_STATIC_DRAW_ARB );
		}

		if( colors != 0 )
		{
			glGenBuffersARB( 1, &vboColors );
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboColors );
			glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Color) * noVertices, colors, GL_STATIC_DRAW_ARB );
		}

		glGenBuffersARB( 1, &vboIndices );
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, vboIndices );
		glBufferDataARB( GL_ELEMENT_ARRAY_BUFFER_ARB, sizeof(unsigned int) * noIndices, indices, GL_STATIC_DRAW_ARB );

		glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0 );
	}

/*
	if( uvCoords2 != 0 && gluxIsAvailable("GL_ARB_multitexture") == GLUX_AVAILABLE  )
	{
		bMultitexture = true;
	}*/

	this->bShadow = bShadow;
	if( bShadow )
		GenerateConnectivity();

	return true;
}

bool Mesh::BuildShadow()
{
	if( bShadow == true )
		return true;

	bShadow = true;
	GenerateConnectivity();

	return true;
}

bool Mesh::BuildStaticShadow( Vector3& lightPos )
{
	GenerateConnectivity();
	CreateShadow( lightPos, true );

	return true;
}

void Mesh::CreateShadow( Vector3& lightPos, bool bStatic )
{
	if( noFaces <= 0 )
		return;

	//loop through each
	// face and determine if visible
	std::set< int > visFaces;
	std::set< unsigned int > edgeVerts;
	std::set< unsigned int >::iterator edgeIt;
	std::vector< Vector3 > extudedVerts;
	
	shadowVerts.clear();
	shadowIndxs.clear();
	shadowPolys = 0;

	for( int f = 0; f < noFaces; ++f )
	{

		float result = faces[f].plane.SideOfPlane( lightPos );

		if( result >= 0 )
		{
			faces[f].bVisible = true;

			for( int i = 0; i < 3; ++i )
			{
				//if( !bStatic )
				//{
				/*
					ShadowMgr::GetInstance().indices[ ShadowMgr::GetInstance().noIndices ] = ShadowMgr::GetInstance().noVertices;
					ShadowMgr::GetInstance().vertices[ ShadowMgr::GetInstance().noVertices ] = positions[ faces[f].indices[i] ];
					++ShadowMgr::GetInstance().noIndices;
					++ShadowMgr::GetInstance().noVertices;*/

					
					shadowIndxs.push_back( shadowVerts.size() );
					shadowVerts.push_back( positions[ faces[f].indices[i] ] );	
				/*}
				else
				{
					//printf("vertex size: %i\n", ShadowMgr::GetInstance().shadowVerts.size() );
					ShadowMgr::GetInstance().shadowIndxs.push_back( ShadowMgr::GetInstance().shadowVerts.size() );
					ShadowMgr::GetInstance().shadowVerts.push_back( positions[ faces[f].indices[i] ] );
				}*/							
			}

			//if( !bStatic )
			//{
			//	++ShadowMgr::GetInstance().noPolys;
				++shadowPolys;
			/*}
			else
			{
				//if( ShadowMgr::GetInstance().shadowPolys >= 100 )
				//	return;

				++ShadowMgr::GetInstance().shadowPolys;
			}*/			
		}
		else
		{
			faces[f].bVisible = false;
		}
	}

	//for each visible face
	//check its neibours are visible,
	//if not, then we have an edge
	for( f = 0; f < noFaces; ++f )
	{
		if( faces[f].bVisible == true )
		{
			for( int n = 0; n < 3; ++n )
			{
				if( faces[ faces[f].neighbors[n] ].bVisible == false )
				{
					//so we know neighbour n is not visible
					//so get our 2 points extrude and make 2 faces from them
					Vector3 extrude;
					Vector3 p1, p2;
					Vector3 e1, e2;

					p1 = positions[ faces[f].indices[n] ];
					p2 = positions[ faces[f].indices[(n + 1) % 3] ];

					extrude = p1 - lightPos;
					extrude.Normalize();		
					e1 = p1 + (extrude * 500);

					extrude = p2 - lightPos;
					extrude.Normalize();		
					e2 = p2 + (extrude * 500);

					//we have 4 points, create 2 polys from this

					//if( !bStatic )
					//{
					/*
						
						int curVert = ShadowMgr::GetInstance().noVertices;
						
						ShadowMgr::GetInstance().vertices[ ShadowMgr::GetInstance().noVertices ] = p1;
						ShadowMgr::GetInstance().vertices[ ShadowMgr::GetInstance().noVertices + 1 ] = p2;
						ShadowMgr::GetInstance().vertices[ ShadowMgr::GetInstance().noVertices + 2 ] = e1;
						ShadowMgr::GetInstance().vertices[ ShadowMgr::GetInstance().noVertices + 3 ] = e2;
						ShadowMgr::GetInstance().noVertices += 4;

						ShadowMgr::GetInstance().indices[ ShadowMgr::GetInstance().noIndices ] = curVert + 2;
						ShadowMgr::GetInstance().indices[ ShadowMgr::GetInstance().noIndices + 1 ] = curVert + 1;
						ShadowMgr::GetInstance().indices[ ShadowMgr::GetInstance().noIndices + 2] = curVert + 3;

						ShadowMgr::GetInstance().indices[ ShadowMgr::GetInstance().noIndices + 3] = curVert + 1;
						ShadowMgr::GetInstance().indices[ ShadowMgr::GetInstance().noIndices + 4] = curVert + 2;
						ShadowMgr::GetInstance().indices[ ShadowMgr::GetInstance().noIndices + 5] = curVert + 0;
						ShadowMgr::GetInstance().noIndices += 6;

						ShadowMgr::GetInstance().noPolys += 2;
	*/

						int curIdx = shadowVerts.size();
						shadowVerts.push_back( p1 );
						shadowVerts.push_back( p2 );
						shadowVerts.push_back( e1 );
						shadowVerts.push_back( e2 );

						shadowIndxs.push_back( curIdx + 2 );
						shadowIndxs.push_back( curIdx + 1 );
						shadowIndxs.push_back( curIdx + 3 );

						shadowIndxs.push_back( curIdx + 1 );
						shadowIndxs.push_back( curIdx + 2 );
						shadowIndxs.push_back( curIdx + 0 );

						shadowPolys += 2;
					/*}
					else
					{
						int curVert = ShadowMgr::GetInstance().shadowVerts.size();

						ShadowMgr::GetInstance().shadowVerts.push_back( p1 );
						ShadowMgr::GetInstance().shadowVerts.push_back( p2 );
						ShadowMgr::GetInstance().shadowVerts.push_back( e1 );
						ShadowMgr::GetInstance().shadowVerts.push_back( e2 );

						ShadowMgr::GetInstance().shadowIndxs.push_back( curVert + 2 );
						ShadowMgr::GetInstance().shadowIndxs.push_back( curVert + 1 );
						ShadowMgr::GetInstance().shadowIndxs.push_back( curVert + 3 );

						ShadowMgr::GetInstance().shadowIndxs.push_back( curVert + 1 );
						ShadowMgr::GetInstance().shadowIndxs.push_back( curVert + 2 );
						ShadowMgr::GetInstance().shadowIndxs.push_back( curVert + 0 );

						ShadowMgr::GetInstance().shadowPolys += 2;
					}*/
				}
			}
		}
	}
}

void Mesh::GenerateConnectivity()
{
	faces = new Face[ noPolys ];
	noFaces = noPolys;

	if( noPolys * 3 != noIndices )
	{
		printf("we in da shit\n");
		printf("poly:%i ind: %i\n",noPolys, noIndices );
	}

	unsigned int curIdx = 0;
	for( int f = 0; f < noFaces; ++f )
	{
		faces[f].bVisible = false;

		for( int i = 0; i < 3; ++i, ++curIdx )
		{
			faces[f].neighbors[i] = -1;
			faces[f].indices[i] = indices[ curIdx ];
		}

		faces[f].plane.CreateFromFace( positions[ faces[f].indices[0] ],
			positions[ faces[f].indices[1] ], positions[ faces[f].indices[2] ] );
	}

	printf("no polys: %i, no faces: %i\n", noPolys, noFaces );
	for( f = 0; f < noFaces; ++f )
	{
		for( int t = (f + 1); t < noFaces; ++t )
		{		
			//printf("no face: %i, f: %i t: %i\n", noFaces, f, t);
			//sharing an edge means:
			// 2 of the indices are the same
			ShareEdge( faces[f], faces[t], f, t );
			
			
		}
	}
}

inline void Mesh::ShareEdge( Face& face1, Face& face2, int idx1, int idx2 )
{
	for( int i = 0; i < 3; ++i )
	{
		for( int j = 0; j < 3; ++j )
		{
			Vector3 f_v1 = positions[ face1.indices[i] ];
			Vector3 f_v2 = positions[ face1.indices[(i + 1) % 3] ];

			Vector3 t_v1 = positions[ face2.indices[j] ];
			Vector3 t_v2 = positions[ face2.indices[(j + 1) % 3] ];
							
			if( ( f_v1 == t_v1 && f_v2 == t_v2 ) ||
				( f_v1 == t_v2 && f_v2 == t_v1 ) )
			{
				face1.neighbors[i] = idx2;
				face2.neighbors[j] = idx1;
				return;
			}
		}
	}
}

int Mesh::Render( Material* material )
{
	if( material == 0 && defaultMaterial != 0 )
		material = defaultMaterial;

	if( noPolys <= 0 || positions == 0 || indices == 0 )
	{
		printf("empty!");
		return 0;
	}
	
	if( bCompiled )
	{
		glEnableClientState( GL_VERTEX_ARRAY );
		glEnableClientState( GL_INDEX_ARRAY );

		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboPositions );
		glVertexPointer( 3, GL_FLOAT, sizeof(Vector3), 0 );	
		
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, vboIndices );
		glIndexPointer( GL_UNSIGNED_INT, sizeof(unsigned int), 0 );

		if( uvCoords2 != 0 && material->texture2 != 0 )
		{		
			glClientActiveTexture( GL_TEXTURE1_ARB );			
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboTexCoords2 );
			glTexCoordPointer( 2, GL_FLOAT, sizeof(UVCoord), 0 );
			glEnableClientState( GL_TEXTURE_COORD_ARRAY );

			glActiveTextureARB( GL_TEXTURE1_ARB );
			glEnable(GL_TEXTURE_2D);
			material->texture2->SetTexture(1);
		}

		if( uvCoords != 0 && material != 0 )
		{
			glClientActiveTexture( GL_TEXTURE0_ARB );			
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboTexCoords );
			glTexCoordPointer( 2, GL_FLOAT, sizeof(UVCoord), 0 );
			glEnableClientState( GL_TEXTURE_COORD_ARRAY );

			glActiveTextureARB( GL_TEXTURE0_ARB );
			glEnable(GL_TEXTURE_2D);
			material->SetMaterial(0);
		}
	/*
		if( colors != 0 )
		{
			glEnable(GL_COLOR_ARRAY);
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboColors );
			glColorPointer( 4, GL_FLOAT, sizeof(Color), 0 );
		}
		else
		{
			glDisable(GL_COLOR_ARRAY);
		}*/

		glDrawElements( GL_TRIANGLES, noPolys*3, GL_UNSIGNED_INT, 0);

		glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
        glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0 );

		glClientActiveTexture( GL_TEXTURE0_ARB );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
		glDisableClientState( GL_TEXTURE_COORD_ARRAY );
		
		glClientActiveTexture( GL_TEXTURE1_ARB );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
		glDisableClientState( GL_TEXTURE_COORD_ARRAY );
		
		glActiveTextureARB( GL_TEXTURE1_ARB );
		glDisable(GL_TEXTURE_2D);
		glActiveTextureARB( GL_TEXTURE0_ARB );
		glDisable(GL_COLOR_ARRAY);
	}
	else
	{
		glEnableClientState(GL_VERTEX_ARRAY);

		if( uvCoords != 0 )
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		if( normals != 0 )
			glEnableClientState(GL_NORMAL_ARRAY);
/*
		if( colors != 0 )
			glEnableClientState(GL_COLOR_ARRAY);
		else
			glDisableClientState(GL_COLOR_ARRAY);
*/
		glVertexPointer( 3, GL_FLOAT, sizeof(Vector3), positions );

		if( uvCoords != 0 )
		{
			//if( bMultitexture )
				//glClientActiveTexture( GL_TEXTURE0_ARB );

			glTexCoordPointer( 2, GL_FLOAT, sizeof(UVCoord), uvCoords );
		}

		if( normals != 0 )
			glNormalPointer( GL_FLOAT, sizeof(Vector3), normals );
/*
		if( colors != 0 )
			glColorPointer( 4, GL_FLOAT, sizeof(Color), colors );
*/
/*
		if( bMultitexture )
		{
			//set second channel
			//glClientActiveTexture( GL_TEXTURE1_ARB );
			glTexCoordPointer( 2, GL_FLOAT, sizeof(UVCoord), uvCoords2 );
		}
		else if( uvCoords2 != 0 ) //multi-pass rendering
		{
			glDrawElements( GL_TRIANGLES, noPolys*3, GL_UNSIGNED_INT, indices);
			polys += noPolys;

			//set second into first
			glTexCoordPointer( 2, GL_FLOAT, sizeof(UVCoord), uvCoords2 );

			//set some blending
		}*/

		glDrawElements( GL_TRIANGLES, noPolys*3, GL_UNSIGNED_INT, indices);
	}

	//render shadow volume if any
	if( shadowPolys > 0 )
	{
		glDisable(GL_LIGHTING);
		glDepthMask(GL_FALSE);
		glDepthFunc(GL_LEQUAL);

		glEnable(GL_STENCIL_TEST);
		glColorMask(0, 0, 0, 0);
		glStencilFunc(GL_ALWAYS, 1, 0xffffffff);

		// first pass, stencil operation decreases stencil value
		glFrontFace(GL_CCW);
		glStencilOp(GL_KEEP, GL_KEEP, GL_INCR);

		for( int i = 0; i < shadowIndxs.size(); i += 3 )
		{
			glBegin(GL_TRIANGLES);

			for( int j = 0; j < 3; ++j )
			{
				Vector3 vert = shadowVerts[ shadowIndxs[i + j] ];
				glVertex3f( vert[X], vert[Y], vert[Z] );
			}

			glEnd();
		}

		// second pass, stencil operation increases stencil value
		glFrontFace(GL_CW);
		glStencilOp(GL_KEEP, GL_KEEP, GL_DECR);

		for( i = 0; i < shadowIndxs.size(); i += 3 )
		{
			glBegin(GL_TRIANGLES);

			for( int j = 0; j < 3; ++j )
			{
				Vector3 vert = shadowVerts[ shadowIndxs[i + j] ];
				glVertex3f( vert[X], vert[Y], vert[Z] );
			}

			glEnd();
		}
/*
		//glDisable(GL_STENCIL_TEST);
		glColorMask(1, 1, 1, 1);

		glFrontFace(GL_CW);
		glDepthFunc(GL_LEQUAL);
		glDepthMask(GL_TRUE);
		glEnable(GL_LIGHTING);

		//glFrontFace(GL_CCW);
		


		glMatrixMode(GL_PROJECTION); // and initalize Ortho mode
  		glPushMatrix();
  		glLoadIdentity();
  		glOrtho(0.0f, 800,600, 0.0f,-1.0f,1.0f);

  		glMatrixMode(GL_MODELVIEW);
  		glPushMatrix();

		glDisable(GL_DEPTH_TEST);
		glDepthFunc(GL_ALWAYS);
		glDepthMask(GL_FALSE);

		//draw a shadowing rectangle covering the entire screen
		glColor4f(0.0f, 0.0f, 0.0f, 0.4f);

		//glDisable(GL_STENCIL_TEST);
		//glEnable(GL_ALPHA_TEST);
		//glAlphaFunc(GL_GREATER, 0.5);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//glBlendFunc( GL_ZERO, GL_SRC_COLOR );
		glStencilFunc(GL_NOTEQUAL, 0, 0xffffffff);
		glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
		//glPushMatrix();
		glLoadIdentity();
		glBegin(GL_TRIANGLE_STRIP);
		//	glColor3f(0.5f, 0.5f, 0.5f);//, 0.4f );
		//	glColor3f(1.0f,1.0f,1.0f);
/*
		glVertex2f( 0, 100 );
			glVertex2f( 0, 0 );
			glVertex2f( 100, 100 );
			glVertex2f( 100, 0 );* /
		
			glVertex2f( 0, 600 );
			glVertex2f( 0, 0 );
			glVertex2f( 800, 600 );
			glVertex2f( 800, 0 );		
			
			/*
			glVertex3f(-0.2f, 0.2f,-0.10f);
			glVertex3f(-0.2f,-0.2f,-0.10f);
			glVertex3f( 0.2f, 0.2f,-0.10f);
			glVertex3f( 0.2f,-0.2f,-0.10f);* /
		glEnd();
		//glPopMatrix();
		//glDisable(GL_BLEND);

		//glDisable(GL_ALPHA_TEST);

  		glMatrixMode(GL_PROJECTION);
  		glPopMatrix();
  		glMatrixMode(GL_MODELVIEW);
  		glPopMatrix();

	*/
		glColorMask(1, 1, 1, 1);
		glFrontFace(GL_CW);

		glEnable(GL_DEPTH_TEST);
		//glDepthFunc(GL_LEQUAL);
		//glDepthMask(GL_TRUE);

		glDepthFunc(GL_LEQUAL);
		glDepthMask(GL_TRUE);
		//glEnable(GL_LIGHTING);
		glDisable(GL_STENCIL_TEST);
		glShadeModel(GL_SMOOTH);
	}

	return noPolys + (shadowPolys * 2);
}

Model::Model() : mesh(0), material(0)
{

}

void Model::SetPosition( float x, float y, float z )
{
	position[X] = x;
	position[Y] = y;
	position[Z] = z;
}

void Model::Transform()
{
	world.Translate( position[X], position[Y], position[Z] );
	//world.R( position[X], position[Y], position[Z] );
}

int Model::Render( Camera& camera )
{
	if( !mesh )
	{
		printf("NO MODEL!!!");
		return 0;
	}
/*
	if( material != 0 )
	{
		material->SetMaterial();
	}/*
	else if( mesh->defaultMaterial != 0 )
	{
		mesh->defaultMaterial->SetMaterial();
	}*/

	
	glLoadIdentity();
	camera.LookAt();

//	glPushMatrix();

	//Transform();
	//trans.Set();
/*
	//glLoadIdentity();
	printf("\nbefore:\n");
	trans.LoadCurrent();

	Matrix4 pos;
	pos.Translate( position[X], position[Y], position[Z] );

	trans = trans * pos;

	//trans.Print();
	//Transform();
*/
	glTranslatef( position[X], position[Y], position[Z] );
/*
	printf("\nafter:\n");
//	trans.LoadCurrent();
//	trans.Print();
	trans.Set();


	/*
	glRotatef( rotation[PITCH], 1, 0, 0 );
	glRotatef( rotation[YAW], 0, 1, 0 );
	glRotatef( rotation[ROLL], 0, 0, 1);
	*/
	
	int polys = mesh->Render();

//	glPopMatrix();

	return polys;
}

bool Model::Load( char* file )
{
	return true;
}


void Model::SetMesh( Mesh* mesh )
{
	 this->mesh = mesh;
}

void Model::SetMaterial( Material* material )
{
	this->material = material;
}
