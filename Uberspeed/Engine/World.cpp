#include "SiphonGL.h"

bool World::Load( char* file )
{
	bool ret = ImportStatic::Load( file );
/*
	//sort here
	std::list<Mesh>::iterator meshIt;
	std::list<Mesh>::iterator sMeshIt;

	for( meshIt = meshes.begin(); meshIt != meshes.end(); meshIt++ )
	{
		if( meshIt->defaultMaterial->texture->GetBlendType() != BLEND )
		{
			sortedMeshes.push_back( (*meshIt) );
		}
	}

	for( meshIt = meshes.begin(); meshIt != meshes.end(); meshIt++ )
	{
		if( meshIt->defaultMaterial->texture->GetBlendType() == BLEND )
		{
			sortedMeshes.push_back( (*meshIt) );
		}
	}*/

	std::vector<Mesh*>::iterator meshIt;
	for( meshIt = renderSectors[ 0 ].meshes.begin();
						meshIt != renderSectors[ 0 ].meshes.end(); ++meshIt )
	{
		renderQue[ (*meshIt)->defaultMaterial ].push_front( (*meshIt) );
	}

	printf("WORLD LOADED\n");
/*
	//set the camera at any spawn spot...
	int r = rand() % spawnPoints.size();
	RenderMgr::GetInstance().GetCamera().SetPosition( spawnPoints[r].position[0], 
		spawnPoints[r].position[1], spawnPoints[r].position[2] );

	RenderMgr::GetInstance().GetCamera().view = GetSpawnRotation(r);
	RenderMgr::GetInstance().GetCamera().view.Normalize();

	RenderMgr::GetInstance().GetCamera().up = Vector3(0,1,0);

	RenderMgr::GetInstance().GetCamera().right = 
		RenderMgr::GetInstance().GetCamera().up.CrossProduct( 
			RenderMgr::GetInstance().GetCamera().view );
	RenderMgr::GetInstance().GetCamera().right.Normalize();

	printf("rotation: %f %f %f\n", GetSpawnRotation(r)[0], GetSpawnRotation(r)[1], GetSpawnRotation(r)[2] );
*/
	//play sounds
	std::vector< Sound >::iterator soundIt;
	for( soundIt = sounds.begin(); soundIt != sounds.end(); ++soundIt )
	{
		(soundIt)->PlaySound();
	}
	return ret;
}

Matrix4 World::GetSpawn( int r )
{
	return Matrix4( spawnPoints[r].matrix );
}

int World::GetRandomSpawn()
{
	return rand() % spawnPoints.size();
}

int World::Render( Camera& camera )
{
	int polys = 0;
	

	//glLoadIdentity(); // Reset the view
	//camera.LookAt();
	glPushMatrix();
/*
	std::list<Mesh>::iterator meshIt;
	for( meshIt = sortedMeshes.begin(); meshIt != sortedMeshes.end(); meshIt++ )
	{
		meshIt->defaultMaterial->SetMaterial();
		polys += meshIt->Render();
	}*/
/*
	//simply go through each sector and render everything
	std::vector< RenderSector >::iterator secIt;
	std::vector<Mesh*>::iterator mIt;

	for( secIt = renderSectors.begin(); secIt != renderSectors.end(); ++secIt )
	{
		for( mIt = secIt->meshes.begin(); mIt != secIt->meshes.end(); ++mIt )
		{
			(*mIt)->defaultMaterial->SetMaterial();
			polys += (*mIt)->Render();
		}
	}
*/

	/*
	std::list<Mesh*>::iterator renderIt;
	for( renderIt = renderMeshes.begin(); renderIt != renderMeshes.end(); ++renderIt )
	{
		(*renderIt)->defaultMaterial->SetMaterial();
		polys += (*renderIt)->Render();
	}*/

	std::map< Material*,  std::list<Mesh*> >::iterator renderIt;
	std::list<Mesh*>::iterator meshIt;
	for( renderIt = renderQue.begin(); renderIt != renderQue.end(); ++renderIt )
	{
		renderIt->first->SetMaterial();
		for( meshIt = renderIt->second.begin(); meshIt != renderIt->second.end(); ++meshIt )
		{
			polys += (*meshIt)->Render( renderIt->first );
		}
	}

	for( renderIt = renderQueTransparent.begin(); renderIt != renderQueTransparent.end(); ++renderIt )
	{
		renderIt->first->SetMaterial();
		for( meshIt = renderIt->second.begin(); meshIt != renderIt->second.end(); ++meshIt )
		{
			polys += (*meshIt)->Render( renderIt->first );
		}
	}

	//collisionMesh.Render( camera );

	glPopMatrix();
	//

	return polys;
}

dSpaceID* World::AddToSpace( Vector3& position )
{
	int x = (position[X] - collisionSectorInfo.startValue)/collisionSectorInfo.cellGap;
	int z = (position[Z] - collisionSectorInfo.startValue)/collisionSectorInfo.cellGap;

	//dSpaceRemove( collisionSectors[ ((x * (collisionSectorInfo.cols))+z) ].space, geom );
	//dSpaceAdd( collisionSectors[ ((x * (collisionSectorInfo.cols))+z) ].space, geom );

	//printf("Returning cell number: %i\n", ((x * (collisionSectorInfo.cols))+z) );
	return &(collisionSectors[ ((x * (collisionSectorInfo.cols))+z) ].space);
}

void World::Update( unsigned int tick )
{
	if( renderSectorInfo.cols <= 1 )
	{
		return;
	}

	//determine what should be rendered here!!!!

	//clear current terrain render list
	renderMeshes.clear();
	renderQue.clear();
	renderQueTransparent.clear();

	//set all sectors to be not visible
	std::vector< RenderSector >::iterator secIt;
	for( secIt = renderSectors.begin(); secIt != renderSectors.end(); ++secIt )
	{
		secIt->bVisible = false;
	}

	//get cameras transformed frustrum
	BOUNDS camBounds = RenderMgr::GetInstance().GetCamera().GetTransformedBounds();
/*
	//invert z axis
	for(int i=0; i < 8; ++i )
	{
	//	camBounds.points[i][Z] = -camBounds.points[i][Z];
	//	camBounds.points[i][X] = -camBounds.points[i][X];
	}
*/
	//determine which cells are visible
	//and que them in the renderlist
	float posZ = camBounds.points[0][Z], negZ = camBounds.points[0][Z];
	float posX = camBounds.points[0][X], negX = camBounds.points[0][X];

	for(int i=0; i < 8; ++i )
	{
		//printf("point: %f %f\n", camBounds.points[i][X],  camBounds.points[i][Z]);

		if( camBounds.points[i][Z] > posZ )
			posZ = camBounds.points[i][Z];
		if( camBounds.points[i][Z] < negZ )
			negZ = camBounds.points[i][Z];

		if( camBounds.points[i][X] > posX )
			posX = camBounds.points[i][X];
		if( camBounds.points[i][X] < negX )
			negX = camBounds.points[i][X];
	}

	//move camera in to map bounds if outside!
	if( posZ > abs(renderSectorInfo.startValue) )
		posZ = abs(renderSectorInfo.startValue);
	if( posX > abs(renderSectorInfo.startValue) )
		posX = abs(renderSectorInfo.startValue);

	if( negZ < renderSectorInfo.startValue )
		negZ = renderSectorInfo.startValue;
	if( negX < renderSectorInfo.startValue )
		negX = renderSectorInfo.startValue;

//	printf("posZ: %f negZ: %f posX: %f, negX: %f\n", posZ, negZ, posX, negX );

	std::vector<Mesh*>::iterator meshIt;
	std::list<Mesh*>::iterator renderIt;
	std::map<Material*, int>::iterator matIt;

//	printf("\nrendering cells:");
	for( int x=(negX - renderSectorInfo.startValue)/renderSectorInfo.cellGap; x <= (posX - renderSectorInfo.startValue)/renderSectorInfo.cellGap; x++)
	{
		for( int z=(negZ - renderSectorInfo.startValue)/renderSectorInfo.cellGap; z <= (posZ - renderSectorInfo.startValue)/renderSectorInfo.cellGap; z++)
		{
			//Log("we made it here: X: %i, Z:%i", x,z);
			if( x >= 0 && z >= 0 && ((x * (renderSectorInfo.cols))+z) < renderSectors.size() )						
			{
				if( (x <= renderSectorInfo.cols-1 && z <= renderSectorInfo.cols-1) )
				{
				//	printf("%i   ", ((x * (renderSectorInfo.cols))+z) );

					//renderSectors[ ((z * (renderSectorInfo.cols))+x) ].bVisible = true;
					for( meshIt = renderSectors[ ((x * (renderSectorInfo.cols))+z) ].meshes.begin();
						meshIt != renderSectors[ ((x * (renderSectorInfo.cols))+z) ].meshes.end(); ++meshIt )
					{
						if( (*meshIt)->defaultMaterial->GetTexture()->GetBlendType() != BLEND )
							renderQue[ (*meshIt)->defaultMaterial ].push_front( (*meshIt) );
						else
							renderQueTransparent[ (*meshIt)->defaultMaterial ].push_front( (*meshIt) );
						//map[ (*meshIt)->defaultMaterial ] = (*meshIt);
/*
						//std::list<Material*> 
						for( matIt = renderMaterials.begin(); matIt != renderMaterials.end(); ++matIt )
						{
							if( matIt->first == (*meshIt)->defaultMaterial )
							{
								renderMeshes.insert( (renderMeshes.begin() + 2), (*meshIt));
								break;	
							}
						}
						if( renderIt == renderMeshes.end() )
						{
							if( (*meshIt)->defaultMaterial->texture->GetBlendType() == BLEND )
							{
								renderMeshes.push_back( (*meshIt) );
							}
							else
							{
								renderMeshes.push_front( (*meshIt) );
							}
						}*/
/*
						//renderMeshes.push_back( (*meshIt) );
						for( renderIt = renderMeshes.begin(); renderIt != renderMeshes.end(); ++renderIt )
						{
							if( (*renderIt)->defaultMaterial == (*meshIt)->defaultMaterial )
							{
								renderMeshes.insert(renderIt, (*meshIt));
								break;
							}
						}
						if( renderIt == renderMeshes.end() )
						{
							if( (*meshIt)->defaultMaterial->texture->GetBlendType() == BLEND )
							{
								renderMeshes.push_back( (*meshIt) );
							}
							else
							{
								renderMeshes.push_front( (*meshIt) );
							}
						}*/
					}
					//renderCells[ ((x * (renderSectorInfo.cols))+z) ].PreRender();
					//renderMeshes
				}
			}
		}
	}
}

