#ifndef _SIPHON_MODEL_
#define _SIPHON_MODEL_

/**
 * ColTriMesh is a collsion tri mesh structure
 */
class ColTriMesh
{
public:
	ColTriMesh();

	/**
	 * build collision info
	 */
	bool Build();

	void Render( Camera& cam );

	unsigned int noVertices;
	unsigned int noIndices;
	unsigned int noPolys;

	unsigned int* indices;
	Vector3* positions;

	dTriMeshDataID meshData;
	dGeomID geom;
};

class Mesh : public Resource
{
public:
	Mesh();
	~Mesh();

	bool Build( bool bShadow = false );
	bool BuildShadow();
	bool BuildStaticShadow( Vector3& lightPos );


	void CreateShadow( Vector3& lightPos, bool bStatic = false );

	void GenerateConnectivity();

	virtual int Render( Material* material = 0 );

//protected:
	unsigned int noVertices;
	unsigned int noIndices;
	unsigned int noPolys; //no primitives

	Color* colors;
	Vector3* positions;
	Vector3* normals;
	UVCoord* uvCoords;
	UVCoord* uvCoords2;
	unsigned int* indices;

	//the following are VBO's, which quadrupel my frames :)
	GLuint vboPositions;
	GLuint vboTexCoords;
	GLuint vboTexCoords2;
	GLuint vboIndices;
	GLuint vboNormals;
	GLuint vboColors;

	bool bCompiled; //are we using vbos?
	bool bMultitexture; //does it have a second set of uvs & its supported?

	Material* defaultMaterial; //the default material

	struct Face
	{
		bool bVisible;
		Plane plane;
		unsigned int indices[3];
		unsigned int neighbors[3];
	};

	unsigned int noFaces;
	Face* faces; //for shadows!!!!!!!!!!!!!
	bool bShadow; //do we project a shadow?

	std::vector< Vector3 > shadowVerts;
	std::vector< unsigned int > shadowIndxs;
	unsigned int shadowPolys;

	void ShareEdge( Face& face1, Face& face2, int idx1, int idx2 );
};

/**
 * A MODEL IS NOT UPDATEABLE!!!!
 * this 
 */
class Model //: public virtual Obj
{
public:
	Model();

	virtual int Render( Camera& camera );
	bool Load( char* file );
	void SetPosition( float x, float y, float z );
	void SetMesh( Mesh* mesh );
	void SetMaterial( Material* material );

	void Set();
	void Transform();

	Matrix4 world;
//protected:
	Vector3 position;
	Vector3 rotation;
	bool bUseMatrix;

	Mesh* mesh;
	Material* material;
};

#endif

