#ifndef _SIPHON_CLIENT_
#define _SIPHON_CLIENT

#include <list>
#include <map>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_net.h>
#include "Singleton.h"

#include "NetObj.h"
#include "ObjMgr.h"

enum MESSAGE_TYPE
{
	TO_SERVER, //message for server
	TO_OWNER, //message for owner of obj
	TO_ALL, //mesage to all
	TO_CLIENT, //message for the client class

	FROM_SERVER, //mesage from server
	FROM_CLIENT, // message from any joe blow
	FROM_OWNER //message from owner of obj
};

enum SERVER_MESSAGE
{
	CLIENTID, //clientid will be followed by a int(id)
	CREATE, //create message is followed by a class id to create
	DESTROY //this is followed by a net obj id to kill
};

#pragma pack(push,1)
struct PacketHead
{
	unsigned int size; //size of this packet (till next packet head)
	unsigned char to; //who needs to recive the message
	unsigned int netObjId; //which netobj is this meant for?
};
#pragma pop

class Client : public Singleton<Client>
{
public:

	/*
	 * ONCE we connect we must, send a request for all items
	 * in the quedNetworkObjects!!!
	 */
	bool Connect( char* address, int port = 1555 );
	void Disconnect();

	/*
	 * takes the data to send, how big it is,
	 * and the message type (MESAGE_TYPE)
	 * eg. TO_OWNER
	 */
	bool SendReliable( void* data, int length, NetObj* obj = 0, unsigned char to = TO_ALL );

	void RecivePacket();

	bool IsConnected();

	void Update( unsigned int tick );

	void CheckReciveReliable();

	/*
	 * called by register netobj
	 * to create and send the message to server
	 */
	bool RequestCreate( NetObj* obj );

	/*
	 * experimental
	 */
	SDLNet_SocketSet CreateSocketSet();

	/*
	 * Returns the ID this netobject should take on
	 */
	bool RegisterNetObject( NetObj* obj );

//protected:
	friend class Singleton<Client>;

	Client();
	~Client();

	SDLNet_SocketSet reliableSet;

	TCPsocket reliableSocket;
	bool bConnected;
	int clientId; //my unique network id

	std::list< NetObj* > quedNetworkObjects; //these objects are wating for ID's
	std::map< int, NetObj* > networkObjects; //these have been assigned ID's

	bool bBlockNext; //use this to block an object from being made, because we made it!!!
	bool bConnecting; //in the process of being connected
};

#endif
