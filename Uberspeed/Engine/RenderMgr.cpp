#include "SiphonGL.h"

void RenderMgr::Update( unsigned int tick )
{
	camera.Update( tick );
	renderQue.clear();
}

void RenderMgr::Render()
{
	std::list< Model* >::iterator mdlIt;

	for( mdlIt = renderQue.begin(); mdlIt != renderQue.end(); ++mdlIt )
	{
		(*mdlIt)->Render( camera );
	}
}

void RenderMgr::Que( Model* model )
{
	renderQue.push_back( model );
}

