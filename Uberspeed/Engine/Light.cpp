#include "SiphonGL.h"

int Light::noLights = 0;

Light::Light()
{
	lightNo = 0x4000 + noLights;
	noLights++;
}

void Light::SetAmbient( Color value )
{
	glLightfv( GL_LIGHT1, GL_AMBIENT, value.GetPointer() );
}

void Light::SetDiffuse( Color value )
{
	glLightfv( GL_LIGHT1, GL_DIFFUSE, value.GetPointer() );
}

Vector3& Light::GetPosition()
{
	return position;
}

void Light::SetPosition( float x, float y, float z )
{
	float position[4] = { x, y, z, 1.0f};
	this->position = Vector3(x, y, z);

	glLightfv( GL_LIGHT1, GL_POSITION, position );
}

void Light::SetPosition( Vector3 position )
{
	this->position = position;
	float pos[4] = { position[X], position[Y], position[Z], 1.0f};
	glLightfv( GL_LIGHT1, GL_POSITION, pos );
}

void Light::Enable()
{
	glEnable( GL_LIGHT1 );
}

void Light::Disable()
{
	glDisable( GL_LIGHT1 );
}