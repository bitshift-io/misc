#ifndef _SIPHON_NETOBJ_
#define _SIPHON_NETOBJ_

#include "ObjMgr.h"
class Client;


class NetObj : public Obj
{
public:
	virtual bool Send() //virtual 
	{
		printf("NETOBJ: send\n");
		return true;
	}//= 0;

	virtual bool Recive( void* message, int length ) //virtual 
	{
		printf("NETOBJ: recive\n");	
		return true;
	}//= 0;

	/**
	 * Is this class a shared network object
	 * that means only 1 instance is possible
	 * so its like a network singleton
	 */
	virtual bool SharedNetObj() = 0;

	virtual char* GetClassId() = 0;
	/*{
		printf("this will return netobj, calling getclassid\n");
		return "netobj";
	}*/

	void SetNetId( int netId )
	{
		this->netId = netId;
	}

	bool IsNetObject()
	{
		printf("this netobj check has been called\n");
		return true;
	}

	virtual void AnotherFn()
	{
		printf("Another fn called from NetObj\n");
	}
//protected:
	friend class Client;

	int ownerId; // to identify who is in control of this obj
	int netId; //to identify this object on netowkred pc's*/
};

#endif

