#ifndef _SIPHON_GLDEVICE_
#define _SIPHON_GLDEVICE_

/*	
#include <OGL/glext.h>
#include <OGL/glxext.h>
#include <OGL/wglext.h>
*/
#include <SDL/SDL.h>
#include "string.h"


typedef void (APIENTRY * PFNGLACTIVETEXTUREARBPROC) (GLenum target);
extern PFNGLACTIVETEXTUREARBPROC glActiveTextureARB;
/*
GLUX_LOAD(GL_ARB_multitexture);

#ifdef WIN32
GLUX_LOAD(GL_ARB_vertex_buffer_object);
#endif
*/



/*
 * Device and platform stuff
 */
class GLDevice : public Singleton<GLDevice>
{
public:
	void ResizeDevice( int width, int height );
	bool CreateDeviceWindow( INI& systemIni, char* title = "SiphonGL", int bits = 32  );
	bool CreateDeviceWindow( int width = 640, int height = 480, char* title = "SiphonGL", int bits = 32, bool fullscreen = false );
	void DestroyWindow();
	bool CreateDevice();
	void SetDefaultMaterial();
	void EnableFog();
	void ToggleFullScreen();
	int ExtensionSupported( const char* extension );
	void GetDeviceSize( int& width, int& height );

	void BeginRender();
	void EndRender();

	int GetTextureQuality();

	bool UseVBO();

protected:
	friend class Singleton<GLDevice>;
	~GLDevice();
	SDL_Surface* windowHandle;

	float farPlane;

	int width;
	int height;
	int textureQuality; 
	bool useVBO;
};

inline bool GLDevice::UseVBO()
{
	return useVBO;
}

#endif
