#ifndef _SIPHON_WORLD_
#define _SIPHON_WORLD_

/*
 * so what is a world?
 * the level!
 * brings together spatial divising
 * what to be renderes or what not
 * to be rendered.
 * we do batching and level loading/unloading
 */
class World : public ImportStatic, public Singleton<World>
{
public:
	virtual bool Load( char* file );
	virtual int Render( Camera& camera );

	virtual void Update( unsigned int tick );

	virtual Matrix4 GetSpawn( int r );
	virtual int GetRandomSpawn();

	//virtual void AddToSpace( Vector3& position, dGeomID& geom );
	virtual dSpaceID* World::AddToSpace( Vector3& position );
protected:
	std::list<Mesh> sortedMeshes;

	std::map< Material*,  std::list<Mesh*> > renderQue;
	std::map< Material*,  std::list<Mesh*> > renderQueTransparent;

	std::multimap<Material*, Mesh*> map;
	std::map<Material*, int> renderMaterials;
	std::list<Mesh*> renderMeshes;


	//std::vector< sCheckpoint > checkPoints;

/*
	struct CollisionSector
	{
		snMesh collisionMesh;
		//a list of ODE shapes also?
	};*/

};

#endif

