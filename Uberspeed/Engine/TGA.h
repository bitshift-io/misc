#ifndef _SIPHON_TGA_
#define _SIPHON_TGA_

#include <GL/gl.h>
//#include <GL/glu.h>
//#include <SDL/SDL.h>

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

class TGA
{
public:

	TGA();
	bool LoadImage( const char* file, bool bMipMap = true );
	void GetInfo( int& width, int& height, int& bpp, int& blendType );
	void SetTexture();
	GLuint GetTexture();

	#pragma pack(push,1)
	typedef struct TGAHead
        {
		char identsize;
		char colorType;
		char imageType; //compressed if 10, uncom if 2
		short mapstart;
		short length;
		char mapBits;
		short xstart;
		short ystart;
		short width;
		short height;
		char bpp;
		char descritor;
        } TGAHead;
	#pragma pop

protected:

	TGAHead header;
	GLuint texture;
	bool bMipMap;
	int blendType;

	bool LoadUncompresed( FILE* pFile, TGAHead& header );
	bool LoadCompresed( FILE* pFile, TGAHead& header );

	bool GenerateTexture( char* image, int width, int height, GLint type = GL_RGB );
};

#endif
