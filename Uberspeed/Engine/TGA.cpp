#include "SiphonGL.h"

bool TGA::LoadUncompresed( FILE* pFile, TGAHead& header )
{
	if( header.width <= 0 || header.height <= 0 || header.bpp < 24 )
	{
		printf("something wrong bpp: %i width: %i height: %i\n", header.bpp, header.width, header.height);
		return false;
	}

	char* image = 0;
	unsigned int bytesPerPixel = header.bpp / 8;

	image = new char[ header.width * header.height * bytesPerPixel ]; //(char*)malloc( header.width * header.height * bytesPerPixel );
	if( !image )
	{
		printf( "failed to alloc mem of size: %i bytes\n", (header.width * header.height * bytesPerPixel) );
		return false;
	}

	if( header.bpp >= 32 )
		blendType = MASK; //assume its a mask if alpha exists
	else
		blendType = SOLID; //assume its solid

	int curByte = 0;
	int pixelsRead = 0;
	int noPixels = header.width * header.height;
	char* pixel = (char*)malloc( bytesPerPixel );
/*
	if( !fread( image, bytesPerPixel, header.width * header.height, pFile)  )
	{
		printf( "failed to from file (pixel)\n");
        return false;
	}

	char temp;
	for( int b = 0; b < header.width * header.height * bytesPerPixel; b += bytesPerPixel )
	{
		temp = image[ b + 0 ];
		image[ b + 0 ] = image[ b + 2 ];
		image[ b + 2 ] = temp;

		if( bytesPerPixel >= 4 )
		{
			//if this alpha pixel is not 0 or 255, then we are laoding a blend
			//texture
			if( pixel[3] > 0 && pixel[3] < 255 )
					blendType = BLEND;
		}
	}*/


	do
	{
		//Read 1 Pixel
        if( fread( pixel, 1, bytesPerPixel, pFile) != bytesPerPixel )
		{
			printf( "failed to from file (pixel)\n");
                       	return false;
		}

		image[ curByte + 0 ] = pixel[2];
		image[ curByte + 1 ] = pixel[1];
		image[ curByte + 2 ] = pixel[0];

		if( bytesPerPixel >= 4 )
		{
			image[ curByte + 3 ] = pixel[3];

			//if this alpha pixel is not 0 or 255, then we are laoding a blend
			//texture
			if( pixel[3] > 0 && pixel[3] < 255 )
					blendType = BLEND;
		}

		curByte += bytesPerPixel;
		++pixelsRead;

	} while( pixelsRead < noPixels );

	if( header.bpp == 32 )
		GenerateTexture( image, header.width, header.height, GL_RGBA );
	else
		GenerateTexture( image, header.width, header.height, GL_RGB );

	free(image);

	return true;
}

bool TGA::LoadCompresed( FILE* pFile, TGAHead& header )
{
	if( header.width <= 0 || header.height <= 0 || header.bpp < 24 )
	{
		printf("something wrong bpp: %i width: %i height: %i\n", header.bpp, header.width, header.height);
		return false;
	}

	char* image = 0;
	unsigned int bytesPerPixel = header.bpp / 8;

	image = (char*)malloc( header.width * header.height * bytesPerPixel );
	if( !image )
	{
		printf( "failed to alloc mem\n");
		return false;
	}

	if( header.bpp >= 32 )
		blendType = MASK; //assume its a mask if alpha exists
	else
		blendType = SOLID; //assume its solid

	unsigned int curByte = 0;
	unsigned int pixelsRead = 0;
	int noPixels = header.width * header.height;
	char* pixel = (char*)malloc( bytesPerPixel );
	unsigned char chunkHead;

	do
	{
		if( fread( &chunkHead, sizeof(char), 1, pFile ) == 0 )
		{
			printf( "failed to from file (chunkHead)\n");
			return false;
		}

		//SPEED UP LOAD TIME BY READING A CHUNK AT A TIME!!!
		if( chunkHead < 128 ) //RAW - not compressesd
		{
			++chunkHead; //increase by one to determine how many pixels follow
			pixelsRead += chunkHead;
			
/*
			if( !fread( &image[ curByte ], bytesPerPixel, chunkHead, pFile) )
			{
				printf( "failed to from file (pixel RAW)\n");

				if( feof( pFile ) )
					printf("END OF FILE!!\n");

                return false;
			}
/*
			char temp;
			for( int i = 0; i < chunkHead; ++i )
			{
				curByte += bytesPerPixel;

				temp = image[ curByte + 0 ];
				image[ curByte + 0 ] = image[ curByte + 2 ];
				image[ curByte + 2 ] = temp;

				if( bytesPerPixel >= 4 )
				{
					//if this alpha pixel is not 0 or 255, then we are laoding a blend
					//texture
					if( pixel[3] > 0 && pixel[3] < 255 )
							blendType = BLEND;
				}
			}*/

			//curByte += bytesPerPixel * chunkHead;

			for(short counter = 0; counter < chunkHead; ++counter, curByte += bytesPerPixel )
       		{
            	//Read 1 Pixel
            	if( fread( pixel, 1, bytesPerPixel, pFile) != bytesPerPixel )
				{
					printf( "failed to from file (pixel RAW)\n");

					if( feof( pFile ) )
						printf("END OF FILE!!\n");
                    return false;
				}

				image[ curByte + 0 ] = pixel[2];
				image[ curByte + 1 ] = pixel[1];
				image[ curByte + 2 ] = pixel[0];

				//if this alpha pixel is not 0 or 255, then we are laoding a blend
				//texture
				//if( pixel[3] != 0 && pixel[3] != 255 )
				//{
					//blendType = BLEND;
				//	printf("alpha value: %i\n", pixel[3] );
				//}

				if( bytesPerPixel >= 4 )
				{
					image[ curByte + 3 ] = pixel[3];

					if( pixel[3] > 0 && pixel[3] < 255 )
					{
						blendType = BLEND;
					}
				}

				if(pixelsRead > noPixels)											// Make sure we havent read too many pixels
				{
					printf("ERROR! READ TO MANY PIXELS!\n");
				}
			}
		}
		else //RLE - compressed
		{
			chunkHead -= 127;
			pixelsRead += chunkHead;

			//Read 1 Pixel
            if( fread( pixel, 1, bytesPerPixel, pFile) != bytesPerPixel )
			{
				printf( "failed to from file (pixel RLE)\n");

				if( feof( pFile ) )
						printf("END OF FILE!!\n");
                return false;
			}

			for(short counter = 0; counter < chunkHead; ++counter, curByte += bytesPerPixel)
       		{
				image[ curByte + 0 ] = pixel[2];
				image[ curByte + 1 ] = pixel[1];
				image[ curByte + 2 ] = pixel[0];

				if( bytesPerPixel >= 4 )
					image[ curByte + 3 ] = pixel[3];
			}
		}
	} while( pixelsRead < noPixels );

	if( header.bpp == 32 )
		GenerateTexture( image, header.width, header.height, GL_RGBA );
	else
		GenerateTexture( image, header.width, header.height, GL_RGB );

	free(image);
	free(pixel);

	return true;
}

bool TGA::GenerateTexture( char* image, int width, int height, GLint type )
{
	glGenTextures(1, &texture);
	glBindTexture( GL_TEXTURE_2D, texture );

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

	int newWidth = width;
	int newHeight = height;

	if( bMipMap )
	{
		int qual = GLDevice::GetInstance().GetTextureQuality();
		for( int texQual = 0; texQual < qual; ++texQual )
		{
			if( newWidth / 2 < 1 || newHeight / 2 < 1 )
			{
				//no point mip mapping if we get so small
				bMipMap = false;
				break;
			}

			newWidth /= 2;
			newHeight /= 2;
		}
	}
	void* newImage = 0;

	if( GL_RGBA )
		newImage = malloc( width * height * sizeof(char) * 4 ); 
	else 
		newImage = malloc( width * height * sizeof(char) * 3 ); 

	gluScaleImage( type, width, height, GL_UNSIGNED_BYTE, image, newWidth, newHeight, GL_UNSIGNED_BYTE, newImage );

	if( bMipMap )
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
	else
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, 3, newWidth, newHeight, 0, type, GL_UNSIGNED_BYTE, newImage); //GL_COMPRESSED_RGB_ARB swap 3

	if( bMipMap )
		gluBuild2DMipmaps( GL_TEXTURE_2D, type, newWidth, newHeight, type, GL_UNSIGNED_BYTE, newImage );

	free(newImage);

	return true;
}


TGA::TGA()
{

}

bool TGA::LoadImage( const char* file, bool bMipMap )
{
	FILE* pFile = 0;
	pFile = fopen( file, "rb" );

	if( pFile == 0 )
	{
		printf("Failed to open texture: %s\n", file);
		return false;
	}

	this->bMipMap = bMipMap;

	printf("Loading Texture: %s\n", file );

	if( !fread( &header, sizeof( TGAHead ), 1, pFile ) )
		return false;

	if( header.imageType == 10 )
	{
		return LoadCompresed( pFile, header );
	}
	else //if( header.imageType == 2 )
	{
		printf("WARNING: Loading uncompressed texture: %s\n", file );
		return LoadUncompresed( pFile, header );
	}
	/*else
	{
		printf("\tERROR: Unknown texture format: %s\n", file );
		return false;
	}*/

	fclose( pFile );

	return true;
}

void TGA::GetInfo( int& width, int& height, int& bpp, int& type )
{
	width = header.width;
	height = header.height;
	bpp = header.bpp;
	type = blendType;

	//if( header.bpp == 32 )
		//type = ;
}

void TGA::SetTexture()
{
	glBindTexture( GL_TEXTURE_2D, texture );
}

GLuint TGA::GetTexture()
{
	return texture;
}
