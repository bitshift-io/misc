#include "SiphonGL.h"

  dBodyID body;			// the body
  dGeomID geom;

PhysicsMgr::PhysicsMgr()
{
	Initialize();
}

void PhysicsMgr::Initialize()
{
	printf("START CREATING A NEW SIMULATION\n");
	//create an ODE world
	world = dWorldCreate();
	space = dHashSpaceCreate(0);
	contactgroup = dJointGroupCreate(0);
	dWorldSetGravity(world,0,-9.8,0);
	dWorldSetCFM(world,1e-5);
//	dCreatePlane(space,0,1,0,0);
/*	
	body = dBodyCreate(world);
	dBodySetPosition(body, 0, 25, 0);

	dMass m;
	dMassSetSphere( &m, 0.01, 10 );
	geom = dCreateSphere( space,10 );
	dGeomSetBody( geom, body);
*/
	printf("END CREATING A NEW SIMULATION\n");
}

static void nearCollisionCallback(void *data, dGeomID o1, dGeomID o2)
{
	PhysicsMgr::GetInstance().NearCollisionCallback( data, o1, o2);
}
/*
void Bounceable::nearCollisionCallback(void* data, dGeomID o1, dGeomID o2){
  int i=0;
  dBodyID b1=dGeomGetBody(o1);
  dBodyID b2=dGeomGetBody(o2);
  if(b1 && b2 && dAreConnectedExcluding(b1,b2,dJointTypeContact))return;
  dContact contact[MAX_CONTACTS];
  for(i=0;i<MAX_CONTACTS;i++){
    contact[i].surface.mode=dContactBounce | dContactSoftCFM;
    contact[i].surface.mu=dInfinity;
    contact[i].surface.mu2=0;
    contact[i].surface.bounce=1e-5f;
    contact[i].surface.bounce_vel=1e-9f;
    contact[i].surface.soft_cfm=1e-6f;
  }
  int numc=dCollide(o1,o2,MAX_CONTACTS,&contact[0].geom,sizeof(dContact));
  if(numc>0){			
    for(i=0;i<numc;i++){		
      dJointID c=dJointCreateContact(theWorld,theJointGroup,&contact[i]);
      dJointAttach(c,b1,b2);
    }
  }
}*/
#define MAX_CONTACTS 40

void PhysicsMgr::NearCollisionCallback(void *data, dGeomID o1, dGeomID o2)
{
	int i;

	// exit without doing anything if the two bodies are connected by a joint
	dBodyID b1 = dGeomGetBody(o1);
	dBodyID b2 = dGeomGetBody(o2);
	if( b1 && b2 && dAreConnectedExcluding(b1,b2,dJointTypeContact) ) 
		return;

	dContact contact[MAX_CONTACTS];   // up to MAX_CONTACTS contacts per box-box
	for (i=0; i<MAX_CONTACTS; i++) 
	{
		contact[i].surface.mode = dContactSlip1 | dContactSlip2;// | dContactBounce;
		contact[i].surface.mu = dInfinity;
		contact[i].surface.mu2 = 0;
		contact[i].surface.slip1 = 0.02;
		contact[i].surface.slip2 = 0.02;
		//contact[i].surface.bounce = 0.015;
		//contact[i].surface.bounce_vel = 0.01;
	}

	if( int numc = dCollide(o1,o2,MAX_CONTACTS,&contact[0].geom,sizeof(dContact)) )
	{			
		for( i = 0 ; i < numc; ++i )
		{		
			dJointID c = dJointCreateContact( world, contactgroup, &contact[i] );
			dJointAttach( c, b1, b2 );
		}
	}


  /*
  if (int numc = dCollide(o1,o2,MAX_CONTACTS,&contact[0].geom,
			   sizeof(dContact))) 
  {

    for (i=0; i<numc; i++) 
	{		
      dJointID c = dJointCreateContact (world,contactgroup,contact+i);
      dJointAttach (c,b1,b2);
    }
  }

  


	/*
  int i=0;
  dBodyID b1=dGeomGetBody(o1);
  dBodyID b2=dGeomGetBody(o2);
  if(b1 && b2 && dAreConnectedExcluding(b1,b2,dJointTypeContact))return;

  dContact contact[MAX_CONTACTS];

  for(i=0;i<MAX_CONTACTS;i++)
  {

	  		//	contact[i].surface.mode = dContactSlip1 | dContactSlip2 |
		//	dContactSoftERP | dContactSoftCFM | dContactApprox1;
			contact[i].surface.mu = dInfinity;
			contact[i].surface.slip1 = 0.1;
			contact[i].surface.slip2 = 0.1;
			contact[i].surface.soft_erp = 0.5;
			contact[i].surface.soft_cfm = 0.3;

    contact[i].surface.mode=dContactBounce | dContactSoftCFM;
  /*  contact[i].surface.mu=dInfinity;
    contact[i].surface.mu2=0;
    contact[i].surface.bounce=1e-5f;
    contact[i].surface.bounce_vel=1e-9f;
    contact[i].surface.soft_cfm=1e-6f;* /
  }

  int numc=dCollide(o1,o2,MAX_CONTACTS,&contact[0].geom,sizeof(dContact));

  if(numc>0)
  {			
    for(i=0;i<numc;i++)
	{		
      dJointID c = dJointCreateContact(world,contactgroup,&contact[i]);
      dJointAttach(c,b1,b2);
    }
  }*/


/*
	const int N = 10;
	dContact contact[N];

	int n = dCollide( o1, o2 ,N, &contact[0].geom, sizeof(dContact) );

	if( n > 0 ) 
	{
		for( int i=0; i < n; i++ ) 
		{
			contact[i].surface.mode = dContactSlip1 | dContactSlip2 |
			dContactSoftERP | dContactSoftCFM | dContactApprox1;
			contact[i].surface.mu = dInfinity;
			contact[i].surface.slip1 = 0.1;
			contact[i].surface.slip2 = 0.1;
			contact[i].surface.soft_erp = 0.5;
			contact[i].surface.soft_cfm = 0.3;

			dJointID c = dJointCreateContact(world, contactgroup, &contact[i] );
			dJointAttach(c, dGeomGetBody( contact[i].geom.g1), dGeomGetBody(contact[i].geom.g2));
		}
	}*/
}

void PhysicsMgr::Update( unsigned long tick )
{
	dSpaceCollide( space, 0, &nearCollisionCallback );

	std::vector<CollisionSector>::iterator spaceIt;
	for( spaceIt = World::GetInstance().collisionSectors.begin(); 
		spaceIt != World::GetInstance().collisionSectors.end(); ++spaceIt )
	{
		dSpaceCollide( spaceIt->space, 0, &nearCollisionCallback );
	}

	dWorldStep(world, 0.04f);
	//dWorldStepFast1(world, 0.04f, 2);

	dJointGroupEmpty(contactgroup);
}
