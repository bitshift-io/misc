#ifndef _SIPHON_SOUND_
#define _SIPHON_SOUND_

#ifdef WIN32
#include <alc.h>
#include <al.h>
#include <alut.h>
#endif

#ifdef LINUX
#include <al/alc.h>
#include <al/al.h>
#include <al/alut.h>
#endif

/**
 * main sound class
 */
class SoundListener : public Singleton<SoundListener>
{
public:
	void SetListenerPosition(float x, float y, float z);
	void SetListenerOrientation(float fx, float fy, float fz, float ux, float uy, float uz);

	virtual void Update( unsigned long tick );
protected:
	friend class Singleton<SoundListener>;
	SoundListener();
	~SoundListener();

	ALCcontext* context;
	ALCdevice* device;
};

//class Resource;

class Sound// : public Resource
{
public:
	char* file;

	bool LoadSound(char fname[40], bool looping = false );
	void SetRadius( float radius );
	void SetProperties(float x, float y, float z, float vx, float vy, float vz);
	void SetSourceRelative();
	void PlaySound();
	void StopSound();
	void SetLooping( bool state );
	void DestroySound();
private:
	char*			alBuffer;
	ALenum		alFormatBuffer;
	ALsizei		alFreqBuffer;
	long			alBufferLen;
	ALboolean		alLoop;
	unsigned int	alSource;
	unsigned int	alSampleSet;
/*
	FSOUND_SAMPLE* fSound;
	int fChannel;*/
};

#endif
