#ifndef SIPHON_SHADOWMGR
#define SIPHON_SHADOWMGR

class ShadowMgr : public Singleton<ShadowMgr>
{
public:
	ShadowMgr();
	~ShadowMgr();

	int Render( Camera& camera );
	void Update();

	bool BuildStaticShadows();

//protected:
	unsigned int noIndices;
	unsigned int noVertices;
	unsigned int noPolys;

	unsigned int* indices;
	Vector3* vertices; 


	//below is for STATIC shadows generated at load time!!!
	GLuint vboVertices;
	GLuint vboIndices;

	Vector3* staticShadowVerts;
	unsigned int* staticShadowIndxs;

	std::list< Vector3 > shadowVerts;
	std::list< unsigned int > shadowIndxs;

	unsigned int shadowPolys;
	unsigned int noStaticVertices;
	unsigned int noStaticIndices;

	bool bCompiled;
};

#endif
