#ifndef _SIPHON_INPUT_
#define _SIPHON_INPUT_

/*
 * nice wrapper for SDL input
 */
class Input : public Singleton<Input>
{
public:
	/**
	 * Poll key and mouse state only one per update!
	 */
	virtual void Update( unsigned int tick );

	/**
	 * Return the state of the mouse
	 */
	void GetMouseState( int& x, int& y );

	/**
	 * Determines if they key is currently down
	 */
	bool KeyDown(int key);

	/**
	 * determines if the key was pressed
	 */
	bool KeyPressed(int key);

	/**
	 * looks for any pressed keys, and returns it
	 * as a string, but is alphaNumeric is set to true
	 * then it will only return a value if a-z or 0-9 is pressed
	 */
	std::string GetPressedKey( bool alphaNumeric = false ); 

	/**
	 * give this a string like "a" or "shift"
	 * and this function will return the
	 * dx key code eg. DIK_A or DIK_SHIFT
	 */
	int GetKeyCode( std::string str );

	/**
	 * give this something like DIK_A
	 * and it will return "a"
	 */
	std::string GetAsciiCode( int keyCode, bool alphaNumeric = false );

	/**
	 * set this to the window focus state and it will free/lock mouse accordingly
	 */
	void SetFocus( bool bLocked );

protected:
	friend class Singleton<Input>;

	Input();

	//keyboard
	Uint8* keys;
	bool keyLock[256];

	//mouse
	int x, y;
	int last_x, last_y;
	int new_x, new_y;
	bool bLocked; //lock mouse to window
};

#endif
