#ifndef _SIPHON_INI_
#define _SIPHON_INI_

//temporary
void Log(const char *lpszText, ...);

class INI
{
public:
	INI( std::string path );
	~INI();

	std::string GetValue( std::string section, std::string key );

	//overloaded stuffs
	/*
	void GetValue( std::string section, std::string key, std::string& value );
	void GetValue( std::string section, std::string key, float& value  );
	void GetValue( std::string section, std::string key, int& value  );
	void GetValue( std::string section, std::string key, bool& value  );*/

	void SetValue( std::string section, std::string key, std::string value );
	/*
	void SetValue( std::string section, std::string key, float value );
	void SetValue( std::string section, std::string key, int value );
	void SetValue( std::string section, std::string key, bool value );*/

protected:
	std::map<std::string, std::map<std::string, std::string> > data;
	std::string path;
};

#endif
