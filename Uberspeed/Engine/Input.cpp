#include "SiphonGL.h"

Input::Input() : bLocked(true), last_x(0), last_y(0)
{

}

void Input::Update( unsigned int tick )
{
	//get mouse input, we lock the mouse inside the center of our window
	//so we need to modify x and y to acount for this
	SDL_GetMouseState( &x, &y );

	int width, height;
	GLDevice::GetInstance().GetDeviceSize( width, height );

	if( bLocked )
	{
		width /= 2;
		height /= 2;

		SDL_WarpMouse(width, height);
	}

	last_x = x;
	last_y = y;

	if( bLocked )
	{
		x -= width;
		y -= height;
	}
	else
	{
		x -= last_x;
		y -= last_y;
	}

	//get key state
	keys = SDL_GetKeyState(0);
}

void Input::GetMouseState( int& px, int& py )
{
	px = this->x;
	py = this->y;
}

bool Input::KeyDown(int key)
{
	return keys[key];
}

void Input::SetFocus( bool bLocked )
{
	this->bLocked = bLocked;
	SDL_ShowCursor( !bLocked );
}

bool Input::KeyPressed(int key)
{
	if(	KeyDown(key) && keyLock[key] == false )
	{
		keyLock[key] = true;
		return true;
	}
	if( !KeyDown(key) )
	{
		keyLock[key] = false;
	}

	return false;
}

std::string Input::GetPressedKey( bool alphaNumeric )
{
	for(int i=0; i < 256; i++)
	{
		if( KeyPressed(i) )
		{
			return std::string( GetAsciiCode(i, alphaNumeric) );
		}
	}

	return std::string("");
}

std::string Input::GetAsciiCode( int keyCode, bool alphaNumeric )
{
	//alpha numeric keys
	switch( keyCode )
	{
	case SDLK_a:
		return std::string("a");
	case SDLK_b:
		return std::string("b");
	case SDLK_c:
		return std::string("c");
	case SDLK_d:
		return std::string("d");
	case SDLK_e:
		return std::string("e");
	case SDLK_f:
		return std::string("f");
	case SDLK_g:
		return std::string("g");
	case SDLK_h:
		return std::string("h");
	case SDLK_i:
		return std::string("i");
	case SDLK_j:
		return std::string("j");
	case SDLK_k:
		return std::string("k");
	case SDLK_l:
		return std::string("l");
	case SDLK_m:
		return std::string("m");
	case SDLK_n:
		return std::string("n");
	case SDLK_o:
		return std::string("o");
	case SDLK_p:
		return std::string("p");
	case SDLK_q:
		return std::string("q");
	case SDLK_r:
		return std::string("r");
	case SDLK_s:
		return std::string("s");
	case SDLK_t:
		return std::string("t");
	case SDLK_u:
		return std::string("u");
	case SDLK_v:
		return std::string("v");
	case SDLK_w:
		return std::string("w");
	case SDLK_x:
		return std::string("x");
	case SDLK_y:
		return std::string("y");
	case SDLK_z:
		return std::string("z");

	case SDLK_0:
		return std::string("0");
	case SDLK_1:
		return std::string("1");
	case SDLK_2:
		return std::string("2");
	case SDLK_3:
		return std::string("3");
	case SDLK_4:
		return std::string("4");
	case SDLK_5:
		return std::string("5");
	case SDLK_6:
		return std::string("6");
	case SDLK_7:
		return std::string("7");
	case SDLK_8:
		return std::string("8");
	case SDLK_9:
		return std::string("9");

	case SDLK_SPACE:
		return std::string(" ");
	case SDLK_RETURN:
		return std::string("\n");
	}

	if( alphaNumeric )
		return std::string("");

	//special keys
	switch( keyCode )
	{
	case SDLK_SPACE:
		return std::string("space");
	case SDLK_RETURN:
		return std::string("return");
	}
	//TODO: finish adding special keys

	return std::string("");
}

int Input::GetKeyCode( std::string str )
{
	if( str == "a" )
		return SDLK_a;
	else if( str == "b" )
		return SDLK_b;
	else if( str == "c" )
		return SDLK_c;
	else if( str == "d" )
		return SDLK_d;
	else if( str == "e" )
		return SDLK_e;
	else if( str == "f" )
		return SDLK_f;
	else if( str == "g" )
		return SDLK_g;
	else if( str == "h" )
		return SDLK_h;
	else if( str == "i" )
		return SDLK_i;
	else if( str == "j" )
		return SDLK_j;
	else if( str == "k" )
		return SDLK_k;
	else if( str == "l" )
		return SDLK_l;
	else if( str == "m" )
		return SDLK_m;
	else if( str == "n" )
		return SDLK_n;
	else if( str == "o" )
		return SDLK_o;
	else if( str == "p" )
		return SDLK_p;
	else if( str == "q" )
		return SDLK_q;
	else if( str == "r" )
		return SDLK_r;
	else if( str == "s" )
		return SDLK_s;
	else if( str == "t" )
		return SDLK_t;
	else if( str == "u" )
		return SDLK_u;
	else if( str == "v" )
		return SDLK_v;
	else if( str == "w" )
		return SDLK_w;
	else if( str == "x" )
		return SDLK_x;
	else if( str == "y" )
		return SDLK_y;
	else if( str == "z" )
		return SDLK_z;

	if( str == "space" )
		return SDLK_SPACE;
	//TODO: finish adding special keys
}

