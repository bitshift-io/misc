#ifndef _SIPHON_LIGHT_
#define _SIPHON_LIGHT_
/*
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>
*/

#include "Math3D.h"
#include "Material.h"

class Light
{
protected:
	int lightNo;
	static int noLights;
	Matrix4 world;
	Vector3 position;

public:
	Light();
	void SetAmbient( Color value );
	void SetDiffuse( Color value );
	void SetPosition( float x, float y, float z );
	void SetPosition( Vector3 position );
	Vector3& GetPosition();
	void Enable();
	void Disable();
};

#endif

