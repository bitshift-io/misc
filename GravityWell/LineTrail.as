﻿package  
{	
	import flash.display.Sprite;
	import Vector2;
	
	public class LineTrail
	{
		var mSprite : Sprite = new Sprite();
		var mEnabled : Boolean;
		var mHistory : Array;
		var mLengthMax : int = 6;
		var mColour : int = 0xFF794B;
		
		public function LineTrail()
		{
			var game : Game = Game.Get();
			
			game.stage.addChild(mSprite);			
			mEnabled = true;			
			mHistory = new Array();
		}
	
		public function SetEnabled(enabled : Boolean) : void
		{
			var game : Game = Game.Get();
			
			if (mEnabled && !enabled)
				game.stage.removeChild(mSprite);
			else if (!mEnabled && enabled)
				game.stage.addChild(mSprite);
			
			mEnabled = enabled;
		}
		
		public function SetColour(colour : int) : void
		{
			mColour = colour;
		}
		
		public function Update(position : Vector2) : void
		{
			mHistory.unshift(position);
			
			if (mHistory.length > mLengthMax)
				mHistory.pop();
				
			mSprite.graphics.clear();
			mSprite.graphics.beginFill(mColour); //0xFF794B);
			
			for (var i : int = 1; i < mHistory.length; ++i)
			{
				var percent : Number = 1 - (i / mHistory.length);
				mSprite.graphics.lineStyle(2, mColour, percent);
				
				var prevPos = mHistory[i - 1];
				var pos = mHistory[i];
				mSprite.graphics.moveTo(prevPos.x, prevPos.y);
				mSprite.graphics.lineTo(pos.x, pos.y);
			}
			
			mSprite.graphics.endFill();
		}
		
		public function Reset() : void
		{
			mHistory.length = 0;
		}
	}
	
}
