﻿package
{	
	public class Vector2
	{				
		var x		: Number	 	= 0;
		var y 		: Number 		= 0;
		
		public function Vector2(_x : Number = 0, _y : Number = 0)
		{
			x = _x;
			y = _y;
		}
		
		public function Copy(other : Vector2) : void
		{
			x = other.x;
			y = other.y;
		}
		
		public function DistanceTo(other : Vector2) : Number
		{
			return Math.sqrt(DistanceToSqrd(other));
		}
		
		public function DistanceToSqrd(other : Vector2) : Number
		{
			var xDelta : Number = x - other.x;
			var yDelta : Number = y - other.y;
			
			return xDelta * xDelta + yDelta * yDelta;
		}
		
		public function Add(other : Vector2) : Vector2
		{
			return new Vector2(x + other.x, y + other.y);
		}
		
		public function Subtract(other : Vector2) : Vector2
		{
			return new Vector2(x - other.x, y - other.y);
		}
		
		public function Multiply(other : Number) : Vector2
		{
			return new Vector2(x * other, y * other);
		}
		
		public function Magnitude() : Number
		{
			return Math.sqrt(x * x + y * y);
		}
		
		public function Normalize() : Number
		{
			var mag = Magnitude();
			var invMag : Number = 1 / mag;
			x *= invMag;
			y *= invMag;
			return mag;
		}
		
		public function Dot(other : Vector2) : Number
		{
			return x * other.x + y * other.y;
		}
		
		public function AngleBetween(other : Vector2) : Number
		{
			return Math.acos(Dot(other));
		}
	}
}
