#include "StdAfx.h"
#include "AreaMgr.h"

void AreaMgr::LoadDone()
{
	// go through all areas and connections,
	// generate a wall mesh
	// connect areas and connections

/*
	Geometry* geometry = gRender.mFactory->Create<Geometry>();

	int numVert = mSubArea.size() * 4;
	int numIndex = mSubArea.size() * 6;

	geometry->Init(gRender.mDevice, VF_Position | VF_TexCoord0, numVert, numIndex, RBU_Static);

	VertexFrame& frame = geometry->GetVertexFrame();
	RenderBuffer* indexBuffer = geometry->GetIndexBuffer();
	RenderBuffer* posBuffer = frame.mPosition;
	RenderBuffer* texBuffer = frame.mTexCoord[0];

	RenderBufferLock lock;

	indexBuffer->Lock(lock);
	for (int a = 0; a < mSubArea.size(); ++a)
	{
		lock.uintData[a * 6 + 0] = (a * 4) + 0;
		lock.uintData[a * 6 + 1] = (a * 4) + 1;
		lock.uintData[a * 6 + 2] = (a * 4) + 2;

		lock.uintData[a * 6 + 3] = (a * 4) + 2;
		lock.uintData[a * 6 + 4] = (a * 4) + 3;
		lock.uintData[a * 6 + 5] = (a * 4) + 0;
	}
	indexBuffer->Unlock(lock);

	posBuffer->Lock(lock);
	for (int a = 0; a < mSubArea.size(); ++a)
	{
		Vector4& rect = mSubArea[a]->mRect;
		lock.vector4Data[a * 4 + 0] = Vector4(rect.x, 0.f, rect.y);
		lock.vector4Data[a * 4 + 1] = Vector4(rect.x, 0.f, rect.w);
		lock.vector4Data[a * 4 + 2] = Vector4(rect.z, 0.f, rect.w);
		lock.vector4Data[a * 4 + 3] = Vector4(rect.z, 0.f, rect.y);
	}
	posBuffer->Unlock(lock);

	texBuffer->Lock(lock);
	for (int a = 0; a < mSubArea.size(); ++a)
	{
		Vector4& rect = mSubArea[a]->mRect;
		float zSize = Math::Abs(rect.x - rect.z);
		float xSize = Math::Abs(rect.y - rect.w);
		lock.vector4Data[a * 4 + 0] = Vector4(0.f, 0.f);
		lock.vector4Data[a * 4 + 1] = Vector4(xSize, 0.f);
		lock.vector4Data[a * 4 + 2] = Vector4(xSize, zSize);
		lock.vector4Data[a * 4 + 3] = Vector4(0.f, zSize);
	}
	texBuffer->Unlock(lock);

	mWall = gRender.mFactory->CreateScene();
	SceneNode* sceneNode = mWall->GetRootNode();

	Material* material = gRender.mFactory->Load<Material>(&ObjectLoadParams(gRender.mDevice, "Area"));

	Mesh* mesh = gRender.mFactory->Create<Mesh>();
	sceneNode->SetObject(mesh);
	mesh->AddMeshMat(MeshMat(material, mGeometry)); // TODO - set up material here


	gRender.mMain->InsertScene(mWall);*/
}