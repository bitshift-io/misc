#include "StdAfx.h"
#include "World.h"

void CreateEntityCallback(void* instance, const ScriptClass* scriptClass, const ClassDesc* classDesc)
{
	if (classDesc->InheritsFrom(GetClassDesc<Entity>()))
	{
		Entity* entity = static_cast<Entity*>(instance);
		entity->SetName(scriptClass->GetName().c_str());
	}
}

void World::Init()
{
	LineSegment P(Vector4(-1.f, 0.f, 0.f), Vector4(1.f, 0.f, 0.f));
	LineSegment R(Vector4(0.f, 0.f, -1.f), Vector4(0.f, 0.f, 1.f));
	LineSegment result;
	bool distance = R.ShortestSegmentBetweenSegment(R, &result);

	mCamera = new GameCamera();
	mCamera->Init();

	mNavMesh = new NavMesh();

	mTestPath = new NavPath();
	mTestPath->mSource = Vector4(Vector4(1.5f, 0.f, -0.5f));
	mTestPath->mDestination = Vector4(Vector4(1.5f, 0.f, 4.5f));
	mTestPath->mNavMesh = mNavMesh;
	
	mCursor = new Cursor();

	mActor = new Actor*[2];
	mActor[0] = new Actor();
	mActor[1] = new Actor();
	mNumActor = 2;
}

void World::Deinit()
{
}


bool World::Load(const char* file)
{
	string worldFile = string(file) + string(".wld");

	list<void*> entityList;
	ReflectScript script;
	if (!script.Open(worldFile.c_str(), &entityList, &CreateEntityCallback))
		return false;

	list<void*>::iterator entityIt;
	for (entityIt = entityList.begin(); entityIt != entityList.end(); ++entityIt)
	{
		Entity* entity = reinterpret_cast<Entity*>(*entityIt);
		gEntityMgr.InsertEntity(entity);
	}

	gEntityMgr.InitEntity();

	gAreaMgr.LoadDone();

	LoadTest(file);

	return true;
}

void World::Unload()
{
	gEntityMgr.DeinitEntity();

	EntityMgr::EntityIterator it;
	while (it != gEntityMgr.EntityEnd())
	{
		it = gEntityMgr.EntityRemove(it);
		delete (*it); // should each entity clean up its components? or should it be cleaned up by the ReflectScript?
	}
}

bool World::LoadTest(const char* level)
{
	mScene = gRender.LoadScene(level);
	if (!mScene)
		return false;

	//gRender.InsertScene(mScene);

	mNavMesh->Init();
	mCursor->Init();
	mActor[0]->Init();
	mActor[1]->Init();

	Matrix4 transform = mActor[0]->mScene->GetTransform();
	transform.SetTranslation(0.5f, 0.f, 0.5f);
	mActor[0]->mScene->SetTransform(transform);
	mActor[0]->mNavPath->mSource = transform.GetTranslation();

	transform = mActor[1]->mScene->GetTransform();
	transform.SetTranslation(1.5f, 0.f, -0.5f);
	mActor[1]->mScene->SetTransform(transform);
	mActor[1]->mNavPath->mSource = transform.GetTranslation();

	mTestPath->Update();
	return true;
}

void World::Update()
{
	if (gGame.mInput.WasPressed(KEY_Space))
		gGame.mPause = !gGame.mPause;

	mCamera->Update();
	mCursor->Update();
	mActor[0]->Update();
	mActor[1]->Update();
}