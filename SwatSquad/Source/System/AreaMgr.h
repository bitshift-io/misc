#ifndef _AREAMGR_H_
#define _AREAMGR_H_

class Area;
class AreaConnection;

#define gAreaMgr AreaMgr::GetInstance()

class AreaMgr : public Singleton<AreaMgr>
{
public:

	typedef vector<AreaConnection*>::iterator ConnectionIterator;

	void	LoadDone();

	vector<Area*>				mArea;
	vector<AreaConnection*>		mConnection;

	vector<Scene*>				mWall;
};

#endif