#ifndef _WORLD_H_
#define _WORLD_H_

#include "StdAfx.h"

class Cursor;
class GameCamera;
class NavMesh;
class NavPath;
class Actor;

#define gWorld World::GetInstance()

class World : public Singleton<World>
{
public:

	void Init();
	void Deinit();
	bool Load(const char* file);
	void Unload();

	bool LoadTest(const char* file);


	void Update();

	Scene*		mScene;
	GameCamera*	mCamera;
	NavMesh*	mNavMesh;
	NavPath*	mTestPath;
	Cursor*		mCursor;
	Actor**		mActor;
	int			mNumActor;
};

#endif