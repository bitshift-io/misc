#include "StdAfx.h"
#include "Render.h"

#define SOFTWARE 1

bool RenderMgr::Init()
{
	RenderFactoryParameter param;
#ifndef SOFTWARE
	mFactory = new OGLRenderFactory(param);
#else
	mFactory = new SoftwareRenderFactory(param);
#endif
	mDevice = mFactory->CreateDevice();
	mWindow = mFactory->CreateWindow();

	WindowResolution res;
	res.bits = 32;
	res.width = 640;
	res.height = 480;
	mWindow->Init("Swat Squad", &res, false);

	mDevice->Init(mFactory, mWindow);
	mDevice->SetClearColour(Vector4(0.f, 0.f, 0.5f));

	mWindow->Show();

	mCamera = mFactory->Create<Camera>();

	mMain = mFactory->CreateRenderer();
	mMain->SetCamera(mCamera);

	

#ifdef SOFTWARE
	// set up software effects

	// vertex colour + texture
	{
		SoftwareEffect* effect = (SoftwareEffect*)mFactory->Create(Effect::Type);
		effect->SetName("null");
		VertexShaderColourTexture* vertexShader = new VertexShaderColourTexture;
		effect->AddTechnique(new SoftwareEffectTechnique("Render", (SoftwareVertexShader*)vertexShader, new PixelShaderColourTexture, new ShaderColourTextureParameterData));
		effect->Load(&ObjectLoadParams(mFactory, mDevice, "null"));
	}

	// vertex colour + colour multiplier
	{
		SoftwareEffect* effect = (SoftwareEffect*)mFactory->Create(Effect::Type);
		effect->SetName("Colour");
		VertexShaderColour* vertexShader = new VertexShaderColour;
		effect->AddTechnique(new SoftwareEffectTechnique("Render", (SoftwareVertexShader*)vertexShader, new PixelShaderColour, new ShaderColourParameterData));
		effect->Load(&ObjectLoadParams(mFactory, mDevice, "Colour"));
	}

	// shader for rendering flash
	{
		SoftwareEffect* effect = (SoftwareEffect*)mFactory->Create(Effect::Type);
		effect->SetName("Flash");
		VertexShaderFlash* vertexShader = new VertexShaderFlash;
		effect->AddTechnique(new SoftwareEffectTechnique("Colour", (SoftwareVertexShader*)vertexShader, new PixelShaderFlashColour, new ShaderFlashParameterData));
		effect->AddTechnique(new SoftwareEffectTechnique("Texture", (SoftwareVertexShader*)vertexShader, new PixelShaderFlashTexture, new ShaderFlashParameterData));
		effect->Load(&ObjectLoadParams(mFactory, mDevice, "Flash"));
	}
#endif

	mDebugEffect = mFactory->Load<Effect>(&ObjectLoadParams(mDevice, "Debug"));

	return true;
}

void RenderMgr::Deinit()
{
}

bool RenderMgr::Update()
{
	return !mWindow->HandleMessages();
}

void RenderMgr::Render()
{
	mDevice->Begin();
	mMain->Render(mDevice);

	if (mDebugEffect)
	{
		mDebugEffect->Begin();
		mDebugEffect->BeginPass(0);
		mDebugEffect->SetMatrixParameters(mCamera->GetProjection(), mCamera->GetView(), mCamera->GetWorld(), Matrix4(MI_Identity));

		EffectParameter* colour = mDebugEffect->GetParameterByName("colour");


		colour->SetValue(&Vector4(1.f, 0.f, 0.f, 1.f));
		colour->Apply();
		mDebugEffect->SetPassParameter();
		
		//cgSetPassState(0);
		//mDevice->SetMatrix(Matrix4(MI_Identity), MM_Model);
		//gWorld.mActor[0]->Render();

		colour->SetValue(&Vector4(0.f, 1.f, 0.f, 1.f));
		colour->Apply();
		mDebugEffect->SetPassParameter();

		//gWorld.mActor[1]->Render();

		colour->SetValue(&Vector4(0.3f, 0.3f, 0.3f, 1.f));
		colour->Apply();
		mDebugEffect->SetPassParameter();

		//gWorld.mNavMesh->Render(); // debugging

		colour->SetValue(&Vector4(0.f, 0.f, 1.f, 1.f));
		colour->Apply();
		mDebugEffect->SetPassParameter();

		//gWorld.mTestPath->Render();

		gEntityMgr.RenderEntity(RL_Debug);

		mDebugEffect->EndPass();
		mDebugEffect->End();
	}
	mDevice->End();
}

Scene* RenderMgr::LoadScene(const char* name)
{
	return gRender.mFactory->LoadScene(&ObjectLoadParams(gRender.mDevice, name));
}

void RenderMgr::InsertScene(Scene* scene)
{
	mMain->InsertScene(scene);
}