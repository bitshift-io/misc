#ifndef _GAME_H__
#define _GAME_H__

#include "StdAfx.h"

#define gGame Game::GetInstance()

class Game : public Singleton<Game>
{
public:

	Game() :
		mPause(false)
	{
		int nothing = 0;
	}

	bool					Init(const char* cmdLine);
	void					Deinit();
	void					Render();
	bool					Run();
	bool					Update();

	static int				Main(const char* cmdLine);

	//CmdLine					mCmdLine;
	FrameUpdate				mUpdate;
	Input					mInput;
	bool					mPause;
};

#endif
