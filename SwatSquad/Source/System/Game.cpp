#include "StdAfx.h"
#include "Game.h"

#include "Render/OGL/Cg/cg.h"
#include "Render/OGL/Cg/cgGL.h"

bool Game::Init(const char* cmdLine)
{
	//mCmdLine = CmdLine(cmdLine);
	mUpdate.SetUpdateRate(30);

	gFilesystem.SetDLLDirectory(gFilesystem.GetBaseDirectory() + "/Binary");
	string dataPath = gFilesystem.GetBaseDirectory() + "/Data";
	gFilesystem.SetBaseDirectory(dataPath);
	gFilesystem.RegisterDirectoryContents(dataPath);

	gRender.Init();
	gWorld.Init();
	gWorld.Load("TestLevel1");

	mInput.Init(gRender.mWindow, true);
	return true;
}

void Game::Deinit()
{
	gWorld.Deinit();
	gRender.Deinit();	
}

void Game::Render()
{
	gRender.Render();
}

bool Game::Update()
{
	mInput.Update();
	bool bResult = gRender.Update();
	gWorld.Update();
	return bResult;
}

bool Game::Run()
{
	int updateCount = 0;

	bool playing = true;
	while (playing)
	{
		mUpdate.Update();

		// debug mode we dont care about frame rate independance!
		// might needs some tweaks to do at least 1 update and no more than 5 updates per loop,
		// ie push some of the updates forwards or backwards so game runs slightly faster/slower for a while
#ifdef _DEBUG

		if (!(playing = Update()))
			return true;
#else
		for (int i = 0; i < mUpdate.GetNumUpdates(); i++)
		{
			++updateCount;
			if (!(playing = Update()))
                return true;
		}
#endif

		Render();
	}

	return true;
}

int Game::Main(const char* cmdLine)
{
	if (gGame.Init(cmdLine))
	{
		gGame.Run();
		gGame.Deinit();
	}

	return 0;
}



