#ifndef _RENDER_H_
#define _RENDER_H_

#include "StdAfx.h"

#define gRender RenderMgr::GetInstance()

class RenderMgr : public Singleton<RenderMgr>
{
public:

	enum // RenderLayer
	{
		RL_Debug,
	};

	bool Init();
	void Deinit();
	void Render();
	bool Update();

	Scene* LoadScene(const char* name);
	void InsertScene(Scene* scene);

	RenderFactory*	mFactory;
	Window*			mWindow;
	Device*			mDevice;

	Camera*			mCamera;
	Renderer*		mMain;

	Effect*			mDebugEffect;
};

#endif