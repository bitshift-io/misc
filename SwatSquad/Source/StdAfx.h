#ifndef ___STDAFX_H_
#define ___STDAFX_H_

//
// Library Includes
//

#include "Template/FrameUpdate.h"
#include "Template/Singleton.h"
#include "Template/Time.h"
#include "Template/String.h"

#include "Math/Vector.h"
#include "Math/Line.h"
#include "Math/Ray.h"
#include "Math/LineSegment.h"
#include "Math/Triangle.h"

#include "Render/RenderFactory.h"
#include "Render/Device.h"
#include "Render/Window.h"
#include "Render/Scene.h"
#include "Render/Camera.h"
#include "Render/Renderer.h"
#include "Render/Mesh.h"
#include "Render/Material.h"
#include "Render/Geometry.h"
#include "Render/RenderBuffer.h"

#include "Render/OGL/OGLRenderFactory.h"

#include "Render/Software/SoftwareRenderFactory.h"
#include "Render/Software/SoftwareEffect.h"

#include "Input/Input.h"

#include "Reflect/ReflectSystem.h"
#include "Reflect/Class.h"
#include "Reflect/Enum.h"
#include "Reflect/ReflectScript.h"
#include "Reflect/STLType.h"

//
// Game Includes
//

#include "Reflect/Math.h"

#include "Template/Observer.h"

#include "System/Game.h"
#include "System/Render.h"
#include "System/World.h"
#include "System/EntityMgr.h"
#include "System/AreaMgr.h"

#include "Entity/Entity.h"
#include "Entity/Camera.h"
#include "Entity/NavMesh.h"
#include "Entity/NavPath.h"
#include "Entity/Cursor.h"
#include "Entity/Actor.h"
#include "Entity/Area.h"

#include "SoftwareEffect/ShaderColour.h"
#include "SoftwareEffect/ShaderColourTexture.h"
#include "SoftwareEffect/ShaderFlash.h"
#include "SoftwareEffect/ShaderVertexColour.h"

#endif
