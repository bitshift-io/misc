#ifndef _SHADERCOLOUR_H_
#define _SHADERCOLOUR_H_

#include "Render/Software/SoftwareEffect.h"

class ShaderColourParameterData : public ParameterData
{
public:

	ShaderColourParameterData()
	{
		layout[0].type = EPT_Matrix4;
		layout[0].ID = EPID_WorldViewProjection;
		layout[0].offset = VAR_OFFSET(worldViewProj);
		layout[0].size = sizeof(Matrix4);

		layout[1].type = EPT_Vector4;
		layout[1].ID = EPID_Unknown;
		layout[1].offset = VAR_OFFSET(colour);
		layout[1].size = sizeof(Vector4);
		layout[1].name = "colour";

		layout[2].type = EPT_Unknown;
		layout[2].ID = EPID_Max;
		layout[2].offset = -1;
		layout[2].size = -1;
	}

	ParameterData* Clone() 
	{ 
		return new ShaderColourParameterData; 
	}

	ParameterDataLayout* GetParameterDataLayout()
	{
		return layout;
	}

	Matrix4 worldViewProj;
	Vector4 colour;

	ParameterDataLayout layout[3];
};

struct ShaderColourVertexInput : public VertexInput
{
	Vector4 position;
	Vector4 colour;
	Vector4 uv;
};

struct ShaderColourPixelInput : public PixelInput
{
	Vector4 colour;
	Vector4 uv;
};

class VertexShaderColour : public SoftwareVertexShaderTemplate<ShaderColourVertexInput>
{
public:

	VertexShaderColour()
	{
		layout[0].dataType = VF_Position;
		layout[0].offset = 0;
		layout[0].size = sizeof(Vector4);

		layout[1].dataType = VF_Colour;
		layout[1].offset = sizeof(Vector4);
		layout[1].size = sizeof(Vector4);

		layout[2].dataType = VF_TexCoord0;
		layout[2].offset = sizeof(Vector4) + sizeof(Vector4);
		layout[2].size = sizeof(Vector4);

		layout[3].dataType = VF_Unknown;
		layout[3].offset = -1;
		layout[3].size = -1;
	}

	virtual void Apply(const ParameterData* parameter, const VertexInput* input, PixelInput* output)
	{
		ShaderColourVertexInput* in = (ShaderColourVertexInput*)input;
		ShaderColourPixelInput* out = (ShaderColourPixelInput*)output;
		ShaderColourParameterData* param = (ShaderColourParameterData*)parameter;

		in->position.w = 1.0f;
		param->worldViewProj.Transform(in->position, &out->position);
		out->colour = in->colour;
		out->uv = in->uv;
	}

	virtual VertexInputLayout* GetVertexInputLayout() 
	{
		return layout;
	}

	VertexInputLayout layout[4];
};

class PixelShaderColour : public SoftwarePixelShaderTemplate<PixelShaderColour, ShaderColourPixelInput>
{
public:

	inline static void Apply(const ParameterData* parameter, const PixelInput* input, PixelOutput* output)
	{
		register ShaderColourPixelInput* in = (ShaderColourPixelInput*)input;
		register ShaderColourParameterData* param = (ShaderColourParameterData*)parameter;
		output->colour = in->colour * param->colour; //* texture look up
	}
};

#endif