#ifndef _SHADERVERTEXCOLOUR_H_
#define _SHADERVERTEXCOLOUR_H_

#include "Render/Software/SoftwareEffect.h"

class ShaderVertexColourParameterData : public ParameterData
{
public:

	ShaderVertexColourParameterData()
	{
		layout[0].type = EPT_Matrix4;
		layout[0].ID = EPID_WorldViewProjection;
		layout[0].offset = VAR_OFFSET(worldViewProj);
		layout[0].size = sizeof(Matrix4);

		layout[1].type = EPT_Unknown;
		layout[1].ID = EPID_Max;
		layout[1].offset = -1;
		layout[1].size = -1;
	}

	ParameterData* Clone() 
	{ 
		return new ShaderVertexColourParameterData; 
	}

	ParameterDataLayout* GetParameterDataLayout()
	{
		return layout;
	}

	Matrix4 worldViewProj;

	ParameterDataLayout layout[2];
};

struct ShaderVertexColourVertexInput : public VertexInput
{
	Vector4 position;
	Vector4 colour;
	Vector4 uv;
};

struct ShaderVertexColourPixelInput : public PixelInput
{
	Vector4 colour;
	Vector4 uv;
};

class VertexShaderVertexColour : public SoftwareVertexShaderTemplate<ShaderVertexColourVertexInput>
{
public:

	VertexShaderVertexColour()
	{
		layout[0].dataType = VF_Position;
		layout[0].offset = 0;
		layout[0].size = sizeof(Vector4);

		layout[1].dataType = VF_Colour;
		layout[1].offset = sizeof(Vector4);
		layout[1].size = sizeof(Vector4);

		layout[2].dataType = VF_TexCoord0;
		layout[2].offset = sizeof(Vector4) + sizeof(Vector4);
		layout[2].size = sizeof(Vector4);

		layout[3].dataType = VF_Unknown;
		layout[3].offset = -1;
		layout[3].size = -1;
	}

	virtual void Apply(const ParameterData* parameter, const VertexInput* input, PixelInput* output)
	{
		ShaderVertexColourVertexInput* in = (ShaderVertexColourVertexInput*)input;
		ShaderVertexColourPixelInput* out = (ShaderVertexColourPixelInput*)output;
		ShaderVertexColourParameterData* param = (ShaderVertexColourParameterData*)parameter;

		in->position.w = 1.0f;
		param->worldViewProj.Transform(in->position, &out->position);
		out->colour = in->colour;
		out->uv = in->uv;
	}

	virtual VertexInputLayout* GetVertexInputLayout() 
	{
		return layout;
	}

	VertexInputLayout layout[4];
};

class PixelShaderVertexColour : public SoftwarePixelShaderTemplate<PixelShaderColour, ShaderVertexColourPixelInput>
{
public:

	inline static void Apply(const ParameterData* parameter, const PixelInput* input, PixelOutput* output)
	{
		register ShaderVertexColourPixelInput* in = (ShaderVertexColourPixelInput*)input;
		register ShaderVertexColourParameterData* param = (ShaderVertexColourParameterData*)parameter;
		output->colour = in->colour;
	}
};

#endif