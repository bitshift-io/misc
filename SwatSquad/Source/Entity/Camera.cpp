#include "StdAfx.h"
#include "Camera.h"

void GameCamera::Init()
{
	mTarget = Vector4(0.f, 0.f, 0.f);
	mDirection = Vector4(0.f, 5.f, -1.f); //Vector4(0.f, 5.f, -3.f);
	mZoom = mDirection.Normalize3();
}

void GameCamera::Update()
{
	Matrix4 camera = gRender.mCamera->GetWorld();
	Vector4 up(VI_Up);
	Vector4 forward = up.Cross(camera.GetXAxis());
	forward.Normalize3();
	Vector4 right = forward.Cross(up);
	right.Normalize3();

	float moveSpeed = 1.f * gGame.mUpdate.GetDeltaTime();

	if (gGame.mInput.GetState(KEY_Up) || gGame.mInput.GetState(KEY_w))
		mTarget += forward * moveSpeed;

	if (gGame.mInput.GetState(KEY_Down) || gGame.mInput.GetState(KEY_s))
		mTarget -= forward * moveSpeed;

	if (gGame.mInput.GetState(KEY_Left) || gGame.mInput.GetState(KEY_a))
		mTarget -= right * moveSpeed;

	if (gGame.mInput.GetState(KEY_Right) || gGame.mInput.GetState(KEY_d))
		mTarget += right * moveSpeed;

	float zoom = gGame.mInput.GetState(MOUSE_Wheel);
	mZoom -= zoom;

	// middle mouse, change what x any y do
	if (gGame.mInput.GetState(MOUSE_Middle))
	{
		float yaw = gGame.mInput.GetState(MOUSE_AxisX);
		float pitch = gGame.mInput.GetState(MOUSE_AxisY);

		Matrix4 mYaw(MI_Identity);
		mYaw.RotateY(yaw);

		Matrix4 mPitch(MI_Identity);
		mPitch.RotateX(pitch);

		Matrix4 mAlign(MI_Identity);
		mAlign.SetXAxis(right);
		mAlign.SetYAxis(up);
		mAlign.SetZAxis(forward);

		Matrix4 mPitchResult = mAlign * mPitch;

		mPitchResult.TransformRotation3(mDirection, &mDirection);
		mYaw.TransformRotation3(mDirection, &mDirection);
	}


	gRender.mCamera->LookAt(mTarget + mDirection * mZoom, mTarget);
}