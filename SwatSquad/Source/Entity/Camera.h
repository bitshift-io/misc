#ifndef _GAMECAMERA_H_
#define _GAMECAMERA_H_

class GameCamera
{
public:

	void Init();
	void Update();

	Vector4 mDirection;
	float	mZoom;
	Vector4 mTarget;
};

#endif