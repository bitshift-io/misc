#ifndef _NAVMESH_H_
#define _NAVMESH_H_

class NavNode
{
public:

	NavNode() :
	  mFaceIdx(-1)
	{
		memset(mAdjacent, 0, sizeof(NavNode*) * 3);
	}

	int			mFaceIdx;
	NavNode*	mAdjacent[3];
	Vector4		mAveragePos;
};

//
// Generates nav mesh
//
class NavMesh
{
public:

	void Init();
	void Process(Geometry* geometry);
	void Render();

	NavNode*	GetClosest(Vector4& pos);

	NavNode*		mNavNode;
	int				mNumNavNode;

	Vector4*		mVertices;
	int				mNumVertices;

	unsigned int*	mIndices;
	int				mNumIndices;
};

#endif