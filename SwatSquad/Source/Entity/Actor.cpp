#include "StdAfx.h"
#include "Actor.h"

Actor::Actor() :
	mScene(0),
	mSelected(false),
	mState(AS_Idle),
	mTargetNavNodeIdx(0),
	mMovementSpeed(0.3f)
{
}

void Actor::Init()
{
	mSnapDistance = Vector4(1.f, 1.f);

	mScene = gRender.LoadScene("Actor");
	// should we name renderes so we can decide what model goes in what renderer?
	gRender.InsertScene(mScene);

	Vector4 pos(0.5f, 0.f, 0.5f);
	Matrix4 transform(MI_Identity);
	transform.SetTranslation(pos);
	mScene->SetTransform(transform);

	mNavPath = new NavPath();
	mNavPath->mNavMesh = gWorld.mNavMesh;
	mNavPath->mSource = pos;
}

void Actor::Deinit()
{

}

void Actor::Update()
{
	if (gGame.mPause)
		return;

	if (mState == AS_Move)
	{
		if (mTargetNavNodeIdx >= mNavPath->mNumPoints) //mNavPath->mPath.size())
		{
			mState = AS_Idle;

			// snap to grid
			Matrix4 transform = mScene->GetTransform();

			// apply snapping
			// how many times does the position go in to the snap distance (rounded off)
			// then use this to generate the grid location
			Vector4 pos = transform.GetTranslation();
			Vector4 intervals = Vector4(pos.x, pos.z) / mSnapDistance;
			intervals.x = floor(intervals.x);
			intervals.y = floor(intervals.y);

			pos.x = intervals.x * mSnapDistance.x;
			pos.z = intervals.y * mSnapDistance.y;

			// assume center of cursor aligns with center of tile
			pos.x += mSnapDistance.x * 0.5f;
			pos.z += mSnapDistance.y * 0.5f;

			transform.SetTranslation(pos);
			mScene->SetTransform(transform);
			mNavPath->mSource = transform.GetTranslation();
			return;
		}

		//NavNode* node = mNavPath->mPath[mTargetNavNodeIdx];
		Vector4 pos = mScene->GetTransform().GetTranslation();

		Vector4 nodePos = mNavPath->mPoints[mTargetNavNodeIdx];
		Vector4 dir = nodePos - pos;
		//Vector4 dir = node->mAveragePos - pos;
		float dist = dir.Normalize3();

		Matrix4 transform(MI_Identity);
		transform.SetZAxis(dir);
		transform.SetXAxis(dir.Cross(transform.GetYAxis()));
		transform.SetTranslation(pos + dir * gGame.mUpdate.GetDeltaTime() * mMovementSpeed);
		mScene->SetTransform(transform);
		mNavPath->mSource = transform.GetTranslation();

		// sleect next node
		if (dist <= 0.1f)
		{
			++mTargetNavNodeIdx;
		}
	}
}

void Actor::Render()
{
	mNavPath->Render();
}

void Actor::SetDestination(const Vector4& destination)
{
	mNavPath->mDestination = destination;
	mNavPath->Update();
	mTargetNavNodeIdx = 1;
}

