#ifndef _AREA_H_
#define _AREA_H_

#include "Reflect/Math.h"

// move this rectangle to reflect folder
class Rect
{
public:

	//REFLECT_CLASS(Rectangle)

	REFLECT_CLASS_V
	(
		Rect, 
		(
			VAR(mRect)
		)
	)

	Vector4	GetPoint(int index)
	{
		switch (index)
		{
		case 0:
			return Vector4(mRect.x, 0.f, mRect.y);

		case 1:
			return Vector4(mRect.x, 0.f, mRect.w);

		case 2:
			return Vector4(mRect.z, 0.f, mRect.w);

		case 3:
			return Vector4(mRect.z, 0.f, mRect.y);
		}
	}

	Vector4 operator[](int index)
	{
		return GetPoint(index);
	}


	Vector4	mRect;
};

class Area;

// represents connections between areas where the actors can pass through
class AreaConnection : public Entity
{
public:

	REFLECT_CLASS_V
	(
		AreaConnection, 
		(
			VAR_PTR(mConnection)
		)
	)

	virtual void		Init(Entity* parent);
	virtual void		Deinit();

	virtual void		Render(int layer);

	Rect*			mConnection;
	vector<Area*>	mArea;
};

// represents a space/room/area
class Area : public Entity
{
public:

	REFLECT_CLASS_V
	(
		Area, 
		(
			VAR_PTR_STL(mSubArea)
		)
	)

	virtual void		Init(Entity* parent);
	virtual void		Deinit();
	virtual void		Resolve();

	Scene*					mScene;

	vector<Rect*>			mSubArea;
	vector<AreaConnection*> mConnection;
};

#endif