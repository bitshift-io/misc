#include "StdAfx.h"
#include "NavPath.h"

struct PathStep
{
    PathStep() :  current(0), next(0)
    {
    }

    PathStep(NavNode* _current, NavNode* _next) : current(_current), next(_next)
    {
    }

    NavNode* current;
    NavNode* next;
};

NavPath::NavPath() :
	mPoints(0)
{
	closestEdgePoints = 0;
}

void NavPath::Update()
{
	NavNode* destNode = mNavMesh->GetClosest(mDestination);
	NavNode* sourceNode = mNavMesh->GetClosest(mSource);

	if (!destNode || !sourceNode)
		return;

	list<PathStep>::iterator it;
	list<PathStep>	mVisited;
	list<PathStep>	mUnvisited;

	// do A* path finding
	mUnvisited.push_front(PathStep(destNode, 0));

	while (mUnvisited.size())
	{
		PathStep step = mUnvisited.front();
		mUnvisited.pop_front();

		bool bVisited = false;
		for (it = mVisited.begin(); it != mVisited.end(); ++it)
		{
			if (it->current == step.current)
			{
				bVisited = true;
				break;
			}
		}

		if (bVisited)
			continue;

		mVisited.push_back(step);

		// if we found the target waypoint
		if (step.current == sourceNode)
		{
			mPath.clear();
			mPath.push_back(step.current);

			// back track the valid path till we find which node we need to go to next to get to the enemy
			while (step.next)
			{
				for (it = mVisited.begin(); it != mVisited.end(); ++it)
				{
					if (step.next == it->current)
					{
						step = *it;
						break;
					}
				}
				mPath.push_back(step.current);
			}

			GenerateShortestPath();
			return;
		}

		// no path found, push children on
		for (int i = 0; i < 3; ++i)
		{
			NavNode* next = step.current->mAdjacent[i];
			if (next && step.next != next)
				mUnvisited.push_back(PathStep(next, step.current));
		}
	}
}

int InsertVector4(Vector4* vector, const Vector4& insert, int position, int curSize)
{
	if (position >= curSize)
	{
		vector[position] = insert;
		return curSize + 1;
	}

	// shift all vectors forward
	for (int i = curSize; i > position; --i)
	{
		vector[i] = vector[i - 1];
	}

	vector[position] = insert;
	return  curSize + 1;
}

void NavPath::CalculateVisibility(NavNodeVert* vert)
{
	NavNode* pNode = vert->mNavNode;

	int nodeIdx = 0;
	vector<NavNode*>::iterator it;
	for (it = mPath.begin(); it != mPath.end(); ++it, ++nodeIdx)
	{
		if (*it == pNode)
		{
			break;
		}
	}

	// set up visibility to other verts part of this node
	for (int f = 0; f < 3; ++f)
	{
		NavNodeVert* neighbourVert = mNavNodeVert[mNavMesh->mIndices[pNode->mFaceIdx + f]];
		if (vert != neighbourVert)
			vert->mVisible.push_back(neighbourVert);
	}

	++nodeIdx;

	while (nodeIdx < mPath.size())
	{
		pNode = mPath[nodeIdx];

		for (int f = 0; f < 3; ++f)
		{
			NavNodeVert* testVert = mNavNodeVert[mNavMesh->mIndices[pNode->mFaceIdx + f]];
			if (testVert == vert)
				continue;

			LineSegment best(vert->mPos, testVert->mPos);
	
			bool bIntersectOutside = false;
			for (int i = 0; i < outsideCount; ++i)
			{
				LineSegment& outsideSegment = outside[i];

				float time0, time1;
				LineSegment result;
				if (outsideSegment.ShortestSegmentBetweenSegment(best, &result, &time0, &time1))
				{
					if ((time0 > 0.f && time0 < 1.f)
						&& (time1 > 0.f && time1 < 1.f))
					{
						bIntersectOutside = true;
						break;
					}

					continue;
				}
			}

			if (!bIntersectOutside)
			{
				// testVert is visible
				vert->mVisible.push_back(testVert);
			}
		}

		++nodeIdx;
		//pNode = mNavNodeVert[mNavMesh->mIndices[pNode->mFaceIdx]]->mNext;
	}

	int onthing = 0;

/*
	NavNode* pTestNode = vert->mNext;
	NavNode* pOccludeNode = vert->mNext;

	//while (pOccludeNode != pTestNode)
	{
		for (int f = 0; f < 3; ++f)
		{
			NavNodeVert* testVert = mNavNodeVert[pNextNode->mFaceIdx + f];
			LineSegment a(vert->mPos, testVert->mPos);

			for ()
			{
				Triangle tri(mNavMesh->mVertices[mNavMesh->mIndices[node.mFaceIdx]],
					mNavMesh->mVertices[mNavMesh->mIndices[node.mFaceIdx+1]],
					mNavMesh->mVertices[mNavMesh->mIndices[node.mFaceIdx+2]]);

				// check to see if line segment intersects triangle
				// if it does, skip adding a point
				Vector4 closestPoint;
				float closestDist = FLT_MAX;
				bool bIntersection = false;
				for (int x = 0; x < 3; ++x)
				{
					LineSegment edge = tri.GetEdgeAsSegment(x);

					LineSegment result;
					if (edge.ShortestSegmentBetweenSegment(best, &result))
					{
						float dist = result.Length();
						if (dist <= 0.1f)
						{
							bIntersection = true;
							break;
						}
						
						// calculate distance to end point, as a vert may be closer to the line
						// than another vert but is past the end point for the ideal shortest length
						// meaning this 'closer' vert will result in a longer path
						float distToEndPoint2 = (best.p[1] - result.p[1]).Mag3();
						float distToEndPoint1 = (best.p[0] - result.p[0]).Mag3();
						if (dist < closestDist && Math::Min(distToEndPoint1, distToEndPoint2) > Math::Epsilon)
						{
							closestDist = dist;
							closestPoint = result.p[0]; // closest point on 'edge' to 'best'
						}
					}
				}

				if (bIntersection)
					continue;
					}

		}
	}*/
}

void NavPath::GenerateShortestPath()
{
	for (int i = 0; i < mPath.size(); ++i)
	{
		NavNode* pNextNode = ((i+1) == mPath.size()) ? 0 : mPath[i + 1];
		NavNode* pNode = mPath[i];

		NavTriangle* pNavTri = new NavTriangle;
		
		for (int e = 0; e < 3; ++e)
		{
			int idx0 = pNode->mFaceIdx + e;
			int idx1 = pNode->mFaceIdx + ((e + 1) % 3);

			NavVert* pNavVert = new NavVert;

			NavLine* pNavLine = new NavLine;
			pNavLine->line = LineSegment(mNavMesh->mVertices[mNavMesh->mIndices[idx0]], mNavMesh->mVertices[mNavMesh->mIndices[idx1]]);

			// this list of 3 adjacent triangles tells us which edge the connected node connects on
			if (pNode->mAdjacent[e] == pNextNode)
				pNavTri->mInsideLine.push_back(pNavLine);
			else
				pNavTri->mOutsideLine.push_back(pNavLine);
		}

		mNavTriangle.push_back(pNavTri);
	}


	// find the common/connecting edges between triangles
	int maxMiddleEdgeCount = mPath.size() - 1;
	middle = new LineSegment[maxMiddleEdgeCount];
	middleCount = 0;

	outside = new LineSegment[mPath.size() * 2];
	outsideCount = 0;

	for (int i = 1; i < mPath.size(); ++i)
	{
		NavNode* pPrevNode = mPath[i - 1];
		NavNode& node = *mPath[i];

		for (int e = 0; e < 3; ++e)
		{
			// this list of 3 adjacent triangles tells us which edge the connected node connects on
			if (node.mAdjacent[e] == pPrevNode)
			{
				int idx0 = node.mFaceIdx + e;
				int idx1 = node.mFaceIdx + ((e + 1) % 3);
				middle[middleCount] = LineSegment(mNavMesh->mVertices[mNavMesh->mIndices[idx0]],
					mNavMesh->mVertices[mNavMesh->mIndices[idx1]]);
				++middleCount;
			}
		}
	}

	for (int i = 1; i < mPath.size(); ++i)
	{
		NavNode& node = *mPath[i];

		for (int e = 0; e < 3; ++e)
		{
			int idx0 = node.mFaceIdx + e;
			int idx1 = node.mFaceIdx + ((e + 1) % 3);
			LineSegment segment = LineSegment(mNavMesh->mVertices[mNavMesh->mIndices[idx0]],
				mNavMesh->mVertices[mNavMesh->mIndices[idx1]]);
			
			bool bFound = false;
			for (int m = 0; m < middleCount; ++m)
			{
				LineSegment& middleSegment = middle[m];
				if (segment.p[0] == middleSegment.p[0] && segment.p[1] == middleSegment.p[1])
				{
					bFound = true;
					break;
				}			
			}

			if (!bFound)
			{
				outside[outsideCount] = segment;
				++outsideCount;
			}
		}
	}

	// for each vertex create visibility list
	for (int i = 0; i < mPath.size(); ++i)
	{
		NavNode& node = *mPath[i];
		
		for (int j = 0; j < 3; ++j)
		{
			NavNodeVert* vert = mNavNodeVert[mNavMesh->mIndices[node.mFaceIdx + j]];
			if (!vert)
			{
				NavNodeVert* vert = new NavNodeVert;
				mNavNodeVert[mNavMesh->mIndices[node.mFaceIdx + j]] = vert;
				vert->mNavNode = &node;
				vert->mNext = ((i+1) == mPath.size()) ? 0 : mPath[i+1];
				vert->mPos = mNavMesh->mVertices[mNavMesh->mIndices[node.mFaceIdx + j]];
			}
		}
	}

	// for each vertex create visibility list
	for (int i = 0; i < mPath.size(); ++i)
	{
		NavNode& node = *mPath[i];

		// all verts on a single triangle can see each other, 
		for (int j = 0; j < 3; ++j)
		{
			NavNodeVert* vert = mNavNodeVert[mNavMesh->mIndices[node.mFaceIdx + j]];
			CalculateVisibility(vert);
		}
	}

	int nothing = 0;

	//mNavNodeVert.push_back(new NavNodeVert


	/*
	if (mPoints)
		delete[] mPoints;

	mNumPoints = 2 + mPath.size();
	mPoints = new Vector4[mNumPoints];

	// find the common/connecting edges between triangles
	LineSegment* edges = new LineSegment[mPath.size() - 1];
	int numEdges = 0;

	Ray* vertices = new Ray[(mPath.size() - 1) * 2];
	int numVertices = 0;

	for (int i = 1; i < mPath.size(); ++i)
	{
		NavNode* pPrevNode = mPath[i - 1];
		NavNode& node = *mPath[i];

		for (int e = 0; e < 3; ++e)
		{
			// this list of 3 adjacent triangles tells us which edge the connected node connects on
			if (node.mAdjacent[e] == pPrevNode)
			{
				int idx0 = node.mFaceIdx + e;
				int idx1 = node.mFaceIdx + ((e + 1) % 3);
				edges[numEdges] = LineSegment(mNavMesh->mVertices[mNavMesh->mIndices[idx0]],
					mNavMesh->mVertices[mNavMesh->mIndices[idx1]]);

				// add verts with direction, make sure we dont add duplicate vertices
				bool vert1Found = false;
				bool vert2Found = false;
				Ray vert1 = edges[numEdges].AsRay();
				Ray vert2 = Ray(edges[numEdges].p[1], -vert1.direction);
				for (int v = Math::Max(0, numVertices - 2); v < numVertices; ++v)
				{
					if (vertices[v].origin == vert1.origin)
						vert1Found = true;

					if (vertices[v].origin == vert2.origin)
						vert2Found = true;
				}

				if (!vert1Found)
				{
					vertices[numVertices] = vert1;
					++numVertices;
				}

				if (!vert2Found)
				{
					vertices[numVertices] = vert2;
					++numVertices;
				}

				++numEdges;
			}
		}
	}

	int p = 0;
	mPoints[p] = mSource;
	++p;

	// iterate over each edge
	Vector4 start = mSource;
	for (int i = 0; i < numVertices; ++i)
	{
		Ray& a = vertices[i];
		for (int j = i + 2; j < numEdges; ++j)
		{
			Ray& b = vertices[j];

			Vector4 lineDir = b.origin - a.origin;
			float length = lineDir.Normalize3();

			Ray ab(a.origin, lineDir);

			Vector4 pointOnCorrectSideOfLine = a.origin + a.direction;
			Vector4 closestPoint = ab.ClosestPoint(pointOnCorrectSideOfLine);

			Vector4 normal = pointOnCorrectSideOfLine - closestPoint;
			normal.Normalize3();
			
			Plane plane(a.origin, normal);

			for (int k = i + 1; k < j; ++k)
			{
				Ray& c = vertices[k];

				
				float sideOfPlane = plane.DistanceToPoint(c.origin);

				// plane is occluded by c.
				if (sideOfPlane > 0.f)
				{
					int nothing = 0;
				}
			}
		}
	}

	mPoints[p] = mDestination;
	++p;
	mNumPoints = p;

/*
	// for each edge, find the point that would result in least distance if we were the only point of interest
	if (closestEdgePoints)
		delete[] closestEdgePoints;

	closestEdgePoints = new Vector4[numEdges];
	numClosestPoints = 0;
	for (int i = 0; i < numEdges; ++i)
	{
		float distance0 = (edges[i].p[0] - mSource).MagSquared() + (edges[i].p[0] - mDestination).MagSquared();
		float distance1 = (edges[i].p[1] - mSource).MagSquared() + (edges[i].p[1] - mDestination).MagSquared();

		closestEdgePoints[numClosestPoints] = (distance0 < distance1) ? edges[i].p[0] : edges[i].p[1];
		++numClosestPoints;
	}


	int p = 0;

	mPoints[p] = mSource;
	++p;

	LineSegment best(mSource, mDestination);

	// to generate the shortest back from the line segment best (mSource to mDestination),
	// 1. iterate forward through the path until we find a nav node the line segment does not intersect
	// 2. adjust the line segment best using the closest point on the tri to current best
	// 3. work backwards through the path till a line segment does not intersect best
	// 4. adjust the line segment best using closest point on the tri to current best
	// 5. go to step 1, repeat using last point of intersection as the start, repeat till there are no points left

	int frontIdx = 0;
	int backIdx = numEdges - 1;

	int insertIdx = 1;

	while (frontIdx != backIdx)
	{
		// go forwards
		while (frontIdx < backIdx)
		{
			LineSegment& edge = edges[frontIdx];
			++frontIdx;

			LineSegment result;
			if (!edge.ShortestSegmentBetweenSegment(best, &result))
				continue; // wtf?

			// if best intersects this edge, continue on, as we dont need to add a special
			// point to the point list
			if (result.Length() < Math::Epsilon)
				continue;

			// at this point we know the closest point from the triangle to the line segment 'best'
			Vector4& closestPoint = result.p[0];

			p = InsertVector4(mPoints, closestPoint, insertIdx, p);
			++insertIdx;

			// adjust best start position
			best.p[0] = closestPoint;
			break;
		}
/*
		// go backwards
		while (backIdx > frontIdx)
		{
			LineSegment& edge = edges[backIdx];
			--backIdx;

			LineSegment result;
			if (!edge.ShortestSegmentBetweenSegment(best, &result))
			 continue; // wtf?

			// if best intersects this edge, continue on, as we dont need to add a special
			// point to the point list
			if (result.Length() < Math::Epsilon)
				continue;

			// at this point we know the closest point from the triangle to the line segment 'best'
			Vector4& closestPoint = result.p[1];

			p = InsertVector4(mPoints, closestPoint, insertIdx, p);

			// adjust best end position
			best.p[1] = closestPoint;
			break;
		}* /
	}
/*
	// we need to see if the line line from source to destination intersects this triangle, if it doesnt,
	// iterate through all points and find the closest point
	// skip first and last nodes, as we know the src/dest points are both inside the triangles

	
	for (int i = 1; i < mPath.size() - 1; ++i)
	{
		NavNode& node = *mPath[i];

		Triangle tri(mNavMesh->mVertices[mNavMesh->mIndices[node.mFaceIdx]],
			mNavMesh->mVertices[mNavMesh->mIndices[node.mFaceIdx+1]],
			mNavMesh->mVertices[mNavMesh->mIndices[node.mFaceIdx+2]]);

		// check to see if line segment intersects triangle
		// if it does, skip adding a point
		Vector4 closestPoint;
		float closestDist = FLT_MAX;
		bool bIntersection = false;
		for (int x = 0; x < 3; ++x)
		{
			LineSegment edge = tri.GetEdgeAsSegment(x);

			LineSegment result;
			if (edge.ShortestSegmentBetweenSegment(best, &result))
			{
				float dist = result.Length();
				if (dist <= 0.1f)
				{
					bIntersection = true;
					break;
				}
				
				// calculate distance to end point, as a vert may be closer to the line
				// than another vert but is past the end point for the ideal shortest length
				// meaning this 'closer' vert will result in a longer path
				float distToEndPoint2 = (best.p[1] - result.p[1]).Mag3();
				float distToEndPoint1 = (best.p[0] - result.p[0]).Mag3();
				if (dist < closestDist && Math::Min(distToEndPoint1, distToEndPoint2) > Math::Epsilon)
				{
					closestDist = dist;
					closestPoint = result.p[0]; // closest point on 'edge' to 'best'
				}
			}
		}

		if (bIntersection)
			continue;

		// at this point we know the closest point from the triangle to the line segment 'best'
		mPoints[p] = closestPoint;
		++p;
	}
* /
	mPoints[p] = mDestination;
	++p;

	mNumPoints = p;*/
}

void NavPath::Render()
{
	const float cHeight = 0.1f;

	EffectParameter* colour = gRender.mDebugEffect->GetParameterByName("colour");

	colour->SetValue(&Vector4(0.f, 1.f, 0.f, 1.f));
	colour->Apply();
	gRender.mDebugEffect->SetPassParameter();

	//mSource.y = cHeight;
	//mDestination.y = cHeight;
	gRender.mDevice->DrawLine(mSource, mDestination);
	//mSource.y = 0.f;
	//mDestination.y = 0.f;

	colour->SetValue(&Vector4(0.f, 0.f, 1.f, 1.f));
	colour->Apply();
	gRender.mDebugEffect->SetPassParameter();

	// draw polys
	vector<NavNode*>::iterator it;
	for (it = mPath.begin(); it != mPath.end(); ++it)
	{
		NavNode& node = *(*it);

		Vector4 v0 = mNavMesh->mVertices[mNavMesh->mIndices[node.mFaceIdx]];
		v0.y = cHeight;

		Vector4 v1 = mNavMesh->mVertices[mNavMesh->mIndices[node.mFaceIdx+1]];
		v1.y = cHeight;

		Vector4 v2 = mNavMesh->mVertices[mNavMesh->mIndices[node.mFaceIdx+2]];
		v2.y = cHeight;

		gRender.mDevice->DrawLine(v0, v1);
		gRender.mDevice->DrawLine(v1, v2);
		gRender.mDevice->DrawLine(v2, v0);
	}
/*
	vector<NavNode*>::iterator it;
	for (it = mPath.begin(); it != mPath.end(); ++it)
	{
		NavNode& node = *(*it);
		node.mAveragePos.y = cHeight;
		Sphere sphere(node.mAveragePos, 0.1f);
		gRender.mDevice->DrawSphere(sphere);

		if ((it+1) == mPath.end())
			continue;

		NavNode* nextNode = *(it+1);

		// draw node links
		for (int j = 0; j < 3; ++j)
		{
			NavNode* adjacentNode = node.mAdjacent[j];
			if (adjacentNode != nextNode)
				continue;

			adjacentNode->mAveragePos.y = cHeight;
			gRender.mDevice->DrawLine(node.mAveragePos, adjacentNode->mAveragePos);
		}
	}*/

	colour->SetValue(&Vector4(1.f, 0.f, 0.f, 1.f));
	colour->Apply();
	gRender.mDebugEffect->SetPassParameter();

	for (int i = 0; i < (mNumPoints - 1); ++i)
	{
		mPoints[i].y = cHeight;
		mPoints[i+1].y = cHeight;
		gRender.mDevice->DrawLine(mPoints[i], mPoints[i+1]);
		mPoints[i].y = 0.f;
		mPoints[i+1].y = 0.f;
	}

	// testing
	colour->SetValue(&Vector4(1.f, 1.f, 1.f, 1.f));
	colour->Apply();
	gRender.mDebugEffect->SetPassParameter();

	for (int i = 0; i < numClosestPoints; ++i)
	{
		Sphere sphere(closestEdgePoints[i], 0.05f);
		sphere.position.y = cHeight;
		gRender.mDevice->DrawSphere(sphere);
	}


	// draw vert visibility
	colour->SetValue(&Vector4(0.f, 0.3f, 0.3f, 1.f));
	colour->Apply();
	gRender.mDebugEffect->SetPassParameter();

	{
		map<int, NavNodeVert*>::iterator it;
		for (it = mNavNodeVert.begin(); it != mNavNodeVert.end(); ++it)
		{
			NavNodeVert* vert = it->second;

			vector<NavNodeVert*>::iterator vIt;
			for (vIt = vert->mVisible.begin(); vIt != vert->mVisible.end(); ++vIt)
			{
				gRender.mDevice->DrawLine(vert->mPos, (*vIt)->mPos);
			}
		}
	}
}