#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <vector>

using namespace std;

class Component;
class EntityMgr;

//
// a list of components, thats it!
//
class Entity : public Observable
{
public:

	REFLECT_CLASS(Entity)
/*
	REFLECT_CLASS_VM
	(
		Entity, 
		(
			VAR_PTR(mParent),
			VAR_PTR(mName),
			VAR_PTR_STL(mEntity)
		),
		(
			FUNC_VOID(Init),
			FUNC_VOID(InsertEntity),
			FUNC_VOID(RemoveEntity),
			FUNC_VOID(Update)
		)
	)
*/
	Entity();

	virtual void		Init(Entity* parent);
	virtual void		Deinit();
	virtual void		Resolve();
	virtual Entity*		Clone();

	void				InsertEntity(Entity* entity);
	void				RemoveEntity(Entity* entity);

	virtual void		Update();
	virtual void		Render(int layer)								{}

	const char*			GetName()										{ return mName; }
	void				SetName(const char* name);

	template <class T>
	T*					GetEntity(Entity* previous = 0);

	// we should be able to iterate over multiple components of the same type!
	Entity*				GetEntity(ClassDesc* classDesc, Entity* previous = 0);

	// entity which has no parent
	Entity*				GetRootEntity();

	Entity*				GetEntity(const char* name);

	const char*			mName;
	Entity*				mParent;
	vector<Entity*>		mEntity;
};



template <class T>
T* Entity::GetEntity(Entity* previous)
{
	Entity* e = GetEntity(::GetClassDesc<T>() /*GetClassDesc()*/, previous);
	return static_cast<T*>(e);
}

#endif