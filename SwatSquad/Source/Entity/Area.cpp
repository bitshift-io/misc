#include "StdAfx.h"
#include "Area.h"

REGISTER_CLASS(Rect)
REGISTER_CLASS_P(AreaConnection, Entity)
REGISTER_CLASS_P(Area, Entity)

void AreaConnection::Init(Entity* parent)
{
	gAreaMgr.mConnection.push_back(this);
}

void AreaConnection::Deinit()
{
}

void AreaConnection::Render(int layer)
{
	if (layer != RenderMgr::RL_Debug)
		return;

	EffectParameter* colour = gRender.mDebugEffect->GetParameterByName("colour");

	colour->SetValue(&Vector4(0.f, 1.f, 0.f, 1.f));
	colour->Apply();
	gRender.mDebugEffect->SetPassParameter();

	Vector4 v0 = (*mConnection)[0];
	Vector4 v1 = (*mConnection)[1];
	Vector4 v2 = (*mConnection)[2];
	Vector4 v3 = (*mConnection)[3];

	v0.y += 0.1f;
	v1.y += 0.1f;
	v2.y += 0.1f;
	v3.y += 0.1f;

	gRender.mDevice->DrawLine(v0, v1);
	gRender.mDevice->DrawLine(v1, v2);
	gRender.mDevice->DrawLine(v2, v3);
	gRender.mDevice->DrawLine(v3, v0);
}




void Area::Init(Entity* parent)
{
	gAreaMgr.mArea.push_back(this);

	Geometry* geometry = gRender.mFactory->Create<Geometry>();

	int numVert = mSubArea.size() * 4;
	int numIndex = mSubArea.size() * 6;

	geometry->Init(gRender.mDevice, VF_Position | VF_TexCoord0, numVert, numIndex, RBU_Static);

	VertexFrame& frame = geometry->GetVertexFrame();
	RenderBuffer* indexBuffer = geometry->GetIndexBuffer();
	RenderBuffer* posBuffer = frame.mPosition;
	RenderBuffer* texBuffer = frame.mTexCoord[0];

	RenderBufferLock lock;

	indexBuffer->Lock(lock);
	for (int a = 0; a < mSubArea.size(); ++a)
	{
		lock.uintData[a * 6 + 0] = (a * 4) + 0;
		lock.uintData[a * 6 + 1] = (a * 4) + 1;
		lock.uintData[a * 6 + 2] = (a * 4) + 2;

		lock.uintData[a * 6 + 3] = (a * 4) + 2;
		lock.uintData[a * 6 + 4] = (a * 4) + 3;
		lock.uintData[a * 6 + 5] = (a * 4) + 0;
	}
	indexBuffer->Unlock(lock);

	posBuffer->Lock(lock);
	for (int a = 0; a < mSubArea.size(); ++a)
	{
		Vector4& rect = mSubArea[a]->mRect;
		lock.vector4Data[a * 4 + 0] = Vector4(rect.x, 0.f, rect.y);
		lock.vector4Data[a * 4 + 1] = Vector4(rect.x, 0.f, rect.w);
		lock.vector4Data[a * 4 + 2] = Vector4(rect.z, 0.f, rect.w);
		lock.vector4Data[a * 4 + 3] = Vector4(rect.z, 0.f, rect.y);
	}
	posBuffer->Unlock(lock);

	texBuffer->Lock(lock);
	for (int a = 0; a < mSubArea.size(); ++a)
	{
		Vector4& rect = mSubArea[a]->mRect;
		float zSize = Math::Abs(rect.x - rect.z);
		float xSize = Math::Abs(rect.y - rect.w);
		lock.vector4Data[a * 4 + 0] = Vector4(0.f, 0.f);
		lock.vector4Data[a * 4 + 1] = Vector4(xSize, 0.f);
		lock.vector4Data[a * 4 + 2] = Vector4(xSize, zSize);
		lock.vector4Data[a * 4 + 3] = Vector4(0.f, zSize);
	}
	texBuffer->Unlock(lock);

	mScene = gRender.mFactory->CreateScene();
	SceneNode* sceneNode = mScene->GetRootNode();

	Material* material = gRender.mFactory->Load<Material>(&ObjectLoadParams(gRender.mDevice, "Area"));

	Mesh* mesh = gRender.mFactory->Create<Mesh>();
	sceneNode->SetObject(mesh);
	mesh->AddMeshMat(MeshMat(material, geometry)); // TODO - set up material here

	gRender.mMain->InsertScene(mScene);
}

void Area::Deinit()
{
}

bool Overlap(const LineSegment& ls1, const LineSegment& ls2)
{/*
	Ray r1 = ls1.AsRay();
	//Ray r2 = ls2.AsRay();

	//Ray r1.ClosestPoint(ls2.p[0]);

	if (r1.)


	// overlap if the both lie on the same line
	if (ls1.p[0].x == ls2.p[0].x)*/
	return false;
}

void Area::Resolve()
{
	// iterate through all areas, work out if it conects to us
	AreaMgr::ConnectionIterator it;
	for (it = gAreaMgr.mConnection.begin(); it != gAreaMgr.mConnection.end(); ++it)
	{
		AreaConnection* connection = *it;
		Rect& connectionRect = *connection->mConnection;

		for (int s = 0; s < mSubArea.size(); ++s)
		{
			Rect& rect = *mSubArea[s];

			for (int w = 0; w < 4; ++w)
			{
				LineSegment wall(rect[w], rect[(w + 1) % 4]);

				for (int c = 0; c < 4; ++c)
				{
					LineSegment connectionWall(connectionRect[c], connectionRect[(c + 1) % 4]);

					if (Overlap(connectionWall, wall))
					{
					}
/*
					LineSegment result;
					float time0, time1;
					bool bResult = wall.ShortestSegmentBetweenSegment(connectionWall, &result, &time0, &time1);

					// some error occured
					if (!bResult)
						continue;


					int nothing = 0;

					float time = 0.f;
					wall.ClosestPoint(connectionRect[c], &time);
					if (time >= 0.f && time <= 1.f)
					{
						connection->mArea.push_back(this);
						mConnection.push_back(connection);
						goto endConnectionLoop;
					}
					else
					{
						int nothing = 0;
					}*/
				}
			}
		}

	endConnectionLoop:
		{
		}
	}
}