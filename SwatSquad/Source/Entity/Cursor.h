#ifndef _CURSOR_H_
#define _CURSOR_H_

class Cursor
{
public:

	Cursor();

	virtual void		Init();
	virtual void		Deinit();
	virtual void		Update();

protected:

	void				CompleteSelection();
	void				UpdateSelection();

	Scene*				mScene;
	//Vector4				mScreenLocation; // location in screenspace (mapped (-1, -1) to (1, 1))
	Vector4				mSnapDistance;
	float				mSensitivity;

	vector<Actor*>		mSelection; // selection
};

#endif