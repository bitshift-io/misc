#ifndef _ACTOR_H_
#define _ACTOR_H_

enum ActorState
{
	AS_Idle,
	AS_Move,
};

class Actor
{
public:

	Actor();

	virtual void		Init();
	virtual void		Deinit();
	virtual void		Update();
	void				Render();

	void				SetDestination(const Vector4& destination);

//protected:

	Vector4				mSnapDistance;
	Scene*				mScene;
	bool				mSelected;
	NavPath*			mNavPath;
	int					mTargetNavNodeIdx;

	ActorState			mState;
	float				mMovementSpeed;
};

#endif