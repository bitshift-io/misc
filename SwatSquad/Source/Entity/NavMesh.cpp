#include "StdAfx.h"
#include "NavMesh.h"

void NavMesh::Init()
{
	SceneNode* sceneNode = gWorld.mScene->GetNodeByName("Nav");
	sceneNode->SetFlags(SNF_Hidden);
	Process(static_cast<Mesh*>(sceneNode->GetObject())->GetMeshMat(0).geometry);
}

void NavMesh::Process(Geometry* geometry)
{/*
	RenderBuffer* idx = geometry->GetIndexBuffer();
	VertexFrame& frame = geometry->GetVertexFrame();
	RenderBuffer* pos = frame.mPosition;

	RenderBufferLock posLock;
	pos->Lock(posLock, RBLF_ReadOnly);
	mVertices = posLock.vector4Data;
	mNumVertices = posLock.count;

	// calculate face connectivity
	RenderBufferLock idxLock;
	idx->Lock(idxLock, RBLF_ReadOnly);
	mIndices = idxLock.uintData;
	mNumIndices = idxLock.count;

	mNumNavNode = idxLock.count / 3;
	mNavNode = new NavNode[mNumNavNode];
	
	for (unsigned int i = 0; i < idxLock.count; i += 3)
	{
		NavNode& iNode = mNavNode[i / 3];
		iNode.mFaceIdx = i;
		int iIdx[3] = {idxLock.uintData[i], idxLock.uintData[i+1], idxLock.uintData[i+2]};

		// get average node pos
		Vector4 nodePos(VI_Zero);
		for (int j = 0; j < 3; ++j)
			nodePos += mVertices[mIndices[iNode.mFaceIdx + j]];
		iNode.mAveragePos = nodePos / 3.f;

		for (unsigned int j = i + 3; j < idxLock.count; j += 3)
		{
			NavNode& jNode = mNavNode[j / 3];
			int jIdx[3] = {idxLock.uintData[j], idxLock.uintData[j+1], idxLock.uintData[j+2]};
			
			// check if any pairs from i face meets j face
			for (int x = 0; x < 3; ++x)
			{
				for (int y = 0; y < 3; ++y)
				{
					int x2 = (x+1)%3;
					int y2 = (y+1)%3;
					if (iIdx[x] == jIdx[y] && iIdx[x2] == jIdx[y2]
						|| iIdx[x] == jIdx[y2] && iIdx[x2] == jIdx[y])
					{
						iNode.mAdjacent[x] = &jNode;
						jNode.mAdjacent[y] = &iNode;
					}
				}
			}
		}
	}

	//idxLock.*/
}

void NavMesh::Render()
{/*
	const float cHeight = 0.1f;

	// draw polys
	for (int i = 0; i < mNumIndices; i += 3)
	{
		Vector4 v0 = mVertices[mIndices[i]];
		v0.y = cHeight;

		Vector4 v1 = mVertices[mIndices[i+1]];
		v1.y = cHeight;

		Vector4 v2 = mVertices[mIndices[i+2]];
		v2.y = cHeight;

		gRender.mDevice->DrawLine(v0, v1);
		gRender.mDevice->DrawLine(v1, v2);
		gRender.mDevice->DrawLine(v2, v0);
	}

	// draw nodes (center of polys)
	for (int i = 0; i < mNumNavNode; ++i)
	{
		NavNode& node = mNavNode[i];
		node.mAveragePos.y = cHeight;
		Sphere sphere(node.mAveragePos, 0.1f);
		gRender.mDevice->DrawSphere(sphere);

		// draw node links
		for (int j = 0; j < 3; ++j)
		{
			NavNode* adjacentNode = node.mAdjacent[j];
			if (!adjacentNode)
				continue;

			adjacentNode->mAveragePos.y = cHeight;
			gRender.mDevice->DrawLine(node.mAveragePos, adjacentNode->mAveragePos);
		}
	}*/
}

// make a triangle math class?
// http://www.blackpawn.com/texts/pointinpoly/default.html
bool PointInTriangle(Vector4& a, Vector4& b, Vector4& c, Vector4& point)
{
	// Compute vectors        
	Vector4 v0 = c - a;
	Vector4 v1 = b - a;
	Vector4 v2 = point - a;

	// Compute dot products
	float dot00 = v0.Dot3(v0);
	float dot01 = v0.Dot3(v1);
	float dot02 = v0.Dot3(v2);
	float dot11 = v1.Dot3(v1);
	float dot12 = v1.Dot3(v2);

	// Compute barycentric coordinates
	float invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
	float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
	float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

	// Check if point is in triangle
	return (u > 0) && (v > 0) && (u + v < 1);
}

NavNode* NavMesh::GetClosest(Vector4& pos)
{
	// if pos is inside of a triangle, simply return that node
	for (int i = 0; i < mNumNavNode; ++i)
	{
		NavNode& node = mNavNode[i];
		
		Vector4 v0 = mVertices[mIndices[node.mFaceIdx]];
		Vector4 v1 = mVertices[mIndices[node.mFaceIdx+1]];
		Vector4 v2 = mVertices[mIndices[node.mFaceIdx+2]];

		if (PointInTriangle(v0, v1, v2, pos))
			return &node;
	}

	return 0;
}