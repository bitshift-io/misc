#include "StdAfx.h"
#include "Cursor.h"

Cursor::Cursor() :
	mScene(0)
{
}

void Cursor::Init()
{
	mSensitivity = 0.2f;
	//mScreenLocation = Vector4(0.f, 0.f);
	mSnapDistance = Vector4(1.f, 1.f); // snap to every meter

	mScene = gRender.LoadScene("Cursor");
	if (!mScene)
	{
		return;
	}

	// should we name renderes so we can decide what model goes in what renderer?
	gRender.InsertScene(mScene);
}

void Cursor::Deinit()
{

}

void Cursor::Update()
{
	WindowResolution res;
	int x, y;
	gRender.mWindow->GetCursorPosition(x, y);
	gRender.mWindow->GetWindowResolution(res);

	// need to update the cursors location
	Camera* camera = gRender.mCamera;
	Matrix4 viewProjInverse = camera->GetView() * camera->GetProjection();
	viewProjInverse.Inverse();
/*
	float deltaX = gGame.mInput.GetState(MOUSE_AxisX) * mSensitivity;
	float deltaY = gGame.mInput.GetState(MOUSE_AxisY) * mSensitivity;

	// might want to convert world to screen so the mouse cursor doesnt move, unless it needs to be clamped
	if (gGame.mInput.GetStateBool(MOUSE_Right) || gGame.mInput.GetStateBool(MOUSE_Middle))
	{
		deltaX = 0.f;
		deltaY = 0.f;
	}*/

	Vector4	mScreenLocation;
	mScreenLocation.x = (float(x) / res.width) * 2.f - 1.f;
		//Math::Clamp(-1.f, mScreenLocation.x + deltaX, 1.f);
	mScreenLocation.y = -((float(y) / res.height) * 2.f - 1.f);
		//Math::Clamp(-1.f, mScreenLocation.y - deltaY, 1.f);

	// map (-1, -1)(1,1) to (0,0)(1,1)
	Vector4 convertedScreenSpace = mScreenLocation; //(mScreenLocation + 1.f) * 0.5f;
	convertedScreenSpace.w = 1.f;
	convertedScreenSpace.z = 0.f;//camera->GetNearPlane();

	Vector4 convertedScreenSpace2 = convertedScreenSpace;
	convertedScreenSpace2.z = 1.f; //camera->GetFarPlane();

	// convert screen space to 3d coord for the model
	Vector4 screenPos = viewProjInverse * convertedScreenSpace;
	Vector4 screenPos2 = viewProjInverse * convertedScreenSpace2;

	screenPos /= screenPos.w;
	screenPos2 /= screenPos2.w;

	// hard coded ground plane ATM
	Plane groundPlane(Vector4(0.f, 0.f, 0.f), Vector4(0.f, 1.f, 0.f));

	Vector4 cursorDir = screenPos2 - screenPos;
	cursorDir.Normalize();
	Ray cursorRay(screenPos, cursorDir);

	float intersectTime = groundPlane.Intersect(cursorRay);
	Vector4 intersectPos = Vector4(screenPos) + (cursorDir * intersectTime);

	// apply snapping
	// how many times does the position go in to the snap distance (rounded off)
	// then use this to generate the grid location
	Vector4 intervals = Vector4(intersectPos.x, intersectPos.z) / mSnapDistance;
	intervals.x = floor(intervals.x);
	intervals.y = floor(intervals.y);

	intersectPos.x = intervals.x * mSnapDistance.x;
	intersectPos.z = intervals.y * mSnapDistance.y;

	// assume center of cursor aligns with center of tile
	intersectPos.x += mSnapDistance.x * 0.5f;
	intersectPos.z += mSnapDistance.y * 0.5f;

	// apply the results
	Matrix4 world = mScene->GetTransform();
	world.SetTranslation(intersectPos);
	mScene->SetTransform(world);

	if (gGame.mInput.WasPressed(MOUSE_Left))
	{
		CompleteSelection();
	}

	UpdateSelection();
}

void Cursor::UpdateSelection()
{
	vector<Actor*>::iterator it;
	for (it = mSelection.begin(); it != mSelection.end(); ++it)
	{
		Actor* actor = *it;
		actor->SetDestination(mScene->GetTransform().GetTranslation());
	}
}

void Cursor::CompleteSelection()
{
	// tell any selected actors to move
	vector<Actor*>::iterator it;
	for (it = mSelection.begin(); it != mSelection.end(); ++it)
	{
		Actor* actor = *it;
		actor->mState = AS_Move;
	}

	mSelection.clear();

	for (int i = 0; i < gWorld.mNumActor; ++i)
	{
		Vector4 cursorPos = mScene->GetTransform().GetTranslation();
		Vector4 actorPos = gWorld.mActor[i]->mScene->GetTransform().GetTranslation();
		float dist = (actorPos - cursorPos).Mag3();
		if (dist <= 0.5f)
		{
			mSelection.push_back(gWorld.mActor[i]);
		}
	}
}

