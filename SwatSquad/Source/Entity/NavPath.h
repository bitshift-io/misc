#ifndef _NAVPATH_H_
#define _NAVPATH_H_

#include <stack>

using namespace std;

class NavLine
{
public:

	LineSegment line;
};

class NavVert
{
public:

	vector<NavVert*>	mVisible;
	Vector4				mPos;
	int					mIndex;
};

class NavTriangle
{
public:

	Triangle triangle;

	vector<NavVert*> mNavVert;
	vector<NavLine*> mInsideLine;
	vector<NavLine*> mOutsideLine;
};

class NavNodeVert
{
public:

	vector<NavNodeVert*>	mVisible;
	Vector4					mPos;
	NavNode*				mNavNode;
	NavNode*				mNext;
};

//
// Uses nav mesh to generate a path between two points
//
class NavPath
{
public:

	NavPath();

	void Update();
	void Render();

	void GenerateShortestPath();

	void CalculateVisibility(NavNodeVert* vert);

	Vector4		mSource;
	Vector4		mDestination;

	NavMesh*	mNavMesh;

	vector<NavNode*>	mPath;
	Vector4*			mPoints;
	int					mNumPoints;

	// testing
	Vector4* closestEdgePoints;
	int numClosestPoints;

	map<int, NavNodeVert*>	mNavNodeVert;

	LineSegment* middle;
	int middleCount;

	LineSegment* outside;
	int outsideCount;

	vector<NavTriangle*>	mNavTriangle;
	
};

#endif