#ifndef _REFLECTMATH_H_
#define _REFLECTMATH_H_

//#define VAR_MATH(x) \
//	*(new VariableDesc(#x, (char*)&x-(char*)this, sizeof(x), 0, TypeOf(x)))

class Matrix4TypeDesc : public TypeDesc
{
public:

	void					GetTypeName(char* buf);

	virtual bool			LoadString(void* variable, const char* strValue, int index = 0) const;
	virtual bool			LoadBinary(void* variable, void* data, int index = 0) const;

	virtual Type			GetType() const																{ return T_Unknown; }
};

class Vector4TypeDesc : public TypeDesc
{
public:

	void					GetTypeName(char* buf);

	virtual bool			LoadString(void* variable, const char* strValue, int index = 0) const;
	virtual bool			LoadBinary(void* variable, void* data, int index = 0) const;

	virtual Type			GetType() const																{ return T_Unknown; }
};


template<>
inline TypeDesc* TypeOf(Matrix4&) 
{ 
	static Matrix4TypeDesc mathType;
    return &mathType;
}

template<>
inline TypeDesc* TypeOf(Vector4&) 
{ 
	static Vector4TypeDesc mathType;
    return &mathType;
}

#endif