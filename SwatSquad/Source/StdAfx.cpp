#include "StdAfx.h"

static const char *sBuild = __DATE__ " " __TIME__;

#ifdef WIN32
#include "Memory/NoMemory.h"
#include <tchar.h>
#include <windows.h>

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	char cmdLine[256];
	WideCharToMultiByte(CP_ACP, 0, lpCmdLine, -1, cmdLine, 256, NULL, NULL);
	return Game::Main(cmdLine);
}
#else
int main(int argc, char* argv[])
{
    string cmdLine;
    for (int i = 0; i < argc; ++i)
    {
        cmdLine += string(argv[i]);
    }
    return Game::Main(cmdLine.c_str());
}
#endif





