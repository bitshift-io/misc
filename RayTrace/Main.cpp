#include "StdAfx.h"
#include "Main.h"

static const char *sBuild = __DATE__ " " __TIME__;
								
int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	char cmdLine[256];
	WideCharToMultiByte(CP_ACP, 0, lpCmdLine, -1, cmdLine, 256, NULL, NULL);
	return RayTrace::Main(cmdLine);
}

bool RayTrace::Init()
{
	Log::Print("Build: %s\n", sBuild);

	// render
	RenderFactoryParameter param;
	param.path.mBasePath = "";
	param.path.mTexture = "texture";
	param.path.mScene = "scene";
	param.path.mEffect = "effect";
	param.path.mMaterial = "material";

	mRenderFactory = new OGLRenderFactory(param);

	mWindow = mRenderFactory->CreateWindow();
	mWindow->Init("Ray Trace", 1024, 768, false);
	mDevice = mRenderFactory->CreateDevice();
	mDevice->Init(mWindow);
	//mWindow->Show();

	mScene = mRenderFactory->LoadScene(&ObjectLoadParams(mRenderFactory, mDevice, "sample"));

	Matrix4 transform(MI_Identity);
	transform.Translate(Vector3(0.f, -0.5f, 1.f));
	mScene->SetTransform(transform);

	mLight = (Light*)mRenderFactory->Create(Light::Type);
	mLight->SetPosition(Vector3(10.f, 10.f, 0.f));
	mLight->SetDiffuseColour(Vector4(1.f, 0.f, 0.f, 1.f));

	mRScene.Init(mScene);
	return true;
}

void RayTrace::Deinit()
{
	// render
	mDevice->Deinit(mWindow);
	mRenderFactory->ReleaseDevice(&mDevice);

	mWindow->Deinit();
	mRenderFactory->ReleaseWindow(&mWindow);
}

void RayTrace::Render()
{
	//mDevice->Begin();

	int width = 512;
	int height = 512;

	float fov = Math::DtoR(90.f);
	float fovInc = fov / float(width);

	char* frame = new char[width * height * 3];
	memset(frame, 128, sizeof(char) * width * height * 3);


	// launch a bunch of rays
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			Matrix4 rot(MI_Identity);
			rot.RotateY(fovInc * (x - (width / 2)));
			rot.RotateX(fovInc * (y - (height / 2)));
			RRay ray;
			if (ray.Intersect(rot.GetZAxis(), Vector3(0.f, 0.f, 0.f), &mRScene, false))
			{
				Vector4 colour = ray.mColour;
				colour = colour * 255;
				frame[(x + (y * width)) * 3 + 0] = (char)colour.x;
				frame[(x + (y * width)) * 3 + 1] = (char)colour.y;
				frame[(x + (y * width)) * 3 + 2] = (char)colour.z;
			}
		}

		int percent = int(100.f * float(float(y) / float(height)));
		Log::Print("%i\n", percent);
	}

	TGA tga;
	tga.Save("frame.tga", frame, width, height, 3 * 8);

	delete[] frame;

	//mDevice->End();
}

int RayTrace::Main(const char* cmdLine)
{
	if (gRayTrace.Init())
		gRayTrace.Render();

	gRayTrace.Deinit();
	return 0;
}




