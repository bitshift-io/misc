#ifndef _RRAY_H_
#define _RRAY_H_

struct Tri;

class RRay
{
public:
	
	bool Intersect(const Vector3& vDir, const Vector3& vStart, RScene* scene, bool bShadowRay);
/*
	bool Intersect(const Vector3& vDir, const Vector3& vStart, Scene* scene, bool bShadowRay);
	
//protected:

	bool Intersect(SceneNode* node, Scene* scene);
	bool Intersect(Mesh* mesh, SceneNode* node, Scene* scene);
*/
	Vector3	mDir;
	Vector3 mStart;
	float	mTime;
	bool	mShadow;
	bool	mShadowRay;
	Vector4 mColour;
	Tri*	mTri;

};

#endif