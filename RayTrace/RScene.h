#ifndef _RSCENE_H_
#define _RSCENE_H_

class RMesh;

class RScene
{
public:

	void	Init(Scene*	scene);
	void	Deinit();

	void	Init(SceneNode* node);
	void	Init(Mesh* mesh, SceneNode* node);

	vector<RMesh*>	mMesh;
};

#endif