#include "StdAfx.h"
#include "RMesh.h"

void RMesh::Init(Mesh* mesh, SceneNode* node)
{
	// generate tri's and bounds
	Matrix4 transform = node->GetWorldTransform();
	Vector3 center(VI_Zero);
	float radius = 0.f;
	int count = 0;

	for (int i = 0; i < mesh->GetNumMeshMat(); ++i)
	{
		MeshMat& meshMat = mesh->GetMeshMat(i);
		VertexFrame& frame = meshMat.geometry->GetVertexFrame();
		RenderBuffer* index = meshMat.geometry->GetIndexBuffer();
		RenderBuffer* pos = frame.mPosition;
		RenderBuffer* norm = frame.mNormal;
		
		for (int j = 0; j < index->GetSize(); j += 3)
		{
			Tri triangle;
			triangle.position[0] = transform * pos->GetVector3()[ index->GetUnsignedInt()[j + 0] ];
			triangle.position[1] = transform * pos->GetVector3()[ index->GetUnsignedInt()[j + 1] ];
			triangle.position[2] = transform * pos->GetVector3()[ index->GetUnsignedInt()[j + 2] ];
			triangle.bound.CreateFromTri(triangle.position[0], triangle.position[1], triangle.position[2]);
			mTri.push_back(triangle);

			center += triangle.position[0];
			center += triangle.position[1];
			center += triangle.position[2];

			count += 3;
		}
	}

	center /= count;

	vector<Tri>::iterator it;
	for (it = mTri.begin(); it != mTri.end(); ++it)
	{
		for (int i = 0; i < 3; ++i)
		{
			float dist = (it->position[i] - center).MagSquared();
			if (dist > radius)
				radius = dist;
		}
	}

	radius = sqrt(radius);

	mBound.radius = radius;
	mBound.position = center;
}