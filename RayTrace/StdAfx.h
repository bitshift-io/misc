#ifndef ___STDAFX_H_
#define ___STDAFX_H_

// STL
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <algorithm>

using namespace std;

// boost
#include <boost/token_iterator.hpp>

// win32 crap
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#define NOMINMAX
#include <windows.h>

#undef min
#undef max
//#undef GetObject
#undef GetClassInfo
#undef SendMessage

#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include "Resource.h"


// more libraries
#include "Render/Window.h"
#include "Render/Device.h"
#include "Render/Font.h"
#include "Render/Renderer.h"
#include "Render/Camera.h"
#include "Render/Geometry.h"
#include "Render/Material.h"
#include "Render/ParticleSystem.h"
#include "Render/Shape.h"
#include "Render/SceneObject.h"
#include "Render/Mesh.h"
#include "Render/Spline.h"
#include "Render/Light.h"
#include "Render/TGA.h"

#include "Render/OGL/OGLRenderFactory.h"

#include "Template/Singleton.h"
#include "File/Log.h"

#include "Math/Ray.h"

#include "Main.h"
#include "Ray.h"
#include "Triangle.h"
#include "RScene.h"
#include "RMesh.h"

#endif