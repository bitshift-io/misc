#include "StdAfx.h"
#include "Ray.h"

bool RRay::Intersect(const Vector3& vDir, const Vector3& vStart, RScene* scene, bool bShadowRay)
{
	mTri = 0;

	bool intersect = false;
	Ray r(vStart, vDir);

	vector<RMesh*>::iterator it;
	for (it = scene->mMesh.begin(); it != scene->mMesh.end(); ++it)
	{
		RMesh* mesh = *it;
		if (r.Intersect(mesh->mBound))
		{
			vector<Tri>::iterator triIt;
			for (triIt = mesh->mTri.begin(); triIt != mesh->mTri.end(); ++triIt)
			{
				Tri& tri = *triIt;
				if (r.Intersect(tri.bound))
				{
					TriIntersect intersection;
					if (r.IntersectTri(tri.position[0], tri.position[1], tri.position[2], true, &intersection))
					{
						if (intersect == false)
						{
							mTime = intersection.rayDistance;
							mTri = &tri;
						}
						else
						{
							if (intersection.rayDistance < mTime)
							{
								mTri = &tri;
								mTime = intersection.rayDistance;
							}
						}

						intersect = true;
					}
				}
			}
		}
	}

	// cast a shadow ray
	if (intersect && !bShadowRay && mTri)
	{
		Vector3 intersectPoint = vStart + vDir * mTime;
		Vector3 lightDir = gRayTrace.mLight->GetPosition() - intersectPoint;
		float lightDist = lightDir.Normalize();

		// if this poly is facing away from the light, its shadowed
		Vector3 edge1 = mTri->position[1] - mTri->position[0];
		Vector3 edge2 = mTri->position[2] - mTri->position[0];

		Vector3 pVec = lightDir.Cross(edge2);
		float dot = edge1.Dot(pVec);

		RRay shadowRay;
		if (dot < 0.f || shadowRay.Intersect(lightDir, intersectPoint, scene, true))
		{
			// shadowed
			mColour = Vector4(0.f, 0.f, 0.f, 1.f);
		}
		else
		{
			// visible
			float att = (10.0f / lightDist);
			mColour = gRayTrace.mLight->GetDiffuseColour() * att;
		}
	}

	return intersect;
}

/*
bool Ray::Intersect(const Vector3& vDir, const Vector3& vStart, Scene* scene, bool bShadowRay)
{
	mShadow = false;
	mTime = -1.f;
	mStart = vStart;
	mDir = vDir;
	mShadowRay = bShadowRay;

	return Intersect(scene->GetRootNode(), scene);
}

bool Ray::Intersect(SceneNode* node, Scene* scene)
{
	bool intersects = false;

	SceneObject* obj = node->GetObject();
	if (obj)
	{
		switch (obj->GetType())
		{
		case Mesh::Type:
			intersects |= Intersect(static_cast<Mesh*>(obj), node, scene);
			break;
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
	{
		intersects |= Intersect(*it, scene);
	}

	return intersects;
}

bool Ray::Intersect(Mesh* mesh, SceneNode* node, Scene* scene)
{
	bool intersects = false;

	Matrix4 transform = node->GetWorldTransform();

	for (int i = 0; i < mesh->GetNumMeshMat(); ++i)
	{
		MeshMat& meshMat = mesh->GetMeshMat(0);
		VertexFrame& frame = meshMat.geometry->GetVertexFrame();
		RenderBuffer* index = meshMat.geometry->GetIndexBuffer();
		RenderBuffer* pos = frame.mPosition;
		RenderBuffer* norm = frame.mNormal;
		
		for (int j = 0; j < index->GetSize(); j += 3)
		{
			Triangle triangle;
			triangle.Create(pos->GetVector3()[ index->GetUnsignedInt()[j + 0] ],
				pos->GetVector3()[ index->GetUnsignedInt()[j + 1] ],
				pos->GetVector3()[ index->GetUnsignedInt()[j + 2] ], transform);

			//triangle.Create(Vector3(-1.f, -1.f, 1.f), Vector3(-1.f, 1.f, 1.f), Vector3(1.f, 0.f, 1.f), transform);

			// http://www.lighthouse3d.com/opengl/maths/index.php?raytriint
			// http://jgt.akpeters.com/papers/MollerTrumbore97/code.html

			Vector3 edge1 = triangle.mPosition[1] - triangle.mPosition[0];
			Vector3 edge2 = triangle.mPosition[2] - triangle.mPosition[0];

			Vector3 pVec = mDir.Cross(edge2);
			float det = edge1.Dot(pVec);

			if (det < 0.f)
				continue;

			//if (det > -Math::Epsilon && det < Math::Epsilon)
			//	continue;

			float invDet = 1.f / det;

			// distance from ray origin to vert0
			Vector3 tVec = mStart - triangle.mPosition[0];

			// calulate u
			float u = invDet * tVec.Dot(pVec);
			if (u < 0.f || u > 1.f)
				continue;

			// calulate v
			Vector3 qVec = tVec.Cross(edge1);
			float v = invDet * mDir.Dot(qVec);
			if (v < 0.f || u + v > 1.f)
				continue;

			// calculate intersection point
			float time = invDet * edge2.Dot(qVec);
			if (time > Math::Epsilon) // ray intersection
			{
				mTime = Math::Min(time, mTime);
				if (mTime < 0.f)
					mTime = time;

				intersects = true;

				// shoot a ray towards the light, see if its in shadow
				if (!mShadowRay)
				{
					Vector3 intersectPoint = mStart + mDir * mTime;
					Ray lightRay;
					Vector3 lightDir = gRayTrace.mLight->GetPosition() - intersectPoint;
					float lightDist = lightDir.Normalize();
					mShadow = lightRay.Intersect(lightDir, intersectPoint, scene, true);
					if (mShadow)
					{
						mColour = Vector4(0.f, 0.f, 0.f, 1.f);
					}
					else
					{
						float att = (10.0f / lightDist);
						mColour = gRayTrace.mLight->GetDiffuseColour() * att;
					}
				}
			}

			
/*
			// http://www.vhml.org/theses/usher/node27.html

			// plane ray intersection
			float intersect = triangle.mPlane.normal.Dot(mDir);
			if (intersect == 0.f)
				continue;

			float time = (triangle.mPlane.distance + triangle.mPlane.normal.Dot(mStart)) / intersect;

			int diminanteAxis = 0;
			float max = Math::Max(Math::Max(triangle.mPlane.normal.x, triangle.mPlane.normal.y), triangle.mPlane.normal.z);
			if (triangle.mPlane.normal.y == max)
				diminanteAxis = 1;
			else if (triangle.mPlane.normal.z == max)
				diminanteAxis = 2;
/*
			float alpha = 0;
			float beta = 0;

			// if an intersection has occured
			if (alpha >= 0.f && beta >= 0.f && (alpha + beta) <= 1.f)
			{
				int nothing = 0;
			}* /
		}
	}

	return intersects;
}*/