#ifndef _RMESH_H_
#define _RMESH_H_

struct Tri
{
	Sphere	bound;
	Vector3 position[3];
};

class RMesh
{
public:

	void	Init(Mesh* mesh, SceneNode* node);
	void	Deinit();

	Sphere			mBound;
	vector<Tri>		mTri;
};

#endif