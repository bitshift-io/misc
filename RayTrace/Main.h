#ifndef _MAIN_H_
#define _MAIN_H_

#include "StdAfx.h"
#include "RScene.h"

using namespace std;

#define gRayTrace RayTrace::GetInstance()

//
// There can only be one game running per application
//
class RayTrace : public Singleton<RayTrace>
{
public:

	bool					Init();
	void					Deinit();
	void					Render();

	static int				Main(const char* cmdLine);

	// render
	RenderFactory*			mRenderFactory;
	Window*					mWindow;
	Device*					mDevice;
	Scene*					mScene;
	Light*					mLight;

	// ray trace optimised structs
	RScene					mRScene;

protected:

};

#endif
