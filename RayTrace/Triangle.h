#ifndef _TRIANGLE_H_
#define _TRIANGLE_H_

#include "Math/Plane.h"

class Triangle
{
public:

	Triangle()
	{
	}

	void Create(const Vector3& p0, const Vector3& p1, const Vector3& p2, const Matrix4& transform);

	Vector3	mPosition[3];
	Vector3	mNormal;
	Plane	mPlane;
};

#endif