#include "StdAfx.h"

void RScene::Init(Scene* scene)
{
	Init(scene->GetRootNode());
}

void RScene::Init(SceneNode* node)
{
	SceneObject* obj = node->GetObject();
	if (obj)
	{
		switch (obj->GetType())
		{
		case Mesh::Type:
			Init(static_cast<Mesh*>(obj), node);
			break;
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
	{
		Init(*it);
	}
}

void RScene::Init(Mesh* mesh, SceneNode* node)
{
	RMesh* rMesh = new RMesh();
	rMesh->Init(mesh, node);
	mMesh.push_back(rMesh);
}

void RScene::Deinit()
{

}