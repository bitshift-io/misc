#ifndef _CMD_LINE_H_
#define _CMD_LINE_H_

#include <string>
#include <vector>

using namespace std;

typedef vector<pair<string, string> > TokenPair;

class CmdLine
{
public:

	CmdLine(const string& cmdLine)
	{
		GenerateTokenPairs(cmdLine);
	}

	bool GetToken(const string& name, int& value) const
	{
		string str;
		if (!GetToken(name, str))
			return false;

		const char* cStr = str.c_str();
		sscanf(cStr, "%i", &value);
		return true;
	}

	bool GetToken(const string& name, string& str) const
	{
		TokenPair::const_iterator it;
		for (it = mTokens.begin(); it != mTokens.end(); ++it)
		{
			if (name.compare(it->first) == 0)
			{
				str = it->second;
				return true;
			}
		}

		return false;
	}

protected:

	void GenerateTokenPairs(const string& cmdLine)
	{
		vector<string> tokens = Tokenize(cmdLine, "-");
		vector<string>::iterator it;
		for (it = tokens.begin(); it != tokens.end(); ++it)
		{
			vector<string> tokenCmd = Tokenize(*it, " ");

			if (tokenCmd.size() >= 2)
				mTokens.push_back(pair<string, string>(tokenCmd[0], tokenCmd[1]));
			else if (tokenCmd.size() >= 1)
				mTokens.push_back(pair<string, string>(tokenCmd[0], ""));
		}
	}

	vector<string> Tokenize(const string& str, const string& delimiters)
	{
		vector<string> tokens;
	    	
		// skip delimiters at beginning.
    	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	    	
		// find first "non-delimiter".
		string::size_type pos = str.find_first_of(delimiters, lastPos);

		while (string::npos != pos || string::npos != lastPos)
		{
    		// found a token, add it to the vector.
    		tokens.push_back(str.substr(lastPos, pos - lastPos));
		
    		// skip delimiters.  Note the "not_of"
    		lastPos = str.find_first_not_of(delimiters, pos);
		
    		// find next "non-delimiter"
    		pos = str.find_first_of(delimiters, lastPos);
		}

		return tokens;
	}

	TokenPair	mTokens;
};

#endif