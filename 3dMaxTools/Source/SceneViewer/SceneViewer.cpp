#include "PhysX/PhysicsFactory.h"
#include "PhysX/PhysicsScene.h"
#include "PhysX/PhysicsBody.h"
#include "SceneViewer.h"
#include "Render/Window.h"
#include "Render/Device.h"
#include "Render/Geometry.h"
#include "Render/Spline.h"
#include "Render/OGL/OGLRenderFactory.h"
#include "Render/Camera.h"
#include "Render/Light.h"
#include "Render/Renderer.h"
#include "Input/Input.h"
#include "FreeCamera.h"
#include "CmdLine.h"
#include "Memory/Memory.h"
#include "Time/Time.h"

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>

#undef GetClassInfo
#undef CreateWindow
#undef GetObject

#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include "Resource.h"

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	char cmdLine[256];
	WideCharToMultiByte(CP_ACP, 0, lpCmdLine, -1, cmdLine, 256, NULL, NULL);
	return SceneViewer::Main(cmdLine);
}

void SceneViewer::Init(const CmdLine& cmd)
{
	//MemTracker::SetBreakOnAllocNumber(43);

	RenderFactoryParameter params;

	string basePath;
	cmd.GetToken("basepath", basePath);
	params.path.mTexture = "texture";
	params.path.mScene = "scene";
	params.path.mMaterial = "material";
	params.path.mEffect = "effect";
	params.path.mBasePath = basePath;
	mRenderFactory = new OGLRenderFactory(params);
	mWindow = mRenderFactory->CreateWindow();
	mDevice = mRenderFactory->CreateDevice();
	mCamera = (Camera*)mRenderFactory->Create(Camera::Type);

	int width = 800;
	int height = 600;
	cmd.GetToken("width", width);
	cmd.GetToken("height", height);
	mWindow->Init("Scene Viewer", width, height);
	mDevice->Init(mWindow);

	mInput = new Input();
	mInput->Init(mWindow, true);

	mWindow->Show();
	mFreeCamera.Init(mCamera, mInput);

	mScene = mRenderFactory->CreateScene();

	string scene;
	cmd.GetToken("scene", scene);
	mScene->Load(mRenderFactory, mDevice, scene);

	mRenderer = mRenderFactory->CreateRenderer();
	mRenderer->SetCamera(mCamera);
	mRenderer->InsertScene(mScene);
	mCameraIdx = -1;

	// physics
	mPhysicsFactory = new PhysicsFactory;
	mPhysicsFactory->Init();

	mPhysicsScene = mPhysicsFactory->CreatePhysicsScene();
	mPhysicsScene->Init();

	mBox = mPhysicsFactory->CreatePhysicsBody();
	PhysicsBodyConstructor boxConst;
	boxConst.AddBox();
	boxConst.SetDynamicBody();
	mBox->Init(mPhysicsScene, boxConst);

	mPlane = mPhysicsFactory->CreatePhysicsBody();
	PhysicsBodyConstructor planeConst;
	planeConst.AddPlane();
	mPlane->Init(mPhysicsScene, planeConst);
}

void SceneViewer::Deinit()
{
	// physics
	mPhysicsScene->Deinit();
	mPhysicsFactory->ReleasePhysicsScene(&mPhysicsScene);
	mPhysicsFactory->Deinit();
	delete mPhysicsFactory;


	mInput->Deinit();
	delete mInput;

	mRenderFactory->ReleaseScene(&mScene);
	mRenderFactory->ReleaseRenderer(&mRenderer);
	mRenderFactory->Release((SceneObject**)&mCamera);

	mRenderFactory->ReleaseWindow(&mWindow);
	mRenderFactory->ReleaseDevice(&mDevice);
	delete mRenderFactory;
}

bool SceneViewer::Update()
{
	// update physics
	mPhysicsScene->Update();

	if (mInput->KeyDown(KEY_Up))
		mBox->ApplyAngularForce(Vector3(0.f, 0.f, 1.f) * 1000.f, PhysicsBody::Local);

	if (mInput->KeyDown(KEY_Down))
		mBox->ApplyAngularForce(Vector3(0.f, 0.f, -1.f) * 1000.f, PhysicsBody::Local);

	if (mInput->KeyDown(KEY_Left))
		mBox->ApplyAngularForce(Vector3(0.f, -1.f, 0.f) * 1000.f);

	if (mInput->KeyDown(KEY_Right))
		mBox->ApplyAngularForce(Vector3(0.f, 1.f, 0.f) * 1000.f);

	// update rendering, and input
	if (mCameraIdx == -1)
		mFreeCamera.Update();

	mDevice->Begin();
	mCamera->Bind(mDevice);

	mPhysicsScene->Draw(mDevice);

	mRenderer->Render(mDevice, false);

	mRenderer->GetCamera()->Bind(mDevice);
	mDevice->DrawMatrix(Matrix4(MI_Identity));

	bool found = false;
	int cameraIdx = 0;
	Scene::Iterator it;
	for (it = mScene->Begin(); it != mScene->End(); ++it)
	{
		SceneNode* node = *it;
		SceneObject* object = node->GetObject();
		if (!object)
			continue;

		switch (object->GetType())
		{
		case Spline::Type:
			{
				Spline* spline = static_cast<Spline*>(object);
				spline->Draw(mDevice);
			}
			break;

		case Light::Type:
			{
				Light* light = static_cast<Light*>(object);
				light->Draw(mDevice);
			}
			break;

		case Camera::Type:
			{
				Camera* camera = static_cast<Camera*>(object);
				camera->Draw(mDevice);

				if (mCameraIdx == cameraIdx)
				{
					mRenderer->SetCamera(camera);
					found = true;
				}

				++cameraIdx;
			}
			break;
		}
	}

	if (!found)
	{
		mCameraIdx = -1;
		mRenderer->SetCamera(mCamera);
	}

	if (mInput->KeyPressed(KEY_c))
	{
		++mCameraIdx;
	}

	mDevice->End();
	return true;
}

int SceneViewer::Main(const char* cmdLine)
{
	SceneViewer scene;
	scene.Init(CmdLine(cmdLine));

	while (scene.Update())
	{
		if (scene.mWindow->HandleMessages())
			break;

		scene.mInput->Update();
	}

	scene.Deinit();
	return 0;
}