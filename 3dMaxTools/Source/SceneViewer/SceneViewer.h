#ifndef _SCENEVIEWER_H_
#define _SCENEVIEWER_H_

#include "FreeCamera.h"

class RenderFactory;
class Window;
class Device;
class Scene;
class Camera;
class Input;
class Renderer;
class CmdLine;
class PhysicsFactory;
class PhysicsScene;

//
// Load and display the scene
//
class SceneViewer
{
public:
	
	void Init(const CmdLine& cmd);
	void Deinit();
	bool Update();

	static int Main(const char* cmdLine);

protected:

	RenderFactory*	mRenderFactory;
	Window*			mWindow;
	Device*			mDevice;
	Scene*			mScene;
	Camera*			mCamera;
	Input*			mInput;
	FreeCamera		mFreeCamera;
	Renderer*		mRenderer;
	int				mCameraIdx;

	PhysicsFactory*	mPhysicsFactory;
	PhysicsScene*	mPhysicsScene;
	PhysicsBody*	mBox;
	PhysicsBody*	mPlane;
};

#endif
