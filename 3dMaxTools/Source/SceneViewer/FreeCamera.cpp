#include "FreeCamera.h"
#include "Render/Camera.h"
#include "Input/Input.h"

void FreeCamera::Init(Camera* camera, Input* input)
{
	mCamera = camera;
	mInput = input;
	mRotation.SetZero();
	mSensitivity = 10.f;
	mSpeed = 0.01f;
}


void FreeCamera::Update()
{
	int x = 0;
	int y = 0;
	mInput->GetMouseState(x, y);

	mRotation.yaw -= (float)x;
	mRotation.pitch -= (float)y;

	float invSensitivity = 1000.0f / mSensitivity;
	Matrix4 m(MI_Identity);
	Matrix4 m2(MI_Identity);
	m.SetRotateY(mRotation.yaw / invSensitivity);
	m2.SetRotateX(mRotation.pitch / invSensitivity);
	m = m2 * m;

	Vector3 up = m.Column(1);
	Vector3 view = m.Column(2);
	Vector3 right = m.Column(0);
	Vector3 position = mCamera->GetPosition();

	if (mInput->KeyDown(KEY_w))
		position = position + (view * mSpeed);

	if (mInput->KeyDown(KEY_s))
		position = position - (view * mSpeed);

	if (mInput->KeyDown(KEY_a))
		position = position - (right * mSpeed);

	if (mInput->KeyDown(KEY_d))
		position = position + (right * mSpeed);

	if (mInput->KeyDown(KEY_Space))
		position.y += mSpeed;

	if (mInput->KeyDown(KEY_LControl))
		position.y -= mSpeed;

	if (mInput->KeyPressed(KEY_2))
		mSpeed *= 2.f;

	if (mInput->KeyPressed(KEY_1))
		mSpeed *= 0.5f;

	mCamera->LookAt(view, up, right);
	mCamera->SetPosition(position);
}

void FreeCamera::SetCamera(Camera* camera)
{
	if (mCamera == camera)
		return;

	mCamera = camera;
	mRotation.SetZero();
}