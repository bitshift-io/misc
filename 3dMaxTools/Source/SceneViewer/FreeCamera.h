#ifndef _FREECAMERA_H_
#define _FREECAMERA_H_

#include "Math/Vector.h"

class Camera;
class Input;

class FreeCamera
{
public:

	void		Init(Camera* camera, Input* input);
	void		Update();
	void		SetCamera(Camera* camera);

protected:

	Vector3		mRotation;
	Camera*		mCamera;
	Input*		mInput;
	float		mSensitivity;
	float		mSpeed;
};

#endif