#ifndef _HELPER_H_
#define _HELPER_H_

#include "PCH.h"
#include "Math/Vector.h"
#include "Math/Matrix.h"

#define METERS_TO_MAX	39.3700787f // convert inches to meters
#define MAX_TO_METERS	1.0f / METERS_TO_MAX
#define MAX_TO_SECONDS  1.0f / (float)(gExporter->mIGameScene->GetSceneTicks() * GetFrameRate())

class Helper
{
public:

	static Matrix4 ConvertMatrix4(const GMatrix& matrix, bool convertToMetres = true)
	{
		Matrix4 m(MI_Identity);

		Point4 p = matrix.GetRow(0);
		m[0] = p[0];
		m[1] = p[1];
		m[2] = p[2];

		p = matrix.GetRow(1);
		m[4] = p[0];
		m[5] = p[1];
		m[6] = p[2];

		p = matrix.GetRow(2);
		m[8] = p[0];
		m[9] = p[1];
		m[10] = p[2];

		p = matrix.GetRow(3);

		DispInfo info;
		GetUnitDisplayInfo(&info);
		if (info.dispType == UNITDISP_METRIC)
		{
			if (convertToMetres)
				p = p * MAX_TO_METERS;
		}

		m[12] = p[0];
		m[13] = p[1];
		m[14] = p[2];

		return m;
	}

	static Vector4 ConvertVector4(const Point4& point, bool convertToMetres = false)
	{
		Vector4 v;
		v.x = point.x;
		v.y = point.y;
		v.z = point.z;
		v.w = point.w;

		DispInfo info;
		GetUnitDisplayInfo(&info);
		if (info.dispType == UNITDISP_METRIC)
		{
			if (convertToMetres)
				v *= MAX_TO_METERS;
		}

		return v;
	}

	static Vector4 ConvertVector4(const Point3& point, bool convertToMetres = false)
	{
		Vector4 v;
		v.x = point.x;
		v.y = point.y;
		v.z = point.z;
		
		DispInfo info;
		GetUnitDisplayInfo(&info);
		if (info.dispType == UNITDISP_METRIC)
		{
			if (convertToMetres)
				v *= MAX_TO_METERS;
		}

		v.w = 1.f;
		return v;
	}
};

#endif