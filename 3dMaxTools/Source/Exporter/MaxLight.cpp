#include "MaxLight.h"
#include "Render/Light.h"
#include "Exporter.h"

MaxLight::MaxLight(IGameNode* pGameNode) : MaxObject(pGameNode)
{
}

bool MaxLight::Convert(Light* light)
{
	IGameLight* pGameLight = static_cast<IGameLight*>(mGameObject);
	pGameLight->InitializeData();

	if (!pGameLight->GetLightColor())
	{
		gExporter->Log("Not exported (no colour)");
		return false;
	}

	switch (pGameLight->GetLightType())
	{
	case IGameLight::IGAME_TSPOT:
	case IGameLight::IGAME_FSPOT:
		light->SetLightType(LT_Spot);
		break;

	case IGameLight::IGAME_TDIR:
	case IGameLight::IGAME_DIR:
		light->SetLightType(LT_Directional);
		break;

	default:
	case IGameLight::IGAME_OMNI:
		light->SetLightType(LT_Omni);
		break;
	}

	Point3 colour;
	pGameLight->GetLightColor()->GetPropertyValue(colour);
	light->SetDiffuseColour(Helper::ConvertVector4(colour));

	float brightness;
	pGameLight->GetLightMultiplier()->GetPropertyValue(brightness);
	light->SetBrightness(brightness);

	light->SetDecayType((ELightDecayType)pGameLight->GetLightDecayType());

	GMatrix nodeMatrix = mGameNode->GetWorldTM();
	Vector4 position = Helper::ConvertVector4(nodeMatrix.Translation(), true);
	Vector4 target(-nodeMatrix.GetColumn(2).x, -nodeMatrix.GetColumn(2).z, -nodeMatrix.GetColumn(2).y); // st0pid max
	if (pGameLight->GetLightTarget())
		target = Helper::ConvertVector4(pGameLight->GetLightTarget()->GetWorldTM().Translation(), true) - position;

	target.Normalize();

	light->SetPosition(position);
	light->SetDirection(target);
	return true;
}