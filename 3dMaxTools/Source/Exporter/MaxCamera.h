#ifndef _MAX_CAMERA_H_
#define _MAX_CAMERA_H_

#include "MaxObject.h"

class Camera;

class MaxCamera : public MaxObject
{
public:

	MaxCamera(IGameNode* pGameNode);

	bool	Convert(Camera* camera);
};

#endif