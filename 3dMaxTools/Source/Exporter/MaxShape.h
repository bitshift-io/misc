#ifndef _MAXSHAPE_H_
#define _MAXSPAHE_H_

#include "MaxObject.h"

class Shape;

class MaxShape : public MaxObject
{
public:

	MaxShape(IGameNode* pGameNode);

	bool	Convert(Shape* shape);
};

#endif