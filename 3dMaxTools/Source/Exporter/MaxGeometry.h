#ifndef _MAX_GEOMETRY_H_
#define _MAX_GEOMETRY_H_

#include "MaxObject.h"
#include <vector>
#include <hash_map>

using namespace std;
using namespace stdext;

class Geometry;
class MaxMesh;

class Vertex
{
public:

	Vertex()
	{
		mPointSet = false;
		mNormalSet = false;
		mTangentSet = false;
		mBinormalSet = false;
		mColourSet = false;
		mSkinSet = false;

		for (int i = 0; i < 4; ++i)
			mTexCoordSet[i] = false;
	}

	bool Equals(const Vertex& v2) const
	{
		const Vertex& v1 = (*this);

		// if arrays are not the same size, return false
		if (!(v1.mPointSet == v2.mPointSet
		   && v1.mNormalSet == v2.mNormalSet
		   && v1.mTangentSet == v2.mTangentSet
		   && v1.mBinormalSet == v2.mBinormalSet
		   && v1.mTexCoordSet[0] == v2.mTexCoordSet[0]
			&& v1.mTexCoordSet[1] == v2.mTexCoordSet[1]
			&& v1.mTexCoordSet[2] == v2.mTexCoordSet[2]
			&& v1.mTexCoordSet[3] == v2.mTexCoordSet[3]
		   && v1.mColourSet == v2.mColourSet))
			return false;

		if (v1.mPointSet && v1.mPoint != v2.mPoint)
			return false;

		if (v1.mNormalSet && v1.mNormal != v2.mNormal)
			return false;

		for (int i = 0; i < 4; ++i)
		{
			if (v1.mTexCoordSet[i] && v1.mTexCoord[i] != v2.mTexCoord[i])
				return false;
		}

		if (v1.mColourSet && v1.mColour != v2.mColour)
			return false;

		if (v1.mTangentSet && v1.mTangent != v2.mTangent)
			return false;

		if (v1.mBinormalSet && v1.mBinormal != v2.mBinormal)
			return false;

		if (v1.mSkinSet && v1.mBoneWeight != v2.mBoneWeight)
			return false;

		if (v1.mSkinSet && v1.mBoneIndex != v2.mBoneIndex)
			return false;

		return true;
	}

	float Hash() const
	{
		const Vertex& v1 = (*this);

		Vector4 vec(VI_Zero);

		if (v1.mPointSet)
			vec += v1.mPoint;

		if (v1.mNormalSet)
			vec += v1.mNormal;

		for (int i = 0; i < 4; ++i)
		{
			if (v1.mTexCoordSet[i])
				vec += Vector4(v1.mTexCoord[i]);
		}

		if (v1.mColourSet)
			vec += v1.mColour;

		return vec.x + vec.y + vec.z;
	}

	bool mPointSet;
	bool mNormalSet;
	bool mTangentSet;
	bool mBinormalSet;
	bool mColourSet;
	bool mTexCoordSet[4];
	bool mSkinSet;

	Vector4 mPoint;
	Vector4 mNormal;
	Vector4 mTangent;
	Vector4 mBinormal;
	Vector4 mColour;
	Vector4 mTexCoord[4];

	Vector4 mBoneWeight;
	Vector4 mBoneIndex;

	unsigned int		mIndex; // index this is used by the hash table
};

typedef hash_multimap<float, Vertex> VertexHash;

struct VertexHashFn
{
	float operator()(const Vertex& v1)
	{/*
		Vector4 vec(VI_Zero);

		for (int i = 0; i < v1.mPoint.size(); ++i)
			vec += v1.mPoint[i];

		for (int i = 0; i < v1.mNormal.size(); ++i)
			vec += v1.mNormal[i];

		for (int i = 0; i < v1.mTexCoord.size(); ++i)
			vec += Vector4(v1.mTexCoord[i]);

		for (int i = 0; i < v1.mColour.size(); ++i)
			vec += v1.mColour[i];

		return vec.x + vec.y + vec.z;*/

		return v1.Hash();
	}
};

struct VertexEqualKey
{
	bool operator()(const Vertex& v1, const Vertex& v2) const
	{/*
		// if arrays are not the same size, return false
		if (!(v1.mPoint.size() == v2.mPoint.size()
		   && v1.mNormal.size() == v2.mNormal.size()
		   && v1.mTangent.size() == v2.mTangent.size()
		   && v1.mBinormal.size() == v2.mBinormal.size()
		   && v1.mTexCoord.size() == v2.mTexCoord.size()
		   && v1.mColour.size() == v2.mColour.size()))
			return false;

		for (int i = 0; i < v1.mPoint.size(); ++i)
		{
			if (v1.mPoint[i] != v2.mPoint[i])
				return false;
		}

		for (int i = 0; i < v1.mNormal.size(); ++i)
		{
			if (v1.mNormal[i] != v2.mNormal[i])
				return false;
		}

		for (int i = 0; i < v1.mTexCoord.size(); ++i)
		{
			if (v1.mTexCoord[i] != v2.mTexCoord[i])
				return false;
		}

		for (int i = 0; i < v1.mColour.size(); ++i)
		{
			if (v1.mColour[i] != v2.mColour[i])
				return false;
		}

		for (int i = 0; i < v1.mTangent.size(); ++i)
		{
			if (v1.mTangent[i] != v2.mTangent[i])
				return false;
		}

		for (int i = 0; i < v1.mBinormal.size(); ++i)
		{
			if (v1.mBinormal[i] != v2.mBinormal[i])
				return false;
		}

		return true;*/

		return v1.Equals(v2);
	}
};

class MaxGeometry : public MaxObject
{
public:

	MaxGeometry(IGameNode* pGameNode, MaxMesh* maxMesh);

	bool	Convert(Geometry* geometry, IGameMesh* pGameMesh, const Tab<FaceEx*>& faces, int vertexFormat, bool localSpace, IGameSkin* pSkin);

protected:

	void			ConvertFaces(Geometry* geometry, IGameMesh* pGameMesh, const Tab<FaceEx*>& faces, unsigned int vertexFormat, IGameSkin* pSkin);
	unsigned int	GetIndex(Geometry* geometry, const Vertex& vertex);
	unsigned int 	InsertVertex(Geometry* geometry, const Vertex& vertex);
	void			InsertIndex(Geometry* geometry, unsigned int index);
	void			ConvertToLocal(Geometry* geometry);

	VertexHash		mVertexHash; //VertexHashFn> test;//, VertexEqualKey> mVertexHash;
	MaxMesh*		mMaxMesh;
};

#endif