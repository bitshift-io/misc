#include "MaxObject.h"

MaxObject::MaxObject(IGameNode* pGameNode)
{
	mGameNode	= pGameNode;
	mGameObject = pGameNode->GetIGameObject();
}

MaxObject::~MaxObject()
{
	if (mGameNode)
		mGameNode->ReleaseIGameObject();
}
