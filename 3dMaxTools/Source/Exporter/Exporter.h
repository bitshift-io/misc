#ifndef _EXPORTER_H_
#define _EXPORTER_H_

#include "PCH.h"
#include <vector>

using namespace std;

#define EXPORTER_CLASS_ID	Class_ID(0x19c02652, 0x2746b2a6)
#define gExporter			Exporter::sInstance

class RenderFactory;
class SceneNode;
class SceneObject;
class Scene;
class Animation;
class Device;
class Property;

//=========================================================================
// Exporter
//=========================================================================
class Exporter : public SceneExport
{
public:

	Exporter();
	~Exporter();

	int				ExtCount()					{ return 1; }
	const TCHAR*	Ext(int n);
	const TCHAR*	LongDesc()					{ return _T("Exporter"); }
	const TCHAR*	ShortDesc()					{ return _T("Exporter"); }
	const TCHAR*	AuthorName()				{ return _T("Fabian Mathews"); }
	const TCHAR*	CopyrightMessage()			{ return _T(""); }
	const TCHAR*	OtherMessage1()				{ return _T(""); }
	const TCHAR*	OtherMessage2()				{ return _T(""); }
	unsigned int	Version()					{ return 100; }
	void			ShowAbout(HWND hWnd)		{ }

	BOOL			SupportsOptions(int ext, DWORD options)		{ return TRUE; }

	int				DoExport(const TCHAR* name, ExpInterface* ei, Interface* i, BOOL suppressPrompts = FALSE, DWORD options = 0);
	bool			ProcessNodes();
	bool			ProcessNode(IGameNode* pGameNode, SceneNode* pSceneNode);
	bool			ProcessNodeProperties(IGameNode* pGameNode, SceneNode* pSceneNode);
	bool			ProcessNodeControl(IGameNode* pGameNode, SceneNode* pSceneNode);
	bool			ProcessParticleSystems();
	bool			IgnoreNode(IGameNode* pGameNode);

	SceneObject*	ProcessMesh(IGameNode* pGameNode);
	SceneObject*	ProcessHelper(IGameNode* pGameNode);
	SceneObject*	ProcessLight(IGameNode* pGameNode);
	SceneObject*	ProcessSpline(IGameNode* pGameNode);
	SceneObject*	ProcessCamera(IGameNode* pGameNode);
	SceneObject*	ProcessBone(SceneNode* pSceneNode, IGameNode* pGameNode);
	SceneObject*	ProcessParticleSystem(IGameNode* pGameNode);

	bool			ExportSpecificProperty(const Property& property);
	
	bool			ReadConfig();
	bool			WriteConfig();
	TSTR			GetCfgFilename();
	void			Log(const char* pText);

	HWND			mLog;
	HWND			mProgress;
	HWND			mDialog;
	IGameScene*		mIGameScene;

	const TCHAR*	mName;
	ExpInterface*	mExpInterface;
	Interface*		mInterface;
	bool			mSuppressPrompts;
	DWORD			mOptions;
	bool			mExportSelected;
	RenderFactory*	mRenderFactory;
	Scene*			mScene;
	Animation*		mAnimation;
	Device*			mDevice;

	static Exporter*	sInstance;
};

//=========================================================================
// ExporterClassDesc
//=========================================================================
class ExporterClassDesc : public ClassDesc2
{
	public:
	int 			IsPublic()						{ return TRUE; }
	void*			Create(BOOL loading = FALSE)	{ return new Exporter(); }
	const TCHAR*	ClassName()						{ return GetString(STR_CLASS_NAME); }
	SClass_ID		SuperClassID()					{ return SCENE_EXPORT_CLASS_ID; }
	Class_ID		ClassID()						{ return EXPORTER_CLASS_ID; }
	const TCHAR* 	Category()						{ return GetString(STR_CATEGORY); }

	const TCHAR*	InternalName()					{ return _T("Exporter"); }				// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance()						{ return hInstance; }					// returns owning module handle
	virtual char*	GetRsrcString(long t)			{ return NULL; }
};

#endif
