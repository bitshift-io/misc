#ifndef _MAX_MESH_H_
#define _MAX_MESH_H_

#include "Math/Box.h"
#include "MaxObject.h"
#include <string>
#include <vector>

using namespace std;

class Geometry;
class Material;

struct MaxBone
{
	IGameNode*	gameNode;
	string		boneName;
	Matrix4		defaultPose;
	Matrix4		skinToBone;
	Box			bounds;
	int			parentIdx;
};

class MaxMesh : public MaxObject
{
public:

	MaxMesh(IGameNode* pGameNode);

	bool		Convert(Mesh* mesh);

	// skinning stuffs
	bool		IsSkinned();

	// given a bone, we can calculate its index if this skeleton has been updated
	void		ProcessSkin(Mesh* mesh);
	int			GetBoneIdx(IGameNode* bone);
	void		AddBone(IGameNode* bone, IGameNode* parent, int parentIdx);
	static bool IsBone(IGameNode* bone);

protected:

	Geometry* CreateGeometry(IGameMesh* pGameMesh, const Tab<FaceEx*>& faces, int vertexFormat, bool localSpace, IGameSkin* pSkin);
	Material* CreateMaterial(IGameMaterial* pGameMaterial);

	unsigned int GetVertexFormat(IGameMesh* pGameMesh);
	int			GetFlags();

	vector<MaxBone*> mGameBones;
};

#endif
