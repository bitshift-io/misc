#include "MaxParticleSystem.h"
#include "MaxGeometry.h"
#include "Exporter.h"
#include "Render/RenderFactory.h"
#include "Render/Mesh.h"
#include "Render/Geometry.h"
#include "Render/Material.h"
#include "Render/Export/ExpRenderBuffer.h"
#include "Render/ParticleSystem.h"
#include "MaxMaterial.h"
#include "IGame/IGame.h"

#include "PFlow/PFOperatorSimpleBirth_ParamBlock.h"
#include "PFlow/PFOperatorSimpleSpeed_ParamBlock.h"
#include "PFlow/PFOperatorPositionOnObject_ParamBlock.h"
#include "PFlow/PFOperatorSimplePosition_ParamBlock.h"
#include "PFlow/PFOperatorExit_ParamBlock.h"
#include "PFlow/PFOperatorForceSpaceWarp_ParamBlock.h"

MaxParticleSystem::MaxParticleSystem(IGameNode* pGameNode) : MaxObject(pGameNode)
{
}

bool MaxParticleSystem::Convert(ParticleActionGroup* actionGroup, Scene* scene)
{/*
	INode* pMaxNode = mGameNode->GetMaxNode();
	ObjectState os = pMaxNode->EvalWorldState(0);
	Object* obj = os.obj;
	ParticleObject* particleObj = GetParticleInterface(obj);
	Object* pFlowObj = GetPFObject(obj);
	if (pFlowObj != 0)
		int nothing = 0;

	IParticleGroup* groupInterface = GetParticleGroupInterface(obj);
	if (groupInterface)
	{
		INode* n = groupInterface->GetParticleSystem();
		int nothing = 0;
	}

	IPFActionList* actionList = GetPFActionListInterface(obj);

	IPFSystem* system = GetPFSystemInterface(obj); 
* /
	IPFSystemPool* systemPool = GetPFSystemPool();
	for (int i = 0; i < systemPool->NumPFSystems(); ++i)
	{
		INode* n = systemPool->GetPFSystem(i);
		const char* nodeName = n->GetName();

		IPFSystem* system = PFSystemInterface(n);
/*
		ObjectState os = n->EvalWorldState(0);
		Object* obj = os.obj;
		IPFSystem* system = GetPFSystemInterface(obj); 
/*
			enum {	kEmitterType_none = -1,
			kEmitterType_rectangle = 0,
			kEmitterType_circle,
			kEmitterType_box,
			kEmitterType_sphere,
			kEmitterType_mesh
	};
			* /
		int emitterType = system->GetEmitterType(0);
		if (emitterType == IPFSystem::kEmitterType_mesh)
		{
			// we should simply make this a pointer to a scene node...
			Mesh* emitterMesh = system->GetEmitterGeometry(0);
		}

		for (int j = 0; j < system->NumActionListsSelected(); ++j)
		{
			INode* actionListNode = system->GetSelectedActionList(j);
			const char* nodeName = actionListNode->GetName();

			IPFActionList* actionList = PFActionListInterface(actionListNode);
			
			for (int k = 0; k < actionList->NumActions(); ++k)
			{
				INode* actionNode = actionList->GetAction(k);
				const char* actionNodeName = actionListNode->GetName();
				int nothing = 0;
			}
		}


		ProcessChild(n);
	}*/

	//IPFActionListPool* actionListPool = GetPFActionListPool();
	//for (int i = 0; i < actionListPool->NumActionLists(); ++i)
	//{
		INode* actionListNode = mGameNode->GetMaxNode(); //actionListPool->GetActionList(i);
		const char* nodeName = actionListNode->GetName();
		IPFActionList* actionList = PFActionListInterface(actionListNode);

		//INode* actionListParentNode = actionListNode->GetParentNode();
		//const char* parentNodeName = actionListParentNode->GetName();

		//ParticleActionGroup* actionGroup = system->CreateActionGroup();
		//system->AddActionGroup(actionGroup);
		//actionGroup->SetName(nodeName);
		
		for (int k = 0; k < actionList->NumActions(); ++k)
		{
			INode* actionNode = actionList->GetAction(k);
			IPFAction* action = PFActionInterface(actionNode);

			ObjectState os = actionNode->EvalWorldState(0);
			Object* obj = os.obj;

			Class_ID classId = obj->ClassID();

			if (classId == PFOperatorSimpleBirth_Class_ID)
			{
				for (int l = 0; l < obj->NumParamBlocks(); ++l)
				{
					IParamBlock2* pBlock = ((Animatable*)obj)->GetParamBlock(l);

					int start = -1;
					int finish = -1;
					int type = -1;
					float rate = -1;
					int amount = -1;
					pBlock->GetValue(PFActions::kSimpleBirth_start, 0, start, FOREVER);
					pBlock->GetValue(PFActions::kSimpleBirth_finish, 0, finish, FOREVER);
					pBlock->GetValue(PFActions::kSimpleBirth_type, 0, type, FOREVER);
					pBlock->GetValue(PFActions::kSimpleBirth_amount, 0, amount, FOREVER);
					pBlock->GetValue(PFActions::kSimpleBirth_rate, 0, rate, FOREVER);

					ParticleBirth* birth = (ParticleBirth*)actionGroup->CreateAction(ParticleBirth::Type);
					switch (type)
					{
					case PFActions::kSimpleBirth_type_rate:
						birth->mType = BT_Rate;
						birth->mRate = rate;
						break;
					case PFActions::kSimpleBirth_type_amount:
						birth->mType = BT_Amount;
						birth->mAmount = amount;
					}

					birth->mStartTime = float(start) * MAX_TO_SECONDS;
					birth->mEndTime = float(finish) * MAX_TO_SECONDS;

					actionGroup->AddAction(birth);
				}
			}

			else if (classId == PFOperatorExit_Class_ID)
			{
				for (int l = 0; l < obj->NumParamBlocks(); ++l)
				{
					IParamBlock2* pBlock = ((Animatable*)obj)->GetParamBlock(l);

					int type = -1;
					int lifeSpan = -1;
					int variation;
					pBlock->GetValue(PFActions::kExit_type, 0, type, FOREVER);
					pBlock->GetValue(PFActions::kExit_lifeSpan, 0, lifeSpan, FOREVER);
					pBlock->GetValue(PFActions::kExit_variation, 0, variation, FOREVER);
					pBlock->GetValue(PFActions::kExit_variation, 0, variation, FOREVER);

					ParticleDeath* death = (ParticleDeath*)actionGroup->CreateAction(ParticleDeath::Type);
					actionGroup->AddAction(death);

					death->mLife = lifeSpan * MAX_TO_SECONDS;
					death->mVariation = variation * MAX_TO_SECONDS;

					switch (type)
					{
					case PFActions::kExit_type_all:
						death->mType = DT_All;
						break;

					case PFActions::kExit_type_byAge:
						death->mType = DT_Life;
						break;
					}
				}
			}

			else if (classId == PFOperatorPositionOnObject_Class_ID)
			{
				gExporter->Log("PFLOW: Position on Object not yet supported");
			}

			else if (classId == PFOperatorSimplePosition_Class_ID)
			{
				for (int l = 0; l < obj->NumParamBlocks(); ++l)
				{
					IParamBlock2* pBlock = ((Animatable*)obj)->GetParamBlock(l);

					int lock;
					int inherit;
					float inheritAmount;
					int type;
					pBlock->GetValue(PFActions::kSimplePosition_lock, 0, lock, FOREVER);
					pBlock->GetValue(PFActions::kSimplePosition_inherit, 0, inherit, FOREVER);
					pBlock->GetValue(PFActions::kSimplePosition_inheritAmount, 0, inheritAmount, FOREVER);
					pBlock->GetValue(PFActions::kSimplePosition_type, 0, type, FOREVER);

					ParticlePosition* particlePos = (ParticlePosition*)actionGroup->CreateAction(ParticlePosition::Type);
					actionGroup->AddAction(particlePos);
					particlePos->mInheritSpeedMovementPercent = inheritAmount;

					switch (type)
					{
					case PFActions::kSimplePosition_locationType_vertex:
						particlePos->mPositionType = PT_Vertex;
						break;
					case PFActions::kSimplePosition_locationType_edge:
						particlePos->mPositionType = PT_Edge;
						break;
					case PFActions::kSimplePosition_locationType_volume:
					case PFActions::kSimplePosition_locationType_surface:
						particlePos->mPositionType = PT_Surface;
						break;
					case PFActions::kSimplePosition_locationType_pivot:
						particlePos->mPositionType = PT_Centre;
						break;
					}
				}
			}

			else if (classId == PFOperatorSimpleSpeed_Class_ID)
			{
				for (int l = 0; l < obj->NumParamBlocks(); ++l)
				{
					IParamBlock2* pBlock = ((Animatable*)obj)->GetParamBlock(l);

					float speed;
					float reverse;
					float variation;
					int direction;
					float divergence;
					pBlock->GetValue(PFActions::kSimpleSpeed_speed, 0, speed, FOREVER);
					pBlock->GetValue(PFActions::kSimpleSpeed_reverse, 0, reverse, FOREVER);
					pBlock->GetValue(PFActions::kSimpleSpeed_variation, 0, variation, FOREVER);
					pBlock->GetValue(PFActions::kSimpleSpeed_direction, 0, direction, FOREVER);
					pBlock->GetValue(PFActions::kSimpleSpeed_divergence, 0, divergence, FOREVER);

					ParticleSpeed* particleSpeed = (ParticleSpeed*)actionGroup->CreateAction(ParticleSpeed::Type);
					actionGroup->AddAction(particleSpeed);

					particleSpeed->mSpeed = speed * MAX_TO_METERS;
					particleSpeed->mVariation = variation * MAX_TO_METERS;
					particleSpeed->mDivergence = divergence;
					switch (direction)
					{
					case PFActions::kSS_Along_Icon_Arrow:
						particleSpeed->mDirection = SD_ZAxis;
						break;
					case PFActions::kSS_Icon_Center_Out:
						particleSpeed->mDirection = SD_XAxis;
						break;
					case PFActions::kSS_Icon_Arrow_Out:
						particleSpeed->mDirection = SD_YAxis;
						break;
					case PFActions::kSS_Rand_3D:
						particleSpeed->mDirection = SD_Random;
						break;
					case PFActions::kSS_Inherit_Prev:
					default:
						particleSpeed->mDirection = SD_Random;
						break;
					}
				}
			}

			else if (classId == PFOperatorSimpleOrientation_Class_ID)
			{
				for (int l = 0; l < obj->NumParamBlocks(); ++l)
				{
					IParamBlock2* pBlock = ((Animatable*)obj)->GetParamBlock(l);
					gExporter->Log("PFLOW: Orientation not yet supported");
				}
			}

			else if (classId == PFOperatorSimpleSpin_Class_ID)
			{
				for (int l = 0; l < obj->NumParamBlocks(); ++l)
				{
					IParamBlock2* pBlock = ((Animatable*)obj)->GetParamBlock(l);
					gExporter->Log("PFLOW: Spin not yet supported");
				}
			}

			else if (classId == PFOperatorSimpleScale_Class_ID)
			{
				for (int l = 0; l < obj->NumParamBlocks(); ++l)
				{
					IParamBlock2* pBlock = ((Animatable*)obj)->GetParamBlock(l);
					gExporter->Log("PFLOW: Scale not yet supported");
				}
			}

			else if (classId == PFOperatorForceSpaceWarp_Class_ID)
			{
				int l = 0;
				//for (int l = 0; l < obj->NumParamBlocks(); ++l)
				{
					IParamBlock2* pBlock = ((Animatable*)obj)->GetParamBlock(l);

					for (int m = 0; m < pBlock->Count(PFActions::kForceSpaceWarp_ForceNodeList); ++m)
					{
						INode* spaceWarp;
						pBlock->GetValue(PFActions::kForceSpaceWarp_ForceNodeList, 0, spaceWarp, FOREVER, m);

						if (!spaceWarp)
							continue;

						const char* warpName = spaceWarp->GetName();

						float influence;
						pBlock->GetValue(PFActions::kForceSpaceWarp_Influence, 0, influence, FOREVER);

						ObjectState warpOS = spaceWarp->EvalWorldState(0);
						Object* warpObj = warpOS.obj;

						for (int n = 0; n < warpObj->NumParamBlocks(); ++n)
						{
							//IParamBlock2* pBlock = warpObj->GetParamBlock();
							IParamBlock2* pBlock = ((Animatable*)warpObj)->GetParamBlock(n);
							
							Class_ID classId = warpObj->ClassID();
							if (classId == Class_ID(GRAVITYOBJECT_CLASS_ID, 0))
							{
								float strength;
								int type;
								float decay;
								float dispLength;

								pBlock->GetValue(GRAVITY_STRENGTH, 0, strength, FOREVER);
								pBlock->GetValue(GRAVITY_TYPE, 0, type, FOREVER);
								pBlock->GetValue(GRAVITY_DECAY, 0, decay, FOREVER);
								pBlock->GetValue(GRAVITY_DISPLENGTH, 0, dispLength, FOREVER);

								// convert strength and decay values from max to my engine
								// see gravity.cpp in max sdk
								float forceScaleFactor = float(1200*1200)/float(4800*4800);
								float conversion = (MAX_TO_METERS * 4800 * 4800) * forceScaleFactor * 0.0001f;
								strength *= conversion;
								decay *= METERS_TO_MAX;

								ParticleForce* particleForce = (ParticleForce*)actionGroup->CreateAction(ParticleForce::Type);
								actionGroup->AddAction(particleForce);

								particleForce->mInfluence = influence;
								particleForce->mStrength = strength;
								particleForce->mDecay = decay;
								particleForce->mType = (ForceType)type;
								particleForce->mNode = scene->GetNodeByName(warpName);
								int nothing = 0;
							}

							else if (classId == Class_ID(WINDOBJECT_CLASS_ID, 0))
							{
								float strength;
								float decay;
								int type;
								float dispLength;
								float turbulence;
								float frequency;
								float scale;

								pBlock->GetValue(WIND_STRENGTH, 0, strength, FOREVER);
								pBlock->GetValue(WIND_TYPE, 0, type, FOREVER);
								pBlock->GetValue(WIND_DECAY, 0, decay, FOREVER);
								pBlock->GetValue(WIND_DISPLENGTH, 0, dispLength, FOREVER);
								pBlock->GetValue(WIND_TURBULENCE, 0, turbulence, FOREVER);
								pBlock->GetValue(WIND_FREQUENCY, 0, frequency, FOREVER);
								pBlock->GetValue(WIND_SCALE, 0, scale, FOREVER);

								// convert strength and decay values from max to my engine
								// see wind.cpp in max sdk
								float forceScaleFactor = float(1200*1200)/float(4800*4800);
								float conversion = (MAX_TO_METERS * 4800 * 4800) * forceScaleFactor * 0.0001f;
								strength *= conversion;
								decay *= METERS_TO_MAX;

								// adjust turb. and freq. according to wind.cpp
								turbulence *= conversion;
								frequency *= 0.01f;
								//scale *= METERS_TO_MAX;

								ParticleWind* particleWind = (ParticleWind*)actionGroup->CreateAction(ParticleWind::Type);
								actionGroup->AddAction(particleWind);

								particleWind->mInfluence = influence;
								particleWind->mStrength = strength;
								particleWind->mDecay = decay;
								particleWind->mType = (ForceType)type;
								particleWind->mTurbulence = turbulence;
								particleWind->mFrequency = frequency;
								particleWind->mScale = scale;
								particleWind->mNode = scene->GetNodeByName(warpName);
								int nothing = 0;
							}

							else
							{
								int nothing = 0;
							}
						}
					}
				}
			}

			else
			{
				char buffer[256];
				sprintf(buffer, "Class_ID(%X, %X) unsupported PFlow Action", classId.PartA(), classId.PartB());
				gExporter->Log(buffer);
			}
		}
	//}


	// Function Map for Function Publish System 
	//***********************************
	/*
	BEGIN_FUNCTION_MAP

	FN_1(kGetMultiplier, TYPE_FLOAT, GetMultiplier, TYPE_TIMEVALUE );
	FN_0(kGetBornAllowance, TYPE_INT, GetBornAllowance );
	FN_0(kHasEmitter, TYPE_bool, HasEmitter);
	FN_1(kGetEmitterType, TYPE_INT, GetEmitterType, TYPE_TIMEVALUE );
	VFN_2(kGetEmitterDimensions, GetEmitterDimensions, TYPE_TIMEVALUE, TYPE_FLOAT_TAB_BR );
	FN_1(kGetEmitterGeometry, TYPE_MESH, GetEmitterGeometry, TYPE_TIMEVALUE);
	FN_0(kIsEmitterGeometryAnimated, TYPE_bool, IsEmitterGeometryAnimated);
	VFN_1(kSetRenderState, SetRenderState, TYPE_bool);
	FN_0(kIsRenderState, TYPE_bool, IsRenderState);
	FN_0(kGetIntegrationStep, TYPE_TIMEVALUE, GetIntegrationStep);
	FN_0(kGetUpdateType, TYPE_INT, GetUpdateType);
	FN_0(kNumParticlesSelected, TYPE_INT, NumParticlesSelected);
	FN_1(kGetSelectedParticleID, TYPE_INDEX, GetSelectedParticleID, TYPE_INDEX);
	FN_1(kIsParticleSelected, TYPE_bool, IsParticleSelected, TYPE_INDEX);
	FN_0(kNumActionListsSelected, TYPE_INT, NumActionListsSelected);
	FN_1(kGetSelectedActionList, TYPE_INODE, GetSelectedActionList, TYPE_INDEX);
	FN_1(kIsActionListSelected, TYPE_bool, IsActionListSelected, TYPE_INODE);
	FN_0(kIsRunningScript, TYPE_bool, IsRunningScript);

	END_FUNCTION_MAP
	*/


	return true;
}
