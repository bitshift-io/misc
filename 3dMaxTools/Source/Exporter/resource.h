//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Exporter.rc
//
#define IDD_TEST5_DIALOG                102
#define BTN_CLOSE                       102
#define IDS_APP_TITLE                   103
#define STR_CLASS_NAME                  103
#define IDM_ABOUT                       104
#define STR_LIBDESCRIPTION              104
#define IDM_EXIT                        105
#define IDC_TEST5                       109
#define STR_CATEGORY                    109
#define IDR_MAINFRAME                   128
#define DLG_EXPORT                      129
#define TXT_LOG                         1001
#define IDC_PROGRESS1                   1002
#define PRO_PROGRESS                    1002
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
