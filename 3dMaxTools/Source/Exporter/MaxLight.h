#ifndef _LIGHT_H_
#define _LIGHT_H_

#include "MaxObject.h"

class Light;

class MaxLight : public MaxObject
{
public:

	MaxLight(IGameNode* pGameNode);

	bool	Convert(Light* light);

};

#endif