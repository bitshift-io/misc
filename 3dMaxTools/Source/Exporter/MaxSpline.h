#ifndef _MAX_SPLINE_H_
#define _MAX_SPLINE_H_

#include "MaxObject.h"
#include <vector>

using namespace std;

class Spline;

class MaxSpline : public MaxObject
{
public:

	MaxSpline(IGameNode* pGameNode);

	bool	Convert(Spline* spline);

protected:

};

#endif