#ifndef _PCH_H_
#define _PCH_H_

#pragma comment(lib, "USER32.LIB")
#pragma comment(lib, "ParticleFlow.LIB")

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
#include "ILayerControl.h"
//#include "decomp.h"

#include "IGame\IGame.h"
#include "IGame\IGameObject.h"
#include "IGame\IGameProperty.h"
#include "IGame\IGameControl.h"
#include "IGame\IGameModifier.h"
#include "IGame\IConversionManager.h"
#include "IGame\IGameError.h"
#include "IGame\IGameFX.h"

#include "ParticleFlow/PFExport.h"
#include "ParticleFlow/IParticleGroup.h"
#include "ParticleFlow/IPFActionList.h"
#include "ParticleFlow/IPFSystem.h"
#include "ParticleFlow/IPFSystemPool.h"
#include "ParticleFlow/IPFActionListPool.h"
#include "ParticleFlow/PFClassIDs.h"

#include <windows.h>
#include <winuser.h>

#include "Helper.h"
#include "MaxObject.h"
//#include "MaxGeometry.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

#endif