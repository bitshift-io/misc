#include "MaxControl.h"
#include "PCH.h"
#include "MaxMesh.h"
#include "Render/SceneObject.h"
#include "Render/AnimationController.h"
#include "Exporter.h"
#include "Math/Affine.h"

// this is the class for all biped controllers except the root and the footsteps
#define BIPSLAVE_CONTROL_CLASS_ID Class_ID(0x9154,0)

// this is the class for the center of mass, biped root controller ("Bip01")
#define BIPBODY_CONTROL_CLASS_ID  Class_ID(0x9156,0)


bool MaxControl::GetTimeValue(IGameControl* gameControl, Tab<TimeValue>& times, IGameControlType type)
{
	TimeValue firstFrameTime = GetIGameInterface()->GetSceneStartTime();
	times.ZeroCount();

	Control* maxControl = gameControl->GetMaxControl(type);
	if (!maxControl)
	{
		times.Insert(0, 1, &firstFrameTime);
		return true;
	}

	// get list of key frame times
	if (maxControl->ClassID() == BIPSLAVE_CONTROL_CLASS_ID)
	{
		maxControl->GetKeyTimes(times, Interval(GetIGameInterface()->GetSceneStartTime(), GetIGameInterface()->GetSceneEndTime()), 0);
	}
	else
	{
		IGameKeyTab keys;
		if (!gameControl->GetQuickSampledKeys(keys, type))
		{
			if (!gameControl->GetFullSampledKeys(keys, 30, type))
				return false;
		}

		for (int i = 0; i < keys.Count(); ++i)
		{
			IGameKey& key = keys[i];
			if (key.t < GetIGameInterface()->GetSceneStartTime() || key.t > GetIGameInterface()->GetSceneEndTime())
				continue;

			times.Append(1, &key.t);
		}
	}

	// forces a keyframe on first frame if one doesnt already exist
	int i = 0;
	for (; i < times.Count(); ++i)
	{
		if (times[i] == GetIGameInterface()->GetSceneStartTime())
			break;
	}

	if (i >= times.Count())
		times.Insert(0, 1, &firstFrameTime);

	return true;
}

Matrix4	MaxControl::GetLocalTransform(IGameNode* gameNode, const TimeValue& time)
{
	IGameNode* parentNode = gameNode->GetNodeParent();
	GMatrix worldMat = gameNode->GetWorldTM(time);
	GMatrix localMat = worldMat;

	GMatrix parentWorldMat;
	parentWorldMat.SetIdentity();
	
	if (parentNode)
	{
		GMatrix parentWorldMat = parentNode->GetWorldTM(time);
		localMat = worldMat * parentWorldMat.Inverse();
	}
	
	return Helper::ConvertMatrix4(localMat);
}

bool MaxControl::Convert(IGameNode* pGameNode, SceneNode* pSceneNode)
{
	IGameObject* obj = pGameNode->GetIGameObject();
	if (!obj->InitializeData())
	{
		//gExporter->Log("Failed to convert obj");
		//return false;
	}

	const char* name = pGameNode->GetName();
	
	// is it animated?
	IGameControl* gameControl = pGameNode->GetIGameControl();
	if (!gameControl)
		return true;

	// biped controlls can say they are not animated yet they have keys
	if (!(gameControl->GetIGameKeyCount(IGAME_POS) != 0 
		|| gameControl->GetIGameKeyCount(IGAME_ROT) != 0 
		|| gameControl->GetIGameKeyCount(IGAME_SCALE) != 0 
		|| gameControl->IsAnimated(IGAME_POS) 
		|| gameControl->IsAnimated(IGAME_ROT) 
		|| gameControl->IsAnimated(IGAME_SCALE)))
		return false;

	AnimationNode* animationNode = gExporter->mAnimation->CreateNode();
	gExporter->mAnimation->InsertNode(animationNode);
	animationNode->SetName(name);

/*
	IAnimLayerControlManager* layerMgr = static_cast<IAnimLayerControlManager*>(GetCOREInterface(IANIMLAYERCONTROLMANAGER_INTERFACE ));
	anim->SetName(layerMgr->GetLayerName(0));
	if (!anim->GetName() || strlen(anim->GetName()) <= 0)
		anim->SetName("default");
*/

	float tickesPerFrame = (float)GetTicksPerFrame();
	float frameRate = (float)GetFrameRate();

	float startTimeInSeconds = (GetIGameInterface()->GetSceneStartTime() / tickesPerFrame) / frameRate; 
	float endTimeInSeconds = (GetIGameInterface()->GetSceneEndTime() / tickesPerFrame) / frameRate; 
	gExporter->mAnimation->SetLength(endTimeInSeconds - startTimeInSeconds);

	Tab<TimeValue> times;
	if (GetTimeValue(gameControl, times, IGAME_POS))
	{
		AnimationStream* posStream = animationNode->CreateStream();
		posStream->SetType(ST_Position);

		for (int i = 0; i < times.Count(); ++i)
		{
			float timeInSeconds = (times[i] / tickesPerFrame) / frameRate;
			Vector4 translation = GetLocalTransform(pGameNode, times[i]).GetTranslation();
			posStream->AppendValue(translation, timeInSeconds - startTimeInSeconds);
		}
	}

	if (GetTimeValue(gameControl, times, IGAME_ROT))
	{
		AnimationStream* rotStream = animationNode->CreateStream();
		rotStream->SetType(ST_Rotation);

		for (int i = 0; i < times.Count(); ++i)
		{
			Matrix4 localMat = GetLocalTransform(pGameNode, times[i]);
			AffineParts ap;
			ap.DecomposeAffine(localMat);

			float timeInSeconds = (times[i] / tickesPerFrame) / frameRate;
			rotStream->AppendValue(ap.rotation, timeInSeconds - startTimeInSeconds);
		}
	}

	if (GetTimeValue(gameControl, times, IGAME_SCALE) && times.Count() > 1)
	{
		AnimationStream* scaleStream = animationNode->CreateStream();
		scaleStream->SetType(ST_Scale);
		
		//Vector4 lastScale(1.f, 1.f, 1.f, 1.f);
		for (int i = 0; i < times.Count(); ++i)
		{
			Matrix4 localMat = GetLocalTransform(pGameNode, times[i]);
			float xScale = localMat.Column(0).Mag();
			float yScale = localMat.Column(1).Mag();
			float zScale = localMat.Column(2).Mag();
			Vector4 scale = Vector4(xScale, yScale, zScale, 1.f);
			//if (lastScale == scale)
			//	continue;

			float timeInSeconds = (times[i] / tickesPerFrame) / frameRate;
			scaleStream->AppendValue(scale, timeInSeconds - startTimeInSeconds);
		}

		//if (lastScale == Vector4(1.f, 1.f, 1.f, 1.f) && scaleStream->GetCount() <= 1)
		//{
		//}
	}

	// check for animated visiblity
	Control* visControl = pGameNode->GetMaxNode()->GetVisController();
	if (visControl)
	{
		IKeyControl* ikeys = GetKeyControlInterface(visControl);

		AnimationStream* visStream = animationNode->CreateStream();
		visStream->SetType(ST_Alpha);

		int numKeys = ikeys->GetNumKeys();	
		for (int i = 0; i < numKeys; ++i)
		{
			float value = 0.f;
			TimeValue time;
			switch (visControl->ClassID().PartA())
			{
			case LININTERP_FLOAT_CLASS_ID:
				{
					ILinFloatKey key;
					ikeys->GetKey(i, &key);
					value = key.val;
					time = key.time;
				}
				break;

			case HYBRIDINTERP_FLOAT_CLASS_ID:
				{
					IBezFloatKey key;
					ikeys->GetKey(i, &key);
					value = key.val;
					time = key.time;
				}
				break;

			case TCBINTERP_FLOAT_CLASS_ID:
				{
					ITCBFloatKey key;
					ikeys->GetKey(i, &key);
					value = key.val;
					time = key.time;
				}
				break;
			}

			float timeInSeconds = (time / tickesPerFrame) / frameRate;
			visStream->AppendValue(value, timeInSeconds - startTimeInSeconds);
		}
	}

	return true;
}
