#include "MaxShape.h"
#include "Render/Shape.h"
#include "Exporter.h"

#include "ParticleFlow/PFExport.h"
#include "ParticleFlow/IParticleGroup.h"

MaxShape::MaxShape(IGameNode* pGameNode) : MaxObject(pGameNode)
{

}

bool MaxShape::Convert(Shape* shape)
{
	INode* maxNode = mGameNode->GetMaxNode();
	ObjectState os = maxNode->EvalWorldState(0);

	//if (os.obj->SuperClassID() != GEOMOBJECT_CLASS_ID || os.obj->SuperClassID() != HELPER_CLASS_ID) 
	//	return false;

	Object* obj = os.obj;
	if (!obj)
		return false;

	IParamArray* array = obj->GetParamBlock();

	if (obj->ClassID() == PLANE_CLASS_ID)
	{
		PlaneShape plane;

		Box3 box;
		obj->GetLocalBoundBox(0, maxNode, gExporter->mInterface->GetActiveViewport(), box);

		plane.width = abs(box.pmax.x) * 2.f * MAX_TO_METERS;
		plane.length = abs(box.pmax.y) * 2.f * MAX_TO_METERS;

		shape->mPlane = plane;
		shape->mType = ST_Plane;
	}
	else if (obj->ClassID() == Class_ID(BOXOBJ_CLASS_ID, 0))
	{
		BoxShape box;
		array->GetValue(obj->GetParamBlockIndex(BOXOBJ_LENGTH), 0, box.length, FOREVER);
		array->GetValue(obj->GetParamBlockIndex(BOXOBJ_HEIGHT), 0, box.height, FOREVER);
		array->GetValue(obj->GetParamBlockIndex(BOXOBJ_WIDTH), 0, box.width, FOREVER);

		box.length *= MAX_TO_METERS;
		box.height *= MAX_TO_METERS;
		box.width *= MAX_TO_METERS;

		shape->mBox = box;
		shape->mType = ST_Box;
	}
	else if (obj->ClassID() == Class_ID(SPHERE_CLASS_ID, 0))
	{
		SphereShape sphere;
		array->GetValue(obj->GetParamBlockIndex(SPHERE_RADIUS), 0, sphere.radius, FOREVER);

		sphere.radius *= MAX_TO_METERS;

		shape->mSphere = sphere;
		shape->mType = ST_Sphere;
	}
	else if (obj->ClassID() == Class_ID(CYLINDER_CLASS_ID, 0))
	{
		CylinderShape cylinder;
		array->GetValue(obj->GetParamBlockIndex(CYLINDER_RADIUS), 0, cylinder.radius, FOREVER);
		array->GetValue(obj->GetParamBlockIndex(CYLINDER_HEIGHT), 0, cylinder.height, FOREVER);

		cylinder.radius *= MAX_TO_METERS;
		cylinder.height *= MAX_TO_METERS;

		shape->mCylinder = cylinder;
		shape->mType = ST_Cylinder;
	}
	else if (obj->ClassID() == Class_ID(DUMMY_CLASS_ID, 0) 
		|| obj->ClassID() == Class_ID(POINTHELP_CLASS_ID, 0))
	{
		//DummyObject* dummyObj = dynamic_cast<DummyObject*>(obj);

		BoxShape box;
		box.length = 0;
		box.width = 0;
		box.height = 0;

		// TODO - calulate width height and length
		//Box3 box3 = dummyObj->GetBox();
		//box.length = box3.pmax;

		shape->mBox = box;
		shape->mType = ST_Box;

	}
	else
	{
		gExporter->Log("Invalid shape");
		/*
		Object* pFlowObj = GetPFObject(obj);
		if (pFlowObj != 0)
			int nothing = 0;*/
	}

	return true;
}
