myScriptFileName = #("uv_tools.mse","HB_AssignVertexColors.ms" , "HB_selectPolygonByTopology.ms","HB_findUnusedMaterials.ms","HB_pitsAndPeaks.ms","HB_selectObjectByModifier.ms","HB_centrePivot.ms","HB_groundPivot.ms", "HB_breakObject.ms", "HB_cloneObjectsOverOld.ms" , "HB_purgeMaterials.ms", "HB_mapToVertexColours.ms", "HB_vertColBlend.ms", "HB_clearMappingChannels.ms" , "HB_VertexAlphaCleaner.ms" , "HB_dropVerts.ms" , "HB_ShadowVolumeExtrude.ms", "HB_FindBadUVs.ms","","HB_ConvertToDX9.ms","HB_ConvertToStandard.ms","" , "XMLCameraImporter.ms" , "XMLCameraImporterSpline.ms" , "XMLPropImporter.ms" , "" ,"HB_TriggerZone.ms" ,"HB_propTillYouDrop.ms")
myDropdownItems = #("UV Tools", "Assign Vertex Colors" , "Select Polygon Topology", "Unused Materials", "Pits & Peaks", "Select by Modifier", "Center Pivots","Ground Pivot","Break Objects", "Clone Over Old" , "Purge Materials", "Texture to Vertex Colours", "Vertex Colour Blender" , "Clear Mapping Channels" , "Vertex Alpha Cleaner" , "Drop Verts" , "Shadow Volume Extrude" ,"Find Bad UV's","-----------------", "Convert To DX9" , "Convert To Standard" , "-----------------" , "Camera Importer" , "Camera Importer Spline" , "Prop Importer" , "-----------------" , "Trigger Zone" , "Prop Till You Drop" ) 
myNetworkPath = "k:/hellboy/graphics/max scripts/"
myLocalPath = "C:/Program Files/Autodesk/3dsMax8/Scripts/"

--delete unused scripts
deleteFile (myLocalPath + "uv_tools.ms")
deleteFile (myLocalPath + "uv_tools_run.ms")
deleteFile (myLocalPath + "uv_tools_run.mse")

rollout myCustomScript "script"
 (
 	dropDownList myDropdownList "" items:#(".......................................................") pos:[4,4] width:113
 	button myGoButton ">>" pos:[120,4] height:21 width:25

 	on myCustomScript open do
 	(
		myCustomScript.myDropdownList.items = myDropdownItems
		myCustomScript.myDropdownList.height = (myDropdownItems.count * 20)
 		cui.registerDialogBar myCustomScript
 		cui.dockDialogBar myCustomScript #cui_dock_top
 	)

	on myGoButton Pressed do
	(
		try
		(
			i = myDropdownList.selection
			myLocalRunScript = myLocalPath + myScriptFileName[i] 
			myNetworkScript = myNetworkPath + myScriptFileName[i] 
			deleteFile myLocalRunScript
			copyFile myNetworkScript myLocalRunScript
			fileIn myLocalRunScript
		)
		catch()
	)
	
	on myDropdownlist selected i do
	(
		try
		(
			myLocalRunScript = myLocalPath + myScriptFileName[i] 
			myNetworkScript = myNetworkPath + myScriptFileName[i] 
			deleteFile myLocalRunScript
			copyFile myNetworkScript myLocalRunScript
			fileIn myLocalRunScript
		)
		catch()
	)
 )
 createDialog myCustomScript 160 39
