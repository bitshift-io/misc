--###############################################################################
--## Script Description
--## This script sets up individual node settings for the exporter
--## Bronson Mathews 2007, gibbz1@yahoo.com
--###############################################################################


--###############################################################################
--## Define Globals
--## Without Globals we cany use functions here there and everywhere!
--###############################################################################

-- for the timer
global mLastSelection = #()

global mRolloutUserDefined


--###############################################################################
--## Functions
--##The shiz!
--###############################################################################


--## Read the keystring, and then Tokenize the value and returns it as an array
fn fnReadString valNode valKeyString =
(
	mValue = getUserProp valNode valKeyString
	if mValue == undefined do
		return mValue
		
	mArray = #()
	mArray = filterstring mValue "|, "
	return mArray	
)

--## Returns an index to a string in the array from tokenize
fn fnFindStringInArray valArray valString =
(
	
)

--## writes the new string. The final step to bliss!
fn fnWriteString valNode valKeyString valValueArray =
(
	mValue = " "
	for i = 1 to valValueArray.count do
	(
		if i > 1 do
			mValue = mValue + " | "
			
		mValue = mValue + valValueArray[i]
	)

	setUserProp valNode valKeyString mValue
)

--fnAppendKeyStringValue $ "#VertexFormat" "test"
--## function that will append the value to the keystring in the user defined section
fn fnAppendKeyStringValue valNode valKeyString valValue =
(
	-- get the current string as array
	mValueArray = #()
	mValueArray = fnReadString valNode valKeyString
	
	-- if nothing found, just write a new one
	if mValueArray == undefined then
	(
		mValueArray = #()
		append mValueArray valValue
	)
	else
	(
		mIndex = findItem mValueArray valValue
		--0 == not existing, so we need to add it to the array
		if mIndex == 0 do 
			append mValueArray valValue		
	)
	fnWriteString valNode valKeyString mValueArray
)

--## function that will remove the value from a keystring in the user defined section
fn fnRemoveKeyStringValue valNode valKeyString valValue =
(
	-- get the current string as array
	mValueArray = #()
	mValueArray = fnReadString valNode valKeyString

	-- if something found check if out string exists...
	if mValueArray != undefined do
	(
		mIndex = findItem mValueArray valValue
		--0 == not existing, which is great, but we need to remove if exists
		-- NOTE: May need to add a while loop if duplicates start occuring!
		if mIndex != 0 do
		( 
			deleteItem mValueArray mIndex
			fnWriteString valNode valKeyString mValueArray			
		)	
	)
)


--## Overwrite the user defined prop
fn fnOverwriteUserDefined valSelection valString =
(
	for mObj in valSelection do
		setUserPropBuffer mObj valString
)

--## Default UI Settings
fn fnResetUI = 
(
	mRolloutFlags.checkShape.tristate = 1
	
	mRolloutFormats.checkVertColor.tristate = 1
	
	mRolloutUserDefined.editUserDefined.text = ""
)

--## update the ui
fn fnUpdateUI =
(
	mSelection = selection as array
	mUserPropBuffer = undefined
	
	-- collect our info from user defined
	for i = 1 to mSelection.count do
	(
		mObj = mSelection[i]
		
		-- collect from first mesh
		if i == 1 then
		(
			mUserPropBuffer = getUserPropBuffer mObj
			
		)
		-- compare to see if any matches
		else
		(
			-- user defined prop buffer
			if mUserPropBuffer != (getUserPropBuffer mObj) do
				mUserPropBuffer = undefined
			
		)


	)
	
	-- update UI
	-- user defined prop buffer
	if mUserPropBuffer != undefined then
		mRolloutUserDefined.editUserDefined.text = mUserPropBuffer
	else
		mRolloutUserDefined.editUserDefined.text = "** Warning! Editing this field will overwrite all your settings\n on selected objects! **"

	
)

--## function to check for selection changed
fn fnHasSelectionChanged = 
(
	mCurrentSelection = selection as array
	mBool = false
	
	-- first we check if the counts are different
	if mLastSelection.count != mCurrentSelection.count then
	(
		mLastSelection = mCurrentSelection
		mBool = true
	)
	else
	(
		for i = 1 to mCurrentSelection.count do
		(
			if mCurrentSelection[i] != mLastSelection[i] do
			(
				mLastSelection = mCurrentSelection
				mBool = true	
				exit			
			)
		)		
	)
	
	return mBool
)

--###############################################################################
--## Rollouts and Shiz
--## Main UI here with buttons and stuff...
--###############################################################################


rollout mRolloutFlags "Flags"
( 
	local uiY = 2
	local uiX = 7
	
	checkbox checkRenderable "Non-Renderable" width:100 height:20 pos:[uiX,uiY] tooltip:"Object not visible"  triState:0
	checkbox checkTransformWorld "World Transform" width:100 height:20 pos:[uiX,uiY+=20] tooltip:"Use world as transform" triState:0
	checkbox checkShape "Shape" width:100 height:20 pos:[uiX,uiY+=20] tooltip:"Export as shape(works on primitives)" triState:0
	
	on checkRenderable changed mVal do 
	(
		mCurrentSelection = getcurrentselection()
		if mVal == true then
		(
			for i = 1 to mCurrentSelection.count do (setUserProp mCurrentSelection[i] "#flags" "renderable")
		)
		else
		(
			for i = 1 to mCurrentSelection.count do (setUserProp mCurrentSelection[i] "#flags" "nonrenderable")
		)
	)
	
	on checkTransformWorld changed mVal do 
	(
		mCurrentSelection = getcurrentselection()
		if mVal == true then
		(
			for i = 1 to mCurrentSelection.count do (setUserProp mCurrentSelection[i] "#trasform" "world")
		)
		else
		(
			for i = 1 to mCurrentSelection.count do (setUserProp mCurrentSelection[i] "#trasform" "local")
		)
	)	
)


rollout mRolloutFormats "Vertex Formats"
( 
	local uiY = 2
	local uiX = 7
	
	checkbox checkVertColor "Vertex Color" height:20 pos:[uiX,uiY] tooltip:"Export Vertex Color" triState:1
	checkbox checkVertAlpha "Vertex Alpha" height:20 pos:[uiX,uiY+=20] tooltip:"Export Vertex Alpha" triState:1
	checkbox checkVertIllum "Vertex Illumination" height:20 pos:[uiX,uiY+=20] tooltip:"Export Vertex Alpha" triState:1
	checkbox checkTextureCoordinates "Texture Coordinates" height:20 pos:[uiX,uiY+=20] tooltip:"Export TextureCoordinates" triState:1
	
	local uiY = 2
	local uiX = 147
	
	checkbox checkNormal "Normals" height:20 pos:[uiX,uiY] tooltip:"Export Normals" triState:1
	checkbox checkTangents "Bi-Normals/Tangents" height:20 pos:[uiX,uiY+=20] tooltip:"Export Tangents/BiNormals" triState:1
	checkbox checkPosition "Vertex Positions" height:20 pos:[uiX,uiY+=20] tooltip:"Export Object Positions" triState:1 enabled:true
	
	
	-- Vertex Color
	on checkVertColor changed mBool do
	(
		if mBool then
			for mObj in selection do
				fnAppendKeyStringValue mObj "#vertexformat" "colour"
		else
			for mObj in selection do
				fnRemoveKeyStringValue mObj "#vertexformat" "colour"
	)
	
	-- Vertex Alpha
	on checkVertAlpha changed mBool do
	(
		if mBool then
			for mObj in selection do
				fnAppendKeyStringValue mObj "#vertexformat" "alpha"
		else
			for mObj in selection do
				fnRemoveKeyStringValue mObj "#vertexformat" "alpha"
	)	
	
	-- Vertex Illumination
	on checkVertIllum changed mBool do
	(
		if mBool then
			for mObj in selection do
				fnAppendKeyStringValue mObj "#vertexformat" "illumination"
		else
			for mObj in selection do
				fnRemoveKeyStringValue mObj "#vertexformat" "illumination"
	)		
	
	-- UVW coords
	on checkTextureCoordinates changed mBool do
	(
		if mBool then
			for mObj in selection do
				fnAppendKeyStringValue mObj "#vertexformat" "texcoord"
		else
			for mObj in selection do
				fnRemoveKeyStringValue mObj "#Vertexformat" "texcoord"
	)

	-- Normals
	on checkNormal changed mBool do
	(
		if mBool then
			for mObj in selection do
				fnAppendKeyStringValue mObj "#vertexformat" "normal"
		else
			for mObj in selection do
				fnRemoveKeyStringValue mObj "#vertexformat" "normal"
	)

	-- BiNormals/Tangents
	on checkTangents changed mBool do
	(
		if mBool then
			for mObj in selection do
				fnAppendKeyStringValue mObj "#vertexformat" "tangent"
		else
			for mObj in selection do
				fnRemoveKeyStringValue mObj "#vertexformat" "tangent"
	)

	-- Position
	on checkPosition changed mBool do
	(
		if mBool then
			for mObj in selection do
				fnAppendKeyStringValue mObj "#vertexformat" "position"
		else
			for mObj in selection do
				fnRemoveKeyStringValue mObj "#vertexformat" "position"
	)		
)


rollout mRolloutUserDefined "User Defined"
( 
	local uiY = 2
	local uiX = 7
	
	edittext editUserDefined "" height:200 fieldWidth:275 pos:[0,uiY]
	
	on editUserDefined changed mString do
	(
		mSelection = selection as array
		fnOverwriteUserDefined mSelection mString
	)
	
	-- timmer shoved in here so it loads the vital goodies :)
	timer mExportOptionsClock interval:500 
	on mExportOptionsClock tick do
	(		
		-- check if selection has changed since last update
		mBool = fnHasSelectionChanged()

		-- if selection has changed, we should update the ui!
		if mBool do
		(	
			mSelection = selection as array
			if mSelection.count == 0 then
				fnResetUI()
			else
				fnUpdateUI()
		)
	)	
)

--###############################################################################
--## Create Dialogs & Rollouts
--## These put the UI onscreen!
--###############################################################################

options.printAllElements = true
if mExportOptionsFloater != undefined do (closeRolloutFloater mExportOptionsFloater)
mExportOptionsFloater = newRolloutFloater "Export Options" 300 445
addRollout mRolloutFlags mExportOptionsFloater 
addRollout mRolloutFormats mExportOptionsFloater 
addRollout mRolloutUserDefined mExportOptionsFloater 
clearListener()




