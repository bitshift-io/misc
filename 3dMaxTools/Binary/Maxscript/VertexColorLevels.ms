-- photoshop levels for max!! max you suck!
-- Bronson Mathews 2007

mapped fn fnGetMapVertArray valObj valChannel =
(
	VertexColorArray = #()
	
	if (classof valObj) == Editable_Poly do
	(
		mNumMapVerts = polyOp.getNumMapVerts valObj valChannel
		mGetMapVert = polyOp.getMapVert -- quick optimize

		for i = 1 to mNumMapVerts do
			append VertexColorArray (mGetMapVert valObj valChannel i)
	)

	if (classof valObj) == Editable_Mesh do
	(
		mNumMapVerts = meshop.getNumMapVerts valObj valChannel
		mGetMapVert = meshop.getMapVert -- quick optimize

		for i = 1 to mNumMapVerts do
			append VertexColorArray (mGetMapVert valObj valChannel i)
	)
	
	return VertexColorArray
)

mapped fn fnSetMapVertArray valObj valChannel valMapVertArray =
(
	if (classof valObj) == Editable_Poly do
	(
		mNumMapVerts = polyOp.getNumMapVerts valObj valChannel
		mSetMapVert = polyOp.setMapVert -- quick optimize

		for i = 1 to mNumMapVerts do
			mSetMapVert valObj valChannel i valMapVertArray[i]
			
		update valObj			
		convertToPoly valObj --hack, but why?
	)

	if (classof valObj) == Editable_Mesh do
	(
		mNumMapVerts = meshop.getNumMapVerts valObj valChannel
		mSetMapVert = meshop.setMapVert -- quick optimize

		for i = 1 to mNumMapVerts do
			mSetMapVert valObj valChannel i valMapVertArray[i]
		
		update valObj		
	)
	

)

mapped fn fnOutputLevels valFloat valBool valChannel=
(
	valFloat = valFloat / 255.0
	
	for mObj in selection do
	(
		mMapVerts = fnGetMapVertArray mObj valChannel

		if valBool then --white range
		(
			for i = 1 to mMapVerts.count do
				mMapVerts[i] = mMapVerts[i] * valFloat
		)
		else --black range
		(
			for i = 1 to mMapVerts.count do
				mMapVerts[i] = ((mMapVerts[i] - 1) * valFloat) + 1
		)
		
		fnSetMapVertArray mObj valChannel mMapVerts 
	)
)

mapped fn fnClampLevels valFloat valBool valChannel=
(
	valFloat = valFloat / 255.0
	
	for mObj in selection do
	(
		mMapVerts = fnGetMapVertArray mObj valChannel
		
		if valBool then --white range
		(
			for i in mMapVerts do
			(
				if i.x > valFloat do
					i.x = valFloat 
					
				if i.y > valFloat do
					i.y = valFloat 
					
				if i.z > valFloat do
					i.z = valFloat 		
			)
		)
		else --black range
		(
			for i in mMapVerts do
			(
				if i.x < valFloat do
					i.x = valFloat 
					
				if i.y < valFloat do
					i.y = valFloat 
					
				if i.z < valFloat do
					i.z = valFloat 		
			)		
		)
		
		fnSetMapVertArray mObj valChannel mMapVerts 
	)
)


-- ========= DIALOG ==========
rollout mVertexColorLevels "Vertex Color Levels"
(
	local uiX = 5
	local uiY = 5

	label lbl1 "Input Levels:" pos:[uiX, uiY]
	slider slide "noob" orient:#horizontal ticks:5 range:[0,100,0]

	local uiX = 5
	local uiY = 100
	label lbl2 "Output Levels:" pos:[uiX, uiY]
	spinner spinOutputBlack "B:" range:[0,255,0] type:#integer pos:[uiX,uiY+=20] fieldwidth:50
	spinner spinOutputWhite "W:" range:[0,255,255] type:#integer pos:[uiX + 100,uiY] fieldwidth:50	
	
	on spinOutputBlack changed val do
	(
		fnOutputLevels val false 0
	)
	on spinOutputWhite changed val do
	(
		fnOutputLevels val true 0
	)	
	
	local uiX = 5
	local uiY = 150
	label lbl3 "Clamp Levels:" pos:[uiX, uiY]
	spinner spinClampBlack "B:" range:[0,255,0] type:#integer pos:[uiX,uiY+=20] fieldwidth:50
	spinner spinClampWhite "W:" range:[0,255,255] type:#integer pos:[uiX + 100,uiY] fieldwidth:50	
	
	on spinClampBlack changed val do
	(
		fnClampLevels val false 0
	)
	on spinClampWhite changed val do
	(
		fnClampLevels val true 0
	)		
)


createdialog mVertexColorLevels 300 220

--theNewFloater = newRolloutFloater "Grinning" 300 220
--addRollout grin theNewFloater
