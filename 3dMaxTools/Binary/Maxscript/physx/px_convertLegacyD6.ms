--
-- Convert legacy PhysX D6 joints to new D6 joints
--
-- Instructions for use:
--    1. Build and install the legacy PhysX max plug-in.
--    2. Run Max and load a max file containing legacy style D6 joints.
--    3. Run this script. It will convert all the existing legacy style joints
--       to the latest form of D6Joint. All properties are copied as best as
--       possible, but there may be some changes in joint behavior. See the
--       limitations below. The name of th enew joint is the old joint name
--       with "converted" appended.
--    4. Save the file. You can now uninstall the legacy max plug-in and
--       install the latest plug-in.
--
--
-- Known issues and limitations:
--    1. The "Legacy" attachment point option for a legacy joint will
--       become a "Joint Center" attachment.
--    2. Joints in which one of the bodies is a group will be converted
--       such that the first object in the group is the new body.
--    3. Some objects that were previously hidden may become visible
--       after running the script.
--
-- Last Modified April 22, 2008.
--
select $*

(
    local legacyD6
    local newPxJoint
    for legacyD6 in getCurrentSelection() do
    (
        if (iskindof legacyD6 px6DOF) then
        (
            conversionMsg = "Converting " + legacyD6.name;
            print conversionMsg

            -- figure out which type of attachment point we should calculate
            local attachmentPointType = 1;
            if (legacyD6.aptype != undefined) then
            (
                attachmentPointType = legacyD6.aptype - 1;
                if (attachmentPointType > 4) then
                (
                    attachmentPointType = 1;
                )
            )

            newPxJoint = pxJoint name:(legacyD6.name + "converted");
			
            newPxJoint.transform = legacyD6.transform;
			
            newPxJoint.body0 = legacyD6.body0;
            newPxJoint.body1 = legacyD6.body1;
            newPxJoint.aptype = attachmentPointType;
            newPxJoint.swing1_angle = legacyD6.swing1_angle;
            newPxJoint.swing1_rest = legacyD6.swing1_rest;
            newPxJoint.swing1_spring = legacyD6.swing1_spring;
            newPxJoint.swing1_damp = legacyD6.swing1_damp;
            newPxJoint.swing1_locked = legacyD6.swing1_locked;
            newPxJoint.swing1_limited = legacyD6.swing1_limited;
            newPxJoint.swing2_angle = legacyD6.swing2_angle;
            newPxJoint.swing2_rest = legacyD6.swing2_rest;
            newPxJoint.swing2_spring = legacyD6.swing2_spring;
            newPxJoint.swing2_damp = legacyD6.swing2_damp;
            newPxJoint.swing2_locked = legacyD6.swing2_locked;
            newPxJoint.swing2_limited = legacyD6.swing2_limited;
            newPxJoint.twist_rest = legacyD6.twist_rest;
            newPxJoint.twist_spring = legacyD6.twist_spring;
            newPxJoint.twist_damp = legacyD6.twist_damp;
            newPxJoint.twist_enbl = legacyD6.twist_enbl;
            newPxJoint.twistlow = legacyD6.twistlow;
            newPxJoint.twisthigh = legacyD6.twisthigh;
            newPxJoint.twist_lmt = legacyD6.twist_lmt;
            newPxJoint.x_state = legacyD6.x_state;
            newPxJoint.y_state = legacyD6.y_state;
            newPxJoint.z_state = legacyD6.z_state;
            newPxJoint.xlate_rad = legacyD6.xlate_rad;
            if (legacyD6.projectionMode != undefined) then
                newPxJoint.projectionMode = legacyD6.projectionMode;
            newPxJoint.projectionDist = legacyD6.projectionDist;
            newPxJoint.projectionAngle = legacyD6.projectionAngle;
            newPxJoint.collision = legacyD6.collision;
            newPxJoint.gearing = legacyD6.gearing;
            newPxJoint.gearRatio = legacyD6.gearRatio;
            newPxJoint.breakable = legacyD6.breakable;
            newPxJoint.maxForce = legacyD6.maxForce;
            newPxJoint.maxTorque = legacyD6.maxTorque;
            newPxJoint.helpersize = legacyD6.helpersize;

            delete legacyD6;
        )
    )
)

clearSelection()

