//////////////////////////////////////////////////
//simple light map shader with blend amount
//It uses map channel 1 for the diffuse and map channel 4 for the light, and is designed for RTT usage.
//Complete over kill, but simple to write..
//////////////////////////////////////////////////

float4x4 World      : 		WORLD;
float4x4 View       : 		VIEW;
float4x4 Projection : 		PROJECTION;
float4x4 WorldViewProj : 	WORLDVIEWPROJ;
float4x4 WorldView : 		WORLDVIEW;

// tweakables

texture diffuseTexture : DiffuseMap< 
	string UIName = "Diffuse Texture";
	int Texcoord = 1;
	int MapChannel = 1;	
>;
	
//add spacer here


float  Mix<
	string UIName = "Overbright amount";
	string UIType = "MaxSpinner";
	float UIMin = 0.0f;
	float UIMax = 4.0f;	
	float UIStep = 0.1;
	>  = 2.0f;
	

struct VS_OUTPUT
{
	float4 oPos : POSITION;
	float4 oColour : COLOR0;
	float2 oDiffuseTex :TEXCOORD0;
};

int texcoord0 : Texcoord
<
	int Texcoord = 0;
	int MapChannel = 0;
>;

int texcoord2 : Texcoord
<
	int Texcoord = 2;
	int MapChannel = -2;
>;

// very simple - we don't need lighting as it is provided by the lightmap.

VS_OUTPUT VS(
	float3 Pos  : POSITION, 
	float3 Norm : NORMAL, 
	float4 Colour : TEXCOORD0,
	float2 DiffuseTex  : TEXCOORD1,
  float4  Alpha : TEXCOORD2)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;


	Out.oDiffuseTex = DiffuseTex;	// diffuse 
	Out.oColour = float4(Mix * Colour.rgb, Alpha.r);
	float3 P = mul(float4(Pos, 1),(float4x4)WorldView);  // position (view space)
	Out.oPos = mul(float4(P,1),Projection);
	return Out;
   
}


sampler DiffuseSampler = sampler_state
{
    Texture   = (diffuseTexture);
    MipFilter = Point; //LINEAR;
    MinFilter = Point; //LINEAR;
    MagFilter = Point; //LINEAR;
};



float4 PS(
    float4 Colour : COLOR0,
    float2 DiffuseTex  : TEXCOORD0) : COLOR
{
    float4 diff = tex2D(DiffuseSampler, DiffuseTex);
    return diff * Colour;
}

technique Default
{
    pass P0
    {
      //ZFunc = Greaterequal;
      CullMode = CW;
      VertexShader = compile vs_3_0 VS();
      PixelShader  = compile ps_3_0 PS();
    }  
}
