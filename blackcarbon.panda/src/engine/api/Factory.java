package engine.api;

import java.io.InputStream;
import java.util.Vector;

//
// Create an instance of this,
// holds pointers to all framework resources
//
// create implies creating a new one regardless of name
// acquire implies looking for an existing resource with said name and returning that, else creating a new one
//
public class Factory
{	
	public enum Platform
	{
		Unknown,
		Desktop,
		Android,
	}
	
	public Factory()
	{
		mFactory = this;
	}

	public RenderBuffer CreateRenderBuffer()
	{
		return null;
	}
	
	public RenderBuffer AcquireRenderBuffer(String name)
	{
		return null;
	}
	
	public void ReleaseRenderBuffer(RenderBuffer resource)
	{
		
	}
	
	public RenderBuffer.AttributeBuffer AcquireAttributeBuffer()
	{
		return null;
	}
	
	public void ReleaseAttributeBuffer(RenderBuffer.AttributeBuffer resource)
	{
		
	}
	
	public Texture AcquireTexture(String name)
	{
		return null;
	}
	
	public void ReleaseTexture(Texture resource)
	{

	}
	
	public Shader AcquireShader(String name)
	{
		return null;
	}
	
	public void ReleaseShader(RenderBuffer resource)
	{
		
	}
	
	public Font AcquireFont(String name)
	{
		Font font = new Font();
		font.mName = name;
		font.mFactory = this;
		font.Load();
		return font;
	}
	
	public void ReleaseFont(Font resource)
	{

	}
	
	public Mesh AcquireMesh(String name)
	{
		Mesh mesh = new Mesh();
		mesh.mName = name;
		mesh.mFactory = this;
		mesh.Load();
		return mesh;
	}
	
	public void ReleaseMesh(Mesh resource)
	{

	}
	
	public Material AcquireMaterial(String name)
	{
		Material material = new Material();
		material.mName = name;
		material.mFactory = this;
		material.Load();
		return material;
	}
	
	public void ReleaseMaterial(Material resource)
	{

	}
	
	public void ReleaseSound()
	{
		
	}
	
	public void SetDevice(Device device)
	{
		mDevice = device;
	}
	
	public Device GetDevice()
	{
		return mDevice;
	}
		
	// returns false if the application should close
	public boolean Update()
	{		
		if (mDevice != null)
			return mDevice.Update();
		
		return true;
	}
	
	static public Factory Get()
	{
		return mFactory;
	}
	
	public InputStream GetFileResource(String name, String extension)
	{
		return null;
	}
	
	public Platform GetPlatform()
	{
		return mPlatform;
	}
	
	public void ReloadResources()
	{
		// called when OGL context lost and re-aquired
		// for example, on screen rotation or resume from pause
		// we need to re-setup OGL states
		for (int i = 0; i < mTexture.size(); ++i)
		{
			Texture resource = mTexture.get(i);
			resource.Reload();
		}
		
		for (int i = 0; i < mRenderBuffer.size(); ++i)
		{
			RenderBuffer resource = mRenderBuffer.get(i);
			resource.Reload();
		}

		for (int i = 0; i < mShader.size(); ++i)
		{
			Shader resource = mShader.get(i);
			resource.Reload();
		}
	}
	
	protected Device			mDevice;
	protected Platform			mPlatform				= Platform.Unknown;
	
	protected static Factory	mFactory;
	
	protected Vector<Material>			mMaterial				= new Vector<Material>();
	protected Vector<Shader>			mShader					= new Vector<Shader>();
	protected Vector<Texture>			mTexture				= new Vector<Texture>();
	protected Vector<Mesh>				mMesh					= new Vector<Mesh>(); 
	protected Vector<RenderBuffer>		mRenderBuffer			= new Vector<RenderBuffer>();
}


class Resource
{
	public String	mName;
	public Factory	mFactory;
}