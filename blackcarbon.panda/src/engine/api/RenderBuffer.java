package engine.api;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import engine.android.AndroidRenderBuffer.AttributeBuffer;

import engine.math.Math;
import engine.math.Vector4;

public class RenderBuffer extends Resource
{
	//Related to the Shader.Attribute
	// http://stackoverflow.com/questions/6854315/opengl-es-2-0-and-vertex-buffer-objects-vbo
	public static class AttributeBuffer
	{		
		// usage
		public static byte Static			= 1;
		public static byte Dynamic			= 2;
		
		public void SetByte(byte vec[])
		{
			mByteBuffer = ByteBuffer.allocateDirect(vec.length);
			mByteBuffer.put(vec);
			mByteBuffer.position(0);
			mSize = 1;
		}
		
		public void SetFloat(float vec[], int size)
		{
			ByteBuffer uvbb = ByteBuffer.allocateDirect(vec.length * 4);
	        uvbb.order(ByteOrder.nativeOrder());
	        mFloatBuffer = uvbb.asFloatBuffer();
		    
		    for (int i = 0; i < vec.length; ++i)
		    {
		    	mFloatBuffer.put(vec[i]);
		    }
		    mFloatBuffer.position(0);
		    mSize = size;
		}
		
		public void SetFloat(Vector4 vec[], int size)
		{
			ByteBuffer uvbb = ByteBuffer.allocateDirect(vec.length * size * 4);
	        uvbb.order(ByteOrder.nativeOrder());
	        mFloatBuffer = uvbb.asFloatBuffer();
		    for (int i = 0; i < vec.length; ++i)
		    {
		    	for (int s = 0; s < size; ++s)
		    	{
		    		mFloatBuffer.put(vec[i].v[s]);
		    	}
		    }
		    mFloatBuffer.position(0);
		    mSize = size;
		}
		
		public int Count()
		{
			if (mByteBuffer != null)
			{
				return mByteBuffer.capacity();
			}
			
			if (mFloatBuffer != null)
			{
				return mFloatBuffer.capacity() / mSize;
			}
			
			return 0;
		}
		
		public byte Usage()
		{
			return mUsage;
		}
		
		public Buffer buffer()
		{
			if (mByteBuffer != null)
				return mByteBuffer;
			
			return mFloatBuffer;
		}
		
		public void Reload()
		{
			
		}
				
		protected byte				mUsage			= Dynamic;
		protected FloatBuffer   	mFloatBuffer	= null;
		protected ByteBuffer   		mByteBuffer		= null;
		protected int				mSize;
	}
	/*
	// vertex format
	public static byte Position 		= 1 << 0;
	public static byte UV				= 1 << 1;
	public static byte Colour			= 1 << 2;
	
	// usage
	public static byte Static			= 1;
	public static byte Dynamic			= 2;
	
	public void Bind()
	{

	}
	
	public void Unbind()
	{
		
	}
	
	public void Render()
	{
		
	}
	
	public void CreateDebugMatrix()
	{
		/*
		Vector4 position = matrix.GetTranslation();
		
		Vector4 endPosition = matrix.GetZAxis();
		endPosition = endPosition.Multiply(scale);
		DrawLine(position, endPosition, new Vector4(0.f, 0.f, 1.f));
		
		endPosition = matrix.GetXAxis();
		endPosition = endPosition.Multiply(scale);
		DrawLine(position, endPosition, new Vector4(1.f, 0.f, 0.f));
		
		endPosition = matrix.GetYAxis();
		endPosition = endPosition.Multiply(scale);
		DrawLine(position, endPosition, new Vector4(0.f, 1.f, 0.f));
		* /	
		SetTopology(Device.GeometryTopology.LineList);
	}
	
	public void CreateDebugUnitSphere(Vector4 colour)
	{
		/*
		Vector4 cur = new Vector4();
		Vector4 last = new Vector4();	
		
		last = sphere.Add(new Vector4(Math.Sin(0.f) * sphere.GetW(), Math.Cos(0.f) * sphere.GetW(), 0.f, 0.f));
		for (float a = 0.1f; a <= (Math.PI * 2) + 0.1f; a += 0.1f)
		{
			cur = sphere.Add(new Vector4(Math.Sin(a) * sphere.GetW(), Math.Cos(a) * sphere.GetW(), 0.f, 0.f));
			DrawLine(last, cur, colour);
			last = cur;
		}

		last = sphere.Add(new Vector4(Math.Sin(0.f) * sphere.GetW(), Math.Cos(0.f) * sphere.GetW(), 0.f, 0.f));
		for (float a = 0.1f; a <= (Math.PI * 2) + 0.1f; a += 0.1f)
		{
			cur = sphere.Add(new Vector4(Math.Sin(a) * sphere.GetW(), Math.Cos(a) * sphere.GetW(), 0.f, 0.f));
			DrawLine(last, cur, colour);
			last = cur;
		}

		last = sphere.Add(new Vector4(Math.Sin(0.f) * sphere.GetW(), Math.Cos(0.f) * sphere.GetW(), 0.f, 0.f));
		for (float a = 0.1f; a <= (Math.PI * 2) + 0.1f; a += 0.1f)
		{
			cur = sphere.Add(new Vector4(Math.Sin(a) * sphere.GetW(), Math.Cos(a) * sphere.GetW(), 0.f, 0.f));
			DrawLine(last, cur, colour);
			last = cur;
		}
		* /
		SetTopology(Device.GeometryTopology.LineList);
	}
	*/
	
	public void CreateQuad(Vector4 normal)
	{
		Vector4 positions[] = {
	           	new Vector4(-0.5f, -0.5f, 0),
	           	new Vector4( 0.5f, -0.5f, 0),
			   	new Vector4(-0.5f,  0.5f, 0),
			   	new Vector4( 0.5f,  0.5f, 0),
	        };
		
		Vector4 uvs[] = {
				new Vector4(0,   1.f),
				new Vector4(1.f, 1.f),
				new Vector4(  0, 0.f),
				new Vector4(1.f, 0.f),
	        };

		byte indices[] = {
		        0, 1, 3,
		        0, 3, 2,
	        };
	        
		SetIndicies(indices);
		SetPositions(positions);
		SetUVs(uvs);
	}
	
	public void CreateUnitCube()
	{
		float half = 0.5f;

        Vector4 positions[] = {
               new Vector4(-half, -half, -half),
               new Vector4( half, -half, -half),
    		   new Vector4( half,  half, -half),
			   new Vector4(-half,  half, -half),
			   new Vector4(-half, -half,  half),
			   new Vector4( half, -half,  half),
			   new Vector4( half,  half,  half),
			   new Vector4(-half,  half,  half),
            };
        
        byte indices[] = {
                0, 4, 5,
                0, 5, 1,
                1, 5, 6,
                1, 6, 2,
                2, 6, 7,
                2, 7, 3,
                3, 7, 4,
                3, 4, 0,
                4, 7, 6,
                4, 6, 5,
                3, 0, 1,
                3, 1, 2
        };
        
        SetIndicies(indices);
        SetPositions(positions);
        //SetColours(colours);
	}


	// Buffers to be passed to gl*Pointer() functions
    // must be direct, i.e., they must be placed on the
    // native heap where the garbage collector cannot
    // move them.
    //
    // Buffers with multi-byte datatypes (e.g., short, int, float)
    // must have their byte order set to native order
	
	public void SetUVs(Vector4 uvs[])
	{
		AttributeBuffer buff = Factory.Get().AcquireAttributeBuffer();
		buff.SetFloat(uvs, 2);
		SetAttributeBuffer(Shader.Attribute.Type.UV, buff);
	}
	
	public void SetUVs(float uvs[])
	{
		AttributeBuffer buff = Factory.Get().AcquireAttributeBuffer();
		buff.SetFloat(uvs, 2);
		SetAttributeBuffer(Shader.Attribute.Type.UV, buff);
	}
	
	public void SetPositions(Vector4 positions[])
	{
		AttributeBuffer buff = Factory.Get().AcquireAttributeBuffer();
		buff.SetFloat(positions, 3);
		SetAttributeBuffer(Shader.Attribute.Type.Position, buff);
	}
	
	public void SetPositions(float positions[])
	{
		AttributeBuffer buff = Factory.Get().AcquireAttributeBuffer();
		buff.SetFloat(positions, 3);
		SetAttributeBuffer(Shader.Attribute.Type.Position, buff);
	}
	
	public void SetColours(Vector4 colours[])
	{
		AttributeBuffer buff = Factory.Get().AcquireAttributeBuffer();
		buff.SetFloat(colours, 4);
		SetAttributeBuffer(Shader.Attribute.Type.Colour, buff);
	}
	
	public void SetColours(float colours[])
	{
		AttributeBuffer buff = Factory.Get().AcquireAttributeBuffer();
		buff.SetFloat(colours, 4);
		SetAttributeBuffer(Shader.Attribute.Type.Colour, buff);
	}
	
	public void SetIndicies(byte indices[])
	{
		AttributeBuffer buff = Factory.Get().AcquireAttributeBuffer();
		buff.SetByte(indices);
		SetAttributeBuffer(Shader.Attribute.Type.Index, buff);
	}
		
	public void Bind(Shader shader)
	{
		for (int a = 0; a < Shader.Attribute.Type.Max.ordinal(); ++a)
		{
			Shader.Attribute.Type att = Shader.Attribute.Type.values()[a];
			AttributeBuffer attBuf = GetAttributeBuffer(att);
			
			Shader.Attribute shaderAtt = shader.GetAttribute(att);
			if (shaderAtt != null && attBuf != null)
			{
				shaderAtt.Bind(attBuf);
			}
		}
	}
	
	public void SetTopology(Device.GeometryTopology topology)
	{
		mTopology = topology;
	}
	
	public AttributeBuffer GetAttributeBuffer(Shader.Attribute.Type attribute)
	{
		return mAttributes.get(attribute);
	}
	
	public void SetAttributeBuffer(Shader.Attribute.Type attribute, AttributeBuffer buffer)
	{
		mAttributes.put(attribute, buffer);
	}
	
	public void Render()
	{
		
	}
	
	public void MakeStatic()
	{

	}
	
	public void Reload()
	{
		for (int a = 0; a < Shader.Attribute.Type.Max.ordinal(); ++a)
		{
			Shader.Attribute.Type att = Shader.Attribute.Type.values()[a];
			AttributeBuffer attBuf = (AttributeBuffer) GetAttributeBuffer(att);
			if (attBuf != null)
			{
				attBuf.Reload();
			}
		}
	}
	
	public int AttributeCount()
	{
		return mAttributes.size();
	}
		
	protected Map<Shader.Attribute.Type, AttributeBuffer> mAttributes = new HashMap<Shader.Attribute.Type, AttributeBuffer>();
	protected Device.GeometryTopology 	mTopology	= Device.GeometryTopology.TriangleList;  
}
