package engine.api;

import java.util.HashMap;
import java.util.Map;

import android.opengl.GLES20;
import engine.android.AndroidDevice;
import engine.api.Shader.Uniform;
import engine.math.Matrix4;
import engine.math.Vector4;

public class Material extends Resource
{
	Material()
	{
		SetUniform(Uniform.Type.DiffuseColour, new Vector4(1, 1, 1, 1));
	}
	
	void SetTexture(Texture texture)
	{
		mTexture = texture;
	}
	
	void SetShader(Shader shader)
	{
		mShader = shader;
	}
	
	int Begin()
	{
		return 1;
	}
	
	Shader BeginPass(int pass)
	{
		/*
		public enum Type
		{
			WorldMatrix,
			ViewMatrix,
			ModelViewPerspectiveMatrix,
		}
		*/
		

		mShader.Bind();
		
		Device device = Factory.Get().GetDevice();
		
		Matrix4 world = device.GetMatrix(Device.MatrixMode.Model);
		Matrix4 view = device.GetMatrix(Device.MatrixMode.View);
		Matrix4 projection = device.GetMatrix(Device.MatrixMode.Projection);
		Matrix4 texture = device.GetMatrix(Device.MatrixMode.Texture);
				
		for (int u = 0; u < Uniform.Type.Max.ordinal(); ++u)
		{
			Uniform.Type uniformType = Uniform.Type.values()[u];
			Uniform uniform = mShader.GetUniform(uniformType);
			if (uniform == null)
				continue;
			
			if (mVectorUniform.containsKey(uniformType))
			{
				uniform.Bind(mVectorUniform.get(uniformType));
			}
			
			switch (uniformType)
			{
			case WorldMatrix:
				uniform.Bind(world);
				break;
				
			case ViewMatrix:
				uniform.Bind(view);
				break;
				
			case ProjectionMatrix:
				uniform.Bind(projection);
				break;
				
			case WorldViewProjectionMatrix:
				Matrix4 wvp = world.Multiply(view);
				wvp = wvp.Multiply(projection);
				uniform.Bind(wvp);
				break;
				
			case TextureMatrix:
				uniform.Bind(texture);
				break;
				
			case Texture:
				uniform.Bind(mTexture);
				break;
				
			default:
				break;
			}
		}
	
		// Device state should be a part of each pass
		AndroidDevice temp = (AndroidDevice)device;
		temp.ApplyDeviceState();
		
		// disable culling for testing
		GLES20.glDisable(GLES20.GL_CULL_FACE);
		
		return mShader;
	}
	
	void EndPass()
	{
		
	}
	
	void End()
	{
		
	}
	
	public void Load()
	{
		mShader = mFactory.AcquireShader("default");
		mTexture = mFactory.AcquireTexture(mName);
	}
	
	// TODO: replace with a uniform
	public Texture GetTexture()
	{
		return mTexture;
	}

	public Shader GetShader()
	{
		return mShader;
	}
	
	public void SetUniform(Uniform.Type uniformType, Vector4 vec)
	{
		mVectorUniform.put(uniformType, vec);
	}
	
	Shader	mShader;
	Texture	mTexture;
	
	Map<Uniform.Type, Vector4> mVectorUniform = new HashMap<Uniform.Type, Vector4>();
}
