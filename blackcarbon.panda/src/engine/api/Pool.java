package engine.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import blackcarbon.panda.IProperty;

public class Pool<T>
{
	public Pool(Class<T> classOfT, int size)
	{
		mClassOfT = classOfT;
		resize(size);
	}
	
	public void resize(int size)
	{
		for (int i = 0; i < size; ++i)
		{
			mDeadList.add(createNew());
		}
	}
	
	T createNew()
	{
		try 
		{
			return mClassOfT.newInstance();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public T get()
	{
		if (mDeadList.size() <= 0)
		{
			return null;
		}
		
		T obj = mDeadList.get(0);
		mDeadList.remove(0);
		mLiveList.add(obj);
		return obj;
	}
	
	public T getLive(int index)
	{
		return mLiveList.get(index);
	}
	
	public void release(T t)
	{
		mLiveList.remove(t);
		mDeadList.add(t);
	}
	
	public void getAll()
	{
		while (get() != null)
		{
		}
	}
	
	public Iterator<T> liveIterator()
	{
		return mLiveList.iterator();
	}
	
	public Iterator<T> deadIterator()
	{
		return mDeadList.iterator();
	}
	
	ArrayList<T>	mDeadList = new ArrayList<T>();
	public ArrayList<T>	mLiveList = new ArrayList<T>();
	Class<T>		mClassOfT;
}
