package engine.api;

import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Stack;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import engine.math.Matrix4;
import engine.math.Vector4;

//
// make non-clonable objects private!
//
public class XMLSerialize extends DefaultHandler
{
	public XMLSerialize()
	{
		
	}
	
	public XMLSerialize(Factory factory, String file, String packageName)
	{
		mPackageName.add("");
		mPackageName.add("BlackCarbon.Math");
		mPackageName.add("BlackCarbon.UI");
		mPackageName.add("BlackCarbon.API");
		mPackageName.add(packageName);
		//mName = name;

    	SAXParserFactory spf = SAXParserFactory.newInstance();
    	try 
    	{	
    		SAXParser sp = spf.newSAXParser();

            XMLReader xr = sp.getXMLReader();
            
            xr.setContentHandler(this);
        	xr.setErrorHandler(this);
        	
            //InputStream fis = mDevice.GetResources().openRawResource(mDevice.GetResourceID(name));
        	//String resourceDir = Factory.Get().GetInitParam().resourceDir;
        	InputSource inputStream = new InputSource(factory.GetFileResource(file, "xml"));
        		//new InputSource(new FileInputStream(resourceDir + "/" + file));
            xr.parse(inputStream);
    	}
    	catch (FileNotFoundException e)
    	{
    		System.out.println("[XMLSerialize] exception, file '" + file + "' doesnt exist?");
    		//e.printStackTrace();
    	}
    	catch (Exception e)
    	{
    		System.out.println("[XMLSerialize] " + e.getMessage());
    	}
	}
	
	public void startDocument()
    {

    }

    public void endDocument()
    {
    	for (int i = 0; i < mResolve.size(); ++i)
    	{
	    	try
	    	{
	    		XMLSerializable serObj = XMLSerializable.class.cast(mResolve.get(i));
	    		serObj.Resolve(this);
	    	}
	    	catch (Exception e)
	    	{
	    		// not serializable
	    	}
    	}
    }
    
    public void ParseAttribute(Object obj, String name, String stringValue)
    {
  
    }
    
    @SuppressWarnings("unchecked")
	public void CloneAttribute(Object from, Object to, Field field)
    {	
    	try
    	{
    		// if not public
    		//if (!field.isAccessible())
    		//	return;
    		/*
    		if (field.getType() == float[].class
    			|| field.getType() == float[][].class
    			|| field.getType() == int[].class
    			|| field.getType() == int[].class)
    		{
    			Object value = field.get(from);
		    	field.set(to, value);
		    	mResolve.add(value);
    		}
    		else*/
    		if (field.getType() == Matrix4.class)
    		{
    			Matrix4 value = (Matrix4) field.get(from);
    			Matrix4 clone = new Matrix4(value);
    			field.set(to, clone);
    		}
    		if (field.getType() == Vector4.class)
    		{
    			Vector4 value = (Vector4) field.get(from);
    			Vector4 clone = new Vector4(value);
    			field.set(to, clone);
    		}
    		else if (field.getType() == float.class
	    			|| field.getType() == int.class
	    			|| field.getType() == String.class)
	    	{
    			// DO I NEED TO CLONE THESE?
		    	Object value = field.get(from);
		    	field.set(to, value);
		    	mResolve.add(value);
	    	}
	    	else
	    	{
	    		Object existingValue = field.get(to);
	    		if (existingValue == null)
	    		{
	    			Object value = field.get(from);
	    			
	    			if (value == null)
	    				return;
	    				
	    			Object clone = CloneInternal(value);
	    			field.set(to, clone);
	    		}
	    		else
	    		{
	    			// see if its a collection
	    			try
	    			{
	    				Object possibleCollection = field.get(from);
	    				Collection collection = (Collection)possibleCollection;
	    				
	    				Object toPossibleCollection = field.get(to);
	    				Collection toCollection = (Collection)toPossibleCollection;
	    				
	    				Object[] array = collection.toArray();
	    				for (int i = 0; i < array.length; ++i)
	    				{
	    					Object clone = CloneInternal(array[i]);
	    					toCollection.add(clone);
	    				}
	    			}
	    			catch (Exception e)
	    			{/*
	    				// so a valid value already exists....
	    				// this is untested!
	    				Object value = field.get(from);
		    			
		    			if (value == null)
		    				return;
		    				
		    			Object clone = CloneInternal(value);
		    			if (clone != null)
		    				field.set(to, clone);*/
	    			}
	    		}
	    		
	    		//System.out.println("cloning an object, need to do a deep copy! not yet supported: " + field.getName());
	    	}
    	}
    	catch (Exception e)
    	{
    		//System.out.println("[XMLSerialize] Error attempting to clone: " + field.toString() + " - " + e.getMessage());
    	}
    }
    
    // copy object from to to
    public void Copy(Object from, Object to)
    {    		
    	Field[] fields = from.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; ++i)
		{
			CloneAttribute(from, to, fields[i]);
		}
    }
    
    @SuppressWarnings("unchecked")
	private Object CloneInternal(Object object)
	{
    	Object clone = null;
    	
    	try
    	{
	    	Class c = object.getClass();
	    	clone = c.newInstance();
			
			Field[] fields = c.getDeclaredFields();
			for (int i = 0; i < fields.length; ++i)
			{
				CloneAttribute(object, clone, fields[i]);
			}
			
			mResolve.add(clone);
    	}
    	catch (Exception e)
    	{
    		System.out.println("[XMLSerialize] " + e.getMessage());
    	}
		
    	
		return clone;
	}
    
    public Object Clone(Object object)
	{
    	Object clone = CloneInternal(object);
    	endDocument(); // resolve all values
    	return clone;
	}

    public void startElement(String uri, String name,
		      String qName, Attributes atts)
	{
    	// android fix, not sure why...
    	if (qName.length() == 0)
    		qName = name;
    	
    	// http://scribblethink.org/Computer/java_reflection.html
    	Object object = null;
    	
    	// see if its a field
		if (mCurObject.size() > 0)
		{	
			// if we get here, we assume we are trying to load a variable
			Object obj = mCurObject.peek();
			if (obj == null)
				return;
			
			Field field = GetField(obj, qName);
			if (field == null)
			{
				// dont return, push on a dummy object to get popped on the end element
				System.out.println("[XMLSerialize.startElement] '" + qName + "' failed to get field");
				//return;
				object = new Object();
			}
			
			// check for a 'class' attribute
			String className = null;
			for (int a = 0; a < atts.getLength(); ++a)
			{
				String attName = atts.getLocalName(a);
				if (!attName.equalsIgnoreCase("class"))
					continue;
					
				String attValue = atts.getValue(a);
				className = attValue;
			}
			
			if (className != null)
				object = CreateClass(className);
			
			if (object == null)
			{
				try
				{
					object = field.getType().newInstance();
				}
				catch (Exception e)
				{
					try
					{
						object = field.get(obj);
					}
					catch (Exception e1)
					{
						System.out.println("[XMLSerializable.startElement] couldnt instance class");
					}
				}
			}
		}

		// its not a field, so it must be a class
		if (object == null) 
			object = CreateClass(qName);
		
		if (object != null)
		{
			// set up parent and child pointers
			@SuppressWarnings("unused")
			Object parentObj = null;
			if (mCurObject.size() > 0)
				parentObj = mCurObject.peek();
			/*
			if (parentObj != null)
			{
				try
		    	{
		    		XMLSerializable serObj = XMLSerializable.class.cast(object);
		    		serObj.SetParent(this, parentObj);
		    	}
		    	catch (Exception e)
		    	{
		    		// not serializable
		    	}
		    	
		    	try
		    	{
		    		XMLSerializable serParentObj = XMLSerializable.class.cast(parentObj);
		    		serParentObj.InsertChild(this, object);
		    	}
		    	catch (Exception e)
		    	{
		    		// not serializable
		    	}
			}*/
			
			mCurObject.add(object);
			/*
			// any attributes are member variables of the class
			// classes are assigned via new elements somehow
			for (int a = 0; a < atts.getLength(); ++a)
			{
				String attName = atts.getLocalName(a);
				String attValue = atts.getValue(a);
				
				ParseAttribute(object, attName, attValue);
			}*/
		}
	}
    
	@SuppressWarnings("unchecked")
	public void endElement(String uri, String name, String qName)
	{	
		// android fix, not sure why...
    	if (qName.length() == 0)
    		qName = name;
    	
		if (mCurObject.size() <= 0)
			return;
		
		// pop current object is it matches the name
		// push it in to the finished loading list
		// TODO - may need to push it to a vector of another object
		Object object = mCurObject.peek();
		if (object == null)
			return;
		

		mCurObject.pop();
		mResolve.add(object);
		
		// if this was a root object, add it to the object list
		if (mCurObject.size() <= 0)
		{
			mObject.add(object);
			return;
		}

		

		// set the field
		Object parentObj = mCurObject.peek();
		Field field = GetField(parentObj, qName);
		if (field == null)
		{
			System.out.println("[XMLSerialize.endElement] '" + qName + "' failed to get field");
			return;
		}

		
		try
		{
			Object possibleCollection = field.get(parentObj);
			Collection collection = (Collection)possibleCollection;
			collection.add(object);
		}
		catch (Exception e)
		{
			try
			{
				field.set(parentObj, object);
			}
			catch (Exception e1)
			{
				System.out.println("[XMLSerializable.endElement] couldnt set object");
			}
		}
	}

	public void characters(char ch[], int start, int length)
	{
		try 
    	{
			if (mCurObject.size() <= 0)
				return;
			
			Object object = mCurObject.peek();
			if (object == null)
				return;
			
			// string is stupid!
 			mCurObject.pop();
 			
			String stringValue = new String(ch, start, length);
	
	 		if (object.getClass() == Float.class)
	 		{
		 		float value = Float.valueOf(stringValue.trim()).floatValue();	
		 		object = value;
	 		}
	 		else if (object.getClass() == Integer.class)
	 		{
		 		int value = Integer.valueOf(stringValue.trim()).intValue();
		 		object = value;
	 		}
	 		else if (object.getClass() == String.class)
	 		{ 
	 			object = stringValue;
	 		} 	
	 		else if (object.getClass() == Vector4.class)
	 		{
	 			((Vector4)object).Copy(new Vector4(stringValue));
	 		}
	 		else if (object.getClass() == Matrix4.class)
	 		{
	 			((Matrix4)object).Copy(new Matrix4(stringValue));
	 		}
	 		
	 		mCurObject.push(object);
    	}
 		catch (Exception e)
    	{
 			//e.printStackTrace();
 			System.out.println("[XMLSerialize.characters] failed to set field");
    	}
	}

    @SuppressWarnings("unchecked")
	Object CreateClass(String name, int packageIndex)
    {
    	
    	try
    	{
    		Class c = null;
    		
    		if (mPackageName.get(packageIndex).length() > 0)
    			c = Class.forName(mPackageName.get(packageIndex) + "." + name);
    		else
    			c = Class.forName(name);
    			
			Object object = c.newInstance();
			return object;
    	}
    	catch (ClassNotFoundException e)
    	{
    		// ignore	
    	}
    	catch (Exception e)
    	{
    		System.out.println("[XMLSerialize.CreateClass] failed to create instance of class '" + mPackageName.get(packageIndex) + "." + name + "'");
    	}
    	
    	return null;
    }
    
    Object CreateClass(String name)
    {    	
    	// ignore, special case
		if (name.equalsIgnoreCase("root"))
			return null;
		
    	for (int i = 0; i < mPackageName.size(); ++i)
    	{
    		Object object = CreateClass(name, i);
    		if (object != null)
    			return object;
    	}
    	
    	System.out.println("[XMLSerialize.CreateClass] Class '" + name + "' not found, check it exists");
    	return null;
    }
    
    Field GetField(Object object, String name)
    {
    	Field[] fields = object.getClass().getFields();
		for (int i = 0; i < fields.length; ++i)
		{
			Field f = fields[i];
			if (f.getName().equalsIgnoreCase(name)
					|| f.getName().equalsIgnoreCase("m_" + name)
					|| f.getName().equalsIgnoreCase("m" + name))
			{
				return f;
			}
		}
		
		System.out.println("[XMLSerialize.GetField] '" + name + "' failed to get field");
		return null;
    }
	
	public int GetSize()
	{
		return mObject.size();
	}
	
	public Object Get(int index)
	{
		return mObject.get(index);
	}
	 
	 private Stack<Object>	mCurObject 		= new Stack<Object>(); // currently loading objects
	 private Vector<Object> mObject			= new Vector<Object>();	// array of root level loaded objects
	 
	 private Vector<Object> mResolve		= new Vector<Object>(); // objects to attempt to resolve
	 
	 private Vector<String>	mPackageName	= new Vector<String>();
	 //private Field 	mCurField;
}
