package engine.android;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.view.View;
import engine.api.*;

public class AndroidFactory extends Factory
{
	public AndroidFactory()
	{	
		mPlatform = Platform.Android;
	}
	
	public void SetContext(Context context)
	{
		mContext = context;
	}
		
	public RenderBuffer CreateRenderBuffer()
	{
		RenderBuffer resource = new AndroidRenderBuffer();
		resource.mFactory = this;
		return resource;
	}
	
	public RenderBuffer AcquireRenderBuffer(String name)
	{
		for (int i = 0; i < mRenderBuffer.size(); ++i)
		{
			RenderBuffer resource = mRenderBuffer.get(i);
			if (resource.mName.equalsIgnoreCase(name))
				return resource;
		}
		
		RenderBuffer resource = new AndroidRenderBuffer();
		resource.mFactory = this;
		resource.mName = name;
		mRenderBuffer.add(resource);
		return resource;
	}
	
	public Texture AcquireTexture(String name)
	{
		for (int i = 0; i < mTexture.size(); ++i)
		{
			Texture resource = mTexture.get(i);
			if (resource.mName.equalsIgnoreCase(name))
				return resource;
		}
		
		Texture resource = new AndroidTexture();
		resource.mFactory = this;
		resource.mName = name;
		resource.Load();
		mTexture.add(resource);
		return resource;
	}
	
	public Shader AcquireShader(String name)
	{
		for (int i = 0; i < mShader.size(); ++i)
		{
			Shader resource = mShader.get(i);
			if (resource.mName.equalsIgnoreCase(name))
				return resource;
		}
		
		Shader resource = new AndroidShader();
		resource.mFactory = this;
		resource.mName = name;
		resource.Load();
		mShader.add(resource);
		return resource;
	}
	
	public RenderBuffer.AttributeBuffer AcquireAttributeBuffer()
	{
		return new AndroidRenderBuffer.AttributeBuffer();
	}
	
	public void ReleaseAttributeBuffer(RenderBuffer.AttributeBuffer resource)
	{
		
	}
		
	public InputStream GetFileResource(String name, String extension)
	{
		AssetManager am = mContext.getAssets();
		try 
		{
			return am.open(name);
		} 
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	// Android specific
	public Resources GetResources()
	{
		return mContext.getResources();
	}
	
	static public void waitForDebugger()
	{
		//if (mDebug)
		{
			System.out.println("Waiting for debugger to attach...");
			android.os.Debug.waitForDebugger();
			System.out.println("Debugger attached");
		}
	}
	
		
	protected	Context		mContext;
}
