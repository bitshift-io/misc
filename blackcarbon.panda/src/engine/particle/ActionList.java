package engine.particle;

import java.util.ArrayList;
import java.util.Iterator;

/*!
 * ActionList
 * List of actions to perform on ParticleList
 * ActionList is an action so these can be chained in a tree like fashion
 */
public class ActionList extends Action
{	
	ArrayList<Action>	mList = new ArrayList<Action>();
	
	public Iterator<Action> iterator()
	{
		return mList.iterator();
	}
	
	public void execute(ParticleList particleList)
	{
		Iterator<Action> it = iterator();
		while (it.hasNext())
		{
			Action a = it.next();
			a.execute(particleList);
		}
	}
	
	public void update(float deltaTime)
	{
		Iterator<Action> it = iterator();
		while (it.hasNext())
		{
			Action a = it.next();
			a.update(deltaTime);
		}
	}
	
	/*!
	 * Find an action by its class
	 */
	public Action findAction(Class<Action> actionClass)
	{
		Iterator<Action> it = iterator();
		while (it.hasNext())
		{
			Action a = it.next();
			if (a.getClass() == actionClass)
			{
				return a;
			}
		}
		
		return null;
	}
	
	public void add(Action a)
	{
		mList.add(a);
	}
	
	public void remove(Action a)
	{
		mList.remove(a);
	}
}
