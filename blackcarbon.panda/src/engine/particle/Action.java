package engine.particle;

/*!
 * Manipulate particles in some fashion
 * eg. Bounce, gravity, explosions, sort, follow
 * 
 * if we want to make this multithreaded, change the execute to take an begin/end iterators
 * each thread will perform the action on a subset of the particle list
 */
public class Action 
{
	boolean mEnabled = true;
	
	void execute(ParticleList particleList)
	{
		
	}
	
	void update(float deltaTime)
	{
		
	}
	
	public boolean enabled()
	{
		return mEnabled;
	}
	
	public void setEnabled(boolean enabled)
	{
		mEnabled = enabled;
	}
}
