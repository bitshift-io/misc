package engine.particle;

import java.util.Iterator;

import engine.math.Vector4;

public class DampAction extends Action
{
	public float mDamping = 0.0f; // percent of velocity to be wipped off per second
	float mDeltaTime;
	
	void execute(ParticleList particleList)
	{
		if (!mEnabled)
		{
			return;
		}
		
		Iterator<Particle> it = particleList.iterator();
		while (it.hasNext())
		{
			Particle particle = it.next();
			
			Vector4 damp = particle.mVelocity.Multiply(-1.f * mDeltaTime * mDamping);
			particle.mVelocity = particle.mVelocity.Add(damp);
		}
	}
	
	void update(float deltaTime)
	{
		mDeltaTime = deltaTime;
	}
}
