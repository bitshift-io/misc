package engine.particle;

import engine.math.Matrix4;

public class PositionAction extends Action
{
	enum PositionType
	{
		PT_Centre,
		PT_Vertex,
		PT_Edge,
		PT_Surface,
	}
	
	PositionType	mPositionType;
	float			mInheritSpeedMovementPercent;
	Matrix4			mPreviousTransform;
	//Mesh*			mMesh;

}
