package engine.particle;

import java.util.ArrayList;
import java.util.Iterator;

/*!
 * Action list/particle list pair
 * the action list is applied to the particle list
 */
public class ParticleActionGroup
{
	ActionList	mActionList;
	ParticleList mParticleList;
	
	public ParticleActionGroup(ActionList al, ParticleList pl)
	{
		mActionList = al;
		mParticleList = pl;
	}
	
	public void updateAndExec(float deltaTime)
	{
		mActionList.update(deltaTime);
		mActionList.execute(mParticleList);
		mParticleList.update(deltaTime);
	}
		
	public ParticleList particleList() 
	{
		return mParticleList;
	}
	
	public ActionList actionList()
	{
		return mActionList;
	}
}
