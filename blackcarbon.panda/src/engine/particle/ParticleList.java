package engine.particle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import engine.api.Pool;
import engine.math.Matrix4;

/*!
 * Group of particles for Actions to work on
 */
public class ParticleList
{
	Pool<Particle>	mPool;
	ArrayList<Particle> mDespawnList = new ArrayList<Particle>();
	ArrayList<Particle> mSpawnList = new ArrayList<Particle>();
	
	public ParticleList(Class particleClass, int poolSize)
	{
		mPool = new Pool<Particle>(particleClass, poolSize);
	}
	
	public Particle spawn()
	{
		Particle p = mPool.get();
		if (p == null)
		{
			return null;
		}
		
		// to: randomise the particle
		// scale, life, position, velocity
		mSpawnList.add(p);
		return p;
	}
	
	public void despawn(Particle p)
	{
		mDespawnList.add(p);
	}
	
	public void update(float deltaTime)
	{				
		mSpawnList.clear();
		
		// release dead particle to pool
		while (mDespawnList.size() > 0)
		{
			Particle p = mDespawnList.get(0);
			mDespawnList.remove(0);
			mPool.release(p);
		}
		
		// update particles
		Iterator<Particle> it = iterator();
		while (it.hasNext())
		{
			Particle p = it.next();
			p.update(deltaTime);
		}
	}
	
	public void draw(Matrix4 transform)
	{
		// draw particles
		Iterator<Particle> it = iterator();
		while (it.hasNext())
		{
			Particle p = it.next();
			p.draw(transform);
		}
	}
	
	public Particle get(int index)
	{
		return mPool.getLive(index);
	}
	
	public Iterator<Particle> iterator()
	{		      
		return mPool.liveIterator();
	}
	
	public Iterator<Particle> deadIterator()
	{		      
		return mPool.deadIterator();
	}
}
