package engine.particle;

import java.util.Iterator;

import engine.api.Camera;
import engine.math.Matrix4;
import engine.math.Vector4;

// kills off particle that leave the camera frustum
public class OffCameraDeathAction extends Action
{
	public Camera			mCamera;
	public Matrix4			mTransform; // transform of the particle system we want to cull
	public float			mBoundRadius; // bound of particles
	public float			mAge;		// age of particles killed
	
	public void execute(ParticleList particleList)
	{
		if (!mEnabled)
		{
			return;
		}
		
		float halfWidth = (mCamera.Width() * 0.5f) + mBoundRadius;
		float halfHeight = (mCamera.Height() * 0.5f) + mBoundRadius;
		
		Iterator<Particle> it = particleList.iterator();
		while (it.hasNext())
		{
			Particle p = it.next();
			if (mAge < 0.f || p.mAge > mAge)
			{
				Vector4 particlePos = mTransform.GetTranslation().Add(p.mTransform.GetTranslation());
				if (particlePos.GetX() > halfWidth || particlePos.GetX() < -halfWidth
						|| particlePos.GetY() > halfHeight || particlePos.GetY() < -halfHeight)
				{
					p.despawn();
					particleList.despawn(p);
				}
			}
		}
	}
}
