package engine.particle;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import engine.math.Matrix4;

public class ParticleSystem
{		
	ArrayList<ParticleActionGroup> mList = new ArrayList<ParticleActionGroup>();
	HashSet<ParticleList> mParticleList = new HashSet<ParticleList>();
	
	public ParticleSystem()
	{	
	}
		
	public void update(float deltaTime)
	{
		Iterator<ParticleActionGroup> it = mList.iterator();
		while (it.hasNext())
		{
			ParticleActionGroup pag = it.next();
			pag.updateAndExec(deltaTime);
		}
	}
		
	public void draw(Matrix4 transform)
	{
		Iterator<ParticleList> it = particleListIterator();
		while (it.hasNext())
		{
			ParticleList pl = it.next();
			pl.draw(transform);
		}
	}
	
	public void add(ParticleActionGroup pag)
	{
		mList.add(pag);
		mParticleList.add(pag.particleList());
	}
	
	public void remove(ParticleActionGroup pag)
	{
		mList.remove(pag);
		mParticleList.remove(pag.particleList());
	}
	
	public Iterator<ParticleList> particleListIterator()
	{
		return mParticleList.iterator();
	}
	
	public ParticleActionGroup get(int index)
	{
		return mList.get(index);
	}
	
	public int size()
	{
		return mList.size();
	}
}
