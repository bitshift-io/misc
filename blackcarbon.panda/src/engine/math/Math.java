package engine.math;

import java.util.Random;

public class Math
{
	static public float Epsilon 	= 0.001f;
	static public float PI 			= 3.1415f; // fixme! more details
	static public float PI2			= PI * 2;
	static public Random rand		= new Random();
	
	// stuff for noise
	static int B = 0x100;
	static int BM = 0xff;
	
	static int N = 0x1000;
	static int NP = 12;   /* 2^N */
	static int NM = 0xfff;
	
	static int[] p = new int[B + B + 2];
	static Vector4[] g3 = new Vector4[B + B + 2];
	static Vector4[] g2 = new Vector4[B + B + 2];
	static float[] g1 = new float[B + B + 2];
	static boolean start = true;
	// end stuff for noise
	
	public static float Sqrt(float value)
	{
		return (float)java.lang.Math.sqrt(value);
	}
	
	public static float Abs(float value)
	{
		return (float)java.lang.Math.abs(value);
	}
	
	public static int Abs(int value)
	{
		return java.lang.Math.abs(value);
	}
	
	public static long Abs(long value)
	{
		return java.lang.Math.abs(value);
	}
	
	public static float Sin(float value)
	{
		return (float)java.lang.Math.sin(value);
	}
	
	public static float Cos(float value)
	{
		return (float)java.lang.Math.cos(value);
	}
	
	public static float ACos(float value)
	{
		return (float)java.lang.Math.acos(value);
	}
	
	public static float Tan(float value)
	{
		return (float)java.lang.Math.tan(value);
	}
	
	public static float DegToRad(float value)
	{
		return (float)java.lang.Math.toRadians(value);
	}
	
	public static float Clamp(float min, float value, float max)
	{
		if (value < min)
			return min;

		if (value > max)
			return max;

		return value;
	}
	
	public static float Floor(float value)
	{
		return (float)java.lang.Math.floor(value);
	}
	
	// get decimal value from float
	public static float Decimal(float value)
	{
		int integer = (int)value;
		float decimal = value - integer;
		return decimal;
	}
	
	public static float Max(float a, float b)
	{
		return (float)java.lang.Math.max(a, b);
	}
	
	public static float Min(float a, float b)
	{
		return (float)java.lang.Math.min(a, b);
	}
	
	public static float Random(float min, float max)
	{
		return min + (Abs(rand.nextFloat()) * (max - min));
	}
	
	public static long Random(long min, long max)
	{
		long delta = max - min;
		if (delta <= 0)
		{
			return min;
		}
		
		return min + (Abs(rand.nextLong()) % delta);
	}
	
	public static int Random(int min, int max)
	{
		int delta = max - min;
		if (delta <= 0)
		{
			return min;
		}
		
		return min + (Abs(rand.nextInt()) % delta);
	}
	
	public static float Pow(float a, float b)
	{
		return (float)java.lang.Math.pow(a, b);
	}
	
	public static float Exp(float e)
	{
		return (float)java.lang.Math.exp(e);
	}

	// stuff for noise
	static void initNoise()
	{
		int i, j, k;
		
		for (i = 0; i < B + B + 2; ++i)
		{
			g3[i] = new Vector4();
			g2[i] = new Vector4();
		}
	
		for (i = 0 ; i < B ; i++) 
		{
			p[i] = i;
	
			g1[i] = (float)((Abs(rand.nextInt()) % (B + B)) - B) / B;
	
			for (j = 0 ; j < 2 ; j++)
				g2[i].v[j] = (float)((Abs(rand.nextInt()) % (B + B)) - B) / B;
			g2[i].Normalize3();
	
			for (j = 0 ; j < 3 ; j++)
				g3[i].v[j] = (float)((Abs(rand.nextInt()) % (B + B)) - B) / B;
			g3[i].Normalize3();
		}
	
		for (; i >= 0; --i)
		{
			k = p[i];
			j = Abs(rand.nextInt()) % B;
			p[i] = p[j];
			p[j] = k;
		}
	
		for (i = 0 ; i < B + 2 ; i++) 
		{
			p[B + i] = p[i];
			g1[B + i] = g1[i];
			for (j = 0 ; j < 2 ; j++)
				g2[B + i].v[j] = g2[i].v[j];
	
			for (j = 0 ; j < 3 ; j++)
				g3[B + i].v[j] = g3[i].v[j];
		}
	}

	static class NoiseData
	{
		int b0;
		int b1;
		float r0;
		float r1;
	}
	
	static float setup(int i, NoiseData nd, Vector4 vec)
	{
		float t = vec.v[i] + N;
		nd.b0 = ((int)t) & BM;
		nd.b1 = (nd.b0+1) & BM;
		nd.r0 = t - (int)t;
		nd.r1 = nd.r0 - 1.f;
		return t;
	}
	
	public static float Noise1(float arg)
	{
		NoiseData x = new NoiseData();
		float sx, t, u, v;
		Vector4 vec = new Vector4(arg, 0.f, 0.f, 0.f);

		if (start) 
		{
			start = false;
			initNoise();
		}

		t = setup(0, x, vec);

		sx = s_curve(x.r0);
		u = x.r0 * g1[ p[ x.b0 ] ];
		v = x.r1 * g1[ p[ x.b1 ] ];

		return lerp(sx, u, v);
	}
	
	public static float Noise2(Vector4 vec)
	{
		NoiseData x = new NoiseData();
		NoiseData y = new NoiseData();
		
		int b00, b10, b01, b11;
		float sx, sy, a, b, t, u, v;
		int i, j;

		if (start) 
		{
			start = false;
			initNoise();
		}

		t = setup(0, x, vec);
		t = setup(1, y, vec);

		i = p[ x.b0 ];
		j = p[ x.b1 ];

		b00 = p[ i + y.b0 ];
		b10 = p[ j + y.b0 ];
		b01 = p[ i + y.b1 ];
		b11 = p[ j + y.b1 ];

		sx = s_curve(x.r0);
		sy = s_curve(y.r0);

		u = g2[ b00 ].Dot3(new Vector4(x.r0,y.r0, 0.f, 0.f));
		v = g2[ b10 ].Dot3(new Vector4(x.r1,y.r0, 0.f, 0.f));
		a = lerp(sx, u, v);

		u = g2[ b01 ].Dot3(new Vector4(x.r0,y.r1, 0.f, 0.f));
		v = g2[ b11 ].Dot3(new Vector4(x.r1,y.r1, 0.f, 0.f));
		b = lerp(sx, u, v);

		return lerp(sy, a, b);
	}
	
	public static float Noise3(Vector4 vec)
	{
		NoiseData x = new NoiseData();
		NoiseData y = new NoiseData();
		NoiseData z = new NoiseData();
		
		int b00, b10, b01, b11;
		float sy, sz, a, b, c, d, t, u, v;
		int i, j;

		if (start) 
		{
			start = false;
			initNoise();
		}

		t = setup(0, x, vec);
		t = setup(1, y, vec);
		t = setup(2, z, vec);

		i = p[ x.b0 ];
		j = p[ x.b1 ];

		b00 = p[ i + y.b0 ];
		b10 = p[ j + y.b0 ];
		b01 = p[ i + y.b1 ];
		b11 = p[ j + y.b1 ];

		t  = s_curve(x.r0);
		sy = s_curve(y.r0);
		sz = s_curve(z.r0);

		u = g3[ b00 + z.b0 ].Dot3(new Vector4(x.r0,y.r0,z.r0));
		v = g3[ b10 + z.b0 ].Dot3(new Vector4(x.r1,y.r0,z.r0));
		a = lerp(t, u, v);

		u = g3[ b01 + z.b0 ].Dot3(new Vector4(x.r0,y.r1,z.r0));
		v = g3[ b11 + z.b0 ].Dot3(new Vector4(x.r1,y.r1,z.r0));
		b = lerp(t, u, v);

		c = lerp(sy, a, b);

		u = g3[ b00 + z.b1 ].Dot3(new Vector4(x.r0,y.r0,z.r1));
		v = g3[ b10 + z.b1 ].Dot3(new Vector4(x.r1,y.r0,z.r1));
		a = lerp(t, u, v);

		u = g3[ b01 + z.b1 ].Dot3(new Vector4(x.r0,y.r1,z.r1));
		v = g3[ b11 + z.b1 ].Dot3(new Vector4(x.r1,y.r1,z.r1));
		b = lerp(t, u, v);

		d = lerp(sy, a, b);

		return lerp(sz, c, d);
	}
	
	static float s_curve(float t) 
	{ 
		return t * t * (3.f - 2.f * t);
	}
	
	public static float lerp(float t, float a, float b)
	{
		return a + t * (b - a);
	}
}
