package engine.math;

import engine.math.Quaternion;
import engine.math.Vector4;

public class Quaternion
{
	public float[] q = new float[4];
	
	public Quaternion()
	{
		
	}
	
	public Quaternion(Vector4 vector)
	{
		q[0] = vector.v[0];
		q[1] = vector.v[1];
		q[2] = vector.v[2];
		q[3] = vector.v[3];
	}
	
	public Quaternion(float x, float y, float z, float w)
	{
		q[0] = x;
		q[1] = y;
		q[2] = z;
		q[3] = w;
	}
	
	public Quaternion(Vector4 axis, float angle)
	{
		CreateFromAxisAngle(axis, angle);
	}
	
	public void Normalise()
	{
		float mag = Magnitude();
		float inverseMag = 1.f / mag;
		q[0] *= inverseMag;
		q[1] *= inverseMag;
		q[2] *= inverseMag;
		q[3] *= inverseMag;
	}
	
	public float Magnitude()
	{
		return Math.Sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
	}
	
	public void CreateFromAxisAngle(Vector4 axis, float angle)
	{
		float s = Math.Sin(angle * 0.5f);
		q[0] = axis.GetX() * s;
		q[1] = axis.GetY() * s;
		q[2] = axis.GetZ() * s;
		q[3] = Math.Cos(angle * 0.5f);

		Normalise();
	}
	
	public float Dot(Quaternion rhs)
	{
		float dot = 0;

		for( int i = 0; i < 4; ++i )
			dot += q[i] * rhs.q[i];

		return dot;
	}
	
	public Quaternion Interpolate(Quaternion to, float weight)
	{
		// http://www.gamasutra.com/features/19980703/quaternions_01.htm

		Quaternion temp = new Quaternion();
	    float omega, cosom, sinom, scale0, scale1;

	    // calc cosine
	    cosom = Dot(to);

	    // adjust signs (if necessary)
	    if (cosom < 0.0)
		{
			cosom = -cosom;
			temp.q[0] = -to.q[0];
			temp.q[1] = -to.q[1];
			temp.q[2] = -to.q[2];
			temp.q[3] = -to.q[3];
	    }
		else
		{
			temp.q[0] = to.q[0];
			temp.q[1] = to.q[1];
			temp.q[2] = to.q[2];
			temp.q[3] = to.q[3];
	    }

	    // calculate coefficients
	   if ((1.0f - cosom) > 0.0001f)
	   {
	        // standard case (slerp)
	        omega = Math.ACos(cosom);
	        sinom = Math.Sin(omega);
	        scale0 = Math.Sin((1.0f - weight) * omega) / sinom;
	        scale1 = Math.Sin(weight * omega) / sinom;
	   }
	   else
	   {
			// "from" and "to" quaternions are very close
			//  ... so we can do a linear interpolation
	        scale0 = 1.0f - weight;
	        scale1 = weight;
	    }

		Quaternion res = new Quaternion();

		// calculate final values
		res.q[0] = scale0 * q[0] + scale1 * temp.q[0];
		res.q[1] = scale0 * q[1] + scale1 * temp.q[1];
		res.q[2] = scale0 * q[2] + scale1 * temp.q[2];
		res.q[3] = scale0 * q[3] + scale1 * temp.q[3];

		return res;
	}

	public float GetX()
	{
		return q[0];
	}
	
	public float GetY()
	{
		return q[1];
	}
	
	public float GetZ()
	{
		return q[2];
	}
	
	public float GetW()
	{
		return q[3];
	}
	
	public void SetX(float x)
	{
		q[0] = x;
	}
	
	public void SetY(float y)
	{
		q[1] = y;
	}
	
	public void SetZ(float z)
	{
		q[2] = z;
	}
	
	public void SetW(float w)
	{
		q[2] = w;
	}
}
