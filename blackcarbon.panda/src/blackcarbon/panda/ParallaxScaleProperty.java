package blackcarbon.panda;

import engine.math.Matrix4;
import engine.math.Vector4;

public class ParallaxScaleProperty extends IProperty
{
	public ParallaxScaleProperty(float aspectRatio, Vector4 scale)
	{
		mAspectRatio = aspectRatio;
		mScale = scale;
	}

	public Matrix4 transform(Matrix4 transform)
	{
		LiveWallpaperService service = LiveWallpaperService.Get();
    	
		// scale image
    	float scaleX = service.mCamera.Height() * mAspectRatio * mScale.GetX();
    	float scaleY = service.mCamera.Height() * mScale.GetY();
    	transform.SetScale(new Vector4(scaleX, scaleY, 1));
    	return transform;
	}
	
	float mAspectRatio;
	Vector4 mScale;
}
