package blackcarbon.panda;

import engine.api.Device;
import engine.api.Factory;
import engine.api.RenderBuffer;
import engine.api.Texture;
import engine.api.TextureAnimation;
import engine.math.Math;
import engine.math.Matrix4;

public class BlinkProperty extends IProperty
{	
	public BlinkProperty()
	{		
		mTexturedQuad = new TexturedQuadProperty("panda_blink.png");
		mTextureAnimation = new TextureAnimation(
				TextureAnimation.AF_Horizontal | TextureAnimation.AF_NoLoop | TextureAnimation.AF_Bounce, 4, 1);
		mTextureAnimation.SetFrameRate(60);
	}
	
	public void setLayer(ILayer layer)
	{
		mTexturedQuad.setLayer(layer);
		super.setLayer(layer);
	}
	
	public void update(float deltaTime)
	{
		mTextureAnimation.Update(deltaTime);
		
		mTime -= deltaTime;
		if (mTime <= 0.f)
		{
			mTextureAnimation.Reset();
			mTime = Math.Random(mTimeMin, mTimeMax);
		}		
	}
	
	public void draw(Matrix4 transform)
	{
		if ((mTextureAnimation.Flags() & TextureAnimation.AF_Playing) == 0)
		{
			return;
		}
		
		mTextureAnimation.Bind();
		mTexturedQuad.draw(transform);
		
		// remove texture matrix
		Matrix4 identity = new Matrix4(Matrix4.Init.Identity);
		Device device = Factory.Get().GetDevice();
		device.SetMatrix(identity, Device.MatrixMode.Texture);
	}
	
	float	mTimeMax		= 2.0f;
	float	mTimeMin		= 6.0f;
	float	mTime;
	
	TextureAnimation		mTextureAnimation;
	TexturedQuadProperty 	mTexturedQuad;
}