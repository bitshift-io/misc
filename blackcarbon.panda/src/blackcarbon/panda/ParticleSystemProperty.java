package blackcarbon.panda;

import java.util.Iterator;

import engine.api.Device;
import engine.api.Factory;
import engine.api.TextureAnimation;
import engine.api.Device.DeviceState;
import engine.math.Math;
import engine.math.Matrix4;
import engine.math.Vector4;
import engine.particle.ActionList;
import engine.particle.BirthAction;
import engine.particle.DeathAction;
import engine.particle.ParticleActionGroup;
import engine.particle.ParticleList;
import engine.particle.ParticleSystem;
import engine.api.Shader;

public class ParticleSystemProperty extends IProperty
{
	public static class Particle extends engine.particle.Particle
	{		
		public Particle()
		{		
		}
		
		public void Init(String textureName, int frameCount, int variationCount, float frameRate)
		{
			mDrawProperty = new TexturedQuadProperty(textureName);
			mTextureAnimation = new TextureAnimation(
					TextureAnimation.AF_Horizontal, frameCount, variationCount);
			mTextureAnimation.SetFrameRate(frameRate);
			mTextureAnimation.Reset();
		}
				
		public void setLayer(ILayer layer)
		{
			mDrawProperty.setLayer(layer);
		}

		public void update(float deltaTime)
		{			
			mVelocity.SetZ(0.f);
			super.update(deltaTime);
			mTextureAnimation.Update(deltaTime);
		}
		
		public void draw(Matrix4 transform)
		{
			Device device = Factory.Get().GetDevice();
			Matrix4 particleTransform = new Matrix4(transform);
			particleTransform.SetTranslation(transform.GetTranslation().Add(mTransform.GetTranslation()));
			
			// modify colour
			mDrawProperty.mMaterial.SetUniform(Shader.Uniform.Type.DiffuseColour, mColour);
			
			mTextureAnimation.Bind();
			mDrawProperty.draw(particleTransform);
			
			// remove texture matrix
			Matrix4 identity = new Matrix4(Matrix4.Init.Identity);
			device.SetMatrix(identity, Device.MatrixMode.Texture);
		}

		public void spawn()
		{
			super.spawn();
			
			// pick a random spawn spot off the screen
			LiveWallpaperService service = LiveWallpaperService.Get();
			float halfWidth = service.mCamera.Width() / 2.f;
			float halfHeight = service.mCamera.Height() / 2.f;
			float centerFocus = 1.0f; // lower is more focused around center of screen (Range 0-1)
			float widthMultiplier = 2.0f; // this will spawn them offscreen, but the width of live wallpaper is 2 * screen width
			
			mTransform.SetTranslation(new Vector4(Math.Random(-halfWidth, halfWidth) * widthMultiplier, Math.Random(-halfHeight, halfHeight) * centerFocus));
			mVelocity = new Vector4(0.f, 0.f); // screen heights per second
			
			// apply particle randomization
			mTextureAnimation.SetCurrentHeight(Math.Random(0, mTextureAnimation.Height()));
		}
		
		TexturedQuadProperty mDrawProperty;
		TextureAnimation mTextureAnimation;
	}
		
	public ParticleSystemProperty()
	{
	}
	
	public void Init(ActionList al, Class particleClass, int poolSize)
	{
		ParticleList pl = new ParticleList(particleClass, poolSize);		
		mParticleSystem.add(new ParticleActionGroup(al, pl));
	}
	
	public void setLayer(ILayer layer)
	{
		ParticleList pl = mParticleSystem.get(0).particleList();
		Iterator<engine.particle.Particle> it = pl.deadIterator();
		while (it.hasNext())
		{
			Particle p = (Particle)it.next();
			p.setLayer(layer);
		}
		
		super.setLayer(layer);
	}
	
	public void update(float deltaTime)
	{
		mParticleSystem.update(deltaTime);
	}
	
	public float scale()
	{
		LiveWallpaperService service = LiveWallpaperService.Get();
		return mScale * service.mCamera.Height();
	}
	
	public void draw(Matrix4 transform)
	{
		transform.SetScale(new Vector4(scale(), scale(), 1));
		mParticleSystem.draw(transform);
	}
	
	ParticleSystem mParticleSystem = new ParticleSystem();
	float 	mScale = 1.0f;
}
