package blackcarbon.panda;

import java.util.Iterator;

import blackcarbon.panda.FireflyParticleSystem.FireflyParticle;
import blackcarbon.panda.ILayer.Event;

import engine.api.TextureAnimation;
import engine.math.Math;
import engine.math.Matrix4;
import engine.math.Vector4;
import engine.particle.ActionList;
import engine.particle.BirthAction;
import engine.particle.DampAction;
import engine.particle.DeathAction;
import engine.particle.ForceAction;
import engine.particle.OffCameraBirthAction;
import engine.particle.OffCameraDeathAction;
import engine.particle.ParticleActionGroup;
import engine.particle.ParticleList;
import engine.particle.ParticleSystem;
import engine.particle.SpeedClamp;
import engine.particle.WindAction;

public class ButterflyParticleSystem extends ParticleSystemProperty
{
	static float mBoundRadius = 0.1f;
	
	public static class ButterflyParticle extends ParticleSystemProperty.Particle
	{
		public ButterflyParticle()
		{		
			Init("butterfly.png", 8, 2, 12);
		}
		
		public void spawn()
		{
			super.spawn();		
		}
		
		public void update(float deltaTime)
		{
			super.update(deltaTime);
		}
	}
	
	ButterflyParticleSystem(int poolSize, float scale)
	{				
		LiveWallpaperService service = LiveWallpaperService.Get();
		
		mScale = scale;
		
		// set up the action list
		ActionList al = new ActionList();
		BirthAction ba = new BirthAction();
		ba.mType = BirthAction.BirthType.BT_Rate;
		ba.mRate = 0.9f;
		al.add(ba);
		
		mBirthAction.mCamera = service.mCamera;
		mBirthAction.mBoundRadius = mBoundRadius;
		mBirthAction.mType = OffCameraBirthAction.BirthType.BT_Rate;
		mBirthAction.mRate = 1.f; // once in 10 seconds
		mBirthAction.mSpeed = 1.f;
		al.add(mBirthAction);
		
		mDeathAction.mCamera = service.mCamera;
		mDeathAction.mAge = 5.f; // give particles some time to get on screen
		mDeathAction.mBoundRadius = mBoundRadius;
		al.add(mDeathAction);
		
		// pushes butterfly to this point
		mTouchForce.mType = ForceAction.ForceType.FT_Point;
		mTouchForce.mStrength = 0.5f;
		//mTouchForce.mInfluence = 5.f;
		al.add(mTouchForce);
		
		SpeedClamp clamp = new SpeedClamp();
		clamp.mMin = 0.05f;
		clamp.mMax = 0.05f;
		al.add(clamp);
			
		Init(al, ButterflyParticle.class, poolSize);
	}
	
	public void notifyEvent(Event event)
	{
		switch (event)
		{
		case Touch:
			mTouchForce.setEnabled(true);
			break;
		}
	}
	
	public void update(float deltaTime)
	{
		super.update(deltaTime);
	}
	
	public void draw(Matrix4 transform)
	{
		LiveWallpaperService service = LiveWallpaperService.Get();
		mDeathAction.mTransform = transform;
		mBirthAction.mTransform = transform;
		
		Vector4 localTouchPoint = touchPointLocal(transform);
		mTouchForce.mTransform.SetTranslation(localTouchPoint);
		
		// if any butterfly gets close enough to the touch point
		// disable its effect
		Iterator<engine.particle.Particle> it = mParticleSystem.get(0).particleList().iterator();
		while (it.hasNext())
		{
			engine.particle.Particle p = it.next();
	
			float distanceSqrd = localTouchPoint.Subtract(p.mTransform.GetTranslation()).MagnitudeSqrd3();
			float disableDistanceSqrd = (mDisableTouchForceDistance * service.mCamera.Height()) * (mDisableTouchForceDistance * service.mCamera.Height());
			if (distanceSqrd < disableDistanceSqrd)
			{
				mTouchForce.setEnabled(false);
			}
		}
					
		super.draw(transform);
	}
	
	public Vector4 touchPointLocal(Matrix4 transform)
	{
		// as long as we don't do any rotation,
		// this code should work, otherwise we will need to do a matrix inverse!
		LiveWallpaperService service = LiveWallpaperService.Get();
		Vector4 localTouchPoint = service.touchPoint().Subtract(transform.GetTranslation()); //r.touchPoint(). 
		return localTouchPoint;
	}
	
	ForceAction mTouchForce = new ForceAction();
	WindAction	mWind = new WindAction();
	float		mDisableTouchForceDistance = 0.01f;
	
	OffCameraDeathAction mDeathAction = new OffCameraDeathAction();
	OffCameraBirthAction mBirthAction = new OffCameraBirthAction();
}
