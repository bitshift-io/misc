package blackcarbon.panda;

import engine.math.Vector4;

public class LayerFactory 
{
	static ILayer createParallax(String name, float parallax, float lag, Vector4 scale, Vector4 offset)
	{
		ILayer layer = new ILayer();
		ParallaxTranslateProperty ptp = new ParallaxTranslateProperty();
		ptp.mLag = lag;
		ptp.mParallax = parallax;
		ptp.mOffset = offset;
		layer.add(ptp);
		
		TexturedQuadProperty tqp = new TexturedQuadProperty(name);
		layer.add(tqp);
		
		ParallaxScaleProperty psp = new ParallaxScaleProperty(tqp.textureAspectRatio(), scale);
		layer.add(psp);
		return layer;
	}
	
	static ILayer createStatic(String name)
	{
		ILayer layer = new ILayer();
		ScaleToCanvasProperty sp = new ScaleToCanvasProperty();	
		layer.add(sp);
		TexturedQuadProperty tqp = new TexturedQuadProperty(name);
		layer.add(tqp);
		return layer;
	}
	
	static ILayer createFireflyParticleSystem(int poolSize, float parallax, float lag, float scale)
	{
		ILayer layer = new ILayer();
		ParallaxTranslateProperty ptp = new ParallaxTranslateProperty();
		ptp.mLag = lag;
		ptp.mParallax = parallax;
		ptp.mOffset = new Vector4();
		layer.add(ptp);
		
		FireflyParticleSystem fps = new FireflyParticleSystem(poolSize, scale);
		layer.add(fps);
		return layer;
	}
	
	static ILayer createButterflyParticleSystem(int poolSize, float parallax, float lag, float scale)
	{
		ILayer layer = new ILayer();
		ParallaxTranslateProperty ptp = new ParallaxTranslateProperty();
		ptp.mLag = lag;
		ptp.mParallax = parallax;
		ptp.mOffset = new Vector4();
		layer.add(ptp);
		
		ButterflyParticleSystem fps = new ButterflyParticleSystem(poolSize, scale);
		layer.add(fps);
		return layer;
	}
}
