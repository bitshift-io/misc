package blackcarbon.panda;

import engine.math.Matrix4;
import engine.math.Vector4;

public class ParallaxTranslateProperty extends IProperty
{
	public ParallaxTranslateProperty() 
	{
	}

	@Override
	public void update(float deltaTime) 
	{
		LiveWallpaperService service = LiveWallpaperService.Get();
		float deltaOffset = service.mCanvasOffset - mCurrentOffset;
		mCurrentOffset += deltaOffset * mLag * 0.1;
	}

	@Override
	public Matrix4 transform(Matrix4 transform) 
	{
		LiveWallpaperService service = LiveWallpaperService.Get();
		float canvasWidth = service.mCamera.Width();
		
		float offsetX = (mCurrentOffset * canvasWidth * mParallax) + (mOffset.GetX() * (canvasWidth * 2.0f));
		float offsetY = mOffset.GetY() * service.mCamera.Height();
    	transform.SetTranslation(new Vector4(offsetX, offsetY));
		return transform;
	}

	Vector4	mOffset;
	float	mParallax;
	float	mLag;		// lag = 1 is no lag, closer to 0 is higher lag
	float	mCurrentOffset;
}
