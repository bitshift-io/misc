package blackcarbon.panda;

import engine.math.Matrix4;
import engine.math.Vector4;
import android.graphics.PointF;

public class ScaleToCanvasProperty extends IProperty 
{
	public ScaleToCanvasProperty() 
	{
	}
	
	public Matrix4 transform(Matrix4 transform)
	{
		LiveWallpaperService service = LiveWallpaperService.Get();
		transform.SetScale(new Vector4(service.mCamera.Width(), service.mCamera.Height(), 1));
		return transform;
	}
}
