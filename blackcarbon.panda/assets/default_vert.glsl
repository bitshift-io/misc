uniform mat4 u_WorldViewProjectionMatrix;
uniform mat4 u_TextureMatrix;

attribute vec4 a_Position;	
attribute vec4 a_UV;

varying vec2 v_tc;

void main()
{
   gl_Position = u_WorldViewProjectionMatrix
               * a_Position;
	v_tc = (u_TextureMatrix * a_UV).xy;
}       