precision mediump float;
varying vec2 v_tc;
uniform sampler2D u_Texture;
uniform vec4 u_DiffuseColour;

void main()
{
   gl_FragColor = texture2D(u_Texture, v_tc) * u_DiffuseColour;
} 