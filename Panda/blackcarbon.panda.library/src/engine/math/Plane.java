package engine.math;

public class Plane
{
	public Plane(Vector4 point, Vector4 normal)
	{
		this.normal = normal;
		distance = normal.Dot3(point);
	}
	
	public float DistanceToPoint(Vector4 point)
	{
		return normal.Dot3(point) - distance;
	}
	
	public Vector4 Intersect3(Plane p1, Plane p2)
	{
		Vector4 n1 = normal;
		Vector4 n2 = p1.normal;
		Vector4 n3 = p2.normal;
		float d1 = distance;
		float d2 = p1.distance;
		float d3 = p2.distance;
	
		//http://astronomy.swin.edu.au/~pbourke/geometry/3planes/
		Vector4 a = (n2.Cross3(n3).Multiply(d1));
		Vector4 b = (n1.Cross3(n3).Multiply(d2));
		Vector4 c = (n1.Cross3(n2).Multiply(d3));
		float d = n1.Dot3(n2.Cross3(n3));
		Vector4 point = a.Add(b).Add(c).Divide(d); //((n2.Cross3(n3).Multiply(d1)) + (n1.Cross3(n3).Multiply(d2)) + (n1.Cross3(n2).Multiply(d3))) / n1.Dot(n2.Cross3(n3));
		return point;
	}
	/*
	public Plane Transform(Matrix4 transform)
	{
		Plane ret;
	
		// not sure if this works
		ret.normal.x = (transform[0] * normal.x) + (transform[4] * normal.y) + (transform[8] * normal.z)  + (transform[12] * distance);
		ret.normal.y = (transform[1] * normal.x) + (transform[5] * normal.y) + (transform[9] * normal.z)  + (transform[13] * distance);
		ret.normal.z = (transform[2] * normal.x) + (transform[6] * normal.y) + (transform[10] * normal.z) + (transform[14] * distance);
		ret.distance = (transform[3] * normal.x) + (transform[7] * normal.y) + (transform[11] * normal.z) + (transform[15] * distance);
	
		return ret;
	}

	public float Normalize()
	{
		float mag;
		mag = sqrtf(normal.x*normal.x + normal.y*normal.y + normal.z*normal.z);

		normal.x /= mag;
		normal.y /= mag;
		normal.z /= mag;
		distance /= mag;

		return mag;
	}
	
	public float Intersect(const Ray& ray) const
	{
		float normDotDir = normal.Dot3(ray.direction);
		if (normDotDir == 0.f)
			return 0.f;

		Vector4 pointOnPlane = normal * distance;
		return normal.Dot3(pointOnPlane - ray.origin) / normDotDir;
	}
	*/
	
	float	distance;
	Vector4	normal;
}
