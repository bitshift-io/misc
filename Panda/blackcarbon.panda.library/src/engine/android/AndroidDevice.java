package engine.android;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import engine.android.GLSurfaceView.Renderer;
import engine.api.Device;
import engine.api.Device.DeviceState;
import engine.math.Matrix4;
import engine.math.Vector4;

import android.opengl.GLES20;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;


public class AndroidDevice extends Device
{
	public AndroidDevice()
	{		
		// dirty all flags to ensure default set
		mDeviceState.dirtyFlags = -1;
		ApplyDeviceState();
	}
	
	public void Resize(int width, int height)
	{		
		mWidth  = width;
		mHeight = height;
		
		GLES20.glViewport(0, 0, width, height);
	
		// improve performance at cost of quality
		//gl.glDisable(GL10.GL_DITHER);
/*
        /*
         * Set our projection matrix. This doesn't have to be done
         * each time we draw, but usually a new projection needs to be set
         * when the viewport is resized.
         * /

        float ratio = (float)width / height;
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glFrustumf(-ratio, ratio, -1, 1, 2, 12);

        /*
         * By default, OpenGL enables features that improve quality
         * but reduce performance. One might want to tweak that
         * especially on software renderer.
         * /
        gl.glDisable(GL10.GL_DITHER);
        gl.glActiveTexture(GL10.GL_TEXTURE0);*/
	}
	
	@Override
	public void SetClearColour(Vector4 colour)
	{
		GLES20.glClearColor(colour.GetX(), colour.GetY(), colour.GetZ(), colour.GetW());
	}
	
	@Override
	public void Clear(int clearFlags)
	{
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
	}
	
	@Override
	public void Begin()
	{
		AcquireSemaphore();
		//Memory.Stack.Push();
		
		try 
		{
			mGLView.MakeCurrent();
		} 
		catch (Exception e)
		{
			System.out.println("[AndroidDevice::Begin] Exception: " + e.getMessage());
		}
		/*
		// restore default rendering
		Matrix4 identity = Memory.Stack.New(Matrix4.class);
		identity.SetIdentity();

		SetMatrix(identity, MatrixMode.Model);
		SetMatrix(identity, MatrixMode.Texture);
		SetMatrix(identity, MatrixMode.View);
		SetMatrix(identity, MatrixMode.Projection);
		
		Memory.Stack.Pop();*/
	}
	
	@Override
	public void End()
	{/*
		try
		{
			Display.releaseContext();
		} 
		catch (Exception e)
		{
			System.out.println("[AndroidDevice::End] Exception: " + e.getMessage());
		}*/
		
		ReleaseSemaphore();
	}

	@Override
	public void SetMatrix(Matrix4 matrix, MatrixMode mode)
	{		
		switch (mode)
		{
		case Texture:
		{
			mTextureMatrix.Copy(matrix);
			break;
		}
			
		case Model:
		{
			mWorld.Copy(matrix);
			/*
			Matrix4 worldView = mWorld.Multiply(mView);
			LoadMatrix(worldView);*/
			break;
		}
		
		case View:
		{
			mView.Copy(matrix);
			/*
			GLES20.glMatrixMode(GLES20.GL_MODELVIEW);
			Matrix4 worldView = mWorld.Multiply(mView);
			LoadMatrix(worldView);*/
			break;
		}
		
		case Projection:
		{
			mProjection.Copy(matrix);
			break;
		}
		}
		
	}
		
	@Override
	public Matrix4 GetMatrix(MatrixMode mode)
	{
		switch (mode)
		{
		case Texture:
			return mTextureMatrix;
			
		case Model:
			return mWorld;
		
		case View:
			return mView;
		
		case Projection:
			return mProjection;
		}
		
		return null;
	}
	
	@Override
	public int GetWidth()
	{
		return mWidth;
	}
	
	@Override
	public int GetHeight()
	{
		return mHeight;
	}
	
	@Override
	public float GetAspectRatio()
	{
		return (float)(mWidth) / (float)(mHeight);
	}
	
	@Override
	public DeviceState GetDeviceState()
	{
		return mDeviceState;
	}
	
	/*
	@Override
	public void SetDeviceState(DeviceState state)
	{
		GLES20 gl = GetGL();
		
		if (state.alphaBlendEnable)
			gl.glEnable(GLES20.GL_BLEND);
		else
			gl.glDisable(GLES20.GL_BLEND);
			
		int alphaBlendSource = GetBlendMode(state.alphaBlendSource);
		int alphaBlendDest = GetBlendMode(state.alphaBlendDest);
		gl.glBlendFunc(alphaBlendSource, alphaBlendDest);
		
		if (state.alphaCompare == Device.Compare.Always)
			gl.glDisable(GLES20.GL_ALPHA_TEST);
		else
			gl.glEnable(GLES20.GL_ALPHA_TEST);
		
		int alphaCompare = GetCompare(state.alphaCompare);
		gl.glAlphaFunc(alphaCompare, state.alphaCompareValue);
	}*/
	
	public void ApplyDeviceState()
	{		
		// depth functionality
		if ((mDeviceState.dirtyFlags & DeviceState.DF_DepthTestEnable) != 0)
		{
			if (mDeviceState.depthTestEnable)
				GLES20.glEnable(GLES20.GL_DEPTH_TEST);
			else
				GLES20.glDisable(GLES20.GL_DEPTH_TEST);
		}
		
		// alpha functionality
		if ((mDeviceState.dirtyFlags & DeviceState.DF_AlphaBlendEnable) != 0)
		{
			if (mDeviceState.alphaBlendEnable)
				GLES20.glEnable(GLES20.GL_BLEND);
			else
				GLES20.glDisable(GLES20.GL_BLEND);
		}
		
		if ((mDeviceState.dirtyFlags & (DeviceState.DF_AlphaBlendSource | DeviceState.DF_AlphaBlendDest)) != 0)
		{
			int alphaBlendSource = GetBlendMode(mDeviceState.alphaBlendSource);
			int alphaBlendDest = GetBlendMode(mDeviceState.alphaBlendDest);
			GLES20.glBlendFunc(alphaBlendSource, alphaBlendDest);
		}
		/*
		if ((mDeviceState.dirtyFlags & (DeviceState.DF_AlphaCompare | DeviceState.DF_AlphaCompareValue)) != 0)
		{
			if (mDeviceState.alphaCompare == Device.Compare.Always)
				GLES20.glDisable(GLES20.GL_ALPHA_TEST);
			else
				GLES20.glEnable(GLES20.GL_ALPHA_TEST);
			
			int alphaCompare = GetCompare(mDeviceState.alphaCompare);
			GLES20.glAlphaFunc(alphaCompare, mDeviceState.alphaCompareValue);
		}
		
		if ((mDeviceState.dirtyFlags & DeviceState.DF_Colour) != 0)
		{
			GLES20.glColor4f(mDeviceState.colour.GetX(), mDeviceState.colour.GetY(), mDeviceState.colour.GetZ(), mDeviceState.colour.GetW());
		}*/
	}
/*
	@Override
	public void Render()
	{		
		ApplyDeviceState();
		
		if ((mDirtyFlags & DF_Texture) != 0)
		{
			if (mTexture != null)
			{
				mTexture.Bind();
			}
			else
			{
				GLES20.glDisable(GLES20.GL_TEXTURE_2D);
				GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, -1);
			}
		}
		
		if ((mDirtyFlags & DF_RenderBuffer) != 0)
		{
			// need to unbind incase going from VBO to vertex arrays
			mRenderBuffer.Unbind();
			mRenderBuffer.Bind();
		}
		
		mRenderBuffer.Render();
		
		mDeviceState.dirtyFlags = 0;
		mDirtyFlags = 0;
	}
*/
	
	int GetCompare(Compare value)
	{/*
		public enum Compare
		{
			Less,
			LessEqual,
			Equal,
			Greater,
			GreaterEqual,
			Always, // TODO: fix device enums
			Never,
			NotEqual,
		}*/	
		
		int lookup[] = {
				GLES20.GL_LESS, 
				GLES20.GL_LEQUAL,
				GLES20.GL_EQUAL, 
				GLES20.GL_GREATER, 
				GLES20.GL_GEQUAL,
				GLES20.GL_ALWAYS,
				GLES20.GL_NEVER,
				GLES20.GL_NOTEQUAL, 
		};
		
		return lookup[value.ordinal()];
	}
	
	int GetBlendMode(BlendMode value)
	{/*
		public enum BlendMode
		{
			Zero,
			One,
			DestColour,
			SrcColour,
			OneMinusDestColour,
			OneMinusSrcColour,
			SrcAlpha,
			OneMinusSrcAlpha,
			DestAlpha,
			OneMinusDestAlpha,
			SrcAlphaSaturate,
		}
		*/
		int lookup[] = {
				GLES20.GL_ZERO, 
				GLES20.GL_ONE,
				GLES20.GL_DST_COLOR, 
				GLES20.GL_SRC_COLOR, 
				GLES20.GL_ONE_MINUS_DST_COLOR,
				GLES20.GL_ONE_MINUS_SRC_COLOR,
				GLES20.GL_SRC_ALPHA,
				GLES20.GL_ONE_MINUS_SRC_ALPHA, 
				GLES20.GL_DST_ALPHA,
				GLES20.GL_ONE_MINUS_DST_ALPHA, 
				GLES20.GL_SRC_ALPHA_SATURATE
		};
		
		return lookup[value.ordinal()];
	}
	
	int GetTopology(GeometryTopology value)
	{
		/*
		public enum GeometryTopology
	{
		LineList,
		PointList,
		TriangleList,
		TriangleListAdjacent,
	}
	
		 */
		int lookup[] = {
				GLES20.GL_LINES, 
				-1,
				GLES20.GL_TRIANGLES, 
				//GLES20.GL_TRIANGLES_ADJACENCY_EXT, 
				GLES20.GL_TRIANGLE_STRIP,
				-1,
		};
		
		return lookup[value.ordinal()];
	}
	
	private int			mWidth;
	private int			mHeight;
	
	private engine.android.GLSurfaceView 	mGLView;
}


