package engine.android;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import android.opengl.GLES20;

import engine.android.AndroidShader.Attribute;
import engine.api.Device;
import engine.api.Device.GeometryTopology;
import engine.api.Factory;
import engine.api.RenderBuffer;
import engine.api.Shader;
import engine.api.RenderBuffer.AttributeBuffer;
import engine.math.Vector4;

public class AndroidRenderBuffer extends RenderBuffer
{
	public static class AttributeBuffer extends RenderBuffer.AttributeBuffer
	{
		public void Bind(AndroidShader.Attribute att)
		{			
			GLES20.glEnableVertexAttribArray(att.mHandle);
			
			if (mUsage == Static) // ie. using VBO
			{
				GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mVboHandle);
				GLES20.glVertexAttribPointer(att.mHandle, mSize, GLES20.GL_FLOAT, false, 0, null);
			}
			else // Dynamic
			{
				//mFloatBuffer.position(0);
				GLES20.glVertexAttribPointer(att.mHandle, mSize, GLES20.GL_FLOAT, false, 0, mFloatBuffer);
			}

		}
		
		public void RenderIndexBuffer(GeometryTopology meshTopology)
		{
			AndroidDevice device = (AndroidDevice)Factory.Get().GetDevice();
			int topology = device.GetTopology(meshTopology);
			
			//GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 1); // draw the first tringle with out index buffer
			
			if (mUsage == AttributeBuffer.Dynamic)
			{
				GLES20.glDrawElements(topology, mByteBuffer.capacity(), GLES20.GL_UNSIGNED_BYTE, mByteBuffer);
			}
			else
			{
				GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mVboHandle);
				GLES20.glDrawElements(topology, mByteBuffer.capacity(), GLES20.GL_UNSIGNED_BYTE, null);
			}			
		}
		
		public void MakeStatic()
		{
			/*
			mUsage = Static;
			
			if (mFloatBuffer != null)
			{
				mVboHandle = CreateVBOWithData(mFloatBuffer);
			}
			else if (mByteBuffer != null)
			{
				mVboHandle = CreateVBOWithData(mByteBuffer);
			}*/
		}
		
		public void Reload()
		{
			if (mUsage == Static)
				MakeStatic();
		}
		
		private int CreateVBOWithData(FloatBuffer data)
		{
			IntBuffer buffer = ByteBuffer.allocateDirect(4).order(ByteOrder.nativeOrder()).asIntBuffer();
			GLES20.glGenBuffers(1, buffer);
			int id = buffer.get(0);
			
			GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, id);
			GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, data.capacity() * 4, data, GLES20.GL_STATIC_DRAW);

		    return id;
		}
		
		private int CreateVBOWithData(ByteBuffer data)
		{
			IntBuffer buffer = ByteBuffer.allocateDirect(4).order(ByteOrder.nativeOrder()).asIntBuffer();
			GLES20.glGenBuffers(1, buffer);
			int id = buffer.get(0);
			
			GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, id);
			GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, data.capacity(), data, GLES20.GL_STATIC_DRAW);
			return id;
		}

		
		int 	mVboHandle		= -1;
	}
	
	/*
	private void BindStatic()
	{
		if (mPositionID != -1)
		{
			GLES20.glEnableClientState(GLES20.GL_VERTEX_ARRAY);
			GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mPositionID);
			GLES20.glVertexPointer(3, GLES20.GL_FLOAT, 0, 0);
		}
		else
		{
			GLES20.glDisableClientState(GLES20.GL_VERTEX_ARRAY);
		}
		
		if (mColourID != -1)
		{
			GLES20.glEnableClientState(GLES20.GL_COLOR_ARRAY);
			GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mColourID);
			GLES20.glColorPointer(4, GLES20.GL_FLOAT, 0, 0);
		}
		else
		{
			GLES20.glDisableClientState(GLES20.GL_COLOR_ARRAY);
		}
        
		if (mUVID != -1)
		{
			GLES20.glEnableClientState(GLES20.GL_TEXTURE_COORD_ARRAY);
			GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mUVID);
			GLES20.glTexCoordPointer(2, GLES20.GL_FLOAT, 0, 0);
		}
		else
		{
			GLES20.glDisableClientState(GLES20.GL_TEXTURE_COORD_ARRAY);
		}
		
		if (mIndexID != -1)
		{
			GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mIndexID);
		}
	}
	
	private void BindDynamic()
	{
		if (mPosition != null)
		{
			//float *vertices = ...;
			int loc = 0;// glGetAttribLocation(program, "position");
			GLES20.glVertexAttribPointer(loc, 3, GLES20.GL_FLOAT, false, 0, mPosition);
			
			//GLES20.glEnableClientState(GLES20.GL_VERTEX_ARRAY);
			//GLES20.glVertexPointer(3, GLES20.GL_FLOAT, 0, mPosition);
		}
		else
		{
			GLES20.glDisableClientState(GLES20.GL_VERTEX_ARRAY);
		}
		
		if (mColour != null)
		{
			GLES20.glEnableClientState(GLES20.GL_COLOR_ARRAY);
			GLES20.glColorPointer(4, GLES20.GL_FLOAT, 0, mColour);
		}
		else
		{
			GLES20.glDisableClientState(GLES20.GL_COLOR_ARRAY);
		}
        
		if (mUV != null)
		{
			GLES20.glEnableClientState(GLES20.GL_TEXTURE_COORD_ARRAY);
			GLES20.glTexCoordPointer(2, GLES20.GL_FLOAT, 0, mUV);
		}
		else
		{
			GLES20.glDisableClientState(GLES20.GL_TEXTURE_COORD_ARRAY);
		}
	}
	
	public void Bind()
	{
		if (mUsage == Dynamic)
			BindDynamic();
		else
			BindStatic();
	}
	
	public void Unbind()
	{	
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
		
		// FM: error in OGL2
		GLES20.glDisableClientState(GLES20.GL_VERTEX_ARRAY);
		GLES20.glDisableClientState(GLES20.GL_COLOR_ARRAY);
		GLES20.glDisableClientState(GLES20.GL_TEXTURE_COORD_ARRAY);
	}
	
	public void Render()
	{	
//		gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//		gl.glDisable(GLES20.GL_CULL_FACE); // double sided plz for a bit
//		gl.glFrontFace(GLES20.GL_CW);

		try
		{
			AndroidDevice device = (AndroidDevice)Factory.Get().GetDevice();
			int topology = device.GetTopology(mTopology);
			if (mUsage == Dynamic)
				GLES20.glDrawElements(topology, mIndex.capacity(), GLES20.GL_UNSIGNED_BYTE, mIndex);
			else
				GLES20.glDrawElements(topology, mIndexCount, GLES20.GL_UNSIGNED_BYTE, 0);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private int CreateVBOWithData(FloatBuffer data, boolean fixed)
	{
		/*
		IntBuffer fixedData = null;
		if (fixed)
		{
			ByteBuffer bb = ByteBuffer.allocateDirect(data.capacity() * 4);
			bb.order(ByteOrder.nativeOrder());
		    fixedData = bb.asIntBuffer();
		    
			for (int d = 0; d < data.capacity(); ++d)
			{
				float f = data.get(d);
				
				int i = (int)(f * 65536);
				fixedData.put((int)i);
				data.put(d, f * 65536);
			}
		}* /

		IntBuffer buffer = ByteBuffer.allocateDirect(4).order(ByteOrder.nativeOrder()).asIntBuffer();
		GLES20.glGenBuffers(1, buffer);
		int id = buffer.get(0);
		
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, id);
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, data.capacity() * 4, /*fixed ? fixedData : * /data, GLES20.GL_STATIC_DRAW);

	    return id;
	}

	@Override
	public void MakeStatic()
	{
		mUsage = Static;
		
		AndroidDevice device = ((AndroidDevice)Factory.Get().GetDevice());
		//device.Begin();
		
		// index buffer to VBO
		mIndexCount = mIndex.capacity();

		IntBuffer buffer = ByteBuffer.allocateDirect(4).order(ByteOrder.nativeOrder()).asIntBuffer();
		GLES20.glGenBuffers(1, buffer);
		mIndexID = buffer.get(0);
		
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, mIndexID);
		GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, mIndexCount, mIndex, GLES20.GL_STATIC_DRAW);
		
		//mIndex = null;
		
		if (mPosition != null)
		{
			mPositionID = CreateVBOWithData(mPosition, false);
			//mPosition = null;
		}
		
		if (mColour != null)
		{
			mColourID = CreateVBOWithData(mColour, false);
			//mColour = null;
		}
		
		if (mUV != null)
		{
			mUVID = CreateVBOWithData(mUV, false);
			//mUV = null;
		}
		
		Unbind();
		//device.End();
	}
	
	@Override
	public int GetIndexCount()
	{
		if (mUsage == Static)
			return mIndexCount;
		
		return super.GetIndexCount();
	}*/
	
	public void Bind(Shader shader)
	{
		for (int a = 0; a < Shader.Attribute.Type.Max.ordinal(); ++a)
		{
			Shader.Attribute.Type att = Shader.Attribute.Type.values()[a];
			AttributeBuffer attBuf = (AttributeBuffer) GetAttributeBuffer(att);
			
			AndroidShader.Attribute shaderAtt = (Attribute) shader.GetAttribute(att);
			if (shaderAtt != null && attBuf != null)
			{
				attBuf.Bind(shaderAtt);
			}
		}
	}
	
	public void Render()
	{
		AttributeBuffer indexBuffer = (AttributeBuffer) GetAttributeBuffer(Shader.Attribute.Type.Index);
		indexBuffer.RenderIndexBuffer(mTopology);
	}
	
	public void MakeStatic()
	{
		for (int a = 0; a < Shader.Attribute.Type.Max.ordinal(); ++a)
		{
			Shader.Attribute.Type att = Shader.Attribute.Type.values()[a];
			AttributeBuffer attBuf = (AttributeBuffer) GetAttributeBuffer(att);
			if (attBuf != null)
			{
				attBuf.MakeStatic();
			}
		}
	}
	
	int 	mPositionID		= -1;
	int		mUVID			= -1;
	int		mColourID		= -1;
	int		mIndexID		= -1;
	
	int		mIndexCount		= 0;
}