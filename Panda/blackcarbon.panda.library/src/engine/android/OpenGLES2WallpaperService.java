package engine.android;

import java.io.FileDescriptor;
import java.io.PrintWriter;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView.Renderer;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.service.wallpaper.WallpaperService.Engine;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import engine.android.GLWallpaperService;
import engine.api.Factory;

public abstract class OpenGLES2WallpaperService extends GLWallpaperService
{
	boolean mFirstSurfaceCreated = false;
	
	public enum EngineType
	{
		GlSurface,
		ES2Rgbrn,
	}
	
	protected ProxyEngine mEngine;
	
	public Engine engine() 
	{
		return mEngine;
	}
	
	public Renderer renderer() 
	{
		return mEngine;
	}
	
	@Override
	public Engine onCreateEngine() 
	{
		mEngine = new ProxyEngine(this, EngineType.ES2Rgbrn);
		return mEngine;
	}
	
	/*
	@Override
	public Engine onCreateEngine() {
		if (PreferenceManager.getDefaultSharedPreferences(
				OpenGLES2WallpaperService.this).getBoolean(SETTINGS_KEY, true)) {
			return new ES2GLSurfaceViewEngine();
		} else {
			return new ES2RgbrnGLEngine();
		}
	}*/

	public class ES2GLSurfaceViewEngine extends GLWallpaperService.GLSurfaceViewEngine {

		@Override
		public void onCreate(SurfaceHolder surfaceHolder) {
			super.onCreate(surfaceHolder);
			init(this);
		}
		
		@Override
		public void	 dump(String prefix, FileDescriptor fd, PrintWriter out, String[] args)
		{
			super.dump(prefix, fd, out, args);
		}
	}

	public class ES2RgbrnGLEngine extends GLWallpaperService.RgbrnGLEngine {

		@Override
		public void onCreate(SurfaceHolder surfaceHolder) {
			super.onCreate(surfaceHolder);
			init(this);
			setRenderMode(RgbrnGLEngine.RENDERMODE_WHEN_DIRTY);
		}
		
		@Override
		public void	 dump(String prefix, FileDescriptor fd, PrintWriter out, String[] args)
		{
			super.dump(prefix, fd, out, args);
		}
	}
	
	public class ProxyEngine extends WallpaperService.Engine implements Renderer
	{				
		
		Engine mEngine;
		OpenGLES2WallpaperService mService;
		boolean	mVisible = false;
		
		private final Handler mHandler = new Handler();
		private final Runnable mDrawFrame = new Runnable() {
            public void run() {
                drawFrame();
            }
        };
        
        public ProxyEngine(OpenGLES2WallpaperService service, EngineType engineType)
        {
        	mService = service;
        	mEngine = createEngine(engineType);
        }
        
		public Engine createEngine(EngineType type)
		{
			switch (type)
			{
			case GlSurface:
				return new ES2GLSurfaceViewEngine();
				
			case ES2Rgbrn:
				{
					return new ES2RgbrnGLEngine();
				}
			}

			return null;
		}
		        
        void drawFrame()
        {
        	if (mEngine instanceof ES2RgbrnGLEngine)
        	{
	        	ES2RgbrnGLEngine rgbEngine = (ES2RgbrnGLEngine)mEngine;
	        	if (rgbEngine != null)
	        	{
	        		rgbEngine.requestRender();
	        	}
        	}
        	
            // Reschedule the next redraw
            mHandler.removeCallbacks(mDrawFrame);
            if (mVisible) 
            {
                mHandler.postDelayed(mDrawFrame, mService.frameDelay()); //ms delay
            }
        }
        
        @Override
        public void onDrawFrame(GL10 gl)
        {
        	mService.drawFrame();
        }
        
		@Override
		public int getDesiredMinimumHeight()
		{
			return mEngine.getDesiredMinimumHeight();
		}
		
		@Override
		public int getDesiredMinimumWidth()
		{
			return mEngine.getDesiredMinimumWidth();
		}
				
		@Override
		public SurfaceHolder getSurfaceHolder()
		{
			return mEngine.getSurfaceHolder();
		}
		
		@Override
		public boolean isPreview()
		{
			return mEngine.isPreview();
		}
		
		@Override
		public boolean	 isVisible()
		{
			return mEngine.isVisible();
		}
		
		@Override
		public Bundle	 onCommand(String action, int x, int y, int z, Bundle extras, boolean resultRequested)
		{
			return mEngine.onCommand(action, x, y, z, extras, resultRequested);
		}
		
		@Override
		public void	 onDesiredSizeChanged(int desiredWidth, int desiredHeight)
		{
			mEngine.onDesiredSizeChanged(desiredWidth, desiredHeight);
		}
		
		@Override
		public void	 onDestroy()
		{
			mHandler.removeCallbacks(mDrawFrame);
			mEngine.onDestroy();
		}
		
		@Override
		public void	 onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset)
		{
			mEngine.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);
			mService.offsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);
		}
		
		@Override
		public void	 onSurfaceChanged(SurfaceHolder holder, int format, int width, int height)
		{
			mEngine.onSurfaceChanged(holder, format, width, height);
		}
		
		@Override
		public void	 onSurfaceCreated(SurfaceHolder holder)
		{
			// By default we don't get touch events, so enable them.
            setTouchEventsEnabled(true);
            
            mEngine.onSurfaceCreated(holder);
		}
		
		@Override
		public void	 onSurfaceDestroyed(SurfaceHolder holder)
		{
			mHandler.removeCallbacks(mDrawFrame);
            mVisible = false;
            
			mEngine.onSurfaceDestroyed(holder);
		}
		
		@Override
		public void	onSurfaceRedrawNeeded(SurfaceHolder holder)
		{
			mEngine.onSurfaceRedrawNeeded(holder);
		}
		
		@Override
		public void	 onTouchEvent(MotionEvent event)
		{
			mEngine.onTouchEvent(event);
			mService.touchEvent(event);
		}
		
		@Override
		public void onVisibilityChanged(boolean visible)
		{
			if (mVisible != visible)
        	{
        		mService.visibilityChanged(visible);
        	}
        	
        	mVisible = visible;
            if (visible) 
            {
                drawFrame();
            } 
            else 
            {
                mHandler.removeCallbacks(mDrawFrame);
            }
            
            mEngine.onVisibilityChanged(visible);
		}
		
		/*
		@Override
		public void	setOffsetNotificationsEnabled(boolean enabled)
		{
			engine().setOffsetNotificationsEnabled(enabled);
		}*/
		
		@Override
		public void	setTouchEventsEnabled(boolean enabled)
		{
			mEngine.setTouchEventsEnabled(enabled);
		}
		/*
		@Override
		void dump(String prefix, FileDescriptor fd, PrintWriter out, String[] args)
		{
			super.dump(prefix, fd, out, args);
			//engine().dump(prefix, fd, out, args);
		}*/
		
		@Override
		public void onCreate(SurfaceHolder surfaceHolder)
		{
			mEngine.onCreate(surfaceHolder);
		}

		@Override
		public void onSurfaceChanged(GL10 gl, int width, int height) 
		{
			mService.surfaceChanged(width, height);
		}

		@Override
		public void onSurfaceCreated(GL10 gl, EGLConfig config)
		{
			mService.surfaceCreated();
		}
	}

	void init(OpenGLEngine engine)
	{
		// Check if the system supports OpenGL ES 2.0.
		final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		final ConfigurationInfo configurationInfo = activityManager
				.getDeviceConfigurationInfo();
		final boolean supportsEs2 = configurationInfo.reqGlEsVersion >= 0x20000;

		if (supportsEs2) {
			// Request an OpenGL ES 2.0 compatible context.
			engine.setEGLContextClientVersion(2);

			// Set the renderer to our user-defined renderer.
			engine.setRenderer(mEngine);
						
		} else {
			// This is where you could create an OpenGL ES 1.x compatible
			// renderer if you wanted to support both ES 1 and ES 2.
			return;
		}
	}
	
	public long frameDelay() {
		return 33;
	}

	public void visibilityChanged(boolean visible) {
		// TODO Auto-generated method stub
		
	}

	public void offsetsChanged(float xOffset, float yOffset, float xOffsetStep,
			float yOffsetStep, int xPixelOffset, int yPixelOffset) {
		// TODO Auto-generated method stub
		
	}

	public void touchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		
	}

	public void surfaceCreated() 
	{
		if (!mFirstSurfaceCreated)
		{
			mFirstSurfaceCreated = true;
			init();
		}
	}

	public void surfaceChanged(int width, int height) 
	{
		// TODO Auto-generated method stub
		
	}

	public void init()
	{
		
	}

	public void drawFrame()
	{
		// TODO Auto-generated method stub
		
	}
}
