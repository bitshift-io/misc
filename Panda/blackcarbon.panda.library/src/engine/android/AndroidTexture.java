package engine.android;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;

import engine.api.Factory;
import engine.api.Texture;

public class AndroidTexture extends Texture
{
	public void Load()
	{
		Delete();
		
		//TextureReader.Texture texture = 
        //    TextureReader.readTexture("demos/data/images/glass.png");
				
		if (mPixels == null || mDownscale != downscale())
		{
			try
			{			
				Bitmap bmp = BitmapFactory.decodeStream(mFactory.GetFileResource(mName, "tex"));
				if (bmp == null)
				{
					System.out.println("[AndroidTexture.Load] Resource '" + mName + "' does not exist in apk");
					return;
				}
				
				// down scale textures
				mDownscale = downscale();
				if (mDownscale > 0)
				{
					int scale = 2 * mDownscale;
					bmp = Bitmap.createScaledBitmap(bmp, bmp.getWidth() / scale, bmp.getHeight() / scale, true);
				}
				
				mWidth = bmp.getWidth();
				mHeight = bmp.getHeight();
								
				
				mPixels = ByteBuffer.allocateDirect(mWidth * mHeight * 4);
				mPixels.order(ByteOrder.BIG_ENDIAN);
		        IntBuffer ib = mPixels.asIntBuffer();

		        int[] pixels = new int[mWidth * mHeight];
		        bmp.getPixels(pixels, 0, mWidth, 0, 0, mWidth, mHeight);
		        for (int i = 0; i < pixels.length; ++i)
		        {
		        	/*
		        	int r = (pixels[i] >> 16) & 0x000000ff;
					int b = pixels[i] & 0x000000ff;
					
					pixels[i] &= 0xff00ff00;
					pixels[i] |= r;
					pixels[i] |= (b << 16);
					
					ib.put(pixels[i]);
					*/
		            ib.put(pixels[i] << 8 | pixels[i] >>> 24);
		        }
		        
		        mPixels.position(0);
		        bmp.recycle();
			        
			        /*
				mPixels = ByteBuffer.allocateDirect(mWidth * mHeight * 4); //new int[mWidth * mHeight];
				bmp.getPixels(mPixels, 0, mWidth, 0, 0, mWidth, mHeight);
				
				//pixels = bi.getRGB(0, 0, width, height, null, 0, width);  
				
				// this is in the format ARGB and i need to convert it 
				// to ABGR 
				for (int i = 0; i < mPixels.length; ++i)
				{				
					int r = (mPixels[i] >> 16) & 0x000000ff;
					int b = mPixels[i] & 0x000000ff;
					
					mPixels[i] &= 0xff00ff00;
					mPixels[i] |= r;
					mPixels[i] |= (b << 16);
				}*/
				
				mTextureIds = new int[1];
				mTextureIds[0] = -1;
			}
			catch (Exception e)
			{
				System.out.println("[AndroidTexture::Load1] Exception: " + e.getMessage());
				mTextureIds = null;
				return;
			}
		}

		try
		{
			//device.Begin();
			
			//IntBuffer ib = ByteBuffer.allocateDirect(4).order(ByteOrder.nativeOrder()).asIntBuffer();
			
			GLES20.glEnable(GLES20.GL_TEXTURE_2D);
			GLES20.glGenTextures(1, mTextureIds, 0);
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureIds[0]);
			
			//IntBuffer tbuf = IntBuffer.wrap(mPixels);
			//tbuf.position(0);	
			
			//gl.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			//gl.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
			
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
			GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);

			GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, mWidth, mHeight, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, mPixels);
			GLES20.glDisable(GLES20.GL_TEXTURE_2D);
		}
		catch (Exception e)
		{
			System.out.println("[AndroidTexture::Load2] Exception: " + e.getMessage());
			mTextureIds = null;
		}
	}
	
	void Delete()
	{
		// release old texture
		if (mTextureIds != null && mTextureIds[0] != -1)
		{
			GLES20.glDeleteTextures(mTextureIds.length, mTextureIds, 0);
		}
	}
	
	public void Bind()
	{
		if (mTextureIds == null)
		{
			return;
		}
		
		GLES20.glEnable(GLES20.GL_TEXTURE_2D);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureIds[0]);
	}
	
	public void Unbind()
	{
		GLES20.glDisable(GLES20.GL_TEXTURE_2D);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, -1);
	}
	
	public int width()
	{
		return mWidth;
	}
	
	public int height()
	{
		return mHeight;
	}
	
	int mTextureIds[];
	private ByteBuffer mPixels = null;
	private int mWidth = 0;
	private int mHeight = 0;
	int mDownscale = 0;
}
