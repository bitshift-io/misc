package engine.android;

import java.io.InputStream;

import engine.api.RenderBuffer;
import engine.api.Shader;
import engine.api.Texture;
import engine.api.Shader.Attribute;
import engine.api.Shader.Uniform;
import engine.math.Matrix4;
import engine.math.Vector4;
import engine.util.StringUtil;
import android.opengl.GLES20;
import android.util.Log;

public class AndroidShader extends Shader
{
	private static final String TAG = "AndroidShader";
		
	String 	mVertexShader;
	String 	mFragmentShader;
	int 	mProgramHandle = -1;
	int 	mVertexShaderHandle = -1;
    int 	mFragmentShaderHandle = -1;
	
	public static class Attribute extends Shader.Attribute
	{
		Attribute(int handle)
		{
			mHandle = handle;
		}
		
		public void Bind(RenderBuffer.AttributeBuffer buffer)
		{
		}
		
		public int mHandle;
	}
	
	public static class Uniform extends Shader.Uniform
	{
		Uniform(int handle)
		{
			mHandle = handle;
		}
		
		public void Bind(Matrix4 matrix)
		{
			float[] m = new float[16];
			for (int c = 0; c < 4; ++c)
			{
				for (int r = 0; r < 4; ++r)
				{	
					m[(c * 4) + r] = matrix.m[c][r];
				}
			}
			GLES20.glUniformMatrix4fv(mHandle, 1, false, m, 0);
		}
		
		public void Bind(Vector4 vec)
		{
			GLES20.glUniform4fv(mHandle, 1, vec.v, 0);
		}
		
		public void Bind(Texture tex)
		{
			AndroidTexture texture = (AndroidTexture) tex;
			if (texture == null || texture.mTextureIds == null)
			{
				return;
			}
			
			//GLES20.glEnable(GLES20.GL_TEXTURE_2D);
			GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture.mTextureIds[0]);
			GLES20.glUniform1i(mHandle, 0);
		}
		
		int mHandle;
	}
	
	public void Reload()
	{
		// NOTE: may need to keep an array of Attributes and Uniforms
		// and add a Reload to those as well, for now im assuming the handles shouldnt
		// change across a reload
		Load();
	}
	
	void Delete()
	{
		if (mVertexShaderHandle != -1)
		{
			GLES20.glDeleteShader(mVertexShaderHandle);
		}
		
		if (mFragmentShaderHandle != -1)
		{
			GLES20.glDeleteShader(mFragmentShaderHandle);
		}
		
		if (mProgramHandle != -1)
		{
			GLES20.glDeleteProgram(mProgramHandle);
		}
	}
	
	public void Load()
	{
		try
		{
			Delete();
			
			String vertexShader = StringUtil.readAll(mFactory.GetFileResource(mName + "_vert.glsl", "shd"));
			String fragmentShader = StringUtil.readAll(mFactory.GetFileResource(mName + "_frag.glsl", "shd"));
			
	        mVertexShaderHandle = compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);
	        mFragmentShaderHandle = compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);
	        mProgramHandle = createAndLinkProgram(mVertexShaderHandle, mFragmentShaderHandle, 
	        		new String[] {"a_Position"});     
		}
		catch (Exception e)
		{
			System.out.println("[AndroidTexture::Load] Exception: " + e.getMessage());
			return;
		}
	}
	
	public Attribute GetAttribute(Attribute.Type type)
	{
		// TODO: push this to an array thats setup on load 
		String typeName = "a_" + type.name();
		int handle = GLES20.glGetAttribLocation(mProgramHandle, typeName);
		if (handle < 0)
		{
			return null;
		}
		
		return new Attribute(handle);
	}
	
	public Uniform GetUniform(Uniform.Type type)
	{
		// TODO: push this to an array thats setup on load 
		String typeName = "u_" + type.name();
		int handle = GLES20.glGetUniformLocation(mProgramHandle, typeName);
		if (handle < 0)
		{
			return null;
		}
		
		return new Uniform(handle);
	}
		
	public void Bind()
	{
        GLES20.glUseProgram(mProgramHandle);
	}
	
	public void Unbind()
	{
		
	}
	
	/** 
	 * Helper function to compile a shader.
	 * 
	 * @param shaderType The shader type.
	 * @param shaderSource The shader source code.
	 * @return An OpenGL handle to the shader.
	 */
	private int compileShader(final int shaderType, final String shaderSource) 
	{
		int shaderHandle = GLES20.glCreateShader(shaderType);

		if (shaderHandle != 0) 
		{
			// Pass in the shader source.
			GLES20.glShaderSource(shaderHandle, shaderSource);

			// Compile the shader.
			GLES20.glCompileShader(shaderHandle);

			// Get the compilation status.
			final int[] compileStatus = new int[1];
			GLES20.glGetShaderiv(shaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

			// If the compilation failed, delete the shader.
			if (compileStatus[0] == 0) 
			{
				Log.e(TAG, "Error compiling shader: " + GLES20.glGetShaderInfoLog(shaderHandle));
				GLES20.glDeleteShader(shaderHandle);
				shaderHandle = 0;
			}
		}

		if (shaderHandle == 0)
		{			
			throw new RuntimeException("Error creating shader.");
		}
		
		return shaderHandle;
	}
	

	/**
	 * Helper function to compile and link a program.
	 * 
	 * @param vertexShaderHandle An OpenGL handle to an already-compiled vertex shader.
	 * @param fragmentShaderHandle An OpenGL handle to an already-compiled fragment shader.
	 * @param attributes Attributes that need to be bound to the program.
	 * @return An OpenGL handle to the program.
	 */
	private int createAndLinkProgram(final int vertexShaderHandle, final int fragmentShaderHandle, final String[] attributes) 
	{
		int programHandle = GLES20.glCreateProgram();
		
		if (programHandle != 0) 
		{
			// Bind the vertex shader to the program.
			GLES20.glAttachShader(programHandle, vertexShaderHandle);			

			// Bind the fragment shader to the program.
			GLES20.glAttachShader(programHandle, fragmentShaderHandle);
			
			// Bind attributes
			if (attributes != null)
			{
				final int size = attributes.length;
				for (int i = 0; i < size; i++)
				{
					GLES20.glBindAttribLocation(programHandle, i, attributes[i]);
				}						
			}
			
			// Link the two shaders together into a program.
			GLES20.glLinkProgram(programHandle);

			// Get the link status.
			final int[] linkStatus = new int[1];
			GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);

			// If the link failed, delete the program.
			if (linkStatus[0] == 0) 
			{				
				Log.e(TAG, "Error compiling program: " + GLES20.glGetProgramInfoLog(programHandle));
				GLES20.glDeleteProgram(programHandle);
				programHandle = 0;
			}
		}
		
		if (programHandle == 0)
		{
			throw new RuntimeException("Error creating program.");
		}
		
		return programHandle;
	}
}
