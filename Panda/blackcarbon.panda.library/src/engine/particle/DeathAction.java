package engine.particle;

import java.util.Iterator;

public class DeathAction extends Action
{
	public float			mAge;		// age of particles killed
	public float			mVariation;
	
	public void execute(ParticleList particleList)
	{
		if (!mEnabled)
		{
			return;
		}
		
		Iterator<Particle> it = particleList.iterator();
		while (it.hasNext())
		{
			Particle p = it.next();
			if (mAge < 0.f || p.mAge > mAge)
			{
				p.despawn();
				particleList.despawn(p);
			}
		}
	}
}
