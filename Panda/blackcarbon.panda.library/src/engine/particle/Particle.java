package engine.particle;

import engine.math.Matrix4;
import engine.math.Vector4;
import engine.math.Math;

public class Particle
{		
	public void update(float deltaTime)
	{
		mAge += deltaTime;
		mTransform.SetTranslation(mTransform.GetTranslation().Add(mVelocity.Multiply(deltaTime)));
	}
	
	public void spawn()
	{
		mAge = 0.f;
		mRandom = Math.Random(0.f, 1.f);
		mTransform 	= new Matrix4(Matrix4.Init.Identity);
		mVelocity	= new Vector4();
	}
	
	public void despawn()
	{
		
	}
	
	public void draw(Matrix4 transform)
	{
		
	}
		
	public float					mAge;
	public Vector4					mColour		= new Vector4(1, 1, 1, 1);
	public Vector4					mSize		= new Vector4(1, 1, 1);
	public float					mMass;
	public Matrix4					mTransform 	= new Matrix4(Matrix4.Init.Identity);
	public Vector4					mVelocity	= new Vector4();
	public float					mDistToPointSqrd;
	public int						mId;
	public float					mRandom; // random value between 0>1
}
