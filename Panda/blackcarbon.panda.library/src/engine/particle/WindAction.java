package engine.particle;

import java.util.Iterator;

import engine.math.Plane;
import engine.math.Vector4;
import engine.math.Math;

public class WindAction extends ForceAction
{
	public float	mTurbulence;
	public float	mFrequency;
	public float	mScale;

	void executeDirectional(ParticleList particleList)
	{
		if (!mEnabled)
		{
			return;
		}
		
		Iterator<Particle> it = particleList.iterator();
		while (it.hasNext())
		{
			Particle particle = it.next();
			
			float strength = mStrength;
			if (mDecay != 0.f)
			{
				Plane p = new Plane(mTransform.GetTranslation(), mTransform.GetZAxis());
				float dist = Math.Abs(p.DistanceToPoint(particle.mTransform.GetTranslation()));

				float decayValue = Math.Exp(-mDecay * dist);
				strength *= decayValue;
			}
			
			Vector4 turbulence = new Vector4();
			if (mTurbulence != 0.0f) 
			{
				/*
				#define METERS_TO_MAX	39.37f
				#define MAX_TO_METERS	1.0f / 39.37f
				*/
				float time = mTime * 4800;
				Vector4 vForceDir = mTransform.GetTranslation().Subtract(particle.mTransform.GetTranslation()); // * METERS_TO_MAX;
				Vector4 p;
				p    = vForceDir;
				p.SetX(mFrequency * time);
				turbulence.SetX(Math.Noise3(p.Multiply(mScale)));
				p    = vForceDir;
				p.SetY(mFrequency * time);
				turbulence.SetZ(Math.Noise3(p.Multiply(mScale))); // swap z and y cuz max blows
				p    = vForceDir;
				p.SetZ(mFrequency * time);
				turbulence.SetY(Math.Noise3(p.Multiply(mScale)));

				turbulence = turbulence.Multiply(mTurbulence);
			} 
			
			Vector4 velocityDelta = ((mTransform.GetZAxis().Multiply(strength)).Add(turbulence)).Multiply(mDeltaTime * mInfluence);
			particle.mVelocity = particle.mVelocity.Add(velocityDelta);
		}
	}
	
	void executePoint(ParticleList particleList)
	{
		
	}
}
