package engine.particle;

import java.util.Iterator;
import engine.math.Math;

public class SpeedClamp extends Action
{
	public float 	mMax = -1.0f;
	public float	mMin = -1.0f;
	
	void execute(ParticleList particleList)
	{
		if (!mEnabled)
		{
			return;
		}
		
		Iterator<Particle> it = particleList.iterator();
		while (it.hasNext())
		{
			Particle particle = it.next();
			
			float mag = particle.mVelocity.Normalize3();
			
			if (mMin > 0.f)
			{
				mag = Math.Max(mMin, mag);
			}
			
			if (mMax > 0.f)
			{
				mag = Math.Min(mMax, mag);
			}
			
			particle.mVelocity = particle.mVelocity.Multiply(mag);
		}
	}
}
