package engine.particle;

import java.util.Iterator;

import engine.math.Plane;
import engine.math.Matrix4;
import engine.math.Math;
import engine.math.Vector4;

/*!
 * 
 */
public class ForceAction extends Action
{
	public enum ForceType
	{
		FT_Directional, // Particles are pushed away alone the z-axis
		FT_Point,
	}
	
	public float			mInfluence = 1.0f; 	// how much particle is affected by wind
	public float			mStrength = 1.0f;	// how string the wind is
	public float			mDecay;				// decay on distance from wind source
	public ForceType		mType;
	//SceneNode*		mNode;
	public Matrix4			mTransform = new Matrix4(Matrix4.Init.Identity);
	
	float 			mDeltaTime;
	float			mTime;

	void execute(ParticleList particleList)
	{
		if (!mEnabled)
		{
			return;
		}
		
		switch (mType)
		{
		case FT_Directional:
			executeDirectional(particleList);
			break;
			
		case FT_Point:
			executePoint(particleList);
			break;
		}
	}
	
	void executeDirectional(ParticleList particleList)
	{
		Iterator<Particle> it = particleList.iterator();
		while (it.hasNext())
		{
			Particle particle = it.next();
			
			float strength = mStrength;
			if (mDecay != 0.f)
			{
				Plane p = new Plane(mTransform.GetTranslation(), mTransform.GetZAxis());
				float dist = Math.Abs(p.DistanceToPoint(particle.mTransform.GetTranslation()));
	
				float decayValue = Math.Exp(-mDecay * dist);
				strength *= decayValue;
			}
			
			Vector4 velocityDelta = mTransform.GetZAxis().Multiply(mDeltaTime * mInfluence * strength);
			particle.mVelocity = particle.mVelocity.Add(velocityDelta);
		}
	}
	
	void executePoint(ParticleList particleList)
	{
		Iterator<Particle> it = particleList.iterator();
		while (it.hasNext())
		{
			Particle particle = it.next();
			
			Vector4 vForceDir = mTransform.GetTranslation().Subtract(particle.mTransform.GetTranslation());
			float dist = vForceDir.Normalize3();
	
			float strength = mStrength;
			if (mDecay != 0.f)
			{
				float decayValue = Math.Exp(-mDecay * dist);
				strength *= decayValue;
			}
			
			Vector4 velocityDelta = vForceDir.Multiply(mDeltaTime * mInfluence * strength);
			particle.mVelocity = particle.mVelocity.Add(velocityDelta); 
		}
	}

	void update(float deltaTime)
	{
		mDeltaTime = deltaTime;
		mTime += deltaTime;
	}
}
