package engine.particle;

import java.util.Iterator;

import engine.api.Camera;
import engine.math.Matrix4;
import engine.math.Vector4;
import engine.math.Math;

public class OffCameraBirthAction extends BirthAction
{
	public Camera			mCamera;
	public Matrix4			mTransform; // transform of the particle system we want to cull
	public float			mBoundRadius; // bound of particles
	public float			mSpeed;		// if set, will apply a velocity so the particle attempts to go onscreen
	
	public void execute(ParticleList particleList)
	{
		if (!mEnabled)
		{
			return;
		}
		
		super.execute(particleList);
		
		float halfWidth = (mCamera.Width() * 0.5f) + mBoundRadius;
		float halfHeight = (mCamera.Height() * 0.5f) + mBoundRadius;
				
		Iterator<Particle> it = particleList.iterator();
		while (it.hasNext())
		{
			Particle p = it.next();
			if (p.mAge <= 0.f)
			{
				Vector4 offCameraPos = null;
				Vector4 velocity = null;
	
				int side = Math.Random(0, 4);
				switch (side)
				{
				// top
				case 0:
					offCameraPos = new Vector4(Math.Random(-halfWidth, halfWidth), halfHeight);
					velocity = Vector4.FromAngle(Math.Random(Math.PI, Math.PI2));
					break;
					
				// bottom
				case 1:
					offCameraPos = new Vector4(Math.Random(-halfWidth, halfWidth), -halfHeight);
					velocity = Vector4.FromAngle(Math.Random(0, Math.PI));
					break;
					
				// right
				case 2:
					offCameraPos = new Vector4(halfWidth, Math.Random(-halfHeight, halfHeight));
					velocity = Vector4.FromAngle(Math.Random(Math.PI * 0.5f, Math.PI * 1.5f));
					break;
					
				// left
				case 3:
					offCameraPos = new Vector4(-halfWidth, Math.Random(-halfHeight, halfHeight));
					velocity = Vector4.FromAngle(Math.Random(Math.PI * 0.5f, Math.PI * 1.5f)).Multiply(-1.f);
					break;
				}
								
				if (mSpeed > 0.f)
				{
					velocity.Multiply(mSpeed);
					p.mVelocity = velocity;
				}
				
				Vector4 particlePos = offCameraPos.Subtract(mTransform.GetTranslation());
				p.mTransform.SetTranslation(particlePos);
				
			}
		}
	}
}
