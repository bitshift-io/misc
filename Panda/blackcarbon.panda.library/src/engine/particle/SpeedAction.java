package engine.particle;

public class SpeedAction extends Action
{
	enum SpeedDirection
	{
		SD_ZAxis,
		SD_XAxis,
		SD_YAxis,
		SD_Random,
		SD_Horizontal,
	}
	
	float			mSpeed;
	float			mVariation;
	float			mDivergence;
	SpeedDirection	mDirection;
}
