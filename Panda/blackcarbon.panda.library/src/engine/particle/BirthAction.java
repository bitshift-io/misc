package engine.particle;

public class BirthAction extends Action
{
	public enum BirthType
	{
		BT_Amount,
		BT_Rate,
	}

	public BirthType	mType;
	public float		mTime;
	public float		mStartTime;
	public float		mEndTime;
	public float		mEmitCount;
	public float		mRate;
	public float		mAmount;
	
	public void update(float deltaTime)
	{
		if (!mEnabled)
		{
			return;
		}
		
		mTime += deltaTime;
		
		// only want to leave the decimal value behind
		// call update, then execute action
		while (mEmitCount >= 1.f)
		{
			mEmitCount -= 1.f;
		}
		
		mEmitCount += deltaTime * mRate;
	}
	
	public void execute(ParticleList particleGroup)
	{
		if (!mEnabled)
		{
			return;
		}
		
		switch (mType)
		{
		case BT_Rate:
			{
				float emitCount = mEmitCount;
				while (emitCount >= 1.f)
				{
					Particle p = particleGroup.spawn();
					if (p != null)
					{
						p.spawn();
					}
					emitCount -= 1.f;
				}
			}
			break;

		case BT_Amount:
			{
				/*
				if (system->GetTime() < mStartTime || system->GetTime() > mEndTime)
					return;

				float delta = system->GetDeltaTime();
				float rate = mAmount / (mEndTime - mStartTime);
				int amount = (int)(delta * rate);
				amount = Math::Max(1, amount); // TODO: look in to a better way to do this

				// add new particles - we need to save ur state some where, in the container :-/
				for (int i = 0; i < amount; ++i)
					system->AddParticle(mActionGroup);

				//system->AddToIgnoreList(this);
				 
				*/
			}
			break;
			
		default:
			break;
		}	
	}
}
