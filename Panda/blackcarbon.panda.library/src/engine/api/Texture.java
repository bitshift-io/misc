package engine.api;

public class Texture extends Resource 
{
	static int sDownscale = 0;
	
	public void Load()
	{
		
	}
	
	public void Bind()
	{

	}
	
	public void Unbind()
	{
		
	}
	
	public int width()
	{
		return 0;
	}
	
	public int height()
	{
		return 0;
	}
	
	public void Reload()
	{
		Load();
	}
	
	public static int downscale()
	{
		return sDownscale;
	}
	
	public static void setDownscale(int downscale)
	{
		sDownscale = downscale;
	}
}
