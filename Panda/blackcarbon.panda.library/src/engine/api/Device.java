package engine.api;

import java.util.concurrent.Semaphore;

import engine.math.Math;
import engine.math.Matrix4;
import engine.math.Vector4;

//
// handles window and 3d functionality
//
public class Device 
{
	// Clear flags
	static public int CF_Depth			= (1 << 0);
	static public int CF_Colour			= (1 << 1);
	
	public enum GeometryTopology
	{
		LineList,
		PointList,
		TriangleList,
		//TriangleListAdjacent,
		TriangleStrip,
		QuadList,
	}

	public enum MatrixMode
	{
		Texture,
		Model,
		View,
		Projection,
	}

	public enum BlendMode
	{
		Zero,
		One,
		DestColour,
		SrcColour,
		OneMinusDestColour,
		OneMinusSrcColour,
		SrcAlpha,
		OneMinusSrcAlpha,
		DestAlpha,
		OneMinusDestAlpha,
		SrcAlphaSaturate,
	}

	public enum Compare
	{
		Less,
		LessEqual,
		Equal,
		Greater,
		GreaterEqual,
		Always, // TODO: fix device enums
		Never,
		NotEqual,
	}

	public enum Operation
	{
		Zero, // biatch!
		Keep,
		Replace,
		Increment,
		Decrement,
		Invert,
	}

	public enum CullFace
	{
		Front,
		Back,
		None,
	}

	public enum DrawType
	{
		NotInstanced// = -1,
	}

	//TODO: have a clear it takes a ptr to a clear state
	//and flags on what to clear
	public enum CleaFlags
	{
		Stencil,
		Colour,
	}

	public static class Viewport
	{/*
		Viewport() :
			nearFarDepth(0.f, 1.f)
		{
		}

		Vector4 topLeft;
		Vector4 bottomRight;
		Vector4 nearFarDepth;*/
	};

	public static class ClearState
	{/*
		Vector4			clearColour;
		unsigned int	stencilClear;
		float			clearDepth;*/
	}

	public static class StencilFace
	{
		Compare		compare		=	Compare.LessEqual;
		Operation	pass		=	Operation.Zero;
		Operation	fail		=	Operation.Zero;
		Operation	depthFail	=	Operation.Zero;			
	}

	public static class DeviceState
	{
		// dirty flags
		public static int DF_VertexFormat 						= 1 << 0;
		public static int DF_Topology 							= 1 << 1;
		public static int DF_CullFace 							= 1 << 2;
		
		public static int DF_AlphaToCoverageEnable 				= 1 << 3;
		public static int DF_AlphaBlendEnable 					= 1 << 4;
		public static int DF_AlphaBlendSource 					= 1 << 5;
		public static int DF_AlphaBlendDest 					= 1 << 6;
		public static int DF_AlphaCompare 						= 1 << 7;
		public static int DF_AlphaCompareValue 					= 1 << 8;
		
		public static int DF_DepthTestEnable 					= 1 << 9;
		public static int DF_DepthWriteEnable 					= 1 << 10;
		public static int DF_DepthCompare 						= 1 << 11;
		
		public static int DF_StencilEnable 						= 1 << 12;
		public static int DF_StencilReadMask 					= 1 << 13;
		public static int DF_StencilWriteMask 					= 1 << 14;
		
		public static int DF_StencilFace 						= 1 << 15;
		public static int DF_Colour								= 1 << 16;
		
		public void Copy(DeviceState other)
		{
			if (vertexFormat != other.vertexFormat)
			{
				vertexFormat = other.vertexFormat;
				dirtyFlags |= DF_VertexFormat;
			}
			
			if (topology != other.topology)
			{
				topology = other.topology;
				dirtyFlags |= DF_Topology;
			}
			
			if (cullFace != other.cullFace)
			{
				cullFace = other.cullFace;
				dirtyFlags |= DF_CullFace;
			}
			
			if (alphaToCoverageEnable != other.alphaToCoverageEnable)
			{
				alphaToCoverageEnable = other.alphaToCoverageEnable;
				dirtyFlags |= DF_AlphaToCoverageEnable;
			}
			
			if (alphaBlendEnable != other.alphaBlendEnable)
			{
				alphaBlendEnable = other.alphaBlendEnable;
				dirtyFlags |= DF_AlphaBlendEnable;
			}
			
			if (alphaBlendSource != other.alphaBlendSource)
			{
				alphaBlendSource = other.alphaBlendSource;
				dirtyFlags |= DF_AlphaBlendSource;
			}
			
			if (alphaBlendDest != other.alphaBlendDest)
			{
				alphaBlendDest = other.alphaBlendDest;
				dirtyFlags |= DF_AlphaBlendDest;
			}
			
			if (alphaCompare != other.alphaCompare)
			{
				alphaCompare = other.alphaCompare;
				dirtyFlags |= DF_AlphaCompare;
			}
			
			if (alphaCompareValue != other.alphaCompareValue)
			{
				alphaCompareValue = other.alphaCompareValue;
				dirtyFlags |= DF_AlphaCompareValue;
			}
			
			if (depthTestEnable != other.depthTestEnable)
			{
				depthTestEnable = other.depthTestEnable;
				dirtyFlags |= DF_DepthTestEnable;
			}
			
			if (depthWriteEnable != other.depthWriteEnable)
			{
				depthWriteEnable = other.depthWriteEnable;
				dirtyFlags |= DF_DepthWriteEnable;
			}
			
			if (depthCompare != other.depthCompare)
			{
				depthCompare = other.depthCompare;
				dirtyFlags |= DF_DepthCompare;
			}
			
			if (stencilEnable != other.stencilEnable)
			{
				stencilEnable = other.stencilEnable;
				dirtyFlags |= DF_StencilEnable;
			}
			
			if (stencilReadMask != other.stencilReadMask)
			{
				stencilReadMask = other.stencilReadMask;
				dirtyFlags |= DF_StencilReadMask;
			}
			
			if (stencilWriteMask != other.stencilWriteMask)
			{
				stencilWriteMask = other.stencilWriteMask;
				dirtyFlags |= DF_StencilWriteMask;
			}
			
			if (stencilFace[0] != other.stencilFace[0])
			{
				stencilFace[0] = other.stencilFace[0];
				dirtyFlags |= DF_StencilFace;
			}
			
			if (stencilFace[1] != other.stencilFace[1])
			{
				stencilFace[1] = other.stencilFace[1];
				dirtyFlags |= DF_StencilFace;
			}
			
			if (colour.GetX() != other.colour.GetX() || colour.GetY() != other.colour.GetY()
				|| colour.GetZ() != other.colour.GetZ() || colour.GetW() != other.colour.GetW())
			{
				colour.Copy(other.colour);
				dirtyFlags |= DF_Colour;
			}
		}
		
		// geometry
		public int					vertexFormat			=	-1;
		public GeometryTopology		topology				=	GeometryTopology.TriangleList;
		public CullFace				cullFace				= 	CullFace.Back;

		// alpha
		public boolean				alphaToCoverageEnable	=	false;
		public boolean				alphaBlendEnable		=	false;
		public BlendMode			alphaBlendSource		=	BlendMode.SrcAlpha;
		public BlendMode			alphaBlendDest			=	BlendMode.OneMinusSrcAlpha;
		public Compare				alphaCompare			=	Compare.Always;
		public float				alphaCompareValue		= 	0.5f;
		
		// depth
		public boolean				depthTestEnable			=	true;
		public boolean				depthWriteEnable		=	true;
		public Compare				depthCompare			=	Compare.LessEqual;

		// stencil
		public boolean				stencilEnable			=	false;
		public int					stencilReadMask			=	-1;
		public int					stencilWriteMask		=	-1;

		public StencilFace			stencilFace[]			=	new StencilFace[2];
		
		public Vector4				colour					= 	new Vector4(1.f, 1.f, 1.f, 1.f);
		
		public int					dirtyFlags				= 	0;
	}
	
	public Device()
	{
		ReleaseSemaphore();
	}

	public void SetWindowVisible(boolean visible)
	{

	}
	
	public void SetClearColour(Vector4 colour)
	{
		
	}
	
	public void Clear(int clearFlags)
	{
		
	}
	
	public void Begin()
	{
		AcquireSemaphore();
		/*
		// restore default rendering
		Matrix4 identity = Memory.Stack.New(Matrix4.Init.Identity); //new Matrix4(Matrix4.Init.Identity);
		identity.SetIdentity();
		SetMatrix(identity, MatrixMode.Model);
		SetMatrix(identity, MatrixMode.Texture);
		SetMatrix(identity, MatrixMode.View);
		SetMatrix(identity, MatrixMode.Projection);*/
	}
	
	public void End()
	{
		ReleaseSemaphore();
	}

	public void SetMatrix(Matrix4 matrix, MatrixMode mode)
	{
	
	}

	public Matrix4 GetMatrix(MatrixMode mode)
	{
		return null;
	}
	
	public int GetWidth()
	{
		return -1;
	}
	
	public int GetHeight()
	{
		return -1;
	}
	
	public float GetAspectRatio()
	{
		return (float)(GetWidth()) / (float)(GetHeight());
	}
	
	public void Repaint()
	{

	}
	
	public void Resize(int width, int height)
	{
	
	}
	
	public void SetDeviceState(DeviceState state)
	{
		mDeviceState.Copy(state);
	}
	/*
	public void SetTexture(Texture texture)
	{
		if (mTexture != texture)
		{
			if (texture == null)
			{
				mTexture.Unbind();
			}
			else
			{
				mDirtyFlags |= DF_Texture;
			}
			
			mTexture = texture;
		}
	}
	
	public void SetRenderBuffer(RenderBuffer renderBuffer)
	{
		// dynamic buffers are designed to change frequently
		if (mRenderBuffer != renderBuffer || (mRenderBuffer != null && mRenderBuffer.GetUsage() == RenderBuffer.Dynamic))
		{
			if (renderBuffer == null)
			{
				mRenderBuffer.Unbind();
			}
			else
			{
				mDirtyFlags |= DF_RenderBuffer;
			}
			
			mRenderBuffer = renderBuffer;
		}
	}
		
	public void Render()
	{
		// change device state here
		// make sure you clear mDirtyFlags
	}*/
		
	public DeviceState GetDeviceState()
	{
		return null;
	}
	
	public boolean Update()
	{
		return true;
	}
	

	public void AcquireSemaphore()
	{
		//try
		//{
			mSemaphore.acquireUninterruptibly();
		//} 
		//catch (InterruptedException e) 
		//{
		//	e.printStackTrace();
		//}
	}
	
	public void ReleaseSemaphore()
	{
		mSemaphore.release();
	}
	
	public static int 	DF_Texture			= 1 << 0;
	public static int	DF_RenderBuffer		= 1 << 1;
	public static int 	DF_World			= 1 << 2;
	public static int 	DF_View				= 1 << 3;
	public static int 	DF_Projection		= 1 << 4;
	public static int 	DF_TextureMatrix	= 1 << 5;

	protected int			mDirtyFlags				= 0;
	/*
	protected Texture		mTexture				= null;
	protected RenderBuffer	mRenderBuffer			= null;
	
*/
	protected Matrix4		mView					= new Matrix4(Matrix4.Init.Identity);
    protected Matrix4		mWorld					= new Matrix4(Matrix4.Init.Identity);
	protected Matrix4		mProjection				= new Matrix4(Matrix4.Init.Identity);
	protected Matrix4		mTextureMatrix			= new Matrix4(Matrix4.Init.Identity);
	
	protected DeviceState	mDeviceState			= new DeviceState();
	
	public Semaphore		mSemaphore					= new Semaphore(0, true); // for multithreaded loading
	public Factory			mFactory;
}
