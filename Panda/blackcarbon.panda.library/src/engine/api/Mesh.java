package engine.api;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import engine.api.RenderBuffer.AttributeBuffer;

public class Mesh extends Resource 
{
	// http://www.captain.at/howto-java-convert-binary-data.php
	public static float arr2float (byte[] arr, int start) 
	{
		int i = 0;
		int len = 4;
		int cnt = 0;
		byte[] tmp = new byte[len];
		for (i = start; i < (start + len); i++) {
			tmp[cnt] = arr[i];
			cnt++;
		}
		int accum = 0;
		i = 0;
		for ( int shiftBy = 0; shiftBy < 32; shiftBy += 8 ) {
			accum |= ( (long)( tmp[i] & 0xff ) ) << shiftBy;
			i++;
		}
		return Float.intBitsToFloat(accum);
	}
	
	public float[] ReadFloats(DataInputStream dis, int num) throws IOException
	{
		byte[] bData = new byte[num * 4];
		//int bytesRead = 
			dis.read(bData);
		
		float[] data = new float[num];
		
		int cnt = 0;
		for (int start = 0; start < (num * 4); start = start + 4)
		{
			data[cnt] = arr2float(bData, start);
	        cnt++;
	    }

		return data;
	}
	
	public void Load()
	{/*
		InputStream is = mFactory.GetFileResource(mName, "msh");
		if (is == null)
			return;
		
		DataInputStream dis =  new DataInputStream(is);

		Device device = mFactory.GetDevice();
		
		mRenderBuffer = mFactory.CreateRenderBuffer();
		
		try 
		{
			// read in the header information
			byte numIndicies = dis.readByte();
			byte numVertices = dis.readByte();
			byte vertexFormat = dis.readByte();
			byte materialNameLength = dis.readByte();
			
			if (materialNameLength > 0)
			{
				materialNameLength += 1; // read in null terminator
				
				byte[] materialName = new byte[materialNameLength];
				dis.read(materialName, 0, materialNameLength);
				
				String materialNameStr = new String(materialName);
				mMaterial = mFactory.AcquireMaterial(materialNameStr);
			}
			else
			{
				mMaterial = mFactory.AcquireMaterial(mName);
			}
			
			// after texture has freed its mutex
			device.Begin();
			
			// read indices
			byte indices[] = new byte[numIndicies];
			dis.read(indices, 0, numIndicies);
			mRenderBuffer.SetIndicies(indices);
			
			// read in position
			if ((vertexFormat & RenderBuffer.Position) != 0)
			{
				float[] data = ReadFloats(dis, numVertices * 3);
				mRenderBuffer.SetPositions(data);
			}
			
			// read in uvs
			if ((vertexFormat & RenderBuffer.UV) != 0)
			{
				float[] data = ReadFloats(dis, numVertices * 2);
				mRenderBuffer.SetUVs(data);
			}
			
			// read in colours
			if ((vertexFormat & RenderBuffer.Colour) != 0)
			{
				float[] data = ReadFloats(dis, numVertices * 4);
				mRenderBuffer.SetColours(data);
			}             
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		device.End();
		*/
	}
	
	public void SetRenderBuffer(RenderBuffer rb)
	{
		mRenderBuffer = rb;
	}
	
	public void SetMaterial(Material material)
	{
		mMaterial = material;
	}
	
	public void Render()
	{		
		int numPasses = mMaterial.Begin();
		for (int p = 0; p < numPasses; ++p)
		{
			Shader shader = mMaterial.BeginPass(p);
			mRenderBuffer.Bind(shader);
			mRenderBuffer.Render();
			mMaterial.EndPass();
		}
	}
	
	public void MakeStatic()
	{
		mRenderBuffer.MakeStatic();
	}
	
	RenderBuffer	mRenderBuffer;
	Material		mMaterial;
}
