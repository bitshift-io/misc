package engine.api;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import engine.math.Vector4;

public class Font extends Resource
{
	// alignment is in relation to the pivot point
	// eg. right = right of pivot point
	public enum HorizontalAlign
	{
		Left,
		Centre,
		Right,
	}
	
	public enum VerticalAlign
	{
		Above,
		Centre,
		Below,
	}
	
	class CharDesc
	{
		short	x			=	0;
		short	y			=	0;

		short	width		=	0;
		short	height		=	0;

		short	xOffset		=	0;
		short	yOffset		=	0;

		short	xAdvance	=	0;

		Vector4	position[];
		Vector4	texCoord[];
	}
	
	public void Load()
	{/*
		int resID = device.GetResourceID(mName + "_fnt");
		if (resID == 0)
			return;
		
		
		
		InputStream is = device.GetResources().openRawResource(resID);
		*/
		try
		{
			InputStream is = mFactory.GetFileResource(mName, "fnt");
			
			//String resourceDir = mFactory.GetInitParam().resourceDir;
			//String fileName = mName + "_fnt.fnt";
			//if (resourceDir != null && resourceDir.length() > 0)
			//	fileName = resourceDir + "/" + fileName;
			//InputStream is = new FileInputStream(resourceDir + "/" + mName + "_fnt.fnt");
			InputStreamReader isr =  new InputStreamReader(is);
			BufferedReader input = new BufferedReader(isr);
		
			mTexture = mFactory.AcquireTexture(mName);
			mRenderBuffer = mFactory.CreateRenderBuffer();
		
			short spacing[] = new short[2];
			short scaleW = 0;
			short scaleH = 0;
			short lineHeight = 0;
			
			// read the head:
			// info face="Arial" size=60 bold=0 italic=0 charset="ANSI" stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=1,1
			
			String line = input.readLine(); //not declared within while loop
			StringTokenizer st = new StringTokenizer(line);
			String token = null;
			while (st.hasMoreTokens())
			{
				token = st.nextToken("= ");
				if (token.equalsIgnoreCase("spacing"))
				{
					token = st.nextToken("= ");
					StringTokenizer spacingST = new StringTokenizer(token);
					token = spacingST.nextToken(",");
					spacing[0] = Integer.valueOf(token).shortValue();
					token = spacingST.nextToken(",");
					spacing[1] = Integer.valueOf(token).shortValue();
				}
			}
			
			// parse the second line:
			// common lineHeight=12 base=10 scaleW=128 scaleH=128 pages=1 packed=0 encoded=0
			
			line = input.readLine(); //not declared within while loop
			st = new StringTokenizer(line);
			while (st.hasMoreTokens())
			{
				token = st.nextToken("= ");
				if (token.equalsIgnoreCase("scaleW"))
				{
					token = st.nextToken("= ");
					scaleW = Integer.valueOf(token).shortValue();
				}
				else if (token.equalsIgnoreCase("scaleH"))
				{
					token = st.nextToken("= ");
					scaleH = Integer.valueOf(token).shortValue();
				}
				else if (token.equalsIgnoreCase("lineHeight"))
				{
					token = st.nextToken("= ");
					lineHeight = Integer.valueOf(token).shortValue();
				}
			}
			
			mHeight = (short) (lineHeight + spacing[1]);
			mTexWidth = scaleW;
			mTexHeight = scaleH;
			
			// skip 2 lines of unneeded stuff
			input.readLine();
			input.readLine();

			// read the body, each character
			// char id=32   x=0     y=120   width=1     height=0     xoffset=0     yoffset=12    xadvance=4     page=0  chnl=15
			while ((line = input.readLine()) != null)
			{
				st = new StringTokenizer(line);
				token = st.nextToken("= ");
				if (!token.equalsIgnoreCase("char"))
					continue;
				
				CharDesc charDesc = null;
				while (st.hasMoreTokens())
				{
					token = st.nextToken("= ");
					String valueToken = st.nextToken("= ");
					short value = Integer.valueOf(valueToken).shortValue();
					
					if (token.equalsIgnoreCase("id"))
						charDesc = mCharacter[value] = new CharDesc();
					else if (token.equalsIgnoreCase("x"))
						charDesc.x = value;
					else if (token.equalsIgnoreCase("y"))
						charDesc.y = value;
					else if (token.equalsIgnoreCase("width"))
						charDesc.width = value;
					else if (token.equalsIgnoreCase("height"))
						charDesc.height = value;
					else if (token.equalsIgnoreCase("xoffset"))
						charDesc.xOffset = value;
					else if (token.equalsIgnoreCase("yoffset"))
						charDesc.yOffset = value;
					else if (token.equalsIgnoreCase("xadvance"))
						charDesc.xAdvance = (short) (value + spacing[0]);
				}
			}
			
			// set up position and uv's
			for (int i = 0; i < 256; ++i)
			{
				CharDesc charDesc = mCharacter[i];
				if (charDesc == null)
					continue;

				charDesc.position = new Vector4[4];
				charDesc.texCoord = new Vector4[4];
				
				// top left
				charDesc.position[0] = new Vector4((float)(charDesc.xOffset), -(float)(charDesc.yOffset + charDesc.height), 0.f);
				charDesc.texCoord[0] = new Vector4((float)(charDesc.x) / (float)(mTexWidth), (float)(charDesc.y + charDesc.height) / (float)(mTexHeight));

				// bottom left
				charDesc.position[1] = new Vector4((float)(charDesc.xOffset), -(float)(charDesc.yOffset), 0.f);
				charDesc.texCoord[1] = new Vector4((float)(charDesc.x) / (float)(mTexWidth), (float)(charDesc.y) / (float)(mTexHeight));

				// bottom right
				charDesc.position[2] = new Vector4((float)(charDesc.xOffset + charDesc.width), -(float)(charDesc.yOffset), 0.f);
				charDesc.texCoord[2] = new Vector4((float)(charDesc.x + charDesc.width) / (float)(mTexWidth), (float)(charDesc.y) / (float)(mTexHeight));

				// top right
				charDesc.position[3] = new Vector4((float)(charDesc.xOffset + charDesc.width), -(float)(charDesc.yOffset + charDesc.height), 0.f);
				charDesc.texCoord[3] = new Vector4((float)(charDesc.x + charDesc.width) / (float)(mTexWidth), (float)(charDesc.y + charDesc.height) / (float)(mTexHeight));
			}

		}
		catch (Exception e)
		{
			System.out.println("[Font::Load] Exception: " + e.getMessage());
		}
	}

	public void SetVerticalAlign(VerticalAlign align)
	{
		mVerticalAlign = align;
	}
	
	public void SetHorizontalAlign(HorizontalAlign align)
	{
		mHorizontalAlign = align;
	}
	
	public Vector4 GetDimensions(String text)
	{		
		Vector4 dimensions = new Vector4();
		dimensions.Set(0.f, mHeight, 0.f, 0.f);

		float curLineLength = 0.f;
		for (int i = 0; i < text.length(); ++i)
		{
			char c = text.charAt(i);
			
			if (c == '\n')
			{
				if (curLineLength > dimensions.GetX())
					dimensions.SetX(curLineLength);
				
				dimensions.SetY(dimensions.GetY() + mHeight);
				curLineLength = 0.f;
				continue;
			}
			
			CharDesc desc = mCharacter[(byte)(c)];
			if (desc == null)
				continue;
			
			curLineLength += desc.xAdvance;
		}
		
		if (curLineLength > dimensions.GetX())
			dimensions.SetX(curLineLength);

		return dimensions;
	}
	
	public float GetHeight()
	{
		return mHeight;
	}
	/*
	// render using current view world projection settings
	public void Render(String text)
	{
		if (text == null)
			return;
	
		RenderToRenderBuffer(mRenderBuffer, text);
		
		Device device = Factory.Get().GetDevice();
		device.SetTexture(mTexture);
		device.SetRenderBuffer(mRenderBuffer);
		device.Render();
		
		/*
		mTexture.Bind();
		mRenderBuffer.Bind();
		mRenderBuffer.Render();
		mRenderBuffer.Unbind();
		mTexture.Unbind();* /
	}*/
	
	public void RenderToRenderBuffer(RenderBuffer rb, String text)
	{		
		Vector4 dimensions = GetDimensions(text);
		Vector4 offset = new Vector4();
			//new Vector4();
		/*
		enum VerticalAlign
		{
			Left,
			Centre,
			Right,
		}
		
		enum HorizontalAlign
		{
			Above,
			Centre,
			Below,
		}*/
		
		switch (mHorizontalAlign)
		{
		case Left:
			offset.SetX(-dimensions.GetX());
			break;
			
		case Centre:
			offset.SetX(-dimensions.GetX() * 0.5f);
			break;
			
		case Right:
			break;
		}
		
		switch (mVerticalAlign)
		{
		case Above:
			offset.SetY(dimensions.GetY());
			break;
			
		case Centre:
			offset.SetY(dimensions.GetY() * 0.5f);
			break;
			
		case Below:
			break;
		}
		

		float xOrig = 0.f;

		// count number of valid characters
		int validCharCount = 0;
		for (int i = 0; i < text.length(); ++i)
		{
			char c = text.charAt(i);
			
			if (c == '\n')
				continue;
			
			CharDesc desc = mCharacter[(byte)(c)];
			if (desc == null)
				continue;
			
			++validCharCount;
		}
		
		Vector4 vert[] = new Vector4[validCharCount * 4];
		Vector4 uvs[] = new Vector4[validCharCount * 4];
		
		int vertIdx = 0;
		for (int i = 0; i < text.length(); ++i)
		{
			char c = text.charAt(i);
			
			if (c == '\n')
			{
				offset.SetY(offset.GetY() - mHeight);
				offset.SetX(xOrig);
				continue;
			}
			
			// calculate text pos and put quad into buffer
			CharDesc desc = mCharacter[(byte)(c)];
			if (desc == null)
				continue;
			
			for (int j = 0; j < 4; ++j)
			{
				vert[vertIdx + j] = new Vector4();
				
				vert[vertIdx + j] = offset.Add(desc.position[j]);;
				uvs[vertIdx + j] = desc.texCoord[j];
			}

			vertIdx += 4;
			offset.SetX(offset.GetX() + desc.xAdvance);
		}
		
		mRenderBuffer.SetPositions(vert);
		mRenderBuffer.SetUVs(uvs);
		
		int idx = 0;
		byte vIdx = 0;
		int numIndicies = validCharCount * 2 * 3;
		byte indicies[] = new byte[numIndicies];
		for (int i = 0; i < validCharCount; ++i)
		{
			// set up indices
			indicies[idx + 0] = (byte) (vIdx + 0);
			indicies[idx + 1] = (byte) (vIdx + 2);
			indicies[idx + 2] = (byte) (vIdx + 1);

			indicies[idx + 3] = (byte) (vIdx + 0);
			indicies[idx + 4] = (byte) (vIdx + 3);
			indicies[idx + 5] = (byte) (vIdx + 2);

			idx += 6;
			vIdx += 4;
		}
		
		mRenderBuffer.SetIndicies(indicies);
	}
	
	public Texture GetTexture()
	{
		return mTexture;
	}
	
	private CharDesc		mCharacter[] 	= 	new CharDesc[256];
	public Texture 		mTexture;
	private RenderBuffer 	mRenderBuffer;
	
	private short			mHeight;
	private short			mTexWidth;
	private short			mTexHeight;
	
	private VerticalAlign	mVerticalAlign		= VerticalAlign.Below;
	private HorizontalAlign mHorizontalAlign	= HorizontalAlign.Right;
}
