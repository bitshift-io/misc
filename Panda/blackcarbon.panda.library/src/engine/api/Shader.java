package engine.api;

import engine.math.Matrix4;
import engine.math.Vector4;

public class Shader extends Resource
{
	public static class Attribute
	{
		public enum Type
		{
			Position,
			Colour,
			UV,
			Normal,
			Tangent,
			Binormal,
			Index,
			Max, // leave this last
		}
		
		public void Bind(RenderBuffer.AttributeBuffer buffer)
		{
		}
	}
	
	public static class Uniform
	{
		public enum Type
		{
			WorldMatrix,
			ViewMatrix,
			ProjectionMatrix,
			WorldViewProjectionMatrix,
			TextureMatrix,
			Texture,
			DiffuseColour,
			Max, // leave this last
		}
		
		public void Bind(Matrix4 matrix)
		{
		}
		
		public void Bind(Vector4 vec)
		{
		}

		public void Bind(Texture tex)
		{
		}
	}
	
	public Attribute GetAttribute(Attribute.Type type)
	{
		return null;
	}
	
	public Uniform GetUniform(Uniform.Type type)
	{
		return null;
	}
	
	public void Bind()
	{
	}
	
	public void Load()
	{
		
	}
	
	public void Reload()
	{
	}
}
