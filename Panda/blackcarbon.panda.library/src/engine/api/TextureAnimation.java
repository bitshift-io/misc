package engine.api;

import engine.math.Vector4;
import engine.math.Matrix4;

public class TextureAnimation
{
	// Animation flags
	static public int AF_None				= 0;
	static public int AF_Horizontal			= (1 << 0);
	static public int AF_Vertical			= (1 << 1);
	static public int AF_NoLoop				= (1 << 2);
	static public int AF_Paused				= (1 << 3);
	static public int AF_Bounce				= (1 << 4);
	static public int AF_Playing			= (1 << 5);
	static public int AF_PlayingReverse		= (1 << 6);
		
	int		mFlags 					= AF_None;
	Matrix4	mTransform				= new Matrix4();
	
	float	mTime					= 0.f;
	float	mFrameRate				= 12.f;
	
	int		mCurWidth				= 0;
	int		mCurHeight				= 0;
	
	int		mNumWidth				= 1;
	int		mNumHeight				= 1;
	
	public TextureAnimation(int flags, int numWidth, int numHeight)
	{
		mFlags = flags;
		mNumWidth = numWidth;
		mNumHeight = numHeight;
	}
	
	public int Width()
	{
		return mNumWidth;
	}
	
	public int Height()
	{
		return mNumHeight;
	}
	
	public void SetCurrentHeight(int height)
	{
		mCurHeight = height;
	}
	
	public void SetFrameRate(float frameRate)
	{
		mFrameRate = frameRate;
	}
	
	public int Flags()
	{
		return mFlags;
	}
	
	public Matrix4 Transform()
	{
		return mTransform;
	}
	
	public void Reset()
	{
		mTime = 0.f;
		mCurWidth = 0;
		mCurHeight = 0;
		mFlags = mFlags & ~(AF_Paused | AF_PlayingReverse);
		mFlags |= AF_Playing;
	}
		
	int updateFrame(int curFrame, int maxFrame)
	{
		boolean playingInReverse = ((mFlags & AF_PlayingReverse) != 0);
		if (playingInReverse)
		{
			if (curFrame <= 0)
			{
				if ((mFlags & AF_NoLoop) != 0)
					mFlags &= ~(AF_Playing | AF_PlayingReverse);
				else
					curFrame = 1;
			}
			else
			{
				--curFrame;
			}
		}
		else
		{
			if ((curFrame + 1) >= maxFrame)
			{
				// bounce indicates that the coomplete animation now needs to be played in reverse
				if ((mFlags & AF_Bounce) != 0)
				{
					mFlags |= AF_PlayingReverse;
					--curFrame;
					return curFrame;
				}
				
				if ((mFlags & AF_NoLoop) != 0)
					mFlags &= ~AF_Playing;
				else
					curFrame = 0;
			}
			else
			{
				++curFrame;
			}
		}
		
		return curFrame;
	}
	
	public void Update(float deltaTime)
	{
		if (mFlags == AF_None || (mFlags & AF_Paused) != 0 || (mFlags & AF_Playing) == 0)
			return;
		
		// deal with sprite animation
		mTime += deltaTime;
		
		float frameTime = 1.f / mFrameRate;
		if (mTime > frameTime)
		{
			mTime -= frameTime;
			
			if ((mFlags & AF_Horizontal) != 0)
			{
				mCurWidth = updateFrame(mCurWidth, mNumWidth);
				/*
				if ((mCurWidth + 1) >= mNumWidth)
				{
					if ((mFlags & AF_NoLoop) != 0)
						mFlags &= ~AF_Playing;
					else
						mCurWidth = 0;
				}
				else
				{
					++mCurWidth;
				}*/
			}
				
			if ((mFlags & AF_Vertical) != 0)
			{
				mCurHeight = updateFrame(mCurHeight, mNumHeight);
				/*
				if ((mCurHeight + 1) >= mNumHeight)
				{
					if ((mFlags & AF_NoLoop) != 0)
						mFlags &= ~AF_Playing;
					else
						mCurHeight = 0;
				}
				else
				{
					++mCurHeight;
				}*/
			}
		}
		
		// set new texture matrix to scroll the texture
		Vector4 translation = new Vector4();
		translation.Set((float) mCurWidth / (float)mNumWidth, (float)mCurHeight / (float)mNumHeight, 0.f, 1.f);
		Vector4 scale = new Vector4();
		scale.Set(1.f / (float)mNumWidth, 1.f / (float)mNumHeight, 1.f, 0.f);
		mTransform = new Matrix4(Matrix4.Init.Identity);
		mTransform.SetScale(scale);
		mTransform.SetTranslation(translation);
	}
	
	public void Bind()
	{
		Device device = Factory.Get().GetDevice();
		device.SetMatrix(mTransform, Device.MatrixMode.Texture);
	}
}
