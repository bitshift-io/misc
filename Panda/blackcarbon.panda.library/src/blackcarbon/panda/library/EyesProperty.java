package blackcarbon.panda.library;

import blackcarbon.panda.library.ILayer.Event;
import engine.math.Math;
import engine.math.Matrix4;
import engine.math.Vector4;
import android.graphics.PointF;

public class EyesProperty extends IProperty
{
	public EyesProperty(String name, Vector4 scale, float eyeRadius, float blinkRadius, Vector4 leftCenter, Vector4 rightCenter) 
	{		
		// scale as a % of canvas height
		mScale = scale;
		
		mLeftCenter = leftCenter;
		mRightCenter = rightCenter;
		
		mLeftOffset = new Vector4(-140.0f, -200.f);
		mRightOffset = new Vector4(140.0f, -200.f);
		
		mRadius = eyeRadius;
		mBlinkRadius = blinkRadius;
		/*
		ParallaxTranslateProperty ptp = new ParallaxTranslateProperty(this);
		ptp.mLag = lag;
		ptp.mParallax = parallax;
		ptp.mOffset = new Vector4(0.0f, 0.0f);
		*/
		mDrawProperty = new TexturedQuadProperty(name);
		mBlinkProperty = new BlinkProperty();
	}
	
	public void setLayer(ILayer layer)
	{
		mDrawProperty.setLayer(layer);
		mBlinkProperty.setLayer(layer);
		super.setLayer(layer);
	}

	public void update(float deltaTime)
	{
		mBlinkProperty.update(deltaTime);
		
		if (System.currentTimeMillis() > mTime)
		{
			ChangeEyeState();
		}
		
		// update eye location
		Vector4 dir = mLookAt.Subtract(mCurrentLookAt);
		float distance = dir.Normalize3();
		if (distance > 0.f)
		{
			mCurrentLookAt = mCurrentLookAt.Add(dir.Multiply(distance * mEyeLag));
		}
	}
	
	public void ChangeEyeState()
	{
		// change look at location
		LiveWallpaperService service = LiveWallpaperService.Get();
		
		boolean adjustHeight = true;
		int lookIdx = Math.Random(0, 9);
		switch (lookIdx)
		{
		default:
			mLookAt = service.touchPoint();
			adjustHeight = false;
			break;
						
		case 2:
			mLookAt = new Vector4(-service.mCamera.Width() * 0.5f, -service.mCamera.Height() * 0.5f);
			break;
			
		case 3:
			mLookAt = new Vector4(service.mCamera.Width() * 0.5f, service.mCamera.Height() * 0.5f);
			break;
			
		case 4:
			mLookAt = new Vector4(0, service.mCamera.Width() * 0.5f);
			break;
			
		case 5:
			mLookAt = new Vector4(0, -service.mCamera.Width() * 0.5f);
			break;
			
		case 6:
			mLookAt = new Vector4(service.mCamera.Width() * 0.5f, 0);
			break;
			
		case 7:
			mLookAt = new Vector4(-service.mCamera.Width() * 0.5f, 0);
			break;
			
		case 8:
			mLookAt = new Vector4(-service.mCamera.Width() * 0.5f, service.mCamera.Height() * 0.5f);
			break;
			
		case 9:
			mLookAt = new Vector4(service.mCamera.Width() * 0.5f, -service.mCamera.Height() * 0.5f);
			break;
		}
		if (adjustHeight)
		{
			// bring the whole grid to be based on the x eye location so panda isnt always looking up so much
			mLookAt.SetY(mLookAt.GetY() - (mLeftCenter.GetY() * scale()));
		}
		
		mTime = System.currentTimeMillis() + Math.Random(mTimeMin, mTimeMax);
	}
	
	public void notifyEvent(Event event)
	{
		LiveWallpaperService service = LiveWallpaperService.Get();
		switch (event)
		{
		case Touch:
			ChangeEyeState();
			mLookAt = service.touchPoint();
			break;
			
		case Unpause:
			ChangeEyeState();
			break;
			
		default:
			ChangeEyeState();
			break;
		}
	}
	
	public float scale()
	{
		LiveWallpaperService service = LiveWallpaperService.Get();
		return mScale.GetY() * service.mCamera.Height();
	}
	
	public Vector4 touchPointLocal(Matrix4 transform)
	{
		// as long as we don't do any rotation,
		// this code should work, otherwise we will need to do a matrix inverse!
		//LiveWallpaperRenderer r = mRenderer;
		Vector4 localTouchPoint = mCurrentLookAt.Subtract(transform.GetTranslation()); //r.touchPoint(). 
		return localTouchPoint;
	}
	
	@Override
	public void draw(Matrix4 transform)
	{
		LiveWallpaperService service = LiveWallpaperService.Get();
		
		Matrix4 t = new Matrix4(transform);
		t.SetScale(new Vector4(scale(), scale(), 1.f));
		
		Vector4 lookAt = touchPointLocal(transform);
		
		// lower values lower the responsiveness of the eyes to move
		// towards the touch point
		float distanceScale = 0.14f;
		
		Vector4 leftCenter =  mLeftCenter.Multiply(scale());		
		Vector4 lookAtDirectionLeft = lookAt.Subtract(leftCenter);
		float distanceLeft = lookAtDirectionLeft.Normalize3();
		distanceLeft = Math.Min(distanceLeft * distanceScale, mRadius * scale());
		mLeftOffset = leftCenter.Add(lookAtDirectionLeft.Multiply(distanceLeft));
		
		
		
		Vector4 rightCenter =  mRightCenter.Multiply(scale());
		Vector4 lookAtDirectionRight = lookAt.Subtract(rightCenter);
		float distanceRight = lookAtDirectionRight.Normalize3();
		distanceRight = Math.Min(distanceRight * distanceScale, mRadius * scale());
		mRightOffset = rightCenter.Add(lookAtDirectionRight.Multiply(distanceRight));

		// average the eye height
		float averageY = (mLeftOffset.GetY() + mRightOffset.GetY()) * 0.5f;
		mLeftOffset.SetY(averageY);
		mRightOffset.SetY(averageY);
		

		Matrix4 transformLeft = new Matrix4(t);
		transformLeft.SetTranslation(transform.GetTranslation().Add(mLeftOffset));

		Matrix4 transformRight = new Matrix4(t);
		transformRight.SetTranslation(transform.GetTranslation().Add(mRightOffset));
		
		mDrawProperty.draw(transformLeft);
		mDrawProperty.draw(transformRight);
		
		
		
		
		// draw blink 
		transformLeft.SetScale(new Vector4(mBlinkRadius * service.mCamera.Height(), mBlinkRadius * service.mCamera.Height()));
		transformLeft.SetTranslation(transform.GetTranslation().Add(leftCenter));
		mBlinkProperty.draw(transformLeft);
		
		transformRight.SetScale(new Vector4(-mBlinkRadius * service.mCamera.Height(), mBlinkRadius * service.mCamera.Height()));
		transformRight.SetTranslation(transform.GetTranslation().Add(rightCenter));
		mBlinkProperty.draw(transformRight);
	}	
	
	BlinkProperty			mBlinkProperty;	
	TexturedQuadProperty	mDrawProperty;
	
	Vector4 mLookAt = new Vector4();
	Vector4 mCurrentLookAt = new Vector4();
	
	long	mTimeMax		= 2000;
	long	mTimeMin		= 4000;
	
	float	mEyeLag			= 0.05f; // lower is slower eyes
	long	mTime;
	float	mRadius;		// radius of eye socket
	float	mBlinkRadius;	// radius of blink
	
	Vector4 mLeftOffset;	// current eye location, relative to center
	Vector4 mRightOffset;	// current eye location, relative to center
	Vector4	mScale;			// scale of eyes
	
	Vector4	mLeftCenter;	// center of eye
	Vector4 mRightCenter;	// center of eye
}
