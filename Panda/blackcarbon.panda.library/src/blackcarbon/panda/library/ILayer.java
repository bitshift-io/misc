package blackcarbon.panda.library;

import java.util.ArrayList;

import engine.math.Matrix4;

// In future, 
// we can add separate lists
// for update, transform, draw
// to avoid calling empty functions if we want
public class ILayer 
{
	enum Event
	{
		Touch,
		Unpause,
		SurfaceChanged, // probably rotation
	}
	
	public ILayer()
	{
	}
	
	public void update(float deltaTime)
	{
		for (int l = 0; l < mPropertyList.size(); ++l)
    	{
    		IProperty property = mPropertyList.get(l);
    		property.update(deltaTime);
    	}	
	}
	
	public Matrix4 transform()
	{
		Matrix4 t = new Matrix4();
		t.SetIdentity();
		
		for (int l = 0; l < mPropertyList.size(); ++l)
    	{
    		IProperty property = mPropertyList.get(l);
    		t = property.transform(t);
    	}	
		
		return t;
	}
	
	public void draw()
	{
		Matrix4 transform = transform();
		for (int l = 0; l < mPropertyList.size(); ++l)
    	{
    		IProperty property = mPropertyList.get(l);
    		property.draw(transform);
    	}	
	}
	
	public void notifyEvent(Event event)
	{
		for (int l = 0; l < mPropertyList.size(); ++l)
    	{
    		IProperty property = mPropertyList.get(l);
    		property.notifyEvent(event);
    	}
	}
	
	public void add(IProperty property)
	{
		property.setLayer(this);
		mPropertyList.add(property);
	}

	ArrayList<IProperty>	mPropertyList = new ArrayList<IProperty>();
}
