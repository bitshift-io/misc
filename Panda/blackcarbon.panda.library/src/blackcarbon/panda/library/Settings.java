package blackcarbon.panda.library;

import engine.android.OpenGLES2WallpaperService.EngineType;
import engine.api.Factory;
import engine.api.Texture;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
//import blackcarbon.panda.android.R;
import android.preference.PreferenceManager;
import android.util.Log;

public class Settings extends PreferenceActivity 
{
	public static final String IMAGE_QUALITY = "image_quality";
	public static final String FRAME_RATE = "frame_rate";
		
	static class SettingsChangeListener implements SharedPreferences.OnSharedPreferenceChangeListener
	{
		@Override
        public void onSharedPreferenceChanged(SharedPreferences prefs, String key) 
		{
			if (key.equals(IMAGE_QUALITY))
			{
				Texture.setDownscale(Integer.parseInt(prefs.getString(key, "0")));
				Factory.Get().ReloadResources();
			}
			else if (key.equals(FRAME_RATE))
			{
				LiveWallpaperService.Get().setFrameRate(Float.parseFloat(prefs.getString(key, "30")));
			}
        }
	}
	
	
	static SettingsChangeListener mListener = new  SettingsChangeListener();
	
	public static void applySettings(Context context)
	{
		SharedPreferences prefs = PreferenceManager
		          .getDefaultSharedPreferences(context);
		
		mListener.onSharedPreferenceChanged(prefs, IMAGE_QUALITY);
		mListener.onSharedPreferenceChanged(prefs, FRAME_RATE);
	}
	
	protected void onCreate(Bundle savedInstanceState)
	{		
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);	
	}	
	
	@Override
	protected void onResume()
	{
	    super.onResume();
	    // Set up a listener whenever a key changes
	    getPreferenceScreen().getSharedPreferences()
	            .registerOnSharedPreferenceChangeListener(mListener);
	}

	@Override
	protected void onPause()
	{
	    super.onPause();
	    // Unregister the listener whenever a key changes
	    getPreferenceScreen().getSharedPreferences()
	            .unregisterOnSharedPreferenceChangeListener(mListener);
	}
	
	boolean getBool(String key, boolean defValue)
	{
		SharedPreferences prefs = PreferenceManager
		          .getDefaultSharedPreferences(this);
		
		return prefs.getBoolean(key, defValue);
	}
	
	int getInt(String key, int defValue)
	{
		SharedPreferences prefs = PreferenceManager
		          .getDefaultSharedPreferences(this);
		
		return prefs.getInt(key, defValue);
	}
	
}
