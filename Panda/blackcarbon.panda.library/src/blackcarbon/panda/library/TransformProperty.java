package blackcarbon.panda.library;

import engine.math.Matrix4;
import engine.math.Vector4;

public class TransformProperty extends IProperty 
{
	Matrix4 mTransform;
	
	public TransformProperty(Matrix4 transform) 
	{
		mTransform = transform;
	}
	
	public Matrix4 transform(Matrix4 transform)
	{
		// TODO: matrix multiply
		return mTransform;
	}
}

