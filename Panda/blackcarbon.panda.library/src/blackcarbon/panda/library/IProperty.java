package blackcarbon.panda.library;

import blackcarbon.panda.library.ILayer.Event;
import engine.math.Matrix4;

public class IProperty 
{
	public IProperty()
	{	
	}
		
	public void setLayer(ILayer layer)
	{
		mLayer = layer;
		/*
		if (mLayer != null)
		{
			mLayer.mPropertyList.add(this);
		}*/
	}
	
	public void update(float deltaTime)
	{
	
	}
	
	public Matrix4 transform(Matrix4 transform)
	{
		return transform;
	}
	
	public void draw(Matrix4 transform)
	{

	}
		
	public void notifyEvent(Event event)
	{
		
	}
	
	ILayer	mLayer;
}
