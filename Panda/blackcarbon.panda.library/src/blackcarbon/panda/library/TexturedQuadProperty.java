package blackcarbon.panda.library;

import android.graphics.PointF;
import engine.api.Device;
import engine.api.Factory;
import engine.api.Material;
import engine.api.Mesh;
import engine.api.RenderBuffer;
import engine.api.Texture;
import engine.math.Matrix4;
import engine.math.Vector4;

public class TexturedQuadProperty extends IProperty
{
	public TexturedQuadProperty(String name)
	{			
		if (name.length() > 0)
		{
			mMaterial = Factory.Get().AcquireMaterial(name);
		}
		
		mGeometry = Factory.Get().AcquireRenderBuffer("quad");
		if (mGeometry.AttributeCount() <= 0)
		{
			mGeometry.CreateQuad(null);
			mGeometry.MakeStatic();
		}
		
		mMesh = Factory.Get().AcquireMesh(name);
		mMesh.SetMaterial(mMaterial);
		mMesh.SetRenderBuffer(mGeometry);
	}
	
	public void draw(Matrix4 transform)
	{
		// test fill rate:
		//transform.SetScale(new Vector4(10, 10, 10));
		
		Device device = Factory.Get().GetDevice();
		device.SetMatrix(transform, Device.MatrixMode.Model);
		mMesh.Render();
	}
	
	public float textureAspectRatio()
	{
		return mMaterial.GetTexture().width() / mMaterial.GetTexture().height();
	}
		Mesh	mMesh;
	RenderBuffer mGeometry;
	Material mMaterial;
}
