package blackcarbon.panda.library;

import engine.math.Matrix4;
import engine.math.Vector4;

public class LayerFactory 
{
	static ILayer createParallax(String name, float parallax, float lag, Vector4 scale, Vector4 offset)
	{
		ILayer layer = new ILayer();
		ParallaxTranslateProperty ptp = new ParallaxTranslateProperty();
		ptp.mLag = lag;
		ptp.mParallax = parallax;
		ptp.mOffset = offset;
		layer.add(ptp);
		
		TexturedQuadProperty tqp = new TexturedQuadProperty(name);
		layer.add(tqp);
		
		ParallaxScaleProperty psp = new ParallaxScaleProperty(tqp.textureAspectRatio(), scale);
		layer.add(psp);
		return layer;
	}
	
	static ILayer createStatic(String name)
	{
		ILayer layer = new ILayer();
		ScaleToCanvasProperty sp = new ScaleToCanvasProperty();	
		layer.add(sp);
		TexturedQuadProperty tqp = new TexturedQuadProperty(name);
		layer.add(tqp);
		return layer;
	}
	
	static ILayer createTrialWatermark(String name, Vector4 scale)
	{
		ILayer layer = new ILayer();
		Matrix4 transform = new Matrix4(Matrix4.Init.Identity);
		transform.SetScale(scale);
		transform.SetTranslation(new Vector4(0.f, 0.3f, 0.f));
		TransformProperty tp = new TransformProperty(transform);	
		layer.add(tp);
		TexturedQuadProperty tqp = new TexturedQuadProperty(name);
		layer.add(tqp);
		return layer;		
	}
	
	static ILayer createFireflyParticleSystem(int poolSize, float parallax, float lag, float scale)
	{
		ILayer layer = new ILayer();
		ParallaxTranslateProperty ptp = new ParallaxTranslateProperty();
		ptp.mLag = lag;
		ptp.mParallax = parallax;
		ptp.mOffset = new Vector4();
		layer.add(ptp);
		
		FireflyParticleSystem fps = new FireflyParticleSystem(poolSize, scale);
		layer.add(fps);
		return layer;
	}
	
	static ILayer createButterflyParticleSystem(int poolSize, float parallax, float lag, float scale)
	{
		ILayer layer = new ILayer();
		ParallaxTranslateProperty ptp = new ParallaxTranslateProperty();
		ptp.mLag = lag;
		ptp.mParallax = parallax;
		ptp.mOffset = new Vector4();
		layer.add(ptp);
		
		ButterflyParticleSystem fps = new ButterflyParticleSystem(poolSize, scale);
		layer.add(fps);
		return layer;
	}
}
