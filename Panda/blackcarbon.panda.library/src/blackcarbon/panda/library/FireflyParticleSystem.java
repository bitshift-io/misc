package blackcarbon.panda.library;

import java.util.Iterator;

import blackcarbon.panda.library.ILayer.Event;

import engine.api.TextureAnimation;
import engine.math.Math;
import engine.math.Matrix4;
import engine.math.Vector4;
import engine.particle.ActionList;
import engine.particle.BirthAction;
import engine.particle.DeathAction;
import engine.particle.DampAction;
import engine.particle.OffCameraDeathAction;
import engine.particle.ParticleActionGroup;
import engine.particle.ParticleList;
import engine.particle.ParticleSystem;
import engine.particle.WindAction;
import engine.particle.ForceAction;

public class FireflyParticleSystem extends ParticleSystemProperty
{
	static float mMaxLife = 5.f;
	
	public static class FireflyParticle extends ParticleSystemProperty.Particle
	{
		public FireflyParticle()
		{		
			Init("firefly.png", 8, 2, 15);
		}
		
		public void update(float deltaTime)
		{
			super.update(deltaTime);
			
			// update alpha
			// sin from 0 > PI gives us the curve we are after,
			// we can use POW to push it up/outward
			float lifePercent = mAge / mMaxLife;
			mColour.SetW(Math.Sin(lifePercent * Math.PI));
		}
	}
	
	FireflyParticleSystem(int poolSize, float scale)
	{				
		mScale = scale;
		
		// set up the action list
		ActionList al = new ActionList();
		BirthAction ba = new BirthAction();
		ba.mType = BirthAction.BirthType.BT_Rate;
		ba.mRate = 1.3f;
		al.add(ba);
		
		DeathAction da = new DeathAction();
		da.mAge = mMaxLife;
		al.add(da);

		mWind = new WindAction();
		mWind.mTurbulence = 0.5f;
		mWind.mFrequency = 100.5f;
		mWind.mScale = 0.1f;
		mWind.mStrength = 0.04f; //screen % per sec
		mWind.mInfluence = 0.5f;
		mWind.mType = ForceAction.ForceType.FT_Directional;
		mWind.mTransform.CreateFromForward(new Vector4(1, 0, 0), new Vector4(0, 1, 0));		
		al.add(mWind);
		
		// helps negate effect of touch repelant
		// and causes the wind to take over again 
		// as fireflys move away from touch point
		DampAction drag = new DampAction();
		drag.mDamping = 0.8f;
		al.add(drag);
		
		// pushes fireflys away from this point
		mTouchForce.mType = ForceAction.ForceType.FT_Point;
		mTouchForce.mDecay = 50.0f;
		mTouchForce.mStrength = -5.f;
		mTouchForce.mInfluence = 1.f;
		al.add(mTouchForce);
				
		Init(al, FireflyParticle.class, poolSize);
	}
	
	public void notifyEvent(Event event)
	{
		switch (event)
		{
		case Touch:
			// update wind direction
			LiveWallpaperService service = LiveWallpaperService.Get();
			Vector4 forward = new Vector4(1, 0, 0);
			if (service.swipDirection().GetX() < 0.f)
			{
				forward = new Vector4(-1, 0, 0);
			}
			mWind.mTransform.CreateFromForward(forward, new Vector4(0, 1, 0));
			break;
		}
	}
	
	public void update(float deltaTime)
	{
		super.update(deltaTime);
	}
	
	public void draw(Matrix4 transform)
	{
		if (mTouchForce != null)
		{
			mTouchForce.mTransform.SetTranslation(touchPointLocal(transform));
		}
		super.draw(transform);
	}
	
	public Vector4 touchPointLocal(Matrix4 transform)
	{
		// as long as we don't do any rotation,
		// this code should work, otherwise we will need to do a matrix inverse!
		LiveWallpaperService service = LiveWallpaperService.Get();
		Vector4 localTouchPoint = service.touchPoint().Subtract(transform.GetTranslation()); //r.touchPoint(). 
		return localTouchPoint;
	}
	
	ForceAction mTouchForce = new ForceAction();
	WindAction	mWind = new WindAction();
}
