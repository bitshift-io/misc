package blackcarbon.panda.library;

import engine.math.Matrix4;
import engine.math.Vector4;

public class SkewSwayProperty extends IProperty
{
	public SkewSwayProperty()
	{
	}

	@Override
	public void update(float deltaTime) 
	{
		mTime += mSpeed;
		float s = (float) Math.sin(mTime);
		mSkew = mMin + (mMax - mMin) * s;
	}

	@Override
	public Matrix4 transform(Matrix4 transform)
	{
		float scaleY = transform.GetScale().GetY() * 0.5f;
		
		Matrix4 skewMatrix = new Matrix4();
		skewMatrix.SetIdentity();
		skewMatrix.SetSkewX(mSkew);
		
		Matrix4 preTranslate = new Matrix4();
		preTranslate.SetIdentity();
		preTranslate.SetTranslation(new Vector4(0, scaleY, 0));
		
		Matrix4 postTranslate = new Matrix4();
		postTranslate.SetIdentity();
		postTranslate.SetTranslation(new Vector4(0, -scaleY, 0));
		
		transform = transform.Multiply(preTranslate);
		transform = transform.Multiply(skewMatrix);
		transform = transform.Multiply(postTranslate);
		
		return transform;
	}
	
	float 	mTime = 0.0f;
	float	mSkew = 0.0f;
	float	mSpeed = 0.01f;
	float	mMin = -0.02f;
	float	mMax = 0.02f;
}
