/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package blackcarbon.panda.library;

import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import blackcarbon.panda.library.ILayer.Event;
import engine.android.AndroidDevice;
import engine.android.AndroidFactory;
import engine.android.GLWallpaperService;
import engine.android.OpenGLES2WallpaperService;
import engine.android.OpenGLES2WallpaperService.ES2GLSurfaceViewEngine;
import engine.android.OpenGLES2WallpaperService.ES2RgbrnGLEngine;
import engine.android.OpenGLES2WallpaperService.EngineType;
import engine.android.OpenGLES2WallpaperService.ProxyEngine;
import engine.api.Camera;
import engine.api.Device;
import engine.api.Factory;
import engine.api.RenderBuffer;
import engine.math.Matrix4;
import engine.math.Vector4;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.service.wallpaper.WallpaperService.Engine;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import android.opengl.GLSurfaceView.Renderer;
import android.preference.PreferenceManager;

public class LiveWallpaperService extends OpenGLES2WallpaperService  {
	
	private static final String SETTINGS_KEY = "use_gl_surface_view";
		
	ArrayList<ArrayList<ILayer>>	mLayerList;
	
	long mTime;
	
	float mCanvasOffset;
    int mOrientation = 1; //  1 = portrait  2 = landscape
    
    float mCanvasWidth;
    float mCanvasHeight;
    
    Vector4 mTouchPoint = new Vector4();
    Vector4	mSwipDir = new Vector4();
    
	public Camera mCamera = new Camera();
	
	public AndroidFactory mFactory = new AndroidFactory();
	public Device mDevice;
	
	public int mFramesThisSecond = 0;
	public float mFrameTime = 0.f;
	
	RenderBuffer mDebugQuad;
	public static boolean mDebug = false; // enable debugging code
	public static boolean mTrial = false; // enable trial code
	
	protected static LiveWallpaperService	mService;
	
	public long mFrameDelay = 50;
	
	static public LiveWallpaperService Get()
	{
		return mService;
	}

	public LiveWallpaperService()
	{
		super();
		
		// ensure debug mode cannot be enabled in a release build!
		if (BuildConfig.DEBUG == false)
		{
			mDebug = false;
		}
		
		if (mDebug)
		{
			AndroidFactory.waitForDebugger();
		}
		
		mService = this;		
		mFactory.SetContext(this);	
	}
	
	@Override
	public Engine onCreateEngine() 
	{
		SharedPreferences prefs = PreferenceManager
		          .getDefaultSharedPreferences(this);
		
		EngineType type = EngineType.ES2Rgbrn;
		if (prefs.getBoolean(SETTINGS_KEY, false))
		{
			//type = EngineType.GlSurface;
		}
		
		mEngine = new ProxyEngine(this, type);
		return mEngine;
	}

	public void init()
	{
		mDevice = new AndroidDevice();
		mFactory.SetDevice(mDevice);
		
		Settings.applySettings(this);
		
		setup();
		
		Matrix4 world = new Matrix4();
		world.SetIdentity();
		world.SetTranslation(new Vector4(0, 0, -17));
		mCamera.SetWorld(world);
	}
	
	public void setFrameRate(float frameRate)
	{
		mFrameDelay = (long) (1000.f / frameRate);
	}
	
	@Override
	public long frameDelay() 
	{
		return mFrameDelay;
	}
	
	public void drawFrame()
	{
		Device device = Factory.Get().GetDevice();
		device.Clear(0);
		
		update(); // todo: move to frame rate independent code
		
		mCamera.Bind();
		
		// set device state - enable alpha blending
    	Device.DeviceState state = new Device.DeviceState();
    	state.alphaBlendEnable = true;
    	//state.alphaCompare = Device.Compare.Greater;
    	state.alphaBlendSource = Device.BlendMode.SrcAlpha;
    	state.alphaBlendDest = Device.BlendMode.OneMinusSrcAlpha;
    	state.depthTestEnable = false; // TODO: MAKE ENGINE SUPPORT DPEHT TEST    	
		
		device.SetDeviceState(state);
		
		ArrayList<ILayer> layerList = layerList();
    	for (int l = 0; l < layerList.size(); ++l)
    	{
    		ILayer layer = layerList.get(l);
    		layer.draw();
    	}
    	
    	/*
    	// debug touch location
    	if (mDebug)
    	{
	    	Matrix4 touchMatrix = new Matrix4();
	    	touchMatrix.SetIdentity();
	    	touchMatrix.SetTranslation(touchPoint());
	    	touchMatrix.SetScale(new Vector4(50, 50));
	    	device.SetMatrix(touchMatrix, Device.MatrixMode.Model);
	    	
	    	state.colour = new Vector4(1,0,0,1);
	    	device.SetDeviceState(state);
	    	
	    	device.SetTexture(null);
	    	device.SetRenderBuffer(mDebugQuad);
	    	device.Render();
    	}*/
    	
    	++mFramesThisSecond;
	}
	
	@Override
	public void touchEvent(MotionEvent event)
	{
		// convert to the range -0.5 -> 0.5
		// so we can just multiply by camera width/height save math
		// in touchPoint()
		
		Vector4 prevTouchPoint = new Vector4(mTouchPoint);
		
		float x = event.getRawX();
		float y = event.getRawY();
		mTouchPoint.SetX((x / mCanvasWidth) - 0.5f);
		mTouchPoint.SetY(((mCanvasHeight - y) / mCanvasHeight) - 0.5f);
				
		Vector4 swip = mTouchPoint.Subtract(prevTouchPoint);
		if (swip.MagnitudeSqrd3() > 0.001)
		{		
			mSwipDir = mTouchPoint.Subtract(prevTouchPoint);
			//Log.d("SwipDir", "SwipDirX:" + mSwipDir.GetX());
		}
		
		notifyEvent(ILayer.Event.Touch);
	}
	
	public Vector4 touchPoint()
	{
		// mTouchX and mTouchY are in the space 
		// where 0,0 is top left,
		// and in screen coords
		// the camera othographic view makes the center of the screen
		// 0,0
		// so we need to offset it
		// on top of this we need to flip y axis
		// also need to scale it to fit camera coords
		//return new Vector4((mTouchX / mCamera.Width()) - (mCamera.Width() * 0.5f), -((mTouchY / mCamera.Height()) - (mCamera.Height() * 0.5f)));
		return new Vector4(mTouchPoint.GetX() * mCamera.Width(), mTouchPoint.GetY() * mCamera.Height());
	}
	
	public Vector4 swipDirection()
	{
		return new Vector4(mSwipDir.GetX() * mCamera.Width(), mSwipDir.GetY() * mCamera.Height());
	}
	
	@Override
	public void offsetsChanged(float xOffset, float yOffset, float xOffsetStep,
			float yOffsetStep, int xPixelOffset, int yPixelOffset)
    {
        mCanvasOffset = -(xOffset - 0.5f) * 2.0f; // make this -1 to 1 with 0 center
    }
		
	ArrayList<ILayer> layerList()
	{
		return mLayerList.get(mOrientation - 1);
	}
	
	public void setup()
	{				
		mLayerList = new ArrayList<ArrayList<ILayer>>();
    	mLayerList.add(new ArrayList<ILayer>());
    	mLayerList.add(new ArrayList<ILayer>());
    			
        // Layer Array
        ArrayList<ILayer> landscapeLayerList = mLayerList.get(Configuration.ORIENTATION_LANDSCAPE - 1); 
        ArrayList<ILayer> portraitLayerList = mLayerList.get(Configuration.ORIENTATION_PORTRAIT - 1);
        
        ILayer l = null;
        SkewSwayProperty swp = null;
        EyesProperty ep = null;

        //
        // Load in our layers here!
        //
        // image, parallax, lag, scale, offset
        // background
        
        l = LayerFactory.createParallax("background.png", 0.06f, 0.5f, new Vector4(1.0f, 1.0f), new Vector4(0.f, 0.f));
        landscapeLayerList.add(l);
        
        l = LayerFactory.createParallax("background.png", 0.06f, 0.5f, new Vector4(1.0f, 1.0f), new Vector4(0.f, 0.f));
        portraitLayerList.add(l);

        // bamboo mid     
        l = LayerFactory.createParallax("bamboo_mid.png", 0.08f, 0.4f, new Vector4(1.0f, 1.0f), new Vector4(0.f, 0.f));
        swp = new SkewSwayProperty();
        swp.mTime = 0.12f;
        swp.mSpeed = 0.02f;
        l.add(swp);
        landscapeLayerList.add(l);
        
        l = LayerFactory.createParallax("bamboo_mid.png", 0.15f, 0.4f, new Vector4(1.0f, 1.0f), new Vector4(0.f, 0.f));
        swp = new SkewSwayProperty();
        swp.mTime = 0.12f;
        swp.mSpeed = 0.02f;
        l.add(swp);
        portraitLayerList.add(l);
              
        
        // bamboo fore
        l = LayerFactory.createParallax("bamboo_fore.png", 0.1f, 0.3f, new Vector4(1.0f, 1.0f), new Vector4(0.f, 0.f));
        swp = new SkewSwayProperty();
        swp.mTime = -0.12f;
        swp.mSpeed = 0.015f;
        l.add(swp);
        landscapeLayerList.add(l);
        
        l = LayerFactory.createParallax("bamboo_fore.png", 0.25f, 0.3f, new Vector4(1.0f, 1.0f), new Vector4(0.f, 0.f));
        swp = new SkewSwayProperty();
        swp.mTime = -0.12f;
        swp.mSpeed = 0.015f;
        l.add(swp);
        portraitLayerList.add(l);
        
        // shadow 1
        l = LayerFactory.createParallax("bamboo_shadow.png", 0.15f, 0.3f, new Vector4(1.2f, 1.0f), new Vector4(0.f, 0.f));
        swp = new SkewSwayProperty();
        swp.mTime = 0.2f;
        swp.mSpeed = 0.03f;
        l.add(swp);
        landscapeLayerList.add(l);
        
        l = LayerFactory.createParallax("bamboo_shadow.png", 0.3f, 0.3f, new Vector4(1.2f, 1.0f), new Vector4(0.f, 0.f));
        swp = new SkewSwayProperty();
        swp.mTime = 0.2f;
        swp.mSpeed = 0.03f;
        l.add(swp);
        portraitLayerList.add(l);
   
        // shadow 2 (negative scaled)
        l = LayerFactory.createParallax("bamboo_shadow.png", 0.15f, 0.31f, new Vector4(-1.1f, 1.2f), new Vector4(0.f, 0.02f));
        swp = new SkewSwayProperty();
        swp.mTime = 0.5f;
        swp.mSpeed = 0.04f;
        l.add(swp);
        landscapeLayerList.add(l);
        
        l = LayerFactory.createParallax("bamboo_shadow.png", 0.3f, 0.31f, new Vector4(-1.1f, 1.2f), new Vector4(0.f, 0.02f));
        swp = new SkewSwayProperty();
        swp.mTime = 0.5f;
        swp.mSpeed = 0.04f;
        l.add(swp);
        portraitLayerList.add(l);          
                   
        // firefly particle system (near)
        l = LayerFactory.createFireflyParticleSystem(8, 0.17f, 0.37f, 0.2f);
        landscapeLayerList.add(l);
        
        l = LayerFactory.createFireflyParticleSystem(8, 0.32f, 0.37f, 0.1f);
        portraitLayerList.add(l);
  
        
        // panda head
        l = LayerFactory.createParallax("panda.png", 0.18f, 0.4f, new Vector4(0.9f, 0.9f), new Vector4(0.0f, -0.28f));
        // image, parallax, lag, scale, radius, left center, right center
        ep = new EyesProperty("panda_eye.png", new Vector4(0.19f, 0.19f), 0.18f, 0.25f, new Vector4(-0.92f, 0.5f), new Vector4(0.96f, 0.5f));
        l.add(ep);
        landscapeLayerList.add(l);
        
        l = LayerFactory.createParallax("panda.png", 0.33f, 0.4f, new Vector4(0.6f, 0.6f), new Vector4(0.0f, -0.22f));
        // image, parallax, lag, scale, radius, left center, right center
        ep = new EyesProperty("panda_eye.png", new Vector4(0.125f, 0.125f), 0.18f, 0.18f, new Vector4(-0.95f, 0.5f), new Vector4(0.99f, 0.5f));
        l.add(ep);
        portraitLayerList.add(l); 
        
        // butterfly particle system
        l = LayerFactory.createButterflyParticleSystem(1, 0.17f, 0.37f, 0.3f);
        landscapeLayerList.add(l);
        
        l = LayerFactory.createButterflyParticleSystem(1, 0.32f, 0.37f, 0.2f);
        portraitLayerList.add(l);
        
        // panda overlay - static layer
        l = LayerFactory.createStatic("l_overlay.png");
        landscapeLayerList.add(l);
        
        l = LayerFactory.createStatic("p_overlay.png");
        portraitLayerList.add(l);
        
        // add trial water mark
        if (mTrial)
        {
        	l = LayerFactory.createTrialWatermark("trial.png",  new Vector4(0.4f, 0.2f, 1.0f));
            landscapeLayerList.add(l);
            
            l = LayerFactory.createTrialWatermark("trial.png",  new Vector4(0.3f, 0.15f, 1.0f));
            portraitLayerList.add(l);
        }
        
        mDebugQuad = Factory.Get().AcquireRenderBuffer("quad");

        mTime = System.currentTimeMillis();
	}
		
	public void update()
	{
		/*
		if (LiveWallpaperRenderer.mDebug)
        {
        	AndroidFactory.waitForDebugger();
        }*/
		
		long currentTime = System.currentTimeMillis();
		float deltaTime = (currentTime - mTime) / 1000.f;
		mTime = currentTime;
		
		//Log.d("deltaTime", "deltaTime:" + deltaTime);
		
		if (mDebug)
		{
			deltaTime = 0.033f;
		}
		else
		{
			mFrameTime += deltaTime;
			if (mFrameTime > 1.0f)
			{
				//Log.d("FPS", "FPS: " + mFramesThisSecond);
				mFrameTime = 0;
				mFramesThisSecond = 0;			
			}
		}
		
		//Log.d("DeltaTime", "deltaTime: " + deltaTime);
		
		ArrayList<ILayer> layerList = layerList();
    	for (int l = 0; l < layerList.size(); ++l)
    	{
    		ILayer layer = layerList.get(l);
    		layer.update(deltaTime);
    	}
	}
	
	public void visibilityChanged(boolean visible)
	{
		if (visible)
		{
			mTime = System.currentTimeMillis();
			notifyEvent(ILayer.Event.Unpause);
		}
	}
	
	public void notifyEvent(Event event)
	{
		if (mLayerList == null)
		{
			return;
		}
		
		ArrayList<ILayer> layerList = layerList();
    	for (int l = 0; l < layerList.size(); ++l)
    	{
    		ILayer layer = layerList.get(l);
    		layer.notifyEvent(event);
    	}	
	}
	
	public void surfaceCreated()
	{
		mFactory.ReloadResources();
		super.surfaceCreated();
	}

	public void surfaceChanged(int width, int height) 
	{
		mDevice.Resize(width, height);
						
		// get orientation
		// expects screens to be higher then wider
		if (height > width)
		{
			mOrientation = 1;
		}
		else
		{
			mOrientation = 2;
		}
   
        // store the canvas width and height
        mCanvasWidth = width;
        mCanvasHeight = height;
                
        float aspectRatio = mCanvasWidth / mCanvasHeight;
        mCamera.SetOrthographic(aspectRatio, 1.0f, 0.1f, 100);
        
        notifyEvent(ILayer.Event.SurfaceChanged);
	}
}