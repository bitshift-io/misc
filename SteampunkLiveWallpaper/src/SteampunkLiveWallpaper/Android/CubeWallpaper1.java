/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package SteampunkLiveWallpaper.Android;

import SteampunkLiveWallpaper.Android.R;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.SystemClock;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.ads.AdSenseSpec;
import com.google.ads.GoogleAdView;
import com.google.ads.AdSenseSpec.AdType;

/*
 * This animated wallpaper draws a rotating wireframe cube.
 */
public class CubeWallpaper1 extends WallpaperService {

    private final Handler mHandler = new Handler();
    

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public Engine onCreateEngine() {
        return new CubeEngine(this);
    }

    class CubeEngine extends Engine {

        private final Paint mPaint = new Paint();
        private float mOffset;
        private float mTouchX = -1;
        private float mTouchY = -1;
        private long mStartTime;
        private float mCenterX;
        private float mCenterY;
        
        // touch events cause the background to shift around
        float mOffsetX = 0.f;
        float mOffsetY = 0.f;
        
        float mRotationDegrees = 0.f;
        
        Bitmap mBackground;

        Bitmap mGear01;
        Bitmap mGear02;
        Bitmap mForeground;
        
        CubeWallpaper1 mApplication;
        
        RelativeLayout mRelativeView;
        AbsoluteLayout mLayout;
        GoogleAdView mAdView;
        WebView mWebView;
        
        Button mTestButton;

        private final Runnable mDrawCube = new Runnable() {
            public void run() {
                drawFrame();
            }
        };
        private boolean mVisible;

        CubeEngine(CubeWallpaper1 application) {
        	//application.setContentView(R.layout.main);
        	//TextView temp = (TextView)this.findViewById(R.id.advert);
        	
        	// set up ads
        	// http://www.google.com/codesearch/p?hl=en#XXteUaxV20E/trunk/ZombieRun/src/net/peterd/zombierun/activity/Util.java&q=GoogleAdView%20package:http://zombie-run%5C.googlecode%5C.com&sa=N&cd=1&ct=rc
        	// http://code.google.com/intl/zh-CN/mobile/ads/docs/android/
        	AdSenseSpec adSenseSpec =
        		new AdSenseSpec("pub-9246408363565140")
            .setCompanyName("Black Carbon")
            .setAppName("SteamPunk Live Wallpaper")
            //.setKeywords(activity.getString(R.string.adsense_keywords))
            .setChannel("2510425528")
            .setAdType(AdType.TEXT_IMAGE)
            //.setWebEquivalentUrl(activity.getString(R.string.about_url))
            .setAdTestEnabled(true)
            .setExpandDirection(AdSenseSpec.ExpandDirection.TOP)
            .setAdType(AdSenseSpec.AdType.TEXT_IMAGE)
            .setAdFormat(AdSenseSpec.AdFormat.FORMAT_320x50);
        	
        	// Set up GoogleAdView and fetch ad
        	mAdView = new GoogleAdView(application.getApplicationContext());
        	/*
        	
        	new Handler().post(new Runnable() {
                public void run() {
                  AdSenseSpec adSenseSpec =
                      new AdSenseSpec("pub-9246408363565140")
                          .setCompanyName("Black Carbon")
                          .setAppName("SteamPunk Live Wallpaper")
                          //.setKeywords(activity.getString(R.string.adsense_keywords))
                          .setChannel("2510425528")
                          .setAdType(AdType.TEXT_IMAGE)
                          //.setWebEquivalentUrl(activity.getString(R.string.about_url))
                          .setAdTestEnabled(true)
                          .setExpandDirection(AdSenseSpec.ExpandDirection.TOP)
                          .setAdType(AdSenseSpec.AdType.TEXT_IMAGE)
                          .setAdFormat(AdSenseSpec.AdFormat.FORMAT_320x50);

                  // Fetch Google ad.
                  // PLEASE DO NOT CLICK ON THE AD UNLESS YOU ARE IN TEST MODE.
                  // OTHERWISE, YOUR ACCOUNT MAY BE DISABLED.
                  mAdView.showAds(adSenseSpec);
                }
              });
        	
        	*/
        
        	
        	mLayout = new AbsoluteLayout(application.getApplicationContext());
        	//mLayout.
        	//AbsoluteLayout.LayoutParams p;arams = new AbsoluteLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        	//LayoutParams params = absoluteLayout.generateDefaultLayoutParams();
        	LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            
            mAdView.setLayoutParams(params);
            mAdView.showAds(adSenseSpec);
            mAdView.setEnabled(true);
            mAdView.setVisibility(View.VISIBLE);
            mAdView.bringToFront(); 
            mAdView.requestFocus(); 
            mAdView.invalidate();
            
            mTestButton = new Button(application.getApplicationContext());
            mTestButton.setText("test");
            mTestButton.setLayoutParams(params);
            
        	
            // Create a Paint to draw the lines for our cube
            final Paint paint = mPaint;
            paint.setColor(0xffffffff);
            paint.setAntiAlias(true);
            paint.setStrokeWidth(2);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStyle(Paint.Style.STROKE);

            mStartTime = SystemClock.elapsedRealtime();
            mApplication = application;
            
            mGear01 = BitmapFactory.decodeResource(application.getResources(), R.drawable.gear01);
            mGear02 = BitmapFactory.decodeResource(application.getResources(), R.drawable.gear02);
            mBackground = BitmapFactory.decodeResource(application.getResources(), R.drawable.background);
            mForeground = BitmapFactory.decodeResource(application.getResources(), R.drawable.foreground);
            
            
            mRelativeView = new RelativeLayout(application.getApplicationContext());
            	
            // http://www.anddev.org/sell_your_app_or_run_ads_in_it-t4376.html
            // http://developer.android.com/reference/android/webkit/WebView.html
            mWebView = new WebView(application.getApplicationContext());
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.setLayoutParams(params);
            mWebView.loadUrl("file:///android_asset/adsense.html");
            //mWebView.loadData("<html><body bgcolor=red>Hi dude<br><br>Hi again</body></html>", "text/html", "UTF-8");
            
            
            mRelativeView.addView(mWebView);
            //mRelativeView.addView(mTestButton);
        }

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);

            // By default we don't get touch events, so enable them.
            setTouchEventsEnabled(true);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mHandler.removeCallbacks(mDrawCube);
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            mVisible = visible;
            if (visible) {
                drawFrame();
            } else {
                mHandler.removeCallbacks(mDrawCube);
            }
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            // store the center of the surface, so we can draw the cube in the right spot
            mCenterX = width/2.0f;
            mCenterY = height/2.0f;
            drawFrame();
        }

        @Override
        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);
        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            mVisible = false;
            mHandler.removeCallbacks(mDrawCube);
        }

        @Override
        public void onOffsetsChanged(float xOffset, float yOffset,
                float xStep, float yStep, int xPixels, int yPixels) {
            mOffset = xOffset;
            drawFrame();
        }

        /*
         * Store the position of the touch event so we can use it for drawing later
         */
        @Override
        public void onTouchEvent(MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
            	
            	// offset based on mouse movement from last frame
            	if (mTouchX != -1)
            	{
	            	mOffsetX += (event.getX() - mTouchX) * 0.1f;
	            	mOffsetY += (event.getY() - mTouchY) * 0.1f;
            	}
            	
                mTouchX = event.getX();
                mTouchY = event.getY();
            } else {
                mTouchX = -1;
                mTouchY = -1;
            }
            super.onTouchEvent(event);
        }

        /*
         * Draw one frame of the animation. This method gets called repeatedly
         * by posting a delayed Runnable. You can do any drawing you want in
         * here. This example draws a wireframe cube.
         */
        void drawFrame() {
            final SurfaceHolder holder = getSurfaceHolder();

            Canvas c = null;
            try {
                c = holder.lockCanvas();
                if (c != null) {
                    // draw something
                	drawWallpaper(c);
                    drawTouchPoint(c);
                }
            } finally {
                if (c != null) holder.unlockCanvasAndPost(c);
            }

            // Reschedule the next redraw
            mHandler.removeCallbacks(mDrawCube);
            if (mVisible) {
                mHandler.postDelayed(mDrawCube, 1000 / 25);
            }
        }
        
        void drawWallpaper(Canvas c)
        {            
        	//c.drawColor(0xffff0000);
        	
        	float parallax = -200.f;
        	

    		
        	if (mBackground != null)
        	{
            	float width = c.getWidth();
        		float height = c.getHeight();
        		

        		float backgroundWidth =  mBackground.getWidth();
        		float backgroundHeight =  mBackground.getHeight();
        		
        		//float xScale = width / backgroundWidth;
        		float yScale = height / backgroundHeight;
        		
        		// mOffset is a percentage, subtract width as its 1 screen size reduced
        		float translation = -((backgroundWidth * yScale) - width) * mOffset;
        		
        		Matrix matrix = new Matrix();
        		matrix.setScale(yScale, yScale);
        		matrix.postTranslate(translation, 0.f);
        		c.drawBitmap(mBackground, matrix, mPaint);
        	}
        	
        	mRotationDegrees += 0.5f;
        	
            if (mGear01 != null)
            {
            	
            	float halfWidth =  mGear01.getWidth() * 0.5f;
            	float halfHeight = mGear01.getHeight() * 0.5f;
	            Matrix matrix = new Matrix();
	            matrix.setRotate(-mRotationDegrees, halfWidth, halfHeight); // rotate around centre of bmp
	            //matrix.setTranslate(mCenterX, mCenterY);
	            matrix.postTranslate(mCenterX - halfWidth, mCenterY - halfHeight);
	            matrix.postTranslate(mOffset * parallax, 0.f);
	            c.drawBitmap(mGear01, matrix, mPaint);
            }
            
            if (mGear02 != null)
            {
            	float halfWidth =  mGear02.getWidth() * 0.5f;
            	float halfHeight = mGear02.getHeight() * 0.5f;
	            Matrix matrix = new Matrix();
	            matrix.setRotate(mRotationDegrees, mGear02.getWidth() * 0.5f, mGear02.getHeight() * 0.5f); // rotate around centre of bmp
	            //matrix.setTranslate(mCenterX, mCenterY);
	            matrix.postTranslate(c.getWidth() - halfWidth, c.getHeight() - halfHeight);
	            matrix.postTranslate(mOffset * parallax, 0.f);
	            c.drawBitmap(mGear02, matrix, mPaint);
            }
            
            if (mForeground != null)
        	{
        		Matrix matrix = new Matrix();
        		matrix.setScale(0.6f, 0.6f);
        		c.drawBitmap(mForeground, matrix, mPaint);
        	}
            
            int minWidth = getDesiredMinimumWidth();
            int minHeight = getDesiredMinimumHeight();
            
            {
	            mTestButton.setPadding(10, 10, 10, 10); // pads the view
	            mTestButton.measure(minWidth, minHeight); // let view calculate how big it should be such that it fits inside this view
	
	            int measuredWidth = mTestButton.getMeasuredWidth();
	            int measuredHeight = mTestButton.getMeasuredHeight();
	            mTestButton.layout(300, 300, 300 + measuredWidth, 300 + measuredHeight);
	            //mTestButton.draw(c);
            }
            
            
            int pageHeight = 75;
            boolean adAtTop = true;
            
            {
            	int width = c.getWidth();
            	
            	//mWebView.setPadding(10, 10, 10, 10); // pads the view
            	mWebView.measure(width, minHeight); // let view calculate how big it should be such that it fits inside this view
	
	            int measuredWidth = width; //mWebView.getMeasuredWidth();
	            int measuredHeight = pageHeight; //mWebView.getMeasuredHeight();
	            int top = adAtTop ? 40 : (minHeight - measuredHeight) - 100;
	            mWebView.layout(0, top, measuredWidth, top + measuredHeight);
	            //mWebView.draw(c);
            }
            
            mRelativeView.draw(c);

        }

        /*
        void drawCube(Canvas c) {
            c.save();
            c.translate(mCenterX, mCenterY);
            c.drawColor(0xff000000);
            drawLine(c, -400, -400, -400,  400, -400, -400);
            drawLine(c,  400, -400, -400,  400,  400, -400);
            drawLine(c,  400,  400, -400, -400,  400, -400);
            drawLine(c, -400,  400, -400, -400, -400, -400);

            drawLine(c, -400, -400,  400,  400, -400,  400);
            drawLine(c,  400, -400,  400,  400,  400,  400);
            drawLine(c,  400,  400,  400, -400,  400,  400);
            drawLine(c, -400,  400,  400, -400, -400,  400);

            drawLine(c, -400, -400,  400, -400, -400, -400);
            drawLine(c,  400, -400,  400,  400, -400, -400);
            drawLine(c,  400,  400,  400,  400,  400, -400);
            drawLine(c, -400,  400,  400, -400,  400, -400);
            c.restore();
            
        }

        void drawLine(Canvas c, int x1, int y1, int z1, int x2, int y2, int z2) {
            long now = SystemClock.elapsedRealtime();
            float xrot = ((float)(now - mStartTime)) / 1000;
            float yrot = (0.5f - mOffset) * 2.0f;
            float zrot = 0;

            // 3D transformations

            // rotation around X-axis
            float newy1 = (float)(Math.sin(xrot) * z1 + Math.cos(xrot) * y1);
            float newy2 = (float)(Math.sin(xrot) * z2 + Math.cos(xrot) * y2);
            float newz1 = (float)(Math.cos(xrot) * z1 - Math.sin(xrot) * y1);
            float newz2 = (float)(Math.cos(xrot) * z2 - Math.sin(xrot) * y2);

            // rotation around Y-axis
            float newx1 = (float)(Math.sin(yrot) * newz1 + Math.cos(yrot) * x1);
            float newx2 = (float)(Math.sin(yrot) * newz2 + Math.cos(yrot) * x2);
            newz1 = (float)(Math.cos(yrot) * newz1 - Math.sin(yrot) * x1);
            newz2 = (float)(Math.cos(yrot) * newz2 - Math.sin(yrot) * x2);

            // 3D-to-2D projection
            float startX = newx1 / (4 - newz1 / 400);
            float startY = newy1 / (4 - newz1 / 400);
            float stopX =  newx2 / (4 - newz2 / 400);
            float stopY =  newy2 / (4 - newz2 / 400);

            c.drawLine(startX, startY, stopX, stopY, mPaint);
        }*/

        /*
         * Draw a circle around the current touch point, if any.
         */
        void drawTouchPoint(Canvas c) {
            if (mTouchX >=0 && mTouchY >= 0) {
                c.drawCircle(mTouchX, mTouchY, 80, mPaint);
            }
        }

    }
}
