#include "StdAfx.h"
#include "MathParser.h"

bool MathParser::HandlesType(const char* name) const
{
	if (strcmpi(name, "Vector4") == 0)
		return true;

	if (strcmpi(name, "Matrix4") == 0)
		return true;

	return false;
}

bool MathParser::Write(const ClassMetadataInfo& info, void* data, string& out) const
{
	return true;
}

bool MathParser::Read(const ClassMetadataInfo& info, void* data, const string& value, MetadataScript* metaScript) const
{/*
	if (strcmpi(info.type, "Matrix4") == 0)
	{
		vector<string> values = ScriptFile::Tokenize(value, " ", true);
		vector<string>::const_iterator it;

		int nothing = 0;
	}
	else*/
	{
		// initilize with defaults
		Vector4* vecArray = static_cast<Vector4*>((void*)((char*)data + info.offset));
		*vecArray = Vector4(0.f, 0.f, 0.f, 1.f);

		vector<string> values = ScriptFile::Tokenize(value, ",", true);
		vector<string>::const_iterator it;
		int i = 0;
		for (it = values.begin(); it != values.end(); ++it, ++i)
		{
			float value = 0.f;
			sscanf(it->c_str(), "%f", &value);

			float* floatArray = static_cast<float*>((void*)((char*)data + info.offset));
			floatArray[i] = value;
		}
	}
	return true;
}

bool MathParser::Clone(const ClassMetadataInfo& info, void* from, void* to) const
{
	int size = 0;

	if (strcmpi(info.name, "Vector4") == 0)
		size = sizeof(Vector4);

	if (strcmpi(info.name, "Matrix4") == 0)
		size = sizeof(Matrix4);

	memcpy((char*)to + info.offset, (char*)from + info.offset, size);
	return true;
}

