#ifndef _MATHPARSER_H_
#define _MATHPARSER_H_

#include "Metadata/MetadataParser.h"

class MathParser : public MetadataParser
{
public:

	virtual bool HandlesType(const char* name) const;
	virtual bool Write(const ClassMetadataInfo& info, void* data, string& out) const;
	virtual bool Read(const ClassMetadataInfo& info, void* data, const string& value, MetadataScript* metaScript) const;
	virtual bool ReadInstance(const ClassMetadataInfo& info, void* data, void* newInstance, MetadataScript* metaScript) const { return false; }

	virtual bool Clone(const ClassMetadataInfo& info, void* from, void* to) const;
};
/*
template <class T>
Packet& operator &(Packet& packet, Matrix4& data);

*/

template <class T>
Packet& operator &(Packet& packet, Matrix4& data)
{
	// compress for network transfer
	switch (packet.GetStreamOperation())
	{
	case SO_Read:
		{
			Quaternion rot;
			rot.CreateFromMatrix(data);
			packet & rot;

			Vector4 translation = data.GetTranslation();
			packet & translation;
		}
		break;

	case SO_Write:
		{
			Quaternion rot;
			packet & rot;
			data = rot.GetRotationMatrix();

			Vector4 translation;
			packet & translation;
			data.SetTranslation(translation);
		}
		break;
	}

    return packet;
}

#endif
