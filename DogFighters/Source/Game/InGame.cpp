#include "StdAfx.h"
#include "InGame.h"

CLASS_METADATA_BEGIN(InGame)
	CLASS_VARIABLE(InGame, string, mNextStateName)
	CLASS_VARIABLE(InGame, string, mStateName)
CLASS_METADATA_END(InGame)

bool InGame::Init(Game* game)
{
	mUpdateTime = 0;
	mRenderTime = 0;
	mPhysicsTime = 0;
	mNetworkTime = 0;

	mGame = game;	
	mFreeCamera = false;
	mPhysicsDebug = false;
	mEntityDebug = false;
	mPause = false;

	mCentreCursor = true;
	//mDebug = true;
	return true;
}

void InGame::Deinit()
{

}

void InGame::StateEnter()
{
	// hide cursor
	if (gGame.mWindow->IsCursorVisible())
		gGame.mWindow->SetCursorVisibility(false);
}

void InGame::StateExit()
{
	// make cursor visible
	if (!gGame.mWindow->IsCursorVisible())
		gGame.mWindow->SetCursorVisibility(true);

	SetPauseMsg pauseMsg;
	pauseMsg.mPause = true;
	gMessageMgr.BroadcastMessage(&pauseMsg);
}

bool InGame::UpdateGame()
{
	unsigned int updateTime = Time::GetMilliseconds();

	gGame.mMusicPlayer->Update();
	gGame.mInput->Update();

	if (gGame.mInput->WasPressed(KEY_Esc) || gGame.mWindow->HandleMessages()
		|| (gGame.mInput->GetJoystickCount() && gGame.mInput->WasPressed(JOYSTICK0_Start))
#ifdef RELEASE
		|| !gGame.mWindow->HasFocus()
#endif
		)
	{
		gGame.SetState(gGame.GetStateByName(mNextStateName));
		return true;
	}

#ifdef DEBUG
	if (gGame.mInput->WasPressed(KEY_m))
		mGame->GetGameModeMgr()->GetGameMode()->AddAI(1);

	if (gGame.mInput->GetStateBool(KEY_f))
	{
		mFreeCamera = !mFreeCamera;
		/*
		if (mFreeCamera)
			gGame.GetEnvironment()->mCameraMgr->HandleMsg(&SetCameraTypeMsg(CT_Free));
		else
			gGame.GetEnvironment()->mCameraMgr->HandleMsg(&SetCameraTypeMsg(CT_FirstPerson));*/
	}
/*
	if (gGame.mInput->WasPressed(KEY_p))
	{
		mPause = !mPause;

		if (mPause)
			gGame.GetEnvironment()->mCameraMgr->HandleMsg(&SetCameraTypeMsg(CT_Free));
		else
			gGame.GetEnvironment()->mCameraMgr->HandleMsg(&SetCameraTypeMsg(CT_FirstPerson));
	}*/

	if (gGame.mInput->WasPressed(KEY_o))
		mEntityDebug = !mEntityDebug;

	if (gGame.mInput->WasPressed(KEY_i))
		mPhysicsDebug = !mPhysicsDebug;
#endif

	// update ewnvironment
	{
		// receive any network stuff needed
#ifdef NETWORK
		{
			unsigned int time = time.GetMilliseconds();
			gGame.GetEnvironment()->mNetwork->UpdateReceive();
			mNetworkTime = time.GetMilliseconds() - time;
		}
#endif

		gGame.GetEnvironment()->mCameraMgr->Update();
		gGame.GetEnvironment()->mRenderMgr->Update();
		gGame.GetEnvironment()->mEntityMgr->Update();
		gGame.GetGameModeMgr()->Update();

		// update network again, this time send any packets queued
#ifdef NETWORK
		{
			unsigned int time = time.GetMilliseconds();
			gGame.GetEnvironment()->mNetwork->UpdateSend();
			mNetworkTime += time.GetMilliseconds() - time;
		}
#endif
	}

	// update physics
#ifdef PHYSICS
	{
		unsigned int time = time.GetMilliseconds();
		gGame.mPhysicsFactory->Update();
		gGame.GetEnvironment()->mPhysicsScene->Update(gGame.mGameUpdate.GetDeltaTime());
		mPhysicsTime = time.GetMilliseconds() - time;
	}
#endif

	mUpdateTime = Time::GetMilliseconds() - updateTime;
	return true;
}

bool InGame::UpdateNetwork()
{
	return true;
}

bool InGame::UpdatePhysics()
{
	return true;
}

void InGame::Render()
{
	unsigned int time = Time::GetMilliseconds();

	// this needs to be done each frame!
	if (gGame.mWindow->HasFocus())
	{
		if (mCentreCursor)
			gGame.mWindow->CenterCursor();

		if (gGame.mWindow->IsCursorVisible())
			gGame.mWindow->SetCursorVisibility(false);
	}
	else
	{
		if (!gGame.mWindow->IsCursorVisible())
			gGame.mWindow->SetCursorVisibility(true);
	}

	gGame.mDevice->Begin();	
	gGame.GetEnvironment()->mRenderMgr->Render(gGame.mDevice);
	gGame.mMusicPlayer->Render();

	BEGINDEBUGEVENT(gGame.mDevice, "debug render", Vector4(0.f, 1.f, 0.f, 1.f));

	if (mEntityDebug)
	{
		gGame.GetEnvironment()->mCameraMgr->GetCamera()->Bind(gGame.mDevice);
		DebugDrawMsg debugDraw;
		gGame.GetEnvironment()->mEntityMgr->HandleMsg(&debugDraw);
	}

#ifdef PHYSICS
	if (mPhysicsDebug)
	{
		gGame.GetEnvironment()->mCameraMgr->GetCamera()->Bind(gGame.mDevice);
		gGame.GetEnvironment()->mPhysicsScene->Draw(gGame.mDevice); // debugging
	}
#endif

	if (gGame.mDisplayStats)
	{
		// render profiling info
		gGame.mDebugFont->RenderText(-0.95f, 0.2f, 1.f, "Render (ms)   %i\nPhysics (ms)   %i\nNetwork (ms)   %i\nUpdate (ms)    %i", mRenderTime, mPhysicsTime, mNetworkTime, mUpdateTime);

		// render fps
		gGame.mDebugFont->RenderText(-0.95f, 0.f, 1.f, "FPS   %i\nDrawCallCount   %i\nPolyCount   %i", gGame.mDevice->GetFPS(), gGame.mDevice->GetDrawCallCount(), gGame.mDevice->GetPolyCount());

		// memory stats
		gGame.mDebugFont->RenderText(-0.95f, 0.4f, 1.f, "Memory used (MB)   %f\nNumber of allocations   %i", (float(MemTracker::GetAllocationSize()) / 1024.f) / 1024.0f, MemTracker::GetAllocationCount());

#ifdef NETWORK
		// render network bandwidth
		gGame.mDebugFont->RenderText(-0.95f, 0.6f, 1.f, "In (bytes)   %i\nOut (bytes)   %i\nChoke    %.2f\nPackets Sent    %.2f", 
			gGame.GetEnvironment()->mNetwork->GetInBandwidth(), gGame.GetEnvironment()->mNetwork->GetOutBandwidth(), gGame.GetEnvironment()->mNetwork->GetChoke(), gGame.GetEnvironment()->mNetwork->GetPacketsSent());

		// ping times
		RDPStream* stream = gGame.GetEnvironment()->mNetwork->GetStream();
		RDPStream::Iterator peerIt;

		int idx = 0;
		for (peerIt = stream->Begin(); peerIt != stream->End(); ++peerIt)
		{
			gGame.mDebugFont->RenderText(-0.95f, 0.8f + idx * 0.05f, 1.f, "Ping to client %i - %i", idx, (*peerIt)->GetPing());
			++idx;
		}
#endif
	}

	ENDDEBUGEVENT(gGame.mDevice);

	gGame.mDevice->End();
	mRenderTime = Time::GetMilliseconds() - time;
}

MESSAGE_BEGIN(InGame)
	MESSAGE_PASS(SetCentreCursor)
MESSAGE_END()

bool InGame::SetCentreCursor(SetCentreCursorMsg* msg)
{
	SetCentreCursor(msg->mCentre);
	return true;
}