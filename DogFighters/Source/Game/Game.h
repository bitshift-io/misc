#ifndef _GAME_H_
#define _GAME_H_

#include "StdAfx.h"
#include "Util/StateMachine.h"
#include "Util/CmdLine.h"
#include "Util/Precache.h"
#include "GameState.h"
#include "Thread/Thread.h"


using namespace std;

class Environment;
class StateMsg;
class SetGameModeMsg;
class ClientConnectMsg;
class GameModeMgr;
class MasterServer;
class NetworkFactory;
class MusicPlayer;

#define gGame Game::GetInstance()


//
// There can only be one game running per application
//
class Game :
    public MessageReceiver,
    public StateMachine<GameState>,
    public Singleton<Game>
{
public:

	USES_METADATA
	typedef vector<GameState*>::iterator Iterator;

	bool					Init(const CmdLine& cmdLine);
	void					Deinit();
	bool					UpdateGame();
	bool					UpdateNetwork();
	bool					UpdatePhysics();
	void					Render();
	bool					Run();
	void					Update();

	GameState*				GetStateByName(const string& name);

	Iterator				Begin()										{ return mState.begin(); }
	Iterator				End()										{ return mState.end(); }

	virtual bool			HandleMsg(Message* msg);
	virtual const char*		GetName()									{ return "game"; }

	static int				Main(const char* cmdLine);

	Environment*			CreateEnvironment();
	void					ReleaseEnvironemnt(Environment** env);

	void					PushEnvironment(Environment* env);
	void					PopEnvironment();

	Environment*			GetEnvironment()							{ return mEnvStack.top(); }
	GameModeMgr*			GetGameModeMgr()							{ return mGameModeMgr; }

	void					SetState(GameState* state);
	bool					Exit();
	bool					SetState(SetStateMsg* msg);
	bool					SetGameMode(SetGameModeMsg* msg);

	void					ReinitWindowAndDevice(const WindowResolution& res, bool fullscreen, int aa);
	void					SetAudioOptions(int volume, int musicVolume);

#ifdef NETWORK
	bool					ClientConnect(ClientConnectMsg* msg);
#endif

	void					SaveProfile();

	// command line
	CmdLine					mCmdLine;

	bool					mDisplayStats;

	Precache				mPrecache;

	// render
	RenderFactory*			mRenderFactory;
	Window*					mWindow;
	Device*					mDevice;
	Input*					mInput;

#ifdef PHYSICS
	// physics
	PhysicsFactory*			mPhysicsFactory;
#endif

	// sound
	SoundFactory*			mSoundFactory;

#ifdef NETWORK
	// network
	NetworkFactory*			mNetworkFactory;

	MasterServer*			mMasterServer;
#endif

	// frame updates
	FrameUpdate				mGameUpdate;
	FrameUpdate				mNetworkUpdate;
	FrameUpdate				mPhysicsUpdate;

	Font*					mDebugFont;
	Playback				mPlayback;

	// settings config file
	MetadataScript*			mProfile;

	MusicPlayer*			mMusicPlayer;
	float					mSoundVolume;
	int						mPerformance;

protected:

	bool					InitWindowAndDevice();

	GameModeMgr*			mGameModeMgr;
	stack<Environment*>		mEnvStack;
	bool					mExit;
	string					mMasterServerName;
	string					mGameName;

	Thread					mSoundThread;
	Task					mSoundTask;
};

#endif
