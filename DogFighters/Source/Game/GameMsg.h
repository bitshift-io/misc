#ifndef _GAMEMSG_H_
#define _GAMEMSG_H_

#include "Core/Message.h"

class SetStateMsg : public Message
{
public:

	TYPE(21)

	virtual bool	ConvertFromString(vector<string>& params, TranslationTable* translationTable = 0);

	string			mStateName;
};

class ExitMsg : public Message
{
public:

	TYPE(20)
};

//
// sent by game when window resolution changed
//
class ReloadResourceMsg : public Message
{
public:

	TYPE(22)

};

class ReloadResourcePostMsg : public Message
{
public:

	TYPE(27)

};

class SetPerformanceMsg : public Message
{
public:

	TYPE(26)

};

class SetCentreCursorMsg : public Message
{
public:

	TYPE(23)

	virtual bool	ConvertFromString(vector<string>& params, TranslationTable* translationTable = 0);

	bool mCentre;
};

class SetPauseMsg : public Message
{
public:

	TYPE(24)

	bool mPause;
};

class VolumeChangeMsg : public Message
{
public:

	TYPE(25)

};


#endif
