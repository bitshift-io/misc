#include "StdAfx.h"
#include "GameMsg.h"

RegisterMessageWithMessageMgr(SetState);
RegisterMessageWithMessageMgr(Exit);
RegisterMessageWithMessageMgr(ReloadResource);
RegisterMessageWithMessageMgr(SetCentreCursor);
RegisterMessageWithMessageMgr(SetPause);
RegisterMessageWithMessageMgr(VolumeChange);

bool SetStateMsg::ConvertFromString(vector<string>& params, TranslationTable* translationTable)
{
	if (params.size() <= 0)
		return false;

	mStateName = params[0];
	return true;
}

bool SetCentreCursorMsg::ConvertFromString(vector<string>& params, TranslationTable* translationTable)
{
	if (params.size() <= 0)
		return false;

	mCentre = strcmpi(params[0].c_str(), "true") == 0 ? true : false;
	return true;
}