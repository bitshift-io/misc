#ifndef _MENUMSG_H_
#define _MENUMSG_H_

#include "Core/Message.h"

class SetVisibleMenuMsg : public Message
{
public:

	TYPE(100)

	virtual bool	ConvertFromString(vector<string>& params, TranslationTable* translationTable = 0);

	string			mWindowName;
};

#endif
