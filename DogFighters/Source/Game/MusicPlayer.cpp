#include "StdAfx.h"
#include "MusicPlayer.h"

CLASS_METADATA_BEGIN(MusicPlayer)
/*
	CLASS_VARIABLE(MusicPlayer, string, mFlashFile)
	*/
	CLASS_VARIABLE(MusicPlayer, string, mMusicFiles)
	CLASS_VARIABLE(MusicPlayer, bool, mFirstIsThemeMusic)
CLASS_METADATA_END(MusicPlayer)

//#define NOMUSIC

bool MusicPlayer::Init()
{
#ifndef NOMUSIC
	mMusic = 0;
	mCurrentTrack = -1;
	mMusicFile = ScriptFile::Tokenize(mMusicFiles, ",", true);	
/*
	mFlash.Init(gGame.mRenderFactory, gGame.mDevice, gGame.mWindow);
	mFlash.SetCallback(this);
	mFlash.Load(mFlashFile.c_str());
	*/
#endif
	return true;
}

void MusicPlayer::Deinit()
{
#ifndef NOMUSIC
	gGame.mSoundFactory->ReleaseSound(&mMusic);
	/*
	mFlash.Deinit();
	*/
#endif
}

void MusicPlayer::Play()
{
#ifndef NOMUSIC
	int track = 0;
	if (mMusic || !mFirstIsThemeMusic)
		track = (int)Math::Rand(0, mMusicFile.size());

	// make sure we dont play the same track 2 times in a row
	if (mMusicFile.size() > 1 && mCurrentTrack == track)
	{
		track = (track + 1) % mMusicFile.size();
	}

	mCurrentTrack = track;

	if (mMusic)
		gGame.mSoundFactory->ReleaseSound(&mMusic);

	char text[1024];
	sprintf(text, "Now playing %s\nby Arik Burns", mMusicFile[track].c_str());

	if (gGame.GetGameModeMgr()->GetGameMode())
	{
		HUD* hud = gGame.GetGameModeMgr()->GetGameMode()->GetHUD();
		if (hud)
			hud->Message("music", text);
	}
/*
	// dodgey hack to get this code working
	mFlash.CallMethod("PlayTrack", "");
	mFlash.CallMethod("PlayTrack", "%s", mMusicFile[track].c_str());
*/
	mMusic = gGame.mSoundFactory->CreateSound(mMusicFile[track], true);
	if (mMusic)
	{
		mMusic->Play();
		mMusic->SetGain(mVolume);
	}
#endif
}

bool MusicPlayer::Update()
{
#ifndef NOMUSIC
	/*
	mFlash.Update(gGame.mGameUpdate.GetDeltaTime(), gGame.mInput);
	//mFlash.CallMethod("Update");
*/
	// pressing m forces a track change
	if (mMusic && (!mMusic->IsPlaying() || gGame.mInput->WasPressed(KEY_m)))
		Play();
#endif
	return true;
}

void MusicPlayer::Render()
{
#ifndef NOMUSIC
	/*
	if (mRender)
		mFlash.Render();
		*/
#endif
}

void MusicPlayer::FsCommand(Flash* flash, const char* command, const char* arg)
{/*
	if (strcmpi(command, "enable_render") == 0)
		mRender = true;
	else if (strcmpi(command, "disable_render") == 0)
		mRender = false;*/
}

bool MusicPlayer::ReloadResource()
{
#ifndef NOMUSIC
	/*
	mFlash.ReloadResource();
	*/
#endif
	return true;
}

bool MusicPlayer::SetVolume(float volume)
{
	//if (mVolume == volume)
	//	return false;

	mVolume = volume;
	if (mMusic)
		mMusic->SetGain(mVolume);

	return true;
}