#ifndef _INGAME_H_
#define _INGAME_H_

#include "GameState.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"

class InGame : public GameState
{
public:

	USES_METADATA

	virtual bool			Init(Game* game);
	virtual void			Deinit();

	virtual void			StateEnter();
	virtual void			StateExit();

	virtual bool			UpdateGame();
	virtual bool			UpdateNetwork();
	virtual bool			UpdatePhysics();
	virtual void			Render();

	virtual bool			HandleMsg(Message* msg);
	virtual bool			SetCentreCursor(SetCentreCursorMsg* msg);
	
	virtual const char*		GetName()						{ return mStateName.c_str(); }

	void					SetCentreCursor(bool centre)	{ mCentreCursor = centre; }

	const char*				GetNextStateName()				{ return mNextStateName.c_str(); }

protected:

	bool		mCentreCursor;
	Game*		mGame;

	string		mStateName;
	string		mNextStateName;

	bool		mExit;
	string		mOnInit;
	string		mLocalActor;
	string		mNetworkActor;
	string		mAIActor;
	bool		mPause;
	bool		mPhysicsDebug;
	bool		mEntityDebug;
	bool		mFreeCamera;

	// profiling
	unsigned int	mPhysicsTime;
	unsigned int	mRenderTime;
	unsigned int	mUpdateTime;
	unsigned int	mNetworkTime;
};

#endif
