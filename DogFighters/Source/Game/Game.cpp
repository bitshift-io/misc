#include "StdAfx.h"
#include "Game.h"

#include "Render/Win32/Win32Window.h"

#ifdef _DEBUG
#define SINGLEFRAMESINGLEUPDATE
#endif

// set up some parsers
USE_BASIC_PARSER
StringParser	strParser;
MathParser		mathParser;

static const char *sBuild = __DATE__ " " __TIME__;

#ifdef WIN32
int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	char cmdLine[256];
	WideCharToMultiByte(CP_ACP, 0, lpCmdLine, -1, cmdLine, 256, NULL, NULL);
	return Game::Main(cmdLine);
}
#else
int main(int argc, char* argv[])
{
    string cmdLine;
    for (int i = 0; i < argc; ++i)
    {
        cmdLine += string(argv[i]);
    }
    return Game::Main(cmdLine.c_str());
}

#endif

VectorParser<GameState*> gameStateParser("GameState*");

CLASS_METADATA_BEGIN(Game)
	CLASS_VARIABLE(Game, vector<GameState*>, mState)
	CLASS_VARIABLE(Game, GameModeMgr*, mGameModeMgr)
	CLASS_VARIABLE(Game, MusicPlayer*, mMusicPlayer)
	CLASS_VARIABLE(Game, string, mMasterServerName)
	CLASS_VARIABLE(Game, string, mGameName)
CLASS_METADATA_END(Game)

unsigned long SoundTask(void* data, Thread* thread)
{
	gGame.mSoundFactory->Update();
	Sleep(33); // run sound at 30fps
	return 0;
}

bool Game::Init(const CmdLine& cmdLine)
{
#ifndef _DEBUG
	MemTracker::mEnableLog = false;
#endif

	mMusicPlayer = 0;
	mGameModeMgr = 0;

	//MemTracker::SetBreakOnAllocNumber(110425);
	MemTracker::PushMemoryGroup("Game::Init");
	//MemTracker::SetBreakOnAllocNumber(61686);

	string game = "Data"; // default game is called "Data"
	cmdLine.GetToken("game", game);

	gFilesystem.SetDLLDirectory(gFilesystem.GetBaseDirectory() + "/Binary");
	gFilesystem.SetBaseDirectory(gFilesystem.GetBaseDirectory() + "/" + game);

	Log::Print("Build: %s\n", sBuild);
	Log::Print("Running game: %s\n", game.c_str());

	// build package mode!, build the package and quit
	vector<string> tokens;
	if (cmdLine.GetToken("package", tokens))
	{
		Log::Print("Building package: %s\n", tokens[0].c_str());
		Log::Print("Packaging files: %s\n", tokens[1].c_str());

		Package p;
		p.Open(tokens[0].c_str(), FAT_Write);
		p.AddDirectoryContents(gFilesystem.GetBaseDirectory(), true, tokens[1]);
		p.Close();
		return false;
	}


	// we should search for all packages in a directory instead of hard coding
	gFilesystem.RegisterPackage("game.pkg");
	gFilesystem.RegisterPackage("patch.pkg");
	gFilesystem.RegisterDirectoryContents(gFilesystem.GetBaseDirectory(), true);

	// load from file
	MetadataScript script;
	if (!script.Open("game.mdd", 0))
	{
		Log::Error("Failed to load metadata/game.mdd\n");
		return false;
	}

	const ScriptClass* gameClass = script.GetScriptClass("game");
	script.PopulateInstance(gameClass, this, gMetadataMgr.GetClassMetadata("game"));

#ifdef DEBUG
	mProfile = new MetadataScript("debugprofile.mdd");
#else
	mProfile = new MetadataScript("profile.mdd");
#endif

	mExit = false;
	RegisterReceiver();

	mCmdLine = cmdLine;

	// load additional command line parameters from file
	ScriptClass* cmdLineClass = mProfile->GetScriptClass("CommandLine");
	if (!cmdLineClass)
		cmdLineClass = mProfile->CreateScriptClass("CommandLine");

	ScriptVariable* variable = cmdLineClass->GetScriptVariable("commandLine");
	if (!variable)
	{
		ScriptVariable var("commandLine");
		var.AppendValue("(null)");
		cmdLineClass->AppendVariable(var);
	}
	else
	{
		string additionalCmdLineVars = variable->ValueBegin()->mString;
		if (strcmpi(additionalCmdLineVars.c_str(), "(null)") != 0)
			mCmdLine.GenerateTokenPairs(additionalCmdLineVars);
	}

	// render
	RenderFactoryParameter param;
	param.mCacheDir = "Cache";
	param.mUseCache = !cmdLine.GetToken("nocache");
	param.mDebugMode = cmdLine.GetToken("debugmode") ? true : param.mDebugMode;

	// attempt to create device
	mDevice = 0;

	Log::Print("[Game::Init] Attempting to Init Device\n");

#ifdef DX10

	if (!cmdLine.GetToken("opengl") && !cmdLine.GetToken("software"))
	{
		mRenderFactory = new DX10RenderFactory(param);
		if (InitWindowAndDevice())
		{
			Log::Print("[Game::Init] Using DirectX 10");
		}
		else
		{
			delete mRenderFactory;
			mRenderFactory = 0;
		}
	}

#endif

#ifdef OGL

	if (!mDevice && !cmdLine.GetToken("software"))
	{
		mRenderFactory = new OGLRenderFactory(param);
		if (InitWindowAndDevice())
		{
			Log::Print("[Game::Init] Using OpenGL");
		}
		else
		{
			delete mRenderFactory;
			mRenderFactory = 0;
		}
	}

#endif

#ifdef SOFTWARE

	if (!mDevice)
	{
		mRenderFactory = new SoftwareRenderFactory(param);
		if (InitWindowAndDevice())
		{
			Log::Print("[Game::Init] Using Software");

			SoftwareDevice* device = (SoftwareDevice*)mDevice;

			// software specific vars
			ScriptClass* renderOptions = gGame.mProfile->GetScriptClass("Render");
			ScriptVariable* interlaceVar = renderOptions->GetScriptVariable("softwareinterlace");
			bool interlace = interlaceVar ? (strcmpi(interlaceVar->ValueBegin()->mString.c_str(), "true") == 0 ? true : false) : false;
			device->SetInterlaced(interlace);

			// set up software effects

			// vertex colour + texture
			{
				SoftwareEffect* effect = (SoftwareEffect*)mRenderFactory->Create(Effect::Type);
				effect->SetName("null");
				VertexShaderColourTexture* vertexShader = new VertexShaderColourTexture;
				effect->AddTechnique(new SoftwareEffectTechnique("Render", (SoftwareVertexShader*)vertexShader, new PixelShaderColourTexture, new ShaderColourTextureParameterData));
				effect->Load(&ObjectLoadParams(mRenderFactory, device, "null"));
			}

			// vertex colour + colour multiplier
			{
				SoftwareEffect* effect = (SoftwareEffect*)mRenderFactory->Create(Effect::Type);
				effect->SetName("Colour");
				VertexShaderColour* vertexShader = new VertexShaderColour;
				effect->AddTechnique(new SoftwareEffectTechnique("Render", (SoftwareVertexShader*)vertexShader, new PixelShaderColour, new ShaderColourParameterData));
				effect->Load(&ObjectLoadParams(mRenderFactory, device, "Colour"));
			}

			// shader for rendering flash
			{
				SoftwareEffect* effect = (SoftwareEffect*)mRenderFactory->Create(Effect::Type);
				effect->SetName("Flash");
				VertexShaderFlash* vertexShader = new VertexShaderFlash;
				effect->AddTechnique(new SoftwareEffectTechnique("Colour", (SoftwareVertexShader*)vertexShader, new PixelShaderFlashColour, new ShaderFlashParameterData));
				effect->AddTechnique(new SoftwareEffectTechnique("Texture", (SoftwareVertexShader*)vertexShader, new PixelShaderFlashTexture, new ShaderFlashParameterData));
				effect->Load(&ObjectLoadParams(mRenderFactory, device, "Flash"));
			}
		}
		else
		{
			delete mRenderFactory;
			mRenderFactory = 0;
		}
	}

#endif

	if (!mDevice)
	{
		mWindow->MessageBox("An error has occured trying to obtain a render device", "Device Error", MBF_Ok);
		Log::Error("[Game::Init] Failed to Init Device");
		return false;
	}

	// sound - this may fail, but actually can work after a failure
	Log::Print("[Game::Init] Attempting to Init Sound");
	mSoundFactory = new SoundFactory();
	if (!mSoundFactory->Init(cmdLine.GetToken("nosound")))
	{
		mWindow->MessageBox("The sound system has failed, this is most likely caused by not having OpenAL installed, please run oalinst.exe", "Audio Error", MBF_Ok);
	}

	mSoundTask = Task(SoundTask);
	mSoundThread.Init();
	mSoundThread.SetPriority(TP_Low);
	mSoundThread.SetTask(&mSoundTask);
	mSoundThread.Resume();

	// load render options from config file
	ScriptClass* audioOptions = gGame.mProfile->GetScriptClass("Audio");
	if (!audioOptions)
		audioOptions = gGame.mProfile->CreateScriptClass("Audio");

	ScriptVariable* volumeVar = audioOptions->GetScriptVariable("volume");
	int volume = volumeVar ? atoi(volumeVar->ValueBegin()->mString.c_str()) : 80;
	//mSoundFactory->SetListenerGain(float(volume) / 100.f);
	mSoundVolume = float(volume) / 100.f;

	Log::Print("[Game::Init] Init Sound Complete\n");

	// get music playing ASAP!
	if (mMusicPlayer)
	{
		mMusicPlayer->Init();
		
		ScriptVariable* musicVolumeVar = audioOptions->GetScriptVariable("musicVolume");
		int musicVolume = musicVolumeVar ? atoi(musicVolumeVar->ValueBegin()->mString.c_str()) : 80;
		mMusicPlayer->SetVolume(float(musicVolume) / 100.f);
		mMusicPlayer->Play();
	}

	// show and clear to a dark colour so we dont hurt our precious eyes!
	mWindow->Show();
	mDevice->Begin();
	mDevice->End();

	Log::Print("[Game::Init] Attempting to Init Input");
	mInput = new Input();
#ifdef DEBUG
	if (!mInput->Init(mWindow, true))
#else
	if (!mInput->Init(mWindow))
#endif
	{
		Log::Error("Input Init Failed");
		mWindow->MessageBox("An error has occured with the input system, please check Data/log.html for more information", "Input Error", MBF_Ok);
	}

	Log::Print("[Game::Init] Init Input Complete");

#ifdef NETWORK
	// network
	mNetworkFactory = new NetworkFactory();
	mNetworkFactory->Init();

	// master server
	mMasterServer = new MasterServer;
	mMasterServer->Init(mNetworkFactory, mGameName.c_str(), mMasterServerName);
#endif

#ifdef PHYSICS
	// physics
	mPhysicsFactory = new PhysicsFactory;
	mPhysicsFactory->Init();
#endif

	mGameUpdate.SetUpdateRate(30);
	mNetworkUpdate.SetUpdateRate(1);
	mPhysicsUpdate.SetUpdateRate(1);

	mPlayback.Init();

	mPrecache.Init();

	mGameModeMgr->Init();

	// set up the default environment
	Environment* environment = CreateEnvironment();
	PushEnvironment(environment);

	// init states
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		(*it)->Init(this);
	}

#ifdef DEMO
	// push on a demo exit screen
	FlashMenu* pDemoMenu = new FlashMenu;
	pDemoMenu->mStateName = "Demo";
	pDemoMenu->mFlashFile = "Demo";
	pDemoMenu->mBackgroundFlashFile = "MenuBackground";
	//pDemoMenu->mNextStateName = "InGame"
	pDemoMenu->Init(this);
	mState.push_back(pDemoMenu);
#endif

	// push on the first state
	SetState((GameState*)GetState(0));


	//string str("menu.setvisiblemenu(loading); game.state(SO_Push, ingame); network.host(6000); environment.loadenvironment(metadata/environment/playground.mdd); actormgr.createactor(localinputcontroller, actor);");
	//gMessageMgr.SendStringMessage(str);

	mDebugFont = (Font*)gGame.mRenderFactory->Load(Font::Type, &ObjectLoadParams(gGame.mDevice, "UI/mini"));//mini")); //mini_10px"));

	Log::Success("[Game::Init] Complete\n");

	string gameMode;
	if (cmdLine.GetToken("gamemode", gameMode))
	{
		SetGameModeMsg setGameMode(gameMode);
		GetGameModeMgr()->HandleMsg(&setGameMode);
	}

	vector<string> joinList;
	if (cmdLine.GetToken("join", joinList))
	{
#ifdef NETWORK
		JoinMsg joinMsg;
		joinMsg.mServerPort = cmdLine.ToInt(joinList[1]);
		joinMsg.mServerAddress = joinList[0];
		joinMsg.mClientPort = 6465; // move client port to metadata
		gMessageMgr.BroadcastMessage(&joinMsg);
#endif

		SetState(GetStateByName("InGame"));
	}

	string level;
	if (cmdLine.GetToken("level", level))
	{
		GetEnvironment()->Load(level.c_str());
		SetState(GetStateByName("InGame"));
	}

	mDisplayStats = cmdLine.GetToken("stats");

	MemTracker::PopMemoryGroup();
	MemTracker::PushMemoryGroup("Game::Update");

	mGameUpdate.Reset();
	mNetworkUpdate.Reset();
	mPhysicsUpdate.Reset();
	return true;
}

bool Game::InitWindowAndDevice()
{
	// load render options from config file
	ScriptClass* renderOptions = gGame.mProfile->GetScriptClass("Render");
	if (!renderOptions)
		renderOptions = gGame.mProfile->CreateScriptClass("Render");

	ScriptVariable* widthVar = renderOptions->GetScriptVariable("windowWidth");
	int width = widthVar ? atoi(widthVar->ValueBegin()->mString.c_str()) : 1024;
	
	ScriptVariable* heightVar = renderOptions->GetScriptVariable("windowHeight");
	int height = heightVar ? atoi(heightVar->ValueBegin()->mString.c_str()) : 768;

	ScriptVariable* fullscreenVar = renderOptions->GetScriptVariable("fullscreen");
	bool fullscreen = fullscreenVar ? (strcmpi(fullscreenVar->ValueBegin()->mString.c_str(), "true") == 0 ? true : false) : true;

	ScriptVariable* aaVar = renderOptions->GetScriptVariable("antialiasing");
	int aa = aaVar ? atoi(aaVar->ValueBegin()->mString.c_str()) : 4;

	bool bSoftware = gGame.mRenderFactory->GetRenderFactoryType() == RFT_Software;
	ScriptVariable* perfVar = renderOptions->GetScriptVariable("performance");
	mPerformance = perfVar ? atoi(perfVar->ValueBegin()->mString.c_str()) : (bSoftware ? 0 : 2);

	mWindow = mRenderFactory->CreateWindow();
	WindowResolution res;
	res.bits = 32;
	res.width = width;
	res.height = height;

	// no ini file, means looks for heighest res possible for this monitor
	if (!widthVar || !heightVar)
	{
		Log::Print("Setting automatic resolution %i\n", fullscreen);
		mWindow->Init(mGameName.c_str(), 0, fullscreen);

		// grab the resolution and save to config
		char strBuffer[1024];
		WindowResolution res;
		mWindow->GetWindowResolution(res);
		if (!widthVar)
		{
			renderOptions->AppendVariable(ScriptVariable("windowWidth"));
			widthVar = renderOptions->GetScriptVariable("windowWidth");
			sprintf(strBuffer, "%i", res.width);
			widthVar->AppendValue(strBuffer);
		}

		if (!heightVar)
		{
			renderOptions->AppendVariable(ScriptVariable("windowHeight"));
			heightVar = renderOptions->GetScriptVariable("windowHeight");
			sprintf(strBuffer, "%i", res.height);
			heightVar->AppendValue(strBuffer);
		}

		SaveProfile();
	}
	else
	{
		Log::Print("Setting resolution %ix%ix%i:%i\n", res.width, res.height, res.bits, fullscreen);
		mWindow->Init(mGameName.c_str(), &res, fullscreen);
	}

	mDevice = mRenderFactory->CreateDevice();
	if (!mDevice->Init(mRenderFactory, mWindow, aa))
	{
		mWindow->Deinit();
		mRenderFactory->ReleaseWindow(&mWindow);
		mRenderFactory->ReleaseDevice(&mDevice);
		mDevice = 0;
		return false;
	}

#ifdef DEBUG
	mDevice->SetClearColour(Vector4(0.f, 0.125f, 0.3f, 1.f));
#endif

	return true;
}

void Game::Deinit()
{
	gFilesystem.OutputUsageStats();

	gGame.mRenderFactory->Release<Font>(&mDebugFont);

	// game mode mgr
	mGameModeMgr->Deinit();
	delete mGameModeMgr;

	// environment
	Environment* env = 0;
	while (mEnvStack.size())
	{
		env = mEnvStack.top();
		env->Deinit();

		mEnvStack.pop();

		delete env;
		env = 0;
	}

	if (mMusicPlayer)
	{
		mMusicPlayer->Deinit();
		delete mMusicPlayer;
	}

	mPrecache.Deinit();

	// free states here
	Iterator it;
	for (it = Begin(); it != End(); )
	{
		(*it)->Deinit();
		delete (*it);
		it = mState.erase(it);
	}

	mPlayback.Deinit();

	delete mProfile;

#ifdef PHYSICS
	// physics
	mPhysicsFactory->Deinit();
	delete mPhysicsFactory;
#endif

#ifdef NETWORK
	// network
	mNetworkFactory->Deinit();
	delete mNetworkFactory;

	// master server
	mMasterServer->Deinit();
	delete mMasterServer;
#endif

	// sound
	mSoundThread.Deinit();
	mSoundFactory->Deinit();
	delete mSoundFactory;

	mInput->Deinit();
	delete mInput;

#ifdef SOFTWARE
	if (gGame.mRenderFactory->GetRenderFactoryType() == RFT_Software)
	{
		// TODO: free software effects we allocated above
	}
#endif
	// render
	mDevice->Deinit(mRenderFactory, mWindow);
	mRenderFactory->ReleaseDevice(&mDevice);

	mWindow->Deinit();
	mRenderFactory->ReleaseWindow(&mWindow);

	mRenderFactory->Deinit();
	delete mRenderFactory;
	

	DeregisterReceiver();

	MemTracker::PopMemoryGroup();

	gFilesystem.Deinit(); // help track memory leaks
}

void Game::SetAudioOptions(int volume, int musicVolume)
{
	ScriptClass* audioOptions = gGame.mProfile->GetScriptClass("Audio");

	float floatMusicVolume = float(musicVolume) / 100.f;
	if (mMusicPlayer->SetVolume(floatMusicVolume))
	{
		char strBuffer[256];
		ScriptVariable* volumeVar = audioOptions->GetScriptVariable("musicVolume");
		sprintf(strBuffer, "%i", musicVolume);
		volumeVar->ValueBegin()->mString = strBuffer;

		SaveProfile();
	}

	// no change
	float floatVolume = float(volume) / 100.f;
	//if (floatVolume != mSoundFactory->GetListenerGain())
	{
		mSoundVolume = float(volume) / 100.f;
		//mSoundFactory->SetListenerGain(floatVolume);

		char strBuffer[256];
		ScriptVariable* volumeVar = audioOptions->GetScriptVariable("volume");
		sprintf(strBuffer, "%i", volume);
		volumeVar->ValueBegin()->mString = strBuffer;

		SaveProfile();
	}

	VolumeChangeMsg volumeMsg;
	gMessageMgr.BroadcastMessage(&volumeMsg);
}

void Game::ReinitWindowAndDevice(const WindowResolution& res, bool fullscreen, int aa)
{
	// no change
	WindowResolution curRes;
	mWindow->GetWindowResolution(curRes);
	if (aa == mDevice->GetMultisample()
		&& mWindow->IsFullScreen() == fullscreen
		&& res.width == curRes.width
		&& res.height == curRes.height)
		return;

	ScriptClass* renderOptions = gGame.mProfile->GetScriptClass("Render");

	mDevice->Deinit(mRenderFactory, mWindow);
	mWindow->Deinit();
	mInput->Deinit();

	mWindow->Init(mWindow->GetTitle(), &res, fullscreen);
	mDevice->Init(mRenderFactory, mWindow, aa);

	// stop flash blow out!
	mDevice->SetClearColour(Vector4(0.f, 0.f, 0.f, 1.f));
	mDevice->Begin();
	mDevice->End();

#ifdef SOFTWARE
	if (mRenderFactory->GetRenderFactoryType() == RFT_Software)
	{
		SoftwareDevice* device = (SoftwareDevice*)mDevice;
		ScriptVariable* interlaceVar = renderOptions->GetScriptVariable("softwareinterlace");
		bool interlace = interlaceVar ? (strcmpi(interlaceVar->ValueBegin()->mString.c_str(), "true") == 0 ? true : false) : false;
		device->SetInterlaced(interlace);
	}
#endif

	if (!mInput->Init(mWindow))
		Log::Error("Input Init Failed");

	char strBuffer[256];

	ScriptVariable* widthVar = renderOptions->GetScriptVariable("windowWidth");
	sprintf(strBuffer, "%i", res.width);
	widthVar->ValueBegin()->mString = strBuffer;

	ScriptVariable* heightVar = renderOptions->GetScriptVariable("windowHeight");
	sprintf(strBuffer, "%i", res.height);
	heightVar->ValueBegin()->mString = strBuffer;

	ScriptVariable* fullscreenVar = renderOptions->GetScriptVariable("fullscreen");
	fullscreenVar->ValueBegin()->mString = fullscreen ? "true" : "false";

	ScriptVariable* aaVar = renderOptions->GetScriptVariable("antialiasing");
	sprintf(strBuffer, "%i", aa);
	aaVar->ValueBegin()->mString = strBuffer;

	ScriptVariable* perfVar = renderOptions->GetScriptVariable("performance");
	sprintf(strBuffer, "%i", mPerformance);
	perfVar->ValueBegin()->mString = strBuffer;

	SaveProfile();

	mWindow->Show();
	mDevice->Begin();
	mDevice->End();

	mRenderFactory->ReloadResource(mDevice);

	ReloadResourceMsg reloadMsg;
	gMessageMgr.BroadcastMessage(&reloadMsg);

	ReloadResourcePostMsg reloadPostMsg;
	gMessageMgr.BroadcastMessage(&reloadPostMsg);

	if (mMusicPlayer)
	{
		mMusicPlayer->ReloadResource();
		mMusicPlayer->Play();
	}
}

bool Game::UpdateGame()
{
	// alt enter - toggle fullscreen
	if (gGame.mInput->GetStateBool(KEY_RAlt) && gGame.mInput->WasPressed(KEY_Enter))
	{
		bool fullscreen = !mWindow->IsFullScreen();
		WindowResolution res;
		mWindow->GetWindowResolution(res);
		ReinitWindowAndDevice(res, fullscreen, mDevice->GetMultisample());
	}

	if (GetCurrentState()->UpdateGame())
		return !mExit;

	return false;
}

bool Game::UpdateNetwork()
{
	return GetCurrentState()->UpdateNetwork();
}

bool Game::UpdatePhysics()
{
	return GetCurrentState()->UpdatePhysics();
}

void Game::Render()
{
	GetCurrentState()->Render();
}

bool Game::Run()
{
	int updateCount = 0;

	int i;
	bool playing = true;
	while (playing)
	{
		mGameUpdate.Update();
		mNetworkUpdate.Update();
		mPhysicsUpdate.Update();

		// debug mode we dont care about frame rate independance!
		// might needs some tweaks to do at least 1 update and no more than 5 updates per loop,
		// ie push some of the updates forwards or backwards so game runs slightly faster/slower for a while
#ifdef SINGLEFRAMESINGLEUPDATE

		unsigned long curTime = Time::GetMilliseconds();

		if (!(playing = UpdateGame()))
			return true;

		if (!(playing = UpdateNetwork()))
			return true;

		if (!(playing = UpdatePhysics()))
			return true;

#else

		for (i = 0; i < mGameUpdate.GetNumUpdates(); i++)
		{
			++updateCount;
			if (!(playing = UpdateGame()))
                return true;
		}

		for (i = 0; i < mNetworkUpdate.GetNumUpdates(); i++)
		{
			if (!(playing = UpdateNetwork()))
                 return true;
		}

		for (i = 0; i < mPhysicsUpdate.GetNumUpdates(); i++)
		{
			if (!(playing = UpdatePhysics()))
                return true;
		}
#endif

		Render();

#ifdef SINGLEFRAMESINGLEUPDATE
		unsigned long frameUpdateRate = (1000 / mGameUpdate.GetUpdateRate());
		unsigned long deltaTime = Time::GetMilliseconds() - curTime;
		if (deltaTime < frameUpdateRate)
		{
			unsigned long sleepTime = frameUpdateRate - deltaTime;
			Sleep(sleepTime);
		}
#endif
	}

	return true;
}

void Game::SaveProfile()
{
#ifdef DEBUG
	mProfile->Save("DebugProfile.mdd");
#else
	mProfile->Save("Profile.mdd");
#endif
}

void Game::SetState(GameState* state)
{
	StateMachine<GameState>::SetState(state);
}

MESSAGE_BEGIN(Game)
	MESSAGE_PASS_SIMPLE(Exit)
	MESSAGE_PASS(SetState)
#ifdef NETWORK
	MESSAGE_PASS(ClientConnect)
#endif
MESSAGE_END()

bool Game::SetState(SetStateMsg* msg)
{
	SetState(GetStateByName(msg->mStateName));
	return true;
}

#ifdef NETWORK
bool Game::ClientConnect(ClientConnectMsg* msg)
{
	SetGameModeMsg gameMode(GetGameMode()->GetName());
	if (GetEnvironment()->mNetwork->IsHosting())
		GetEnvironment()->mNetwork->SendMessage(&gameMode, this);

	return true;
}
#endif

GameState* Game::GetStateByName(const string& name)
{
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		if (strcmpi(name.c_str(), (*it)->GetName()) == 0)
			return *it;
	}

	return 0;
}

Environment* Game::CreateEnvironment()
{
	Environment* env = new Environment;
	PushEnvironment(env);
	env->Init(this);
	PopEnvironment();
	return env;
}

void Game::ReleaseEnvironemnt(Environment** env)
{
	if (!*env)
		return;

	delete *env;
	*env = 0;
}

void Game::PushEnvironment(Environment* env)
{
	mEnvStack.push(env);
}

void Game::PopEnvironment()
{
	mEnvStack.pop();
}

bool Game::Exit()
{
	// this should be moved else where
	mPlayback.Save();

	mExit = true;
	return true;
}

int Game::Main(const char* cmdLine)
{
	Game game;
	if (game.Init(CmdLine(cmdLine)))
	{
		game.Run();
		game.Deinit();
	}

	return 0;
}




