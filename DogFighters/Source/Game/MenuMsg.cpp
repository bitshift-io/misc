#include "StdAfx.h"
#include "MenuMsg.h"

RegisterMessageWithMessageMgr(SetVisibleMenu);

bool SetVisibleMenuMsg::ConvertFromString(vector<string>& params, TranslationTable* translationTable)
{
	if (params.size() <= 0)
		return false;

	mWindowName = params[0];
	return true;
}