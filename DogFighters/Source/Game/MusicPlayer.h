#ifndef _MUSICPLAYER_H_
#define _MUSICPLAYER_H_

#include "GameState.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"
#include "Flash/Flash.h"
#include "Environment/Environment.h"
#include <string>
#include <vector>

using namespace std;

class MusicPlayer : public FlashCallback
{
public:

	USES_METADATA

	bool					Init();
	void					Deinit();
	
	bool					Update();
	void					Render();

	void					Play();
	void					Stop();
	void					Pause();
	void					Resume();
	bool					SetVolume(float volume);
	float					GetVolume()					{ return mVolume; }

	bool					ReloadResource();

protected:

	void					FsCommand(Flash* flash, const char* command, const char* arg);
/*
	string					mFlashFile;
	Flash					mFlash;
	*/
	bool					mFirstIsThemeMusic;
	string					mMusicFiles;
	vector<string>			mMusicFile;
	Sound*					mMusic;
	float					mVolume;
	/*
	bool					mRender;
	*/
	int						mCurrentTrack;
};

#endif
