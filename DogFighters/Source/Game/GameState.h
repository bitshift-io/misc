#ifndef _GAMESTATE_H_
#define _GAMESTATE_H_

class Message;
class Game;

class GameState
{
public:

	virtual bool			Init(Game* game)	= 0;
	virtual void			Deinit()			= 0;

	virtual void			StateEnter()		= 0;
	virtual void			StateExit()			= 0;

	virtual bool			UpdateGame()		= 0;
	virtual bool			UpdateNetwork()		= 0;
	virtual bool			UpdatePhysics()		= 0;
	virtual void			Render()			= 0;

	virtual bool			HandleMsg(Message* msg) = 0;

	virtual const char*		GetName()			= 0;
};

#endif

