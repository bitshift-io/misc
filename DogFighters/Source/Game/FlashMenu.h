#ifndef _FLASHMENU_H_
#define _FLASHMENU_H_

#include "GameState.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"
#include "Flash/Flash.h"
#include "Environment/Environment.h"
#include <string>
#include <list>

using namespace std;

class UIWindow;
class Camera;
class SetVisibleMenuMsg;

class FlashMenu : public GameState, public MessageReceiver, public FlashCallback
{
public:

	USES_METADATA

	virtual bool			Init(Game* game);
	virtual void			Deinit();
	virtual void			StateEnter();
	virtual void			StateExit();

	virtual bool			UpdateGame();
	virtual bool			UpdateNetwork();
	virtual bool			UpdatePhysics();
	virtual void			Render();

	virtual bool			HandleMsg(Message* msg);

	virtual const char*		GetName()						{ return mStateName.c_str(); }

	virtual bool			ReloadResource(ReloadResourceMsg* msg);

	void					ClearPreviousState();

//protected:

	void					InitOptions();
	void					FsCommand(Flash* flash, const char* command, const char* arg);

	bool					mAllowNextStateChange;
	string					mStateName;
	string					mNextStateName;
	string					mFlashFile;
	Flash					mFlash;
	Vector4					mPrevBackgroundColour;

	string					mBackgroundFlashFile;
	Flash					mBackgroundFlash;

	WindowResolution		mResolution;
	bool					mFullscreen;
	int						mAA;
	bool					mApplyVisualOptions;

	const char*				mPreviousState;
};

#endif
