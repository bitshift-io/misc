#include "StdAfx.h"
#include "FlashMenu.h"

CLASS_METADATA_BEGIN(FlashMenu)
	CLASS_VARIABLE(FlashMenu, string, mFlashFile)
	CLASS_VARIABLE(FlashMenu, string, mBackgroundFlashFile)
	CLASS_VARIABLE(FlashMenu, string, mNextStateName)
	CLASS_VARIABLE(FlashMenu, string, mStateName)
CLASS_METADATA_END(FlashMenu)

bool FlashMenu::Init(Game* game)
{
	mPreviousState = 0;
	mAllowNextStateChange = true;
	mApplyVisualOptions = false;
	RegisterReceiver();

	mFlash.Init(gGame.mRenderFactory, gGame.mDevice, gGame.mWindow);
	mFlash.SetCallback(this);
	mFlash.Load(mFlashFile.c_str());	

	if (mBackgroundFlashFile[0] != '\0')
	{
		mBackgroundFlash.Init(gGame.mRenderFactory, gGame.mDevice, gGame.mWindow);
		mBackgroundFlash.SetCallback(this);
		mBackgroundFlash.Load(mBackgroundFlashFile.c_str());		
	}

	InitOptions();
	return true;
}

void FlashMenu::InitOptions()
{
	// inform flash of resolution options
	bool curResSet = false;
	int resIdx = 0;
	char res[256];
	WindowResolution desktopRes;
	WindowResolution curRes;
	gGame.mWindow->GetDesktopResolution(desktopRes);
	gGame.mWindow->GetWindowResolution(curRes);
	float desktopAspectRatio = (float)desktopRes.width / (float)desktopRes.height;
	float alternateApectRatio = 4.f / 3.f;
	WindowResolution lowestNativeRes = desktopRes;
	Window::WindowResolutionIterator resIt;

	for (resIt = gGame.mWindow->WindowResolutionBegin(); resIt != gGame.mWindow->WindowResolutionEnd(); ++resIt)
	{
		// 16 bit is useless!
		if (resIt->bits <= 16)
			continue;

		float curAspectRatio = (float)resIt->width / (float)resIt->height;
		if (desktopAspectRatio != curAspectRatio)
			continue;

		if (resIt->width < lowestNativeRes.width)
			lowestNativeRes = *resIt;
	}

	for (resIt = gGame.mWindow->WindowResolutionBegin(); resIt != gGame.mWindow->WindowResolutionEnd(); ++resIt)
	{
		// 16 bit is useless!
		if (resIt->bits <= 16)
			continue;

		// only put in resolution with the same aspect ratio as the desktop resolution
		// to make things easy
		float curAspectRatio = (float)resIt->width / (float)resIt->height;
		if (desktopAspectRatio != curAspectRatio && alternateApectRatio != curAspectRatio)
			continue;

		// we also put in 4:3 resolution LESS than the minimum native aspect ratio
		// for window mode
		if (resIt->width > lowestNativeRes.width && alternateApectRatio == curAspectRatio)
			continue;

		char* pAspectRatio = NULL;
		float aspectRatio = (float(resIt->width) / float(resIt->height));
		if (aspectRatio == (4.f / 3.f))
			pAspectRatio = "4:3";
		else if (aspectRatio == (16.f / 10.f))
			pAspectRatio = "16:10";
		else if (aspectRatio == (16.f / 9.f))
			pAspectRatio = "16:9";
		else if (aspectRatio == (5.f / 4.f))
			pAspectRatio = "5:4";

		if (pAspectRatio)
			sprintf(res, "%ix%i [%s]", resIt->width, resIt->height, pAspectRatio);
		else
			sprintf(res, "%ix%i", resIt->width, resIt->height);

		mFlash.CallMethod("AddResolution", "%s", res);

		// set this as the current resolution
		if (resIt->width == curRes.width && resIt->height == curRes.height)
		{
			mFlash.CallMethod("SetResolutionIdx", "%i", resIdx);
			curResSet = true;
		}

		++resIdx;
	}

	// if custom res...
	if (!curResSet)
	{
		sprintf(res, "%ix%i", curRes.width, curRes.height);
		mFlash.CallMethod("AddResolution", "%s", res);
		mFlash.CallMethod("SetResolutionIdx", "%i", resIdx);
	}

	// upload number of gamepads/joysticks
	int joystickCount = gGame.mInput->GetJoystickCount();
	mFlash.CallMethod("SetNumGamepads", "%i", joystickCount);

	// multisample level
	int multisample = gGame.mDevice->GetMultisample();
	mFlash.CallMethod("SetAA", "%i", multisample);

	// volume. the / then * 10 should round off to the closest 10
	int volume = int(gGame.mSoundVolume * 100.f);
	volume /= 10;
	volume *= 10;
	mFlash.CallMethod("SetVolume", "%i", volume);

	int musicVolume = int(gGame.mMusicPlayer->GetVolume() * 100.f);
	musicVolume /= 10;
	musicVolume *= 10;
	mFlash.CallMethod("SetMusicVolume", "%i", musicVolume);

	// fullscreen
	mFlash.CallMethod("SetWindowMode", "%i", gGame.mWindow->IsFullScreen() ? 1 : 0);

	// performance
	mFlash.CallMethod("SetPerformance", "%i", gGame.mPerformance);

#ifdef DEMO
	mFlash.CallMethod("SetDemo");
#endif
}

void FlashMenu::Deinit()
{
	mFlash.Deinit();

	if (mBackgroundFlashFile[0] != '\0')
		mBackgroundFlash.Deinit();

	DeregisterReceiver();
}

void FlashMenu::StateEnter()
{
	mFlash.Restart();

	if (!mPreviousState && gGame.GetCurrentState())
		mPreviousState = gGame.GetCurrentState()->GetName();

	// let the menu decide what to do, we just tell it its being displayed and the last state the game was in
	mFlash.CallMethod("ShowMenu", "%s", mPreviousState ? mPreviousState : "");

	if (mBackgroundFlashFile[0] != '\0')
		mBackgroundFlash.Restart();

	mPrevBackgroundColour = gGame.mDevice->GetClearColour();
	gGame.mDevice->SetClearColour(mFlash.GetBackgroundColour());
}

void FlashMenu::StateExit()
{
	// a prop can change the background colour if it was loaded while the menu was displayed!
	Vector4 currentBackgroundColour = gGame.mDevice->GetClearColour();
	if (currentBackgroundColour == mFlash.GetBackgroundColour())
		gGame.mDevice->SetClearColour(mPrevBackgroundColour);

	mFlash.Stop();

	if (mBackgroundFlashFile[0] != '\0')
		mBackgroundFlash.Stop();

	mPreviousState = 0;
}

bool FlashMenu::UpdateGame()
{
	if (mApplyVisualOptions)
	{
		gGame.ReinitWindowAndDevice(mResolution, mFullscreen, mAA);
		mApplyVisualOptions = false;
	}

	if (gGame.mWindow->HandleMessages())
		return false;

	int x, y;
	// hide cursor if cursor is over the window and is visible
	// ELSE show the cursor if the cursor is invisible and not over the client area
	if (gGame.mWindow->GetCursorPosition(x, y) && gGame.mWindow->IsCursorVisible())
		gGame.mWindow->SetCursorVisibility(false);
	else if (!gGame.mWindow->GetCursorPosition(x, y) && !gGame.mWindow->IsCursorVisible())
		gGame.mWindow->SetCursorVisibility(true);

	gGame.mMusicPlayer->Update();
	gGame.mInput->Update();

	if (gGame.mInput->WasPressed(KEY_r))
	{
		mFlash.Restart();

		if (mBackgroundFlashFile[0] != '\0')
			mBackgroundFlash.Restart();
	}

	mFlash.Update(gGame.mGameUpdate.GetDeltaTime(), gGame.mInput);

	if (mBackgroundFlashFile[0] != '\0')
		mBackgroundFlash.Update(gGame.mGameUpdate.GetDeltaTime(), gGame.mInput);

	// this could be moved to metadata
	// ie. on ke pressed, do this command
	// eg. input = key_esc
	// action = game.setstate(ingame)
	if (mAllowNextStateChange && mNextStateName.length() && (gGame.mInput->WasPressed(KEY_Esc) || mFlash.HasLooped()
		|| (gGame.mInput->GetJoystickCount() && gGame.mInput->WasPressed(JOYSTICK0_Button8))))
	{
		gGame.SetState(gGame.GetStateByName(mNextStateName));
	}

	return true;
}

bool FlashMenu::UpdateNetwork()
{
	return true;
}

bool FlashMenu::UpdatePhysics()
{
	return true;
}

void FlashMenu::Render()
{
	gGame.mDevice->Begin();

	if (gGame.mRenderFactory->GetRenderFactoryType() != RFT_Software && mBackgroundFlashFile[0] != '\0')
		mBackgroundFlash.Render();

	mFlash.Render();
	gGame.mMusicPlayer->Render();

	// render fps
	if (gGame.mDisplayStats)
	{
		gGame.mDebugFont->RenderText(-0.95f, 0.f, 1.f, "FPS   %i\nDrawCallCount   %i\nPolyCount   %i", gGame.mDevice->GetFPS(), gGame.mDevice->GetDrawCallCount(), gGame.mDevice->GetPolyCount());
	}

	gGame.mDevice->End();
}

void FlashMenu::FsCommand(Flash* flash, const char* command, const char* arg)
{
	if (strcmpi(command, "AllowNextStateChange(true)") == 0)
	{
		mAllowNextStateChange = true;
	}
	else if (strcmpi(command, "AllowNextStateChange(false)") == 0)
	{
		mAllowNextStateChange = false;
	}
	if (strcmpi(command, "ApplyVisualOptions") == 0)
	{
		mApplyVisualOptions = true;

		vector<string> args = ScriptFile::Tokenize(arg, ",", true);
		vector<string> resArgs = ScriptFile::Tokenize(args[0], "x", true);
		mResolution.bits = 32;
		mResolution.width = atoi(resArgs[0].c_str());
		mResolution.height = atoi(resArgs[1].c_str());

		if (strcmpi(args[1].c_str(), "true") == 0 || strcmpi(args[1].c_str(), "1") == 0)
			mFullscreen =  true;
		else
			mFullscreen = false;

		mAA = atoi(args[2].c_str());

		int performance = atoi(args[3].c_str());
		if (gGame.mPerformance != performance)
		{
			SetPerformanceMsg msg;
			gGame.mPerformance = performance;
			gMessageMgr.BroadcastMessage(&msg);
			
			char strBuffer[256];
			ScriptClass* renderOptions = gGame.mProfile->GetScriptClass("Render");
			ScriptVariable* perfVar = renderOptions->GetScriptVariable("performance");
			sprintf(strBuffer, "%i", performance);
			perfVar->ValueBegin()->mString = strBuffer;

			gGame.SaveProfile();
		}
	}
	else if (strcmpi(command, "ApplyAudioOptions") == 0)
	{
		vector<string> args = ScriptFile::Tokenize(arg, ",", true);
		gGame.SetAudioOptions(atoi(args[0].c_str()), atoi(args[1].c_str()));
	}
	else if (strcmpi(command, "Quit") == 0)
	{
#ifdef DEMO
		if (strcmpi(mStateName.c_str(), "Demo") == 0)
		{
			ExitMsg exit;
			gGame.HandleMsg(&exit);
		}
		else
		{
			gGame.SetState(gGame.GetStateByName("Demo"));
		}
#else
		ExitMsg exit;
		gGame.HandleMsg(&exit);
#endif
	}
	else if (strcmpi(command, "BuyNow") == 0)
	{
#ifdef WIN32
		const char* buffer = "http://archivegames.net/game.php?gameID=dogfighters";

		WCHAR wideBuffer[2048];
		MultiByteToWideChar(CP_ACP, 0, buffer, -1, wideBuffer, 2048);

		SHELLEXECUTEINFO info;
		memset(&info, 0 , sizeof(info));
	   
		info.cbSize = sizeof(info);
		info.dwHotKey = 0;
		info.cbSize = sizeof(SHELLEXECUTEINFO);
		info.fMask = 0;
		info.hIcon = NULL;
		info.hInstApp = NULL;
		info.hkeyClass = NULL;
		info.hProcess = NULL;
		info.hwnd = NULL;
		info.lpClass = NULL;
		info.lpDirectory = NULL;
		info.lpFile = wideBuffer;
		info.lpIDList = NULL;
		info.lpParameters = NULL;
		info.lpVerb = TEXT("open");
		info.nShow = SW_SHOWNORMAL;       
	   
		ShellExecuteEx(&info);

		ExitMsg exit;
		gGame.HandleMsg(&exit);
#endif
	}

	else
	{
		gMessageMgr.SendStringMessage(command);
	}

	// this should be done as components
#ifdef NETWORK
	if (strcmpi(command, "Join") == 0)
	{
		UIWindow* window = mActive->FindWindowWithAttachment(UIServerList::Type);
		UIServerList* serverList = static_cast<UIServerList*>(window->GetAttachmentByType(UIServerList::Type));
		if (serverList)
		{
			ServerInfo* serverInfo = serverList->GetCurrentServerInfo();
			if (serverList->Internet())
			{
				gGame.mMasterServer->JoinServer(serverInfo->mServerHandle, 6465); // move client port to metadata
			}

			Log::Print("Attempting to join '%s', '%s:%i'", serverInfo->mServerName.c_str(), serverInfo->mIP.c_str(), serverInfo->mPort);

			gGame.SaveConfig();

			SetVisibleMenuMsg menuMsg;
			menuMsg.mWindowName = "loading";
			SetVisibleMenu(&menuMsg);

			JoinMsg joinMsg;
			joinMsg.mServerAddress = serverInfo->mIP;
			joinMsg.mServerPort = serverInfo->mPort;
			joinMsg.mClientPort = 6465; // move client port to metadata
			gMessageMgr.BroadcastMessage(&joinMsg);	

			StateMsg stateMsg;
			stateMsg.mOperation = SO_Push;
			stateMsg.mStateName = "ingame";
			gMessageMgr.BroadcastMessage(&stateMsg);
		}
	}

	if (strcmpi(command, "Update") == 0)
	{
		UIWindow* window = mActive->FindWindowWithAttachment(UIServerList::Type);
		UIServerList* serverList = static_cast<UIServerList*>(window->GetAttachmentByType(UIServerList::Type));
		if (serverList)
		{
			serverList->Refresh();
		}
	}

	if (strcmpi(command, "Host") == 0)
	{
		// hrmm this sucks, need a more reliable way to do this shit
		UIWindow* window = mActive->FindWindowWithAttachment(UIDirectoryList::Type);
		if (window)
		{
			list<UIWindow*>::iterator it;
			for (it = mWindow.begin(); it != mWindow.end(); ++it)
			{
				window = (*it)->FindWindowWithAttachment(UIDirectoryList::Type);
				if (window)
					break;
			}
		}

		UIDirectoryList* mapList = static_cast<UIDirectoryList*>(window->GetAttachmentByType(UIDirectoryList::Type));
		if (!mapList)
			return;

		gGame.SaveConfig();

		SetVisibleMenuMsg menuMsg;
		menuMsg.mWindowName = "loading";
		SetVisibleMenu(&menuMsg);

		StateMsg stateMsg;
		stateMsg.mOperation = SO_Push;
		stateMsg.mStateName = "ingame";
		gMessageMgr.BroadcastMessage(&stateMsg);		

		HostMsg hostMsg;
		hostMsg.mInternet = true;
		hostMsg.mLAN = true;
		hostMsg.mLocalPort = 6000;
		hostMsg.mServerName = "fabs server";
		gGame.GetEnvironment()->mNetwork->Host(&hostMsg);

		LoadEnvironmentMsg loadEnv;
		loadEnv.mEnvironment = mapList->GetCurrentSelectedDirectory();
		gGame.GetEnvironment()->LoadEnvironment(&loadEnv);		
	}
#endif
}

void FlashMenu::ClearPreviousState()
{
	mPreviousState = 0;
}

MESSAGE_BEGIN(FlashMenu)
	MESSAGE_PASS(ReloadResource)
MESSAGE_END()

bool FlashMenu::ReloadResource(ReloadResourceMsg* msg)
{
	mFlash.ReloadResource();
	if (mBackgroundFlashFile[0] != '\0')
		mBackgroundFlash.ReloadResource();

	InitOptions();

	// reenter state to set background colour
	if (gGame.GetCurrentState() == this)
	{
		StateEnter();
	}

	return true;
}
