#ifndef _POOLCONTROL_H_
#define _POOLCONTROL_H_

#include "Core/Entity.h"

//
// give this to an entity that needs to be pooled, when the entity disables it self, this will return it to the pool
//
class PoolControl : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	PoolControl() : 
		mPool(0)
	{
	}

	virtual bool		HandleMsg(Message* msg);
	virtual bool		Enable(EnableMsg* msg);
	virtual bool		CreateFromPool(CreateFromPoolMsg* msg);

protected:

	Pool<Entity>*		mPool;
};

#endif