#include "StdAfx.h"
#include "MeshEmitter.h"

VectorParser<StringValue*> stringValueParser("StringValue*");

CLASS_METADATA_BEGIN(StringValue)
	CLASS_VARIABLE(StringValue, string, mString)
CLASS_METADATA_END(StringValue)

CLASS_METADATA_BEGIN(MeshEmitter)
	CLASS_VARIABLE(MeshEmitter, Vector4, mEmitDir)
	CLASS_VARIABLE(MeshEmitter, float, mEmitSpeed)
	CLASS_VARIABLE(MeshEmitter, int, mEmitCount)
	CLASS_VARIABLE(MeshEmitter, float, mMeshRadius)
	CLASS_VARIABLE(MeshEmitter, vector<StringValue*>, mSceneFile)
	CLASS_VARIABLE(MeshEmitter, float, mMinHeight)
	CLASS_VARIABLE(MeshEmitter, float, mMaxHeight)
	CLASS_VARIABLE(MeshEmitter, float, mDeviationScale)
	CLASS_VARIABLE(MeshEmitter, RenderFlags, mRenderFlags)
CLASS_METADATA_END(MeshEmitter)

MeshEmitter::MeshEmitter() :
	mRenderFlags(RF_Default)
{
}

void MeshEmitter::Init()
{
	Super::Init();

	// sever performance
	//if (gGame.mRenderFactory->GetRenderFactoryType() == RFT_Software)
	//	return;

	mEmitDir = Vector4(Math::Rand(-1.f, 1.f), 0.0f, Math::Rand(-1.f, 1.f));
	mEmitDir.Normalize();

	float halfWidth;
	float halfHeight;
	GetHalfWidthHeight(halfWidth, halfHeight, mMeshRadius);

	for (int i = 0; i < mEmitCount; ++i)
	{
		Scene* scene = gGame.mRenderFactory->LoadScene(&ObjectLoadParams(gGame.mDevice, mSceneFile[(int)Math::Rand(0, mSceneFile.size())]->mString));

		Matrix4 transform(MI_Identity);
		transform.RotateY(Math::DtoR(Math::Rand(-180.f, 180.f)));
		transform.SetTranslation(Vector4(Math::Rand(-halfWidth, halfWidth), Math::Rand(mMinHeight, mMaxHeight), Math::Rand(-halfHeight, halfHeight)));
		scene->SetTransform(transform);

		// should we name renderes so we can decide what model goes in what renderer?
		if (gGame.mPerformance >= 1)
			gGame.GetEnvironment()->mRenderMgr->InsertScene(scene, mRenderFlags);

		mMesh.push_back(scene);
	}
}

void MeshEmitter::Deinit()
{
	// clean up meshes
	vector<Scene*>::iterator meshIt = mMesh.begin();
	while (mMesh.size())
	{
		Scene* scene = *meshIt;
		gGame.GetEnvironment()->mRenderMgr->RemoveScene(scene);
		gGame.mRenderFactory->ReleaseScene(&scene);
		meshIt = mMesh.erase(meshIt);
	}

	Super::Deinit();
}

void MeshEmitter::Update()
{
	vector<Scene*>::iterator meshIt;
	for (meshIt = mMesh.begin(); meshIt != mMesh.end(); ++meshIt)
	{
		Scene* scene = *meshIt;

		Matrix4 transform = scene->GetTransform();

		Vector4 deviation(Math::Rand(-1.f, 1.f), 0.0f, Math::Rand(-1.f, 1.f));
        deviation.Normalize();
		deviation *= (float)Math::Rand(-1.f, 1.f) * mDeviationScale;

		Vector4 position = transform.GetTranslation();
		position += (mEmitDir * mEmitSpeed + deviation) * gGame.mGameUpdate.GetDeltaTime();

		float halfWidth;
		float halfHeight;
		GetHalfWidthHeight(halfWidth, halfHeight, mMeshRadius);

		if (OutOfView(halfWidth, halfHeight, position))
		{
			float vertical = Math::Rand(-halfHeight, halfHeight);
			float horizontal = Math::Rand(-halfWidth, halfWidth);

			position.SetZero();
			position.y = Math::Rand(mMinHeight, mMaxHeight);

			// if we can only spawn down one side... then spawn down that side....
			if (mEmitDir.x == 0.f)
			{
				position.z = vertical;
			}
			else if (mEmitDir.z == 0.f)
			{
				position.z = horizontal;
			}
			else
			{
				// ... else pick a side at random

				// we need to bias this to pick the biggest side more
				float horizontalDot = 1.0f - Math::Abs(mEmitDir.Dot(Vector4(1.f, 0.f, 0.f)));
				float verticalDot = 1.0f - Math::Abs(mEmitDir.Dot(Vector4(0.f, 0.f, 1.f)));

				float ratio = horizontalDot == 0.f ? 0.f : (verticalDot / horizontalDot);

				bool sideToSpawn = Math::Rand(0.f, 1.f) < ratio;
				if (sideToSpawn) // vertical
				{
					position.z = vertical;
					if (mEmitDir.x > 0.f)
						position.x = -halfWidth;
					else
						position.x = halfWidth;
				}
				else // hotizontal
				{
					position.x = horizontal;
					if (mEmitDir.z > 0.f)
						position.z = -halfHeight;
					else
						position.z = halfHeight;					
				}
			}
		}

		transform.SetTranslation(position);
		scene->SetTransform(transform);
	}

	Super::Update();
}

MESSAGE_BEGIN(MeshEmitter)
	MESSAGE_PASS(SetPerformance)
MESSAGE_SUPER_END()

bool MeshEmitter::SetPerformance(SetPerformanceMsg* msg)
{
	for (int i = 0; i < mEmitCount; ++i)
	{
		Scene* scene = mMesh[i];
		gGame.GetEnvironment()->mRenderMgr->RemoveScene(scene);
	}

	for (int i = 0; i < mEmitCount; ++i)
	{
		Scene* scene = mMesh[i];
		if (gGame.mPerformance >= 1)
			gGame.GetEnvironment()->mRenderMgr->InsertScene(scene);
	}

	return true;
}
