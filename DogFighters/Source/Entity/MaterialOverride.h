#ifndef _MATERIALOVERRIDE_H_
#define _MATERIALOVERRIDE_H_

#include "Core/Entity.h"

//
// when received the override message, it overrides all parent materials with this new material
//
class MaterialOverride : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	void				GetParentScene(GetSceneMsg& sceneMsg);
	void				OverrideMaterial(SceneNode* node, OverrideMaterialMsg* msg);

	virtual bool		HandleMsg(Message* msg);
	virtual bool		OverrideMaterial(OverrideMaterialMsg* msg);

protected:

	Scene*				mScene;
	string				mSceneName;
};

#endif