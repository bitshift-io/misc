#include "StdAfx.h"
#include "AlignToCamera.h"

CLASS_METADATA_BEGIN(AlignToCamera)
	CLASS_VARIABLE(AlignToCamera, string, mNodeName)
CLASS_EXTENDS_METADATA_END(AlignToCamera, Model)

AlignToCamera::AlignToCamera() : 
	mNode(0)
{
}

void AlignToCamera::Init()
{
	Scene* scene = GetParentScene();
	mNode = scene->GetNodeByName(mNodeName);
}

Scene* AlignToCamera::GetParentScene()
{
	Entity* parent = GetParent();
	GetSceneMsg sceneMsg;
	do
	{
		parent->HandleMsg(&sceneMsg);
		parent = parent->GetParent();
	} while (!sceneMsg.mScene);
	return sceneMsg.mScene;
}

void AlignToCamera::Update()
{
	Camera* camera = gGame.GetEnvironment()->mCameraMgr->GetCamera();
	Vector4 camPos = camera->GetPosition();

	Matrix4 parentTransform = mNode->GetParent()->GetWorldTransform();
	Matrix4 transform = mNode->GetWorldTransform();

	Matrix4 localTransform = mNode->GetLocalTransform();

	Vector4 localTranslation = localTransform.GetTranslation();

	Vector4 dirToCam = camPos - transform.GetTranslation();
	dirToCam.Normalize();

	Vector4 up = camera->GetWorld().GetYAxis();
	Vector4 right = dirToCam.Cross(up);
	up = right.Cross(dirToCam);

	Matrix4 newLocalTransform(MI_Identity);
	newLocalTransform.SetXAxis(right);
	newLocalTransform.SetYAxis(up);
	newLocalTransform.SetZAxis(dirToCam);

	Matrix4 parentInv = parentTransform;
	parentInv.Inverse();

	newLocalTransform = newLocalTransform * parentInv;
	newLocalTransform.SetTranslation(localTranslation);

	mNode->SetLocalTransform(newLocalTransform);

	Super::Update();
}