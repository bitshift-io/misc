#include "StdAfx.h"
#include "ParticleEffect.h"

CLASS_METADATA_BEGIN(ParticleEffect)
	CLASS_VARIABLE(ParticleEffect, string, mSceneFile)
	CLASS_VARIABLE(ParticleEffect, string, mNodeName)
CLASS_METADATA_END(ParticleEffect)

ParticleEffect::ParticleEffect() :
	mScene(0)
{
}

void ParticleEffect::Init()
{
	Super::Init();

	// sever performance
	if (gGame.mRenderFactory->GetRenderFactoryType() == RFT_Software)
		return;

	Scene* scene = static_cast<Pawn*>(GetTopMostParent())->GetScene(); //GetParentScene();
	mNode = scene->GetNodeByName(mNodeName.c_str());

	mParticleSystem = (ParticleSystem*)gGame.mRenderFactory->Create(ParticleSystem::Type);
	mParticleSystem->Init(scene, mContainer);

	gGame.GetEnvironment()->mEntityMgr->RegisterRenderable(this);
}

void ParticleEffect::Deinit()
{
	gGame.GetEnvironment()->mEntityMgr->DeregisterRenderable(this);
	gGame.mRenderFactory->Release((SceneObject**)&mParticleSystem);
	Super::Deinit();
}

bool ParticleEffect::HandleMsg(Message* msg)
{
	switch (msg->GetType())
	{
	case SpawnMsg::Type:
	case WarpMsg::Type:
		Reset();
		break;
	}

	return Super::HandleMsg(msg);
}

void ParticleEffect::Reset()
{
}

void ParticleEffect::Update()
{
	Super::Update();
}

void ParticleEffect::Render(int layer)
{
	if (layer != 1)
		return;
}

Scene* ParticleEffect::GetParentScene()
{
	Entity* parent = GetParent();
	GetSceneMsg sceneMsg;
	do
	{
		parent->HandleMsg(&sceneMsg);
		parent = parent->GetParent();
	} while (!sceneMsg.mScene);
	return sceneMsg.mScene;
}
