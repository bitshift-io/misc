#ifndef _PARTICLEFFECT_H_
#define _PARTICLEFFECT_H_

#include "Core/Entity.h"

//
// when queried for a spawn location it returns a random location
//
class ParticleEffect : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	ParticleEffect();

	virtual void	Init();
	virtual void	Deinit();
	virtual void	Update();
	virtual void	Render(int layer);
	virtual bool	HandleMsg(Message* msg);

	Scene*			GetParentScene();
	void			Reset();

protected:

	ParticleContainer*		mContainer;
	ParticleSystem*			mParticleSystem;

	string			mNodeName;
	SceneNode*		mNode;
	string			mSceneFile;
	Scene*			mScene;
};

#endif