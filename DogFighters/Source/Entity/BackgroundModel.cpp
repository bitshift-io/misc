#include "StdAfx.h"
#include "BackgroundModel.h"

CLASS_METADATA_BEGIN(BackgroundModel)
	CLASS_VARIABLE(BackgroundModel, float, mVelocityScale)
	CLASS_VARIABLE(BackgroundModel, Vector4, mMaxMovement)
CLASS_EXTENDS_METADATA_END(BackgroundModel, Model)

BackgroundModel::BackgroundModel() :
	mVelocityScale(0.08f)
{
}

void BackgroundModel::Init()
{
	Super::Init();

///#ifdef DEBUG
//	gGame.GetEnvironment()->mRenderMgr->RemoveScene(mScene);
//#endif

	// sever performance
	//if (gGame.mRenderFactory->GetRenderFactoryType() == RFT_Software)
	//	gGame.GetEnvironment()->mRenderMgr->RemoveScene(mScene);
	if (gGame.mPerformance < 2)
		gGame.GetEnvironment()->mRenderMgr->RemoveScene(mScene);
}

void BackgroundModel::Deinit()
{
	Super::Deinit();
}

void BackgroundModel::Update()
{
	// get controller/pawn list and move according to average velocity
	// we cant use this as we get a pop when player wraps
	Vector4 netVelocity(VI_Zero);
	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		CController* controller = (*controllerIt);
		Pawn* pawn = controller->GetPawn();
		if (!pawn)
			continue;

		PawnState* pawnState = pawn->GetCurrentState();
		if (controller->GetType() == PlayerController::Type && pawnState->GetType() == FlyState::Type)
		{
			FlyState* flyState = (FlyState*)pawnState;
			netVelocity += pawn->GetTransform().GetZAxis() * flyState->GetLinearSpeed() * gGame.mGameUpdate.GetDeltaTime();
		}
	}

	netVelocity.y = 0.0f;

	Matrix4 transform = mScene->GetTransform();
	Vector4 position = transform.GetTranslation();
	position -= netVelocity * mVelocityScale;

	position.x = Math::Clamp(-mMaxMovement.x, position.x, mMaxMovement.x);
    position.z = Math::Clamp(-mMaxMovement.y, position.z, mMaxMovement.y);

	transform.SetTranslation(position);
	mScene->SetTransform(transform);

	Super::Update();
}

MESSAGE_BEGIN(BackgroundModel)
	MESSAGE_PASS(SetPerformance)
MESSAGE_SUPER_END()

bool BackgroundModel::SetPerformance(SetPerformanceMsg* msg)
{
	gGame.GetEnvironment()->mRenderMgr->RemoveScene(mScene);

	if (gGame.mPerformance >= 2)
		gGame.GetEnvironment()->mRenderMgr->InsertScene(mScene);

	return true;
}
