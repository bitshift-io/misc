#ifndef _ENTITYPOOL_H_
#define _ENTITYPOOL_H_

#include "Core/Entity.h"

class PawnKilledMsg;
class ReturnToPoolMsg;

class EntityPool : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	virtual void		Init();
	virtual void		Deinit();
	virtual void		Update();

	virtual bool		HandleMsg(Message* msg);

	virtual bool		ReturnToPool(ReturnToPoolMsg* msg);

	// this should be changed to spawnEntityMsg, or a child entity should handle this
	virtual bool		PawnKilled(PawnKilledMsg* msg);

protected:

	vector<Pool<Entity> >		mEntityPool;
	list<Entity*>				mEntityToClone;
	int							mPoolSize;

	float						mSpawnChance;
};

#endif