#include "StdAfx.h"
#include "DeviceClear.h"

CLASS_METADATA_BEGIN(DeviceClear)
	CLASS_VARIABLE(DeviceClear, Vector4, mClearColour)
CLASS_METADATA_END(DeviceClear)

DeviceClear::DeviceClear() :
	mClearColour(VI_Zero)
{
}

void DeviceClear::Init()
{
	Super::Init();
	gGame.mDevice->SetClearColour(mClearColour);
}

MESSAGE_BEGIN(DeviceClear)
	MESSAGE_PASS(ReloadResource)
MESSAGE_END()

bool DeviceClear::ReloadResource(ReloadResourceMsg* msg)
{
	gGame.mDevice->SetClearColour(mClearColour);
	return true;
}

