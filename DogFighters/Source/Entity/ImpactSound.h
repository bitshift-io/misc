#ifndef _IMPACTSOUND_H_
#define _IMPACTSOUND_H_

#include "MsgSound.h"

//
// plays a sound on the parent when a certain message is received
// but only when they are in the fly state
//
class ImpactSound : public MsgSound
{
public:

	typedef MsgSound Super;
	USES_METADATA
	CLASSNAME

	ImpactSound();

	virtual void		Init();
	virtual bool		HandleMsg(Message* msg);
};


#endif