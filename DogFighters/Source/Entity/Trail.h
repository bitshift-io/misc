#ifndef _TRAIL_H_
#define _TRAIL_H_

#include "Core/Entity.h"

//
// when queried for a spawn location it returns a random location
//
class Trail : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	Trail();

	virtual void	Init();
	virtual void	Deinit();
	virtual void	Update();
	virtual void	Render(int layer);
	virtual bool	HandleMsg(Message* msg);
	
	bool ReloadResourcePost(ReloadResourcePostMsg* msg);

	Scene*			GetParentScene();
	void			Reset();

protected:

	MaterialBase*	mMaterialBase;
	Geometry*		mGeometry;
	string			mNodeName;
	SceneNode*		mNode;
	int				mSegments;
	float			mWidth;		
	int				mSegmentCount;
	float			mAlpha;
	float			mWidthVariation;
};

#endif