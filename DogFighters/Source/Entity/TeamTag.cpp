#include "StdAfx.h"
#include "TeamTag.h"

CLASS_METADATA_BEGIN(TeamTag)
CLASS_EXTENDS_METADATA_END(TeamTag, Model)

void TeamTag::Init()
{
	Super::Init();

	// disabled by default
	mEnabled = false;
	mScene->SetFlags(SNF_Hidden);
}

void TeamTag::Update()
{
	Super::Update();
}

MESSAGE_BEGIN(TeamTag)
	MESSAGE_PASS(EnableTeamTag)
	MESSAGE_PASS(NotifyTeamChange)
MESSAGE_SUPER_END()

bool TeamTag::EnableTeamTag(EnableTeamTagMsg* msg)
{
	mEnabled = msg->mEnable;
	if (mEnabled)
		mScene->ClearFlags(SNF_Hidden);
	else
		mScene->SetFlags(SNF_Hidden);

	return false;
}

bool TeamTag::Enable(EnableMsg* msg)
{
	return false;
}

bool TeamTag::NotifyTeamChange(NotifyTeamChangeMsg* msg)
{
	OverrideMaterialMsg overrideMsg;
	overrideMsg.mMaterialBase = msg->mTeam ? msg->mTeam->mMaterialBase : 0;
	if (overrideMsg.mMaterialBase)
		HandleMsg(&overrideMsg);
	return false;
}