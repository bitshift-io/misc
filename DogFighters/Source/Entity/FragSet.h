#ifndef _FRAGSET_H_
#define _FRAGSET_H_

#include "Core/Entity.h"

//
// when queried for a spawn location it returns a random location
//
class FragSet : public Model
{
public:

	typedef Model Super;
	USES_METADATA
	CLASSNAME

	FragSet();

	virtual void	Init();
	virtual void	Deinit();
	virtual void	Update();

	virtual bool	HandleMsg(Message* msg);

	void			InitFrag(SceneNode* node);

	void			Activate(Matrix4& projectileTrans);

	struct Frag
	{
		Vector4		rotationVel;
		Vector4		linearVel;
		SceneNode*	node;
	};

	Frag*			mFrag;
	int				mFragCount;
	float			mLife;
	float			mTimeToLive;
	float			mAcceleration;
	float			mMaxImpactVel;
	float			mMinImpactVel;
	float			mMaxRotationVel;
	float			mMinRotationVel;
};

#endif