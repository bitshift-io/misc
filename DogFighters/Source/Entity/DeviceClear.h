#ifndef _DEVICECLEAR_H_
#define _DEVICECLEAR_H_

class DeviceClear : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	DeviceClear();

	virtual void		Init();
	virtual bool		HandleMsg(Message* msg);
	virtual bool		ReloadResource(ReloadResourceMsg* msg);

protected:

	Vector4 mClearColour;
};

#endif