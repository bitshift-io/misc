#ifndef _MESHEMITTER_H_
#define _MESHEMITTER_H_

#include "Core/Entity.h"

struct StringValue
{
	USES_METADATA

	string mString;
};

//
// emits meshes
//
class MeshEmitter : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	MeshEmitter();

	virtual void		Init();
	virtual void		Deinit();
	virtual void		Update();

	virtual bool		HandleMsg(Message* msg);
	virtual bool		SetPerformance(SetPerformanceMsg* msg);

protected:

	Vector4				mEmitDir;
	float				mEmitSpeed;
	float				mMeshRadius;
	int					mEmitCount;
	float				mMinHeight;
	float				mMaxHeight;
	float				mDeviationScale;
	RenderFlags			mRenderFlags;

	vector<StringValue*>	mSceneFile;

	vector<Scene*>		mMesh;
};


#endif