#include "StdAfx.h"
#include "PoolControl.h"

CLASS_METADATA_BEGIN(PoolControl)
CLASS_METADATA_END(PoolControl)

MESSAGE_BEGIN(PoolControl)
	MESSAGE_PASS(Enable)
	MESSAGE_PASS(CreateFromPool)
MESSAGE_SUPER_END()

bool PoolControl::Enable(EnableMsg* msg)
{
	if (msg->mEnable)
		return true;
/*
	Entity* parent = GetParent();
	while (parent && parent->GetType() != EntityPool::Type)
	{
		parent = parent->GetParent();
	}

	if (!parent)
		return;

	ReturnToPoolMsg returnToPoolMsg(GetParent());
	parent->HandleMsg(&msg);
*/
	if (mPool)
		mPool->ReturnToPool(GetParent());

	mPool = 0;
	return true;
}

bool PoolControl::CreateFromPool(CreateFromPoolMsg* msg)
{
	mPool = msg->mPool;
	return true;
}