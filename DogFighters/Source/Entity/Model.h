#ifndef _MODEL_H_
#define _MODEL_H_

//
// simple model
//
class Model : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	Model();

	virtual void		Init();
	virtual void		Deinit();
	virtual bool		HandleMsg(Message* msg);
	virtual bool		GetScene(GetSceneMsg* msg);
	virtual bool		DebugDraw(DebugDrawMsg* msg);
	virtual bool		Enable(EnableMsg* msg);
	virtual bool		SetTransform(SetTransformMsg* msg);

protected:

	string				mNodeName;
	string				mSceneFile;
	Scene*				mScene;
	Matrix4				mTransform;
	RenderFlags			mRenderFlags;
	SceneNode*			mParentNode;
	bool				mVisible;
	bool				mPrecache;
};


#endif