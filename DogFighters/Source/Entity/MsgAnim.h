#ifndef _MSGANIM_H_
#define _MSGANIM_H_

//
// plays an animation on the parent when a certain message is received
// can optionally enable disable subobjects when the anim starts/completes
//
class MsgAnim : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME
	TYPE(10)

	MsgAnim();

	virtual void		Init();
	virtual void		Deinit();
	virtual bool		HandleMsg(Message* msg);
	virtual void		Update();
	Scene*				GetParentScene();

	virtual bool		Enable(EnableMsg* msg);
	virtual bool		IsEnabled(IsEnabledMsg* msg);

protected:

	bool				mLoop;
	string				mAnimationName;
	string				mMessageName;		// we should resolve this to a message type!
	int					mMessageType;
	float				mAnimTime;
	Animation*			mAnimation;
	bool				mOnlyVisibleWhileAnimating;
};


#endif