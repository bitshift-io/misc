#ifndef _MSGFORCEFEEDBACK_H_
#define _MSGFORCEFEEDBACK_H_

//
// plays an animation on the parent when a certain message is received
// can optionally enable disable subobjects when the anim starts/completes
//
class MsgForceFeedback : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME
	TYPE(13)

	MsgForceFeedback();

	virtual void		Init();
	virtual void		Deinit();
	virtual bool		HandleMsg(Message* msg);
	virtual void		Update();

protected:

	string				mMessageName;		// we should resolve this to a message type!
	int					mMessageType;
	ForceFeedbackEffect*	mForceFeedbackEffect;
};


#endif