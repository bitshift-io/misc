#include "StdAfx.h"
#include "Model.h"


CLASS_METADATA_BEGIN(Model)
	CLASS_VARIABLE(Model, list<Entity*>, mEntity)
	CLASS_VARIABLE(Model, string, mSceneFile)
	CLASS_VARIABLE(Model, string, mNodeName)
	CLASS_VARIABLE(Model, Matrix4, mTransform)
	CLASS_VARIABLE(Model, RenderFlags, mRenderFlags)
	CLASS_VARIABLE(Model, bool, mVisible)
	CLASS_VARIABLE(Model, bool, mPrecache)
CLASS_METADATA_END(Model)

Model::Model() : 
	mTransform(MI_Identity), 
	mScene(0),
	mRenderFlags(RF_Default),
	mVisible(true),
	mPrecache(false)
{

}

void Model::Init()
{
	if (mPrecache)
		gGame.mPrecache.CacheScene(mSceneFile.c_str()); // register with precache system if not already loaded

	mScene = gGame.mRenderFactory->LoadScene(&ObjectLoadParams(gGame.mDevice, mSceneFile));
	if (!mScene)
	{
		Log::Print("[Model::Init] Failed to load scene: %s\n", mSceneFile);
		gGame.mRenderFactory->ReleaseScene(&mScene);
		return;
	}

	mScene->SetTransform(mTransform);

	// iterate through parents to find valid model, this is the correct way to find a model
	mParentNode = 0;
	if (mNodeName.length())
	{
		GetSceneMsg scene;
		Entity* parent = mParent;
		while (parent && !mParentNode)
		{
			parent->HandleMsg(&scene);
			parent = parent->GetParent();

			if (scene.mScene && scene.mScene != mScene)
				mParentNode = scene.mScene->GetNodeByName(mNodeName);
		}

		if (mParentNode)
			mParentNode->InsertChild(mScene->GetRootNode());
	}

	// should we name renderes so we can decide what model goes in what renderer?
	gGame.GetEnvironment()->mRenderMgr->InsertScene(mScene, mRenderFlags);

	if (!mVisible)
		mScene->SetFlags(SNF_Hidden);

	Super::Init();
}

void Model::Deinit()
{
	Super::Deinit();

	if (mParentNode)
		mParentNode->RemoveChild(mScene->GetRootNode());

	gGame.GetEnvironment()->mRenderMgr->RemoveScene(mScene);
	gGame.mRenderFactory->ReleaseScene(&mScene);
}

MESSAGE_BEGIN(Model)
	MESSAGE_PASS(GetScene)
	MESSAGE_PASS(DebugDraw)
	MESSAGE_PASS(Enable)
	MESSAGE_PASS(SetTransform)
MESSAGE_SUPER_END()

bool Model::SetTransform(SetTransformMsg* msg)
{
	mScene->SetTransform(msg->mTransform);
	return true;
}

bool Model::Enable(EnableMsg* msg)
{
	if (!mScene)
		return false;

	if (!msg->mEnable)
		mScene->SetFlags(SNF_Hidden);

	if (msg->mEnable)
		mScene->ClearFlags(SNF_Hidden);

	return false;
}

bool Model::DebugDraw(DebugDrawMsg* msg)
{
	mScene->DebugBoundBox(gGame.mDevice);
	return true;
}

bool Model::GetScene(GetSceneMsg* msg)
{
	msg->mScene = mScene;
	//msg->mRenderFlags mRenderFlags;
	return true;
}
