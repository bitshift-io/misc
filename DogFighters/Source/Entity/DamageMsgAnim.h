#ifndef _DAMAGEMSGANIM_H_
#define _DAMAGEMSGANIM_H_

//
// special case for rabbit game mode, that plays when you take damage and arent killed
//
class DamageMsgAnim : public MsgAnim
{
public:

	typedef MsgAnim Super;
	USES_METADATA
	CLASSNAME

	DamageMsgAnim();

	virtual void		Init();
	virtual bool		HandleMsg(Message* msg);
};


#endif