#ifndef _RANDOMSPAWN_H_
#define _RANDOMSPAWN_H_

#include "Core/Entity.h"

//
// when queried for a spawn location it returns a random location
//
class RandomSpawn : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	virtual void		Init();

	virtual bool		HandleMsg(Message* msg);
	virtual bool		GetPawnSpawn(GetPawnSpawnMsg* msg);

protected:

	float				mOppositeTeamSpace;
	int					mFreeLocationAttempt;
};

#endif