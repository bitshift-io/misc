#include "StdAfx.h"
#include "MsgAnim.h"

CLASS_METADATA_BEGIN(MsgAnim)
	CLASS_VARIABLE(MsgAnim, bool, mOnlyVisibleWhileAnimating)
	CLASS_VARIABLE(MsgAnim, string, mAnimationName)
	CLASS_VARIABLE(MsgAnim, string, mMessageName)
	CLASS_VARIABLE(MsgAnim, bool, mLoop)
CLASS_METADATA_END(MsgAnim)

MsgAnim::MsgAnim() : 
	mOnlyVisibleWhileAnimating(false),
	mMessageType(-1),
	mLoop(false)
{
}

void MsgAnim::Init()
{
	mAnimation = (Animation*)gGame.mRenderFactory->Load(Animation::Type, &ObjectLoadParams(gGame.mDevice, mAnimationName));

	if (mMessageName.length())
		mMessageType = gMessageMgr.GetType(mMessageName.c_str());

	mAnimTime = -1.f;

	// register animation with the scene
	Scene* scene = GetParentScene();
	scene->InsertAnimation(mAnimation);

	if (mOnlyVisibleWhileAnimating)
		scene->SetFlags(SNF_Hidden);
}

void MsgAnim::Deinit()
{
	gGame.mRenderFactory->Release((SceneObject**)&mAnimation);
}

Scene* MsgAnim::GetParentScene()
{
	Entity* parent = GetParent();
	GetSceneMsg sceneMsg;
	do
	{
		parent->HandleMsg(&sceneMsg);
		parent = parent->GetParent();
	} while (!sceneMsg.mScene);
	return sceneMsg.mScene;
}

bool MsgAnim::HandleMsg(Message* msg)
{
	if (msg->GetType() == mMessageType)
	{
		mAnimTime = 0.f;

		// update 1 frame.. does this fix the initial 1 frame randomness were getting?
		//Update();
	}

	switch (msg->GetType())
	{
		MESSAGE_PASS(IsEnabled)
		MESSAGE_PASS(Enable)
	}

	return Super::HandleMsg(msg);
}

void MsgAnim::Update()
{
	Super::Update();

	if (mAnimTime < 0.f)
		return;

	Scene* scene = GetParentScene();

	// just activate if time = 0.f
	if (mAnimTime == 0.f)
	{
		scene->SetAnimation(mAnimation);
		if (mOnlyVisibleWhileAnimating)
		{			
			scene->ClearFlags(SNF_Hidden);
		}
	}

	mAnimTime += gGame.mGameUpdate.GetDeltaTime();

	
	scene->ProgressAnimation(gGame.mGameUpdate.GetDeltaTime());
	scene->ApplyAnimation();

	// check if we need to be disabled
	if (mAnimTime >= mAnimation->GetLength())
	{
		if (mLoop)
		{
			mAnimTime = 0.f;
		}
		else
		{
			mAnimTime = -1.f;
			if (mOnlyVisibleWhileAnimating)
				scene->SetFlags(SNF_Hidden);		
		}
	}
}

bool MsgAnim::Enable(EnableMsg* msg)
{
	if (!msg->mEnable && mAnimTime != -1.f)
	{
		Scene* scene = GetParentScene();

		mAnimTime = -1.f;

		if (mOnlyVisibleWhileAnimating)
			scene->SetFlags(SNF_Hidden);
	}
	return false;
}

bool MsgAnim::IsEnabled(IsEnabledMsg* msg)
{
	msg->mEnabled = (mAnimTime != -1.f);
	return false;
}