#include "StdAfx.h"
#include "DamageMsgAnim.h"

CLASS_METADATA_BEGIN(DamageMsgAnim)
CLASS_EXTENDS_METADATA_END(DamageMsgAnim, MsgAnim)


DamageMsgAnim::DamageMsgAnim()
{
	mMessageName = "TakeDamage";
	MsgAnim();
}

void DamageMsgAnim::Init()
{
	Super::Init();
}

bool DamageMsgAnim::HandleMsg(Message* msg)
{
	if (msg->GetType() == mMessageType)
	{
		TakeDamageMsg* damageMsg = (TakeDamageMsg*)msg;
		if (!damageMsg->mKilled)
			return Super::HandleMsg(msg);	
	}

	return false;
}
