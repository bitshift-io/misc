#include "StdAfx.h"
#include "Trail.h"

CLASS_METADATA_BEGIN(Trail)
	CLASS_VARIABLE(Trail, string, mNodeName)
	CLASS_VARIABLE(Trail, int, mSegments)
	CLASS_VARIABLE(Trail, float, mWidth)
	CLASS_VARIABLE(Trail, float, mAlpha)
	CLASS_VARIABLE(Trail, float, mWidthVariation)
CLASS_METADATA_END(Trail)

Trail::Trail() :
	mNode(0)
{
}

void Trail::Init()
{
	Super::Init();

	// sever performance
	if (gGame.mRenderFactory->GetRenderFactoryType() == RFT_Software)
		return;

	Pawn* pawn = static_cast<Pawn*>(GetTopMostParent());
	Scene* scene = pawn->GetScene(); //GetParentScene();
	mNode = scene->GetNodeByName(mNodeName.c_str());

	mMaterialBase = pawn->GetMaterialBaseOverride();
	mSegmentCount = 0;

	mGeometry = (Geometry*)gGame.mRenderFactory->Create(Geometry::Type);
	mGeometry->Init(gGame.mDevice, VF_Position | VF_Colour, mSegments * 2, mSegments * 6, RBU_Dynamic);

	// set defaults
	RenderBufferLock lock;
	VertexFrame& frame = mGeometry->GetVertexFrame();

	// colours
	frame.mColour->Lock(lock);

	for (unsigned int i = 0; i < (mSegments * 2); ++i)
		lock.vector4Data[i] = Vector4(1.f, 1.f, 1.f, 0.5f);

	frame.mColour->Unlock(lock);

	// indices
	mGeometry->GetIndexBuffer()->Lock(lock);

	unsigned int v = 0;
	for (unsigned int i = 0; i < (mSegments * 6); i += 6, v += 2)
	{
		lock.uintData[i + 0] = 2 + v;
		lock.uintData[i + 1] = 1 + v;
		lock.uintData[i + 2] = 0 + v;

		lock.uintData[i + 3] = 1 + v;
		lock.uintData[i + 4] = 2 + v;
		lock.uintData[i + 5] = 3 + v;
	}

	mGeometry->GetIndexBuffer()->Unlock(lock);

	gGame.GetEnvironment()->mEntityMgr->RegisterRenderable(this);
}

void Trail::Deinit()
{
	gGame.mRenderFactory->Release((SceneObject**)&mGeometry);
	gGame.GetEnvironment()->mEntityMgr->DeregisterRenderable(this);
	Super::Deinit();
}

bool Trail::HandleMsg(Message* msg)
{
	switch (msg->GetType())
	{
	MESSAGE_PASS(ReloadResourcePost)

	case SpawnMsg::Type:
	case WarpMsg::Type:
		Reset();
		break;
	}

	return Super::HandleMsg(msg);
}

bool Trail::ReloadResourcePost(ReloadResourcePostMsg* msg)
{/*
	Pawn* pawn = static_cast<Pawn*>(GetTopMostParent());
	mMaterialBase = pawn->GetMaterialBaseOverride();*/
	return false;
}

void Trail::Reset()
{
	mSegmentCount = 0;
}

void Trail::Update()
{
	Super::Update();

	if (!mNode)
		return;

	// only draw trails if in flystate
	if (static_cast<Pawn*>(GetTopMostParent())->GetCurrentState()->GetType() != FlyState::Type)
	{
		Reset();
		return;
	}

	Matrix4 transform = mNode->GetWorldTransform();

	RenderBufferLock lock;
	VertexFrame& frame = mGeometry->GetVertexFrame();
	frame.mPosition->Lock(lock);

	// shift all existing verts across 2 to make space for the new leading position
	if (mSegmentCount)
	{
		for (int v = (mSegmentCount * 2) + 1; v >= 2; --v)
		{
			lock.vector4Data[v] = lock.vector4Data[v - 2];
		}
	}

	lock.vector4Data[0] = transform.GetTranslation() + transform.GetXAxis() * (mWidth - Math::Rand(0.f, mWidthVariation)) * 0.5f;
	lock.vector4Data[1] = transform.GetTranslation() - transform.GetXAxis() * (mWidth - Math::Rand(0.f, mWidthVariation)) * 0.5f;

	frame.mPosition->Unlock(lock);
	
	// colours
	// here we shift all alpha values
	frame.mColour->Lock(lock);

	if (mSegmentCount)
	{
		for (int v = 0; v < (mSegmentCount * 2); v += 2)
		{
			lock.vector4Data[v].a = (1.0f - (float(v) / float(mSegmentCount))) * mAlpha;
			lock.vector4Data[v + 1].a = lock.vector4Data[v].a;
		}
	}

	frame.mColour->Unlock(lock);

	mSegmentCount = Math::Min(mSegmentCount + 1, mSegments - 1);
}

void Trail::Render(int layer)
{
	if (layer != 2 || !mMaterialBase)
		return;

	if ((mSegmentCount - 1) > 0)
	{
		Camera* camera = gGame.GetEnvironment()->mCameraMgr->GetCamera();

		// we can do this because we are rendering in immediate mode
		EffectParameterValue* colourParam = mMaterialBase->GetCurrentTechnique()->GetParameterByName("colour");
		if (!colourParam)
			return;

		Vector4 colour;
		colourParam->GetValue(&colour);
		colourParam->SetValue(&Math::Lerp(Vector4(1.f, 1.f, 1.f, 1.f), colour, 0.25f));

		MaterialPass& pass = (*mMaterialBase->GetCurrentTechnique()->PassBegin());
		pass.mDeviceState.alphaBlendEnable = true;
		CullFace cullFace = pass.mDeviceState.cullFace;
		pass.mDeviceState.cullFace = CF_None;

		mMaterialBase->Begin();
		mMaterialBase->BeginPass(0);

		mMaterialBase->GetEffect()->SetMatrixParameters(camera->GetProjection(), camera->GetView(), camera->GetWorld(), Matrix4(MI_Identity));
		mMaterialBase->SetPassParameter();

		//gGame.mDevice->Draw(1, mMaterial, mGeometry, VF_Position | VF_Colour, 0, (mSegmentCount - 1) * 6);
		mGeometry->Draw(0, (mSegmentCount - 1) * 6);

		mMaterialBase->EndPass();
		mMaterialBase->End();

		// restore
		pass.mDeviceState.cullFace = cullFace;
		pass.mDeviceState.alphaBlendEnable = false; 
		colourParam->SetValue(&colour);
	}
}

Scene* Trail::GetParentScene()
{
	Entity* parent = GetParent();
	GetSceneMsg sceneMsg;
	do
	{
		parent->HandleMsg(&sceneMsg);
		parent = parent->GetParent();
	} while (!sceneMsg.mScene);
	return sceneMsg.mScene;
}
