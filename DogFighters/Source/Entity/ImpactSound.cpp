#include "StdAfx.h"
#include "ImpactSound.h"

CLASS_METADATA_BEGIN(ImpactSound)
CLASS_EXTENDS_METADATA_END(ImpactSound, MsgSound)

ImpactSound::ImpactSound()
{
}

void ImpactSound::Init()
{
	Super::Init();
}

bool ImpactSound::HandleMsg(Message* msg)
{
	if (msg->GetType() == mMessageType 
		&& static_cast<Pawn*>(GetTopMostParent())->GetCurrentState()->GetType() == FlyState::Type)
	{
		if (mSound)
		{
			mSound->Play();
		}
	}

	switch (msg->GetType())
	{
	MESSAGE_PASS(SetPause);
	MESSAGE_PASS(VolumeChange);
	}

	return Super::HandleMsg(msg);
}
