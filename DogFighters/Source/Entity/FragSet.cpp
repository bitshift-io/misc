#include "StdAfx.h"
#include "FragSet.h"

CLASS_METADATA_BEGIN(FragSet)
	CLASS_VARIABLE(FragSet, float, mTimeToLive)
	CLASS_VARIABLE(FragSet, float, mAcceleration)
	CLASS_VARIABLE(FragSet, float, mMaxImpactVel)
	CLASS_VARIABLE(FragSet, float, mMinImpactVel)
	CLASS_VARIABLE(FragSet, float, mMaxRotationVel)
	CLASS_VARIABLE(FragSet, float, mMinRotationVel)
CLASS_EXTENDS_METADATA_END(FragSet, Model)

FragSet::FragSet()
{
}

void FragSet::Init()
{
	Super::Init();

	mLife = 0.f;
	mFrag = 0;
	mFragCount = 0;
	InitFrag(mScene->GetRootNode());

	mMaxRotationVel = Math::DtoR(mMaxRotationVel);
	mMinRotationVel = Math::DtoR(mMinRotationVel);
}

void FragSet::Deinit()
{
	Super::Deinit();

	delete[] mFrag;
	mFrag = 0;
}

void FragSet::InitFrag(SceneNode* node)
{
	SceneObject* obj = node->GetObject();
	if (obj && obj->GetType() == Mesh::Type)
	{
		// make a bigger array for frags
		Frag* oldFrag = mFrag;
		mFrag = new Frag[mFragCount+1];
		memcpy(mFrag, oldFrag, mFragCount * sizeof(Frag));
		mFrag[mFragCount].node = node;
		++mFragCount;

		// delete old frags array
		delete[] oldFrag;
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
	{
		InitFrag(*it);
	}
}

bool FragSet::HandleMsg(Message* msg)
{
	Pawn* pawn = (Pawn*)GetTopMostParent();
	if (msg->GetType() == TakeDamageMsg::Type/* && pawn->GetCurrentState()->GetType() != ParachuteDyingState::Type*/)
	{
		// only play IF killed OR we are in parachute state
		// this fixes the problem where bombers that take multiple damage
		// were emitting frags when hit but not killed
		TakeDamageMsg* damageMsg = (TakeDamageMsg*)msg;
		if (damageMsg->mKilled || pawn->GetCurrentState()->GetType() == ParachuteState::Type)
		{
			if (!damageMsg->mProjectile)
			{
				// calculate an activate matrix
				Vector4 attackerPos = damageMsg->mAttacker->GetTransform().GetTranslation(); 
				Vector4 attackedPos = pawn->GetTransform().GetTranslation();
				Vector4 dirToAttacked = attackedPos - attackerPos;
				dirToAttacked.Normalize3();
				Vector4 up(VI_Up);
				Vector4 right = dirToAttacked.Cross(up);

				Matrix4 transform(MI_Identity);
				transform.SetTranslation(attackerPos);
				transform.SetXAxis(right);
				transform.SetYAxis(up);
				transform.SetZAxis(dirToAttacked);
				Activate(transform);
			}
			else
			{
				Activate(((Projectile*)damageMsg->mProjectile)->GetTransform());	
			}
		}
	}

	return Super::HandleMsg(msg);
}

void FragSet::Update()
{
	Super::Update();

	if (mLife <= 0.f)
		return;

	mLife -= gGame.mGameUpdate.GetDeltaTime();

	if (mLife <= 0.f)
	{
		EnableMsg enable(false);
		Enable(&enable);
	}

	float scale = Math::Lerp(1.f, mLife / mTimeToLive, 0.5f);
	for (int i = 0; i < mFragCount; ++i)
	{
		Frag& frag = mFrag[i];
		frag.linearVel.y -= mAcceleration * gGame.mGameUpdate.GetDeltaTime();

		Matrix4 transform = frag.node->GetWorldTransform();
		Vector4 translation = transform.GetTranslation() + frag.linearVel * gGame.mGameUpdate.GetDeltaTime();

		Matrix4 rot(MI_Identity);
		rot.RotateX(frag.rotationVel.x * gGame.mGameUpdate.GetDeltaTime());
		rot.RotateY(frag.rotationVel.y * gGame.mGameUpdate.GetDeltaTime());
		rot.RotateZ(frag.rotationVel.z * gGame.mGameUpdate.GetDeltaTime());

		transform = transform.Multiply3(rot);
		transform.SetScale(scale);
		transform.SetTranslation(translation);

		// expensive!
		frag.node->SetWorldTransform(transform);
	}
}


void FragSet::Activate(Matrix4& projectileTrans)
{
	Pawn* pawn = (Pawn*)GetTopMostParent();
	Matrix4 transform = pawn->GetTransformLastFrame();
	mScene->SetTransform(transform);

	mLife = mTimeToLive;
	for (int i = 0; i < mFragCount; ++i)
	{
		Frag& frag = mFrag[i];
		frag.node->SetLocalTransform(frag.node->GetOrigonalLocalTransform());

		frag.linearVel = frag.node->GetWorldTransform().GetTranslation() - projectileTrans.GetTranslation();
		frag.linearVel.Normalize3();

		// aligns frags more with direction of bullet
		frag.linearVel += projectileTrans.GetZAxis();// * 2.f;		
		frag.linearVel.Normalize3();

		// velocity goes forwards as well, mostly!
		//frag.linearVel += transform.GetZAxis() * 5.f;
		//frag.linearVel.Normalize3();

		frag.linearVel *= Math::Rand(mMinImpactVel, mMaxImpactVel);
		frag.rotationVel = Vector4(Math::Rand(mMinRotationVel, mMaxRotationVel), Math::Rand(mMinRotationVel, mMaxRotationVel), Math::Rand(mMinRotationVel, mMaxRotationVel));
	}

	EnableMsg enable(true);
	Enable(&enable);
}
