#include "StdAfx.h"
#include "RandomSpawn.h"

CLASS_METADATA_BEGIN(RandomSpawn)
	CLASS_VARIABLE(RandomSpawn, int, mFreeLocationAttempt)
	CLASS_VARIABLE(RandomSpawn, float, mOppositeTeamSpace)
CLASS_METADATA_END(RandomSpawn)

MESSAGE_BEGIN(RandomSpawn)
	MESSAGE_PASS(GetPawnSpawn)
MESSAGE_SUPER_END()

void RandomSpawn::Init()
{
	Super::Init();
	mOppositeTeamSpace *= mOppositeTeamSpace; // sqr it
}

bool RandomSpawn::GetPawnSpawn(GetPawnSpawnMsg* msg)
{
	float halfWidth;
	float halfHeight;
	GetHalfWidthHeight(halfWidth, halfHeight, msg->mPawn->GetRadius());

	Matrix4 transform(MI_Identity);
	transform.RotateY(Math::DtoR(Math::Rand(-180.f, 180.f)));

	for (int i = 0; i < mFreeLocationAttempt; ++i)
	{
		bool bFarEnoughFromEnemy = true;
		transform.SetTranslation(Vector4(Math::Rand(-halfWidth, halfWidth), 0.f, Math::Rand(-halfHeight, halfHeight)));

		Vector4 spawnPos = transform.GetTranslation();
		Team* team = msg->mPawn->GetController()->GetTeam();
		GameModeMgr::ControllerIterator it;
		for (it = gGame.GetGameModeMgr()->ControllerBegin(); it != gGame.GetGameModeMgr()->ControllerEnd(); ++it)
		{
			if ((*it)->GetTeam() == team)
				continue;

			Vector4 pos = (*it)->GetPawn()->GetTransform().GetTranslation();
			Vector4 deltaPos = spawnPos - pos;
			float magSqrd = deltaPos.MagSquared3();
			if (magSqrd < mOppositeTeamSpace)
			{
				bFarEnoughFromEnemy = false;
				break;
			}
		}

		if (bFarEnoughFromEnemy)
			break;
	}

	msg->mTransform = transform;
	return true;
}