#include "StdAfx.h"
#include "MsgSound.h"

CLASS_METADATA_BEGIN(MsgSound)
	CLASS_VARIABLE(MsgSound, string, mMessageName)
	CLASS_VARIABLE(MsgSound, SoundPool*, mSound)
CLASS_METADATA_END(MsgSound)

MsgSound::MsgSound() :
	mSound(0)
{
}

void MsgSound::Init()
{
	mMessageType = gMessageMgr.GetType(mMessageName.c_str());
	//mSound = gGame.mSoundFactory->CreateSound(mSoundName);
	mSound->Init();
	mSound->SetGain(gGame.mSoundVolume);
}

void MsgSound::Deinit()
{
	//gGame.mSoundFactory->ReleaseSound(&mSound);
	mSound->Deinit();
	delete mSound;
	mSound = 0;
}

bool MsgSound::HandleMsg(Message* msg)
{
	if (msg->GetType() == mMessageType)
	{
		if (mSound)
		{
			mSound->Play();
		}
	}

	switch (msg->GetType())
	{
	MESSAGE_PASS(SetPause);
	MESSAGE_PASS(VolumeChange);
	}

	return Super::HandleMsg(msg);
}


bool MsgSound::VolumeChange(VolumeChangeMsg* msg)
{
	if (mSound)
		mSound->SetGain(gGame.mSoundVolume);

	return false;
}


bool MsgSound::SetPause(SetPauseMsg* msg)
{
	if (mSound)
		mSound->Stop();

	return false;
}


void MsgSound::Update()
{
	Super::Update();

	if (mSound)
	{
		GetTransformMsg transform;
		GetTopMostParent()->HandleMsg(&transform);

		mSound->SetPosition(transform.mTransform.GetTranslation());
		mSound->Update();
	}
}