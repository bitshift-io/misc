#include "StdAfx.h"
#include "MaterialOverride.h"

CLASS_METADATA_BEGIN(MaterialOverride)
CLASS_METADATA_END(MaterialOverride)

void MaterialOverride::GetParentScene(GetSceneMsg& sceneMsg)
{
	Entity* parent = GetParent();
	do
	{
		parent->HandleMsg(&sceneMsg);
		parent = parent->GetParent();
	} while (!sceneMsg.mScene);
}

MESSAGE_BEGIN(MaterialOverride)
	MESSAGE_PASS(OverrideMaterial)
MESSAGE_SUPER_END()

bool MaterialOverride::OverrideMaterial(OverrideMaterialMsg* msg)
{
#if !defined (NO_CLONING)
	// we need to remove the scene, then reinsert once materials are adjusted!
	GetSceneMsg sceneMsg;
	GetParentScene(sceneMsg);
	gGame.GetEnvironment()->mRenderMgr->RemoveScene(sceneMsg.mScene);
	OverrideMaterial(sceneMsg.mScene->GetRootNode(), msg);
	gGame.GetEnvironment()->mRenderMgr->InsertScene(sceneMsg.mScene, sceneMsg.mRenderFlags);
#endif
	return false;
}

void MaterialOverride::OverrideMaterial(SceneNode* node, OverrideMaterialMsg* msg)
{
	// we now need to instance any meshes and materials so we can apply unique colours
	if (node->GetObject())
	{
		switch (node->GetObject()->GetType())
		{
		case Mesh::Type:
			{
				// clone the mesh, so we can clone the materials
				// to apply material specific overrides
				Mesh* mesh = static_cast<Mesh*>(node->GetObject());
				//Mesh* clone = (Mesh*)mesh->Clone();
				Mesh::MeshMatIterator it;
				for (it = mesh->Begin(); it != mesh->End(); ++it)
				{
					it->material->SetMaterialBase(msg->mMaterialBase);
					//it->material->ReleaseReference();
					//msg->mMaterial->AddReference();
					//it->material = msg->mMaterial;
				}
				//node->SetObject(clone);
			}
			break;

		default:
			break;
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
		OverrideMaterial(*it, msg);
}
