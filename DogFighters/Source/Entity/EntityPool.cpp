#include "StdAfx.h"
#include "EntityPool.h"

CLASS_METADATA_BEGIN(EntityPool)
	CLASS_VARIABLE(EntityPool, list<Entity*>, mEntity)
	CLASS_VARIABLE(EntityPool, list<Entity*>, mEntityToClone)	
	CLASS_VARIABLE(EntityPool, int, mPoolSize)
	CLASS_VARIABLE(EntityPool, float, mSpawnChance)
CLASS_METADATA_END(EntityPool)

void EntityPool::Init()
{
	// init the pools
	mEntityPool.resize(mEntityToClone.size());
	list<Entity*>::iterator it;
	vector<Pool<Entity> >::iterator poolIt = mEntityPool.begin();
	for (it = mEntityToClone.begin(); it != mEntityToClone.end(); ++poolIt)
	{
		poolIt->Init(mPoolSize, false);

		vector<Entity*>::iterator entityIt;
		for (entityIt = poolIt->mPool.begin(); entityIt != poolIt->mPool.end(); ++entityIt)
		{
			*entityIt = (*it)->Clone();
			(*entityIt)->SetParent(this);
			(*entityIt)->Init();

			// disable
			EnableMsg enableMsg(false);
			(*entityIt)->HandleMsg(&enableMsg);
		}

		// free the temporary projectile loaded from metadata
		delete *it;
		it = mEntityToClone.erase(it);
	}

	Super::Init();
}

void EntityPool::Deinit()
{
	Super::Deinit();

	vector<Pool<Entity> >::iterator poolIt;
	for (poolIt = mEntityPool.begin(); poolIt != mEntityPool.end(); ++poolIt)
	{
		vector<Entity*>::iterator it;
		for (it = poolIt->mPool.begin(); it != poolIt->mPool.end(); ++it)
		{
			(*it)->Deinit();
		}
	}
}

void EntityPool::Update()
{
	Super::Update();

	vector<Pool<Entity> >::iterator poolIt;
	for (poolIt = mEntityPool.begin(); poolIt != mEntityPool.end(); ++poolIt)
	{
		vector<Entity*>::iterator it;
		for (it = poolIt->mPool.begin(); it != poolIt->mPool.end(); ++it)
		{
			(*it)->Update();
		}
	}
}

MESSAGE_BEGIN(EntityPool)
	MESSAGE_PASS(ReturnToPool)
	MESSAGE_PASS(PawnKilled)

	// foward any unhandled messages to props
	default:
		{
			vector<Pool<Entity> >::iterator poolIt;
			for (poolIt = mEntityPool.begin(); poolIt != mEntityPool.end(); ++poolIt)
			{
				vector<Entity*>::iterator it;
				for (it = poolIt->mPool.begin(); it != poolIt->mPool.end(); ++it)
				{
					(*it)->HandleMsg(msg);
				}
			}
		}
		break;

MESSAGE_SUPER_END()

bool EntityPool::ReturnToPool(ReturnToPoolMsg* msg)
{
	vector<Pool<Entity> >::iterator poolIt;
	for (poolIt = mEntityPool.begin(); poolIt != mEntityPool.end(); ++poolIt)
	{
		poolIt->ReturnToPool(msg->mEntity);
	}
	return true;
}

bool EntityPool::PawnKilled(PawnKilledMsg* msg)
{
	// players dont spawn goodies!
	//if (msg->mKilled->GetController()->GetType() == PlayerController::Type)
	//	return true;

	bool spawnEntity = Math::Rand(0.f, 1.f) <= mSpawnChance;
	if (!spawnEntity)
		return true;

	int rand = ((int)Math::Rand(0, mEntityPool.size() * 1000)) % mEntityPool.size();
	Pool<Entity>& entityPool = mEntityPool[rand];
	Entity* spawnedEntity = entityPool.Create();
	if (!spawnedEntity)
		return true;

	// notify entity what pool it spawned from
	CreateFromPoolMsg createFromPoolMsg(&entityPool);
	spawnedEntity->HandleMsg(&createFromPoolMsg);

	// do we need a special "spawn from pool" message so we know were to return when disabled?
	EnableMsg enableMsg(true);
	spawnedEntity->HandleMsg(&enableMsg);

	SetTransformMsg setTransformMsg;
	setTransformMsg.mTransform = msg->mKilled->GetTransform();
	spawnedEntity->HandleMsg(&setTransformMsg);


	return true;
}