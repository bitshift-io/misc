#ifndef _BACKGROUNDMODEL_H_
#define _BACKGROUNDMODEL_H_

#include "Entity/Model.h"

//
// special background model which moves based on 
// average pawn velocity
//
class BackgroundModel : public Model
{
public:

	typedef Model Super;
	USES_METADATA
	CLASSNAME

	BackgroundModel();

	virtual void		Init();
	virtual void		Deinit();
	virtual void		Update();

	virtual bool		HandleMsg(Message* msg);
	virtual bool		SetPerformance(SetPerformanceMsg* msg);

protected:

	float				mVelocityScale;
	Vector4				mMaxMovement;
};


#endif