#ifndef _TEAMTAG_H_
#define _TEAMTAG_H_

#include "Entity/Model.h"

class EnableTeamTagMsg : public Message
{
public:

	TYPE(160)

	EnableTeamTagMsg(bool bEnable) :
		mEnable(bEnable)
	{
	}

	bool	mEnable;
};

class NotifyTeamChangeMsg : public Message
{
public:

	TYPE(161)

	Team*	mTeam;
};

class TeamTag : public Model
{
public:

	typedef Model Super;
	USES_METADATA
	CLASSNAME

	virtual void	Init();
	virtual void	Update();
	virtual bool	HandleMsg(Message* msg);
	virtual bool	EnableTeamTag(EnableTeamTagMsg* msg);
	virtual bool	Enable(EnableMsg* msg);
	virtual bool	NotifyTeamChange(NotifyTeamChangeMsg* msg);

protected:

	bool			mEnabled;
};

#endif