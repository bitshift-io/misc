#ifndef _ALIGNTOCAMERA_H_
#define _ALIGNTOCAMERA_H_

//
// aligns one of the parents scene nodes to the camera
//
class AlignToCamera : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	AlignToCamera();

	Scene* GetParentScene();

	virtual void Init();
	virtual void Update();

protected:
	
	SceneNode*	mNode;
	string		mNodeName;
};

#endif