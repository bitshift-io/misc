#include "StdAfx.h"
#include "MsgForceFeedback.h"

CLASS_METADATA_BEGIN(MsgForceFeedback)
	CLASS_VARIABLE(MsgForceFeedback, ForceFeedbackEffect*, mForceFeedbackEffect)
	CLASS_VARIABLE(MsgForceFeedback, string, mMessageName)
CLASS_METADATA_END(MsgForceFeedback)

MsgForceFeedback::MsgForceFeedback()
{
}

void MsgForceFeedback::Init()
{
	mMessageType = gMessageMgr.GetType(mMessageName.c_str());
}

void MsgForceFeedback::Deinit()
{
}

bool MsgForceFeedback::HandleMsg(Message* msg)
{
	if (msg->GetType() == mMessageType)
	{
		if (mForceFeedbackEffect)
		{
			if (msg->GetType() == ShakeCameraMsg::Type)
			{
				ShakeCameraMsg* shake = (ShakeCameraMsg*)msg;
				mForceFeedbackEffect->Play(shake->mShakeStrength, shake->mShakeTime);
			}
			else
			{
				mForceFeedbackEffect->Play();
			}
		}

		// update 1 frame.. does this fix the initial 1 frame randomness were getting?
		//Update();
	}

	return Super::HandleMsg(msg);
}

void MsgForceFeedback::Update()
{
	Super::Update();

	CController* controller = ((Pawn*)GetTopMostParent())->GetController();
	if (mForceFeedbackEffect && controller)
	{
		mForceFeedbackEffect->SetJoystick(controller->GetJoystick());
		mForceFeedbackEffect->Update();
	}
}
