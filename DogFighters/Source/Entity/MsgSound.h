#ifndef _MSGSOUND_H_
#define _MSGSOUND_H_

class SoundPool;

//
// plays a sound on the parent when a certain message is received
//
class MsgSound : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	MsgSound();

	virtual void		Init();
	virtual void		Deinit();
	virtual bool		HandleMsg(Message* msg);
	virtual bool		SetPause(SetPauseMsg* msg);
	virtual bool		VolumeChange(VolumeChangeMsg* msg);
	virtual void		Update();

protected:

	string				mMessageName;
	int					mMessageType;
	SoundPool*			mSound;
};


#endif