#ifndef _HUD_H_
#define _HUD_H_

#include "StdAfx.h"
//#include "Flash/Flash.h"

//
// game specific hud, here we are
//
class HUD : public FlashCallback
{
public:

	void Init();
	void Deinit();
	void Update();
	void Render();

	void Reset();

	void NotifyWave(int wave);
	void InsertFadingText(const Vector4& pos, const Vector4& colour, const char* text);
	void Message(const char* icon, const char* text);

	// converts world pos to screen space coords
	Vector4 WorldToScreen(const Vector4& pos);

	void ReloadResource();

	void ClearScoreBoard();
	void DisplayScoreBoard(const char* gameMode, const char* gameMessage);
	void InsertPlayerScore(const char* name, int score, const char* stats1, const Vector4& colour);
	void CloseScoreBoard();
	
protected:

	void					FsCommand(Flash* flash, const char* command, const char* arg);

	Flash	mHUD;
	Flash	mHUD2;
	Flash	mHUDMessage;

	//bool	mRenderHud2;
	bool	mRenderHud;
	bool	mRenderHUDMessage;
};

#endif
