#include "StdAfx.h"
#include "RenderMgr.h"

ENUM_METADATA_BEGIN(RenderFlags)
	ENUM_VALUE(RF_Default)
	ENUM_VALUE(RF_NoCastShadow)
	ENUM_VALUE(RF_NoReceiveShadow)

	ENUM_VALUE(RF_Particle)
	ENUM_VALUE(RF_NoBloom)
ENUM_METADATA_END(RenderFlags)

void RenderMgr::Init(Environment* environment)
{
	mEnvironment = environment;

	ScriptClass* renderOptions = gGame.mProfile->GetScriptClass("Render");

	mMainRenderer = gGame.mRenderFactory->CreateRenderer();
	mMainRenderer->SetCamera(mEnvironment->mCameraMgr->GetCamera());
}

void RenderMgr::Deinit()
{
	gGame.mRenderFactory->ReleaseRenderer(&mMainRenderer);	
}

void RenderMgr::SetupRenderTexture(Material* material)
{
	return;
}

void RenderMgr::InsertScene(Scene* scene, int flags)
{
	mMainRenderer->InsertScene(scene);
}

void RenderMgr::SetupRenderTexture(SceneNode* node)
{
}

void RenderMgr::SetupOccluder(SceneNode* node)
{
}

void RenderMgr::RemoveScene(Scene* scene)
{
	mMainRenderer->RemoveScene(scene);
}

void RenderMgr::Update()
{
	float time = Time::GetSeconds();
	mMainRenderer->SetTime(time);
}

void RenderMgr::Render(Device* device)
{
	mEnvironment->mEntityMgr->Render(gGame.mDevice, mEnvironment->mCameraMgr->GetCamera(), 0);

	// now render the scene
	BEGINDEBUGEVENT(device, "main scene", Vector4(0.f, 1.f, 0.f, 1.f));
	mMainRenderer->Render(device);
	mEnvironment->mEntityMgr->Render(gGame.mDevice, mEnvironment->mCameraMgr->GetCamera(), 1);
	mEnvironment->mEntityMgr->Render(gGame.mDevice, mEnvironment->mCameraMgr->GetCamera(), 2);
	gGame.GetGameModeMgr()->Render();
	ENDDEBUGEVENT(device);
}
