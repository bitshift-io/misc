#ifndef _RENDERMGR_H_
#define _RENDERMGR_H_

class Environment;

enum RenderFlags
{
	RF_Default = 0,
	RF_NoCastShadow = 1 << 0,
	RF_NoReceiveShadow = 1 << 1,
	RF_Particle = 1 << 2,
	RF_NoBloom = 1 << 3,
};

//
// handles putting models in to the appripriate renderer, we should have another class which the entity uses, and passes in here
// would be nice to be bale to load some stuff form here from MD
//
class RenderMgr
{
public:

	void Init(Environment* environment);
	void Deinit();

	void InsertScene(Scene* scene, int flags = RF_Default);
	void RemoveScene(Scene* scene);
	void Render(Device* device);

	Renderer* GetMainRenderer()		{ return (Renderer*)mMainRenderer; }

	void		Update();

	// look for render textures and apply the appropriate one
	void		SetupRenderTexture(Material* material);
	void		SetupRenderTexture(SceneNode* node);

	// look for occluders and add them to the occluder cullers
	void		SetupOccluder(SceneNode* node);

protected:

	Environment*	mEnvironment;
	Renderer*		mMainRenderer;
};

#endif
