#include "StdAfx.h"
#include "HUD.h"

void HUD::Init()
{
	mHUD.Init(gGame.mRenderFactory, gGame.mDevice, gGame.mWindow);
	mHUD.SetCallback(this);
	mHUD.Load("Hud"); //mFlashFile.c_str());
	mHUD.SetBackgroundColour(Vector4(0.f, 0.f, 0.f, 0.f));
/*
	mHUD2.Init(gGame.mRenderFactory, gGame.mDevice, gGame.mWindow);
	mHUD2.SetCallback(this);
	mHUD2.Load("Hud2"); //mFlashFile.c_str());
	mHUD2.SetBackgroundColour(Vector4(0.f, 0.f, 0.f, 0.f));
*/
	mHUDMessage.Init(gGame.mRenderFactory, gGame.mDevice, gGame.mWindow);
	mHUDMessage.SetCallback(this);
	mHUDMessage.Load("HUDMessage"); //mFlashFile.c_str());
	mHUDMessage.SetBackgroundColour(Vector4(0.f, 0.f, 0.f, 0.f));

	//mRenderHud2 = false;
	mRenderHud = false;
	mRenderHUDMessage = false;
}

void HUD::Deinit()
{
	mHUD.Deinit();
	//mHUD2.Deinit();
	mHUDMessage.Deinit();
}

void HUD::Update()
{
	//InsertFadingText(Vector4(0.f, 0.f, 0.f, 0.f), Vector4(0.f, 1.f, 0.f, 1.f), "test");
	mHUD.CallMethod("Update");
	mHUD.Update(gGame.mGameUpdate.GetDeltaTime(), gGame.mInput);
	//mHUD2.Update(gGame.mGameUpdate.GetDeltaTime(), gGame.mInput);
	mHUDMessage.Update(gGame.mGameUpdate.GetDeltaTime(), gGame.mInput);
}

void HUD::Render()
{
	if (mRenderHud)
		mHUD.Render();

	//if (mRenderHud2)
	//	mHUD2.Render();

	if (mRenderHUDMessage)
		mHUDMessage.Render();
}

void HUD::NotifyWave(int wave)
{
	mHUD.CallMethod("NotifyWave", "%i", wave);	
}

void HUD::Reset()
{
	mHUD.GotoScene("HUD");
}

void HUD::InsertFadingText(const Vector4& pos, const Vector4& colour, const char* text)
{/*
	unsigned int flashColour = (unsigned int(colour.x * 255.f) << 16) | (unsigned int(colour.y * 255.f) << 8) | unsigned int(colour.z * 255.f);
	mHUD.CallMethod("InsertFadingText", "%f%f%i%s", (pos.x * 0.5 + 0.5) * mHUD.GetWidth(), (-pos.y * 0.5 + 0.5) * mHUD.GetHeight(), flashColour, text);*/
}

void HUD::Message(const char* icon, const char* text)
{
	// some flash bug
	mHUDMessage.CallMethod("Message", "%s%s", icon, text);
	mHUDMessage.CallMethod("Message", "%s%s", icon, text);
}

void HUD::FsCommand(Flash* flash, const char* command, const char* arg)
{
	if (strcmpi("ShowMainMenu", command) == 0)
	{
		// trick flash menu to go back to main menu
		InGame* gameState = (InGame*)gGame.GetCurrentState();
		gameState->SetCentreCursor(false);
		FlashMenu* menu = (FlashMenu*)gGame.GetStateByName(gameState->GetNextStateName());
		gGame.SetState(menu);
		menu->ClearPreviousState();
		gGame.SetState(menu);		
	}
	else if (strcmpi("CloseScoreBoard", command) == 0)
	{
		CloseScoreBoard();
	}
	else if (strcmpi(command, "enable_render") == 0)
	{
		if (flash == &mHUDMessage)
			mRenderHUDMessage = true;
		else
			mRenderHud = true;
	}
	else if (strcmpi(command, "disable_render") == 0)
	{
		if (flash == &mHUDMessage)
			mRenderHUDMessage = false;
		else
			mRenderHud = false;
	}
	else
	{
		gMessageMgr.SendStringMessage(command);
	}
}

Vector4 HUD::WorldToScreen(const Vector4& pos)
{
	Camera* camera = gGame.GetEnvironment()->mCameraMgr->GetCamera();
	Matrix4 viewProjection = camera->GetView() * camera->GetProjection();
	Vector4 screenPos = viewProjection * pos;
	screenPos /= screenPos.w;
	return screenPos;
}

void HUD::ReloadResource()
{
	mHUD.ReloadResource();
	//mHUD2.ReloadResource();
	mHUDMessage.ReloadResource();
}

void HUD::ClearScoreBoard()
{
	mHUD.CallMethod("ClearScoreBoard");
}

void HUD::DisplayScoreBoard(const char* gameMode, const char* gameMessage)
{
	InGame* gameState = (InGame*)gGame.GetCurrentState();
	gameState->SetCentreCursor(false);
	//mHUD.GotoScene("ScoreBoard");
	mHUD.CallMethod("DisplayScoreBoard", "%s%s", gameMode, gameMessage);
}

void HUD::InsertPlayerScore(const char* name, int score, const char* stats1, const Vector4& colour)
{
	//unsigned int flashColour = (unsigned int(colour.x * 255.f) << 16) | (unsigned int(colour.y * 255.f) << 8) | unsigned int(colour.z * 255.f);
	mHUD.CallMethod("InsertPlayerScore", "%s%i%s", name, score, stats1);
}

void HUD::CloseScoreBoard()
{
	InGame* gameState = (InGame*)gGame.GetCurrentState();
	gameState->SetCentreCursor(true);
	mHUD.GotoScene("HUD");
}