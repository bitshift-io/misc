#ifndef _PLAYERCONTROLLER_H_
#define _PLAYERCONTROLLER_H_

#include "Controller.h"

class PlayerController : public CController
{
public:

	USES_METADATA
	TYPE(CT_Player)
	typedef CController Super;

	virtual void		Init();
	virtual void		Deinit();
	virtual void		Update();

	virtual HUD*		GetHUD()							{ return mHUD; }

	virtual float		GetActionState(ControllerActionType actionType);
	virtual float		GetActionStateNormalized(ControllerActionType actionType);

	virtual Joystick*	GetJoystick()						{ return mJoystick; }

protected:

	InputControllerAction*	GetInputControllerAction(ControllerActionType actionType);

	HUD*						mHUD;
	PlaybackStream*				mPlaybackStream;
	Joystick*					mJoystick;
};


#endif


