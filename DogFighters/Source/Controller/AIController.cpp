#include "StdAfx.h"
#include "AIController.h"

VectorParser<AIControllerState*> aiControllerStateParser("AIControllerState*");

CLASS_METADATA_BEGIN(AIController)
	CLASS_VARIABLE(AIController, vector<AIControllerState*>, mState)
CLASS_EXTENDS_METADATA_END(AIController, CController)

void AIController::Init()
{
	Super::Init();

	mView = Vector4(0.f, 0.f, 1.f);

	for (int i = 0; i < CAT_Max; ++i)
	{
		ControllerAction* action = new ControllerAction;
		action->mCommand = ControllerActionType(i);
		mAction.push_back(action);
	}	

	// set up states
	// init states
	vector<AIControllerState*>::iterator it;
	for (it = mState.begin(); it != mState.end(); ++it)
	{
		(*it)->Init(this);
	}

	// push on the first state
	if (mState.size() > 0)
		SetState(GetState(0));
}

void AIController::Deinit()
{
	// clean up actions
	while (mAction.size())
	{
		delete mAction.front();
		mAction.pop_front();
	}

	Super::Deinit();
}

void AIController::Update()
{
	// dont update if dead
	if (!mPawn || mPawn->GetHealthPercent() <= 0.f)
		return;

	// clear all states
	list<ControllerAction*>::iterator it;
	for (it = mAction.begin(); it != mAction.end(); ++it)
	{
		if ((*it)->mCommand == CAT_ViewX
			|| (*it)->mCommand == CAT_ViewY)
			continue;

		(*it)->mState = 0.f;
	}

	if (GetCurrentState())
		GetCurrentState()->StateUpdate();

	Super::Update();
}

void AIController::SetStateByName(const string& name)
{
	vector<AIControllerState*>::iterator it;
	for (it = mState.begin(); it != mState.end(); ++it)
	{
		if (strcmpi((*it)->GetName().c_str(), name.c_str()) == 0)
		{
			SetState(*it);
			return;
		}
	}
}