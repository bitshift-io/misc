#ifndef _AICONTROLLER_H_
#define _AICONTROLLER_H_

#include "Util/StateMachine.h"
#include "Controller.h"
#include "AI/AIControllerState.h"

class AIController : public CController, public StateMachine<AIControllerState>
{
public:

	USES_METADATA
	TYPE(CT_AI)
	typedef CController Super;

	virtual void		Init();
	virtual void		Deinit();

	virtual void		Update();
	virtual Vector4		GetView()				{ return mView; }

	void				SetStateByName(const string& name);

	Pawn*				GetPawn()				{ return mPawn; }

//protected:

	Vector4				mView;
};

#endif
