#include "StdAfx.h"
#include "PlayerController.h"

CLASS_METADATA_BEGIN(PlayerController)
	CLASS_VARIABLE(PlayerController, list<ControllerAction*>, mAction)
CLASS_EXTENDS_METADATA_END(PlayerController, CController)

void PlayerController::Init()
{
	mJoystick = 0;

	list<ControllerAction*>::iterator it;
	for (it = mAction.begin(); it != mAction.end(); ++it)
	{
		InputControllerAction* action = (InputControllerAction*)*it;

		if (gGame.mInput->GetJoystickIndex(action->mInput) >= 0)
		{
			mJoystick = gGame.mInput->GetJoystick(gGame.mInput->GetJoystickIndex(action->mInput));
		}

		if (gGame.mInput->GetJoystickIndex(action->mSecondInput) >= 0)
		{
			mJoystick = gGame.mInput->GetJoystick(gGame.mInput->GetJoystickIndex(action->mSecondInput));
		}
	}

	Super::Init();

	gGame.GetEnvironment()->mCameraMgr->HandleMsg(&AttachToEntityMsg(mPawn));
	
	// load the hud
	mHUD = gGame.GetGameModeMgr()->GetGameMode()->CreateHUD(this);

	// get a playback stream
	mPlaybackStream = gGame.mPlayback.GetStream();
}

void PlayerController::Deinit()
{
	Super::Deinit();

	gGame.GetGameModeMgr()->GetGameMode()->ReleaseHUD(&mHUD);

	// free stream
	gGame.mPlayback.ReleaseStream(&mPlaybackStream);
}

InputControllerAction* PlayerController::GetInputControllerAction(ControllerActionType actionType)
{
	return (InputControllerAction*)GetControllerAction(actionType);
}

float PlayerController::GetActionState(ControllerActionType actionType)
{
	ControllerAction* action = GetControllerAction(actionType);
	if (!action)
		return 0.f;

	return action->mState;
}

float PlayerController::GetActionStateNormalized(ControllerActionType actionType)
{
	ControllerAction* action = GetControllerAction(actionType);
	if (!action)
		return 0.f;

	return action->mStateNormalized;
}

void PlayerController::Update()
{
	list<ControllerAction*>::iterator it;
	for (it = mAction.begin(); it != mAction.end(); ++it)
	{
		InputControllerAction* action = (InputControllerAction*)*it;
		if (action->mOnRelease)
		{
			bool secondState = action->mSecondInput == -1 ? false : gGame.mInput->WasPressed(action->mSecondInput) * action->mSecondAxis;
			bool finalState = gGame.mInput->WasPressed(action->mInput) * action->mAxis || secondState;
			action->mState = finalState ? 1.f : 0.f;

			*mPlaybackStream & action->mState;

			if (action->mState && action->mScriptCommand.length())
			{
				gMessageMgr.SendStringMessage(action->mScriptCommand);
			}
		}
		else
		{
			float secondState = action->mSecondInput == -1 ? 0.f : gGame.mInput->GetState(action->mSecondInput) * action->mSecondAxis;
			if (fabs(secondState) < action->mSecondDeadZone)
				secondState = 0.f;

			float secondStateNormalized = Math::Max(secondState, 0.f);
			secondState *= action->mSecondAxisSensitivity;

			float state = action->mInput == -1 ? 0.f : gGame.mInput->GetState(action->mInput) * action->mAxis;
			if (fabs(state) < action->mDeadZone)
				state = 0.f;

			float stateNormalized = Math::Max(state, 0.f);
			if (state && action->mInput == MOUSE_Wheel)
				int nothing = 0;

			if (state != 0.f)
				int nothing = 0;

			state *= action->mAxisSensitivity;
			state = state + secondState;
			stateNormalized = stateNormalized + secondStateNormalized;

			switch (action->mCommand)
			{
			case CAT_ViewX:
				{
					action->mState += state;
					action->mStateNormalized = stateNormalized;
				}
				break;

			case CAT_ViewY:
				{
					action->mState += state;
					action->mState = Math::Clamp(Math::DtoR(-85.f), action->mState, Math::DtoR(85.f));
					action->mStateNormalized = stateNormalized;
				}
				break;

			default:
				action->mState = state;
				action->mStateNormalized = stateNormalized;
				break;
			}
			
			*mPlaybackStream & action->mState;
		}
	}
/*
	gGame.mSoundFactory->SetListenerPosition(gGame.GetEnvironment()->mCameraMgr->GetCamera()->GetPosition());
	gGame.mSoundFactory->SetListenerOrientation(GetView(), Vector4(0.f, 0.f, 0.f));
*/
	//if (mHUD)
	//	mHUD->Update(mPawn);

	UpdateViewFromState();

#ifdef NETWORK
	// send movement message to server
	if (mPawn)
	{
		ControllerSyncMsg sync;
		sync.mPosition = mPawn->GetTransform().GetTranslation();
		sync.mUseEntity = mPawn->GetUseEntity();
		sync.mView = GetView();
		//sync.mTimeStamp = gTime.GetSeconds();
		
		for (it = mAction.begin(); it != mAction.end(); ++it)
		{
			InputControllerAction* action = (InputControllerAction*)*it;

			switch (action->mCommand)
			{
			//case CAT_ViewX:
			//case CAT_ViewY:
			case CAT_MoveForward:
			case CAT_MoveBackward:
			case CAT_StrafeLeft:
			case CAT_StrafeRight:

			case CAT_Jump:
			case CAT_Crouch:
			case CAT_Use:
			case CAT_Fire:	
			case CAT_AltFire:	
				{
					ControllerSyncMsg::CommandState cmdState;
					cmdState.mCommand = (*it)->mCommand;
					cmdState.mState = (*it)->mState;
					sync.mCommandState.push_back(cmdState);
				}
			}
		}

		gGame.GetEnvironment()->mNetwork->SendMessage(&sync, this);
	}
#endif

	// fab testing
	//((Win32Joystick*)mJoystick)->xboxIdx = 0;
	//mJoystick->ApplyForce(Vector4(0.75f, 0.7f, 0.f, 0.f), 100);
}
