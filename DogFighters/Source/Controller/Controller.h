#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"
#include "Math/Vector.h"
#include "Input/Input.h"
#include "Core/Entity.h"

class Pawn;
class HUD;
class ControllerMoveMsg;
class ControllerSyncMsg;
class PlaybackStream;

enum ControllerType
{
	CT_AI,
	CT_Player,
	CT_Network,
};

enum ControllerActionType
{
	// standard shooter controls
	CAT_None,
	CAT_ViewX,
	CAT_ViewY,

	CAT_MoveForward,
	CAT_MoveBackward,
	CAT_StrafeLeft,
	CAT_StrafeRight,

	CAT_Jump,
	CAT_Crouch,
	CAT_Use,
	CAT_Fire,
	CAT_AltFire,
	CAT_NextCameraView,

	CAT_SelectNext,
	CAT_SelectLast,

	// arcade controls
	CAT_Left,
	CAT_Right,
	CAT_Up,
	CAT_Down,

	CAT_Max,
};

class ControllerAction
{
public:

	USES_METADATA

	ControllerAction() :
		mCommand(CAT_None),
		mState(0.f),
		mStateNormalized(0.f)
	{
	}

	ControllerActionType			mCommand;
	float							mState;				// sensitivity may make this exceed -1 or 1
	float							mStateNormalized;	// clamped from -1 to 1
};

class InputControllerAction : public ControllerAction
{
public:

	USES_METADATA

	InputControllerAction() : ControllerAction(),
		mOnRelease(false),
		mSecondInput(InputControl(-1)),
		mAxisSensitivity(1.f),
		mSecondAxisSensitivity(1.f),
		mAxis(1.f),
		mSecondAxis(1.f),
		mDeadZone(0.f),
		mSecondDeadZone(0.f),
		mInput(InputControl(-1))
	{
	}

	InputControl	mInput;
	float			mAxisSensitivity;
	float			mAxis;
	float			mDeadZone;

	InputControl	mSecondInput;
	float			mSecondAxisSensitivity;
	float			mSecondAxis;
	float			mSecondDeadZone;

	bool			mOnRelease;
	string			mScriptCommand;
};

class Team;

class Stats
{
public:

	Stats()
	{
		Reset();
	}

	void Reset()
	{
		mPickups = 0;
		mHits = 0;
		mDeaths = 0;
		mShotsFired = 0;
		mKills = 0;
		mTime = 0.f;
		mScore = 0;
	}

	int					mPickups;
	int					mHits;
	int					mDeaths;
	int					mShotsFired;
	int					mKills;
	float				mTime;			// used for rabbit game mode
	int					mScore;
};

//
// An intermediary between human and an actor, or AI and an actor
// the actor knows how to move, jump roll dive, the controller handles input
// from the keyboard, network or AI and passes it to the actor
// This means the actor needs to know nothing of AI, humans or networking
// its the brain while the actor is the body
//
// controller may even have multiple pawns (eg strategy game)
//
// - All network syncing is done via this class
// - All client info is available via this class
//
class CController : public MessageReceiver
{
public:

	USES_METADATA

	virtual int			GetType() const									{ return -1; }

	virtual void		Init();
	virtual void		Deinit();
	virtual void		Update()										{}

	virtual float		GetActionStateNormalized(ControllerActionType actionType);
	virtual float		GetActionState(ControllerActionType actionType);
	virtual Vector4		GetView();

	virtual const string&	GetPawnName() const							{ return mPawnName; }
	const char*				GetName() const								{ return mName.c_str(); }
	void					SetName(const char* name)					{ mName = name; }

	virtual bool		HandleMsg(Message* msg);
	virtual bool		SetPause(SetPauseMsg* msg);

#ifdef NETWORK
	virtual bool		ControllerSync(ControllerSyncMsg* msg);
#endif

	virtual HUD*		GetHUD()										{ return 0; }
	virtual Pawn*		GetPawn()										{ return mPawn; }
	void				SetPawn(Pawn* pawn);

	ControllerAction*	GetControllerAction(ControllerActionType actionType);

	Team*				GetTeam()									{ return mTeam; }
	void				SetTeam(Team* team);

	void				UpdateViewFromState();

	Stats*				GetStats()									{ return &mStats; }

	virtual Joystick*	GetJoystick()								{ return 0; }

protected:

	Vector4						mView;
	Team*						mTeam;
	string						mName;
	string						mPawnName;
	string						mModelName;
	Pawn*						mPawn;
	list<ControllerAction*>		mAction;
	Vector4						mColour;
	Stats						mStats;
};

#endif


