#include "StdAfx.h"
#include "Controller.h"

ListParser<ControllerAction*> controllerActionParser("ControllerAction*");

ENUM_METADATA_BEGIN(InputControl)
	ENUM_VALUE(KEY_Space)

	ENUM_VALUE(KEY_KP0)
	ENUM_VALUE(KEY_KP1)
	ENUM_VALUE(KEY_KP2)
	ENUM_VALUE(KEY_KP3)
	ENUM_VALUE(KEY_KP4)
	ENUM_VALUE(KEY_KP5)
	ENUM_VALUE(KEY_KP6)
	ENUM_VALUE(KEY_KP7)
	ENUM_VALUE(KEY_KP8)
	ENUM_VALUE(KEY_KP9)

	ENUM_VALUE(KEY_0)
	ENUM_VALUE(KEY_1)
	ENUM_VALUE(KEY_2)
	ENUM_VALUE(KEY_3)
	ENUM_VALUE(KEY_4)
	ENUM_VALUE(KEY_5)
	ENUM_VALUE(KEY_6)
	ENUM_VALUE(KEY_7)
	ENUM_VALUE(KEY_8)
	ENUM_VALUE(KEY_9)

	ENUM_VALUE(KEY_a)
	ENUM_VALUE(KEY_b)
	ENUM_VALUE(KEY_c)
	ENUM_VALUE(KEY_d)
	ENUM_VALUE(KEY_e)
	ENUM_VALUE(KEY_f)
	ENUM_VALUE(KEY_g)
	ENUM_VALUE(KEY_h)
	ENUM_VALUE(KEY_i)
	ENUM_VALUE(KEY_j)
	ENUM_VALUE(KEY_k)
	ENUM_VALUE(KEY_l)
	ENUM_VALUE(KEY_m)
	ENUM_VALUE(KEY_n)
	ENUM_VALUE(KEY_o)
	ENUM_VALUE(KEY_p)
	ENUM_VALUE(KEY_q)
	ENUM_VALUE(KEY_r)
	ENUM_VALUE(KEY_s)
	ENUM_VALUE(KEY_t)
	ENUM_VALUE(KEY_u)
	ENUM_VALUE(KEY_v)
	ENUM_VALUE(KEY_w)
	ENUM_VALUE(KEY_x)
	ENUM_VALUE(KEY_y)
	ENUM_VALUE(KEY_z)

	ENUM_VALUE(KEY_F1)
	ENUM_VALUE(KEY_F2)
	ENUM_VALUE(KEY_F3)
	ENUM_VALUE(KEY_F4)
	ENUM_VALUE(KEY_F5)
	ENUM_VALUE(KEY_F6)
	ENUM_VALUE(KEY_F7)
	ENUM_VALUE(KEY_F8)
	ENUM_VALUE(KEY_F9)
	ENUM_VALUE(KEY_F10)
	ENUM_VALUE(KEY_F11)
	ENUM_VALUE(KEY_F12)

	ENUM_VALUE(KEY_Esc)
	ENUM_VALUE(KEY_Enter)
	ENUM_VALUE(KEY_BackSpace)
	ENUM_VALUE(KEY_Tab)
	ENUM_VALUE(KEY_Pause)
	ENUM_VALUE(KEY_ScrollLock)
	ENUM_VALUE(KEY_PrintScreen)
	ENUM_VALUE(KEY_Delete)

	ENUM_VALUE(KEY_LShift)
	ENUM_VALUE(KEY_RShift)
	ENUM_VALUE(KEY_LControl)
	ENUM_VALUE(KEY_RControl)
	ENUM_VALUE(KEY_CapsLock)
	ENUM_VALUE(KEY_LAlt)
	ENUM_VALUE(KEY_RAlt)

	ENUM_VALUE(KEY_Home)
	ENUM_VALUE(KEY_Left)
	ENUM_VALUE(KEY_Up)
	ENUM_VALUE(KEY_Down)
	ENUM_VALUE(KEY_Right)
	ENUM_VALUE(KEY_PageUp)
	ENUM_VALUE(KEY_PageDown)
	ENUM_VALUE(KEY_End)
	ENUM_VALUE(KEY_Begin)

	ENUM_VALUE(MOUSE_Left)
	ENUM_VALUE(MOUSE_Right)
	ENUM_VALUE(MOUSE_Middle)

	ENUM_VALUE(MOUSE_AxisX)
	ENUM_VALUE(MOUSE_AxisY)
	ENUM_VALUE(MOUSE_Wheel)

	ENUM_VALUE(JOYSTICK0_AxisX)
	ENUM_VALUE(JOYSTICK0_AxisY)
	ENUM_VALUE(JOYSTICK0_AxisZ)

	ENUM_VALUE(JOYSTICK0_Axis0)
	ENUM_VALUE(JOYSTICK0_Axis1)
	ENUM_VALUE(JOYSTICK0_Axis2)
	ENUM_VALUE(JOYSTICK0_Axis3)
	ENUM_VALUE(JOYSTICK0_Axis4)

	ENUM_VALUE(JOYSTICK0_Button1)
	ENUM_VALUE(JOYSTICK0_Button2)
	ENUM_VALUE(JOYSTICK0_Button3)
	ENUM_VALUE(JOYSTICK0_Button4)

	ENUM_VALUE(JOYSTICK0_Button5)
	ENUM_VALUE(JOYSTICK0_Button6)
	ENUM_VALUE(JOYSTICK0_Button7)
	ENUM_VALUE(JOYSTICK0_Button8)
	ENUM_VALUE(JOYSTICK0_Button9)
	ENUM_VALUE(JOYSTICK0_Button10)
	ENUM_VALUE(JOYSTICK0_Button11)
	ENUM_VALUE(JOYSTICK0_Button12)

	ENUM_VALUE(JOYSTICK1_AxisX)
	ENUM_VALUE(JOYSTICK1_AxisY)
	ENUM_VALUE(JOYSTICK1_AxisZ)

	ENUM_VALUE(JOYSTICK1_Axis0)
	ENUM_VALUE(JOYSTICK1_Axis1)
	ENUM_VALUE(JOYSTICK1_Axis2)
	ENUM_VALUE(JOYSTICK1_Axis3)
	ENUM_VALUE(JOYSTICK1_Axis4)

	ENUM_VALUE(JOYSTICK1_Button1)
	ENUM_VALUE(JOYSTICK1_Button2)
	ENUM_VALUE(JOYSTICK1_Button3)
	ENUM_VALUE(JOYSTICK1_Button4)

	ENUM_VALUE(JOYSTICK1_Button5)
	ENUM_VALUE(JOYSTICK1_Button6)
	ENUM_VALUE(JOYSTICK1_Button7)
	ENUM_VALUE(JOYSTICK1_Button8)
	ENUM_VALUE(JOYSTICK1_Button9)
	ENUM_VALUE(JOYSTICK1_Button10)
	ENUM_VALUE(JOYSTICK1_Button11)
	ENUM_VALUE(JOYSTICK1_Button12)

	ENUM_VALUE(JOYSTICK2_AxisX)
	ENUM_VALUE(JOYSTICK2_AxisY)
	ENUM_VALUE(JOYSTICK2_AxisZ)

	ENUM_VALUE(JOYSTICK2_Axis0)
	ENUM_VALUE(JOYSTICK2_Axis1)
	ENUM_VALUE(JOYSTICK2_Axis2)
	ENUM_VALUE(JOYSTICK2_Axis3)
	ENUM_VALUE(JOYSTICK2_Axis4)

	ENUM_VALUE(JOYSTICK2_Button1)
	ENUM_VALUE(JOYSTICK2_Button2)
	ENUM_VALUE(JOYSTICK2_Button3)
	ENUM_VALUE(JOYSTICK2_Button4)

	ENUM_VALUE(JOYSTICK2_Button5)
	ENUM_VALUE(JOYSTICK2_Button6)
	ENUM_VALUE(JOYSTICK2_Button7)
	ENUM_VALUE(JOYSTICK2_Button8)
	ENUM_VALUE(JOYSTICK2_Button9)
	ENUM_VALUE(JOYSTICK2_Button10)
	ENUM_VALUE(JOYSTICK2_Button11)
	ENUM_VALUE(JOYSTICK2_Button12)

	ENUM_VALUE(JOYSTICK3_AxisX)
	ENUM_VALUE(JOYSTICK3_AxisY)
	ENUM_VALUE(JOYSTICK3_AxisZ)

	ENUM_VALUE(JOYSTICK3_Axis0)
	ENUM_VALUE(JOYSTICK3_Axis1)
	ENUM_VALUE(JOYSTICK3_Axis2)
	ENUM_VALUE(JOYSTICK3_Axis3)
	ENUM_VALUE(JOYSTICK3_Axis4)

	ENUM_VALUE(JOYSTICK3_Button1)
	ENUM_VALUE(JOYSTICK3_Button2)
	ENUM_VALUE(JOYSTICK3_Button3)
	ENUM_VALUE(JOYSTICK3_Button4)

	ENUM_VALUE(JOYSTICK3_Button5)
	ENUM_VALUE(JOYSTICK3_Button6)
	ENUM_VALUE(JOYSTICK3_Button7)
	ENUM_VALUE(JOYSTICK3_Button8)
	ENUM_VALUE(JOYSTICK3_Button9)
	ENUM_VALUE(JOYSTICK3_Button10)
	ENUM_VALUE(JOYSTICK3_Button11)
	ENUM_VALUE(JOYSTICK3_Button12)
ENUM_METADATA_END(InputControl)

ENUM_METADATA_BEGIN(ControllerActionType)
	ENUM_VALUE(CAT_None)
	ENUM_VALUE(CAT_ViewX)
	ENUM_VALUE(CAT_ViewY)

	ENUM_VALUE(CAT_MoveForward)
	ENUM_VALUE(CAT_MoveBackward)
	ENUM_VALUE(CAT_StrafeLeft)
	ENUM_VALUE(CAT_StrafeRight)

	ENUM_VALUE(CAT_Jump)
	ENUM_VALUE(CAT_Crouch)
	ENUM_VALUE(CAT_Use)

	ENUM_VALUE(CAT_Fire)
	ENUM_VALUE(CAT_AltFire)
	ENUM_VALUE(CAT_NextCameraView)	

	ENUM_VALUE(CAT_SelectNext)
	ENUM_VALUE(CAT_SelectLast)

	ENUM_VALUE(CAT_Left)
	ENUM_VALUE(CAT_Right)
	ENUM_VALUE(CAT_Up)
	ENUM_VALUE(CAT_Down)
ENUM_METADATA_END(ControllerActionType)

CLASS_METADATA_BEGIN(ControllerAction)
	CLASS_VARIABLE(ControllerAction, ControllerActionType, mCommand)
CLASS_METADATA_END(ControllerAction)

CLASS_METADATA_BEGIN(InputControllerAction)
	CLASS_VARIABLE(InputControllerAction, InputControl, mInput)
	CLASS_VARIABLE(InputControllerAction, InputControl, mSecondInput)	
	CLASS_VARIABLE(InputControllerAction, string, mScriptCommand)
	CLASS_VARIABLE(InputControllerAction, bool, mOnRelease)
	CLASS_VARIABLE(InputControllerAction, float, mAxisSensitivity)
	CLASS_VARIABLE(InputControllerAction, float, mSecondAxisSensitivity)
	CLASS_VARIABLE(InputControllerAction, float, mAxis)
	CLASS_VARIABLE(InputControllerAction, float, mSecondAxis)
	CLASS_VARIABLE(InputControllerAction, float, mDeadZone)
	CLASS_VARIABLE(InputControllerAction, float, mSecondDeadZone)
CLASS_EXTENDS_METADATA_END(InputControllerAction, ControllerAction)

CLASS_METADATA_BEGIN(CController)
	CLASS_VARIABLE(CController, string, mName)
	CLASS_VARIABLE(CController, string, mPawnName)
	CLASS_VARIABLE(CController, string, mModelName)
	CLASS_VARIABLE(CController, Vector4, mColour)
CLASS_METADATA_END(CController)

void CController::Init()
{
	mPawn = 0;
	mTeam = 0;
	mView = Vector4(0.f, 0.f, 1.f);

	mPawn = gGame.GetGameModeMgr()->GetGameMode()->CreatePawn(this);

	SetColourMsg colour(mColour);
	mPawn->HandleMsg(&colour);

	// controllers go in to channel 1
	RegisterReceiver(MessageReceiverHandle(1));

	mStats.mPickups = 0;
	mStats.mHits = 0;
	mStats.mDeaths = 0;
	mStats.mShotsFired = 0;
}

void CController::Deinit()
{
	if (mTeam)
		mTeam->mController.remove(this);

	if (GetJoystick())
		GetJoystick()->ApplyForce(Vector4(0.f, 0.f, 0.f, 0.f), 0.f);

	gGame.GetGameModeMgr()->GetGameMode()->ReleasePawn(&mPawn);
	DeregisterReceiver();
}

void CController::UpdateViewFromState()
{
	float x = GetActionState(CAT_ViewX);
	float y = GetActionState(CAT_ViewY);

	Quaternion rotY(Vector4(0.f, 1.f, 0.f), x);
	Quaternion rotX(Vector4(1.f, 0.f, 0.f), y);
	mView = (rotY * rotX) * Vector4(0.f, 0.f, 1.f);
}

Vector4 CController::GetView()
{
	return mView;
}

ControllerAction* CController::GetControllerAction(ControllerActionType actionType)
{
	list<ControllerAction*>::iterator it;
	for (it = mAction.begin(); it != mAction.end(); ++it)
	{
		if ((*it)->mCommand == actionType)
			return (*it);
	}

	return 0;
}

float CController::GetActionState(ControllerActionType actionType)
{
	ControllerAction* action = GetControllerAction(actionType);
	return action->mState;
}

float CController::GetActionStateNormalized(ControllerActionType actionType)
{
	ControllerAction* action = GetControllerAction(actionType);
	return action->mStateNormalized;
}

void CController::SetPawn(Pawn* pawn)
{
	if (GetJoystick())
		GetJoystick()->ApplyForce(Vector4(0.f, 0.f, 0.f, 0.f), 0.f);

	pawn->SetController(this);
	mPawn = pawn;
}

MESSAGE_BEGIN(CController)
	MESSAGE_PASS(SetPause)
#ifdef NETWORK
	MESSAGE_PASS(ControllerSync)
#endif
MESSAGE_END()

#ifdef NETWORK
bool CController::ControllerSync(ControllerSyncMsg* msg)
{
	// clients view is always right!
	if (gGame.GetEnvironment()->mNetwork->IsHosting())
		mView = msg->mView;

	// all clients must move thier pawns according to what the server says
	if (mPawn && !gGame.GetEnvironment()->mNetwork->IsHosting())
	{
		Matrix4 transform = mPawn->GetTransform();
		transform.SetTranslation(msg->mPosition);
		mPawn->SetTransform(transform, true);
	}

	if (mPawn)
	{
		mPawn->UseEntity(msg->mUseEntity);
	}

	for (int i = 0; i < msg->mCommandState.size(); ++i)
	{
		ControllerAction* action = GetControllerAction(msg->mCommandState[i].mCommand);
		action->mState = msg->mCommandState[i].mState;
	}

	return true;
}
#endif

bool CController::SetPause(SetPauseMsg* msg)
{
	if (GetJoystick())
		GetJoystick()->ApplyForce(Vector4(0.f, 0.f, 0.f, 0.f), 0.f);

	return false;
}

void CController::SetTeam(Team* team)
{
	// remove from existing team
	if (mTeam)
		mTeam->mController.remove(this);

	mTeam = team;
	if (team)
		mTeam->mController.push_back(this);

	NotifyTeamChangeMsg teamChangeMsg;
	teamChangeMsg.mTeam = team;
	if (GetPawn())
		GetPawn()->HandleMsg(&teamChangeMsg);
}
