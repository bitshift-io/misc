#ifndef _SHADERFLASH_H_
#define _SHADERFLASH_H_

#include "Render/Software/SoftwareEffect.h"
#include "Render/Software/SoftwareTexture.h"

class ShaderFlashParameterData : public ParameterData
{
public:

	ShaderFlashParameterData()
	{
		layout[0].type = EPT_Matrix4;
		layout[0].ID = EPID_WorldViewProjection;
		layout[0].offset = VAR_OFFSET(worldViewProj);
		layout[0].size = sizeof(Matrix4);

		layout[1].type = EPT_Vector4;
		layout[1].ID = EPID_Unknown;
		layout[1].offset = VAR_OFFSET(colour);
		layout[1].size = sizeof(Vector4);
		layout[1].name = "colour";

		layout[2].type = EPT_Matrix4;
		layout[2].ID = EPID_Unknown;
		layout[2].offset = VAR_OFFSET(textureMatrix);
		layout[2].size = sizeof(Matrix4);
		layout[2].name = "textureMatrix";

		layout[3].type = EPT_Texture;
		layout[3].ID = EPID_Unknown;
		layout[3].offset = VAR_OFFSET(texture);
		layout[3].size = sizeof(SoftwareTexture*);
		layout[3].name = "diffuseTexture";

		layout[4].type = EPT_Unknown;
		layout[4].ID = EPID_Max;
		layout[4].offset = -1;
		layout[4].size = -1;
	}

	ParameterData* Clone() 
	{ 
		return new ShaderFlashParameterData; 
	}

	ParameterDataLayout* GetParameterDataLayout()
	{
		return layout;
	}

	Matrix4 worldViewProj;
	Vector4 colour;
	Matrix4 textureMatrix;
	SoftwareTexture* texture;

	ParameterDataLayout layout[5];
};

struct ShaderFlashVertexInput : public VertexInput
{
	Vector4 position;
};

struct ShaderFlashPixelInput : public PixelInput
{
	Vector4 uv;
};

class VertexShaderFlash : public SoftwareVertexShaderTemplate<ShaderFlashVertexInput>
{
public:

	VertexShaderFlash()
	{
		layout[0].dataType = VF_Position;
		layout[0].offset = 0;
		layout[0].size = sizeof(Vector4);

		layout[1].dataType = VF_Unknown;
		layout[1].offset = -1;
		layout[1].size = -1;
	}

	virtual void Apply(const ParameterData* parameter, const VertexInput* input, PixelInput* output)
	{
		ShaderFlashVertexInput* in = (ShaderFlashVertexInput*)input;
		ShaderFlashPixelInput* out = (ShaderFlashPixelInput*)output;
		ShaderFlashParameterData* param = (ShaderFlashParameterData*)parameter;

		in->position.w = 1.0f;
		param->worldViewProj.Transform(in->position, &out->position);
		param->textureMatrix.Transform(in->position, &out->uv);
	}

	virtual VertexInputLayout* GetVertexInputLayout() 
	{
		return layout;
	}

	VertexInputLayout layout[2];
};

class PixelShaderFlashColour : public SoftwarePixelShaderTemplate<PixelShaderFlashColour, ShaderFlashPixelInput>
{
public:

	inline static void Apply(const ParameterData* parameter, const PixelInput* input, PixelOutput* output)
	{
		register ShaderFlashPixelInput* in = (ShaderFlashPixelInput*)input;
		register ShaderFlashParameterData* param = (ShaderFlashParameterData*)parameter;
		output->colour = param->colour;
	}
};

class PixelShaderFlashTexture : public SoftwarePixelShaderTemplate<PixelShaderFlashTexture, ShaderFlashPixelInput>
{
public:

	inline static void Apply(const ParameterData* parameter, const PixelInput* input, PixelOutput* output)
	{
		register ShaderFlashPixelInput* in = (ShaderFlashPixelInput*)input;
		register ShaderFlashParameterData* param = (ShaderFlashParameterData*)parameter;

		Vector4 texColour = param->texture->Tex2D(in->uv);
		output->colour = param->colour * texColour;
	}
};

#endif