#ifndef _SHADER_COLOURTEXTURE_H_
#define _SHADER_COLOURTEXTURE_H_

#include "Render/Software/SoftwareEffect.h"

#ifdef WIN32
	#define inline __forceinline
#endif

class ShaderColourTextureParameterData : public ParameterData
{
public:

    virtual ~ShaderColourTextureParameterData() {}

	ShaderColourTextureParameterData()
	{
		layout[0].type = EPT_Matrix4;
		layout[0].ID = EPID_WorldViewProjection;
		layout[0].offset = VAR_OFFSET(worldViewProj);
		layout[0].size = sizeof(Matrix4);

		layout[1].type = EPT_Unknown;
		layout[1].ID = EPID_Max;
		layout[1].offset = -1;
		layout[1].size = -1;
	}

	ParameterData* Clone()
	{
		return new ShaderColourTextureParameterData;
	}

	ParameterDataLayout* GetParameterDataLayout()
	{
		return layout;
	}

	Matrix4 worldViewProj;

	ParameterDataLayout layout[4];
};

struct ShaderColourTextureVertexInput : public VertexInput
{
	Vector4 position;
	Vector4 colour;
	Vector4 uv;
};

struct ShaderColourTexturePixelInput : public PixelInput
{
	Vector4 colour;
	Vector4 uv;
};

class VertexShaderColourTexture : public SoftwareVertexShaderTemplate<ShaderColourTextureVertexInput>
{
public:

	VertexShaderColourTexture()
	{
		layout[0].dataType = VF_Position;
		layout[0].offset = 0;
		layout[0].size = sizeof(Vector4);

		layout[1].dataType = VF_Colour;
		layout[1].offset = sizeof(Vector4);
		layout[1].size = sizeof(Vector4);

		layout[2].dataType = VF_TexCoord0;
		layout[2].offset = sizeof(Vector4) + sizeof(Vector4);
		layout[2].size = sizeof(Vector4);

		layout[3].dataType = VF_Unknown;
		layout[3].offset = -1;
		layout[3].size = -1;
	}

	virtual ~VertexShaderColourTexture()
	{
	}

	virtual void Apply(const ParameterData* parameter, const VertexInput* input, PixelInput* output)
	{
		ShaderColourTextureVertexInput* in = (ShaderColourTextureVertexInput*)input;
		ShaderColourTexturePixelInput* out = (ShaderColourTexturePixelInput*)output;
		ShaderColourTextureParameterData* param = (ShaderColourTextureParameterData*)parameter;

		in->position.w = 1.0f;
		param->worldViewProj.Transform(in->position, &out->position);
		out->colour = in->colour;
		out->uv = in->uv;
	}

	virtual VertexInputLayout* GetVertexInputLayout()
	{
		return layout;
	}

	VertexInputLayout layout[4];
};

class PixelShaderColourTexture : public SoftwarePixelShaderTemplate<PixelShaderColourTexture, ShaderColourTexturePixelInput>
{
public:

	inline static void Apply(const ParameterData* parameter, const PixelInput* input, PixelOutput* output)
	{
		register ShaderColourTexturePixelInput* in = (ShaderColourTexturePixelInput*)input;
		output->colour = in->colour; //* texture look up
	}
};

#endif
