#ifndef _ENVIRONMENTMSG_H_
#define _ENVIRONMENTMSG_H_

#include "Core/Message.h"
#include "Core/MessageMgr.h"

class LoadEnvironmentMsg : public Message
{
public:

	TYPE(10);

	virtual bool ConvertFromString(vector<string>& params, TranslationTable* translationTable)
	{
		mEnvironment = params[0];
		return true;
	}

#ifdef NETWORK
	virtual bool ConvertToNetwork(Packet* data)				
	{ 
		*data & mEnvironment;
		return true; 
	}

	virtual bool ConvertFromNetwork(Packet* data)	
	{ 
		*data & mEnvironment;
		return true; 
	}
#endif

	string			mEnvironment;
};

class UnloadEnvironmentMsg : public Message
{
public:

	TYPE(31)
};

#endif