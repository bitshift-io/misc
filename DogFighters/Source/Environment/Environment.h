#ifndef _ENVIRONMENT_H_
#define _ENVIRONMENT_H_

#ifdef NETWORK
	#include "Network/Network.h"
#endif

#include <list>
#include "Core/Message.h"
#include "Game/Game.h"
#include "Core/EntityMgr.h"
#include "Camera/CameraMgr.h"

using namespace std;

class Renderer;
class PhysicsScene;
class Entity;
class Spawn;
class LoadEnvironmentMsg;
class UnloadEnvironmentMsg;
class ClientConnectMsg;
class Scene;
class Network;
class RenderMgr;

//
// This should be called SceneMgr ? its the environment + all actors in the environment
// should this have the render in it so multiple environments dont conflict
//
class Environment : public MessageReceiver
{
public:

	typedef list<Entity*>::iterator EntityIterator;

	void Init(Game* game);
	void Deinit();
	void Update();
	void Render();

	bool Load(const char* path);
	void Unload();

	virtual bool			HandleMsg(Message* msg);
	virtual const char*		GetName()					{ return "environment"; }

	bool		LoadEnvironment(LoadEnvironmentMsg* msg);
	bool		UnloadEnvironment(UnloadEnvironmentMsg* msg);

#ifdef NETWORK
	bool		ClientConnect(ClientConnectMsg* msg);
#endif

	// simply use pointers to speed things up, compile time
	EntityMgr*		mEntityMgr;

	// camera
	CameraMgr*		mCameraMgr;


#ifdef NETWORK
	// network
	Network*		mNetwork;
#endif

#ifdef PHYSICS
	PhysicsScene*	mPhysicsScene;
#endif

	RenderMgr*		mRenderMgr;

	Game*			GetGame()				{ return mGame; }

	const char*		GetEnvironmentName()	{ return mEnvironment.c_str(); }

	EntityIterator	BeginEntity()			{ return mEntity.begin(); }
	EntityIterator	EndEntity()				{ return mEntity.end(); }

protected:

	string			mEnvironment; // what environment we have loaded
	bool			mLoaded;
	list<Entity*>	mEntity;
	Game*			mGame;
};

#endif
