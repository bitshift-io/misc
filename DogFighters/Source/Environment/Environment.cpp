#include "StdAfx.h"
#include "Environment.h"

void Environment::Init(Game* game)
{
	mGame = game;

	mCameraMgr = new CameraMgr;
	mCameraMgr->Init();

	mRenderMgr = new RenderMgr;
	mRenderMgr->Init(this);

#ifdef PHYSICS
	mPhysicsScene = gGame.mPhysicsFactory->CreatePhysicsScene();
	if (mPhysicsScene->Init())
	{
		// this should be moved to metadata
		//
		// group:
		// 1 - static player interaction
		// 2 - dynamic pushable by player
		// 3 - dynamic nonpushable player interaction
		// 4 - dynamic (convex) no player interaction
		// 5 - dynamic (trimesh) player interaction no interaction with group 1
		// 6 - players
		mPhysicsScene->SetCollisionGroupFlags(2, CGF_Pushable);
		mPhysicsScene->EnableCollisionBetweenGroup(6, 4, false);
		mPhysicsScene->EnableCollisionBetweenGroup(5, 1, false);
		mPhysicsScene->EnableCollisionBetweenGroup(4, 5, false);
		mPhysicsScene->EnableCollisionBetweenGroup(4, 3, false);
		mPhysicsScene->EnableCollisionBetweenGroup(4, 2, false);
	}
#endif

#ifdef NETWORK
	mNetwork = new Network;
	mNetwork->Init(game->mMasterServer);
#endif

	mEntityMgr = new EntityMgr;
	mEntityMgr->Init();

	RegisterReceiver();
}

void Environment::Deinit()
{
	Unload();

#ifdef PHYSICS
	if (mPhysicsScene)
	{
		mPhysicsScene->Deinit();
		gGame.mPhysicsFactory->ReleasePhysicsScene(&mPhysicsScene);
	}
#endif

	mEntityMgr->Deinit();
	delete mEntityMgr;

#ifdef NETWORK
	mNetwork->Deinit();
	delete mNetwork;
#endif

	mCameraMgr->Deinit();
	delete mCameraMgr;

	mRenderMgr->Deinit();
	delete mRenderMgr;

	DeregisterReceiver();
}

void Environment::Update()
{
	//mRenderMgr->Update();
	//mNetwork->Update();
	//mEntityMgr->Update();
}

bool Environment::Load(const char* ph)
{
	// unload old environment!
	Unload();

	mEnvironment = ph;
	Log::Print("[Environment::Load] Loading environment '%s'\n", ph);

	unsigned int metadataTime = Time::GetMilliseconds();

	MetadataScript metaScript;
	list<void*> entityList;
	mLoaded = metaScript.Open(ph, &entityList);
	if (!mLoaded)
	{
		Log::Error("[Environment::Load] Failed to load environment '%s'\n", ph);
		return false;
	}

	metadataTime = Time::GetMilliseconds() - metadataTime;
	Log::Print("\t[Environment::Load] Metadata loaded in %f seconds\n", (float)metadataTime / 1000.0f);

	unsigned int entityInitTime = Time::GetMilliseconds();

	int idx = 1;
	list<void*>::iterator it;
	for (it = entityList.begin(); it != entityList.end(); ++it)
	{
		Entity* pEntity = static_cast<Entity*>(*it);
		if (!pEntity)
			continue;

		Log::Print("\t[Environment::Load] Init'ing entity %i of %i\n", idx, entityList.size());

		unsigned int individualEntityInitTime = Time::GetMilliseconds();

		// we should prolly do this and pass in a pointer to this environment
		// then the environment can have a pointer to the game and no need for gGame :D
		pEntity->Init();
		mEntity.push_back(pEntity);

		individualEntityInitTime = Time::GetMilliseconds() - individualEntityInitTime;
		Log::Print("\t[Environment::Load] Entity %i Init'ed in %f seconds\n", idx, (float)individualEntityInitTime / 1000.0f);

		++idx;
	}

	entityInitTime = Time::GetMilliseconds() - entityInitTime;
	mLoaded = true;

	// flush all updates
	gGame.mGameUpdate.Reset();
	gGame.mPhysicsUpdate.Reset();
	gGame.mNetworkUpdate.Reset();

	Log::Print("[Environment::Load] Loading Complete. Entitys Init'ed in %f seconds\n", (float)entityInitTime / 1000.0f);
	return true;
}

void Environment::Unload()
{
	// free all entity's
	list<Entity*>::iterator it;
	for (it = mEntity.begin(); it != mEntity.end(); )
	{
		(*it)->Deinit();
		//DeinitEntity(*it);
		it = mEntity.erase(it);
	}
}

MESSAGE_BEGIN(Environment)
	MESSAGE_PASS(LoadEnvironment)
	MESSAGE_PASS(UnloadEnvironment)
#ifdef NETWORK
	MESSAGE_PASS(ClientConnect)
#endif
MESSAGE_END()

bool Environment::LoadEnvironment(LoadEnvironmentMsg* msg)
{
#ifdef NETWORK
	if (mNetwork->IsHosting())
		mNetwork->SendMessage(msg, this);
#endif

	return Load(msg->mEnvironment.c_str());
}

bool Environment::UnloadEnvironment(UnloadEnvironmentMsg* msg)
{
	Unload();
	return true;
}

#ifdef NETWORK
bool Environment::ClientConnect(ClientConnectMsg* msg)
{

	if (!mNetwork->IsHosting())
		return true;

	if (msg->mConnectType == CCT_Local)
		return true;

	// tell client what map to load
	LoadEnvironmentMsg loadEnv;
	loadEnv.mEnvironment = mEnvironment;
	mNetwork->SendMessage(&loadEnv, this, msg->mPeer);
	return true;
}
#endif