#ifndef _SPAWNMSG_H_
#define _SPAWNMSG_H_

class GetPawnSpawnMsg : public Message
{
public:

	TYPE(90)

	GetPawnSpawnMsg(Pawn* pawn, Team* team) :
	  mPawn(pawn),
	  mTransform(MI_Identity),
	  mTeam(team)
	{
	}

	Matrix4		mTransform;
	Pawn*		mPawn;
	Team*		mTeam;
};

#endif