#ifndef _ENABLEMSG_H_
#define _ENABLEMSG_H_

class EnableMsg : public Message
{
public:

	TYPE(120)
	NAME(EnableMsg)

	EnableMsg() : mEnable(true)
	{
	}

	EnableMsg(bool enable) : mEnable(enable)
	{
	}

	bool	mEnable; // if false, disable
};


class IsEnabledMsg : public Message
{
public:

	TYPE(121)
	NAME(IsEnabledMsg)

	IsEnabledMsg() : mEnabled(false)
	{
	}

	bool	mEnabled;
};

#endif