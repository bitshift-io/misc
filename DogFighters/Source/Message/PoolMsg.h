#ifndef _POOLMSG_H_
#define _POOLMSG_H_

#include "Core/Message.h"

class ReturnToPoolMsg : public Message
{
public:

	TYPE(140)
	NAME(ReturnToPoolMsg)

	Entity*	mEntity;
};

class CreateFromPoolMsg : public Message
{
public:

	TYPE(141)
	NAME(CreateFromPoolMsg)

	CreateFromPoolMsg()
	{
	}

	CreateFromPoolMsg(Pool<Entity>* pool) :
		mPool(pool)
	{
	}


	Pool<Entity>* mPool;
};

#endif
