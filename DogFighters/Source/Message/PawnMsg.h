#ifndef _PAWNMSG_H_
#define _PAWNMSG_H_

#include "Core/Message.h"

class SpawnMsg : public Message
{
public:

	TYPE(110)
	NAME(SpawnMsg)
};

class PawnKilledMsg : public Message
{
public:

	TYPE(111)
	NAME(PawnKilledMsg)

	PawnKilledMsg() : mKilled(0), mDamageMsg(0)
	{
	}

	PawnKilledMsg(Pawn* killed, TakeDamageMsg* damageMsg) :
		mKilled(killed),
		mDamageMsg(damageMsg)
	{
	}

	Pawn*			mKilled;
	TakeDamageMsg*	mDamageMsg;

};

//
// sent when you wrap around the screen
//
class WarpMsg : public Message
{
public:
	
	TYPE(112)
	NAME(WarpMsg)
};

class CopyPawnMsg : public Message
{
public:

	TYPE(113)
	NAME(CopyPawnMsg)

	CopyPawnMsg() : mCopyFrom(0)
	{
	}

	CopyPawnMsg(Pawn* copyFrom) : mCopyFrom(copyFrom)
	{
	}

	Pawn*	mCopyFrom;
};

#endif
