#ifndef _SCENEMESSAGE_H_
#define _SCENEMESSAGE_H_

class Scene;

class GetSceneMsg : public Message
{
public:

	TYPE(5);

	explicit GetSceneMsg() :
		mScene(0),
		mRenderFlags(RF_Default)
	{
	}

	Scene*		mScene;
	RenderFlags mRenderFlags;
};

class GetTransformMsg : public Message
{
public:

	TYPE(18);

	explicit GetTransformMsg() :
		mTransform(MI_Identity)
	{
	}

	Matrix4	mTransform;
};


class SetTransformMsg : public Message
{
public:

	TYPE(19);

	Matrix4	mTransform;
};

#endif
