#ifndef _COLOURMSG_H_
#define _COLOURMSG_H_

#include "Core/Message.h"

class SetColourMsg : public Message
{
public:

	TYPE(80)

	SetColourMsg()
	{
	}

	SetColourMsg(const Vector4& colour) :
		mColour(colour)
	{
	}

	Vector4	mColour;
};

class OverrideMaterialMsg : public Message
{
public:

	TYPE(81)

	OverrideMaterialMsg()
	{
	}

	OverrideMaterialMsg(MaterialBase* materialBase)
	{
		mMaterialBase = materialBase;
	}

	MaterialBase*	mMaterialBase;
};

#endif
