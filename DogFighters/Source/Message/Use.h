#ifndef _USE_H_
#define _USE_H_

#include "Core/Entity.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"
#include "Core/Message.h"
#include "Math/Sphere.h"
#include "Math/Matrix.h"

class SceneNode;
class Pawn;

class GetUseInfoMsg : public Message
{
public:

	TYPE(6);

	GetUseInfoMsg() : mCanUse(false)
	{
	}

	Matrix4	mTransform;
	bool	mCanMove;
	bool	mCanLook;
	bool	mCanUse;
};

class UseMsg : public Message
{
public:

	TYPE(8);

	explicit UseMsg(Pawn* pawn, bool visible = true, SceneNode* attachNode = 0) :
		mPawn(pawn),
		mVisible(visible),
		mAttachNode(attachNode)
	{
	}

	bool			mVisible;
	SceneNode*		mAttachNode;
	Pawn*			mPawn;
};

class FindUseableMsg : public Message
{
public:

	TYPE(4);

	explicit FindUseableMsg(const Sphere& sphere) :
		mUseSphere(sphere),
		mCount(0)
	{
		memset(mEntity, 0, sizeof(Entity*) * 256);
	}

	bool Respond(Entity* entity)
	{
		if (mCount >= 256)
			return true;

		mEntity[mCount] = entity;
		++mCount;
		return false;
	}

	Sphere	mUseSphere; // the sphere we are checking in
	Entity* mEntity[256];
	int		mCount;
};

class SetEnabledMsg : public Message
{
public:

	TYPE(28);

	explicit SetEnabledMsg(bool enabled) :
		mEnabled(enabled)
	{
	}

	bool	mEnabled;
};

#endif
