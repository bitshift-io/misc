#ifndef _DAMAGE_H_
#define _DAMAGE_H_

#include "../Core/Message.h"

class TakeDamageMsg : public Message
{
public:

	TYPE(0);

	explicit TakeDamageMsg(float damage = 0.f, Pawn* attacker = 0, Entity* projectile = 0) :
		mDamage(damage),
		mAttacker(attacker),
		mDamageTaken(0.f),
		mKilled(false),
		mProjectile(projectile)
	{	
	}

	// input
	float			mDamage;
	Pawn*			mAttacker;
	Entity*			mProjectile;

	// outputs
	float			mDamageTaken;
	bool			mKilled;
};

#endif

