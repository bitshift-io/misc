#ifndef _RESOLVE_H_
#define _RESOLVE_H_

#include "Core/Entity.h"

//
// find a node by name, optionally filter by type
//
class ResolveByNameMsg : public Message
{
public:

	TYPE(15);

	explicit ResolveByNameMsg(const string& name, int type = -1) :
		mName(name),
		mType(-1),
		mCount(0)
	{
		memset(mEntity, 0, sizeof(Entity*) * 256);
	}

	bool Respond(Entity* entity)
	{
		if (mCount >= 256)
			return true;

		mEntity[mCount] = entity;
		++mCount;
		return false;
	}

	int		mType;
	string	mName;
	Entity* mEntity[256];
	int		mCount;
};

class ResolveByTypeMsg : public Message
{
public:

	TYPE(17);

	explicit ResolveByTypeMsg(int type) :
		mType(type),
		mEntity(0)
	{
	}

	int		mType;
	Entity* mEntity;
};

//
// tell another enitity to take this entity and append it to its entity list (or take other appropriate action)
//
class AppendEntityMsg : public Message
{
public:

	TYPE(16);

	explicit AppendEntityMsg(Entity* entity) :
		mEntity(entity)
	{
	}

	Entity* mEntity;
};

#endif
