#ifndef _FIRE_H_
#define _FIRE_H_

#include "Core/Message.h"
#include "Math/Matrix.h"

class FireBegin : public Message
{
public:

	TYPE(1);

	explicit FireBegin()
	{
	}
};

class FireEnd : public Message
{
public:

	TYPE(2);

	explicit FireEnd()
	{
	}
};

class FireMsg : public Message
{
public:

	TYPE(11);

	FireMsg() : mAttacker(0)
	{
	}

	explicit FireMsg(Pawn* attacker, const Matrix4& transform, const Vector4& linearVelocity = Vector4(0.f, 0.f, 0.f))
	{
		mAttacker = attacker;
		mTransform = transform;
		mLinearVelocity = linearVelocity;
	}

#ifdef NETWORK
	bool ConvertToNetwork(Packet* data)
	{
		*data & mMuzzleTransform;
		return true;
	}

	bool ConvertFromNetwork(Packet* data)
	{
		*data & mMuzzleTransform;
		return true;
	}
#endif

	Matrix4			mTransform;
	Vector4			mLinearVelocity;
	Pawn*			mAttacker;
};

#endif
