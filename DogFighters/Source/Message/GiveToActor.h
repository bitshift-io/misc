#ifndef _GIVETOACTOR_H_
#define _GIVETOACTOR_H_

#include "../Core/Message.h"

class GiveToActor : public Message
{
public:

	TYPE(3);

	explicit GiveToActor(Entity* entity) :
		mEntity(entity)
	{
	}

	Entity* mEntity;
};

#endif
