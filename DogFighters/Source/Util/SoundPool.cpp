#include "StdAfx.h"
#include "SoundPool.h"

CLASS_METADATA_BEGIN(SoundPool)
	CLASS_VARIABLE(SoundPool, string, mSoundName)
	CLASS_VARIABLE(SoundPool, float, mPitchVariation)
	CLASS_VARIABLE(SoundPool, float, mPitchSpeedMax)
	CLASS_VARIABLE(SoundPool, float, mPitchSpeedMin)
CLASS_METADATA_END(SoundPool)

SoundPool::SoundPool() :
	mPitchVariation(0.f),
	mActiveSound(0),
	mSoundName("")
{
}

void SoundPool::Init()
{
	vector<string>::iterator soundNameIt;
	vector<string> soundNames = String::Tokenize(mSoundName, ",", true);
	for (soundNameIt = soundNames.begin(); soundNameIt != soundNames.end(); ++soundNameIt)
	{
		Sound* sound = gGame.mSoundFactory->CreateSound(*soundNameIt);
		if (sound)
		{
			mSound.push_back(sound);
			sound->SetGain(gGame.mSoundVolume);
		}
	}

	mPitchTimer = 0.f;
	mPitchAdjustRate = 0.f;
	mPitch = 1.f;
}

void SoundPool::Deinit()
{
	vector<Sound*>::iterator it;
	for (it = mSound.begin(); it != mSound.end(); ++it)
	{
		gGame.mSoundFactory->ReleaseSound(&(*it));
	}
}

void SoundPool::Update()
{
	if (!IsPlaying())
		return;

	mPitchTimer -= gGame.mGameUpdate.GetDeltaTime();
	if (mPitchTimer <= 0.f)
	{
		mPitchTimer = Math::Rand(mPitchSpeedMin, mPitchSpeedMax);
		float pitch = mActiveSound->GetPitch();
		float pitchNext = Math::Rand(1.f - mPitchVariation * 0.5f, 1.f + mPitchVariation * 0.5f);
		mPitchAdjustRate = (pitchNext - pitch) / mPitchTimer;
	}

	mPitch += mPitchAdjustRate;
	mPitch = Math::Clamp(1.f - mPitchVariation * 0.5f, mPitch, 1.f + mPitchVariation * 0.5f);

	vector<Sound*>::iterator it;
	for (it = mSound.begin(); it != mSound.end(); ++it)
	{
		(*it)->SetPitch(mPitch);
	}
}

void SoundPool::Play()
{
	if (mSound.size() <= 0)
		return;

	int soundIdx = Math::Min(Math::Rand(0, mSound.size()), (float)(mSound.size() - 1));
	mActiveSound = mSound[soundIdx];
	mActiveSound->Play();
}

void SoundPool::Stop()
{
	if (!mActiveSound)
		return;

	mActiveSound->Stop();
	mActiveSound = 0;
}

bool SoundPool::IsPlaying()
{
	if (mActiveSound)
		return mActiveSound->IsPlaying();

	return false;
}

void SoundPool::SetPosition(const Vector4& position)
{
	vector<Sound*>::iterator it;
	for (it = mSound.begin(); it != mSound.end(); ++it)
	{
		(*it)->SetPosition(position);
	}
}

void SoundPool::SetGain(float gain)
{
	vector<Sound*>::iterator it;
	for (it = mSound.begin(); it != mSound.end(); ++it)
	{
		(*it)->SetGain(gain);
	}
}
