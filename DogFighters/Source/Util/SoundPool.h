#ifndef _SOUNDPOOL_H_
#define _SOUNDPOOL_H_

//
// same interface as a sound but handles multiple sounds
//
class SoundPool
{
public:

	USES_METADATA
	CLASSNAME

	SoundPool();

	void		Init();
	void		Deinit();
	void		Update();

	void SetPosition(const Vector4& position);
	void SetGain(float gain);
	void Play();
	void Stop();
	bool IsPlaying();

	

protected:

	vector<Sound*>		mSound;
	Sound*				mActiveSound;
	float				mPitchVariation;
	float				mPitch;
	float				mPitchSpeedMax;
	float				mPitchSpeedMin;
	float				mPitchTimer;
	float				mPitchAdjustRate;
	string				mSoundName;
};


#endif