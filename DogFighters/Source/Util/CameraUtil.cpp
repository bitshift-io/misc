#include "StdAfx.h"
#include "CameraUtil.h"


void GetHalfWidthHeight(float& halfWidth, float& halfHeight, float radius)
{
	// check for border wrap around
	Camera* camera = gGame.GetEnvironment()->mCameraMgr->GetCamera();
	float cameraHeight = camera->GetPosition().y;
	float halfFov = Math::DtoR(camera->GetFOV() * 0.5f);
	halfWidth = (float)tanf(halfFov) * cameraHeight * camera->GetAspectRatio();
    halfHeight = (float)tanf(halfFov) * cameraHeight;

	halfWidth += radius / 2;
	halfHeight += radius / 2;
}

bool PositionWrapAround(float halfWidth, float halfHeight, Vector4& position)
{
	if (position.x > halfWidth)
	{
        position.x = -halfWidth;
		return true;
	}
 
    if (position.x < -halfWidth)
	{
        position.x = halfWidth;
		return true;
	}

    if (position.z > halfHeight)
	{
        position.z = -halfHeight;
		return true;
	}

    if (position.z < -halfHeight)
	{
        position.z = halfHeight;
		return true;
	}

	return false;
}

bool OutOfView(float halfWidth, float halfHeight, Vector4& position)
{
	if (position.x > halfWidth)
        return true;
 
    if (position.x < -halfWidth)
         return true;

    if (position.z > halfHeight)
         return true;

    if (position.z < -halfHeight)
         return true;

	return false;
}
