#ifndef _CAMERAUTIL_H_
#define _CAMERAUTIL_H_

//
// gets half height and half width from the current camera, this can also be given a radius
// to help with camera wrap around code
//
void GetHalfWidthHeight(float& halfWidth, float& halfHeight, float radius = 0.f);

//
// wraps the position around so its always with in the cameras view
//
bool PositionWrapAround(float halfWidth, float halfHeight, Vector4& position);

//
// check if the position is out of view
//
bool OutOfView(float halfWidth, float halfHeight, Vector4& position);

#endif