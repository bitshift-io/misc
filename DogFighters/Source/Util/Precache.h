#ifndef _PRECACHE_H_
#define _PRECACHE_H_

//
// Class to handle caching of engine goodies
//
class Precache
{
public:

	void	Init();
	void	Deinit();

	Scene*	CacheScene(const char* name);

protected:

	vector<Scene*>	mSceneCache;
};

#endif