#include "StdAfx.h"
#include "ForceFeedbackEffect.h"

CLASS_METADATA_BEGIN(ForceFeedbackEffect)
	CLASS_VARIABLE(ForceFeedbackEffect, float, mForce)
	CLASS_VARIABLE(ForceFeedbackEffect, float, mTime)
CLASS_METADATA_END(ForceFeedbackEffect)

ForceFeedbackEffect::ForceFeedbackEffect() :
	mJoystick(0),
	mTimer(0.f),
	mTime(1.f),
	mForce(1.f)
{
}

void ForceFeedbackEffect::SetJoystick(Joystick* joystick)
{
	mJoystick = joystick;
}

void ForceFeedbackEffect::Update()
{
	if (!mJoystick || !IsPlaying())
		return;

	mTimer -= gGame.mGameUpdate.GetDeltaTime();
	mJoystick->ApplyForce(Vector4(1.f, 1.f, 1.f, 1.f), Math::Clamp(0.f, mForce * (mTimer / mTime), 1.f));
}

void ForceFeedbackEffect::Play(float force, float time)
{
	if (!mJoystick)
		return;

	if (force != -1.f)
		mForce = force;

	if (time != -1.f)
		mTime = time;

	mJoystick->ApplyForce(Vector4(1.f, 1.f, 1.f, 1.f), mForce);
	mTimer = mTime;
}

void ForceFeedbackEffect::Stop()
{
}

bool ForceFeedbackEffect::IsPlaying()
{
	return mTimer > 0.f;
}
