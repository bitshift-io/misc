#include "StdAfx.h"
#include "Precache.h"


void Precache::Init()
{
}

void Precache::Deinit()
{
	vector<Scene*>::iterator it;
	for (it = mSceneCache.begin(); it != mSceneCache.end(); ++it)
	{
		gGame.mRenderFactory->ReleaseScene(&(*it));
	}
}

Scene* Precache::CacheScene(const char* name)
{
	vector<Scene*>::iterator it;
	for (it = mSceneCache.begin(); it != mSceneCache.end(); ++it)
	{
		if (strcmpi((*it)->GetName().c_str(), name) == 0)
			return (*it);
	}

	Scene* scene = gGame.mRenderFactory->LoadScene(&ObjectLoadParams(gGame.mDevice, name));
	mSceneCache.push_back(scene);
	return scene;
}
