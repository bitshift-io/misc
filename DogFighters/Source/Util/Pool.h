#ifndef _POOL_H_
#define _POOL_H_

#include <vector>

template <class T>
class Pool
{
public:

	//typedef vector<T*>::iterator Iterator;

	void	Init(int size, bool create);
	void	Deinit();

	bool	ReturnToPool(T* item);
	T*		Create();

	//Iterator	Begin()				{ return mPool.begin(); }
	//Iterator	End()				{ return mPool.end(); }

//protected:

	vector<T*>	mPool;
	int			mSeperator;
	bool		mCreate;
};

template <class T>
void Pool<T>::Init(int size, bool create)
{
	mSeperator = 0;
	mPool.resize(size);

	mCreate = create;
	if (create)
	{
		for (int i = 0; i < size; ++i)
			mPool[i] = new T;
	}
}

template <class T>
void Pool<T>::Deinit()
{
	if (mCreate)
	{
		typename vector<T*>::iterator it;
		for (it = mPool.begin(); it != mPool.end(); ++it)
		{
			delete *it;
		}
	}
}

template <class T>
bool Pool<T>::ReturnToPool(T* item)
{
	for (int i = 0; i < mPool.size(); ++i)
	{
		if (mPool[i] == item)
		{
			--mSeperator;
			T* temp = mPool[mSeperator];
			mPool[mSeperator] = item;
			mPool[i] = temp;
			return true;
		}
	}

	return false;
}

template <class T>
T* Pool<T>::Create()
{
	if (mSeperator >= mPool.size())
		return 0;

	T* item = mPool[mSeperator];
	++mSeperator;
	return item;
}

#endif
