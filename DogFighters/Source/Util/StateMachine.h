#ifndef _STATEMACHINE_H_
#define _STATEMACHINE_H_

//#include "StdAfx.h"
#include <vector>

using namespace std;

template <class T>
class StateMachine
{
public:

	//typedef vector<T*>::iterator Iterator;

	StateMachine() : mCurrentState(0)
	{
	}

	// adds to top of stack
	T*			GetCurrentState();

	T*			GetState(int idx)				{ return mState[idx]; }

	void		InsertState(T* newState);

	// removes previous state from stack
	void		SetState(T* newState);
	void		SetState(int stateType);

	//Iterator	Begin()							{ return mState.begin(); }
	//Iterator	End()							{ return mState.end(); }

protected:

	vector<T*>	mState;
	T*			mCurrentState;
};

template <class T>
void StateMachine<T>::SetState(int stateType)
{
	typename vector<T*>::iterator it;
	for (it = mState.begin(); it != mState.end(); ++it)
	{
		if ((*it)->GetType() == stateType)
		{
			SetState(*it);
			return;
		}
	}
}

template <class T>
void StateMachine<T>::SetState(T* newState)
{
	if (mCurrentState)
		mCurrentState->StateExit();

	newState->StateEnter();	
	mCurrentState = newState;
}

template <class T>
T* StateMachine<T>::GetCurrentState()
{
	return mCurrentState;
}

template <class T>
void StateMachine<T>::InsertState(T* newState)
{
	mState.push_back(newState);
}

#endif
