#ifndef _FORCEFEEDBACKEFFECT_H_
#define _FORCEFEEDBACKEFFECT_H_

//
// same interface as a sound but handles multiple sounds
//
class ForceFeedbackEffect
{
public:

	USES_METADATA
	CLASSNAME

	ForceFeedbackEffect();

	void		SetJoystick(Joystick* joystick);
	void		Update();

	void Play(float force = -1.f, float time = -1.f);
	void Stop();
	bool IsPlaying();


protected:

	Joystick*	mJoystick;

	float		mForce;
	float		mTime;
	float		mTimer;
};


#endif