#include "StdAfx.h"
#include "ParticleEffect.h"

CLASS_METADATA_BEGIN(ParticleEffect)
	CLASS_VARIABLE(ParticleEffect, list<Entity*>, mEntity)
	CLASS_VARIABLE(ParticleEffect, string, mSceneName)
	CLASS_VARIABLE(ParticleEffect, string, mAttachNodeName)
	CLASS_VARIABLE(ParticleEffect, Matrix4, mTransform)
CLASS_METADATA_END(ParticleEffect)

ParticleEffect::ParticleEffect() : 
	mTransform(MI_Identity),
	mEnabled(true)
{
}

void ParticleEffect::Init()
{
	Super::Init();

	// sever performance
	if (gGame.mCmdLine.GetToken("noparticle") || gGame.mRenderFactory->GetRenderFactoryType() == RFT_Software)
		return;

	gGame.GetEnvironment()->mEntityMgr->RegisterRenderable(this);

	mAttachNode = 0;
	mScene = gGame.mRenderFactory->LoadScene(&ObjectLoadParams(gGame.mDevice, mSceneName));
	Scene* scene = static_cast<Pawn*>(GetTopMostParent())->GetScene();
	mAttachNode = scene->GetNodeByName(mAttachNodeName.c_str()); 

	if (mAttachNode)
		mAttachNode->InsertChild(mScene->GetRootNode());

/*
	// iterate through parents to find valid model, this is the correct way to find a model
	if (mAttachNodeName.length())
	{
		GetSceneMsg scene;
		SceneNode* node = 0;
		Entity* parent = mParent;
		while (parent && !node)
		{
			parent->HandleMsg(&scene);
			parent = parent->GetParent();

			if (scene.mScene)
				node = scene.mScene->GetNodeByName(mAttachNodeName);
		}

		mAttachNode = node;

		if (node)
			node->InsertChild(mScene->GetRootNode());
	}*/
/*
	GetSceneMsg scene;
	mParent->HandleMsg(&scene);
	if (!scene.mScene)
	{
		Log::Print("[ParticleEffect::Init] failed to get scene from parent '%s'\n");
		return;
	}
*/
	

	//mAttachNode = scene.mScene->GetNodeByName(mAttachNodeName);
	//mAttachNode->InsertChild(mScene->GetRootNode());

	gGame.GetEnvironment()->mRenderMgr->InsertScene(mScene);
	//mScene->SetTransform(mTransform);

	mParticleSystem = (ParticleSystem*)gGame.mRenderFactory->Create(ParticleSystem::Type);
	mParticleSystem->Init(mScene, this);
	mParticleSystem->SetTransform(mTransform);
}

void ParticleEffect::Deinit()
{
	Super::Deinit();

	gGame.GetEnvironment()->mEntityMgr->DeregisterRenderable(this);

	if (mAttachNode)
		mAttachNode->RemoveChild(mScene->GetRootNode());

	gGame.mRenderFactory->Release((SceneObject**)&mParticleSystem);
}

void ParticleEffect::Update()
{
	//Matrix4 transform = mAttachNode->GetWorldTransform();

	Super::Update();

	if (!mEnabled)
		return;

	//if (mAttachNode)
	//	mParticleSystem->SetTransform(mAttachNode->GetWorldTransform());

	//Matrix4 identity(MI_Identity);
	//mParticleSystem->SetTransform(identity);

	//Log::Print("updating particle system\n");
	mParticleSystem->Update(gGame.mGameUpdate.GetDeltaTime());

	if (mParticleSystem->IsFinished())
	{
		// return this to the pool
		// for now, just reset the system
		mParticleSystem->Reset();
	}
}

void ParticleEffect::Render(int layer)
{
	if (!mEnabled)
		return;
	//mParticleSystem->Draw(gGame.mDevice, gGame.GetEnvironment()->mCameraMgr->GetCamera());
}

Particle* ParticleEffect::Create(ParticleActionGroup* actionGroup)
{
	// use metadata to return custom particles here, based on actionGroup name :)
	return ParticleContainer::Create(actionGroup);
}

Scene* ParticleEffect::GetParentScene()
{
	Entity* parent = GetParent();
	GetSceneMsg sceneMsg;
	do
	{
		parent->HandleMsg(&sceneMsg);
		parent = parent->GetParent();
	} while (!sceneMsg.mScene);
	return sceneMsg.mScene;
}

MESSAGE_BEGIN(ParticleEffect)
	MESSAGE_PASS(DebugDraw)
MESSAGE_SUPER_END()

bool ParticleEffect::DebugDraw(DebugDrawMsg* msg)
{
	if (!mEnabled)
		return true;

	mParticleSystem->Draw(gGame.mDevice, gGame.GetEnvironment()->mCameraMgr->GetCamera());
	return true;
}