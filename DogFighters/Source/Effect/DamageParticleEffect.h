#ifndef _DAMAGEMODELPARTICLEEFFECT_H_
#define _DAMAGEMODELPARTICLEEFFECT_H_

#include "ModelParticleEffect.h"

class DamageParticleEffect : public ModelParticleEffect
{
public:

	typedef ModelParticleEffect Super;
	USES_METADATA
	CLASSNAME

	virtual void		Init();
	virtual void		Render(int layer);
	void				Update();

protected:

	virtual void		Render(SceneNode* node, Particle* particle);

	void				GetMaxLife(SceneNode* node);
	void				GetColourParam(SceneNode* node);
	virtual void		SetColour(const Vector4& colour);
	//virtual void		Render(int layer);
	//virtual void		Render(SceneNode* node, Particle* particle);

	EffectParameterValue*	mColourParam;
	float					mDamageLevel;
	Vector4					mStartColour;
	Vector4					mEndColour;
	float					mMaxLife;
	float					mStartScale;
	float					mEndScale;
	Material*				mMaterial;
};

#endif
