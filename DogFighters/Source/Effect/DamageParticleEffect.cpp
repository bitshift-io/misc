#include "StdAfx.h"
#include "DamageParticleEffect.h"

CLASS_METADATA_BEGIN(DamageParticleEffect)
	CLASS_VARIABLE(DamageParticleEffect, float, mDamageLevel)
	CLASS_VARIABLE(DamageParticleEffect, Vector4, mStartColour)
	CLASS_VARIABLE(DamageParticleEffect, Vector4, mEndColour)
	CLASS_VARIABLE(DamageParticleEffect, float, mStartScale)
	CLASS_VARIABLE(DamageParticleEffect, float, mEndScale)
CLASS_EXTENDS_METADATA_END(DamageParticleEffect, ModelParticleEffect)

void DamageParticleEffect::Init()
{
	Super::Init();
	GetColourParam(mParticle->GetRootNode());
	GetMaxLife(mParticle->GetRootNode());
}

void DamageParticleEffect::GetMaxLife(SceneNode* node)
{
	if (node->GetObject())
	{
		switch (node->GetObject()->GetType())
		{
		case ParticleActionGroup::Type:
			{
				ParticleActionGroup* group = static_cast<ParticleActionGroup*>(node->GetObject());
				ParticleActionGroup::Iterator it;
				for (it = group->Begin(); it != group->End(); ++it)
				{
					if ((*it)->GetType() == ParticleDeath::Type)
					{
						ParticleDeath* death =  static_cast<ParticleDeath*>(*it);
						mMaxLife = death->mLife + death->mVariation;
					}
				}
			}
			break;

		default:
			break;
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
		GetMaxLife(*it);
}

void DamageParticleEffect::GetColourParam(SceneNode* node)
{
	// we now need to instance any meshes and materials so we can apply unique colours
	if (node->GetObject())
	{
		switch (node->GetObject()->GetType())
		{
		case Mesh::Type:
			{
				// clone the mesh, so we can clone the materials
				// to apply material specific overrides
				Mesh* mesh = static_cast<Mesh*>(node->GetObject());
				Mesh::MeshMatIterator it;
				for (it = mesh->Begin(); it != mesh->End(); ++it)
				{
					mMaterial = it->material;
					mColourParam = it->material->GetMaterialBase()->GetCurrentTechnique()->AddParameterByName("colour");
				}				
			}
			break;

		default:
			break;
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
		GetColourParam(*it);
}

void DamageParticleEffect::SetColour(const Vector4& colour)
{
	if (mColourParam)
		mColourParam->SetValue(&colour);
}

void DamageParticleEffect::Update()
{
	// only update if in flystate
	float health = static_cast<Pawn*>(GetTopMostParent())->GetHealth();
	if (static_cast<Pawn*>(GetTopMostParent())->GetCurrentState()->GetType() != FlyState::Type 
		|| health > mDamageLevel)
	{
		Reset();
		return;
	}

	Super::Update();
}

void DamageParticleEffect::Render(int layer)
{
	// only draw if in flystate
	float health = static_cast<Pawn*>(GetTopMostParent())->GetHealth();
	if (static_cast<Pawn*>(GetTopMostParent())->GetCurrentState()->GetType() != FlyState::Type
		|| health > mDamageLevel)
	{
		return;
	}

	if (layer != 1)
		return;

	// cuz we are rendering in immidiate mode,
	// we can screw with the material how ever we like, as long as we retore it 
	Vector4 originalColour;
	mColourParam->GetValue(&originalColour);

	// because i know this effects will only have a single mesh and material, i can bind the material a single time :)
	mMaterial->GetMaterialBase()->Begin();
	
	ParticleContainer::Iterator it;
	for (it = ParticleContainer::Begin(); it != ParticleContainer::End(); ++it)
	{
		SetColour(Math::Lerp(mStartColour, mEndColour, (*it)->mLife / mMaxLife));
		Matrix4 transform = (*it)->mTransform;
		transform.SetScale(Math::Lerp(mStartScale, mEndScale, (*it)->mLife / mMaxLife));
		mParticle->SetTransform(transform);
		DamageParticleEffect::Render(mParticle->GetRootNode(), *it);
	}

	mMaterial->GetMaterialBase()->EndPass();	
	mMaterial->GetMaterialBase()->End();

	SetColour(originalColour);
}

void DamageParticleEffect::Render(SceneNode* node, Particle* particle)
{
	if (node && node->GetObject() && node->GetObject()->GetType() == Mesh::Type && !(node->GetFlags() & SNF_Hidden))
	{
		Camera* camera = gGame.GetEnvironment()->mCameraMgr->GetCamera();
		Mesh* mesh = static_cast<Mesh*>(node->GetObject());
		for (unsigned int i = 0; i < mesh->GetNumMeshMat(); ++i)
		{
			MeshMat& meshMat = mesh->GetMeshMat(i);
			//int numPasses = meshMat.material->Begin();
			//for (int i = 0; i < numPasses; ++i)
			{
				//meshMat.material->GetMaterialBase()->BeginPass(gGame.mDevice, i);
				mMaterial->GetMaterialBase()->BeginPass(0);
				mMaterial->GetMaterialBase()->GetEffect()->SetMatrixParameters(camera->GetProjection(), camera->GetView(), camera->GetWorld(), node->GetWorldTransform());
				mMaterial->GetMaterialBase()->SetPassParameter();

				//gGame.mDevice->Draw(DT_NotInstanced, meshMat.material, meshMat.geometry, meshMat.material->GetVertexFormat(0));
				meshMat.geometry->Draw();

				//meshMat.material->GetMaterialBase()->EndPass();	
			}

			//meshMat.material->GetMaterialBase()->End();	
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
	{
		Render(*it, particle);
	}
}


/*
void DamageParticleEffect::Render(SceneNode* node, Particle* particle)
{
	if (node && node->GetObject() && node->GetObject()->GetType() == Mesh::Type && !(node->GetFlags() & SNF_Hidden))
	{
		Camera* camera = gGame.GetEnvironment()->mCameraMgr->GetCamera();
		Mesh* mesh = static_cast<Mesh*>(node->GetObject());
		for (unsigned int i = 0; i < mesh->GetNumMeshMat(); ++i)
		{
			MeshMat& meshMat = mesh->GetMeshMat(i);
			int numPasses = meshMat.material->Begin();
			for (int i = 0; i < numPasses; ++i)
			{
				meshMat.material->BeginPass(gGame.mDevice, i);
				meshMat.material->GetEffect()->SetMatrixParameters(camera->GetProjection(), camera->GetView(), camera->GetWorld(), node->GetWorldTransform());

				
				meshMat.material->SetPassParameter();

				gGame.mDevice->Draw(DT_NotInstanced, meshMat.material, meshMat.geometry, meshMat.material->GetVertexFormat(0));

				meshMat.material->EndPass();	
			}

			meshMat.material->End();	
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
	{
		Render(*it, particle);
	}
}*/