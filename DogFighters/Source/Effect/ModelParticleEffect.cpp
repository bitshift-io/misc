#include "StdAfx.h"
#include "ModelParticleEffect.h"

CLASS_METADATA_BEGIN(ModelParticleEffect)
	CLASS_VARIABLE(ModelParticleEffect, string, mParticleName)
CLASS_EXTENDS_METADATA_END(ModelParticleEffect, ParticleEffect)

void ModelParticleEffect::Init()
{
	mParticle = gGame.mRenderFactory->LoadScene(&ObjectLoadParams(gGame.mDevice, mParticleName));
	Super::Init();
}

void ModelParticleEffect::Deinit()
{
	gGame.mRenderFactory->ReleaseScene(&mParticle);
	Super::Deinit();
}

void ModelParticleEffect::Update()
{
	Super::Update();
}

void ModelParticleEffect::Render(int layer)
{
	if (layer != 1)
		return;

	ParticleContainer::Iterator it;
	for (it = ParticleContainer::Begin(); it != ParticleContainer::End(); ++it)
	{
		mParticle->SetTransform((*it)->mTransform);
		Render(mParticle->GetRootNode(), *it);
	}
}

void ModelParticleEffect::Render(SceneNode* node, Particle* particle)
{
	if (node && node->GetObject() && node->GetObject()->GetType() == Mesh::Type && !(node->GetFlags() & SNF_Hidden))
	{
		Camera* camera = gGame.GetEnvironment()->mCameraMgr->GetCamera();
		Mesh* mesh = static_cast<Mesh*>(node->GetObject());
		for (unsigned int i = 0; i < mesh->GetNumMeshMat(); ++i)
		{
			MeshMat& meshMat = mesh->GetMeshMat(i);
			int numPasses = meshMat.material->Begin();
			for (int i = 0; i < numPasses; ++i)
			{
				meshMat.material->GetMaterialBase()->BeginPass(i);
				meshMat.material->GetMaterialBase()->GetEffect()->SetMatrixParameters(camera->GetProjection(), camera->GetView(), camera->GetWorld(), node->GetWorldTransform());
				meshMat.material->GetMaterialBase()->SetPassParameter();

				//gGame.mDevice->Draw(DT_NotInstanced, meshMat.material, meshMat.geometry, meshMat.material->GetVertexFormat(0));
				meshMat.geometry->Draw();

				meshMat.material->GetMaterialBase()->EndPass();	
			}

			meshMat.material->GetMaterialBase()->End();	
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
	{
		Render(*it, particle);
	}
}
