#ifndef _PARTICLEEFFECT_H_
#define _PARTICLEEFFECT_H_

//
// Effects using particle system
//
class ParticleEffect : public Entity, public ParticleContainer
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME

	ParticleEffect();

	virtual void		Init();
	virtual void		Deinit();

	virtual void		Update();

	virtual bool		HandleMsg(Message* msg);
	virtual bool		DebugDraw(DebugDrawMsg* msg);

	virtual void		Render(int layer);

	virtual Particle*	Create(ParticleActionGroup* actionGroup);

	Scene*				GetParentScene();

protected:

	string				mAttachNodeName;
	string				mSceneName;

	SceneNode*			mAttachNode;
	ParticleSystem*		mParticleSystem;
	Scene*				mScene;
	Matrix4				mTransform;
	bool				mEnabled;
};

#endif

