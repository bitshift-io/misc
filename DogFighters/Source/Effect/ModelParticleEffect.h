#ifndef _MODELPARTICLEEFFECT_H_
#define _MODELPARTICLEEFFECT_H_

#include "ParticleEffect.h"

class ModelParticleEffect : public ParticleEffect
{
public:

	typedef ParticleEffect Super;
	USES_METADATA
	CLASSNAME

	virtual void		Init();
	virtual void		Deinit();

	virtual void		Render(int layer);
	void				Update();

protected:

	virtual void		Render(SceneNode* node, Particle* particle);

	string	mParticleName; // a model list to emit a random model?
	Scene*	mParticle;
};

#endif
