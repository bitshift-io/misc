#include "StdAfx.h"
#include "SimpleState.h"

CLASS_METADATA_BEGIN(SimpleState)
	CLASS_VARIABLE(SimpleState, float, mStraitPercent)
	CLASS_VARIABLE(SimpleState, float, mMinStraitTime)
	CLASS_VARIABLE(SimpleState, float, mMaxStraitTime)
	CLASS_VARIABLE(SimpleState, float, mMaxTurnTime)
	CLASS_VARIABLE(SimpleState, float, mMinTurnTime)
	CLASS_VARIABLE(SimpleState, float, mMinFireDot)
	CLASS_VARIABLE(SimpleState, float, mMaxFireDistance)
	CLASS_VARIABLE(SimpleState, float, mMinFireDistance)
CLASS_METADATA_END(SimpleState)

SimpleState::SimpleState() :
	mStraitPercent(0.f)
{
}

void SimpleState::StateEnter()
{
	mTurnTimer = 0.f;
}

void SimpleState::StateExit()				
{
}

void SimpleState::StateUpdate()
{
	mTimeInState += gGame.mGameUpdate.GetDeltaTime();

	mTurnTimer -= gGame.mGameUpdate.GetDeltaTime();
	if (mTurnTimer <= 0.f)
	{
		// determine if this move should be strait or turning
		float straitOrTurn = Math::Rand(0.f, 100.f);
		if (straitOrTurn <= mStraitPercent)
		{
			mTurnTimer += Math::Rand(mMinStraitTime, mMaxStraitTime);

			ControllerAction* leftAction = mOwner->GetControllerAction(CAT_Left);
			ControllerAction* rightAction = mOwner->GetControllerAction(CAT_Right);

			leftAction->mState = 0.f;
			leftAction->mStateNormalized = leftAction->mState;

			rightAction->mState = 0.f;
			rightAction->mStateNormalized = rightAction->mState;
		}
		else
		{
			mTurnTimer += Math::Rand(mMinTurnTime, mMaxTurnTime);

			// turn
			bool leftOrRight = Math::Rand(0.f, 100.f) > 50.f;
			ControllerAction* leftAction = mOwner->GetControllerAction(CAT_Left);
			ControllerAction* rightAction = mOwner->GetControllerAction(CAT_Right);

			ControllerAction* turnAction = leftOrRight ? leftAction : rightAction;
			turnAction->mState = Math::Rand(0.f, 1.f);
			turnAction->mStateNormalized = turnAction->mState;
		}

		if (ShouldFire())
		{
			ControllerAction* fireAction = mOwner->GetControllerAction(CAT_Fire);
			fireAction->mState = 1.f;
			fireAction->mStateNormalized = 1.f;
		}
	}
}

bool SimpleState::ShouldFire()
{
	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		Pawn* pawn = (*controllerIt)->GetPawn();
		Pawn* aiPawn = mOwner->GetPawn();
		if (!pawn || !aiPawn || pawn->IsDead() || pawn == mOwner->GetPawn() || mOwner->GetTeam() == (*controllerIt)->GetTeam())
			continue;

		// calculate dot
		Vector4 enemyPawnPos = pawn->GetTransform().GetTranslation();
		Vector4 aiPawnPos = aiPawn->GetTransform().GetTranslation();
		Vector4 dirToEnemy = enemyPawnPos - aiPawnPos;
		float distToEnemy = dirToEnemy.Normalize();

		Vector4 aiPawnDir = aiPawn->GetTransform().GetZAxis();

		float dot = aiPawnDir.Dot(dirToEnemy);

		if (dot > mMinFireDot && distToEnemy > mMinFireDistance && distToEnemy < mMaxFireDistance)
		{
			return true;
		}
	}

	return false;
}