#ifndef _AICONTROLLERSTATE_H_
#define _AICONTROLLERSTATE_H_

class AIController;

//
// used to set up transition rules
//
class AIStateTransition
{
public:

	virtual bool Evaulate();

protected:

	list<AIStateTransition*>		mTransition;
};

class AIControllerState
{
public:

	virtual int				GetType() const					{ return -1; }

	virtual void			Init(AIController* owner)		{ mOwner = owner; }
	virtual void			StateEnter()							= 0;
	virtual void			StateExit()							= 0;
	virtual void			StateUpdate()						= 0;

	virtual string&			GetName()						{ return mName; }

protected:

	string							mName;
	AIController*					mOwner;
	list<AIStateTransition*>		mTransition;
};

#endif


