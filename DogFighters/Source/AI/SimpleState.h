#ifndef _SIMPLESTATE_H_
#define _SIMPLESTATE_H_

//
// simple AI
//
class SimpleState : public AIControllerState
{
public:

	USES_METADATA

	SimpleState();

	void			StateEnter();
	void			StateExit();
	void			StateUpdate();

	bool			ShouldFire();

	float			mMaxTurnTime;
	float			mMinTurnTime;

	float			mMinStraitTime;
	float			mMaxStraitTime;

	float			mStraitPercent;

	float			mTimeInState;
	float			mTurnTimer;

	float			mMinFireDot;				// fire if dot is greater than this
	float			mMaxFireDistance;		// fire if distance is less than this
	float			mMinFireDistance;		// fire if dist further than this
};

#endif
