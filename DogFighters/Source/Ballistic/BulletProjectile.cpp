#include "StdAfx.h"
#include "BulletProjectile.h"

CLASS_METADATA_BEGIN(BulletProjectile)
	CLASS_VARIABLE(BulletProjectile, float, mRadius)
CLASS_EXTENDS_METADATA_END(BulletProjectile, Projectile)

void BulletProjectile::Init()
{
	Super::Init();
}

void BulletProjectile::Fire(Pool<Projectile>* pool, FireMsg* msg)
{
	mInheritedSpeed = msg->mLinearVelocity.Mag();
	mScene->SetTransform(msg->mTransform);
	Super::Fire(pool, msg);
}

void BulletProjectile::Update()
{
	if (!mPool)
		return;

	Matrix4 transform = GetTransform();
	transform.SetTranslation(transform.GetTranslation() + transform.GetZAxis() * (mMuzzleVelocity + mInheritedSpeed) * gGame.mGameUpdate.GetDeltaTime());
	SetTransform(transform);

	// check for collision
	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		// dont collide with self
		// check against alive pawns only
		Pawn* pawn = (*controllerIt)->GetPawn();
		if (!pawn || pawn == mAttacker || pawn->IsDead())
			continue;

		float pawnRadius = pawn->GetRadius();
		Vector4 pawnPos = pawn->GetTransform().GetTranslation();

		Vector4 distVector = transform.GetTranslation() - pawnPos;
		float dist = distVector.Mag();
        dist = dist - (pawnRadius + mRadius);
        if (dist < 0 && mAttacker->GetController())// pawn with no controller anymore, cant do damage
        {
			++(mAttacker->GetController()->GetStats()->mHits);

			TakeDamageMsg damageMsg(mDamage, mAttacker, this);
            gGame.GetGameModeMgr()->GetGameMode()->TakeDamage(pawn, &damageMsg);
			ReturnToPool();
            return;
        }
	}

	Super::Update();
}
