#ifndef _BULLETPROJECTILE_H_
#define _BULLETPROJECTILE_H_

#include "Core/Entity.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"
#include "Math/Matrix.h"

class Scene;
class PhysicsBody;

class BulletProjectile : public Projectile
{
public:

	USES_METADATA
	CLASSNAME
	typedef Projectile Super;

	virtual void		Init();

	virtual void		Fire(Pool<Projectile>* pool, FireMsg* msg);
	virtual void		Update();

protected:

	float				mInheritedSpeed;
	float				mRadius;
};

#endif
