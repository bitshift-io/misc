#include "StdAfx.h"
#include "ColourWeapon.h"
#include "Pickup/WeaponUpgrade.h"

CLASS_METADATA_BEGIN(ColourWeapon)	
	CLASS_VARIABLE(ColourWeapon, float, mDoubleBeamAngle)
	CLASS_VARIABLE(ColourWeapon, float, mTripleBeamAngle)
	CLASS_VARIABLE(ColourWeapon, int, mUpgradeLevel)
CLASS_EXTENDS_METADATA_END(ColourWeapon, Weapon)

void ColourWeapon::Init()
{
	Super::Init();

	mColourMesh = 0;
	//mUpgradeLevel = WUL_SingleBeam;
	mInitialFireDelay = mFireDelay;
	/*
	mColourParam = 0;
/*
	// set up material instances
	{
		vector<Pool<Projectile> >::iterator poolIt;
		for (poolIt = mProjectilePool.begin(); poolIt != mProjectilePool.end(); ++poolIt)
		{
			Pool<Projectile>& pool = *poolIt;

			vector<Projectile*>::iterator projIt;
			for (projIt = pool.mPool.begin(); projIt != pool.mPool.end(); ++projIt)
			{
				GetSceneMsg scene;
				(*projIt)->GetScene(&scene);
				InitInstanceMaterials(scene.mScene->GetRootNode());
			}
		}
	}

	SetColour(&SetColourMsg(Vector4(1.f, 1.f, 1.f, 1.f)));*/
}

void ColourWeapon::Deinit()
{
	Super::Deinit();
}

void ColourWeapon::OverrideMaterial(SceneNode* node, OverrideMaterialMsg* msg)
{
	// we now need to instance any meshes and materials so we can apply unique colours
	if (node->GetObject())
	{
		switch (node->GetObject()->GetType())
		{
		case Mesh::Type:
			{
				//if (mColourMesh)
				//{
				//	mColourMesh->AddReference();
				//	node->SetObject(mColourMesh);
				//}
				//else
				{
					// clone the mesh, so we can clone the materials
					// to apply material specific overrides
					Mesh* mesh = static_cast<Mesh*>(node->GetObject());
					//Mesh* clone = (Mesh*)mesh->Clone();
					Mesh::MeshMatIterator it;
					for (it = mesh->Begin(); it != mesh->End(); ++it)
					{
						//it->material->ReleaseReference();
						//msg->mMaterialBase->AddReference();
						//it->material = msg->mMaterial;
						it->material->SetMaterialBase(msg->mMaterialBase);
					}
					//node->SetObject(clone);
					//mColourMesh = clone;
				}
			}
			break;

		default:
			break;
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
		OverrideMaterial(*it, msg);
}

MESSAGE_BEGIN(ColourWeapon)
	MESSAGE_PASS(SetColour)
	MESSAGE_PASS(OverrideMaterial)
	MESSAGE_PASS(ModifyFireDelay)
	MESSAGE_PASS(WeaponUpgrade)
MESSAGE_SUPER_END()

bool ColourWeapon::OverrideMaterial(OverrideMaterialMsg* msg)
{
	vector<Pool<Projectile> >::iterator poolIt;
	for (poolIt = mProjectilePool.begin(); poolIt != mProjectilePool.end(); ++poolIt)
	{
		Pool<Projectile>& pool = *poolIt;

		vector<Projectile*>::iterator projIt;
		for (projIt = pool.mPool.begin(); projIt != pool.mPool.end(); ++projIt)
		{
			GetSceneMsg scene;
			(*projIt)->GetScene(&scene);
	
			// we need to remove the scene, then reinsert once materials are adjusted!
			gGame.GetEnvironment()->mRenderMgr->RemoveScene(scene.mScene);
			OverrideMaterial(scene.mScene->GetRootNode(), msg);
			gGame.GetEnvironment()->mRenderMgr->InsertScene(scene.mScene, (*projIt)->GetRenderFlags());
		}
	}

	return false;
}


bool ColourWeapon::SetColour(SetColourMsg* msg)
{/*
	if (mColourParam)
		mColourParam->SetValue(msg->mColour);
*/
	return false;
}

bool ColourWeapon::ModifyFireDelay(ModifyFireDelayMsg* msg)
{
	if (msg->mIncrement < 0.f)
		mFireDelay = Math::Max(msg->mMinFireDelay, mFireDelay + msg->mIncrement);
	else
		mFireDelay = Math::Min(msg->mMaxFireDelay, mFireDelay + msg->mIncrement);

	return false;
}

bool ColourWeapon::WeaponUpgrade(WeaponUpgradeMsg* msg)
{
	// what this code does is decrement the reload rate, until min is reached, once min is reached,
	// weapons get upgraded to level 2... rinse and repeat
	if (mFireDelay <= msg->mMinFireDelay)
	{
		unsigned int currentLevel = mUpgradeLevel;
		mUpgradeLevel = Math::Min(mUpgradeLevel + 1, unsigned int(WUL_TripleBeam));

		// have we changed level? if so, reset fire speed
		if (currentLevel != mUpgradeLevel)
		{
			mFireDelay = mInitialFireDelay;
		}
	}
	else
	{
		if (msg->mIncrement < 0.f)
			mFireDelay = Math::Max(msg->mMinFireDelay, mFireDelay + msg->mIncrement);
		else
			mFireDelay = Math::Min(msg->mMaxFireDelay, mFireDelay + msg->mIncrement);
	}

	return false;
}

bool ColourWeapon::Fire(FireMsg* msg)
{
	if (mTimeSinceFire >= mFireDelay)
	{
		++(msg->mAttacker->GetController()->GetStats()->mShotsFired);

		mTimeSinceFire = 0.f;

		if (mMuzzleNode)
			msg->mTransform = mMuzzleNode->GetWorldTransform();

		switch (mUpgradeLevel)
		{
		case WUL_SingleBeam:
			{
				FireProjectile(msg);
			}
			break;

		case WUL_DoubleBeam:
			{
				Matrix4 origonalTransform = msg->mTransform;

				{
					Matrix4 rotation(MI_Identity);
					rotation.RotateY(Math::DtoR(mDoubleBeamAngle));
					Matrix4 finalTransform;
					finalTransform = origonalTransform.Multiply3(rotation);
					finalTransform.SetTranslation(origonalTransform.GetTranslation());
					msg->mTransform = finalTransform;
					FireProjectile(msg);
				}

				{
					Matrix4 rotation(MI_Identity);
					rotation.RotateY(Math::DtoR(-mDoubleBeamAngle));
					Matrix4 finalTransform;
					finalTransform = origonalTransform.Multiply3(rotation);
					finalTransform.SetTranslation(origonalTransform.GetTranslation());
					msg->mTransform = finalTransform;
					FireProjectile(msg);
				}
			}
			break;

		case WUL_TripleBeam:
			{
				Matrix4 origonalTransform = msg->mTransform;
				FireProjectile(msg);

				{
					Matrix4 rotation(MI_Identity);
					rotation.RotateY(Math::DtoR(mTripleBeamAngle));
					Matrix4 finalTransform;
					finalTransform = origonalTransform.Multiply3(rotation);
					finalTransform.SetTranslation(origonalTransform.GetTranslation());
					msg->mTransform = finalTransform;
					FireProjectile(msg);
				}

				{
					Matrix4 rotation(MI_Identity);
					rotation.RotateY(Math::DtoR(-mTripleBeamAngle));
					Matrix4 finalTransform;
					finalTransform = origonalTransform.Multiply3(rotation);
					finalTransform.SetTranslation(origonalTransform.GetTranslation());
					msg->mTransform = finalTransform;
					FireProjectile(msg);
				}
			}
			break;
		}
	}
	return true;
}