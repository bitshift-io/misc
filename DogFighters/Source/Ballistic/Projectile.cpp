#include "StdAfx.h"
#include "Projectile.h"

CLASS_METADATA_BEGIN(Projectile)
	CLASS_VARIABLE(Projectile, list<Entity*>, mEntity)
	CLASS_VARIABLE(Projectile, string, mSceneName)
	CLASS_VARIABLE(Projectile, float, mMaxDeviation)
	CLASS_VARIABLE(Projectile, float, mMuzzleVelocity)
	CLASS_VARIABLE(Projectile, float, mDamage)	
	CLASS_VARIABLE(Projectile, float, mTimeToLive)
	CLASS_VARIABLE(Projectile, bool, mFaceCamera)
	CLASS_VARIABLE(Projectile, RenderFlags, mRenderFlags)
CLASS_METADATA_END(Projectile)

Entity*	Projectile::Clone()
{
	return Super::Clone();
	/*
	Projectile* clone = new Projectile;
	clone->Init(this);
	return clone;*/
}

void Projectile::Init(/*Projectile* clone*/)
{/*
	mMaxDeviation = clone->mMaxDeviation;
	mMuzzleVelocity = clone->mMuzzleVelocity;
	mDamage = clone->mDamage;
	mTimeToLive = clone->mTimeToLive;
	mFaceCamera = clone->mFaceCamera;
	mSceneName = clone->mSceneName;
	*/
	gGame.mPrecache.CacheScene(mSceneName.c_str()); // register with precache system if not already loaded
	mScene = gGame.mRenderFactory->LoadScene(&ObjectLoadParams(gGame.mDevice, mSceneName)); // this will result in a clone
	mPool = 0;

	gGame.GetEnvironment()->mRenderMgr->InsertScene(mScene, mRenderFlags);

	mScene->SetFlags(SNF_Hidden);

	Super::Init();
}

void Projectile::Deinit()
{
	gGame.GetEnvironment()->mRenderMgr->RemoveScene(mScene);

	// release scene
	gGame.mRenderFactory->ReleaseScene(&mScene);

	Super::Deinit();
}

void Projectile::Fire(Pool<Projectile>* pool, FireMsg* msg)
{
	mAttacker = msg->mAttacker;
	mPool = pool;
	mLife = 0.f;

	mScene->ClearFlags(SNF_Hidden);
}

void Projectile::ReturnToPool()
{
	if (!mPool)
		return;

	mScene->SetFlags(SNF_Hidden);
	mPool->ReturnToPool(this);
	mPool = 0;
}

void Projectile::SetTransform(const Matrix4& transform)
{
	mScene->SetTransform(transform);
}

Matrix4 Projectile::GetTransform()
{
	return mScene->GetTransform();
}

void Projectile::Update()
{
	// check life and return to pool if needed
	mLife += gGame.mGameUpdate.GetDeltaTime();
	if (mLife >= mTimeToLive)
	{
		ReturnToPool();
	}

	if (mFaceCamera)
	{
		Matrix4 transform = GetTransform();

		// x-axis to face camera
		const Matrix4& camWorld = gGame.GetEnvironment()->mCameraMgr->GetCamera()->GetWorld();
		Vector4 camPos = camWorld.GetTranslation();
		Vector4 pos = transform.GetTranslation();
		Vector4 toCam = (camPos - pos);
		toCam.Normalize();

		Vector4 up = toCam.Cross(transform.GetZAxis());
		up.Normalize();

		Vector4 right = -transform.GetZAxis().Cross(up);
		right.Normalize();

		transform.SetYAxis(up);
		transform.SetXAxis(right);
		SetTransform(transform);
	}

	Super::Update();
}

void Projectile::CauseDamage(Entity* entity)
{
	if (!entity)
		return;

	TakeDamageMsg damage(mDamage, mAttacker, this);
	entity->HandleMsg(&damage);
}

MESSAGE_BEGIN(Projectile)
	MESSAGE_PASS(GetScene)
MESSAGE_SUPER_END()

bool Projectile::GetScene(GetSceneMsg* msg)
{
	msg->mScene = mScene;
	msg->mRenderFlags = mRenderFlags;
	return true;
}
