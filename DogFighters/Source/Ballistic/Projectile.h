#ifndef _PROJECTILE_H_
#define _PROJECTILE_H_

#include "Core/Entity.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"
#include "Math/Matrix.h"

class Scene;

//
// A weapon fires a projectile,
// our parent is the weapon that fired us, the top most parent should be a pawn
//
class Projectile : public Entity
{
public:

	USES_METADATA
	CLASSNAME
	typedef Entity Super;

	Projectile() : mFaceCamera(false), mRenderFlags(RF_Default)
	{
	}

	virtual Entity*		Clone();
	virtual void		Init(/*Projectile* clone*/);
	virtual void		Deinit();

	virtual void		Fire(Pool<Projectile>* pool, FireMsg* msg);
	virtual void		ReturnToPool();

	virtual void		Update();
	virtual void		SetTransform(const Matrix4& transform);
	virtual Matrix4		GetTransform();

	virtual void		CauseDamage(Entity* entity);

	virtual bool		HandleMsg(Message* msg);
	virtual bool		GetScene(GetSceneMsg* msg);

	RenderFlags			GetRenderFlags()				{ return mRenderFlags; }

protected:

	Pawn*				mAttacker;		// who fired the projectile
	RenderFlags			mRenderFlags;
	string				mSceneName;
	float				mMaxDeviation;
	float				mMuzzleVelocity;
	float				mDamage;
	float				mTimeToLive;
	bool				mFaceCamera;
	Scene*				mScene;
	float				mLife;
	Pool<Projectile>*	mPool;
};

#endif
