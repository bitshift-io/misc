#ifndef _WEAPON_H_
#define _WEAPON_H_

#include "Core/Entity.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"
#include "Controller/Controller.h"

class SceneNode;
class Controller;
class ProjectileInfo;
class Projectile;

//
// This entity is responsible for firing of projectiles,
// tracking ammo/clips shell ejection etc...
//
class Weapon : public Entity
{
public:

	USES_METADATA
	CLASSNAME
	typedef Entity Super;

	Weapon() :
		mFireDelay(1.f),
		mPrimary(CAT_Fire),
		mFireSound(0),
		mMuzzleNode(0),
		mActiveProjectile(0),
		mForceFeedbackEffect(0)
	{
	}

	virtual void		Init();
	virtual void		Deinit();
	virtual void		Update();

	virtual bool		HandleMsg(Message* msg);
	virtual bool		SetPause(SetPauseMsg* msg);
	virtual bool		GetUseInfo(GetUseInfoMsg* msg);
	virtual bool		DebugDraw(DebugDrawMsg* msg);
	virtual bool		Use(UseMsg* msg);
	virtual bool		Fire(FireMsg* msg);
	virtual bool		VolumeChange(VolumeChangeMsg* msg);

	Projectile*			FireProjectile(FireMsg* msg);

protected:

	ControllerActionType	mPrimary;
	float					mFireDelay;
	string					mMuzzleNodeName;
	SceneNode*				mMuzzleNode;
	int						mActiveProjectile;
	Pawn*					mPawn;					// owner
	float					mTimeSinceFire;
	int						mPoolSize;
	SoundPool*				mFireSound;
	ForceFeedbackEffect*	mForceFeedbackEffect;

	vector<Pool<Projectile> >	mProjectilePool;
	list<Projectile*>			mProjectile;
};

#endif
