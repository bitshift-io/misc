#ifndef _COLOURWEAPON_H_
#define _COLOURWEAPON_H_

#include "Ballistic/Weapon.h"

class ModifyFireDelayMsg;
class WeaponUpgradeMsg;

enum WeaponUpgradeLevel
{
	WUL_SingleBeam,
	WUL_DoubleBeam,
	WUL_TripleBeam,
};

//
// weapon that handles colouring of projectiles
//
class ColourWeapon : public Weapon
{
public:

	USES_METADATA
	CLASSNAME
	typedef Weapon Super;

	virtual void		Init();
	virtual void		Deinit();

	void				OverrideMaterial(SceneNode* node, OverrideMaterialMsg* msg);

	virtual bool		HandleMsg(Message* msg);
	virtual bool		SetColour(SetColourMsg* msg);
	virtual bool		OverrideMaterial(OverrideMaterialMsg* msg);
	virtual bool		ModifyFireDelay(ModifyFireDelayMsg* msg);
	virtual bool		WeaponUpgrade(WeaponUpgradeMsg* msg);
	virtual bool		Fire(FireMsg* msg);

protected:

	Mesh*					mColourMesh;
	unsigned int			mUpgradeLevel;
	float					mTripleBeamAngle;
	float					mDoubleBeamAngle;
	float					mInitialFireDelay;
};

#endif
