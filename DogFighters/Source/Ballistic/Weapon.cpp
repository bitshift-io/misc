#include "StdAfx.h"
#include "Weapon.h"

ListParser<ProjectileInfo*> projInfoParser("Projectile*");

CLASS_METADATA_BEGIN(Weapon)
	CLASS_VARIABLE(Weapon, list<Entity*>, mEntity)
	CLASS_VARIABLE(Weapon, float, mFireDelay)
	CLASS_VARIABLE(Weapon, int, mActiveProjectile)
	CLASS_VARIABLE(Weapon, string, mMuzzleNodeName)
	CLASS_VARIABLE(Weapon, list<Projectile*>, mProjectile)	
	CLASS_VARIABLE(Weapon, ControllerActionType, mPrimary)
	CLASS_VARIABLE(Weapon, int, mPoolSize)
	CLASS_VARIABLE(Weapon, SoundPool*, mFireSound)	
	CLASS_VARIABLE(Weapon, ForceFeedbackEffect*, mForceFeedbackEffect)	
CLASS_METADATA_END(Weapon)

void Weapon::Init()
{
	Super::Init();

	// set up networking
	RegisterReceiver();

	mTimeSinceFire = 0.f;
	mPawn = 0;

	GetSceneMsg scene;
	mParent->HandleMsg(&scene);
	if (!scene.mScene)
	{
		Log::Error("[WeaponMuzzle::Init] failed to get scene from parent '%s'\n");
		return;
	}

	mMuzzleNode = 0;
	if (mMuzzleNodeName.length())
	{
		mMuzzleNode = scene.mScene->GetNodeByName(mMuzzleNodeName);

		if (!mMuzzleNode)
		{
			GetSceneMsg scene;
			GetTopMostParent()->HandleMsg(&scene);
			if (!scene.mScene)
			{
				Log::Error("[WeaponMuzzle::Init] failed to get scene from parent '%s'\n");
				return;
			}

			mMuzzleNode = scene.mScene->GetNodeByName(mMuzzleNodeName);

			if (!mMuzzleNode)
				Log::Error("[WeaponMuzzle::Init] failed to find node '%s'\n", mMuzzleNodeName.c_str());
		}
	}

	// init the pool of projectiles
	{
		mProjectilePool.resize(mProjectile.size());
		list<Projectile*>::iterator it;
		vector<Pool<Projectile> >::iterator poolIt = mProjectilePool.begin();
		for (it = mProjectile.begin(); it != mProjectile.end(); ++poolIt)
		{
			poolIt->Init(mPoolSize, false);

			vector<Projectile*>::iterator projIt;
			for (projIt = poolIt->mPool.begin(); projIt != poolIt->mPool.end(); ++projIt)
			{
				*projIt = (Projectile*)(*it)->Clone();
				(*projIt)->SetParent(this);
				(*projIt)->Init();
				//Projectile* p = *projIt;
				//p->Init(*it);
			}

			// free the temporary projectile loaded from metadata
			delete *it;
			it = mProjectile.erase(it);
		}
	}

	// init sounds
	if (mFireSound)
	{
		mFireSound->Init();
		mFireSound->SetGain(gGame.mSoundVolume);
	}
}

void Weapon::Deinit()
{
	if (mFireSound)
	{
		mFireSound->Deinit();
		delete mFireSound;
		mFireSound = 0;
	}

	// deinit projectile pool
	{
		vector<Pool<Projectile> >::iterator poolIt;
		for (poolIt = mProjectilePool.begin(); poolIt != mProjectilePool.end(); )
		{
			vector<Projectile*>::iterator it;
			for (it = poolIt->mPool.begin(); it != poolIt->mPool.end(); ++it)
			{
				(*it)->Deinit();
				delete (*it);
			}

			poolIt->Deinit();
			poolIt = mProjectilePool.erase(poolIt);
		}
	}

	DeregisterReceiver();
	Super::Deinit();
}

void Weapon::Update()
{
	CController* controller = ((Pawn*)GetTopMostParent())->GetController();
	if (mForceFeedbackEffect && controller)
	{
		mForceFeedbackEffect->SetJoystick(controller->GetJoystick());
		mForceFeedbackEffect->Update();
	}

	if (mFireSound)
		mFireSound->Update();

	mTimeSinceFire += gGame.mGameUpdate.GetDeltaTime();
/*
	if (mPawn)
	{
		if (mPawn->GetController()->GetActionState(mPrimary) != 0.f && mTimeSinceFire >= mFireDelay)
			Fire();
	}*/

	// update projectiles
	vector<Pool<Projectile> >::iterator poolIt;
	for (poolIt = mProjectilePool.begin(); poolIt != mProjectilePool.end(); ++poolIt)
	{
		vector<Projectile*>::iterator it;
		for (it = poolIt->mPool.begin(); it != poolIt->mPool.end(); ++it)
		{
			(*it)->Update();
		}
	}

	Super::Update();
}


MESSAGE_BEGIN(Weapon)
	MESSAGE_PASS(DebugDraw)
	MESSAGE_PASS(Use)
	MESSAGE_PASS(Fire)
	MESSAGE_PASS(GetUseInfo)
	MESSAGE_PASS(SetPause)
	MESSAGE_PASS(VolumeChange)
MESSAGE_SUPER_END()

bool Weapon::VolumeChange(VolumeChangeMsg* msg)
{
	if (mFireSound)
		mFireSound->SetGain(gGame.mSoundVolume);

	return false;
}

bool Weapon::SetPause(SetPauseMsg* msg)
{
	if (mFireSound)
		mFireSound->Stop();

	return false;
}

bool Weapon::GetUseInfo(GetUseInfoMsg* msg)
{
	msg->mCanUse = true;
	return true;
}


bool Weapon::DebugDraw(DebugDrawMsg* msg)
{
	if (!mMuzzleNode)
		return true;

	Matrix4 transform = mMuzzleNode->GetWorldTransform();
	gGame.mDevice->DrawMatrix(transform);
	return true;
}

bool Weapon::Use(UseMsg* msg)
{
	UseMsg* cMsg = static_cast<UseMsg*>(msg);
	mPawn = cMsg->mPawn;
	return false;
}

bool Weapon::Fire(FireMsg* msg)
{
	if (mTimeSinceFire >= mFireDelay)
	{
		mTimeSinceFire = 0.f;

		if (mMuzzleNode)
		{
			msg->mTransform = mMuzzleNode->GetWorldTransform();
		}

		FireProjectile(msg);
	}

	return true;
}

Projectile* Weapon::FireProjectile(FireMsg* msg)
{
	Pool<Projectile>& pool = mProjectilePool[mActiveProjectile];
	Projectile* proj = pool.Create();
	if (proj)
	{
		if (mFireSound)
		{
			mFireSound->SetPosition(msg->mTransform.GetTranslation());
			mFireSound->Play();
		}

		if (mForceFeedbackEffect)
		{
			mForceFeedbackEffect->Play();
		}

		proj->Fire(&pool, msg);
	}

	return proj;
}
