#ifndef _PLAYBACK_H_
#define _PLAYBACK_H_

#include "Core/Message.h"

class PlaybackStream;

enum PlaybackType
{
	PB_Record,
	PB_Play,
};

//
// Game recording and playback support
// - this relies on the order during recording is the same as the order when playing back
//
class Playback : public MessageReceiver
{
public:

	void					Init();
	void					Deinit();

	bool					Load(const string& name);
	bool					Save(const string& name);
	bool					Save();

	PlaybackStream*			GetStream();
	void					ReleaseStream(PlaybackStream** stream);

	PlaybackType			GetPlaybackType()			{ return mType; }

	virtual bool			HandleMsg(Message* msg);
	virtual const char*		GetName()					{ return "playback"; }

protected:

	PlaybackType			mType;
	vector<PlaybackStream*> mStream;
	File					mFile;
	unsigned int			mIndex;
};

#endif
