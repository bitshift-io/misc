#include "StdAfx.h"
#include "Playback.h"

void Playback::Init()
{
	RegisterReceiver();
	mType = PB_Record;

	string file;
	if (gGame.mCmdLine.GetToken("play", file))
	{
		Load(file);
	}
}

void Playback::Deinit()
{
	unsigned int streamCount = mStream.size();
	for (unsigned int i = 0; i < streamCount; ++i)
	{
		delete mStream[i];
	}

	mStream.clear();
	DeregisterReceiver();
}

bool Playback::Save(const string& name)
{
	mIndex = 0;
	mType = PB_Record;
	string file = name + ".rec";
	if (!mFile.Open(file.c_str(), FAT_Write | FAT_Binary))
		return false;

	unsigned int streamCount = mStream.size();
	mFile.Write(&streamCount, sizeof(unsigned int));

	for (unsigned int i = 0; i < streamCount; ++i)
	{
		mStream[i]->Save(mFile);
	}

	return true;
}

bool Playback::Load(const string& name)
{
	mIndex = 0;
	mType = PB_Play;
	string file = name + ".rec";
	if (!mFile.Open(file.c_str(), FAT_Read | FAT_Binary))
		return false;

	unsigned int streamCount = 0;
	mFile.Read(&streamCount, sizeof(unsigned int));

	for (unsigned int i = 0; i < streamCount; ++i)
	{
		PlaybackStream* stream = new PlaybackStream(this);
		stream->Load(mFile);
		mStream.push_back(stream);
	}

	return true;
}

PlaybackStream*	Playback::GetStream()
{
	PlaybackStream* stream = 0;
	switch (mType)
	{
	case PB_Record:
		{
			stream = new PlaybackStream(this);
			mStream.push_back(stream);
		}
		break;

	case PB_Play:
		{
			stream = mStream[mIndex];
			++mIndex;
		}
		break;
	}

	return stream;
}

void Playback::ReleaseStream(PlaybackStream** stream)
{
	if (!*stream)
		return;

	switch (mType)
	{
	case PB_Record:
		{
			vector<PlaybackStream*>::iterator it;
			for (it = mStream.begin(); it != mStream.end(); ++it)
			{
				if (*it == *stream)
				{
					it = mStream.erase(it);
					break;
				}
			}

			delete *stream;
			stream = 0;
		}
		break;
	}
}

bool Playback::Save()
{
	File file;
	bool valid = true;
	int i = 0;
	while (valid)
	{
		char buffer[256];
		sprintf(buffer, "playback/recording%i.rec", i);
		valid = file.Open(buffer, FAT_Read | FAT_Binary);
		file.Close();

		if (!valid)
		{
			buffer[strlen(buffer) - 4] = '\0';
			return Save(buffer);
		}

		++i;
	}

	return false;
}

MESSAGE_BEGIN(Playback)
MESSAGE_END()

