#ifndef _PARACHUTEDYINGSTATE_H_
#define _PARACHUTEDYINGSTATE_H_

#include "Core/Entity.h"
#include "Pawn/PawnState.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"

//
// General dying state after puarachute
//
class ParachuteDyingState : public PawnState
{
public:

	TYPE(7)
	USES_METADATA
	typedef PawnState Super;

	ParachuteDyingState();

	virtual void			Init(Pawn* owner);
	virtual void			StateEnter();
	virtual void			StateExit();
	virtual void			StateUpdate();

protected:

	float	mLinearSpeed;
	float	mTimeInState;
	float	mMaxTimeInState;
	float	mAcceleration;
	float	mShakeTime;
	float	mShakeStrength;

	// scene node to enable in this state, all others will be hidden
	string				mVisibleNodeName;
	SceneNode*			mVisibleNode;
};


#endif
