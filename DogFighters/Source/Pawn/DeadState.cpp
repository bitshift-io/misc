#include "StdAfx.h"
#include "DeadState.h"

CLASS_METADATA_BEGIN(DeadState)
	CLASS_VARIABLE(DeadState, float, mShakeTime)
	CLASS_VARIABLE(DeadState, float, mShakeStrength)
CLASS_METADATA_END(DeadState)

DeadState::DeadState() : 
	mShakeTime(0.f), 
	mShakeStrength(0.f)
{
}

void DeadState::StateEnter()
{
	// do camera shake
	gGame.GetEnvironment()->mCameraMgr->GetGameCamera()->Shake(mShakeStrength, mShakeTime);

	GetSceneMsg scene;
	mOwner->GetScene(&scene);
	scene.mScene->SetFlags(SNF_Hidden, 0, true);

	CController* controller = mOwner->GetController();
	if (controller)
	{
		++(controller->GetStats()->mDeaths);
	}

	Matrix4 transform(MI_Identity);
	transform.SetTranslation(mOwner->GetTransform().GetTranslation());
	mOwner->SetTransform(transform);
}

void DeadState::StateExit()
{
	GetSceneMsg scene;
	mOwner->GetScene(&scene);
	scene.mScene->ClearFlags(SNF_Hidden, 0, true);
}

void DeadState::StateUpdate()
{

}
