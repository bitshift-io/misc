#ifndef _BLIMPFIRESTATE_H_
#define _BLIMPFIRESTATE_H_

#include "Core/Entity.h"
#include "Pawn/PawnState.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"

//
// General dead state
//
class BlimpFireState : public PawnState
{
public:

	TYPE(5)
	USES_METADATA
	typedef PawnState Super;

	virtual void		Init(Pawn* owner);
	virtual void		StateUpdate();

protected:

	float	mYaw;
	float	mRotationSpeed;
	float	mRotationSpeedDir;
};


#endif
