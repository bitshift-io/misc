#ifndef _FLYSTATE_H_
#define _FLYSTATE_H_

#include "Core/Entity.h"
#include "Pawn/PawnState.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"

//
// General dead state
//
class FlyState : public PawnState
{
public:

	TYPE(3)
	USES_METADATA
	typedef PawnState Super;

	FlyState();

	virtual void		Init(Pawn* owner);
	virtual void		Deinit();

	virtual void		StateEnter();
	virtual void		StateExit();
	virtual void		StateUpdate();

	virtual void		Update();

	virtual bool		HandleMsg(Message* msg);
	virtual bool		TakeDamage(TakeDamageMsg* msg);
	virtual bool		VolumeChange(VolumeChangeMsg* msg);
	virtual bool		SetPause(SetPauseMsg* msg);
	virtual bool		CopyPawn(CopyPawnMsg* msg);

	float				GetLinearSpeed()		{ return mLinearSpeed; }
	virtual float		GetRadius()				{ return mRadius; }


protected:

	float				mTimeInState;
	float				mRadius;
	float				mLinearSpeed;
	float				mRotationSpeed;
	float				mMaxRoll;
	float				mRollSpeed;
	
	// scene node to enable in this state, all others will be hidden
	string				mVisibleNodeName;
	SceneNode*			mVisibleNode;
	float				mRoll;
	SoundPool*			mReviveSound;

	ForceFeedbackEffect*	mDamageForceFeedbackEffect;
};


#endif
