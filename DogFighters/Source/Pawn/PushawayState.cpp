#include "StdAfx.h"
#include "PushawayState.h"

CLASS_METADATA_BEGIN(PushawayState)
	CLASS_VARIABLE(PushawayState, bool, mCauseDamage)
	CLASS_VARIABLE(PushawayState, SoundPool*, mReviveSound)	
	CLASS_VARIABLE(PushawayState, ForceFeedbackEffect*, mForceFeedbackEffect)	
CLASS_METADATA_END(PushawayState)

PushawayState::PushawayState() :
	mCauseDamage(false),
	mReviveSound(0),
	mForceFeedbackEffect(0)
{
}

void PushawayState::Init(Pawn* owner)
{
	Super::Init(owner);

	// init sounds
	if (mReviveSound)
	{
		mReviveSound->Init();
		mReviveSound->SetGain(gGame.mSoundVolume);
	}
}

void PushawayState::Deinit()
{
	if (mReviveSound)
	{
		mReviveSound->Deinit();
		delete mReviveSound;
		mReviveSound = 0;
	}
}

void PushawayState::StateUpdate()
{
	if (mForceFeedbackEffect)
	{
		mForceFeedbackEffect->SetJoystick(mOwner->GetController()->GetJoystick());
		mForceFeedbackEffect->Update();
	}

	if (mReviveSound)
		mReviveSound->Update();

	Matrix4 transform = mOwner->GetTransform();
	Vector4 position = transform.GetTranslation();
	Assert(position.y == 0.f);

	// check for collision
	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		// dont collide with self
		// check against alive pawns only
		Pawn* pawn = (*controllerIt)->GetPawn();
		if (!pawn || pawn == mOwner || pawn->IsDead())
			continue;

		float pawnRadius = pawn->GetRadius();
		Vector4 pawnPos = pawn->GetTransform().GetTranslation();
		Assert(pawnPos.y == 0.f);

		Vector4 distVector = position - pawnPos;
		float dist = distVector.Mag();
		dist = dist - (pawnRadius + mOwner->GetCurrentState()->GetRadius());
        if (dist < 0)
        {
			// do revive buddies when u run them over
			if ((*controllerIt)->GetTeam() != 0 
				&& (*controllerIt)->GetTeam() == mOwner->GetController()->GetTeam())
			{
				if (pawn->GetCurrentState()->GetType() == ParachuteState::Type)
				{
					pawn->SetHealthPercent(1.f);
					pawn->SetState(FlyState::Type);

					if (mReviveSound)
					{
						mReviveSound->SetPosition(pawn->GetTransform().GetTranslation());
						mReviveSound->Play();
					}
				}					
			}

			// if im alive and the hit pawn is parachuting, dont pushaway 
			if (mOwner->GetCurrentState()->GetType() == FlyState::Type
				&& pawn->GetCurrentState()->GetType() == ParachuteState::Type)
				continue;

			// check if we are allowed to cause damage to hit pawn
			// this is only if they are flying though!
			if (mCauseDamage && pawn->GetCurrentState()->GetType() == FlyState::Type)
			{
				TakeDamageMsg damageMsg(1.f, mOwner);
				gGame.GetGameModeMgr()->GetGameMode()->TakeDamage(pawn, &damageMsg);
			}

			if (mForceFeedbackEffect)
			{
				mForceFeedbackEffect->Play();
			}

			dist *= 0.5f; // 50% pushoff
			distVector.Normalize();
			position += distVector * -dist;
			Assert(position.y == 0.f);
        }
	}

	// apply new matrix
	transform.SetTranslation(position);
	mOwner->SetTransform(transform);

	Super::StateUpdate();
}

MESSAGE_BEGIN(PushawayState)
	MESSAGE_PASS(SetPause)
	MESSAGE_PASS(VolumeChange)
MESSAGE_SUPER_END()

bool PushawayState::VolumeChange(VolumeChangeMsg* msg)
{
	if (mReviveSound)
		mReviveSound->SetGain(gGame.mSoundVolume);

	return false;
}

bool PushawayState::SetPause(SetPauseMsg* msg)
{
	if (mReviveSound)
		mReviveSound->Stop();

	return false;
}