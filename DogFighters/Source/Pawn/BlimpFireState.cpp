#include "StdAfx.h"
#include "BlimpFireState.h"

CLASS_METADATA_BEGIN(BlimpFireState)
CLASS_VARIABLE(BlimpFireState, float, mRotationSpeed)
CLASS_METADATA_END(BlimpFireState)

void BlimpFireState::Init(Pawn* owner)
{
	Super::Init(owner);
	mYaw = 0.f;

	mRotationSpeedDir = Math::Rand(0.f, 100.f) > 50.f ? -1.f : 1.f;
}

void BlimpFireState::StateUpdate()
{
	// select a human target at random
	Pawn* target = 0;
	int livePlayerCount = 0;

	// check for collision
	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		if ((*controllerIt)->GetType() != PlayerController::Type)
			continue;

		Pawn* pawn = (*controllerIt)->GetPawn();
		if (pawn->IsDead())
			continue;

		++livePlayerCount;
	}

	if (livePlayerCount <= 0)
		return;

	int targetPlayer = rand() % livePlayerCount;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		if ((*controllerIt)->GetType() != PlayerController::Type)
			continue;

		Pawn* pawn = (*controllerIt)->GetPawn();
		if (pawn->IsDead())
			continue;

		if (targetPlayer == 0)
		{
			target = pawn;
			break;
		}

		--targetPlayer;
	}


	Vector4 fireDir = target->GetTransform().GetTranslation() - mOwner->GetTransform().GetTranslation();
	fireDir.Normalize();

	//mYaw += mRotationSpeedDir * Math::DtoR(mRotationSpeed) * gGame.mGameUpdate.GetDeltaTime();

	//Matrix4 transform = mOwner->GetTransform();

	// check for weapon firing
	//if (mOwner->GetController()->GetActionState(CAT_Fire))
	{
		if (!mOwner->GetUseEntity())
			mOwner->SelectItem(0);

		// fire selected weapon
		if (mOwner->GetUseEntity())
		{
			//mYaw = Math::Rand(-Math::PI, Math::PI);

			//Matrix4 rotation(MI_Identity);
			//rotation.RotateY(mYaw);

			//Vector4 position = transform.GetTranslation();
			//transform.SetTranslation(0.f, 0.f, 0.f);
			//transform = transform * rotation;
			//transform.SetTranslation(position);

			//FireMsg fireMsg(mOwner, transform);
			//mOwner->GetUseEntity()->HandleMsg(&fireMsg);

			Vector4 up(VI_Up);
			Matrix4 transform(MI_Identity);
			transform.SetZAxis(fireDir);
			transform.SetYAxis(up);
			Vector4 right = fireDir.Cross(up);
			right.Normalize();
			transform.SetXAxis(right);
			transform.SetTranslation(mOwner->GetTransform().GetTranslation());

			FireMsg fireMsg(mOwner, transform);
			mOwner->GetUseEntity()->HandleMsg(&fireMsg);
		}
	}


	Super::Update();
}
