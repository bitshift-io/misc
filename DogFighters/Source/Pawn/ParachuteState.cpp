#include "StdAfx.h"
#include "ParachuteState.h"

CLASS_METADATA_BEGIN(ParachuteState)
	CLASS_VARIABLE(ParachuteState, float, mLinearSpeed)
	CLASS_VARIABLE(ParachuteState, float, mTimeTillFly)
	CLASS_VARIABLE(ParachuteState, float, mHorizontalMovementSpeed)	
	CLASS_VARIABLE(ParachuteState, float, mVerticalMovementSpeed)		
	CLASS_VARIABLE(ParachuteState, string, mVisibleNodeName)
	CLASS_VARIABLE(ParachuteState, float, mMaxRoll)
	CLASS_VARIABLE(ParachuteState, float, mRollSpeed)
	CLASS_VARIABLE(ParachuteState, float, mWobbleAmount)
	CLASS_VARIABLE(ParachuteState, float, mWobbleSpeed)
	CLASS_VARIABLE(ParachuteState, float, mRadius)
	CLASS_VARIABLE(ParachuteState, float, mShakeTime)
	CLASS_VARIABLE(ParachuteState, float, mShakeStrength)
	CLASS_VARIABLE(ParachuteState, ForceFeedbackEffect*, mDamageForceFeedbackEffect)
CLASS_EXTENDS_METADATA_END(ParachuteState, PawnState)

ParachuteState::ParachuteState() :
	mShakeTime(0.f), 
	mShakeStrength(0.f),
	mDamageForceFeedbackEffect(0)
{
}

void ParachuteState::Init(Pawn* owner)
{
	Super::Init(owner);
	mVisibleNode = mOwner->GetScene()->GetNodeByName(mVisibleNodeName);		
}

void ParachuteState::Deinit()
{
	Super::Deinit();
}

void ParachuteState::StateEnter()
{
	Super::StateEnter();

	// do camera shake
	gGame.GetEnvironment()->mCameraMgr->GetGameCamera()->Shake(mShakeStrength, mShakeTime);

	mTimeInState = 0.f;

	Matrix4 transform(MI_Identity);
	transform.SetTranslation(mOwner->GetTransform().GetTranslation());
	mOwner->SetTransform(transform);

	mOwner->GetScene()->SetFlags(SNF_Hidden, 0, true);
	mVisibleNode->ClearFlags(SNF_Hidden, true, mVisibleNode->GetOwner());
	mRoll = 0.f;
}

void ParachuteState::StateExit()
{
	Super::StateExit();

	mVisibleNode->SetFlags(SNF_Hidden, true, mVisibleNode->GetOwner());
}

void ParachuteState::Update()
{
	Super::Update();

	if (mDamageForceFeedbackEffect && mOwner->GetController())
	{
		mDamageForceFeedbackEffect->SetJoystick(mOwner->GetController()->GetJoystick());
		mDamageForceFeedbackEffect->Update();
	}
}

void ParachuteState::StateUpdate()
{
	Super::StateUpdate();

	if (mDamageForceFeedbackEffect)
		mDamageForceFeedbackEffect->Update();


	mTimeInState += gGame.mGameUpdate.GetDeltaTime();

	if (mTimeInState > mTimeTillFly)
	{
		mOwner->SetState(FlyState::Type);
		mOwner->SetHealthPercent(1.f);
		return;
	}

	CController* controller = mOwner->GetController();
	float horizMovementInputValue = controller->GetActionStateNormalized(CAT_Left) - controller->GetActionStateNormalized(CAT_Right);
	Vector4 horizMovement = Vector4(mHorizontalMovementSpeed, 0.f, 0.f) * horizMovementInputValue;

	float verticalMovementInputValue = controller->GetActionStateNormalized(CAT_Down) - controller->GetActionStateNormalized(CAT_Up);
	Vector4 verticalMovement = Vector4(0.f, 0.f, mVerticalMovementSpeed) * verticalMovementInputValue;

	Matrix4 transform = mOwner->GetTransform();

	// move down
	Vector4 position = transform.GetTranslation();
	position = position - ((Vector4(0.f, 0.f, 1.f) * mLinearSpeed) + horizMovement + verticalMovement) * gGame.mGameUpdate.GetDeltaTime();

	transform.SetIdentity();

	// add some wave/wobble affect for visual candies
	float wobble = ((float)sinf(mTimeInState * mWobbleSpeed) - 0.5f) * Math::DtoR(mWobbleAmount);
    Matrix4 wobbleMat(MI_Identity);
	wobbleMat.RotateY(wobble);

	float rollSpeedRad = Math::DtoR(mRollSpeed);
	float desiredRoll = horizMovementInputValue * Math::DtoR(mMaxRoll);
	float rollDiff = desiredRoll - mRoll;
	if (rollDiff < 0.f)
		mRoll -= Math::Min(rollSpeedRad * gGame.mGameUpdate.GetDeltaTime(), Math::Abs(rollDiff));
	else
		mRoll += Math::Min(rollSpeedRad * gGame.mGameUpdate.GetDeltaTime(), Math::Abs(rollDiff));

	Matrix4 rollMat(MI_Identity);
	rollMat.RotateZ(-mRoll);

	transform = transform * wobbleMat * rollMat;

	// check for border wrap around
	float halfWidth;
	float halfHeight;
	GetHalfWidthHeight(halfWidth, halfHeight, mOwner->GetRadius());
	if (PositionWrapAround(halfWidth, halfHeight, position))
	{
		WarpMsg warp;
		mOwner->HandleMsg(&warp);
	}

	// apply movement
	Assert(position.y == 0.f);
	transform.SetTranslation(position);

	mOwner->SetTransform(transform);
	
	Super::Update();
}

MESSAGE_BEGIN(ParachuteState)
	MESSAGE_PASS(TakeDamage)
MESSAGE_SUPER_END()

bool ParachuteState::TakeDamage(TakeDamageMsg* msg)
{
	if (mOwner->GetHealth() <= 0 || mOwner->GetTimeAlive() <= mOwner->GetSpawnProtectionTime()
		|| mOwner->mDamageTime < mOwner->mDamageProtectionTime)
		return false;

	if (mDamageForceFeedbackEffect)
		mDamageForceFeedbackEffect->Play();

	mOwner->mDamageTime = 0.f;

	msg->mDamageTaken = Math::Min(msg->mDamage, mOwner->GetHealth());
	mOwner->SetHealth(mOwner->GetHealth() - msg->mDamageTaken);

	if (mOwner->GetHealth() <= 0)
	{
		msg->mKilled = true;
		mOwner->SetState(mOwner->GetStateType(ParachuteDyingState::Type));
	}

	return false;
}