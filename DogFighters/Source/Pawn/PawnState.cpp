#include "StdAfx.h"
#include "PawnState.h"

CLASS_METADATA_BEGIN(PawnState)
	CLASS_VARIABLE(PawnState, list<Entity*>, mEntity)
	CLASS_VARIABLE(PawnState, vector<PawnState*>, mState)
CLASS_METADATA_END(PawnState)

void PawnState::Init(Pawn* owner)		
{
	mOwner = owner;
	mParent = owner; 
	
	StateIterator stateIt;
	for (stateIt = StateBegin(); stateIt != StateEnd(); ++stateIt)
		 (*stateIt)->Init(owner); 

	Super::Init();
}

void PawnState::Deinit()				
{
	StateIterator stateIt;
	for (stateIt = StateBegin(); stateIt != StateEnd(); ++stateIt)
		 (*stateIt)->Deinit();

	Super::Deinit();
}

void PawnState::StateEnter()					
{ 
	StateIterator stateIt;
	for (stateIt = StateBegin(); stateIt != StateEnd(); ++stateIt)
		 (*stateIt)->StateEnter();
}

void PawnState::StateExit()	
{ 
	StateIterator stateIt;
	for (stateIt = StateBegin(); stateIt != StateEnd(); ++stateIt)
		 (*stateIt)->StateExit();
}

void PawnState::Update()	
{ 
	StateIterator stateIt;
	for (stateIt = StateBegin(); stateIt != StateEnd(); ++stateIt)
		 (*stateIt)->Update();

	Super::Update();
}

void PawnState::StateUpdate()	
{ 
	StateIterator stateIt;
	for (stateIt = StateBegin(); stateIt != StateEnd(); ++stateIt)
		 (*stateIt)->StateUpdate();
}

bool PawnState::HandleMsg(Message* msg)
{
	switch (msg->GetType())
	{
		MESSAGE_PASS(GetScene)
	}

	StateIterator stateIt;
	for (stateIt = StateBegin(); stateIt != StateEnd(); ++stateIt)
		 (*stateIt)->HandleMsg(msg);

	return Super::HandleMsg(msg);
}

bool PawnState::GetScene(GetSceneMsg* msg)
{
	// forward to parent
	mOwner->HandleMsg(msg);
	return true;
}