#ifndef _ACTOR_H_
#define _ACTOR_H_

#include "Core/Entity.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"
#include "Math/Matrix.h"
#include "Util/StateMachine.h"
#include "Pawn/PawnState.h"

class Scene;
class Inventory;
class Message;
class Controller;
class Mesh;
class UseMsg;
class DebugDrawMsg;
class GetViewMatrixMsg;
class SetColourMsg;
class TakeDamageMsg;
class SpawnMsg;
class CController;
class SceneNode;

//
// The players, AI, humans, network players
//
class Pawn : public Entity, public StateMachine<PawnState>
{
	friend class MoveState;
	friend class FlyState;
	friend class ParachuteState;

public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME
	TYPE(3)

	Pawn() :
		mInventory(0),
		mDamageProtectionTime(0.f)
	{
	}

	virtual void		Init();
	virtual void		Deinit();
	virtual void		Update();
	virtual void		Render();

	void				InitInstanceMaterials(SceneNode* node);

	virtual bool		HandleMsg(Message* msg);
	bool				DrawDebug(DebugDrawMsg* msg);
	bool				GetViewMatrix(GetViewMatrixMsg* msg);
	bool				GetScene(GetSceneMsg* msg);
	bool				SetColour(SetColourMsg* msg);
	bool				Spawn(SpawnMsg* msg);
	bool				GetTransform(GetTransformMsg* msg);
	bool				PawnKilled(PawnKilledMsg* msg);
	bool				CopyPawn(CopyPawnMsg* msg);
	bool				Enable(EnableMsg* msg);
	bool				OverrideMaterial(OverrideMaterialMsg* msg);

	Matrix4				GetViewMatrix();

	CController*		GetController();

	void				SetTransform(const Matrix4& transform);
	Matrix4				GetTransform();
	Matrix4				GetTransformLastFrame();

	Vector4				GetLinearVelocity();

	Entity*				GetUseEntity()						{ return mUseEntity; }
	void				UseEntity(Entity* entity);

	// health / death / damage
	float				GetHealthPercent()					{ return mHealth / mMaxHealth; }
	float				GetHealth()							{ return mHealth; }
	void				SetHealth(float health)				{ mHealth = health; }
	void				SetHealthPercent(float percent);
	bool				IsDead()							{ return mHealth <= 0.f; }

	Inventory*			GetInventory()						{ return mInventory; }

	void				SetItemVisibility(bool visible);
	void				SelectItem(int itemIdx, SceneNode* attachNode = 0);

	PawnState*			GetStateType(int type);

	float				GetRadius()							{ return GetCurrentState()->GetRadius(); }
	float				GetSpawnProtectionTime()			{ return mSpawnProtectionTime; }
	float				GetTimeAlive()						{ return mTimeAlive; }

	Vector4 			GetColour();
	void				SetColour(const Vector4& colour);

	void				SetController(CController* controller);

	// changes the scene, returns the old scene
	Scene*				SetScene(Scene* scene);
	Scene*				GetScene()							{ return mScene; }

	MaterialBase*		GetMaterialBaseOverride()			{ return mColourMaterialBase; }

protected:

	float				mTimeAlive;
	string				mSceneName;
	string				mAnimationName;
	float				mHealth;
	float				mMaxHealth;
	Inventory*			mInventory;
	Scene*				mScene;
	Animation*			mAnimation;
	Entity*				mUseEntity;			// current use entity
	Entity*				mLastUseEntity;
	CController*		mController;
	int					mSelectedItemIndex;
	float				mTimer;
	Matrix4				mTransformLastFrame;
	float				mSpawnProtectionTime;
	float				mDamageProtectionTime;
	float				mDamageTime;
	float				mAnimTime;

	EffectParameterValue*	mColourParam;
	MaterialBase*			mColourMaterialBase;
};

#endif
