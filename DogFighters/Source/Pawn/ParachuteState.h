#ifndef _PARACHUTESTATE_H_
#define _PARACHUTESTATE_H_

#include "Core/Entity.h"
#include "Pawn/PawnState.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"

class ForceFeedbackEffect;

//
// General dead state
//
class ParachuteState : public PawnState
{
public:

	TYPE(2)
	USES_METADATA
	typedef PawnState Super;

	ParachuteState();

	virtual void		Init(Pawn* owner);
	virtual void		Deinit();

	virtual void		Update();

	virtual void		StateEnter();
	virtual void		StateExit();
	virtual void		StateUpdate();
	virtual float		GetRadius()							{ return mRadius; }

	virtual bool		HandleMsg(Message* msg);
	virtual bool		TakeDamage(TakeDamageMsg* msg);

protected:

	float				mRadius;
	float				mLinearSpeed;
	float				mTimeTillFly;
	float				mTimeInState;
	float				mHorizontalMovementSpeed;
	float				mVerticalMovementSpeed;

	float				mMaxRoll;
	float				mRollSpeed;
	float				mRoll;

	float				mWobbleAmount;
	float				mWobbleSpeed;

	// scene node to enable in this state, all others will be hidden
	string				mVisibleNodeName;
	SceneNode*			mVisibleNode;

	float				mShakeTime;
	float				mShakeStrength;

	ForceFeedbackEffect*	mDamageForceFeedbackEffect;
};


#endif
