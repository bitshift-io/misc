#ifndef _PUSHAWAYSTATE_H_
#define _PUSHAWAYSTATE_H_

#include "Core/Entity.h"
#include "Pawn/PawnState.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"

//
// Does push away collision detection
//
class PushawayState : public PawnState
{
public:

	TYPE(6)
	USES_METADATA
	typedef PawnState Super;

	PushawayState();

	virtual void		Init(Pawn* owner);
	virtual void		Deinit();

	virtual void		StateUpdate();

	virtual bool		HandleMsg(Message* msg);
	virtual bool		VolumeChange(VolumeChangeMsg* msg);
	virtual bool		SetPause(SetPauseMsg* msg);

protected:

	bool					mCauseDamage;
	SoundPool*				mReviveSound;
	ForceFeedbackEffect*	mForceFeedbackEffect;
};


#endif
