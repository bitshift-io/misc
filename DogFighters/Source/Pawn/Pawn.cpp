#include "StdAfx.h"
#include "Pawn.h"

VectorParser<PawnState*> pawnStateParser("PawnState*");

CLASS_METADATA_BEGIN(Pawn)
	CLASS_VARIABLE(Pawn, list<Entity*>, mEntity)
	CLASS_VARIABLE(Pawn, Inventory*, mInventory)  // see above note
	CLASS_VARIABLE(Pawn, string, mSceneName)
	CLASS_VARIABLE(Pawn, string, mAnimationName)
	CLASS_VARIABLE(Pawn, float, mMaxHealth)	
	CLASS_VARIABLE(Pawn, vector<PawnState*>, mState)
	CLASS_VARIABLE(Pawn, float, mSpawnProtectionTime)
	CLASS_VARIABLE(Pawn, float, mDamageProtectionTime)	
CLASS_METADATA_END(Pawn)

void Pawn::Init()
{
	mDamageTime = 0;
	mColourParam = 0;
	mColourMaterialBase = 0;
	//mController = 0;
	mUseEntity = false;
	mScene = 0;
	mHealth = 0;
	mTimer = 0.f;

	gGame.GetEnvironment()->mEntityMgr->RegisterRenderable(this);

	// load the model
	gGame.mPrecache.CacheScene(mSceneName.c_str()); // register with precache system if not already loaded
	mScene = gGame.mRenderFactory->LoadScene(&ObjectLoadParams(gGame.mDevice, mSceneName));

	if (mScene)
	{
#if !defined (NO_CLONING)
		InitInstanceMaterials(mScene->GetRootNode());
#endif
		gGame.GetEnvironment()->mRenderMgr->InsertScene(mScene);
	}

	mAnimation = 0;
	if (mAnimationName.length())
	{
		mAnimation = (Animation*)gGame.mRenderFactory->Load(Animation::Type, &ObjectLoadParams(gGame.mDevice, mAnimationName));
		mScene->InsertAnimation(mAnimation);
		mScene->SetAnimation(mAnimation);
		mAnimTime = Math::Rand(0.f, mAnimation->GetLength());
	}

	// set up states
	// init states
	vector<PawnState*>::iterator it;
	for (it = mState.begin(); it != mState.end(); ++it)
	{
		(*it)->Init(this);
	}

/*
	TranslationTable table[] = { 
		{"this", this},
		{0, 0}};
	gMessageMgr.SendStringMessage(mDefinition->mOnInit.GetMessage(mController), table);
*/


	Super::Init();
	
#if !defined (NO_CLONING)
	// notify children of the colour override material
	OverrideMaterialMsg materialOverride(mColourMaterialBase);
	HandleMsg(&materialOverride);
#endif
	
	//Respawn();


	// this shuold be replaced with DeadState in metadata
	//PopState();
	SetState(GetStateType(DeadState::Type));

	SelectItem(0);
}

void Pawn::Deinit()
{
	// detach any used items
	if (mUseEntity)
	{
		UseMsg use(0);
		mUseEntity->HandleMsg(&use);
		mUseEntity = 0;
	}

	Super::Deinit();

	// deinit states
	vector<PawnState*>::iterator it;
	for (it = mState.begin(); it != mState.end(); ++it)
	{
		(*it)->Deinit();
	}

	gGame.mRenderFactory->Release<MaterialBase>(&mColourMaterialBase);
	gGame.GetEnvironment()->mEntityMgr->DeregisterRenderable(this);
	gGame.GetEnvironment()->mRenderMgr->RemoveScene(mScene);
	gGame.mRenderFactory->Release((SceneObject**)&mAnimation);
	gGame.mRenderFactory->ReleaseScene(&mScene);

	if (mInventory)
		delete mInventory;
}

void Pawn::InitInstanceMaterials(SceneNode* node)
{
	// we now need to instance any meshes and materials so we can apply unique colours
	if (node->GetObject())
	{
		switch (node->GetObject()->GetType())
		{
		case Mesh::Type:
			{
				// clone the mesh, so we can clone the materials
				// to apply material specific overrides
				Mesh* mesh = static_cast<Mesh*>(node->GetObject());
				//Mesh* clone = (Mesh*)mesh->Clone();
				Mesh::MeshMatIterator it;
				for (it = mesh->Begin(); it != mesh->End(); ++it)
				{
					if (mColourMaterialBase)
					{/*
						gGame.mRenderFactory->Release<Material>(&it->material);
						//it->material->ReleaseReference();
						mColourMaterial->AddReference();
						it->material = mColourMaterial;*/
						it->material->SetMaterialBase(mColourMaterialBase);
					}
					else
					{
						mColourMaterialBase = gGame.mRenderFactory->Clone<MaterialBase>(it->material->GetMaterialBase());
						mColourParam = mColourMaterialBase->GetCurrentTechnique()->AddParameterByName("colour");

						it->material->SetMaterialBase(mColourMaterialBase);
						//Material* newMaterial = (Material*)it->materialBase->Clone();
						//Material* newMaterial = (Material*)it->material->Clone();
						//gGame.mRenderFactory->Release<Material>(&it->material);
						//it->material->ReleaseReference();

						//mColourParam = newMaterial->GetCurrentTechnique()->AddParameterByName("colour");
						SetColour(Vector4(1.f, 0.f, 0.f, 1.f));
						//it->material = newMaterial;
						//mColourMaterial = newMaterial;
					}
				}
				//node->SetObject(clone);
			}
			break;

		default:
			break;
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
		InitInstanceMaterials(*it);
}

Vector4 Pawn::GetColour()
{
	Vector4 colour(1.f, 1.f, 1.f, 1.f);
	if (mColourParam)
		mColourParam->GetValue(&colour);

	return colour;
}

void Pawn::SetColour(const Vector4& colour)
{
	if (GetController() && strcmpi(GetController()->GetPawnName().c_str(), "PlayerPawn.mdd") == 0)
	{
		int nothing = 0;
	}

	if (mColourParam)
		mColourParam->SetValue(&colour);
}

void Pawn::SetItemVisibility(bool visible)
{
	// notify all children that they belong to us
	int itemCount = 0;
	Iterator child;
	for (child = Begin(); child != End(); ++child, ++itemCount)
	{
		if (visible == false || 
			(visible && itemCount == mSelectedItemIndex))
		{
			UseMsg use(visible ? this : 0, visible, 0);
			(*child)->HandleMsg(&use);
		}
	}
}

void Pawn::SelectItem(int itemIdx, SceneNode* attachNode)
{
	// hide/stop using current item
	if (mUseEntity)
	{
		mLastUseEntity = mUseEntity;
		UseMsg use(0, false, attachNode);
		mUseEntity->HandleMsg(&use);
		mUseEntity = 0;
	}

	// can we get away with this without mSelectedItemIndex?
	vector<Entity*> useableList;
	Iterator child;
	for (child = Begin(); child != End(); ++child)
	{
		// send a message to see if we can use this entity
		GetUseInfoMsg useInfo;
		(*child)->HandleMsg(&useInfo);
		if (useInfo.mCanUse)
		{
			useableList.push_back(*child);
		}
	}

	if (useableList.size() == 0)
		return;

	mSelectedItemIndex = itemIdx % useableList.size();
	if (mSelectedItemIndex < 0)
		mSelectedItemIndex = useableList.size() - 1;

	mUseEntity = useableList[mSelectedItemIndex];
	UseMsg use(this, true, attachNode);
	mUseEntity->HandleMsg(&use);
}

Matrix4 Pawn::GetViewMatrix()
{
	Matrix4 view(MI_Identity);
	view.CreateLookAt(Vector4(0.0f, 400.0f, -10.0f), Vector4(VI_Zero));
	return view;
}

void Pawn::Update()
{
	mDamageTime += gGame.mGameUpdate.GetDeltaTime();

	if (mAnimation)
	{
		mAnimTime += gGame.mGameUpdate.GetDeltaTime();
		if (mAnimTime >= mAnimation->GetLength())
			mAnimTime = 0.f;

		mScene->SetAnimationTime(mAnimTime, mAnimation);
		mScene->ApplyAnimation();
	}

	// grab the previous frame transform before anything updates
	mTransformLastFrame = GetTransform();

	if (GetController())
	{
		if (GetController()->GetActionState(CAT_SelectNext) > 0.f)
			SelectItem(mSelectedItemIndex + 1);

		if (GetController()->GetActionState(CAT_SelectLast) > 0.f)
			SelectItem(mSelectedItemIndex - 1);
	}

	// update alive time
	if (!IsDead())
		mTimeAlive += gGame.mGameUpdate.GetDeltaTime();
	else
		mTimeAlive = 0.f;

	// update states and entities
	GetCurrentState()->StateUpdate();

	vector<PawnState*>::iterator it;
	for	(it = mState.begin(); it != mState.end(); ++it)
		(*it)->Update();

	Entity::Update();


	//TakeDamage(GetController(), 5.0f * gGame.mGameUpdate.GetDeltaTime());

	// temp
	mTimer += gGame.mGameUpdate.GetDeltaTime();
	if (mTimer > 3.0f)
		mTimer = 0.f;
/*
	mScene->SetAnimationTime(mTimer);*/
}

void Pawn::Render()
{/*
	HUD* hud = GetController()->GetHUD();
	if (hud)
		hud->Render();*/
}

void Pawn::SetTransform(const Matrix4& transform)
{
	if (GetCurrentState() != GetStateType(ParachuteDyingState::Type))
	{
		Assert(transform.GetTranslation().y == 0.f);
	}

	mScene->SetTransform(transform);
}

Matrix4 Pawn::GetTransform()
{
	return mScene->GetTransform();
}

Matrix4 Pawn::GetTransformLastFrame()
{
	return mTransformLastFrame;
}

Vector4 Pawn::GetLinearVelocity()
{
	Vector4 currentPos = GetTransform().GetTranslation();
	return (currentPos - mTransformLastFrame.GetTranslation()) / gGame.mGameUpdate.GetDeltaTime();
}

PawnState* Pawn::GetStateType(int type)
{
	vector<PawnState*>::iterator it;
	for (it = mState.begin(); it != mState.end(); ++it)
	{
		if ((*it)->GetType() == type)
			return (*it);
	}

#ifdef _DEBUG
	Log::Print("[Pawn::GetStateType] Failed to find state '%i'\n", type);
#endif
	return 0;
}

void Pawn::UseEntity(Entity* entity)
{
	if (entity == mUseEntity)
		return;

	// un-use old entity
	if (mUseEntity)
	{
		UseMsg use(0);
		mUseEntity->HandleMsg(&use);
	}

	mLastUseEntity = mUseEntity;
	mUseEntity = entity;

	// use new entity
	if (entity)
	{
		UseMsg use(this);
		entity->HandleMsg(&use);		
	}
	else // not using any item, so select a inventory item
	{
		SelectItem(0);
	}	
}

void Pawn::SetHealthPercent(float percent)
{
	mHealth = mMaxHealth * percent;
}

Scene* Pawn::SetScene(Scene* scene)
{
	Scene* old = mScene;
	mScene = scene;
	mScene->SetTransform(old->GetTransform());
	return old;
}

MESSAGE_BEGIN(Pawn)
	//MESSAGE_PASS(DebugDraw)
	//case DebugDrawMsg::Type: { if (DebugDraw(/*static_cast<DebugDrawMsg*>(msg)*/)) return true; } break;
	MESSAGE_PASS(GetViewMatrix)
	MESSAGE_PASS(GetScene)
	MESSAGE_PASS(SetColour)
	MESSAGE_PASS(Spawn)
	MESSAGE_PASS(GetTransform)
	MESSAGE_PASS(PawnKilled)
	MESSAGE_PASS(CopyPawn)
	MESSAGE_PASS(Enable)
	MESSAGE_PASS(OverrideMaterial)

	// forward these messages to all states
	case SetPauseMsg::Type:
	case VolumeChangeMsg::Type:
		{
			vector<PawnState*>::iterator it;
			for (it = mState.begin(); it != mState.end(); ++it)
			{
				(*it)->HandleMsg(msg);
			}
		}
		break;
	}

	if (GetCurrentState())
		GetCurrentState()->HandleMsg(msg);

	return Super::HandleMsg(msg);
}

bool Pawn::OverrideMaterial(OverrideMaterialMsg* msg)
{
	vector<PawnState*>::iterator it;
	for (it = mState.begin(); it != mState.end(); ++it)
	{
		(*it)->HandleMsg(msg);
	}

	return false;
}

bool Pawn::CopyPawn(CopyPawnMsg* msg)
{
	SetTransform(msg->mCopyFrom->GetTransform());
	SetState(msg->mCopyFrom->GetCurrentState()->GetType());

	vector<PawnState*>::iterator it;
	for (it = mState.begin(); it != mState.end(); ++it)
	{
		(*it)->HandleMsg(msg);
	}

	return false;
}

bool Pawn::Enable(EnableMsg* msg)
{
	vector<PawnState*>::iterator it;
	for (it = mState.begin(); it != mState.end(); ++it)
	{
		(*it)->HandleMsg(msg);
	}

	return false;
}

bool Pawn::PawnKilled(PawnKilledMsg* msg)
{
	// this is here as a performance boosting thing, to stop iterating over all children :|
	return true;
}

bool Pawn::GetTransform(GetTransformMsg* msg)
{
	msg->mTransform = GetTransform();
	return true;
}

bool Pawn::Spawn(SpawnMsg* msg)
{
	SetItemVisibility(false);
	SelectItem(0);

	mTimeAlive = 0.f;
	mHealth = mMaxHealth;

	//gGame.GetGameMode()->Respawn(GetController());

	// find a spawn spot - this should be done in the gamemode
	GetPawnSpawnMsg spawn(this, GetController()->GetTeam());
	gMessageMgr.BroadcastMessage(&spawn);
	SetTransform(spawn.mTransform);
	mTransformLastFrame = spawn.mTransform;

	// push first state as the spawn state
	SetState(GetState(0));

	return false; // send message to super/children
}

bool Pawn::SetColour(SetColourMsg* msg)
{
	SetColour(msg->mColour);
	return false; // send message to super/children
}

bool Pawn::GetScene(GetSceneMsg* msg)
{
	msg->mScene = mScene;
	//msg->mRenderFlags = mRenderFlags;
	return true;
}

void Pawn::SetController(CController* controller)
{
	mController = controller;
	SetItemVisibility(true);
}

bool Pawn::GetViewMatrix(GetViewMatrixMsg* msg)
{
	msg->mView = GetViewMatrix();
	return true;
}

bool Pawn::DrawDebug(DebugDrawMsg* msg)
{
	Vector4 pos = GetTransform().GetTranslation();
	Sphere sphere;
	sphere.position = pos;
	sphere.radius = 0.01f;
	gGame.mDevice->DrawSphere(sphere);

	Box box;
	box.min = Vector4(-0.3f,0.f,-0.3f);
	box.max = Vector4(0.3f, 1.8f, 0.3f);
	box.transform = GetTransform();
	gGame.mDevice->DrawBox(box);
	return true;
}

CController* Pawn::GetController()		
{ 
	return mController; 
}
