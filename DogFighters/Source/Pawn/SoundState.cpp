#include "StdAfx.h"
#include "SoundState.h"

CLASS_METADATA_BEGIN(SoundState)
	CLASS_VARIABLE(SoundState, SoundPool*, mEnterStateSound)
	CLASS_VARIABLE(SoundState, SoundPool*, mExitStateSound)
	CLASS_VARIABLE(SoundState, SoundPool*, mUpdateStateSound)
CLASS_EXTENDS_METADATA_END(SoundState, PawnState)

SoundState::SoundState() :
	mEnterStateSound(0),
	mExitStateSound(0),
	mUpdateStateSound(0)
{
}

void SoundState::Init(Pawn* owner)
{
	Super::Init(owner);

	if (mEnterStateSound)
	{
		mEnterStateSound->Init();
		mEnterStateSound->SetGain(gGame.mSoundVolume);
	}

	if (mExitStateSound)
	{
		mExitStateSound->Init();
		mExitStateSound->SetGain(gGame.mSoundVolume);
	}

	if (mUpdateStateSound)
	{
		mUpdateStateSound->Init();
		mUpdateStateSound->SetGain(gGame.mSoundVolume);
	}
}

void SoundState::Deinit()
{
	Super::Deinit();

	if (mEnterStateSound)
	{
		mEnterStateSound->Deinit();
		delete mEnterStateSound;
		mEnterStateSound = 0;
	}

	if (mExitStateSound)
	{
		mExitStateSound->Deinit();
		delete mExitStateSound;
		mExitStateSound = 0;
	}

	if (mUpdateStateSound)
	{
		mUpdateStateSound->Deinit();
		delete mUpdateStateSound;
		mUpdateStateSound = 0;
	}
}

void SoundState::Update()
{
	if (mUpdateStateSound)
		mUpdateStateSound->Update();

	if (mExitStateSound)
		mExitStateSound->Update();

	if (mEnterStateSound)
		mEnterStateSound->Update();
}

void SoundState::StateEnter()
{
	Super::StateEnter();

	if (mEnterStateSound)
	{
		mEnterStateSound->SetPosition(mOwner->GetTransform().GetTranslation());
		mEnterStateSound->Play();
	}

	if (mUpdateStateSound)
	{
		mUpdateStateSound->SetPosition(mOwner->GetTransform().GetTranslation());
		mUpdateStateSound->Play();
	}
}

void SoundState::StateExit()
{
	Super::StateExit();

	if (mUpdateStateSound)
		mUpdateStateSound->Stop();

	if (mExitStateSound)
	{
		mExitStateSound->SetPosition(mOwner->GetTransform().GetTranslation());
		mExitStateSound->Play();
	}
}

void SoundState::StateUpdate()
{
	Super::StateUpdate();

	if (mUpdateStateSound)
	{
		mUpdateStateSound->SetPosition(mOwner->GetTransform().GetTranslation());

		if (!mUpdateStateSound->IsPlaying())
			mUpdateStateSound->Play();
	}
}


MESSAGE_BEGIN(SoundState)
	MESSAGE_PASS(SetPause)
	MESSAGE_PASS(VolumeChange)
MESSAGE_SUPER_END()

bool SoundState::VolumeChange(VolumeChangeMsg* msg)
{
	if (mUpdateStateSound)
		mUpdateStateSound->SetGain(gGame.mSoundVolume);

	if (mExitStateSound)
		mExitStateSound->SetGain(gGame.mSoundVolume);

	if (mEnterStateSound)
		mEnterStateSound->SetGain(gGame.mSoundVolume);

	return false;
}

bool SoundState::SetPause(SetPauseMsg* msg)
{
	if (mUpdateStateSound && msg->mPause)
		mUpdateStateSound->Stop();

	if (mExitStateSound)
		mExitStateSound->Stop();

	if (mEnterStateSound)
		mEnterStateSound->Stop();

	return false;
}	