#ifndef _DEADSTATE_H_
#define _DEADSTATE_H_

#include "Core/Entity.h"
#include "Pawn/PawnState.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"

//
// General dead state
//
class DeadState : public PawnState
{
public:

	TYPE(1)
	USES_METADATA

	DeadState();

	virtual void			StateEnter();
	virtual void			StateExit();
	virtual void			StateUpdate();

protected:

	float	mShakeTime;
	float	mShakeStrength;
};


#endif
