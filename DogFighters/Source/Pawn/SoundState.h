#ifndef _SOUNDSTATE_H_
#define _SOUNDSTATE_H_

#include "Core/Entity.h"
#include "Pawn/PawnState.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"

class SoundPool;

//
// General dead state
//
class SoundState : public PawnState
{
public:

	TYPE(3)
	USES_METADATA
	typedef PawnState Super;

	SoundState();

	virtual void		Init(Pawn* owner);
	virtual void		Deinit();
	virtual void		Update();

	virtual void		StateEnter();
	virtual void		StateExit();
	virtual void		StateUpdate();

	virtual bool		HandleMsg(Message* msg);
	virtual bool		SetPause(SetPauseMsg* msg);
	virtual bool		VolumeChange(VolumeChangeMsg* msg);

protected:

	SoundPool*			mEnterStateSound;
	SoundPool*			mExitStateSound;
	SoundPool*			mUpdateStateSound;
};


#endif
