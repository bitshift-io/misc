#include "StdAfx.h"
#include "FireState.h"

CLASS_METADATA_BEGIN(FireState)
CLASS_METADATA_END(FireState)

void FireState::StateUpdate()
{
	Matrix4 transform = mOwner->GetTransform();

	// check for weapon firing
	if (mOwner->GetController()->GetActionState(CAT_Fire))
	{
		if (!mOwner->GetUseEntity())
			mOwner->SelectItem(0);

		// fire selected weapon
		if (mOwner->GetUseEntity())
		{
			FireMsg fireMsg(mOwner, transform);
			mOwner->GetUseEntity()->HandleMsg(&fireMsg);
		}
	}


	Super::StateUpdate();
}
