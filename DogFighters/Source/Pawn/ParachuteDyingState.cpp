#include "StdAfx.h"
#include "ParachuteDyingState.h"

CLASS_METADATA_BEGIN(ParachuteDyingState)
	CLASS_VARIABLE(ParachuteDyingState, float, mMaxTimeInState)
	CLASS_VARIABLE(ParachuteDyingState, float, mAcceleration)
	CLASS_VARIABLE(ParachuteDyingState, float, mShakeTime)
	CLASS_VARIABLE(ParachuteDyingState, float, mShakeStrength)
	CLASS_VARIABLE(ParachuteDyingState, string, mVisibleNodeName)
CLASS_EXTENDS_METADATA_END(ParachuteDyingState, PawnState)

ParachuteDyingState::ParachuteDyingState() : 
	mShakeTime(0.f), 
	mShakeStrength(0.f)
{
}

void ParachuteDyingState::Init(Pawn* owner)
{
	Super::Init(owner);
	mVisibleNode = mOwner->GetScene()->GetNodeByName(mVisibleNodeName);
}

void ParachuteDyingState::StateEnter()
{
	Super::StateEnter();

	mTimeInState = 0.f;
	mLinearSpeed = 0.f;

	// do camera shake
	gGame.GetEnvironment()->mCameraMgr->GetGameCamera()->Shake(mShakeStrength, mShakeTime);

	mOwner->GetScene()->SetFlags(SNF_Hidden, 0, true);
	mVisibleNode->ClearFlags(SNF_Hidden, true, mVisibleNode->GetOwner());
}

void ParachuteDyingState::StateExit()
{
	Super::StateExit();

	mVisibleNode->SetFlags(SNF_Hidden, true, mVisibleNode->GetOwner());
}

void ParachuteDyingState::StateUpdate()
{
	mTimeInState += gGame.mGameUpdate.GetDeltaTime();

	Matrix4 transform = mOwner->GetTransform();

	// accelerate move down
	mLinearSpeed += gGame.mGameUpdate.GetDeltaTime() * mAcceleration;
	Vector4 position = transform.GetTranslation();
	position = position - Vector4(0.f, mLinearSpeed * gGame.mGameUpdate.GetDeltaTime(), 0.f);
	transform.SetTranslation(position);
	mOwner->SetTransform(transform);

	Super::Update();

	if (mTimeInState >= mMaxTimeInState)
	{
		mOwner->SetState(mOwner->GetStateType(DeadState::Type));
	}
}
