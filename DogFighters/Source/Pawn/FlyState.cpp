#include "StdAfx.h"
#include "FlyState.h"

CLASS_METADATA_BEGIN(FlyState)
	CLASS_VARIABLE(FlyState, float, mLinearSpeed)
	CLASS_VARIABLE(FlyState, float, mRotationSpeed)
	CLASS_VARIABLE(FlyState, float, mMaxRoll)
	CLASS_VARIABLE(FlyState, float, mRollSpeed)
	CLASS_VARIABLE(FlyState, string, mVisibleNodeName)
	CLASS_VARIABLE(FlyState, float, mRadius)
	CLASS_VARIABLE(FlyState, SoundPool*, mReviveSound)	
	CLASS_VARIABLE(FlyState, ForceFeedbackEffect*, mDamageForceFeedbackEffect)
CLASS_EXTENDS_METADATA_END(FlyState, PawnState)

FlyState::FlyState() : 
	mLinearSpeed(1.f), 
	mRotationSpeed(1.f),
	mMaxRoll(90.f),
	mReviveSound(0),
	mDamageForceFeedbackEffect(0)
{
}

void FlyState::Init(Pawn* owner)
{
	Super::Init(owner);
	mVisibleNode = mOwner->GetScene()->GetNodeByName(mVisibleNodeName);

	// init sounds
	if (mReviveSound)
	{
		mReviveSound->Init();
		mReviveSound->SetGain(gGame.mSoundVolume);
	}		
}

void FlyState::Deinit()
{
	Super::Deinit();

	if (mReviveSound)
	{
		mReviveSound->Deinit();
		delete mReviveSound;
		mReviveSound = 0;
	}
}

void FlyState::StateEnter()
{
	Super::StateEnter();

	if (mOwner->GetCurrentState()->GetType() == ParachuteState::Type && mReviveSound)
	{
		mReviveSound->SetPosition(mOwner->GetTransform().GetTranslation());
		mReviveSound->Play();
	}

	mOwner->GetScene()->SetFlags(SNF_Hidden, 0, true);
	mVisibleNode->ClearFlags(SNF_Hidden, true, mVisibleNode->GetOwner());

	mTimeInState = 0.f;

	// get a new rotation
	GetPawnSpawnMsg spawn(mOwner, mOwner->GetController()->GetTeam());
	gMessageMgr.BroadcastMessage(&spawn);
	spawn.mTransform.SetTranslation(mOwner->GetTransform().GetTranslation());
	mOwner->SetTransform(spawn.mTransform);
	mRoll = 0.f;

	Assert(spawn.mTransform.GetZAxis().y == 0.f);
}

void FlyState::StateExit()
{
	Super::StateExit();

	mVisibleNode->SetFlags(SNF_Hidden, true, mVisibleNode->GetOwner());
}

void FlyState::Update()
{
	Super::Update();

	if (mDamageForceFeedbackEffect && mOwner->GetController())
	{
		mDamageForceFeedbackEffect->SetJoystick(mOwner->GetController()->GetJoystick());
		mDamageForceFeedbackEffect->Update();
	}

	if (mReviveSound)
		mReviveSound->Update();
}

void FlyState::StateUpdate()
{
	mTimeInState += gGame.mGameUpdate.GetDeltaTime();

	Matrix4 transform = mOwner->GetTransform();
	Assert(transform.GetZAxis().y == 0.f);

	Vector4 position = transform.GetTranslation();
	transform.SetTranslationZero();

	// this removes any roll from the transform
	Vector4 forward = transform.GetZAxis();
	Vector4 up = Vector4(0.f, 1.f, 0.f);
	Vector4 right = forward.Cross(up);

	transform.SetYAxis(up);
	transform.SetXAxis(right);

	CController* controller = mOwner->GetController();
	if (!controller)
		return;

	float rotationInputValue = (controller->GetActionStateNormalized(CAT_Left) - controller->GetActionStateNormalized(CAT_Right));

	float rollSpeedRad = Math::DtoR(mRollSpeed);
	float desiredRoll = rotationInputValue * Math::DtoR(mMaxRoll);
	float rollDiff = desiredRoll - mRoll;
	if (rollDiff < 0.f)
		mRoll -= Math::Min(rollSpeedRad * gGame.mGameUpdate.GetDeltaTime(), Math::Abs(rollDiff));
	else
		mRoll += Math::Min(rollSpeedRad * gGame.mGameUpdate.GetDeltaTime(), Math::Abs(rollDiff));

	// apply roll and yaw to plane
	Matrix4 yaw(MI_Identity);
	yaw.RotateY(rotationInputValue * mRotationSpeed * gGame.mGameUpdate.GetDeltaTime());

	Matrix4 roll(MI_Identity);
	roll.RotateZ(-mRoll);

	Assert(transform.GetZAxis().y == 0.f);

	transform = roll.Multiply(transform);
	transform = transform.Multiply(yaw);
	
	Assert(transform.GetZAxis().y == 0.f);

	// move forwards
	position = position + transform.GetZAxis() * mLinearSpeed * gGame.mGameUpdate.GetDeltaTime();
	
	// check for border wrap around
	float halfWidth;
	float halfHeight;
	GetHalfWidthHeight(halfWidth, halfHeight, mOwner->GetRadius());
	if (PositionWrapAround(halfWidth, halfHeight, position))
	{
		WarpMsg warp;
		mOwner->HandleMsg(&warp);
	}

	// apply movement
	transform.SetTranslation(position + transform.GetZAxis() * mLinearSpeed * gGame.mGameUpdate.GetDeltaTime());

	// apply new matrix
	Assert(transform.GetZAxis().y == 0.f);
	mOwner->SetTransform(transform);

	Super::StateUpdate();
}

MESSAGE_BEGIN(FlyState)
	MESSAGE_PASS(TakeDamage)
	MESSAGE_PASS(SetPause)
	MESSAGE_PASS(VolumeChange)
	MESSAGE_PASS(CopyPawn)
MESSAGE_SUPER_END()

bool FlyState::VolumeChange(VolumeChangeMsg* msg)
{
	if (mReviveSound)
		mReviveSound->SetGain(gGame.mSoundVolume);

	return false;
}

bool FlyState::SetPause(SetPauseMsg* msg)
{
	if (mReviveSound)
		mReviveSound->Stop();

	return false;
}

bool FlyState::CopyPawn(CopyPawnMsg* msg)
{
	FlyState* flyState = (FlyState*)msg->mCopyFrom->GetStateType(FlyState::Type);
	mRoll = flyState->mRoll;

	// recalc a proper transform
	Matrix4 transform = msg->mCopyFrom->GetTransform();

	Vector4 position = transform.GetTranslation();
	position.y = 0.f;
	transform.SetTranslation(position);

	Vector4 forward = transform.GetZAxis();
	Vector4 up = Vector4(0.f, 1.f, 0.f);
	Vector4 right = forward.Cross(up);
	forward = up.Cross(right);

	transform.SetYAxis(up);
	transform.SetXAxis(right);
	transform.SetZAxis(forward);
	
	mOwner->SetTransform(transform);
	Assert(transform.GetZAxis().y == 0.f);
	Assert(transform.GetTranslation().y == 0.f);

	return false;
}

bool FlyState::TakeDamage(TakeDamageMsg* msg)
{
	if (mOwner->GetHealth() <= 0 || mOwner->GetTimeAlive() <= mOwner->GetSpawnProtectionTime()
		|| mOwner->mDamageTime < mOwner->mDamageProtectionTime)
		return false;

	if (mDamageForceFeedbackEffect)
		mDamageForceFeedbackEffect->Play();

	mOwner->mDamageTime = 0.f;

	msg->mDamageTaken = Math::Min(msg->mDamage, mOwner->GetHealth());
	mOwner->SetHealth(mOwner->GetHealth() - msg->mDamageTaken);

	// if health less than or equal 50% go to parachute state if we have a parachute state
	if (mOwner->GetStateType(ParachuteState::Type) && mOwner->GetHealthPercent() <= 0.5f)
	{
		mOwner->SetState(ParachuteState::Type);
	}

	if (mOwner->GetHealth() <= 0)
	{
		msg->mKilled = true;

		// this suold be replaced with DeadState in metadata
		mOwner->SetState(mOwner->GetStateType(DeadState::Type));
	}

	return false;
}