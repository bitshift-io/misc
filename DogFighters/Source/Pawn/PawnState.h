#ifndef _PAWNSTATE_H_
#define _PAWNSTATE_H_

class Pawn;


//
// this state may be composed of substates
//
class PawnState : public Entity
{
public:

	USES_METADATA

	typedef Entity Super;
	typedef vector<PawnState*>::iterator StateIterator;

	virtual int				GetType() const			{ return -1; }

	virtual void			Init(Pawn* owner);
	virtual void			Deinit();

	virtual void			StateEnter();
	virtual void			StateExit();
	virtual void			StateUpdate(); // state update only occurs on the active state

	// occurs on all states
	virtual void			Update();

	virtual bool			HandleMsg(Message* msg);
	virtual bool			GetScene(GetSceneMsg* msg);

	virtual float			GetRadius()	{ return 0; }

	StateIterator			StateBegin()					{ return mState.begin(); }
	StateIterator			StateEnd()						{ return mState.end(); }

protected:

	Pawn*					mOwner;
	vector<PawnState*>		mState;
};

#endif