#ifndef _FIRESTATE_H_
#define _FIRESTATE_H_

#include "Core/Entity.h"
#include "Pawn/PawnState.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"

//
// General dead state
//
class FireState : public PawnState
{
public:

	TYPE(4)
	USES_METADATA
	typedef PawnState Super;

	virtual void		StateUpdate();

protected:

};


#endif
