#include "StdAfx.h"
#include "MessageMgr.h"
#include "Memory/Memory.h"

Message* MessageMgr::Create(int type) const
{
	vector<CreatorInfo>::const_iterator it;
	for (it = mCreator.begin(); it != mCreator.end(); ++it)
	{
		if (it->type == type)
		{
			Message* obj = it->createFn();
			return obj;
		}
	}

	return 0;
}

Message* MessageMgr::Create(const char* name) const
{
	vector<CreatorInfo>::const_iterator it;
	for (it = mCreator.begin(); it != mCreator.end(); ++it)
	{
		if (strcmpi(it->name, name) == 0)
		{
			Message* obj = it->createFn();
			return obj;
		}
	}

	return 0;
}

int	MessageMgr::GetType(const char* name) const
{
	vector<CreatorInfo>::const_iterator it;
	for (it = mCreator.begin(); it != mCreator.end(); ++it)
	{
		if (strcmpi(it->name, name) == 0)
		{
			return it->type;
		}
	}

	Assert(false); // type not registered
	return 0;
}

void MessageMgr::Release(Message** message)
{
	if (*message == 0)
		return;

	delete *message;
	*message = 0;
}

void MessageMgr::RegisterMessage(FnMessageCreate createFn, int type, const char* name)
{
#ifdef DEBUG
	vector<CreatorInfo>::iterator it;
	for (it = mCreator.begin(); it != mCreator.end(); ++it)
	{
		if (it->type == type)
		{
			Log::Print("TYPE HAS ALREADY BEEN REGISTERED!\n");
		}
	}
#endif

	CreatorInfo info;
	info.createFn = createFn;
	info.type = type;
	info.name = name;
	mCreator.push_back(info);
}

bool MessageMgr::DeregisterReceiver(const MessageReceiverHandle& handle)
{
	// Null handle
	if (MessageReceiverHandle(-1, -1) == handle)
		return false;

	map<int, list<MessageReceiver*> >::iterator recIt;
	for (recIt = mReceiver.begin(); recIt != mReceiver.end(); ++recIt)
	{
		list<MessageReceiver*>::iterator it;
		for (it = recIt->second.begin(); it != recIt->second.end(); ++it)
		{
			if ((*it)->GetMessageHandle() == handle)
			{
				recIt->second.erase(it);
				return true;
			}
		}
	}

	return false;	
}

MessageReceiverHandle MessageMgr::RegisterReceiver(MessageReceiver* receiver, const MessageReceiverHandle& handle)
{
	map<int, int>::const_iterator it = mReceiverIdx.find(handle.channel);
	if (it == mReceiverIdx.end())
		mReceiverIdx[handle.channel] = 0;

	MessageReceiverHandle newHandle = handle;
	if (newHandle.index == -1)
	{
		newHandle.index = ++mReceiverIdx[newHandle.channel];
	}

	mReceiver[newHandle.channel].push_back(receiver);
	return newHandle;
}

MessageReceiver* MessageMgr::GetReceiver(const MessageReceiverHandle& handle)
{
	// Null handle
	if (MessageReceiverHandle(-1, -1) == handle)
		return 0;

	map<int, list<MessageReceiver*> >::iterator recIt;
	for (recIt = mReceiver.begin(); recIt != mReceiver.end(); ++recIt)
	{
		list<MessageReceiver*>::iterator it;
		for (it = recIt->second.begin(); it != recIt->second.end(); ++it)
		{
			if ((*it)->GetMessageHandle() == handle)
			{
				return *it;
			}
		}
	}

	return 0;
}

MessageReceiver* MessageMgr::GetReceiver(const char* name)
{
	map<int, list<MessageReceiver*> >::iterator recIt;
	for (recIt = mReceiver.begin(); recIt != mReceiver.end(); ++recIt)
	{
		list<MessageReceiver*>::iterator it;
		for (it = recIt->second.begin(); it != recIt->second.end(); ++it)
		{
			if (strcmpi((*it)->GetName(), name) == 0)
			{
				return *it;
			}
		}
	}

	return 0;
}

bool MessageMgr::SendStringMessage(const string& commandLine, TranslationTable* translationTable)
{
	vector<string> commands = ScriptFile::Tokenize(commandLine, ";", true);
	vector<string>::iterator it;
	for (it = commands.begin(); it != commands.end(); ++it)
	{
		if (!SendMessage(*it, translationTable))
			return false;
	}

	return true;
}

bool MessageMgr::SendMessage(const string& command, TranslationTable* translationTable)
{
	char receiverName[256];
	char messageName[256];
	char parameters[2048] = {'\0'};

	// do this manually for speed purposes

	// get the receiver name
	int len = command.length();
	int i = 0;
	for (; i < len; ++i)
	{
		if (command[i] == '.')
		{
			memcpy(&receiverName, command.c_str(), i);
			receiverName[i] = '\0';
			break;
		}
	}
	if (i == len)
		return false;

	// get the message name
	++i;
	int j = i;
	for (; j < len; ++j)
	{
		if (command[j] == '(')
			break;
	}

	memcpy(&messageName, command.c_str() + i, j - i);
	messageName[j - i] = '\0';

	vector<string> params;
	if (j < len)
	{
		++j;
		memcpy(&parameters, command.c_str() + j, len - 1);
		parameters[(len - 1) - j] = '\0';
		params = ScriptFile::Tokenize(parameters, ",", true);
	}

	// now send away!
	Message* msg = gMessageMgr.Create(messageName);
	if (!msg || !msg->ConvertFromString(params, translationTable))
	{
		gMessageMgr.Release(&msg);
		Log::Error("Failed to convert message from string: %s\n", messageName);
		return false;
	}

	if (strcmpi(receiverName, "broadcast") == 0)
	{
		BroadcastMessage(msg);
	}
	else
	{
		MessageReceiver* receiver = 0;
		if (msg->RequiresTranslation(receiverName))
			receiver = (MessageReceiver*)msg->TranslateAddress(receiverName, translationTable);
		else
			receiver = gMessageMgr.GetReceiver(receiverName);

		if (!receiver)
		{
			Log::Error("Failed to find message receiver: %s\n", receiverName);
			gMessageMgr.Release(&msg);
			return false;
		}

		receiver->HandleMsg(msg);
	}

	gMessageMgr.Release(&msg);
	return true;
}

void MessageMgr::BroadcastMessage(Message* msg)
{
	map<int, list<MessageReceiver*> >::iterator recIt;
	for (recIt = mReceiver.begin(); recIt != mReceiver.end(); ++recIt)
	{
		list<MessageReceiver*>::iterator it;
		for (it = recIt->second.begin(); it != recIt->second.end(); ++it)
		{
			(*it)->HandleMsg(msg);
		}
	}
}

void MessageMgr::ResetReceiverChannel(int channel)
{
	if (channel == -1)
	{
		// reset all channels
	}
	else
	{
		mReceiverIdx[channel] = 0;
	}
}
