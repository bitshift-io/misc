#include "StdAfx.h"
#include "Entity.h"

ListParser<Entity*> entityParser("Entity*");
/*
CLASS_METADATA_BEGIN(Entity)
	CLASS_VARIABLE(Entity, list<Entity*>, mEntity)
CLASS_METADATA_END(Entity)
*/

Entity::Entity() : 
	mParent(0)
{
}

Entity* Entity::Clone()
{
	Entity* clone = (Entity*)gMetadataMgr.Clone(this, GetClassName());

	// clone children also
	Iterator it;
	for (it = Begin(); it != End(); ++it)
		clone->mEntity.push_back((*it)->Clone());

	return clone;
}

void Entity::Init()
{
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		(*it)->SetParent(this);
		(*it)->Init();
	}

	if (!mParent)
		gGame.GetEnvironment()->mEntityMgr->Register(this, true, true);
}

void Entity::Deinit()
{
	Iterator it;
	for (it = Begin(); it != End(); )
	{
		(*it)->Deinit();
		delete (*it);
		it = mEntity.erase(it);
	}

	if (!mParent)
		gGame.GetEnvironment()->mEntityMgr->Deregister(this, true, true);
}

void Entity::Update()
{
	Iterator it;
	for (it = Begin(); it != End(); ++it)
		(*it)->Update();
}

bool Entity::HandleMsg(Message* msg)
{
	switch (msg->GetType())
	{
		MESSAGE_PASS(ResolveByType)
	}

	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		if ((*it)->HandleMsg(msg))
			return true;
	}

	return false;
}

bool Entity::ResolveByType(ResolveByTypeMsg* msg)
{
	if (msg->mEntity)
		return true;

	msg->mEntity = GetEntity(msg->mType);
	if (msg->mEntity)
		return true;

	return false;
}

Entity* Entity::GetTopMostParent()
{
	if (mParent == 0)
		return this;

	return mParent->GetTopMostParent();
}

Entity* Entity::ResolveUID(int uid)
{
	if (GetUID() == uid)
		return this;

	list<Entity*>::iterator it;
	for (it = mEntity.begin(); it != mEntity.end(); ++it)
	{
		Entity* entity = 0;
		if ((entity = (*it)->ResolveUID(uid)) != 0)
			return entity;
	}

	return 0;
}

bool Entity::HasEntity(Entity* entity)
{
	// see if its in our list
	{
		list<Entity*>::iterator it;
		for (it = mEntity.begin(); it != mEntity.end(); ++it)
		{
			if (*it == entity)
				return true;
		}
	}

	// see if its in our childs list
	{
		list<Entity*>::iterator it;
		for (it = mEntity.begin(); it != mEntity.end(); ++it)
		{
			if ((*it)->HasEntity(entity))
				return true;
		}
	}

	return 0;
}

Entity* Entity::GetEntity(int type, bool recursive)
{
	if (type == -1)
		return 0;

	if (GetType() == type)
		return this;

	list<Entity*>::iterator it;
	for (it = mEntity.begin(); it != mEntity.end(); ++it)
	{
		if ((*it)->GetType() == type)
			return *it;

		Entity* child = (*it)->GetEntity(type);
		if (child)
			return child;
	}

	return 0;
}

bool Entity::AddEntity(Entity* entity)
{
	if (!entity || HasEntity(entity))
		return false;

	mEntity.push_back(entity);
	return true;
}

bool Entity::RemoveEntity(Entity* entity)
{
	if (!entity)
		return false;

	// see if its in our list
	{
		list<Entity*>::iterator it;
		for (it = mEntity.begin(); it != mEntity.end(); ++it)
		{
			if ((*it) == entity)
			{
				mEntity.erase(it);
				return true;
			}
		}
	}

	// see if its in our childs list
	{
		list<Entity*>::iterator it;
		for (it = mEntity.begin(); it != mEntity.end(); ++it)
		{
			if ((*it)->RemoveEntity(entity))
				return true;
		}
	}

	return false;
}