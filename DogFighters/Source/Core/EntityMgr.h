#ifndef _ENTITYMGR_H_
#define _ENTITYMGR_H_

#include <list>
#include "Message.h"

using namespace std;

class Entity;
class Camera;
class Device;

//
// A registry of existing entitys
// you can also create entitys via this interface
//
class EntityMgr : public MessageReceiver
{
public:

	void	Init();
	void	Deinit();

	// received messages are broadcast
	bool	HandleMsg(Message* msg);
	void	Update();

	void	Register(Entity* obj, bool broadcast, bool update);
	void	Deregister(Entity* obj, bool broadcast, bool update);
	void	RegisterRenderable(Entity* obj);
	void	DeregisterRenderable(Entity* obj);

	int		GetNewUID()		{ return ++mCurUID; }

	Entity*	ResolveUID(int uid);

	void	Render(Device* device, Camera* camera, int layer);

protected:

	list<Entity*>			mBroadcast;
	list<Entity*>			mUpdate;
	list<Entity*>			mRender;
	int						mCurUID;
};

#endif

