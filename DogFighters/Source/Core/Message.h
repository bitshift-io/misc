#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#include <string>
#include <vector>
#include <map>
#include "MessageMgr.h"

using namespace std;

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

#define TYPE(t) enum { Type = t }; virtual int GetType() const { return Type; }


#define NAME(_X_)	\
	virtual const char* GetClassName() const	{ return #_X_; }


#define MESSAGE_BEGIN(_c)				\
bool _c::HandleMsg(Message* msg)		\
{										\
	switch (msg->GetType())				\
	{

/*
#define MESSAGE_PASS(_t, _c)			\
case _t::Type:							\
	{									\
		if (_c(msg))					\
			return true;				\
	}									\
	break;								\
*/
#define MESSAGE_PASS(_c) case _c##Msg::Type: { if (_c(static_cast<_c##Msg*>(msg))) return true; } break;

#define MESSAGE_PASS_SIMPLE(_c) case _c##Msg::Type: { if (_c()) return true; } break;


#define MESSAGE_END()					\
	}									\
	return false;						\
}

#define MESSAGE_SUPER_END()				\
	}									\
	return Super::HandleMsg(msg);		\
}


//
// Used to translate a string to a specific value
// eg. $this gets converted to an address
//
struct TranslationTable
{
	const char* name;
	void*		data;
};

class Packet;

//
// Message object
//
class Message
{
public:

	virtual int			GetType() const			{ return -1; }
	virtual const char* GetClassName() const	{ return "unknown"; }


	// used for the script system
	virtual bool	ConvertFromString(vector<string>& params, TranslationTable* translationTable = 0)				{ return true; }

	bool			RequiresTranslation(const string& str)	{ return str[0] == '$'; }

	void*			TranslateAddress(const string& str, TranslationTable* translationTable)
	{
		int idx = 0;
		while (translationTable[idx].name != 0)
		{
			// add 1 to leave off the $ sign
			if (strcmpi(str.c_str() + 1, translationTable[idx].name) == 0)
				return translationTable[idx].data;

			++idx;
		}

		return 0;
	}

	// compress and add the message to a network compatiable format
	virtual bool ConvertToNetwork(Packet* data)				{ return true; }
	virtual bool ConvertFromNetwork(Packet* data)			{ return true; }

protected:

};

class MessageReceiver
{
public:

	void RegisterReceiver(const MessageReceiverHandle& handle = MessageReceiverHandle())
	{
		mHandle = gMessageMgr.RegisterReceiver(this, handle);
	}

	void DeregisterReceiver()
	{
		gMessageMgr.DeregisterReceiver(mHandle);
	}

	MessageReceiverHandle&	GetMessageHandle()			{ return mHandle; }

	virtual bool			HandleMsg(Message* msg)		= 0;
	virtual const char*		GetName()					{ return ""; }

	MessageReceiverHandle	mHandle;
};

#endif

