#ifndef _MESSAGEMGR_H_
#define _MESSAGEMGR_H_

#include <vector>
#include <list>
#include "Template/Singleton.h"

using namespace std;

class Message;
class MessageReceiver;
struct TranslationTable;

typedef Message* (*FnMessageCreate)();

template <class T>
Message* ObjectCreator()
{
	return new T();
}

struct MessageReceiverHandle
{
	MessageReceiverHandle(int _channel = 0, int _index = -1) : channel(_channel), index(_index)
	{
	}

	bool operator == (const MessageReceiverHandle& rhs)
	{
		return channel == rhs.channel && index == rhs.index;
	}

	int channel;
	int index;
};

//
// This class is used to create a message from a script command
// needs to be a singleton so we can register messages
//
// we should probably change this so each class need to register what messages
// the class accepts
// eg. msgMgr->Register(this, MyMessage::Type, this::SomeFunction)
// this would remove the HandleMessage?
//
#define gMessageMgr MessageMgr::GetInstance()

class MessageMgr : public Singleton<MessageMgr>
{
	friend class MessageReceiver;

public:

	Message*				Create(int type) const;
	Message*				Create(const char* name) const;
	int						GetType(const char* name) const;
	void					Release(Message** message);

	void					RegisterMessage(FnMessageCreate createFn, int type, const char* name);
	void					ResetReceiverChannel(int channel = -1);

	MessageReceiver*		GetReceiver(const char* name);
	MessageReceiver*		GetReceiver(const MessageReceiverHandle& handle);

	void					BroadcastMessage(Message* msg); // we may wish to only broadcast to a channel

	//
	// This takes a string of the form:
	// [receiver].[message]:[parameter 0],[parameter 1],...,[parameter n]; [other messages]
	//
	bool					SendStringMessage(const string& commandLine, TranslationTable* translationTable = 0);

protected:

	struct CreatorInfo
	{
		FnMessageCreate	createFn;
		int				type;
		const char*		name;
	};

	bool					DeregisterReceiver(const MessageReceiverHandle& handle);
	MessageReceiverHandle 	RegisterReceiver(MessageReceiver* receiver, const MessageReceiverHandle& handle = MessageReceiverHandle());
	bool					SendMessage(const string& command, TranslationTable* translationTable);

	vector<CreatorInfo>					mCreator;
	map<int, list<MessageReceiver*> >	mReceiver;		// receivers that can accept messages via script
	map<int, int>						mReceiverIdx;	// used for assigning an automatic receiver handle
};

#define RegisterMessageWithMessageMgr(n)									\
class n##Register															\
{																			\
public:																		\
	n##Register()															\
	{																		\
	gMessageMgr.RegisterMessage(ObjectCreator<n##Msg>, n##Msg::Type, #n);	\
	}																		\
} s_##n##Register;

#endif

