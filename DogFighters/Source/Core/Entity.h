#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <list>
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"
#include "Message.h"

using namespace std;

class ResolveByTypeMsg;

#define CLASSNAME	\
	virtual const char* GetClassName() const	{ return mMetaInfo.mName; }

//
// The base for all game objects, deals with message handling and hierachey
//
class Entity : public MessageReceiver
{
public:

	typedef MessageReceiver Super;

	typedef list<Entity*>::iterator Iterator;

	virtual int			GetType() const						{ return -1; }
	virtual const char* GetClassName() const				{ return ""; }


	Entity();

	virtual Entity* 	Clone();

	virtual void		Init();
	virtual void		Deinit();
	virtual void		Update();
	virtual void		Render(int layer)					{}

	virtual bool		HandleMsg(Message* msg);
	virtual bool		ResolveByType(ResolveByTypeMsg* msg);

	virtual Iterator	Begin()								{ return mEntity.begin(); }
	virtual Iterator	End()								{ return mEntity.end(); }
	virtual Iterator	Erase(Iterator it)					{ return mEntity.erase(it); }

	void				SetParent(Entity* parent)			{ mParent = parent; }
	Entity*				GetTopMostParent();
	Entity*				GetParent()							{ return mParent; }

	int					GetUID()							{ return mUID; }
	Entity*				ResolveUID(int uid);


	bool				HasEntity(Entity* entity);
	Entity*				GetEntity(int type, bool recursive = false);
	bool				AddEntity(Entity* entity);
	bool				RemoveEntity(Entity* entity);

protected:

	Entity*				mParent;
	list<Entity*>		mEntity;
	int					mUID;		// for networking
};

#endif
