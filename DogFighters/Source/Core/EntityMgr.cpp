#include "StdAfx.h"
#include "EntityMgr.h"

void EntityMgr::Init()
{
	RegisterReceiver();
	mCurUID = 1000;
}

void EntityMgr::Deinit()
{
	DeregisterReceiver();
}

bool EntityMgr::HandleMsg(Message* msg)
{
	list<Entity*>::iterator it;
	for (it = mBroadcast.begin(); it != mBroadcast.end(); ++it)
	{
		(*it)->HandleMsg(msg);
	}

	return true;
}

void EntityMgr::Update()
{
	list<Entity*>::iterator it;
	for (it = mUpdate.begin(); it != mUpdate.end(); ++it)
	{
		(*it)->Update();
	}
}

void EntityMgr::Register(Entity* obj, bool broadcast, bool update)
{
	if (broadcast)
		mBroadcast.push_back(obj);

	if (update)
		mUpdate.push_back(obj);
}

void EntityMgr::Deregister(Entity* obj, bool broadcast, bool update)
{
	if (broadcast)
		mBroadcast.remove(obj);

	if (update)
		mUpdate.remove(obj);
}

Entity*	EntityMgr::ResolveUID(int uid)
{
	list<Entity*>::iterator it;
	for (it = mUpdate.begin(); it != mUpdate.end(); ++it)
	{
		Entity* entity = (*it)->ResolveUID(uid);
		if (entity)
			return entity;
	}

	return 0;
}

void EntityMgr::RegisterRenderable(Entity* obj)
{
	mRender.push_back(obj);
}

void EntityMgr::DeregisterRenderable(Entity* obj)
{
	mRender.remove(obj);
}

void EntityMgr::Render(Device* device, Camera* camera, int layer)
{
	list<Entity*>::iterator it;
	for (it = mRender.begin(); it != mRender.end(); ++it)
	{
		(*it)->Render(layer);
	}
}