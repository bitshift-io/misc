#ifndef ___STDAFX_H_
#define ___STDAFX_H_

// testing memory leaks stuff
//#define NO_CLONING 1

// memmanager
#include "Memory/NoMemory.h"

// STL
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <algorithm>

using namespace std;

#ifdef WIN32
/*
namespace Win32
{
    // win32 crap
    #define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
    #define NOMINMAX
    #include <windows.h>
}
/*
    #undef min
    #undef max
    //#undef GetObject
    #undef GetClassInfo
    #undef SendMessage
	#undef GetCurrentDirectory
	#undef SetCurrentDirectory
	#undef GetWorldTransform
	#undef SetWorldTransform
*/
    #include <tchar.h>
#endif

//#include <cctype>

#include <stdlib.h>
#include <malloc.h>
#include <memory.h>

#include "Resource.h"

// memmanager
#include "Memory/Memory.h"

// libraries

// more libraries
#include "Render/Window.h"
#include "Render/Device.h"
#include "Render/Font.h"
#include "Render/Renderer.h"
#include "Render/Occluder.h"
#include "Render/Camera.h"
#include "Render/Geometry.h"
#include "Render/Material.h"
#include "Render/ParticleSystem.h"
#include "Render/Shape.h"
#include "Render/SceneObject.h"
#include "Render/Mesh.h"
#include "Render/Spline.h"
#include "Render/RenderTexture.h"
#include "Render/PostEffect.h"
#include "Render/AnimationController.h"

#ifdef OGL
	#include "Render/OGL/OGLRenderFactory.h"
#endif

#ifdef SOFTWARE
	#include "Render/Software/SoftwareRenderFactory.h"
#endif

#include "Flash/Flash.h"

#include "Input/Input.h"

#include "Sound/SoundFactory.h"
#include "Sound/Sound.h"

#include "Network/NetworkFactory.h"
#include "Network/HTTP.h"
#include "Network/Packet.h"
#include "Network/Base64.h"

#include "File/Filesystem.h"
#include "File/Package.h"
#include "File/ScriptFile.h"
#include "File/ScriptClass.h"
#include "File/ScriptVariable.h"
#include "File/Log.h"

#include "Template/Singleton.h"
#include "Template/FrameUpdate.h"
#include "Template/Time.h"
#include "Template/String.h"

#include "Math/Vector.h"
#include "Math/Matrix.h"
#include "Math/Plane.h"
#include "Math/Line.h"
#include "Math/Quaternion.h"

#include "Metadata/EnumMetadata.h"
#include "Metadata/MetadataScript.h"
#include "Metadata/STLParser.h"
#include "Metadata/MetadataMacros.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"
#include "Metadata/MetadataMgr.h"
#include "Metadata/MetadataParser.h"

// game
#include "SoftwareEffect/ShaderFlash.h"
#include "SoftwareEffect/ShaderColourTexture.h"
#include "SoftwareEffect/ShaderColour.h"
#include "SoftwareEffect/ShaderVertexColour.h"

#include "Playback/Playback.h"
#include "Playback/PlaybackStream.h"

#include "Core/Message.h"
#include "Core/MessageMgr.h"

#include "Core/EntityMgr.h"

#include "Game/GameMsg.h"
#include "Game/Game.h"
#include "Game/GameState.h"
#include "Game/InGame.h"
#include "Game/MenuMsg.h"
#include "Game/FlashMenu.h"
#include "Game/MusicPlayer.h"

#include "GameMode/GameModeMgr.h"
#include "GameMode/GameModeMsg.h"
#include "GameMode/GameMode.h"
#include "GameMode/Team.h"

#include "Util/StateMachine.h"
#include "Util/CmdLine.h"
#include "Util/Pool.h"
#include "Util/Precache.h"

#include "Environment/Environment.h"

#include "Render/RenderMgr.h"
#include "Render/HUD.h"

#include "Camera/CameraMgr.h"
#include "Camera/CameraMsg.h"
#include "Camera/FixedCamera.h"

#include "Message/Enable.h"
#include "Message/Debug.h"
#include "Message/Use.h"
#include "Message/Fire.h"
#include "Message/Scene.h"
#include "Message/GiveToActor.h"
#include "Message/Damage.h"
#include "Message/Resolve.h"
#include "Message/ColourMsg.h"
#include "Message/PawnMsg.h"
#include "Message/PoolMsg.h"

#include "Pickup/Pickup.h"
#include "Pickup/PickupMsg.h"
#include "Pickup/ExplosionBlast.h"
#include "Pickup/GiveTeamLife.h"
#include "Pickup/WeaponUpgrade.h"
#include "Pickup/IncreaseFireRate.h"

#include "Environment/Environment.h"
#include "Environment/EnvironmentMsg.h"
#include "Environment/SpawnMsg.h"

#include "Pawn/Pawn.h"
#include "Pawn/PawnState.h"
#include "Pawn/DeadState.h"
#include "Pawn/ParachuteState.h"
#include "Pawn/ParachuteDyingState.h"
#include "Pawn/FlyState.h"
#include "Pawn/SoundState.h"

#include "Controller/Controller.h"
#include "Controller/PlayerController.h"
#include "Controller/AIController.h"

#include "Metadata/MathParser.h"

#include "Util/CameraUtil.h"
#include "Util/SoundPool.h"
#include "Util/ForceFeedbackEffect.h"

#include "Ballistic/Weapon.h"
#include "Ballistic/Projectile.h"
#include "Ballistic/BulletProjectile.h"

#include "Entity/Model.h"
#include "Entity/MsgAnim.h"
#include "Entity/TeamTag.h"

#include "Effect/ParticleEffect.h"
#include "Effect/ModelParticleEffect.h"
#include "Effect/DamageParticleEffect.h"

#endif
