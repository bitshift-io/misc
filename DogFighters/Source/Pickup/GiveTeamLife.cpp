#include "StdAfx.h"
#include "GiveTeamLife.h"

CLASS_METADATA_BEGIN(GiveTeamLife)
CLASS_METADATA_END(GiveTeamLife)

MESSAGE_BEGIN(GiveTeamLife)
	MESSAGE_PASS(Pickup)
MESSAGE_SUPER_END()

bool GiveTeamLife::Pickup(PickupMsg* msg)
{
	Team* team = msg->mReceiver->GetController()->GetTeam();
	if (team)
	{
		// some hud text and player stats
		Pawn* receiver = msg->mReceiver;
		Matrix4 transform = receiver->GetTransform();		
		HUD* hud = gGame.GetGameModeMgr()->GetGameMode()->GetHUD();
		hud->InsertFadingText(hud->WorldToScreen(transform.GetTranslation()), receiver->GetColour(), "+Life");
		if (receiver->GetController()->GetTeam())
			receiver->GetController()->GetTeam()->mPickupCount++;

		++team->mRemaningLives;
	}

	return false;
}