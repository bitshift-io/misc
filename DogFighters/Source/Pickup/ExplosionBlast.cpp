#include "StdAfx.h"
#include "ExplosionBlast.h"

CLASS_METADATA_BEGIN(ExplosionBlast)
	CLASS_VARIABLE(ExplosionBlast, list<Entity*>, mEntity)
	CLASS_VARIABLE(ExplosionBlast, float, mRadius)
	CLASS_VARIABLE(ExplosionBlast, float, mDamage)
	CLASS_VARIABLE(ExplosionBlast, float, mShakeTime)
	CLASS_VARIABLE(ExplosionBlast, float, mShakeStrength)
CLASS_METADATA_END(ExplosionBlast)

void ExplosionBlast::Init()
{
	Super::Init();
	mMsgAnim = (MsgAnim*)GetEntity(MsgAnim::Type, true);
	mPickedUp = false;
}

Entity* ExplosionBlast::Clone()
{
	return Super::Clone();
}

void ExplosionBlast::Update()
{
	Super::Update();

	if (mPickedUp)
	{
		IsEnabledMsg isEnabled;
		mMsgAnim->HandleMsg(&isEnabled);
		if (!isEnabled.mEnabled)
		{
			mPickedUp = false;
			EnableMsg enableMsg(false);
			GetParent()->HandleMsg(&enableMsg);
		}
	}
}

MESSAGE_BEGIN(ExplosionBlast)
	MESSAGE_PASS(Pickup)
MESSAGE_SUPER_END()

bool ExplosionBlast::Pickup(PickupMsg* msg)
{
	mPickedUp = true;

	Pawn* receiver = msg->mReceiver;
	Matrix4 transform = receiver->GetTransform();

	// some hud text and player stats
	if (receiver->GetController()->GetTeam())
		receiver->GetController()->GetTeam()->mPickupCount++;

	// notify model of the colour override material
	OverrideMaterialMsg materialOverride(receiver->GetMaterialBaseOverride());
	HandleMsg(&materialOverride);

	// this triggers the effect part which is setup in metadata
	EnableMsg enable(true);
	HandleMsg(&enable);

	// now kill near by enemies
	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		// dont collide with self
		// check against alive pawns only
		Pawn* pawn = (*controllerIt)->GetPawn();
		if (pawn == receiver || pawn->IsDead())
			continue;

		float pawnRadius = pawn->GetRadius();
		Vector4 pawnPos = pawn->GetTransform().GetTranslation();

		Vector4 distVector = transform.GetTranslation() - pawnPos;
		float dist = distVector.Mag();
        dist = dist - (pawnRadius + mRadius);
        if (dist < 0)
        {
			TakeDamageMsg damageMsg(mDamage, receiver);
            gGame.GetGameModeMgr()->GetGameMode()->TakeDamage(pawn, &damageMsg);
        }
	}

	// do camera shake
	gGame.GetEnvironment()->mCameraMgr->GetGameCamera()->Shake(mShakeStrength, mShakeTime);

	msg->mReturnToPool = false;
	return false;
}