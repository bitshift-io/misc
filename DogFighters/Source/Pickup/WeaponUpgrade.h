#ifndef _WEAPONUPGRADE_H_
#define _WEAPONUPGRADE_H_

class PickupMsg;

//
// message sent to weapon to modify weapon fire delay
//
class WeaponUpgradeMsg : public Message
{
public:

	TYPE(150)

	float	mIncrement;
	float	mMaxFireDelay;
	float	mMinFireDelay;
};

class WeaponUpgrade : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME
	TYPE(163)

	virtual bool HandleMsg(Message* msg);
	virtual bool Pickup(PickupMsg* msg);

protected:
	
	float	mDecrement;
	float	mMinFireDelay;
};

#endif