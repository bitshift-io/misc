#ifndef _PICKUP_H_
#define _PICKUP_H_

#include "Entity/Model.h"

enum PickupFlags
{
	PF_None				= 0,
	PF_DoesntExpire		= 1 << 0,
	PF_PickedUp			= 1 << 1,
};

class Pickup : public Model
{
public:

	typedef Model Super;
	USES_METADATA
	CLASSNAME

	virtual void		Init();
	virtual void		Update();

	virtual bool		Enable(EnableMsg* msg);
	virtual bool		SetTransform(SetTransformMsg* msg);

protected:

	virtual void		UpdateAlive();
	virtual void		UpdateFall();

	float				mAcceleration;
	float				mFallLinearSpeed;

	float				mFallTime;

	float				mExpireTime;
	float				mAliveTime;

	float				mWobbleAmount;
	float				mWobbleSpeed;

	float				mLinearSpeed;

	float				mRadius;

	unsigned int		mFlags;
};

#endif