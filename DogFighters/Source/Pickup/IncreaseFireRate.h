#ifndef _INCREASEFIRERATE_H_
#define _INCREASEFIRERATE_H_

class PickupMsg;

//
// message sent to weapon to modify weapon fire delay
//
class ModifyFireDelayMsg : public Message
{
public:

	TYPE(50)

	float	mIncrement;
	float	mMaxFireDelay;
	float	mMinFireDelay;
};

class IncreaseFireRate : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME
	TYPE(162)

	virtual bool HandleMsg(Message* msg);
	virtual bool Pickup(PickupMsg* msg);

protected:

	float	mDecrement;
	float	mMinFireDelay;
};

#endif