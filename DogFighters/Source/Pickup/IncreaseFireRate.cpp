#include "StdAfx.h"
#include "IncreaseFireRate.h"

RegisterMessageWithMessageMgr(ModifyFireDelay);

CLASS_METADATA_BEGIN(IncreaseFireRate)
	CLASS_VARIABLE(IncreaseFireRate, float, mDecrement)
	CLASS_VARIABLE(IncreaseFireRate, float, mMinFireDelay)
CLASS_METADATA_END(IncreaseFireRate)

MESSAGE_BEGIN(IncreaseFireRate)
	MESSAGE_PASS(Pickup)
MESSAGE_SUPER_END()

bool IncreaseFireRate::Pickup(PickupMsg* msg)
{
	Pawn* receiver = msg->mReceiver;

	ModifyFireDelayMsg modifyFireRateMsg;
	modifyFireRateMsg.mIncrement = -mDecrement;
	modifyFireRateMsg.mMinFireDelay = mMinFireDelay;
	receiver->HandleMsg(&modifyFireRateMsg);

	// some hud text and player stats
	Matrix4 transform = receiver->GetTransform();		
	HUD* hud = gGame.GetGameModeMgr()->GetGameMode()->GetHUD();
	hud->InsertFadingText(hud->WorldToScreen(transform.GetTranslation()), receiver->GetColour(), "+Fire");
	if (receiver->GetController()->GetTeam())
		receiver->GetController()->GetTeam()->mPickupCount++;
	
	return false;
}