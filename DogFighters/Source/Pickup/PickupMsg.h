#ifndef _PICKUPMSG_H_
#define _PICKUPMSG_H_

class PickupMsg : public Message
{
public:

	TYPE(130)
	NAME(PickupMsg)

	PickupMsg() :
		mReceiver(0),
		mReturnToPool(true)
	{
	}

	PickupMsg(Pawn* pawn) :
		mReceiver(pawn),
		mReturnToPool(true)
	{
	}

	Pawn*	mReceiver;
	bool	mReturnToPool;
};

#endif