#ifndef _GIVETEAMLIFE_H_
#define _GIVETEAMLIFE_H_

class PickupMsg;

class GiveTeamLife : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME
	TYPE(161)

	virtual bool HandleMsg(Message* msg);
	virtual bool Pickup(PickupMsg* msg);

protected:
};

#endif