#include "StdAfx.h"
#include "WeaponUpgrade.h"

RegisterMessageWithMessageMgr(WeaponUpgrade);

CLASS_METADATA_BEGIN(WeaponUpgrade)
	CLASS_VARIABLE(WeaponUpgrade, float, mDecrement)
	CLASS_VARIABLE(WeaponUpgrade, float, mMinFireDelay)
CLASS_METADATA_END(WeaponUpgrade)

MESSAGE_BEGIN(WeaponUpgrade)
	MESSAGE_PASS(Pickup)
MESSAGE_SUPER_END()

bool WeaponUpgrade::Pickup(PickupMsg* msg)
{
	Pawn* receiver = msg->mReceiver;
	Team* team = receiver->GetController()->GetTeam();

	// give this pickup to all team members!
	list<CController*>::iterator it;
	for (it = team->mController.begin(); it != team->mController.end(); ++it)
	{
		WeaponUpgradeMsg upgradeMsg;
		upgradeMsg.mIncrement = -mDecrement;
		upgradeMsg.mMinFireDelay = mMinFireDelay;
		(*it)->GetPawn()->HandleMsg(&upgradeMsg);
	}

	// some hud text and player stats
	Matrix4 transform = receiver->GetTransform();		
	HUD* hud = gGame.GetGameModeMgr()->GetGameMode()->GetHUD();
	hud->InsertFadingText(hud->WorldToScreen(transform.GetTranslation()), receiver->GetColour(), "+Weapon");
	receiver->GetController()->GetTeam();
	if (receiver->GetController()->GetTeam())
		receiver->GetController()->GetTeam()->mPickupCount++;

	return false;
}