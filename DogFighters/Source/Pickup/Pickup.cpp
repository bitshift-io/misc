#include "StdAfx.h"
#include "Pickup.h"

CLASS_METADATA_BEGIN(Pickup)
	CLASS_VARIABLE(Pickup, float, mExpireTime)
	CLASS_VARIABLE(Pickup, float, mAcceleration)
	CLASS_VARIABLE(Pickup, float, mFallTime)
	CLASS_VARIABLE(Pickup, float, mWobbleAmount)
	CLASS_VARIABLE(Pickup, float, mWobbleSpeed)
	CLASS_VARIABLE(Pickup, float, mLinearSpeed)
	CLASS_VARIABLE(Pickup, float, mRadius)
CLASS_EXTENDS_METADATA_END(Pickup, Model)

void Pickup::Init()
{
	Super::Init();
	mAliveTime = -1.f;
	mFlags = 0;
	mFallLinearSpeed = 0.f;
}

void Pickup::Update()
{
	if (mAliveTime < 0.f)
		return;

	// if already picked up, just update children
	if (mFlags & PF_PickedUp)
	{
		Super::Update();
		mAliveTime += gGame.mGameUpdate.GetDeltaTime();
		return;
	}

	if (!(mFlags & PF_DoesntExpire) && mAliveTime < mExpireTime)
		UpdateAlive();
	else
		UpdateFall();
}

void Pickup::UpdateAlive()
{
	/*
	if (mAliveTime < 0.f)
		return;

	if (!(mFlags & PF_DoesntExpire) && mAliveTime > mExpireTime)
	{
		mAliveTime = -1.f;

		// return to pool!
		EnableMsg enableMsg(false);
		HandleMsg(&enableMsg);		
	}
*/

	Matrix4 transform = mScene->GetTransform();

	// move up
	Vector4 position = transform.GetTranslation();
	position = position - (Vector4(0.f, 0.f, 1.f) * mLinearSpeed) * gGame.mGameUpdate.GetDeltaTime();

	transform.SetIdentity();


	// add some wave/wobble affect for visual candies
	float wobble = ((float)sinf(mAliveTime * mWobbleSpeed) - 0.5f) * Math::DtoR(mWobbleAmount);
    Matrix4 wobbleMat(MI_Identity);
	wobbleMat.RotateY(wobble);

	transform = transform * wobbleMat;

	// check for border wrap around
	float halfWidth;
	float halfHeight;
	GetHalfWidthHeight(halfWidth, halfHeight, mRadius);
	PositionWrapAround(halfWidth, halfHeight, position);

	// apply movement
	transform.SetTranslation(position);

	mScene->SetTransform(transform);

	// check for collision
	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		// dont collide with self
		// check against alive pawns only
		Pawn* pawn = (*controllerIt)->GetPawn();
		if (pawn->IsDead() || !gGame.GetGameModeMgr()->GetGameMode()->CanPickup(pawn, mEntity.back()->GetType()))
			continue;

		float pawnRadius = pawn->GetRadius();
		Vector4 pawnPos = pawn->GetTransform().GetTranslation();

		Vector4 distVector = position - pawnPos;
		float dist = distVector.Mag();
		dist = dist - (pawnRadius + mRadius);
        if (dist < 0)
        {
			// let children know we have been picked up
			PickupMsg pickupMsg(pawn);
			HandleMsg(&pickupMsg);

			// send to pawn to let them know of the pickup as well!
			pawn->HandleMsg(&pickupMsg);

			++(pawn->GetController()->GetStats()->mPickups);

			// return to pool!
			if (pickupMsg.mReturnToPool)
			{
				EnableMsg enableMsg(false);
				HandleMsg(&enableMsg);
			}
			else
			{
				// picked up, but dont expire as the children need to do something, they will disable us when they are ready
				// so hide the model
				mScene->SetFlags(SNF_Hidden);
				mFlags |= (PF_DoesntExpire | PF_PickedUp);
			}

			break;
        }
	}

	Super::Update();
	mAliveTime += gGame.mGameUpdate.GetDeltaTime();
}

void Pickup::UpdateFall()
{
	if (!(mFlags & PF_DoesntExpire) && mAliveTime > (mExpireTime + mFallTime))
	{
		mAliveTime = -1.f;

		// return to pool!
		EnableMsg enableMsg(false);
		HandleMsg(&enableMsg);		
	}

	Matrix4 transform = mScene->GetTransform();

	// accelerate move down
	mFallLinearSpeed += gGame.mGameUpdate.GetDeltaTime() * mAcceleration;
	Vector4 position = transform.GetTranslation();
	position = position - Vector4(0.f, mFallLinearSpeed * gGame.mGameUpdate.GetDeltaTime(), 0.f);
	transform.SetTranslation(position);
	mScene->SetTransform(transform);

	Super::Update();
	mAliveTime += gGame.mGameUpdate.GetDeltaTime();
}

bool Pickup::Enable(EnableMsg* msg)
{
	if (msg->mEnable)
	{
		// assume the last attachment if of the type of pickup we are
		if (!gGame.GetGameModeMgr()->GetGameMode()->PickupAllowed(mEntity.back()->GetType()))
		{
			return true;
		}

		mFallLinearSpeed = 0.f;
		mAliveTime = 0.f;
	}
	else
	{
		mFlags = 0;
		mAliveTime = -1.f;
	}

	return Super::Enable(msg);
}

bool Pickup::SetTransform(SetTransformMsg* msg)
{
	Matrix4 transform(MI_Identity);
	transform.SetTranslation(msg->mTransform.GetTranslation());
	mScene->SetTransform(transform);
	return true;
}