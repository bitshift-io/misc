#ifndef _EXPLOSIONBLAST_H_
#define _EXPLOSIONBLAST_H_

class PickupMsg;
class MsgAnim;

class ExplosionBlast : public Entity
{
public:

	typedef Entity Super;
	USES_METADATA
	CLASSNAME
	TYPE(160)

	virtual void		Init();
	virtual Entity* 	Clone();
	virtual bool		HandleMsg(Message* msg);
	virtual bool		Pickup(PickupMsg* msg);
	virtual void		Update();

protected:

	MsgAnim* mMsgAnim;

	float	mRadius;
	float	mDamage;
	bool	mPickedUp;
	float	mShakeTime;
	float	mShakeStrength;
};

#endif