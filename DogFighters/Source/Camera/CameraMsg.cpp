#include "StdAfx.h"
#include "CameraMsg.h"

RegisterMessageWithMessageMgr(GetViewMatrix);
RegisterMessageWithMessageMgr(AttachToEntity);
RegisterMessageWithMessageMgr(SetCameraType);
RegisterMessageWithMessageMgr(ShakeCamera);