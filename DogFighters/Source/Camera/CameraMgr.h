#ifndef _CAMERAMGR_H_
#define _CAMERAMGR_H_

#include "Math/Matrix.h"
#include "Core/Message.h"
#include "Core/Entity.h"
#include <list>

using namespace std;

class Entity;
class Camera;
class Input;
class AttachToEntityMsg;
class SetCameraTypeMsg;
class CameraMgr;

// base class for cameras
class GameCamera : public Entity
{
public:

	virtual void Shake(float strength, float time)	{}
};

class CameraMgr : public MessageReceiver
{
public:

	void			Init();
	void			Deinit();
	bool			SetCameraType(SetCameraTypeMsg* msg);
	bool			AttachToEntity(AttachToEntityMsg* msg);
	void			Update();

	bool			HandleMsg(Message* msg);
	const char*		GetName()							{ return "CameraMgr"; }

	Camera*			GetCamera()							{ return mCamera; }
	Entity*			GetTarget()							{ return mTarget; }
	GameCamera*		GetGameCamera()						{ return mActive; }

	void			RegisterGameCamera(GameCamera* camera);
	void			SetGameCamera(GameCamera* camera);

protected:

	Entity*			mTarget;
	GameCamera*		mActive;

	list<GameCamera*>	mGameCamera;
	Camera*				mCamera;
};

#endif


