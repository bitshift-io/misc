#ifndef _FIXEDCAMERA_H_
#define _FIXEDCAMERA_H_

#include "Core/Entity.h"

//
// fixed camera, will not move
//
class FixedCamera : public GameCamera
{
public:

	typedef GameCamera Super;
	USES_METADATA
	CLASSNAME

	FixedCamera();

	virtual void		Init();
	virtual void		Deinit();
	virtual void		Update();

	virtual void		Shake(float strength, float time);

protected:

	float				mNearPlane;
	float				mFarPlane;
	float				mFOV;
	Vector4				mTarget;
	Vector4				mPosition;

	float				mShakeTime;
	float				mShakeStrength;
	Vector4				mShakeOffset;
};


#endif