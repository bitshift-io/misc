#ifndef _CAMERAMESSAGE_H_
#define _CAMERAMESSAGE_H_

#include "Core/MessageMgr.h"
#include "Core/Message.h"
#include "Math/Matrix.h"
#include "CameraMgr.h"

class Entity;

class GetViewMatrixMsg : public Message
{
public:

	TYPE(40)

	Matrix4	mView;
};

class AttachToEntityMsg : public Message
{
public:

	TYPE(41)

	AttachToEntityMsg()
	{
	}

	AttachToEntityMsg(Entity* entity) :
		mEntity(entity)
	{
	}

	virtual bool ConvertFromString(vector<string>& params, TranslationTable* translationTable = 0)
	{
		mEntity = 0;
		mEntity = (Entity*)TranslateAddress(params[0], translationTable);
		return mEntity != 0;
	}

	Entity*	mEntity;
};

class SetCameraTypeMsg : public Message
{
public:

	TYPE(42)

	SetCameraTypeMsg()
	{
	}

	SetCameraTypeMsg(int type) :
		mType(type)
	{
	}

	int mType;
};

class ShakeCameraMsg : public Message
{
public:

	TYPE(43)

	ShakeCameraMsg()
	{
	}

	ShakeCameraMsg(float shakeTime, float shakeStrength) :
		mShakeTime(shakeTime),
		mShakeStrength(shakeStrength)
	{
	}

	float				mShakeTime;
	float				mShakeStrength;
};

#endif
