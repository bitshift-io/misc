#include "StdAfx.h"
#include "CameraMgr.h"

void CameraMgr::Init()
{
	mTarget = 0;
	RegisterReceiver();

	mCamera = (Camera*)gGame.mRenderFactory->Create(Camera::Type);
	mCamera->SetPerspective(60.f, 0.01f, 2000.0f);
	mCamera->SetPosition(Vector4(0.f, 0.f, 0.f));

	mActive = 0;
}

void CameraMgr::Deinit()
{
	// free cameras
	list<GameCamera*>::iterator it;
	for (it = mGameCamera.begin(); it != mGameCamera.end(); ++it)
	{
		//(*it)->Deinit();
		delete *it;
	}

	gGame.mRenderFactory->Release((SceneObject**)&mCamera);
	DeregisterReceiver();
}

void CameraMgr::Update()
{
	mCamera->SetAspectRatio(gGame.mWindow->GetAspectRatio());
	
	if (mActive)
		mActive->Update();
}

void CameraMgr::RegisterGameCamera(GameCamera* camera)
{
	mGameCamera.push_back(camera);

	if (!mActive)
		mActive = camera;
}

void CameraMgr::SetGameCamera(GameCamera* camera)
{
	mActive = camera;
}

MESSAGE_BEGIN(CameraMgr)
	MESSAGE_PASS(AttachToEntity)
	MESSAGE_PASS(SetCameraType)
MESSAGE_END()

bool CameraMgr::AttachToEntity(AttachToEntityMsg* msg)
{
	mTarget = msg->mEntity;
	return true;
}

bool CameraMgr::SetCameraType(SetCameraTypeMsg* msg)
{
	list<GameCamera*>::iterator it;
	for (it = mGameCamera.begin(); it != mGameCamera.end(); ++it)
	{
		if ((*it)->GetType() == msg->mType)
		{
			mActive = (*it);
			return true;
		}
	}

	return true;
}