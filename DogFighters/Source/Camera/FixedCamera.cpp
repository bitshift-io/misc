#include "StdAfx.h"
#include "FixedCamera.h"

CLASS_METADATA_BEGIN(FixedCamera)
	CLASS_VARIABLE(FixedCamera, float, mNearPlane)
	CLASS_VARIABLE(FixedCamera, float, mFarPlane)
	CLASS_VARIABLE(FixedCamera, float, mFOV)
	CLASS_VARIABLE(FixedCamera, Vector4, mPosition)
	CLASS_VARIABLE(FixedCamera, Vector4, mTarget)
CLASS_METADATA_END(FixedCamera)

FixedCamera::FixedCamera() :
	mFOV(45.f),
	mNearPlane(0.01f),
	mFarPlane(1000.f),
	mPosition(0.f, 0.f, -100.f),
	mTarget(0.f, 0.f, 0.f),
	mShakeOffset(VI_Zero),
	mShakeTime(0.f),
	mShakeStrength(0.f)
{
}

void FixedCamera::Init()
{
	gGame.GetEnvironment()->mCameraMgr->RegisterGameCamera(this);
	Camera* camera = gGame.GetEnvironment()->mCameraMgr->GetCamera();
	camera->LookAt(mPosition, mTarget);
	camera->SetPerspective(mFOV, mNearPlane, mFarPlane);
}

void FixedCamera::Deinit()
{
	Super::Deinit();
}

void FixedCamera::Update()
{
	Super::Update();

	// do camera shaking
	Camera* camera = gGame.GetEnvironment()->mCameraMgr->GetCamera();

	float prevShakeTime = mShakeTime;
	mShakeTime -= gGame.mGameUpdate.GetDeltaTime();
	if (mShakeTime <= 0 && prevShakeTime > 0.f)
	{
		camera->SetPosition(mPosition);
		mShakeOffset = Vector4(VI_Zero);
	}
	else if (mShakeTime > 0.f)
	{
		mShakeOffset = Vector4(float(rand() % 1000) / 1000.0f, float(rand() % 1000) / 1000.0f, float(rand() % 1000) / 1000.0f) * mShakeStrength;
		camera->SetPosition(mPosition + mShakeOffset);
		mShakeStrength *= 0.9f;		
	}

	gGame.mSoundFactory->SetListenerPosition(camera->GetPosition());
}

void FixedCamera::Shake(float strength, float time)
{
	if (time <= 0.f || strength <= 0.f)
		return;

	mShakeTime = Math::Max(mShakeTime, time);
	mShakeStrength = Math::Max(mShakeStrength, strength);

	ShakeCameraMsg shakeMsg(strength * 0.4f, time * 0.4f);
	gMessageMgr.BroadcastMessage(&shakeMsg);
}