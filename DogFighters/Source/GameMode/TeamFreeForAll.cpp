#include "StdAfx.h"
#include "TeamFreeForAll.h"

#ifndef DEMO
CLASS_METADATA_BEGIN(TeamFreeForAll)
	CLASS_VARIABLE(TeamFreeForAll, int, mMaxKillCount)	
CLASS_EXTENDS_METADATA_END(TeamFreeForAll, GameMode)
#endif

bool TeamFreeForAll::Init()
{
	return Super::Init();
}

void TeamFreeForAll::Deinit()
{
	Super::Deinit();
}

void TeamFreeForAll::BeginRound()
{
	Super::BeginRound();

	SetState(TFFAGS_None);

	// make 2 teams, team 0 = humans, team 1 = enemys
    Team* team0 = new Team();
	mTeam.push_back(team0);

    Team* team1 = new Team();
    mTeam.push_back(team1);

	// assign to random teams
	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		// set up team colour as first player to join the team
		Team* team = mTeam[Math::Rand(0, 2)];
		if (!team->mMaterialBase)
		{
			team->mMaterialBase = (*controllerIt)->GetPawn()->GetMaterialBaseOverride();
			team->mTeamName = (*controllerIt)->GetPawn()->GetName();
		}

		(*controllerIt)->SetTeam(team);

		SpawnMsg spawnMsg;
		(*controllerIt)->GetPawn()->HandleMsg(&spawnMsg);

		EnableTeamTagMsg enableTeamTagMsg(true);
		(*controllerIt)->GetPawn()->HandleMsg(&enableTeamTagMsg);
	}

	// even up the teams
	// this code makes team 0 the largest team
	// then swaps as many people over to to team 1 to even the teams
	int team0Size = team0->mController.size();
	int team1Size = team1->mController.size();
	if (team1Size > team0Size)
	{
		int tempSize = team0Size;
		team0Size = team1Size;
		team1Size = tempSize;

		Team* tempTeam = team0;
		team0 = team1;
		team1= tempTeam;
	}

	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		if ((*controllerIt)->GetTeam() == team0)
		{
			(*controllerIt)->SetTeam(team1);
			--team0Size;
			++team1Size;
			if (team0Size <= team1Size)
				break;
		}
	}
}

void TeamFreeForAll::EndRound()
{
	mHUD->ClearScoreBoard();

	Team* winningTeam = mTeam[0]->mKillCount > mTeam[1]->mKillCount ? mTeam[0] : mTeam[1];
	char gameMessage[1024];
	sprintf(gameMessage, "%s Team wins the round", winningTeam->mTeamName.c_str());

	vector<CController*> scoreSort;
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		CController* controller = *controllerIt;
		Stats* stats = controller->GetStats();
		stats->mScore = CalculateScore(stats);
		scoreSort.push_back(*controllerIt);
		controller->SetTeam(0);
	}	

	// sort and display scores
	SortScore(scoreSort);
	InsertScore(scoreSort);
	
	mHUD->DisplayScoreBoard(mName.c_str(), gameMessage);

	while (mTeam.size())
	{
		delete mTeam.back();
		mTeam.pop_back();
	}
}

void TeamFreeForAll::TakeDamage(Pawn* attacked, TakeDamageMsg* damageMsg)
{
	// cant kill each other while the score board is up
	if (mGameState == TFFAGS_RoundEnded)
		return;

	if (attacked->GetController()->GetTeam() == damageMsg->mAttacker->GetController()->GetTeam())
		return;

	Super::TakeDamage(attacked, damageMsg);
	if (damageMsg->mKilled)
	{
		// show affect on players score
		HUD* hud = mHUD;
		hud->InsertFadingText(hud->WorldToScreen(damageMsg->mAttacker->GetTransform().GetTranslation()), damageMsg->mAttacker->GetColour(), "+1");

		// broadcast so all know a pawn was killed
		PawnKilledMsg killedMsg(attacked, damageMsg);
		gMessageMgr.BroadcastMessage(&killedMsg);

		// display notification when a team takes the lead
		bool currentlyDrawing = mTeam[0]->mKillCount == mTeam[1]->mKillCount;
		if (currentlyDrawing)
		{
			char gameMessage[1024];
			sprintf(gameMessage, "%s Team takes the lead", damageMsg->mAttacker->GetController()->GetTeam()->mTeamName.c_str());
			hud->Message("score", gameMessage);
		}

		damageMsg->mAttacker->GetController()->GetTeam()->mKillCount++;
		damageMsg->mAttacker->GetController()->GetStats()->mKills++;

		if (damageMsg->mAttacker->GetController()->GetTeam()->mKillCount >= mMaxKillCount)
		{
			SetState(TFFAGS_EndRound);
		}
	}
}

void TeamFreeForAll::Update()
{
	// respawn players in dead state
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		CController* controller = *controllerIt;
		if (controller->GetPawn()->GetCurrentState()->GetType() != DeadState::Type)
			continue;

		SpawnMsg spawnMsg;
		controller->GetPawn()->HandleMsg(&spawnMsg);
	}

	if (mGameState == TFFAGS_EndRound)
	{
		EndRound();
		mGameState = TFFAGS_RoundEnded;
	}

	Super::Update();
}

void TeamFreeForAll::Render()
{
	Super::Render();
}

bool TeamFreeForAll::PickupAllowed(int type)
{
	if (type == GiveTeamLife::Type)
		return false;

	return true;
}

bool TeamFreeForAll::CanPickup(Pawn* pawn, int type)
{
	if (mGameState == TFFAGS_RoundEnded)
		return false;

	return true;
}