#include "StdAfx.h"
#include "Team.h"

Team::Team() :
	mPickupCount(0),
	mKillCount(0),
	mRemaningLives(0),
	mMaterialBase(0)
{

}

Team::~Team()
{
}

int Team::GetPawnAliveCount()
{
	int pawnAliveCount = 0;
	list<CController*>::iterator it;
	for (it = mController.begin(); it != mController.end(); ++it)
	{	
		Pawn* pawn = (*it)->GetPawn();
		if (!pawn->IsDead())
			++pawnAliveCount;				
	}

	return pawnAliveCount;
}