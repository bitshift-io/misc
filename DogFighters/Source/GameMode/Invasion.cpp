#include "StdAfx.h"
#include "Invasion.h"

CLASS_METADATA_BEGIN(Invasion)
	CLASS_VARIABLE(Invasion, int, mInitialTeamLives)
	CLASS_VARIABLE(Invasion, float, mEnemyRespawnDelay)
	CLASS_VARIABLE(Invasion, SoundPool*, mSchwingSound)	
CLASS_EXTENDS_METADATA_END(Invasion, GameMode)

Invasion::Invasion() :
	mSchwingSound(0)
{
}

bool Invasion::Init()
{
	if (mSchwingSound)
	{
		mSchwingSound->Init();
		mSchwingSound->SetGain(gGame.mSoundVolume);
	}

    mWaveCount = 0;
	return Super::Init();
}

void Invasion::Deinit()
{
	if (mSchwingSound)
		mSchwingSound->Deinit();

	Super::Deinit();
}

void Invasion::BeginRound()
{
	Super::BeginRound();

	SetState(IGS_None);

	// make 2 teams, team 0 = humans, team 1 = enemys
    Team* team0 = new Team();
    team0->mRemaningLives = mInitialTeamLives;
	mTeam.push_back(team0);

    Team* team1 = new Team();
    team1->mRemaningLives = 0;
    mTeam.push_back(team1);

	mWaveCount = 0;

	int lives = 1;
	if (gGame.mCmdLine.GetToken("lives", lives))
		mTeam[IT_Player]->mRemaningLives = lives;

	// set team, and respawn dead players
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		(*controllerIt)->SetTeam(mTeam[IT_Player]);
	}

#ifdef DEMO
	// start on wave 3
	for (int i = 0; i < 3; ++i)
		ResetNextWave();
#else
	int wave = 1;
	gGame.mCmdLine.GetToken("wave", wave);

	for (int i = 0; i < wave; ++i)
		ResetNextWave();
#endif

	mStartTime = Time::GetMilliseconds();
}

int Invasion::CalculateScore(Stats* stats)
{
	return GameMode::CalculateScore(stats) * mWaveCount;
}

void Invasion::EndRound()
{
	// this needs to occur when user clicks on a button!
	SubmitHighscore();

	mHUD->ClearScoreBoard();

	vector<CController*> scoreSort;
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); )
	{
		CController* controller = *controllerIt;
		if ((*controllerIt)->GetTeam() == mTeam[IT_Player])
		{
			controller->SetTeam(0);
			Stats* stats = controller->GetStats();
			stats->mScore = CalculateScore(stats);
			scoreSort.push_back(*controllerIt);
			++controllerIt;
			continue;
		}
		
		// remove the enemy players
		controller->SetTeam(0);
		controller->Deinit();
		delete controller;
		controllerIt = gGame.GetGameModeMgr()->mController.erase(controllerIt);
	}	

	// sort and display scores
	SortScore(scoreSort);
	InsertScore(scoreSort);

	while (mTeam.size())
	{
		delete mTeam.back();
		mTeam.pop_back();
	}

	char gameMessage[1024];
	sprintf(gameMessage, "Wave %i", mWaveCount);
	mHUD->DisplayScoreBoard(mName.c_str(), gameMessage);
}

void Invasion::RemoveAllBiplaneEnemies()
{
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); )
	{
		if (strcmpi((*controllerIt)->GetName(), "AIBiplane") == 0 && (*controllerIt)->GetTeam() == mTeam[IT_Enemy])
		{
			// remove the enemy biplanes
			CController* controller = *controllerIt;
			controller->Deinit();
			delete controller;
			controllerIt = gGame.GetGameModeMgr()->mController.erase(controllerIt);
		}
		else
		{
			++controllerIt;
		}
	}
}

void Invasion::ResetNextWave()
{
	mCurEnemyRespawnDelay = mEnemyRespawnDelay;
	++mWaveCount;

	CController* enemyController = 0;

	if (mWaveCount % 20 == 0 && mWaveCount > 0)
	{
		RemoveAllBiplaneEnemies();
		enemyController = gGame.GetGameModeMgr()->InsertController(gGame.GetGameModeMgr()->CreateController("UFOAIController"));
	}
	else if (mWaveCount % 15 == 0 && mWaveCount > 0)
	{
		RemoveAllBiplaneEnemies();
		enemyController = gGame.GetGameModeMgr()->InsertController(gGame.GetGameModeMgr()->CreateController("BlimpAIController"));
	}
	else if (mWaveCount % 10 == 0 && mWaveCount > 0)
	{
		RemoveAllBiplaneEnemies();
		enemyController = gGame.GetGameModeMgr()->InsertController(gGame.GetGameModeMgr()->CreateController("JetAIController"));
	}
	else if (mWaveCount % 5 == 0 && mWaveCount > 0)
	{
		RemoveAllBiplaneEnemies();
		enemyController = gGame.GetGameModeMgr()->InsertController(gGame.GetGameModeMgr()->CreateController("BomberAIController"));
	}
	else
	{
		enemyController = gGame.GetGameModeMgr()->InsertController(gGame.GetGameModeMgr()->CreateController("BiplaneAIController"));
	}

	if (enemyController)
		enemyController->SetTeam(mTeam[IT_Enemy]);

	// respawn dead players (this doesnt cost lives here)
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		if ((*controllerIt)->GetTeam() == mTeam[IT_Player])
		{
			if ((*controllerIt)->GetPawn()->IsDead())
			{
				SpawnMsg spawnMsg;
				(*controllerIt)->GetPawn()->HandleMsg(&spawnMsg);
			}
		}
	}

	mHUD->NotifyWave(mWaveCount);

	if (mSchwingSound)
		mSchwingSound->Play();

	// temporary
	//SubmitHighscore();
}

void Invasion::TakeDamage(Pawn* attacked, TakeDamageMsg* damageMsg)
{
	if (attacked->GetController()->GetTeam() == damageMsg->mAttacker->GetController()->GetTeam())
		return;

	Super::TakeDamage(attacked, damageMsg);
	if (damageMsg->mKilled)
	{
		// only humans score gets shown in invasion
		if (damageMsg->mAttacker->GetController()->GetType() == PlayerController::Type)
		{
			// show affect on players score
			HUD* hud = gGame.GetGameModeMgr()->GetGameMode()->GetHUD();
			hud->InsertFadingText(hud->WorldToScreen(damageMsg->mAttacker->GetTransform().GetTranslation()), damageMsg->mAttacker->GetColour(), "+1");
		}

		// broadcast so all know a pawn was killed
		PawnKilledMsg killedMsg(attacked, damageMsg);
		gMessageMgr.BroadcastMessage(&killedMsg);

		damageMsg->mAttacker->GetController()->GetTeam()->mKillCount++;
		damageMsg->mAttacker->GetController()->GetStats()->mKills++;

		// if all players died, end the round
		if (mTeam[IT_Player] == attacked->GetController()->GetTeam())
		{
			if (attacked->GetController()->GetTeam()->mRemaningLives <= 0 && mTeam[IT_Player]->GetPawnAliveCount() == 0)
			{
				SetState(IGS_EndRound);
				return;
			}
		}
		else
		{
			// check for end of round
			if (mTeam[IT_Enemy]->GetPawnAliveCount() == 0)
				SetState(IGS_ResetNextWave);
		}
	}
}

void Invasion::Update()
{
	mSchwingSound->Update();

	float prevCurenemyRespawnDelay = mCurEnemyRespawnDelay;
	mCurEnemyRespawnDelay -= gGame.mGameUpdate.GetDeltaTime();

	if (mGameState == IGS_ResetNextWave)
	{
#ifdef DEMO
		int wave = 0;
		switch (mWaveCount)
		{
		case 3:
			wave = 8;
			break;

		case 8:
			wave = 13;
			break;

		case 13:
			wave = 18;
			break;

		default:
		case 18:
			SetState(IGS_EndRound);
			return;
		}

		int additionalWaves = wave - mWaveCount;
		for (int i = 0; i < additionalWaves; ++i)
			ResetNextWave();
#else
		ResetNextWave();
#endif
		mGameState = IGS_None;
	}
	else if (mGameState == IGS_EndRound)
	{
		EndRound();
		mGameState = IGS_RoundEnded;
	}
	else if(mGameState == IGS_RoundEnded)
	{
	}
	else
	{
		// respawn dead players
		bool bAtLeastOneDeadPlayer = false;
		vector<CController*>::iterator controllerIt;
		for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
		{
			if (mTeam[IT_Player] == (*controllerIt)->GetTeam() 
				&& (*controllerIt)->GetPawn()->GetCurrentState()->GetType() == DeadState::Type
				&& mTeam[IT_Player]->mRemaningLives > 0)
			{
				bAtLeastOneDeadPlayer = true;

				SpawnMsg spawnMsg;
				(*controllerIt)->GetPawn()->HandleMsg(&spawnMsg);

				// show remaning team lives		
				HUD* hud = gGame.GetGameModeMgr()->GetGameMode()->GetHUD();
				char buffer[256];
				sprintf(buffer, "%i Lives Remaining", mTeam[IT_Player]->mRemaningLives); 
				hud->Message("life", buffer);
				//if (mGongSound)
				//	mGongSound->Play();
			}

			// respawn enemies, this occurs after a delay
			if ((*controllerIt)->GetPawn() && mTeam[IT_Enemy] == (*controllerIt)->GetTeam() && prevCurenemyRespawnDelay >= 0.f && mCurEnemyRespawnDelay < 0.f)
			{
				SpawnMsg spawnMsg;
				(*controllerIt)->GetPawn()->HandleMsg(&spawnMsg);
			}
		}

		if (bAtLeastOneDeadPlayer)
			mTeam[IT_Player]->mRemaningLives--;
	}
	
	Super::Update();
}

void Invasion::Render()
{
	Super::Render();
}

bool Invasion::CanPickup(Pawn* pawn)
{
	// only good guys can pick things up
	return pawn->GetController()->GetTeam() == mTeam[IT_Player];
}

void Invasion::SubmitHighscore()
{
#ifndef DEMO
	// dont submit highscores while cheating!
	if (gGame.mCmdLine.GetToken("wave") || gGame.mCmdLine.GetToken("lives"))
		return;

	// only do this in single player mode
	if (mTeam[IT_Player]->mController.size() != 1)
		return;

	CController* controller = mTeam[IT_Player]->mController.front();
	Stats* stats = controller->GetStats();

	string highscoreURL = "http://www.blackcarbon.net/dogfighters/highscore.php";
	gGame.mCmdLine.GetToken("highscore", highscoreURL);

	unsigned long runTime = Time::GetMilliseconds() - mStartTime;

	// you can get this info by going:
	//$test = new Score();
    //echo serialize($test);
	// in PHP
	char phpSerialize[2048];
	sprintf(phpSerialize, "O:5:\"Score\":7:{s:4:\"hits\";i:%d;s:6:\"deaths\";i:%d;s:10:\"shotsFired\";i:%d;s:7:\"pickups\";i:%d;s:9:\"waveCount\";i:%d;s:4:\"time\";i:%d;s:5:\"score\";i:%d;}",
		stats->mHits, stats->mDeaths, stats->mShotsFired, stats->mPickups, mWaveCount, runTime, CalculateScore(stats));

	string phpSerializeEncoded = base64_encode(phpSerialize, strlen(phpSerialize));

	char buffer[2048];
	sprintf(buffer, "%s?action=submit&content=%s", 
		highscoreURL.c_str(), phpSerializeEncoded.c_str());
	Assert(strlen(buffer) <= 2048)

#ifdef WIN32
	WCHAR wideBuffer[2048];
	MultiByteToWideChar(CP_ACP, 0, buffer, -1, wideBuffer, 2048);

	SHELLEXECUTEINFO info;
    memset(&info, 0 , sizeof(info));
   
    info.cbSize = sizeof(info);
    info.dwHotKey = 0;
    info.cbSize = sizeof(SHELLEXECUTEINFO);
    info.fMask = 0;
    info.hIcon = NULL;
    info.hInstApp = NULL;
    info.hkeyClass = NULL;
    info.hProcess = NULL;
    info.hwnd = NULL;
    info.lpClass = NULL;
    info.lpDirectory = NULL;
    info.lpFile = wideBuffer;
    info.lpIDList = NULL;
    info.lpParameters = NULL;
    info.lpVerb = TEXT("open");
    info.nShow = SW_SHOWNORMAL;       
   
    ShellExecuteEx(&info);
#endif
#endif
}

bool Invasion::VolumeChange(VolumeChangeMsg* msg)
{
	if (mSchwingSound)
		mSchwingSound->SetGain(gGame.mSoundVolume);

	return false;
}

bool Invasion::SetPause(SetPauseMsg* msg)
{
	if (mSchwingSound)
		mSchwingSound->Stop();

	return false;
}	

bool Invasion::CanPickup(Pawn* pawn, int type)
{
	if (mGameState == IGS_RoundEnded)
		return false;

	if (pawn->GetController()->GetTeam() == mTeam[IT_Enemy])
		return false;

	return true;
}