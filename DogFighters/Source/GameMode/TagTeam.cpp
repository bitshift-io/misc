#include "StdAfx.h"
#include "TagTeam.h"

#ifndef DEMO
CLASS_METADATA_BEGIN(TagTeam)
CLASS_EXTENDS_METADATA_END(TagTeam, GameMode)
#endif

bool TagTeam::Init()
{
	return Super::Init();
}

void TagTeam::Deinit()
{
	Super::Deinit();
}

void TagTeam::BeginRound()
{
	Super::BeginRound();

	SetState(TTGS_None);

	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		Team* team = new Team();
		mTeam.push_back(team);
		team->mMaterialBase = (*controllerIt)->GetPawn()->GetMaterialBaseOverride();
		team->mTeamName = (*controllerIt)->GetName();
		(*controllerIt)->SetTeam(team);

		SpawnMsg spawnMsg;
		(*controllerIt)->GetPawn()->HandleMsg(&spawnMsg);

		EnableTeamTagMsg enableTeamTagMsg(true);
		(*controllerIt)->GetPawn()->HandleMsg(&enableTeamTagMsg);
	}
}

void TagTeam::EndRound()
{
	mHUD->ClearScoreBoard();

	CController* winner = 0;
	vector<CController*> scoreSort;
	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		CController* controller = *controllerIt;
		Stats* stats = controller->GetStats();
		scoreSort.push_back(*controllerIt);
		controller->SetTeam(0);

		if (winner == 0 || stats->mScore > winner->GetStats()->mScore)
			winner = controller;
	}

	while (mTeam.size())
	{
		delete mTeam.back();
		mTeam.pop_back();
	}

	// sort and display scores
	SortScore(scoreSort);
	InsertScore(scoreSort);

	char gameMessage[1024];
	sprintf(gameMessage, "%s wins the round", winner->GetName());
	mHUD->DisplayScoreBoard(mName.c_str(), gameMessage);
}

void TagTeam::TakeDamage(Pawn* attacked, TakeDamageMsg* damageMsg)
{
	// cant kill each other while the score board is up
	if (mGameState == TTGS_RoundEnded)
		return;

	if (attacked->GetController()->GetTeam() == damageMsg->mAttacker->GetController()->GetTeam())
		return;

	Super::TakeDamage(attacked, damageMsg);
	if (damageMsg->mKilled)
	{
		// show affect on players score
		HUD* hud = mHUD;
		hud->InsertFadingText(hud->WorldToScreen(damageMsg->mAttacker->GetTransform().GetTranslation()), damageMsg->mAttacker->GetColour(), "+1");

		Team* newTeam = damageMsg->mAttacker->GetController()->GetTeam();

		// see if this is the winning team
		bool bWasWinningTeam = true;
		vector<Team*>::iterator it;
		for (it = mTeam.begin(); it != mTeam.end(); ++it)
		{
			if (*it == newTeam)
				continue;
			
			if (newTeam->mController.size() <= (*it)->mController.size())
				bWasWinningTeam = false;
		}

		attacked->GetController()->SetTeam(newTeam);

		// broadcast so all know a pawn was killed
		PawnKilledMsg killedMsg(attacked, damageMsg);
		gMessageMgr.BroadcastMessage(&killedMsg);

		damageMsg->mAttacker->GetController()->GetTeam()->mKillCount++;
		damageMsg->mAttacker->GetController()->GetStats()->mKills++;

		// give every one on the newTeam a point
		{
			list<CController*>::iterator it;
			for (it = newTeam->mController.begin(); it != newTeam->mController.end(); ++it)
			{
				(*it)->GetStats()->mScore++;
			}
		}

		attacked->GetController()->GetStats()->mScore = 0; // reset score! for attacked

		// if all players on the same team, end round
		// determine if this team is now got the most players, if so notify users
		bool bNowWinningTeam = true;
		
		int teamsWithPlayers = 0;
		for (it = mTeam.begin(); it != mTeam.end(); ++it)
		{
			if ((*it)->mController.size())
				++teamsWithPlayers;

			if (*it == newTeam)
				continue;

			// are we actually winning?
			if (newTeam->mController.size() <= (*it)->mController.size())
				bNowWinningTeam = false;
		}

		if (bNowWinningTeam && !bWasWinningTeam)
		{
			char buffer[256];
			sprintf(buffer, "%s Team has taken the lead", newTeam->mTeamName.c_str());
			hud->Message("score", buffer);
		}

		if (teamsWithPlayers <= 1)
			SetState(TTGS_EndRound);
	}
}

void TagTeam::Update()
{
	// respawn players in dead state
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		CController* controller = *controllerIt;
		if (controller->GetPawn()->GetCurrentState()->GetType() != DeadState::Type)
			continue;

		SpawnMsg spawnMsg;
		controller->GetPawn()->HandleMsg(&spawnMsg);
	}

	if (mGameState == TTGS_EndRound)
	{
		EndRound();
		mGameState = TTGS_RoundEnded;
	}

	Super::Update();
}

void TagTeam::Render()
{
	Super::Render();
}

bool TagTeam::PickupAllowed(int type)
{
	if (type == GiveTeamLife::Type)
		return false;

	return true;
}

bool TagTeam::CanPickup(Pawn* pawn, int type)
{
	if (mGameState == TTGS_RoundEnded)
		return false;

	return true;
}