#ifndef _FREEFORALL_H_
#define _FREEFORALL_H_

#include "GameMode/GameMode.h"


enum TeamFreeForAllGameState
{
	TFFAGS_None,
	TFFAGS_EndRound,
	TFFAGS_RoundEnded,
	TFFAGS_ResetRound,
};

//
// dont need this class any more, its redundant, it should be merged with gamemode
// dm is just each player on thier own team :)
//
class TeamFreeForAll : public GameMode
{
public:
#ifndef DEMO
	USES_METADATA
#endif
	typedef GameMode Super;

	virtual bool			Init();
	virtual void			Deinit();

	virtual void			BeginRound();
	virtual void			EndRound();

	virtual void			TakeDamage(Pawn* attacked, TakeDamageMsg* damageMsg);

	virtual void			Update();
	virtual void			Render();

	virtual bool			PickupAllowed(int type);
	virtual bool			CanPickup(Pawn* pawn, int type);

protected:

	int						mMaxKillCount;
};

#endif
