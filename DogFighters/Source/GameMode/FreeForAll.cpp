#include "StdAfx.h"
#include "FreeForAll.h"

#ifndef DEMO
CLASS_METADATA_BEGIN(FreeForAll)
	CLASS_VARIABLE(FreeForAll, int, mMaxKillCount)	
CLASS_EXTENDS_METADATA_END(FreeForAll, GameMode)
#endif

bool FreeForAll::Init()
{
	return Super::Init();
}

void FreeForAll::Deinit()
{
	Super::Deinit();
}

void FreeForAll::BeginRound()
{
	Super::BeginRound();

	SetState(FFA_None);

	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		Team* team = new Team();
		mTeam.push_back(team);
		(*controllerIt)->SetTeam(team);

		SpawnMsg spawnMsg;
		(*controllerIt)->GetPawn()->HandleMsg(&spawnMsg);
	}
}

void FreeForAll::EndRound()
{
	mHUD->ClearScoreBoard();

	CController* winner = 0;
	vector<CController*> scoreSort;
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		CController* controller = *controllerIt;
		Stats* stats = controller->GetStats();
		stats->mScore = CalculateScore(stats);
		controller->SetTeam(0);
		scoreSort.push_back(*controllerIt);

		if (winner == 0 || stats->mScore > winner->GetStats()->mScore)
			winner = controller;
	}

	// sort and display scores
	SortScore(scoreSort);
	InsertScore(scoreSort);

	while (mTeam.size())
	{
		delete mTeam.back();
		mTeam.pop_back();
	}

	char gameMessage[1024];
	sprintf(gameMessage, "%s wins the round", winner->GetName());
	mHUD->DisplayScoreBoard(mName.c_str(), gameMessage);
}

void FreeForAll::TakeDamage(Pawn* attacked, TakeDamageMsg* damageMsg)
{
	// cant kill each other while the score board is up
	if (mGameState == FFA_RoundEnded)
		return;

	if (attacked->GetController()->GetTeam() == damageMsg->mAttacker->GetController()->GetTeam())
		return;

	Super::TakeDamage(attacked, damageMsg);
	if (damageMsg->mKilled)
	{
		// show affect on players score
		HUD* hud = mHUD;
		hud->InsertFadingText(hud->WorldToScreen(damageMsg->mAttacker->GetTransform().GetTranslation()), damageMsg->mAttacker->GetColour(), "+1");

		// broadcast so all know a pawn was killed
		PawnKilledMsg killedMsg(attacked, damageMsg);
		gMessageMgr.BroadcastMessage(&killedMsg);

		// find the leading player
		CController* lead = 0;
		vector<CController*>::iterator controllerIt;
		for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
		{
			if ((*controllerIt) == damageMsg->mAttacker->GetController())
				continue;

			if (!lead || (*controllerIt)->GetStats()->mKills >= lead->GetStats()->mKills)
				lead = (*controllerIt);
		}

		damageMsg->mAttacker->GetController()->GetTeam()->mKillCount++;
		damageMsg->mAttacker->GetController()->GetStats()->mKills++;

		// check if we just took the lead
		if (damageMsg->mAttacker->GetController()->GetStats()->mKills == (lead->GetStats()->mKills + 1))
		{
			char buffer[256];
			sprintf(buffer, "%s takes the lead", damageMsg->mAttacker->GetController()->GetName()); 
			hud->Message("score", buffer);
		}

		if (damageMsg->mAttacker->GetController()->GetTeam()->mKillCount >= mMaxKillCount)
		{
			SetState(FFA_EndRound);
			return;
		}
	}
}

void FreeForAll::Update()
{
	// respawn players in dead state
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		CController* controller = *controllerIt;
		if (controller->GetPawn()->GetCurrentState()->GetType() != DeadState::Type)
			continue;

		SpawnMsg spawnMsg;
		controller->GetPawn()->HandleMsg(&spawnMsg);
	}

	if (mGameState == FFA_EndRound)
	{
		EndRound();
		mGameState = FFA_RoundEnded;
	}

	Super::Update();
}

void FreeForAll::Render()
{
	Super::Render();
}

bool FreeForAll::PickupAllowed(int type)
{
	if (type == GiveTeamLife::Type)
		return false;

	return true;
}

bool FreeForAll::CanPickup(Pawn* pawn, int type)
{
	if (mGameState == FFA_RoundEnded)
		return false;

	return true;
}