#ifndef _TEAM_H_
#define _TEAM_H_

class CController;

class Team
{
public:

	Team();
	~Team();

	int					GetPawnAliveCount();

	int					mPickupCount;
	int					mKillCount;
	int					mRemaningLives;
	list<CController*>	mController;
	MaterialBase*		mMaterialBase;
	string				mTeamName;
};

#endif
