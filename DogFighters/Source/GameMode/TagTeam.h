#ifndef _TAGTEAM_H_
#define _TAGTEAM_H_

#include "GameMode/GameMode.h"

class TeamInfo;


enum TagTeamGameState
{
	TTGS_None,
	TTGS_EndRound,
	TTGS_RoundEnded,
};

//
// dont need this class any more, its redundant, it should be merged with gamemode
// dm is just each player on thier own team :)
//
class TagTeam : public GameMode
{
public:
#ifndef DEMO
	USES_METADATA
#endif
	typedef GameMode Super;

	virtual bool			Init();
	virtual void			Deinit();

	virtual void			BeginRound();
	virtual void			EndRound();

	virtual void			TakeDamage(Pawn* attacked, TakeDamageMsg* damageMsg);

	virtual void			Update();
	virtual void			Render();

	virtual bool			PickupAllowed(int type);

	virtual bool			CanPickup(Pawn* pawn, int type);

protected:

	int			mWaveCount;
};

#endif
