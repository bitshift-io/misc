#include "StdAfx.h"
#include "GameModeMsg.h"

RegisterMessageWithMessageMgr(SetGameMode);
RegisterMessageWithMessageMgr(SyncGameMode);
RegisterMessageWithMessageMgr(InsertController);
RegisterMessageWithMessageMgr(BeginRound);
RegisterMessageWithMessageMgr(Reset);
RegisterMessageWithMessageMgr(Restart);

bool SetGameModeMsg::ConvertFromString(vector<string>& params, TranslationTable* translationTable)
{
	if (params.size() <= 0)
		return false;

	mName = params[0];
	return true;
}




bool InsertControllerMsg::ConvertFromString(vector<string>& params, TranslationTable* translationTable)
{
	if (params.size() <= 4)
		return false;

	string& name = params[0];

	float r = float(atoi(params[1].c_str())) / 255.f;
	float g = float(atoi(params[2].c_str())) / 255.f;
	float b = float(atoi(params[3].c_str())) / 255.f;

	MetadataScript* script = gGame.GetGameModeMgr()->mControls;
	mController = gGame.GetGameModeMgr()->CreateController(script, script->GetScriptClass(name));
	mController->GetPawn()->SetColour(Vector4(r, g, b, 1.f));
	mController->SetName(params[4].c_str());
	return true;
}