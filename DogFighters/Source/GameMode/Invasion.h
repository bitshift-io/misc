#ifndef _INVASION_H_
#define _INVASION_H_

#include "GameMode/GameMode.h"

class TeamInfo;


enum InvasionTeam
{
	IT_Player,
	IT_Enemy,	
};

enum InvasionGameState
{
	IGS_None,
	IGS_ResetNextWave,
	IGS_EndRound,
	IGS_RoundEnded,
};

//
// dont need this class any more, its redundant, it should be merged with gamemode
// dm is just each player on thier own team :)
//
class Invasion : public GameMode
{
public:

	USES_METADATA
	typedef GameMode Super;

	Invasion();

	virtual int				CalculateScore(Stats* stats);

	virtual bool			Init();
	virtual void			Deinit();

	virtual void			BeginRound();
	virtual void			EndRound();

	virtual void			TakeDamage(Pawn* attacked, TakeDamageMsg* damageMsg);

	//virtual CController*	JoinGame(ClientConnectType connectType);
	void					ResetNextWave();
	void					RemoveAllBiplaneEnemies();

	virtual void			Update();
	virtual void			Render();

	virtual bool			CanPickup(Pawn* pawn);
	virtual bool			CanPickup(Pawn* pawn, int type);

	void					SubmitHighscore();

	virtual bool			SetPause(SetPauseMsg* msg);
	virtual bool			VolumeChange(VolumeChangeMsg* msg);

protected:

	int						mInitialTeamLives;
	float					mEnemyRespawnDelay;

	float					mCurEnemyRespawnDelay;
	int						mWaveCount;

	unsigned long			mStartTime;
	SoundPool*				mSchwingSound;
};

#endif
