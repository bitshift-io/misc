#ifndef _GAMEMODEMSG_H_
#define _GAMEMODEMSG_H_

#include "Core/Message.h"

class SetGameModeMsg : public Message
{
public:

	TYPE(70)

	SetGameModeMsg()
	{
	}

	SetGameModeMsg(const string& name) :
		mName(name)
	{
	}

	virtual bool	ConvertFromString(vector<string>& params, TranslationTable* translationTable = 0);

#ifdef NETWORK
	virtual bool ConvertToNetwork(Packet* data)
	{
		*data & mName;
		return true;
	}

	virtual bool ConvertFromNetwork(Packet* data)
	{
		*data & mName;
		return true;
	}
#endif

	string			mName;
};

class SyncGameModeMsg : public Message
{
public:

	TYPE(71)

#ifdef NETWORK
	virtual bool ConvertToNetwork(Packet* data)
	{
		*data & mControllerCount;
		return true;
	}

	virtual bool ConvertFromNetwork(Packet* data)
	{
		*data & mControllerCount;
		return true;
	}
#endif

	int	mControllerCount;
};

class InsertControllerMsg : public Message
{
public:

	TYPE(72)

	InsertControllerMsg() :
		mController(0)
	{
	}

	virtual bool	ConvertFromString(vector<string>& params, TranslationTable* translationTable = 0);

	CController*	mController;
};

class BeginRoundMsg : public Message
{
public:

	TYPE(73)
};

class ResetMsg : public Message
{
public:

	TYPE(74)
};

class RestartMsg : public Message
{
public:

	TYPE(75)
};

#endif
