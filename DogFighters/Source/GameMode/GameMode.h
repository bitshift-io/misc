#ifndef _GAMEMODE_H__
#define _GAMEMODE_H__

#include "GameMode/Team.h"
#include "Metadata/ClassMetadata.h"
#include "Metadata/MetadataMacros.h"
#include "Core/Message.h"

#ifdef NETWORK
	#include "Network/NetworkMsg.h"
#endif

#include <vector>
#include <list>

using namespace std;

class SoundPool;
class Pawn;
class CController;
class SyncGameModeMsg;
class HUD;
class TakeDamageMsg;
class Stats;

//
// GameMode
// such as death match, TDM, tag team
// treasure hunt etc...
//
class GameMode : public MessageReceiver/*, public StateMachine<GameState>*/
{
public:

	USES_METADATA

	GameMode();

	virtual int				CalculateScore(Stats* stats);
	virtual void			SortScore(vector<CController*>& controller);
	virtual void			InsertScore(vector<CController*>& controller);

	virtual const char*		GetName()											{ return mName.c_str(); }
	virtual bool			Init();
	virtual void			Deinit();

	// functions called when the game mode begins/end (ie when environment has loaded and when changing levels)
	virtual void			BeginRound();
	virtual void			EndRound();

	// game mode ended mid game
	virtual void			Reset()												{}

	virtual void			TakeDamage(Pawn* attacked, TakeDamageMsg* damageMsg);
	virtual HUD*			GetHUD()																{ return mHUD; }

	// we should really release the hud and pawn in this class also
	virtual HUD*			CreateHUD(CController* controller);
	virtual Pawn*			CreatePawn(CController* controller);
	virtual Pawn*			CreatePawn(const char* pawnName);
	virtual void			ReleaseHUD(HUD** hud);
	virtual void			ReleasePawn(Pawn** pawn);

#ifdef NETWORK
	virtual CController*	JoinGame(ClientConnectType connectType);
	virtual CController*	AddController(ClientConnectType connectType, bool addToFront = false);
#endif

	virtual CController*	AddController(CController* controller, bool addToFront = false);
	
	virtual void			ScoreObjective(CController* scorer, int score)		{}
	virtual void			KillAI(int numToKill)								{}

	virtual bool			HandleMsg(Message* msg);

	virtual bool			SetPause(SetPauseMsg* msg);
	virtual bool			VolumeChange(VolumeChangeMsg* msg);

#ifdef NETWORK
	virtual bool			ClientConnect(ClientConnectMsg* msg);
	virtual bool			SyncGameMode(SyncGameModeMsg* msg);
#endif

	bool					BeginRound(BeginRoundMsg* msg);

	virtual void			Update();
	virtual void			Render();
	virtual const string&	GetHUDName() const									{ return mHUDName; }
	virtual void			AddAI(int count);

	virtual vector<Team*>&		GetTeams()											{ return mTeam; }

	virtual bool			ReloadResource(ReloadResourceMsg* msg);

	void					SetState(int state);

	virtual bool			PickupAllowed(int type)								{ return true; }
	virtual bool			CanPickup(Pawn* pawn, int type)						{ return true; }

protected:

	vector<Team*>			mTeam;
	string					mName;												// name of this game mode
	int						mMaxPlayers;
	string					mHUDName;
	string					mPlayerControllerName;
	string					mNetworkControllerName;
	string					mAIControllerName;
	string					mPawnName;
	list<Pawn*>				mPawn;
	HUD*					mHUD;
	int						mGameState;
	SoundPool*				mGongSound;
};

#endif
