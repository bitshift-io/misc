#include "StdAfx.h"
#include "Rabbit.h"

#ifndef DEMO
RegisterMessageWithMessageMgr(BecomeRabbit);

CLASS_METADATA_BEGIN(Rabbit)
	CLASS_VARIABLE(Rabbit, float, mMaxRabbitTime)	
CLASS_EXTENDS_METADATA_END(Rabbit, GameMode)
#endif

bool Rabbit::Init()
{
	mJetPawn = 0;
	mRabbitOldPawn = 0;
	return Super::Init();
}

void Rabbit::Deinit()
{
	Super::Deinit();
}

void Rabbit::BeginRound()
{
	Super::BeginRound();

	//mMaxRabbitTime = 0.1f; //temp
	SetState(RGS_None);

	// make 2 teams, team 0 = rabbit, team 1 = every one else
    Team* team0 = new Team();
	mTeam.push_back(team0);

    Team* team1 = new Team();
    mTeam.push_back(team1);

	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		Team* team = new Team();
		mTeam.push_back(team);
		(*controllerIt)->SetTeam(team);

		SpawnMsg spawnMsg;
		(*controllerIt)->GetPawn()->HandleMsg(&spawnMsg);
	}
 
	mNewRabbit = 0;
	mRabbitOldPawn = 0;
	mJetPawn = CreatePawn("RabbitPawn.mdd");
}

void Rabbit::Reset()
{
	RestoreRabbitOldPawn();
	ReleasePawn(&mJetPawn);
}

void Rabbit::EndRound()
{
	mHUD->ClearScoreBoard();

	CController* winner = 0;
	vector<CController*> scoreSort;
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		CController* controller = *controllerIt;
		Stats* stats = controller->GetStats();
		stats->mScore = CalculateScore(stats);
		scoreSort.push_back(*controllerIt);
		controller->SetTeam(0);

		if (winner == 0 || stats->mTime > winner->GetStats()->mTime)
			winner = controller;
	}	

	while (mTeam.size())
	{
		delete mTeam.back();
		mTeam.pop_back();
	}

	// sort and display scores
	SortScore(scoreSort);
	InsertScore(scoreSort);

	char gameMessage[1024];
	sprintf(gameMessage, "%s wins the round", winner->GetName());
	mHUD->DisplayScoreBoard(mName.c_str(), gameMessage);
}

void Rabbit::FirstKill(Pawn* attacker)
{
	// put every one on the every one team
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		(*controllerIt)->SetTeam(mTeam[RT_Everyone]);
	}

	// remove all teams except the rabbit teams now
	while (mTeam.size() > 2)
	{
		delete mTeam.back();
		mTeam.pop_back();
	}

	attacker->GetController()->SetTeam(mTeam[RT_Rabbit]);
}

void Rabbit::TakeDamage(Pawn* attacked, TakeDamageMsg* damageMsg)
{
	CController* attackerController = damageMsg->mAttacker->GetController();
	if (!attackerController)
		return;

	// cant kill each other while the score board is up
	if (mGameState == RGS_RoundEnded)
		return;

	if (mTeam[RT_Rabbit]->mController.size() > 0 && attacked->GetController()->GetTeam() == attackerController->GetTeam())
		return;

	Super::TakeDamage(attacked, damageMsg);
	if (damageMsg->mKilled)
	{
		// show affect on players score
		HUD* hud = mHUD;
		hud->InsertFadingText(hud->WorldToScreen(damageMsg->mAttacker->GetTransform().GetTranslation()), damageMsg->mAttacker->GetColour(), "+1");

		// if attacker was not on rabbit team, make them the new rabbit
		if (attackerController->GetTeam() != mTeam[RT_Rabbit])
		{
			mNewRabbit = damageMsg->mAttacker;
		}
		
		// broadcast so all know a pawn was killed
		PawnKilledMsg killedMsg(attacked, damageMsg);
		gMessageMgr.BroadcastMessage(&killedMsg);
	}
}

void Rabbit::RestoreRabbitOldPawn()
{
	if (/*mTeam.size() <= 0 || */!mRabbitOldPawn || /*mTeam[RT_Rabbit]->mController.size() <= 0 ||*/ !mJetPawn)
		return;

	CController* rabbitController = mJetPawn->GetController(); //mTeam[RT_Rabbit]->mController.front();
	rabbitController->SetPawn(mRabbitOldPawn);
	if (mTeam.size())
		rabbitController->SetTeam(mTeam[RT_Everyone]);

	mRabbitOldPawn->HandleMsg(&CopyPawnMsg(mJetPawn));

	// need to go via parachute state as this sets up a proper transform to fall from
	mRabbitOldPawn->SetState(mRabbitOldPawn->GetStateType(ParachuteState::Type));
	//mRabbitOldPawn->SetState(mRabbitOldPawn->GetStateType(ParachuteDyingState::Type));

	// dead buddy! for no collisions or pickups
	//mRabbitOldPawn->SetHealth(0.f); 
	TakeDamageMsg damageMsg(mRabbitOldPawn->GetHealth(), rabbitController->GetPawn());
	mRabbitOldPawn->HandleMsg(&damageMsg);

	mJetPawn->SetController(0);
}

void Rabbit::UpdateRabbit()
{
	if (!mNewRabbit)
		return;

	// this is done here instead of TakeDamage, as this causes issues as TakeDamage is called by the controller
	CController* attackerController = mNewRabbit->GetController();
	
	if (mTeam[RT_Rabbit]->mController.size() <= 0)
		FirstKill(mNewRabbit);

	CController* rabbitController = mTeam[RT_Rabbit]->mController.front();
	Pawn* rabbitPawn = rabbitController->GetPawn();

	float rabbitTime = rabbitController->GetStats()->mTime;
	int rabbitMinutes = rabbitTime / 60.f;
	int rabbitSeconds = (int)rabbitTime - (rabbitMinutes * 60);
	char time[256];
	if (rabbitSeconds >= 10)
		sprintf(time, "+%i:%i", rabbitMinutes, rabbitSeconds);
	else
		sprintf(time, "+%i:0%i", rabbitMinutes, rabbitSeconds);

	HUD* hud = mHUD;
	hud->InsertFadingText(hud->WorldToScreen(rabbitPawn->GetTransform().GetTranslation()), rabbitPawn->GetColour(), time);

	char buffer[256];
	sprintf(buffer, "%s is now the Rabbit", attackerController->GetName()); 
	hud->Message("rabbit", buffer);
	if (mGongSound)
		mGongSound->Play();

	// give the old rabbit back his shitkicker	
	// put him in parachute dying state and send on his way down!
	RestoreRabbitOldPawn();

	// give the new rabbit the jet, disable the old pawn
	mRabbitOldPawn = attackerController->GetPawn();
	mRabbitOldPawn->SetController(0);
	attackerController->SetTeam(0);
	attackerController->SetPawn(mJetPawn);
	attackerController->SetTeam(mTeam[RT_Rabbit]);

	// do spawn fx and shit.. . gives the jet back full health
	SpawnMsg spawnMsg;
	mJetPawn->HandleMsg(&spawnMsg);

	mJetPawn->HandleMsg(&CopyPawnMsg(mRabbitOldPawn));
	mRabbitOldPawn->SetState(DeadState::Type);

	// disable any fx
	EnableMsg enableMsg(false);
	mRabbitOldPawn->HandleMsg(&enableMsg);

	mJetPawn->SetColour(mRabbitOldPawn->GetColour());

	rabbitController->GetTeam()->mKillCount++;
	rabbitController->GetStats()->mKills++;

	// im just using the pickup msg to let the jet know to play
	// any special effects it needs to when a new player takes control
	BecomeRabbitMsg becomeRabbitMsg;
	mJetPawn->HandleMsg(&becomeRabbitMsg);

	mNewRabbit = 0;
}

void Rabbit::Update()
{
	UpdateRabbit();

	// respawn players in dead state
	vector<CController*>::iterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		CController* controller = *controllerIt;
		if (controller->GetPawn()->GetCurrentState()->GetType() != DeadState::Type)
			continue;

		SpawnMsg spawnMsg;
		controller->GetPawn()->HandleMsg(&spawnMsg);
	}

	if (mGameState == RGS_EndRound)
	{
		EndRound();
		mGameState = RGS_RoundEnded;
	}
	else if (mGameState == RGS_RoundEnded)
	{
	}
	else if (mTeam[RT_Rabbit]->mController.size() > 0)
	{
		// find the leading player
		CController* rabbit = mTeam[RT_Rabbit]->mController.front();
		CController* lead = rabbit;
		vector<CController*>::iterator controllerIt;
		for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
		{
			if ((*controllerIt)->GetStats()->mTime > lead->GetStats()->mTime)
				lead = (*controllerIt);
		}

		float prevTime = rabbit->GetStats()->mTime;
		rabbit->GetStats()->mTime += gGame.mGameUpdate.GetDeltaTime();
		if (rabbit->GetStats()->mTime >= mMaxRabbitTime)
		{
			SetState(RGS_EndRound);
		}
		else if (lead != rabbit)
		{
			// if we just overtook the leading player, display a message
			if (rabbit->GetStats()->mTime >= lead->GetStats()->mTime)
			{
				HUD* hud = gGame.GetGameModeMgr()->GetGameMode()->GetHUD();
				char buffer[256];
				sprintf(buffer, "%s takes the lead", rabbit->GetName()); 
				hud->Message("score", buffer);
				if (mGongSound)
					mGongSound->Play();
			}
		}
		else
		{
			// display score every 20 seconds
			int prevIntTime = int(prevTime / 20.f);
			int curIntTime = int(rabbit->GetStats()->mTime / 20.f);
			if (prevIntTime != curIntTime)
			{
				CController* rabbitController = mTeam[RT_Rabbit]->mController.front();

				float rabbitTime = rabbitController->GetStats()->mTime;
				int rabbitMinutes = rabbitTime / 60.f;
				int rabbitSeconds = (int)rabbitTime - (rabbitMinutes * 60);
				char time[256];
				if (rabbitSeconds >= 10)
					sprintf(time, "%s has been the rabbit for %i:%i", rabbit->GetName(), rabbitMinutes, rabbitSeconds);
				else
					sprintf(time, "%s has been the rabbit for %i:0%i", rabbit->GetName(), rabbitMinutes, rabbitSeconds);

				HUD* hud = gGame.GetGameModeMgr()->GetGameMode()->GetHUD();
				hud->Message("score", time);
			}
		}
	}

	Super::Update();
}

void Rabbit::Render()
{
	Super::Render();
}

bool Rabbit::PickupAllowed(int type)
{
	if (type == GiveTeamLife::Type)
		return false;

	return true;
}

bool Rabbit::CanPickup(Pawn* pawn, int type)
{
	if (mGameState == RGS_RoundEnded)
		return false;

	if (pawn == mJetPawn && type == WeaponUpgrade::Type)
		return false;

	return true;
}

int Rabbit::CalculateScore(Stats* stats)
{
	return (int)stats->mTime;
}