#include "StdAfx.h"
#include "GameModeMgr.h"

VectorParser<GameMode*> gameModeParser("GameMode*");

CLASS_METADATA_BEGIN(GameModeMgr)
	CLASS_VARIABLE(GameModeMgr, vector<GameMode*>, mGameMode)
CLASS_METADATA_END(GameModeMgr)

bool GameModeMgr::Init()
{
	RegisterReceiver();

	// set default game mode
	mCurGameMode = 0;
	SetGameMode(mGameMode[0]);

	vector<GameMode*>::iterator it;
	for (it = mGameMode.begin(); it != mGameMode.end(); ++it)
	{
		if ((*it) && !(*it)->Init())
			return false;
	}

	mControls = new MetadataScript("controls.mdd");
	return true;
}

void GameModeMgr::Deinit()
{
	delete mControls;

	// this cleans up all controllers
	Reset(0);

	vector<GameMode*>::iterator it;
	for (it = mGameMode.begin(); it != mGameMode.end(); ++it)
	{
		if ((*it))
			(*it)->Deinit();
	}

	DeregisterReceiver();
}

void GameModeMgr::SetGameMode(GameMode* gameMode)
{
	//if (mCurGameMode)
	//	mCurGameMode->Deinit();

	//gameMode->SetCurrent();
	mCurGameMode = gameMode;
}

void GameModeMgr::Update()
{
	mCurGameMode->Update();

	ControllerIterator it;
	for (it = mController.begin(); it != mController.end(); ++it)
	{
		(*it)->Update();
	}
}

void GameModeMgr::Render()
{
	mCurGameMode->Render();
}

void GameModeMgr::ReleaseController(CController** controller)
{
	if (!*controller)
		return;

	(*controller)->Deinit();
	delete *controller;
	*controller = 0;
}

CController* GameModeMgr::CreateController(const char* name)
{
	string controllerFile = string(name) + string(".mdd");
	MetadataScript metaScript;
	list<void*> controller;
	bool loaded = metaScript.Open(controllerFile.c_str(), &controller);
	if (!loaded || controller.size() != 1)
		return 0;

	CController* cont = (CController*)controller.front();
	cont->Init();
	return cont;
}

CController* GameModeMgr::CreateController(MetadataScript* matadataScript, ScriptClass* controllerClass)
{
	const ClassMetadata* info = 0;
	CController* cont = (CController*)matadataScript->CreateInstance(controllerClass, &info);
	if (!cont)
		return 0;

	if (!matadataScript->PopulateInstance(controllerClass, cont, info))
		return 0;

	cont->Init();
	return cont;
}

CController* GameModeMgr::InsertController(CController* controller)
{
	if (controller)
		mController.push_back(controller);

	return controller;
}

void GameModeMgr::RemoveController(CController* controller)
{
	ControllerIterator it;
	for (it = mController.begin(); it != mController.end(); ++it)
	{
		if (controller = (*it))
		{
			mController.erase(it);
			return;
		}
	}
}

MESSAGE_BEGIN(GameModeMgr)
	MESSAGE_PASS(SetGameMode)
	MESSAGE_PASS(InsertController)	
	MESSAGE_PASS(Reset)
	MESSAGE_PASS(Restart)
	}

	// forward all messages to the current game mode
	GetGameMode()->HandleMsg(msg);
	return false;
}

bool GameModeMgr::SetGameMode(SetGameModeMsg* msg)
{
	vector<GameMode*>::iterator it;
	for (it = mGameMode.begin(); it != mGameMode.end(); ++it)
	{
		if ((*it) && strcmpi((*it)->GetName(), msg->mName.c_str()) == 0)
		{
			SetGameMode(*it);
			return true;
		}
	}
	Log::Error("[GameModeMgr::SetGameMode] Game mode '%s' not found", msg->mName.c_str());
	return true;
}

bool GameModeMgr::InsertController(InsertControllerMsg* msg)
{
	InsertController(msg->mController);
	return true;
}

bool GameModeMgr::Reset(ResetMsg* msg)
{
	// reset game modes
	{
		vector<GameMode*>::iterator it;
		for (it = mGameMode.begin(); it != mGameMode.end(); ++it)
		{
			if ((*it))
				(*it)->Reset();
		}
	}

	// reset game mode
	SetGameMode(mGameMode[0]);

	// remove all controllers
	ControllerIterator it = mController.begin();
	while (mController.size())
	{
		CController* controller = *it;
		ReleaseController(&controller);
		it = mController.erase(it);
	}

	return true;
}


bool GameModeMgr::Restart(RestartMsg* msg)
{
	// reset game modes
	{
		vector<GameMode*>::iterator it;
		for (it = mGameMode.begin(); it != mGameMode.end(); ++it)
		{
			(*it)->Reset();
		}
	}

	return true;
}

