#ifndef _GAMEMODEMGR_H_
#define _GAMEMODEMGR_H_

class CController;
class GameMode;
class InsertControllerMsg;
class SetGameModeMsg;
class ResetMsg;
class RestartMsg;

//
// stores a list of game modes,
// also has a list of controllers (controllers are added by network clients or local player)
//
class GameModeMgr : public MessageReceiver
{
public:

	USES_METADATA
	typedef vector<GameMode*>::iterator		GameModeIterator;
	typedef vector<CController*>::iterator	ControllerIterator;

	GameModeMgr() :
		mCurGameMode(0)
	{
	}

	bool					Init();
	void					Deinit();

	GameMode*				GetGameMode()								{ return mCurGameMode; }
	void					SetGameMode(GameMode* gameMode);
	void					Update();
	void					Render();

	//
	// create controller from file
	// 
	CController*			CreateController(const char* name);
	void					ReleaseController(CController** controller);

	//
	// create controller from a script class
	//
	CController*			CreateController(MetadataScript* matadataScript, ScriptClass* controllerClass);

	CController*			InsertController(CController* controller);
	void					RemoveController(CController* controller);

	ControllerIterator		ControllerBegin()							{ return mController.begin(); }
	ControllerIterator		ControllerEnd()								{ return mController.end(); }
	ControllerIterator		RemoveController(ControllerIterator it)		{ return mController.erase(it); }

	virtual bool			HandleMsg(Message* msg);
	virtual const char*		GetName()									{ return "gameModeMgr"; }

	bool					InsertController(InsertControllerMsg* msg);
	bool					SetGameMode(SetGameModeMsg* msg);
	bool					Reset(ResetMsg* msg);
	bool					Restart(RestartMsg* msg);

//protected:

	MetadataScript*			mControls;
	GameMode*				mCurGameMode;
	vector<GameMode*>		mGameMode;
	vector<CController*>	mController;
};

#endif