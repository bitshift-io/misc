#ifndef _RABBIT_H_
#define _RABBIT_H_

#include "GameMode/GameMode.h"


enum RabbitGameState
{
	RGS_None,
	RGS_EndRound,
	RGS_RoundEnded,
	RGS_ResetRound,
};

enum RabbitTeam
{
	RT_Rabbit,
	RT_Everyone,	
};

class BecomeRabbitMsg : public Message
{
public:

	TYPE(170)
	NAME(BecomeRabbitMsg)
};

class Rabbit : public GameMode
{
public:
#ifndef DEMO
	USES_METADATA
#endif
	typedef GameMode Super;

	virtual bool			Init();
	virtual void			Deinit();

	virtual void			BeginRound();
	virtual void			EndRound();
	virtual void			Reset();

	virtual void			TakeDamage(Pawn* attacked, TakeDamageMsg* damageMsg);

	virtual void			Update();
	virtual void			Render();

	virtual bool			PickupAllowed(int type);
	virtual bool			CanPickup(Pawn* pawn, int type);

	virtual int				CalculateScore(Stats* stats);

protected:

	virtual void			UpdateRabbit();
	virtual void			FirstKill(Pawn* attacker);
	void					RestoreRabbitOldPawn();

	float					mMaxRabbitTime;

	Pawn*					mNewRabbit;

	Pawn*					mJetPawn;
	Pawn*					mRabbitOldPawn; // store a pointer to the rabbits old pawn so we can restore it when we are done
};

#endif
