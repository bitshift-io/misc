#include "StdAfx.h"
#include "GameMode.h"

CLASS_METADATA_BEGIN(GameMode)
	CLASS_VARIABLE(GameMode, int, mMaxPlayers)
	CLASS_VARIABLE(GameMode, string, mPlayerControllerName)
	CLASS_VARIABLE(GameMode, string, mNetworkControllerName)
	CLASS_VARIABLE(GameMode, string, mAIControllerName)
	CLASS_VARIABLE(GameMode, string, mHUDName)
	CLASS_VARIABLE(GameMode, string, mPawnName)
	CLASS_VARIABLE(GameMode, string, mName)
	CLASS_VARIABLE(GameMode, SoundPool*, mGongSound)	
CLASS_METADATA_END(GameMode)

GameMode::GameMode() :
	mGongSound(0)
{
}

bool GameMode::Init()
{
	RegisterReceiver();

	mHUD = new HUD();
	mHUD->Init();

	if (mGongSound)
	{
		mGongSound->Init();
		mGongSound->SetGain(gGame.mSoundVolume);
	}

	return true;
}

void GameMode::Deinit()
{
	mHUD->Deinit();
	delete mHUD;
	mHUD = 0;

	if (mGongSound)
		mGongSound->Deinit();

	DeregisterReceiver();
	// deinit pools
}

void GameMode::BeginRound()
{
	// reset player statistics
	GameModeMgr::ControllerIterator controllerIt;
	for (controllerIt = gGame.GetGameModeMgr()->ControllerBegin(); controllerIt != gGame.GetGameModeMgr()->ControllerEnd(); ++controllerIt)
	{
		Stats* stats = (*controllerIt)->GetStats();
		stats->Reset(); 
	}

	//mHUD->Reset();
}

void GameMode::EndRound()
{
}

void GameMode::TakeDamage(Pawn* attacked, TakeDamageMsg* damageMsg)
{
	attacked->HandleMsg(damageMsg);
}

HUD* GameMode::CreateHUD(CController* controller)
{
	MetadataScript metaScript;
	list<void*> hudList;
	bool loaded = metaScript.Open(gGame.GetGameModeMgr()->GetGameMode()->GetHUDName().c_str(), &hudList);
	if (!loaded || hudList.size() != 1)
	{
		Log::Print("[GameMode::GetHUD] Failed to load hud\n");
		return 0;
	}

	HUD* hud = (HUD*)hudList.front();
	hud->Init();
	return hud;
}

Pawn* GameMode::CreatePawn(CController* controller)											
{ 
	MetadataScript metaScript;
	list<void*> pawnList;
	bool loaded = metaScript.Open(controller->GetPawnName().c_str(), &pawnList);
	if (!loaded || pawnList.size() != 1)
	{
		Log::Print("[GameMode::GetPawn] Failed to load pawn\n");
		return 0;
	}

	Pawn* pawn = (Pawn*)pawnList.front();
	pawn->SetController(controller);
	pawn->Init();
	mPawn.push_back(pawn);
	return pawn;
}

Pawn* GameMode::CreatePawn(const char* pawnName)
{
	MetadataScript metaScript;
	list<void*> pawnList;
	bool loaded = metaScript.Open(pawnName, &pawnList);
	if (!loaded || pawnList.size() != 1)
	{
		Log::Print("[GameMode::GetPawn] Failed to load pawn\n");
		return 0;
	}

	Pawn* pawn = (Pawn*)pawnList.front();
	pawn->SetController(0);
	pawn->Init();
	mPawn.push_back(pawn);
	return pawn;
}

void GameMode::ReleaseHUD(HUD** hud)
{
	if (!*hud)
		return;

	(*hud)->Deinit();
	delete *hud;
	*hud = 0;
}

void GameMode::ReleasePawn(Pawn** pawn)
{
	if (!*pawn)
		return;

	(*pawn)->Deinit();
	delete *pawn;
	*pawn = 0;
}

#ifdef NETWORK
CController* GameMode::JoinGame(ClientConnectType connectType)											
{
	string controllerName;
	switch (connectType)
	{
	case CCT_Local:
		controllerName = mPlayerControllerName;
		break;

	case CCT_Network:
		controllerName = mNetworkControllerName;
		break;

	case CCT_AI:
		controllerName = mAIControllerName;
		break;
	}

	MetadataScript metaScript;
	list<void*> controller;
	bool loaded = metaScript.Open(controllerName.c_str(), &controller);
	if (!loaded || controller.size() != 1)
		return 0;

	CController* cont = (CController*)controller.front();
	cont->Init();
	return cont;
}

CController* GameMode::AddController(ClientConnectType connectType, bool addToFront)
{/*
	CController* controller = JoinGame(connectType);

	if (addToFront)
		mController.push_front(controller);
	else
		mController.push_back(controller);

	return controller;*/
	return 0;
}
#endif

void GameMode::AddAI(int count)
{/*
	for (int i = 0; i < count; ++i)
		mController.push_back(JoinGame(CCT_AI));*/
}

void GameMode::Update()
{
	if (mGongSound)
		mGongSound->Update();

	mHUD->Update();
}

CController* GameMode::AddController(CController* controller, bool addToFront)
{/*
	if (addToFront)
		mController.push_front(controller);
	else
		mController.push_back(controller);

	return controller;*/
	return 0;
}

void GameMode::SetState(int state)
{
	mGameState = state;
}

MESSAGE_BEGIN(GameMode)
#ifdef NETWORK
	MESSAGE_PASS(ClientConnect)
	MESSAGE_PASS(SyncGameMode)
#endif
	MESSAGE_PASS(BeginRound)
	MESSAGE_PASS(ReloadResource)
	MESSAGE_PASS(SetPause)
	MESSAGE_PASS(VolumeChange)
MESSAGE_END()

bool GameMode::VolumeChange(VolumeChangeMsg* msg)
{
	if (mGongSound)
		mGongSound->SetGain(gGame.mSoundVolume);

	return false;
}

bool GameMode::SetPause(SetPauseMsg* msg)
{
	if (mGongSound)
		mGongSound->Stop();

	return false;
}	

bool GameMode::ReloadResource(ReloadResourceMsg* msg)
{
	if (mHUD)
		mHUD->ReloadResource();

	return true;
}

bool GameMode::BeginRound(BeginRoundMsg* msg)
{
	BeginRound();
	return true;
}

#ifdef NETWORK
bool GameMode::ClientConnect(ClientConnectMsg* msg)
{
	if (gGame.GetEnvironment()->mNetwork->IsHosting())
	{
		SyncGameModeMsg syncMsg;
		syncMsg.mControllerCount = mController.size();

		gGame.GetEnvironment()->mNetwork->SendMessage(&syncMsg, this, msg->mPeer); // does this broadcast? we want to send to conencting client only
		
		AddController(msg->mConnectType);
	}

	return true;
}

bool GameMode::SyncGameMode(SyncGameModeMsg* msg)
{
	// do stuff to sync up our controller list and the controller list order! 
	// client will receive this message only

	// push controllers to the front, after this the controller list's should be synced up
	for (int i = 0; i < msg->mControllerCount; ++i)
	{
		AddController(CCT_Network, true);
	}

	AddController(CCT_Local);
	return true;
}
#endif

void GameMode::Render()
{
	mHUD->Render();
}

int GameMode::CalculateScore(Stats* stats)
{
	// factor modifier to score for each wave
	// up to 100 points for high accuracy
	// up to 100 points for no deaths
	// up to 100 points for pickups
	int accuracyScore = float(stats->mHits) / Math::Max(1.f, float(stats->mShotsFired)) * 100.f;
	int pickupScore = ((1.f - (1.f / Math::Max(1.f, stats->mPickups * 0.3f))) * 100.f);
	int deathsScore = 100.f / Math::Max(1.f, float(stats->mDeaths));
	int score = (accuracyScore + pickupScore + deathsScore);
	return score;
}

void GameMode::SortScore(vector<CController*>& controller)
{
	for (int i = 0; i < controller.size(); ++i)
	{
		for (int j = i; j < controller.size(); ++j)
		{
			if (controller[j]->GetStats()->mScore > controller[i]->GetStats()->mScore)
			{
				CController* temp = controller[j];
				controller[j] = controller[i];
				controller[i] = temp;
			}
		}
	}
}

void GameMode::InsertScore(vector<CController*>& controller)
{
	vector<CController*>::iterator controllerIt;
	for (controllerIt = controller.begin(); controllerIt != controller.end(); ++controllerIt)
	{
		CController* controller = *controllerIt;
		Stats* stats = controller->GetStats();

		float accuracy = (stats->mShotsFired == 0) ? 0.f : (float(stats->mHits) / float(stats->mShotsFired)) * 100.f;
		char stats1[2048];
		//char stats2[2048];
		sprintf(stats1, "Pickups: %i\nLives Used: %i\nAccuracy: %.1f%% (%i/%i)", stats->mPickups, stats->mDeaths, accuracy, stats->mHits, stats->mShotsFired);
		//sprintf(stats2, "Shots Fired: %i\nPickups: %i", stats->mShotsFired, stats->mPickups);
		mHUD->InsertPlayerScore(controller->GetName(), stats->mScore, stats1, controller->GetPawn()->GetColour());
	}
}
