; -- Dogfighters --
; Installer so um yeah!

[Setup]
AppName=Dog Fighters Demo
AppVerName=Dog Fighters Demo 1.0
DefaultDirName=C:\Games\DogFightersMediaDemo
DisableProgramGroupPage=true
UninstallDisplayIcon={app}\DogFighters.exe
OutputDir=D:\BlackCarbon\Project\DogFighters\Install\Output
SourceDir=D:\BlackCarbon\Project\DogFighters\Install\
OutputBaseFilename=DogFightersMediaDemo
VersionInfoVersion=1.0
VersionInfoCompany=www.Blackcarbon.net
ShowLanguageDialog=auto
WizardImageFile=D:\BlackCarbon\Project\DogFighters\Install\Resources\SideBar.bmp
WizardSmallImageFile=D:\BlackCarbon\Project\DogFighters\Install\Resources\TopBar.bmp
InternalCompressLevel=ultra
MergeDuplicateFiles=true
Compression=lzma/ultra
DirExistsWarning=no
LicenseFile=D:\BlackCarbon\Project\DogFighters\Install\Resources\Eula.txt
VersionInfoCopyright=2005-2009
SolidCompression=true
Encryption=false
Password=mediaonly
SetupIconFile=D:\BlackCarbon\Project\DogFighters\Install\Resources\icon.ico
AppPublisher=Black Carbon
AppPublisherURL=www.blackcarbon.net
AppVersion=1.0

[Files]
Source: RetailBuild\Binary\cg.dll; DestDir: {app}\Binary\
Source: RetailBuild\Binary\cgGL.dll; DestDir: {app}\Binary\
Source: RetailBuild\Binary\jpeg62.dll; DestDir: {app}\Binary\
Source: RetailBuild\Binary\oalinst.exe; DestDir: {app}\Binary\
Source: RetailBuild\Binary\ogg.dll; DestDir: {app}\Binary\
Source: RetailBuild\Binary\OpenAL32.dll; DestDir: {app}\Binary\
Source: RetailBuild\Binary\vorbis.dll; DestDir: {app}\Binary\
Source: RetailBuild\Binary\vorbisfile.dll; DestDir: {app}\Binary\
Source: RetailBuild\Binary\xinput1_3.dll; DestDir: {app}\Binary\
Source: RetailBuild\Binary\zlib1.dll; DestDir: {app}\Binary\
Source: RetailBuild\Data\Controls.mdd; DestDir: {app}\Data\
Source: RetailBuild\Data\game.pkg; DestDir: {app}\Data\
Source: RetailBuild\Data\Profile.mdd; DestDir: {app}\Data\
Source: RetailBuild\DogFighters.exe; DestDir: {app}

[Icons]
Name: {commonprograms}\Games\DogFighters; Filename: {app}\DogFighters.exe; WorkingDir: {app}
Name: {userdesktop}\DogFighters; Filename: {app}\DogFighters.exe; WorkingDir: {app}

[Dirs]
Name: {app}\Binary
Name: {app}\Data
Name: {app}\Data\Cache

[UninstallDelete]
Name: {app}; Type: filesandordirs
Name: {userdesktop}\DogFighters; Type: files
Name: {commonprograms}\Games\DogFighters; Type: files
