; -- Blackarbon Installer --
; Installer so um yeah!

[Setup]
AppName=Dog Fighters Demo
AppVerName=Dog Fighters Demo 1.0
DefaultDirName=C:\Games\DogFightersDemo
DisableProgramGroupPage=true
UninstallDisplayIcon={app}\DogFightersDemo.exe
OutputDir=D:\BlackCarbon\Project\DogFighters\Install\Output
SourceDir=D:\BlackCarbon\Project\DogFighters\Install
OutputBaseFilename=DogFightersDemo
VersionInfoVersion=1.0
VersionInfoCompany=www.Blackcarbon.net
ShowLanguageDialog=auto
WizardImageFile=D:\BlackCarbon\Project\DogFighters\Install\Resources\SideBar.bmp
WizardSmallImageFile=D:\BlackCarbon\Project\DogFighters\Install\Resources\TopBar.bmp
InternalCompressLevel=ultra
MergeDuplicateFiles=true
Compression=lzma/ultra
DirExistsWarning=no
LicenseFile=D:\BlackCarbon\Project\DogFighters\Install\Resources\Eula.txt
VersionInfoCopyright=2005-2009
SolidCompression=true
Encryption=false
Password=
SetupIconFile=D:\BlackCarbon\Project\DogFighters\Install\Resources\icon.ico
AppPublisher=Black Carbon
AppPublisherURL=www.blackcarbon.net
AppVersion=1.0
UninstallDisplayName=Dog Fighters Demo

[Files]
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Binary\cg.dll; DestDir: {app}\Binary\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Binary\cgGL.dll; DestDir: {app}\Binary\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Binary\jpeg62.dll; DestDir: {app}\Binary\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Binary\oalinst.exe; DestDir: {app}\Binary\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Binary\ogg.dll; DestDir: {app}\Binary\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Binary\OpenAL32.dll; DestDir: {app}\Binary\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Binary\vorbis.dll; DestDir: {app}\Binary\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Binary\vorbisfile.dll; DestDir: {app}\Binary\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Binary\xinput1_3.dll; DestDir: {app}\Binary\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Binary\zlib1.dll; DestDir: {app}\Binary\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Data\Controls.mdd; DestDir: {app}\Data\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Data\game.pkg; DestDir: {app}\Data\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\Data\Profile.mdd; DestDir: {app}\Data\
Source: ..\..\..\..\BlackCarbon\Project\DogFighters\Install\DemoBuild\DogFightersDemo.exe; DestDir: {app}

[Icons]
Name: {commonprograms}\Games\DogFightersDemo; Filename: {app}\DogFightersDemo.exe; WorkingDir: {app}; IconIndex: 0
Name: {userdesktop}\DogFightersDemo; Filename: {app}\DogFightersDemo.exe; WorkingDir: {app}; IconIndex: 0

[Dirs]
Name: {app}\Binary
Name: {app}\Data
Name: {app}\Data\Cache

[UninstallDelete]
Name: {app}; Type: filesandordirs
Name: {userdesktop}\DogFightersDemo; Type: files
Name: {commonprograms}\Games\DogFightersDemo; Type: files
