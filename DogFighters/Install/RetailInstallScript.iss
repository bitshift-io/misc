; -- Explosive --
; Installer so um yeah!

[Setup]
AppName=Explosive
AppVerName=Explosive 1.0
DefaultDirName=C:\Games\Explosive
DisableProgramGroupPage=true
UninstallDisplayIcon={app}\Explosive.exe
OutputDir=D:\Bronson\BlackCarbon\Explosive\Install\Output
SourceDir=D:\Bronson\BlackCarbon\Explosive\Install
OutputBaseFilename=Explosive
VersionInfoVersion=1.0
VersionInfoCompany=www.Blackcarbon.net
ShowLanguageDialog=auto
WizardImageFile=D:\Bronson\BlackCarbon\Explosive\Install\Resources\SideBar.bmp
WizardSmallImageFile=D:\Bronson\BlackCarbon\Explosive\Install\Resources\TopBar.bmp
InternalCompressLevel=ultra
MergeDuplicateFiles=true
Compression=lzma/ultra
DirExistsWarning=no
LicenseFile=D:\Bronson\BlackCarbon\Explosive\Install\Resources\Eula.txt
VersionInfoCopyright=2005-2007
SolidCompression=true

[Files]
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\wrap_oal.dll; DestDir: {app}
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\levels\bonus.ini; DestDir: {app}\data\levels
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\levels\Cavern.ini; DestDir: {app}\data\levels
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\levels\chess.ini; DestDir: {app}\data\levels
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\levels\Classic.ini; DestDir: {app}\data\levels
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\levels\Island.ini; DestDir: {app}\data\levels
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\levels\kitchen.ini; DestDir: {app}\data\levels
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\levels\SpaceMaze.ini; DestDir: {app}\data\levels
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\arial.fnt; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\arial.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\bannana.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\bannana.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\bannanaBark.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\bannanaBark.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\bannanaLeaf.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\bannanaLeaf.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\blackMarble.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\blackMarble.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\blackMarble2.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\blackMarble2.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\blob_shadow.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Blob_Shadow.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Blob_Shadow.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\bomb.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\bomb.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\bomb.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\bonus.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\bonus.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\breadBoard.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\breadBoard.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\brick_rocky.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Brick_Rocky.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Button_Generic.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Button_Generic.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Button_Right.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\caustics.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\caustics.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Cavern.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\cavern.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\character.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\character.skl; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Character.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\character_skin.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\character_skin.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\chess.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\chess.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\chessClock.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\chessClock.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\chessClock2.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\chessClock2.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\chessCube.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\chessCube2.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Classic.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\classic.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\colour_texture_additive.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\colour_texture_additive_nodepth.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\colour_texture_additive_scrolling.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\concrete_tile.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Concrete_Tile.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\concrete_trim.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Concrete_Trim.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\drop.wav; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\dynamic_woodbox.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\dynamic_woodbox.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\dynamic_woodbox.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\earth_clouds.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Earth_Clouds.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\earth_day_night.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\earth_day_night.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Explode01.wav; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Explode02.wav; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Explode03.wav; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\explosion.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Explosion.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\explosion_center.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\explosion_end.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\explosion_middle.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\explosion_ring.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\explosion_ring.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Explosion_Ring.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\explosion_ring_shadow.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Explosion_Ring_Shadow.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\explosion_shadow.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Explosion_Shadow.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Five_Cents.fnt; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Five_Cents.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Five_Cents2.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Five_Cents_MouseOver.fnt; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Five_Cents_MouseOver.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\fx_death.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\fx_death.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\FX_Death.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\fx_pickup.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\fx_shield.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\fx_shield.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\FX_Shield.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\fx_shield_inner.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\fx_shield_outter.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\fx_teleport.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\fx_teleport.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\fx_teleport.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\grassOverhang.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\grassOverhang.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\idle.anm; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Island.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\island.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\kick.wav; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\kitchen.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\kitchen.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\kitchenCube01.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\lava.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Lava.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\LeftKeyboard.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\MarbleChecker.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\MarbleChecker.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\marbleTableTop.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\marbleTableTop.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\metal_01.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Metal_01.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Metal_Bumped.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\moon_craters.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\moon_craters.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Mouse_Pointer.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\ornament_01.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Ornament_01.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pea.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pea.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup.wav; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Pickups1.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Pickups2.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_bomb.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_bomb.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_disease.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_disease.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_fire.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_fire.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_kick.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_kick.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_random.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_random.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_reverse.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_reverse.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_shield.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_shield.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_snail.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_snail.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_speed.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_speed.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_teleport.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\pickup_teleport.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\plain_texture.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\plain_texture_additive.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\plain_texture_additive_depth.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\plain_texture_additive_oscillating.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\plain_texture_additive_scrolling.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\plain_texture_clip.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\plain_texture_cube.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\plain_texture_oscillating.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\plain_texture_scrolling.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\planet_halo.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\planet_halo.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\polishedWood.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\polishedWood.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\RightKeyboard.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\rockHard.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\rockHard.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\RockyFace.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\RockyFace.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\rock_cracked.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Rock_Cracked.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\skin.fx; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\SpaceMaze.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\spacemaze.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\sporkDecal.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\sporkDecal.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\stalagmite.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\stalagmite.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\stars_background.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\stars_background.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\steam_fine.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\steam_fine.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Stroke_Outline.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Stroke_Outline.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\teammarker.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\teamMarker.MDL; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\TeamMarker.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\teleport.wav; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\tomato.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\tomato.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\trim_metalicfrills.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Trim_MetalicFrills.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Voice_EvilLaugh.wav; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Voice_InsaneLaugh.wav; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\walk.anm; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\WaterRipple.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\WaterRipple.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\whiteMarble.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\whiteMarble.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\whiteMarble2.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\whiteMarble2.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\woodenBevel.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\woodenBevel.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\wood_01.mat; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\data\Wood_01.tga; DestDir: {app}\data\
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\cg.dll; DestDir: {app}
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\cgGL.dll; DestDir: {app}
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\Explosive.exe; DestDir: {app}
Source: ..\..\..\BlackCarbon\Explosive\Install\Explosive\OpenAL32.dll; DestDir: {app}

[Icons]
Name: {commonprograms}\Games\Explosive; Filename: {app}\Explosive.exe; WorkingDir: {app}; IconIndex: 0
Name: {userdesktop}\Explosive; Filename: {app}\Explosive.exe; WorkingDir: {app}; IconIndex: 0

[Dirs]
Name: {app}\data
Name: {app}\data\levels

[UninstallDelete]
Name: {app}; Type: filesandordirs
Name: {userdesktop}\Explosive; Type: files
Name: {commonprograms}\Games\Explosive; Type: files
