@ECHO OFF

SET fileExclude0=UFOPawn_01.sce
SET fileExclude1=UFOPawn_01.anm
SET fileExclude2=UFOPawn_01.anm
SET fileExclude3=UFOFrag_01.sce

ECHO Removing full version only files
rename Data\Scene\%fileExclude0% %fileExclude0%_
rename Data\Scene\%fileExclude1% %fileExclude1%_
rename Data\Scene\%fileExclude2% %fileExclude2%_
rename Data\Scene\%fileExclude3% %fileExclude3%_

ECHO Building packages
DogFighters -package game.pkg mat,sce,fx,cg,anm,swf,ogg,wav,mdd,fnt,tga

ECHO Restoring full version only files
rename Data\Scene\%fileExclude0%_ %fileExclude0%
rename Data\Scene\%fileExclude1%_ %fileExclude1%
rename Data\Scene\%fileExclude2%_ %fileExclude2%
rename Data\Scene\%fileExclude3%_ %fileExclude3%

ECHO Removing current build
rd /s /q BuildDemo 

ECHO Creating required directory structure
md BuildDemo
cd BuildDemo
md Binary
md Data
cd Data
md Cache
cd ..
cd ..

ECHO Copying build files
copy *.dll BuildDemo
copy DogFightersDemo.exe BuildDemo
copy Binary\*.* BuildDemo\Binary
copy Data\*.pkg BuildDemo\Data
copy Data\Profile.mdd BuildDemo\Data
copy Data\Controls.mdd BuildDemo\Data

ECHO Removing temp files
del Data\*.pkg

ECHO Build complete
PAUSE