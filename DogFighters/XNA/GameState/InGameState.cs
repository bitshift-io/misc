
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    public class InGameState : GameState
    {
        public InGameState(Game game) : base(game)
        {
        }

        public override void Enter()
        {
            //temporarily set game mode
            //mGame.mCurGameMode = mGame.mGameMode[3];
            mGame.mCurGameMode.Init();
        }

        public override void Exit()
        {
            mGame.GetFontMgr().Clear();
        }

        public override void Update(GameTime gameTime)
        {
            mGame.GetFontMgr().Update();

            // Allows the default game to exit on Xbox 360 and Windows
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Game.GetInstance().PushState(new MenuState(Game.GetInstance()));

            foreach (Controller c in mGame.mController)
            {
                c.Update(gameTime);
            }

            for (int i = 0; i < mGame.mProjectile.Count; ++i)
            {
                Projectile p = (Projectile)mGame.mProjectile[i];
                if (p.Update() == false)
                {
                    mGame.mProjectile.Remove(p);
                }
            }

            for (int i = 0; i < mGame.mPickup.Count; ++i)
            {
                Pickup p = mGame.mPickup[i];
                if (p.Update(gameTime) == false)
                {
                    mGame.mPickup.Remove(p);
                }
            }

            
            mGame.GetEnvironment().Update();

            mGame.GetGameMode().Update(gameTime);
        }

        public override void Render()
        {
            mGame.GetGraphics().Begin(Color.Blue);

            mGame.GetEnvironment().Render();

            //draw our controllers(ships)
            foreach (Controller c in mGame.mController)
            {
                c.mPawn.Render();
            }

            //draw pickups
            foreach (Pickup p in mGame.mPickup)
            {
                p.Render();
            }

            //draw projectiles
            foreach (Projectile p in mGame.mProjectile)
            {
                p.Render();
            }

            mGame.GetGameMode().Render();

            //draw test font
            mGame.GetFontMgr().Render();

            mGame.GetGraphics().End();
        }
    }
}
