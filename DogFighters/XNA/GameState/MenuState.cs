
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    public enum WindowId
    {
        MenuMain,
        MenuHelp,
        MenuGameMode,
        ButtonExit,
        ButtonStart,
        ButtonHelp,
        ButtonBack,
        ListGameMode,
    }

    public class MenuState : GameState
    {
        UIWindow    mWindow;
        bool        mInputLock = false;

        public MenuState(Game game) : base(game)
        {
        }

        public override void Enter()
        {
            if (mWindow == null)
                InitUI();
        }

        public void InitUI()
        {
            mWindow = new UIWindow(NotifyEvent, -1);
            mWindow.SetTexture(Game.GetInstance().content.Load<Texture2D>("content\\textures\\background"));

            //
            // main menu
            //
            UIWindow mainMenu = new UIWindow(NotifyEvent, (int)WindowId.MenuMain);
            UIButton exit = new UIButton(NotifyEvent, (int)WindowId.ButtonExit, "Exit");
            UIButton help = new UIButton(NotifyEvent, (int)WindowId.ButtonHelp, "Help");            
            UIButton start = new UIButton(NotifyEvent, (int)WindowId.ButtonStart, "Start");
            start.SetFocus(true);

            mainMenu.AddChild(start);
            mainMenu.AddChild(help);
            mainMenu.AddChild(exit);
            LayoutWindow(mainMenu);
            mWindow.AddChild(mainMenu);

            //
            // help screen
            //
            UIWindow helpMenu = new UIWindow(NotifyEvent, (int)WindowId.MenuHelp);
            UIWindow helpController = new UIWindow(NotifyEvent, -1);
            helpController.SetTexture(Game.GetInstance().content.Load<Texture2D>("content\\textures\\controllerhelp"));
            helpController.SetRectangle(new Vector4(0.3f, 0.3f, 0.5f, 0.5f));

            UIButton helpBack = new UIButton(NotifyEvent, (int)WindowId.ButtonBack, "Back");
            helpBack.SetFocus(true);

            helpMenu.SetVisible(false);
            helpMenu.AddChild(helpController);
            helpMenu.AddChild(helpBack);
            //LayoutWindow(helpMenu);
            mWindow.AddChild(helpMenu);

            //
            // game mode menu
            //
            UIWindow gameModeMenu = new UIWindow(NotifyEvent, (int)WindowId.MenuGameMode);
            
            for (int i = 0; i < Game.GetInstance().mGameMode.Count; ++i)
            {
                GameMode g = (GameMode)Game.GetInstance().mGameMode[i];
                UIButton gameButton = new UIButton(NotifyEvent, -1, g.GetName());

                if (i == 0)
                    gameButton.SetFocus(true);

                gameModeMenu.AddChild(gameButton);
            }

            UIButton gameModeBack = new UIButton(NotifyEvent, (int)WindowId.ButtonBack, "Back");

            gameModeMenu.SetVisible(false);
            gameModeMenu.AddChild(gameModeBack);            
            LayoutWindow(gameModeMenu);
            mWindow.AddChild(gameModeMenu);
            
        }

        public void LayoutWindow(UIWindow window)
        {
            List<UIWindow> children = window.GetChildren();

            float width = 0.5f;
            float height = 0.025f;
            float heightPad = 0.01f;
            Vector2 pos = new Vector2(0.5f, 0.5f);
            foreach (UIWindow c in children)
            {
                c.SetRectangle(new Vector4(pos, width, height));
                pos.Y += height + heightPad;
            }
        }

        public void NotifyEvent(UIWindow window)
        {
            // special window code
            switch ((WindowId)GetActiveMenu().GetId())
            {
                case WindowId.MenuGameMode:
                    {
                        if (window.GetId() == -1)
                        {
                            if (Game.GetInstance().mCurGameMode != null)
                                Game.GetInstance().mCurGameMode.Deinit();

                            Game.GetInstance().mCurGameMode = Game.GetInstance().mGameMode[GetActiveMenu().GetChildIndex(window)];
                            Game.GetInstance().PopState();
                        }
                    }
                    break;
            }

            switch ((WindowId)window.GetId())
            {
                case WindowId.ButtonExit:
                    Game.GetInstance().Exit();
                    break;

                case WindowId.ButtonStart:
                    SetActiveMenu(WindowId.MenuGameMode);
                    break;

                case WindowId.ButtonHelp:
                    SetActiveMenu(WindowId.MenuHelp);
                    break;

                case WindowId.ButtonBack:
                    {
                        switch ((WindowId)GetActiveMenu().GetId())
                        {
                            case WindowId.MenuHelp:
                            case WindowId.MenuGameMode:
                                SetActiveMenu(WindowId.MenuMain);
                                break;

                        }
                    }
                    break;
            }
        }

        public void SetActiveMenu(WindowId menu)
        {
            UIWindow active = GetActiveMenu();
            active.SetVisible(false);

            UIWindow window = mWindow.GetWindowById((int)menu);
            window.SetVisible(true);
        }

        public override void Update(GameTime gameTime)
        {
            mGame.GetFontMgr().Update();

            //GamePadState currentState = GamePad.GetState(PlayerIndex.One);
            UIInput currentState = new UIInput();
            currentState.Update(GamePad.GetState(PlayerIndex.One), Keyboard.GetState());
            
            // focus changing code, focus lock is used so we dont constantly change focus till user lets go of the joystick
            if (!mInputLock)
            {
                if (currentState.mVertMovement < -0.5f)
                {
                    UIWindow active = GetActiveMenu();
                    UIWindow focus = active.GetChildWithFocus();
                    focus.SetFocus(false);
                    int idx = active.GetChildIndex(focus);
                    UIWindow nextFocus = active.GetChild((idx + 1) % active.GetChildCount());
                    nextFocus.SetFocus(true);

                    mInputLock = true;
                }
                else if (currentState.mVertMovement > 0.5f)
                {
                    UIWindow active = GetActiveMenu();
                    UIWindow focus = active.GetChildWithFocus();
                    focus.SetFocus(false);
                    int idx = active.GetChildIndex(focus);
                    int nextIdx = idx - 1;
                    UIWindow nextFocus = active.GetChild(nextIdx < 0 ? active.GetChildCount() - 1 : nextIdx);
                    nextFocus.SetFocus(true);

                    mInputLock = true;
                }

                mInputLock |= mWindow.Update(currentState); 
            }
            else
            {
                // make sure nothing is pressed
                if (Math.Abs(currentState.mHorizMovement) < 0.5f 
                    && Math.Abs(currentState.mVertMovement) < 0.5f
                    && currentState.mAccept == false
                    && currentState.mCancel == false)
                    mInputLock = false;
            }                   
        }

        public UIWindow GetActiveMenu()
        {
            return mWindow.GetFirstVisibleChild();
        }

        public override void Render()
        {
            mGame.GetGraphics().Begin(Color.Blue);

            mWindow.Render();
            mGame.GetFontMgr().Render();

            mGame.GetGraphics().End();
        }
    }
}
