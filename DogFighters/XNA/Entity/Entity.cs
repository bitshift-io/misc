#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    public class CollisionInfo
    {
        public Vector3  mPosition;
        public float    mRadius;
    }

    //
    // base class for all entitys in the game, projectiles, pawns etc...
    //
    public class Entity
    {
        public virtual bool GetTransform(Matrix transform)
        {
            return false;
        }

        public virtual CollisionInfo GetCollisionInfo()
        {
            return null;
        }

        public virtual bool TakeDamage(Controller attacker, float damage)
        {
            return false;
        }

        public virtual bool GiveToController(Controller c)
        {
            return true;
        }

        // return false to be deleted
        public virtual bool Update(GameTime gameTime)
        {
            return true;
        }

        public virtual void Render()
        {
        }
    }
}
