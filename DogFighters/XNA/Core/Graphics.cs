
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    // 
    // covers graphics
    // rendering and window shiz
    //
    public class Graphics
    {
        public GraphicsDeviceManager    mGraphics;
        public EffectPool               mEffectPool;
        // stats


        public Graphics(Microsoft.Xna.Framework.Game game)
        {
            // init graphics and other XNA goodies
            mGraphics = new GraphicsDeviceManager(game);
            mGraphics.PreferredBackBufferWidth = 1024;
            mGraphics.PreferredBackBufferHeight = 768;
            mGraphics.PreferMultiSampling = true;
            mGraphics.ApplyChanges();

            mEffectPool = new EffectPool();
        }

        public void Init()
        {
            mGraphics.GraphicsDevice.RenderState.CullMode = CullMode.CullClockwiseFace;
            mGraphics.GraphicsDevice.RenderState.MultiSampleAntiAlias = true;
            mGraphics.GraphicsDevice.PresentationParameters.MultiSampleType = MultiSampleType.SixteenSamples;
            mGraphics.GraphicsDevice.RenderState.DepthBufferEnable = true;
            mGraphics.ApplyChanges();
        }

        public void Begin(Color color)
        {
            mGraphics.GraphicsDevice.Clear(color);
        }

        public void End()
        {

        }

        public void ToggleFullscreen()
        {
            mGraphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            mGraphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            mGraphics.ToggleFullScreen();
            mGraphics.ApplyChanges();
        }

        public void DrawIndexed(Camera camera, Effect effect, VertexBuffer vertexBuffer, IndexBuffer indexBuffer, VertexDeclaration vertexDeclaration, int numVertices, int numIndices, int sizeOfVertex)
        {
            mGraphics.GraphicsDevice.VertexDeclaration =
                vertexDeclaration;

            mGraphics.GraphicsDevice.RenderState.CullMode =
                CullMode.None; //.CullClockwiseFace;

            mGraphics.GraphicsDevice.Indices = indexBuffer;

            mGraphics.GraphicsDevice.Vertices[0].SetSource(vertexBuffer, 0, sizeOfVertex);

            if (effect.GetType() == typeof(BasicEffect))
            {
                BasicEffect basicEffect = (BasicEffect)effect;
                //basicEffect.EnableDefaultLighting();
                basicEffect.World = Matrix.Identity;
                basicEffect.View = camera.GetView();
                basicEffect.Projection = camera.GetProjection();
            }

            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();

                mGraphics.GraphicsDevice.DrawIndexedPrimitives(
                    PrimitiveType.TriangleList,
                    0,
                    0,
                    numVertices,
                    0,
                    numIndices / 3);

                pass.End();
            }
            effect.End();
        }

        public VertexBuffer CreateVertexBuffer(int maxVerts, int sizeOfVertex)
        {
            return new VertexBuffer(mGraphics.GraphicsDevice, maxVerts * sizeOfVertex, ResourceUsage.None, ResourceManagementMode.Automatic);
        }

        public IndexBuffer CreateIndexBuffer(int maxIndices, int sizeOfIndex)
        {
            return new IndexBuffer(mGraphics.GraphicsDevice, sizeOfIndex * maxIndices, ResourceUsage.None, ResourceManagementMode.Automatic, IndexElementSize.SixteenBits);
        }

        public VertexDeclaration CreateVertexDeclaration(VertexElement[] elements)
        {
            return  new VertexDeclaration(mGraphics.GraphicsDevice, elements);
        }

        public void Render(Model model, Matrix transform, Camera camera)
        {
            //background mesh
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
            float count = model.Meshes.Count;

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * transform;
                    effect.View = camera.GetView();
                    effect.Projection = camera.GetProjection();
                }

                mGraphics.GraphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
                mGraphics.GraphicsDevice.RenderState.DepthBufferEnable = true;

                mesh.Draw();
            }
        }

        public void SetModelDiffuseColor(Model model, Vector3 color)
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                //this is where mesh orientation is set as well as camera and projection!
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.DiffuseColor = color;
                }
            }
        }

        public SpriteBatch CreateSpriteBatch()
        {
            return new SpriteBatch(mGraphics.GraphicsDevice);
        }

        public Vector2 GetDeviceDimensions()
        {
            return new Vector2(mGraphics.PreferredBackBufferWidth, mGraphics.PreferredBackBufferHeight);
        }

        public BasicEffect CreateBasicEffect()
        {
            return new BasicEffect(mGraphics.GraphicsDevice, mEffectPool);
        }
    }
}
