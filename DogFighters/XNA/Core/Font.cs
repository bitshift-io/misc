using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;

namespace DogFighter
{
    public class Font
    {
        Texture2D mTexture = null;
        SpriteBatch mSpriteBatch;
        public Vector4 mPadding = Vector4.Zero;
        public Vector2 mSpacing = Vector2.Zero;

        struct GlyphInfo
        {
            public Vector2 position;
            public Vector2 dimension;      
            public Vector2 offset;
            public int xAdvance;
        }
        GlyphInfo[] mGlyphInfo = new GlyphInfo[256];

        struct Text
        {
            public Vector2 position;
            public Color color;
            public String format;
        }
        ArrayList mText = new ArrayList();



        public bool Load(string name) //no extension!
        {
            mSpriteBatch = Game.GetInstance().GetGraphics().CreateSpriteBatch(); // new SpriteBatch(Game.GetInstance().graphics.GraphicsDevice);

            String textureName = name + "";
            String fontName = name + ".fnt";
            mTexture = Game.GetInstance().content.Load<Texture2D>(textureName);

            //create fnt reader
            TextReader textRead = new StreamReader(fontName);

            String line1 = textRead.ReadLine();
            do
            {
                //read line

                String[] tokens = line1.Split(' ');

                String lineType = tokens[0];

                Dictionary<String,String> lineDictionary = new Dictionary<string,string>();

                for (int i = 1; i < tokens.Length; ++i)
                {
                    String token = tokens[i];
                    String[] subToken = token.Split('=');

                    if (subToken.Length == 2)
                        lineDictionary[subToken[0]] = subToken[1];
                }

                switch (lineType)
                {
                    case "info":
                        {
                            String strPadding = lineDictionary["padding"];
                            String strSpacing = lineDictionary["spacing"];
                            String[] paddingToken = strPadding.Split(',');
                            String[] spacingToken = strSpacing.Split(',');

                            mPadding.X = Convert.ToInt32(paddingToken[0]);
                            mPadding.Y = Convert.ToInt32(paddingToken[1]);
                            mPadding.Z = Convert.ToInt32(paddingToken[2]);
                            mPadding.W = Convert.ToInt32(paddingToken[3]);
                            mSpacing.X = Convert.ToInt32(spacingToken[0]);
                            mSpacing.Y = Convert.ToInt32(spacingToken[1]);
                        }
                        break;

                    case "common":
                        {
                            int nothing = 0;
                        }
                        break;

                    case "page":
                        {
                            int nothing = 0;
                        }
                        break;

                    case "char":
                        {
                            int id = Convert.ToInt32(lineDictionary["id"]);
                            mGlyphInfo[id].offset.X = Convert.ToInt32(lineDictionary["xoffset"]);
                            mGlyphInfo[id].offset.Y = Convert.ToInt32(lineDictionary["yoffset"]);
                            mGlyphInfo[id].position.X = Convert.ToInt32(lineDictionary["x"]);
                            mGlyphInfo[id].position.Y = Convert.ToInt32(lineDictionary["y"]);
                            mGlyphInfo[id].dimension.X = Convert.ToInt32(lineDictionary["width"]);
                            mGlyphInfo[id].dimension.Y = Convert.ToInt32(lineDictionary["height"]);
                            mGlyphInfo[id].xAdvance = Convert.ToInt32(lineDictionary["xadvance"]);
                        }
                        break;

                    case "kerning":
                        {
                            int nothing = 0;
                        }
                        break;
                }

                line1 = textRead.ReadLine();
            }
            while (line1 != null);

            //close teh file!
            textRead.Close();

            return true;
        }

        //the good stuff!
        public int DrawString(Vector2 position, Color color, string format)
        {
            mSpriteBatch.Begin(SpriteBlendMode.AlphaBlend);
            Vector2 curPos = position;
            //setup padding as Vector2 for now
            Vector2 padding = new Vector2(mPadding.X,0);

            // draw each character in the string
            foreach (char c in format)
            {
                Rectangle rectangle = new Rectangle((int)mGlyphInfo[c].position.X, (int)mGlyphInfo[c].position.Y, (int)mGlyphInfo[c].dimension.X, (int)mGlyphInfo[c].dimension.Y);
                //offset for off size chars
                Vector2 offset = new Vector2((float)mGlyphInfo[c].offset.X, (float)mGlyphInfo[c].offset.Y);
                mSpriteBatch.Draw(mTexture, curPos + offset , rectangle, color);
                curPos.X += mGlyphInfo[c].xAdvance + padding.X;
            }
            mSpriteBatch.End();
            return 0;
        }

        public void Clear()
        {
            mText.Clear();
        }
        public void Format(Vector2 position, Color color, string format, params object[] args)
        {
            Vector2 deviceSize = Game.GetInstance().GetGraphics().GetDeviceDimensions();
            //convert position 0 to 1 range to screen range
            position.Y = deviceSize.Y * position.Y;
            position.X = deviceSize.X * position.X;

            string str = string.Format(format, args);
            Text txt = new Text();
            txt.position = position;
            txt.color = color;
            txt.format = str;
            mText.Add(txt);
        }



        public void Render()
        {
            foreach (Text t in mText)
            {
                DrawString(t.position, t.color, t.format);
            }
        }

    }
}
