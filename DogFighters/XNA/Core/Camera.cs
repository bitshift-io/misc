
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    public class Camera
    {
        Matrix  mView;
        Matrix  mWorld;
        Matrix  mProjection;
        bool    mViewDirty;

        public Camera()
        {
            SetPerspective(MathHelper.ToRadians(90.0f), 1.44f, 0.1f, 10000.0f);
            LookAt(Vector3.Zero, Vector3.Forward, Vector3.Up);
        }

        public Matrix GetView()
        {
            if (mViewDirty)
            {
                mView = Matrix.CreateLookAt(mWorld.Translation, mWorld.Translation + mWorld.Forward, mWorld.Up);
                mViewDirty = false;
            }

            return mView;
        }

        public Matrix GetProjection()
        {
            return mProjection;
        }

        public void SetTranslation(Vector3 position)
        {
            mWorld.Translation = position;
            mViewDirty = true;
        }

        public void SetPerspective(float fov, float aspectRatio, float near, float far)
        {
            mProjection = Matrix.CreatePerspectiveFieldOfView(fov, aspectRatio, near, far);
        }

        public void SetOrtho(float width, float height, float near, float far)
        {
        }

        public void LookAt(Vector3 eyePosition, Vector3 target, Vector3 up)
        {
            mViewDirty = true;

            mWorld.Translation = eyePosition;

            Vector3 view = Vector3.Normalize(target - eyePosition);
            Vector3 right = Vector3.Cross(up, view);
            Vector3 newUp = Vector3.Cross(view, right);

            mWorld.Forward = view;
            mWorld.Right = right;
            mWorld.Up = newUp;
        }

        public Vector2 GetScreenSpaceCoord(Vector3 worldPosition)
        {
            Matrix worldToscreen = GetView() * GetProjection();
            Vector4 homoScreenSpacePos = Vector4.Transform(new Vector4(worldPosition, 1.0f), worldToscreen);
            Vector2 screenSpacePos = new Vector2(homoScreenSpacePos.X, homoScreenSpacePos.Y) / homoScreenSpacePos.W;

            // convert from -1, 1 to 0, 1
            screenSpacePos.X = ((screenSpacePos.X / 2.0f) + 0.5f);
            screenSpacePos.Y = 1.0f - ((screenSpacePos.Y / 2.0f) + 0.5f);

            return screenSpacePos;
        }
    }
}
