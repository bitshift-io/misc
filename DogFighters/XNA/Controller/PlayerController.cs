using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;


namespace DogFighter
{
    public class PlayerController : Controller
    {

        float mFireVibrateTime = 0.2f;
        float mColVibrateTime = 0.2f;
        float mCurVibrateTime = 0.0f;

        PlayerIndex mIndex;

        public PlayerController(PlayerIndex index)
        {
            mIndex = index;
            switch (index)
            {
                case PlayerIndex.One:
                    mPawn = new Pawn(this, Color.Yellow);
                    break;
                case PlayerIndex.Two:
                    mPawn = new Pawn(this, Color.Green);
                    break;
                case PlayerIndex.Three:
                    mPawn = new Pawn(this, Color.Blue);
                    break;
                case PlayerIndex.Four:
                    mPawn = new Pawn(this, Color.Violet);
                    break;
            }
            
        }

        public PlayerIndex GetPlayerIndex()
        {
            return mIndex;
        }

        public override void Update(GameTime gameTime)
        {
            //get gamepad state
            GamePadState currentState = GamePad.GetState(mIndex);
            if (currentState.IsConnected)
            {
                mSteerDir = currentState.ThumbSticks.Left;
                if (mSteerDir.Length() == 0.0f)
                    mSteerDir = currentState.ThumbSticks.Right;

                mFirePrimary = currentState.Triggers.Right > 0.5f ? true : false;
                mFirePrimary |= currentState.Buttons.A == ButtonState.Pressed ? true : false;
                mFireSecondary = currentState.Triggers.Left > 0.5f ? true : false;
                mFireSecondary |= currentState.Buttons.B == ButtonState.Pressed ? true : false;

                mCurVibrateTime -= 0.01f;
                if (mCurVibrateTime < 0)
                    GamePad.SetVibration(mIndex, currentState.Triggers.Left * 0.1f, currentState.Triggers.Right * 0.3f);
            }
            else
            {
                KeyboardState state = Keyboard.GetState();
                mFirePrimary = state.IsKeyDown(Keys.LeftShift);
                mFireSecondary = state.IsKeyDown(Keys.LeftControl);
                mSteerDir.X = (state.IsKeyDown(Keys.Left) ? 1: 0) * -1 + (state.IsKeyDown(Keys.Right) ? 1 : 0);
                mSteerDir.Y = (state.IsKeyDown(Keys.Down) ? 1 : 0) * -1 + (state.IsKeyDown(Keys.Up) ? 1 : 0);
            }
            
            base.Update(gameTime);
        }


        public override void  NotifyFire()
        {
            mCurVibrateTime = mFireVibrateTime;
            GamePad.SetVibration(mIndex, 0.3f, 0.5f);
        }

        public override void NotifyKilled()
        {
            base.NotifyKilled();
            Game.GetInstance().mFontMgr.FormatFading3D(Game.GetInstance().mFontMgr.mFontMain, mPawn.mPosition, mPawn.mColor, 2000, "Kills: {0}", mKills.ToString());
        }

        public override void NotifyCollision()
        {
            mCurVibrateTime = mColVibrateTime;
            GamePad.SetVibration(mIndex, 1.0f, 1.0f);
        }
    }
}
