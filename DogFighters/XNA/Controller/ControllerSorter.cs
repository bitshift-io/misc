using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace DogFighter
{
    class ControllerDotSorter : IComparer
    {
        public Vector3 mPosition = Vector3.Zero;
        public Vector3 mDirection = Vector3.Zero;

        public ControllerDotSorter(float direction,Vector3 position)
        {
            //rotation to direction
            mDirection.X = -(float)Math.Sin(direction);
            mDirection.Z = -(float)Math.Cos(direction);

            mPosition = position;
        }

        public int Compare(object x, object y)
        {
            Controller c1 = (Controller)x;
            Controller c2 = (Controller)y;

            Vector3 v1 = Vector3.Normalize(c1.mPawn.mPosition - mPosition);
            Vector3 v2 = Vector3.Normalize(c2.mPawn.mPosition - mPosition);

            float d1 = Vector3.Dot(v1, mDirection);
            float d2 = Vector3.Dot(v2, mDirection);

            if (d1 > d2)
                return 1;
            else if (d1 < d2)
                return -1;

            return 0;
        }
    }

    class ControllerDistSorter : IComparer
    {
        public Vector3 mPosition = Vector3.Zero;

        public ControllerDistSorter(Vector3 position)
        {
            mPosition = position;
        }

        public int Compare(object x, object y)
        {
            Controller c1 = (Controller)x;
            Controller c2 = (Controller)y;

            float distance1 = (c1.mPawn.mPosition - mPosition).Length();
            float distance2 = (c2.mPawn.mPosition - mPosition).Length();

            if (distance1 > distance2)
                return 1;
            else if (distance1 < distance2) 
                return -1;

            return 0;
        }
    }

}
