using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;


namespace DogFighter
{
    public class AIController : Controller
    {
        Controller mTarget = null;
        long mGameTimeMS = 0;

        public override void Update(GameTime gameTime)
        {
            Game game = Game.GetInstance();

            long gameTimeMS = game.gameMS;

            if (mGameTimeMS < gameTimeMS)
            {
                mGameTimeMS = gameTimeMS + game.random.Next(100, 2000);
                mSteerDir.X = (float)game.random.NextDouble();
            }

            //if (mTarget == null || mTarget.mShip.mState == Ship.State.Dead)
            //    PickTarget();

            if (ShouldFire() /*d1 > 0.9f*/)
            {
                mFirePrimary = true;
                mFireSecondary = false;
            }
            else
            {
                mFirePrimary = false;
                mFireSecondary = false;
            }

            base.Update(gameTime);
        }

        public bool ShouldFire()
        {
            // make sure we dont shoot towards buddies
            foreach (Controller c in Game.GetInstance().mController)
            {
                if (this == c || c.GetTeam() == null || (c.GetTeam() != null && c.GetTeam() != GetTeam())) // c.GetType() == typeof(PlayerController) || c.mPawn.mState == Pawn.State.Dead)
                    continue;

                //calculate dot
                Vector3 v1 = Vector3.Normalize(c.mPawn.mPosition - mPawn.mPosition);
                float dot = Vector3.Dot(v1, mPawn.GetDirection());

                //calculate distance
                Vector3 distVector = mPawn.mPosition - c.mPawn.mPosition;
                float distance = distVector.Length();

                if (dot > 0.95 && distance < 8000)
                    return false;
            }

            // get non team members ....
            foreach (Controller c in Game.GetInstance().mController) 
            {
                if (this == c || (c.GetTeam() == null && c.GetTeam() == GetTeam()))//c.GetType() != typeof(PlayerController) || c.mPawn.mState == Pawn.State.Dead)
                    continue;

                //calculate dot
                Vector3 v1 = Vector3.Normalize(c.mPawn.mPosition - mPawn.mPosition);
                float dot = Vector3.Dot(v1, mPawn.GetDirection());

                //calculate distance
                Vector3 distVector = mPawn.mPosition - c.mPawn.mPosition;
                float distance = distVector.Length();

                if (dot > 0.9 && distance < 18000 && distance > 5000)
                    return true;
            }
            return false;
        }

        public AIController()
        {
            mPawn = new Pawn(this, Color.Red);
        }


        /*
        public void PickTarget()
        {
            ArrayList dotSorted = new ArrayList();
            ArrayList positionSorted = new ArrayList();

            foreach (Controller c in Game.GetInstance().mController) //game knows about teh controllers
            {
                if (this == c)
                    continue;
                positionSorted.Add(c);
                dotSorted.Add(c);
            }

            ControllerDotSorter dSort = new ControllerDotSorter(mShip.mRotation, mShip.mPosition);
            dotSorted.Sort(dSort);
            ControllerDistSorter disSort = new ControllerDistSorter(mShip.mPosition);
            positionSorted.Sort(disSort);

            //pick somone to kill(prefer fab)!
            int rand = Game.GetInstance().random.Next(0,1);
            if (rand == 0)
            {
                mTarget = (Controller)dotSorted[0];
            }
            else
            {
                mTarget = (Controller)positionSorted[0];
            }
        }
         */
    }
}
