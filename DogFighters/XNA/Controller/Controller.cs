using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace DogFighter
{
    public class Controller
    {
        public Vector2  mSteerDir;
        public bool     mFirePrimary;
        public bool     mFireSecondary;
        public float    mThrust = 1.0f;
        public Pawn     mPawn = null;
        public Team     mTeam = null;


        //score
        public int mKills = 0;
        public int mDeaths = 0;
        public int mPickups = 0;

        public Controller()
        {
        }

        public Team GetTeam()
        {
            return mTeam;
        }

        public virtual Pawn GetPawn()
        {
            return mPawn;
        }

        virtual public void Update(GameTime gameTime)
        {
            mPawn.Update(gameTime);
        }

        virtual public void NotifyFire()
        {
            Game game = Game.GetInstance();
            //Cue fireSound = game.soundBank.GetCue("\\Content\\Sounds\\Explode01");
            //fireSound.Play();
        }

        //player died
        virtual public void NotifyDead()
        {
            mDeaths += 1;
        }

        //player killed somone
        virtual public void NotifyKilled()
        {
            mKills += 1;
        }

        virtual public void NotifySuicide()
        {
            mDeaths += 1;
        }

        virtual public void NotifyCollision()
        {
        }
    }
}
