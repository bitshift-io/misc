
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    //
    // duh
    //
    public class FreeForAll : GameMode
    {
        public int mKillLimit = 20;
        public int mAICount = 1;

        public override void Init()
        {
            Deinit();
           
            // add AI
            for (int i = 0; i < mAICount; ++i)
            {
                Controller c = Game.GetInstance().AddAIController();
                c.GetPawn().Respawn(true);
            }

            base.Init();
        }

        public override void Deinit()
        {
            base.Deinit();
        }

        public void EndGame(Controller winner)
        {
            if (winner.GetType() == typeof(PlayerController))
            {
                Game.GetInstance().GetFontMgr().FormatNotify(Game.GetInstance().GetFontMgr().mFontHeader, new Vector2(0.5f, 0.5f), winner.GetPawn().GetColor(), 1000, "Player {0} wins!", ((PlayerController)winner).GetPlayerIndex());
            }
            else
            {
                Game.GetInstance().GetFontMgr().FormatNotify(Game.GetInstance().GetFontMgr().mFontHeader, new Vector2(0.5f, 0.5f), winner.GetPawn().GetColor(), 1000, "You been beaten by ME!!! <insert evil coder laugh>");
            }

            Init();
        }

        public override void TakeDamage(Controller attacker, Controller attacked, float damage)
        {
            attacked.GetPawn().TakeDamage(attacker, damage);

            if (attacker.mKills > mKillLimit)
                EndGame(attacker);
        }

        public override void Update(GameTime gameTime)
        {
            // respawn players
            foreach (Controller c in Game.GetInstance().mController)
            {
                if (!c.GetPawn().IsAlive() && c.GetPawn().GetTimeInState() > 1.0f && c.GetPawn().CanRespawn())
                    c.GetPawn().Respawn(false);
            }
        }

        public override String GetName()
        {
            return "Free For All";
        }

        public override String GetDescription()
        {
            return "Free for all, shoot or be shot";
        }
    }
}
