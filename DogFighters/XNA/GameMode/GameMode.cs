
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    public class GameMode
    {
        public List<Team>   mTeam = new List<Team>();
        public Texture2D    mTeamTag;
        public SpriteBatch  mSpriteBatch;

        public virtual void Init()
        {
            mTeamTag = Game.GetInstance().content.Load<Texture2D>("Content\\Textures\\TeamTag");
            mSpriteBatch = Game.GetInstance().GetGraphics().CreateSpriteBatch();
        }

        public virtual void Deinit()
        {
            mTeamTag = null;
            mSpriteBatch = null;

            // remove AI, reset scores
            for (int i = 0; i < Game.GetInstance().mController.Count; ++i) //Controller c in Game.GetInstance().mController)
            {
                Controller c = (Controller)Game.GetInstance().mController[i];
                if (c.GetType() != typeof(PlayerController))
                {
                    Game.GetInstance().RemoveController(c);
                    --i;
                }
                else
                {
                    c.mKills = 0;
                    c.mDeaths = 0;
                    c.GetPawn().Respawn(false);
                    Game.GetInstance().mController[i] = c; // stupid c#
                }
            }

            // remove old teams
            mTeam.Clear();
        }

        public virtual bool GiveEntityToController(Entity item, Controller c)
        {
            if (c.GetType() != typeof(PlayerController))
                return false;

            return item.GiveToController(c);
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public virtual void TakeDamage(Controller attacker, Controller attacked, float damage)
        {
        }

        public void AddControllerToTeam(Controller controller, Team team)
        {
            if (controller.mTeam != null)
                controller.mTeam.mController.Remove(controller);

            team.mController.Add(controller);
            controller.mTeam = team;
        }

        public void RenderTeamIcon(Vector3 color, Vector3 position)
        {
            Vector2 screenPos = Game.GetInstance().GetCamera().GetScreenSpaceCoord(position);

            Vector2 screenSize = Game.GetInstance().GetGraphics().GetDeviceDimensions();
            screenPos *= screenSize;
            int pixelSize = 20;

            mSpriteBatch.Begin(SpriteBlendMode.AlphaBlend);
            Rectangle rect = new Rectangle(((int)screenPos.X) - (pixelSize / 2), ((int)screenPos.Y) - 2 * pixelSize, pixelSize, pixelSize);
            mSpriteBatch.Draw(mTeamTag, rect, new Color(color));
            mSpriteBatch.End();
            
        }

        public virtual void Render()
        {
        }

        public virtual String GetName()
        {
            return "";
        }

        public virtual String GetDescription()
        {
            return "";
        }
    }
}
