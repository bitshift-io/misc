
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    //
    // 2 teams (eg humans vs AI, human vs human)
    //
    public class TeamFreeForAll : GameMode
    {
        bool        mPlayersVsAI    = true;
        public int  mAICount        = 5;
        public int  mKillLimit      = 20;

        public override void Init()
        {
            Deinit();

            // 2 teams
            Team team0 = new Team();
            team0.mColor = Color.White.ToVector3();
            mTeam.Add(team0);

            Team team1 = new Team();
            team0.mColor = Color.Black.ToVector3();
            mTeam.Add(team1);

            if (mPlayersVsAI)
            {
                // add players to player team
                foreach (Controller c in Game.GetInstance().mController)
                {
                    if (c.GetType() == typeof(PlayerController))
                    {
                        c.GetPawn().Respawn(true);
                        AddControllerToTeam(c, team0);
                    }
                }

                // add AI
                for (int i = 0; i < mAICount; ++i)
                {
                    Controller c = Game.GetInstance().AddAIController();
                    c.GetPawn().Respawn(true);
                    AddControllerToTeam(c, team1);
                }
            }/*
            else
            {
                // add AI
                for (int i = 0; i < mAICount; ++i)
                {
                    Controller c = Game.GetInstance().AddAIController();
                    c.GetPawn().Respawn();
                    
                }

                AddControllerToTeam(c, team1);
            }*/

            

            base.Init();
        }

        public override void Deinit()
        {
            base.Deinit();
        }

        public void EndGame(Controller winner)
        {
            Team winnerTeam = winner.GetTeam();
            Game.GetInstance().GetFontMgr().FormatNotify(Game.GetInstance().GetFontMgr().mFontHeader, new Vector2(0.5f, 0.5f), Color.Yellow.ToVector3(), 1000, "Team wins!");

            Init();
        }

        public override void TakeDamage(Controller attacker, Controller attacked, float damage)
        {
            // oi! its humn v AI only!
            if (attacker.GetTeam() == attacked.GetTeam())
                return;

            Team attackerTeam = attacker.GetTeam();
            attackerTeam.mScore++;

            attacked.GetPawn().TakeDamage(attacker, damage);

            if (attackerTeam.mScore > mKillLimit)
                EndGame(attacker);
        }

        public override void Update(GameTime gameTime)
        {
            // respawn players
            foreach (Controller c in Game.GetInstance().mController)
            {
                if (!c.GetPawn().IsAlive() && c.GetPawn().GetTimeInState() > 1.0f && c.GetPawn().CanRespawn())
                    c.GetPawn().Respawn(false);
            }
        }

        public override void Render()
        {
            // render team tags
            foreach (Team t in mTeam)
            {
                foreach (Controller c in t.mController)
                {
                    RenderTeamIcon(t.mColor, c.GetPawn().mPosition);
                }
            }
        }

        public override String GetName()
        {
            return "Team Free For All";
        }

        public override String GetDescription()
        {
            return "Multiple teams, take each others teams down";
        }
    }
}
