
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    //
    // Enemy spawns in waves
    // must kill all enemys to go to next wave
    // next wave is more difficult
    //
    public class Invasion : GameMode
    {
        public enum TeamType
        {
            Player,
            Enemy,
        }

        public enum State
        {
            Playing,
            EndGame,
        }

        public int      mWaveCount;
        public float    mPickupSpawnChance = 0.3f;
        public State    mState = State.Playing;
        public float    mTimeInState = 0.0f;

        public override void Init()
        {
            Deinit();

            // make 2 teams, team 0 = humans, team 1 = enemys
            Team team0 = new Team();
            team0.mRemaningLives = 10;
            mTeam.Add(team0);

            Team team1 = new Team();
            team1.mRemaningLives = 0;
            mTeam.Add(team1);

            // add players to player team
            foreach (Controller c in Game.GetInstance().mController)
            {
                if (c.GetType() == typeof(PlayerController))
                {
                    c.GetPawn().Respawn(true);
                    AddControllerToTeam(c, team0);
                }
            }

            mWaveCount = 0;
            ResetNextWave();

            base.Init();
        }

        public void CheckPickupSpawn(Controller from)
        {
            bool spawn = Game.GetInstance().random.NextDouble() < mPickupSpawnChance;
            if (!spawn)
                return;

            Pickup p = Pickup.SpawnRandom(from.GetPawn().mPosition);
            if (p != null)
                Game.GetInstance().mPickup.Add(p);
        }

        public void ResetNextWave()
        {
            // make players invulnerable
            foreach (Controller c in Game.GetInstance().mController)
            {
                if (c.GetType() == typeof(PlayerController))
                {
                    c.GetPawn().SetState(Pawn.State.Alive);
                }
            }

            int enemyCount = mWaveCount + 1;

            // clear the enemy team
            Team enemyTeam = GetTeam(TeamType.Enemy);
            foreach (Controller c in enemyTeam.mController)
            {
                Game.GetInstance().RemoveController(c);
            }

            // spawn new enemys
            enemyTeam.mController.Clear();

            for (int i = 0; i < enemyCount; ++i)
            {
                Controller c = Game.GetInstance().AddAIController();
                c.GetPawn().Respawn(true);
                AddControllerToTeam(c, enemyTeam);
            }


            Game.GetInstance().GetFontMgr().FormatNotify(Game.GetInstance().GetFontMgr().mFontHeader, new Vector2(0.5f, 0.5f), Color.Yellow.ToVector3(), 2000, "Wave {0}", mWaveCount + 1);

            ++mWaveCount;
        }

        public void EndGame()
        {
            Game.GetInstance().GetFontMgr().FormatNotify(Game.GetInstance().GetFontMgr().mFontHeader, new Vector2(0.5f, 0.5f), Color.Yellow.ToVector3(), 2000, "Game Over");
            Init();
        }
        
        public override void TakeDamage(Controller attacker, Controller attacked, float damage)
        {
            // oi! its humn v AI only!
            Team attackedTeam = attacked.GetTeam();
            if (attacker.GetTeam() == attackedTeam)
                return;

            attacked.GetPawn().TakeDamage(attacker, damage);

            // random chance to spawn a pickup if a foe dies
            Team enemyTeam = GetTeam(TeamType.Enemy);
            if (attacked.GetPawn().mState == Pawn.State.Dead && attackedTeam == enemyTeam)
                CheckPickupSpawn(attacked);

            // check if any players are left alive
            int aliveCount = 0;
            Team playerTeam = GetTeam(TeamType.Player);
            if (playerTeam.mRemaningLives <= 0)
            {
                foreach (Controller c in playerTeam.mController)
                {
                    if (c.GetPawn().IsAlive())
                        ++aliveCount;
                }

                if (aliveCount <= 0)
                {
                    EndGame();
                    return;
                }
            }

            // checked if any enemys are left alive
            aliveCount = 0;
            foreach (Controller c in enemyTeam.mController)
            {
                if (c.GetPawn().IsAlive())
                    ++aliveCount;
            }

            if (aliveCount <= 0)
            {
                ResetNextWave();
            }
        }

        public override void Update(GameTime gameTime)
        {
            mTimeInState += (float)gameTime.ElapsedGameTime.Ticks / (float)System.TimeSpan.TicksPerSecond;

            // respawn players if they have sufficient team lives left
            Team playerTeam = GetTeam(TeamType.Player);
            if (playerTeam.mRemaningLives > 0)
            {
                foreach (Controller c in playerTeam.mController)
                {
                    if (!c.GetPawn().IsAlive() && c.GetPawn().GetTimeInState() > 1.0f && c.GetPawn().CanRespawn())
                    {
                        c.GetPawn().Respawn(false);

                        Game.GetInstance().GetFontMgr().FormatNotify(Game.GetInstance().GetFontMgr().mFontHeader, new Vector2(0.5f, 0.5f), Color.Yellow.ToVector3(), 1000, "{0} lives remain", playerTeam.mRemaningLives);

                        --mTeam[(int)TeamType.Player].mRemaningLives;
                    }
                }
            }
        }

        public Team GetTeam(TeamType type)
        {
            if ((int)type >= mTeam.Count)
                return null;

            return mTeam[(int)type];
        }

        public override String GetName()
        {
            return "Invasion";
        }

        public override String GetDescription()
        {
            return "Enemy spawns in waves, players VS the enemy waves";
        }
    }
}
