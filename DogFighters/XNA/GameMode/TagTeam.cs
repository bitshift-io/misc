
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    //
    // Shoot an enemy and he will join your team
    // game ends when only 1 team remains
    //
    public class TagTeam : GameMode
    {
        public int mNumAI = 5;

        public override void Init()
        {
            Deinit();

            // remove old teams
            //mTeam.Clear();

            // make a new team for each player
            foreach (Controller c in Game.GetInstance().mController)
            {
                if (c.GetType() == typeof(PlayerController))
                {
                    Team team = new Team();
                    c.GetPawn().Respawn(true);
                    team.mColor = c.GetPawn().GetColor();
                    AddControllerToTeam(c, team);
                    mTeam.Add(team);
                }
            }
            
            // add some ai players
            for (int i = 0; i < mNumAI; ++i)
            {
                Team team = new Team();
                Controller c = Game.GetInstance().AddAIController();

                switch (i)
                {
                case 0:
                    c.GetPawn().SetColor(Color.WhiteSmoke.ToVector3());
                    break;

                case 1:
                    c.GetPawn().SetColor(Color.Violet.ToVector3());
                    break;

                case 2:
                    c.GetPawn().SetColor(Color.Tomato.ToVector3());
                    break;

                case 3:
                    c.GetPawn().SetColor(Color.SteelBlue.ToVector3());
                    break;

                case 4:
                    c.GetPawn().SetColor(Color.Teal.ToVector3());
                    break;
                }

                team.mColor = c.GetPawn().GetColor();
                AddControllerToTeam(c, team);
                mTeam.Add(team);
            }

            base.Init();
        }

        public override void Deinit()
        {
            // remove old teams
            //mTeam.Clear();

            base.Deinit();
        }

        public void EndGame()
        {
            Game.GetInstance().GetFontMgr().FormatNotify(Game.GetInstance().GetFontMgr().mFontHeader, new Vector2(0.5f, 0.5f), Color.Yellow.ToVector3(), 1000, "Game Over");
            Init();
        }
        
        public override void TakeDamage(Controller attacker, Controller attacked, float damage)
        {
            // oi! its humn v AI only!
            if (attacker.GetTeam() == attacked.GetTeam())
                return;

            // team eliminated
            Team attackedTeam = attacked.GetTeam();

            AddControllerToTeam(attacked, attacker.GetTeam());

            if (attackedTeam.mController.Count <= 0)
                mTeam.Remove(attackedTeam);

            if (mTeam.Count <= 1)
                EndGame();
        }

        public override void Render()
        {
            // render team tags
            foreach (Team t in mTeam)
            {
                foreach (Controller c in t.mController)
                {
                    RenderTeamIcon(t.mColor, c.GetPawn().mPosition);
                }
            }
        }

        public override String GetName()
        {
            return "Tag Team";
        }

        public override String GetDescription()
        {
            return "All players start on thier own team, apon shooting an enemy player reults in them joining your team." +
                "GameEnd when one team remains";
        }
    }
}
