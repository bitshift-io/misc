using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace DogFighter
{
    public class Pawn : Entity
    {
        //weapons
        public Weapon mWeaponPrimary;
        public Weapon mWeaponSecondary;
                       
        //health
        public float mHealth = 0.0f;
        public float mShield = 0.0f;

        //initialize model shit
        public Vector3 mPosition = Vector3.Zero;
        public float mRotation = 0.0f;
        public float mTilt = 0.0f;
        public float mRadius = 1000.0f;
        public Vector3 mVelocity = Vector3.Zero;
        public float mSpeed = 110.0f;
        public float mSin = 0;

        public float mRespawnProtectionTime = 1.0f;

        Effect mEffect = null;

        MeshRibbon mContrail = null;

        //3d m to draw
        Model mModel = null;
        Model mParachute = null;

        public Vector3 mColor = new Vector3(0.0f, 1.0f, 0.0f);

        //controller
        public Controller mController = null;



        //states
        public enum State
        {
            Alive,
            Parachute,
            Dead
        }
        public State mState = State.Alive;
        public State mPreviousState = State.Alive;

        float mTimeInState = 0.0f;

        public Vector3 GetColor()
        {
            return mColor;
        }

        public void SetColor(Vector3 color)
        {
            mContrail.mColor = color;
            mColor = color;
        }

        public Controller GetController()
        {
            return mController;
        }

        public Vector3 GetDirection()
        {
            //rotation to direction
            Vector3 direction = Vector3.Zero;
            direction.X = -(float)Math.Sin(mRotation);
            direction.Z = -(float)Math.Cos(mRotation);
            return direction;
        }

        public bool IsAlive()
        {
            return (mState != State.Dead);
        }

        public void TakeDamage(Controller attacker, float healthDamage)
        {
            if (!CanTakeDamage())
                return;

            mHealth -= healthDamage;

            if (mHealth <= 0)
            {
                //goto parachure mode, reset health
                if (mState == State.Alive) 
                {
                    if (attacker == mController)
                    {
                        mController.NotifySuicide();
                        mHealth = 1.0f;
                    }
                    else
                    {
                        mController.NotifyDead();
                        attacker.NotifyKilled();
                        mHealth = 1.0f;
                    }
                    SetState(State.Parachute);
                }
                //dead for good! dont notify
                else if (mState == State.Parachute)
                {
                    if (attacker == mController)
                        mController.NotifySuicide();
                    else
                    {
                        mController.NotifyDead();
                        attacker.NotifyKilled();
                    }
                    SetState(State.Dead);
                }
            }
        }

        public bool CanTakeDamage()
        {
            // respawn protection
            if (mState == State.Alive && mTimeInState < mRespawnProtectionTime)
                return false;

            return true;
        }

        public Pawn(Controller controller, Color color)
        {
            mController = controller;
            
            mContrail = new MeshRibbon(Game.GetInstance().content.Load<Texture2D>("Content\\Textures\\Contrail01"));
            SetColor(color.ToVector3());

            LoadGraphicsContent(true);
            Respawn(true);

            //GiveWeapon(Weapon.StandardPrimary());
            //GiveWeapon(Weapon.StandardSecondary());
        }

        public void SetState(State state)
        {
            mPreviousState = mState;

            switch (state)
            {
                case State.Alive:
                    EnterAlive();
                    break;

                case State.Parachute:
                    EnterParachute();
                    break;

                case State.Dead:
                    EnterDead();
                    break;
            }

           
            mState = state;
            mTimeInState = 0.0f;
        }

        public void EnterAlive()
        {
            if (mPreviousState != State.Alive)
                mRotation = MathHelper.ToRadians(Game.GetInstance().random.Next(0, 360));

            mPosition.Y = 0.0f;
            mContrail.Reset();
        }

        public void EnterParachute()
        {

        }

        public void EnterDead()
        {

        }

        protected void LoadGraphicsContent(bool loadAllContent)
        {
            if (loadAllContent)
            {
                Game game = Game.GetInstance();
                mModel = game.content.Load<Model>("Content\\Models\\biplane01");
                mParachute = game.content.Load<Model>("Content\\Models\\parachute01");
                //mEffect = game.content.Load<Effect>("Content\\Effects\\Test");
            }

            // TODO: Load any ResourceManagementMode.Manual content
        }

        public void Render()
        {
            switch (mState)
            {
            case State.Alive:
                DrawAlive();
                break;

            case State.Parachute:
                DrawParachute();
                break;

            case State.Dead:
                DrawDead();
                break;
            }
        }

        public void DrawModel(Model model)
        {
            Game game = Game.GetInstance();
            Graphics graphics = game.GetGraphics();

            // pulse colour if under repawn protection
            Vector3 color = mColor;
            if (!CanTakeDamage())
            {
                float wave = (float)Math.Sin(mTimeInState * Math.PI * 2.0f * 5.0f);
                color = color * wave + (color * 0.8f) * (1.0f - wave);
            }

            graphics.SetModelDiffuseColor(model, color);
            graphics.Render(model, Matrix.CreateRotationZ(mTilt) * Matrix.CreateRotationY(mRotation) * Matrix.CreateTranslation(mPosition), game.GetCamera());
        }

        public void DrawParachute()
        {
            DrawModel(mParachute);
        }

        public void DrawAlive()
        {
            DrawModel(mModel);
            mContrail.Render();
        }

        public void DrawDead()
        {
            DrawModel(mParachute);
        }


        public void Respawn(bool reset)
        {
            SetState(State.Alive);

            //player health/shields
            mHealth = 100.0f;
            mShield = 50.0f;

            Game game = Game.GetInstance();
            Environment environment = game.GetEnvironment();

            float halfWidth = environment.mHalfWidth + mRadius / 2;
            float halfHeight = environment.mHalfHeight + mRadius / 2;

            bool verticalSpawn = game.random.Next(0, 2) == 0 ? false : true;
            float spawnSide = game.random.Next(0, 2) == 0 ? -1.0f : 1.0f;
            if (verticalSpawn)
            {
                mPosition.X = spawnSide * halfWidth;
                mPosition.Z = ((float)game.random.NextDouble() * 2.0f - 1.0f) * environment.mHalfHeight;

                // make sure we come out the correct side with in 90 degrees
                mRotation = ((float)game.random.NextDouble() * (float)Math.PI - (float)Math.PI * 0.5f) * spawnSide;
            }
            else
            {
                mPosition.X = ((float)game.random.NextDouble() * 2.0f - 1.0f) * environment.mHalfWidth;
                mPosition.Z = spawnSide * halfHeight;

                // make sure we come out the correct side with in 90 degrees
                mRotation = ((float)game.random.NextDouble() * (float)Math.PI + (float)Math.PI * 0.5f) * spawnSide;
            }

            mPosition.Y = 0.0f;

            //mPosition.X = ((float)game.random.NextDouble() * 2.0f - 1.0f) * game.halfWidth;
            //mPosition.Z = ((float)game.random.NextDouble() * 2.0f - 1.0f) * game.halfHeight;
            //mRotation = (float)game.random.NextDouble() * (float)Math.PI * 2.0f;

            if (reset)
            {
                GiveWeapon(Weapon.StandardPrimary());
                GiveWeapon(Weapon.StandardSecondary());
            }
        }

        public void GiveWeapon(Weapon weapon)
        {
            if (weapon.IsPrimary())
            {
                mWeaponPrimary = weapon;
            }
            else
            {
                mWeaponSecondary = weapon;
            }
            weapon.SetOwner(this);
        }

        public void Update(GameTime gameTime)
        {
            mTimeInState += (float)gameTime.ElapsedGameTime.Ticks / (float)System.TimeSpan.TicksPerSecond;
            switch (mState)
            {
                case State.Alive:
                    UpdateAlive();
                    break;

                case State.Parachute:
                    UpdateParachute();
                    break;

                case State.Dead:
                    UpdateDead();
                    break;
            }
        }

        public void UpdateParachute()
        {
            if (mTimeInState > 4.0f)
                SetState(State.Alive);

            Game game = Game.GetInstance();
            Environment environment = game.GetEnvironment();

            //rotate m using left humb stick, and scale it down
            float move = mController.mSteerDir.X * 40.0f; 

            //if mController.mSteerDir.X -, tilt left, else tile right
            mTilt = mController.mSteerDir.X * -0.7f;

            //add some wave affect for fun
            mSin = ((float)Math.Sin((game.gameMS * 0.003f) + mTimeInState) - 0.5f) * 0.2f;
            mRotation = mSin;

            //Constant downward movment
            Vector3 mVelocity = (Vector3.Backward * mSpeed * 0.6f) + (Vector3.Right * move) + (Vector3.Forward * mController.mSteerDir.Y * mSpeed * 0.3f);

            float halfWidth = environment.mHalfWidth + mRadius / 2;
            float halfHeight = environment.mHalfHeight + mRadius / 2;

            //Check for wrap around shiz!
            if (mPosition.X > halfWidth)
            {
                mPosition.X = -halfWidth;
            }
            if (mPosition.X < -halfWidth)
            {
                mPosition.X = halfWidth;
            }

            if (mPosition.Z > halfHeight)
            {
                mPosition.Z = -halfHeight;
            }
            if (mPosition.Z < -halfHeight)
            {
                mPosition.Z = halfHeight;
            }

            //add velocity
            mPosition += mVelocity;

            //Collision checkage!
            CheckCollision(false, true);
        }

        public void UpdateAlive()
        {
            Game game = Game.GetInstance();
            Environment environment = game.GetEnvironment();

            //update weapons
            mWeaponPrimary.Update();
            if (mWeaponSecondary != null)
            {
                mWeaponSecondary.Update();
            }

            //rotate m using left humb stick, and scale it down
            mRotation -= mController.mSteerDir.X * 0.04f; //rotate sensitivity

            //if mController.mSteerDir.X -, tilt left, else tile right
            mTilt = mController.mSteerDir.X * -0.7f;

            //Constant forwards movment
            Vector3 mVelocity = GetDirection() * mSpeed;

            //Fire :)
            if (mController.mFirePrimary == true)
            {
                if (mWeaponPrimary != null)
                {
                    if (mWeaponPrimary.Fire())
                        mController.NotifyFire();
                }
            }

            if (mController.mFireSecondary == true)
            {
                if (mWeaponSecondary != null)
                {
                    if (mWeaponSecondary.Fire())
                        mController.NotifyFire();
                }
            }

            float halfWidth = environment.mHalfWidth + mRadius / 2;
            float halfHeight = environment.mHalfHeight + mRadius / 2;

            //Check for wrap around shiz!
            if (mPosition.X > halfWidth)
            {
                mPosition.X = -halfWidth;
                mContrail.Reset();
            }
            if (mPosition.X < -halfWidth)
            {
                mPosition.X = halfWidth;
                mContrail.Reset();
            }

            if (mPosition.Z > halfHeight)
            {
                mPosition.Z = -halfHeight;
                mContrail.Reset();
            }
            if (mPosition.Z < -halfHeight)
            {
                mPosition.Z = halfHeight;
                mContrail.Reset();
            }

            //add velocity
            mPosition += mVelocity;

            //Collision checkage!
            CheckCollision(true, false);

            // update effects
            GameEffectData effectData = new GameEffectData();
            effectData.transform = Matrix.CreateTranslation(mPosition);
            effectData.scale = 1.0f;
            effectData.velocity = mVelocity;
            mContrail.Update(effectData);
        }

        public bool CheckCollision(bool causeDamage, bool pushAway)
        {
            bool collision = false;

            Game game = Game.GetInstance();

            foreach (Controller c in game.mController)
            {
                //dont want to check against out own ship!
                if (this == c.mPawn)
                    continue;

                //dont want to check against dead ships
                if (c.mPawn.mState == State.Dead)
                    continue;

                //we need to check what state the attacker is in, if hes parachute, dont damage us
                if (c.mPawn.mState == State.Parachute && mState != State.Parachute)
                    continue;

                Vector3 distVector = mPosition - c.mPawn.mPosition;
                float dist = distVector.Length();
                dist = dist - (c.mPawn.mRadius + mRadius);
                if (dist < 0)
                {
                    // controllers of same type push away from each other
                    bool sameTeamPushAway = (c.GetTeam() != null && c.GetTeam() == mController.GetTeam());

                    if (pushAway || sameTeamPushAway)
                    {
                        distVector.Normalize();
                        mPosition += distVector * -dist;
                    }

                    if (causeDamage && !sameTeamPushAway)
                    {
                        TakeDamage(mController, 100.0f);
                        c.mPawn.TakeDamage(c, 100.0f);
                    }
                    collision = true;
                }

            }

            return collision;
        }

        public void UpdateDead()
        {
            Vector3 direction = Vector3.Down;
            mVelocity += direction * 10.0f;

            //add velocity
            mPosition += mVelocity;

            //if (mTimeInState > 1.0f && CanRespawn())
            //    Respawn();
        }

        public float GetTimeInState()
        {
            return mTimeInState;
        }

        virtual public bool CanRespawn()
        {
            // check for collisions (dont cause damage)
            return !CheckCollision(false, false);
        }

        /*
        //loads model effect and we can replace it :)
        [ContentProcessor]
        public class CustomEffectModelProcessor : ModelProcessor
        {
            protected override MaterialContent ConvertMaterial(MaterialContent material, ContentProcessorContext context)
            {
                EffectMaterialContent myMaterial = new EffectMaterialContent();
                string directory = Path.GetDirectoryName(rootNode.Identity.SourceFilename);
                string effectPath = Path.Combine(directory, "MyEffect.fx");
                myMaterial.Effect = new ExternalReference<EffectContent>(effectPath);
                return base.ConvertMaterial(myMaterial, context);
            }
        }
        */

    }
}
