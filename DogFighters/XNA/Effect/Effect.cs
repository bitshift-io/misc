
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion


namespace DogFighter
{
    public struct GameEffectData
    {
        public Vector3  velocity;
        public Matrix   transform;
        public Vector3  color;
        public float    scale;
    }

    public class GameEffect
    {
        public virtual void Update(GameEffectData effectData)
        {
        }

        public virtual void Render()
        {
        }

        public virtual void Reset()
        {
        }
    }
}
