
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    class MeshRibbon : GameEffect
    {
        BasicEffect             mEffect;

        VertexDeclaration       mVertexDeclaration;
        VertexPositionTexture[] mVertices;
        VertexBuffer            mVertexBuffer;

        short[]                 mIndices;
        IndexBuffer             mIndexBuffer;

        int                     mMaxSegment = 30;
        float                   mWidth = 800.0f;

        int                     mUpdateRate = 0;
        int                     mCurUpdate = 0;

        List<Vector3>           mRibbon = new List<Vector3>();
        Texture2D               mTexture = null;

        public Vector3          mColor = new Vector3(1.0f, 1.0f, 1.0f);

        public MeshRibbon(Texture2D texture)
        {
            Graphics graphics = Game.GetInstance().GetGraphics();

            mTexture = texture;

            mEffect = graphics.CreateBasicEffect();
            mVertexDeclaration = graphics.CreateVertexDeclaration(VertexPositionTexture.VertexElements);            

            int maxVerts = (mMaxSegment + 1) * 2;
            mVertices = new VertexPositionTexture[maxVerts];
            mVertexBuffer = graphics.CreateVertexBuffer(maxVerts, VertexPositionTexture.SizeInBytes);

            int maxTris = mMaxSegment * 2;
            int maxIndices = maxTris * 3;
            mIndices = new short[maxIndices];

            int idx = 0;
            int startVert = 0;
            for (int i = 0; i < maxTris; i += 2)
            {
                mIndices[idx + 0] = (short)(startVert + 0);
                mIndices[idx + 1] = (short)(startVert + 1);
                mIndices[idx + 2] = (short)(startVert + 2);
                mIndices[idx + 3] = (short)(startVert + 1);
                mIndices[idx + 4] = (short)(startVert + 3);
                mIndices[idx + 5] = (short)(startVert + 2);
                 
                startVert += 2;
                idx += 6;
            }

            mIndexBuffer = graphics.CreateIndexBuffer(mIndices.Length, sizeof(short));

            mIndexBuffer.SetData<short>(mIndices);
        }

        public override void Update(GameEffectData effectData)
        {
            ++mCurUpdate;

            Vector3 curPos = effectData.transform.Translation;

            if (mCurUpdate < mUpdateRate && mRibbon.Count > 1)
            {
                mRibbon[mRibbon.Count - 1] = curPos;
            }
            else
            {
                mRibbon.Add(curPos);
                mCurUpdate = 0;
            }

            // remove the end of the ribbon (ie the start, the oldest)
            if (mRibbon.Count > (mMaxSegment + 1))
                mRibbon.RemoveAt(0);

            if (mRibbon.Count <= 1)
                return;

            // generate vertex data from current info
            int vertIdx = 0;
            Vector3 lastPos = mRibbon[1];
            for (int i = 0; i < mRibbon.Count; ++i)
            {
                float percent = 1.0f - (float)i / (float)(mRibbon.Count - 1);

                curPos = mRibbon[i];
                Vector3 right = Vector3.Normalize(curPos - lastPos);
                right = Vector3.Cross(right, i == 0 ? -Vector3.Up : Vector3.Up); // up should be camera vector

                mVertices[vertIdx].TextureCoordinate = new Vector2(0.0f, percent);
                mVertices[vertIdx].Position = curPos + right * mWidth;
                ++vertIdx;

                mVertices[vertIdx].TextureCoordinate = new Vector2(1.0f, percent); 
                mVertices[vertIdx].Position = curPos - right * mWidth;
                ++vertIdx;

                lastPos = curPos;
            }

            mVertexBuffer.SetData<VertexPositionTexture>(mVertices);
        }

        public override void Render()
        {
            if (mRibbon.Count <= 1)
                return;

            //mEffect.Alpha = 0.5f;
            mEffect.Texture = mTexture;
            mEffect.TextureEnabled = true;
            mEffect.DiffuseColor = mColor;

            //mEffect.CommitChanges();

            //mEffect.EnableDefaultLighting();

            Game.GetInstance().GetGraphics().DrawIndexed(Game.GetInstance().GetCamera(), mEffect, mVertexBuffer, mIndexBuffer, mVertexDeclaration, mRibbon.Count * 2, (mRibbon.Count - 1) * 2 * 3, VertexPositionTexture.SizeInBytes);
        }

        public override void Reset()
        {
            mRibbon.Clear();
        }
    }
}
