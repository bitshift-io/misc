using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;

namespace DogFighter
{
    class CloudMgr
    {
        struct Cloud
        {
            public Vector3 position;
            public float rotation;
            public float scale;
        }
        public ArrayList mCloud = new ArrayList();

        public Model mModel = null;
        public float mRadius = 30000.0f;

        public Vector3 mWindDirection;
        public float mWindSpeed = 10.0f;

        public void Update()
        {
            Game game = Game.GetInstance();
            Environment environment = game.GetEnvironment();

            float halfWidth = (environment.mHalfWidth + mRadius) / 2.0f;
            float halfHeight = (environment.mHalfHeight + mRadius) / 2.0f;

            for (int i = 0; i < mCloud.Count; ++i)
            {
                Cloud cloud = (Cloud)mCloud[i];
                Vector3 deviation = new Vector3((float)game.random.NextDouble() * 2.0f - 1.0f, 0.0f, (float)game.random.NextDouble() * 2.0f - 1.0f);
                deviation.Normalize();
                deviation *= (float)game.random.NextDouble() * 5.0f;
                cloud.position += mWindDirection * mWindSpeed + deviation;

                //Check for wrap around shiz!
                if (cloud.position.X > halfWidth)
                {
                    cloud.position.X = -halfWidth;
                }
                if (cloud.position.X < -halfWidth)
                {
                    cloud.position.X = halfWidth;
                }

                if (cloud.position.Z > halfHeight)
                {
                    cloud.position.Z = -halfHeight;
                }
                if (cloud.position.Z < -halfHeight)
                {
                    cloud.position.Z = halfHeight;
                }              

                mCloud[i] = cloud;
            }
        }

        public void Render()
        {
            for (int i = 0; i < mCloud.Count; ++i)
            {
                Cloud cloud = (Cloud)mCloud[i];

                Game game = Game.GetInstance();
                Graphics graphics = game.GetGraphics();

                graphics.Render(mModel, Matrix.CreateRotationY(cloud.rotation) * Matrix.CreateTranslation(cloud.position), game.GetCamera());
            }
        }

        public void SpawnInitial()
        {
            Game game = Game.GetInstance();
            Environment environment = game.GetEnvironment();

            mWindDirection = new Vector3(((float)game.random.NextDouble() * 2.0f - 1.0f), 0.0f, ((float)game.random.NextDouble() * 2.0f - 1.0f));
            mWindDirection.Normalize();

            // spawn some clouds
            float halfWidth = (environment.mHalfWidth + mRadius) / 2.0f;
            float halfHeight = (environment.mHalfHeight + mRadius) / 2.0f;

            for (int i = 0; i < 10; ++i)
            {
                Cloud cloud = new Cloud();

                float height = game.random.NextDouble() > 0.5f ? 2000.0f : -2000.0f;
                cloud.position = new Vector3(((float)game.random.NextDouble() * 2.0f - 1.0f) * halfWidth, height, ((float)game.random.NextDouble() * 2.0f - 1.0f) * halfHeight);
                cloud.rotation = 0.0f;
                cloud.scale = 1.0f;
                mCloud.Add(cloud);
            }
        }

        public void LoadGraphicsContent(bool loadAllContent)
        {
            if (loadAllContent)
            {
                mModel = Game.GetInstance().content.Load<Model>("Content\\Models\\cloud01");
            }

            // TODO: Load any ResourceManagementMode.Manual content
        }
    }
}
