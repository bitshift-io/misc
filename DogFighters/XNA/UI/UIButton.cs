
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    class UIButton : UIWindow
    {
        String mText;

        public UIButton(NotifyEvent callback, int id, String text) : base(callback, id)
        {
            mText = text;
        }

        //
        // returns true if input has been handled and should not be handled again
        //
        public override bool Update(UIInput input)
        {
            if (mFocus && IsVisible())
            {
                if (input.mAccept)
                {
                    mNotifyEvent(this);
                    return true;
                }
            }

            return base.Update(input);
        }

        public override void Render()
        {
            if (!IsVisible())
                return;

            Color color;
            if (mFocus)
                color = Color.Yellow;
            else
                color = Color.White;

            base.Render();

            Game.GetInstance().GetFontMgr().mFontMain.Format(new Vector2(mRect.X, mRect.Y), color, mText); 
        }
    }
}
