
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    public class UIWindow
    {
        public delegate void NotifyEvent(UIWindow window);

        protected NotifyEvent       mNotifyEvent    = null;
        protected List<UIWindow>    mChild          = new List<UIWindow>();
        protected SpriteBatch       mSpriteBatch    = null;
        protected Texture2D         mTexture        = null;
        protected Vector4           mRect           = new Vector4(0.0f, 0.0f, 1.0f, 1.0f); // x, y, width, height
        protected bool              mVisible        = true;
        protected bool              mEnabled        = true;
        protected int               mId             = -1;
        protected bool              mFocus          = false;
        protected UIWindow          mParent         = null;

        public UIWindow(NotifyEvent callback, int id)
        {
            mId = id;
            mNotifyEvent = callback;
        }

        public void SetTexture(Texture2D texture)
        {
            if (mSpriteBatch == null)
                mSpriteBatch = Game.GetInstance().GetGraphics().CreateSpriteBatch();

            mTexture = texture;
        }

        public UIWindow GetParent()
        {
            return mParent;
        }

        public bool IsVisible()
        {
            if (GetParent() != null && !GetParent().IsVisible())
                return false;

            return mVisible;
        }

        public void SetVisible(bool visible)
        {
            mVisible = visible;
        }

        public void AddChild(UIWindow window)
        {
            window.mParent = this;
            mChild.Add(window);
        }

        public List<UIWindow> GetChildren()
        {
            return mChild;
        }

        public void SetRectangle(Vector4 rectangle)
        {
            mRect = rectangle;
        }

        public void SetFocus(bool focus)
        {
            mFocus = focus;
        }

        public int GetId()
        {
            return mId;
        }

        public UIWindow GetWindowById(int id)
        {
            if (mId == id)
                return this;

            foreach (UIWindow w in mChild)
            {
                UIWindow window = w.GetWindowById(id);
                if (window != null)
                    return window;
            }

            return null;
        }

        public UIWindow GetFirstVisibleChild()
        {
            foreach (UIWindow w in mChild)
            {
                if (w.IsVisible())
                    return w;
            }

            return null;
        }

        public UIWindow GetChildWithFocus()
        {
            foreach (UIWindow w in mChild)
            {
                if (w.mFocus)
                    return w;
            }

            return null;
        }

        public int GetChildIndex(UIWindow window)
        {
            return mChild.IndexOf(window);
        }

        public UIWindow GetChild(int idx)
        {
            return mChild[idx];
        }

        public int GetChildCount()
        {
            return mChild.Count;
        }

        public virtual bool Update(UIInput input)
        {
            foreach (UIWindow w in mChild)
            {
                if (w.Update(input))
                    return true;
            }

            return false;
        }

        public virtual void Render()
        {
            if (!IsVisible())
                return;

            Vector2 screenSize = Game.GetInstance().GetGraphics().GetDeviceDimensions();
            Vector4 rect = new Vector4(mRect.X * screenSize.X, mRect.Y * screenSize.Y, mRect.Z * screenSize.X, mRect.W * screenSize.Y);

            if (mTexture != null)
            {
                mSpriteBatch.Begin();
                mSpriteBatch.Draw(mTexture, new Rectangle((int)rect.X, (int)rect.Y, (int)rect.Z, (int)rect.W), Color.White);
                mSpriteBatch.End();
            }

            foreach (UIWindow w in mChild)
                w.Render();
        }        
    }
}
