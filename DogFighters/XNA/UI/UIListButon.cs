
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter.UI
{
    //
    // a list pressing left or right will toggle the current element
    //
    public class UIListButon : UIWindow
    {
        String[]    mText;
        int         mActive;

        public UIListButon(NotifyEvent callback, int id, String[] text) : base(callback, id)
        {
            mText = text;
            mActive = 0;
        }

        //
        // returns true if input has been handled and should not be handled again
        //
        public override bool Update(UIInput input)
        {
            if (mFocus && IsVisible())
            {
                if (input.mHorizMovement > 0.5f)
                {
                    mActive = (mActive + 1) % mText.Length;
                }

                if (input.mHorizMovement < 0.5f)
                {
                    mActive = (mActive - 1);
                    mActive = mActive < 0 ? (mText.Length  - 1) : mActive;
                }

                if (input.mAccept)
                {
                    mNotifyEvent(this);
                    return true;
                }
            }

            return base.Update(input);
        }

        public override void Render()
        {
            if (!IsVisible())
                return;

            Color color;
            if (mFocus)
                color = Color.Yellow;
            else
                color = Color.White;

            base.Render();

            Game.GetInstance().GetFontMgr().mFontMain.Format(new Vector2(mRect.X, mRect.Y), color, mText[mActive]); 
        }
    }
}
