
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    public class UIInput
    {
        public bool mAccept;
        public bool mCancel;

        public float mVertMovement;
        public float mHorizMovement;

        public void Update(GamePadState gamepad, KeyboardState keyboard)
        {
            mAccept = (gamepad.Buttons.A == ButtonState.Pressed) || keyboard.IsKeyDown(Keys.Enter);
            mCancel = (gamepad.Buttons.B == ButtonState.Pressed) || keyboard.IsKeyDown(Keys.Escape);

            mVertMovement = gamepad.ThumbSticks.Left.Y;
            mVertMovement += (keyboard.IsKeyDown(Keys.Up) ? 1.0f : 0.0f) + (keyboard.IsKeyDown(Keys.Down) ? -1.0f : 0.0f);

            mHorizMovement = gamepad.ThumbSticks.Left.X;
            mHorizMovement += (keyboard.IsKeyDown(Keys.Right) ? 1.0f : 0.0f) + (keyboard.IsKeyDown(Keys.Left) ? -1.0f : 0.0f);
        }
    }
}
