using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace DogFighter
{
    class FasterFiring : Pickup
    {
        public FasterFiring(Vector3 position)
            : base(position)
        {
            mColor = Color.Green.ToVector3();
        }

        public override bool Update(GameTime gameTime)
        {
            if (base.Update(gameTime) == false)
                return false;

            return !CheckCollision(mPosition, mRadius);
        }

        public override bool GiveToController(Controller c)
        {
            if (c.GetPawn().mWeaponPrimary.mReloadTime < 0.1f)
                return false;

            FontMgr fontMgr = Game.GetInstance().GetFontMgr();
            fontMgr.FormatFading3D(fontMgr.mFontMain, mPosition, c.GetPawn().GetColor(), 1000, "Faster Firing");

            c.GetPawn().mWeaponPrimary.mReloadTime *= 0.8f;
            return true;
        }
    }
}
