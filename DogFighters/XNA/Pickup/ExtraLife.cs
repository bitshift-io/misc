using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace DogFighter
{
    public class ExtraLife : Pickup
    {
        public ExtraLife(Vector3 position) : base(position)
        {
            mColor = Color.Yellow.ToVector3();
        }

        public override bool Update(GameTime gameTime)
        {
            if (base.Update(gameTime) == false)
                return false;

            return !CheckCollision(mPosition, mRadius);
        }

        public override bool GiveToController(Controller c)
        {
            FontMgr fontMgr = Game.GetInstance().GetFontMgr();
            fontMgr.FormatFading3D(fontMgr.mFontMain, mPosition, c.GetPawn().GetColor(), 1000, "Extra Life");

            c.GetTeam().mRemaningLives++;
            return true;
        }
    }
}
