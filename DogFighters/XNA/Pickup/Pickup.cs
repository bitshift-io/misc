using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;


namespace DogFighter
{
    public class Pickup : Entity
    {
        public Model    mModel = null;
        public Vector3  mPosition;
        public float    mLife = 20.0f;
        public float    mRadius = 1000.0f;
        public float    mCurLife = 0.0f;
        public float    mSpawnProtectionTime = 2.0f;
        public Vector3  mColor = Vector3.One;

        public Pickup(Vector3 position)
        {
            mPosition = position;
            mModel = Game.GetInstance().content.Load<Model>("content\\models\\pickup01");
        }

        public override bool Update(GameTime gameTime)
        {
            Game game = Game.GetInstance();
            Environment environment = game.GetEnvironment();

            mCurLife += (float)gameTime.ElapsedGameTime.Ticks / (float)System.TimeSpan.TicksPerSecond;

            mPosition -= Vector3.Backward * 20.0f;

            float halfWidth = environment.mHalfWidth + mRadius / 2;
            float halfHeight = environment.mHalfHeight + mRadius / 2;

            //Check for wrap around shiz!
            if (mPosition.X > halfWidth)
            {
                mPosition.X = -halfWidth;
            }
            if (mPosition.X < -halfWidth)
            {
                mPosition.X = halfWidth;
            }

            if (mPosition.Z > halfHeight)
            {
                mPosition.Z = -halfHeight;
            }
            if (mPosition.Z < -halfHeight)
            {
                mPosition.Z = halfHeight;
            }

            if (mCurLife > mLife)
            {
                // kill me, make me fall like a dead player
                return false;
            }

            return true;
        }

        public bool CanTakeDamage()
        {
            // respawn protection
            if (mCurLife < mSpawnProtectionTime)
                return false;

            return true;
        }

        virtual public bool CheckCollision(Vector3 position, float radius)
        {
            Game game = Game.GetInstance();

            //Collision checkage against ships!
            foreach (Controller c in game.mController)
            {
                // only players get pickups - done by gamemode!
                //if (c.GetType() != typeof(PlayerController))
                //    continue;

                //dont want to check against dead ships
                if (c.mPawn.mState == Pawn.State.Dead)
                    continue;

                // dont want human or oai to kill their type
                //if (c.GetTeam() != null && c.GetTeam())
                //if (c.GetType() == mController.GetType())
                //    continue;

                Vector3 distVector = position - c.mPawn.mPosition;
                float dist = distVector.Length();
                dist = dist - (c.mPawn.mRadius + radius);
                if (dist < 0)
                {
                    if (NotifyCollisionController(c))
                    {
                        return true;
                    }
                    else // push away
                    {
                    }
                }
            }

            return false;
        }

        public override bool TakeDamage(Controller attacker, float damage)
        {
            if (!CanTakeDamage())
                return false;

            mCurLife = mLife;
            return base.TakeDamage(attacker, damage);
        }

        public virtual bool NotifyCollisionController(Controller controller)
        {
            return Game.GetInstance().mCurGameMode.GiveEntityToController(this, controller);
        }

        public virtual void Render()
        {
            // pulse colour if under repawn protection
            Vector3 color = mColor;
            if (!CanTakeDamage())
            {
                float wave = (float)Math.Sin(mCurLife * Math.PI * 2.0f * 5.0f);
                color = color * wave + (color * 0.8f) * (1.0f - wave);
            }

            Graphics graphics = Game.GetInstance().GetGraphics();

            Matrix transform = Matrix.CreateTranslation(mPosition);
            graphics.SetModelDiffuseColor(mModel, color);
            graphics.Render(mModel, transform, Game.GetInstance().GetCamera());
        }

        public static Pickup SpawnRandom(Vector3 position)
        {
            float rand = (float)Game.GetInstance().random.NextDouble();
            if (rand > 0.5f)
            {
                return new ExtraLife(position);
            }
            else
            {
                return new FasterFiring(position);
            }

            return null;
        }
    }
}
