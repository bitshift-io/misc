using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;


namespace DogFighter
{
    class Bullet : Projectile
    {
        float mRotation = 0.0f;
        Vector3 mPosition = Vector3.Zero;
        float mSpeed = 300.0f;
        float mDamage = 0.0f;
        float mLife = 0.7f;
        Vector3 mVelocity = Vector3.Zero;
        float mRadius = 10.0f;
        int mNumFireLocation = 1;
        Vector3 mColor = Vector3.One;

        Model mModel = null;

        public override bool Update()
        {
            Vector3 mVelocityAdd = Vector3.Zero;

            //find out what direction we should be shooting using rotation.
            mVelocityAdd.X = -(float)Math.Sin(mRotation);
            mVelocityAdd.Z = -(float)Math.Cos(mRotation);

            //move the bullet
            mPosition += (mVelocityAdd * mSpeed) + mVelocity;

            //check for collision
            if (CheckCollision(mPosition, mRadius))
            {
                mLife = 0;
            }

            mLife -= 0.01f;
            if (mLife <= 0)
            {
                return false;
            }
            return true;
        }

        public override void Render()
        {
            Game.GetInstance().GetGraphics().SetModelDiffuseColor(mModel, mColor);            
            Game.GetInstance().GetGraphics().Render(mModel, Matrix.CreateRotationY(mRotation) * Matrix.CreateTranslation(mPosition), Game.GetInstance().GetCamera());
        }

        override public void NotifyCollisionController(Pawn pawn)
        {
            Game.GetInstance().GetGameMode().TakeDamage(mController, pawn.GetController(), 100.0f);
        }

        public override void Fire(Weapon weapon, float rotation, int currentFireLocation, Vector3 color)
        {
            base.Fire(weapon,rotation, currentFireLocation, color);

            mColor = color;

            //initial side thrust for missiles
            Vector3 direction = GetDirection(rotation);
            Vector3 sidePosition = Vector3.Normalize(direction) * 1000.0f;
            //Vector3 right = Vector3.Cross(direction, Vector3.Up);
            //Vector3 sidePosition = right * 1000.0f;
            //if (currentFireLocation == 0)
                //sidePosition = -sidePosition;

            mRotation = rotation;
            mPosition = weapon.mPawn.mPosition + sidePosition;
            
            LoadGraphicsContent(true);

        }

        protected void LoadGraphicsContent(bool loadAllContent)
        {
            if (loadAllContent)
            {
                mModel = Game.GetInstance().content.Load<Model>("Content\\Models\\bullet01");
            }
        }
    }
}
