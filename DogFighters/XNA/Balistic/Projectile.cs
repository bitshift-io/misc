using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace DogFighter
{
    class Projectile : Entity
    {
        public Controller mController = null;

        virtual public void Fire(Weapon weapon, float rotation, int currentFireLocation, Vector3 color)
        {
            mController = weapon.mPawn.mController;
        }

        virtual public bool Update()
        {
            return false;
        }

        virtual public void Render()
        {
        }

        virtual public bool CheckCollision(Vector3 position, float radius)
        {
            Game game = Game.GetInstance();

            //Collision checkage against ships!
            foreach (Controller c in game.mController)
            {
                //dont want to check against our own!
                if (mController == c)
                    continue;

                //dont want to check against dead ships
                if (c.mPawn.mState == Pawn.State.Dead)
                    continue;

                // dont want human or oai to kill their type
                //if (c.GetTeam() != null && c.GetTeam())
                //if (c.GetType() == mController.GetType())
                //    continue;

                Vector3 distVector = position - c.mPawn.mPosition;
                float dist = distVector.Length();
                dist = dist - (c.mPawn.mRadius + radius);
                if (dist < 0)
                {
                    NotifyCollisionController(c.mPawn);
                    return true;
                }
            }


            //Collision checkage against pickups
            foreach (Pickup p in game.mPickup)
            {
                Vector3 distVector = position - p.mPosition;
                float dist = distVector.Length();
                dist = dist - (p.mRadius + radius);
                if (dist < 0)
                {
                    NotifyCollisionPickup(p);
                    return true;
                }
            }

            return false;
        }

        virtual public void NotifyCollisionController(Pawn ship)
        {

        }

        virtual public void NotifyCollisionPickup(Pickup pickup)
        {
            Game game = Game.GetInstance();
            pickup.TakeDamage(mController, 1.0f);
        }

        virtual public Vector3 GetDirection(float rotation)
        {
            Vector3 direction = Vector3.Zero;
            //find out what direction we should be shooting using rotation.
            direction.X = -(float)Math.Sin(rotation);
            direction.Z = -(float)Math.Cos(rotation);
            return direction;
        }

    }
}
