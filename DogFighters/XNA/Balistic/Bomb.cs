using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace DogFighter
{
    class Bomb : Projectile
    {

        float mRotation = 0.0f;
        Vector3 mPosition = Vector3.Zero;
        float mSpeed = 1000.0f;
        float mDamage = 0.0f;
        float mLife = 1.0f;
        Vector3 mVelocity = Vector3.Zero;
        float mRadius = 10.0f;

        Model mModel = null;

        public override bool Update()
        {
            //Vector3 direction = GetDirection(mRotation);

            //side velocity reduces over time
            //mVelocity *= 0.6f;

            //move the bullet
            Vector3 direction = Vector3.Down;
            mVelocity += direction * 100.0f;

            if (mVelocity.Length() > mSpeed)
                mVelocity = Vector3.Normalize(mVelocity) * mSpeed;

            mPosition += mVelocity;

            //check for collision
            if (CheckCollision(mPosition, mRadius))
            {
                mLife = 0;
            }

            mLife -= 0.01f;
            if (mLife <= 0)
            {
                return false;
            }
            return true;
        }

        public override void Render()
        {
            Game.GetInstance().GetGraphics().Render(mModel, Matrix.CreateRotationY(mRotation) * Matrix.CreateTranslation(mPosition), Game.GetInstance().GetCamera());
        }

        override public void NotifyCollisionController(Pawn pawn)
        {
            Game.GetInstance().GetGameMode().TakeDamage(mController, pawn.GetController(), 100.0f);
        }

        public override void Fire(Weapon weapon, float rotation, int currentFireLocation, Vector3 color)
        {
            base.Fire(weapon, rotation, currentFireLocation, color);

            /*
            //initial side thrust for missiles
            Vector3 direction = GetDirection(rotation);
            Vector3 right = Vector3.Cross(direction,Vector3.Up);
            Vector3 sideVelocity = right * 1000.0f;
            if (currentFireLocation == 0)
                sideVelocity = -sideVelocity;
            */

            //inherit velocity from the mothership!
            mVelocity = weapon.mPawn.mVelocity;
            mRotation = rotation;
            mPosition = weapon.mPawn.mPosition;

            LoadGraphicsContent(true);

        }

        protected void LoadGraphicsContent(bool loadAllContent)
        {
            if (loadAllContent)
            {
                mModel = Game.GetInstance().content.Load<Model>("Content\\Models\\bomb01");
            }
        }

    }
}
