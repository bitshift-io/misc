using System;
using System.Collections.Generic;
using System.Text;

namespace DogFighter
{
    public class Weapon
    {
        public Pawn mPawn = null;
        bool mIsPrimary = true;

        public float mReloadTime = 0.0f;
        float        mCurReloadTime = 0.0f;

        int mNumFireLocation = 1;
        int mCurrentFireLocation = 0;

        public bool Update()
        {
            mCurReloadTime -= 0.01f;
            return true;
        }

        public bool Fire()
        {
            if (mCurReloadTime > 0)
            {
                return false;
            }

            //wrap around firelocation
            mCurrentFireLocation = (mCurrentFireLocation + 1) % mNumFireLocation;
 
            mCurReloadTime = mReloadTime;

            Projectile p = null;
            if (mIsPrimary == true)
                 p = new Bullet();
            else
                 p = new Bomb();

             p.Fire(this, mPawn.mRotation, mCurrentFireLocation, mPawn.GetColor());
            Game.GetInstance().mProjectile.Add(p);
            return true;
        }

        public void SetOwner(Pawn ship)
        {
            mPawn = ship;
        }

        static public Weapon StandardPrimary()
        {
            Weapon w = new Weapon();
            w.mIsPrimary = true;
            w.mReloadTime = 0.5f;
            w.mNumFireLocation = 2;
            return w;
        }

        static public Weapon StandardSecondary()
        {
            Weapon w = new Weapon();
            w.mIsPrimary = false;
            w.mReloadTime = 1.0f;
            w.mNumFireLocation = 2;
            return w;
        }

        public bool IsPrimary()
        {
            return mIsPrimary;
        }

    }
}
