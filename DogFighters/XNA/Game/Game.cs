
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
#endregion

namespace DogFighter
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        public static Game          mInstance        = null;

        public Environment          mEnvironment    = new Environment();
        public Stack<GameState>     mState          = new Stack<GameState>();
        public List<GameMode>       mGameMode       = new List<GameMode>();
        public GameMode             mCurGameMode    = null;
        public List<Entity>         mEntity         = new List<Entity>();

        public Camera               mCamera          = new Camera();

        public Graphics             mGraphics;

        //sound shiz
        public AudioEngine audioEngine;
        public WaveBank waveBank;
        public SoundBank soundBank;


        //public GraphicsDeviceManager graphics;
        public ContentManager content;

        public float fieldOfView = MathHelper.ToRadians(45.0f);
        public float aspectRatio = 1024.0f / 768.0f;


        //public Vector3 cameraPosition = new Vector3(0.0f, 40000.0f, 0.0f);
        public ArrayList mController = new ArrayList();
        public ArrayList mProjectile = new ArrayList();
        public List<Pickup> mPickup = new List<Pickup>();

        //SpriteBatch mSpriteBatch;

        public Random random = null;

        bool mPaused = false;
        float fps = 0.0f;
        int framecounter = 0;
        long lastFrameTime = 0;
        public long gameMS = 0;

        public FontMgr mFontMgr = null;



        public Game()
        {
            // set up singleton
            mInstance = this;

            // init game states
            mState.Push(new InGameState(this));
            mState.Push(new MenuState(this));

            // init game modes
            mGameMode.Add(new Invasion());
            mGameMode.Add(new FreeForAll());
            mGameMode.Add(new TeamFreeForAll());
            mGameMode.Add(new TagTeam());

            mGraphics = new Graphics(this);
            content = new ContentManager(Services);

            // init other systems
            random = new Random((int)System.DateTime.Now.Ticks);
            mFontMgr = new FontMgr();

        }

        public void PopState()
        {
            GameState last = mState.Pop();
            last.Exit();

            GameState cur = mState.Peek();
            if (cur != null)
                cur.Enter();
        }

        public void PushState(GameState state)
        {
            mState.Peek().Exit();
            state.Enter();
            mState.Push(state);
        }

        public GameState GetState()
        {
            return mState.Peek();
        }

        static public Game GetInstance()
        {
            return mInstance;
        }

        public Environment GetEnvironment()
        {
            return mEnvironment;
        }

        public Graphics GetGraphics()
        {
            return mGraphics;
        }

        public Camera GetCamera()
        {
            return mCamera;
        }


        protected override void Initialize()
        {
            base.Initialize();
            
            //audio setup
            //audioEngine = new AudioEngine("Content\\Sounds\\GameAudio.xgs");
            //waveBank = new WaveBank(audioEngine, "Content\\Sounds\\Wave Bank.xwb");
            //soundBank = new SoundBank(audioEngine, "Content\\Sounds\\Sound Bank.xsb");

            mGraphics.Init();
            
            mController.Add(new PlayerController(PlayerIndex.One));
            mController.Add(new PlayerController(PlayerIndex.Two));

            //for (int i = 0; i < 5; ++i)
            //    mController.Add(new AIController());

            mEnvironment.Init();
            mEnvironment.Load("test");

            // skip the menu, go strait ingame, until we get some UI code
            //PopState();
            GetState().Enter();
        }

        public GameMode GetGameMode()
        {
            return mCurGameMode;
        }

        public Controller AddAIController()
        {
            Controller c = new AIController();
            mController.Add(c);
            return c;
        }

        public FontMgr GetFontMgr()
        {
            return mFontMgr;
        }

        public void RemoveController(Controller c)
        {
            mController.Remove(c);
        }

        //load gfx content after this stage!
        protected override void LoadGraphicsContent(bool loadAllContent)
        {
            if (loadAllContent)
            {
                mFontMgr.LoadGraphicsContent(true);
            }
        }

        protected override void UnloadGraphicsContent(bool unloadAllContent)
        {
            if (unloadAllContent == true)
            {
                content.Unload();
            }
        }

        //Main game loop!
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // The time since Update was called last + calclate fps
            gameMS = gameTime.TotalGameTime.Ticks / System.TimeSpan.TicksPerMillisecond;
            int div = (int)((float)lastFrameTime / 1000.0f);
            int div2 = (int)((float)gameMS / 1000.0f);
            if (div != div2)
            {
                fps = framecounter;
                framecounter = 0;
                lastFrameTime = 0;
            }
            lastFrameTime = gameMS;

            //keyboard State
            KeyboardState keyboardState = Keyboard.GetState();

            //Keyboard Exit
            if (keyboardState.IsKeyDown(Keys.Escape))
                this.Exit();

            //pause
            if (keyboardState.IsKeyDown(Keys.P) || GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed)
                mPaused = !mPaused;

            if (mPaused)
                return;

            //fullscreen :)
            if ((keyboardState.IsKeyDown(Keys.LeftAlt) && keyboardState.IsKeyDown(Keys.Enter)) || (keyboardState.IsKeyDown(Keys.RightAlt) && keyboardState.IsKeyDown(Keys.Enter)))
                mGraphics.ToggleFullscreen();

            GetState().Update(gameTime);           

            base.Update(gameTime);


            //draw fonts
            mFontMgr.mFontSystem.Format(new Vector2(0.01f, 0.45f), Color.White, "FPS: {0}", System.Convert.ToString(fps));
            mFontMgr.mFontSystem.Format(new Vector2(0.60f, 0.95f), Color.Gray, "{0} {1}", System.DateTime.Now.ToLongTimeString(), System.DateTime.Today.ToLongDateString());

        }

        protected override void Draw(GameTime gameTime)
        {
            GetState().Render();
            base.Draw(gameTime);
            framecounter++;
        }
    }
}