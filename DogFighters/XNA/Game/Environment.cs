
#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;
using Org.Mentalis.Files;
#endregion

namespace DogFighter
{
    //
    // Environment specific info
    //
    public class Environment
    {
        Model               mBackground         = null;
        public Vector3      mModelPosition      = Vector3.Zero;
        CloudMgr            mCloudMgr           = new CloudMgr();

        public float        mHalfHeight         = 0.0f;
        public float        mHalfWidth          = 0.0f;

        public void Init()
        {
            // init environment
            Game game = Game.GetInstance();
        }

        public bool Load(string name)
        {
            Game game = Game.GetInstance();
            IniReader ini = new IniReader(name + ".ini");
            string test = ini.ReadString("section", "key", "invalid");

            // set up camera
            float fov = MathHelper.ToRadians(45.0f);
            float aspectRatio = 1024.0f / 768.0f;
            Vector3 cameraPosition = new Vector3(0.0f, 40000.0f, 0.0f);

            game.mCamera.SetPerspective(fov, aspectRatio, 20000.0f, 120000.0f);
            game.mCamera.LookAt(cameraPosition, Vector3.Zero, Vector3.Forward);

            mHalfWidth = (float)Math.Tan(game.fieldOfView * 0.5f) * cameraPosition.Y * game.aspectRatio;
            mHalfHeight = (float)Math.Tan(game.fieldOfView * 0.5f) * cameraPosition.Y;

            mBackground = game.content.Load<Model>("Content\\Models\\city01");

            mCloudMgr.LoadGraphicsContent(true);
            mCloudMgr.SpawnInitial();
            return false;
        }

        public void Update()
        {
            Game game = Game.GetInstance();

            Vector3 averageVelPos = Vector3.Zero;
            foreach (Controller c in game.mController)
            {
                if (c.GetType() == typeof(PlayerController) && c.mPawn.mState == Pawn.State.Alive)
                {
                    averageVelPos -= c.mPawn.GetDirection() * c.mPawn.mSpeed;
                }
            }

            averageVelPos.Y = 0.0f;
            mModelPosition += averageVelPos * 0.08f;

            mModelPosition.X = MathHelper.Clamp(mModelPosition.X, -20000.0f, 20000.0f);
            mModelPosition.Z = MathHelper.Clamp(mModelPosition.Z, -20000.0f, 20000.0f);

            mCloudMgr.Update();
        }

        public void Render()
        {
            Game game = Game.GetInstance();
            Graphics graphics = game.GetGraphics();

            graphics.Render(mBackground, Matrix.CreateTranslation(mModelPosition), game.GetCamera());
            /*
 
            //background mesh
            Matrix[] transforms = new Matrix[mBackground.Bones.Count];
            mBackground.CopyAbsoluteBoneTransformsTo(transforms);
            float count = mBackground.Meshes.Count;
            foreach (ModelMesh mesh in mBackground.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * Matrix.CreateTranslation(mModelPosition);
                    effect.View = game.mCamera.GetView(); // Matrix.CreateLookAt(Game.GetInstance().cameraPosition, Vector3.Zero, Vector3.Forward);
                    effect.Projection = game.mCamera.GetProjection();  //Matrix.CreatePerspectiveFieldOfView(Game.GetInstance().fieldOfView, Game.GetInstance().aspectRatio, 20000.0f, 120000.0f); //clip planes
                }
                game.graphics.GraphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
                game.graphics.GraphicsDevice.RenderState.DepthBufferEnable = true;
                //graphics.ApplyChanges();
                mesh.Draw();
            }*/

            mCloudMgr.Render();
        }
    }
}
