using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text;
using System.Xml;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using System.Xml;
using System.IO;

namespace DogFighter
{
    public class FontMgr
    {
        public struct Fading3D
        {
            public Vector3 position;
            public Vector3 color;
            public long time;
            public long curTime;
            public String str;
            public Font font;
        }

        public struct NotifyText
        {
            public Vector2 position;
            public Vector3 color;
            public long time;
            public long curTime;
            public String str;
            public Font font;
        }

        public ArrayList mFading3D = new ArrayList();
        public ArrayList mNotify = new ArrayList();


        public Font mFontSystem = new Font();
        public Font mFontMain = new Font();
        public Font mFontHeader = new Font();


        //
        // Text for overhead scores etc... fades out over time, centered
        //
        public void FormatFading3D(Font font, Vector3 position, Vector3 color, long time, String str, params object[] args)
        {
            Fading3D text = new Fading3D();
            text.position = position;
            text.color = color;
            text.time = time;
            text.curTime = time;
            text.str = String.Format(str, args);
            text.font = font;
            mFading3D.Add(text);
        }

        //
        // Notification messages
        //
        public void FormatNotify(Font font, Vector2 position, Vector3 color, long time, String str, params object[] args)
        {
            NotifyText text = new NotifyText();
            text.position = position;
            text.color = color;
            text.time = time;
            text.curTime = time;
            text.str = String.Format(str, args);
            text.font = font;
            mNotify.Add(text);
        }

        public void Clear()
        {
            mFading3D.Clear();
            mNotify.Clear();
        }

        public void Update()
        {
            //clear the fonts
            mFontSystem.Clear();
            mFontMain.Clear();
            mFontHeader.Clear();

            for (int i = 0; i < mFading3D.Count; ++i)
            {
                Fading3D text = (Fading3D)mFading3D[i];

                float alpha = (float)text.curTime / (float)text.time;
                Color color = new Color(new Vector4(text.color, alpha));

                Vector2 screenSpacePos = Game.GetInstance().GetCamera().GetScreenSpaceCoord(text.position);

                text.font.Format(screenSpacePos, color, text.str);

                text.curTime -= 10;

                if (text.curTime < 0)
                    mFading3D.RemoveAt(i);
                else
                    mFading3D[i] = text;
            }

            for (int i = 0; i < mNotify.Count; ++i)
            {
                NotifyText text = (NotifyText)mNotify[i];

                float alpha = (float)text.curTime / (float)text.time;
                Color color = new Color(new Vector4(text.color, alpha));

                text.font.Format(text.position, color, text.str);

                text.curTime -= 10;

                if (text.curTime < 0)
                    mNotify.RemoveAt(i);
                else
                    mNotify[i] = text;
            }
        }

        public void LoadGraphicsContent(bool loadAllContent)
        {
            if (loadAllContent)
            {
                mFontSystem.Load("Content\\Fonts\\Verdana_14");
                mFontMain.Load("Content\\Fonts\\digitalstrip");
                mFontHeader.Load("Content\\Fonts\\04b03b");
            }
        }

        public void Render()
        {
            mFontSystem.Render();
            mFontMain.Render();
            mFontHeader.Render();
        }

    }
}
