#include "EnumMetadata.h"
#include "ClassMetadata.h"
#include "MetadataMgr.h"
#include "File/Log.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

EnumMetadata::EnumMetadata(const char* name, EnumMetadataInfo* metadata) :
	mName(name),
	mMetadata(metadata)
{
	gMetadataMgr.RegisterParser(this);
}

bool EnumMetadata::Read(const ClassMetadataInfo& info, void* data, const string& value, MetadataScript* metaScript) const
{
	// this should handle |'s
	int i = 0;
	while (mMetadata[i].name != 0)
	{
		if (strcmpi(value.c_str(), mMetadata[i].name) == 0)
		{
			*(int*)(((char*)data) + info.offset) = mMetadata[i].value;
			return true;
		}
		++i;
	}

	*(int*)(((char*)data) + info.offset) = -1;
	Log::Error("[EnumMetadata::Read] Unknown enum '%s'", value.c_str());
	return false;
}

bool EnumMetadata::ReadInstance(const ClassMetadataInfo& info, void* data, void* newInstance, MetadataScript* metaScript) const
{
	return false;
}

bool EnumMetadata::Clone(const ClassMetadataInfo& info, void* from, void* to) const
{
	*(int*)(((char*)to) + info.offset) = *(int*)(((char*)from) + info.offset);
	return true;
}
