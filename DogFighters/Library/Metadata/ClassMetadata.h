#ifndef _METADATA_H_
#define _METADATA_H_

#include <string>

using namespace std;

class MetaScript;

//
// Creates an object when this function is called
//
typedef void* (*FnCreate)();

template <class T>
void* Creator()
{
	return new T();
}

struct ClassMetadataInfo
{
	const char*			type;		// eg. int, float
	const char* 		name;
	long int			offset;
	const char*			custom;		// custom string
};

class ClassMetadata
{
public:

	ClassMetadata(const char* name, ClassMetadataInfo* metadata, FnCreate creator, const char* extends = 0);

	const ClassMetadataInfo*	GetClassMetadataInfo(const char* name) const;

	const char*			mName;
	const char*			mExtends;
	ClassMetadataInfo*	mMetadata;
	FnCreate			mCreator;
};

#endif



