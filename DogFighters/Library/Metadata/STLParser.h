#ifndef _STLPARSER_H_
#define _STLPARSER_H_

#include "MetadataParser.h"
#include "MetadataScript.h"

#include "Memory/NoMemory.h"
#include <string>
#include "Memory/Memory.h"

using namespace std;

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

//
// Common parses for STL containers
//

template <class T>
class ListParser : public MetadataParser
{
public:

	ListParser(const char* name) : MetadataParser(), mName(string("list<") + name + string(">"))
	{
	}

	virtual bool HandlesType(const char* name) const
	{
		if (strcmpi(name, mName.c_str()) == 0)
			return true;

		return false;
	}

	virtual bool Write(const ClassMetadataInfo& info, void* data, string& out) const
	{
		return false;
	}

	virtual bool ReadInstance(const ClassMetadataInfo& info, void* data, void* newInstance, MetadataScript* metaScript) const
	{
		list<T>* stlList = static_cast<list<T>* >(data); //(void*)((char*)data + info.offset));
		stlList->push_back((T)newInstance);
		return true;
	}

	virtual bool Read(const ClassMetadataInfo& info, void* data, const string& value, MetadataScript* metaScript) const
	{
		// eww! cast to char to change the offset, cast back to void* to stop errors
		// about casting to a template
		list<T>* stlList = static_cast<list<T>* >((void*)((char*)data + info.offset));
		return true;
	}

	virtual bool Clone(const ClassMetadataInfo& info, void* from, void* to) const { return false; }

protected:

	string	mName;
};


template <class T>
class VectorParser : public MetadataParser
{
public:

	VectorParser(const char* name) : MetadataParser(), mName(string("vector<") + name + string(">"))
	{
	}

	virtual bool HandlesType(const char* name) const
	{
		if (strcmpi(name, mName.c_str()) == 0)
			return true;

		return false;
	}

	virtual bool Write(const ClassMetadataInfo& info, void* data, string& out) const
	{
		return false;
	}

	virtual bool ReadInstance(const ClassMetadataInfo& info, void* data, void* newInstance, MetadataScript* metaScript) const
	{
		vector<T>* stl = static_cast<vector<T>* >(data); //(void*)((char*)data + info.offset));
		stl->push_back((T)newInstance);
		return true;
	}

	virtual bool Read(const ClassMetadataInfo& info, void* data, const string& value, MetadataScript* metaScript) const
	{
		// eww! cast to char to change the offset, cast back to void* to stop errors
		// about casting to a template
		vector<T>* stlList = static_cast<vector<T>* >((void*)((char*)data + info.offset));
		return true;
	}

	virtual bool Clone(const ClassMetadataInfo& info, void* from, void* to) const { return false; }

protected:

	string	mName;
};

class StringParser : public MetadataParser
{
public:

	virtual bool HandlesType(const char* name) const;
	virtual bool Write(const ClassMetadataInfo& info, void* data, string& out) const;
	virtual bool Read(const ClassMetadataInfo& info, void* data, const string& value, MetadataScript* metaScript) const;
	virtual bool ReadInstance(const ClassMetadataInfo& info, void* data, void* newInstance, MetadataScript* metaScript) const { return true; }

	virtual bool Clone(const ClassMetadataInfo& info, void* from, void* to) const;
};


#endif
