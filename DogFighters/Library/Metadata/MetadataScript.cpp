#include "MetadataScript.h"
#include <iostream>
#include "File/Log.h"

using namespace std;

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

bool MetadataScript::Open(const char* ph, list<void*>* entityList)
{
	Super::Open(ph);
	if (!Valid())
		return false;

	if (!entityList)
		return true;

	// we only create instances of those classes that are not inherited from
	map<string, const ScriptClass*>		extendedClass;
	list<ScriptClass>					instanceClass;	// classes we need to instance


	// found out which classes not to create instances of
	ScriptFile::ClassIterator classIt;
/*
	for (classIt = mScript.ClassBegin(); classIt != mScript.ClassEnd(); ++classIt)
	{
		GetNonInstanceClasses(*classIt, extendedClass);
		/*
		const string& name = classIt->GetExtendsName();
		const ScriptClass* scriptClass = mScript.GetScriptClass(name);

		if (scriptClass)
			extendedClass[name] = scriptClass;* /
	}
*/
	// create instances of the classes
	for (classIt = ClassBegin(); classIt != ClassEnd(); ++classIt)
	{
		// if its not one of the classes we are not allowed to create instances of
		if (extendedClass.find(classIt->GetName()) == extendedClass.end() && !HasProperty("abstract", *classIt))
		{
			const ClassMetadata* info = 0;
			entityList->push_back(CreateInstance(&(*classIt), &info));
		}
	}

	return true;
}

void MetadataScript::GetNonInstanceClasses(const ScriptClass& scriptClass, map<string, const ScriptClass*>& extendedClass)
{
	const string& name = scriptClass.GetExtendsName();
	const ScriptClass* extendScriptClass = GetScriptClass(name);

	if (extendScriptClass)
		extendedClass[name] = extendScriptClass;
/*

	ScriptFile::ClassIterator classIt;
	for (classIt = mScript.ClassBegin(); classIt != mScript.ClassEnd(); ++classIt)
	{
		const string& name = classIt->GetExtendsName();
		const ScriptClass* scriptClass = mScript.GetScriptClass(name);

		if (scriptClass)
			extendedClass[name] = scriptClass;
	}
*/
	// rinse and repeat on children
	ScriptFile::ClassIterator classIt;
	for (classIt = scriptClass.ClassBegin(); classIt != scriptClass.ClassEnd(); ++classIt)
		GetNonInstanceClasses(*classIt, extendedClass);
}

void* MetadataScript::CreateInstance(const ScriptClass* scriptClass, const ClassMetadata** info)
{
	const ScriptClass* parent = GetScriptClass(scriptClass->GetExtendsName());
	*info = gMetadataMgr.GetClassMetadata(scriptClass->GetName().c_str());
	void* instance = gMetadataMgr.Create(scriptClass->GetName().c_str());
	if (!instance)
	{
		if (!parent)
		{
			*info = gMetadataMgr.GetClassMetadata(scriptClass->GetExtendsName().c_str());
			instance = gMetadataMgr.Create(scriptClass->GetExtendsName().c_str());
		}
		else
		{
			instance = CreateInstance(parent, info);
		}
	}

	if (!instance)
	{
		Log::Error("[MetadataScript::CreateInstance] Failed to create an instance of: %s\n", scriptClass->GetName().length() <= 0 ? scriptClass->GetExtendsName().c_str() : scriptClass->GetName().c_str());
		return 0;
	}
	/*
	void* instance = 0;
	const ScriptClass* parent = mScript.GetScriptClass(scriptClass.GetExtendsName());

	if (parent)
	{
		instance = CreateInstance(*parent, info);
	}
	else
	{
		// if we have no parent we will get null back,
		// so then check the name
		instance = gMetadataMgr.Create(scriptClass.GetExtendsName().c_str());
		if (!instance)
		{
			instance = gMetadataMgr.Create(scriptClass.GetName().c_str());
			*info = gMetadataMgr.GetClassMetadata(scriptClass.GetName().c_str());
		}
		else
		{
			*info = gMetadataMgr.GetClassMetadata(scriptClass.GetExtendsName().c_str());
		}
	}

	if (!instance || !info || !*info)
	{
		Log::Print("Failed to create an instance of class: %s", scriptClass.GetName());
		return 0;
	}
*/
	
	PopulateInstance(scriptClass, instance, *info);
	return instance;
}

bool MetadataScript::PopulateInstance(const ScriptClass* scriptClass, void* instance, const ClassMetadata* info)
{
	if (!scriptClass)
		return true;

	// iterate through all variables of this class
	ScriptClass::VariableIterator varIt;
	for (varIt = scriptClass->VariableBegin(); varIt != scriptClass->VariableEnd(); ++varIt)
	{
		// get variable info
		const ClassMetadataInfo* variableInfo = info->GetClassMetadataInfo(varIt->GetName().c_str());
		if (!variableInfo)
		{
			Log::Print("Couldn't find variable relating to '%s' for class [%s : %s]\n", varIt->GetName().c_str(), scriptClass->GetName().c_str(), scriptClass->GetExtendsName().c_str());
			continue;
		}

		// search for a parser for this variable
		bool isPointer = false;
		const ClassMetadata* metaInfo = gMetadataMgr.GetClassMetadata(variableInfo->type, &isPointer);
		const MetadataParser* metaParser = gMetadataMgr.GetMetadataParser(variableInfo->type);
		if (!metaParser && !metaInfo)
		{
			Log::Print("Failed to find parser '%s' for class [%s : %s] of type '%s'\n", variableInfo->name, scriptClass->GetName().c_str(), scriptClass->GetExtendsName().c_str(), variableInfo->type);
			continue;
		}

		// iterate through all values
		const ScriptVariable& variable = *varIt;
		if (!isPointer && variable.ValueSize() > 1)
		{
			Log::Print("Metadata wants to load an array of data, while only one can be loaded: variable %s, class %s\n", variableInfo->name, scriptClass->GetName().c_str());
			continue;
		}

		// iterate through all values assigned to this variable
		const ScriptClass* valueClass = 0;
		ScriptVariable::ValueConstIterator valueIt;
		for (valueIt = variable.ValueConstBegin(); valueIt != variable.ValueConstEnd(); ++valueIt)
		{
			switch (valueIt->mType)
			{
			case ScriptVariable::VVT_String:
				metaParser->Read(*variableInfo, instance, valueIt->mString, this); // this should take an array?
				break;

			case ScriptVariable::VVT_Instance:
				valueClass = &valueIt->mScriptClass;
				break;

			case ScriptVariable::VVT_Pointer:
				valueClass = valueIt->mScriptClassPtr;
				break;
			}

			if (valueClass)
			{
				if (isPointer)
				{
					bool isNextPointer = false;
					const ClassMetadata* metaInfo = 0;// = gMetadataMgr.GetClassMetadata(valueClass->GetName().c_str(), &isNextPointer);

					/*
					if (!metaInfo)
						metaInfo = gMetadataMgr.GetClassMetadata(valueClass->GetExtendsName().c_str(), &isNextPointer);

					if (!metaInfo)
					{
						Log::Print("No metadata for: variable %s, class %s\n", variableInfo->name, scriptClass->GetName().c_str());
						continue;
					}*/

					
			//if (strcmpi(variableInfo->GetName().c_str(), "mywnd") == 0)
			//			int ntihng = 0;

					void* nextInstance = CreateInstance(valueClass, &metaInfo);
					if (metaParser)
					{
						metaParser->ReadInstance(*variableInfo, (char*)instance + variableInfo->offset, nextInstance, this);
					}
					else
					{
						// THIS IS 64BIT SAFE
						int** ptr = (int**)((char*)instance + variableInfo->offset);
						*ptr = (int*)nextInstance;
					}
				}
				else
				{
					PopulateInstance(valueClass, (char*)instance + variableInfo->offset, metaInfo);
				}
			}
		}
	}

	return true;
}


bool MetadataScript::HasProperty(const string& property, const ScriptClass& sClass)
{
	ScriptClass::PropertyIterator propIt;
	for (propIt = sClass.PropertyBegin(); propIt != sClass.PropertyEnd(); ++propIt)
	{
		if (strcmpi(propIt->c_str(), "abstract") == 0)
		{
			return true;
		}
	}

	return false;
}
