#include "STLParser.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

bool StringParser::HandlesType(const char* name) const
{
	if (strcmpi(name,"string") == 0)
		return true;

	return false;
}

bool StringParser::Write(const ClassMetadataInfo& info, void* data, string& out) const
{
	return false;
}

bool StringParser::Read(const ClassMetadataInfo& info, void* data, const string& value, MetadataScript* metaScript) const
{
	string* str = static_cast<string*>((void*)((char*)data + info.offset));
	*str = value;
	return true;
}

bool StringParser::Clone(const ClassMetadataInfo& info, void* from, void* to) const
{
	string* strTo = static_cast<string*>((void*)((char*)to + info.offset));
	string* strFrom = static_cast<string*>((void*)((char*)from + info.offset));
	*strTo = *strFrom;
	return true;
}
