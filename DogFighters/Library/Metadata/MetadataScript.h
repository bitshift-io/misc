#ifndef _METASCRIPT_H_
#define _METASCRIPT_H_

#include <list>
#include <string>
#include "File/ScriptFile.h"
#include "ClassMetadata.h"
#include "MetadataMgr.h"
#include "MetadataParser.h"
#include <map>

using namespace std;

//
// might be useful one day:
// c++ typeid
// http://publib.boulder.ibm.com/infocenter/iadthelp/v6r0/index.jsp?topic=/com.ibm.etools.iseries.pgmgd.doc/cpprog612.htm
//

class MetadataScript : public ScriptFile
{
public:

	typedef ScriptFile Super;

	MetadataScript() : ScriptFile()							{}
	MetadataScript(const string& file) : ScriptFile(file)	{}

	bool				Open(const char* ph, list<void*>* entityList);

	void*				CreateInstance(const ScriptClass* scriptClass, const ClassMetadata** info);
	bool				PopulateInstance(const ScriptClass* scriptClass, void* instance, const ClassMetadata* info);

protected:

	bool				HasProperty(const string& property, const ScriptClass& sClass);
	void				GetNonInstanceClasses(const ScriptClass& scriptClass, map<string, const ScriptClass*>& extendedClass);
};

#endif
