#ifndef _MACROS_H_
#define _MACROS_H_

#define USE_BASIC_PARSER	\
	BasicTypeParser basicParser;


#define USES_METADATA												\
	virtual ClassMetadata& GetClassMetadata() { return mMetaInfo; }	\
	static ClassMetadataInfo mMetadata[];							\
	static ClassMetadata mMetaInfo;


#define CLASS_VARIABLE(c, t, n)					\
	#t, #n, (long int)&c##temp.n - (long int)&c##temp, 0,


#define CLASS_METADATA_BEGIN(c)				\
											\
	c c##temp;								\
	ClassMetadataInfo c::mMetadata[] =		\
	{


// NOTE: we could supply another parameter at the end
// which gets parsed to the MetaInfo
// which is a notification class
// this can be used so we dont have to have metadata specific info
// in our classes, but we can have another static callback class that knows how to
// deal with the game specific stuff :)
#define CLASS_EXTENDS_METADATA_END(c, e)		\
		0, 0, 0, 0								\
	};											\
	ClassMetadata c::mMetaInfo(#c, c::mMetadata, Creator<c>, #e);

#define CLASS_METADATA_END(c)					\
		0, 0, 0, 0								\
	};											\
	ClassMetadata c::mMetaInfo(#c, c::mMetadata, Creator<c>, 0);



#define ENUM_METADATA_BEGIN(e)					\
	EnumMetadataInfo e##enummetadatainfo[] =	\
	{											\

#define ENUM_METADATA_END(e)					\
		0, 0, 0, 0								\
	};											\
	EnumMetadata e##enummetadata(#e, e##enummetadatainfo);

#define ENUM_VALUE(v)					\
	#v, (int)v,

#endif


