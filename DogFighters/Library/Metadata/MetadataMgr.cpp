#include "MetadataMgr.h"
#include "MetadataParser.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

void MetadataMgr::RegisterParser(const MetadataParser* parser)
{
	mParser.push_back(parser);
}

void MetadataMgr::Register(const ClassMetadata* metaInfo)
{
	mClassData.push_back(metaInfo);
}

void* MetadataMgr::Create(const char* name)
{
	const ClassMetadata* info = GetClassMetadata(name);
	if (!info)
		return 0;

	return info->mCreator();
}

void* MetadataMgr::Clone(void* mdClass, const char* name)
{
	const ClassMetadata* info = GetClassMetadata(name);
	if (!info)
		return 0;	

	// create a new instance
	void* clone = Create(name);

	// copy parameters over
	const ClassMetadata* curInfo = info;
	while (curInfo)
	{
		int i = 0;
		while (curInfo->mMetadata[i].type != 0)
		{
			bool isPointer = false;
			ClassMetadataInfo* variableInfo = &curInfo->mMetadata[i];
			const ClassMetadata* metaInfo = GetClassMetadata(curInfo->mMetadata[i].type, &isPointer);
			const MetadataParser* parser = GetMetadataParser(curInfo->mMetadata[i].type);

			if (parser)
			{
				parser->Clone(curInfo->mMetadata[i], mdClass, clone);
			}
			else
			{
				if (isPointer)
				{
					int** clonePtr = (int**)((char*)mdClass + variableInfo->offset);
					void* nextInstance = Clone(*clonePtr, metaInfo->mName);

					// THIS IS 64BIT SAFE
					int** ptr = (int**)((char*)clone + variableInfo->offset);
					*ptr = (int*)nextInstance;
				}
				else
				{
					// in place memory clone
					int nothing = 0;
					//Log::Error("not yet implemented");
				}
			}

			++i;
		}

		if (curInfo->mExtends)
			curInfo = GetClassMetadata(curInfo->mExtends);
		else
			curInfo = 0;
	} 

	return clone;
}

const ClassMetadata* MetadataMgr::GetClassMetadata(const char* name, bool* isPointer)
{
	// determine if we are a pointer, if so, strip the *
	if (isPointer)
		*isPointer = false;

	string rawName = name;
	int lastCharIdx = rawName.length() - 1;

	// 2 short!
	if (lastCharIdx < 2)
		return 0;

	if (rawName[lastCharIdx] == '*' || rawName[lastCharIdx - 1] == '*')
	{
		if (isPointer)
			*isPointer = true;

		rawName[lastCharIdx] = '\0';
	}

	list<const ClassMetadata*>::iterator it;
	for (it = mClassData.begin(); it != mClassData.end(); ++it)
	{
		if (strcmpi(rawName.c_str(), (*it)->mName) == 0)
			return (*it);
	}

	return 0;
}

const MetadataParser* MetadataMgr::GetMetadataParser(const char* type)
{
	list<const MetadataParser*>::iterator it;
	for (it = mParser.begin(); it != mParser.end(); ++it)
	{
		if ((*it)->HandlesType(type))
			return (*it);
	}

	return 0;
}
