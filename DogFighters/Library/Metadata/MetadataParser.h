#ifndef _METAPARSER_H_
#define _METAPARSER_H_

#include "ClassMetadata.h"
#include <string>
#include <vector>

using namespace std;

class MetadataScript;

class MetadataParser
{
public:

	MetadataParser();
	virtual ~MetadataParser() { }

	// eg: int, float - MetadataTypeInfo::Type, this allows handling of multiple type :)
	virtual bool HandlesType(const char* name) const  = 0;

	// does what it needs too here...
	// the MetaScript will generate the myVar = 
	// and out will be appended on the end so we get:
	// myVar = {out}
	virtual bool Write(const ClassMetadataInfo& info, void* data, string& out) const = 0;	
	virtual bool Read(const ClassMetadataInfo& info, void* data, const string& value, MetadataScript* metaScript) const = 0;
	virtual bool ReadInstance(const ClassMetadataInfo& info, void* data, void* newInstance, MetadataScript* metaScript) const = 0;

	virtual bool Clone(const ClassMetadataInfo& info, void* from, void* to) const = 0;
};

class BasicTypeParser : public MetadataParser
{
public:

	BasicTypeParser() : MetadataParser() { }

	virtual bool HandlesType(const char* name) const;
	virtual bool Write(const ClassMetadataInfo& info, void* data, string& out) const;	
	virtual bool Read(const ClassMetadataInfo& info, void* data, const string& value, MetadataScript* metaScript) const;
	virtual bool ReadInstance(const ClassMetadataInfo& info, void* data, void* newInstance, MetadataScript* metaScript) const { return true; }

	virtual bool Clone(const ClassMetadataInfo& info, void* from, void* to) const;

protected:

	bool		IsPointer(const string& typeName);
};

#endif
