#ifndef _ENUMMETADATA_H_
#define _ENUMMETADATA_H_

#include "MetadataParser.h"
#include <string>

using namespace std;

struct ClassMetadataInfo;
class MetadataScript;

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

struct EnumMetadataInfo
{
	const char* 		name;
	int					value;
};

class EnumMetadata : public MetadataParser
{
public:

	EnumMetadata(const char* name, EnumMetadataInfo* metadata);

	virtual bool HandlesType(const char* name) const			{ return strcmpi(name, mName) == 0; }

	virtual bool Write(const ClassMetadataInfo& info, void* data, string& out) const { return false; }	
	virtual bool Read(const ClassMetadataInfo& info, void* data, const string& value, MetadataScript* metaScript) const;
	virtual bool ReadInstance(const ClassMetadataInfo& info, void* data, void* newInstance, MetadataScript* metaScript) const;

	virtual bool Clone(const ClassMetadataInfo& info, void* from, void* to) const;

	const char*			mName;
	EnumMetadataInfo*	mMetadata;
};

#endif
