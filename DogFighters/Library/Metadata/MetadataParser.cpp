#include "MetadataParser.h"
#include "MetadataMgr.h"
#include "Memory/Memory.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

MetadataParser::MetadataParser()
{
	gMetadataMgr.RegisterParser(this);
}




bool BasicTypeParser::HandlesType(const char* name) const
{
	// may need to use regex here for unsigned stuffs
	if (strcmpi(name, "const char*") == 0)
		return true;

	if (strcmpi(name, "unsigned char") == 0)
		return true;
		
	if (strcmpi(name, "char") == 0)
		return true;

	if (strcmpi(name, "int") == 0)
		return true;

	if (strcmpi(name, "float") == 0)
		return true;

	if (strcmpi(name, "bool") == 0)
		return true;

	return false;
}

bool BasicTypeParser::Write(const ClassMetadataInfo& info, void* data, string& out) const
{
	char buffer[256];
	const char* format = 0;

	//if (strcmpi(info.name, "const char*") == 0)
	//	format = "%s";

	if (strcmpi(info.name, "unsigned char") == 0)
		format = "%c";

	if (strcmpi(info.name, "char") == 0)
		format = "%c";

	if (strcmpi(info.name, "int") == 0)
		format = "%i";

	if (strcmpi(info.name, "float") == 0)
		format = "%f";

	if (strcmpi(info.name, "bool") == 0)
		format = "%b";

	sprintf(buffer, format, (char*)data + info.offset);
	out = buffer;
	return true;
}

bool BasicTypeParser::Read(const ClassMetadataInfo& info, void* data, const string& value, MetadataScript* metaScript) const
{
	const char* format = 0;

	//bool pointer = IsPointer(info.type);
/*
	vector<string>::const_iterator valueIt;
	for (valueIt = values.begin(); valueIt != values.end(); ++valueIt)
	{

	}*/

	if (strcmpi(info.type, "const char*") == 0)
	{
		// 'oly crap! - NEEDS TESTING
		char* pChar = ((char*)data) + info.offset;
		pChar = strdup(value.c_str());

		//*((int*)(((char*)data) + info.offset)) = (int)strdup(value.c_str());
		return true;
	}

	if (strcmpi(info.type, "unsigned char") == 0)
		format = "%u";

	if (strcmpi(info.type, "char") == 0)
		format = "%d";

	if (strcmpi(info.type, "int") == 0)
		format = "%d";

	if (strcmpi(info.type, "float") == 0)
		format = "%f";

	if (strcmpi(info.type, "bool") == 0)
	{
		bool bVal = false;
		if (strcmpi(value.c_str(), "true") == 0 || strcmpi(value.c_str(), "1") == 0)
			bVal = true;

		*(((char*)data) + info.offset) = bVal;
		return true;
	}

	sscanf(value.c_str(), format, ((char*)data) + info.offset);
	return true;
}

bool BasicTypeParser::IsPointer(const string& typeName)
{
	if (typeName[typeName.size() - 1] == '*')
		return true;

	return false;
}

bool BasicTypeParser::Clone(const ClassMetadataInfo& info, void* from, void* to) const
{
	int size = 0;

	if (strcmpi(info.type, "const char*") == 0)
	{
		// 'oly crap! - NEEDS TESTING
		char* pCharFrom = ((char*)from) + info.offset;
		char* pCharTo = ((char*)to) + info.offset;
		pCharTo = pCharFrom;
		//*((int*)(((char*)to) + info.offset)) = (int)strdup((((char*)from) + info.offset));
		return true;
	}

	if (strcmpi(info.type, "unsigned char") == 0)
		size = sizeof(unsigned char);

	if (strcmpi(info.type, "char") == 0)
		size = sizeof(char);

	if (strcmpi(info.type, "int") == 0)
		size = sizeof(int);

	if (strcmpi(info.type, "float") == 0)
		size = sizeof(float);

	if (strcmpi(info.type, "bool") == 0)
		size = sizeof(float);


	memcpy((char*)to + info.offset, (char*)from + info.offset, size);
}
