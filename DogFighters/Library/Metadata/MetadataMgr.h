#ifndef _METADATAMGR_H_
#define _METADATAMGR_H_

#include "Template/Singleton.h"
#include <list>

using namespace std;

class MetadataParser;
class ClassMetadata;

#define gMetadataMgr MetadataMgr::GetInstance()

struct MetadataMgr : public Singleton<MetadataMgr>
{
public:

	void RegisterParser(const MetadataParser* parser);
	void Register(const ClassMetadata* metaInfo);

	void*					Clone(void* mdClass, const char* name);
	void*					Create(const char* name);
	const ClassMetadata*	GetClassMetadata(const char* name, bool* isPointer = 0);
	const MetadataParser*	GetMetadataParser(const char* type);

protected:

	list<const ClassMetadata*>		mClassData;
	list<const MetadataParser*>		mParser;
	
};

#endif
