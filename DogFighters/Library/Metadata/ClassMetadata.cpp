#include "ClassMetadata.h"
#include "MetadataMgr.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

ClassMetadata::ClassMetadata(const char* name, ClassMetadataInfo* metadata, FnCreate creator, const char* extends) : 
	mName(name),
	mMetadata(metadata),
	mCreator(creator),
	mExtends(extends)
{
	gMetadataMgr.Register(this);
}

const ClassMetadataInfo* ClassMetadata::GetClassMetadataInfo(const char* name) const
{
	int idx = 0;
	while (mMetadata[idx].name != 0)
	{
		if (strcmpi(name, mMetadata[idx].name) == 0)
			return &mMetadata[idx];

		++idx;
	}

	// see if this is a property of our parent
	if (mExtends != 0)
	{
		bool isPointer = false;
		const ClassMetadata* parent = gMetadataMgr.GetClassMetadata(mExtends, &isPointer);
		return parent->GetClassMetadataInfo(name);
	}

	return 0;
}
