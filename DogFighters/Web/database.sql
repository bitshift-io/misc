CREATE TABLE `dogfighters_highscore` 
(
	`id` int(10) unsigned NOT NULL auto_increment,
	`userid` int(10) unsigned NOT NULL,
	`hits` int(10) unsigned NOT NULL default '0',
	`deaths` int(10) unsigned NOT NULL default '0',
	`shotsFired` int(10) unsigned NOT NULL default '0',
	`pickups` int(10) unsigned NOT NULL default '0',
	`waveCount` int(10) unsigned NOT NULL default '0',
	`time` int(10) unsigned NOT NULL default '0',
	`score` int(10) unsigned NOT NULL default '0',
	`submitTime` datetime NOT NULL default '0000-00-00 00:00:00',
	KEY `id` (`id`)
);