#!/usr/bin/env python3

# need the 'shebang' for linux

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

#imports
import os
import os.path
import glob
import fnmatch
import shutil
import subprocess
import datetime

__author__ = 'Bronson Mathews'
__date__ = '$11/08/2015 11:48:16 AM$'

# setup paths
mOS = os.name
if mOS == 'nt': # windows
    mRootWorkingDir='c://Sermons//'
    mFFMPEG='c://Apps//ffmpeg//ffmpeg.exe'
    mSOX='c://Apps//sox//sox.exe'
else: #linux
    mRootWorkingDir=os.path.expanduser('~') + '/Sermons/'
    mFFMPEG='ffmpeg'
    mSOX='sox'

# setup some variables
mVideoComplete = 'video_complete.mts'
mAudioWorking = ['audio_left.wav','audio_right.wav'] # left/right audio channel names
mAudioClean =  'audio_clean.wav'
mVideoClean = 'video_clean.mp4'
mSettings = 'settings.txt'
mScriptDir = os.path.normpath(os.getcwd()) # directory script executed from
mWorkingDir = mRootWorkingDir

# ultrafast superfast veryfast faster fast medium slow slower veryslow #reccomend slower
mX264Preset= 'slower'
mVideoRate = '1500k'
mVerbose = '-hide_banner -loglevel error -stats'

# clears the terminal
def cls():
    os.system('cls' if os.name=='nt' else 'clear')
    return

# normalise a file path (fix slashes)
def normalisePath(path):
    return os.path.normpath(path)

# function to create the settings file
def CreateSettings( path, filename=mSettings ):
    filename = os.path.join( path, mSettings )
    exists = os.path.exists( filename )

    if exists:
        print('Settings: {} exists'.format(filename))
    else:
        # get the directory name
        pathList = path.split(os.sep)
        pathLength = len(pathList) - 1
        pathDir = pathList[pathLength]

        # write to tthe text file
        f = open( filename, 'w' )
        f.write( '{}\n'.format(pathDir) )
        f.write( '00:00:00.000\n' )
        f.write( '00:35:00.000\n' )
        f.close()
        print('Settings: {} created'.format( filename ))

    return


# function to read the settings to list
# returns list of settings
def ReadSettings(path, filename=mSettings):

    filename = os.path.join( path, filename )
    exists = os.path.exists(filename)
    settings = []
    if exists:
        f = open(filename, 'r')

        for line in f:
            settings.append(line.replace('\n', ''))

        f.close()
    else:
        print('ReadSettings: {} missing, aborting!'.format( filename ))
        return False

    return settings


# function searches current dir for files of this extension
# returns list of files according to the pattern
def SearchDir(path, pattern='*.MTS',matchCase=True):

    fileList = []
    search = path + '/**/' + pattern #os.path.join(path, '/**/', pattern) doesnt work with *

    # recursive search for files! woo!
    for filename in glob.iglob(search, recursive=True):
        if matchCase:
            if fnmatch.fnmatchcase(filename, pattern):
                fileList.append(filename)
        else:
            if fnmatch.fnmatch(filename, pattern):
                fileList.append(filename)

    # sort the list
    fileList.sort()
    #print(fileList)
    return fileList


# does a binary copy/Concatenate of mts files
# takes new file name, and a list of files to combine
# returns the file
def Concatenate(path, filename, fileList):
    filename = os.path.join(path, filename)
    exists = os.path.exists(filename)
    if exists:
        print('Concatenate: {} exists, skipping...'.format(filename))
    else:
        print('Concatenate: combining...')
        #create file
        destination = open(filename,'wb')

        #concatenate
        length = len(fileList)
        i = 1
        for file in fileList:
            print('Concatenate: {} of {} - {}'.format(i, length, file))
            i+=1
            shutil.copyfileobj(open(file,'rb'), destination)

        destination.close()
        print('Concatenate: {} created successfully'.format(filename))

    return filename


# Extract WAV file
# extract from source to fileList
def ExtractWAV(path, fileList, sourceFilename):

    newList = []
    for i in fileList:
        newList.append( os.path.join( path, i ) )

    exists = os.path.exists( newList[0] )
    if exists:
        print('Extract WAV: {} & {} exists, skipping...'.format(newList[0], newList[1]))
    else:
        args = '{} -i {} -map_channel 0.1.0 {} -map_channel 0.1.1 {} {}'.format(mFFMPEG, sourceFilename, newList[0], newList[1], mVerbose)
        print('Extract WAV: Processing...')
        sp = subprocess.call(args.split(' '))
        print('Extract WAV: {} & {} created successfully'.format(newList[0], newList[1]))

    return


# render mp3 complete
def RenderAudio(path, filename, settings):

    filename = os.path.join( path, filename )
    exists = os.path.exists(filename)

    if exists:
        # check if the target file exists
        outputAudioPath = os.path.join( path, settings[0] + '.mp3' )
        exists = os.path.exists(outputAudioPath)
        if exists:
            print('Render Audio: {} exists, skipping...'.format(outputAudioPath))
        else:
            # -qscale:a 8 - https://trac.ffmpeg.org/wiki/Encode/MP3 - 70-105kb/s
            # 44100, 22050
            args = '{} -i {} -ss {} -to {} -codec:a libmp3lame -qscale:a 8 -ar 22050 -ac 1 {} {}'.format(mFFMPEG, filename, settings[1], settings[2], outputAudioPath, mVerbose)
            print('Render Audio: Processing...')
            sp = subprocess.call(args.split(' '))
            print('Render Audio: {} created successfully'.format(outputAudioPath))

        # output wav info
        print('\nRender Audio: Audio Information:')
        args = '{} -i {} -hide_banner'.format(mFFMPEG, outputAudioPath)
        sp = subprocess.call(args.split(' '))
        print('\n')
    else:
        print('Render Audio: {} missing, aborting!'.format( filename ))

    return


# renders compressed version for youtube as mp4
def RenderVideo(path, filenameVideo=mVideoComplete, filenameAudio=mAudioClean, settings=mSettings):
    filenameVideo = os.path.join( path, filenameVideo )
    filenameAudio = os.path.join( path, filenameAudio )
    existsVideo = os.path.exists(filenameVideo)
    existsAudio = os.path.exists(filenameAudio)

    if filenameVideo and existsAudio:
        # check if the target file exists
        outputVideoPath = os.path.join( path, settings[0] + '.mp4' )
        exists = os.path.exists(outputVideoPath)
        if exists:
            print('Render Video: {} exists, skipping...'.format(outputVideoPath))
        else:
            # split up args to make easy to understand
            audioArgs = '-codec:a libmp3lame -q:a 4 -ar 22050 -ac 1 -af pan=1c:c0=c0 -map 1:a'
            videoArgs = '-codec:v libx264 -preset {} -b:v {} -minrate {} -maxrate {} -bufsize 2048k -vf hqdn3d=1.5:1.5:6:6,scale=1280:720 -map 0:v'.format(mX264Preset, mVideoRate, mVideoRate, mVideoRate)
            args = '{} -i {} -i {} -ss {} -to {} {} {} -shortest {} {}'.format(mFFMPEG, filenameVideo, filenameAudio, settings[1], settings[2], videoArgs, audioArgs, outputVideoPath, mVerbose)
            print('Render Video: Processing...')
            sp = subprocess.call(args.split(' '))

            print('Render Video: {} created'.format( outputVideoPath ))

        # output video info
        print('\nRender Video: Video Information:')
        args = '{} -i {} -hide_banner'.format(mFFMPEG, outputVideoPath)
        sp = subprocess.call(args.split(' '))
        print('\n')
    else:
        print('Render Video: {} or {} missing, aborting!'.format( filenameVideo, filenameAudio ))

    return


# this outputs the working files
def OutputWorking(path):
    # get a list of the files to combine
    fileList = SearchDir(path,'*.MTS',True)

    if len( fileList ) != 0:
        # combine them to a new file
        concat = Concatenate(path, mVideoComplete, fileList)
        # Extract wav from the joined video
        ExtractWAV(path, mAudioWorking, concat)
    else:
        print( 'OutputWorking: {} no files found'.format( path ) )

    return


# outputs a clean uncompressed video for use in video editing
def OutputVideoClean(path):
    videoCompletePath = os.path.join( path, mVideoComplete )
    audioCleanPath = os.path.join( path, mAudioClean )
    videoExists = os.path.exists(videoCompletePath)
    audioExists = os.path.exists(audioCleanPath)

    if videoExists and audioExists:
        # combine video complete and audio clean to make video clean!
        # we need to trim times
        settings = ReadSettings( path )
        # split up args to make easy to understand
        videoCleanPath = os.path.join( path, mVideoClean )
        audioArgs = '-codec:a aac -b:a 240k -af pan=1c:c0=c0 -strict experimental' #libmp3lame -q:a 1
        videoArgs = '-codec:v libx264 -preset fast -b:v 20000k -minrate 10k -maxrate 20000k -bufsize 2048k -vf hqdn3d=1.5:1.5:6:6'
        args = '{} -i {} -i {} -ss {} -to {} {} {} -map 0:v:0 -map 1:a:0 {} {}'.format(mFFMPEG, videoCompletePath, audioCleanPath, settings[1], settings[2], videoArgs, audioArgs, videoCleanPath, mVerbose)
        print('Output Video Clean: Processing...')
        sp = subprocess.call(args.split(' '))

        print('Output Video Clean: {} created'.format(mVideoClean))
    else:
        print('Output Video Clean: {} or {} missing, aborting!'.format( videoCompletePath, audioCleanPath ) )

    return


# this renders the complete files
def RenderFinal(path):
    # read the settings file
    settings = ReadSettings( path )

    # if there are no settings, abort!
    if settings == False:
        print('RenderFinal: No settings, aborting!')
        return

    # output mp3
    RenderAudio(path, mAudioClean, settings)

    # encode video
    RenderVideo(path, mVideoComplete, mAudioClean, settings)

    return


# check if binary exists
def BinaryCheck(path):
    # this works on linux, test windows - which
    from shutil import which
    return which(path) is not None

    if not os.path.exists(path):
        print('Binary Check: ERROR! Cannot find binary {}'.format(path))
    else:
        print('Binary Check: Found {}'.format(path))
    return

# will get a list of directories sorted by date
def GetDirsByDate(path):
    exists = os.path.exists(path)

    if exists:
        newList = []
        listDir = sorted(os.listdir(path), key=lambda filename: os.path.getmtime(os.path.join(path, filename)), reverse=True)
        for file in listDir:
            filename = os.path.join(path,file)
            if os.path.isdir(filename):
                newList.append(filename)

        return newList

    print( 'GetDirsByDate: {} does not exist, please create it'.format( path ) )
    return

# this function will search and list the last created directory
def FindWorkingDir(path=mRootWorkingDir):
    baseDir = GetDirsByDate(path)
    if len( baseDir ) != 0 :
        workingDir = GetDirsByDate(baseDir[0])

        if len ( workingDir ) != 0:
            workingDir = normalisePath(workingDir[0])
            print('Working Directory: {}'.format(workingDir))
            return workingDir

    print( 'FindWorkingDir: {} No sub folders found, please create series/date'.format( mRootWorkingDir ) )
    return

# this function uses sox to clean the audio
def CleanAudio(path=mWorkingDir):
    # check if audio clean exists
    cleanAudioPath = os.path.join ( path, mAudioClean )
    workingAudioPath = os.path.join ( path, mAudioWorking[0] )
    existsClean = os.path.exists( cleanAudioPath )
    existsWorking = os.path.exists( workingAudioPath )

    if existsClean:
        print('Clean Audio: {} exists, skipping...'.format( cleanAudioPath ) )
    else:
        # SOX - http://sox.sourceforge.net/sox.html
        # http://www.zoharbabin.com/how-to-do-noise-reduction-using-ffmpeg-and-sox/

        # need a cache/temp file
        tempAudioPath = os.path.join ( path, 'temp.wav' )

        # duplicate the left channel
        if existsWorking is not True:
            print('Clean Audio: {} missing, aborting!'.format( workingAudioPath ) )
            return

        shutil.copyfile( workingAudioPath, cleanAudioPath )

        # TODO: make noise profile wav file and test it

        # sox noise.wav -n noiseprof | play sound.wav noisered

        # Create background noise profile from mp3
        #/usr/bin/sox noise.mp3 -n noiseprof noise.prof
        # Remove noise from mp3 using profile
        #/usr/bin/sox input.mp3 output.mp3 noisered noise.prof 0.21


        # normalise
        args = '{} --show-progress --norm=-0.5 {} {}'.format(mSOX, cleanAudioPath, tempAudioPath) # default in audacity = -1db
        sp = subprocess.call(args.split(' '))
        shutil.move( tempAudioPath, cleanAudioPath )

        # this does the same as the noise gate (cut low volumes), limiter (smooth high volumes) and compressor all in 1 nifty graph and pass!
        # whatever this means, if you can sus it your a genious!
        # http://forum.doom9.org/showthread.php?t=165807
        # batch script to output plot files
        # sox.exe -n -n --plot=gnuplot compand 0,0 9:-15,0,-9 > try.plt
        # pause
        #
        # compressor attack1,decay1{,attack2,decay2} [soft-knee-dB:]in-dB1[,out-dB1]{,in-dB2,out-dB2} [gain [initial-volume-dB [delay]]]
        # soft knee is like a limiter 20: will soften/smoothen the graph, without this its linear
        # ploted points on graph x =input vol , y = outputvolume, -2db drop on the whole graph(normalise after to fix)
        args = '{} --show-progress {} {} compand 0.3,1 50:-60,-150,-50,-90,-40,-50,-30,-30,-20,-15,0,0 -0.2 0 0.2'.format(mSOX, cleanAudioPath, tempAudioPath)
        # compand .3,.8 -24,-12 0 0 .1
        sp = subprocess.call(args.split(' '))
        shutil.move( tempAudioPath, cleanAudioPath )

        print('Clean Audio: {} created successfully'.format( cleanAudioPath ) )
    return


# display a user menu + main function
def DisplayMenu():

    # initial checks and directories stuffs
    print('\n_______________________________________________________________')
    BinaryCheck(mSOX)
    BinaryCheck(mFFMPEG)
    print('\n')
    global mWorkingDir
    mWorkingDir = FindWorkingDir()
    CreateSettings( mWorkingDir )

    print ('\nAudio Video Edit:')
    print(' 1) Output Working Audio & Video'.format(mVideoComplete,mAudioWorking[0], mAudioWorking[1], mAudioClean))
    print(' 2) Render Final (MP3 Audio, MP4 Video)')
    print('\n')
    print(' h) Generate High Quality - {}'.format(mVideoClean))
    print(' r) Refresh (Menu + Settings)')
    print(' q) Quit (any key)')
    x = input(' -> ')
    print('\n')

    # clear screen
    cls()

    # no switch in python, this looks similar :)
    if x == 'r':
        DisplayMenu()
    if x == '1':
        OutputWorking( mWorkingDir )
        CleanAudio( mWorkingDir )
        DisplayMenu()
    elif x == '2':
        RenderFinal( mWorkingDir )
        DisplayMenu()

    elif x == 'h':
        OutputVideoClean( mWorkingDir )
        DisplayMenu()
    else:
        print('Exiting')
        return


# main
if __name__ == '__main__':
    cls()
    DisplayMenu()
