import std.stdio;
import std.string;
import std.file;
import std.path;
import std.system;
import std.process;
import std.algorithm;
import std.array;
import std.json;

auto g_FFMPEG = "ffmpeg";
auto g_SOX = "sox";
auto g_videoComplete = "video_complete.mts";
auto g_videoCropped = "video_cropped.mts";
auto g_audioWorking = ["audio_left.wav", "audio_right.wav"]; // left/right audio channel names
auto g_audioComplete =  "audio_complete.wav";
auto g_videoHighQuality = "video_highquality.mp4";
auto g_settings = "settings.txt";
auto g_workingDir = ""; 
JSONValue g_config;


// ultrafast superfast veryfast faster fast medium slow slower veryslow 
auto g_x264Preset= "fast"; //recomend fast (about 2x encode rate)
auto g_videoRate = "1500k";
auto g_verbose = "-hide_banner -loglevel error -stats";

void init()
{
	version(Windows)
	{
		g_workingDir = "c:/Sermons/";
		g_FFMPEG = g_FFMPEG ~ ".exe";
		g_SOX = g_SOX ~ ".exe";
	}
	else // assume linux
	{
		g_workingDir = expandTilde("~/Sermons/");
	}
}
 
void initConfig()
{
	// setup settings
	g_config = [
	"info_date": "",
	"info_title": "",
	"info_series": "",
	"info_speaker": "",
	"crop_start": "00:00:00.000",
	"crop_end": "00:35:00.000"
	];

	// get working dir /series/date/
	auto path = pathSplitter(g_workingDir).array;
	g_config["info_date"] = cast(string)path[path.length - 1];
	g_config["info_series"] = cast(string)path[path.length - 2];
}

// this renders the complete files
void renderCompressedAV()
{
    // read the settings file
    readSettings();
    
    // crop video to correct length
    cropVideo();

    // output mp3
    renderCompressedAudio();

    // encode video
    renderCompressedVideo();
}

// render mp3 complete
void renderCompressedAudio()
{
	// check if the target file exists
	auto outputAudioPath = buildPath(g_workingDir, g_config["info_date"].str ~ ".mp3");
	if (exists(outputAudioPath))
	{
		writeln("Render Audio: %s exists, skipping...".format(g_config["info_date"].str ~ ".mp3"));
		return;
	}
		
	// source video
    auto filename = buildPath(g_workingDir, g_videoCropped);
    if (!exists(filename))
    {
		writeln("Render Audio: %s missing, aborting!".format(g_videoCropped));
		return;
	}

	// -qscale:a 8 - https://trac.ffmpeg.org/wiki/Encode/MP3 - 70-105kb/s
	// 44100, 22050
	auto args = "%s -i %s -codec:a libmp3lame -qscale:a 8 -ar 22050 -ac 1 %s %s".format(g_FFMPEG, filename, outputAudioPath, g_verbose);
	writeln("Render Audio: Processing...");
	executeProcess(args);
	writeln("Render Audio: %s created successfully".format(g_config["info_date"].str ~ ".mp3"));

	/*
	// output wav info
	writeln("Render Audio: Audio Information:");
	args = "%s -i %s -hide_banner".format(g_FFMPEG, outputAudioPath);
	executeProcess(args);
	writeln();
	*/
}

void executeProcess(string cmd)
{
	auto cmdList = cmd.split(" ");
	//writeln("Proc: %s\n".format(cmd));
	auto pid = spawnProcess(cmdList);
	scope(exit) wait(pid);
}

// check if binary exists
void binaryCheck(string path)
{
	version (linux)
	{
		auto exec = executeShell("which %s".format(path));
		if (exec.status != 0)
			writeln("Binary Check: ERROR! Cannot find binary %s".format(path));
	}
	
	version (Windows)
	{
		if (!exists(path))
			writeln("Binary Check: ERROR! Cannot find binary %s".format(path));
	}
}

// renders compressed version for youtube as mp4
void renderCompressedVideo()
{
	// source video
    auto filenameVideo = buildPath(g_workingDir, g_videoCropped);
    
    if (!exists(filenameVideo))
    {
		writeln("Render Video: %s missing, aborting!".format(g_videoCropped));
		return;
	}
	
	// check if the target file exists
	auto outputVideoPath = buildPath(g_workingDir, g_config["info_date"].str ~ ".mp4");
	if (exists(outputVideoPath))
	{
		writeln("Render Video: %s exists, skipping...".format(outputVideoPath));
		return;
	}

	// split up args to make easy to understand
	auto audioArgs = "-codec:a libmp3lame -q:a 4 -ar 22050 -ac 1"; //  -af pan=1c:c0=c0 -map 1:a
	auto videoArgs = "-codec:v libx264 -preset %s -b:v %s -minrate %s -maxrate %s -bufsize 2048k -vf hqdn3d=1.5:1.5:6:6,scale=1280:720".format(g_x264Preset, g_videoRate, g_videoRate, g_videoRate); //  -map 0:v
	auto args = "%s -i %s %s %s -shortest %s %s".format(g_FFMPEG, filenameVideo, videoArgs, audioArgs, outputVideoPath, g_verbose);
	writeln("Render Video: Processing...");
	executeProcess(args);
	writeln("Render Video: %s created".format(g_config["info_date"].str ~ ".mp4"));

	/*
	// output video info
	writeln("Render Video: Video Information:");
	auto args = "%s -i %s -hide_banner".format(g_FFMPEG, outputVideoPath);
	executeProcess(args);
	writeln("");
	*/
}

// pre trim video
void cropVideo()
{
	auto videoCroppedPath = buildPath(g_workingDir, g_videoCropped);
	auto videoCompletePath = buildPath(g_workingDir, g_videoComplete);
	auto audioCompletePath = buildPath(g_workingDir, g_audioComplete);
	
	if (exists(videoCroppedPath))
	{
		writeln("Crop Video: %s exists, skipping...".format(g_videoCropped));
		return;
	}
	if (!exists(videoCompletePath) || !exists(audioCompletePath))
	{
		writeln("Crop Video: Error, %s or %s is missing!".format(g_videoComplete, g_audioComplete));
		return;	
	}
	
	// cant copy audio as it doesnt like wav, so convert to the mts default of ac3
	// ffmpeg -i movie.mp4 -ss 00:00:03 -t 00:00:08 -async 1 cut.mp4
	auto audioArgs = "-acodec ac3 -b:a 256k -af pan=1c:c0=c0 -map 1:a:0"; //  "-async 1" may cause sync issue, test this!
	auto videoArgs = "-vcodec copy -map 0:v:0";
	auto args = "%s -i %s -i %s -ss %s -to %s %s %s %s %s".format(g_FFMPEG, videoCompletePath, audioCompletePath, g_config["crop_start"].str, g_config["crop_end"].str, videoArgs, audioArgs, videoCroppedPath, g_verbose);
	writeln("Crop Video: Start: %s | End: %s".format(g_config["crop_start"],g_config["crop_end"]));
	writeln(args);
	executeProcess(args);
	writeln("Crop Video: %s created".format(g_videoCropped));	
	
}

// uncompressed video for use in video editing
void renderHighQualityVideo()
{
	auto videoCompletePath = buildPath(g_workingDir, g_videoComplete);
	auto audioCompletePath = buildPath(g_workingDir, g_audioComplete);

    if (!exists(videoCompletePath) || exists(videoCompletePath))
    {
		writeln("Output Video High Quality: %s or %s missing, aborting!".format(videoCompletePath, audioCompletePath));
		return;
	}
	
	// combine video complete and audio clean to make video clean!
	// we need to trim times
	readSettings();
	
	// split up args to make easy to understand
	auto videoCleanPath = buildPath(g_videoComplete, g_videoHighQuality);
	//auto audioArgs = "-codec:a aac -b:a 240k -af pan=1c:c0=c0 -strict experimental"; //libmp3lame -q:a 1
	auto audioArgs = "-acodec ac3 -b:a 256k -af pan=1c:c0=c0 -map 1:a:0 ";
	auto videoArgs = "-codec:v libx264 -preset fast -b:v 20000k -minrate 10k -maxrate 20000k -bufsize 2048k -vf hqdn3d=1.5:1.5:6:6 -map 0:v:0";
	auto args = "%s -i %s -i %s -ss %s -to %s %s %s %s %s".format(g_FFMPEG, videoCompletePath, audioCompletePath, g_config["crop_start"].str, g_config["crop_end"].str, videoArgs, audioArgs, videoCleanPath, g_verbose);
	writeln("Output Video High Quality: Processing...");
	executeProcess(args);
	writeln("Output Video High Quality: %s created".format(g_videoHighQuality));
}

void clearScreen()
{
	version (Windows) 
	{
		executeProcess("cls");
	}
    version (linux) 
    {
		executeProcess("reset");
	}
}

// search and list the last created directory at specified path
string findWorkingDir()
{
	auto baseDir = getDirByDate(g_workingDir);
    if (baseDir.length == 0)
    {
		writeln("FindWorkingDir: %s no folder, please create series".format(g_workingDir));
		return "";
	}
	
	auto workingDir = getDirByDate(baseDir[0]);
	if (workingDir.length == 0)
	{
		writeln("FindWorkingDir: %s no folder, please create date".format(baseDir[0]));
		return "";
	}
	
	g_workingDir = workingDir[0];
	writeln("Working Directory: %s".format(g_workingDir));
	return workingDir[0];
}

// get a list of directories sorted by date
auto getDirByDate(string path)
{
	
	if (!exists(path))
	{
		writeln("GetDirByDate: %s does not exist!".format(path));
		return null;
	}

	auto dirList = dirEntries(path, SpanMode.shallow)
		.filter!(f => f.isDir)
		.array;
		
	sort!"a.timeLastModified > b.timeLastModified"(dirList);

	return dirList[];
}

// find files in directory (recursive)
auto findInDir(string path, string extension)
{
	auto fileList = dirEntries(path, SpanMode.depth)
		.filter!(f => f.isFile)
		.filter!(f => f.name.endsWith(extension))
		.array;

    // sort the list by name
    sort!"a.name < b.name"(fileList);

    return fileList;
}

// read the settings to config
void readSettings()
{
    auto filename = buildPath(g_workingDir, g_settings);
    
    if (!exists(filename))
    {
		writeln("ReadSettings: %s missing, aborting!".format(g_settings));
        return;
	}
	
	// read from file into config
	auto f = File(filename, "r");
    g_config = parseJSON(f.readln());
    writeln("Settings: %s".format(g_config));
}
// function to create the settings file
void writeSettings()
{
    auto settings = buildPath(g_workingDir, g_settings);
    if (exists(settings))
    {
		readSettings();
        return;
	}

	initConfig();

	auto f = File(settings, "w"); // open for write
	f.writeln(g_config.toString());
	f.close();
	writeln("Settings: %s created".format(g_settings));
}

// does a binary copy/Concatenate of mts files
// takes new file name, and a list of files to combine
// returns the file
void concatenateVideo(string filename, DirEntry[] fileList)
{
    if (exists(filename))
    {
        writeln("Concatenate Video: %s exists, skipping...".format(g_videoComplete));
        return;
	}

	// create file
	auto destination = File(filename,"w"); // open for write

	// concatenate
	auto i = 1;
	foreach (fileName; fileList)
    {
        writeln("Concatenate Video: %s of %s - %s".format(i, fileList.length, baseName(fileName)));
		i+=1;
		
		auto f = File(fileName,"r"); // open for read
		
		foreach (ubyte[] buffer; f.byChunk(1024 * 1000)) // 100mb chunks
		{
			destination.rawWrite(buffer);
		}

    }
    
	destination.close();
	writeln("Concatenate Video: %s created successfully".format(g_videoComplete));
}

// this creates the working files
void createWorkingFiles()
{
    // get a list of the files to combine
    auto fileList = findInDir(g_workingDir,".MTS");

	if (fileList.length == 0)
	{
		writeln("OutputWorking: %s no files found".format(g_workingDir));
		return;
	}

	// video
	auto outputVideo = buildPath(g_workingDir, g_videoComplete);
	concatenateVideo(outputVideo, fileList);
	
	// audio
	auto appender = appender!(string[])();
	foreach (channel; g_audioWorking)
	{
		appender.put(buildPath(g_workingDir, channel));
	}
	auto audioList = appender.data;

	extractAudio(audioList, outputVideo);
	
	// clean audio
	cleanAudio();
}

// SOX - http://sox.sourceforge.net/sox.html
// http://www.zoharbabin.com/how-to-do-noise-reduction-using-ffmpeg-and-sox/
// sox to clean the audio
void cleanAudio()
{
    auto cleanAudioPath = buildPath(g_workingDir, g_audioComplete);
    auto workingAudioPath = buildPath(g_workingDir, g_audioWorking[0]); // only need left for now
	auto tempAudioPath = buildPath(g_workingDir, "temp.wav");
	
    if (exists(cleanAudioPath))
    {
        writeln("Clean Audio: %s exists, skipping...".format(g_audioComplete));
        return;
	}
	
	if (!exists(workingAudioPath))
	{
		writeln("Clean Audio: %s missing, aborting!".format(g_audioWorking));
		return;
	}	

	// duplicate the left channel
	copy(workingAudioPath, cleanAudioPath);

	/*
	# TODO: make noise profile wav file and test it

	# sox noise.wav -n noiseprof | play sound.wav noisered

	# Create background noise profile from mp3
	#/usr/bin/sox noise.mp3 -n noiseprof noise.prof
	# Remove noise from mp3 using profile
	#/usr/bin/sox input.mp3 output.mp3 noisered noise.prof 0.21
	*/


	// normalise
	auto args = "%s --show-progress --norm=-0.5 %s %s".format(g_SOX, cleanAudioPath, tempAudioPath); // default in audacity = -1db
	executeProcess(args);
	rename(tempAudioPath, cleanAudioPath);

	/*
	this does the same as the noise gate (cut low volumes), limiter (smooth high volumes) and compressor all in 1 nifty graph and pass!
	http://forum.doom9.org/showthread.php?t=165807
	batch script to output plot files
	sox.exe -n -n --plot=gnuplot compand 0,0 9:-15,0,-9 > try.plt
	pause
	
	compressor attack1,decay1{,attack2,decay2} [soft-knee-dB:]in-dB1[,out-dB1]{,in-dB2,out-dB2} [gain [initial-volume-dB [delay]]]
	soft knee is like a limiter 20: will soften/smoothen the graph, without this its linear
	ploted points on graph x =input vol , y = outputvolume, -2db drop on the whole graph(normalise after to fix)
	*/
	args = "%s --show-progress %s %s compand 0.3,1 50:-60,-150,-50,-90,-40,-50,-30,-30,-20,-15,0,0 -0.2 0 0.2".format(g_SOX, cleanAudioPath, tempAudioPath);
	// compand .3,.8 -24,-12 0 0 .1
	executeProcess(args);
	rename(tempAudioPath, cleanAudioPath);

	writeln("Clean Audio: %s created successfully".format(g_audioComplete));
}


// Extract WAV file
void extractAudio(string[] fileList, string sourceFile)
{
	if (exists(fileList[0]) && exists(fileList[1]))
	{
		writeln("Extract Audio: %s exists, skipping...".format(g_audioWorking));
		return;
	}
	
	auto args = "%s -i %s -acodec pcm_s16le -map_channel 0.1.0 %s -map_channel 0.1.1 %s %s".format(g_FFMPEG, sourceFile, fileList[0], fileList[1], g_verbose);
	writeln("Extract Audio: Processing...");
	executeProcess(args);
	writeln("Extract Audio: %s created successfully".format(g_audioWorking));
}

void displayMenu()
{
	// initial checks and directories stuffs
    writeln("---------------------------------------------------------------");
    
    initConfig();
    binaryCheck(g_SOX);
    binaryCheck(g_FFMPEG);
    writeSettings();
    
	writeln("---------------------------------------------------------------");
    writeln ("= Audio Video Edit =");
    writeln(" 1) Create Working Audio & Video");
    writeln(" 2) Render Compressed Audio (mp3) & Video (mp4)");
    writeln();
    writeln(" h) Render High Quality");
    writeln(" r) Refresh (Menu + Settings)");
    writeln(" q) Quit (any key)");
	writeln("");
	write("> ");
    auto x = strip(stdin.readln());
    writeln("\n");

    clearScreen();
    
	switch (x)
	{
	case "r":
		init();
		findWorkingDir();
		displayMenu();
		break;
		
	case "1":
		createWorkingFiles();
		displayMenu();
		break;
		
	case "2":
        renderCompressedAV();
        displayMenu();
        break;

    case "h":
        renderHighQualityVideo();
        displayMenu();
        break;
		
	default:
		writeln("Exiting");
        return;
	}
}

void main(string[] args)
{
	init();   
	clearScreen();
	
	// direct run
	if (args.length < 2)
	{
		findWorkingDir();
	}
	
	// drag and drop
	if (args.length == 2)
	{
		auto binPath = args[0];
		g_workingDir = args[1];
		if (!isDir(g_workingDir))
		{
			write("Opos! Please drag and drop a folder on to the exe");
			return;
		}
	}	
	
    displayMenu();
}
