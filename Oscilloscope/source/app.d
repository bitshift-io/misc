import std.stdio;
import std.system;
import std.process;
import std.string;
import serial.device; // serial-port lib
import core.time;
import std.array;
import std.json;
import core.thread;
import std.concurrency;
import std.parallelism;
import core.time;
import std.random;
import std.algorithm;
import std.range;
import core.sync.mutex;
import dlangui; // gui lib

/*
Install DUB, DMD, MonoDevelop with Mono-D plugin.
Required libraries: libsdl2, x11, libfreetype, libfontconfig and OpenGL.
 
All i needed was:
	sudo apt-get install libsdl2-2.0-0

Doesnt work on gdc for first run (library issue)
	dub --compiler=dmd

To fix "which terminal?"
	echo 'export TERM=linux' >> ~/.bash_profile
	echo $TERM # should output 'linux'

Add to serial group
	sudo adduser <the user you want to add> dialout
	sudo reboot
*/

// mutithread share
__gshared PlotBuffer g_readA0 = new PlotBuffer(2000);
__gshared PlotBuffer g_readA1 = new PlotBuffer(2000);

__gshared Mutex mutex;

static this()
{
	mutex = new Mutex;
}

class PlotBuffer
{
	// http://www.embedded.com/electronics-blogs/embedded-round-table/4419407/2/The-ring-buffer
	// https://github.com/dhess/c-ringbuf/blob/master/ringbuf.c
	// http://codereview.stackexchange.com/questions/16468/circular-ringbuffer

	plot[] buff; // data buffer array
	int head;
	int tail;
	int capacity; // max number of items in buffer
	int count; // number of items (length)


	this(int size)
	{
		buff = new plot[size]; //eg 10
		capacity = size;
		count = 0;
		tail = 0;
		head = capacity;
	}

	// add to head
	void put(plot data)
	{
		synchronized(mutex)
		{
			// increment head
			head = next(head);
			
			if (count < capacity)
				++count;  // increment count
			else
				tail = next(tail); //increment head
			
			// data
			buff[head] = data;
		}
	}

	void flush()
	{
		count = 0;
		tail = 0;
		head = capacity;
	}

	void append(plot data)
	{
		put(data);
	}

	// get from the que
	plot get(int index)
	{
		// empty or to large
		assert (index < count);

		int pos = head - index;
		if (pos < 0)
			pos += capacity;

		return buff[pos];
	}

	// a function that makes a copy
	void copy(PlotBuffer data)
	{
		synchronized(mutex)
		{
			data.head = head;
			data.tail = tail;
			data.buff = buff;
			data.count = count;
			data.capacity = capacity;
		}
	}

	// return length of buffer
	int length()
	{
		return count;
	}

	// check are we empty
	bool isEmpty()
	{
		return (count == 0);
	}

	// are we full
	bool isFull()
	{
		return (count >= capacity);
	}

	// return the next location in the buffer
	int next (int index)
	{
		int i = ++index;

		if  (i >= capacity)
			return 0;

		return i;
	}
}

class PlotWidget : CanvasWidget 
{
	MonoTime prevTime;
	int framesPassed = 0;
	int framesLastTime = 0;
	PlotBuffer drawA0;
	PlotBuffer drawA1;
	
	this()
	{
		prevTime = MonoTime.currTime;
		drawA0 = new PlotBuffer(g_readA0.capacity);
		drawA1 = new PlotBuffer(g_readA1.capacity);
	}

	/// returns true is widget is being animated - need to call animate() and redraw
	@property override bool animating() { return true; }

	/// animates window; interval is time left from previous draw, in hnsecs (1/10000000 of second)
	override void animate(long interval) 
	{
		invalidate();
	}

	// draw bg elements
	void drawBackground(DrawBuf buf, Rect rc)
	{
		// background color
		//buf.fill(0x000000);
		buf.fillRect(Rect(rc.left, rc.top, rc.right, rc.bottom), 0x000000);
		
		// draw horizontal background lines
		auto hLines = [1,2,3,4,6,7,8,9]; 
		foreach (y; hLines)
		{
			buf.drawLine(Point(rc.left, (y * (rc.bottom - rc.top)/10) + rc.top), Point(rc.right, (y * (rc.bottom - rc.top)/10) + rc.top), 0x14a083);
		}
		
		// draw vert background lines backwards from rc.right
		for (int x = rc.right; x > rc.left ; x-=100)
		{
			buf.drawLine(Point(x, rc.top), Point(x, rc.bottom), 0x14a083);
		}
		
		// center line
		buf.drawLine(Point(rc.left,(rc.bottom + rc.top)/2), Point(rc.right,(rc.bottom + rc.top)/2), 0x14a083);
	}

	void drawPlot(DrawBuf buf, Rect rc)
	{

		// set some initial point
		Point pCur = Point(rc.right, rc.bottom/2);
		Point pPrev = pCur;
		float scale = -(rc.bottom - rc.top) / 10; 
		float offset = ((rc.bottom - rc.top) / 2.0) + rc.top;

		// clone, thread safe
		g_readA0.copy(drawA0);
		g_readA0.copy(drawA1);
		
		// draw plot line
		if (drawA0.length() > 0)
		{
			// calc length for loop
			int len = rc.left;
			if (drawA0.length() < rc.right)
				len = cast(int) (rc.right - drawA0.length());
			
			// need to draw from right to left
			// loop for the graph
			for (int i = rc.right; i > len; i-=2) // change last val changes scale
			{
				// previous point
				pPrev = pCur;
				
				// new point
				pCur.x = i; 
				pCur.y = cast(int)((drawA0.get(rc.right - i).voltage * scale) + offset);
				
				// draw
				buf.drawLine(pCur, pPrev, 0xe1e461);
			}
			
			//overlay ui element
			string strA = "A : %sv".format(drawA0.get(drawA0.length-1).voltage);
			this.font.drawText(buf, rc.left + 10, rc.top + 10, toUTF32(strA), 0xe1e461);
		}
	}

	override void doDraw(DrawBuf buf, Rect rc) 
	{

		//Rect rc = rect; 
		//rc = Rect(100,100,500,300); // testing
		
		// frame rate calc
		framesPassed++;
		MonoTime curTime = MonoTime.currTime;
		Duration delta = curTime - prevTime;
		if (delta > dur!"seconds"(1))
		{
			framesLastTime = framesPassed;
			framesPassed = 0;
			prevTime = curTime;
		}

		// bg element
		drawBackground(buf, rc);

		// plot element
		drawPlot(buf, rc);

		// draw fps
		string strFPS = "FPS : %s".format(framesLastTime);
		this.font.drawText(buf, rc.left + 20, rc.bottom - 40, toUTF32(strFPS), 0xe1e461);

    }
}

// data container
struct plot
{
	float voltage;
	MonoTime time;
	Duration deltaTime;
}

// clear terminal window
void clearScreen()
{
	version (Windows) 
	{
		executeProcess("cls");
	}
    version (linux) 
    {
		executeProcess("reset");
	}
}

// run command
void executeProcess(string cmd)
{
	auto cmdList = cmd.split(" ");
	//writeln("Proc: %s\n".format(cmd));
	auto pid = spawnProcess(cmdList);
	scope(exit) wait(pid);
}

void init()
{
}

// get list of serial ports
auto getSerialPorts()
{
	auto ports = SerialPort.ports;
	
	// loop over each port and check if its connectable
	auto timeout = dur!("msecs")(200);
	SerialPort[] result;
	SerialPort s;
    foreach (port; ports)
    {
		try
		{
			s = new SerialPort(port, timeout, timeout);
			s.close();
			result ~= s;
		}
		catch(Throwable ex) {}
	}

	return result;
}

// open serial port
auto openSerialPort()
{
	// if we found one only, connect to that device
	auto timeout = dur!("msecs")(2000000);
	auto dev = getSerialPorts();
	SerialPort g_serialPort;
	
	if (dev.length == 1)
	{
		g_serialPort = new SerialPort(dev[0].toString, timeout, timeout);
		g_serialPort.speed(BaudRate.BR_38400); // BR_38400 seems to be max speed?
		writeln("Auto-connected device: %s @ %s".format(g_serialPort.toString, g_serialPort.speed));
	}
	else
	{
		version(linux)
		{
			g_serialPort = new SerialPort("/dev/ttyUSB0", timeout, timeout);
		}
		version(Windows)
		{
			g_serialPort = new SerialPort("COM4", timeout, timeout);
		}
		g_serialPort.speed(BaudRate.BR_38400);
		writeln("Connected device: %s @ %s".format(g_serialPort.toString, g_serialPort.speed));
	}

	// sleep
	//Thread.sleep(dur!("seconds")(2));

	return g_serialPort;
}


// threaded function to read serial to array
auto readSerial()
{
	SerialPort serialPort = openSerialPort();
	// 2d array for each channel
	// contains: [0] = analogue value, [1] = time stamp
	float offset = 128;
	float scale = 2.0/256;
	ubyte[1] buff;
	MonoTime curTime = MonoTime.currTime;
	MonoTime prevTime = curTime;
	plot p;

	auto connected = false;

	while (true)
	{	
		// set time
		prevTime = curTime;

		// read data
		try
		{
			serialPort.read(buff); 
		}
		catch(Exception e) 
		{
			//writeln(e.msg);
		}
		curTime = MonoTime.currTime;

		// set data
		p.time = curTime;
		p.deltaTime = curTime - prevTime;

		// A0 data
		p.voltage = ((cast(float)buff[0] - offset) * scale); // change value from -128 to + 128
		g_readA0.append(p);

		// A1 data
		//p.voltage = ((buff[1] - offset) * scale);
		//inA1 ~= p;
		
		// copy into global
		//g_inA0[] = inA0;
		//g_inA1[] = inA1;
		
		//writeln(g_inA0.length());
		//writeln(g_inA0.get(0));
	}

}

// dlangui main entry point
mixin APP_ENTRY_POINT;
extern (C) int UIAppMain(string[] args) 
{
	init();   
	clearScreen();
	writeln("Loading Oscilloscope.");

	//auto updateTask = task!readSerial();
	//updateTask.executeInNewThread();  
	//Thread.sleep(dur!("seconds")(10));

	// create window
	Window window = Platform.instance.createWindow("Scope'd"d, null, WindowFlag.Resizable, 1280, 720);

    // layout
    VerticalLayout contentLayout = new VerticalLayout();
    
	//contentLayout.addChild((new Button()).text("Hello, world!"d).margins(Rect(20,20,20,20)));

	// http://forum.dlang.org/post/merq7m$28db$1@digitalmars.com
	PlotWidget canvas = new PlotWidget();
	canvas.layoutWidth(FILL_PARENT).layoutHeight(FILL_PARENT);
	contentLayout.addChild(canvas);
	
    // create some widget to show in window
    window.mainWidget = canvas;
    
    // show window
    window.show();

	// read serial port thread
	auto updateTask = task!readSerial();
   	updateTask.executeInNewThread();    

    // run message loop
    return Platform.instance.enterMessageLoop();

}

