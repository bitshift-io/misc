#!/usr/bin/env python3

# http://www.toptechboy.com/tutorial/python-with-arduino-lesson-11-plotting-and-graphing-live-data-from-arduino-with-matplotlib/
# http://bastibe.de/2013-05-30-speeding-up-matplotlib.html

import time
import matplotlib.pyplot as plt
import serial
import struct
import thread as thread
import datetime
import matplotlib.widgets as widget
import sys
import glob
 
print ("Loading Oscilloscope...")
 
offset = 128
scale = 2.0/256
DataPoints = 600 # limited by physcial pixels so leave this unless increasing res
stepSize = 10 # this affects the time scale

global InA0, InA1, TempA0, TempA1
InA0 = []
InA1 = []
TempA0 = []
TempA1 = []
for i in range(0, DataPoints):
    InA0.append(0)
    InA1.append(0)

# list serial ports
# linux list ports which are connectable only
def serial_ports():
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result
    
# find and connect to device else, hardcoded if multi devices    
availableDevices = serial_ports()
if (len(availableDevices) == 1):
	print ("Found single device")
	print (availableDevices[0])
	board = serial.Serial(availableDevices[0], baudrate=57600, timeout=10)
else:
	board = serial.Serial('/dev/ttyUSB1', baudrate=57600, timeout=10)

# time in ms
def TimeMS():
    return int((datetime.datetime.utcnow() - datetime.datetime(1970, 1, 1)).total_seconds() * 1000)
    
# serial read thread
def handle_data(data):
	global InA0, InA1, stepSize
	# copy array for multithreaded fudge
	# TODO: thread locking??
	TempA0 = InA0[:]
	TempA1 = InA1[:]
		
	for i in range(0, len(data), stepSize):
		read = ord(data[i])
		TempA0.append( ( read - offset ) * scale)                    
		TempA1.append( ( read - offset ) * scale)
		
		# pop the last value from the array
		TempA0.pop(0)
		TempA1.pop(0) 	
	
	# copy data to old array
	InA0 = TempA0[:]
	#InA1 = TempA1[:]

def read_from_port(ser):
	connected = False
	while not connected:
		connected = True
		while True:
			reading = ser.read(20)
			handle_data(reading)
           
t = thread.start_new_thread( read_from_port, (board,) )  
       

#plt.ion() #Tell matplotlib you want interactive mode to plot live data
plt.switch_backend('QT5Agg') # brons ubuntu fix 

# setup the plot interface
# get figure and subplots
fig, (ax1, ax2) = plt.subplots(2, sharex=True)
fig.canvas.set_window_title('Oscilloscope')
fig.tight_layout() # reduce border
fm = plt.get_current_fig_manager()
fm.resize(1100,800)

# subplot 1
ax1.set_ylim([-1, 1])
ax1.grid(True)
ax1.set_title('Ch. A')

# subplot 2
ax2.set_ylim([-1, 1])
ax2.grid(True)
ax2.set_title('Ch. B')

# show it!
plt.show(block=False)
fig.canvas.draw()

# save the bg
bg = fig.canvas.copy_from_bbox(fig.bbox) 

# add the lines
line, = ax1.plot(InA0)
line2, = ax2.plot(InA1)

# while window is open
while plt.fignum_exists(1): 
	lastTime = TimeMS()

	# draw lines	
	line.set_ydata(InA0)
	line2.set_ydata(InA1)
	
	# redraw background
	fig.canvas.restore_region(bg) 
	
	# draw line
	ax1.draw_artist(line)
	ax2.draw_artist(line2)
	
	fig.canvas.update()
	fig.canvas.flush_events()
	
	# frame limiting 60fps = 1000/60 = 16.7ms | 30fps = 1000/30 = 33.3
	while (lastTime + 33) > TimeMS():
		continue
			
# exit clean
board.close() 

