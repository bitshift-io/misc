// https://www.element14.com/community/groups/arduino/blog/2016/02/07/arduino-as-slow-oscilloscope    
byte mask=B01000000;//mask for start of conversion & state of conversion  
char t=' ';  
  
void setup()  
{  
  Serial.begin(57600);//500kByte/s, super speed!  
  ADMUX=B01100000;//I'm using input A0,result is left adjusted  
  DIDR0=0x01;//shut off digital input for A0  
  ADCSRA=B11000011;//I'm using 8 as division factor,with 4 I obtain quite noisy measurements  
  ADCSRB=0x00;//free running, default setting  
}  
  
void loop()  
{  
  startofconversion();//start a new conversion  
  Serial.print(t);//send old data  
  t=waitandget();//obtain new data, it will be send next cicle  
}  
  
void startofconversion()  
{  
  ADCSRA=ADCSRA|mask;//let's start a conversion!  
}  
  
byte waitandget()  
{  
  while((ADCSRA&mask)==mask)//wait  
  {  
  }  
  
  return ADCH;  
}  
