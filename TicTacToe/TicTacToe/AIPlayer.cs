﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Collections;

namespace TicTacToe
{
    class AIEnvironment
    {
        Game m_game;

        public AIEnvironment(Game game)
        {
            m_game = game;
        }

        public Game GetGame()
        {
            return m_game;
        }

        public Board GetBoard()
        {
            return m_game.GetBoard();
        }
    }

    class AIPlayer : Player
    {
        NNet m_network;
        Random m_random;
        AIEnvironment m_environment;

        public AIPlayer(int index, AIEnvironment environment) 
            : base(index)
        {
            m_environment = environment;
            m_random = new Random();

            //NNet test = new NNet();
            //test.test();

            m_network = LoadObject<NNet>("network_" + m_index.ToString() + ".ser");
            if (m_network == null)
            {
                m_network = new NNet();
                m_network.mFunction = new Sigmoid();
                m_network.mLearnRate = 1.0;

                const int numPlayers = 2;
                const int numTiles = 9;
                int inputCount = (numPlayers * numTiles) + numPlayers;
                NeuronLayer[] layer = new NeuronLayer[1];
                layer[0] = new NeuronLayer(numTiles); // output, which tile to return

                // this min/max weight is important as it does assumes initially all moves ar roughly a draw
                // and stops the network thinking it can win when it really cant!
                m_network.create(inputCount, layer, 0.25, 0.75);
            }
        }

        ~AIPlayer()
        {
        }

        private void SaveObject<T>(T List, string FileName)
        {
            try
            {
                //create a backup
                string backupName = Path.ChangeExtension(FileName, ".old");
                if (File.Exists(FileName))
                {
                    if (File.Exists(backupName))
                        File.Delete(backupName);
                    File.Move(FileName, backupName);
                }

                using (FileStream fs = new FileStream(FileName, FileMode.Create))
                {
                    BinaryFormatter ser = new BinaryFormatter();
                    ser.Serialize(fs, List);
                    fs.Flush();
                    fs.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private static T LoadObject<T>(string FileName)
        {
            if (!File.Exists(FileName))
            {
                return default(T);
            }

            T result = default(T);

            try
            {
                using (FileStream fs = new FileStream(FileName, FileMode.Open))
                {
                    BinaryFormatter ser = new BinaryFormatter();
                    result = (T)ser.Deserialize(fs);
                }
            }
            catch
            {
            }
            return result;
        }

        public void StartGame()
        {
        }

        public void EndGame(bool winner)
        {
        }

        public void Save()
        {
            SaveObject<NNet>(m_network, "network_" + m_index.ToString() + ".ser");
        }
     
        //
        // Look ahead n moves and determine the best chance of winning
        //
        double[] PredictFutureReward(Board board, int lookAhead)
        {
            int boardSize = board.GetBoardSize();
            double[] futureReward = new double[(boardSize * boardSize) + 1];
            for (int y = 0; y < boardSize; ++y)
            {
                for (int x = 0; x < boardSize; ++x)
                {
                    Board.Tile tile = board.GetTile(new Point(x, y));
                    if (tile == null || tile.player == null)
                    {
                        Board clone = board.Clone();
                        Board.Tile moveTile = new Board.Tile();
                        moveTile.player = this;
                        moveTile.index = new Point(x, y);
                        clone.SetTile(moveTile);

                        futureReward[(y * boardSize) + x] = PredictFutureRewardRecursive(clone, lookAhead - 1, GetNextPlayer(this));
                    }
                }
            }

            return futureReward;
        }

        Player GetNextPlayer(Player currentPlayer)
        {
            return m_environment.GetGame().GetNextPlayer(currentPlayer);
        }

        double PredictFutureRewardRecursive(Board board, int lookAhead, Player player)
        {
            Player winner = m_environment.GetGame().CheckForWinner(board);
            if (winner != null)
            {
                if (winner == this)
                    return 1.0;

                return 0.0;
            }

            double totalValue = 0.0;
            int numValue = 0;
            int boardSize = board.GetBoardSize();
            for (int y = 0; y < boardSize; ++y)
            {
                for (int x = 0; x < boardSize; ++x)
                {
                    Board.Tile tile = board.GetTile(new Point(x, y));
                    if (tile == null || tile.player == null)
                    {
                        int newLookAhead = lookAhead - 1;

                        if (newLookAhead <= 0)
                        {
                            // dont know if this leads to a win draw lose, so make a guess
                            // return what we think the future reward for this square is
                            double[] inputs = GetInputsFromBoard(board);
                            double[] outputs = m_network.Compute(inputs);

                            totalValue += outputs[(y * boardSize) + x];
                            ++numValue;
                        }
                        else
                        {
                            Board clone = board.Clone();
                            Board.Tile moveTile = new Board.Tile();
                            moveTile.player = player;
                            moveTile.index = new Point(x, y);
                            clone.SetTile(moveTile);

                            totalValue += PredictFutureRewardRecursive(clone, newLookAhead, GetNextPlayer(player));
                            ++numValue;
                        }
                    }
                }
            }

            // draw
            if (numValue <= 0)
                return 0.5;

            double averageValue = totalValue / numValue;
            return averageValue;
        }

        double[] GetInputsFromBoard(Board board)
        {
            // feed state of board to nerual network and get a resulting move
            const int numPlayers = 2;
            const int numTiles = 9;
            int inputCount = (numPlayers * numTiles) + numPlayers;
            double[] inputs = new double[inputCount];

            int inputIndex = 0;
            int boardSize = board.GetBoardSize();
            for (int y = 0; y < boardSize; ++y)
            {
                for (int x = 0; x < boardSize; ++x)
                {
                    Board.Tile tile = board.GetTile(new Point(x, y));
                    if (tile != null && tile.player != null)
                    {
                        inputs[inputIndex + tile.player.GetIndex()] = 1.0;
                    }

                    inputIndex += numPlayers;
                }
            }

            return inputs;
        }

        public Point? HaveTurn()
        {
            // future reward is the expexted outputs for the neruons
            // we can learn from this!
            double[] futureReward = PredictFutureReward(m_environment.GetBoard(), 3); // look ahead two of our moves
            //double lookAheadError = futureReward - m_previousFutureReward;
            //Point? move = GetMove(m_environment.GetBoard());

            Board board = m_environment.GetBoard();
            int boardSize = board.GetBoardSize();
            double[] inputs = GetInputsFromBoard(board);

            
            double[] outputs = null;
            //for (int i = 0; i < 1000; ++i )
            {
                //outputs = 
                double[] currentOuputs = m_network.Train(inputs, futureReward);
                    outputs = futureReward;
                int nothing = 0;
            }

            Point? bestMovePoint = null;
            double bestMoveValue = double.MinValue;
            for (int y = 0; y < boardSize; ++y)
            {
                for (int x = 0; x < boardSize; ++x)
                {
                    double value = outputs[(y * boardSize) + x];
                    if (value > bestMoveValue)
                    {
                        Point move = new Point(x, y);
                        if (m_environment.GetGame().IsValidMove(move))
                        {
                            bestMovePoint = move;
                            bestMoveValue = value;
                        }
                    }
                }
            }

            return bestMovePoint;

            /*

            // http://www.clear.rice.edu/comp440/handouts/pa4.pdf
            List<AIEnvironment.ValidMoveData> validMoveDataList = m_environment.GetValidMoveDataList();
            return validMoveDataList[m_random.Next(validMoveDataList.Count())].validMove;*/
           
        }
    }
}
