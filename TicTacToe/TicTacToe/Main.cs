﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Main : Form
    {
        Game m_game;

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            m_game = new Game(this);
        }

        private void board_Click(object sender, EventArgs e)
        {
            m_game.MouseClick();
        }

        public void SetGameMessage(String message)
        {
            game_state.Text = message;
        }

        private void new_game_Click(object sender, EventArgs e)
        {
            m_game.StartHumanAI();
        }

        public PictureBox GetGamePictureBox()
        {
            return board;
        }

        private void ai_v_ai_Click(object sender, EventArgs e)
        {
            m_game.StartAIAI();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_game.SaveAI();
        }

    }
}
