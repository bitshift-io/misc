﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;

namespace TicTacToe
{
    //
    // Responsible for manipulating the board
    // ie. handling rules and telling the board where the peices are
    //
    class Game
    {
        Main m_form;
        Board m_board;
        int m_playerCount = 2;
        Player[] m_playerList;
        int m_currentPlayerIndex;
        System.Timers.Timer m_updateTimer;
        Point m_cursorTile;
        bool m_mouseClick = false;

        List<Point> m_validMoveList = new List<Point>();

        public Game(Main form)
        {
            m_form = form;
            m_updateTimer = new System.Timers.Timer();
            m_updateTimer.Elapsed += new ElapsedEventHandler(UpdateTimerElapsed);
            m_updateTimer.Interval = 1000 / 30; // 30 times per second

            m_playerList = new Player[m_playerCount];

            StartHumanAI();
        }

        int GetAIPlayerCount()
        {
            int aiPlayerCount = 0;
            foreach (Player player in m_playerList)
            {
                if (player.GetType() == typeof(AIPlayer))
                {
                    ++aiPlayerCount;
                }
            }

            return aiPlayerCount;
        }

        public void StartHumanAI()
        {
            m_playerList[0] = new Player(0);
            m_playerList[1] = new AIPlayer(1, new AIEnvironment(this));

            StartNewGame();
        }

        public void StartAIAI()
        {
            m_playerList[0] = new AIPlayer(0, new AIEnvironment(this));
            m_playerList[1] = new AIPlayer(1, new AIEnvironment(this));

            StartNewGame();
        }

        public void StartNewGame()
        {
            m_board = new Board(m_form.GetGamePictureBox());
            m_currentPlayerIndex = 0;

            int boardSize = m_board.GetBoardSize();
            m_validMoveList = CalculateValidMovesForPlayer(GetCurrentPlayer(), m_board);

            SetGameMessage("Player 1's Turn");

            Draw();

            foreach (Player player in m_playerList)
            {
                if (player.GetType() == typeof(AIPlayer))
                {
                    AIPlayer ai = (AIPlayer)player;
                    ai.StartGame();
                }
            }

            m_updateTimer.Start();
        }

        public List<Point> GetValidMoveList()
        {
            return m_validMoveList;
        }

        public Board GetBoard()
        {
            return m_board;
        }

        public Player[] GetPlayerList()
        {
            return m_playerList;
        }

        public Player GetCurrentPlayer()
        {
            return m_playerList[m_currentPlayerIndex];
        }

        public void SaveAI()
        {
            foreach (Player player in m_playerList)
            {
                if (player.GetType() == typeof(AIPlayer))
                {
                    AIPlayer ai = (AIPlayer)player;
                    ai.Save();
                }
            }
        }

        void EndCurrentPlayerTurn()
        {
            ++m_currentPlayerIndex;
            if (m_currentPlayerIndex >= m_playerList.Count())
            {
                m_currentPlayerIndex = 0;
            }
        }

        public void UpdateTimerElapsed(object source, ElapsedEventArgs e)
        {
            try
            {
                MethodInvoker mi = new MethodInvoker(Update);
                m_form.BeginInvoke(mi);
            }
            catch (Exception ex)
            {
            }
        }

        public void MouseClick()
        {
            m_mouseClick = true;
        }

        public bool IsValidMove(Point tile)
        {
            foreach (Point validMove in m_validMoveList)
            {
                if (tile.X == validMove.X && tile.Y == validMove.Y)
                    return true;
            }

            return false;
        }

        public List<Point> CalculateValidMovesForPlayer(Player player, Board board)
        {
            List<Point> validMoveList = new List<Point>();

            int boardSize = m_board.GetBoardSize();
            for (int y = 0; y < boardSize; ++y)
            {
                for (int x = 0; x < boardSize; ++x)
                {
                    Board.Tile tile = m_board.GetTile(new Point(x, y));
                    if (tile == null)
                    {
                        validMoveList.Add(new Point(x, y));
                    }
                }
            }

            return validMoveList;
        }

        public void PerformMove(Point index)
        {
            m_board = GetBoardAfterMove(index);
        }

        public Board GetBoardAfterMove(Point index)
        {
            Board postMoveBoard = m_board.Clone();

            Player player = GetCurrentPlayer();
            Board.Tile tile = new Board.Tile();
            tile.player = player;
            tile.index = index;
            postMoveBoard.SetTile(tile);

            return postMoveBoard;
        }

        public Player GetNextPlayer(Player currentPlayer)
        {
            int currentPlayerIndex = 0;
            for (int p = 0; p < m_playerList.Count(); ++p)
            {
                if (currentPlayer == m_playerList[p])
                {
                    currentPlayerIndex = p;
                    break;
                }
            }

            currentPlayerIndex++;
            if (currentPlayerIndex >= m_playerList.Count())
                currentPlayerIndex = 0;

            return m_playerList[currentPlayerIndex];
        }

        public void SetGameMessage(String message)
        {
            m_form.SetGameMessage(message);
        }
       
        public Player CheckForWinner(Board board)
        {
            Player winner = null;
            int boardSize = board.GetBoardSize();

            // check horizontal solutions
            for (int y = 0; y < boardSize; ++y)
            {
                winner = null;
                for (int x = 0; x < boardSize; ++x)
                {
                    Board.Tile tile = board.GetTile(new Point(x, y));
                    if (tile != null && x == 0)
                    {
                        winner = tile.player;
                    }

                    if (tile == null || winner == null || tile.player != winner)
                    {
                        winner = null;
                        break;
                    }
                }

                if (winner != null)
                    return winner;
            }

            // check vertical solutions
            for (int x = 0; x < boardSize; ++x)
            {
                winner = null;
                for (int y = 0; y < boardSize; ++y)
                {
                    Board.Tile tile = board.GetTile(new Point(x, y));
                    if (tile != null && y == 0)
                    {
                        winner = tile.player;
                    }

                    if (tile == null || winner == null || tile.player != winner)
                    {
                        winner = null;
                        break;
                    }
                }

                if (winner != null)
                    return winner;
            }

            // check for diagonal solutions
            winner = null;
            for (int x = 0; x < boardSize; ++x)
            {
                Board.Tile tile = board.GetTile(new Point(x, x));
                if (tile != null && x == 0)
                {
                    winner = tile.player;
                }

                if (tile == null || winner == null || tile.player != winner)
                {
                    winner = null;
                    break;
                }
            }

            if (winner != null)
                return winner;

            winner = null;
            for (int x = 0; x < boardSize; ++x)
            {
                Board.Tile tile = board.GetTile(new Point(x, (boardSize - 1) - x));
                if (tile != null && x == 0)
                {
                    winner = tile.player;
                }

                if (tile == null || winner == null || tile.player != winner)
                {
                    winner = null;
                    break;
                }
            }

            if (winner != null)
                return winner;

            return null;
        }

        public void EndGame(Player winner)
        {
            if (winner == null)
                SetGameMessage("Draw");
            else
                SetGameMessage("Player " + (winner.GetIndex() + 1) + " Wins");

            m_updateTimer.Stop();

            foreach (Player player in m_playerList)
            {
                if (player.GetType() == typeof(AIPlayer))
                {
                    AIPlayer ai = (AIPlayer)player;
                    ai.EndGame(winner == player);
                }
            }

            // AI dont need to wait for new game, keep going!
            if (GetAIPlayerCount() >= 2)
            {
                StartNewGame();
            }
        }

        public void Update()
        {
            bool draw = false;
            Point cursorTile = m_board.PointToTile(System.Windows.Forms.Cursor.Position);
            if (cursorTile != m_cursorTile)
            {
                m_cursorTile = cursorTile;
                draw = true;
            }

            Player player = GetCurrentPlayer();
            if (player.GetType() == typeof(AIPlayer))
            {
                AIPlayer ai = (AIPlayer)player;

                Point? aiCursor = ai.HaveTurn();
                if (aiCursor.HasValue)
                {
                    m_cursorTile = aiCursor.Value;
                    m_mouseClick = true;
                }
            }

            if (m_mouseClick)
            {
                if (IsValidMove(m_cursorTile))
                {
                    PerformMove(m_cursorTile);
                    Player winner = CheckForWinner(m_board);

                    Draw(); // for debugging

                    int playerCantMoveCount = 0;
                    for (int i = 0; i < m_playerCount; ++i)
                    {
                        EndCurrentPlayerTurn();
                        m_validMoveList = CalculateValidMovesForPlayer(GetCurrentPlayer(), m_board);
                        if (m_validMoveList.Count() <= 0)
                            ++playerCantMoveCount;
                        else
                            break;
                    }

                    SetGameMessage("Player " + (GetCurrentPlayer().GetIndex() + 1) + "'s Turn");

                    if (playerCantMoveCount >= m_playerCount || winner != null)
                    {
                        EndGame(winner);
                    }

                    draw = true;
                }
                m_mouseClick = false;
            }

            if (draw)
                Draw();
        }

        public void Draw()
        {
            Graphics g = m_board.DrawBegin();
            m_board.DrawBoard(g);
            m_board.DrawCursor(g, m_cursorTile, Color.Green);
            m_board.DrawEnd(g);
        }
    }
}
