﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTacToe
{
    //[Serializable]
    public interface ActivationFunction // wont serialize if its an interface
    {
        // Given X return Y 
        double FunctionX(double x);

        // Given Y, get rate of change of equation
        double DerivativeY(double y);
    }

    [Serializable]
    public class Sigmoid : ActivationFunction
    {
        public Sigmoid(double alpha = 2)
        {
            m_alpha = alpha;
        }

        double ActivationFunction.FunctionX(double x)
        {
            return (1.0 / (1.0 + Math.Exp(-m_alpha * x)));
        }

        double ActivationFunction.DerivativeY(double y)
        {
            return m_alpha * y * (1 - y);
        }

        double m_alpha = 2;
    }

    [Serializable]
    public class BipolarSigmoid : ActivationFunction
    {
        public BipolarSigmoid(double alpha = 6)
        {
            m_alpha = alpha;
        }

        double ActivationFunction.FunctionX(double x)
        {
            return (2.0 / (1.0 + Math.Exp(-m_alpha * x))) - 1;
        }

        double ActivationFunction.DerivativeY(double y)
        {
            return m_alpha * (1 - y * y) / 2;
        }

        double m_alpha = 6;
    }

    [Serializable]
    public class NeuronLayer
    {
        public NeuronLayer()
        {
        }

        public NeuronLayer(int neuronCount)
        {
            neuron = new Neuron[neuronCount];
        }

        public Neuron[] neuron;
    }

    [Serializable]
    public class Neuron
    {
        public double[] weight; // input weights
        public double input; // only for debugging
        public double output;
        public double learnRate;
        public double error;
        public bool enabled = true;
        public bool feedThrough = false; // if false, will feed the output from the last neruon straight through as its output, as it it doesnt exist
    }

    [Serializable]
    public class NNet
    {
        public void create(int inputCount, NeuronLayer[] layer)
        {
            create(inputCount, layer, -1, 1, -1, 1);
        }

        public void create(int inputCount, NeuronLayer[] layer, double minWeight, double maxWeight)
        {
            create(inputCount, layer, minWeight, maxWeight, -1, 1);
        }


        public void create(int inputCount, NeuronLayer[] layer, double minWeight, double maxWeight, double minLearnRate, double maxLearnRate)
        {
            mLayer = layer;
            //new NeuronLayer[2];
            //mLayer[0] = new NeuronLayer(3);
            //mLayer[1] = new NeuronLayer(1);

            for (int l = 0; l < mLayer.Length; ++l)
            {
                for (int n = 0; n < mLayer[l].neuron.Length; ++n)
                {
                    int previousLayerCount = (l == 0) ? inputCount : mLayer[l - 1].neuron.Length;
                    Neuron neuron = new Neuron();
                    mLayer[l].neuron[n] = neuron;
                    neuron.weight = new double[previousLayerCount + 1]; // +1 for threshold weight
                    neuron.learnRate = mRandom.NextDouble() * (maxLearnRate - minLearnRate) + minLearnRate;

                    for (int w = 0; w < neuron.weight.Length; ++w)
                    {
                        neuron.weight[w] = mRandom.NextDouble() * (maxWeight - minWeight) + minWeight;
                        neuron.weight[w] = round(neuron.weight[w]);
                    }
                }
            }
        }

        public double round(double value)
        {
            if (mRound < 0)
                return value;

            return Math.Round(value, mRound);
        }

        public double[] Train(double[] input, double[] expectedOutput)
        {
            double[] output = Compute(input);

            // go through and calculate errors
            for (int l = (mLayer.Length - 1); l >= 0; --l)
            {
                for (int n = 0; n < mLayer[l].neuron.Length; ++n)
                {
                    Neuron neuron = mLayer[l].neuron[n];

                    // Derivative here indicates the rate of change of the curve at the point n
                    // so if the rate of change is high, the error will be high
                    // if the rate of change is low, the error will be low
                    if ((l + 1) == mLayer.Length)
                    {
                        double outputExpectedDelta = expectedOutput[n] - neuron.output;
                        neuron.error = outputExpectedDelta * mFunction.DerivativeY(neuron.output);
                    }
                    else
                    {
                        // multiply error and weights, propagating this backwards
                        double error = 0.0;
                        for (int on = 0; on < mLayer[l + 1].neuron.Length; ++on)
                        {
                            Neuron outputNeuron = mLayer[l + 1].neuron[on];
                            error += outputNeuron.weight[n] * outputNeuron.error;
                        }

                        neuron.error = error * mFunction.DerivativeY(neuron.output);
                    }
                }
            }

            // now alter the weights
            for (int l = 0; l < mLayer.Length; ++l)
            {
                for (int n = 0; n < mLayer[l].neuron.Length; ++n)
                {
                    Neuron neuron = mLayer[l].neuron[n];

                    for (int w = 0; w < neuron.weight.Length; ++w)
                    {
                        if ((w + 1) == neuron.weight.Length)
                        {
                            neuron.weight[w] += mLearnRate * neuron.error;
                        }
                        else if (l == 0)
                        {
                            neuron.weight[w] += mLearnRate * neuron.error * input[w];
                        }
                        else
                        {
                            Neuron previousNeuron = mLayer[l - 1].neuron[w];
                            neuron.weight[w] += mLearnRate * neuron.error * previousNeuron.output;
                        }

                        neuron.weight[w] = round(neuron.weight[w]);
                    }
                }
            }

            return output;
        }

        public double[] GetOutputs(int layer)
        {
            // gather outputs and return them
            NeuronLayer outputLayer = mLayer[layer];
            double[] outputs = new double[outputLayer.neuron.Length];

            for (int n = 0; n < outputLayer.neuron.Length; ++n)
            {
                outputs[n] = outputLayer.neuron[n].output;
            }

            return outputs;
        }

        public double[] Compute(double[] input)
        {
            for (int l = 0; l < mLayer.Length; ++l)
            {
                for (int n = 0; n < mLayer[l].neuron.Length; ++n)
                {
                    Neuron neuron = mLayer[l].neuron[n];
                    neuron.input = 0;

                    if (!neuron.enabled)
                        continue;

                    if (neuron.feedThrough)
                    {
                        if (l == 0)
                            neuron.input = input[n];
                        else
                            neuron.input = mLayer[l - 1].neuron[n].output;

                        continue;
                    }

                    for (int w = 0; w < neuron.weight.Length; ++w)
                    {
                        if ((w + 1) == neuron.weight.Length)
                        {
                            neuron.input += neuron.weight[w];
                        }
                        else if (l == 0)
                        {
                            neuron.input += neuron.weight[w] * input[w];
                        }
                        else
                        {
                            Neuron previousNeuron = mLayer[l - 1].neuron[w];
                            neuron.input += neuron.weight[w] * previousNeuron.output;
                        }
                    }

                    double output = mFunction.FunctionX(neuron.input);
                    neuron.output = round(output);
                }
            }

            // gather outputs and return them
            return GetOutputs(mLayer.Length - 1);
        }

        public void test()
	    {
		    NeuronLayer[] layer = new NeuronLayer[1];
		    layer[0] = new NeuronLayer(1);
		    create(2, layer);

            mFunction = new Sigmoid();
		
		    float trainingCount = 2000;
		

		    // OR
		    for (int i=0;i<trainingCount;i++)
		    {
                Train(new double[] { 0, 0 }, new double[] { 0 });
                Train(new double[] { 0, 1 }, new double[] { 1 });
                Train(new double[] { 1, 0 }, new double[] { 1 });
                Train(new double[] { 1, 1 }, new double[] { 1 });
		    }

            Console.WriteLine("--- OR ---");
            Console.WriteLine("0,0 = " + Compute(new double[] { 0, 0 })[0]);
            Console.WriteLine("0,1 = " + Compute(new double[] { 0, 1 })[0]);
            Console.WriteLine("1,0 = " + Compute(new double[] { 1, 0 })[0]);
            Console.WriteLine("1,1 = " + Compute(new double[] { 1, 1 })[0]);
    
		    
		    // AND
		    for (int i=0;i<trainingCount;i++)
		    {
                Train(new double[] { 0, 0 }, new double[] { 0 });
                Train(new double[] { 0, 1 }, new double[] { 0 });
                Train(new double[] { 1, 0 }, new double[] { 0 });
                Train(new double[] { 1, 1 }, new double[] { 1 });
		    }

            Console.WriteLine("--- AND ---");
            Console.WriteLine("0,0 = " + Compute(new double[] { 0, 0 })[0]);
            Console.WriteLine("0,1 = " + Compute(new double[] { 0, 1 })[0]);
            Console.WriteLine("1,0 = " + Compute(new double[] { 1, 0 })[0]);
            Console.WriteLine("1,1 = " + Compute(new double[] { 1, 1 })[0]);


            layer = new NeuronLayer[2];
            layer[0] = new NeuronLayer(3);
            layer[1] = new NeuronLayer(1);
            create(2, layer);

            // XOR
            for (int i = 0; i < trainingCount; i++)
            {
                Train(new double[] { 0, 0 }, new double[] { 0 });
                Train(new double[] { 0, 1 }, new double[] { 1 });
                Train(new double[] { 1, 0 }, new double[] { 1 });
                Train(new double[] { 1, 1 }, new double[] { 0 });
            }

            Console.WriteLine("--- XOR ---");
            Console.WriteLine("0,0 = " + Compute(new double[] { 0, 0 })[0]);
            Console.WriteLine("0,1 = " + Compute(new double[] { 0, 1 })[0]);
            Console.WriteLine("1,0 = " + Compute(new double[] { 1, 0 })[0]);
            Console.WriteLine("1,1 = " + Compute(new double[] { 1, 1 })[0]);
	    }

        public int mRound = -1; // round to n decimal points useful for user manual edit mode
        public Random mRandom = new Random();
        public NeuronLayer[] mLayer;
        public double mLearnRate = 0.5;
        public ActivationFunction mFunction = new BipolarSigmoid();
    }
}
