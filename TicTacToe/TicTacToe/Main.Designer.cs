﻿namespace TicTacToe
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.board = new System.Windows.Forms.PictureBox();
            this.ai_v_ai = new System.Windows.Forms.Button();
            this.human_v_ai = new System.Windows.Forms.Button();
            this.game_state = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.board)).BeginInit();
            this.SuspendLayout();
            // 
            // board
            // 
            this.board.Location = new System.Drawing.Point(12, 12);
            this.board.Name = "board";
            this.board.Size = new System.Drawing.Size(150, 150);
            this.board.TabIndex = 1;
            this.board.TabStop = false;
            this.board.Click += new System.EventHandler(this.board_Click);
            // 
            // ai_v_ai
            // 
            this.ai_v_ai.Location = new System.Drawing.Point(168, 111);
            this.ai_v_ai.Name = "ai_v_ai";
            this.ai_v_ai.Size = new System.Drawing.Size(75, 23);
            this.ai_v_ai.TabIndex = 14;
            this.ai_v_ai.Text = "AI v AI";
            this.ai_v_ai.UseVisualStyleBackColor = true;
            this.ai_v_ai.Click += new System.EventHandler(this.ai_v_ai_Click);
            // 
            // human_v_ai
            // 
            this.human_v_ai.Location = new System.Drawing.Point(168, 140);
            this.human_v_ai.Name = "human_v_ai";
            this.human_v_ai.Size = new System.Drawing.Size(75, 23);
            this.human_v_ai.TabIndex = 13;
            this.human_v_ai.Text = "Human v AI";
            this.human_v_ai.UseVisualStyleBackColor = true;
            this.human_v_ai.Click += new System.EventHandler(this.new_game_Click);
            // 
            // game_state
            // 
            this.game_state.AutoSize = true;
            this.game_state.Location = new System.Drawing.Point(168, 12);
            this.game_state.Name = "game_state";
            this.game_state.Size = new System.Drawing.Size(52, 13);
            this.game_state.TabIndex = 12;
            this.game_state.Text = "P1\'s Turn";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 169);
            this.Controls.Add(this.ai_v_ai);
            this.Controls.Add(this.human_v_ai);
            this.Controls.Add(this.game_state);
            this.Controls.Add(this.board);
            this.Name = "Main";
            this.Text = "Tic Tac Toe";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.board)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox board;
        private System.Windows.Forms.Button ai_v_ai;
        private System.Windows.Forms.Button human_v_ai;
        private System.Windows.Forms.Label game_state;
    }
}

