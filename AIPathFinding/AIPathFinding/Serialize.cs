﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace AIPathFinding
{
    class Serialize
    {
        public void SaveObject<T>(T List, string FileName)
        {
            try
            {
                //create a backup
                string backupName = Path.ChangeExtension(FileName, ".old");
                if (File.Exists(FileName))
                {
                    if (File.Exists(backupName))
                        File.Delete(backupName);
                    File.Move(FileName, backupName);
                }

                using (FileStream fs = new FileStream(FileName, FileMode.Create))
                {
                    BinaryFormatter ser = new BinaryFormatter();
                    ser.Serialize(fs, List);
                    fs.Flush();
                    fs.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static T LoadObject<T>(string FileName)
        {
            if (!File.Exists(FileName))
            {
                return default(T);
            }

            T result = default(T);

            try
            {
                using (FileStream fs = new FileStream(FileName, FileMode.Open))
                {
                    BinaryFormatter ser = new BinaryFormatter();
                    result = (T)ser.Deserialize(fs);
                }
            }
            catch
            {
            }
            return result;
        }
    }
}
