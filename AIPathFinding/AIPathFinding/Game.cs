﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;

namespace AIPathFinding
{
    //
    // Responsible for manipulating the board
    // ie. handling rules and telling the board where the peices are
    //
    class Game
    {
        Main m_form;
        Board m_board;
        Editor m_editor;
        Actor m_actor;

        System.Timers.Timer m_updateTimer;
        Point m_cursorTile;
        bool m_mouseClick = false;
        bool m_lockCursor = false;

        public Game(Main form)
        {
            m_form = form;
            m_editor = new Editor(this);
            m_updateTimer = new System.Timers.Timer();
            m_updateTimer.Elapsed += new ElapsedEventHandler(UpdateTimerElapsed);
            m_updateTimer.Interval = 1000 / 30; // 30 times per second

            StartNewGame();
        }

        public Actor GetActor()
        {
            return m_actor;
        }

        public Point GetCursorIndex()
        {
            return m_cursorTile;
        }

        public Main GetForm()
        {
            return m_form;
        }

        public void StartNewGame()
        {
            m_board = new Board(m_form.GetGamePictureBox());
            m_actor = new Actor(this);
            Draw();
            m_updateTimer.Start();
        }

        public Board GetBoard()
        {
            return m_board;
        }

        public void UpdateTimerElapsed(object source, ElapsedEventArgs e)
        {
            try
            {
                MethodInvoker mi = new MethodInvoker(Update);
                m_form.BeginInvoke(mi);
            }
            catch (Exception ex)
            {
            }
        }

        public void SetCursorLock(bool cursorLock)
        {
            m_lockCursor = cursorLock;
        }

        public void MouseClick(object sender, EventArgs e)
        {
            m_mouseClick = true;
        }

        public void Update()
        {
            bool draw = false;
            Point cursorTile = m_board.PointToTile(System.Windows.Forms.Cursor.Position);
            if (cursorTile != m_cursorTile && !m_lockCursor)
            {
                m_cursorTile = cursorTile;
                draw = true;
            }

            if (m_mouseClick)
            {
                m_mouseClick = false;
            }

            m_actor.Update();

            //if (draw)
                Draw();
        }

        public void Draw()
        {
            Graphics g = m_board.DrawBegin();
            m_board.DrawBoard(g);
            m_board.DrawCursor(g, m_cursorTile, Color.Green);
            m_actor.Draw(g);
            m_board.DrawEnd(g);
        }
    }
}
