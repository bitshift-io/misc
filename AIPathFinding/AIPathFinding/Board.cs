﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace AIPathFinding
{
    //
    // Responsible for drawing the board and storing the state of the game board
    //
    class Board
    {
        public class Tile
        {
            public Point index;

            public Tile Clone()
            {
                Tile clone = new Tile();
                clone.index = index;
                return clone;
            }

            public virtual void Draw(Graphics g, Rectangle rect)
            {
            }
        }

        public class Obstruction : Tile
        {
            public override void Draw(Graphics g, Rectangle rect)
            {
                g.FillRectangle(new SolidBrush(Color.Black), rect);
            }
        }

        public class Reward : Tile
        {
            public double reward;
        }

        int m_boardSize = 10;

        PictureBox m_board;

        Tile[,] m_tileList;

        public Board(PictureBox board)
        {
            m_board = board;
            m_tileList = new Tile[m_boardSize, m_boardSize];
        }

        public Board Clone()
        {
            Board clone = new Board(m_board);

            for (int y = 0; y < m_boardSize; ++y)
            {
                for (int x = 0; x < m_boardSize; ++x)
                {
                    Tile tile = m_tileList[x, y];
                    if (tile != null)
                    {
                        clone.SetTile(tile.Clone());
                    }
                }
            }

            return clone;
        }

        public int GetBoardSize()
        {
            return m_boardSize;
        }

        public Point GetTileFromPoint(Point point)
        {
            Point tile = new Point();
            int gridSize = m_board.Width / m_boardSize;
            tile.X = point.X / gridSize;
            tile.Y = point.Y / gridSize;
            return tile;
        }

        // Convert a point like the mouse cursor position
        // to a tile index point
        public Point PointToTile(Point point)
        {
            return GetTileFromPoint(m_board.PointToClient(point));
        }

        public bool IsValidIndex(Point index)
        {
            if (index.X < 0 || index.Y < 0)
                return false;

            if (index.X >= m_boardSize || index.Y >= m_boardSize)
                return false;

            return true;
        }

        public Tile GetTile(Point index)
        {
            if (!IsValidIndex(index))
                return null;

            return m_tileList[index.X, index.Y];
        }

        public void RemoveTile(Point index)
        {
            m_tileList[index.X, index.Y] = null;
        }

        public void SetTile(Tile tile)
        {
            m_tileList[tile.index.X, tile.index.Y] = tile;
        }

        public Graphics DrawBegin()
        {
            m_board.Image = new Bitmap(m_board.Width, m_board.Height);
            return Graphics.FromImage(m_board.Image);
        }

        public void DrawEnd(Graphics g)
        {
            g.Dispose();
            m_board.Refresh();
        }

        public void DrawBoard(Graphics g)
        {
            int gridSize = m_board.Width / m_boardSize;

            for (int y = 0; y < m_boardSize; ++y)
            {
                for (int x = 0; x < m_boardSize; ++x)
                {
                    Brush brush = null;
                    if ((y + x) % 2 == 0)
                        brush = new SolidBrush(Color.DarkGray);
                    else
                        brush = new SolidBrush(Color.Gray);

                    Point position = new Point(x, y);
                    Rectangle rect = GetRectangle(position);
                    g.FillRectangle(brush, rect);

                    DrawPeice(g, position, rect);
                }
            }
        }

        private void DrawPeice(Graphics g, Point position, Rectangle rect)
        {
            Tile tile = GetTile(position);
            if (tile != null)
                tile.Draw(g, rect);
        }

        public Rectangle GetRectangle(Point index)
        {
            int gridSize = m_board.Width / m_boardSize;
            Rectangle rect = new Rectangle(index.X * gridSize, index.Y * gridSize, gridSize, gridSize);
            return rect;
        }

        public void DrawCursor(Graphics g, Point position, Color color)
        {
            if (position == null)
                return;

            Rectangle rect = GetRectangle(position);
            Pen pen = new Pen(color);
            pen.Width = 3;
            g.DrawRectangle(pen, rect);
        }
    }
}
