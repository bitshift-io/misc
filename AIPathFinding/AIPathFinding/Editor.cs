﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace AIPathFinding
{
    class Editor
    {
        Game m_game;
        ContextMenu m_menu;
        bool m_mouseDown;
        Point m_cursorIndex;

        MenuItem m_paintObstruction;
        MenuItem m_clearObstruction;

        public Editor(Game game)
        {
            m_game = game;
            Main form = m_game.GetForm();
            PictureBox board = form.GetGamePictureBox();
            board.MouseClick += new MouseEventHandler(Board_MouseClick);
            board.MouseDown += new MouseEventHandler(Board_MouseDown);
            board.MouseUp += new MouseEventHandler(Board_MouseUp);
            board.MouseMove += new MouseEventHandler(Board_MouseMove);

            CreateContextMenu();
        }

        void CreateContextMenu()
        {
            Main form = m_game.GetForm();

            m_menu = new ContextMenu();
            m_menu.Popup += new EventHandler(Menu_Show);
            m_menu.Collapse += new EventHandler(Menu_Close);

            m_paintObstruction = new MenuItem("&Paint Obstruction", new EventHandler(Menu_PaintObstruction_Click));
            m_paintObstruction.RadioCheck = true;
            m_paintObstruction.Checked = true;
            m_menu.MenuItems.Add(m_paintObstruction);

            m_clearObstruction = new MenuItem("&Clear Obstruction", new EventHandler(Menu_ClearObstruction_Click));
            m_clearObstruction.RadioCheck = true;
            m_clearObstruction.Checked = false;
            m_menu.MenuItems.Add(m_clearObstruction);

            m_menu.MenuItems.Add("-");

            m_menu.MenuItems.Add("Set &Position", new EventHandler(Menu_SetPosition_Click));
            m_menu.MenuItems.Add("Set &Target", new EventHandler(Menu_SetTarget_Click));

            m_menu.MenuItems.Add("-");

            m_menu.MenuItems.Add("&Start", new EventHandler(Menu_Start_Click));

            form.GetGamePictureBox().ContextMenu = m_menu;
        }

        public void Menu_PaintObstruction_Click(object sender, System.EventArgs e)
        {
            m_paintObstruction.Checked = true;
            m_clearObstruction.Checked = false;
        }

        public void Menu_ClearObstruction_Click(object sender, System.EventArgs e)
        {
            m_paintObstruction.Checked = false;
            m_clearObstruction.Checked = true;
        }

        public void Board_MouseMove(object sender, System.EventArgs e)
        {
            Point cursorIndex = m_game.GetCursorIndex();
            if (cursorIndex != m_cursorIndex)
            {
                m_cursorIndex = cursorIndex;

                if (m_mouseDown)
                {
                    PerformMouseDown(m_cursorIndex);
                }
            }
        }

        void PerformMouseDown(Point cursorIndex)
        {
            Board board = m_game.GetBoard();
            Board.Tile existingTile = board.GetTile(cursorIndex);

            if (m_paintObstruction.Checked)
            {
                if (existingTile == null)
                {
                    Board.Obstruction newTile = new Board.Obstruction();
                    newTile.index = cursorIndex;
                    board.SetTile(newTile);
                }
            }
            else
            {
                if (existingTile != null && existingTile.GetType() == typeof(Board.Obstruction))
                {
                    board.RemoveTile(existingTile.index);
                }
            }
        }

        public void Board_MouseClick(object sender, System.EventArgs e)
        {
        }

        public void Board_MouseDown(object sender, System.EventArgs e)
        {
            MouseEventArgs mouseEvent = (MouseEventArgs)e;
            if (mouseEvent.Button == MouseButtons.Left)
            {
                m_mouseDown = true;
                PerformMouseDown(m_cursorIndex);
            }
        }

        public void Board_MouseUp(object sender, System.EventArgs e)
        {
            m_mouseDown = false;
        }

        public void Menu_Close(object sender, System.EventArgs e)
        {
            m_game.SetCursorLock(false);
            m_game.MouseClick(null, null);
        }

        public void Menu_Show(object sender, System.EventArgs e)
        {
            m_game.SetCursorLock(true);
            //m_game.MouseClick(sender, e);
            //Main form = m_game.GetForm();
            //m_menu.Show(
            //m_menu.Show(null/*form/*form.GetGamePictureBox()*/, System.Windows.Forms.Cursor.Position); 
        }

        void Menu_SetPosition_Click(object sender, System.EventArgs e)
        {
            Point cursorIndex = m_game.GetCursorIndex();
            //AddAction("Start (" + cursorIndex.X.ToString() + ", " + cursorIndex.Y.ToString() + ")");

            m_game.GetActor().SetPosition(cursorIndex);
        }

        void Menu_SetTarget_Click(object sender, System.EventArgs e)
        {
            Point cursorIndex = m_game.GetCursorIndex();

            m_game.GetActor().SetTarget(cursorIndex);
            //AddAction("MoveTo (" + cursorIndex.X.ToString() + ", " + cursorIndex.Y.ToString() + ")");
        }

        void Menu_Start_Click(object sender, System.EventArgs e)
        {
            m_game.GetActor().Start();
        }

        void AddAction(String action)
        {
            Main form = m_game.GetForm();
            ListBox actionList = form.GetActionList();
            actionList.Items.Add(action);
        }
    }
}
