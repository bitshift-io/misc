﻿namespace AIPathFinding
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.board = new System.Windows.Forms.PictureBox();
            this.actionlist = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.board)).BeginInit();
            this.SuspendLayout();
            // 
            // board
            // 
            this.board.Location = new System.Drawing.Point(12, 12);
            this.board.Name = "board";
            this.board.Size = new System.Drawing.Size(500, 500);
            this.board.TabIndex = 2;
            this.board.TabStop = false;
            this.board.Click += new System.EventHandler(this.board_Click);

            // 
            // actionlist
            // 
            this.actionlist.FormattingEnabled = true;
            this.actionlist.Location = new System.Drawing.Point(518, 12);
            this.actionlist.Name = "actionlist";
            this.actionlist.Size = new System.Drawing.Size(249, 498);
            this.actionlist.TabIndex = 3;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 525);
            this.Controls.Add(this.actionlist);
            this.Controls.Add(this.board);
            this.Name = "Main";
            this.Text = "AIPathFinding";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.board)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox board;
        private System.Windows.Forms.ListBox actionlist;
    }
}

