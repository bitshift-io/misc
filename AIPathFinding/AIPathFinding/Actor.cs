﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace AIPathFinding
{
    class Actor
    {
        enum Direction
        {
            Up,
            Down,
            Left,
            Right
        };

        int m_memoryLength = 0; // length of memory
        List<double[]> m_memory = new List<double[]>();
        NNet m_network;
        Game m_game;

        double m_fieldOfView = 90.0;
        int m_numRayCasts = 0; //9;

        double m_reward;

        Point m_target;
        Point m_position;
        Direction m_facing;

        Point m_initialPosition;
        Direction m_initialFacing;

        Random m_random = new Random();

        enum State
        {
            Start,
            Stop,
            Step
        }

        State m_state = State.Stop;

        public Actor(Game game)
        {
            // http://www.danielflower.com/Cache/Pictures/13856/9826-neuralnetworkvisionbasedpathfinding.pdf
            m_game = game;

            int otherInputCount = 3; // direction to objective, distance to objective
            int inputCount = (m_memoryLength + 1) * (m_numRayCasts + otherInputCount);

            int memoryOutputCount = inputCount;
            int outputCount = 1; // reward for being in this state

            // hopping that the hidden layers will divide up the inputs in to regions of the map
            // like a low resolution version of the board
            int boardSize = m_game.GetBoard().GetBoardSize();
            int lowResolutionBoard = boardSize;
            int hiddenLayerCount = lowResolutionBoard * lowResolutionBoard;

            m_network = new NNet();
            NeuronLayer[] layer = new NeuronLayer[lowResolutionBoard + 1];
            for (int i = 0; i < lowResolutionBoard; ++i)
            {
                layer[i] = new NeuronLayer(lowResolutionBoard);
            }
            layer[layer.Length - 1] = new NeuronLayer(outputCount);
            /*
            NeuronLayer[] layer = new NeuronLayer[3];
            layer[0] = new NeuronLayer(hiddenLayerCount);
            layer[1] = new NeuronLayer(hiddenLayerCount);
            layer[2] = new NeuronLayer(outputCount);
            */
            m_network.create(inputCount, layer, -0.5, 0.5); //-0.1, 0.1);
            m_network.mLearnRate = 1.0;
        }

        public void Start()
        {
            m_state = State.Start;
        }

        public void Stop()
        {
            m_state = State.Stop;
        }

        public void Step()
        {
            m_state = State.Step;
        }

        Point GetPosition(Point position, Direction direction)
        {
            Point[] directionList = new Point[] {
                new Point(0, -1),
                new Point(0, 1),
                new Point(-1, 0),
                new Point(1, 0),
                new Point(0, 0),
            };

            Point movementDirection = directionList[(int)direction];
            return new Point(position.X + movementDirection.X, position.Y + movementDirection.Y);
        }

        public void SetPosition(Point position)
        {
            m_initialFacing = m_facing;
            m_initialPosition = position;
            m_position = position;
        }

        public void SetTarget(Point target)
        {
            m_target = target;
        }

        double GetExpectedReward(Point position, int lookAhead)
        {
            Board board = m_game.GetBoard();
            if (!board.IsValidIndex(position))
                return 0.0;

            Board.Tile tile = board.GetTile(position);
            if (tile != null && tile.GetType() == typeof(Board.Obstruction))
            {
                return 0.0;
            }

            if (position == m_target)
                return 1.0;
            /*
            // look ahead return the average reward
            if (lookAhead > 0)
            {
                Array values = Enum.GetValues(typeof(Direction));
                double bestReward = 0.0;
                double totalReward = 0.0;
                //double[] expectedRewards = new double[values.Length];
                for (int i = 0; i < values.Length; ++i)
                {
                    double expectedReward = GetExpectedReward(GetPosition(position, (Direction)i), lookAhead - 1);
                    totalReward += expectedReward;
                    if (expectedReward > bestReward)
                    {
                        bestReward = expectedReward;
                    }
                }

                return totalReward / values.Length;
                //return bestReward;
            }
            */
            // ask neural net what it thinks about this position

            // TODO: we need to age nodes such that they should be explored after a certain age
            // this method will NOT explore
            double[] inputArray = GetNetworkInputs(position);
            double[] results = m_network.Compute(inputArray);
            return results[0];
            /*
            // we could look ahead here, but we shall return the best reward for this square
            double totalReward = 0.0;
            //double bestPrediction = 0.0;
            for (int i = 0; i < results.Length; ++i)
            {
                double reward = results[i];
                totalReward += reward;
                /*
                if (reward >= bestPrediction)
                {
                    bestPrediction = reward;
                }* /
            }

            return totalReward / results.Length; // bestPrediction;*/
        }

        double[] GetExpectedDirectionReward(Point position)
        {
            Array values = Enum.GetValues(typeof(Direction));
            double[] expectedRewards = new double[values.Length];
            for (int i = 0; i < values.Length; ++i)
            {
                expectedRewards[i] = GetExpectedReward(GetPosition(position, (Direction)i), 0);
            }

            return expectedRewards;
        }

        public void Age()
        {
            // age the hidden layer, do this by increasing the weights
            // in effect making the older more unexplored nodes
            // dish out more reward. If these nodes are still bad
            // the backpropagation will bring the nodes back down to reality
            NeuronLayer hiddenLayer = m_network.mLayer[1];
            for (int n = 0; n < hiddenLayer.neuron.Count(); ++n)
            {
                for (int w = 0; w < hiddenLayer.neuron[n].weight.Count(); ++w)
                {
                    hiddenLayer.neuron[n].weight[w] = Math.Min(hiddenLayer.neuron[n].weight[w] + 0.01, 1.0);
                }
            }
        }

        public double[] GetNetworkInputs(Point position)
        {
            int otherInputCount = 3; // direction to objective, distance to objective
            List<double> inputs = new List<double>(m_numRayCasts + otherInputCount);

            // do ray casting

            // these need to be in range 0 to 1!
            // add other inputs
            // add normalised direction
            // add distance
            double deltaX = m_target.X - position.X;
            double deltaY = m_target.Y - position.Y;
            double magnitude = Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
            deltaX /= magnitude;
            deltaY /= magnitude;
            deltaX = (deltaX * 0.5) + 0.5; // convert from -1>1 to 0>1
            deltaY = (deltaY * 0.5) + 0.5;
            inputs.Add(deltaX);
            inputs.Add(deltaY);

            int boardSize = m_game.GetBoard().GetBoardSize();
            double boardHypotenuse = Math.Sqrt(boardSize * boardSize + boardSize * boardSize);
            double nomalisedMagnitude = magnitude / boardHypotenuse;
            inputs.Add(nomalisedMagnitude);

            return inputs.ToArray();
        }

        public void Reset()
        {
            m_position = m_initialPosition;
            m_facing = m_initialFacing;
        }

        public void Update()
        {
            if (m_state == State.Stop)
                return;

            if (m_state == State.Step)
                m_state = State.Stop;

            // 1. perhaps instead of storing the inputs as memory, store the outputs as memory
            // this way the NNet can manipulate its own memory
            //
            // 2. perhaps we should probe each direction of movement, say: if we go left, whats our expected reward
            // instead of asking the NNet for a direction

            int otherInputCount = 3; // direction to objective, distance to objective
            int inputCount = (m_memoryLength + 1) * (m_numRayCasts + otherInputCount);


            //Age();

            List<double> inputs = new List<double>(inputCount);
            
            // add existing memory to network
            for (int i = 0; i < m_memory.Count; ++i)
            {
                inputs.AddRange(m_memory[i]);
            }

            // add the inputs for this frame
            inputs.AddRange(GetNetworkInputs(m_position));

            double[] directionRewards = GetExpectedDirectionReward(m_position);
            double highestDirectionReward = 0.0;
            for (int d = 0; d < directionRewards.Length; ++d)
            {
                highestDirectionReward = Math.Max(highestDirectionReward, directionRewards[d]);
            }

            // now compile a list of best directions
            List<Direction> bestDirectionList = new List<Direction>();
            for (int d = 0; d < directionRewards.Length; ++d)
            {
                if (directionRewards[d] >= highestDirectionReward)
                    bestDirectionList.Add((Direction)d);
            }

            // pick one of the best at random
            Direction bestDirection = bestDirectionList[m_random.Next(bestDirectionList.Count)];

            // move the reward of the current square close to the best reward of the square we are moving too
            // apply decay such that the path most recently troden becomes the worst path
            // it will ideally stay the best path if good values keep propagating from the target
            // otherwise it will cause us to explore new paths
            double[] expectedOutputs = new double[1];
            expectedOutputs[0] = highestDirectionReward * 0.5;
            m_network.Train(inputs.ToArray(), expectedOutputs);

            /*
            // predict outputs 
            //double[] inputArray = GetNetworkInputs(m_position);
            double[] predictedOutputs = m_network.Compute(GetNetworkInputs(m_position));
            //return results[results.Length - 1];

            ///double[] predictedOutputs = GetExpectedDirectionReward(m_position);

            // try reducing all ouputs
            Board board = m_game.GetBoard();
            for (int r = 0; r < predictedOutputs.Length; ++r)
            {
                Point position = GetPosition(m_position, (Direction)r);
                if (!board.IsValidIndex(position))
                {
                    predictedOutputs[r] = 0.0;
                    continue;
                }

                Board.Tile tile = board.GetTile(position);
                if (tile != null && tile.GetType() == typeof(Board.Obstruction))
                {
                    predictedOutputs[r] = 0.0;
                    continue;
                }

                if (position == m_target)
                {
                    predictedOutputs[r] = 1.0;
                    continue;
                }

           //     predictedOutputs[r] *= 0.9;
            }

            // Find best move
            // apply decay to expected reward on route we are taking
            // this causes the AI to explore. The more decay the more it will explore
            // this will also stop back tracking
            // in effect causing neural net to remember to not backtrack
            double bestPrediction = 0.0;
            Direction bestDirection = 0;
            for (int i = 0; i < predictedOutputs.Length; ++i)
            {
                double reward = predictedOutputs[i];
                if (reward >= bestPrediction && i != (int)Direction.None)
                {
                    bestPrediction = reward;
                }
            }

            // there may be multiple prediction with the same value, if so, pick one at random
            List<Direction> bestDirectionList = new List<Direction>();
            for (int i = 0; i < predictedOutputs.Length; ++i)
            {
                double reward = predictedOutputs[i];
                if (reward >= bestPrediction && i != (int)Direction.None)
                {
                    bestDirectionList.Add((Direction)i);
                }
            }

            bestDirection = bestDirectionList[m_random.Next(bestDirectionList.Count())];

            // reduce selected move even further
            if (predictedOutputs[(int)bestDirection] < 1.0)
            {
           //     predictedOutputs[(int)bestDirection] *= 0.9;
            }
            

            // update reward for None direction on this square
            double noneDirectionReward = 0.0;
            for (int i = 0; i < predictedOutputs.Length - 1; ++i)
            {
                noneDirectionReward += predictedOutputs[i];
            }
            noneDirectionReward /= (predictedOutputs.Length - 1);
            predictedOutputs[(int)Direction.None] *= noneDirectionReward;

            // get results 
            double[] inputArray = inputs.ToArray();
            double[] outputs = m_network.Train(inputArray, predictedOutputs);
            if (m_memoryLength > 0)
            {
                m_memory.Add(inputArray);
                while (m_memory.Count >= m_memoryLength)
                {
                    m_memory.RemoveAt(0);
                }
            }

            */
            // perform move
            Board board = m_game.GetBoard();
            Point movePosition = GetPosition(m_position, bestDirection);
            
            if (!board.IsValidIndex(movePosition))
                return;

            Board.Tile tile = board.GetTile(movePosition);
            if (tile != null && tile.GetType() == typeof(Board.Obstruction))
            {
                return;
            }

            m_position = movePosition;
            m_facing = bestDirection;
            
            if (m_position.Equals(m_target))
            {
                Reset();
            }
        }

        public void Draw(Graphics g)
        {
            Board board = m_game.GetBoard();

            

            


            if (m_target != null)
            {
                Rectangle rect = board.GetRectangle(m_target);
                g.FillRectangle(new SolidBrush(Color.Blue), rect);
            }

            if (m_position != null)
            { 
                Rectangle rect = board.GetRectangle(m_position);
                Point[] points = new Point[3];

                switch (m_facing)
                {
                    case Direction.Up:
                        points[0] = new Point(rect.Left, rect.Bottom);
                        points[1] = new Point(rect.Right, rect.Bottom);
                        points[2] = new Point(rect.Left + rect.Width / 2, rect.Top);
                        g.FillPolygon(new SolidBrush(Color.LimeGreen), points);
                        break;

                    case Direction.Down:
                        points[0] = new Point(rect.Left, rect.Top);
                        points[1] = new Point(rect.Right, rect.Top);
                        points[2] = new Point(rect.Left + rect.Width / 2, rect.Bottom);
                        g.FillPolygon(new SolidBrush(Color.LimeGreen), points);
                        break;

                    case Direction.Left:
                        points[0] = new Point(rect.Right, rect.Top);
                        points[1] = new Point(rect.Right, rect.Bottom);
                        points[2] = new Point(rect.Left, rect.Top + rect.Height / 2);
                        g.FillPolygon(new SolidBrush(Color.LimeGreen), points);
                        break;

                    case Direction.Right:
                        points[0] = new Point(rect.Left, rect.Top);
                        points[1] = new Point(rect.Left, rect.Bottom);
                        points[2] = new Point(rect.Right, rect.Top + rect.Height / 2);
                        g.FillPolygon(new SolidBrush(Color.LimeGreen), points);
                        break;
                        /*
                    case Direction.None:
                        g.FillEllipse(new SolidBrush(Color.LimeGreen), rect);
                        break;*/
                }
            }
            /*
            // draw expected rewards
            Font font = new Font("Arial", 8);
            double[] predictedOutputs = GetExpectedDirectionReward(m_position);
            for (int d = 0; d < predictedOutputs.Count(); ++d)
            {
                Rectangle rect = board.GetRectangle(GetPosition(m_position, (Direction)d));
                double roundedReward = Math.Round(predictedOutputs[d], 3);
                g.DrawString(roundedReward.ToString(), font, new SolidBrush(Color.White), rect.Location);
            }*/

            // draw the reward for each square
            Font font = new Font("Arial", 7);
            int boardSize = board.GetBoardSize();
            for (int y = 0; y < boardSize; ++y)
            {
                for (int x = 0; x < boardSize; ++x)
                {
                    Point positionIndex = new Point(x, y);
                    //double[] inputs = GetNetworkInputs(positionIndex);
                    //double[] outputs = m_network.Compute(inputs);
                    Rectangle rect = board.GetRectangle(positionIndex);

                    double expectedReward = GetExpectedReward(positionIndex, 0);
                    double roundedReward = Math.Round(expectedReward, 5);
                    g.DrawString(roundedReward.ToString(), font, new SolidBrush(Color.White), rect.Location);
                }
            }
        }
    }
}
