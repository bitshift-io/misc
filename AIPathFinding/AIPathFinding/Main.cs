﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AIPathFinding
{
    public partial class Main : Form
    {
        Game m_game;

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            m_game = new Game(this);
        }

        private void board_Click(object sender, EventArgs e)
        {
            m_game.MouseClick(sender, e);
        }

        public ListBox GetActionList()
        {
            return actionlist;
        }

        public PictureBox GetGraphPictureBox()
        {
            return board;
        }

        public PictureBox GetGamePictureBox()
        {
            return board;
        }
    }
}
