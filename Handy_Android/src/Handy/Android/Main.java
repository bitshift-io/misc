package Handy.Android;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;

public class Main extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    @Override 
    public boolean onKeyUp(int keyCode, KeyEvent event) 
    { 
        switch(keyCode) 
        { 
        case KeyEvent.KEYCODE_HOME: 
            finish();
            return true; 
        } 
        
        return false; 
    } 
}