package Handy.Android;

import java.util.ArrayList;
import java.util.Vector;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.*;

// http://www.tutorialforandroid.com/2009/06/drawing-with-canvas-in-android.html

//
// Area where user can draw with the cursor
//
/*
public class SketchView extends View
{
	protected int		mDownscale		= 1;		// scale of bitmap compared to the window size
	protected Bitmap 	mBitmap;
	protected Canvas	mCanvas;
	
	public class Point
	{
		Point()
		{
			
		}
		
		Point(int _x, int _y)
		{
			x = _x;
			y = _y;
		}
		
		int x;
		int y;
	}
	
	public class Line
	{
		Point[] point = new Point[2];
	}
	
	public class Circle
	{
		Point 	point	= new Point();
		int		radius;
	}
	
	MotionEvent 	mLastEvent;
	long			mLastTouchTime;
	
	Vector<Line>	mLineList = new Vector<Line>();
	Vector<Circle>	mCircleList = new Vector<Circle>();
	
	//Path			mPath = new Path();
	
	private Vector<Path> 	mGraphics = new Vector<Path>();

	private Paint 		mPaint;
	
	public SketchView(Context context) 
	{
		super(context);
	}

	public SketchView(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
	}

	public SketchView(Context context, AttributeSet attrs, int defStyle) 
	{
		super(context, attrs, defStyle);
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		for (Path path : mGraphics) 
		{

		    //canvas.drawPoint(graphic.x, graphic.y, mPaint);

		    canvas.drawPath(path, mPaint);

		  }

		
		
		// called when view is drawn
		Paint paint = new Paint();
		//paint.setFilterBitmap(true);
		
		// The image will be scaled so it will fill the width, and the
		// height will preserve the image�s aspect ration
		//double aspectRatio = ((double) mBitmap.getWidth()) / mBitmap.getHeight();
		//Rect dest = new Rect(0, 0, this.getWidth(),(int) (this.getHeight() / aspectRatio));
		//canvas.drawBitmap(mBitmap, null, dest, paint);
		
		Rect dest = new Rect(0, 0, this.getWidth(), this.getHeight());
		paint.setColor(Color.argb(255, 128, 128, 128));
		canvas.drawRect(dest, paint);
		
		int strokeWidth = 10;
		paint.setStrokeWidth(strokeWidth);
		paint.setColor(Color.argb(255, 0, 0, 0));		
		for (int i = 0; i < mLineList.size(); ++i)
		{
			Line line = mLineList.get(i);
			canvas.drawLine(line.point[0].x, line.point[0].y, line.point[1].x, line.point[1].y, paint);

			// make rounded ends
			canvas.drawCircle(line.point[0].x, line.point[0].y, strokeWidth * 0.5f, paint);
			canvas.drawCircle(line.point[1].x, line.point[1].y, strokeWidth * 0.5f, paint);
		}
		
		for (int i = 0; i < mCircleList.size(); ++i)
		{
			Circle circle = mCircleList.get(i);
			canvas.drawCircle(circle.point.x, circle.point.y, circle.radius, paint);
		}
		
		//canvas.drawPath(mPath, paint);
		//paint.
	}
	
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
		//Init();
		
		mPaint = new Paint();
		mPaint.setDither(true);
		mPaint.setColor(0xFFFFFF00);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(3);
	}
	
	Path path = null;
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//synchronized (_thread.getSurfaceHolder()) {

		
		
		    if(event.getAction() == MotionEvent.ACTION_DOWN){

		      path = new Path();

		      path.moveTo(event.getX(), event.getY());

		      path.lineTo(event.getX(), event.getY());

		    }else if(event.getAction() == MotionEvent.ACTION_MOVE){

		      path.lineTo(event.getX(), event.getY());

		    }else if(event.getAction() == MotionEvent.ACTION_UP){

		      path.lineTo(event.getX(), event.getY());

		      mGraphics.add(path);

		    }

		    invalidate();
		    return true;

		  //}
		
		/*
		int x = (int) event.getX();
		int y = (int) event.getY();
		
		x /= mDownscale;
		y /= mDownscale;
		
		int[] pixels = new int[1];
		pixels[0] = Color.rgb(0, 0, 0);
		
		mBitmap.setPixels(pixels, 0, 1, x, y, 1, 1);
		*/
		
		//Path p = new Path();
		//p.
		
		//mPath.lineTo((int) event.getX(), (int) event.getY());
		
		//mPath.
		/*
		long currentTime = System.currentTimeMillis();
		long timeSinceLastEvent = currentTime - mLastTouchTime;
		
		// create a line from last event
		if (event.getDownTime() >= timeSinceLastEvent && mLastEvent != null)
		//if (mLastEvent != null)
		{
			Line line = new Line();
			line.point[0] = new Point();
			line.point[0].x = (int) mLastEvent.getX();
			line.point[0].y = (int) mLastEvent.getY();
			
			line.point[1] = new Point();
			line.point[1].x = (int) event.getX();
			line.point[1].y = (int) event.getY();
			
			mLineList.add(line);
		}*/
		
		/*
		Circle circle = new Circle();
		circle.point = new Point((int) event.getX(), (int) event.getY());
		circle.radius = 10; //event.getSize()
		mCircleList.add(circle);*/
		/*
		mLastTouchTime = System.currentTimeMillis();
		mLastEvent = MotionEvent.obtain(event);
		*/
		//mLastTouchTime = event.getDownTime();
		//mLastTouchPoint = new Point((int)event.getX(), (int)event.getY());
		
		//if (event.getAction() != MotionEvent.ACTION_MOVE)
		//	return false;
/*
		if (event.getHistorySize() >= 1)
		{	
			Line line = new Line();
			line.point[0] = new Point();
			line.point[0].x = (int) event.getHistoricalX(event.getHistorySize() - 1);
			line.point[0].y = (int) event.getHistoricalY(event.getHistorySize() - 1);
			
			line.point[1] = new Point();
			line.point[1].x = (int) event.getX();
			line.point[1].y = (int) event.getY();
			
			mLineList.add(line);
		}*/
		
		
		/*
		for (int i = 1; i < event.getHistorySize(); ++i)
		{
			Line line = new Line();
			line.point[0] = new Point();
			line.point[0].x = (int) event.getHistoricalX(i - 1);
			line.point[0].y = (int) event.getHistoricalY(i - 1);
			
			line.point[1] = new Point();
			line.point[1].x = (int) event.getHistoricalX(i);
			line.point[1].y = (int) event.getHistoricalY(i);
			
			mLineList.add(line);
		}*/
		/*
		invalidate();
		
		super.onTouchEvent(event);
		return true;* /
	}
	

	protected void ConvertToBitmap()
	{
		try
		{		
			
			
			int width = getWidth() / mDownscale;
			int height = getHeight() / mDownscale;
			mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);
			/*
			int[] pixels = new int[width * height];
			for (int i = 0; i < pixels.length; ++i)
			{	
				pixels[i] = Color.rgb(128, 128, 128);
			}
			
			mBitmap.setPixels(pixels, 0, width, 0, 0, width, height);
			* /
			mCanvas = new Canvas();
			mCanvas.setBitmap(mBitmap);
			onDraw(mCanvas); // draw to this canvas, which will draw to the bitmap
			
			invalidate();
		}
		catch (Exception e)
		{
			System.out.println(e.toString());
		}
	}
}
*/



public class SketchView extends SurfaceView implements SurfaceHolder.Callback
{
	class DrawingThread extends Thread 
	{
	    private SurfaceHolder _surfaceHolder;
	    private SketchView _panel;
	    private boolean _run = false;
		
	    public DrawingThread(SurfaceHolder surfaceHolder, SketchView panel)
	    {
	        _surfaceHolder = surfaceHolder;
	        _panel = panel;
	    }
		
	    public void setRunning(boolean run)
	    {
	        _run = run;
	    }
		
	    public SurfaceHolder getSurfaceHolder()
	    {
	        return _surfaceHolder;
	    }
		
	    @Override
	    public void run()
	    {
	        Canvas c;
	        while (_run)
	        {
	            c = null;
	            try 
	            {
	                c = _surfaceHolder.lockCanvas(null);
	                synchronized (_surfaceHolder)
	                {
	                    _panel.onDraw(c);
	                }
	            } 
	            finally
	            {
	                // do this in a finally so that if an exception is thrown
	                // during the above, we don't leave the Surface in an
	                // inconsistent state
	                if (c != null)
	                {
	                    _surfaceHolder.unlockCanvasAndPost(c);
	                }
	            }
	        }
	    }
	}
	
	class OCRThread extends Thread
	{

		private ArrayList<Path> 	mGraphics = new ArrayList<Path>();
		private OCR					mOCR;	
		
		private SurfaceHolder _surfaceHolder; // for synchronisation
		private SketchView _panel;
		private boolean _run = false;
		
		public OCRThread(SurfaceHolder surfaceHolder, SketchView panel)
	    {
			mOCR = new OCR();
			
	        _surfaceHolder = surfaceHolder;
	        _panel = panel;
	    }
		
		public void RecogniseFrom(ArrayList<Path> graphics)
		{
			mGraphics = (ArrayList<Path>) graphics.clone();
			
			setRunning(true);
			start();
		}
		
		public void setRunning(boolean run)
	    {
	        _run = run;
	    }
		
		@Override
	    public void run()
	    {
			// look for matches,
			// return when done
			//while (_run)
	        //{
			
			Bitmap bitmap = Bitmap.createBitmap(5, 7, Bitmap.Config.ARGB_4444);
			Canvas canvas = new Canvas();
			canvas.setBitmap(bitmap);
			
			Paint paint = new Paint();
			paint.setDither(true);
			paint.setColor(0xFFFFFF00);
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeJoin(Paint.Join.ROUND);
			paint.setStrokeCap(Paint.Cap.ROUND);
			paint.setStrokeWidth(3);
			
			// try to find the least amount of paths that will be recognised as a letter
			for (int i = (mGraphics.size() - 1); i >= 0; --i)	
			{
				RectF combinedBounds = new RectF();
				for (int p = i; p < mGraphics.size(); ++p)
				{
					RectF bounds = new RectF();
					Path path = mGraphics.get(p);
					path.computeBounds(bounds, false);
					combinedBounds.union(bounds);
				}
				
				for (int p = i; p < mGraphics.size(); ++p)
				{
					Path path = mGraphics.get(p);
					canvas.drawPath(path, paint);
				}
				
				// attempt OCR image recognition
				//bitmap.
			}
	    }
	}
	
	private ArrayList<Path> 	mGraphics = new ArrayList<Path>();
	private Paint 				mPaint;
	
	private DrawingThread 		mThread;
	private Path 				mPath;
	
	private OCRThread			mOCRThread;
	
	public SketchView(Context context) 
	{
		super(context);
		Init();
	}

	public SketchView(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		Init();
	}

	public SketchView(Context context, AttributeSet attrs, int defStyle) 
	{
		super(context, attrs, defStyle);
		Init();
	}
	
	void Init() 
	{
        getHolder().addCallback(this);
        mThread = new DrawingThread(getHolder(), this);
        
        mOCRThread = new OCRThread(getHolder(), this);
        
        mPaint = new Paint();
        mPaint.setDither(true);
        mPaint.setColor(0xFFFFFF00);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(3);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
        synchronized (mThread.getSurfaceHolder())
        {
        	if(event.getAction() == MotionEvent.ACTION_DOWN)
        	{
        		mPath = new Path();
        		mPath.moveTo(event.getX(), event.getY());
        		mPath.lineTo(event.getX(), event.getY());
        	}
        	else if(event.getAction() == MotionEvent.ACTION_MOVE)
        	{
        		mPath.lineTo(event.getX(), event.getY());
        	}
        	else if(event.getAction() == MotionEvent.ACTION_UP)
        	{
        		mPath.lineTo(event.getX(), event.getY());
        		mGraphics.add(mPath);
        		
        		// when a path is complete, check for OCR matches
            	//mOCRThread.RecogniseFrom(mGraphics);
        	}
        	
        	return true;
        }
	}
	
	@Override
    public void onDraw(Canvas canvas)
	{/*
		// clear the canavs
		RectF r = new RectF();
		r.left = 0;
		r.top = 0;
		r.right = canvas.getWidth();
		r.bottom = canvas.getHeight();
		mPaint.setARGB(255, 0, 255, 0);
		mPaint.set
		canvas.drawRect(r, mPaint);
		*/
		canvas.drawARGB(255, 0, 0, 0);
		
		RectF combinedBounds = new RectF();
		for (Path path : mGraphics)
		{
			mPaint.setARGB(255, 255, 255, 255);
			canvas.drawPath(path, mPaint);
			
			//int curColour = mPaint.getColor();
			//mPaint.setARGB(255, 0, 0, 128);
			
			RectF bounds = new RectF();
			path.computeBounds(bounds, false);
			combinedBounds.union(bounds);
			//canvas.drawRect(bounds, mPaint);
			
			//mPaint.setColor(curColour);
		}
		
		mPaint.setARGB(255, 0, 0, 128);
		canvas.drawRect(combinedBounds, mPaint);
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
							   int height)
	{
	
	}
	
	public void surfaceCreated(SurfaceHolder holder)
	{
		mThread.setRunning(true);
		mThread.start();
	}
	
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		boolean retry = true;
		mThread.setRunning(false);
        while (retry) 
        {
            try 
            {
            	mThread.join();
                retry = false;
            } 
            catch (InterruptedException e) 
            {
                // we will try it again and again...
            }
        }
	}
}

