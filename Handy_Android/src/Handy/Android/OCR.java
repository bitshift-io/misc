package Handy.Android;

import java.io.File;
import java.util.Vector;

// Optical character recognition
public class OCR 
{
	static class Image
	{
		String 	name;
		int 	width = 0;
		int 	height = 0;
		int 	pixels[];
	}
	
	OCR()
	{
		mNet = new NNet();
		NNet.NeuronLayer[] layer = new NNet.NeuronLayer[2];
		layer[0] = new NNet.NeuronLayer(35);
		layer[1] = new NNet.NeuronLayer(3);
		mNet.Create(35, layer);
	}
	/*
	public void Train()
	{
		// load training data
		Vector<Vector<Image>> imageList = new Vector<Vector<Image>>();		
		for (int c = 0; c < 3; ++c)
		{
			Vector<Image> img = new Vector<Image>();
			imageList.add(img);
			
			for (int i = 0; i < 3; ++i)
			{
				img.add(LoadImage("Data_5x7/" + c + "_" + i + ".png"));
			}
		}
		
		int trainingIterations = 100;
		for (int t = 0; t < trainingIterations; ++t)
		{
			System.out.println("Training Iteration: " + t);
			
			// iterative learning
			// lets just teach this sample neural net 1 character, such that it has a single output
			for (int c = 0; c < imageList.size(); ++c)
			//int c = 0;
			{
				Vector<Image> img = imageList.get(c);
				for (int i = 0; i < img.size(); ++i)
				{
					TrainImage(img.get(i), c);
				}
			}
		}
		
		// test
		System.out.println("Image 0_0 classified as: " + ClassifyImage(imageList.get(0).get(0)));
		System.out.println("Image 1_0 classified as: " + ClassifyImage(imageList.get(1).get(0)));
		System.out.println("Image 2_0 classified as: " + ClassifyImage(imageList.get(2).get(0)));
	}
	*/
	void TrainImage(Image image, int expectedResult)
	{
		float[] inputs = new float[image.pixels.length];
		for (int i = 0; i < image.pixels.length; ++i)
		{
			//int r = (image.pixels[i] >> 16) & 0x000000ff;
			//int g = (image.pixels[i] >> 8) & 0x000000ff;
			int b = image.pixels[i] & 0x000000ff;
			
			inputs[i] = b > 0.f ? 1.f : 0.f;
		}
		
		float expectedResults[] = new float[10];
		for (int i = 0; i < expectedResults.length; ++i)
		{
			expectedResults[i] = 0.f;
			if (i == expectedResult)
				expectedResults[i] = 1.f;
		}
		
		
		float[] output = mNet.Train(inputs, expectedResults);
		
		//System.out.println("\toutput: " + output + " expected: " + expectedResult + " error: " + (expectedResult - output));
	}
	
	
	int ClassifyImage(Image image)
	{
		float[] inputs = new float[image.pixels.length];
		for (int i = 0; i < image.pixels.length; ++i)
		{
			//int r = (image.pixels[i] >> 16) & 0x000000ff;
			//int g = (image.pixels[i] >> 8) & 0x000000ff;
			//int b = image.pixels[i] & 0x000000ff;
			
			inputs[i] = image.pixels[i] > 0.f ? 1.f : 0.f;
		}
		
		//run
		float results[] = mNet.Run(inputs);
		
		// find hit with greatest chance of being our image
		int resultIndex = 0;
		for (int i = 0; i < results.length; ++i)
		{
			if (results[i] > results[resultIndex])
			{
				resultIndex = i;
			}
		}
		
		return resultIndex;
	}
	/*
	Image LoadImage(String name)
	{
		try
		{
			System.out.println("loading: " + name);
			
			File f = new File(name); //resourceDir + "/" + mName + "_tex.png");
			if (!f.exists())
				return null;
			
			int width = 0;
			int height = 0;
			int pixels[];
			
			BufferedImage bi = ImageIO.read(f);
			width = bi.getWidth();
			height = bi.getHeight();
			
			
			pixels = bi.getRGB(0, 0, width, height, null, 0, width);  
			

			for (int i = 0; i < pixels.length; ++i)
			{	
				pixels[i] = (pixels[i] == -1) ? 0 : 1;
				/*
				int r = (pixels[i] >> 16) & 0x000000ff;
				int b = pixels[i] & 0x000000ff;
				
				pixels[i] &= 0xff00ff00;
				pixels[i] |= r;
				pixels[i] |= (b << 16);* /
			}
			
			Image image = new Image();
			image.pixels = pixels;
			image.width = width;
			image.height = height;
			image.name = name;
			return image;
		}
		catch (Exception e)
		{
			
		}
		
		return null;
	}
	*/
	int				mCharacterWidth = 5;
	int				mCharacterHeight = 7;
	
	NNet 			mNet;
}
