#ifndef ___STDAFX_H_
#define ___STDAFX_H_

// memmanager
#include "Memory/NoMemory.h"

// STL
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <algorithm>

using namespace std;

// boost
#include <boost/token_iterator.hpp>
#include <boost/regex.hpp>

// win32 crap
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#define NOMINMAX
#include <windows.h>

#undef min
#undef max
//#undef GetObject
#undef GetClassInfo
#undef SendMessage

#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include "Resource.h"

#include "Template/Singleton.h"
#include "File/Log.h"

#include "Main.h"
#include "Parser.h"
#include "Class.h"
#include "Function.h"
#include "CodeBlock.h"
#include "Variable.h"
#include "Operator.h"

#endif