' Comment test
class Test <X, Y>
{
	int32 	height
	real32	width
	X		template
}

class Test2
{
	int32 	height
	real32	width
}

class Test3 : Test2
{
	int32		length
}

' Specific constructor for Test and Test2
fn Construct ((Test, Test2) t)
{
	t.height = 0
	t.width = 0.0
}

' Specific constructor for Test3
fn Construct (Test3 t)
{
	t.length = 0
	'parent is built in functionality that returns the class as its  parent type
	t.parent.Construct 
}

'
' demonstrates a simple use of class function
' first parameter is the class it takes,
' use the keyword 'class' to indicate any class
' can use this function
fn DoSomething (class t, int32 arg1 = 3)
{
	t.height += t.width
}

' demonstrates returning of multiple variables
' this also demonstrates the use of assigning
' multiple values on the same line
fn (int32 r1, int32 r2) ReturningFn (class t)
{
	r1, r2 = t.height
}

fn Main
{
	Console.Print{"Print some shit out"}

	Test t
	t.DoSomething(5)
	

	Test2 t2
	t2.DoSomething(3)

	int32 v1 = 0, v2 = 0
	v1, v2 = t.ReturningFn
}