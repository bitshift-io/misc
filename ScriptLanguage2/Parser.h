#ifndef _PARSER_H_
#define _PARSER_H_

#include "File/File.h"
#include <vector>

using namespace std;

enum CommentType
{
	CT_None,
	CT_Line,
	CT_Block,
};

class CodeBlock;
class Class;
class Function;
class Variable;
class Node;

class Parser
{
public:

	enum TokenType
	{
		TT_Unknown,
		TT_Class,
		TT_Namespace,
		TT_Function,
		TT_Variable,
		TT_EndOfFile,
		TT_BlockBegin,
		TT_BlockEnd,
		TT_If,
		TT_Else,
		TT_Return,
	};


	bool Open(const string& fileName);
	void Process(File& in);

	Variable*		EvaluateFunction(const string& className, const string& functionName, int count = 0, ...);

protected:

	Class*			FindClass(const string& name);
	Function*		FindFunction(const string& name);

	void			ReadClass(File& in, TokenType tokenType);
	void			ReadFunction(File& in);
	void			ReadVariable(File& in, Class* pClass, const string& token);
	CodeBlock*		ReadCodeBlock(File& in, Function* pFunc, CodeBlock* pParent);

	bool			IsVariable(const char* line);
	Variable*		ProcessVariable(const char* line);
	Node*			ProcessCodeBlockExpression(const char* line, CodeBlock* code);
	Node*			ProcessCodeBlockBooleanExpression(const char* line, CodeBlock* code);
	Node*			ProcessCodeBlockCondition(File& in, CodeBlock* code);

	TokenType		ReadToNextToken(File& in, string* token = 0);
	TokenType		ReadToNextToken(File& in, TokenType tokenType);
	TokenType		ReadToNextIfToken(File& in, TokenType tokenType);

	TokenType		GetToken(File& in, const char* line);

	CommentType		ProcessComment(char* line);

	void			StripBlockComment(File& in);



	vector<Class*>		mClass;
	vector<Function*>	mFunction;
};

#endif