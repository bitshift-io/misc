#include "StdAfx.h"
#include "Class.h"

Function* Class::FindFunction(const string& name)
{
	vector<Function*>::iterator it;
	for (it = mFunction.begin(); it != mFunction.end(); ++it)
	{
		if (strcmpi((*it)->mName.c_str(), name.c_str()) == 0)
			return (*it);
	}

	return 0;
}

Variable* Class::FindVariable(const string& name)
{
	vector<Variable*>::iterator it;
	for (it = mVariable.begin(); it != mVariable.end(); ++it)
	{
		if (strcmp((*it)->mName.c_str(), name.c_str()) == 0)
			return *(it);
	}

	return 0;
}