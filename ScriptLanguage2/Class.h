#ifndef _CLASS_H_
#define _CLASS_H_

class Function;
class Variable;

enum ClassType
{
	CT_Namespace,
	CT_Class,
};

class TemplateArgument
{
	string				mName;
};

class Class
{
public:

	Class()
	{
	}

	Function*			FindFunction(const string& name);
	Variable*			FindVariable(const string& name);

	ClassType					mType;
	string						mName;
	vector<Function*>			mFunction;
	vector<Variable*>			mVariable;
	vector<TemplateArgument*>	mTemplateArgument;
	vector<Class*>				mExtend;
};

#endif