#include "StdAfx.h"
#include "Function.h"

Variable* Function::Evaluate(vector<Variable*> args)
{
	// set up the variables
	for (int i = 0; i < mArgument.size(); ++i)
	{
		if (args[i])
			mArgument[i]->mCurrentValue = args[i]->mCurrentValue;
		else
			mArgument[i]->SetDefault(); //mCurrentValue = mArgument[i]->mDefaultValue;
	}

	// evaluate the code block
	mCodeBlock->Evaluate();

	// copy function args back in to passed args
	for (int i = 0; i < mArgument.size(); ++i)
	{
		if (args[i])
			args[i]->mCurrentValue = mArgument[i]->mCurrentValue;
	}

	// return
	return mReturnArgument;
}

Variable* Function::FindVariable(const string& name)
{
	vector<Variable*>::iterator it;
	for (it = mArgument.begin(); it != mArgument.end(); ++it)
	{
		if (strcmp((*it)->mName.c_str(), name.c_str()) == 0)
			return *(it);
	}

	return 0;
}