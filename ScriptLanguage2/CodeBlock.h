#ifndef _CODEBLOCK_H_
#define _CODEBLOCK_H_

class CodeBlock
{
public:

	CodeBlock() : mParent(0), mFunction(0)
	{
	}

	void				Evaluate();

	Variable*			FindVariable(const string& name);

	CodeBlock*			mParent;
	Function*			mFunction;
	string				mName;
	vector<Variable*>	mVariable;
	vector<Node*>		mNode;
};

#endif