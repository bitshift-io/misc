﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GeneticTest
{
    public class GeneticTest
    {
        Random mRandom = new Random();
        List<List<Action>> mActionLists = new List<List<Action>>();

        public void main(Form1 form)
        {
            
        }

        private List<Action> breed(List<Action> x, List<Action> y)
        {
            Random r = new Random();
            int length = r.Next(Math.Min(x.Count(), y.Count()), Math.Max(x.Count(), y.Count()));

            List<Action> combined = new List<Action>();
            for (int a = 0; a < length; ++a)
            {
                bool xOrY = r.NextDouble() > 0.5;
                Action action = null;
                if (xOrY && a < x.Count())
                   action = x.ElementAt(a);
                else if (a < y.Count())
                    action = y.ElementAt(a);

                if (action == null)
                {
                    xOrY = !xOrY;

                    if (xOrY)
                        action = x.ElementAt(a);
                    else
                        action = y.ElementAt(a);
                }

                combined.Add(action);
            }

            return combined;
        }

        public Action copy(Action action)
        {
            Action clone = new Action();
            clone.mDirection = action.mDirection;
            clone.mDistance = action.mDistance;
            return clone;
        }

        private List<Action> mutate(List<Action> x)
        {
            List<Action> mutated = new List<Action>();
            for (int a = 0; a < x.Count(); ++a)
            {
                Action action = copy(x.ElementAt(a));
                mutated.Add(action);
            }

            int actionMutations = mRandom.Next(1, 5);
            for (int i = 0; i < actionMutations; ++i)
            {
                int index = mRandom.Next(1, mutated.Count() - 1);

                // mutations can remove or add new elements or cause direction/length changes
                bool add = (mRandom.NextDouble() < 0.2);
                bool delete = (mRandom.NextDouble() < 0.2);
                bool adjustDirection = (mRandom.NextDouble() < 0.2);

                try
                {

                    if (delete)
                    {
                        mutated.RemoveAt(index);
                    }
                    else if (adjustDirection)
                    {
                        Action action = x.ElementAt(index);
                        action.mDirection += (mRandom.NextDouble() - 0.5) * 10; // += a few degrees
                    }
                    else if (add)
                    {
                        mutated.Insert(index, generateRandomAction());
                    }

                }
                catch (System.ArgumentOutOfRangeException e)
                {
                    int nothing = 0;
                    ++nothing;
                }
            }

            return mutated;
        }

        private int sortByFitness(List<Action> x, List<Action> y)
        {
            double xFitness = evaluateFitness(x);
            double yFitness = evaluateFitness(y);

            double fitness = xFitness - yFitness;
            return (int)fitness;
        }

        public void paint(Form1 form)
        {
            int topBreedCount = 5; // adds 3 kids per top breed
            int max = 30;
            while (mActionLists.Count() < max)
            {
                mActionLists.Add(generateRandomList(10, 100));
            }


            /*
            List<double> fitnessBefore = new List<double>();
            for (int i = 0; i < mActionLists.Count(); ++i)
            {
                fitnessBefore.Add(evaluateFitness(mActionLists.ElementAt(i)));
            }*/

            mActionLists.Sort(sortByFitness);

            /*
            List<double> fitnessAfter = new List<double>();
            for (int i = 0; i < mActionLists.Count(); ++i)
            {
                fitnessAfter.Add(evaluateFitness(mActionLists.ElementAt(i)));
            }*/


            // draw
            System.Drawing.Graphics graphicsObj;
            graphicsObj = form.CreateGraphics();
            graphicsObj.Clear(System.Drawing.Color.Gray);


            Pen targets = new Pen(System.Drawing.Color.Green, 3);
            graphicsObj.DrawArc(targets, new Rectangle(500, 500, 10, 10), 0, 360);

            graphicsObj.DrawArc(targets, new Rectangle(500, 200, 10, 10), 0, 360);

            Pen myPen = new Pen(System.Drawing.Color.Red, 3);

            // only bother drawing the top breed
            for (int i = 0; i < topBreedCount * 3; ++i)
            {
                List<Action> actionList = mActionLists.ElementAt(i);

                int col = i % 4;
                switch (col)
                {
                    case 0:
                        myPen.Color = System.Drawing.Color.Red;
                        break;

                    case 1:
                        myPen.Color = System.Drawing.Color.Blue;
                        break;

                    case 2:
                        myPen.Color = System.Drawing.Color.Yellow;
                        break;

                    case 3:
                        myPen.Color = System.Drawing.Color.Black;
                        break;
                }

                paintActionList(actionList, graphicsObj, myPen);
            }

            form.Invalidate();
            System.Threading.Thread.Sleep(100);
            // end draw

            List<List<Action>> oldActionLists = mActionLists;
            mActionLists = new List<List<Action>>();

            List<Action> fitestList = oldActionLists.ElementAt(0);
            double fittest = evaluateFitness(fitestList);
            Console.Write("fitest: " + fittest + "\n");

            for (int i = 0; i < topBreedCount; ++i)
            {
                int x = i;
                int y = i + 1;

                List<Action> first = oldActionLists.ElementAt(x);
                List<Action> second = oldActionLists.ElementAt(y);

                List<Action> child = breed(first, second);
                mActionLists.Add(child);

                // add a mutation of the child as well, 2 kids!
                mActionLists.Add(mutate(first));

                // add parent incase its better
                mActionLists.Add(first);
            }
        }

        void paintActionList(List<Action> actionList, System.Drawing.Graphics graphicsObj, Pen pen)
        {
            double xCurrent = 500;
            double yCurrent = 500;

            foreach (Action action in actionList)
            {
                double xNext = xCurrent;
                double yNext = yCurrent;

                xNext += Math.Sin((Math.PI / 180.0) * action.mDirection) * action.mDistance;
                yNext += Math.Cos((Math.PI / 180.0) * action.mDirection) * action.mDistance;

                graphicsObj.DrawLine(pen, (int)xCurrent, (int)yCurrent, (int)xNext, (int)yNext);

                xCurrent = xNext;
                yCurrent = yNext;
            }
        }

        double evaluateFitness(List<Action> actionList)
        {
            double xCurrent = 500;
            double yCurrent = 500;

            foreach (Action action in actionList)
            {
                double xNext = xCurrent;
                double yNext = yCurrent;

                xNext += Math.Sin((Math.PI / 180.0) * action.mDirection) * action.mDistance;
                yNext += Math.Cos((Math.PI / 180.0) * action.mDirection) * action.mDistance;

                xCurrent = xNext;
                yCurrent = yNext;
            }

            double xDeltaFromTarget = 500.0 - xCurrent;
            double yDeltaFromTarget = 200.0 - yCurrent;

            double distanceFitness = Math.Sqrt(xDeltaFromTarget * xDeltaFromTarget + yDeltaFromTarget * yDeltaFromTarget);

            // shorter action lists are fitter
            double lengthFitness = actionList.Count() * 2;

            // as we approach the target, length becomes more important
            double finalFitness = distanceFitness + lengthFitness / distanceFitness;

            return finalFitness;
        }

        Action generateRandomAction()
        {
            Action action = new Action();

            action.mDistance = 10;
            action.mDirection = mRandom.NextDouble() * 360.0;
            return action;
        }

        List<Action> generateRandomList(int minLength, int maxLength)
        {
            int length = mRandom.Next(minLength, maxLength); 
            List<Action> actionList = new List<Action>();

            for (int a = 0; a < length; ++a)
            {
                actionList.Add(generateRandomAction());
            }

            return actionList;
        }

    }
}
