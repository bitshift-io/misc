#include "Kernel.h"

__declspec(naked) void NdosEntryPoint(void)
{
	__asm 
	{
	multiboot_header:
		dd(0x1BADB002)               ; magic
		dd(1 << 16)                    ; flags
		dd(-(0x1BADB002 + (1 << 16)))     ; checksum
		dd(0x00101000)               ; header_addr
		dd(0x00101000)               ; load_addr
		dd(0x0010200F)               ; load_end_addr
		dd(0x0010200F)               ; bss_end_addr
		dd(0x00101030)               ; entry_addr
		dd(0x00000000)               ; mode_type
		dd(0x00000000)               ; width
		dd(0x00000000)               ; height
		dd(0x00000000)               ; depth

	kernel_entry:
		mov     esp,     KERNEL_STACK

		xor     ecx,     ecx
		push     ecx
		popf

		push     eax
		push     ebx
		call     main

		jmp     $
	}
}

void main(unsigned long magic, unsigned long addr)
{
	char *string = "Hello World!", *ch;
	unsigned short *vidmem = (unsigned short *) 0xB8000;
	int i;

	for(ch = string, i = 0; *ch; ch++, i++)
		vidmem[i] = (unsigned char) *ch | 0x0700;
}