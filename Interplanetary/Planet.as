﻿package 
{

	import flash.display.MovieClip;
	import flash.display.Sprite;


	public class Planet extends Entity
	{
		var mass : Number;
		
		//var circle:Sprite = new Sprite();

		public function Planet(xPercent : Number, yPercent : Number, scale : Number)
		{
			// add to entity list
			var game:Game = Game.Get();
			game.entity.push(this);
			
			mMovieClip = new Planet_00();
			game.stage.addChild(mMovieClip);

			mMovieClip.x = game.stage.stageWidth * xPercent;
			mMovieClip.y = game.stage.stageHeight * yPercent;
			mMovieClip.width = scale;
			mMovieClip.height = scale;
			
			mCollisionRadius = scale * 0.5; // hrmm stupid flash, why cant width be equivalent to radius?!

			mass = scale * 0.4;
			
			// debug
			//circle.graphics.beginFill(0xFF794B);
			//circle.graphics.drawCircle(x, y, width);
			//circle.graphics.endFill();
			//game.stage.addChild(circle);
		}

		override public function Update() : void
		{
			//this.width = 100;
			//this.height = 100;
			
			//circle.graphics.clear();
			//circle.graphics.beginFill(0xFF794B);
			//circle.graphics.drawCircle(x, y, width);
			//circle.graphics.endFill();
			
			//mMovieClip.rotation += 2;
		}
		
		override public function IsCollisionEnabled() : Boolean
		{
			return true;
		}
		
		public function ModifyVelocity(velocity : Vector2, position : Vector2, _mass : Number) : Vector2
		{			
			var thisPosition : Vector2 = GetPosition();
			var distance : Number = thisPosition.DistanceTo(position);
			
			trace("0"); // this fixes a stack overflow error somehow o.0?
			
			var distanceSqrd : Number = distance * distance;
			var force : Number = mass * _mass / distanceSqrd;

			var directionToPlanet : Vector2 = thisPosition.Subtract(position);
			directionToPlanet.Normalize();
			
			var gravityToPlanet : Vector2 = directionToPlanet.Multiply(force);
			
			var newVelocity : Vector2 = velocity.Add(gravityToPlanet);
			return newVelocity;
		}
	}

}