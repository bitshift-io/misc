﻿package
{
	import flash.display.Stage;
	
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	public class KeyHelper
	{
		// http://tutorials.flashmymind.com/2009/02/actionscript-3-keyboard-events/
		static var KEY_DOWN		: int = 40;
		static var KEY_UP		: int = 38;
		static var KEY_LEFT		: int = 37;
		static var KEY_RIGHT	: int = 39;
		static var KEY_SPACE	: int = 32;
		
		static var KEY_CTRL		: int = 17;
		static var KEY_Q		: int = 81;
		static var KEY_W		: int = 87;
		static var KEY_S		: int = 83;
		static var KEY_A		: int = 65;
		static var KEY_D		: int = 68;
		
		static var KEY_I		: int = 73;
		static var KEY_K		: int = 75;
		static var KEY_J		: int = 74;
		static var KEY_L		: int = 76;
		static var KEY_U		: int = 85;
		
		static var KEY_FWDSLASH		: int = 191;
				
		var key		: int;
		var down 	: Boolean	=	false;
		
		public function KeyHelper(a_key : int, stage : Stage)
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN, KeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, KeyUp);
			key = a_key;
		}
		
		function KeyDown(event : KeyboardEvent)
		{
			if (event.keyCode == key)
				down = true;
		}
		
		function KeyUp(event : KeyboardEvent)
		{
			if (event.keyCode == key)
				down = false;
		}
	}
}
