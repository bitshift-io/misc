﻿package
{
	import flash.display.Stage;
	import flash.events.Event;
	import flash.display.Scene;
	import flash.display.MovieClip;

	public class Game
	{
		static var 	game : Game;

		var 		stage 	: Stage;
		
		var reset : KeyHelper;
		var	entity = new Array();	
		
		static public function Get()
		{
			return game;
		}
		
		public function Game(s : Stage)
		{
			game = this;
			stage = s;
						
			reset = new KeyHelper(KeyHelper.KEY_SPACE, stage);
			
			
			var planet0 : Planet = new Planet(0.5, 0.5, 120);
			var planet1 : Planet = new Planet(0.2, 0.8, 50);
			var planet2 : Planet = new Planet(0.7, 0.2, 50);
			var planet3 : Planet = new Planet(0.2, 0.5, 100);
			var planet4 : Planet = new Planet(0.8, 0.8, 100);
			
			var actor0 : Actor = new Actor();
			var actor1 : Actor = new Actor();		
			var actor2 : Actor = new Actor();		
		}
						
		public function Update() : void 
		{
			var i : int;
			
			if (reset.down)
			{
				for (i = 0; i < entity.length; ++i)
				{
					entity[i].Reset();
				}
			}
			
			for (i = 0; i < entity.length; ++i)
			{
				entity[i].Update();
			}
			
			// perform collision detection
			for (i = 0; i < entity.length; ++i)
			{
				if (!entity[i].IsCollisionEnabled())
					continue;
				
				for (var k : int = i + 1; k < entity.length; ++k)
				{
					if (!entity[k].IsCollisionEnabled())
						continue;
					
					if (CheckCollisionBetweenEntity(entity[i], entity[k]))
					{
						entity[i].NotifyCollision(entity[k]);
						entity[k].NotifyCollision(entity[i]);
					}
				}
			}
		}
		
		public function CheckCollisionBetweenEntity(a : Entity, b : Entity) : Boolean
		{
			var aPos : Vector2 = a.GetPosition();
			var bPos : Vector2 = b.GetPosition();
			
			// distance between
			var distanceSqrd = aPos.DistanceToSqrd(bPos);
			var radiusSqrd = (a.mCollisionRadius * a.mCollisionRadius) + (b.mCollisionRadius * b.mCollisionRadius);
			
			if (distanceSqrd < radiusSqrd)
				return true;
				
			return false;
		}
		
		// helper functionality
		public function ApplyGravity(velocity : Vector2, position : Vector2, mass : Number) : Vector2
		{
			for (var i : int = 0; i < entity.length; ++i)
			{
				var obj : Object = entity[i];
				if (obj is Planet)
				{
					var planet : Planet = obj as Planet;
					velocity = planet.ModifyVelocity(velocity, position, mass);
				}
			}
			
			return velocity;
		}
		
		function ScreenWrap(movieClip : MovieClip) : Boolean
		{
			var wrap : Boolean = false;
			
			// screen wrap around
			if (movieClip.x > stage.stageWidth)
			{
				movieClip.x -= stage.stageWidth;
				wrap = true;
			}
				
			if (movieClip.x < 0)
			{
				movieClip.x += stage.stageWidth;
				wrap = true;
			}
				
			if (movieClip.y > stage.stageHeight)
			{
				movieClip.y -= stage.stageHeight;
				wrap = true;
			}
				
			if (movieClip.y < 0)
			{
				movieClip.y += stage.stageHeight;
				wrap = true;
			}
				
			return wrap;
		}
	}
}
