﻿package
{
	import flash.display.MovieClip;
	
	public class Entity
	{
		var mMovieClip 			: MovieClip;
		var mCollisionRadius 	: Number;
		
		public function Entity()
		{

		}
		
		public function Update() : void
		{

		}
		
		public function Reset() : void
		{

		}
		
		public function GetPosition() : Vector2
		{
			return new Vector2(mMovieClip.x, mMovieClip.y);
		}
		
		public function NotifyCollision(other : Entity) : void
		{
			
		}
		
		public function IsCollisionEnabled() : Boolean
		{
			return false;
		}
	}
}
