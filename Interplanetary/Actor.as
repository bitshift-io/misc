﻿package 
{
	import flash.display.MovieClip;
	import flash.events.Event;

	import KeyHelper;

	public class Actor extends Entity
	{
		static var STATE_ALIVE : int = 0;
		static var STATE_DEAD : int = 1;
		
		static var ACTOR_INDEX : int = 0;
		
		
		
		// constants
		static var ROTATION_SPEED : Number = 1;
		static var FORWARD_SPEED : Number = 0.2;
		static var REVERSE_SPEED : Number = 0.1;
		static var MIN_SHOOT_FRAME : Number = 20;
		
		
		// keys
		var left : KeyHelper;
		var right : KeyHelper;
		var up : KeyHelper;
		var down : KeyHelper;
		var shoot : KeyHelper;

		// movement		
		var velocity : Vector2 = new Vector2();
		
		// shooting
		var lastShootFrame : Number = 0;
		
		var mass : Number;
		var state : int;
		
		var mActorIndex;

		//var circle:Sprite = new Sprite();
		
		// shield
		var mShieldMovieClip 			: MovieClip;
		var mShieldHealth				: int;
		var mShieldRegenTime			: int;
			 
		public function Actor()
		{			
			var game : Game = Game.Get();	
			
			state = STATE_DEAD;
			
			mActorIndex = ACTOR_INDEX;
			++ACTOR_INDEX;
			
			mShieldMovieClip = new Shield_00();
			mShieldMovieClip.width = 40;
			mShieldMovieClip.height = 40;		
			
			if (mActorIndex == 0)
			{
				mMovieClip = new Actor_00();
				Reset();
								
				// set up keys
				left = new KeyHelper(KeyHelper.KEY_LEFT, mMovieClip.stage);
				right = new KeyHelper(KeyHelper.KEY_RIGHT, mMovieClip.stage);
				up = new KeyHelper(KeyHelper.KEY_UP, mMovieClip.stage);
				down = new KeyHelper(KeyHelper.KEY_DOWN, mMovieClip.stage);
				shoot = new KeyHelper(KeyHelper.KEY_FWDSLASH, mMovieClip.stage);
			}
			else if (mActorIndex == 1)
			{
				mMovieClip = new Actor_01();
				Reset();
								
				// set up keys
				left = new KeyHelper(KeyHelper.KEY_A, mMovieClip.stage);
				right = new KeyHelper(KeyHelper.KEY_D, mMovieClip.stage);
				up = new KeyHelper(KeyHelper.KEY_W, mMovieClip.stage);
				down = new KeyHelper(KeyHelper.KEY_S, mMovieClip.stage);
				shoot = new KeyHelper(KeyHelper.KEY_Q, mMovieClip.stage);
			}
			else if (mActorIndex == 2)
			{
				mMovieClip = new Actor_02();
				Reset();
								
				// set up keys
				left = new KeyHelper(KeyHelper.KEY_J, mMovieClip.stage);
				right = new KeyHelper(KeyHelper.KEY_L, mMovieClip.stage);
				up = new KeyHelper(KeyHelper.KEY_I, mMovieClip.stage);
				down = new KeyHelper(KeyHelper.KEY_K, mMovieClip.stage);
				shoot = new KeyHelper(KeyHelper.KEY_U, mMovieClip.stage);
			}
			
			
			mMovieClip.width = 20;
			mMovieClip.height = 20;		
				
			
			
			// add to entity list
			game.entity.push(this);
			
			//
			//game.stage.addChild(this);
			
			//this.x = game.stage.stageWidth * 0.2;
			//this.y = game.stage.stageHeight * 0.2;
			//this.width = 20;
			//this.height = 20;		
			
			mCollisionRadius = 20;
			
			//this.trans
			
			mass = 10;
			
			
			// debug
			//circle.graphics.beginFill(0xFF794B);
			//circle.graphics.drawCircle(x, y, width);
			//circle.graphics.endFill();
			//game.stage.addChild(circle);
			
			Spawn();
		}
		
		public function Spawn() : void
		{
			var game : Game = Game.Get();
			
			var i : int = 0;
			var planetCount : int = 0;
			for (i = 0; i < game.entity.length; ++i)
			{
				if (game.entity[i] is Planet)
				{
					planetCount++;
				}
			}
			
			var spawnPlanet : int = Math.random() * planetCount;
			planetCount = 0;
			for (i = 0; i < game.entity.length; ++i)
			{
				if (game.entity[i] is Planet)
				{
					if (planetCount == spawnPlanet)
					{
						var angle : int = Math.random() * 360;
						
						var s : Number = Math.sin(angle * (Math.PI / 180));
						var c : Number = -Math.cos(angle * (Math.PI / 180));
				
						var planet : Planet = game.entity[i] as Planet;
						var planetPos : Vector2 = planet.GetPosition();
						mMovieClip.x = planetPos.x + s * (planet.mCollisionRadius + mCollisionRadius);
						mMovieClip.y = planetPos.y + c * (planet.mCollisionRadius + mCollisionRadius);
						return;
					}
					
					planetCount++;
				}
			}
		}
		
		override public function Reset() : void
		{
			var game : Game = Game.Get();
			
			if (mActorIndex == 0)
			{
				mMovieClip.x = game.stage.stageWidth * 0.2;
				mMovieClip.y = game.stage.stageHeight * 0.2;
			}
			else if (mActorIndex == 1)
			{
				mMovieClip.x = game.stage.stageWidth * 0.8;
				mMovieClip.y = game.stage.stageHeight * 0.8;
			}
			
			if (state == STATE_DEAD)
			{
				game.stage.addChild(mMovieClip);
			}
			
			if (mShieldHealth <= 0)
			{
				game.stage.addChild(mShieldMovieClip);
			}
			
			state = STATE_ALIVE;
			
			velocity.x = 0;
			velocity.y = 0;
			
			mShieldHealth = 1;
			Spawn();
		}
		
		override public function IsCollisionEnabled() : Boolean
		{
			return state != STATE_DEAD;
		}
	
		override public function Update() : void
		{
			if (state == STATE_DEAD)
				return;
				
			var game : Game = Game.Get();
			
			//circle.graphics.clear();
			//circle.graphics.beginFill(0xFF794B);
			//circle.graphics.drawCircle(x, y, width);
			//circle.graphics.endFill();
						

			if (left.down)
			{
				mMovieClip.rotation -=  ROTATION_SPEED;
			}

			if (right.down)
			{
				mMovieClip.rotation +=  ROTATION_SPEED;
			}
/*
			if (up.down || down.down)
			{
				var s:Number;
				var c:Number;

				// convert degrees to radians
				s = Math.sin(mMovieClip.rotation * (Math.PI / 180));
				c = -Math.cos(mMovieClip.rotation * (Math.PI / 180));

				if (up.down)
				{
					velocity.x +=  s * FORWARD_SPEED;
					velocity.y +=  c * FORWARD_SPEED;
				}

				if (down.down)
				{
					velocity.x -=  s * REVERSE_SPEED;
					velocity.y -=  c * REVERSE_SPEED;
				}
			}*/
						
			//velocity = game.ApplyGravity(velocity, GetPosition(), mass);
						
			mMovieClip.x +=  velocity.x;
			mMovieClip.y +=  velocity.y;
			
			game.ScreenWrap(mMovieClip);
			
			// update shield
			var prevShieldRegenTime = mShieldRegenTime;
			--mShieldRegenTime;
			if (mShieldRegenTime <= 0 && prevShieldRegenTime > 0)
			{
				mShieldHealth = 1;
				game.stage.addChild(mShieldMovieClip);
			}
			
			mShieldMovieClip.rotation += 5;
			mShieldMovieClip.x = mMovieClip.x;
			mShieldMovieClip.y = mMovieClip.y;
			
			// shoot

			
			++lastShootFrame;
			if (shoot.down && lastShootFrame > MIN_SHOOT_FRAME)
				Shoot();
		}
				
		override public function NotifyCollision(other : Entity) : void
		{
			TakeDamage(other);
		}
		
		function TakeDamage(other : Entity) : void
		{
			var shieldRegenTime : int = 250;
			var game : Game = Game.Get();
			
			//if (state == STATE_DEAD)
				//return;
				

			if (other is Planet)
				return;
			
			// bounce off non bullets if we are still alive after the impact
			if (!(other is Bullet))
			{
				velocity.x = -velocity.x;
				velocity.y = -velocity.y;
			}
			
			if (mShieldHealth <= 0)
			{
				state = STATE_DEAD;
				game.stage.removeChild(mMovieClip);
			}
			else
			{
				mShieldRegenTime = shieldRegenTime;
				mShieldHealth -= 1;
				
				if (mShieldHealth <= 0)
					game.stage.removeChild(mShieldMovieClip);
			}
		}
						
		function Shoot() : void
		{
			lastShootFrame = 0;
			
			var bullet : Bullet;
			
			var game : Game = Game.Get();
			for (var i : int = 0; i < game.entity.length; ++i)
			{
				var obj : Object = game.entity[i];
				if (obj is Bullet)
				{
					bullet = game.entity[i];
					if (!bullet.IsAlive())
					{
						bullet.Shoot(this);
						return;
					}
				}
			}
			
			// no bullets found, make a new one
			bullet = new Bullet();
			bullet.Shoot(this);
		}
	}

}