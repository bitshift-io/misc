﻿package
{
	
	import flash.display.MovieClip;
	import Actor;
	
	
	public class Bullet extends Entity 
	{				
		static var STATE_ALIVE : int = 0;
		static var STATE_DEAD : int = 1;
		
		// movement		
		var velocity : Vector2 = new Vector2();
		var lastFramePosition : Vector2;
		
		var state : int;
		
		var mass : Number;
		
		var mOwner : Actor;
		
		var mTrail : LineTrail = new LineTrail();
				
		public function Bullet()
		{
			// add to entity list
			var game : Game = Game.Get();
			game.entity.push(this);
					
			mMovieClip = new Bullet_00();
			//game.stage.addChild(mMovieClip);
				
				
			
			mMovieClip.width = 5;
			mMovieClip.height = 5;	
			
			mCollisionRadius = 5;
			
			mass = 250; // just to make this more effected by gravity than actors
			
			state = STATE_ALIVE;
			
		}
		
		override public function Reset() : void
		{
			if (state == STATE_DEAD)
				return;
				
			state = STATE_DEAD;
			var game : Game = Game.Get();
			game.stage.removeChild(mMovieClip);
			mTrail.SetEnabled(false);
			mTrail.Reset();
		}
				
		public function Shoot(actor : Actor) : void
		{
			mOwner = actor;
			
			var bulletSpeed : Number = 15;
			var bulletLife : Number = 120;
			
			var game : Game = Game.Get();
			
			mMovieClip.rotation = actor.mMovieClip.rotation;
			
			var s:Number = Math.sin(mMovieClip.rotation * (Math.PI / 180));
			var c:Number = -Math.cos(mMovieClip.rotation * (Math.PI / 180));
			
			mMovieClip.x = actor.mMovieClip.x + s * (actor.mCollisionRadius + mCollisionRadius);
			mMovieClip.y = actor.mMovieClip.y + c * (actor.mCollisionRadius + mCollisionRadius);
			
			
			mMovieClip.width = 5;
			mMovieClip.height = 5;	
			
			velocity.Copy(actor.velocity);
			

			velocity.x += s * bulletSpeed;
			velocity.y += c * bulletSpeed;
			
			state = STATE_ALIVE;
			game.stage.addChild(mMovieClip);
			mTrail.SetEnabled(true);
					
			if (actor.mActorIndex == 0)
				mTrail.SetColour(0x00880000);
			else if (actor.mActorIndex == 1)
				mTrail.SetColour(0x00008800);
			else// (actor.mActorIndex == 2)
				mTrail.SetColour(0x00000088);
			
			lastFramePosition = GetPosition();
		}
		
		override public function IsCollisionEnabled() : Boolean
		{
			return IsAlive();
		}
		
		public function IsAlive() : Boolean
		{
			return state == STATE_ALIVE;
		}
		
		override public function Update() : void
		{
			var game : Game = Game.Get();
			
			//var prevLife : Number  = life;
			//--life;
			//if (life <= 0 && prevLife > 0)
			//{
				//game.stage.removeChild(mMovieClip);
			//}
			
			if (state == STATE_DEAD)
				return;
						
						
			// apply acceleration
			var bulletAcceleration : Number = 0.1;
			var s:Number = Math.sin(mMovieClip.rotation * (Math.PI / 180));
			var c:Number = -Math.cos(mMovieClip.rotation * (Math.PI / 180));
			velocity.x += s * bulletAcceleration;
			velocity.y += c * bulletAcceleration;
			
			velocity = game.ApplyGravity(velocity, GetPosition(), mass);
						
			mMovieClip.x += velocity.x;
			mMovieClip.y += velocity.y;
			
			if (game.ScreenWrap(mMovieClip))
				mTrail.Reset();
		
			// calculate rotation for this actor based on position last frame and position this frame
			var position : Vector2 = GetPosition();
			var direction = position.Subtract(lastFramePosition);
			direction.Normalize();
			
			var up : Vector2 = new Vector2(0, -1);
			var rotationRadians = direction.AngleBetween(up);
			
			// rotation is only from 0 to 180
			if (direction.x < 0)
				rotationRadians = -rotationRadians;
			
			var rotationDegrees = rotationRadians * (180 / Math.PI);
			mMovieClip.rotation = rotationDegrees;
			
			mTrail.Update(position);
			
			lastFramePosition = position;
		}
		
		override public function NotifyCollision(other : Entity) : void
		{
			//if (life <= 0)
				//return;
			
			//if (other is Bullet)
			//{
				//var bullet : Bullet = other as Bullet;
				//if (!bullet.IsAlive())
					//return;
			//}
			
			var game : Game = Game.Get();
			state = STATE_DEAD;
			game.stage.removeChild(mMovieClip);
			mTrail.SetEnabled(false);
			mTrail.Reset();
			//if (other is Actor && other != mOwner)
			//{
				//var actor : Actor = other;
				//if (other.state != Actor.STATE_DEAD)
				//{
					// award score to mOwner
				//}
			//}
		}
	}
	
}
