﻿using UnityEngine;
using System.Collections;

public class PlayerSpawn : MonoBehaviour 
{
	public bool enabled = true;
	
	// pre init
	void Awake()
	{
		//spawn a player spawn, and drop on the needed scripts
		if (isEnabled())
		{
			// add the building to the player object list
			GetComponent<PlayerObjects>().Add(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}
	
	// Use this for initialization
	void Start() 
	{

	}
	
	// Update is called once per frame
	void Update() 
	{
	}
	
	public bool isEnabled()
	{
		return enabled;
	}
}
