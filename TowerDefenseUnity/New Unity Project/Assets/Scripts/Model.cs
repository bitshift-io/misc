﻿using UnityEngine;
using System.Collections;

public class Model : MonoBehaviour 
{
	
	public GameObject model;
	GameObject m_instance;
	
	void Awake()
	{
		if (model)
		{
			m_instance = (GameObject) GameObject.Instantiate(model, gameObject.transform.position, gameObject.transform.rotation);
			m_instance.transform.parent = gameObject.transform;
		}
	}
	
	// Use this for initialization
	void Start() 
	{
	
	}
	
	// Update is called once per frame
	void Update() 
	{
	}
	
	public void SetModel(GameObject obj)
	{
		model = obj;
		Awake();
	}
	
	public GameObject Instance()
	{
		return m_instance;
	}
}
