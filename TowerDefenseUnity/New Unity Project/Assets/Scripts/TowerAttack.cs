﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// controls behavious for automatic attacking
public class TowerAttack : MonoBehaviour 
{
	public float maxTargetRadius = 2.0f;
	public float minTargetRadius = 0.1f;
	public float reloadTime = 2.0f;
	public float targetSpeed = 1.0f;
	public GameObject projectile;
	
	float m_lastTime = 0f;
	
	GameObject m_lastTarget;

	// Use this for initialization
	void Start() 
	{
	}


	// Update is called once per frame (change to FixedUpdate?)
	void Update() 
	{
		m_lastTime += Time.deltaTime;
		
		if (m_lastTime < reloadTime)
			return;
		
		// is last target still in range?
		if (m_lastTarget && InRange(m_lastTarget))
		{
			AttackTarget(m_lastTarget);
			return;
		}
			
		// get a list of all enemys, find the closest, and attack that
		List<GameObject> enemys = GetTeamEnemys();
		
		if (enemys.Count == 0)
			return;
		
		GameObject close = GetClosest(enemys);
		if (!close)
			return;
		
		AttackTarget(close);
		
		m_lastTime = 0f;
	}
	
	void AttackTarget(GameObject obj)
	{
		GameObject proj = (GameObject) GameObject.Instantiate(projectile, gameObject.transform.position, gameObject.transform.rotation);
		proj.GetComponent<Projectile>().FireAtTarget(gameObject, obj);
		
		/*
		// Assign resource by damage!
		float value = obj.GetComponent<ValueManager>().Value();
		GameObject player = gameObject.GetComponent<TeamManager>().Owner();
		player.GetComponent<ResourceManager>().AddResource(value);
		
		// damage after we have our goodies
		obj.GetComponent<Health>().ApplyDamage(20);
		*/
	}
		
	// checks if its within attack range
	bool InRange(GameObject obj)
	{
		Vector3 diff = this.transform.position - obj.transform.position;
		float distance = diff.magnitude;
		
		if (distance < maxTargetRadius && distance > minTargetRadius)
			return true;
		
		return false;
	}
	
	// gets closest object
	GameObject GetClosest(List<GameObject> objectList)
	{
		GameObject closestObject = null;
		float closestDistance = maxTargetRadius;
		
		foreach(GameObject obj in objectList) 
		{
			Vector3 diff = this.transform.position - obj.transform.position;
			float distance = diff.magnitude;
			if (distance < maxTargetRadius && distance > minTargetRadius)
			{
				closestDistance = distance;
				closestObject = obj;
			}
		}	
		
		return closestObject;
	}
	
	// gets all enemys
	GameObject[] GetEnemys()
	{
		return GameObject.FindGameObjectsWithTag("Enemy");	
	}
	
	// gets this teams enemys
	List<GameObject> GetTeamEnemys()
	{
		GameObject[] enemys = GetEnemys();
		List<GameObject> team = new List<GameObject>();
		
		foreach (GameObject e in enemys)
		{
			team.Add(e);
		}
		
		return team;		
	}
}
