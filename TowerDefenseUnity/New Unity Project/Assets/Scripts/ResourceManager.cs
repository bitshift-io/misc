﻿using UnityEngine;
using System.Collections;

public class ResourceManager : MonoBehaviour 
{
	
	GameObject m_gameSettings;
	float m_resources = 0;
	
	// Use this for initialization
	void Start() 
	{
		// get game settings
		GameObject[] gameItems = GameObject.FindGameObjectsWithTag("GameSettings");
		m_gameSettings = gameItems[0];
		
		// give player resources
		AddResource(m_gameSettings.GetComponent<GameSettings>().StartResources());
		
		

	}
	
	// Update is called once per frame
	void Update() 
	{
	
	}
	
	public void AddResource(float val)
	{
		m_resources += val;
		Debug.Log ("res: " + m_resources.ToString());
	}
}
