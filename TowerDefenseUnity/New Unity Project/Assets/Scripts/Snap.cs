﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Snap : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
	
	// assume naming Snap_01 through to Snap_99
	public List<GameObject> GetSnapPoints()
	{
		List<GameObject> list = new List<GameObject>();

		foreach (Transform child in GetComponent<Model>().Instance().transform)
		{
    		if (child)
			{
				if (child.name.StartsWith("Snap"))
				{
					list.Add(child.gameObject);
				}
			}
    	}	

		return list;
	}
}
