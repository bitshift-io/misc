﻿using UnityEngine;
using System.Collections;

// create cameras in code for the interface stuff
// setup layers for the camera to render only those object on

public class CameraHUD : MonoBehaviour 
{
	
	// create
	GameObject m_orthographic;
	GameObject m_perspective;
	public GameObject testUI;
	
	// pre int
	void Awake()
	{
		// config main camera
		Camera.main.depth = 0;
		
		// ortho
		m_orthographic = new GameObject("HUD_Orthographic");
		m_orthographic.transform.position += Vector3.forward; // move
		m_orthographic.transform.rotation = Quaternion.LookRotation(-Vector3.forward); // rotate
		m_orthographic.AddComponent<Camera>();
		Camera cameraOrthographic = m_orthographic.GetComponent<Camera>();
		cameraOrthographic.farClipPlane = 5f;
		cameraOrthographic.fieldOfView = 30f;
		cameraOrthographic.orthographic = true;
		cameraOrthographic.orthographicSize = 0.5f;
		cameraOrthographic.depth = 1; // sort order
		cameraOrthographic.clearFlags = CameraClearFlags.Depth;	
		cameraOrthographic.cullingMask = (1 << LayerMask.NameToLayer("HUD_Orthographic"));
		
		//cameraOrthographic.orthographicSize
		
		
		// pers
		m_perspective = new GameObject("HUD_Perspective");
		m_perspective.AddComponent<Camera>();
		Camera cameraPerspective = m_perspective.GetComponent<Camera>();
		cameraPerspective.farClipPlane = 5f;
		cameraPerspective.fieldOfView = 30f;
		cameraPerspective.depth = 2; 	
		cameraPerspective.clearFlags = CameraClearFlags.Depth;	
		cameraPerspective.cullingMask = (1 << LayerMask.NameToLayer("HUD_Perspective"));
	}
	
	// Use this for initialization
	void Start() 
	{
		// create our test ui
		GameObject obj = (GameObject) GameObject.Instantiate(testUI);
		int layer = LayerMask.NameToLayer("HUD_Orthographic");
		SetLayerRecursively(obj, layer);
		

	}
	
	// Update is called once per frame
	void Update() 
	{
	
	}
	
	void SetLayerRecursively(GameObject obj, int newLayer)
    {
        if (null == obj)
            return;

        obj.layer = newLayer;
        foreach (Transform child in obj.transform)
        {
            if (null == child)
                continue;
			
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }
	
}
