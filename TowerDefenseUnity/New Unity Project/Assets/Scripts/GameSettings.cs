﻿using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour 
{
	public enum EnemyInteraction
	{
		Shared,
		Individual,
		Off
	}
	
	public enum EnemyDifficulty
	{
		Nightmare,
		Insane,
		Hard,
		Mederate,
		Easy
	}	
	
	public enum ResourceSplit
	{
		Shared,
		Individual
	}
	
	public enum ResourceAmount // spawn amount and income stream
	{
		Excessive,
		High,
		Medium,
		Low
	}	

	
	public EnemyInteraction enemyInteraction;
	public EnemyDifficulty enemyDifficulty = EnemyDifficulty.Mederate;
	public int enemySpawnWaveLimit = 0; // 0 = unlimited
	public ResourceSplit resourceSplit;
	public ResourceAmount resourceAmount = ResourceAmount.Medium;
	public float timeLimit = 0f; // convert to minutes (0 = unlimited)

	// Use this for initialization
	void Start() 
	{
	
	}
	
	// Update is called once per frame
	void Update() 
	{
	
	}
	
	public float StartResources()
	{
		float resource = 0;
		
		switch (resourceAmount)
		{
		case ResourceAmount.Excessive:
		{
			resource = 10000;
		}	
		break;
			
		case ResourceAmount.High:
		{
			resource = 1000;
		}	
		break;		
			
		case ResourceAmount.Medium:
		{
			resource = 100;
		}	
		break;	
			
		case ResourceAmount.Low:
		{
			resource = 10;
		}	
		break;				
			
		default:
		{
			resource = 100;
		}
		break;
		}	
		
		return resource;
	}
}
