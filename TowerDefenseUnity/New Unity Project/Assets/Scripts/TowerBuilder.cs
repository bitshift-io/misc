﻿using UnityEngine;
using System.Collections;

public class TowerBuilder : MonoBehaviour 
{
	
	public GameObject[] towerStructureList;
	
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
	

	// snap target structure + node && bridge structure + node
	public void BuildTower(GameObject targetObject, GameObject targetNode, GameObject newObject, GameObject newNode)
	{
		// rotation
		Quaternion rotation = Quaternion.LookRotation(targetNode.transform.forward, Vector3.up); // WORKS
		newObject.transform.rotation = rotation;

		// transform position
		newObject.transform.position = targetNode.transform.position;
		Vector3 offset = newNode.transform.position - targetNode.transform.position;
		newObject.transform.position -= offset;
		
		// add this bridge to the player object list
		gameObject.GetComponent<PlayerObjects>().Add(newObject);
	}
	
	public void BuildTower(GameObject targetObject, GameObject targetNode)
	{
		GameObject obj = CreateBridgePrefab(towerStructureList[1], this.transform.position, this.transform.rotation);
		GameObject node = obj.GetComponent<Snap>().GetSnapPoints()[0];
		BuildTower(targetObject, targetNode, obj, node);
	}	
	
	// create a model and apply scripts to it
	GameObject CreateBridgePrefab(GameObject model, Vector3 position, Quaternion rotation)
	{
		GameObject obj = (GameObject) GameObject.Instantiate(model, position, rotation);
		return obj;
	}
}
