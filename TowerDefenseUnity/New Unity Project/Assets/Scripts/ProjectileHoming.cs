﻿using UnityEngine;
using System.Collections;

public class ProjectileHoming : Projectile 
{
	public float speed = 1;
	public float acceleration = 0;
	
	
	// Use this for initialization
	void Start() 
	{
	}
	
	// Update is called once per frame
	void Update() 
	{
		base.Update();
		gameObject.transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.Self);
	}
	
	// collision trigger
	void OnTriggerEnter(Collider obj)
	{
		Destroy(obj.gameObject);
		Destroy(gameObject);
	}
}
