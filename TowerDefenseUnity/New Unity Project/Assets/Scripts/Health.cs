﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour 
{
	
	public float health = 100f;
	
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		CheckHealth();
	}
	
	void CheckHealth()
	{
		if (health <= 0)
			Destroy ();
	}
	
	void Destroy()
	{
		Destroy(gameObject);
	}
	
	public void ApplyDamage(float damage)
	{
		health -= damage;
	}
}
