﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// this will spawn an array of random unique bridge parts to the left of the screen which the user can select
// keep this in view space

public class BridgeUI : MonoBehaviour 
{
	
	public int 			bridgeDisplayCount = 3;
	
	List<GameObject>	m_currentBridgeStructures = new List<GameObject>(); // list as the contents change dynamic ingame
	GameObject			m_parent;
	Camera				m_camera;
	Transform 			m_lastCameraTransform;

	// pre init
	void Awake()
	{
		m_parent = this.gameObject;
		m_camera = Camera.main;
	}
	
	// Use this for initialization
	void Start() 
	{
	}
	
	// Update is called once per frame
	void Update() 
	{
		// only update on camera change, or peice used...
		if (m_camera.transform == m_lastCameraTransform)
			return;
		
		CreateBridgeStructures();
		
		//DrawBridgeUI();

		m_lastCameraTransform = m_camera.transform;
	}
	
	void DrawBridgeUI()
	{
		// bottom left view space -0.5, -0.5
		// top right view space 0.5 , 0.5
		// 1 unit x 1 unit
		float viewSpacing = 1.0f / bridgeDisplayCount;
		float viewXPos = -0.3f;
		
		foreach (GameObject b in m_currentBridgeStructures)
		{
			Vector3 pos = new Vector3(viewXPos, 0, 0);
			Ray ray = m_camera.ViewportPointToRay(pos);
			Vector3 viewOffset = m_camera.transform.position + ray.direction;
			
			GameObject bridgeObject = (GameObject) GameObject.Instantiate(b, viewOffset, m_parent.transform.rotation);
		}
	}
	
	void CreateBridgeStructures()
	{
		// get array of bridge parts (array as this is static ingame so faster)
		GameObject[] bridgeStructureArray = m_parent.GetComponent<BridgeBuilder>().GetBridgeStructureList();

		while (true)
		{
			if (m_currentBridgeStructures.Count < bridgeDisplayCount)
			{
				int val = Random.Range(0, bridgeStructureArray.Length);
				m_currentBridgeStructures.Add(bridgeStructureArray[val]);
			}
			else
				return;
		}
	}
}
