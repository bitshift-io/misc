﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawnController : MonoBehaviour 
{
	public GameObject ship;
	public bool enabled = true;
	public float spawnTimer = 10f; // seconds
	public float timerReduction = 0.5f; // reduce the time between rounds
	public float minTimer = 2f; // min amount of time between waves
	
	float m_countdownTimer;
	float m_countdownTimerReduction = 0f;
	
	// pre init
	void Awake()
	{
		//spawn a player spawn, and drop on the needed scripts
		if (!isEnabled())
		{
			Destroy (gameObject);
		}
		
		// setup the timer
		m_countdownTimer = spawnTimer;
	}
	
	// Use this for initialization
	void Start() 
	{
	}
	
	// Update is called once per frame
	void Update() 
	{
		m_countdownTimer -= Time.deltaTime;
		if (m_countdownTimer <= 0)
		{
			SpawnEnemy();
			ResetTimer();	
		}
		
		
	}
	
	void ResetTimer()
	{
		m_countdownTimer = spawnTimer - m_countdownTimerReduction;
		
		if (m_countdownTimer <= minTimer)
			m_countdownTimer = minTimer;
		else
			m_countdownTimerReduction += timerReduction;
	}
	
	GameObject[] GetPlayers()
	{
		return GameObject.FindGameObjectsWithTag("Player");	
	}
	
	List<GameObject> GetActivePlayers()
	{
		GameObject[] players = GetPlayers();
		List<GameObject> active = new List<GameObject>();
		
		foreach (GameObject p in players)
		{
			if (p.GetComponent<PlayerSpawn>().isEnabled())
			{
				active.Add(p);
			}			
		}
		
		return active;
	}
	
	void SpawnEnemy()
	{
		List<GameObject> players = GetActivePlayers();
		foreach(GameObject p in players)
		{
			PlayerSpawn spawn = p.GetComponent<PlayerSpawn>();
			if (spawn.enabled)
			{
				GameObject enemy = (GameObject) Instantiate(ship, this.transform.position, this.transform.rotation);
				AirShip ai = enemy.GetComponent<AirShip>();
				ai.SetTarget(p);
			}
		}
	}
	
	public bool isEnabled()
	{
		return enabled;
	}	
}
