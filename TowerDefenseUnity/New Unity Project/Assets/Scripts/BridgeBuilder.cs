﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// this class will take a snap node, place the tile, and do collision checks, finally adding to the player objects list
// contains list of all bridge parts
public class BridgeBuilder : MonoBehaviour 
{
	public GameObject bridgeTemplate;
	public GameObject[] bridgeStructureList;
	
	// pre init
	void Awake()
	{
	}
	
	// Use this for initialization
	void Start() 
	{
	}
	
	// Update is called once per frame
	void Update() 
	{
	}
	
	public GameObject[] GetBridgeStructureList()
	{
		return bridgeStructureList;
	}
	
	// snap target structure + node && bridge structure + node
	public void BuildBridge(GameObject targetObject, GameObject targetNode, GameObject newObject, GameObject newNode)
	{
		// rotation
		Quaternion rotation = Quaternion.LookRotation(targetNode.transform.forward, Vector3.up); // WORKS
		newObject.transform.rotation = rotation;

		// transform position
		newObject.transform.position = targetNode.transform.position;
		Vector3 offset = newNode.transform.position - targetNode.transform.position;
		newObject.transform.position -= offset;
		
		// add this bridge to the player object list
		gameObject.GetComponent<PlayerObjects>().Add(newObject);
	}
	
	// TESTING ONLY!!
	public void BuildBridge(GameObject targetObject, GameObject targetNode)
	{
		GameObject bridgeObject = CreateBridgePrefab(bridgeStructureList[0], this.transform.position, this.transform.rotation);
		GameObject bridgeNode = bridgeObject.GetComponent<Snap>().GetSnapPoints()[0];
		BuildBridge (targetObject, targetNode, bridgeObject, bridgeNode);
	}	
	
	// create a model and apply scripts to it
	GameObject CreateBridgePrefab(GameObject model, Vector3 position, Quaternion rotation)
	{
		GameObject obj = (GameObject) GameObject.Instantiate(bridgeTemplate, position, rotation);
		obj.GetComponent<Model>().SetModel(model);
		return obj;
	}
	
}
