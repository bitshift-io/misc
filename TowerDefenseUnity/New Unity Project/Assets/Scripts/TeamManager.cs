﻿using UnityEngine;
using System.Collections;

// put this on each object to store team information
public class TeamManager : MonoBehaviour 
{
	GameObject m_owner;
	
	// Use this for initialization
	void Start() 
	{
	
	}
	
	// Update is called once per frame
	void Update() 
	{
	
	}
	
	public void SetOwner(GameObject owner)
	{
		m_owner = owner;
	}
	
	public GameObject Owner()
	{
		return m_owner;
	}
}
