﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
	public float life = 2f; // in seconds
	GameObject m_target;
	GameObject m_owner;
	float m_life = 0f;
	
	
	// Use this for pre initialization
	void Awake()
	{
	}
			
	// Use this for initialization
	void Start() 
	{
		
	}
	
	// Update is called once per frame
	protected void Update() 
	{
		m_life += Time.deltaTime;

		if (m_life >= life)
			Destroy(gameObject);
	}

	
	public void FireAtTarget(GameObject owner, GameObject target)
	{
		m_owner = owner;
		m_target = target;
		gameObject.transform.LookAt(target.transform.position, Vector3.up);
	}
}
