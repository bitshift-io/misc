﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerObjects : MonoBehaviour 
{
	
	List<GameObject> m_playerObjectList = new List<GameObject>();
		
	// pre init
	void Awake()
	{		
	}
	// Use this for initialization
	void Start() 
	{
	}
	
	// Update is called once per frame
	void Update() 
	{
	}
	
	public List<GameObject> GetPlayerObjects()
	{
		return m_playerObjectList;
	}	
	
	public GameObject GetPlayerObjects(int val)
	{
		return m_playerObjectList[val];
	}
	
	public void Add(GameObject obj)
	{
		m_playerObjectList.Add(obj);
		obj.AddComponent<TeamManager>().SetOwner(gameObject); // assign the object a parent
	}	
	
}
