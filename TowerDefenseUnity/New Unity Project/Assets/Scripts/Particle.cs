﻿using UnityEngine;
using System.Collections;

public class Particle : MonoBehaviour 
{
	
	public GameObject particle;
	GameObject m_instance;
	
	void Awake()
	{
		m_instance = (GameObject) GameObject.Instantiate(particle, gameObject.transform.position, gameObject.transform.rotation);
		m_instance.transform.parent = gameObject.transform;
	}
	
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
