﻿using UnityEngine;
using System.Collections;

public class AirShip : MonoBehaviour 
{
	public GameObject playerTarget;
	
	public float speed = 0.5f;
	
	
	// Use this for initialization
	void Start() 
	{
	
	}
	
	// Update is called once per frame
	void FixedUpdate() 
	{
		this.transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.Self);
		gameObject.GetComponent<Rigidbody>().MovePosition(this.transform.position);
	}
	
	public void SetTarget(GameObject target)
	{
		playerTarget = target;
		gameObject.transform.LookAt(target.transform.position, Vector3.up);
		//Vector3 dir = this.transform.position - target.transform.position;
	}
}
