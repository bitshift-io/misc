﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraController : MonoBehaviour 
{	
	enum Action
	{
		None,
		DragStart,
		DragMove,
		DragEnd,
		Zoom,
		Rotate,
		ShortClick, // less than 0.x seconds
		LongClick,
	};
			
	Action 		m_action;
	bool 		m_leftMouseLast = false;
	float 		m_zoomAmount = 0.0f;
	float		m_rotateAmount = 0.0f;
	const float		m_shortClickTime = 0.2f;
	const float		m_longClickTime = 0.8f;
	float			m_currentClickTime = 0.0f;
	Camera			m_mainCamera;
	
	const float 	m_maxNodeDistance = 5.0f; // distance to which a node will snap
	public float maxCameraHeight = 10f;
	public float minCameraHeight = 5f;
	public float zoomSpeed = 0.5f;
	public float rotateSpeed = 1.0f;
	Vector3 worldSpaceStartTouchPoint;
	Vector3 worldSpaceStartCameraPosition;
	
	public GameObject currentPlayer;
	
	//private Vector3 _cameraPosition;
	
	// Use this for initialization
	void Awake()
	{
		m_mainCamera = Camera.main;
		
		// check camera is within min and max height ranges, if not move it to the center
		if (m_mainCamera.transform.position.y < minCameraHeight || m_mainCamera.transform.position.y > maxCameraHeight)
		{
			float averageCameraHeight = minCameraHeight + ((maxCameraHeight - minCameraHeight) / 2.0f);
			m_mainCamera.transform.position = new Vector3 (m_mainCamera.transform.position.x, averageCameraHeight, m_mainCamera.transform.position.z);	
			DebugTools.Log("warning, camera out of bounds!");
		}
		
		// move camera to player spawn, (TODO: add rotate so enemy is always down?)
		Vector3 viewSpace = new Vector3(0.5f, 0.5f, 0.0f);
		Vector3 worldSpaceStartCameraCenter = ViewSpaceToWorldGroundPlane(viewSpace);
		Vector3 cameraOffset = currentPlayer.transform.position - worldSpaceStartCameraCenter;
		m_mainCamera.transform.position += cameraOffset;		
	}
	
	// Use this for initialization
	void Start() 
	{

	}
	
	
	// Update is called once per update
	void Update() 
	{
		//
		// determine the action
		//
		m_action = Action.None;
		
		// left mouse clicks
		if (Input.GetMouseButtonDown(0)) //m_leftMouseLast == false && Input.GetMouseButton(0))
		{
			m_action = Action.DragStart;
			m_currentClickTime = 0;
		}
		else if (Input.GetMouseButton(0)) //m_leftMouseLast)
		{
			m_action = Action.DragMove;
			m_currentClickTime += Time.deltaTime;
		}
		else if (Input.GetMouseButtonUp(0))
		{
			if (m_currentClickTime <= m_shortClickTime)
				m_action = Action.ShortClick;
			else if (m_currentClickTime <= m_longClickTime)
				m_action = Action.LongClick;
			
			m_currentClickTime = 0;
		//	m_action = Action.DragEnd;
		}
		
		
		
		
		// input states
		m_leftMouseLast = Input.GetMouseButton(0);
		
		if(Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began) // record initial states
		{
			m_action = Action.DragStart;
		}
		else if(Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved) // movement
		{
			m_action = Action.DragMove;
		}
		else
		{
		//	m_action = Action.DragEnd;
		}
		
		// zoom touch
		if (Input.touchCount == 2)
		{
			Touch touch = Input.GetTouch(0);
			Touch touch1 = Input.GetTouch(1);
			
			if (touch.phase == TouchPhase.Moved && touch1.phase == TouchPhase.Moved)
			{
				m_action = Action.Zoom;
				Vector2 curDist = touch.position - touch1.position;
				Vector2 prevDist = (touch.position - touch.deltaPosition) - (touch1.position - touch1.deltaPosition);
				float delta = curDist.magnitude - prevDist.magnitude;
				m_zoomAmount = delta;
			}
		}
		
		// zoom mouse wheel
		if (Input.GetAxis("Mouse ScrollWheel") != 0)
		{
			m_action = Action.Zoom;
			m_zoomAmount = Input.GetAxis("Mouse ScrollWheel") * 10.0f;
		}
		
		// zoom keybaord
		if (Input.GetKey(KeyCode.KeypadPlus))
		{
			m_action = Action.Zoom;
			m_zoomAmount = 2.0f;
		}		
		if (Input.GetKey(KeyCode.KeypadMinus))
		{
			m_action = Action.Zoom;
			m_zoomAmount = -2.0f;
		}	
		
		
		// rotate keybaord
		if (Input.GetKey(KeyCode.Q))
		{
			m_action = Action.Rotate;
			m_rotateAmount = 2.0f;
		}		
		if (Input.GetKey(KeyCode.E))
		{
			m_action = Action.Rotate;
			m_rotateAmount = -2.0f;
		}		
		
		//
		// use the action
		//
		switch (m_action)
		{
		case Action.DragStart:
		{
			Vector2 screenSpace = Input.mousePosition;
			worldSpaceStartTouchPoint = ScreenSpaceToWorldGroundPlane(screenSpace);
			worldSpaceStartCameraPosition = this.transform.position;
		}
		break;
			
		case Action.DragMove:
		{
			this.transform.position = worldSpaceStartCameraPosition;
			Vector2 screenSpace = Input.mousePosition;
			Vector3 worldSpaceGroundPos = ScreenSpaceToWorldGroundPlane(screenSpace);
			Vector3 cameraOffset = worldSpaceStartTouchPoint - worldSpaceGroundPos;
			this.transform.position = worldSpaceStartCameraPosition + cameraOffset;
		}
		break;
			
		case Action.DragEnd:
		{
		}
		break;
			
		case Action.Zoom:
		{
			Vector3 previousPos = camera.transform.position;
			camera.transform.Translate(0, 0, m_zoomAmount * zoomSpeed);
			
			if (camera.transform.position.y < minCameraHeight || camera.transform.position.y > maxCameraHeight)
				camera.transform.position = previousPos;
		}
		break;
			
		case Action.Rotate:
		{
			Vector3 viewSpace = new Vector3(0.5f, 0.5f, 0.0f);
			Vector3 worldSpaceStartCameraCenter = ViewSpaceToWorldGroundPlane(viewSpace);
			camera.transform.Rotate(Vector3.up, m_rotateAmount * rotateSpeed, Space.World);
			
			Vector3 worldSpaceCameraCenter = ViewSpaceToWorldGroundPlane(viewSpace);
			Vector3 cameraOffset = worldSpaceStartCameraCenter - worldSpaceCameraCenter;
			this.transform.position += cameraOffset;
		}
		break;		
			
		case Action.ShortClick:
			ShortClick();
			break;
			
		case Action.LongClick:
		{
		}
		break;			
			
		default:
		break;
		}
		
		
		// DEBUG SHIZ testing only
		if (Input.GetKeyDown(KeyCode.Z))
		{
			CreateAttackTower();
		}
		
	}
	
	void CreateAttackTower()
	{
		// TODO: move this into the player spawn object
		Vector2 screenSpace = Input.mousePosition;
		Vector3 worldSpaceGroundPos = ScreenSpaceToWorldGroundPlane(screenSpace);
		
		// loop over each of the items and find the closest object
		List<GameObject> playerObjects = currentPlayer.GetComponent<PlayerObjects>().GetPlayerObjects();
		float closestDistance = m_maxNodeDistance;
		GameObject closestObject = null;
		foreach(GameObject p in playerObjects) 
		{
			Vector3 diff = worldSpaceGroundPos - p.transform.position;
			float distance = diff.magnitude;
			if (distance < closestDistance)
			{
				closestDistance = distance;
				closestObject = p;
			}
		}
		
		if (!closestObject)
			return;
		
	
		// now get the snap points
		List<GameObject> snapPoints = closestObject.GetComponent<Snap>().GetSnapPoints();
		float closestSnapDistance = m_maxNodeDistance;
		GameObject closestSnapObject = null;			
		foreach(GameObject s in snapPoints)
		{
			Vector3 diff = worldSpaceGroundPos - s.transform.position;
			float distance = diff.magnitude;
			if (distance < closestSnapDistance)
			{
				closestSnapDistance = distance;
				closestSnapObject = s;
			}				
		}
		
		if (!closestSnapObject)
			return;
			
		currentPlayer.GetComponent<TowerBuilder>().BuildTower(closestObject, closestSnapObject);

	}
	
	void ShortClick()
	{
		// TODO: move this into the player spawn object
		Vector2 screenSpace = Input.mousePosition;
		Vector3 worldSpaceGroundPos = ScreenSpaceToWorldGroundPlane(screenSpace);
		
		// loop over each of the items and find the closest object
		List<GameObject> playerObjects = currentPlayer.GetComponent<PlayerObjects>().GetPlayerObjects();
		float closestDistance = m_maxNodeDistance;
		GameObject closestObject = null;
		foreach(GameObject p in playerObjects) 
		{
			Vector3 diff = worldSpaceGroundPos - p.transform.position;
			float distance = diff.magnitude;
			if (distance < closestDistance)
			{
				closestDistance = distance;
				closestObject = p;
			}
		}
		
		if (!closestObject)
			return;
		
		// now get the snap points
		List<GameObject> snapPoints = closestObject.GetComponent<Snap>().GetSnapPoints();
		float closestSnapDistance = m_maxNodeDistance;
		GameObject closestSnapObject = null;			
		foreach(GameObject s in snapPoints)
		{
			Vector3 diff = worldSpaceGroundPos - s.transform.position;
			float distance = diff.magnitude;
			if (distance < closestSnapDistance)
			{
				closestSnapDistance = distance;
				closestSnapObject = s;
			}				
		}
		
		if (!closestSnapObject)
			return;
		/*
		// debug, create a object at this point!
		if (closestSnapObject)
		{
			GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        	sphere.transform.position = closestSnapObject.transform.position;
		}	
		*/
		
		currentPlayer.GetComponent<BridgeBuilder>().BuildBridge(closestObject, closestSnapObject);
	}

	Vector3 ScreenSpaceToWorldGroundPlane(Vector3 screenSpace)
	{
		Ray ray = camera.ScreenPointToRay(screenSpace);
		Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
		float distance;
		groundPlane.Raycast(ray, out distance);
		Vector3 groundPos = ray.GetPoint(distance);	
		//Debug.DrawRay(ray.origin, ray.direction * distance, Color.red, 1f);
		//Debug.Log (groundPos);
		return groundPos;
	}
	

	Vector3 ViewSpaceToWorldGroundPlane(Vector3 viewSpace)
	{
		Ray ray = camera.ViewportPointToRay(viewSpace);
		Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
		float distance;
		groundPlane.Raycast(ray, out distance);
		Vector3 groundPos = ray.GetPoint(distance);	
		//Debug.DrawRay(ray.origin, ray.direction * distance, Color.red, 1f);
		//Debug.Log (groundPos);		
		return groundPos;		
	}


}
