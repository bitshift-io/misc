CREATE_DATABASE blackcarbon

CREATE TABLE user
(
	id			int,
	firstname		varchar,
	lastname		varchar,
	email			varchar,
	mailinglist		boolean,
	password		varchar,
	verified		boolean,
)

CREATE TABLE product
(
	id			int,
	name			varchar,
	price			int,
      PRIMARY KEY 	id
)

CREATE TABLE userproduct
(
	userid		int,
	keyid			int,
	productid		int,
	orderid		int
)

CREATE TABLE key
(
	id			int,
	key			varchar,
	valid			boolean
)

CREATE TABLE productdownload
(
	productid		int,
	downloadid		int	
)

CREATE TABLE download
(
	id			int,
	url			varchar,
	name			varchar,
	downloads		int
)

CREATE TABLE userdownload
(
	id			int,
	userid		int,
	downloadid		int,
)

CREATE TABLE order
(
	id			int,
	status		int,
	number		int,
	ip			varchar,
	date			date
)

# password is a md5 hash of the password

CREATE TABLE `user` (
`id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
`firstname` VARCHAR( 128 ) NOT NULL ,
`lastname` VARCHAR( 128 ) NOT NULL ,
`email` VARCHAR( 255 ) NOT NULL ,
`mailinglist` BOOL NOT NULL ,
`password` VARCHAR( 32 ) NOT NULL ,
`verified` TINYINT( 1 ) DEFAULT '0' NOT NULL ,
PRIMARY KEY ( `id` )
) TYPE = MYISAM ;

CREATE TABLE `product` (
`id` INT UNSIGNED NOT NULL ,
`name` VARCHAR( 255 ) NOT NULL ,
`price` FLOAT NOT NULL ,
PRIMARY KEY ( `id` )
) TYPE = MYISAM ;

CREATE TABLE `download` (
`id` INT UNSIGNED NOT NULL ,
`url` VARCHAR( 255 ) NOT NULL ,
`filename` VARCHAR( 255 ) NOT NULL ,
`downloads` INT UNSIGNED DEFAULT '0' NOT NULL ,
PRIMARY KEY ( `id` )
) TYPE = MYISAM ;

CREATE TABLE `userdownload` (
`id` INT UNSIGNED NOT NULL ,
`userid` INT UNSIGNED NOT NULL ,
`downloadid` INT UNSIGNED NOT NULL ,
PRIMARY KEY ( `id` ) ,
UNIQUE (
`userid`
)
) TYPE = MYISAM ;

# When a user logs in a sesion is created for them
# so cookies cant be compromised
CREATE TABLE `session` (
`id` INT UNSIGNED NOT NULL ,
`ip` VARCHAR( 15 ) NOT NULL ,
`cookie` VARCHAR( 32 ) NOT NULL ,
`userid` INT UNSIGNED NOT NULL ,
PRIMARY KEY ( `id` )
) TYPE = MYISAM ;

#info needed to geenrate a uvtools license
CREATE TABLE `uvtoolslicence` (
`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
`userid` INT UNSIGNED NOT NULL ,
`type` SMALLINT UNSIGNED NOT NULL ,
`maxid` VARCHAR( 32 ) NOT NULL ,
`number` INT UNSIGNED NOT NULL ,
`company` VARCHAR( 255 ) NOT NULL ,
`verified` TINYINT( 1 ) DEFAULT '0' NOT NULL , 
PRIMARY KEY ( `id` )
) TYPE = MYISAM ;
