<?php

	include('../../include/dbase.inc.php');
	include('../../include/template.inc.php');
	include('../../include/login.inc.php');				
	include('../../include/order.inc.php');
	require_once('../../include/functions.inc.php');
		
	$tHead = & new PageTemplate('../head.htm');
	$tBody = & new PageTemplate('prepurchase.htm');
	$tFoot = & new PageTemplate('../foot.htm');
	
	$tBody->Set('maxId', $_GET['maxId']);
	
	$tHead->Set('admin', IsAdmin());
	$tHead->Set('login', IsLoggedIn());
	$tBody->Set('login', IsLoggedIn());
	$tFoot->Set('login', IsLoggedIn());

	if (IsLoggedIn())
	{
		$login = GetLogin();
		$tBody->Set('username', $login->GetUserName());
		$tFoot->Set('username', $login->GetUserName());
		
		// check for existing order and redirect the user
		{
			$db = new DBase();
			$db->Connect();
			
			$orderId = HasOutstandingOrder($db, 'UV Tools');
			if ($orderId != -1)
				ShowExistingOrderPage($orderId);
				
			$db->Disconnect();
		}
					
		
		$message = '';
		$login = GetLogin();
											
		// individual licence
		if (isset($_POST['submitIndividual']) || isset($_POST['submitIndividual_x']))
		{			
			$maxid = $_POST['maxId'];
			
			if (strlen($maxid) <= 0)
				$message = 'Please enter a valid max id';
							
			$db = new DBase();
			$db->Connect();		
			
			// check this max id isnt already in use
			$result = $db->SelectWhere('uvtoolslicence', array('*'), "maxid='$maxid'");
			if (mysql_num_rows($result) > 0)
				$message = 'This maxid is already in use';
				
			$db->Disconnect();
					
			if ($message == '')
			{					 
				$db = new DBase();
				$db->Connect();		
						
				$db->StartTransaction();
				
				$orderId = CreateOrder($db, 'UV Tools', 1);
				if ($db->HasError())
				{
					$db->RollbackTransaction();
					$db->Disconnect();
					die("Failed to place your order");
				}
				
				// need to get login user id's and such here
				$arrValuePairs = array('userid' => $login->GetUID(), 'type' => 0, 'maxid' => $maxid, 'number' => 1, 'orderid' => $orderId);
				$db->Insert('uvtoolslicence', $arrValuePairs);
				if ($db->HasError())
				{
					$db->RollbackTransaction();
					$db->Disconnect();
					die("Failed to place your order");
				}
				
				$db->CommitTransaction();
				$db->Disconnect();
				
				PlaceOrder($orderId);
				return;
			}
			
			$tBody->Set('individualMessage', $message);					
		}
		/*
		// company licence
		if (isset($_POST['submitBuisness']) || isset($_POST['submitBuisness_x']))
		{		
			$number = $_POST['number'];
			$name = $_POST['companyname'];
			$email = $_POST['companyemail'];
			
			if (strlen($name) <= 0)
				$message = 'Please enter a company name';
				
			if (strlen($email) <= 0)
				$message = 'Please enter a company email we should contact';
				
			if ($message == '')
			{		
				$tMessage = & new Template('message.htm');
				
				$db = new DBase();
				$db->Connect();
				
				$db->StartTransaction();
				
				$orderId = CreateOrder($db, 'UV Tools', $number);
				if ($db->HasError())
				{
					$db->RollbackTransaction();
					$db->Disconnect();
					die("Failed to place your order");
				}
				
				// need to get login user id's and such here
				$arrValuePairs = array('userid' => $login->GetUID(), 'type' => 1, 'companyname' => $name, 'companyemail' => $email, 'number' => $number);
				$orderId = $db->Insert('uvtoolslicence', $arrValuePairs);	
				if ($db->HasError())
				{
					$db->RollbackTransaction();
					$db->Disconnect();
					die("Failed to place your order");
				}
				
				$db->CommitTransaction();
				$db->Disconnect();
				
				$tMessage->Set('title', 'Order Placed');
				$tMessage->Set('messageType', 'success');
				$tMessage->Set('message', 'Thank you for placing an order. We will contact you about setting up UV Tools for your company. In the mean time, leave your order open and do not make the payment until we tell you too.');
				
				echo $tHead->Fetch();
				echo $tMessage->Fetch();
				echo $tFoot->Fetch();
				
				$to = 'sales@blackcarbon.net';
				SendMail($to, '[Black Carbon][UV Tools] Buisness Order Request', "Buisness order request\r\norder id: $orderId\r\ncompany email: $email\r\nnumber of copies: $number", $to);
				return;
			}
			
			$tBody->Set('companyname', $name);
			$tBody->Set('companyemail', $email);
			$tBody->Set('buisnessMessage', $message);					
		}
		*/	
		
		echo $tHead->Fetch();
		echo $tBody->Fetch();
		echo $tFoot->Fetch();	
	}
	else
	{
		$tRedirect = & new PageTemplate();
		$tRedirect->Redirect($tRedirect->secureBasePath . 'login/index.php?redirect=../uvtools/prepurchase.php?maxId=' . $_GET['maxId']);	
	}
	
?>