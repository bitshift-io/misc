<?php

	include_once('../../include/dbase.inc.php');
	include_once('../../include/template.inc.php');
	include_once('../../include/login.inc.php');				
	include_once('../../include/order.inc.php');
		
	$tHead = & new PageTemplate('../head.htm');
	$tBody = & new PageTemplate('prepurchase.htm');
	$tFoot = & new PageTemplate('../foot.htm');
		
	$tHead->Set('admin', IsAdmin());
	$tHead->Set('login', IsLoggedIn());
	$tBody->Set('login', IsLoggedIn());
	$tFoot->Set('login', IsLoggedIn());

	if (IsLoggedIn())
	{
		$login = GetLogin();
		$tBody->Set('username', $login->GetUserName());
		$tFoot->Set('username', $login->GetUserName());
		
		// check for existing order and redirect the user
		$db = new DBase();
		$db->Connect();
		
		$orderId = HasOutstandingOrder($db, 'Explosive');
		if ($orderId != -1)
		{
			ShowExistingOrderPage($orderId);
		}
		else
		{
			$orderId = CreateOrder($db, 'Explosive', 1);
		}
			
		$db->Disconnect();		
		
		// place an order
		if ($orderId != -1)
		{	
			PlaceOrder($orderId);
			return;
		}
		
		echo $tHead->Fetch();
		echo $tBody->Fetch();
		echo $tFoot->Fetch();	
	}
	else
	{
		$tRedirect = & new PageTemplate();
		$tRedirect->Redirect($tRedirect->secureBasePath . 'login/index.php?redirect=../explosive/prepurchase.php');	
	}
	
?>