<?php

//include_once('../../include/order.inc.php');
require_once('../../include/functions.inc.php');

function NotifyOrderComplete($db, $productId, $userId, $orderId, $payPalId, $email, $name, $destEmail)
{
	$efp = fopen('.explosive_ordercomplete.log','a');	

	fwrite($efp, "NotifyOrderComplete: productid - $productId, userid - $userId, orderid - $orderId, paypalid - $payPalId\n");

	// get download id
	$arrValuePairs = array('id');
	$result = $db->SelectWhere('download', $arrValuePairs, "productid='$productId' AND purchasable='1'");
	if ($result == 0)
	{
		fwrite($efp, "error - no download found for order\n");
		return payment_received_order_error;
	}

	for ($i = 0; $i < mysql_num_rows($result); $i++)
	{
		$row = mysql_fetch_array($result, MYSQL_NUM);

		fwrite($efp, "adding download id - $row[0]\n");

		// insert user download
		$arrValuePairs = array('userid' => $userId, 'downloadid' => $row[0], 'orderId' => $orderId);
		$db->Insert('userdownload', $arrValuePairs);
	}
	
	fwrite($efp, "sending email about new download to user\n");

	$msg = $name . ",\r\n\r\nExplosive is now available for your to download at www.blackcarbon.net.\r\nSimply login then go to the profile page to get the download.\r\n\r\nThanks,\r\nBlack Carbon";
	SendMail($email, '[Black Carbon] Explosive available for download', $msg, $destEmail);

	fwrite($efp, "sending email about new download to user: \n");
	fwrite($efp, "\n");
	fclose($efp);

	return order_complete;
}

?>