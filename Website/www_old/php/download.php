<?php

	include ('../../include/dbase.inc.php');
	include ('../../include/downloads_config.inc.php');
	include('../../include/login.inc.php');
		
	$db = new DBase();
	$db->Connect();
	
	$id = $_GET['id'];
	
	$arrColumnNames = array('id', 'path', 'filename', 'loginrequired', 'downloads', 'host');
	$result = $db->SelectWhere('download', $arrColumnNames, 'id = ' .$id);
	
	if (mysql_num_rows($result) > 1)
		die("The database is corrupt");
		
	$row = mysql_fetch_array($result, MYSQL_NUM);
	$id = $row[0];
	$file = $row[2];
	$url = $row[1];
	$host = $row[5];
	$loginRequired = $row[3];
	$downloads = $row[4];
	++$downloads;

	// means we need an entry in the userdownload table to d/l this file!
	if ($loginRequired)
	{
		if (!IsLoggedIn())
			die ('you must be logged in to access this file');
			
		// make sure the user is allowed to download this file
		$login = GetLogin();
		$result = $db->SelectWhereArray(array('userdownload', 'download'), array('*'), "userdownload.downloadid=download.id AND userdownload.userid=" . $login->uid . " AND download.id=" . $id);
			
		if (mysql_num_rows($result) < 1)
			die ('Access denied');
	}
	
	$arrSetNames = array('downloads' => $downloads);
	$db->Update('download', $arrSetNames, 'id=' . $id);
		
	$db->Disconnect();
		
		
	header('Content-type: application/exe');
	header('Content-Disposition: attachment; filename="'. $file .'"');
	
	if ($host != "")
	{
		header('Location: ' . $host . $url);
	}
	else
	{
		header('Content-Length: '. filesize($prepath . $url));
		readfile($prepath . $url);
	}
	
?>