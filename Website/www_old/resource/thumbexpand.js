
distanceFromThumb = 2; // padding away from thumb nail

expandedId = 0;     // the id of the current thumb/screen pair
expanded = 0;       // is there a screen visible

function thumbPressed(id, fullwidth, fullheight)
{
    if (expandedId != 0 && id != expandedId && expanded)
    {
        hideScreen(expandedId);
        expanded = 1;
    }
    else
    {
        expanded = !expanded;
    }
    
    expandedId = id;
    
    if (expanded)
    {
        showScreen(id, fullwidth, fullheight);
    }
    else
    {
        hideScreen(expandedId);
    }
}

function screenPressed(id)
{
    expanded = 0;
    hideScreen(expandedId);
}

function showScreen(id, width, height) 
{
    img = document.getElementById("screen" + id);
    img.style.display = 'block';

    expandingtop = img.offsetTop;
    expandingleft = img.offsetLeft;

    img = document.getElementById("screen" + id);
    thumb = document.getElementById("thumb" + id);
    myscroll = getScroll();

    if (expandingtop + thumb.height > myscroll.top + myscroll.height) 
    {
        finaltop = myscroll.top + myscroll.height - height;
    } 
    else 
    {
        finaltop = expandingtop + thumb.height - height;
    }

    if (finaltop < myscroll.top) 
    { 
     finaltop = myscroll.top; 
    }

    img.style.top = finaltop + 'px';

    if (expandingleft + thumb.width > myscroll.left + myscroll.width) 
    {
        finalleft = myscroll.left + myscroll.width - width;
    } 
    else 
    {
        finalleft = expandingleft - width - distanceFromThumb;
    }

    if (finalleft < myscroll.left) 
    { 
        finalleft = myscroll.left; 
    }

    img.style.left = finalleft + 'px';

    img.width = thumb.width + (width - thumb.width);
    img.height = thumb.height + (height - thumb.height);
}

function hideScreen(id) 
{
  img = document.getElementById("screen" + id);
  img.style.top = '';
  img.style.left = '';
  img.style.display = 'none';
  expandingid = 0;
}

function getScroll() 
{
    // return scroll position and size of the browser
    if (document.all && typeof document.body.scrollTop != "undefined") 
    {  
        var ieBox = document.compatMode != "CSS1Compat";
        var cont = ieBox ? document.body : document.documentElement;
        
        return {
          left:   cont.scrollLeft,
          top:    cont.scrollTop,
          width:  cont.clientWidth,
          height: cont.clientHeight
        };
    } 
    else 
    {
        return {
          left:   window.pageXOffset,
          top:    window.pageYOffset,
          width:  window.innerWidth,
          height: window.innerHeight
        };
    }
}
