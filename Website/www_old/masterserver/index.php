<?php

	include('../../include/dbase.inc.php');

	if (empty($_GET['action'])) 
		$_GET['action'] = 'viewservers';  
		
	if (isset($_GET['debug'])) 
	{
		echo $_GET['action'] . '<br>';
	}
	
	$serverTimeOut = 60; // 60 seconds

	$ip		= getenv('REMOTE_ADDR');
	$port	= $_SERVER['REMOTE_PORT'];
	$time	= time(); // in seconds!
		
	$db = new DBase(isset($_GET['debug']));
	$db->Connect();

	// cleans out timmed out servers
	$db->DeleteWhere('masterserver_serverinfo', "timestamp < " . ($time - $serverTimeOut));		
			
	switch ($_GET['action']) 
	{   
	
	// view servers from the web
	case 'viewservers':	 
		{
			// this hsould use templates
			echo 'servers:<br>';
			
			$result = $db->Select('masterserver_serverinfo', array('name'));			
			while ($row = $db->GetNextRow($result)) 
			{
			   echo 'servername: ' . $row['name'] . '<br>'; 
			}
		}
		break;
				
	// get servers in a game friendly format
	// this has standard info about the servers
	case 'getservers': 
		{
			// only return servers the client can join
			$modName = $_GET['modname'];
			$version = $_GET['version'];
			$gameName = $_GET['gamename'];
			
			$result = $db->SelectWhere('masterserver_serverinfo', array('name', 'gamemode', 'ip', 'port', 'id'), "modname='$modName' AND version='$version' AND gamename='$gameName'");
			
			if ($db->HasError())
			{
				$db->ErrorMessage();
			}
			
			while ($row = $db->GetNextRow($result)) 
			{					
			   echo $row['name'] . '|' . $row['gamemode'] . '|' . $row['ip'] . '|' . $row['port'] . '|' . $row['id'] . "\r\n"; 
			}
		}
		break;
		
	// get details about the clients in this game
	case 'getserverclients': 
		{			

		}
		break;
	
	// get a list of clients trying to join the server
	case 'getjoiningclients':
		{
			$serverId = $_GET['serverid'];
			$result = $db->SelectWhere('masterserver_joiningclients', array('ip', 'port'), "serverid='$serverId'");
			
			if ($db->HasError())
			{
				$db->ErrorMessage();
			}
			
			while ($row = $db->GetNextRow($result)) 
			{
			   echo $row['ip'] . '|' . $row['port'] . "\r\n"; 
			}	
			
			$db->DeleteWhere('masterserver_joiningclients', "serverid='$serverId'");		
		}
		break;
		
	// request to join a server
	case 'clientjoinserver':
		{						
			$arrValuePairs = array('ip' => $ip, 'serverid' => $_GET['serverid'], 'port' => $_GET['port']);
			$serverId = $db->Insert('masterserver_joiningclients', $arrValuePairs);
		}
		break;
		
	// the client has left the server
	case 'clientleaveserver':
		{
		}
		break;
		
	// register a new server
	case 'registerserver':
		{			
			$arrValuePairs = array('name' => $_GET['servername'], 'gamemode' => $_GET['gamemode'], 'mapname' => $_GET['mapname'], 
								'modname' => $_GET['modname'], 'version' => $_GET['version'], 'ip' => $ip, 'port' => $_GET['port'], 'timestamp' => $time, 
								'gamename' => $_GET['gamename']);
			$serverId = $db->Insert('masterserver_serverinfo', $arrValuePairs);
			
			echo $serverId;
		}
		break;

	// update a servers status
	case 'updateserver':
		{
			// find server by ip and port
			$serverId = $_GET['serverid'];
			$result = $db->SelectWhere('masterserver_serverinfo', array('id'), "ip='$ip' AND port='". $_GET['port'] ."'"); //"id='$serverId'");

			$arrValuePairs = array('name' => $_GET['servername'], 'gamemode' => $_GET['gamemode'], 'mapname' => $_GET['mapname'], 
									'modname' => $_GET['modname'], 'version' => $_GET['version'], 'ip' => $ip, 'port' => $_GET['port'], 'timestamp' => $time, 
									'gamename' => $_GET['gamename'],
									'id' => $serverId);

			if ($db->GetNumRows($result) <= 0)
			{
				$serverId = $db->Insert('masterserver_serverinfo', $arrValuePairs);
				echo $serverId;
			}
			else
			{				
				$row = $db->GetNextRow($result);
				$db->Update('masterserver_serverinfo', $arrValuePairs, "id='". $row['id'] ."'");
				echo $row['id'];
			}
		}
		break;

	// remove a server from the list, it went down
	case 'removeserver':
		{			
			// just so not any old joe can remove this server
			$serverId = $_GET['serverid'];
			$result = $db->SelectWhere('masterserver_serverinfo', array('*'), "ip='$ip' AND id='$serverId'");
			if ($db->GetNumRows($result) != 1)
				break;
			
			$db->DeleteWhere('masterserver_serverinfo', "id='$serverId'");
		}
		break;
	}

	$db->Disconnect();
?>