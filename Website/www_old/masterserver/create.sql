CREATE TABLE `masterserver_serverinfo` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `gamemode` varchar(255) NOT NULL default '',
  `mapname` varchar(255) NOT NULL default '',
  `modname` varchar(255) NOT NULL default '',
  `version` varchar(255) NOT NULL default '0',
  `ip` varchar(255) NOT NULL default '',
  `port` int(10) unsigned NOT NULL default '0',
  `timestamp` int(10) unsigned NOT NULL default '0',
  `gamename` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1196 ;

CREATE TABLE `masterserver_serverclients` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `keyhash` varchar(255) NOT NULL default '',
  `serverid` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

CREATE TABLE `masterserver_joiningclients` (
  `ip` varchar(255) NOT NULL default '',
  `serverid` int(10) unsigned NOT NULL default '0',
  `port` int(10) unsigned NOT NULL default '0'
) TYPE=MyISAM;