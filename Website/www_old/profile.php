<?php

	//
	// This is a standard page,
	// rename it and it will execute the htm equivelent
	//
	include_once('../include/template.inc.php');
	include_once('../include/login.inc.php');
	include_once('../include/dbase.inc.php');
	include_once('../include/order.inc.php');
	
	// get currently executing page, and replace .php with .htm
	$phpPage = $_SERVER['PHP_SELF'];
	$htmPage = str_replace('.php', '.htm', $phpPage);
	$htmPage = substr(strrchr($htmPage, '/'), 1);
	
	$tHead = & new PageTemplate('head.htm');
	$tBody = & new PageTemplate($htmPage);
	$tFoot = & new PageTemplate('foot.htm');

	$tHead->Set('admin', IsAdmin());
	$tHead->Set('login', IsLoggedIn());
	$tBody->Set('login', IsLoggedIn());
	$tFoot->Set('login', IsLoggedIn());
	
	if (IsLoggedIn())
	{
		$login = GetLogin();
		$tBody->Set('admin', IsAdmin());

		$tFoot->Set('username', $login->GetUserName());
		
		$db = new DBase();
		$db->Connect();
		
		// user details
		$arrColumnNames = array('firstname', 'lastname', 'email', 'mailinglist');
		$result = $db->SelectWhere('user', $arrColumnNames, "id='$login->uid'");
		$row = mysql_fetch_array($result, MYSQL_NUM);
		$tBody->Set('firstName', $row[0]);
		$tBody->Set('lastName', $row[1]);
		$tBody->Set('email', $row[2]);
		$tBody->Set('mailingList', $row[3]);
		
		
		// get a list downloads sorted by product
		$result = $db->SelectWhereArray(array('userdownload', 'download'), array('download.id', 'download.filename', 'userdownload.orderid'), "userdownload.downloadid=download.id AND userdownload.userid=" .$login->uid);
			
		$i = 0;
		while ($row = mysql_fetch_array($result, MYSQL_NUM)) 
		{
		   $downloads[$i] = $row; 
		   ++$i;
		}
		
		$tBody->Set('downloads', $downloads);
		
			
		// get a list of orders
		$arrColumnNames = array('product.name', 'orderform.date', 'orderform.id', 'orderform.status', 'orderform.number', 'orderform.amount');
		$arrTables = array('product', 'orderform');
		$result = $db->SelectWhereArray($arrTables, $arrColumnNames, "product.id=orderform.productid AND orderform.userid=$login->uid");
		
		$i = 0;
		while ($row = mysql_fetch_array($result, MYSQL_NUM)) 
		{
			$orderStatus = $row[3];
			$row[3] = GetOrderStatusText($orderStatus);
			
			// for this order, generate a list of downloads
			array_push($row, array());
			for ($d = 0; $d < count($downloads); ++$d)
			{
				$download = $downloads[$d];
				
				// if download order id = this order id ....
				if ($download[2] == $row[2])
				{
					array_push($row[6], $download);
				}
			}
			
			// for this order, generate a list of order actions
			
			switch (GetEnumFromNumber($orderStatus))
			{
			case order_created:
				{
					$orderActions = array(	array($tBody->secureBasePath . 'paypal/paypal.php?action=process&id=' . $row[2], 'Continue Order'),
											array($tBody->secureBasePath . 'paypal/paypal.php?action=cancel&id=' . $row[2], 'Cancel Order'));					
					array_push($row, $orderActions);
				}
				break;
			}
			
			$orders[$i] = $row;		   
			++$i;
		}

		$tBody->Set('orders', $orders);
				
		
		$db->Disconnect();
	}
	
	echo $tHead->Fetch();
	echo $tBody->Fetch();
	echo $tFoot->Fetch();
	
?>