<?php

	//
	// This is a standard page,
	// rename it and it will execute the htm equivelent
	//
	include_once('../include/template.inc.php');
	include_once('../include/login.inc.php');
	
	// get currently executing page, and replace .php with .htm
	$phpPage = $_SERVER['PHP_SELF'];
	$htmPage = str_replace('.php', '.htm', $phpPage);
	$htmPage = substr(strrchr($htmPage, '/'), 1);
	
	$tHead = & new PageTemplate('head.htm');
	$tBody = & new PageTemplate($htmPage);
	$tFoot = & new PageTemplate('foot.htm');

	$tHead->Set('admin', IsAdmin());
	$tHead->Set('login', IsLoggedIn());
	$tBody->Set('login', IsLoggedIn());
	$tFoot->Set('login', IsLoggedIn());
	
	if (IsLoggedIn())
	{
		$login = GetLogin();
		$tBody->Set('username', $login->GetUserName());
		$tFoot->Set('username', $login->GetUserName());
	}
	
	echo $tHead->Fetch();
	echo $tBody->Fetch();
	echo $tFoot->Fetch();
	
?>