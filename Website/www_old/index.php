<?php

	include_once('../include/template.inc.php');
	include_once('../include/login.inc.php');
	
	$t = new PageTemplate;
	
	if (IsLoggedIn())
		$t->Redirect('profile.php');
	else
		$t->Redirect('products.php');
	
?>