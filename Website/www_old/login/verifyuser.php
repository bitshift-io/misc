<?php

	include('../../include/dbase.inc.php');
	include('../../include/template.inc.php');
	include_once('../../include/functions.inc.php');
		
	class MD5
	{
		function Generate($string)
		{
			return md5($string);
		}
		
		function Verify($md5, $string)
		{
			return $this->Generate($string) == $md5;
		}
	}
	
	$tTemp = & new PageTemplate();
	$location = $tTemp->secureBasePath . "/login/";
	$destEmail = "support@blackcarbon.net";

	$verify = $_GET['verify'];
	$userid = $_GET['id'];
	$email = $_GET['email'];

	$tHead = new PageTemplate('../head.htm');
	$tFoot = new PageTemplate('../foot.htm');
	$tMessage = new PageTemplate('message.htm');
	$message = '';
	$messageType = '';
	$title = '';
			
	$db = new DBase();
	$db->Connect();
	
	if (isset($verify) && isset($userid))
	{		
		// select email from the user id
		$arrColumnNames = array('email');
		$result = $db->SelectWhere('user', $arrColumnNames, 'id=' . $userid);
		
		if (mysql_num_rows($result) <= 0)
			die($tHead->Fetch() . 'Trying to verify a user that does not exist. Please contact ' . $destEmail . $tFoot->Fetch());
		
		if (mysql_num_rows($result) > 1)
			die($tHead->Fetch() . 'Are you trying to hack? Look, just go to your email and follow the link' . $tFoot->Fetch());
			
		$row = mysql_fetch_array($result, MYSQL_NUM);
		$email = $row[0];
		
		$md5 = new MD5();
		if ($md5->Verify($verify, $email))
		{
			$arrSetNames = array('verified' => '1');
			$db->Update('user', $arrSetNames, 'id=' . $userid);
			$title = 'Your account has been verified';
			$message = 'You may now proceed to purchase any of our available products. If you wish to change any settings or view your order history look at your profile.';
			$messageType = 'success';
		}
		else
		{
			$title = 'Error';
			$message = 'Are you trying to hack? Look, just go to your email and follow the link';
			$messageType = 'error';
		}
	}
	else if (isset($userid))
	{		
		// select email from the user id
		$arrColumnNames = array('firstname', 'email');
		$result = $db->SelectWhere('user', $arrColumnNames, 'id=' . $userid);
	
		if (mysql_num_rows($result) != 1)
			die($tHead->Fetch() . 'An internal error has occured. Please contact ' . $destEmail . $tFoot->Fetch());			
			
		$row = mysql_fetch_array($result, MYSQL_NUM);
		$name = $row[0];
		$email = $row[1];
				
		$md5 = new MD5();
		$verify = $md5->Generate($email);
	
		$msg = $name . ",\r\n\r\nPlease click the following link to verify your email:\r\n" . $location . "verifyuser.php?verify=" . $verify 
			. "&id=" . $userid . "\r\n\r\nThanks,\r\nBlack Carbon";
							   
		if (SendMail($email, 'Black Carbon Account Verification', $msg, $destEmail))
		{	
			$title = 'Email sent';		
			$message = 'An email has been sent to ' . $email . '. This email has been sent to verify that you have correctly entered your email address. Once you receive the verification email you can proced to purchase any available products';
			$messageType = 'email';
		}
		else		
		{
			$title = 'Error';
			$message = 'Failed to send email to ' . $email . ', please contact ' . $destEmail . ' if this is your valid email address';
			$messageType = 'error';
		}
	}
	else
	{
		die($tHead->Fetch() . 'You shouldnt be here. Please contact ' . $destEmail . $tFoot->Fetch());
	}
	
	$db->Disconnect();
	
	$tMessage->Set('messageType', $messageType);
	$tMessage->Set('message', $message);
	$tMessage->Set('title', $title);
	
	echo $tHead->Fetch();
	echo $tMessage->Fetch();
	echo $tFoot->Fetch();
?>