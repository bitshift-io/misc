<?php

	// transfers login data between secure and nonsecure website
	include('../../include/dbase.inc.php');
	include('../../include/template.inc.php');
	include('../../include/login.inc.php');
	include('../../include/guid.inc.php');
	include('../../include/global.inc.php');
	
	$redirect = $_GET['redirect'];

	// need to chack again name in sandbox mode as there is no https
	global $gSandbox;
	if ($gSandbox && $_SERVER['SERVER_NAME'] == 'bronson.dyndns.org')
		$isSecure = true;
	else
		$isSecure = ($_SERVER['HTTPS'] == 'on');

	if (!isset($_GET['set'])) // we are setting the data
	{
		if (isset($_GET['auth']))
			die("An internal error has occured");
		
		// generate a hash
		$uid = new Guid();
		$auth = $uid->ToString();
			
		$db = new DBase();
		$db->Connect();
	
		$login = GetLogin();
		$db->Insert('session', array('auth' => $auth, 'userid' => $login->uid));
		if ($db->HasError())
		{
			$db->ErrorMessage();
		}
		
		$db->Disconnect();

		// redirect to the alternate
		$tRedirect = & new PageTemplate();
		if ($isSecure)
			$tRedirect->Redirect($tRedirect->basePath . 'login/securetransfer.php?set=1&auth=' . $auth . '&redirect=' . $redirect);
		else
			$tRedirect->Redirect($tRedirect->secureBasePath . 'login/securetransfer.php?set=1&auth=' . $auth. '&redirect=' . $redirect);	
	}
	else // we are getting the data and redirecting
	{		
		if (!isset($_GET['auth']))
			die("An internal error has occured");

		$db = new DBase();
		$db->Connect();
			
		// select the row using the authentication hash
		$auth = $_GET['auth'];
		$result = $db->SelectWhere('session', array('userid'), "auth='$auth'");
		if (mysql_num_rows($result) != 1)
			die("An internal error has occured");
			
		// delete the authentication hash
		$row = mysql_fetch_array($result, MYSQL_NUM);
		$db->DeleteWhere('session', "auth='$auth'");
		$userId = $row[0];
		
		// get the users details
		$result = $db->SelectWhere('user', array('*'), "id='$userId'");
		if (mysql_num_rows($result) != 1)
			die("An internal error has occured");
			
		// log em in
		$row = mysql_fetch_array($result, MYSQL_NUM);		
		$admin = $row[7];
		$forumId = $row[8];

		LogIn($row[1], $userId, $admin, $forumId);

		$db->Disconnect();
		
		// redirect
		$tRedirect = & new PageTemplate();
		$tRedirect->Redirect($redirect);	
	}
	
?>