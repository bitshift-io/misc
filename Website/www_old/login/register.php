<?php

	include ('../../include/dbase.inc.php');
	include ('../../include/template.inc.php');

	$first = $_POST['first'];
	$last = $_POST['last'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$confirmpassword = $_POST['confirmpassword'];
	$mail = $_POST['mail'];
	
	$tHead = & new PageTemplate('../head.htm');
	$tLogin = & new PageTemplate('login.htm');
	$tFoot = & new PageTemplate('../foot.htm');
		
	$message = '';
	
	if (!isset($first) || strlen($first) < 2)
		$message .= 'Please enter your first name (at least 2 characters)<br>';
	
	if (!isset($last) || strlen($last) < 2)
		$message .= 'Please enter your last name (at least 2 characters)<br>';
		
	if (!isset($email) || strlen($email) < 3)
	{
		$message .= 'Please enter your email address (at least 3 characters)<br>';
	}
	else
	{
		if (!strrchr($email, '@') || !strrchr($email, '.'))
			$message .= 'Your email is invalid<br>';
	}
	
	if (!isset($password) || strlen($password) < 5)
		$message .= 'Please enter a password of at least 5 characters and/or numbers<br>';
	else if ($password != $confirmpassword)
		$message .= 'Your password was not the same as your confirm password<br>';
		
	if (strlen($message) > 0)
	{
		$tLogin->Set('registerMessage', $message);
		
		echo $tHead->Fetch();
		echo $tLogin->Fetch();
		echo $tFoot->Fetch();
		
		return;
	}
		
	$hashPassword = md5($password);
	
	$db = new DBase();
	$db->Connect();

	$arrColumnNames = array('*');
	$result = $db->SelectWhere('user', $arrColumnNames, "email='$email'");
	
	// if this email has already been used...
	if (mysql_num_rows($result) > 0)
	{		
		$tLogin->Set('registerMessage', 'The email address is already in use, please use another email');
		
		echo $tHead->Fetch();
		echo $tLogin->Fetch();
		echo $tFoot->Fetch();
	}
	else
	{
		// register user with the forum
		//{
			include_once ('../forum/include/functions.php');

			$punHashPassword = 0;//pun_hash($password);
			$arrValuePairs = array('group_id' => '4', 'username' => $first, 'email' => $email, 'password' => $punHashPassword, 'registration_ip' => get_remote_address(), );
			$forumId = $db->Insert('punbb_users', $arrValuePairs);
		//}

		$arrValuePairs = array('firstname' => $first, 'lastname' => $last, 'email' => $email, 'password' => $hashPassword, 'mailinglist' => $mail, 'forumid' => $forumId);
		$db->Insert('user', $arrValuePairs);
				
		$result = $db->SelectWhere('user', $arrColumnNames, "firstname='$first' AND lastname='$last' AND email='$email' AND password='$hashPassword'");
		
		if (mysql_num_rows($result) != 1)
			die("The database is corrupt");		
			
		$row = mysql_fetch_array($result, MYSQL_NUM);
		$id = $row[0];
			
		//echo $first . ', you are now registered.<br>';
		$redirect = new PageTemplate();
		$redirect->Redirect('verifyuser.php?id=' . $id);
				
		//header ('Location: login/verifyuser.php?id=' . $id);
		//$_POST['id'] = $id;
		//include ('login/verifyuser.php');
	}
	
	$db->Disconnect();
?>