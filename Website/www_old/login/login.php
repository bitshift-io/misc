<?php

	include_once('../../include/dbase.inc.php');
	include_once('../../include/template.inc.php');
	include_once('../../include/login.inc.php');
	
	$login = $_POST['login'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$passwordHash = md5($password);
	
	$dBase = new DBase();
	$dBase->Connect();

	$arrColumnNames = array('*');
	$result = $dBase->SelectWhere('user', $arrColumnNames, "email='$email' AND password='$passwordHash'");
			
	$tHead = new PageTemplate('../head.htm');
	$tLogin = new PageTemplate('login.htm');
	$tFoot = new PageTemplate('../foot.htm');
		
	// login failed
	if ($dBase->GetNumRows($result) != 1) //mysql_num_rows($result) != 1)
	{		
		$tLogin->Set('loginMessage', 'Login failed, please check you have entered the correct password and email address');
		
		echo $tHead->Fetch();		
		echo $tLogin->Fetch();	
		echo $tFoot->Fetch();
	}
	else
	{
		$row = $dBase->GetNextRow($result);
		$id = $row['id'];
		$admin = $row['admin'];
		$firstName = $row['firstname'];
		$forumId = $row['forumid'];
			
		// check we are verified
		$result = $dBase->SelectWhere('user', $arrColumnNames, "email='$email' AND password='$passwordHash' AND verified='1'");
		if (mysql_num_rows($result) != 1)
		{			
			$tLogin->Set('loginMessage', 'Your email has not been verified. <a href="verifyuser.php?id=' .$id . '">Click here</a> to resend the verification email.');
		
			echo $tHead->Fetch();		
			echo $tLogin->Fetch();	
			echo $tFoot->Fetch();
		}

		// set up the cookies, and redirect to main website,
		// does the user need to be notified of login then redirection?
		LogIn($row[1], $id, $admin, $forumId);
	
		$tRedirect = & new PageTemplate();
		
		$redirect = urldecode($_GET['redirect']);

		if (isset($redirect) && strlen($redirect) > 0)
			$tRedirect->Redirect($redirect); // assume this site is secure
		else
			$tRedirect->Redirect('securetransfer.php?redirect=' . $tRedirect->basePath);
	}
	
	$dBase->Disconnect();
?>