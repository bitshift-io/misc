<?php

	include('../../include/template.inc.php');
	
	$tHead = & new PageTemplate('../head.htm');
	$tLogin = & new PageTemplate('login.htm');
	$tFoot = & new PageTemplate('../foot.htm');
	
	$redirect = $_GET['redirect'];
	if (isset($redirect))
	{
		//echo $redirect;
		$tLogin->Set('redirect', '?redirect=' . $redirect); 
	}
	
	echo $tHead->Fetch();
	echo $tLogin->Fetch();
	echo $tFoot->Fetch();
	
?>