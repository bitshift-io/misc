<?php

	include('../../include/login.inc.php');
	include('../../include/template.inc.php');
	include('../../include/global.inc.php');

	// need to chack again name in sandbox mode as there is no https
	global $gSandbox;
	if ($gSandbox && $_SERVER['SERVER_NAME'] == 'bronson.dyndns.org')
		$isSecure = true;
	else
		$isSecure = ($_SERVER['HTTPS'] == 'on');

	// log out of forums
	{
		define('PUN_QUIET_VISIT', 1);

		define('PUN_ROOT', '../forum/');
		require PUN_ROOT.'include/common.php';

		// Remove user from "users online" list.
		$db->query('DELETE FROM '.$db->prefix.'online WHERE user_id='.$pun_user['id']);// or error('Unable to delete from online list', __FILE__, __LINE__, $db->error());

		// Update last_visit (make sure there's something to update it with)
		if (isset($pun_user['logged']))
			$db->query('UPDATE '.$db->prefix.'users SET last_visit='.$pun_user['logged'].' WHERE id='.$pun_user['id']);// or error('Unable to update user visit data', __FILE__, __LINE__, $db->error());
	}

	LogOut();

	// log out of secured site
	if ($isSecure)
	{
		// then log out of unsecure site
		$tRedirect = & new PageTemplate();
		$tRedirect->Redirect($tRedirect->basePath . 'login/logout.php');
	}
	else
	{		
		$tRedirect = & new PageTemplate();
		$tRedirect->Redirect('../index.php');
	}
?>