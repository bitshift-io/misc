<?php

	include ('../../include/dbase.inc.php');
	include('../../include/login.inc.php');
	include('../../include/order.inc.php');
	
	if (!IsAdmin())
		die ("This page is unavailable");
		
	$db = new DBase();
	$db->Connect();
	
	// maxid
	// single
	// none
	// name
	// email
	
	$arrColumnNames = array('uvtoolslicence.maxid', 'user.firstname', 'user.lastname', 'user.email', 'orderform.id');
	$arrTables = array('uvtoolslicence', 'user', 'orderform');
	$result = $db->SelectWhereArray($arrTables, $arrColumnNames, "uvtoolslicence.userid=user.id AND uvtoolslicence.orderid=orderform.id AND uvtoolslicence.type='0' AND orderform.status=" . GetStatusNumber(payment_received));
	
	$db->Disconnect();
	
	header('Content-type: text/plain');
	header('Content-Disposition: attachment; filename="export_uvtools.txt"');

	for ($i = 0; $i < mysql_num_rows($result); ++$i)
	{
		$row = mysql_fetch_array($result, MYSQL_NUM);
			
		echo $row[0] . "\r\n";
		echo "single\r\n";
		echo "none\r\n";
		echo $row[1] . " " . $row[2] . "\r\n";
		echo $row[3] . "\r\n";
		echo $row[4] . "\r\n";
		//echo "\r\n";
	}
	
?>