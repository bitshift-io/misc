<?php

	include_once('../../include/dbase.inc.php');
	include_once('../../include/login.inc.php');
	include_once('../../include/template.inc.php');
	include_once('../../include/order.inc.php');
	include_once('../../include/functions.inc.php');
	
	if (!IsAdmin())
		die ("This page is unavailable");
		
	if (isset($_POST['submit']))
	{	
		$destEmail = "support@blackcarbon.net";
		
		$userfilename = $_FILES['file']['name'];
		$filename = $_FILES['file']['tmp_name'];
		$file = fopen($filename, "r");
		$data = fread($file, filesize($filename));
		$array = explode("\n", $data);
		fclose($file);
		
		$db = new DBase(true);
		$db->Connect();
				
		// get uvtools product id
		$result = $db->SelectWhere('product', array('id'), "name='UV Tools'");
		
		if (mysql_num_rows($result) != 1)
		{
			echo "ERROR PROCESSING - None or multiple uv tools products found<BR>";
			return;
		}
				
		$row = mysql_fetch_array($result, MYSQL_NUM);
		$productId = $row[0];
		
		// for each user, add a download
		for ($i = 0; $i < count($array); $i += 3)
		{			
			$email = trim($array[$i + 0]);
			$orderId = trim($array[$i + 1]);
			$encryptedFile = rtrim($array[$i + 2]);
			
				
						
			// get user id from email
			$result = $db->SelectWhere('user', array('id', 'firstname'), "email='" . $email . "'");
			if ($db->HasError())
			{
				echo "Failed to process: $email<BR>";
				continue;
			}
			
			if (mysql_num_rows($result) != 1)
			{
				echo "Failed to process: $email<BR>";
				continue;
			}
			
			$row = mysql_fetch_array($result, MYSQL_NUM);
			$userId = $row[0];
			$name = $row[1];
			
			
			
			
			// make directory in case it doesnt exist
			if (!file_exists('../../files/' . $email))
				mkdir('../../files/' . $email);
			
			// write out encrypted file
			$c = 0;
			do
			{
				$fileName = 'licence' . $c . '.dat';
				$filePath = '../../files/' . $email . '/' . $fileName;
				++$c;
			} while (file_exists($filePath));
			
			$fileHandle = fopen($filePath, 'w');
			fwrite($fileHandle, $encryptedFile);
				
				
				
						
			// make sure order id is 1
			$result = $db->SelectWhere('orderform', array('status'), "id='" . $orderId . "'");
			if (mysql_num_rows($result) != 1)
			{
				echo "Failed to process: $email, multiple order id found: $orderId<BR>";
				continue;
			}
			
			$row = mysql_fetch_array($result, MYSQL_NUM);
			if ($row[0] != GetStatusNumber(payment_received))
			{
				echo "Failed to process: $email, already processed order id: $orderId<BR>";
				continue;
			}
			
			$db->StartTransaction();
			
			
			// add a download for the new file
			$downloadId = $db->Insert('download', array('path' => $email . '/' . $fileName, 'filename' => 'licence.dat', 'loginrequired' => '1', 'productid' => $productId));
			if ($db->HasError())
			{
				echo "Failed to process: $email, failed to insert download<BR>";
				$db->ErrorMessage();
				$db->RollbackTransaction();
				continue;
			}
						
			// add a user download
			$db->Insert('userdownload', array('userid' => $userId, 'downloadid' => $downloadId, 'orderid' => $orderId));	
			if ($db->HasError())
			{
				echo "Failed to process: $email, failed to add user download<BR>";
				$db->ErrorMessage();
				$db->RollbackTransaction();
				continue;
			}	
			
			// update order id to 2
			$db->Update('orderform', array('status' => GetStatusNumber(order_complete)), "id='" . $orderId . "'");
			if ($db->HasError())
			{
				echo "Failed to process: $email, failed to update status<BR>";
				$db->ErrorMessage();
				$db->RollbackTransaction();
				continue;
			}
			
			// send an email to notify them... another happy customer $.$
			$msg = $name . ",\r\n\r\nUV Tools is now available to download from www.blackcarbon.net.\r\nSimply login and then go to your profile page. Download licence.dat from your order (order id: $orderId) and save to \\autodesk\\3dsmax8\\scripts\\ folder.\r\n\r\nThanks,\r\nBlack Carbon";			
			$mailSent = SendMail($email, 'UV Tools is available for download', $msg, $destEmail);
			echo $mailSent ? "Mail sent<br>" : "Mail failed<br>";	
			
			$db->CommitTransaction();
			echo "Processed: $email<BR>";
		}
		
		$db->Disconnect();
	}
	
	$tBody = & new PageTemplate('import_uvtools.htm');
	echo $tBody->Fetch();
?>