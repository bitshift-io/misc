<?php

	include_once('../../include/template.inc.php');
	include_once('../../include/login.inc.php');
	
	$t = new PageTemplate;
	
	
	if (IsAdmin())
	{		
		$page = $_POST['page'];
		
		if (!isset($page) || strlen($page) <= 0)
			$page = 'index.htm';
		
		$tHead = & new PageTemplate('../head.htm');
		$tBody = & new PageTemplate('../../admin/' . $page);
		$tFoot = & new PageTemplate('../foot.htm');
		
		$tHead->Set('admin', IsAdmin());
		$tHead->Set('login', IsLoggedIn());
		$tFoot->Set('login', IsLoggedIn());		
		
		echo $tHead->Fetch();
		echo $tBody->Fetch();
		echo $tFoot->Fetch();
		
	}
	else if (IsLoggedIn())
	{
		$t->Redirect('../profile.php');
	}
	else
	{
		$t->Redirect('../products.php');
	}
	
?>