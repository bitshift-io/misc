<?php

	/*  PHP Paypal IPN Integration Class Demonstration File
	 *  4.16.2005 - Micah Carrick, email@micahcarrick.com
	 *
	 *  This file demonstrates the usage of paypal.class.php, a class designed  
	 *  to aid in the interfacing between your website, paypal, and the instant
	 *  payment notification (IPN) interface.  This single file serves as 4 
	 *  virtual pages depending on the "action" varialble passed in the URL. It's
	 *  the processing page which processes form data being submitted to paypal, it
	 *  is the page paypal returns a user to upon success, it's the page paypal
	 *  returns a user to upon canceling an order, and finally, it's the page that
	 *  handles the IPN request from Paypal.
	 *
	 *  I tried to comment this file, aswell as the acutall class file, as well as
	 *  I possibly could.  Please email me with questions, comments, and suggestions.
	 *  See the header of paypal.class.php for additional resources and information.
	 */

	require_once('../../include/paypal.inc.php');  
	require_once('../../include/dbase.inc.php');
	require_once('../../include/login.inc.php');
	require_once('../../include/template.inc.php');
	require_once('../../include/order.inc.php');
	require_once('../../include/functions.inc.php');
	
	$p = new Paypal;             // initiate an instance of the class
	//$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
	//$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
	            
	// setup a variable for this script (ie: 'http://www.blackcarbon.com/php/paypal.php')
	$this_script = $paypal[site_url] . $_SERVER['PHP_SELF'];
	
	$destEmail = "support@blackcarbon.net";

	// if there is not action variable, set the default action of 'process'
	if (empty($_GET['action'])) 
		die('This page is not directly accessable');;  


	switch ($_GET['action']) 
	{   
	case 'existingorder':
		{
			$orderId = $_GET['id'];
			
			$db = new DBase();
			$db->Connect();
			
			$login = GetLogin();
			$result = $db->SelectWhereArray(array('product', 'orderform'), array('product.name'), "product.id=orderform.productid AND orderform.id='$orderId' AND orderform.userid='$login->uid'");
			if (mysql_num_rows($result) != 1)
				die ('An error has occured placing your order');
				
			$row = mysql_fetch_array($result, MYSQL_NUM);
			$productName = $row[0];
				
			$db->Disconnect();
			
			$tHead = & new PageTemplate('../head.htm');
			$tBody = & new PageTemplate('existingorder.htm');
			$tFoot = & new PageTemplate('../foot.htm');
			
			$tHead->Set('login', IsLoggedIn());
			$tBody->Set('login', IsLoggedIn());
			$tFoot->Set('login', IsLoggedIn());
			
			if (IsLoggedIn())
			{
				$login = GetLogin();
				$tBody->Set('username', $login->GetUserName());
				$tFoot->Set('username', $login->GetUserName());
			}
			
			$tBody->Set('productname', $productName);
			$tBody->Set('orderid', $orderId);
						
			echo $tHead->Fetch();
			echo $tBody->Fetch();
			echo $tFoot->Fetch();
		}
		break;
		
	case 'process':      // Process and order...
		{	
		
			// There should be no output at this point.  To process the POST data,
			// the submit_paypal_post() function will output all the HTML tags which
			// contains a FORM which is submited instantaneously using the BODY onload
			// attribute.  In other words, don't echo or printf anything when you're
			// going to be calling the submit_paypal_post() function.

			// This is where you would have your form validation  and all that jazz.
			// You would take your POST vars and load them into the class like below,
			// only using the POST values instead of constant string expressions.

			// For example, after ensureing all the POST variables from your custom
			// order form are valid, you might have:
			//
			// $p->add_field('first_name', $_POST['first_name']);
			// $p->add_field('last_name', $_POST['last_name']);

			$orderId = $_GET['id'];
			
			$db = new DBase();
			$db->Connect();
			
			$login = GetLogin();
			$result = $db->SelectWhereArray(array('product', 'orderform'), array('product.name', 'product.price'), "product.id=orderform.productid AND orderform.id='$orderId' AND orderform.userid='$login->uid'");
			if (mysql_num_rows($result) != 1)
				die ('An error has occured placing your order');
			
			$db->Disconnect();
				
			$row = mysql_fetch_array($result, MYSQL_NUM);
			$name = $row[0];
			$price = $row[1];
			
			$p->AddField('item_name', $name);
			$p->AddField('invoice', $orderId);
			$p->AddField('custom', $orderId);
			$p->AddField('amount', $price);
			$p->AddField('quantity', $number);	
			$p->AddField('on0', $orderId);		

			$p->AddField('return', $this_script.'?action=success&id=' . $orderId);
			$p->AddField('success_url', $this_script.'?action=success&id=' . $orderId);
			$p->AddField('cancel_url', $this_script.'?action=cancel&id=' . $orderId);
			$p->AddField('notify_url', $this_script.'?action=ipn&id=' . $orderId);
						
			//$p->DumpFields();      // for debugging, output a table of all the fields	
			
			$tHead = & new PageTemplate('../head.htm');
			$tBody = & new PageTemplate('processing.htm');
			$tFoot = & new PageTemplate('../foot.htm');
			
			$tHead->Set('login', IsLoggedIn());
			$tBody->Set('login', IsLoggedIn());
			$tFoot->Set('login', IsLoggedIn());
			
			if (IsLoggedIn())
			{
				$login = GetLogin();
				$tBody->Set('username', $login->GetUserName());
				$tFoot->Set('username', $login->GetUserName());
			}
			
			$tBody->Set('fields', $p->GetFields());
			$tBody->Set('paypal_url', $p->paypal_url);
			
			echo $tHead->Fetch();
			echo $tBody->Fetch();
			echo $tFoot->Fetch();     
		}
		break;
	      
	case 'success':      // Order was successful...
		{
			$orderId = $_GET['id'];
			
			if (!IsLoggedIn())
			{
				$tRedirect = & new PageTemplate();
				$tRedirect->Redirect($tRedirect->secureBasePath . 'login/index.php?redirect=' . urlencode($this_script . '?action=success&id=' . $orderId));	
				return;
			}

			$db = new DBase();
			$db->Connect();

						
			// make sure we can only succed our own order
			$login = GetLogin();

			$result = $db->SelectWhere('orderform', array('status'), "id='$orderId' AND userid='$login->uid'");
			if (mysql_num_rows($result) != 1)
				die ('An error has occured placing your order');
				
			$row = mysql_fetch_array($result, MYSQL_NUM);
			$orderStatus = $row[0];
			
			// dont update the status if the order is already finished or paypal payment has already been approved
			if (GetEnumFromNumber($orderStatus) != payment_received 
				&& GetEnumFromNumber($orderStatus) != order_complete)
			{		
				$arrSetPairs = array('status' => GetStatusNumber(returned_from_paypal_success));
				$db->Update('orderform', $arrSetPairs, "id='$orderId'");
			}
			
			$db->Disconnect();
				
										
			$tHead = & new PageTemplate('../head.htm');
			$tBody = & new PageTemplate('success.htm');
			$tFoot = & new PageTemplate('../foot.htm');
			
			$tHead->Set('login', IsLoggedIn());
			$tBody->Set('login', IsLoggedIn());
			$tFoot->Set('login', IsLoggedIn());
			
			if (IsLoggedIn())
			{
				$login = GetLogin();
				$tBody->Set('username', $login->GetUserName());
				$tFoot->Set('username', $login->GetUserName());
			}
			
			$tBody->Set('orderId', $orderId);
			
			echo $tHead->Fetch();
			echo $tBody->Fetch();
			echo $tFoot->Fetch();
		}
		break;
	      
	case 'cancel':       // Order was canceled...
		{		
			$orderId = $_GET['id'];
			
			$db = new DBase();
			$db->Connect();
			
			// make sure we are deleting a bad order that belongs to us!
			$login = GetLogin();
			$result = $db->SelectWhere('orderform', array('status'), "id='$orderId' AND userid='$login->uid'");
			if (mysql_num_rows($result) != 1)
				die ('An error has occured placing your order');
				
			$row = mysql_fetch_array($result, MYSQL_NUM);
			$orderStatus = $row[0];
			if (GetEnumFromNumber($orderStatus) != order_created)
				die('An error has occured deleting your order');
			
			$db->DeleteWhere('orderform', "id='$orderId' AND userid='$login->uid'");
			
			$db->Disconnect();
			
				
			$tHead = & new PageTemplate('../head.htm');
			$tBody = & new PageTemplate('cancel.htm');
			$tFoot = & new PageTemplate('../foot.htm');
			
			$tHead->Set('login', IsLoggedIn());
			$tBody->Set('login', IsLoggedIn());
			$tFoot->Set('login', IsLoggedIn());
			
			if (IsLoggedIn())
			{
				$login = GetLogin();
				$tBody->Set('username', $login->GetUserName());
				$tFoot->Set('username', $login->GetUserName());
			}
			
			$tBody->Set('orderId', $orderId);
			
			echo $tHead->Fetch();
			echo $tBody->Fetch();
			echo $tFoot->Fetch();
		}
		break;
	      
	case 'ipn':          // Paypal is calling page for IPN validation...
		{			
			// It's important to remember that paypal calling this script.  There
			// is no output here.  This is where you validate the IPN data and if it's
			// valid, update your database to signify that the user has payed.  If
			// you try and use an echo or printf function here it's not going to do you
			// a bit of good.  This is on the "backend".  That is why, by default, the
			// class logs all IPN data to a text file.


			$orderId = $_GET['id'];

			// Write to log
			$fp = fopen('.paypal_ipn.log','a');		  
			fwrite($fp, "IPN RECEIVED - ". $orderId ."\n");

			if (!$p->ValidateIPN()) 
			{
				// For this example, we'll just email ourselves ALL the data.
				$subject = '[Black Carbon][IPN] Failed';
				$to = $destEmail;    //  your email
				$body =  "An instant payment notification was cancelled\n";
				$body .= "from ".$p->ipn_data['payer_email']." on ".date('m/d/Y');
				$body .= " at ".date('g:i A')."\n\nDetails:\n";

				foreach ($p->ipn_data as $key => $value) { $body .= "\n$key: $value"; }
				SendMail($to, $subject, $body, $destEmail);

				fwrite($fp, "IPN Invalid\n");
				return;
			}

			
			fwrite($fp, "IPN Valid\n");

			//include file - not accessible directly
			if (!isset($_POST['business']))
			{	
				fwrite($fp, "business not set, page failed\n");
				die('This page is not directly accessible');				
			}

			
			// Payment has been recieved and IPN is verified.  This is where you
			// update your database to activate or process the order, or setup
			// the database with the user's order details, email an administrator,
			// etc.  You can access a slew of information via the ipn_data() array.

			// Check the paypal documentation for specifics on what information
			// is available in the IPN POST variables.  Basically, all the POST vars
			// which paypal sends, which we send back for validation, are now stored
			// in the ipn_data() array.

			// For this example, we'll just email ourselves ALL the data.
			$subject = '[Black Carbon][IPN] Recieved Payment';
			$to = $destEmail;    //  your email
			$body =  "An instant payment notification was successfully recieved\n";
			$body .= "from ".$p->ipn_data['payer_email']." on ".date('m/d/Y');
			$body .= " at ".date('g:i A')."\n\nDetails:\n";

			foreach ($p->ipn_data as $key => $value) { $body .= "\n$key: $value"; }
			SendMail($to, $subject, $body, $destEmail);

			fwrite($fp, "payment received email sent\n");

			//log successful transaction to file or database
			if (strcasecmp($_POST['payment_status'], "Completed") == 0)
			{				
				fwrite($fp, "payment status complete for " . $orderId . "\n");

				$db = new DBase();
				$db->Connect();			
							
				$customOrderId = $_POST['custom']; //$_POST['invoice'];
				if ($orderId != $customOrderId)
				{
					// order error, multiple orders with same id
					fwrite($fp, "custom 'id' != order id sent\n");					
					die('An error has occured with your order id $destEmail');					
				}

				$orderId = $customOrderId;
				$r = $db->SelectWhere('orderform', array('*'), "id='" . $orderId . "'");				
				if (mysql_num_rows($r) != 1)
				{
					// order error, multiple orders with same id
					fwrite($fp, "order error, multiple orders with same id\n");					
					die('A database error has occured, please contact $destEmail');					
				}

				$row = mysql_fetch_array($r, MYSQL_NUM);		
				$payPalId = $row[5];
				$number = $row[4];			
				$userId = $row[2];
				$productId = $row[1];
								
				// make sure order id is not already processed
				if (strcmp($payPalId, $_POST['txn_id']) == 0)
				{
					// order error, multiple orders with same id
					fwrite($fp, "order error, order id already processed\n");					
					die('order error, order id already processed, please contact $destEmail');					
				}

				$payPalId = $_POST['txn_id'];
				
				// update the amount payed
				$payment = $_POST['payment_gross'];
				
				// send the user a notifiaction thier payment has been accepted
				$arrColumnNames = array('id', 'firstname', 'email');
				$r = $db->SelectWhere('user', $arrColumnNames, "id='$userId'");
				if (mysql_num_rows($r) != 1)
				{
					fwrite($fp, "multiple users found wit the same id\n\n");
					die("A datavbase error has occured, please contact $destEmail");
				}

				$row = mysql_fetch_array($r, MYSQL_NUM);
				$email = $row[2];
				$name = $row[1];

				// verify the payment
				$status = payment_received;
				$r = $db->SelectWhere('product', array('price', 'ordercompletepage'), "id='$productId'");					
				if (mysql_num_rows($r) != 1)
				{
					fwrite($fp, "order error, multiple products found\n");		
					$status = payment_received_order_error;
				}
					
				$row = mysql_fetch_array($r, MYSQL_NUM);
				if (($number * $row[0]) != $payment)
				{
					fwrite($fp, "order error, money error, amount payed not equal to amount asked for\n");	
					$status = payment_received_order_error;
				}
				
				// now we need to tell the product the order was complete
				$orderCompletePage = $row[1];
				fwrite($fp, "order page for product - ../". $orderCompletePage ."\n");	

				if (strlen($orderCompletePage) > 0 && $status != payment_received_order_error)
				{
					fwrite($fp, "calling order complete page\n");	

					if (!include('../' . $orderCompletePage)) //include('../explosive/ordercomplete.php'))
					{
						fwrite($fp, "failed to include order complete page\n");
						$status = payment_received_order_error;
					}
					else
					{
						$status = NotifyOrderComplete($db, $productId, $userId, $orderId, $payPalId, $email, $name, $destEmail);
						fwrite($fp, "order complete page called\n");	
					}
				}
				else
				{
					fwrite($fp, "no order complete page\n");
				}
					
				
				$arrSetPairs = array('paypalid' => $payPalId, 'status' => GetStatusNumber($status), 'amount' => $payment);
				$db->Update('orderform', $arrSetPairs, "id='$orderId'");

				if ($status == payment_received || $status == order_complete)
				{
					$msg = $name . ",\r\n\r\nYour order (invoice number: " . $orderId . ") has succesfully been processed.\r\n\r\nThanks,\r\nBlack Carbon";
				}
				else
				{
					$msg = $name . ",\r\n\r\nYour order (invoice number: " . $orderId . ") has been processed, how ever an error has occured. We will contact you about the problem.\r\n\r\nThanks,\r\nBlack Carbon";
					
					// email me so i know what to look for!
					$subject = '[Black Carbon][IPN] Error with order';
					$to = $destEmail;    //  your email
					$body =  "An instant payment notification was cancelled\n";
					$body .= "from ".$p->ipn_data['payer_email']." on ".date('m/d/Y');
					$body .= " at ".date('g:i A')."\n\nDetails:\n";

					foreach ($p->ipn_data as $key => $value) { $body .= "\n$key: $value"; }
					SendMail($to, $subject, $body, $destEmail);
				}
				
				SendMail($email, '[Black Carbon] Your order has been processed', $msg, $destEmail);
			}
			else
			{
				$subject = '[Black Carbon][IPN] Recieved Status: '. $_POST['payment_status'];
				$to = $destEmail;    //  your email
				$body =  "An instant payment notification was cancelled\n";
				$body .= "from ".$p->ipn_data['payer_email']." on ".date('m/d/Y');
				$body .= " at ".date('g:i A')."\n\nDetails:\n";

				foreach ($p->ipn_data as $key => $value) { $body .= "\n$key: $value"; }
				SendMail($to, $subject, $body, $destEmail);

				fwrite($fp, "error with payment status, email sent\n");
			}

			fwrite($fp, "\n");
			fclose($fp);  // close file
		}
		break;
		
	case 'emailtest':
		{
			echo "sending a test email....<br>";
			
			$subject = '[Black Carbon] paypal test';
			$to = $destEmail;    //  your email
			$body =  "testing sending email from paypal page\n";
			$body .= " on ".date('m/d/Y');
			$body .= " at ".date('g:i A')."\n\n";

			SendMail($to, $subject, $body, $destEmail);
		}
		break;
	}     

?>