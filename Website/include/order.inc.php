<?php 

	// insert in to the dbase this order
	//include('../../include/dbase.inc.php');
	//include('../../include/login.inc.php');
	
	function GetEnumFromNumber($var)
	{
		switch ($var)
		{
		case -1;
			return order_error;
		case 0:
			return order_created;
		case 1:
			return returned_from_paypal_success;
		case 2:
			return order_cancelled;
		case 3:
			return order_complete;
		case 4:
			return payment_received;
		case 5:
			return payment_received_order_error;
		}
	}
	
	function GetStatusNumber($var)
	{
		switch ($var)
		{
		case order_error:
			return -1;
		case order_created:
			return 0;
		case returned_from_paypal_success:
			return 1;
		case order_cancelled:
			return 2;
		case order_complete:
			return 3;
		case payment_received:
			return 4;
		case payment_received_order_error:
			return 5;
		}
	}
	
	function GetOrderStatusText($number)
	{
		switch ($number)
		{
		case -1:
			return "an order error has occured, order will be investigated";
		case 0:
			return "order created, payment to paypal has not been complete";
		case 1:
			return "payment made to paypal, waiting for paypal approval";
		case 2:
			return "order cancelled";
		case 3:
			return "order complete";
		case 4:
			return "payment received";
		case 5:
			return "payment received, an order error has occured";
		}
	}

	function HasOutstandingOrder($db, $productname)
	{
		if (!IsLoggedIn())
			return -1;
		
		$login = GetLogin();

		$result = $db->SelectWhere('product', array('*'), "name='$productname'");
		if (mysql_num_rows($result) != 1)
			die ('An invalid product has been specified.');
			
		$row = mysql_fetch_array($result, MYSQL_NUM);
		$productid = $row[0];
			
		$result = $db->SelectWhere('orderform', array('id', 'status'), "userid='$login->uid' AND productid='$productid'");
		if (mysql_num_rows($result) <= 0)
			return -1;
						
		$row = mysql_fetch_array($result, MYSQL_NUM);
		$orderId = $row[0];
		$status = $row[1];
		
		if ($status == GetStatusNumber(order_cancelled)
			|| $status == GetStatusNumber(order_complete))
			return -1;
		
		return $orderId;
	}

	// insert order in to dbase,
	// then procced to paypal
	function CreateOrder($db, $productname, $number)
	{     
		$result = $db->SelectWhere('product', array('*'), "name='$productname'");

		if (mysql_num_rows($result) != 1)
			die ('An invalid product has been specified.');

		$row = mysql_fetch_array($result, MYSQL_NUM);
		$productid = $row[0];
		$name = $row[1];
		$price = $row[2];

		$login = GetLogin();
		$arrValuePairs = array('productid' => $productid, 'number' => $number, 'userid' => $login->GetUID(), 'date' => 'NOW()', 'status' => GetStatusNumber(order_created));
		$result = $db->Insert('orderform', $arrValuePairs);
		$invoice = mysql_insert_id();

		return $invoice; // order id
	}
	  
	function PlaceOrder($orderId)
	{
		$tRedirect = & new PageTemplate();
		$tRedirect->Redirect($tRedirect->secureBasePath . 'paypal/paypal.php?action=process&id=' . $orderId);
	}
	
	function ShowExistingOrderPage($orderId)
	{
		$tRedirect = & new PageTemplate();
		$tRedirect->Redirect($tRedirect->secureBasePath . 'paypal/paypal.php?action=existingorder&id=' . $orderId);
	}
  
?>