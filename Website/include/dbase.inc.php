<?php

include_once('global.inc.php');
include_once('functions.inc.php');



class DBase
{
	var $debug;
	var $debugEmail;
	var $ident;

	function DBase($d = false, $dEmail = '')
	{
		$this->debug = $d;
		$this->debugEmail = $dEmail;
	}
	

	function ErrorMessage()
	{
		$msg = mysql_error();
		if (strlen($msg) > 0)
		{
			if (strlen($this->debugEmail) > 0)
				SendMail($this->debugEmail, "SQL Error", $msg, $this->debugEmail);
			else
				echo $msg;
		}
	}
	
	function HasError()
	{
		$msg = mysql_error();
		if (strlen($msg) > 0)
			return true;
			
		return false;
	}
	
	function DebugMessage($msg)
	{
		if ($this->debug)
		{
			if (strlen($this->debugEmail) > 0)
				SendMail($this->debugEmail, "SQL Debug", $msg, $this->debugEmail);
			else
				echo $msg . "<BR>";
		}
	}
	
	function Connect()
	{
    global $GB;
  
		// Connect to MySQL
    //echo "host: " . $GB['db']['host'] . " user: " . $GB['db']['user'] . " pwd: " . $GB['db']['pwd'];
		$this->ident = mysql_connect($GB['db']['host'], $GB['db']['user'], $GB['db']['pwd']);
		if (!$this->ident)
		{
			die("Failed to connect to DB. SQL Error: " . mysql_error());
		}

		// Select assigned DB
		if (!mysql_select_db($GB['db']['db'])) 
		{
			die("Could not select DB");
			//die("Could not connect to DB: " . mysql_error());
		}
	}

	function Disconnect()
	{
		// Close the connection
		if (!mysql_close($this->ident)) 
		{
			die("Could not close DB");
		}
	}
	
	function StartTransaction()
	{
		$this->DebugMessage("START TRANSACTION<br>");
		mysql_query("START TRANSACTION"); 
	}
	
	function CommitTransaction()
	{
		$this->DebugMessage("COMMIT<br>");
		mysql_query("COMMIT");
	}
	
	function RollbackTransaction()
	{
		$this->DebugMessage("ROLLBACK<br>");
		mysql_query("ROLLBACK");
	}
  
  function Insert($strTableName, $arrValuePairs)
  {
    $strSeparator = '';
    $strCols = '';
    $strValues = '';

    foreach ($arrValuePairs as $strCol => $strValue) 
    {
      $strCols = $strCols . $strSeparator . $strCol;

      if ($strValue == 'CURDATE()' || $strValue == 'NOW()')
       $strValues = $strValues . $strSeparator . mysql_real_escape_string($strValue);
      else
        $strValues = $strValues . $strSeparator . "'" . mysql_real_escape_string($strValue) . "'";

      $strSeparator = ',';
    }

    $this->DebugMessage("Insert: INSERT INTO `$strTableName`($strCols) VALUES ($strValues)<br>");

    mysql_query("INSERT INTO `$strTableName`($strCols) VALUES ($strValues)");
    return mysql_insert_id();
  }
  
	function Select($strTableName, $arrColumnNames)
	{		
		$strSeparator = '';
		$strCols = '';
		$strValues = '';

		foreach ($arrColumnNames as $strCol => $strValue) 
		{
		   $strCols = $strCols . $strSeparator . $strCol;
		   $strValues = $strValues . $strSeparator . mysql_real_escape_string($strValue);
		   $strSeparator = ',';
		}

		$this->DebugMessage("Select: SELECT $strValues FROM `" . $strTableName . "`<BR>");
			
		$result = mysql_query("SELECT $strValues FROM `" . $strTableName . "`");			
		return $result;
	}
	
	function SelectWhere($strTableName, $arrColumnNames, $strSelectCondition)
	{		
		$strSeparator = '';
		$strCols = '';
		$strValues = '';

		foreach ($arrColumnNames as $strCol => $strValue) 
		{
		   $strCols = $strCols . $strSeparator . $strCol;
		   $strValues = $strValues . $strSeparator . mysql_real_escape_string($strValue);
		   $strSeparator = ',';
		}
	   
		$this->DebugMessage("SelectWhere: SELECT $strValues FROM `" . $strTableName . "` WHERE " . $strSelectCondition . "<BR>");
				
		$result = mysql_query("SELECT $strValues FROM `" . $strTableName . "` WHERE " . $strSelectCondition);
		return $result;
	}
	
	function DeleteWhere($strTableName, $strSelectCondition)
	{			   
		$this->DebugMessage("SelectWhere: DELETE FROM `" . $strTableName . "` WHERE " . $strSelectCondition . "<BR>");
				
		$result = mysql_query("DELETE FROM `" . $strTableName . "` WHERE " . $strSelectCondition);
		return $result;
	}
	
	function SelectWhereArray($arrTables, $arrColumnNames, $strSelectCondition)
	{		
		$strSeparator = '';
		$strCols = '';
		$strValues = '';

		foreach ($arrColumnNames as $strCol => $strValue) 
		{
		   $strCols = $strCols . $strSeparator . $strCol;
		   $strValues = $strValues . $strSeparator . mysql_real_escape_string($strValue);
		   $strSeparator = ',';
		}
		
		$strSeparator = '';
		$strCols = '';
		$strFrom = '';
		foreach ($arrTables as $strTable => $strValue2) 
		{
		   $strCols = $strCols . $strSeparator . $strTable;
		   $strFrom = $strFrom . $strSeparator . mysql_real_escape_string($strValue2);
		   $strSeparator = ',';
		}
		
	   
		$this->DebugMessage("SelectWhere: SELECT $strValues FROM $strFrom WHERE $strSelectCondition<BR>");
				
		$result = mysql_query("SELECT $strValues FROM $strFrom WHERE $strSelectCondition");
		return $result;
	}
	
	function Update($strTableName, $arrSetPairs, $strSelectCondition)
	{
		$strSeparator = '';
		$strValues = '';
		
		foreach ($arrSetPairs as $strCol => $strValue) 
		{
			$strValues = $strValues . $strSeparator . $strCol . '=\'' . mysql_real_escape_string($strValue) . '\'';
			$strSeparator = ',';
		}
		   
		$this->DebugMessage("Update: UPDATE `$strTableName` SET " . $strValues . " WHERE " . $strSelectCondition . "<BR>");
			
		$result = mysql_query("UPDATE `$strTableName` SET " . $strValues . " WHERE " . $strSelectCondition);
		return $result;
	}
	
	function GetNumRows($result)
	{
		if (!$result)
			return 0;

		return mysql_num_rows($result);
	}
	
	function GetNextRow($result)
	{
		return mysql_fetch_assoc($result);
	}
}

?>