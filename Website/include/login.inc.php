<?php

include_once('global.inc.php');

//
// login methods
//
class Login
{
	var $logedIn = false;
	var $userName = "Guest";
	var $uid = -1;
	var $admin = false;
	var $ip;

	var $uidForum = 1;
	
	function GetUserName()
	{
		return $this->userName;
	}

	function GetUID()
	{	
		return $this->uid;
	}
}


function LogIn($userName, $uid, $admin, $uidForum)
{
	session_start();
	$login = new Login();	
	$login->userName = $userName;
	$login->logedIn = true;
	$login->uid = $uid;
	$login->admin = $admin;
	$login->ip = $_SERVER['REMOTE_ADDR'];
	$login->uidForum = $uidForum;

	session_start();
	$_SESSION['login'] = $login;
	return $login;
}

function LogOut()
{
	session_start();
	unset($_SESSION['login']);
}

function IsLoggedIn()
{
	session_start();

	if (isset($_SESSION['login']))
	{
		$login = GetLogin();
		return $login->ip == $_SERVER['REMOTE_ADDR'];
	}

	return false;
}

function IsAdmin()
{
	if (IsLoggedIn())
	{
		$login = GetLogin();
		return $login->admin;
	}
	
	return false;
}

function GetLogin()
{
	session_start();
	return $_SESSION['login'];
}


?>