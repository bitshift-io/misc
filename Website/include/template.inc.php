<?php

// http://www.sitepoint.com/article/beyond-template-engine/2

include('global.inc.php');

class PageTemplate 
{
  var $vars; /// Holds all the template variables
  var $basePath;
  var $secureBasePath;
  var $file;

  function PageTemplate($file = null) 
  {	
    $this->basePath = $GB['url'];
    $this->secureBasePath = $GB['urlSecure'];

    $this->file = $file;	

    $this->Set('basePath', $this->basePath);
    $this->Set('secureBasePath', $this->secureBasePath);	
  }

  function Set($name, $value) 
  {		
    $this->vars[$name] = is_object($value) ? $value->fetch() : $value;
  }

  function FindCharReverse($str, $char, $pos)
  {
    for ($i = $pos; $i >= 0; $i--)
    {
      if ($str[$i] == $char)
        return $i;
    }

    return -1;
  }
  
  // process SSI (server side includes)
  // as php doesnt do that when we just include the file
  // so need to do it manually
  function PreprocessSSI($file)
  {  
    if ($file[0] == '/')
      $file = $_SERVER[DOCUMENT_ROOT] . $file;
          
    $preContents = file_get_contents($file);
    if ($preContents == FALSE)
    {
      echo "failed to open<BR>";
      return;
    }
    
    $contents = "";
        
    $pos = 0;
    $prevPos = 0;
    $prefix = "<!--";
    $prefixLen = strlen($prefix);
    $startStr = "#include virtual=\"";
    $startStrLen = strlen($startStr);
    $endStr = "\"-->";
    $endStrLen = strlen($endStr);

    while (($pos = stripos($preContents, $startStr, $prevPos)) != FALSE)
    {     
      // add and string before <!--#include virtual="
      $str = substr($preContents, $prevPos, ($pos - $prefixLen) - $prevPos);
      $contents .= $str;

      $pos += $startStrLen;
      $nextPos = stripos($preContents, $endStr, $pos);
      $ssi = substr($preContents, $pos, $nextPos - $pos);
      
      // include the include file
      $contents .= $this->PreprocessSSI($ssi);
             
      $prevPos = $nextPos + $endStrLen;
    }
    
    $pos = strlen($preContents);
    $str = substr($preContents, $prevPos, $pos - $prevPos);
    $contents .= $str;
    
    return $contents;
  }
  
  // searches for .shtml links,
  // if found, and a php file of the same name exists
  // it will be redirected to the php file
  function PreprocessURL($preContents)
  {
    $contents = "";
    
    $pos = 0;
    $prevPos = 0;
    
    $shtmlStr = ".shtml";
    $shtmlStrLen = strlen($shtmlStr);
    
    $phpStr = ".php";
    $phpStrLen = strlen($phpStr);
    
    while (($pos = stripos($preContents, $shtmlStr, $prevPos)) != FALSE)
    {
      $str = substr($preContents, $prevPos, $pos - $prevPos);
      $contents .= $str;
      
      $startPos = $this->FindCharReverse($preContents, '"', $pos);
      if ($startPos == -1)
      {
        $prevPos = $pos + $shtmlStrLen;
        continue;
      }
      
      $file = substr($preContents, $startPos + 1, $pos - $startPos - 1);
      
      if ($file[0] == '/')
        $file = $_SERVER[DOCUMENT_ROOT] . $file;
      
      if (file_exists($file . $phpStr))
        $contents .= $phpStr;
      else
        $contents .= $shtmlStr;
        
      $prevPos = $pos + $shtmlStrLen;      
    }
    
    $pos = strlen($preContents);
    $str = substr($preContents, $prevPos, $pos - $prevPos);
    $contents .= $str;
    
    return $contents;
  }
  
  function Fetch($file = null) 
  {
    if (!$file) 
     $file = $this->file;

    extract($this->vars);      // Extract the vars to local namespace

    ob_start();                    // Start output buffering
    
    //if (!file_exists("_" . $file) || (filemtime($file) > filemtime($file)))
    {
      $contents = $this->PreprocessSSI($file);
      $contents = $this->PreprocessURL($contents);
      $outFile = fopen($file . ".cache", "w");
      fwrite($outFile, $contents);
    }

    require($file . ".cache");                // Include the file
    $contents = ob_get_contents(); // Get the contents of the buffer
    ob_end_clean();                // End buffering and discard        
    return $contents;              // Return the contents
  }
  
  // Use this to capture PHP page results
  // for example, if your trying to oncporporate a forum in to your website
  // do:
  // GetContentsBegin
  // ..forum code runs here
  // $result = GetContentsEnd
  // you can then wrap the $result in other code using Fetch
  function GetContentsBegin()
  {
    extract($this->vars);      // Extract the vars to local namespace

    ob_start();                    // Start output buffering
  }
  
  function GetContentsEnd()
  {
    $contents = ob_get_contents(); // Get the contents of the buffer
    ob_end_clean();                // End buffering and discard        
    return $contents;              // Return the contents
  }
    
  function Redirect($file = null)
  {
    session_write_close();

    if (!$file) 
      $file = $this->file;

    header ('Location: ' . $file);
  }
}

?>