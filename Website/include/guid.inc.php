<?php
   class Guid 
   {
       var $guidText;

       function Guid()
       {
           $this->guidText = md5(uniqid(rand(),true));
           return $this->guidText;
       }

       function ToString($separator = '', $case = false)
       {
           $str =& $this->guidText;
           if ($case)
           {
               switch ($case)
               {
               case 'uc':
                 $str = strtoupper($str);
               break;
               case 'lc':
                 $str = strtolower($str);
               break;
               default:
                 $str = $str;
               }
           }

           $str = substr($str,0,8) . $separator .
               substr($str,8,8) . $separator .
               substr($str,16,8). $separator .
               substr($str,24,8);
           return htmlspecialchars($str);
       }

       // do some other stuff
   }

   //$obj = new Guid();
   //echo '<br>Dash: ' . $obj->ToString('-');
?>
