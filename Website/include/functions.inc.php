<?php

include_once("global.inc.php");

if (is_file("Mail.php"))
{
	require_once "Mail.php"; // this is globally on the server i think
}
 
function SendMail($to, $subject, $body, $from)
{	
  global $GB;

  $headers = 'From: ' . $from . "\r\n" .
      'MIME-Version: 1.0' . "\r\n" .
      'Content-type: text/html; charset=iso-8859-1';

  // just return true on local machine as mail wont work
  if ($GB['sandbox'])
  {
    // open the log file and check if the it's opened successfully
    if (!($fp = fopen('c:\email.html', 'a'))) 
    {
      die('Cannot open log file');
    }
 
    fwrite($fp, "<br><br>TO:");
    fwrite($fp, $to);
    fwrite($fp, "<br>FROM:");
    fwrite($fp, $from); 
    fwrite($fp, "<br>SUBJECT:");
    fwrite($fp, $subject);
    fwrite($fp, "<br>BODY:<br>");
    fwrite($fp, $body);
    fclose($fp);
    return true;
  }

 // $headers = "From: " . $from;
 return mail($to, $subject, $body, $headers);


/*
	$host = "mail.blackcarbon.net";
	$username = "sales@blackcarbon.net"; //"smtp_username";
	$password = "hyperlink"; //"smtp_password";

	$headers = array ('From' => "<" . $from . ">",
			'To' => "<" . $to . ">",
			'Subject' => $subject,
      'MIME-Version' => '1.0',
      'Content-type' => 'text/html; charset=iso-8859-1');
	
	$smtp = Mail::factory('smtp',
			array ('host' => $host,
			'auth' => true,
			'username' => $username,
			'password' => $password));
			

	$mail = $smtp->send($to, $headers, $body);
	
	if (PEAR::isError($mail))
	{
		echo "FAILED TO SEND MAIL:<br>";
		echo "To: " . $to . "<br>";
		echo "From: " . $from . "<br>";
		echo "Subject: " . $subject . "<br>";
		echo "<br>" . $body . "<br>";
	}
	
	return !PEAR::isError($mail);*/
}
 
?>