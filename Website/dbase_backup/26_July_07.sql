-- phpMyAdmin SQL Dump
-- version 2.9.0.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jul 26, 2007 at 08:50 PM
-- Server version: 4.0.27
-- PHP Version: 4.4.2
-- 
-- Database: `blackcar_blackcarbon`
-- 
CREATE DATABASE `blackcar_blackcarbon`;
USE blackcar_blackcarbon;

-- --------------------------------------------------------

-- 
-- Table structure for table `download`
-- 

CREATE TABLE `download` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `host` varchar(255) NOT NULL default '',
  `path` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `downloads` int(10) unsigned NOT NULL default '0',
  `loginrequired` tinyint(1) NOT NULL default '0',
  `productid` int(11) NOT NULL default '-1',
  `purchasable` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=46 ;

-- 
-- Dumping data for table `download`
-- 

INSERT INTO `download` VALUES (0, '', 'UVTools1.0.zip', 'UVTools1.0.zip', 2108, 0, 1, 0);
INSERT INTO `download` VALUES (32, '', 'fabricem@funcom.com/licence1.dat', 'licence.dat', 1, 1, 1, 0);
INSERT INTO `download` VALUES (33, '', 'fabricem@funcom.com/licence2.dat', 'licence.dat', 1, 1, 1, 0);
INSERT INTO `download` VALUES (34, '', 'oyvind@funcom.com/licence0.dat', 'licence.dat', 1, 1, 1, 0);
INSERT INTO `download` VALUES (31, '', 'fabricem@funcom.com/licence0.dat', 'licence.dat', 1, 1, 1, 0);
INSERT INTO `download` VALUES (30, '', 'richardc@funcom.com/licence0.dat', 'licence.dat', 3, 1, 1, 0);
INSERT INTO `download` VALUES (29, '', 'supagu64@yahoo.com/licence16.dat', 'licence.dat', 0, 1, 1, 0);
INSERT INTO `download` VALUES (28, '', 'kjetilh@funcom.com/licence0.dat', 'licence.dat', 4, 1, 1, 0);
INSERT INTO `download` VALUES (27, '', 'supagu64@yahoo.com/licence15.dat', 'licence.dat', 1, 1, 1, 0);
INSERT INTO `download` VALUES (1, 'http://members.iinet.com.au', '/~gibbz/ExplosiveDemo.exe', 'ExplosiveDemo.exe', 469, 0, 0, 0);
INSERT INTO `download` VALUES (39, '', 'Explosive.exe', 'Explosive.exe', 1, 1, 0, 1);
INSERT INTO `download` VALUES (41, '', 'supagu64@yahoo.com/licence19.dat', 'licence.dat', 1, 1, 1, 0);
INSERT INTO `download` VALUES (42, '', 'admin@solarkllc.com/licence3.dat', 'licence.dat', 1, 1, 1, 0);
INSERT INTO `download` VALUES (43, '', 'jsiegrist@itsgames.com/licence0.dat', 'licence.dat', 1, 1, 1, 0);
INSERT INTO `download` VALUES (44, '', '3d@z-axis.com.br/licence0.dat', 'licence.dat', 1, 1, 1, 0);
INSERT INTO `download` VALUES (45, '', 'daniel@andersson.pp.se/licence0.dat', 'licence.dat', 1, 1, 1, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `masterserver_joiningclients`
-- 

CREATE TABLE `masterserver_joiningclients` (
  `ip` varchar(255) NOT NULL default '',
  `serverid` int(10) unsigned NOT NULL default '0',
  `port` int(10) unsigned NOT NULL default '0'
) TYPE=MyISAM;

-- 
-- Dumping data for table `masterserver_joiningclients`
-- 

INSERT INTO `masterserver_joiningclients` VALUES ('203.206.44.236', 1403, 6465);
INSERT INTO `masterserver_joiningclients` VALUES ('203.206.44.236', 1409, 6465);

-- --------------------------------------------------------

-- 
-- Table structure for table `masterserver_serverclients`
-- 

CREATE TABLE `masterserver_serverclients` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `keyhash` varchar(255) NOT NULL default '',
  `serverid` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `masterserver_serverclients`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `masterserver_serverinfo`
-- 

CREATE TABLE `masterserver_serverinfo` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `gamemode` varchar(255) NOT NULL default '',
  `mapname` varchar(255) NOT NULL default '',
  `modname` varchar(255) NOT NULL default '',
  `version` varchar(255) NOT NULL default '0',
  `ip` varchar(255) NOT NULL default '',
  `port` int(10) unsigned NOT NULL default '0',
  `timestamp` int(10) unsigned NOT NULL default '0',
  `gamename` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1932 ;

-- 
-- Dumping data for table `masterserver_serverinfo`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `orderform`
-- 

CREATE TABLE `orderform` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `productid` int(10) unsigned NOT NULL default '0',
  `userid` int(10) unsigned NOT NULL default '0',
  `status` smallint(5) NOT NULL default '0',
  `number` int(10) unsigned NOT NULL default '1',
  `paypalid` varchar(255) NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `amount` float NOT NULL default '0',
  KEY `id` (`id`)
) TYPE=MyISAM AUTO_INCREMENT=249 ;

-- 
-- Dumping data for table `orderform`
-- 

INSERT INTO `orderform` VALUES (202, 1, 100, 0, 1, '0', '2006-12-07 02:29:38', 0);
INSERT INTO `orderform` VALUES (197, 1, 92, 3, 1, '4VY222838F2223005', '2006-11-15 00:47:02', 30);
INSERT INTO `orderform` VALUES (196, 1, 94, 3, 1, '5SJ05308D4527353P', '2006-11-06 21:29:59', 30);
INSERT INTO `orderform` VALUES (195, 1, 94, 3, 1, '16R14666WD864794F', '2006-11-03 21:27:58', 30);
INSERT INTO `orderform` VALUES (198, 1, 99, 4, 1, '9RW12359N65570359', '2006-12-02 08:30:54', 30);
INSERT INTO `orderform` VALUES (194, 1, 94, 3, 1, '6GA62822LN191760W', '2006-10-24 18:50:29', 30);
INSERT INTO `orderform` VALUES (190, 1, 91, 3, 1, '72388389R71267201', '2006-10-10 19:15:18', 30);
INSERT INTO `orderform` VALUES (188, 1, 90, 3, 1, '86A4766152311470H', '2006-10-09 21:00:24', 30);
INSERT INTO `orderform` VALUES (187, 1, 83, 0, 1, '0', '2006-08-20 19:02:33', 0);
INSERT INTO `orderform` VALUES (235, 0, 78, 3, 1, '6S414475MF134742J', '2007-01-09 21:21:39', 15);
INSERT INTO `orderform` VALUES (236, 1, 78, 3, 1, '6XC63985TE445540X', '2007-04-11 21:38:07', 30);
INSERT INTO `orderform` VALUES (237, 1, 110, 0, 1, '0', '2007-04-12 00:40:38', 0);
INSERT INTO `orderform` VALUES (238, 1, 113, 0, 1, '0', '2007-04-18 07:52:37', 0);
INSERT INTO `orderform` VALUES (239, 1, 115, 3, 1, '22745616G39498439', '2007-04-26 04:50:48', 30);
INSERT INTO `orderform` VALUES (240, 1, 118, 3, 1, '55V5283599913434W', '2007-05-06 21:46:55', 30);
INSERT INTO `orderform` VALUES (241, 1, 117, 3, 1, '4M509715M22631312', '2007-05-10 02:31:44', 30);
INSERT INTO `orderform` VALUES (242, 1, 123, 3, 1, '0F019369U65128134', '2007-07-20 03:35:36', 30);
INSERT INTO `orderform` VALUES (247, 0, 78, 3, 1, '5SD72313A1917944L', '2007-07-26 20:31:43', 15);
INSERT INTO `orderform` VALUES (245, 0, 78, 1, 1, '0', '2007-07-26 19:52:02', 0);
INSERT INTO `orderform` VALUES (246, 0, 78, 1, 1, '0', '2007-07-26 19:59:47', 0);
INSERT INTO `orderform` VALUES (248, 0, 78, 3, 1, '30E79033810802025', '2007-07-26 20:33:25', 15);

-- --------------------------------------------------------

-- 
-- Table structure for table `product`
-- 

CREATE TABLE `product` (
  `id` int(10) unsigned NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `price` float NOT NULL default '0',
  `ordercompletepage` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `product`
-- 

INSERT INTO `product` VALUES (0, 'Explosive', 15, 'explosive/ordercomplete.php');
INSERT INTO `product` VALUES (1, 'UV Tools', 30, '');

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_bans`
-- 

CREATE TABLE `punbb_bans` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `username` varchar(200) default NULL,
  `ip` varchar(255) default NULL,
  `email` varchar(50) default NULL,
  `message` varchar(255) default NULL,
  `expire` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `punbb_bans`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_categories`
-- 

CREATE TABLE `punbb_categories` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `cat_name` varchar(80) NOT NULL default 'New Category',
  `disp_position` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `punbb_categories`
-- 

INSERT INTO `punbb_categories` VALUES (2, 'General', 0);
INSERT INTO `punbb_categories` VALUES (3, 'Explosive', 0);
INSERT INTO `punbb_categories` VALUES (4, 'UV Tools', 0);
INSERT INTO `punbb_categories` VALUES (5, 'Air Blimbs', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_censoring`
-- 

CREATE TABLE `punbb_censoring` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `search_for` varchar(60) NOT NULL default '',
  `replace_with` varchar(60) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `punbb_censoring`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_config`
-- 

CREATE TABLE `punbb_config` (
  `conf_name` varchar(255) NOT NULL default '',
  `conf_value` text,
  PRIMARY KEY  (`conf_name`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `punbb_config`
-- 

INSERT INTO `punbb_config` VALUES ('o_cur_version', '1.2.14');
INSERT INTO `punbb_config` VALUES ('o_board_title', 'Black Carbon');
INSERT INTO `punbb_config` VALUES ('o_board_desc', NULL);
INSERT INTO `punbb_config` VALUES ('o_server_timezone', '0');
INSERT INTO `punbb_config` VALUES ('o_time_format', 'H:i:s');
INSERT INTO `punbb_config` VALUES ('o_date_format', 'Y-m-d');
INSERT INTO `punbb_config` VALUES ('o_timeout_visit', '600');
INSERT INTO `punbb_config` VALUES ('o_timeout_online', '300');
INSERT INTO `punbb_config` VALUES ('o_redirect_delay', '1');
INSERT INTO `punbb_config` VALUES ('o_show_version', '0');
INSERT INTO `punbb_config` VALUES ('o_show_user_info', '1');
INSERT INTO `punbb_config` VALUES ('o_show_post_count', '1');
INSERT INTO `punbb_config` VALUES ('o_smilies', '1');
INSERT INTO `punbb_config` VALUES ('o_smilies_sig', '1');
INSERT INTO `punbb_config` VALUES ('o_make_links', '1');
INSERT INTO `punbb_config` VALUES ('o_default_lang', 'English');
INSERT INTO `punbb_config` VALUES ('o_default_style', 'Oxygen');
INSERT INTO `punbb_config` VALUES ('o_default_user_group', '4');
INSERT INTO `punbb_config` VALUES ('o_topic_review', '15');
INSERT INTO `punbb_config` VALUES ('o_disp_topics_default', '30');
INSERT INTO `punbb_config` VALUES ('o_disp_posts_default', '25');
INSERT INTO `punbb_config` VALUES ('o_indent_num_spaces', '4');
INSERT INTO `punbb_config` VALUES ('o_quickpost', '0');
INSERT INTO `punbb_config` VALUES ('o_users_online', '1');
INSERT INTO `punbb_config` VALUES ('o_censoring', '0');
INSERT INTO `punbb_config` VALUES ('o_ranks', '1');
INSERT INTO `punbb_config` VALUES ('o_show_dot', '0');
INSERT INTO `punbb_config` VALUES ('o_quickjump', '1');
INSERT INTO `punbb_config` VALUES ('o_gzip', '0');
INSERT INTO `punbb_config` VALUES ('o_additional_navlinks', '');
INSERT INTO `punbb_config` VALUES ('o_report_method', '0');
INSERT INTO `punbb_config` VALUES ('o_regs_report', '0');
INSERT INTO `punbb_config` VALUES ('o_mailing_list', 'supagu64@yahoo.com');
INSERT INTO `punbb_config` VALUES ('o_avatars', '1');
INSERT INTO `punbb_config` VALUES ('o_avatars_dir', 'img/avatars');
INSERT INTO `punbb_config` VALUES ('o_avatars_width', '60');
INSERT INTO `punbb_config` VALUES ('o_avatars_height', '60');
INSERT INTO `punbb_config` VALUES ('o_avatars_size', '10240');
INSERT INTO `punbb_config` VALUES ('o_search_all_forums', '1');
INSERT INTO `punbb_config` VALUES ('o_base_url', 'http://www.blackcarbon.net/forum');
INSERT INTO `punbb_config` VALUES ('o_admin_email', 'supagu64@yahoo.com');
INSERT INTO `punbb_config` VALUES ('o_webmaster_email', 'supagu64@yahoo.com');
INSERT INTO `punbb_config` VALUES ('o_subscriptions', '1');
INSERT INTO `punbb_config` VALUES ('o_smtp_host', NULL);
INSERT INTO `punbb_config` VALUES ('o_smtp_user', NULL);
INSERT INTO `punbb_config` VALUES ('o_smtp_pass', NULL);
INSERT INTO `punbb_config` VALUES ('o_regs_allow', '1');
INSERT INTO `punbb_config` VALUES ('o_regs_verify', '1');
INSERT INTO `punbb_config` VALUES ('o_announcement', '0');
INSERT INTO `punbb_config` VALUES ('o_announcement_message', 'Enter your announcement here.');
INSERT INTO `punbb_config` VALUES ('o_rules', '0');
INSERT INTO `punbb_config` VALUES ('o_rules_message', 'Enter your rules here.');
INSERT INTO `punbb_config` VALUES ('o_maintenance', '0');
INSERT INTO `punbb_config` VALUES ('o_maintenance_message', 'The forums are temporarily down for maintenance. Please try again in a few minutes.<br />\n<br />\n/Administrator');
INSERT INTO `punbb_config` VALUES ('p_mod_edit_users', '1');
INSERT INTO `punbb_config` VALUES ('p_mod_rename_users', '0');
INSERT INTO `punbb_config` VALUES ('p_mod_change_passwords', '0');
INSERT INTO `punbb_config` VALUES ('p_mod_ban_users', '0');
INSERT INTO `punbb_config` VALUES ('p_message_bbcode', '1');
INSERT INTO `punbb_config` VALUES ('p_message_img_tag', '1');
INSERT INTO `punbb_config` VALUES ('p_message_all_caps', '1');
INSERT INTO `punbb_config` VALUES ('p_subject_all_caps', '1');
INSERT INTO `punbb_config` VALUES ('p_sig_all_caps', '1');
INSERT INTO `punbb_config` VALUES ('p_sig_bbcode', '1');
INSERT INTO `punbb_config` VALUES ('p_sig_img_tag', '0');
INSERT INTO `punbb_config` VALUES ('p_sig_length', '400');
INSERT INTO `punbb_config` VALUES ('p_sig_lines', '4');
INSERT INTO `punbb_config` VALUES ('p_allow_banned_email', '1');
INSERT INTO `punbb_config` VALUES ('p_allow_dupe_email', '0');
INSERT INTO `punbb_config` VALUES ('p_force_guest_email', '1');

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_forum_perms`
-- 

CREATE TABLE `punbb_forum_perms` (
  `group_id` int(10) NOT NULL default '0',
  `forum_id` int(10) NOT NULL default '0',
  `read_forum` tinyint(1) NOT NULL default '1',
  `post_replies` tinyint(1) NOT NULL default '1',
  `post_topics` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`group_id`,`forum_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `punbb_forum_perms`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_forums`
-- 

CREATE TABLE `punbb_forums` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `forum_name` varchar(80) NOT NULL default 'New forum',
  `forum_desc` text,
  `redirect_url` varchar(100) default NULL,
  `moderators` text,
  `num_topics` mediumint(8) unsigned NOT NULL default '0',
  `num_posts` mediumint(8) unsigned NOT NULL default '0',
  `last_post` int(10) unsigned default NULL,
  `last_post_id` int(10) unsigned default NULL,
  `last_poster` varchar(200) default NULL,
  `sort_by` tinyint(1) NOT NULL default '0',
  `disp_position` int(10) NOT NULL default '0',
  `cat_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=14 ;

-- 
-- Dumping data for table `punbb_forums`
-- 

INSERT INTO `punbb_forums` VALUES (10, 'Support', NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, 1, 5);
INSERT INTO `punbb_forums` VALUES (11, 'General', NULL, NULL, NULL, 1, 5, 1171711449, 19, 'Fabian', 0, 0, 5);
INSERT INTO `punbb_forums` VALUES (9, 'General', NULL, NULL, NULL, 1, 2, 1178618325, 21, 'Fabian', 0, 0, 4);
INSERT INTO `punbb_forums` VALUES (8, 'Support', NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, 1, 3);
INSERT INTO `punbb_forums` VALUES (7, 'Announcements', NULL, NULL, NULL, 1, 1, 1170483944, 8, 'Fabian', 0, 0, 2);
INSERT INTO `punbb_forums` VALUES (6, 'General', NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, 1, 2);
INSERT INTO `punbb_forums` VALUES (12, 'General', NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, 0, 3);
INSERT INTO `punbb_forums` VALUES (13, 'Support', NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 0, 1, 4);

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_groups`
-- 

CREATE TABLE `punbb_groups` (
  `g_id` int(10) unsigned NOT NULL auto_increment,
  `g_title` varchar(50) NOT NULL default '',
  `g_user_title` varchar(50) default NULL,
  `g_read_board` tinyint(1) NOT NULL default '1',
  `g_post_replies` tinyint(1) NOT NULL default '1',
  `g_post_topics` tinyint(1) NOT NULL default '1',
  `g_post_polls` tinyint(1) NOT NULL default '1',
  `g_edit_posts` tinyint(1) NOT NULL default '1',
  `g_delete_posts` tinyint(1) NOT NULL default '1',
  `g_delete_topics` tinyint(1) NOT NULL default '1',
  `g_set_title` tinyint(1) NOT NULL default '1',
  `g_search` tinyint(1) NOT NULL default '1',
  `g_search_users` tinyint(1) NOT NULL default '1',
  `g_edit_subjects_interval` smallint(6) NOT NULL default '300',
  `g_post_flood` smallint(6) NOT NULL default '30',
  `g_search_flood` smallint(6) NOT NULL default '30',
  PRIMARY KEY  (`g_id`)
) TYPE=MyISAM AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `punbb_groups`
-- 

INSERT INTO `punbb_groups` VALUES (1, 'Administrators', 'Administrator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0);
INSERT INTO `punbb_groups` VALUES (2, 'Moderators', 'Moderator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0);
INSERT INTO `punbb_groups` VALUES (3, 'Guest', NULL, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0);
INSERT INTO `punbb_groups` VALUES (4, 'Members', NULL, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 300, 60, 30);
INSERT INTO `punbb_groups` VALUES (5, 'Tester', NULL, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 300, 60, 30);

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_online`
-- 

CREATE TABLE `punbb_online` (
  `user_id` int(10) unsigned NOT NULL default '1',
  `ident` varchar(200) NOT NULL default '',
  `logged` int(10) unsigned NOT NULL default '0',
  `idle` tinyint(1) NOT NULL default '0',
  KEY `punbb_online_user_id_idx` (`user_id`)
) TYPE=HEAP;

-- 
-- Dumping data for table `punbb_online`
-- 

INSERT INTO `punbb_online` VALUES (1, '66.249.70.163', 1185446399, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_posts`
-- 

CREATE TABLE `punbb_posts` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `poster` varchar(200) NOT NULL default '',
  `poster_id` int(10) unsigned NOT NULL default '1',
  `poster_ip` varchar(15) default NULL,
  `poster_email` varchar(50) default NULL,
  `message` text,
  `hide_smilies` tinyint(1) NOT NULL default '0',
  `posted` int(10) unsigned NOT NULL default '0',
  `edited` int(10) unsigned default NULL,
  `edited_by` varchar(200) default NULL,
  `topic_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `punbb_posts_topic_id_idx` (`topic_id`),
  KEY `punbb_posts_multi_idx` (`poster_id`,`topic_id`)
) TYPE=MyISAM AUTO_INCREMENT=22 ;

-- 
-- Dumping data for table `punbb_posts`
-- 

INSERT INTO `punbb_posts` VALUES (8, 'Fabian', 2, '203.206.44.236', NULL, 'There has been some major changes to the website, if you encounter any problems let us know.\n\nThe biggest difference is the new forum. Once you have registered (if your not already a member) and logged in to the website, you will have access to post on the forums.', 0, 1170483944, NULL, NULL, 4);
INSERT INTO `punbb_posts` VALUES (9, 'Fabian', 2, '203.206.44.236', NULL, 'DX10 Screenshots:\n\nInside the clouds:\n[url=http://members.iinet.com.au/~gibbz/AirBlimbs_Cloud01.jpg][img]http://members.iinet.com.au/~gibbz/AirBlimbs_Cloud01.jpg[/img][/url]', 0, 1170484160, NULL, NULL, 5);
INSERT INTO `punbb_posts` VALUES (10, 'Mat�', 33, '219.90.189.201', NULL, 'That is a magical screen shot. You got to be proud of that. I really like the light as it comes of the cannon. \n\nLooks next-gen.\n\nCould you give a few details about texture sizes, poly count, video card specs and frame rate.\n\nThanks.\nMat�', 0, 1171014606, NULL, NULL, 5);
INSERT INTO `punbb_posts` VALUES (12, 'Bronson', 10, '203.206.44.236', NULL, 'Hey Mate,\n\nDid you do a game art tafe course in Adelaide? \n\nTexture sizes are 256 - 512.\nThe wood planks for example are 512x512, with diffuse, specular, normal map and parallax map.\n\nPolycount and framerate can be seen in the screenshot\n"F:" is the framrate "P:" is the polycount for the whole scene, which is not all visible.', 0, 1171118875, NULL, NULL, 5);
INSERT INTO `punbb_posts` VALUES (18, 'Mat�', 33, '219.90.168.223', NULL, 'Very impressive work. Pardon my ignorance, but what is the cannon project for?\n\nAhyeh, I just finished my Tafe course, right now i''m looking for a job. I hope you don''t mind but i googled you name after you bronson.2ya.com\nwebsite stopped working, and that''s how i found blackcarbon.\n\nI have a website, check it out www.m-a-t-e.com,\nleave a comment if you like.\n\nYou can always catch me on msn messenger, my id is josipovic.mate@gmail.com.', 0, 1171350838, NULL, NULL, 5);
INSERT INTO `punbb_posts` VALUES (19, 'Fabian', 2, '203.206.44.236', NULL, 'Cannon project? you mean this game?\nSomething cool to work on at the moment :)', 0, 1171711449, NULL, NULL, 5);
INSERT INTO `punbb_posts` VALUES (20, 'fredrik', 42, '62.181.217.66', NULL, 'Hi been trying out the UV-tools and they seem great. Some buttons are greyed out though. For instance I can only map X not Y Z and only flip in one direction i think. Is this because of the limitation in the demo or is it because I use 3dsmax 9?  Furthermore I wonder if there is a chance to see the teaser video in higher quality just to grasp the workflow with UV-tools better. Well if everything will work for 3dsmax 9 I will buy it in a heaartbeat.', 0, 1178283529, NULL, NULL, 7);
INSERT INTO `punbb_posts` VALUES (21, 'Fabian', 2, '203.206.64.167', NULL, 'Yes the demo has some buttons greyed out, they all become greyed out once the demo expires.\n\nUnfortunately we have lost the high res video.', 0, 1178618325, NULL, NULL, 7);

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_ranks`
-- 

CREATE TABLE `punbb_ranks` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `rank` varchar(50) NOT NULL default '',
  `min_posts` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `punbb_ranks`
-- 

INSERT INTO `punbb_ranks` VALUES (1, 'New member', 0);
INSERT INTO `punbb_ranks` VALUES (2, 'Member', 10);

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_reports`
-- 

CREATE TABLE `punbb_reports` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `post_id` int(10) unsigned NOT NULL default '0',
  `topic_id` int(10) unsigned NOT NULL default '0',
  `forum_id` int(10) unsigned NOT NULL default '0',
  `reported_by` int(10) unsigned NOT NULL default '0',
  `created` int(10) unsigned NOT NULL default '0',
  `message` text,
  `zapped` int(10) unsigned default NULL,
  `zapped_by` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `punbb_reports_zapped_idx` (`zapped`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `punbb_reports`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_search_cache`
-- 

CREATE TABLE `punbb_search_cache` (
  `id` int(10) unsigned NOT NULL default '0',
  `ident` varchar(200) NOT NULL default '',
  `search_data` text,
  PRIMARY KEY  (`id`),
  KEY `punbb_search_cache_ident_idx` (`ident`(8))
) TYPE=MyISAM;

-- 
-- Dumping data for table `punbb_search_cache`
-- 

INSERT INTO `punbb_search_cache` VALUES (1554626959, '66.249.70.163', 'a:5:{s:14:"search_results";s:1:"5";s:8:"num_hits";i:1;s:7:"sort_by";i:4;s:8:"sort_dir";s:4:"DESC";s:7:"show_as";s:6:"topics";}');

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_search_matches`
-- 

CREATE TABLE `punbb_search_matches` (
  `post_id` int(10) unsigned NOT NULL default '0',
  `word_id` mediumint(8) unsigned NOT NULL default '0',
  `subject_match` tinyint(1) NOT NULL default '0',
  KEY `punbb_search_matches_word_id_idx` (`word_id`),
  KEY `punbb_search_matches_post_id_idx` (`post_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `punbb_search_matches`
-- 

INSERT INTO `punbb_search_matches` VALUES (2, 2, 0);
INSERT INTO `punbb_search_matches` VALUES (2, 1, 0);
INSERT INTO `punbb_search_matches` VALUES (2, 1, 1);
INSERT INTO `punbb_search_matches` VALUES (3, 3, 0);
INSERT INTO `punbb_search_matches` VALUES (4, 4, 0);
INSERT INTO `punbb_search_matches` VALUES (5, 1, 0);
INSERT INTO `punbb_search_matches` VALUES (5, 1, 1);
INSERT INTO `punbb_search_matches` VALUES (6, 5, 0);
INSERT INTO `punbb_search_matches` VALUES (7, 1, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 6, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 7, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 8, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 9, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 10, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 11, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 12, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 13, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 14, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 15, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 16, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 17, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 18, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 19, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 20, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 21, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 22, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 23, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 24, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 25, 0);
INSERT INTO `punbb_search_matches` VALUES (8, 7, 1);
INSERT INTO `punbb_search_matches` VALUES (8, 8, 1);
INSERT INTO `punbb_search_matches` VALUES (9, 26, 1);
INSERT INTO `punbb_search_matches` VALUES (9, 27, 1);
INSERT INTO `punbb_search_matches` VALUES (9, 29, 0);
INSERT INTO `punbb_search_matches` VALUES (9, 31, 0);
INSERT INTO `punbb_search_matches` VALUES (9, 30, 0);
INSERT INTO `punbb_search_matches` VALUES (9, 27, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 21, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 32, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 33, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 34, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 35, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 36, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 37, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 38, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 39, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 40, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 41, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 42, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 43, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 44, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 45, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 46, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 47, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 48, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 49, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 50, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 51, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 52, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 53, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 54, 0);
INSERT INTO `punbb_search_matches` VALUES (10, 55, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 21, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 8, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 61, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 62, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 39, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 63, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 64, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 65, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 66, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 67, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 68, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 69, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 70, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 71, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 72, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 73, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 74, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 75, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 76, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 77, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 78, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 79, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 80, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 81, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 82, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 83, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 84, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 85, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 86, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 87, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 88, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 89, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 90, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 91, 0);
INSERT INTO `punbb_search_matches` VALUES (18, 92, 0);
INSERT INTO `punbb_search_matches` VALUES (19, 39, 0);
INSERT INTO `punbb_search_matches` VALUES (19, 95, 0);
INSERT INTO `punbb_search_matches` VALUES (19, 94, 0);
INSERT INTO `punbb_search_matches` VALUES (19, 93, 0);
INSERT INTO `punbb_search_matches` VALUES (19, 96, 0);
INSERT INTO `punbb_search_matches` VALUES (19, 65, 0);
INSERT INTO `punbb_search_matches` VALUES (19, 62, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 18, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 21, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 49, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 62, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 97, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 98, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 99, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 100, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 101, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 102, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 103, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 104, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 105, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 106, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 107, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 108, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 109, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 110, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 111, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 112, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 113, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 114, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 115, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 116, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 117, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 118, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 119, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 120, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 121, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 122, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 123, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 124, 0);
INSERT INTO `punbb_search_matches` VALUES (20, 101, 1);
INSERT INTO `punbb_search_matches` VALUES (20, 102, 1);
INSERT INTO `punbb_search_matches` VALUES (21, 125, 0);
INSERT INTO `punbb_search_matches` VALUES (21, 101, 0);
INSERT INTO `punbb_search_matches` VALUES (21, 111, 0);
INSERT INTO `punbb_search_matches` VALUES (21, 126, 0);
INSERT INTO `punbb_search_matches` VALUES (21, 102, 0);
INSERT INTO `punbb_search_matches` VALUES (21, 129, 0);
INSERT INTO `punbb_search_matches` VALUES (21, 128, 0);
INSERT INTO `punbb_search_matches` VALUES (21, 130, 0);
INSERT INTO `punbb_search_matches` VALUES (21, 127, 0);
INSERT INTO `punbb_search_matches` VALUES (21, 49, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_search_words`
-- 

CREATE TABLE `punbb_search_words` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `word` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`word`),
  KEY `punbb_search_words_id_idx` (`id`)
) TYPE=MyISAM AUTO_INCREMENT=131 ;

-- 
-- Dumping data for table `punbb_search_words`
-- 

INSERT INTO `punbb_search_words` VALUES (1, 'test');
INSERT INTO `punbb_search_words` VALUES (2, 'msg');
INSERT INTO `punbb_search_words` VALUES (3, 'dfgdfgdf');
INSERT INTO `punbb_search_words` VALUES (4, 'noobey');
INSERT INTO `punbb_search_words` VALUES (5, 'noice');
INSERT INTO `punbb_search_words` VALUES (6, 'major');
INSERT INTO `punbb_search_words` VALUES (7, 'changes');
INSERT INTO `punbb_search_words` VALUES (8, 'website');
INSERT INTO `punbb_search_words` VALUES (9, 'encounter');
INSERT INTO `punbb_search_words` VALUES (10, 'problems');
INSERT INTO `punbb_search_words` VALUES (11, 'let');
INSERT INTO `punbb_search_words` VALUES (12, 'know');
INSERT INTO `punbb_search_words` VALUES (13, 'biggest');
INSERT INTO `punbb_search_words` VALUES (14, 'difference');
INSERT INTO `punbb_search_words` VALUES (15, 'new');
INSERT INTO `punbb_search_words` VALUES (16, 'forum');
INSERT INTO `punbb_search_words` VALUES (17, 'registered');
INSERT INTO `punbb_search_words` VALUES (18, 'not');
INSERT INTO `punbb_search_words` VALUES (19, 'already');
INSERT INTO `punbb_search_words` VALUES (20, 'member');
INSERT INTO `punbb_search_words` VALUES (21, 'and');
INSERT INTO `punbb_search_words` VALUES (22, 'logged');
INSERT INTO `punbb_search_words` VALUES (23, 'access');
INSERT INTO `punbb_search_words` VALUES (24, 'post');
INSERT INTO `punbb_search_words` VALUES (25, 'forums');
INSERT INTO `punbb_search_words` VALUES (26, 'development');
INSERT INTO `punbb_search_words` VALUES (27, 'screenshots');
INSERT INTO `punbb_search_words` VALUES (28, 'edit');
INSERT INTO `punbb_search_words` VALUES (29, 'dx10');
INSERT INTO `punbb_search_words` VALUES (30, 'inside');
INSERT INTO `punbb_search_words` VALUES (31, 'clouds');
INSERT INTO `punbb_search_words` VALUES (32, 'magical');
INSERT INTO `punbb_search_words` VALUES (33, 'screen');
INSERT INTO `punbb_search_words` VALUES (34, 'shot');
INSERT INTO `punbb_search_words` VALUES (35, 'proud');
INSERT INTO `punbb_search_words` VALUES (36, 'that');
INSERT INTO `punbb_search_words` VALUES (37, 'light');
INSERT INTO `punbb_search_words` VALUES (38, 'comes');
INSERT INTO `punbb_search_words` VALUES (39, 'cannon');
INSERT INTO `punbb_search_words` VALUES (40, 'looks');
INSERT INTO `punbb_search_words` VALUES (41, 'next-gen');
INSERT INTO `punbb_search_words` VALUES (42, 'give');
INSERT INTO `punbb_search_words` VALUES (43, 'few');
INSERT INTO `punbb_search_words` VALUES (44, 'details');
INSERT INTO `punbb_search_words` VALUES (45, 'texture');
INSERT INTO `punbb_search_words` VALUES (46, 'sizes');
INSERT INTO `punbb_search_words` VALUES (47, 'poly');
INSERT INTO `punbb_search_words` VALUES (48, 'count');
INSERT INTO `punbb_search_words` VALUES (49, 'video');
INSERT INTO `punbb_search_words` VALUES (50, 'card');
INSERT INTO `punbb_search_words` VALUES (51, 'specs');
INSERT INTO `punbb_search_words` VALUES (52, 'frame');
INSERT INTO `punbb_search_words` VALUES (53, 'rate');
INSERT INTO `punbb_search_words` VALUES (54, 'thanks');
INSERT INTO `punbb_search_words` VALUES (55, 'mat�');
INSERT INTO `punbb_search_words` VALUES (56, 'hey');
INSERT INTO `punbb_search_words` VALUES (57, 'testing');
INSERT INTO `punbb_search_words` VALUES (62, 'work');
INSERT INTO `punbb_search_words` VALUES (61, 'impressive');
INSERT INTO `punbb_search_words` VALUES (63, 'pardon');
INSERT INTO `punbb_search_words` VALUES (64, 'ignorance');
INSERT INTO `punbb_search_words` VALUES (65, 'project');
INSERT INTO `punbb_search_words` VALUES (66, 'ahyeh');
INSERT INTO `punbb_search_words` VALUES (67, 'finished');
INSERT INTO `punbb_search_words` VALUES (68, 'tafe');
INSERT INTO `punbb_search_words` VALUES (69, 'course');
INSERT INTO `punbb_search_words` VALUES (70, 'right');
INSERT INTO `punbb_search_words` VALUES (71, 'looking');
INSERT INTO `punbb_search_words` VALUES (72, 'job');
INSERT INTO `punbb_search_words` VALUES (73, 'hope');
INSERT INTO `punbb_search_words` VALUES (74, 'mind');
INSERT INTO `punbb_search_words` VALUES (75, 'googled');
INSERT INTO `punbb_search_words` VALUES (76, 'name');
INSERT INTO `punbb_search_words` VALUES (77, 'bronson.2ya.com');
INSERT INTO `punbb_search_words` VALUES (78, 'stopped');
INSERT INTO `punbb_search_words` VALUES (79, 'working');
INSERT INTO `punbb_search_words` VALUES (80, 'found');
INSERT INTO `punbb_search_words` VALUES (81, 'blackcarbon');
INSERT INTO `punbb_search_words` VALUES (82, 'check');
INSERT INTO `punbb_search_words` VALUES (83, 'www.m-a-t-e.com');
INSERT INTO `punbb_search_words` VALUES (84, 'leave');
INSERT INTO `punbb_search_words` VALUES (85, 'comment');
INSERT INTO `punbb_search_words` VALUES (86, 'like');
INSERT INTO `punbb_search_words` VALUES (87, 'always');
INSERT INTO `punbb_search_words` VALUES (88, 'catch');
INSERT INTO `punbb_search_words` VALUES (89, 'msn');
INSERT INTO `punbb_search_words` VALUES (90, 'messenger');
INSERT INTO `punbb_search_words` VALUES (91, 'josipovic.mate');
INSERT INTO `punbb_search_words` VALUES (92, 'gmail.com');
INSERT INTO `punbb_search_words` VALUES (93, 'mean');
INSERT INTO `punbb_search_words` VALUES (94, 'game');
INSERT INTO `punbb_search_words` VALUES (95, 'cool');
INSERT INTO `punbb_search_words` VALUES (96, 'moment');
INSERT INTO `punbb_search_words` VALUES (97, 'trying');
INSERT INTO `punbb_search_words` VALUES (98, 'uv-tools');
INSERT INTO `punbb_search_words` VALUES (99, 'seem');
INSERT INTO `punbb_search_words` VALUES (100, 'great');
INSERT INTO `punbb_search_words` VALUES (101, 'buttons');
INSERT INTO `punbb_search_words` VALUES (102, 'greyed');
INSERT INTO `punbb_search_words` VALUES (103, 'though');
INSERT INTO `punbb_search_words` VALUES (104, 'instance');
INSERT INTO `punbb_search_words` VALUES (105, 'map');
INSERT INTO `punbb_search_words` VALUES (106, 'flip');
INSERT INTO `punbb_search_words` VALUES (107, 'direction');
INSERT INTO `punbb_search_words` VALUES (108, 'think');
INSERT INTO `punbb_search_words` VALUES (109, 'because');
INSERT INTO `punbb_search_words` VALUES (110, 'limitation');
INSERT INTO `punbb_search_words` VALUES (111, 'demo');
INSERT INTO `punbb_search_words` VALUES (112, '3dsmax');
INSERT INTO `punbb_search_words` VALUES (113, 'furthermore');
INSERT INTO `punbb_search_words` VALUES (114, 'wonder');
INSERT INTO `punbb_search_words` VALUES (115, 'chance');
INSERT INTO `punbb_search_words` VALUES (116, 'teaser');
INSERT INTO `punbb_search_words` VALUES (117, 'higher');
INSERT INTO `punbb_search_words` VALUES (118, 'quality');
INSERT INTO `punbb_search_words` VALUES (119, 'grasp');
INSERT INTO `punbb_search_words` VALUES (120, 'workflow');
INSERT INTO `punbb_search_words` VALUES (121, 'better');
INSERT INTO `punbb_search_words` VALUES (122, 'everything');
INSERT INTO `punbb_search_words` VALUES (123, 'buy');
INSERT INTO `punbb_search_words` VALUES (124, 'heaartbeat');
INSERT INTO `punbb_search_words` VALUES (125, 'become');
INSERT INTO `punbb_search_words` VALUES (126, 'expires');
INSERT INTO `punbb_search_words` VALUES (127, 'unfortunately');
INSERT INTO `punbb_search_words` VALUES (128, 'lost');
INSERT INTO `punbb_search_words` VALUES (129, 'high');
INSERT INTO `punbb_search_words` VALUES (130, 'res');

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_subscriptions`
-- 

CREATE TABLE `punbb_subscriptions` (
  `user_id` int(10) unsigned NOT NULL default '0',
  `topic_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`topic_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `punbb_subscriptions`
-- 

INSERT INTO `punbb_subscriptions` VALUES (10, 5);

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_topics`
-- 

CREATE TABLE `punbb_topics` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `poster` varchar(200) NOT NULL default '',
  `subject` varchar(255) NOT NULL default '',
  `posted` int(10) unsigned NOT NULL default '0',
  `last_post` int(10) unsigned NOT NULL default '0',
  `last_post_id` int(10) unsigned NOT NULL default '0',
  `last_poster` varchar(200) default NULL,
  `num_views` mediumint(8) unsigned NOT NULL default '0',
  `num_replies` mediumint(8) unsigned NOT NULL default '0',
  `closed` tinyint(1) NOT NULL default '0',
  `sticky` tinyint(1) NOT NULL default '0',
  `moved_to` int(10) unsigned default NULL,
  `forum_id` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `punbb_topics_forum_id_idx` (`forum_id`),
  KEY `punbb_topics_moved_to_idx` (`moved_to`)
) TYPE=MyISAM AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `punbb_topics`
-- 

INSERT INTO `punbb_topics` VALUES (4, 'Fabian', 'Website changes', 1170483944, 1170483944, 8, 'Fabian', 156, 0, 0, 0, NULL, 7);
INSERT INTO `punbb_topics` VALUES (5, 'Fabian', 'Development Screenshots', 1170484160, 1171711449, 19, 'Fabian', 445, 4, 0, 0, NULL, 11);
INSERT INTO `punbb_topics` VALUES (7, 'fredrik', 'Greyed out buttons', 1178283529, 1178618325, 21, 'Fabian', 122, 1, 0, 0, NULL, 9);

-- --------------------------------------------------------

-- 
-- Table structure for table `punbb_users`
-- 

CREATE TABLE `punbb_users` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `group_id` int(10) unsigned NOT NULL default '4',
  `username` varchar(200) NOT NULL default '',
  `password` varchar(40) NOT NULL default '',
  `email` varchar(50) NOT NULL default '',
  `title` varchar(50) default NULL,
  `realname` varchar(40) default NULL,
  `url` varchar(100) default NULL,
  `jabber` varchar(75) default NULL,
  `icq` varchar(12) default NULL,
  `msn` varchar(50) default NULL,
  `aim` varchar(30) default NULL,
  `yahoo` varchar(30) default NULL,
  `location` varchar(30) default NULL,
  `use_avatar` tinyint(1) NOT NULL default '0',
  `signature` text,
  `disp_topics` tinyint(3) unsigned default NULL,
  `disp_posts` tinyint(3) unsigned default NULL,
  `email_setting` tinyint(1) NOT NULL default '1',
  `save_pass` tinyint(1) NOT NULL default '1',
  `notify_with_post` tinyint(1) NOT NULL default '0',
  `show_smilies` tinyint(1) NOT NULL default '1',
  `show_img` tinyint(1) NOT NULL default '1',
  `show_img_sig` tinyint(1) NOT NULL default '1',
  `show_avatars` tinyint(1) NOT NULL default '1',
  `show_sig` tinyint(1) NOT NULL default '1',
  `timezone` float NOT NULL default '0',
  `language` varchar(25) NOT NULL default 'English',
  `style` varchar(25) NOT NULL default 'Oxygen',
  `num_posts` int(10) unsigned NOT NULL default '0',
  `last_post` int(10) unsigned default NULL,
  `registered` int(10) unsigned NOT NULL default '0',
  `registration_ip` varchar(15) NOT NULL default '0.0.0.0',
  `last_visit` int(10) unsigned NOT NULL default '0',
  `admin_note` varchar(30) default NULL,
  `activate_string` varchar(50) default NULL,
  `activate_key` varchar(8) default NULL,
  PRIMARY KEY  (`id`),
  KEY `punbb_users_registered_idx` (`registered`),
  KEY `punbb_users_username_idx` (`username`(8))
) TYPE=MyISAM AUTO_INCREMENT=51 ;

-- 
-- Dumping data for table `punbb_users`
-- 

INSERT INTO `punbb_users` VALUES (1, 3, 'Guest', 'Guest', 'Guest', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 1170472627, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (2, 1, 'Fabian', '0', 'supagu64@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 11, 1178618325, 1169627861, '127.0.0.1', 1183711341, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (10, 4, 'Bronson', '0', 'gibbz1@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 2, 1171159212, 0, '0.0.0.0', 1171159263, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (11, 4, 'Damir', '0', 'BCarbon@simovski.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (12, 4, 'Bryan', '0', 'breynolds@gamesauce.co.uk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (13, 4, 'Kim', '0', 'sharkmouth@nate.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (14, 4, 'bazuka', '0', 'bazuka666@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (15, 4, 'Dmitriy', '0', 'ok@supertema.ru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (16, 4, 'claudio', '0', 'pole3dclaudio@free.Fr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (17, 4, 'Timothy', '0', 'lobo1963@earthlink.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (18, 4, 'Timothy', '0', 'thiggins@vvisions.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (19, 4, 'Aamer', '0', 'i_aamer10@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (20, 4, 'necros', '0', 'OUTROPIA@HOTMAIL.COM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (21, 4, 'Kjetil', '0', 'kjetilh@funcom.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (22, 4, 'Richard', '0', 'richardc@funcom.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (23, 4, '?yvind', '0', 'oyvind@funcom.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (24, 4, 'Tyler', '0', 'tyler@inxile.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (25, 4, 'Funcom', '0', 'fabricem@funcom.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (26, 4, 'Peter', '0', 'hybal@chello.sk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (27, 4, 'ben', '0', 'forcestatus@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (28, 4, 'shaq', '0', 'zmaxz2@yahoo.com.tw', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (29, 4, 'shaq', '0', 'shaq@lager.com.tw', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (30, 4, 'Steve', '0', 'Pixelpusher75@sbcglobal.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (31, 4, 'sugikami', '0', 'sega193@yahoo.co.jp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '0.0.0.0', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (33, 4, 'Mat�', '0', 'josipovic.mate@gmail.com', NULL, 'Mat�', 'http://www.m-a-t-e.com', NULL, NULL, 'josipovic.mate@gmail.com', NULL, NULL, 'Adelaide', 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 2, 1171350838, 0, '219.90.189.201', 1171353239, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (34, 4, 'luis', '0', 'luism31@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '201.161.165.197', 1171466248, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (35, 4, 'dsfd', '0', 'sfds@asda.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '195.188.17.251', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (36, 4, 'Robin', '0', 'robin.ball@gamesauce.co.uk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '89.145.215.237', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (37, 4, 'eduardo', '0', 'edfull@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '200.213.236.22', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (38, 4, 'Phuong', '0', 'fullsofts@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '58.187.121.214', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (39, 4, 'Avery', '0', 'Averytingwong@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '12.191.195.130', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (40, 4, 'josh', '0', 'admin@solarkll.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '72.43.2.91', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (41, 4, 'josh', '0', 'admin@solarkllc.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '72.43.2.91', 1177548003, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (42, 4, 'fredrik', '0', 'fredrik.englund@jadestone.se', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 1, 1178283529, 0, '62.181.217.66', 1178285043, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (43, 4, 'Jane', '0', 'jsiegrist@itsgames.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '65.247.117.243', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (44, 4, 'Marcus', '0', '3d@z-axis.com.br', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '201.6.8.54', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (45, 4, 'Bert', '0', 'bmclendon@viciouscycleinc.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '69.134.23.247', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (46, 4, 'gadah', '0', 'gadah_m_adam@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '82.201.215.216', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (47, 4, 'aaron', '0', 'skillzie@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '76.172.103.86', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (48, 4, 'aaron', '0', 'skillzie@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '76.172.103.86', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (49, 4, 'Daniel', '0', 'daniel@andersson.pp.se', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '87.251.215.22', 0, NULL, NULL, NULL);
INSERT INTO `punbb_users` VALUES (50, 4, 'fabian2', '0', 'supagu@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 'English', 'Oxygen', 0, NULL, 0, '124.171.219.57', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `session`
-- 

CREATE TABLE `session` (
  `auth` varchar(32) NOT NULL default '',
  `userid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`auth`),
  UNIQUE KEY `id` (`auth`,`userid`),
  FULLTEXT KEY `auth` (`auth`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `session`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `firstname` varchar(128) NOT NULL default '',
  `lastname` varchar(128) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `mailinglist` tinyint(1) NOT NULL default '0',
  `password` varchar(32) NOT NULL default '',
  `verified` tinyint(1) NOT NULL default '0',
  `admin` tinyint(1) NOT NULL default '0',
  `forumid` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=125 ;

-- 
-- Dumping data for table `user`
-- 

INSERT INTO `user` VALUES (80, 'Bronson', 'Mathews', 'gibbz1@yahoo.com', 1, '0f6811b05660fde57651fda214919653', 1, 1, 10);
INSERT INTO `user` VALUES (75, 'Damir', 'Simovski', 'BCarbon@simovski.com', 0, 'c37c9ca0ec5b2c0eacad336ee4c66cc6', 1, 0, 11);
INSERT INTO `user` VALUES (78, 'Fabian', 'Mathews', 'supagu64@yahoo.com', 1, '0f6811b05660fde57651fda214919653', 1, 1, 2);
INSERT INTO `user` VALUES (81, 'Bryan', 'Reynolds', 'breynolds@gamesauce.co.uk', 0, '3b7a9fb5fdb44e561b5f87f128e29f72', 1, 0, 12);
INSERT INTO `user` VALUES (82, 'Kim', 'JungHoon', 'sharkmouth@nate.com', 1, '3d186804534370c3c817db0563f0e461', 1, 0, 13);
INSERT INTO `user` VALUES (83, 'bazuka', 'baz', 'bazuka666@yahoo.com', 0, '0b9a54438fba2dc0d39be8f7c6c71a58', 1, 0, 14);
INSERT INTO `user` VALUES (84, 'Dmitriy', 'Nesonov', 'ok@supertema.ru', 1, '0a295b494e0cafee78df9f8b26622504', 1, 0, 15);
INSERT INTO `user` VALUES (85, 'claudio', 'gallego', 'pole3dclaudio@free.Fr', 1, '7a1c5e4f4caa256d73616e87a338b66c', 1, 0, 16);
INSERT INTO `user` VALUES (86, 'Timothy', 'Higgins', 'lobo1963@earthlink.net', 1, '024e9ea690b7d08661dbace2f30fff27', 1, 0, 17);
INSERT INTO `user` VALUES (87, 'Timothy', 'Higgins', 'thiggins@vvisions.com', 1, '024e9ea690b7d08661dbace2f30fff27', 1, 0, 18);
INSERT INTO `user` VALUES (88, 'Aamer', 'Irfan', 'i_aamer10@yahoo.com', 1, '18e3cb8060fa8f1c953ec46bb4ec7523', 1, 0, 19);
INSERT INTO `user` VALUES (89, 'necros', 'asddasd', 'OUTROPIA@HOTMAIL.COM', 0, 'a4f53a5b7221f89199c90bbbb7fc39a9', 1, 0, 20);
INSERT INTO `user` VALUES (90, 'Kjetil', 'Hjeldnes', 'kjetilh@funcom.com', 0, 'e10adc3949ba59abbe56e057f20f883e', 1, 0, 21);
INSERT INTO `user` VALUES (91, 'Richard', 'Cawte', 'richardc@funcom.com', 1, '725337daa392180f4888efbebb994d55', 1, 0, 22);
INSERT INTO `user` VALUES (92, '?yvind', 'Jernskau', 'oyvind@funcom.com', 1, '80e3bbdf754ffa96ae9dcd315bf48673', 1, 0, 23);
INSERT INTO `user` VALUES (93, 'Tyler', 'James', 'tyler@inxile.net', 0, 'eb2a13123e3297b17454fc36e72d161f', 1, 0, 24);
INSERT INTO `user` VALUES (94, 'Funcom', 'ACHA', 'fabricem@funcom.com', 0, '0d0de813c1105498e3435dd2fbf7fa26', 1, 0, 25);
INSERT INTO `user` VALUES (95, 'Peter', 'Hybal', 'hybal@chello.sk', 1, '6f3abf9640e9822fe0fc14d58604a4d0', 1, 0, 26);
INSERT INTO `user` VALUES (96, 'ben', 'rayner', 'forcestatus@hotmail.com', 0, 'd7bee25f622441bee331075cc541c80f', 0, 0, 27);
INSERT INTO `user` VALUES (97, 'shaq', 'shiu', 'zmaxz2@yahoo.com.tw', 1, '9f35da5e9c893693cbb7920e124bdbfa', 0, 0, 28);
INSERT INTO `user` VALUES (98, 'shaq', 'shiu', 'shaq@lager.com.tw', 1, '9f35da5e9c893693cbb7920e124bdbfa', 1, 0, 29);
INSERT INTO `user` VALUES (99, 'Steve', 'Schmidt', 'Pixelpusher75@sbcglobal.net', 1, '2121faf504c0aee6243fd86a6272a380', 1, 0, 30);
INSERT INTO `user` VALUES (100, 'sugikami', 'tetsuya', 'sega193@yahoo.co.jp', 1, '3c8615255fd0614a03245e4bfef9a84a', 1, 0, 31);
INSERT INTO `user` VALUES (107, 'Mat�', 'Josipovic', 'josipovic.mate@gmail.com', 1, 'ac9132a1ca42ac8bded4d4e8d831832f', 1, 0, 33);
INSERT INTO `user` VALUES (108, 'luis', 'Ortiz', 'luism31@hotmail.com', 0, 'ae7b9b89eead4aab80ad39ada5ead146', 1, 0, 34);
INSERT INTO `user` VALUES (109, 'dsfd', 'sdfds', 'sfds@asda.com', 0, '827ccb0eea8a706c4c34a16891f84e7b', 0, 0, 35);
INSERT INTO `user` VALUES (110, 'Robin', 'Ball', 'robin.ball@gamesauce.co.uk', 1, '7c98b8ec4a45506965f4fe9014be59c6', 1, 0, 36);
INSERT INTO `user` VALUES (111, 'eduardo', 'gomes', 'edfull@hotmail.com', 1, '80edc839105c1689cdc84a8258d2edd3', 0, 0, 37);
INSERT INTO `user` VALUES (112, 'Phuong', 'Nguyen', 'fullsofts@hotmail.com', 1, '2b26dda43454ab6aa8ce2c5aa740ebc6', 1, 0, 38);
INSERT INTO `user` VALUES (113, 'Avery', 'Wong', 'Averytingwong@gmail.com', 0, '4bf8eb44b29e3a06e84a5ff771a2536d', 1, 0, 39);
INSERT INTO `user` VALUES (114, 'josh', 'presseisen', 'admin@solarkll.com', 0, '27ec4687dba9430a5fa82fbcb83b3a2b', 0, 0, 40);
INSERT INTO `user` VALUES (115, 'josh', 'presseisen', 'admin@solarkllc.com', 1, '27ec4687dba9430a5fa82fbcb83b3a2b', 1, 0, 41);
INSERT INTO `user` VALUES (116, 'fredrik', 'englund', 'fredrik.englund@jadestone.se', 0, '1495df56476aec0e3aa2b2725f62f9dd', 1, 0, 42);
INSERT INTO `user` VALUES (117, 'Jane', 'Siegrist', 'jsiegrist@itsgames.com', 1, '8fb55ac17dd1f6ae685e5a87c4bab9c6', 1, 0, 43);
INSERT INTO `user` VALUES (118, 'Marcus', 'Moreira', '3d@z-axis.com.br', 1, '55440272eec5cd71dfffb119d5363267', 1, 0, 44);
INSERT INTO `user` VALUES (119, 'Bert', 'McLendon', 'bmclendon@viciouscycleinc.com', 0, '4b70c5a3ebfb5f8706b297db179cbaf6', 1, 0, 45);
INSERT INTO `user` VALUES (120, 'gadah', 'adam', 'gadah_m_adam@yahoo.com', 1, 'e2f711ed5928b0bf6745fe6a6762cf29', 0, 0, 46);
INSERT INTO `user` VALUES (121, 'aaron', 'skillman', 'skillzie@gmail.com', 0, '69ba4ee06c5cb1ba8a7e6f468246e21f', 0, 0, 47);
INSERT INTO `user` VALUES (122, 'aaron', 'skillman', 'skillzie@hotmail.com', 0, '541627c05dac960151b17da245d33006', 0, 0, 48);
INSERT INTO `user` VALUES (123, 'Daniel', 'Andersson', 'daniel@andersson.pp.se', 1, '11a22927c85333ce98510b0fa1a1c27d', 1, 0, 49);
INSERT INTO `user` VALUES (124, 'fabian2', 'fabian2', 'supagu@hotmail.com', 1, '0f6811b05660fde57651fda214919653', 1, 0, 50);

-- --------------------------------------------------------

-- 
-- Table structure for table `userdownload`
-- 

CREATE TABLE `userdownload` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userid` int(10) unsigned NOT NULL default '0',
  `downloadid` int(10) unsigned NOT NULL default '0',
  `orderid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=36 ;

-- 
-- Dumping data for table `userdownload`
-- 

INSERT INTO `userdownload` VALUES (18, 78, 29, 186);
INSERT INTO `userdownload` VALUES (17, 90, 28, 188);
INSERT INTO `userdownload` VALUES (16, 78, 27, 183);
INSERT INTO `userdownload` VALUES (19, 91, 30, 190);
INSERT INTO `userdownload` VALUES (20, 94, 31, 194);
INSERT INTO `userdownload` VALUES (21, 94, 32, 195);
INSERT INTO `userdownload` VALUES (22, 94, 33, 196);
INSERT INTO `userdownload` VALUES (23, 92, 34, 197);
INSERT INTO `userdownload` VALUES (28, 78, 39, 235);
INSERT INTO `userdownload` VALUES (29, 78, 41, 236);
INSERT INTO `userdownload` VALUES (30, 115, 42, 239);
INSERT INTO `userdownload` VALUES (31, 117, 43, 241);
INSERT INTO `userdownload` VALUES (32, 118, 44, 240);
INSERT INTO `userdownload` VALUES (33, 123, 45, 242);
INSERT INTO `userdownload` VALUES (34, 78, 39, 247);
INSERT INTO `userdownload` VALUES (35, 78, 39, 248);

-- --------------------------------------------------------

-- 
-- Table structure for table `uvtoolslicence`
-- 

CREATE TABLE `uvtoolslicence` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userid` int(10) unsigned NOT NULL default '0',
  `type` smallint(5) unsigned NOT NULL default '0',
  `maxid` varchar(32) NOT NULL default '0',
  `number` int(10) unsigned NOT NULL default '0',
  `companyname` varchar(255) NOT NULL default '',
  `verified` tinyint(1) NOT NULL default '0',
  `companyemail` varchar(255) NOT NULL default '',
  `orderid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=214 ;

-- 
-- Dumping data for table `uvtoolslicence`
-- 

INSERT INTO `uvtoolslicence` VALUES (204, 100, 0, '1', 1, '', 0, '', 202);
INSERT INTO `uvtoolslicence` VALUES (203, 80, 0, '111111', 1, '', 0, '', 201);
INSERT INTO `uvtoolslicence` VALUES (200, 99, 1, '0', 1, 'pixelpusher digital', 0, 'pixelpusher75@sbcglobal.net', 0);
INSERT INTO `uvtoolslicence` VALUES (199, 92, 0, '1819643555', 1, '', 0, '', 197);
INSERT INTO `uvtoolslicence` VALUES (198, 94, 0, '1604355633', 1, '', 0, '', 196);
INSERT INTO `uvtoolslicence` VALUES (197, 94, 0, '1694174322', 1, '', 0, '', 195);
INSERT INTO `uvtoolslicence` VALUES (196, 94, 0, '1070696317', 1, '', 0, '', 194);
INSERT INTO `uvtoolslicence` VALUES (195, 94, 1, '0', 6, 'FUNCOM AS', 0, 'fabricem@funcom.com', 0);
INSERT INTO `uvtoolslicence` VALUES (194, 78, 0, '4444', 1, '', 0, '', 192);
INSERT INTO `uvtoolslicence` VALUES (188, 78, 0, '111', 1, '', 0, '', 186);
INSERT INTO `uvtoolslicence` VALUES (189, 83, 1, '0', 1, 'LOI', 0, 'bazuka666@yahoo.com', 0);
INSERT INTO `uvtoolslicence` VALUES (190, 90, 0, '-2053381146', 1, '', 0, '', 188);
INSERT INTO `uvtoolslicence` VALUES (193, 90, 0, '-1597165220', 1, '', 0, '', 191);
INSERT INTO `uvtoolslicence` VALUES (192, 91, 0, '821906827', 1, '', 0, '', 190);
INSERT INTO `uvtoolslicence` VALUES (205, 78, 0, '12345678', 1, '', 0, '', 236);
INSERT INTO `uvtoolslicence` VALUES (206, 110, 0, '1571074215', 1, '', 0, '', 237);
INSERT INTO `uvtoolslicence` VALUES (207, 113, 0, '-1442242157', 1, '', 0, '', 238);
INSERT INTO `uvtoolslicence` VALUES (208, 115, 0, '1172852168', 1, '', 0, '', 239);
INSERT INTO `uvtoolslicence` VALUES (209, 118, 0, '1102967445', 1, '', 0, '', 240);
INSERT INTO `uvtoolslicence` VALUES (210, 117, 0, '400619507', 1, '', 0, '', 241);
INSERT INTO `uvtoolslicence` VALUES (211, 123, 0, '-656008628', 1, '', 0, '', 242);
INSERT INTO `uvtoolslicence` VALUES (212, 78, 0, '87654321', 1, '', 0, '', 243);
INSERT INTO `uvtoolslicence` VALUES (213, 78, 0, '33443344', 1, '', 0, '', 244);
