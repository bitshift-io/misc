-- phpMyAdmin SQL Dump
-- version 2.8.0.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jul 01, 2006 at 02:43 PM
-- Server version: 4.0.27
-- PHP Version: 4.4.2
-- 
-- Database: `blackcar_blackcarbon`
-- 
CREATE DATABASE `blackcar_blackcarbon`;
USE blackcar_blackcarbon;

-- --------------------------------------------------------

-- 
-- Table structure for table `download`
-- 

CREATE TABLE `download` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `url` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `downloads` int(10) unsigned NOT NULL default '0',
  `loginrequired` tinyint(1) NOT NULL default '0',
  `productid` int(11) NOT NULL default '-1',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=28 ;

-- 
-- Dumping data for table `download`
-- 

INSERT INTO `download` VALUES (0, 'UVTools1.0.zip', 'UVTools1.0.zip', 63, 0, 1);
INSERT INTO `download` VALUES (27, 'supagu64@yahoo.com/licence15.dat', 'licence.dat', 1, 1, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `orderform`
-- 

CREATE TABLE `orderform` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `productid` int(10) unsigned NOT NULL default '0',
  `userid` int(10) unsigned NOT NULL default '0',
  `status` smallint(5) NOT NULL default '0',
  `number` int(10) unsigned NOT NULL default '1',
  `paypalid` varchar(255) NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `amount` float NOT NULL default '0',
  KEY `id` (`id`)
) TYPE=MyISAM AUTO_INCREMENT=187 ;

-- 
-- Dumping data for table `orderform`
-- 

INSERT INTO `orderform` VALUES (186, 1, 78, 4, 1, '5HB1153926193141P', '2006-07-01 14:34:14', 30);
INSERT INTO `orderform` VALUES (183, 1, 78, 3, 1, '1LE84449ED216264M', '2006-06-25 15:50:30', 30);

-- --------------------------------------------------------

-- 
-- Table structure for table `product`
-- 

CREATE TABLE `product` (
  `id` int(10) unsigned NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `price` float NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `product`
-- 

INSERT INTO `product` VALUES (0, 'Explosive', 20);
INSERT INTO `product` VALUES (1, 'UV Tools', 30);

-- --------------------------------------------------------

-- 
-- Table structure for table `session`
-- 

CREATE TABLE `session` (
  `auth` varchar(32) NOT NULL default '',
  `userid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`auth`),
  UNIQUE KEY `id` (`auth`,`userid`),
  FULLTEXT KEY `auth` (`auth`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `session`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `firstname` varchar(128) NOT NULL default '',
  `lastname` varchar(128) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `mailinglist` tinyint(1) NOT NULL default '0',
  `password` varchar(32) NOT NULL default '',
  `verified` tinyint(1) NOT NULL default '0',
  `admin` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=81 ;

-- 
-- Dumping data for table `user`
-- 

INSERT INTO `user` VALUES (80, 'Bronson', 'Mathews', 'gibbz1@yahoo.com', 1, '0f6811b05660fde57651fda214919653', 1, 1);
INSERT INTO `user` VALUES (75, 'Damir', 'Simovski', 'BCarbon@simovski.com', 0, 'c37c9ca0ec5b2c0eacad336ee4c66cc6', 1, 0);
INSERT INTO `user` VALUES (78, 'Fabian', 'Mathews', 'supagu64@yahoo.com', 1, '0f6811b05660fde57651fda214919653', 1, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `userdownload`
-- 

CREATE TABLE `userdownload` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userid` int(10) unsigned NOT NULL default '0',
  `downloadid` int(10) unsigned NOT NULL default '0',
  `orderid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=17 ;

-- 
-- Dumping data for table `userdownload`
-- 

INSERT INTO `userdownload` VALUES (16, 78, 27, 183);

-- --------------------------------------------------------

-- 
-- Table structure for table `uvtoolslicence`
-- 

CREATE TABLE `uvtoolslicence` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userid` int(10) unsigned NOT NULL default '0',
  `type` smallint(5) unsigned NOT NULL default '0',
  `maxid` varchar(32) NOT NULL default '0',
  `number` int(10) unsigned NOT NULL default '0',
  `companyname` varchar(255) NOT NULL default '',
  `verified` tinyint(1) NOT NULL default '0',
  `companyemail` varchar(255) NOT NULL default '',
  `orderid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=189 ;

-- 
-- Dumping data for table `uvtoolslicence`
-- 

INSERT INTO `uvtoolslicence` VALUES (188, 78, 0, '111', 1, '', 0, '', 186);
INSERT INTO `uvtoolslicence` VALUES (181, 78, 0, '-1430656134', 1, '', 0, '', 183);
