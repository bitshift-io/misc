-- phpMyAdmin SQL Dump
-- version 2.9.0.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Dec 26, 2006 at 04:23 PM
-- Server version: 4.0.27
-- PHP Version: 4.4.2
-- 
-- Database: `blackcar_blackcarbon`
-- 
CREATE DATABASE `blackcar_blackcarbon`;
USE blackcar_blackcarbon;

-- --------------------------------------------------------

-- 
-- Table structure for table `download`
-- 

CREATE TABLE `download` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `host` varchar(255) NOT NULL default '',
  `path` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `downloads` int(10) unsigned NOT NULL default '0',
  `loginrequired` tinyint(1) NOT NULL default '0',
  `productid` int(11) NOT NULL default '-1',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=37 ;

-- 
-- Dumping data for table `download`
-- 

INSERT INTO `download` VALUES (0, '', 'UVTools1.0.zip', 'UVTools1.0.zip', 1000, 0, 1);
INSERT INTO `download` VALUES (32, '', 'fabricem@funcom.com/licence1.dat', 'licence.dat', 1, 1, 1);
INSERT INTO `download` VALUES (33, '', 'fabricem@funcom.com/licence2.dat', 'licence.dat', 1, 1, 1);
INSERT INTO `download` VALUES (34, '', 'oyvind@funcom.com/licence0.dat', 'licence.dat', 1, 1, 1);
INSERT INTO `download` VALUES (31, '', 'fabricem@funcom.com/licence0.dat', 'licence.dat', 1, 1, 1);
INSERT INTO `download` VALUES (30, '', 'richardc@funcom.com/licence0.dat', 'licence.dat', 3, 1, 1);
INSERT INTO `download` VALUES (29, '', 'supagu64@yahoo.com/licence16.dat', 'licence.dat', 0, 1, 1);
INSERT INTO `download` VALUES (28, '', 'kjetilh@funcom.com/licence0.dat', 'licence.dat', 4, 1, 1);
INSERT INTO `download` VALUES (27, '', 'supagu64@yahoo.com/licence15.dat', 'licence.dat', 1, 1, 1);
INSERT INTO `download` VALUES (1, 'http://members.iinet.com.au', '/~gibbz/ExplosiveDemo.exe', 'ExplosiveDemo.exe', 32, 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `masterserver_joiningclients`
-- 

CREATE TABLE `masterserver_joiningclients` (
  `ip` varchar(255) NOT NULL default '',
  `serverid` int(10) unsigned NOT NULL default '0',
  `port` int(10) unsigned NOT NULL default '0'
) TYPE=MyISAM;

-- 
-- Dumping data for table `masterserver_joiningclients`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `masterserver_serverclients`
-- 

CREATE TABLE `masterserver_serverclients` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `keyhash` varchar(255) NOT NULL default '',
  `serverid` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `masterserver_serverclients`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `masterserver_serverinfo`
-- 

CREATE TABLE `masterserver_serverinfo` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `gamemode` varchar(255) NOT NULL default '',
  `mapname` varchar(255) NOT NULL default '',
  `modname` varchar(255) NOT NULL default '',
  `version` varchar(255) NOT NULL default '0',
  `ip` varchar(255) NOT NULL default '',
  `port` int(10) unsigned NOT NULL default '0',
  `timestamp` time NOT NULL default '00:00:00',
  `gamename` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=897 ;

-- 
-- Dumping data for table `masterserver_serverinfo`
-- 

INSERT INTO `masterserver_serverinfo` VALUES (891, '\\"fabs server\\"', '', '', '', '1.0', '203.206.38.122', 6000, '00:00:00', 'Air Blimbs');
INSERT INTO `masterserver_serverinfo` VALUES (890, '\\"fabs server\\"', '', '', '', '1.0', '203.206.38.122', 6000, '00:00:00', 'Air Blimbs');
INSERT INTO `masterserver_serverinfo` VALUES (892, '\\"fabs server\\"', '', '', '', '1.0', '203.206.38.122', 6000, '838:59:59', 'Air Blimbs');
INSERT INTO `masterserver_serverinfo` VALUES (893, '\\"fabs server\\"', '', '', '', '1.0', '203.206.38.122', 6000, '838:59:59', 'Air Blimbs');
INSERT INTO `masterserver_serverinfo` VALUES (894, '\\"fabs server\\"', '', '', '', '1.0', '203.206.38.122', 6000, '00:00:00', 'Air Blimbs');
INSERT INTO `masterserver_serverinfo` VALUES (895, '\\"fabs server\\"', '', '', '', '1.0', '203.206.38.122', 6000, '838:59:59', 'Air Blimbs');
INSERT INTO `masterserver_serverinfo` VALUES (896, '\\"fabs server\\"', '', '', '', '1.0', '203.206.38.122', 6000, '838:59:59', 'Air Blimbs');

-- --------------------------------------------------------

-- 
-- Table structure for table `orderform`
-- 

CREATE TABLE `orderform` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `productid` int(10) unsigned NOT NULL default '0',
  `userid` int(10) unsigned NOT NULL default '0',
  `status` smallint(5) NOT NULL default '0',
  `number` int(10) unsigned NOT NULL default '1',
  `paypalid` varchar(255) NOT NULL default '0',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `amount` float NOT NULL default '0',
  KEY `id` (`id`)
) TYPE=MyISAM AUTO_INCREMENT=203 ;

-- 
-- Dumping data for table `orderform`
-- 

INSERT INTO `orderform` VALUES (202, 1, 100, 0, 1, '0', '2006-12-07 02:29:38', 0);
INSERT INTO `orderform` VALUES (197, 1, 92, 3, 1, '4VY222838F2223005', '2006-11-15 00:47:02', 30);
INSERT INTO `orderform` VALUES (196, 1, 94, 3, 1, '5SJ05308D4527353P', '2006-11-06 21:29:59', 30);
INSERT INTO `orderform` VALUES (195, 1, 94, 3, 1, '16R14666WD864794F', '2006-11-03 21:27:58', 30);
INSERT INTO `orderform` VALUES (198, 1, 99, 4, 1, '9RW12359N65570359', '2006-12-02 08:30:54', 30);
INSERT INTO `orderform` VALUES (194, 1, 94, 3, 1, '6GA62822LN191760W', '2006-10-24 18:50:29', 30);
INSERT INTO `orderform` VALUES (190, 1, 91, 3, 1, '72388389R71267201', '2006-10-10 19:15:18', 30);
INSERT INTO `orderform` VALUES (188, 1, 90, 3, 1, '86A4766152311470H', '2006-10-09 21:00:24', 30);
INSERT INTO `orderform` VALUES (187, 1, 83, 0, 1, '0', '2006-08-20 19:02:33', 0);
INSERT INTO `orderform` VALUES (186, 1, 78, 3, 1, '5HB1153926193141P', '2006-07-01 14:34:14', 30);
INSERT INTO `orderform` VALUES (183, 1, 78, 3, 1, '1LE84449ED216264M', '2006-06-25 15:50:30', 30);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_auth_access`
-- 

CREATE TABLE `phpbb_auth_access` (
  `group_id` mediumint(8) NOT NULL default '0',
  `forum_id` smallint(5) unsigned NOT NULL default '0',
  `auth_view` tinyint(1) NOT NULL default '0',
  `auth_read` tinyint(1) NOT NULL default '0',
  `auth_post` tinyint(1) NOT NULL default '0',
  `auth_reply` tinyint(1) NOT NULL default '0',
  `auth_edit` tinyint(1) NOT NULL default '0',
  `auth_delete` tinyint(1) NOT NULL default '0',
  `auth_sticky` tinyint(1) NOT NULL default '0',
  `auth_announce` tinyint(1) NOT NULL default '0',
  `auth_vote` tinyint(1) NOT NULL default '0',
  `auth_pollcreate` tinyint(1) NOT NULL default '0',
  `auth_attachments` tinyint(1) NOT NULL default '0',
  `auth_mod` tinyint(1) NOT NULL default '0',
  KEY `group_id` (`group_id`),
  KEY `forum_id` (`forum_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_auth_access`
-- 

INSERT INTO `phpbb_auth_access` VALUES (5, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0);
INSERT INTO `phpbb_auth_access` VALUES (6, 6, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_banlist`
-- 

CREATE TABLE `phpbb_banlist` (
  `ban_id` mediumint(8) unsigned NOT NULL auto_increment,
  `ban_userid` mediumint(8) NOT NULL default '0',
  `ban_ip` varchar(8) NOT NULL default '',
  `ban_email` varchar(255) default NULL,
  PRIMARY KEY  (`ban_id`),
  KEY `ban_ip_user_id` (`ban_ip`,`ban_userid`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `phpbb_banlist`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_categories`
-- 

CREATE TABLE `phpbb_categories` (
  `cat_id` mediumint(8) unsigned NOT NULL auto_increment,
  `cat_title` varchar(100) default NULL,
  `cat_order` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`cat_id`),
  KEY `cat_order` (`cat_order`)
) TYPE=MyISAM AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `phpbb_categories`
-- 

INSERT INTO `phpbb_categories` VALUES (1, 'Explosive', 30);
INSERT INTO `phpbb_categories` VALUES (3, 'UV Tools', 20);
INSERT INTO `phpbb_categories` VALUES (4, 'Air Blimbs', 40);
INSERT INTO `phpbb_categories` VALUES (5, 'General', 10);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_config`
-- 

CREATE TABLE `phpbb_config` (
  `config_name` varchar(255) NOT NULL default '',
  `config_value` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`config_name`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_config`
-- 

INSERT INTO `phpbb_config` VALUES ('config_id', '1');
INSERT INTO `phpbb_config` VALUES ('board_disable', '0');
INSERT INTO `phpbb_config` VALUES ('sitename', 'Black Carbon');
INSERT INTO `phpbb_config` VALUES ('site_desc', '');
INSERT INTO `phpbb_config` VALUES ('cookie_name', 'phpbb2mysql');
INSERT INTO `phpbb_config` VALUES ('cookie_path', '/');
INSERT INTO `phpbb_config` VALUES ('cookie_domain', '');
INSERT INTO `phpbb_config` VALUES ('cookie_secure', '0');
INSERT INTO `phpbb_config` VALUES ('session_length', '3600');
INSERT INTO `phpbb_config` VALUES ('allow_html', '0');
INSERT INTO `phpbb_config` VALUES ('allow_html_tags', 'b,i,u,pre');
INSERT INTO `phpbb_config` VALUES ('allow_bbcode', '1');
INSERT INTO `phpbb_config` VALUES ('allow_smilies', '1');
INSERT INTO `phpbb_config` VALUES ('allow_sig', '1');
INSERT INTO `phpbb_config` VALUES ('allow_namechange', '0');
INSERT INTO `phpbb_config` VALUES ('allow_theme_create', '0');
INSERT INTO `phpbb_config` VALUES ('allow_avatar_local', '0');
INSERT INTO `phpbb_config` VALUES ('allow_avatar_remote', '0');
INSERT INTO `phpbb_config` VALUES ('allow_avatar_upload', '0');
INSERT INTO `phpbb_config` VALUES ('enable_confirm', '1');
INSERT INTO `phpbb_config` VALUES ('allow_autologin', '1');
INSERT INTO `phpbb_config` VALUES ('max_autologin_time', '0');
INSERT INTO `phpbb_config` VALUES ('override_user_style', '0');
INSERT INTO `phpbb_config` VALUES ('posts_per_page', '15');
INSERT INTO `phpbb_config` VALUES ('topics_per_page', '50');
INSERT INTO `phpbb_config` VALUES ('hot_threshold', '10');
INSERT INTO `phpbb_config` VALUES ('max_poll_options', '10');
INSERT INTO `phpbb_config` VALUES ('max_sig_chars', '255');
INSERT INTO `phpbb_config` VALUES ('max_inbox_privmsgs', '10');
INSERT INTO `phpbb_config` VALUES ('max_sentbox_privmsgs', '10');
INSERT INTO `phpbb_config` VALUES ('max_savebox_privmsgs', '10');
INSERT INTO `phpbb_config` VALUES ('board_email_sig', 'Thanks, The BlackCarbon Team');
INSERT INTO `phpbb_config` VALUES ('board_email', 'support@blackcarbon.net');
INSERT INTO `phpbb_config` VALUES ('smtp_delivery', '0');
INSERT INTO `phpbb_config` VALUES ('smtp_host', '');
INSERT INTO `phpbb_config` VALUES ('smtp_username', '');
INSERT INTO `phpbb_config` VALUES ('smtp_password', '');
INSERT INTO `phpbb_config` VALUES ('sendmail_fix', '0');
INSERT INTO `phpbb_config` VALUES ('require_activation', '1');
INSERT INTO `phpbb_config` VALUES ('flood_interval', '15');
INSERT INTO `phpbb_config` VALUES ('search_flood_interval', '15');
INSERT INTO `phpbb_config` VALUES ('search_min_chars', '3');
INSERT INTO `phpbb_config` VALUES ('max_login_attempts', '5');
INSERT INTO `phpbb_config` VALUES ('login_reset_time', '30');
INSERT INTO `phpbb_config` VALUES ('board_email_form', '0');
INSERT INTO `phpbb_config` VALUES ('avatar_filesize', '6144');
INSERT INTO `phpbb_config` VALUES ('avatar_max_width', '80');
INSERT INTO `phpbb_config` VALUES ('avatar_max_height', '80');
INSERT INTO `phpbb_config` VALUES ('avatar_path', 'images/avatars');
INSERT INTO `phpbb_config` VALUES ('avatar_gallery_path', 'images/avatars/gallery');
INSERT INTO `phpbb_config` VALUES ('smilies_path', 'images/smiles');
INSERT INTO `phpbb_config` VALUES ('default_style', '3');
INSERT INTO `phpbb_config` VALUES ('default_dateformat', 'D M d, Y g:i a');
INSERT INTO `phpbb_config` VALUES ('board_timezone', '0');
INSERT INTO `phpbb_config` VALUES ('prune_enable', '0');
INSERT INTO `phpbb_config` VALUES ('privmsg_disable', '0');
INSERT INTO `phpbb_config` VALUES ('gzip_compress', '1');
INSERT INTO `phpbb_config` VALUES ('coppa_fax', '');
INSERT INTO `phpbb_config` VALUES ('coppa_mail', '');
INSERT INTO `phpbb_config` VALUES ('record_online_users', '6');
INSERT INTO `phpbb_config` VALUES ('record_online_date', '1160395685');
INSERT INTO `phpbb_config` VALUES ('server_name', 'www.blackcarbon.net');
INSERT INTO `phpbb_config` VALUES ('server_port', '80');
INSERT INTO `phpbb_config` VALUES ('script_path', '/forum/');
INSERT INTO `phpbb_config` VALUES ('version', '.0.21');
INSERT INTO `phpbb_config` VALUES ('rand_seed', '2e10c37d109708be45b6dc2caefec6da');
INSERT INTO `phpbb_config` VALUES ('board_startdate', '1160385193');
INSERT INTO `phpbb_config` VALUES ('default_lang', 'english');

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_confirm`
-- 

CREATE TABLE `phpbb_confirm` (
  `confirm_id` char(32) NOT NULL default '',
  `session_id` char(32) NOT NULL default '',
  `code` char(6) NOT NULL default '',
  PRIMARY KEY  (`session_id`,`confirm_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_confirm`
-- 

INSERT INTO `phpbb_confirm` VALUES ('28574efa316ed2714fbe71eba569dda1', '1a6814581bc797054b317d552700f946', 'I93LKS');

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_disallow`
-- 

CREATE TABLE `phpbb_disallow` (
  `disallow_id` mediumint(8) unsigned NOT NULL auto_increment,
  `disallow_username` varchar(25) NOT NULL default '',
  PRIMARY KEY  (`disallow_id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `phpbb_disallow`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_forum_prune`
-- 

CREATE TABLE `phpbb_forum_prune` (
  `prune_id` mediumint(8) unsigned NOT NULL auto_increment,
  `forum_id` smallint(5) unsigned NOT NULL default '0',
  `prune_days` smallint(5) unsigned NOT NULL default '0',
  `prune_freq` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`prune_id`),
  KEY `forum_id` (`forum_id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `phpbb_forum_prune`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_forums`
-- 

CREATE TABLE `phpbb_forums` (
  `forum_id` smallint(5) unsigned NOT NULL default '0',
  `cat_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_name` varchar(150) default NULL,
  `forum_desc` text,
  `forum_status` tinyint(4) NOT NULL default '0',
  `forum_order` mediumint(8) unsigned NOT NULL default '1',
  `forum_posts` mediumint(8) unsigned NOT NULL default '0',
  `forum_topics` mediumint(8) unsigned NOT NULL default '0',
  `forum_last_post_id` mediumint(8) unsigned NOT NULL default '0',
  `prune_next` int(11) default NULL,
  `prune_enable` tinyint(1) NOT NULL default '0',
  `auth_view` tinyint(2) NOT NULL default '0',
  `auth_read` tinyint(2) NOT NULL default '0',
  `auth_post` tinyint(2) NOT NULL default '0',
  `auth_reply` tinyint(2) NOT NULL default '0',
  `auth_edit` tinyint(2) NOT NULL default '0',
  `auth_delete` tinyint(2) NOT NULL default '0',
  `auth_sticky` tinyint(2) NOT NULL default '0',
  `auth_announce` tinyint(2) NOT NULL default '0',
  `auth_vote` tinyint(2) NOT NULL default '0',
  `auth_pollcreate` tinyint(2) NOT NULL default '0',
  `auth_attachments` tinyint(2) NOT NULL default '0',
  PRIMARY KEY  (`forum_id`),
  KEY `forums_order` (`forum_order`),
  KEY `cat_id` (`cat_id`),
  KEY `forum_last_post_id` (`forum_last_post_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_forums`
-- 

INSERT INTO `phpbb_forums` VALUES (1, 1, 'Testing/Bug Report', 'Got a bug to report?', 0, 20, 17, 6, 37, NULL, 0, 0, 2, 2, 2, 2, 2, 2, 3, 2, 2, 3);
INSERT INTO `phpbb_forums` VALUES (2, 1, 'General', '', 0, 10, 1, 1, 29, NULL, 0, 0, 0, 1, 1, 1, 1, 3, 3, 1, 1, 0);
INSERT INTO `phpbb_forums` VALUES (3, 3, 'General', 'Discuss uses, tips etc.', 0, 10, 2, 2, 21, NULL, 0, 0, 0, 1, 1, 1, 1, 3, 3, 1, 1, 0);
INSERT INTO `phpbb_forums` VALUES (4, 4, 'General', '', 0, 10, 1, 1, 36, NULL, 0, 0, 0, 1, 1, 1, 1, 3, 3, 1, 1, 0);
INSERT INTO `phpbb_forums` VALUES (5, 3, 'Feedback', 'Bugs/Support/Feedback', 0, 20, 0, 0, 0, NULL, 0, 0, 0, 1, 1, 1, 1, 3, 3, 1, 1, 0);
INSERT INTO `phpbb_forums` VALUES (6, 4, 'Testing/Bug Report', 'Got a bug to report?', 0, 20, 0, 0, 0, NULL, 0, 0, 2, 2, 2, 2, 2, 2, 3, 2, 2, 0);
INSERT INTO `phpbb_forums` VALUES (7, 5, 'News & Announcements', 'Product News and Announcements', 0, 10, 13, 3, 38, NULL, 0, 0, 0, 3, 0, 3, 3, 3, 3, 3, 3, 0);
INSERT INTO `phpbb_forums` VALUES (8, 5, 'General', 'Off topic discussions', 0, 20, 0, 0, 0, NULL, 0, 0, 0, 1, 1, 1, 1, 3, 3, 1, 1, 0);
INSERT INTO `phpbb_forums` VALUES (9, 1, 'Modding', 'Modding/Level Creation', 0, 30, 0, 0, 0, NULL, 0, 0, 0, 1, 1, 1, 1, 3, 3, 1, 1, 0);
INSERT INTO `phpbb_forums` VALUES (10, 4, 'Modding', 'Modding/Level Creation', 0, 30, 0, 0, 0, NULL, 0, 0, 0, 1, 1, 1, 1, 3, 3, 1, 1, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_groups`
-- 

CREATE TABLE `phpbb_groups` (
  `group_id` mediumint(8) NOT NULL auto_increment,
  `group_type` tinyint(4) NOT NULL default '1',
  `group_name` varchar(40) NOT NULL default '',
  `group_description` varchar(255) NOT NULL default '',
  `group_moderator` mediumint(8) NOT NULL default '0',
  `group_single_user` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`group_id`),
  KEY `group_single_user` (`group_single_user`)
) TYPE=MyISAM AUTO_INCREMENT=21 ;

-- 
-- Dumping data for table `phpbb_groups`
-- 

INSERT INTO `phpbb_groups` VALUES (1, 1, 'Anonymous', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (2, 1, 'Admin', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (3, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (4, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (5, 0, 'Explosive Beta', 'The Explosive Beta Team', 2, 0);
INSERT INTO `phpbb_groups` VALUES (6, 0, 'Air Blimbs Beta', 'The Air Blimbs beta team', 2, 0);
INSERT INTO `phpbb_groups` VALUES (7, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (8, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (9, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (10, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (11, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (12, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (13, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (14, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (15, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (16, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (17, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (18, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (19, 1, '', 'Personal User', 0, 1);
INSERT INTO `phpbb_groups` VALUES (20, 1, '', 'Personal User', 0, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_posts`
-- 

CREATE TABLE `phpbb_posts` (
  `post_id` mediumint(8) unsigned NOT NULL auto_increment,
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `forum_id` smallint(5) unsigned NOT NULL default '0',
  `poster_id` mediumint(8) NOT NULL default '0',
  `post_time` int(11) NOT NULL default '0',
  `poster_ip` varchar(8) NOT NULL default '',
  `post_username` varchar(25) default NULL,
  `enable_bbcode` tinyint(1) NOT NULL default '1',
  `enable_html` tinyint(1) NOT NULL default '0',
  `enable_smilies` tinyint(1) NOT NULL default '1',
  `enable_sig` tinyint(1) NOT NULL default '1',
  `post_edit_time` int(11) default NULL,
  `post_edit_count` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`post_id`),
  KEY `forum_id` (`forum_id`),
  KEY `topic_id` (`topic_id`),
  KEY `poster_id` (`poster_id`),
  KEY `post_time` (`post_time`)
) TYPE=MyISAM AUTO_INCREMENT=39 ;

-- 
-- Dumping data for table `phpbb_posts`
-- 

INSERT INTO `phpbb_posts` VALUES (7, 6, 3, 3, 1160394445, 'cbce2dfe', '', 1, 0, 1, 0, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (6, 5, 1, 2, 1160394251, 'cbce2dfe', '', 1, 0, 1, 0, 1167043850, 3);
INSERT INTO `phpbb_posts` VALUES (4, 3, 7, 3, 1160392536, 'cbce2dfe', '', 1, 0, 1, 0, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (5, 4, 7, 2, 1160394038, 'cbce2dfe', '', 1, 0, 1, 0, 1160396025, 1);
INSERT INTO `phpbb_posts` VALUES (9, 4, 7, 5, 1160395636, '3ba76771', '', 1, 0, 1, 0, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (10, 4, 7, 2, 1160395906, 'cbce2dfe', '', 1, 0, 1, 1, 1160396249, 1);
INSERT INTO `phpbb_posts` VALUES (11, 4, 7, 3, 1160396114, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (12, 4, 7, -1, 1160396845, 'cbd50785', '', 1, 0, 1, 0, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (13, 7, 1, 5, 1160399296, '3ba76771', '', 1, 0, 1, 0, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (14, 4, 7, 7, 1160401072, 'd8f40ff5', '', 1, 0, 1, 0, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (15, 7, 1, 2, 1160432435, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (16, 8, 1, 2, 1160432524, 'cbce2dfe', '', 1, 0, 1, 1, 1160476378, 1);
INSERT INTO `phpbb_posts` VALUES (17, 4, 7, 2, 1160432818, 'cbce2dfe', '', 1, 0, 1, 1, 1160469063, 1);
INSERT INTO `phpbb_posts` VALUES (18, 8, 1, 5, 1160438258, 'd2b95a6b', '', 1, 0, 1, 0, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (19, 9, 1, 2, 1160450149, 'cb5e8c05', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (20, 4, 7, 8, 1160453018, 'cb5e8c05', '', 1, 0, 1, 0, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (21, 10, 3, 3, 1160469051, 'cbce2dfe', '', 1, 0, 1, 0, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (22, 9, 1, 2, 1160476419, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (23, 11, 1, 7, 1160487784, 'd8f40ff5', '', 1, 0, 1, 1, 1160628777, 1);
INSERT INTO `phpbb_posts` VALUES (24, 11, 1, 3, 1160518755, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (25, 8, 1, 3, 1160519057, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (26, 11, 1, 7, 1160521166, 'd8f40ff5', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (27, 11, 1, 2, 1160556095, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (28, 11, 1, 7, 1160628323, 'd8f40ff5', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (29, 12, 2, 2, 1160639909, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (30, 11, 1, 2, 1160639998, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (31, 13, 1, 2, 1160641140, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (32, 4, 7, -1, 1160697780, '90860188', 'edd9139', 1, 0, 1, 0, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (33, 4, 7, 2, 1160733326, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (34, 4, 7, 9, 1160736161, '9086011f', '', 1, 0, 1, 0, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (35, 5, 1, 2, 1160797189, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (36, 14, 4, 2, 1160797571, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (37, 5, 1, 2, 1160810372, 'cbce2dfe', '', 1, 0, 1, 1, NULL, 0);
INSERT INTO `phpbb_posts` VALUES (38, 15, 7, 2, 1167044064, 'cbce2cec', '', 1, 0, 1, 1, NULL, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_posts_text`
-- 

CREATE TABLE `phpbb_posts_text` (
  `post_id` mediumint(8) unsigned NOT NULL default '0',
  `bbcode_uid` varchar(10) NOT NULL default '',
  `post_subject` varchar(60) default NULL,
  `post_text` text,
  PRIMARY KEY  (`post_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_posts_text`
-- 

INSERT INTO `phpbb_posts_text` VALUES (7, '291bc250b1', 'UV Tools Information', 'For the UV Tools information page including a link to download the 20 use trial follow the link below.\r\n\r\n[url]http://blackcarbon.net/uvtools/[/url]\r\n\r\nPlease feel free to post feedback.');
INSERT INTO `phpbb_posts_text` VALUES (6, '2423c82e17', 'Beta Download &amp; Reporting Tips', 'http://members.iinet.com.au/~gibbz/ExplosiveDemo.exe\r\n\r\nTips for useful bug reporting:\r\n-Check that the bug has not already been posted by some one else\r\n-How repeatable is the problem? Often, all the time, rarely\r\n-How to repeat the problem (may not be possible to specify for rare bugs)\r\n-Post the contents of the system.log which should be in the same directory as the exe\r\n-Post your computer specs (put this in your signature)\r\n-Title your post appropriately, start any new bug report posts with [BUG] then the category they relate to if known eg. [ART], [CODE], [SOUND]');
INSERT INTO `phpbb_posts_text` VALUES (4, 'a413fe7c13', 'UV Tools 1.0 Release', 'Hi all,\r\n\r\nWe have just released version 1.0 of UV Tools for 3D studio max 8.\r\nUV Tools is a tool to aid in uvw mapping, both reducing the time needed to uvw map objects, and also to add extra functionallity to help with uvw mapping.\r\n\r\nUV Tools was originally designed for the purpose of speeding up uvw mapping for game environments, however it can be used for characters and other objects just as easily.\r\n\r\nFor more information on uv tools, including videos showing off some key features, and a 20 use demo version visit our website.\r\n\r\n[url]www.blackcarbon.net[/url]\r\n\r\nThanks');
INSERT INTO `phpbb_posts_text` VALUES (5, '68d563c006', 'Explosive now in Beta - Looking for Beta testers', 'Explosive is now at the stage were we need to test the multiplayer aspects of the game.\r\nIf you''d like to become a beta tester post a reply to this thread.\r\n\r\nPlease post computer specs, operating system (service packs, optional extras if known, eg. .NET) and any other relavant info\r\n\r\nNote we are on an Aussie time schedule (+9.30 GMT)');
INSERT INTO `phpbb_posts_text` VALUES (9, '01d3963a47', '', 'Hi there,\r\n\r\nI''m in Brisbane, I work fulltime but will be able to spare an hour here or there to help test. I''m actually working on a mp casual game that we hope to release in the near future so maybe we could share testing teams :) I could probably get a couple of our devs to help test too. \r\nJust FYI, our game is a type of online gameshow based around quiz, so its quite different from your game.\r\n\r\nMost nights are good for testing, tuesday, sat and sun are usually not good for me however.');
INSERT INTO `phpbb_posts_text` VALUES (10, '4954a0e39d', '', 'sounds cool. Just check you can get to the beta testing forum under explosive now.');
INSERT INTO `phpbb_posts_text` VALUES (11, 'a397123482', '', 'Im located in Brisbane also  :D');
INSERT INTO `phpbb_posts_text` VALUES (12, 'f3cef941b2', '', 'Hi, I remember when this game was first promoted on the OCAU Gaming forums a few months back. I go by &quot;Dedge&quot; on the OCAU forums and I''m putting my hand up to help beta-test. I''m also located in Brisbane.\r\n\r\nSystem Specs (Nothing fancy):\r\n\r\nAXP 2500+\r\n1GB RAM\r\nNVidia 6800GT\r\nArch Linux (2.6.18)\r\n\r\nIf I recall, you were developing a Linux port and I can be your test monkey for that :)\r\n\r\nCheers,\r\nTate');
INSERT INTO `phpbb_posts_text` VALUES (13, '4396f6af7a', 'First impressions', 'My first thought was that its very very similar to bomberman. Maybe a little too similar. The character stands out as being modelled directly from the original bomberman games, im not sure if this is such a good idea if you plan on selling it because of copyright issues.\r\nThis brings me to another question, are you guys thinking of selling this over real-arcade and the other casual game portals? If so, it might be a good idea to include a single click option for mouse controls. Something like left click to move to that position and right click to drop bombs. \r\nIt''s alot of fun the game itself, the classic level takes a bit of time to smash through those boxes :) Powerups are cool, looking forward to playing a few mp games!');
INSERT INTO `phpbb_posts_text` VALUES (14, 'aefc2db7f7', '', 'Just saw a request for beta testers on CGTalk, sounds interesting\r\n\r\nComp:\r\nIntel Pentium D 805\r\n1GB RAM\r\nATI Radeon 9550\r\nWindows XP SP2');
INSERT INTO `phpbb_posts_text` VALUES (15, 'b6ad4e3188', '', 'yeah we will probably change the player character eventually, still not sure if we are going to distribute through portals yet');
INSERT INTO `phpbb_posts_text` VALUES (16, '00cbe796be', '[BUG][CODE] AI goes idle [FIXED]', 'AI goes idle after a certain amount of time. They simply stop moving unless you drop a bomb near them then they wake up to move out of the way but again stop once clear.');
INSERT INTO `phpbb_posts_text` VALUES (17, '454f3d70c7', '', '''Dedge'',  do you also have windows of some sort, a Linux version is far off if it even comes at all.\r\nUnless you want to try it running on wine/winex?');
INSERT INTO `phpbb_posts_text` VALUES (18, '0af46b328d', '', 'I sortof noticed this too. They also blow themselves up occasionally, but then again, so did I :\\');
INSERT INTO `phpbb_posts_text` VALUES (19, '1b7e5de064', '[BUG][CODE] Game instantly ends on Vista', 'The rounds instantly end on Vista.\r\nSeems the time code is playing up');
INSERT INTO `phpbb_posts_text` VALUES (20, 'cba229da36', 'Lord of Teh BETA', 'I have;\r\n\r\nP4 3.0ghz\r\n1.5gb ram\r\nRadeon 9800Pro\r\nWindows XP SP2\r\nAudigy (incase joo care)\r\n\r\nP4 2.8ghz\r\n1gb ram\r\nNvidia 660GT\r\nWindows XP SP2\r\n\r\nAMDx2 3800+\r\n2gb ram\r\nNvidia 7900GT\r\nAudigy\r\n\r\nAll your betas are belong to me    :shock:');
INSERT INTO `phpbb_posts_text` VALUES (21, '4e3a253276', 'F.A.Q.', '[b:4e3a253276]Q. How much does UV Tools cost?[/b:4e3a253276]\r\nA. UV Tools costs $30US.\r\n\r\n[b:4e3a253276]Q. Will future updates be free?[/b:4e3a253276]\r\nA. Updates will be free. For example going from version 1.0 to 1.x will be free. How ever a new version will not be free, for example going from version 1.x to 2.x  you to purchase a new license.\r\n\r\n[b:4e3a253276]Q. Is there a version 2 planned?[/b:4e3a253276]\r\nA. There are plans for UV Tools 2.0 eventually if time allows. But don''t expect anything.\r\n\r\n[b:4e3a253276]Q. Is UV Tools Network license currently a floating license?[/b:4e3a253276]\r\nA. No, due to limitations with maxscript we have decided not to do this with the current version of UV Tools. A network license is simply a license file that gets loaded off of the network instead of being loaded locally.');
INSERT INTO `phpbb_posts_text` VALUES (22, '92643607e7', '', 'this seems to work fine on 32bit version of vista, probably a 64bit issue');
INSERT INTO `phpbb_posts_text` VALUES (23, 'd629b4c9de', '[BUG][Code or art]only time in window when playing sp[fixed]', 'the game continues as normal, but nothing appears on the screen except the time\r\n(on my desktop)\r\n\r\n[code:1:d629b4c9de]Network Init		&#91;OK&#93;\r\nWindow Init		&#91;OK&#93;\r\n----------------------------------------\r\n	Input\r\n----------------------------------------\r\nKeyboard			&#91;OK&#93;\r\nMouse				&#91;OK&#93;\r\n----------------------------------------\r\nInput Init		&#91;OK&#93;\r\n----------------------------------------\r\n	OpenGL Extensions\r\n----------------------------------------\r\nMultitexture			&#91;OK&#93;\r\nDraw Range Element		&#91;OK&#93;\r\nVertex Buffer Objects		&#91;OK&#93;\r\nFrame Buffer Object		&#91;FAIL&#93;\r\nMultisample			&#91;OK&#93;\r\n----------------------------------------\r\n\r\nDevice Init		&#91;OK&#93;\r\nOpenAL Initilised using&#58; Generic Hardware\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	&#91;OK&#93;\r\nnew SoundSource	OpenAL Error&#58; Invalid Value\r\n&#91;FAIL&#93;\r\nSound Init		&#91;OK&#93;\r\nLoading texture&#58; data/arial.tga\r\nLoading texture&#58; data/five_cents.tga\r\nLoading texture&#58; data/mouse_pointer.tga\r\nLoading texture&#58; data/character.tga\r\nLoading texture&#58; data/five_cents_mouseover.tga\r\nLoading texture&#58; data/button_right.tga\r\nLoading texture&#58; data/button_generic.tga\r\nLoading texture&#58; data/chess.tga\r\nLoading effect&#58; data/skin_colour.fx\r\nLoading texture&#58; data/character_color.tga\r\nLoading effect&#58; data/skin.fx\r\nLoading texture&#58; data/character_skin.tga\r\nLoading effect&#58; data/colour_texture_additive_nodepth.fx\r\nLoading texture&#58; data/teammarker.tga\r\nLoading effect&#58; data/plain_texture_additive.fx\r\nLoading texture&#58; data/blob_shadow.tga\r\nLoading sound&#58; data/pickup.wav\r\nUnknown openal error&#58; Invalid Name\r\nLoading sound&#58; data/drop.wav\r\nLoading sound&#58; data/kick.wav\r\nLoading sound&#58; data/Voice_InsaneLaugh.wav\r\nLoading sound&#58; data/Voice_EvilLaugh.wav\r\nLoading effect&#58; data/plain_texture.fx\r\nLoading texture&#58; data/bomb.tga\r\nLoading sound&#58; data/explode01.wav\r\nLoading sound&#58; data/explode02.wav\r\nLoading sound&#58; data/explode03.wav\r\nLoading effect&#58; data/colour_texture_additive.fx\r\nLoading texture&#58; data/explosion_ring.tga\r\nLoading texture&#58; data/explosion_ring_shadow.tga\r\nLoading texture&#58; data/explosion.tga\r\nLoading texture&#58; data/explosion_shadow.tga\r\nLoading effect&#58; data/colour_texture_additive_scrolling.fx\r\nLoading texture&#58; data/fx_death.tga\r\nGame Init		&#91;OK&#93;\r\nLoading effect&#58; data/plain_texture_cube.fx\r\nLoading texture&#58; data/whiteMarble.tga\r\nLoading cube texture&#58; data/chessCube2.tga#data/chessCube2.tga#data/chessCube.tga#data/chessCube2.tga#data/chessCube2.tga#data/chessCube2.tga\r\nLoading texture&#58; data/blackMarble.tga\r\nLoading cube texture&#58; data/chessCube.tga#data/chessCube2.tga#data/chessCube.tga#data/chessCube2.tga#data/chessCube2.tga#data/chessCube2.tga\r\nLoading texture&#58; data/polishedWood.tga\r\nLoading texture&#58; data/MarbleChecker.tga\r\nLoading texture&#58; data/chessClock.tga\r\nLoading texture&#58; data/woodenBevel.tga\r\nLoading texture&#58; data/dynamic_woodbox.tga\r\nsending explode message to all tile&#40;10, 16&#41; killing 0 bombs\r\nERROR&#58; Failed to load material data/pickup_wire.mat\r\nLoading texture&#58; data/pickup_disease.tga\r\nsending explode message to all tile&#40;15, 3&#41; killing 0 bombs\r\nsending explode message to all tile&#40;4, 4&#41; killing 0 bombs\r\nLoading texture&#58; data/pickup_shield.tga\r\nLoading texture&#58; data/fx_shield.tga\r\nsending explode message to all tile&#40;11, 17&#41; killing 0 bombs\r\nsending explode message to all tile&#40;5, 5&#41; killing 0 bombs\r\nsending explode message to all tile&#40;6, 6&#41; killing 0 bombs\r\nLoading texture&#58; data/pickup_random.tga\r\nsending explode message to all tile&#40;11, 16&#41; killing 0 bombs\r\nLoading texture&#58; data/pickup_bomb.tga\r\nsending explode message to all tile&#40;7, 8&#41; killing 0 bombs\r\nsending explode message to all tile&#40;11, 15&#41; killing 0 bombs\r\nsending explode message to all tile&#40;10, 7&#41; killing 0 bombs\r\nLoading texture&#58; data/pickup_reverse.tga\r\nsending explode message to all tile&#40;10, 5&#41; killing 0 bombs\r\nsending explode message to all tile&#40;12, 7&#41; killing 0 bombs\r\nsending explode message to all tile&#40;11, 8&#41; killing 0 bombs\r\nLoading texture&#58; data/pickup_speed.tga\r\nsending explode message to all tile&#40;7, 9&#41; killing 0 bombs\r\nLoading texture&#58; data/pickup_snail.tga\r\nsending explode message to all tile&#40;8, 9&#41; killing 0 bombs\r\nsending explode message to all tile&#40;9, 10&#41; killing 0 bombs\r\nsending explode message to all tile&#40;12, 4&#41; killing 0 bombs\r\nLoading texture&#58; data/pickup_fire.tga\r\nsending explode message to all tile&#40;17, 6&#41; killing 0 bombs\r\n[/code:1:d629b4c9de]');
INSERT INTO `phpbb_posts_text` VALUES (24, '74986fa222', '', 'do you get this all the time?\r\nso you can see the menu all right, then you load up a single player game and you get a blank screen except for the time up the top?\r\nwhat about if you host a multiplayer game?\r\nalso are you running windowed or fullscreen (give both a go and see the results)?\r\n\r\ni suspect the game wont work on your laptop as it requires shader model 1.1 or above, but im pretty sure your desktop pc (the radeon card) has at least 1.1.\r\nwould be nice to know how the game ''fails'' on your laptop if you get a chance.\r\n\r\nalso try the latest video drivers?');
INSERT INTO `phpbb_posts_text` VALUES (25, '57030cf81b', '', 'Fixed 10/10/06');
INSERT INTO `phpbb_posts_text` VALUES (26, '32bb2f1275', '', 'it happens whenever I start a game\r\nthe menu works fine\r\nmultiplayer doesnt work either, same thing happens\r\ni''ve tried windowed and full screen, with AA at 2 and off\r\n\r\non my laptop, it doesnt get past window init');
INSERT INTO `phpbb_posts_text` VALUES (27, '47369240f2', '', 'sounds like the problem is with shaders, do you know if your video card supports shaders of any sort? do you have the latest drivers?');
INSERT INTO `phpbb_posts_text` VALUES (28, 'a371abd3f6', '', 'I guess I didnt have latest drivers, downloaded them and it works fine\r\n\r\nif its not fixable, you should probably include this problem in a FAQ');
INSERT INTO `phpbb_posts_text` VALUES (29, 'b6688f93b5', 'F.A.Q', '[b:b6688f93b5]Q. When I load in to the game i get a blank screen, all I see is the timer up the top of the screen[/b:b6688f93b5]\r\nA. Check you have the latest drivers\r\n\r\n\r\n[b:b6688f93b5]Q. Can I make my own levels?[/b:b6688f93b5]\r\nA. Yes, In the data/levels directory are the levels, these can be edited in any text editor. To make your own models requires the explosive SDK, and requires a copy of 3ds Max\r\n\r\n[b:b6688f93b5]Q. Can I make mods?[/b:b6688f93b5]\r\nA. You can make new art content to replace the old content using the explosive SDK. You can not make new code how ever\r\n\r\n[b:b6688f93b5]Q. Where do I get the explosive SDK?[/b:b6688f93b5]\r\nA. We will let you know when its ready');
INSERT INTO `phpbb_posts_text` VALUES (30, '8b336a938a', '', 'FIXED - 12/10/06\r\n\r\nYeah thats unfixable, I''ve now started an FAQ in the general section\r\n\r\nit would be nice to get the log that occurs on your laptop so i can try to warn the user the game doesnt work with out it just crashing');
INSERT INTO `phpbb_posts_text` VALUES (31, 'b967169085', 'Testing session', 'When a good time for you guys to do some testing?\r\nSometime next week?');
INSERT INTO `phpbb_posts_text` VALUES (32, 'cf1c483686', '', 'I can test.\r\n\r\n2.8GHZ\r\n1GB Ram\r\nWindows XP SP2\r\n.Net Framewoek\r\n\r\neddie9139@gmail.com');
INSERT INTO `phpbb_posts_text` VALUES (33, 'cb8ba9d193', '', 'If you sign up on the forum, I can join you up edd\r\nBTW what video card do you have?');
INSERT INTO `phpbb_posts_text` VALUES (34, '71313159f2', '', 'Ok I registered.');
INSERT INTO `phpbb_posts_text` VALUES (35, '1c4fe615ac', '', 'I''ve uploaded a new version. Please download again from the URL above.\r\n\r\nChanges:\r\n-AI improvements\r\n-Fixes for dual cores (not yet tested)\r\n-UI changes\r\n-New character model, plus associated art and shaders\r\n-Beta now can have up to 8 players (the demo will only have 4 players), so we can better test net code');
INSERT INTO `phpbb_posts_text` VALUES (36, 'a84c37deab', 'F.A.Q', '[b:a84c37deab]Q. What is this game?[/b:a84c37deab]\r\nA. Our next project... cant say more than that at the moment\r\n\r\n[b:a84c37deab]Q. What stage is this project in?[/b:a84c37deab]\r\nA. We are still prototyping. This project wont be out for ages\r\n\r\n[b:a84c37deab]Q. Can I make mods?[/b:a84c37deab]\r\nA. Yep');
INSERT INTO `phpbb_posts_text` VALUES (38, '008e3ae7df', 'Explosive Demo', 'Merry Xmas all,\r\n\r\nWe have been working hard to bring you this demo. \r\nThis also serves as our public beta, if it performs well we shall release the full version.\r\n\r\nDownload:\r\n[url]http://members.iinet.com.au/~gibbz/ExplosiveDemo.exe[/url]');
INSERT INTO `phpbb_posts_text` VALUES (37, '7a22a20422', '', 'Okay i''ve uploaded a new exe in the Patch rar listed above. Please also make sure you get this. \r\n\r\nFixes:\r\n-multiplayer connection issues\r\n-exiting using the ''X'' now works correctly\r\n\r\nKnown issues:\r\n-effects dont update in this build. Dont worry about this its all under control');

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_privmsgs`
-- 

CREATE TABLE `phpbb_privmsgs` (
  `privmsgs_id` mediumint(8) unsigned NOT NULL auto_increment,
  `privmsgs_type` tinyint(4) NOT NULL default '0',
  `privmsgs_subject` varchar(255) NOT NULL default '0',
  `privmsgs_from_userid` mediumint(8) NOT NULL default '0',
  `privmsgs_to_userid` mediumint(8) NOT NULL default '0',
  `privmsgs_date` int(11) NOT NULL default '0',
  `privmsgs_ip` varchar(8) NOT NULL default '',
  `privmsgs_enable_bbcode` tinyint(1) NOT NULL default '1',
  `privmsgs_enable_html` tinyint(1) NOT NULL default '0',
  `privmsgs_enable_smilies` tinyint(1) NOT NULL default '1',
  `privmsgs_attach_sig` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`privmsgs_id`),
  KEY `privmsgs_from_userid` (`privmsgs_from_userid`),
  KEY `privmsgs_to_userid` (`privmsgs_to_userid`)
) TYPE=MyISAM AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `phpbb_privmsgs`
-- 

INSERT INTO `phpbb_privmsgs` VALUES (1, 0, 'noobie!', 3, 4, 1160392234, 'cbce2dfe', 1, 0, 1, 0);
INSERT INTO `phpbb_privmsgs` VALUES (2, 2, 'noobie!', 3, 4, 1160392234, 'cbce2dfe', 1, 0, 1, 0);
INSERT INTO `phpbb_privmsgs` VALUES (3, 0, 'msn', 5, 2, 1160396457, '3ba76771', 1, 0, 1, 0);
INSERT INTO `phpbb_privmsgs` VALUES (4, 2, 'msn', 5, 2, 1160396457, '3ba76771', 1, 0, 1, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_privmsgs_text`
-- 

CREATE TABLE `phpbb_privmsgs_text` (
  `privmsgs_text_id` mediumint(8) unsigned NOT NULL default '0',
  `privmsgs_bbcode_uid` varchar(10) NOT NULL default '0',
  `privmsgs_text` text,
  PRIMARY KEY  (`privmsgs_text_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_privmsgs_text`
-- 

INSERT INTO `phpbb_privmsgs_text` VALUES (1, 'c487bc300e', 'haha ya noob!');
INSERT INTO `phpbb_privmsgs_text` VALUES (2, 'c487bc300e', 'haha ya noob!');
INSERT INTO `phpbb_privmsgs_text` VALUES (3, 'b6cb146d37', 'hey mate,\r\n\r\ndo you use msn at all?\r\nmy msn is brogers2@bigpond.net.au if you wanna add me up to organise some games. im just getting the client now');
INSERT INTO `phpbb_privmsgs_text` VALUES (4, 'b6cb146d37', 'hey mate,\r\n\r\ndo you use msn at all?\r\nmy msn is brogers2@bigpond.net.au if you wanna add me up to organise some games. im just getting the client now');

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_ranks`
-- 

CREATE TABLE `phpbb_ranks` (
  `rank_id` smallint(5) unsigned NOT NULL auto_increment,
  `rank_title` varchar(50) NOT NULL default '',
  `rank_min` mediumint(8) NOT NULL default '0',
  `rank_special` tinyint(1) default '0',
  `rank_image` varchar(255) default NULL,
  PRIMARY KEY  (`rank_id`)
) TYPE=MyISAM AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `phpbb_ranks`
-- 

INSERT INTO `phpbb_ranks` VALUES (1, 'Site Admin', -1, 1, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_search_results`
-- 

CREATE TABLE `phpbb_search_results` (
  `search_id` int(11) unsigned NOT NULL default '0',
  `session_id` varchar(32) NOT NULL default '',
  `search_time` int(11) NOT NULL default '0',
  `search_array` text NOT NULL,
  PRIMARY KEY  (`search_id`),
  KEY `session_id` (`session_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_search_results`
-- 

INSERT INTO `phpbb_search_results` VALUES (522267040, '3d79629ec86466718d8e043331ecf3b7', 1166879593, 'a:7:{s:14:"search_results";s:16:"3, 6, 10, 12, 14";s:17:"total_match_count";i:5;s:12:"split_search";N;s:7:"sort_by";i:0;s:8:"sort_dir";s:4:"DESC";s:12:"show_results";s:6:"topics";s:12:"return_chars";i:200;}');

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_search_wordlist`
-- 

CREATE TABLE `phpbb_search_wordlist` (
  `word_text` varchar(50) binary NOT NULL default '',
  `word_id` mediumint(8) unsigned NOT NULL auto_increment,
  `word_common` tinyint(1) unsigned NOT NULL default '0',
  PRIMARY KEY  (`word_text`),
  KEY `word_id` (`word_id`)
) TYPE=MyISAM AUTO_INCREMENT=935 ;

-- 
-- Dumping data for table `phpbb_search_wordlist`
-- 

INSERT INTO `phpbb_search_wordlist` VALUES (0x6c696e6b, 142, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7068706262, 3, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73797374656d, 172, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x646f776e6c6f6164, 92, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7370656373, 171, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7469746c65, 925, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x666f72756d73, 15, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73706563696679, 924, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x626c61636b636172626f6e, 13, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7369676e6174757265, 923, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7375636b, 25, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x776f6f74, 26, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x30, 27, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3230, 28, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x616464, 29, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x616964, 30, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x626f7468, 31, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x63686172616374657273, 32, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x64656d6f, 33, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x64657369676e6564, 34, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x656173696c79, 35, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x656e7669726f6e6d656e7473, 36, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578747261, 37, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6665617475726573, 38, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x66756e6374696f6e616c6c697479, 39, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x67616d65, 40, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x68656c70, 41, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x686f7765766572, 42, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e636c7564696e67, 43, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e666f726d6174696f6e, 44, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6b6579, 45, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6170, 46, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d617070696e67, 47, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6178, 48, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6e6565646564, 49, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6e6574, 50, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f626a65637473, 51, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f726967696e616c6c79, 52, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x707572706f7365, 53, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7265647563696e67, 54, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72656c65617365, 55, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72656c6561736564, 56, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73686f77696e67, 57, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7370656564696e67, 58, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73747564696f, 59, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7468616e6b73, 60, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x746f6f6c, 61, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x746f6f6c73, 62, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x75736564, 63, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7576, 64, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x757677, 65, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x766964656f73, 66, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7669736974, 67, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x77656273697465, 68, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7061636b73, 218, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f7074696f6e616c, 217, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f7065726174696e67, 216, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6e6f7465, 215, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x62657461, 73, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578706c6f73697665, 74, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d756c7469706c61796572, 214, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6b6e6f776e, 213, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e666f, 212, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706f7374, 78, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x676d74, 211, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x657874726173, 210, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6265636f6d65, 209, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74657374, 82, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x617573736965, 208, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x61737065637473, 207, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3330, 206, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x66726565, 141, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x666f6c6c6f77, 140, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6665656c, 139, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x666565646261636b, 138, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x62656c6f77, 137, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x617274, 343, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7265706f7274, 922, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x657865, 822, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72657065617461626c65, 921, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x726570656174, 920, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578706c6f6465, 349, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72656c617465, 919, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x726172656c79, 918, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6f67, 352, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72617265, 917, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706f737473, 916, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7265706f7274696e67, 129, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6d7075746572, 149, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636865636b, 148, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706f73746564, 915, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x627567, 146, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706f737369626c65, 914, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74697073, 135, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f6e65, 913, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x747269616c, 143, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x75736566756c, 926, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x61626c65, 174, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x61637475616c6c79, 175, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6261736564, 176, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6272697362616e65, 177, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x63617375616c, 178, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f75706c65, 179, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x64657673, 180, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x646966666572656e74, 181, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x66756c6c74696d65, 182, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x667574757265, 183, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x667969, 184, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x67616d6573686f77, 185, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x686f7065, 186, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x686f7572, 187, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6e6967687473, 188, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f6e6c696e65, 189, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x70726f6261626c79, 190, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7175697465, 191, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7175697a, 192, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x736174, 193, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7368617265, 194, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7370617265, 195, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73756e, 196, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7465616d73, 197, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74657374696e67, 198, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74756573646179, 199, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74797065, 200, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x757375616c6c79, 201, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x776f726b, 202, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x776f726b696e67, 203, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x666f72756d, 229, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6f6c, 228, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72656c6176616e74, 219, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7265706c79, 220, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7363686564756c65, 221, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73657276696365, 222, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7374616765, 223, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x746573746572, 224, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74657374657273, 225, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x746872656164, 226, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6f6361746564, 227, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x736f756e6473, 230, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x316762, 231, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x36, 232, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x32353030, 233, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x363830306774, 234, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x61726368, 235, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x617870, 236, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6261636b, 237, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6265746174657374, 238, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636865657273, 239, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6465646765, 240, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x646576656c6f70696e67, 241, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x66616e6379, 242, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x666577, 243, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6669727374, 244, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x67616d696e67, 245, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x68616e64, 246, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x69, 247, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c696e7578, 248, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6f6e6b6579, 249, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6f6e746873, 250, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6e7669646961, 251, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f636175, 252, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706f7274, 253, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x70726f6d6f746564, 254, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x70757474696e67, 255, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x71756f74, 256, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72616d, 257, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x726563616c6c, 258, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72656d656d626572, 259, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74617465, 260, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x616e6f74686572, 261, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x626974, 262, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x626f6d6265726d616e, 263, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x626f6d6273, 264, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x626f786573, 265, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6272696e6773, 266, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636861726163746572, 267, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636c6173736963, 268, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636c69636b, 269, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6e74726f6c73, 270, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f70797269676874, 271, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6469726563746c79, 272, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x64726f70, 273, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x666f7277617264, 274, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x66756e, 275, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x67616d6573, 276, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x67757973, 277, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x69646561, 278, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696d7072657373696f6e73, 279, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e636c756465, 280, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x697373756573, 281, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x697473656c66, 282, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c656674, 283, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6576656c, 284, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d69676874, 285, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6f64656c6c6564, 286, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6f757365, 287, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6f7665, 288, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f7074696f6e, 289, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f726967696e616c, 290, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706c616e, 291, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706c6179696e67, 292, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706f7274616c73, 293, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706f736974696f6e, 294, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706f776572757073, 295, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7265616c617263616465, 296, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7269676874, 297, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73656c6c696e67, 298, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73696d696c6172, 299, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73696e676c65, 300, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x736d617368, 301, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7374616e6473, 302, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73756368, 303, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73757265, 304, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74616b6573, 305, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7468696e6b696e67, 306, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74686f75676874, 307, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x76657279, 308, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x383035, 309, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x39353530, 310, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x617469, 311, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636774616c6b, 312, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6d70, 313, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e74656c, 314, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e746572657374696e67, 315, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x70656e7469756d, 316, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x726164656f6e, 317, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72657175657374, 318, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x737032, 319, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x77696e646f7773, 320, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6368616e6765, 321, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x64697374726962757465, 322, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6576656e7475616c6c79, 323, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706c61796572, 324, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7374696c6c, 325, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x79656168, 326, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x616761696e, 327, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73746f70, 431, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73696d706c79, 430, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6f76696e67, 429, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6669786564, 428, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6465, 332, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x676f6573, 333, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x69646c65, 334, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636c656172, 427, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6365727461696e, 426, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x626f6d62, 425, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x756e6c657373, 338, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x616d6f756e74, 424, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x726172, 832, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x70726f626c656d, 359, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6179, 912, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x676962627a, 911, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6469726563746f7279, 747, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578706c6f7369766564656d6f, 910, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6e74656e7473, 909, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73616d65, 367, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x63617465676f7279, 908, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x736f756e64, 369, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x62756773, 907, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7374617274, 371, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72756e6e696e67, 419, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6d6573, 418, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x626c6f77, 376, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6e6f7469636564, 377, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f63636173696f6e616c6c79, 378, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x736f72746f66, 379, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7468656d73656c766573, 380, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x656e64, 381, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x656e6473, 382, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e7374616e746c79, 383, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x726f756e6473, 384, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7365656d73, 385, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7669737461, 386, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3067687a, 387, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x32, 388, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x326762, 389, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x33, 390, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x33383030, 391, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x356762, 392, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3636306774, 393, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x373930306774, 394, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3867687a, 395, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3938303070726f, 396, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x616d647832, 397, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x617564696779, 398, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x62656c6f6e67, 399, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6265746173, 400, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x63617265, 401, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e63617365, 402, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6a6f6f, 403, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6f7264, 404, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73686f636b, 405, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x746568, 406, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x31, 407, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x78, 903, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x75706461746573, 902, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7075726368617365, 901, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706c616e73, 900, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706c616e6e6564, 899, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6178736372697074, 898, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6f63616c6c79, 897, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6f61646564, 896, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c696d69746174696f6e73, 895, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6963656e6365, 894, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x736f7274, 420, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x747279, 421, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x77696e65, 422, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x77696e6578, 423, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x77616b65, 432, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3332626974, 433, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3634626974, 434, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x66696e65, 435, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6973737565, 436, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7465616d6d61726b6572, 702, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x736f756e64736f75726365, 701, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x736b696e636f6c6f7572, 700, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x736b696e, 699, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73656e64696e67, 698, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3130, 442, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72616e6765, 697, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706f6c6973686564776f6f64, 696, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706c61696e7465787475726563756265, 695, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706c61696e746578747572656164646974697665, 694, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706c61696e74657874757265, 693, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7069636b757077697265, 692, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7069636b75707370656564, 691, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7069636b7570736e61696c, 690, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7069636b7570736869656c64, 689, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7069636b757072657665727365, 688, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7069636b757072616e646f6d, 687, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7069636b757066697265, 686, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7069636b757064697365617365, 685, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7069636b7570626f6d62, 684, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7069636b7570, 683, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6465736b746f70, 463, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f70656e676c, 682, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f70656e616c, 681, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f6b, 680, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f626a656374, 679, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6e6f726d616c, 678, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6e6574776f726b, 677, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x657863657074, 470, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6e616d65, 676, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d756c746974657874757265, 675, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d756c746973616d706c65, 674, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6f757365706f696e746572, 673, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d657373616765, 672, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6174657269616c, 671, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6174, 670, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6172626c65636865636b6572, 669, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6f6164696e67, 668, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6b696c6c696e67, 667, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6b69636b, 666, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6b6579626f617264, 665, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e76616c6964, 664, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e707574, 663, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e6974696c69736564, 662, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6861726477617265, 661, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x67656e65726963, 660, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e6974, 489, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6678736869656c64, 659, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x66786465617468, 658, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6678, 657, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6672616d65, 656, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6669766563656e74736d6f7573656f766572, 655, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6f6164, 496, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6669766563656e7473, 654, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6661696c6564, 653, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6661696c, 652, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x657874656e73696f6e73, 651, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578706c6f73696f6e736861646f77, 650, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578706c6f73696f6e72696e67736861646f77, 649, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578706c6f73696f6e72696e67, 648, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578706c6f73696f6e, 647, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578706c6f64653033, 646, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578706c6f64653032, 645, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578706c6f64653031, 644, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6572726f72, 643, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x656c656d656e74, 642, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x656666656374, 641, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x64796e616d6963776f6f64626f78, 640, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x64726177, 639, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x646576696365, 638, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x64617461, 637, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x63756265, 636, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6e74696e756573, 635, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x63686573736375626532, 634, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636865737363756265, 633, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6368657373636c6f636b, 632, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6368657373, 631, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636861726163746572736b696e, 630, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636861726163746572636f6c6f72, 629, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73637265656e, 527, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x627574746f6e7269676874, 628, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x627574746f6e67656e65726963, 627, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x627566666572, 626, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x626c6f62736861646f77, 625, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x626c61636b6d6172626c65, 624, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x617269616c, 623, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x61707065617273, 622, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3137, 621, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3135, 620, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3132, 619, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x3131, 618, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x39, 617, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x38, 616, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x37, 615, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x35, 614, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x77696e646f77, 544, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x34, 613, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d656e75, 586, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6f64656c, 587, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6e696365, 588, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x707265747479, 589, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7265717569726573, 590, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c65617374, 585, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6174657374, 584, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6170746f70, 583, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x686f7374, 582, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x67697665, 581, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x66756c6c73637265656e, 580, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6661696c73, 579, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x64726976657273, 578, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6368616e6365, 577, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x63617264, 576, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x626c616e6b, 575, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x61626f7665, 574, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x726573756c7473, 591, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x736861646572, 592, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73757370656374, 593, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x746f70, 594, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x766964656f, 595, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x77696e646f776564, 596, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x66756c6c, 597, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x68617070656e73, 598, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x697665, 599, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x70617374, 600, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7468696e67, 601, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7472696564, 602, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7768656e65766572, 603, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x776f726b73, 604, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x737570706f727473, 608, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73686164657273, 607, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x646f776e6c6f61646564, 609, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x666171, 610, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x66697861626c65, 611, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6775657373, 612, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74657874757265, 703, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x746761, 704, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74696c65, 705, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x756e6b6e6f776e, 706, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7573696e67, 707, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x76616c7565, 708, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x766572746578, 709, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x766f6963656576696c6c61756768, 710, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x766f696365696e73616e656c61756768, 711, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x776176, 712, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x77686974656d6172626c65, 713, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x776f6f64656e626576656c, 714, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74696d6572, 810, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73656374696f6e, 725, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f6363757273, 724, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x67656e6572616c, 723, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6372617368696e67, 722, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73746172746564, 726, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x756e66697861626c65, 727, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x75736572, 728, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7761726e, 729, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6e657874, 730, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73657373696f6e, 731, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7765656b, 732, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6d, 733, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x656464696539313339, 734, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6672616d65776f656b, 735, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x676d61696c, 736, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x656464, 737, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6a6f696e, 738, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7369676e, 739, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x72656769737465726564, 740, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6272696e67, 927, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6173736f636961746564, 768, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6368616e676573, 769, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f726573, 770, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6475616c, 771, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6669786573, 772, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696d70726f76656d656e7473, 773, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706c6179657273, 774, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706c7573, 775, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x746573746564, 776, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x75706c6f61646564, 777, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x75726c, 778, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x74657874, 809, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x73646b, 808, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7265706c616365, 807, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7265616479, 806, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f776e, 805, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6f6473, 804, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6f64656c73, 803, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d616b65, 802, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6576656c73, 801, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6574, 800, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x656469746f72, 799, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x656469746564, 798, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f7079, 797, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6e74656e74, 796, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x336473, 795, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x61676573, 811, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d6f6d656e74, 812, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x70726f6a656374, 813, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x70726f746f747970696e67, 814, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x796570, 815, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x617070726f7072696174656c79, 906, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x616d70, 905, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x616c7265616479, 904, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6275696c64, 843, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6e6e656374696f6e, 844, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f6e74726f6c, 845, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f72726563746c79, 846, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x65666665637473, 847, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x65786974696e67, 848, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6c6973746564, 849, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6f6b6179, 850, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7061746368, 851, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x757064617465, 852, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x776f727279, 853, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x696e7374656164, 893, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x67657473, 892, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x666c6f6174696e67, 891, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x66696c65, 890, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x657870656374, 889, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6578616d706c65, 888, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x647565, 887, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x64656369646564, 886, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x63757272656e746c79, 885, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x63757272656e74, 884, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f737473, 883, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x636f7374, 882, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x616e797468696e67, 881, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x616c6c6f7773, 880, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x33307573, 879, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x68617264, 928, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x6d65727279, 929, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x706572666f726d73, 930, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7075626c6963, 931, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x736572766573, 932, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x7368616c6c, 933, 0);
INSERT INTO `phpbb_search_wordlist` VALUES (0x786d6173, 934, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_search_wordmatch`
-- 

CREATE TABLE `phpbb_search_wordmatch` (
  `post_id` mediumint(8) unsigned NOT NULL default '0',
  `word_id` mediumint(8) unsigned NOT NULL default '0',
  `title_match` tinyint(1) NOT NULL default '0',
  KEY `post_id` (`post_id`),
  KEY `word_id` (`word_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_search_wordmatch`
-- 

INSERT INTO `phpbb_search_wordmatch` VALUES (38, 92, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 62, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 44, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 143, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 137, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 138, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 139, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 140, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 141, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 78, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 62, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 33, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 927, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 73, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 135, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 129, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 92, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 13, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 27, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 28, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 29, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 30, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 31, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 32, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 33, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 34, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 35, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 36, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 37, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 38, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 39, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 40, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 41, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 42, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 43, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 44, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 45, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 46, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 47, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 48, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 49, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 50, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 51, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 52, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 53, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 54, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 56, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 57, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 58, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 59, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 60, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 61, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 62, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 63, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 64, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 65, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 66, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 67, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 68, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 27, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 55, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (4, 62, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 207, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 208, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 82, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 209, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 210, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 211, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 78, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 212, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 213, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 214, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 74, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 73, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 215, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 216, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 217, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 218, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 50, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 40, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 171, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 172, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 44, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 43, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 92, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (7, 142, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 73, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 905, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 926, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 925, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 135, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 172, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 371, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 171, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 924, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 369, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 923, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 367, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 129, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 922, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 921, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 920, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 919, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 918, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 917, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 359, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 916, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 915, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 78, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 914, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 913, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 912, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 352, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 213, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 911, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 910, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 40, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 41, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 42, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 55, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 82, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 174, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 175, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 176, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 177, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 178, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 179, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 180, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 181, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 182, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 183, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 184, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 185, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 186, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 187, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 188, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 189, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 190, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 191, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 192, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 193, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 194, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 195, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 196, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 197, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 198, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 199, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 200, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 201, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 202, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (9, 203, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (10, 148, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (10, 73, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 206, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 149, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 219, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 220, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 221, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 222, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 223, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 224, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 226, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 73, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 74, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (5, 225, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (11, 177, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (11, 227, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (10, 228, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (10, 74, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (10, 229, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (10, 230, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (10, 198, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 172, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 171, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 15, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 40, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 41, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 82, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 177, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 227, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 231, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 232, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 233, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 234, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 235, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 236, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 237, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 238, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 239, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 240, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 241, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 242, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 243, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 244, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 245, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 246, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 247, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 248, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 249, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 250, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 251, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 252, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 253, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 254, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 255, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 256, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 257, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 258, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 259, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (12, 260, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 40, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 178, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 228, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 243, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 244, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 261, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 262, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 263, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 264, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 265, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 266, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 267, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 268, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 269, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 270, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 271, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 272, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 273, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 274, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 275, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 276, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 277, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 278, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 280, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 281, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 282, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 283, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 284, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 285, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 286, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 287, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 288, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 289, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 290, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 291, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 292, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 293, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 294, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 295, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 296, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 297, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 298, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 299, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 300, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 301, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 302, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 303, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 304, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 305, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 306, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 307, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 308, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 244, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (13, 279, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 231, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 309, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 310, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 311, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 73, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 312, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 313, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 314, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 315, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 316, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 317, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 257, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 318, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 230, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 319, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 225, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (14, 320, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (15, 321, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (15, 267, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (15, 322, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (15, 323, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (15, 324, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (15, 293, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (15, 190, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (15, 325, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (15, 304, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (15, 326, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 333, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 428, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 332, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 146, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 432, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 338, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 431, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 430, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 429, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 288, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 334, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 333, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 273, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 427, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 426, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 425, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 424, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 327, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 822, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 747, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 909, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 149, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 332, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 148, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 908, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 907, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 146, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 343, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 906, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (17, 420, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (17, 419, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (17, 248, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (17, 240, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (17, 418, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (18, 327, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (18, 376, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (18, 377, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (18, 378, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (18, 379, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (18, 380, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 332, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 381, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 383, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 292, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 384, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 385, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 386, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 146, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 332, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 382, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 40, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 383, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (19, 386, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 387, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 231, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 388, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 389, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 390, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 391, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 392, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 393, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 394, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 395, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 396, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 397, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 398, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 399, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 400, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 401, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 402, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 403, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 251, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 317, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 257, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 405, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 319, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 320, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 73, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 404, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (20, 406, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 903, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 64, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 902, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 62, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 430, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 901, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 900, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 899, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 677, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 898, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 897, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 896, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 895, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 894, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 893, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 892, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (17, 421, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (17, 338, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (17, 320, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (17, 422, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (17, 423, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (16, 334, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (22, 433, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (22, 434, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (22, 435, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (22, 436, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (22, 190, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (22, 385, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (22, 386, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (22, 202, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 292, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 428, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 332, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 146, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 343, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 714, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 713, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 712, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 711, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 710, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 709, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 708, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 707, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 706, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 705, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 704, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 703, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 613, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 544, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 614, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 615, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 616, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 617, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 618, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 619, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 620, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 621, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 622, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 623, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 624, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 625, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 626, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 627, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 628, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 527, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 629, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 630, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 631, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 632, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 633, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 634, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 635, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 636, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 637, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 638, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 639, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 640, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 641, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 642, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 643, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 644, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 645, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 646, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 647, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 648, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 649, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 650, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 651, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 652, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 653, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 654, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 496, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 655, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 656, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 657, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 658, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 659, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 489, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 660, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 661, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 662, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 663, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 664, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 665, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 666, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 667, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 668, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 669, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 670, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 671, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 672, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 673, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 674, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 675, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 676, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 470, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 677, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 678, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 679, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 680, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 681, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 682, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 463, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 683, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 684, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 685, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 686, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 687, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 688, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 689, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 690, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 691, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 692, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 693, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 694, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 695, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 696, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 697, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 442, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 698, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 699, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 700, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 701, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 702, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 369, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 425, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 287, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 273, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 267, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 264, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 232, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 349, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 51, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 40, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 594, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 593, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 304, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 300, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 592, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 527, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 419, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 297, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 591, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 590, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 317, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 589, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 324, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 588, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 214, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 587, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 586, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 496, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 585, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 584, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 583, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 582, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 581, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 40, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 580, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 579, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 470, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 578, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 463, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 577, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 576, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 31, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 575, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 574, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 407, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (25, 442, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (25, 428, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 421, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 595, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 596, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (24, 202, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 435, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 597, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 40, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 598, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 489, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 599, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 583, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 586, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 214, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 600, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 367, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 527, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 371, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 601, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 602, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 603, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 544, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 596, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 202, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (26, 604, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (27, 230, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (27, 420, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (27, 607, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (27, 359, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (27, 584, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (27, 578, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (27, 576, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (27, 608, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (27, 595, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (28, 609, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (28, 578, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (28, 610, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (28, 435, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (28, 611, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (28, 612, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (28, 280, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (28, 584, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (28, 190, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (28, 359, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (28, 604, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (23, 544, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 594, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 810, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 809, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 808, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 527, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 590, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 807, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 806, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 805, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 583, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 599, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 247, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 723, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 40, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 428, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 610, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 722, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 442, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 352, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 588, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 724, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 725, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 726, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 421, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 727, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 728, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 729, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 202, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (30, 326, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (31, 277, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (31, 730, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (31, 198, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (31, 732, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (31, 731, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (31, 198, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (32, 231, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (32, 395, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (32, 733, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (32, 734, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (32, 735, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (32, 736, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (32, 50, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (32, 257, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (32, 319, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (32, 82, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (32, 320, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (33, 576, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (33, 737, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (33, 229, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (33, 738, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (33, 739, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (33, 595, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (34, 247, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (34, 740, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 574, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 327, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 343, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 768, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 73, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 769, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 267, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 332, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 770, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 33, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 92, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 771, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 772, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 773, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 599, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 587, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 50, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 774, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 775, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 607, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 82, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 776, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 777, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (35, 778, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 804, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 803, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 48, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 802, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 496, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 801, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 800, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 584, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 247, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 40, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 74, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 799, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 798, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 578, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 747, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 637, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 797, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 796, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 332, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 148, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 575, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 343, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 795, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (29, 707, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (36, 811, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (36, 40, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (36, 802, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (36, 804, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (36, 812, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (36, 730, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (36, 813, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (36, 814, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (36, 223, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (36, 325, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (36, 815, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (6, 904, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 574, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 843, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 844, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 845, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 846, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 847, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 822, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 848, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 772, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 281, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 599, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 213, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 849, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 802, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 214, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 850, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 851, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 832, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 304, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 852, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 777, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 707, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 604, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (37, 853, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 183, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 141, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 891, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 890, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 889, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 888, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 323, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 887, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 886, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 885, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 884, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 883, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 882, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 881, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 880, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 879, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 388, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 407, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (21, 27, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 822, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 910, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 597, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 911, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 928, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 929, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 930, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 931, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 55, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 932, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 933, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 203, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 934, 0);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 33, 1);
INSERT INTO `phpbb_search_wordmatch` VALUES (38, 74, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_sessions`
-- 

CREATE TABLE `phpbb_sessions` (
  `session_id` char(32) NOT NULL default '',
  `session_user_id` mediumint(8) NOT NULL default '0',
  `session_start` int(11) NOT NULL default '0',
  `session_time` int(11) NOT NULL default '0',
  `session_ip` char(8) NOT NULL default '0',
  `session_page` int(11) NOT NULL default '0',
  `session_logged_in` tinyint(1) NOT NULL default '0',
  `session_admin` tinyint(2) NOT NULL default '0',
  PRIMARY KEY  (`session_id`),
  KEY `session_user_id` (`session_user_id`),
  KEY `session_id_ip_user_id` (`session_id`,`session_ip`,`session_user_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_sessions`
-- 

INSERT INTO `phpbb_sessions` VALUES ('d6651ec6eb471f70ad4236293d090cdd', -1, 1167048121, 1167065986, '42f941f3', 0, 0, 0);
INSERT INTO `phpbb_sessions` VALUES ('f9189a6f126e4a2107dc5473af24288b', 2, 1167106220, 1167106220, 'cbce2cec', 0, 1, 0);
INSERT INTO `phpbb_sessions` VALUES ('91d9b97c2966e174e1813d38d24fda09', -1, 1167090580, 1167090580, '4a0649dd', 0, 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_sessions_keys`
-- 

CREATE TABLE `phpbb_sessions_keys` (
  `key_id` varchar(32) NOT NULL default '0',
  `user_id` mediumint(8) NOT NULL default '0',
  `last_ip` varchar(8) NOT NULL default '0',
  `last_login` int(11) NOT NULL default '0',
  PRIMARY KEY  (`key_id`,`user_id`),
  KEY `last_login` (`last_login`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_sessions_keys`
-- 

INSERT INTO `phpbb_sessions_keys` VALUES ('eb452acf5767492f3c588cac0f0f887c', 2, 'cbce2cec', 1166098731);
INSERT INTO `phpbb_sessions_keys` VALUES ('f70a1d9995b8fb0d3dafa3d2e8820583', 4, 'cb1c9fa9', 1160392226);
INSERT INTO `phpbb_sessions_keys` VALUES ('984900c9218c3953b1b56b34296e5dfa', 9, '9086011f', 1160736116);
INSERT INTO `phpbb_sessions_keys` VALUES ('e164e3e7b2505bc2b37df26c152cbdcd', 5, '3ba7787d', 1164802442);
INSERT INTO `phpbb_sessions_keys` VALUES ('c168dc002ea14d15567120220c2678c3', 7, 'd8f40ff5', 1160840502);
INSERT INTO `phpbb_sessions_keys` VALUES ('9da6f0f09754c2d95312a75200998fbd', 5, 'd2b95a6b', 1160438138);
INSERT INTO `phpbb_sessions_keys` VALUES ('4a89ad7b22d614578acc2845da9dfa4e', 3, 'cb5e8c05', 1160535931);
INSERT INTO `phpbb_sessions_keys` VALUES ('c0093b85cb9ab0d3f05bdb337ec3cd87', 8, 'cb5e8c05', 1160452746);
INSERT INTO `phpbb_sessions_keys` VALUES ('3338f157609271624207325570824fa7', 7, 'd8f40ff5', 1160536734);
INSERT INTO `phpbb_sessions_keys` VALUES ('f3ec4bcae20d5cecc5f36b91879dd698', 3, 'cb5e8c05', 1160535930);
INSERT INTO `phpbb_sessions_keys` VALUES ('bdbaa8bd905fec943b9c0dc9bad0acf1', 2, 'cbce2dfe', 1160556023);
INSERT INTO `phpbb_sessions_keys` VALUES ('e270363c6e46c1116eb77d25666226e6', 3, 'cbce2dfe', 1161296896);
INSERT INTO `phpbb_sessions_keys` VALUES ('2b2a0fe7829f8f4f51ef520d92718f3f', 7, 'd8f40ff5', 1160928445);
INSERT INTO `phpbb_sessions_keys` VALUES ('5fc975b27164b6cf245d82ed9663abec', 7, 'd8f40ff5', 1161409253);
INSERT INTO `phpbb_sessions_keys` VALUES ('4947c0cbf5bbadd1137ac22dc5f54b88', 7, 'd8f40ff5', 1161911613);
INSERT INTO `phpbb_sessions_keys` VALUES ('172d4841353c9ffbca5f48bb4869cb54', 7, 'd8f40ff5', 1162857029);
INSERT INTO `phpbb_sessions_keys` VALUES ('4ae92a8a61388c45d04f9ad5333b33e1', 2, 'cbce2cec', 1167106220);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_smilies`
-- 

CREATE TABLE `phpbb_smilies` (
  `smilies_id` smallint(5) unsigned NOT NULL auto_increment,
  `code` varchar(50) default NULL,
  `smile_url` varchar(100) default NULL,
  `emoticon` varchar(75) default NULL,
  PRIMARY KEY  (`smilies_id`)
) TYPE=MyISAM AUTO_INCREMENT=43 ;

-- 
-- Dumping data for table `phpbb_smilies`
-- 

INSERT INTO `phpbb_smilies` VALUES (1, ':D', 'icon_biggrin.gif', 'Very Happy');
INSERT INTO `phpbb_smilies` VALUES (2, ':-D', 'icon_biggrin.gif', 'Very Happy');
INSERT INTO `phpbb_smilies` VALUES (3, ':grin:', 'icon_biggrin.gif', 'Very Happy');
INSERT INTO `phpbb_smilies` VALUES (4, ':)', 'icon_smile.gif', 'Smile');
INSERT INTO `phpbb_smilies` VALUES (5, ':-)', 'icon_smile.gif', 'Smile');
INSERT INTO `phpbb_smilies` VALUES (6, ':smile:', 'icon_smile.gif', 'Smile');
INSERT INTO `phpbb_smilies` VALUES (7, ':(', 'icon_sad.gif', 'Sad');
INSERT INTO `phpbb_smilies` VALUES (8, ':-(', 'icon_sad.gif', 'Sad');
INSERT INTO `phpbb_smilies` VALUES (9, ':sad:', 'icon_sad.gif', 'Sad');
INSERT INTO `phpbb_smilies` VALUES (10, ':o', 'icon_surprised.gif', 'Surprised');
INSERT INTO `phpbb_smilies` VALUES (11, ':-o', 'icon_surprised.gif', 'Surprised');
INSERT INTO `phpbb_smilies` VALUES (12, ':eek:', 'icon_surprised.gif', 'Surprised');
INSERT INTO `phpbb_smilies` VALUES (13, ':shock:', 'icon_eek.gif', 'Shocked');
INSERT INTO `phpbb_smilies` VALUES (14, ':?', 'icon_confused.gif', 'Confused');
INSERT INTO `phpbb_smilies` VALUES (15, ':-?', 'icon_confused.gif', 'Confused');
INSERT INTO `phpbb_smilies` VALUES (16, ':???:', 'icon_confused.gif', 'Confused');
INSERT INTO `phpbb_smilies` VALUES (17, '8)', 'icon_cool.gif', 'Cool');
INSERT INTO `phpbb_smilies` VALUES (18, '8-)', 'icon_cool.gif', 'Cool');
INSERT INTO `phpbb_smilies` VALUES (19, ':cool:', 'icon_cool.gif', 'Cool');
INSERT INTO `phpbb_smilies` VALUES (20, ':lol:', 'icon_lol.gif', 'Laughing');
INSERT INTO `phpbb_smilies` VALUES (21, ':x', 'icon_mad.gif', 'Mad');
INSERT INTO `phpbb_smilies` VALUES (22, ':-x', 'icon_mad.gif', 'Mad');
INSERT INTO `phpbb_smilies` VALUES (23, ':mad:', 'icon_mad.gif', 'Mad');
INSERT INTO `phpbb_smilies` VALUES (24, ':P', 'icon_razz.gif', 'Razz');
INSERT INTO `phpbb_smilies` VALUES (25, ':-P', 'icon_razz.gif', 'Razz');
INSERT INTO `phpbb_smilies` VALUES (26, ':razz:', 'icon_razz.gif', 'Razz');
INSERT INTO `phpbb_smilies` VALUES (27, ':oops:', 'icon_redface.gif', 'Embarassed');
INSERT INTO `phpbb_smilies` VALUES (28, ':cry:', 'icon_cry.gif', 'Crying or Very sad');
INSERT INTO `phpbb_smilies` VALUES (29, ':evil:', 'icon_evil.gif', 'Evil or Very Mad');
INSERT INTO `phpbb_smilies` VALUES (30, ':twisted:', 'icon_twisted.gif', 'Twisted Evil');
INSERT INTO `phpbb_smilies` VALUES (31, ':roll:', 'icon_rolleyes.gif', 'Rolling Eyes');
INSERT INTO `phpbb_smilies` VALUES (32, ':wink:', 'icon_wink.gif', 'Wink');
INSERT INTO `phpbb_smilies` VALUES (33, ';)', 'icon_wink.gif', 'Wink');
INSERT INTO `phpbb_smilies` VALUES (34, ';-)', 'icon_wink.gif', 'Wink');
INSERT INTO `phpbb_smilies` VALUES (35, ':!:', 'icon_exclaim.gif', 'Exclamation');
INSERT INTO `phpbb_smilies` VALUES (36, ':?:', 'icon_question.gif', 'Question');
INSERT INTO `phpbb_smilies` VALUES (37, ':idea:', 'icon_idea.gif', 'Idea');
INSERT INTO `phpbb_smilies` VALUES (38, ':arrow:', 'icon_arrow.gif', 'Arrow');
INSERT INTO `phpbb_smilies` VALUES (39, ':|', 'icon_neutral.gif', 'Neutral');
INSERT INTO `phpbb_smilies` VALUES (40, ':-|', 'icon_neutral.gif', 'Neutral');
INSERT INTO `phpbb_smilies` VALUES (41, ':neutral:', 'icon_neutral.gif', 'Neutral');
INSERT INTO `phpbb_smilies` VALUES (42, ':mrgreen:', 'icon_mrgreen.gif', 'Mr. Green');

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_themes`
-- 

CREATE TABLE `phpbb_themes` (
  `themes_id` mediumint(8) unsigned NOT NULL auto_increment,
  `template_name` varchar(30) NOT NULL default '',
  `style_name` varchar(30) NOT NULL default '',
  `head_stylesheet` varchar(100) default NULL,
  `body_background` varchar(100) default NULL,
  `body_bgcolor` varchar(6) default NULL,
  `body_text` varchar(6) default NULL,
  `body_link` varchar(6) default NULL,
  `body_vlink` varchar(6) default NULL,
  `body_alink` varchar(6) default NULL,
  `body_hlink` varchar(6) default NULL,
  `tr_color1` varchar(6) default NULL,
  `tr_color2` varchar(6) default NULL,
  `tr_color3` varchar(6) default NULL,
  `tr_class1` varchar(25) default NULL,
  `tr_class2` varchar(25) default NULL,
  `tr_class3` varchar(25) default NULL,
  `th_color1` varchar(6) default NULL,
  `th_color2` varchar(6) default NULL,
  `th_color3` varchar(6) default NULL,
  `th_class1` varchar(25) default NULL,
  `th_class2` varchar(25) default NULL,
  `th_class3` varchar(25) default NULL,
  `td_color1` varchar(6) default NULL,
  `td_color2` varchar(6) default NULL,
  `td_color3` varchar(6) default NULL,
  `td_class1` varchar(25) default NULL,
  `td_class2` varchar(25) default NULL,
  `td_class3` varchar(25) default NULL,
  `fontface1` varchar(50) default NULL,
  `fontface2` varchar(50) default NULL,
  `fontface3` varchar(50) default NULL,
  `fontsize1` tinyint(4) default NULL,
  `fontsize2` tinyint(4) default NULL,
  `fontsize3` tinyint(4) default NULL,
  `fontcolor1` varchar(6) default NULL,
  `fontcolor2` varchar(6) default NULL,
  `fontcolor3` varchar(6) default NULL,
  `span_class1` varchar(25) default NULL,
  `span_class2` varchar(25) default NULL,
  `span_class3` varchar(25) default NULL,
  `img_size_poll` smallint(5) unsigned default NULL,
  `img_size_privmsg` smallint(5) unsigned default NULL,
  PRIMARY KEY  (`themes_id`)
) TYPE=MyISAM AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `phpbb_themes`
-- 

INSERT INTO `phpbb_themes` VALUES (1, 'subSilver', 'subSilver', 'subSilver.css', '', 'E5E5E5', '000000', '006699', '5493B4', '', 'DD6900', 'EFEFEF', 'DEE3E7', 'D1D7DC', '', '', '', '98AAB1', '006699', 'FFFFFF', 'cellpic1.gif', 'cellpic3.gif', 'cellpic2.jpg', 'FAFAFA', 'FFFFFF', '', 'row1', 'row2', '', 'Verdana, Arial, Helvetica, sans-serif', 'Trebuchet MS', 'Courier, ''Courier New'', sans-serif', 10, 11, 12, '444444', '006600', 'FFA34F', '', '', '', NULL, NULL);
INSERT INTO `phpbb_themes` VALUES (2, 'AcidTech', 'AcidTechEx', 'AcidTech.css', '100%', 'E5E5E5', '000000', '006699', '5493B4', '', 'DD6900', 'EFEFEF', 'DEE3E7', 'D1D7DC', '', '', '', '98AAB1', '006699', 'FFFFFF', 'cellpic1.gif', 'cellpic3.gif', 'cellpic2.jpg', 'FAFAFA', 'FFFFFF', '', 'row1', 'row2', '', 'Verdana, Arial, Helvetica, sans-serif', 'Trebuchet MS', 'Courier, ''Courier New'', sans-serif', 10, 11, 12, '444444', '0AF035', 'F0E80A', '', '', '', 0, 0);
INSERT INTO `phpbb_themes` VALUES (3, 'AcidTechTiger', 'AcidTechTigerEx', 'AcidTechTiger.css', '100%', 'E5E5E5', '000000', '006699', '5493B4', '', 'DD6900', 'EFEFEF', 'DEE3E7', 'D1D7DC', '', '', '', '98AAB1', '006699', 'FFFFFF', 'cellpic1.gif', 'cellpic3.gif', 'cellpic2.jpg', 'FAFAFA', 'FFFFFF', '', 'row1', 'row2', '', 'Verdana, Arial, Helvetica, sans-serif', 'Trebuchet MS', 'Courier, ''Courier New'', sans-serif', 10, 11, 12, '444444', '0AF035', 'FFFFFF', '', '', '', 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_themes_name`
-- 

CREATE TABLE `phpbb_themes_name` (
  `themes_id` smallint(5) unsigned NOT NULL default '0',
  `tr_color1_name` char(50) default NULL,
  `tr_color2_name` char(50) default NULL,
  `tr_color3_name` char(50) default NULL,
  `tr_class1_name` char(50) default NULL,
  `tr_class2_name` char(50) default NULL,
  `tr_class3_name` char(50) default NULL,
  `th_color1_name` char(50) default NULL,
  `th_color2_name` char(50) default NULL,
  `th_color3_name` char(50) default NULL,
  `th_class1_name` char(50) default NULL,
  `th_class2_name` char(50) default NULL,
  `th_class3_name` char(50) default NULL,
  `td_color1_name` char(50) default NULL,
  `td_color2_name` char(50) default NULL,
  `td_color3_name` char(50) default NULL,
  `td_class1_name` char(50) default NULL,
  `td_class2_name` char(50) default NULL,
  `td_class3_name` char(50) default NULL,
  `fontface1_name` char(50) default NULL,
  `fontface2_name` char(50) default NULL,
  `fontface3_name` char(50) default NULL,
  `fontsize1_name` char(50) default NULL,
  `fontsize2_name` char(50) default NULL,
  `fontsize3_name` char(50) default NULL,
  `fontcolor1_name` char(50) default NULL,
  `fontcolor2_name` char(50) default NULL,
  `fontcolor3_name` char(50) default NULL,
  `span_class1_name` char(50) default NULL,
  `span_class2_name` char(50) default NULL,
  `span_class3_name` char(50) default NULL,
  PRIMARY KEY  (`themes_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_themes_name`
-- 

INSERT INTO `phpbb_themes_name` VALUES (1, 'The lightest row colour', 'The medium row color', 'The darkest row colour', '', '', '', 'Border round the whole page', 'Outer table border', 'Inner table border', 'Silver gradient picture', 'Blue gradient picture', 'Fade-out gradient on index', 'Background for quote boxes', 'All white areas', '', 'Background for topic posts', '2nd background for topic posts', '', 'Main fonts', 'Additional topic title font', 'Form fonts', 'Smallest font size', 'Medium font size', 'Normal font size (post body etc)', 'Quote & copyright text', 'Code text colour', 'Main table header text colour', '', '', '');
INSERT INTO `phpbb_themes_name` VALUES (3, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_topics`
-- 

CREATE TABLE `phpbb_topics` (
  `topic_id` mediumint(8) unsigned NOT NULL auto_increment,
  `forum_id` smallint(8) unsigned NOT NULL default '0',
  `topic_title` char(60) NOT NULL default '',
  `topic_poster` mediumint(8) NOT NULL default '0',
  `topic_time` int(11) NOT NULL default '0',
  `topic_views` mediumint(8) unsigned NOT NULL default '0',
  `topic_replies` mediumint(8) unsigned NOT NULL default '0',
  `topic_status` tinyint(3) NOT NULL default '0',
  `topic_vote` tinyint(1) NOT NULL default '0',
  `topic_type` tinyint(3) NOT NULL default '0',
  `topic_first_post_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_last_post_id` mediumint(8) unsigned NOT NULL default '0',
  `topic_moved_id` mediumint(8) unsigned NOT NULL default '0',
  PRIMARY KEY  (`topic_id`),
  KEY `forum_id` (`forum_id`),
  KEY `topic_moved_id` (`topic_moved_id`),
  KEY `topic_status` (`topic_status`),
  KEY `topic_type` (`topic_type`)
) TYPE=MyISAM AUTO_INCREMENT=16 ;

-- 
-- Dumping data for table `phpbb_topics`
-- 

INSERT INTO `phpbb_topics` VALUES (5, 1, 'Beta Download &amp; Reporting Tips', 2, 1160394251, 43, 2, 0, 0, 1, 6, 37, 0);
INSERT INTO `phpbb_topics` VALUES (3, 7, 'UV Tools 1.0 Release', 3, 1160392536, 92, 0, 0, 0, 0, 4, 4, 0);
INSERT INTO `phpbb_topics` VALUES (4, 7, 'Explosive now in Beta - Looking for Beta testers', 2, 1160394038, 461, 10, 0, 0, 0, 5, 34, 0);
INSERT INTO `phpbb_topics` VALUES (6, 3, 'UV Tools Information', 3, 1160394445, 94, 0, 0, 0, 1, 7, 7, 0);
INSERT INTO `phpbb_topics` VALUES (7, 1, 'First impressions', 5, 1160399296, 9, 1, 0, 0, 0, 13, 15, 0);
INSERT INTO `phpbb_topics` VALUES (8, 1, '[BUG][CODE] AI goes idle [FIXED]', 2, 1160432524, 16, 2, 0, 0, 0, 16, 25, 0);
INSERT INTO `phpbb_topics` VALUES (9, 1, '[BUG][CODE] Game instantly ends on Vista', 2, 1160450149, 9, 1, 0, 0, 0, 19, 22, 0);
INSERT INTO `phpbb_topics` VALUES (10, 3, 'F.A.Q.', 3, 1160469051, 86, 0, 0, 0, 1, 21, 21, 0);
INSERT INTO `phpbb_topics` VALUES (11, 1, '[BUG][Code or art]only time in window when playing sp[fixed]', 7, 1160487784, 29, 5, 0, 0, 0, 23, 30, 0);
INSERT INTO `phpbb_topics` VALUES (12, 2, 'F.A.Q', 2, 1160639909, 72, 0, 0, 0, 1, 29, 29, 0);
INSERT INTO `phpbb_topics` VALUES (13, 1, 'Testing session', 2, 1160641140, 7, 0, 0, 0, 0, 31, 31, 0);
INSERT INTO `phpbb_topics` VALUES (14, 4, 'F.A.Q', 2, 1160797571, 44, 0, 0, 0, 1, 36, 36, 0);
INSERT INTO `phpbb_topics` VALUES (15, 7, 'Explosive Demo', 2, 1167044064, 3, 0, 0, 0, 0, 38, 38, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_topics_watch`
-- 

CREATE TABLE `phpbb_topics_watch` (
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `user_id` mediumint(8) NOT NULL default '0',
  `notify_status` tinyint(1) NOT NULL default '0',
  KEY `topic_id` (`topic_id`),
  KEY `user_id` (`user_id`),
  KEY `notify_status` (`notify_status`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_topics_watch`
-- 

INSERT INTO `phpbb_topics_watch` VALUES (6, 3, 0);
INSERT INTO `phpbb_topics_watch` VALUES (4, 3, 0);
INSERT INTO `phpbb_topics_watch` VALUES (10, 3, 0);
INSERT INTO `phpbb_topics_watch` VALUES (11, 3, 0);
INSERT INTO `phpbb_topics_watch` VALUES (8, 3, 0);
INSERT INTO `phpbb_topics_watch` VALUES (12, 3, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_user_group`
-- 

CREATE TABLE `phpbb_user_group` (
  `group_id` mediumint(8) NOT NULL default '0',
  `user_id` mediumint(8) NOT NULL default '0',
  `user_pending` tinyint(1) default NULL,
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_user_group`
-- 

INSERT INTO `phpbb_user_group` VALUES (1, -1, 0);
INSERT INTO `phpbb_user_group` VALUES (2, 2, 0);
INSERT INTO `phpbb_user_group` VALUES (3, 3, 0);
INSERT INTO `phpbb_user_group` VALUES (4, 4, 0);
INSERT INTO `phpbb_user_group` VALUES (5, 2, 0);
INSERT INTO `phpbb_user_group` VALUES (6, 2, 0);
INSERT INTO `phpbb_user_group` VALUES (5, 3, 0);
INSERT INTO `phpbb_user_group` VALUES (5, 4, 0);
INSERT INTO `phpbb_user_group` VALUES (7, 5, 0);
INSERT INTO `phpbb_user_group` VALUES (6, 4, 0);
INSERT INTO `phpbb_user_group` VALUES (5, 5, 0);
INSERT INTO `phpbb_user_group` VALUES (8, 6, 0);
INSERT INTO `phpbb_user_group` VALUES (9, 7, 0);
INSERT INTO `phpbb_user_group` VALUES (5, 7, 0);
INSERT INTO `phpbb_user_group` VALUES (10, 8, 0);
INSERT INTO `phpbb_user_group` VALUES (5, 8, 0);
INSERT INTO `phpbb_user_group` VALUES (11, 9, 0);
INSERT INTO `phpbb_user_group` VALUES (5, 9, 0);
INSERT INTO `phpbb_user_group` VALUES (12, 10, 0);
INSERT INTO `phpbb_user_group` VALUES (13, 11, 0);
INSERT INTO `phpbb_user_group` VALUES (14, 12, 0);
INSERT INTO `phpbb_user_group` VALUES (5, 12, 0);
INSERT INTO `phpbb_user_group` VALUES (15, 13, 0);
INSERT INTO `phpbb_user_group` VALUES (16, 14, 0);
INSERT INTO `phpbb_user_group` VALUES (17, 15, 0);
INSERT INTO `phpbb_user_group` VALUES (18, 16, 0);
INSERT INTO `phpbb_user_group` VALUES (19, 17, 0);
INSERT INTO `phpbb_user_group` VALUES (20, 18, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_users`
-- 

CREATE TABLE `phpbb_users` (
  `user_id` mediumint(8) NOT NULL default '0',
  `user_active` tinyint(1) default '1',
  `username` varchar(25) NOT NULL default '',
  `user_password` varchar(32) NOT NULL default '',
  `user_session_time` int(11) NOT NULL default '0',
  `user_session_page` smallint(5) NOT NULL default '0',
  `user_lastvisit` int(11) NOT NULL default '0',
  `user_regdate` int(11) NOT NULL default '0',
  `user_level` tinyint(4) default '0',
  `user_posts` mediumint(8) unsigned NOT NULL default '0',
  `user_timezone` decimal(5,2) NOT NULL default '0.00',
  `user_style` tinyint(4) default NULL,
  `user_lang` varchar(255) default NULL,
  `user_dateformat` varchar(14) NOT NULL default 'd M Y H:i',
  `user_new_privmsg` smallint(5) unsigned NOT NULL default '0',
  `user_unread_privmsg` smallint(5) unsigned NOT NULL default '0',
  `user_last_privmsg` int(11) NOT NULL default '0',
  `user_login_tries` smallint(5) unsigned NOT NULL default '0',
  `user_last_login_try` int(11) NOT NULL default '0',
  `user_emailtime` int(11) default NULL,
  `user_viewemail` tinyint(1) default NULL,
  `user_attachsig` tinyint(1) default NULL,
  `user_allowhtml` tinyint(1) default '1',
  `user_allowbbcode` tinyint(1) default '1',
  `user_allowsmile` tinyint(1) default '1',
  `user_allowavatar` tinyint(1) NOT NULL default '1',
  `user_allow_pm` tinyint(1) NOT NULL default '1',
  `user_allow_viewonline` tinyint(1) NOT NULL default '1',
  `user_notify` tinyint(1) NOT NULL default '1',
  `user_notify_pm` tinyint(1) NOT NULL default '0',
  `user_popup_pm` tinyint(1) NOT NULL default '0',
  `user_rank` int(11) default '0',
  `user_avatar` varchar(100) default NULL,
  `user_avatar_type` tinyint(4) NOT NULL default '0',
  `user_email` varchar(255) default NULL,
  `user_icq` varchar(15) default NULL,
  `user_website` varchar(100) default NULL,
  `user_from` varchar(100) default NULL,
  `user_sig` text,
  `user_sig_bbcode_uid` varchar(10) default NULL,
  `user_aim` varchar(255) default NULL,
  `user_yim` varchar(255) default NULL,
  `user_msnm` varchar(255) default NULL,
  `user_occ` varchar(100) default NULL,
  `user_interests` varchar(255) default NULL,
  `user_actkey` varchar(32) default NULL,
  `user_newpasswd` varchar(32) default NULL,
  PRIMARY KEY  (`user_id`),
  KEY `user_session_time` (`user_session_time`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_users`
-- 

INSERT INTO `phpbb_users` VALUES (-1, 0, 'Anonymous', '', 0, 0, 0, 1160385193, 0, 2, 0.00, NULL, '', '', 0, 0, 0, 0, 0, NULL, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, NULL, '', 0, '', '', '', '', '', NULL, '', '', '', '', '', '', '');
INSERT INTO `phpbb_users` VALUES (2, 1, 'supagu', '0f6811b05660fde57651fda214919653', 1167106220, 0, 1167045848, 1160385193, 1, 17, 0.00, 3, 'english', 'd M Y h:i a', 0, 0, 1160390512, 0, 0, NULL, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, '', 0, 'support@blackcarbon.net', '70981152', '', 'Australia', 'Lead Software Engineer\r\n\r\nAMD 1500+ XP\r\n1GB Dual Channel RAM\r\nAGP Geforce 6600GT\r\nSegate 250GB HDD\r\nNvidia Mobo\r\nXfi-Creative Sound\r\nDSL2+\r\nWin XP SP2', '70e4f62ceb', '', '', '', 'Software Engineer', '', '', '');
INSERT INTO `phpbb_users` VALUES (3, 1, 'Gibbz', '0f6811b05660fde57651fda214919653', 1161332407, 0, 1161245664, 1160390229, 1, 6, 10.00, 3, 'english', 'D M d, Y g:i a', 0, 0, 1160392039, 0, 0, NULL, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, '', 0, 'gibbz1@yahoo.com', '91779761', 'http://www.blackcarbon.net', 'Australia', 'AMD64 3000+\r\n1 Gig DDR Ram\r\nNvidia 6600GT\r\n2x Seagate 320G Raid-0\r\nX-Fi Music\r\nDual HDTV Tuner\r\nWinXP SP2 / Vista RC1', '81f09819df', '', 'gibbz1@yahoo.com', 'gibbz1@yahoo.com', 'Game Artist', 'Uh games :)', '', NULL);
INSERT INTO `phpbb_users` VALUES (4, 1, 'Knifey', 'f359672cac275af933476bdc2b4e8396', 1160440633, 1, 1160392715, 1160391936, 0, 0, 0.00, 3, 'english', 'D M d, Y g:i a', 0, 0, 1160392226, 0, 0, NULL, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, '', 0, 'j_stott@internode.on.net', '', '', '', 'You heard.', '275b9d6655', '', '', '', '', '', '', NULL);
INSERT INTO `phpbb_users` VALUES (5, 1, 'MrQ', '1adbb3178591fd5bb0c248518f39bf6d', 1164802587, 0, 1160481436, 1160395279, 0, 3, 0.00, 3, 'english', 'D M d, Y g:i a', 0, 0, 1160396304, 0, 0, NULL, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, '', 0, 'caliban32@gmail.com', '', '', '', '', '', '', '', '', '', '', '', NULL);
INSERT INTO `phpbb_users` VALUES (6, 1, 'Tatey', '39d6ac911e9b5fa30ad3fd459a8f4e66', 0, 0, 0, 1160396262, 0, 0, 10.00, 3, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, '', 0, 'tatey86@tpg.com.au', '', 'http://tatey.com', '', '', '', '', '', '', '', '', '', NULL);
INSERT INTO `phpbb_users` VALUES (7, 1, 'MalikDrako', '3bcfb021adab1ebd9c2de7f665021bf8', 1162857029, 0, 1162688344, 1160400348, 0, 4, -8.00, 3, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, '', 0, 'malikdrako@gmail.com', '', '', 'California', 'Desktop:\r\nPentium D 805 2.66Ghz\r\n1GB RAM\r\nATI Radeon 9550\r\nWindows XP SP2\r\n\r\nLaptop:\r\nCeleron M 1.4Ghz\r\n256MB RAM\r\nonboard video\r\nWindows XP SP2', 'cbd7ec068c', '', '', '', '', '', '', NULL);
INSERT INTO `phpbb_users` VALUES (8, 1, 'Neonix', '5f4dcc3b5aa765d61d8327deb882cf99', 1160453018, -9, 1160452731, 1160452670, 0, 1, 0.00, 3, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, '', 0, 'lukebennett@kromestudios.com', '', '', '', '', '', '', '', '', '', '', '', NULL);
INSERT INTO `phpbb_users` VALUES (9, 1, 'edd9139', '355799edbed9c93f755c406c024d7779', 1160736508, 2, 1160736116, 1160736068, 0, 1, 0.00, 3, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, '', 0, 'eddie9139@gmail.com', '', '', '', '', '', '', '', '', '', '', '', NULL);
INSERT INTO `phpbb_users` VALUES (10, 1, 'Hybaj', '6f3abf9640e9822fe0fc14d58604a4d0', 1160994596, 0, 1160994596, 1160994573, 0, 0, 0.00, 3, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, '', 0, 'hybal@chello.sk', '', '', '', '', '', '', '', '', '', '', '', NULL);
INSERT INTO `phpbb_users` VALUES (11, 0, 'Savarn', '7d463de75cbd8b7aaad752627d66dd9c', 0, 0, 0, 1161157806, 0, 0, 0.00, 3, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, '', 0, 'mkennedy@chariot.net.au', '', '', '', '', '', '', '', '', '', '', '83d9ca1ebb', NULL);
INSERT INTO `phpbb_users` VALUES (12, 0, 'Ben Rayner', 'd7bee25f622441bee331075cc541c80f', 0, 0, 0, 1161224039, 0, 0, 0.00, 3, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, '', 0, 'forcestatus@hotmail.com', '', 'http://forcestatus.cgsociety.org', '', '', '', '', '', '', 'Game Art Student', 'Drawing, 3d', '4df24005f4', NULL);
INSERT INTO `phpbb_users` VALUES (13, 0, 'Jesta', '9571fe5fce57186d6f9097eff89f687b', 0, 0, 0, 1161435085, 0, 0, 0.00, 3, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, '', 0, 'thomasgarwood@gmail.com', '', 'http://www.illusiongames.co.nr', '', '', '', '', '', '', '', '', '79c38bcd29', NULL);
INSERT INTO `phpbb_users` VALUES (14, 1, 'carlbropt', 'a740d0d2a82fb736468220b38850a8aa', 0, 0, 0, 1165311742, 0, 0, 1.00, 1, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, '', 0, 'carlbropt@berahe.info', '', 'http://toolsef.com', '', '', '', '', '', '', '', '', '', NULL);
INSERT INTO `phpbb_users` VALUES (15, 0, 'MadTimmy', 'ed6b846216467e7d6318a39699bd58c9', 0, 0, 0, 1165671077, 0, 0, 3.00, 1, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, '', 0, 'ivankuzmich@cashette.com', '', '', '', '', '', '', '', '', '', '', '168b2d6d46', NULL);
INSERT INTO `phpbb_users` VALUES (16, 1, 'melfariost', '86d856309a2550dad88cf64dc5a4ed5f', 0, 0, 0, 1165968819, 0, 0, 1.00, 1, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, '', 0, 'melfariost@rosledin.info', '', 'http://kitchenom.com', '', '', '', '', '', '', '', '', '', NULL);
INSERT INTO `phpbb_users` VALUES (17, 0, 'AdskiyOtji', 'ed6b846216467e7d6318a39699bd58c9', 0, 0, 0, 1165972193, 0, 0, 3.00, 1, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, '', 0, 'adskiyotjig@mail.ru', '', '', '', '', '', '', '', '', '', '', '62fac1dbf6', NULL);
INSERT INTO `phpbb_users` VALUES (18, 0, 'nblamter', '7a24fe4784e12ff7ac0171799b84add9', 0, 0, 0, 1166645981, 0, 0, 1.00, 1, 'english', 'D M d, Y g:i a', 0, 0, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, '', 0, 'nblamter@refiras.info', '', 'http://agrocery.com', '', '', '', '', '', '', '', '', '93be70d54e', NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_vote_desc`
-- 

CREATE TABLE `phpbb_vote_desc` (
  `vote_id` mediumint(8) unsigned NOT NULL auto_increment,
  `topic_id` mediumint(8) unsigned NOT NULL default '0',
  `vote_text` text NOT NULL,
  `vote_start` int(11) NOT NULL default '0',
  `vote_length` int(11) NOT NULL default '0',
  PRIMARY KEY  (`vote_id`),
  KEY `topic_id` (`topic_id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `phpbb_vote_desc`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_vote_results`
-- 

CREATE TABLE `phpbb_vote_results` (
  `vote_id` mediumint(8) unsigned NOT NULL default '0',
  `vote_option_id` tinyint(4) unsigned NOT NULL default '0',
  `vote_option_text` varchar(255) NOT NULL default '',
  `vote_result` int(11) NOT NULL default '0',
  KEY `vote_option_id` (`vote_option_id`),
  KEY `vote_id` (`vote_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_vote_results`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_vote_voters`
-- 

CREATE TABLE `phpbb_vote_voters` (
  `vote_id` mediumint(8) unsigned NOT NULL default '0',
  `vote_user_id` mediumint(8) NOT NULL default '0',
  `vote_user_ip` char(8) NOT NULL default '',
  KEY `vote_id` (`vote_id`),
  KEY `vote_user_id` (`vote_user_id`),
  KEY `vote_user_ip` (`vote_user_ip`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `phpbb_vote_voters`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `phpbb_words`
-- 

CREATE TABLE `phpbb_words` (
  `word_id` mediumint(8) unsigned NOT NULL auto_increment,
  `word` char(100) NOT NULL default '',
  `replacement` char(100) NOT NULL default '',
  PRIMARY KEY  (`word_id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `phpbb_words`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `product`
-- 

CREATE TABLE `product` (
  `id` int(10) unsigned NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `price` float NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `product`
-- 

INSERT INTO `product` VALUES (0, 'Explosive', 20);
INSERT INTO `product` VALUES (1, 'UV Tools', 30);

-- --------------------------------------------------------

-- 
-- Table structure for table `session`
-- 

CREATE TABLE `session` (
  `auth` varchar(32) NOT NULL default '',
  `userid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`auth`),
  UNIQUE KEY `id` (`auth`,`userid`),
  FULLTEXT KEY `auth` (`auth`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `session`
-- 

INSERT INTO `session` VALUES ('7c6f793bc263c991a16a745bd11fbf83', 78);

-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `firstname` varchar(128) NOT NULL default '',
  `lastname` varchar(128) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `mailinglist` tinyint(1) NOT NULL default '0',
  `password` varchar(32) NOT NULL default '',
  `verified` tinyint(1) NOT NULL default '0',
  `admin` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=101 ;

-- 
-- Dumping data for table `user`
-- 

INSERT INTO `user` VALUES (80, 'Bronson', 'Mathews', 'gibbz1@yahoo.com', 1, '0f6811b05660fde57651fda214919653', 1, 1);
INSERT INTO `user` VALUES (75, 'Damir', 'Simovski', 'BCarbon@simovski.com', 0, 'c37c9ca0ec5b2c0eacad336ee4c66cc6', 1, 0);
INSERT INTO `user` VALUES (78, 'Fabian', 'Mathews', 'supagu64@yahoo.com', 1, '0f6811b05660fde57651fda214919653', 1, 1);
INSERT INTO `user` VALUES (81, 'Bryan', 'Reynolds', 'breynolds@gamesauce.co.uk', 0, '3b7a9fb5fdb44e561b5f87f128e29f72', 1, 0);
INSERT INTO `user` VALUES (82, 'Kim', 'JungHoon', 'sharkmouth@nate.com', 1, '3d186804534370c3c817db0563f0e461', 1, 0);
INSERT INTO `user` VALUES (83, 'bazuka', 'baz', 'bazuka666@yahoo.com', 0, '0b9a54438fba2dc0d39be8f7c6c71a58', 1, 0);
INSERT INTO `user` VALUES (84, 'Dmitriy', 'Nesonov', 'ok@supertema.ru', 1, '0a295b494e0cafee78df9f8b26622504', 1, 0);
INSERT INTO `user` VALUES (85, 'claudio', 'gallego', 'pole3dclaudio@free.Fr', 1, '7a1c5e4f4caa256d73616e87a338b66c', 1, 0);
INSERT INTO `user` VALUES (86, 'Timothy', 'Higgins', 'lobo1963@earthlink.net', 1, '024e9ea690b7d08661dbace2f30fff27', 1, 0);
INSERT INTO `user` VALUES (87, 'Timothy', 'Higgins', 'thiggins@vvisions.com', 1, '024e9ea690b7d08661dbace2f30fff27', 1, 0);
INSERT INTO `user` VALUES (88, 'Aamer', 'Irfan', 'i_aamer10@yahoo.com', 1, '18e3cb8060fa8f1c953ec46bb4ec7523', 1, 0);
INSERT INTO `user` VALUES (89, 'necros', 'asddasd', 'OUTROPIA@HOTMAIL.COM', 0, 'a4f53a5b7221f89199c90bbbb7fc39a9', 1, 0);
INSERT INTO `user` VALUES (90, 'Kjetil', 'Hjeldnes', 'kjetilh@funcom.com', 0, 'e10adc3949ba59abbe56e057f20f883e', 1, 0);
INSERT INTO `user` VALUES (91, 'Richard', 'Cawte', 'richardc@funcom.com', 1, '725337daa392180f4888efbebb994d55', 1, 0);
INSERT INTO `user` VALUES (92, '�yvind', 'Jernskau', 'oyvind@funcom.com', 1, '80e3bbdf754ffa96ae9dcd315bf48673', 1, 0);
INSERT INTO `user` VALUES (93, 'Tyler', 'James', 'tyler@inxile.net', 0, 'eb2a13123e3297b17454fc36e72d161f', 1, 0);
INSERT INTO `user` VALUES (94, 'Funcom', 'ACHA', 'fabricem@funcom.com', 0, '0d0de813c1105498e3435dd2fbf7fa26', 1, 0);
INSERT INTO `user` VALUES (95, 'Peter', 'Hybal', 'hybal@chello.sk', 1, '6f3abf9640e9822fe0fc14d58604a4d0', 1, 0);
INSERT INTO `user` VALUES (96, 'ben', 'rayner', 'forcestatus@hotmail.com', 0, 'd7bee25f622441bee331075cc541c80f', 0, 0);
INSERT INTO `user` VALUES (97, 'shaq', 'shiu', 'zmaxz2@yahoo.com.tw', 1, '9f35da5e9c893693cbb7920e124bdbfa', 0, 0);
INSERT INTO `user` VALUES (98, 'shaq', 'shiu', 'shaq@lager.com.tw', 1, '9f35da5e9c893693cbb7920e124bdbfa', 1, 0);
INSERT INTO `user` VALUES (99, 'Steve', 'Schmidt', 'Pixelpusher75@sbcglobal.net', 1, '2121faf504c0aee6243fd86a6272a380', 1, 0);
INSERT INTO `user` VALUES (100, 'sugikami', 'tetsuya', 'sega193@yahoo.co.jp', 1, '3c8615255fd0614a03245e4bfef9a84a', 1, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `userdownload`
-- 

CREATE TABLE `userdownload` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userid` int(10) unsigned NOT NULL default '0',
  `downloadid` int(10) unsigned NOT NULL default '0',
  `orderid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=24 ;

-- 
-- Dumping data for table `userdownload`
-- 

INSERT INTO `userdownload` VALUES (18, 78, 29, 186);
INSERT INTO `userdownload` VALUES (17, 90, 28, 188);
INSERT INTO `userdownload` VALUES (16, 78, 27, 183);
INSERT INTO `userdownload` VALUES (19, 91, 30, 190);
INSERT INTO `userdownload` VALUES (20, 94, 31, 194);
INSERT INTO `userdownload` VALUES (21, 94, 32, 195);
INSERT INTO `userdownload` VALUES (22, 94, 33, 196);
INSERT INTO `userdownload` VALUES (23, 92, 34, 197);

-- --------------------------------------------------------

-- 
-- Table structure for table `uvtoolslicence`
-- 

CREATE TABLE `uvtoolslicence` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userid` int(10) unsigned NOT NULL default '0',
  `type` smallint(5) unsigned NOT NULL default '0',
  `maxid` varchar(32) NOT NULL default '0',
  `number` int(10) unsigned NOT NULL default '0',
  `companyname` varchar(255) NOT NULL default '',
  `verified` tinyint(1) NOT NULL default '0',
  `companyemail` varchar(255) NOT NULL default '',
  `orderid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=205 ;

-- 
-- Dumping data for table `uvtoolslicence`
-- 

INSERT INTO `uvtoolslicence` VALUES (204, 100, 0, '1', 1, '', 0, '', 202);
INSERT INTO `uvtoolslicence` VALUES (203, 80, 0, '111111', 1, '', 0, '', 201);
INSERT INTO `uvtoolslicence` VALUES (202, 78, 0, '000000', 1, '', 0, '', 200);
INSERT INTO `uvtoolslicence` VALUES (201, 78, 1, '0', 1, 'test', 0, 'test@test.com', 0);
INSERT INTO `uvtoolslicence` VALUES (200, 99, 1, '0', 1, 'pixelpusher digital', 0, 'pixelpusher75@sbcglobal.net', 0);
INSERT INTO `uvtoolslicence` VALUES (199, 92, 0, '1819643555', 1, '', 0, '', 197);
INSERT INTO `uvtoolslicence` VALUES (198, 94, 0, '1604355633', 1, '', 0, '', 196);
INSERT INTO `uvtoolslicence` VALUES (197, 94, 0, '1694174322', 1, '', 0, '', 195);
INSERT INTO `uvtoolslicence` VALUES (196, 94, 0, '1070696317', 1, '', 0, '', 194);
INSERT INTO `uvtoolslicence` VALUES (195, 94, 1, '0', 6, 'FUNCOM AS', 0, 'fabricem@funcom.com', 0);
INSERT INTO `uvtoolslicence` VALUES (194, 78, 0, '4444', 1, '', 0, '', 192);
INSERT INTO `uvtoolslicence` VALUES (188, 78, 0, '111', 1, '', 0, '', 186);
INSERT INTO `uvtoolslicence` VALUES (189, 83, 1, '0', 1, 'LOI', 0, 'bazuka666@yahoo.com', 0);
INSERT INTO `uvtoolslicence` VALUES (190, 90, 0, '-2053381146', 1, '', 0, '', 188);
INSERT INTO `uvtoolslicence` VALUES (193, 90, 0, '-1597165220', 1, '', 0, '', 191);
INSERT INTO `uvtoolslicence` VALUES (192, 91, 0, '821906827', 1, '', 0, '', 190);
INSERT INTO `uvtoolslicence` VALUES (181, 78, 0, '-1430656134', 1, '', 0, '', 183);
INSERT INTO `uvtoolslicence` VALUES (191, 78, 0, '333333', 1, '', 0, '', 189);
