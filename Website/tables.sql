CREATE DATABASE db;
USE db;

CREATE TABLE `session` (
  `auth` varchar(32) NOT NULL default '',
  `userid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`auth`),
  UNIQUE KEY `id` (`auth`,`userid`),
  FULLTEXT KEY `auth` (`auth`)
) TYPE=MyISAM;

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `username` varchar(255) NOT NULL default '',
  `firstname` varchar(128) NOT NULL default '',
  `lastname` varchar(128) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `mailinglist` tinyint(1) NOT NULL default '0',
  `password` varchar(32) NOT NULL default '',
  `verified` tinyint(1) NOT NULL default '0',
  `admin` tinyint(1) NOT NULL default '0',
  `forumid` int(11) NOT NULL default '-1',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=132 ;

INSERT INTO `user` VALUES (0, 'SupaGu', 'Fabian', 'Mathews', 'supagu64@yahoo.com', 1, '0f6811b05660fde57651fda214919653', 1, 1, -1);

CREATE TABLE `dogfighters_highscore` 
(
	`id` int(10) unsigned NOT NULL auto_increment,
	`userid` int(10) unsigned NOT NULL,
	`hits` int(10) unsigned NOT NULL default '0',
	`deaths` int(10) unsigned NOT NULL default '0',
	`shotsFired` int(10) unsigned NOT NULL default '0',
	`pickups` int(10) unsigned NOT NULL default '0',
	`waveCount` int(10) unsigned NOT NULL default '0',
	`time` int(10) unsigned NOT NULL default '0',
	`score` int(10) unsigned NOT NULL default '0',
	`submitTime` datetime NOT NULL default '0000-00-00 00:00:00',
	KEY `id` (`id`)
);