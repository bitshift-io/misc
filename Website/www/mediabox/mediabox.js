// -----------------------------------------------------------------------------------
//  Mediabox
//      by Fabian Mathews
//      www.blackcarbon.net
//  
//  Based on lightbox (http://lokeshdhakar.com/projects/lightbox2/) by Lokesh Dhakar
//  and Videobox (http://videobox-lb.sourceforge.net/)
// -----------------------------------------------------------------------------------

var DisplayType = { Image:0, Video:1, Flash:2, WebPage:3 };
var State = { Closed:0, OpeningCenter:1, OpeningBottom:2, Open:3, ClosingBottom:4, Closing:5 };

var overlay;
var center;
var caption;
var number;
var nextImageLink;
var prevImageLink;
var state;
var activeImage;
var preloadImage;
var title;
var anchors = [];
var options;
var morphFx;
var options;
var other;
var clickLink;
var imageArray = [];
var htmlElement;

window.addEvent('domready', init);

Event.domReady.add(init);

function init() 
{
    // init default options
	options = Object.extend(
	        {
	            imageText: "Image {x} of {y}",		// Translate or change as you wish
	            videoText: "Video",
	            showCounter: true,      // Show number
	            loop: true,             // Allow image viewr to loop around
		        resizeDuration: 1400,	// Duration of height and width resizing (ms)
		        resizeTransition: true,
		        initialWidth: 250,		// Initial width of the box (px)
		        initialHeight: 250,		// Initial height of the box (px)
		        defaultWidth: 425,		// Default width of the box (px)
		        defaultHeight: 350,	    // Default height of the box (px)
		        contentWidth: 0,		// set by code (px)
		        contentHeight: 0,	    // set by code (px)
		        animateCaption: true,	// Enable/Disable caption animation
		        flvplayer: '/mediabox/flvplayer.swf'
	        }, options || {}
	    );


	$A($$('a')).each(
	    function(el)
	    {
		    if (el.rel && el.href && el.rel.test('^mediabox', 'i')) 
		    {
			    el.addEvent('click', 
		            function (e) 
		            {
                      e = new Event(e);
                      e.stop();
                      click(el);
		            });
		            
			    anchors.push(el);
			    
			    if (el.href.match(/\.shtml/i) || el.href.match(/\.php/i))
			    {
			        var match = el.rel.match("^mediabox\\[(.*)\\]")[1];
			        htmlElement = document.getElementsByName(match)[0];
			        htmlElement.injectInside(document.body);
			        htmlElement.setStyles({display: 'none'});
			    }
		    }
        }, this);
    
    overlay = new Element('div').setProperty('id', 'lbOverlay').injectInside(document.body);
	center = new Element('div').setProperty('id', 'lbCenter').injectInside(document.body).injectInside(document.body);

    bottom = new Element("div", {id: "lbBottomContainer"}).injectInside(document.body);
    
    bottomInternal = new Element('div').setProperty('id', 'lbBottom').injectInside(bottom/*document.body*/).adopt(
            closeBtn = new Element("a", {id: "lbCloseLink", href: "#_self"}).addEvent("click", close),
            caption = new Element("div", {id: "lbCaption"}),
			number = new Element("div", {id: "lbNumber"}),						
			new Element("div", {styles: {clear: "both"}})
		);
		
	prevLink = new Element("a", {id: "lbPrevLink", href: "#_self"}).addEvent("click", previous);
	nextLink = new Element("a", {id: "lbNextLink", href: "#_self"}).addEvent("click", next);

    morphFx = 
    {
	    overlay: overlay.effect('opacity', {duration: 500}).hide(),
		center: center.effects({duration: 500, transition: Fx.Transitions.sineInOut, onComplete: nextEffect}),
		bottom: bottom.effects({duration: 500, transition: Fx.Transitions.sineInOut, onComplete: nextEffect})
	};
	
	close();
}

function next() 
{
    click(nextImageLink);
	return false;
}

function previous() 
{
    click(prevImageLink);
	return false;
}

function beginResizeEffect()
{
    // start up morph fx
    var windowCenter = getWindowCenter();
	var pageSizeArray = getPageSize();
    overlay.setStyles({'top': '0px', 'height': pageSizeArray[1] +'px'}); //.setStyles({'top': window.getScrollTop()+'px', 'height': pageSizeArray[1] +'px'});
        
    var finalTop = windowCenter[1]/*(pageSizeArray[1] / 2)*/ - (options.contentHeight / 2);
    
	if (displayType == DisplayType.Image)
    {
        var finalTop = windowCenter[1]/*(pageSizeArray[1] / 2)*/ - (preloadImage.height / 2);
        morphFx.center.start({'width': [preloadImage.width], 'marginLeft': [preloadImage.width/-2], 'height': [preloadImage.height], 'top': [finalTop]});
    }
    else
    {
        morphFx.center.start({'width': [options.contentWidth], 'marginLeft': [options.contentWidth/-2], 'height': [options.contentHeight], 'top': [finalTop]});
    }

	morphFx.overlay.start(0.8);
}

function click(link) 
{
    clickLink = link;
    title = link.title;
	parseLink(link, link.href);
	
	center.setHTML("");
	
    // if opened for the first time, set up the window to defaults
    if (state == State.Closed)
    {
        var windowCenter = getWindowCenter();
        //centerHeight = 
        //centerWidth = window.innerWidth;
        
        //var pageSizeArray = getPageSize();
        //var top = (pageSizeArray[1] / 2) - (options.initialHeight / 2);
        var top = windowCenter[1] - (options.initialHeight / 2);
	    center.setStyles({});
	    center.setStyles({top: top+'px', display: '', width: options.initialWidth+'px', height: options.initialHeight+'px', marginLeft: '-'+options.initialWidth/2+'px'}).injectInside(document.body);

        center.className = "";
        
        // hide the bottom
        bottom.setStyles({display: 'none'});
    }
    else if (state != State.ClosingBottom)
    {       
        center.setStyle('background','#fff url(/mediabox/loading.gif) no-repeat center');
        
        // fold in the bottom
        state = State.ClosingBottom;
        morphFx.bottom.start({'height': [0]});       
        return;
    }
    else
    {
        // hide the bottom
        bottom.setStyles({display: 'none'});
    }
    
    caption.innerHTML = title;
		
	state = State.OpeningCenter;
	center.setStyle('background','#fff url(/mediabox/loading.gif) no-repeat center');
	
    if (displayType == DisplayType.Image)
    {
	    preloadImage = new Image();
	    preloadImage.onload = beginResizeEffect;
	    preloadImage.src = link.href;
	    center.className = "lbLoading";
	    
	    var prevImage = ((activeImage || !options.loop) ? activeImage : imageArray.length) - 1;
		var nextImage = activeImage + 1;
		if (nextImage == imageArray.length) 
		    nextImage = options.loop ? 0 : -1;
		    
		prevImageLink = undefined;
		if (prevImage >= 0 && imageArray.length > 1)
		    prevImageLink = imageArray[prevImage][2];
		    
		nextImageLink = undefined;
		if (nextImage >= 0 && imageArray.length > 1)
		    nextImageLink = imageArray[nextImage][2];
	}
	else
	{	
        beginResizeEffect();
    }
}

function displayMedia()
{       
    caption.innerHTML = title;
     
    switch (displayType)
    {
    case DisplayType.Image:
        {                
            var pageSizeArray = getPageSize();
          
            center.setStyles({backgroundImage: "url(" + preloadImage.src + ")", display: ""});
            
			$$(prevLink, nextLink).setStyle("height", preloadImage.height);
			
			if (prevImageLink)
	            prevLink.injectInside(center);    		
	            
	        if (nextImageLink)
                nextLink.injectInside(center);
        }
        break;
        
    case DisplayType.WebPage:
        {
            htmlElement.injectInside(center);
            htmlElement.setStyles({display: ''});
        }
        break;
        
    case DisplayType.Video:
        {
            center.setHTML(other);
        }
        break;
        
    case DisplayType.Flash:
        {
            so.write(center);
        }
        break;
    }
}

function nextEffect()
{
    // webpages dont show the bottom tab, the webpage is required to close its self
    // if needed
    if (displayType == DisplayType.WebPage && state == State.OpeningCenter)
    {        
        state = State.OpeningBottom;
    }
    
    switch (state)
    {
    case State.OpeningCenter:
        {
            state = State.OpeningBottom;
   
            number.innerHTML = options.videoText;
            
            var pageSizeArray = getPageSize();
            var top = center.offsetTop + center.offsetHeight;
            
            if (displayType == DisplayType.Image)
            {
                var numberString = (options.showCounter && (imageArray.length > 1)) ? options.imageText.replace(/{x}/, activeImage + 1).replace(/{y}/, imageArray.length) : "";
                number.innerHTML = numberString;
            }
            
            top -= 10; // the border is 10px
            
            // set up bottom effect to play
            bottom.setStyles({top: top+'px', marginLeft: center.style.marginLeft, width: center.style.width, display: '', height: 0});
            
            morphFx.bottom.start({'height': [30]});
            morphFx.bottom.start();     
        }
        break;
    
    case State.OpeningBottom:
        {
            state = State.Open;
            displayMedia();
        }
        break;
        
    case State.ClosingBottom:
        {
            click(clickLink);
        }
        break;
    }
}

function close()
{
    if (htmlElement)
    {
        htmlElement.setStyles({display: 'none'});
        htmlElement.injectInside(document.body);
        htmlElement = 0;
    }
    
	morphFx.overlay.start(0);
	center.style.display = bottom.style.display = 'none';
	center.innerHTML = '';
	state = State.Closed;
	return true;
}

function getWindowCenter()
{
    return [window.getScrollLeft() + (window.innerWidth / 2), window.getScrollTop() + (window.innerHeight / 2)];
}

function getPageSize() 
{
	return [window.getScrollWidth(), window.getScrollHeight()];
}

function parseLink(link, sLinkHref)
{
	// set up the content size
	var aDim = link.rel.match(/[0-9]+/g);
    options.contentWidth = (aDim && (aDim[0] > 0)) ? aDim[0] : options.defaultWidth;
    options.contentHeight = (aDim && (aDim[1] > 0)) ? aDim[1] : options.defaultHeight;
    
	if (sLinkHref.match(/youtube\.com\/watch/i)) 
	{
	    displayType = DisplayType.Flash;
	    
        flash = true;
		var hRef = sLinkHref;
		var videoId = hRef.split('=');
		videoID = videoId[1];
		so = new SWFObject("http://www.youtube.com/v/"+videoID, "flvvideo", options.contentWidth, options.contentHeight, "0");
		so.addParam("wmode", "transparent");
	}
	else if (sLinkHref.match(/metacafe\.com\/watch/i)) 
	{
	    displayType = DisplayType.Flash;
		var hRef = sLinkHref;
		var videoId = hRef.split('/');
		videoID = videoId[4];
		so = new SWFObject("http://www.metacafe.com/fplayer/"+videoID+"/.swf", "flvvideo", options.contentWidth, options.contentHeight, "0");
		so.addParam("wmode", "transparent");
	}
	else if (sLinkHref.match(/google\.com\/videoplay/i))
	{
	    displayType = DisplayType.Flash;
		var hRef = sLinkHref;
		var videoId = hRef.split('=');
		videoID = videoId[1];
		so = new SWFObject("http://video.google.com/googleplayer.swf?docId="+videoID+"&hl=en", "flvvideo", options.contentWidth, options.contentHeight, "0");
		so.addParam("wmode", "transparent");
	}
	else if (sLinkHref.match(/ifilm\.com\/video/i))
    {
        displayType = DisplayType.Flash;
		var hRef = sLinkHref;
		var videoId = hRef.split('video/');
		videoID = videoId[1];
		so = new SWFObject("http://www.ifilm.com/efp", "flvvideo", options.contentWidth, options.contentHeight, "0", "#000");
		so.addVariable("flvbaseclip", videoID+"&");
		so.addParam("wmode", "transparent");
	}
	else if (sLinkHref.match(/\.mov/i)) 
	{
	    displayType = DisplayType.Video;
		if (navigator.plugins && navigator.plugins.length) 
		{
            other ='<object id="qtboxMovie" type="video/quicktime" codebase="http://www.apple.com/qtactivex/qtplugin.cab" data="'+sLinkHref+'" width="'+options.contentWidth+'" height="'+options.contentHeight+'"><param name="src" value="'+sLinkHref+'" /><param name="scale" value="aspect" /><param name="controller" value="true" /><param name="autoplay" value="true" /><param name="bgcolor" value="#000000" /><param name="enablejavascript" value="true" /></object>';
        }
        else 
        {
            other = '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" width="'+options.contentWidth+'" height="'+options.contentHeight+'" id="qtboxMovie"><param name="src" value="'+sLinkHref+'" /><param name="scale" value="aspect" /><param name="controller" value="true" /><param name="autoplay" value="true" /><param name="bgcolor" value="#000000" /><param name="enablejavascript" value="true" /></object>';
        }
	}
	else if (sLinkHref.match(/\.wmv/i) || sLinkHref.match(/\.asx/i)) 
	{
	    displayType = DisplayType.Video;
	    other = '<object NAME="Player" WIDTH="'+options.contentWidth+'" HEIGHT="'+options.contentHeight+'" align="left" hspace="0" type="application/x-oleobject" CLASSID="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6"><param NAME="URL" VALUE="'+sLinkHref+'"><param><param NAME="AUTOSTART" VALUE="false"></param><param name="showControls" value="true"></param><embed WIDTH="'+options.contentWidth+'" HEIGHT="'+options.contentHeight+'" align="left" hspace="0" SRC="'+sLinkHref+'" TYPE="application/x-oleobject" AUTOSTART="false"></embed></object>'
	}
	else if (sLinkHref.match(/\.flv/i)) 
	{
	    displayType = DisplayType.Flash;
	    so = new SWFObject(options.flvplayer+"?file="+sLinkHref, "flvvideo", options.contentWidth, options.contentHeight, "0", "#000");
	}
	else if (sLinkHref.match(/\.jpg/i) || sLinkHref.match(/\.jpeg/i) || sLinkHref.match(/\.gif/i) || sLinkHref.match(/\.png/i)) 
	{
	    displayType = DisplayType.Image;
	    
        imageArray = [];
        var imageNum = 0;       

        if ((link.rel == 'mediabox'))
        {
            // if image is NOT part of a set, add single image to imageArray
            imageArray.push([link.href, link.title, link]);
            activeImage = 0;         
        }
        else
        {
            // if image is part of a set..
            imageArray = [];
            var imageIdx = 0;
            for (var i = 0; i < anchors.length; ++i)
            {
                var rel = anchors[i].rel;
                if (rel == link.rel)
                {
                    imageArray.push([anchors[i].href, anchors[i].title, anchors[i]]);
                   
                    if (anchors[i] == link)
                        activeImage = imageIdx;
                    
                    ++imageIdx;    
                }
            }
        }
	}
	else if (sLinkHref.match(/\.shtml/i) || sLinkHref.match(/\.php/i)) 
	{
	    displayType = DisplayType.WebPage;
	    var match = link.rel.match("^mediabox\\[(.*)\\]")[1];
	    htmlElement = document.getElementsByName(match)[0];
	}
	else
	{
	    displayType = DisplayType.Flash;
		videoID = sLinkHref;
		so = new SWFObject(videoID, "flvvideo", options.contentWidth, options.contentHeight, "0");
	}
}
