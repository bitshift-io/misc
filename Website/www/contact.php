<?php

	//
	// This is a standard page,
	// rename it and it will execute the htm equivelent
	//
	include_once('../include/template.inc.php');
	include_once('../include/login.inc.php');
  include_once('../include/functions.inc.php');
  include_once('../include/global.inc.php');
	
	// get currently executing page, and replace .php with .shtml
	$phpPage = $_SERVER['PHP_SELF'];
	$htmPage = str_replace('.php', '.shtml', $phpPage);
	$htmPage = substr(strrchr($htmPage, '/'), 1);
		
	$tBody = & new PageTemplate($htmPage);
	$tBody->Set('admin', IsAdmin());
	$tBody->Set('login', IsLoggedIn());
	
	if (IsLoggedIn())
	{
		$login = GetLogin();
		$tBody->Set('username', $login->GetUserName());
	}
  
  switch ($_GET['action'])
  {
  default:
    {
    }
    break;
    
  case 'email':
    {
      if (SendMail($GB['emailAddress'], "[Black Carbon] Question", $_POST['message'], $_POST['email']))
      {
        $tBody->Set('sucessMessage', "You question has succesfully been sent. You should receive a response with in a few days.");
      }
    }
    break;
  }
	
	echo $tBody->Fetch();
	
?>