<?php

	require_once('../../include/dbase.inc.php');
	require_once('../../include/login.inc.php');
	require_once('../../include/template.inc.php');
			
  class Score
  {
    var $hits;
    var $deaths;
    var $shotsFired;
    var $pickups;
    var $waveCount;
    var $time;
    var $score;
  };
  
  $maxScoreEntries = 10;
  $scoresPerPage = 100;
    
	switch ($_GET['action']) 
	{   
	case 'submit':
		{            
			// user needs to login to register highscore
			if (!IsLoggedIn())
			{
				$tBody = & new PageTemplate();
        $redirect = $_SERVER['PHP_SELF'] . '?action=submit&content=' . $_GET['content'];
        
				$tBody->Redirect('/login/?redirect=' . urlencode($redirect));
				return;
			}
			
      $content = base64_decode($_GET['content']);
      $score = unserialize($content);
      
			$db = new DBase();
			$db->Connect();
			
			$login = GetLogin();
      
      $arrValuePairs = array('userid' => $login->uid, 'hits' => $score->hits, 'deaths' => $score->deaths, 'shotsFired' => $score->shotsFired, 'pickups' => $score->pickups, 'waveCount' => $score->waveCount, 'time' => $score->time, 'score' => $score->score, 'submitTime' => 'NOW()');
      
      // make sure this score hasnt already been added
      $result = $db->SelectWhere('dogfighters_highscore', array('*'), 'userid ='. $login->uid .' AND hits='. $score->hits .' AND deaths='. $score->deaths .' AND shotsFired='. $score->shotsFired .' AND pickups='. $score->pickups .' AND waveCount='. $score->waveCount .' AND time='. $score->time .' AND score='. $score->score);
      if ($db->GetNumRows($result) <= 0)
      {      
        // only allow so many scores to be tracked per user,
        // keep the top scores
        // this result has lowest scores at the top, so just need to update the lowest score
        $result = $db->SelectWhere('dogfighters_highscore', array('*'), 'userid = ' . $login->uid . ' ORDER BY score ASC, waveCount ASC');
        if ($db->GetNumRows($result) > $maxScoreEntries)
        {
          $lowest = $db->GetNextRow($result);
          $result = $db->Update('dogfighters_highscore', $arrValuePairs, 'userid == ' . $login->uid . ' AND id == ' . $lowest['id']);
        }
        else
        {			
			    $db->Insert('dogfighters_highscore', $arrValuePairs);
        }
      }
		}
		    
	default:
	case 'display':
		{
      $sortColumn = isset($_GET['sort']) ? $_GET['sort'] : 'score';
      
      if ($sortColumn == 'name')
        $order = 'user.username DESC, dogfighters_highscore.score DESC, dogfighters_highscore.waveCount DESC';
      else if ($sortColumn == 'score')
        $order = 'dogfighters_highscore.score DESC, dogfighters_highscore.waveCount DESC';
      else if ($sortColumn == 'wavecount')
        $order = 'dogfighters_highscore.wavecount DESC, dogfighters_highscore.waveCount DESC';
    
      $tBody = & new PageTemplate('highscore.shtml');
      $tBody->Set('admin', IsAdmin());
	    $tBody->Set('login', IsLoggedIn());
      $tBody->Set('cssOverride', '/dogfighters/resource/stylesheet.css');
      
      if (IsLoggedIn())
	    {
		    $login = GetLogin();
		    $tBody->Set('username', $login->GetUserName());
	    }
  
			$db = new DBase();
			$db->Connect();
			
			$result = $db->SelectWhereArray(array('dogfighters_highscore', 'user'), array('user.id', 'user.username', 'dogfighters_highscore.waveCount', 'dogfighters_highscore.score'), "dogfighters_highscore.userid=user.id ORDER BY $order");
      
      // skip some pages
      $page = isset($_GET['page']) ? $_GET['page'] : 0;
      $startIndex = $scoresPerPage * $page;
      for ($i = 0; $i < $startIndex; ++$i)
      {
        $row = $db->GetNextRow($result);
      }
        
      $maxResult = $db->GetNumRows($result) - $startIndex;
      if ($maxResult > $scoresPerPage)
      {
        $tBody->Set('nextPage', $page + 1);
        $maxResult = $scoresPerPage;
      }
        
      if ($page > 0)
        $tBody->Set('prevPage', $page - 1);
        
      $scores[0] = ""; // set up a a blank array
      for ($i = 0; $i < $maxResult; $i++)
      {
        $row = $db->GetNextRow($result);
        $scores[$i] = $row; 
      }

			$tBody->Set('scores', $scores);      
      echo $tBody->Fetch();
		}
		break;
    
  case 'detail':
    {
      $userId = $_GET['id'];
    
      $tBody = & new PageTemplate('highscoredetail.shtml');
      $tBody->Set('admin', IsAdmin());
	    $tBody->Set('login', IsLoggedIn());
      $tBody->Set('cssOverride', '/dogfighters/resource/stylesheet.css');
      
      if (IsLoggedIn())
	    {
		    $login = GetLogin();
		    $tBody->Set('username', $login->GetUserName());
	    }
  
			$db = new DBase();
			$db->Connect();
			
			$result = $db->SelectWhereArray(array('dogfighters_highscore', 'user'), array('user.id', 'user.username', 'dogfighters_highscore.*'), "dogfighters_highscore.userid=user.id AND user.id=$userId ORDER BY dogfighters_highscore.score DESC, dogfighters_highscore.waveCount DESC");
      
      $scores = array();
      
      $i = 0;
      while ($row = $db->GetNextRow($result))
      {
        $scores[$i] = $row; 
        ++$i;
      }
      
      $tBody->Set('scores', $scores);      
      echo $tBody->Fetch();
    }
    break;
	}
?>