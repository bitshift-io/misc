<?
/*
 +-------------------------------------------------------------------+
 |                  S I M P L E   F O R U M   (v3.4)                 |
 |                                                                   |
 | Copyright Gerd Tentler               www.gerd-tentler.de/tools    |
 | Created: Nov. 21, 2001               Last modified: Mar. 21, 2008 |
 +-------------------------------------------------------------------+
 | This program may be used and hosted free of charge by anyone for  |
 | personal purpose as long as this copyright notice remains intact. |
 |                                                                   |
 | Obtain permission before selling the code for this program or     |
 | hosting this software on a commercial website or redistributing   |
 | this software over the Internet or in any other medium. In all    |
 | cases copyright must remain intact.                               |
 +-------------------------------------------------------------------+
*/
  error_reporting(E_WARNING);
  if(function_exists('session_start')) session_start();

//========================================================================================================
// Set global variables, if they are not registered globally; needs PHP 4.1.0 or higher
//========================================================================================================

  if(isset($_REQUEST['start'])) $start = $_REQUEST['start'];
  if(isset($_REQUEST['showMessage'])) $showMessage = $_REQUEST['showMessage'];
  if(isset($_REQUEST['date_show'])) $date_show = $_REQUEST['date_show'];
  if(isset($_REQUEST['mNr'])) $mNr = $_REQUEST['mNr'];
  if(isset($_REQUEST['tNr'])) $tNr = $_REQUEST['tNr'];

  if(isset($_POST['create'])) $create = $_POST['create'];
  if(isset($_REQUEST['new'])) $new = $_REQUEST['new'];
  if(isset($_REQUEST['delete'])) $delete = $_REQUEST['delete'];
  if(isset($_REQUEST['admin'])) $admin = $_REQUEST['admin'];
  if(isset($_REQUEST['open'])) $open = $_REQUEST['open'];

  if(isset($_POST['tstamp'])) $tstamp = $_POST['tstamp'];
  if(isset($_POST['sfID'])) $sfID = $_POST['sfID'];
  if(isset($_POST['sfName'])) $sfName = $_POST['sfName'];
  if(isset($_POST['sfEMail'])) $sfEMail = $_POST['sfEMail'];
  if(isset($_POST['sfSubject'])) $sfSubject = $_POST['sfSubject'];
  if(isset($_POST['sfText'])) $sfText = $_POST['sfText'];
  if(isset($_POST['sfSignature'])) $sfSignature = $_POST['sfSignature'];

  if(isset($_SERVER['PHP_SELF'])) $PHP_SELF = $_SERVER['PHP_SELF'];
  if(isset($_SERVER['HTTP_HOST'])) $HTTP_HOST = $_SERVER['HTTP_HOST'];
  if(isset($_SERVER['HTTP_USER_AGENT'])) $HTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];
  if(isset($_SERVER['HTTP_REFERER'])) $HTTP_REFERER = $_SERVER['HTTP_REFERER'];

//========================================================================================================
// Make sure that the following variables are integers
//========================================================================================================

  $mNr = (int) $mNr;
  $tNr = (int) $tNr;
  $thr = (int) $thr;
  $new = (int) $new;
  $delete = (int) $delete;
  $start = (int) $start;
  $showMessage = (int) $showMessage;
  $open = (int) $open;

//========================================================================================================
// Check variable contents
//========================================================================================================

  if(!ereg('^[0-9]{4}-[0-9]{2}-[0-9]{2}$', $date_show)) $date_show = '';

//========================================================================================================
// Includes
//========================================================================================================

  if($HTTP_HOST == 'localhost' || $HTTP_HOST == '127.0.0.1' || ereg('^192\.168\.0\.[0-9]+$', $HTTP_HOST)) {
    include('config_local.inc.php');
  }
  else {
    include('config_main.inc.php');
  }

  if(!isset($language)) $language = 'en';
  include("languages/lang_$language.inc");
  include('smilies.inc');
  include('funclib.inc');

//========================================================================================================
// Set session variables (admin login and message ID); needs PHP 4.1.0 or higher
//========================================================================================================

  if($admin && $admin == $adminPass) $_SESSION['sf_admin'] = $admin;

  if(!$new && $enableIDs && !$_SESSION['msgID']) {
    srand((double) microtime() * 1000000);
    $_SESSION['msgID'] = md5(uniqid(rand()));
  }

//========================================================================================================
// Functions
//========================================================================================================

  function showTreeItem($item, $level, $hilight) {
    global $message, $wordLength, $forumWidth, $adminPass,
           $forum, $tNr, $mNr, $lines, $open,
           $tbl_name, $fld_id, $fld_date, $fld_subject, $fld_name, $fld_email;

    $id = $item['id'];
    $pid = $item['pid'];
    $thread = $item['thread'];

    $sql = "SELECT $fld_subject, $fld_date, $fld_name, $fld_email FROM $tbl_name WHERE $fld_id='$id'";
    $row = mysql_fetch_row(mysql_query($sql));
    $subject = format($row[0], $wordLength, $forumWidth - 105, true);
    $date = $row[1];
    $name = format($row[2], $wordLength, $forumWidth - 105, true);
    $email = preg_match('/^[a-z0-9\.\_\-]+@[a-z0-9������\.\-]+\.[a-z]{2,4}$/i', $row[3]) ? $row[3] : '';

    if(($item['open'] || $tNr) && $item['replies']) {
      $img = 'minus.gif';
      $link = ($open || $mNr) ? '' : "$forum&showMessage=$pid";
    }
    else if($item['replies']) {
      $img = 'plus.gif';
      $link = ($open || $mNr) ? '' : "$forum&showMessage=$id";
    }
    else {
      $img = 'point.gif';
      $link = '';
    }
?>
    <table border="0" cellspacing="0" cellpadding="0"><tr valign="top">
    <? for($i = 0; $i < $level; $i++): ?><td width="20"><img src="<? echo $lines[$i]; ?>" width="20" height="17"></td><? endfor; ?>
    <td width="20"><img src="line.gif" width="20" height="3"><? if($link): ?><a href="<? echo $link; ?>"><? endif; ?><img src="<? echo $img; ?>" border="0" width="20" height="9"><? if($link): ?></a><? endif; ?><img src="<? echo $lines[$i]; ?>" width="20" height="5"></td>
    <td nowrap>
<?
    if($id == $hilight) echo '<span class="cssCurThread">';
    else echo '<a href="' . "$forum&mNr=$id&tNr=$thread" . '" class="cssLink1">';
    echo $subject;
    echo ($id == $hilight) ? '</span>' : '</a>';
?>
    <span class="cssDetails"><i>(<? echo $date; ?><? if($name): ?>, <? if($email): ?><a href="mailto:<? echo $email; ?>" class="cssLink2"><? endif; ?><? echo $name; ?></a><? endif; ?>)</i></span>
    <? if($item['replies']): ?><span class="cssDate">[<? echo $item['replies']; ?>] [<? echo $item['update']; ?>]</span><? endif; ?>
    <? if($_SESSION['sf_admin'] && $_SESSION['sf_admin'] == $adminPass): ?><a href="javascript:confirmDelete(<? echo "$id, " . (($mNr == $id) ? $pid : $mNr) . ", $thread"; ?>)"><img src="delete.gif" border="0" width="10" height="10" alt="<? echo $message[8]; ?>"></a><? endif; ?>
    </td>
    </tr></table>
<?
    if(($item['open'] || $tNr) && $item['replies']) {
      $cnt = 0;

      while(list($key, $sub_item) = each($item['items'])) {
        $last = (++$cnt >= count($item['items']));
        $lines[$level+1] = $last ? 'blank.gif' : 'line.gif';
        showTreeItem($sub_item, $level+1, $hilight);
      }
    }
  }

  function showThreads($tree, $hilight) {
    global $threadsPerPage, $mNr, $tNr, $lines;

    $total = count($tree);
    $cnt = 0;

    while(list($thread, $items) = each($tree)) {
      $cnt++;
      $last = ($tNr || $cnt == $total);
      $lines[0] = $last ? 'blank.gif' : 'line.gif';

      while(list($key, $item) = each($items)) {
        showTreeItem($item, 0, $hilight);
      }
    }
  }

  function updateTreeItem(&$item, $date, $sub = false) {
    global $open;

    $item['replies']++;
    if($date > $item['date']) $item['update'] = $date;
    if($open || $sub) $item['open'] = true;
  }

  function addTreeItem(&$tree, $item) {
    $id = $item['id'];
    $pid = $item['pid'];

    if($pid && $tree[$pid]) {
      $tree[$pid]['items'][$id] = $item;
      updateTreeItem($tree[$pid], $item['date'], $item['open']);
      return true;
    }

    reset($tree);
    while(list($key, $val) = each($tree)) {
      if(addTreeItem($tree[$key]['items'], $item)) {
        updateTreeItem($tree[$key], $item['date'], $item['open']);
        return true;
      }
    }
    return false;
  }

  function buildThreads($open = 0) {
    global $threadsPerPage, $start, $date_show, $date_show_from, $tNr, $total_threads,
           $tbl_name, $fld_id, $fld_date, $fld_thread, $fld_pid;

    $errors = $cond = '';
    $threads = $keys = array();

    if($tNr) {
      $cond = "$fld_thread='$tNr'";
    }
    else if($date_show_from && $date_show) {
      $cond = "$fld_date>='$date_show_from' AND $fld_date<='$date_show'";
    }

    $sql = "SELECT $fld_thread, MAX($fld_id) FROM $tbl_name";
    if($cond) $sql .= " WHERE $cond";
    $sql .= " GROUP BY $fld_thread";
    $result = mysql_query($sql);

    if($result) while($row = mysql_fetch_row($result)) {
      $threads[$row[0]] = $tNr ? array() : $row[1];
    }
    else sql_error();

    $total_threads = count($threads);

    if(!$tNr) {
      arsort($threads);
      $cnt = 0;
      while(list($thread, $val) = each($threads)) {
        if($cnt < $start || $cnt >= $start + $threadsPerPage) {
          unset($threads[$thread]);
        }
        else {
          $threads[$thread] = array();
          $keys[] = $thread;
        }
        $cnt++;
      }
      $cond = "$fld_thread IN (" . join(',', $keys) . ")";
    }

    $sql = "SELECT $fld_id, $fld_pid, $fld_thread, $fld_date FROM $tbl_name";
    if($cond) $sql .= " WHERE $cond";
    $sql .= " ORDER BY $fld_id";
    $result = mysql_query($sql);

    if($result) while($row = mysql_fetch_row($result)) {
      $item = array(
        'id'      => $row[0],
        'pid'     => $row[1],
        'thread'  => $row[2],
        'date'    => $row[3],
        'open'    => ($open == $row[0]) ? true : false,
        'replies' => 0,
        'update'  => $row[3],
        'items'   => array()
      );

      if(!addTreeItem($threads[$row[2]], $item)) {
        if(!$row[1]) $threads[$row[2]][$row[0]] = $item;
        else $errors .= 'ERROR: could not add ID ' . $item['id'] . ' to tree<br>';
      }
    }
    else sql_error();

    if($errors) echo '<div class="cssError">' . $errors . '</div>';
    return $threads;
  }

//========================================================================================================
// Main
//========================================================================================================

  if($open == '') $open = $openThreads ? 1 : 0;
  $forum = $forum_all = "$PHP_SELF?open=$open";
  $forum_date = "$forum&date_show";
  if($start >= 0) $forum .= "&start=$start";
  if($date_show) $forum .= "&date_show=$date_show";
  $forum_open = preg_replace('/open=\d+/', 'open=1', $forum);
  $forum_close = preg_replace('/open=\d+/', 'open=0', $forum);

  header('Cache-control: private, no-cache, must-revalidate');
  header('Expires: Sat, 01 Jan 2000 00:00:00 GMT');
  header('Date: ' . gmdate('D, d M Y H:i:s') . ' GMT');
  header('Pragma: no-cache');
?>
<html>
<head>
<meta http-equiv="expires" content="0">
<meta name="robots" content="noindex, nofollow">
<title><? echo $forumTitle; ?></title>
<script language="JavaScript"> <!--
function login() {
  var pass = prompt("<? echo $message[16]; ?>", "");
  if(pass) document.location.href = '<? echo $forum; ?>&admin=' + pass;
}
<?
  if($_SESSION['sf_admin'] && $_SESSION['sf_admin'] == $adminPass) {
?>
    function confirmDelete(del, mNr, tNr) {
      var check = confirm("<? echo $message[14]; ?>");
      if(check) {
        var param = '&delete=' + del;
        if(mNr) param += '&mNr=' + mNr + '&tNr=' + tNr;
        document.location.href = '<? echo $forum; ?>' + param;
      }
    }
<?
  }
?>

function insertSmilie(txt) {
  var el = document.f1.sfText;
  if(!el.value) el.value = txt + ' ';
  else el.value += ((el.value.charAt(el.value.length-1) == ' ') ? '' : ' ') + txt + ' ';
  el.focus();
}

function countdown(cnt) {
  var obj = 0;

  if(document.getElementById) obj = document.getElementById('divSubmit');
  else if(document.all) obj = document.all.divSubmit;

  if(obj) {
    if(cnt < 1) {
      obj.innerHTML = '[ <a href="javascript:document.f1.submit()" class="cssLink3"><? echo $message[9]; ?></a> ]';
    }
    else {
      obj.innerHTML = '[ ' + cnt + ' ]';
      cnt--;
      setTimeout('countdown(' + cnt + ')', 1000);
    }
  }
}
//--> </script>
<link rel="stylesheet" href="forum.css" type="text/css">
</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" width="<? echo $forumWidth; ?>" align="<? echo $forumAlign; ?>"><tr>
<td>
<div class="cssTitle"><? echo $forumTitle; ?></div>
<?
  if($admin && !$_SESSION['sf_admin']) {
?>
    <div class="cssError"><? echo $message[17]; ?></div>
<?
  }

  if(db_open($db_server, $db_user, $db_pass, $db_name)) {

    if(!mysql_query("SELECT 1 FROM $tbl_name LIMIT 1")) {
      $table_exists = false;

      if($create == 'yes') {
        $sql = "CREATE TABLE $tbl_name ( " .
               "$fld_id INT(10) NOT NULL auto_increment, " .
               "$fld_date DATE DEFAULT '0000-00-00' NOT NULL, " .
               "$fld_name VARCHAR(50), " .
               "$fld_email VARCHAR(75), " .
               "$fld_subject VARCHAR(50) NOT NULL, " .
               "$fld_thread INT(10) NOT NULL, " .
               "$fld_pid INT(10), " .
               "$fld_text TEXT NOT NULL, " .
               "PRIMARY KEY ($fld_id))";
        if(!mysql_query($sql)) sql_error();
        else $table_exists = true;
      }
      else if($create == 'no') echo '<div class="cssError">Operation cancelled.</div>';
      else {
        echo '<div class="cssContent">';
        echo '<form name="f1" action="' . $PHP_SELF . '" method="post" style="margin:0px">';
        echo "<b>Table $tbl_name doesn't exist. Create it now?</b> &nbsp; ";
        echo '<input type="radio" name="create" value="yes" onClick="document.f1.submit()">yes &nbsp; ';
        echo '<input type="radio" name="create" value="no" onClick="document.f1.submit()">no';
        echo '</form></div>';
      }
    }
    else $table_exists = true;

    if($table_exists) {

      if($_SESSION['sf_admin'] && $_SESSION['sf_admin'] == $adminPass && $delete) {
        $result = mysql_query("SELECT $fld_thread, $fld_pid FROM $tbl_name WHERE $fld_id='$delete'");

        if($result && mysql_num_rows($result)) {
          $row = mysql_fetch_row($result);
          $thr = $row[0];
          $pid = $row[1];
          if(!mysql_query("DELETE FROM $tbl_name WHERE $fld_thread='$thr' AND $fld_pid>=$delete")) sql_error();
          if(!mysql_query("DELETE FROM $tbl_name WHERE $fld_id='$delete'")) sql_error();
          $date_show = 0;

          if($pid) {
            $tNr = $thr;
            $mNr = $pid;
          }
        }
      }

      if($new) {

        if($tstamp) {

          if(!$sfSubject || !$sfText) $error = $message[1];
          else if(checkSpam($sfID, $tstamp, $sfName, $sfEMail, $sfSubject, $sfText, $sfSignature)) $error = $message[21];

          if($error) {
?>
            <div class="cssError"><? echo $error; ?></div>
<?
          }
          else {
            if($enableSignature) $_SESSION['secCode'] = rand(100000, 999999);
            if($sfEMail && !$sfName) $sfName = str_replace('@', ' @ ', $sfEMail);
            if(!$tNr) $tNr = mysql_result(mysql_query("SELECT MAX($fld_thread) FROM $tbl_name"), $fld_thread) + 1;

            if(!get_magic_quotes_gpc()) {
              $sfName = addslashes($sfName);
              $sfEMail = addslashes($sfEMail);
              $sfSubject = addslashes($sfSubject);
              $sfText = addslashes($sfText);
            }
            $date = date('Y-m-d');

            $sql = "INSERT INTO $tbl_name ($fld_date, $fld_name, $fld_email, $fld_subject, $fld_thread, $fld_pid, $fld_text) ";
            $sql .= "VALUES ('$date', '$sfName', '$sfEMail', '$sfSubject', '$tNr', '$mNr', '$sfText')";

            if(!mysql_query($sql)) sql_error();
            else {
              $nr = mysql_result(mysql_query("SELECT MAX($fld_id) FROM $tbl_name"), $fld_id);
              $link = "$forum&mNr=$nr&tNr=$tNr";
?>
              <script language="JavaScript"> <!--
              document.location.href = "<? echo $link; ?>";
              //--> </script>
<?
            }
          }
        }

        if($mNr && !$sfSubject) {
          $sfSubject = 'RE: ' . mysql_result(mysql_query("SELECT $fld_subject FROM $tbl_name WHERE $fld_id='$mNr'"), $fld_subject);
          $sfSubject = replaceNonos($sfSubject);
        }

        $link = $forum . ($mNr ? "&mNr=$mNr&tNr=$tNr" : '');

        if(get_magic_quotes_gpc()) {
          $sfName = stripslashes($sfName);
          $sfEMail = stripslashes($sfEMail);
          $sfSubject = stripslashes($sfSubject);
          $sfText = stripslashes($sfText);
        }

        $sfName = str_replace('"', '&quot;', $sfName);
        $sfEMail = str_replace('"', '&quot;', $sfEMail);
        $sfSubject = str_replace('"', '&quot;', $sfSubject);
?>
        <div class="cssContent">
        <table border="0" cellspacing="0" cellpadding="4" width="<? echo $forumWidth - 15; ?>"><tr valign="top">
        <td>
          <table border="0" cellspacing="0" cellpadding="2">
          <form name="f1" action="<? echo $PHP_SELF; ?>" method="post">
          <input type="hidden" name="new" value="2">
          <input type="hidden" name="start" value="<? echo $start; ?>">
          <input type="hidden" name="tNr" value="<? echo $tNr; ?>">
          <input type="hidden" name="mNr" value="<? echo $mNr; ?>">
          <input type="hidden" name="date_show" value="<? echo $date_show; ?>">
          <input type="hidden" name="open" value="<? echo $open; ?>">
          <input type="hidden" name="sfID" value="<? echo $_SESSION['msgID']; ?>">
          <input type="hidden" name="tstamp" value="<? echo time(); ?>">
          <tr>
          <td nowrap><b><? echo $message[4]; ?>:</b></td>
          <td><input type="text" name="sfName" size="50" maxlength="50" class="cssForm" value="<? echo $sfName; ?>"></td>
          </tr><tr>
          <td nowrap><b><? echo $message[5]; ?>:</b></td>
          <td><input type="text" name="sfEMail" size="50" maxlength="75" class="cssForm" value="<? echo $sfEMail; ?>"></td>
          </tr><tr>
          <td nowrap><font color="#D00000"><b><? echo $message[6]; ?>:</b></font></td>
          <td><input type="text" name="sfSubject" size="50" maxlength="50" class="cssForm" value="<? echo $sfSubject; ?>"></td>
          </tr><tr valign="top">
          <td nowrap><font color="#D00000"><b><? echo $message[7]; ?>:</b></font></td>
          <td><textarea name="sfText" cols="48" rows="10" wrap="virtual" class="cssForm"><? echo $sfText; ?></textarea></td>
<?
          if($enableSignature) {
?>
            </tr><tr>
            <td nowrap><font color="#D00000"><b><? echo $message[20]; ?>:</b></font></td>
            <td>
              <table border="0" cellspacing="0" cellpadding="0"><tr>
              <td><input type="text" name="sfSignature" size="6" maxlength="6" class="cssForm"></td>
              <td>&nbsp;<b>&laquo;</b>&nbsp;</td>
              <td><img src="seccode.php" width="71" height="21"></td>
              </tr></table>
            </td>
<?
          }
?>
          </tr>
          </form>
          </table>
        </td>
        <td class="cssSmall" align="right">
        <b>HTML:</b> <img src="check<? echo $allowHTML ? '1' : '2'; ?>.gif" width="11" height="14" align="absmiddle"><br>
        <b>URLs:</b> <img src="check<? echo $allowURLs ? '1' : '2'; ?>.gif" width="11" height="14" align="absmiddle"><br>
        <b>UBBs:</b> <img src="check<? echo $allowUBBs ? '1' : '2'; ?>.gif" width="11" height="14" align="absmiddle"><br>
        <br>
<?
        $cnt = 0;
        reset($sm);

        while(list($code, $img) = each($sm)) {
          if($img != $img_old) {
?>
            <a href="javascript:insertSmilie('<? echo $code; ?>')" title="<? echo $code; ?>">
            &nbsp;<img src="smilies/<? echo $img; ?>" border="0" width="15" height="15">&nbsp;</a>
<?
            $cnt++;
            if(!($cnt % 4)) echo '<br><br>';
          }
          $img_old = $img;
        }
?>
        <br></td>
        </tr></table>
        </div>
        <div class="cssNavigation">
        [ <a href="<? echo $link; ?>" class="cssLink3"><? echo $message[2]; ?></a> ] &nbsp;
        <span id="divSubmit">[ <a href="javascript:document.f1.submit()" class="cssLink3"><? echo $message[9]; ?></a> ]</span>
        </div>
        <script language="JavaScript"> <!--
        countdown(5);
        //--> </script>
<?
      }
      else if($mNr) {

        $row = mysql_fetch_array(mysql_query("SELECT * FROM $tbl_name WHERE $fld_id='$mNr'"));
        $date = $row[$fld_date];
        $name = format($row[$fld_name], $wordLength, $forumWidth - 105, true);
        $email = $row[$fld_email];
        $subject = format($row[$fld_subject], $wordLength, $forumWidth - 105, true);
        $text = format($row[$fld_text], $wordLength, $forumWidth - 105, false);
?>
        <div class="cssContent">
        <table border="0"><tr>
        <td nowrap><b><? echo $message[3]; ?>:</b></td>
        <td><? echo $date; ?></td>
        </tr><tr>
        <td nowrap><b><? echo $message[10]; ?>:</b></td>
        <td>
        <? echo $name ? $name : '???'; ?>
        <? if($email) echo '(' . format($email, $wordLength, $forumWidth - 105, true) . ')'; ?>
        </td>
        </tr><tr>
        <td colspan="2" height="5"</td>
        </tr><tr valign="top">
        <td nowrap><b><? echo $message[6]; ?>:</b></td>
        <td nowrap><b><? echo $subject; ?></b></td>
        </tr><tr>
        <td colspan="2" height="5"</td>
        </tr><tr valign="top">
        <td nowrap><b><? echo $message[7]; ?>:</b></td>
        <td><? echo $text; ?></td>
        </tr></table>
        </div>
        <div class="cssThreads">
<?
        echo '<b>Thread:</b><br><br>';
        $showMessage = $mNr;

        $threads = buildThreads();
        showThreads($threads, $mNr);
?>
        </div>
        <div class="cssNavigation">
        [ <a href="<? echo $forum; ?>" class="cssLink3"><? echo $message[2]; ?></a> ] &nbsp;
        [ <a href="<? echo "$forum&new=1&mNr=$mNr&tNr=$tNr"; ?>" class="cssLink3"><? echo $message[11]; ?></a> ]
        </div>
<?
      }
      else {

        if($autoDelete) {
          $date = date('Y-m-d', time() - $autoDelete * 24 * 60 * 60);
          $result = mysql_query("SELECT $fld_thread, MAX($fld_date) FROM $tbl_name GROUP BY $fld_thread");

          if($result) while($row = mysql_fetch_row($result)) {
            if($row[1] <= $date) {
              if(!mysql_query("DELETE FROM $tbl_name WHERE $fld_thread='$row[0]'")) sql_error();
            }
          }
          else sql_error();
        }

        if($date_show) {
          $d = explode('-', $date_show);
          $tstamp_to = mktime(0, 0, 0, $d[1], $d[2], $d[0]);
          $tstamp_from = $tstamp_to - $latestPostsDays * 24 * 3600;
          $date_show_from = date('Y-m-d', $tstamp_from);
        }
        else {
          $result = mysql_query("SELECT MAX($fld_date) AS date FROM $tbl_name");
          $latest = mysql_result($result, 'date');
        }

        $threads = buildThreads($showMessage);

        if($start >= $total_threads) $start = $total_threads - $threadsPerPage;
        if($start < 0) $start = 0;

        $forum2 = preg_replace('/&start=\d+/', '', $forum);
?>
        <div class="cssContent">
        <table border="0" cellspacing="0" cellpadding="0" width="<? echo $forumWidth - 15; ?>"><tr>
        <td><b><? echo ($date_show ? $message[18] . " ($date_show_from - $date_show)" : $message[19]); ?>:</b></td>
        <td align="right">
        <? if($start): ?>
        <a href="<? echo "$forum2&start=" . ($start - $threadsPerPage); ?>"><? echo "&laquo; $message[12]"; ?></a>
        <? endif; ?>
        <? if($start + $threadsPerPage < $total_threads): if($start) echo ' | '; ?>
        <a href="<? echo "$forum2&start=" . ($start + $threadsPerPage); ?>"><? echo "$message[13] &raquo;"; ?></a>
        <? endif; ?>
        </td>
        </tr></table>
        </div>
        <div class="cssThreads">
<?
        showThreads($threads, $mNr);
?>
        </div>
        <div class="cssNavigation">
        [ <a href="<? echo "$forum&new=1"; ?>" class="cssLink3"><? echo $message[0]; ?></a> ]
        <? if($date_show): ?>
        &nbsp; [ <a href="<? echo $forum_all; ?>" class="cssLink3"><? echo $message[19]; ?></a> ]
        <? else: ?>
        &nbsp; [ <a href="<? echo "$forum_date=$latest"; ?>" class="cssLink3"><? echo $message[18]; ?></a> ]
        <? endif; ?>
        <? if($open): ?>
        &nbsp; [ <a href="<? echo $forum_close; ?>" class="cssLink3">&minus;</a> ]
        <? else: ?>
        &nbsp; [ <a href="<? echo $forum_open; ?>" class="cssLink3">+</a> ]
        <? endif; ?>
        <? if(!$_SESSION['sf_admin']): ?>
        &nbsp; [ <a href="javascript:login()" class="cssLink3"><? echo $message[15]; ?></a> ]
        <? endif; ?>
        </div>
<?
      }
    }
    mysql_close();
  }
?>
</td>
</tr></table>
</body>
</html>
