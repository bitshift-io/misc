======================================================================================================================
INTRODUCTION
======================================================================================================================
This is a very simple PHP message board with integrated spam protection. It supports smilies / multiple languages
and requires a MySQL database.

======================================================================================================================
LICENSE
======================================================================================================================
This script is freeware for non-commercial use. If you like it, please feel free to make a donation!
However, if you intend to use the script in a commercial project, please donate at least EUR 10.
You can make a donation on my website: http://www.gerd-tentler.de/tools/forum/.

======================================================================================================================
USAGE
======================================================================================================================
Extract the files to your webserver and adapt the configuration (config_main.inc.php, forum.css) to your needs.
The start page is forum.php. Simple Forum will try to create the necessary MySQL table automatically, according
to your database settings in config_main.inc.php.

Since version 1.14, there are two configuration files:

- config_main.inc.php is the main configuration file; you must adapt it to your needs
- config_local.inc.php is only used for offline testing; if you don't test offline, you don't need to modify
  this file

NOTE: You should change the administrator password (config_main.inc.php) for security reasons! Also please note
that you need PHP 4.1.0 or higher with session support if you want to log in as administrator.

In the configuration you can enable or disable the input of HTML tags, URLs, and UBB codes. If you enable UBB
or HTML, but disable URLs, the UBB code [url] and the HTML tag <A> won't work. In order to make them work, you
have to enable URLs, too. However, you should consider that this will make your forum more vulnerable to
malicious URLs.

NOTE: For security reasons, it's strongly recommended to disable HTML. By default, HTML and URLs are disabled.
Enable them on your own risk!

You can also enable or disable unique message IDs and message signatures which help to protect your forum from
spam bots. Please note that like the admin feature these features need PHP 4.1.0 or higher with session support.

Additionally you can enable or disable referer checks and user agent checks against spam bots. Please note that
the Apache variables HTTP_REFERER and HTTP_USER_AGENT are required for these checks, so if you are using another
HTTP server, make sure that these variables are supported or disable these checks.

======================================================================================================================
Source code + example available at http://www.gerd-tentler.de/tools/forum/.
======================================================================================================================
