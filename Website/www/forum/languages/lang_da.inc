<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by Jonas Horsner

  $message[0]  = "Ny Tr�d";
  $message[1]  = "P�kr�vede felter: 'Emne' og 'Besked'!";
  $message[2]  = "Tilbage";
  $message[3]  = "Dato";
  $message[4]  = "Navn";
  $message[5]  = "Email";
  $message[6]  = "Emne";
  $message[7]  = "Besked";
  $message[8]  = "Slet";
  $message[9]  = "Send";
  $message[10] = "Fra";
  $message[11] = "Besvar";
  $message[12] = "Tidligere";
  $message[13] = "N�ste";
  $message[14] = "Slet denne tr�d permanent?";
  $message[15] = "Admin";
  $message[16] = "Indtast adgangskode:";
  $message[17] = "Forkert adgangskode!";
  $message[18] = "Sidste tr�d";
  $message[19] = "Alle tr�de";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
