<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by E. Tetti

  $message[0]  = "Nuova entrata";
  $message[1]  = "Richiede i campi: 'Oggetto' e 'Messaggio'!";
  $message[2]  = "Indietro";
  $message[3]  = "Data";
  $message[4]  = "Nome";
  $message[5]  = "EMail";
  $message[6]  = "Oggetto";
  $message[7]  = "Messaggio";
  $message[8]  = "Cancella";
  $message[9]  = "Invia";
  $message[10] = "Da";
  $message[11] = "Rispondi";
  $message[12] = "Precedente";
  $message[13] = "Successivo";
  $message[14] = "Cancellare questa discussione irreversibilmente?";
  $message[15] = "Admin";
  $message[16] = "Inserisci la password:";
  $message[17] = "Password errata!";
  $message[18] = "Nuove messaggi";
  $message[19] = "Tutte le discussioni";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
