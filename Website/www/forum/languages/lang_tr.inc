<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by A. Ipek

  $message[0]  = "Yeni giri&#351;";
  $message[1]  = "Zorunlu bilgiler: 'Konu' ve 'Mesaj'!";
  $message[2]  = "Geri";
  $message[3]  = "Tarih";
  $message[4]  = "&#304;sim";
  $message[5]  = "EMail";
  $message[6]  = "Konu";
  $message[7]  = "Mesaj";
  $message[8]  = "Sil";
  $message[9]  = "G�nder";
  $message[10] = "Kimden";
  $message[11] = "Cevapla";
  $message[12] = "�nceki";
  $message[13] = "Sonraki";
  $message[14] = "B�t�n tart&#305;&#351;ma tamamen silinsin mi?";
  $message[15] = "Y�netici";
  $message[16] = "&#350;ifre gir:";
  $message[17] = "Yanl&#305;&#351; &#351;ifre!";
  $message[18] = "Son tart&#305;&#351;malar";
  $message[19] = "B�t�n tart&#305;&#351;malar";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
