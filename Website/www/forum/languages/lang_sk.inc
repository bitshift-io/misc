<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by ivusko

  $message[0]  = "Nová téma";
  $message[1]  = "Povinné polia: 'Subjekt' a 'Správa'!";
  $message[2]  = "Späť";
  $message[3]  = "Dátum";
  $message[4]  = "Meno";
  $message[5]  = "Email";
  $message[6]  = "Subjekt";
  $message[7]  = "Príspevok";
  $message[8]  = "Vymazať";
  $message[9]  = "Odoslať";
  $message[10] = "Od";
  $message[11] = "Odpovedať";
  $message[12] = "Predchadzajúci";
  $message[13] = "Nasledujúci";
  $message[14] = "Naozaj chcete vymazať túto tému?";
  $message[15] = "Admin";
  $message[16] = "Vložte heslo:";
  $message[17] = "Nesprávne heslo!";
  $message[18] = "Posledné príspevky";
  $message[19] = "Všetky témy";
  $message[20] = "Kód";
  $message[21] = "Prosíme bez spamu!";
?>
