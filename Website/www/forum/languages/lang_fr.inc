<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by Fabrice

  $message[0]  = "Nouvelle entr�";
  $message[1]  = "Champs requis: 'Sujet' et 'Message'!";
  $message[2]  = "Retour";
  $message[3]  = "Date";
  $message[4]  = "Nom";
  $message[5]  = "EMail";
  $message[6]  = "Sujet";
  $message[7]  = "Message";
  $message[8]  = "Supprimer";
  $message[9]  = "Envoyer";
  $message[10] = "De";
  $message[11] = "R�pondre";
  $message[12] = "Pr�c�dent";
  $message[13] = "Suivant";
  $message[14] = "Supprimer cette discussion d�finitivement?";
  $message[15] = "Admin";
  $message[16] = "Saisissez le mot de passe:";
  $message[17] = "Mauvais mot de passe!";
  $message[18] = "Derni�res messages";
  $message[19] = "Toutes les discussions";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
