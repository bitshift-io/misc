<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by Chris

  $message[0]  = "Nowy wpis";
  $message[1]  = "Wymagane pola: 'Temat' i 'Wiadomo��'!";
  $message[2]  = "Wstecz";
  $message[3]  = "Data";
  $message[4]  = "Imi�";
  $message[5]  = "EMail";
  $message[6]  = "Temat";
  $message[7]  = "Wiadomo��";
  $message[8]  = "Skasuj";
  $message[9]  = "Wy�lij";
  $message[10] = "Od";
  $message[11] = "Odpowied�";
  $message[12] = "Poprzednie";
  $message[13] = "Nast�pne";
  $message[14] = "Skasowa� ca�y w�tek?";
  $message[15] = "Admin";
  $message[16] = "Wprowad� has�o:";
  $message[17] = "B��dne has�o!";
  $message[18] = "Najnowsze w�tki";
  $message[19] = "Wszystkie w�tki";
  $message[20] = "Kod";
  $message[21] = "BEZ SPAMU PROSZ�!";
?>
