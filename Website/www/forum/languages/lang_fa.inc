<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by shilan

  $message[0]  = "پیام جدید";
  $message[1]  = "فیلدهای موضوع و متن پیام اجباری است";
  $message[2]  = "بازگشت";
  $message[3]  = "تاریخ";
  $message[4]  = "نام";
  $message[5]  = "ایمیل";
  $message[6]  = "موضوع";
  $message[7]  = "متن پیام";
  $message[8]  = "حذف";
  $message[9]  = "ثبت";
  $message[10] = "از طرف";
  $message[11] = "پاسخ";
  $message[12] = "قبلی";
  $message[13] = "بعدی";
  $message[14] = "آیا می خواهید این پیام را برای همیشه حذف کنید";
  $message[15] = "Admin";
  $message[16] = ":رمز عبور را وارد کنید";
  $message[17] = "!رمز عبور اشتباه است";
  $message[18] = "آخرین پاسخها";
  $message[19] = "همه پیامها";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
