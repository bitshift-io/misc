<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by Paulino Pardo

  $message[0]  = "Mensaje nuevo";
  $message[1]  = "�Campos requeridos: 'Asunto' e 'Mensaje'!";
  $message[2]  = "Atr�s";
  $message[3]  = "Fecha";
  $message[4]  = "Nombre";
  $message[5]  = "EMail";
  $message[6]  = "Asunto";
  $message[7]  = "Mensaje";
  $message[8]  = "Borrar";
  $message[9]  = "Enviar";
  $message[10] = "De";
  $message[11] = "Responder";
  $message[12] = "Anterior";
  $message[13] = "Siguiente";
  $message[14] = "�Borrar el hilo irreversiblemente?";
  $message[15] = "Administraci�n";
  $message[16] = "Contrase�a:";
  $message[17] = "�Contrase�a err�nea!";
  $message[18] = "�ltimos mensajes";
  $message[19] = "Todos los hilos";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
