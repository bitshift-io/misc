<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by Conny Wikman

  $message[0]  = "UUsi ketju";
  $message[1]  = "Vaaditut kent�t: 'Otsikko' ja 'Viesti'!";
  $message[2]  = "Takaisin";
  $message[3]  = "P�iv�m��r�";
  $message[4]  = "Nimi";
  $message[5]  = "S�hk�posti";
  $message[6]  = "Otsikko";
  $message[7]  = "Viesti";
  $message[8]  = "Kumoa";
  $message[9]  = "L�het�";
  $message[10] = "L�hett�j�";
  $message[11] = "Vastaa";
  $message[12] = "Edellinen";
  $message[13] = "Seuraava";
  $message[14] = "Kumoa koko ketju?";
  $message[15] = "Valvooja";
  $message[16] = "Anna salasana:";
  $message[17] = "V��r� salasana!";
  $message[18] = "Viimeisin ketju";
  $message[19] = "Kaikki ketjut";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
