<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  $message[0]  = "Neuer Eintrag";
  $message[1]  = "Bitte f�llen Sie mind. die Felder 'Thema' und 'Nachricht' aus!";
  $message[2]  = "Zur�ck";
  $message[3]  = "Datum";
  $message[4]  = "Name";
  $message[5]  = "EMail";
  $message[6]  = "Thema";
  $message[7]  = "Nachricht";
  $message[8]  = "L�schen";
  $message[9]  = "Absenden";
  $message[10] = "Von";
  $message[11] = "Antworten";
  $message[12] = "Zur�ck";
  $message[13] = "Weiter";
  $message[14] = "Diesen Thread unwiderruflich l�schen?";
  $message[15] = "Admin";
  $message[16] = "Kennwort eingeben:";
  $message[17] = "Falsches Kennwort!";
  $message[18] = "Neue Beitr�ge";
  $message[19] = "Alle Threads";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
