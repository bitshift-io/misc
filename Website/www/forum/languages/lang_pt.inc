<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by Fernando Baptista

  $message[0]  = "Nova mensagem";
  $message[1]  = "Campos requeridos: 'Assunto' e 'Mensagem'!";
  $message[2]  = "Voltar";
  $message[3]  = "Fecha";
  $message[4]  = "Nome";
  $message[5]  = "EMail";
  $message[6]  = "Assunto";
  $message[7]  = "Mensagem";
  $message[8]  = "Apagar";
  $message[9]  = "Submeter";
  $message[10] = "De";
  $message[11] = "Responder";
  $message[12] = "Anterior";
  $message[13] = "Seguinte";
  $message[14] = "Apagar irreversivelmente?";
  $message[15] = "Administra��o";
  $message[16] = "Palavra-passe:";
  $message[17] = "Palavra-passe errada!";
  $message[18] = "�ltimas mensagens";
  $message[19] = "Todas as mensagens";
  $message[20] = "C�digo";
  $message[21] = "NO SPAM PLEASE!";
?>
