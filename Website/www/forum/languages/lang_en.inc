<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  $message[0]  = "New entry";
  $message[1]  = "Required fields: 'Subject' and 'Message'!";
  $message[2]  = "Back";
  $message[3]  = "Date";
  $message[4]  = "Name";
  $message[5]  = "EMail";
  $message[6]  = "Subject";
  $message[7]  = "Message";
  $message[8]  = "Delete";
  $message[9]  = "Submit";
  $message[10] = "From";
  $message[11] = "Reply";
  $message[12] = "Previous";
  $message[13] = "Next";
  $message[14] = "Delete this thread irreversibly?";
  $message[15] = "Admin";
  $message[16] = "Enter password:";
  $message[17] = "Wrong password!";
  $message[18] = "Latest posts";
  $message[19] = "All threads";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
