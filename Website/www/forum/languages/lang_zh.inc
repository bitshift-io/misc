<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by Leo Yang

  $message[0]  = "添加留言";
  $message[1]  = "注意：主题和内容是必填内容！";
  $message[2]  = "返回";
  $message[3]  = "时间";
  $message[4]  = "昵称";
  $message[5]  = "邮件";
  $message[6]  = "主题";
  $message[7]  = "内容";
  $message[8]  = "删除";
  $message[9]  = "发布";
  $message[10] = "来自";
  $message[11] = "回复";
  $message[12] = "上一页";
  $message[13] = "下一页";
  $message[14] = "永久删除？";
  $message[15] = "管理留言";
  $message[16] = "输入密码：";
  $message[17] = "密码错误！";
  $message[18] = "最新留言";
  $message[19] = "所有留言";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
