<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by Stefan Knorn

  $message[0]  = "Nytt inl�gg";
  $message[1]  = "Obligatoriska f�lt: '�mne' och 'Meddelande'!";
  $message[2]  = "Tillbaka";
  $message[3]  = "Datum";
  $message[4]  = "Namn";
  $message[5]  = "EMail";
  $message[6]  = "�mne";
  $message[7]  = "Meddelande";
  $message[8]  = "Ta bort";
  $message[9]  = "Skicka";
  $message[10] = "Fr�n";
  $message[11] = "Svara";
  $message[12] = "F�reg�ende";
  $message[13] = "N�sta";
  $message[14] = "Radera denna tr�d f�r alltid?";
  $message[15] = "Admin";
  $message[16] = "Ange l�senord:";
  $message[17] = "Fel l�senord!";
  $message[18] = "Senaste meddelanden";
  $message[19] = "Alla tr�dar";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
