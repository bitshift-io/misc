<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

  // translated by Simon

  $message[0]  = "Nieuw bericht";
  $message[1]  = "Verplichte velden: 'Onderwerp' en 'Mededeling'!";
  $message[2]  = "Terug";
  $message[3]  = "Datum";
  $message[4]  = "Naam";
  $message[5]  = "EMail";
  $message[6]  = "Onderwerp";
  $message[7]  = "Mededeling";
  $message[8]  = "Verwijderen";
  $message[9]  = "Versturen";
  $message[10] = "Van";
  $message[11] = "Antwoord";
  $message[12] = "Vorige pagina";
  $message[13] = "Volgende pagina";
  $message[14] = "Verwijder dit artikel definitief?";
  $message[15] = "Webmaster";
  $message[16] = "Geef paswoord:";
  $message[17] = "Verkeerd paswoord!";
  $message[18] = "Nieuwe mededelingen";
  $message[19] = "Alle artikelen in dit forum";
  $message[20] = "Code";
  $message[21] = "NO SPAM PLEASE!";
?>
