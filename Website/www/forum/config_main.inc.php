<?
/*********************************************************************************************************
 This code is part of the Simple Forum software (www.gerd-tentler.de/tools/forum), copyright by
 Gerd Tentler. Obtain permission before selling this code or hosting it on a commercial website or
 redistributing it over the Internet or in any other medium. In all cases copyright must remain intact.
*********************************************************************************************************/

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// This is the MAIN configuration file
//////////////////////////////////////////////////////////////////////////////////////////////////////////

//========================================================================================================
// Database settings
//========================================================================================================

  $db_server = "localhost";    // server name
  $db_user = "root";           // user name
  $db_pass = "";               // user password
  $db_name = "blackcar_blackcarbon";               // database name

  // don't change unless you know what you're doing:
  $tbl_name = "Forum";         // table name
  $fld_id = "ID";              // field name: ID
  $fld_date = "Date";          // field name: date
  $fld_name = "Name";          // field name: name
  $fld_email = "EMail";        // field name: e-mail
  $fld_subject = "Subject";    // field name: subject
  $fld_thread = "Thread";      // field name: thread
  $fld_pid = "PID";            // field name: PID
  $fld_text = "Text";          // field name: text

//========================================================================================================
// Other settings
//========================================================================================================

  // forum language: da, de, en, es, fa, fi, fr, hu, it, nl, pl, pt, ru, sk, sv, tr, zh
  $language = "en";

  // forum width (pixels)
  $forumWidth = 650;

  // forum align (left / center / right)
  $forumAlign = "center";

  // forum title
  $forumTitle = "Simple Forum";

  // threads per page
  $threadsPerPage = 15;

  // start with all threads open (true = yes, false = no)
  $openThreads = false;

  // number of days for latest posts view
  $latestPostsDays = 7;

  // maximum word length (0 = no limit)
  // NOTE: should be 0 for non-European languages (Asian, Arabic, etc.)
  $wordLength = 50;

  // maximum text length (0 = no limit)
  $textLength = 3000;

  // delete threads with no response after .. days (0 = never delete)
  $autoDelete = 180;

  // administrator password (can delete threads; needs PHP >= 4.1.0)
  $adminPass = "SiFoAd1";

  // allow HTML tags (true = yes, false = no)
  $allowHTML = false;

  // allow URLs (true = yes, false = no)
  $allowURLs = false;

  // allow UBB codes (true = yes, false = no)
  $allowUBBs = true;

  // enable message IDs against spam bots (true = yes, false = no; needs PHP >= 4.1.0)
  $enableIDs = true;

  // enable message signature against spam bots (true = yes, false = no; needs PHP >= 4.1.0)
  $enableSignature = true;

  // enable link check against spam bots (true = yes, false = no)
  // NOTE: works only if $allowURLs and/or $allowHTML is false
  $enableLinkCheck = true;

  // enable referer check against spam bots (true = yes, false = no)
  // NOTE: if enabled, browsers that don't transmit referer will be mistaken for spam bots
  $enableRefererCheck = true;

  // enable user agent check against spam bots (true = yes, false = no)
  // NOTE: if enabled, some browsers might be mistaken for spam bots
  $enableAgentCheck = true;

  // valid user agents; don't change unless you know what you're doing
  $agents = array("Mozilla", "Opera", "Lynx", "Mosaic", "amaya", "WebExplorer", "IBrowse", "iCab");

  // bad words
  $nonos = array("fuck", "asshole");

//========================================================================================================
?>
