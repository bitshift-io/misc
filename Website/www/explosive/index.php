<?php

	//
	// This is a standard page,
	// rename it and it will execute the htm equivelent
	//
	include_once('../../include/template.inc.php');
	include_once('../../include/login.inc.php');
	
	// get currently executing page, and replace .php with .shtml
	$phpPage = $_SERVER['PHP_SELF'];
	$htmPage = str_replace('.php', '.shtml', $phpPage);
	$htmPage = substr(strrchr($htmPage, '/'), 1);
		
	$tBody = & new PageTemplate($htmPage);
	$tBody->Set('admin', IsAdmin());
	$tBody->Set('login', IsLoggedIn());
	$tBody->Set('cssOverride', '/explosive/resource/stylesheet.css');
  
	if (IsLoggedIn())
	{
		$login = GetLogin();
		$tBody->Set('username', $login->GetUserName());
	}
	
	echo $tBody->Fetch();
	
?>