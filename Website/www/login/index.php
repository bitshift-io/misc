<?php

  include_once('../../include/global.inc.php');
	include_once('../../include/dbase.inc.php');
	include_once('../../include/template.inc.php');
	include_once('../../include/login.inc.php');
	
  global $GB;
  
  class MD5
	{
		function Generate($string)
		{
			return md5($string);
		}
		
		function Verify($md5, $string)
		{
			return $this->Generate($string) == $md5;
		}
	}
  
  
  // need to chack again name in sandbox mode as there is no https
	global $gSandbox;
	if ($gSandbox && $_SERVER['SERVER_NAME'] == 'bronson.dyndns.org')
		$isSecure = true;
	else
		$isSecure = ($_SERVER['HTTPS'] == 'on');
  
	switch ($_GET['action'])
  {
  default:
    {
      $tLogin = & new PageTemplate('index.shtml');
      $redirect = urldecode($_GET['redirect']);
      $tLogin->Set('redirect', urlencode($redirect));
      
      echo $tLogin->Fetch();	
    }
    break;
  
  case 'login':
    {
      $password = $_POST['password'];
      $username = $_POST['username'];
      
      $message = '';
      
      if (!isset($password))
        $message = 'Please enter a password';
      
      $passwordHash = md5($password);
      
      $tLogin = & new PageTemplate('index.shtml');
      $redirect = urldecode($_GET['redirect']);
      $tLogin->Set('redirect', urlencode($redirect));
      
      if ($message)
      {
        $tLogin->Set('loginMessage', $message);
		    echo $tLogin->Fetch();
		    return;
      }
      
      $db = new DBase();
	    $db->Connect();

	    $arrColumnNames = array('*');
	    $result = $db->SelectWhere('user', $arrColumnNames, "username='$username' AND password='$passwordHash'");
      
      // login failed
    	if ($db->GetNumRows($result) != 1)
	    {		
        $tLogin->Set('loginMessage', 'Login failed, please check you have entered the correct user name and password');
		    echo $tLogin->Fetch();
        $db->Disconnect();
        return;
      }
			
      $row = $db->GetNextRow($result);
      $id = $row['id'];
		  $admin = $row['admin'];
      $email = $row['email'];
      
      // check we are verified
		  $result = $db->SelectWhere('user', $arrColumnNames, "email='$email' AND password='$passwordHash' AND verified='1'");
		  if ($db->GetNumRows($result) != 1)
		  {			
			  $tLogin->Set('loginMessage', 'Your email has not been verified. <a href="?action=sendverifyemail&id='.$id .'&email='. $email .'">Click here</a> to resend the verification email.');
			  echo $tLogin->Fetch();	
        $db->Disconnect();
        return;
		  }
      
      // set up the cookies, and redirect to main website,
		  // does the user need to be notified of login then redirection?
		  LogIn($row['username'], $id, $admin, 0);
      
      $tRedirect = & new PageTemplate();
      $redirect = urldecode($_GET['redirect']);
      
      $db->Disconnect();
      
		  if (isset($redirect) && strlen($redirect) > 0)
			  $tRedirect->Redirect($redirect); // assume this site is secure
		  else
			  $tRedirect->Redirect($GB['url']);     
    }
    break;
    
  case 'logout':
    {
      LogOut();
      
      $tRedirect = & new PageTemplate();
      $redirect = urldecode($_GET['redirect']);
      if (isset($redirect) && strlen($redirect) > 0)
			  $tRedirect->Redirect($redirect); // assume this site is secure
		  else
			  $tRedirect->Redirect($GB['url']);  
    }
    break;
    
  case 'register':
    {
      $username = $_POST['username'];
      $email = $_POST['email'];
	    $password = $_POST['password'];
	    $confirmpassword = $_POST['confirmpassword'];
  
      $tLogin = & new PageTemplate('index.shtml');
      
      $message = '';
    		
	    if (!isset($email) || strlen($email) < 3)
	    {
		    $message .= 'Please enter your email address (at least 3 characters)<br>';
	    }
	    else
	    {
		    if (!strrchr($email, '@') || !strrchr($email, '.'))
			    $message .= 'Your email is invalid<br>';
	    }
      
      if (!isset($password) || strlen($password) < 5)
		    $message .= 'Please enter a password of at least 5 characters and/or numbers<br>';
	    else if ($password != $confirmpassword)
		    $message .= 'Your password was not the same as your confirm password<br>';
  
      if (strlen($message) > 0)
	    {
        $tLogin->Set('registerMessage', $message);
		    echo $tLogin->Fetch();
		    return;
	    }
	
	    $db = new DBase();
	    $db->Connect();
      
      // check if username is already in use
      $arrColumnNames = array('*');
	    $result = $db->SelectWhere('user', $arrColumnNames, "username='$username'");
      if ($db->GetNumRows($result) > 0)
      {
        $tLogin->Set('registerMessage', 'The user name is already in use, please use another');
        echo $tLogin->Fetch();
        $db->Disconnect();
        return;
      }
      
      // check if email already used
      $arrColumnNames = array('*');
	    $result = $db->SelectWhere('user', $arrColumnNames, "email='$email'");
      if ($db->GetNumRows($result) > 0)
      {
        $tLogin->Set('registerMessage', 'The email address is already in use, please use another email');
        echo $tLogin->Fetch();
        $db->Disconnect();
        return;
      }
      
      $hashPassword = md5($password);
      $arrValuePairs = array('username' => $username, 'email' => $email, 'password' => $hashPassword);
		  $id = $db->Insert('user', $arrValuePairs);
      
      // now proceed to send a verification email
    }
    
  case 'sendverifyemail':
    {
      // if we did not fall thgouh from the above case
      // we were linked here from a external page
      // so need to get some valid info
      if (!isset($id) || !isset($email))
      {
        $id = $_GET['id'];
        $email = $_GET['email'];
      }
      
      // send verification email again
      // generate verify URL
      $md5 = new MD5();
		  $emailMd5 = $md5->Generate($email);
      $verifyLink = $GB['url'] . "login/?action=verify";
      $verifyLink .= "&verify=" . $emailMd5;
      $verifyLink .= "&id=" . $id;
      
      $tEmail = & new PageTemplate('activateemail.shtml');
      $tEmail->Set('email', $email);
      $tEmail->Set('verifyLink', $verifyLink);
      $msg = $tEmail->Fetch();
            
      if (!SendMail($email, 'Registration Verification', $msg, $GB['noReply']))
      {
        $tMessage = & new PageTemplate('error.shtml');
        $tMessage->Set('message', "Failed to send an email to " . $email . ". Please contact " . $GB['email'] . " if this is a valid email address");
        echo $tMessage->Fetch();
        $db->Disconnect();
        return;
      }
      
      $tMessage = & new PageTemplate('emailsent.shtml');
      $tMessage->Set('email', $email);
      echo $tMessage->fetch();
      
      $db->Disconnect();
    }
    break;
    
  case 'verify':
    {
      $verify = $_GET['verify'];
	    $id = $_GET['id'];
      
      $db = new DBase();
	    $db->Connect();
      
      $arrColumnNames = array('*');
		  $result = $db->SelectWhere('user', $arrColumnNames, 'id=' . $id);
      
      if ($db->GetNumRows($result) != 1)
      {
        $tMessage = & new PageTemplate('error.shtml');
        $tMessage->Set('message', "Trying to verify a user that does not exist. Please contact " . $GB['email']);
        echo $tMessage->Fetch();
        $db->Disconnect();
        return;
      }
      
      $row = $db->GetNextRow($result);
      $email = $row['email'];
      
      $md5 = new MD5();
		  if (!$md5->Verify($verify, $email))
		  {
        $tMessage = & new PageTemplate('error.shtml');
        $tMessage->Set('message', "Error verifying email. Please contact " . $GB['email']);
        echo $tMessage->Fetch();
        $db->Disconnect();
        return;
      }
      
		  $arrSetNames = array('verified' => '1');
		  $db->Update('user', $arrSetNames, 'id=' . $id);
      
		  $tMessage = & new PageTemplate('emailverified.shtml');
      $tMessage->Set('email', $email);
      echo $tMessage->fetch();
    
      $db->Disconnect();
    }
    break;
  }
	
?>