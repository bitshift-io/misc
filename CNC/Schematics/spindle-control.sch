EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:relays
LIBS:my-library
LIBS:spindle-control-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R1
U 1 1 5826C2EA
P 5900 2300
F 0 "R1" V 5980 2300 50  0000 C CNN
F 1 "47R" V 5900 2300 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 5830 2300 50  0001 C CNN
F 3 "" H 5900 2300 50  0000 C CNN
	1    5900 2300
	0    1    1    0   
$EndComp
$Comp
L CONN_01X02 Arduino1
U 1 1 5826C395
P 5350 2350
F 0 "Arduino1" H 5350 2500 50  0000 C CNN
F 1 "CONN_01X02" V 5450 2350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 5350 2350 50  0001 C CNN
F 3 "" H 5350 2350 50  0000 C CNN
	1    5350 2350
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X02 P2
U 1 1 5826C71E
P 6700 3750
F 0 "P2" H 6700 3900 50  0000 C CNN
F 1 "CONN_01X02" V 6800 3750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 6700 3750 50  0001 C CNN
F 3 "" H 6700 3750 50  0000 C CNN
	1    6700 3750
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 5826C765
P 5300 3750
F 0 "P1" H 5300 3900 50  0000 C CNN
F 1 "CONN_01X02" V 5400 3750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 5300 3750 50  0001 C CNN
F 3 "" H 5300 3750 50  0000 C CNN
	1    5300 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	5550 2300 5750 2300
$Comp
L 79c-P-1C K2
U 1 1 5826DA1D
P 6000 3400
F 0 "K2" H 6550 3350 60  0000 C CNN
F 1 "79c-P-1C" H 6700 3450 60  0000 C CNN
F 2 "my-footprints:793-P-1C" H 6000 3200 60  0001 C CNN
F 3 "" H 6000 3200 60  0001 C CNN
	1    6000 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2300 6300 2300
Wire Wire Line
	6300 2300 6300 2950
Wire Wire Line
	5700 2950 5700 2400
Wire Wire Line
	5700 2400 5550 2400
Wire Wire Line
	6300 3700 6500 3700
Wire Wire Line
	6500 3800 6500 3900
Wire Wire Line
	6500 3900 5500 3900
Wire Wire Line
	5500 3900 5500 3800
Wire Wire Line
	5500 3700 5500 3550
Wire Wire Line
	5500 3550 5700 3550
$Comp
L D D1
U 1 1 58270014
P 6000 2600
F 0 "D1" H 6000 2700 50  0000 C CNN
F 1 "D" H 6000 2500 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-41_SOD81_Horizontal_RM10" H 6000 2600 50  0001 C CNN
F 3 "" H 6000 2600 50  0000 C CNN
	1    6000 2600
	-1   0    0    1   
$EndComp
Wire Wire Line
	5850 2600 5700 2600
Connection ~ 5700 2600
Wire Wire Line
	6150 2600 6300 2600
Connection ~ 6300 2600
$EndSCHEMATC
