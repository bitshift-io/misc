/*
 Stepper Motor Control - one revolution

 This program drives a unipolar or bipolar stepper motor.
 The motor is attached to digital pins 8 - 11 of the Arduino.

 The motor should revolve one revolution in one direction, then
 one revolution in the other direction.


 Created 11 Mar. 2007
 Modified 30 Nov. 2009
 by Tom Igoe

 */

#include <Stepper.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3); // RX, TX


int ledpin = 13; // LED connected to pin 13 (on-board LED) 

//char val = 0; // variable to receive data from the serial port

const int goLeftBtnPin = 22; // adc7
const int goRightBtnPin = 19; // adc6

const int stepsPerRevolution = 48;  // change this to fit the number of steps per revolution
// for your motor

// initialize the stepper library on pins 9 through 12:
Stepper myStepper1(stepsPerRevolution, 8, 9, 10, 11);
Stepper myStepper2(stepsPerRevolution, 4, 5, 6, 7);
Stepper myStepper3(stepsPerRevolution, A0, A1, A2, A3);

void setup() {
  // set the speed rpm:
  myStepper1.setSpeed(100);
  myStepper2.setSpeed(100);
  myStepper3.setSpeed(100);
  
  Serial.begin(9600);
  Serial.println("Goodnight moon!");
  mySerial.begin(9600);
  mySerial.println("Hello, world?");
  

  pinMode(goLeftBtnPin, INPUT_PULLUP); 
  pinMode(goRightBtnPin, INPUT_PULLUP); 

  pinMode(ledpin, OUTPUT);  // on-board LED as OUTPUT
  digitalWrite(ledpin, HIGH);  // turn ON the LED
}

void loop() {

#if 0
    //myStepper1.step(-stepsPerRevolution);
    //myStepper2.step(-stepsPerRevolution);
    //myStepper3.step(-stepsPerRevolution);
      
    if (digitalRead(goLeftBtnPin) == LOW)
    {
      myStepper1.step(-stepsPerRevolution);
      myStepper2.step(-stepsPerRevolution);
      myStepper3.step(-stepsPerRevolution);
    }
    
    if (digitalRead(goRightBtnPin) == LOW)
    {
      myStepper1.step(stepsPerRevolution);
      myStepper2.step(stepsPerRevolution);
      myStepper3.step(stepsPerRevolution);
    }
    
    digitalWrite(8, LOW);
    digitalWrite(9, LOW);
    digitalWrite(10, LOW);
    digitalWrite(11, LOW);
    
    /*
    delay(10000);
    
    myStepper.step(48 * 10);
    
    digitalWrite(8, LOW);
    digitalWrite(9, LOW);
    digitalWrite(10, LOW);
    digitalWrite(11, LOW);
    delay(10000);*/
#endif 
   
    
#if 0
  int revs = 100;
  
  // step one revolution  in one direction:
  Serial.println("clockwise");
  myStepper.step(stepsPerRevolution * revs);
  delay(500);

  // step one revolution in the other direction:
  Serial.println("counterclockwise");
  myStepper.step(-stepsPerRevolution * revs);
  delay(500);
#endif


#if 0
  if( Serial.available() )       // if data is available to read
  {
    val = Serial.read();         // read it and store it in 'val'
    Serial.print("received a value: ");
    Serial.print(val);
  }
  if( val != 0 )               // if 'H' was received
  {
    digitalWrite(ledpin, HIGH);  // turn ON the LED
  }
  else 
  { 
    digitalWrite(ledpin, LOW);   // otherwise turn it OFF
  }
#endif

#if 0
  if (Serial.available()) 
  {
    char c=Serial.read();
    Serial.write(c);
    mySerial.write(c);
  }
  if (mySerial.available())
  {
    char d=mySerial.read();
    mySerial.write(d);
    Serial.write(d);
  }
 #endif



  // http://www.hansen-motor.com/connection-diagrams.php


  // https://www.arduino.cc/en/Reference/PortManipulation
  const unsigned int del = 5000;


  /*
  digitalWrite(8, HIGH); 
  digitalWrite(9, LOW);
  digitalWrite(10, HIGH);
  digitalWrite(11, LOW);
  */
  PORTB = B00000110; // sets digital pins 9 & 10 HIGH
  //delayMilliseconds(del);
  delay(del);

  /*
  digitalWrite(8, HIGH); 
  digitalWrite(9, LOW);
  digitalWrite(10, LOW);
  digitalWrite(11, HIGH);
  */
  PORTB = B00001010; // sets digital pins 9 & 11 HIGH
  //delayMicroseconds(del);
  delay(del);

  /*
  digitalWrite(8, LOW); 
  digitalWrite(9, HIGH);
  digitalWrite(10, LOW);
  digitalWrite(11, HIGH);
  */
  PORTB = B00001001; // sets digital pins 8 & 11 HIGH
  //delayMicroseconds(del);
  delay(del);

  /*
  digitalWrite(8, LOW); 
  digitalWrite(9, HIGH);
  digitalWrite(10, HIGH);
  digitalWrite(11, LOW);
  */
  PORTB = B00000101; // sets digital pins 8 & 10 HIGH
  //delayMicroseconds(del);
  delay(del);

}
