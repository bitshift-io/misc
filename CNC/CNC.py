#!/usr/bin/env python3

#
# 2016 - Fabian Mathews
#
# Read saves and replays IR commands
#
# apt-get install python3
# apt-get install python3-pip
# pip3 install setuptools
# pip3 install pyserial

# https://pypi.python.org/pypi/PyBluez

import os
import serial
import json
import sys
import time
import bluetooth

target_name = 'Aquaponics'
target_address = None


port = 1
sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )


if __name__ == '__main__':  
	print("Running CNC")
	
	
	# should also just be able to communicate via com port
	
	# connect via bluetooth serial
	bluetooth = None
	try:
		if os.name == 'nt':
			bluetooth = serial.Serial('COM8', 9600)
		else:
			bluetooth = serial.Serial('/dev/ttyUSB0', 9600)
	except e:
		print(e)
		sys.exit()
		
	# connect via the usb cable
	usb = None
	try:
		if os.name == 'nt':
			usb = serial.Serial('COM9', 9600)
		else:
			usb = serial.Serial('/dev/ttyUSB0', 9600)
	except e:
		print(e)
		
	while True:
	
		if bluetooth != None:
			line = bluetooth.readline()
			line = line.strip()
			
			if len(line) >= 0:				
				try:
					line = line.decode()
					print("BT: " + line)
				except:
					pass
					
				ba = bytes("Test command from python\n", encoding="ascii")
				print("Sending:")
				print(ba)
				bluetooth.write(ba)
				bluetooth.flush()
					
		if usb != None:
			line = usb.readline()
			line = line.strip()
			
			if len(line) >= 0:				
				try:
					line = line.decode()
					print("USB: " + line)
				except:
					pass
			
	
'''
	print("Searching for bluetooth device")
	nearby_devices = bluetooth.discover_devices()
	
	for bdaddr in nearby_devices:
		if target_name == bluetooth.lookup_name( bdaddr ):
			target_address = bdaddr
			break
			
	if target_address is not None:
		print("Found target bluetooth device with address " + target_address)
	else:
		print("Could not find target bluetooth device nearby")
		sys.exit()
		
		
	sock.connect((target_address, port))
	print("Connected")
	
	sock.settimeout(1.0)
	sock.send("Hello")
	print('Sent : Hello')
	
	time.sleep(2)
	
	data = sock.recv(1024)
	#data = data.decode()
	print("received [%s]" % data)
	
	time.sleep(10)
	
	sock.send("Low")
	print('Sent : Low')
	
	data = sock.recv(1024)
	print("received [%s]" % data)
	
	sock.close()
'''
