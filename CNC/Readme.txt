
For PCB design 
http://kicad-pcb.org


Arduino G-Code firmware
https://github.com/grbl/grbl

I have patched in to grbl H-bridge stepper driver support.
I should commit this back to grbl repo.

to flash on the arduino, open 
grbl-master\grbl\examples\grblUpload

and flash, if arduino is newly installed you will need to install the the grbl library in:
grbl-master\grbl

once running on ther arduino, you need to set the baud speed to 115200 and set "Carriage return"
