
// https://www.arduino.cc/en/Reference/PortManipulation

//#define DEBUG

#define MIN_STEP_POS 0
#define MAX_STEP_POS 3

int stepCode[] = {
  B0101,
  B0110,
  B1010,
  B1001,
};

struct StepperData
{
  const char* desc;
  
  // from the in-port, we use 2 bits - 1. direction, and 2. step
  uint8_t   *inPort;
  uint8_t   inPortBitShift;

  // for the outport, we use 4 bits - a step code
  uint8_t   *outPort;
  uint8_t   outPortBitShift;


  uint8_t   prevStepValue;
  char      stepCodeIndex;
};

#define STEPPER_COUNT 1

StepperData g_stepperData[] =
{
  {"x", &PINB, 4, &PORTB, 0, 0, 0},  // pin 12, 13 are inputs. pin 8, 9, 10, 11 are outputs
  {"y", &PIND, 2, &PORTD, 4, 0, 0},  // pin 2, 3 are inputs. pin 4, 5, 6, 7 are outputs (+2 to skip pin 0 and 1)
  {"z", &PINC, 0, &PORTC, 2, 0, 0}   // pin A4, A5 are inputs. pin A0, A1, A2, A3 are outputs
};

void setup()
{
  Serial.begin(115200);

  // set port C
  DDRC = B11111100;   // A0, A1 as input pins - the rest as outputs

  // set port B
  DDRB = B11111100;   // 8, 9 as input pins - the rest as outputs
   
  // set port D
  // NOTE: pin 0 and 1 are RX and TX!
  DDRD = B11110000;   // 2, 3 as input pins - the rest as outputs

  // get steppers in first position
  for (int i = 0; i < STEPPER_COUNT; ++i)
  {
    applyStepCodeIndex(g_stepperData[i]);
  }
}

void loop()
{
#ifdef DEBUG
  printBits(PORTB);
  Serial.println();
#endif
  
  for (int i = 0; i < STEPPER_COUNT; ++i)
  {
    checkStepper(g_stepperData[i]);
  }

#ifdef DEBUG
  delay(1000);
#endif
}

void checkStepper(StepperData& stepper)
{  
  // from the input port, get the two bits we need
  uint8_t stepValue = (*stepper.inPort >> stepper.inPortBitShift) & B00000011;

  // extract the 1. direction bit, 2. step bit, and 3. the step bit from the previous time we did a check
  char dir = stepValue & B00000001;
  byte stp = (stepValue >> 1) & B00000001;
  byte prevStp = (stepper.prevStepValue >> 1) & B00000001;

  // back up stepValue
  stepper.prevStepValue = stepValue;
  
  // no request to change direction - there is only a step when we detect a rising edge
  bool risingEdge = (prevStp == 0 && stp == 1);
  if (!risingEdge)
    return;
    
  // at this time, dir is 0 or 1
  // which i need to change to -1 to 1
  // which this line will do:
  dir = (dir * 2) - 1;
  stepper.stepCodeIndex += dir;

  // wrap around position
  if (stepper.stepCodeIndex < MIN_STEP_POS)
    stepper.stepCodeIndex = MAX_STEP_POS;

  if (stepper.stepCodeIndex > MAX_STEP_POS)
    stepper.stepCodeIndex = MIN_STEP_POS;

  applyStepCodeIndex(stepper);
  
#ifdef DEBUG
  // debug
  String str = "Axis: " + String(stepper.desc) + " Dir: " + String((int)dir) + " Step Idx: " + String((byte)stepper.stepCodeIndex);
  Serial.println(str);
#endif
}

void applyStepCodeIndex(StepperData& stepper)
{ 
  // now we set the 4 bits on the port
  // note: for now, this always just does the lower 4 bits
  byte value = stepCode[stepper.stepCodeIndex];

  //printBits(value);
  //Serial.println();
  
  // now set the appropriate bits on the appropriate port
  // this code is effectively doing something like this:
  //      PORTB = (PORTB & ~0x0f) | (value & 0x0f);
  *stepper.outPort = (*stepper.outPort & ~(0x0f << stepper.outPortBitShift)) | (value << stepper.outPortBitShift);
  //*stepper.outPort = (*stepper.outPort & ~0x0f) | (value << stepper.outPortBitShift  & 0x0f);
}

void printBits(byte myByte)
{
  for (byte mask = 0x80; mask; mask >>= 1)
  {
    if (mask  & myByte)
      Serial.print('1');
    else
      Serial.print('0');
  }
}
