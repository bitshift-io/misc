Setting up arduino IDE
-----------------------------
Open File -> Preferences and in the Additional Boards Manager URLs give this url https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json

After this is done open Tools -> Board -> Board Manager
After opening Board Manager scroll down the list where it says "attiny by Davis A. Mellis". Click on that and install it.
After installing now you would be able to see a new entry in the Board menu






http://heliosoph.mit-links.info/arduinoisp-reading-writing-fuses-atmega328p/
http://codeandlife.com/2012/03/21/using-arduino-uno-as-isp/

avr dude is here:
/home/fabian/arduino-1.8.1/hardware/tools/avr/bin/

This will display some info:

avrdude -P /dev/ttyUSB0 -b 19200 -c avrisp -p attiny85 -v


Go here and set the fuse settings you want set:
http://www.engbedded.com/fusecalc/


I selected ATtiny85 and start  with the defaults:
avrdude -P /dev/ttyUSB0 -b 19200 -c avrisp -p attiny85 -v -U lfuse:w:0x62:m -U hfuse:w:0xdf:m -U efuse:w:0xff:m 

That makes sure we can write fuses correctly.


NOW, the dangerous part, these settings have RESET DISABLED!!!! once you run this you can no longer upload code unless you use high voltage programming to reset the fuses!!!!
Make sure you have flashed the tiny stepper driver first!



avrdude -P /dev/ttyUSB0 -b 19200 -c avrisp -p attiny85 -v -U lfuse:w:0x62:m -U hfuse:w:0x5f:m -U efuse:w:0xff:m 



If you want to reset now, use a high voltage fuse reset which is in Electronics/ATTinyFuseReset
