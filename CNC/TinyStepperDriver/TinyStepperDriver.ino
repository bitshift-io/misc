///
// To flash this on to the tiny85 via a Arduino:
// Board: attiny85
// Processor: attiny85
// Clock: Internal 8Mhz
// Programmer: Arduino as ISP



// https://www.arduino.cc/en/Reference/PortManipulation

// Best way to debug this is on a full on arduino nano
// then fix bugs and flash to the attiny85

// 
// using the arduino nano flasher in Arduino:
// Tools > Board: ATiny25/45/85
// Tools > Clock: Internal 8Mhz
// Tools > Processor: ATtiny85
// Tools > Programmer: Arduino as ISP

//#define DEBUG

#define MIN_STEP_POS 0
#define MAX_STEP_POS 3

#define RISING_EDGE_STOP_STEPPERS_COUNT 333333 // about 10 seconds @ 8mhz?


// use these to swap which pin does what
#define STEP_BIT 0
#define DIR_BIT 1

int stepCode[] = {
  B0101,
  B0110,
  B1010,
  B1001,
};

struct StepperData
{
  // from the in-port, we use 2 bits - 1. direction, and 2. step
  uint8_t   *inPort;
  uint8_t   inPortBitShift;

  // for the outport, we use 4 bits - a step code
  uint8_t   *outPort;
  uint8_t   outPortBitShift;

  uint8_t   prevStepValue;
  char      stepCodeIndex;
  uint32_t  risingEdgeTimer; // counter since last rising edge
  byte      value; // value of stepper
};

StepperData g_stepperData = {&PINB, 0, &PORTB, 2, 0, 0, 0, 0}; // pin 0 (step), 1 (direction) are inputs. pin 2, 3, 4, 5 are outputs

void setup()
{
#ifdef DEBUG
  Serial.begin(19200);
  Serial.println("OK");
#endif
  
  // set pin IO
  // pin 0 and 1 are inputs
  // pin 2, 3, 4, 5 are outputs
  DDRB = B11111100;

  // get steppers in off position
  applyStepValue(g_stepperData, 0);
}

void loop()
{  
  checkStepper(g_stepperData);
#ifdef DEBUG
  //delay(1000);
#endif
}

void checkStepper(StepperData& stepper)
{  
  // from the input port, get the two bits we need
  uint8_t stepValue = (*stepper.inPort >> stepper.inPortBitShift) & B00000011;

  // extract the 1. direction bit, 2. step bit, and 3. the step bit from the previous time we did a check
  char dir = (stepValue >> DIR_BIT) & B00000001;
  byte stp = (stepValue >> STEP_BIT) & B00000001;
  byte prevStp = (stepper.prevStepValue >> STEP_BIT) & B00000001;

  // back up stepValue
  stepper.prevStepValue = stepValue;

  // falling edge code doesn't work, doesn't seem to give the steppers any time...
  //
  // falling edge turn off all power
  //bool fallingEdge = (prevStp == 1 && stp == 0);
  //if (fallingEdge)
  //
  // so this method is a counter if exceeded put steppers to rest
  if (stepper.value != 0 && stepper.risingEdgeTimer > RISING_EDGE_STOP_STEPPERS_COUNT)
  {
    #ifdef DEBUG
      // debug
      String str = "Wait time exceeded";
      Serial.println(str);
    #endif

    applyStepValue(stepper, 0);
    return;
  }

  // only increment the timer if the step is low, which indicates possible inactivity
  // if its held high, it wants to hold the motors
  if (stp == 0)
  {
    ++stepper.risingEdgeTimer;
  }
  
  // no request to change direction - there is only a step when we detect a rising edge
  bool risingEdge = (prevStp == 0 && stp == 1);
  if (!risingEdge)
    return;

  stepper.risingEdgeTimer = 0;

#ifdef DEBUG
  // debug
  String str = "Dir: " + String((int)dir) + " Step Idx: " + String((byte)stepper.stepCodeIndex);
  Serial.println(str);
#endif

  // at this time, dir is 0 or 1
  // which i need to change to -1 to 1
  // which this line will do:
  dir = (dir * 2) - 1;
  stepper.stepCodeIndex += dir;

  // wrap around position
  if (stepper.stepCodeIndex < MIN_STEP_POS)
    stepper.stepCodeIndex = MAX_STEP_POS;

  if (stepper.stepCodeIndex > MAX_STEP_POS)
    stepper.stepCodeIndex = MIN_STEP_POS;

  applyStepValue(stepper, stepCode[stepper.stepCodeIndex]);
}

void applyStepValue(StepperData& stepper, byte value)
{
  stepper.value = value;
  
  // now we set the 4 bits on the port
  // note: for now, this always just does the lower 4 bits

#ifdef DEBUG
  printBits(value);
  Serial.println();
#endif

  // now set the appropriate bits on the appropriate port
  // this code is effectively doing something like this:
  //      PORTB = (PORTB & ~0x0f) | (value & 0x0f);
  *stepper.outPort = (*stepper.outPort & ~(0x0f << stepper.outPortBitShift)) | (value << stepper.outPortBitShift);
  //*stepper.outPort = (*stepper.outPort & ~0x0f) | (value << stepper.outPortBitShift  & 0x0f);
}

#ifdef DEBUG
void printBits(byte myByte)
{
  for (byte mask = 0x80; mask; mask >>= 1)
  {
    if (mask  & myByte)
      Serial.print('1');
    else
      Serial.print('0');
  }
}
#endif

