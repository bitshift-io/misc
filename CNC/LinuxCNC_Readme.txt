
once linux cnc is built
edit your .profile in the HOME dir to look like this:

PATH="$HOME/bin:$HOME/.local/bin:$PATH:$HOME/linuxcnc-dev/bin:$HOME/linuxcnc-dev/scripts"
export PYTHONPATH="$HOME/linuxcnc-dev/lib/python"


then open a terminal and run "stepconf"

load up "my-mill.stepconf" which is a setup of this already


run the "test base period jitter"
I get about 114419 so record that and enter it

step time all 4 values: 100000


on the pinout page:

pin 1 - ESTOP out?

pin 2 - X step
pin 3 - X dir

pin 4 - y step
pin 5 - y dir

pin 6 - z step
pin 7 - z step




set the rest to unsed
you can test each axis as you go through, the light will show on the board with a step signal


for each axis, I center the table by making home = 0
and then put table travel
x & y: -250 to 250
z: -10 to 200


microstep: 1 
pitch: 1.25
max speed: 4
accel: 4


now run "linuxcnc"
select under:
My Configurations > my-mill > my-mill




