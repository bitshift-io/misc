#include "StdAfx.h"
#include "Entity.h"
#include "Component.h"

CLASS_BEGIN(Entity)
CLASS_END()

void Entity::Init(EntityMgr* entityMgr)
{
	mEntityMgr = entityMgr;

	vector<Component*>::iterator it;
	for (it = mComponent.begin(); it != mComponent.end(); ++it)
	{
		(*it)->Init(this);
	}
}

void Entity::InsertComponent(Component* component)
{
	mComponent.push_back(component);
}

void Entity::RemoveComponent(Component* component)
{
}

Component* Entity::GetComponent(unsigned int type, Component* previous)
{
	if (type == -1)
		return 0;

	vector<Component*>::iterator it;
	it = mComponent.begin();

	if (previous)
	{
		for (; it != mComponent.end(); ++it)
		{
			if (*it == previous)
			{
				++it;
				break;
			}
		}
	}	

	for (; it != mComponent.end(); ++it)
	{
		Component* component = (*it);
		if (component->GetType() == type)
			return (*it);
	}

	return 0;
}

void Entity::Update()
{
	NotifyObserver(0); //reinterpret_cast<void*>(&Entity::Update));
}