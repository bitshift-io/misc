#ifndef ___STDAFX_H_
#define ___STDAFX_H_

// memmanager
#include "Memory/NoMemory.h"

// STL
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <algorithm>

using namespace std;

#ifdef WIN32
    #include <tchar.h>
#endif

#include <stdlib.h>
#include <malloc.h>
#include <memory.h>

#include "Resource/Resource.h"

// memmanager
#include "Memory/Memory.h"

// libraries

#include "PhysX/PhysicsFactory.h"
#include "PhysX/PhysicsBody.h"
#include "PhysX/PhysicsJoint.h"
#include "PhysX/PhysicsScene.h"
#include "PhysX/PhysicsReport.h"
#include "PhysX/PhysicsActorController.h"
#include "PhysX/PhysicsCloth.h"
#include "PhysX/PhysicsParticleSystem.h"

// more libraries
#include "Render/Window.h"
#include "Render/Device.h"
#include "Render/Font.h"
#include "Render/Renderer.h"
#include "Render/Occluder.h"
#include "Render/Camera.h"
#include "Render/Geometry.h"
#include "Render/Material.h"
#include "Render/ParticleSystem.h"
#include "Render/Shape.h"
#include "Render/SceneObject.h"
#include "Render/Mesh.h"
#include "Render/Spline.h"
#include "Render/RenderTexture.h"
#include "Render/PostEffect.h"
#include "Render/AnimationController.h"

#ifdef OGL
	#include "Render/OGL/OGLRenderFactory.h"
#endif

#ifdef SOFTWARE
	#include "Render/Software/SoftwareRenderFactory.h"
#endif

#include "Flash/Flash.h"

#include "Input/Input.h"

#include "Sound/SoundFactory.h"
#include "Sound/Sound.h"

#include "Template/FrameUpdate.h"
#include "Template/Time.h"

#include "File/Filesystem.h"
#include "File/Package.h"
#include "File/ScriptFile.h"
#include "File/ScriptClass.h"
#include "File/ScriptVariable.h"
#include "File/Log.h"

#include "Template/Singleton.h"

#include "Math/Vector.h"
#include "Math/Matrix.h"
#include "Math/Plane.h"
#include "Math/Line.h"
#include "Math/Quaternion.h"

// game includes
#include "Observer.h"
#include "Component.h"
#include "Entity.h"
#include "CmdLine.h"
#include "GameState.h"
#include "StateMachine.h"
#include "Director.h"

// game states
#include "GameState/InGame.h"
#include "GameState/Menu.h"
#include "GameState/Intro.h"

#define FIRST_PERSON 1 // temp hack for testing purposes

#include "Component/CameraComponent.h"
#include "Component/PhysicsComponent.h"
#include "Component/PlacementComponent.h"
#include "Component/RenderComponent.h"

#include "Manager/World.h"
#include "Manager/EntityMgr.h"
#include "Manager/RenderMgr.h"

#include "TestGame.h"


#include "Game.h"

#endif
