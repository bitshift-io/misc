#include "StdAfx.h"
#include "Observer.h"

void Observer::NotifyObservableChanged(const ObserverEvent* event)
{
	
}



void Observable::NotifyObserver(int eventId)
{
	NotifyObserver(&ObserverEvent(this, eventId));
}

void Observable::NotifyObserver(const ObserverEvent* event)
{
	vector<Observer*>::iterator it;
	for (it = mObserver.begin(); it != mObserver.end(); ++it)
	{
		(*it)->NotifyObservableChanged(event);
	}
}

void Observable::InsertObserver(Observer* observer)
{
	mObserver.push_back(observer);
}

void Observable::RemoveObserver(Observer* observer)
{
	//mObserver.remove(mObserver.finobserver);
}