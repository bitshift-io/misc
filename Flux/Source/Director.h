#ifndef _DIRECTOR_H_
#define _DIRECTOR_H_

enum ControlType
{
	CT_LookX,
	CT_LookY,
	CT_Jump,
	CT_MoveForward,
	CT_MoveBackward,
	CT_StrafeLeft,
	CT_StrafeRight,

	CT_Max,
};

//
// struct used to convert a ControlType to a InputType
//
struct InputControlMap
{
	InputControl	inputControl;
	float			stateModifier;
};

class Director;

//
// gets added to entitys when they are controlled by a director so other components can get access to the director
//
class DirectorComponent : public Component
{
public:

	RTTI_PARENT(Component);

	Director* operator*()
	{
		return mDirector;
	}

	Director* operator->()
	{
		return mDirector;
	}

	Director* mDirector;
};

//
// controls entitys
//
class Director
{
public:

	void				Init();

	void				InsertEntity(Entity* entity);
	void				RemoveEntity(Entity* entity);

	float				GetControlState(ControlType controlType);
	bool				WasPressed(ControlType controlType);

	list<Entity*>		mEntity; // list of entitys being controlled
	InputControlMap		mInputControlMap[CT_Max];

	DirectorComponent	mDirectorComponent;
};

#endif