#ifndef _INGAME_H_
#define _INGAME_H_

class InGame : public GameState
{
public:

	virtual bool			Init()				{ return true; }
	virtual void			Deinit()			{}

	virtual void			StateEnter()		{}
	virtual void			StateExit()			{}

	virtual bool			UpdateGame()		{ return true; }
	virtual bool			UpdateNetwork()		{ return true; }
	virtual bool			UpdatePhysics()		{ return true; }
	virtual void			Render()			{}

	virtual GameStateType	GetType()			{ return GST_InGame; }
};

#endif