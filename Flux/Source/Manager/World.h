#ifndef _WORLD_H_
#define _WORLD_H_

class EntityMgr;
class RenderMgr;

//
// instance of a world
//
class World
{
public:

	void Init();
	void Deinit();
	void Update();
	void Render();

	EntityMgr*	mEntityMgr;
	RenderMgr*	mRenderMgr;

	PhysicsScene*	mPhysicsScene;
};

#endif