#include "StdAfx.h"
#include "EntityMgr.h"

void EntityMgr::Init(World* world)
{
	mWorld = world;
}

void EntityMgr::InitEntity()
{
	EntityIterator it;
	for (it = EntityBegin(); it != EntityEnd(); ++it)
	{
		(*it)->Init(this);
	}
}

void EntityMgr::DeinitEntity()
{/*
	EntityIterator it;
	for (it = EntityBegin(); it != EntityEnd(); ++it)
	{
		(*it)->Deinit();
	}*/
}

void EntityMgr::UpdateEntity()
{
	EntityIterator it;
	for (it = EntityBegin(); it != EntityEnd(); ++it)
	{
		(*it)->Update();
	}
}

void EntityMgr::InsertEntity(Entity* entity)
{
	mEntity.push_back(entity);
}

void EntityMgr::InsertComponent(Component* component)
{
	unsigned int type = component->GetType();
	map<unsigned int, list<Component*> >::iterator componentList = mComponentTable.find(type);

	if (componentList == mComponentTable.end())
	{
		mComponentTable[type] = list<Component*>();
		componentList = mComponentTable.find(type);
	}

	componentList->second.push_back(component);
}

void EntityMgr::RemoveComponent(Component* component)
{
	map<unsigned int, list<Component*> >::iterator componentList = mComponentTable.find(component->GetType());
	if (componentList != mComponentTable.end())
		componentList->second.remove(component);
}
