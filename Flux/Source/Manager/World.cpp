#include "StdAfx.h"
#include "World.h"

void World::Init()
{
	mEntityMgr = new EntityMgr;
	mEntityMgr->Init(this);

	mRenderMgr = new RenderMgr;
	mRenderMgr->Init(this);

	mPhysicsScene = gGame.mPhysicsFactory->CreatePhysicsScene();
	mPhysicsScene->Init(Vector4(0.f, 0.f, 0.f, 0.f));
}

void World::Deinit()
{
}

void World::Update()
{
	mPhysicsScene->Update(gGame.mGameUpdate.GetDeltaTime());
	mEntityMgr->UpdateEntity();
}

void World::Render()
{
	mRenderMgr->Render();
}
