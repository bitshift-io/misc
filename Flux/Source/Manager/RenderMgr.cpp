#include "StdAfx.h"
#include "RenderMgr.h"

void RenderMgr::Init(World* world)
{
	mCamera = (Camera*)gGame.mRenderFactory->Create(Camera::Type);
	mCamera->LookAt(Vector4(0.f, 0.f, 0.f), Vector4(0.f, 0.f, 1.f));
	mCamera->SetPerspective(70.f);

	mWorld = world;
	mRenderer = gGame.mRenderFactory->CreateRenderer();
	mRenderer->SetCamera(mCamera);
}

void RenderMgr::InsertScene(Scene* scene, int renderType)
{
	if (!scene)
		return;

	mRenderer->InsertScene(scene);
}

void RenderMgr::Render()
{
	gGame.mDevice->Begin();
	mRenderer->Render(gGame.mDevice);

	if (mDebugPhysics)
	{
		mCamera->Bind(gGame.mDevice);
		mWorld->mPhysicsScene->Draw(gGame.mDevice);
	}

	gGame.mDevice->End();
}