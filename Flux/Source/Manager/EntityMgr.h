#ifndef _ENTITYMGR_H_
#define _ENTITYMGR_H_

//
// handles entity tracking, broadcasting finding of etc...
//
class EntityMgr
{
public:

	typedef list<Entity*>::iterator		EntityIterator;
	typedef list<Component*>::iterator	ComponentIterator;

	void				Init(World* world);

	void				InitEntity();
	void				DeinitEntity();
	void				UpdateEntity();

	void				InsertEntity(Entity* entity);

	void				InsertComponent(Component* component);
	void				RemoveComponent(Component* component);

	EntityIterator		EntityBegin()								{ return mEntity.begin(); }
	EntityIterator		EntityEnd()									{ return mEntity.end(); }

	ComponentIterator	ComponentBegin(unsigned int type)			{ return mComponentTable[type].begin(); }
	ComponentIterator	ComponentEnd(unsigned int type)				{ return mComponentTable[type].end(); }

	map<unsigned int, list<Component*> >	mComponentTable;

	list<Entity*>				mEntity;
	World*						mWorld;
};

#endif