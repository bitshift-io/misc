#ifndef _CAMERACOMPONENT_H_
#define _CAMERACOMPONENT_H_

class PlacementComponent;
class DirectorComponent;

//
// controls the camera
//
class CameraComponent : public Component
{
public:

	RTTI_PARENT(Component)

	bool	IsActive();
	void	Activate();
};

class FirstPersonCamera : public CameraComponent
{
public:

	RTTI_PARENT(CameraComponent)

	typedef CameraComponent Parent;

	virtual void			Init(Entity* entity);
	virtual void			NotifyObservableChanged(const ObserverEvent* event);
	void					Update();

	float					mMouseX;
	float					mMouseY;

	DirectorComponent*		mDirector;
	PlacementComponent*		mPlacement;
};

class FixedCamera : public CameraComponent
{
public:

	RTTI_PARENT(CameraComponent)

	typedef CameraComponent Parent;

};

#endif