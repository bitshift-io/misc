#include "StdAfx.h"
#include "PhysicsComponent.h"

const float cCameraHeight = 1.8f;
const float cSphereRadius = 1.f;


const float cMoveSpeed = 10.f;
const float cJumpSpeed = 50.f;

void ActorPhysics::Init(Entity* owner)
{
	Parent::Init(owner);
	mEntity->InsertObserver(this);

	mContactThisFrame = false;
	mContactLastFrame = false;

	mPlacement = (PlacementComponent*)mEntity->GetComponent(PlacementComponent::Type);

	mLinearVelocity.SetZero();
	mTransform = Matrix4(MI_Identity);

	mDirector = 0;

	mMouseX = 0.f;
	mMouseY = 0.f;
}

void ActorPhysics::Update()
{
	if (!mDirector)
		return;


	Director* director = mDirector->mDirector; // cuz the deref overloading aint working :-/
	Camera* camera = mEntity->mEntityMgr->mWorld->mRenderMgr->mCamera;

	float mouseX = director->GetControlState(CT_LookX); //-gGame.mInput->GetState(MOUSE_AxisX);
	float mouseY = director->GetControlState(CT_LookY); //-gGame.mInput->GetState(MOUSE_AxisY);

	mMouseX += mouseX;
	mMouseY = Math::Clamp(-Math::PI * 0.5f, mMouseY - mouseY, Math::PI * 0.5f);

	if (mouseX != 0.f)
		int ntohing = 0;

	Matrix4 yaw(MI_Identity);
	yaw.RotateY(mouseX);
	mTransform = yaw.Multiply3(mTransform);

	// jump
	if (director->GetControlState(CT_Jump))
		mLinearVelocity = mTransform.GetYAxis() * cJumpSpeed * gGame.mGameUpdate.GetDeltaTime();


	Vector4 movementVelocity(VI_Zero);

	if (director->GetControlState(CT_MoveForward))
		movementVelocity += mTransform.GetZAxis();

	if (director->GetControlState(CT_MoveBackward))
		movementVelocity -= mTransform.GetZAxis();

	if (director->GetControlState(CT_StrafeLeft))
		movementVelocity -= mTransform.GetXAxis();

	if (director->GetControlState(CT_StrafeRight))
		movementVelocity += mTransform.GetXAxis();

	float mag = movementVelocity.Normalize();
	if (mag > 0.f)
		mLinearVelocity = movementVelocity * cMoveSpeed * gGame.mGameUpdate.GetDeltaTime();
	else
		mLinearVelocity = Vector4(VI_Zero);

	if (gGame.mInput->WasPressed(KEY_v))
	{
		mLinearVelocity = Vector4(0.f, 1.f, 0.f);
	}

	if (gGame.mInput->WasPressed(KEY_r))
	{
		Matrix4 transform(MI_Identity);
		mTransform = transform;
	}

	//if (!mContactLastFrame)
		mLinearVelocity += mTransform.GetYAxis() * -9.8f * gGame.mGameUpdate.GetDeltaTime();

	mContactThisFrame = false;
	mActor->Move(mLinearVelocity);
	mTransform.SetTranslation(mActor->GetTransform().GetTranslation());//GetPosition());

	if (mPlacement)
		mPlacement->SetTransform(mTransform);

	mContactLastFrame = mContactThisFrame;
}

void ActorPhysics::NotifyObservableChanged(const ObserverEvent* event)
{
	if (event->observable == mEntity)
		Update();

	mDirector = (DirectorComponent*)mEntity->GetComponent(DirectorComponent::Type);
}

void ActorPhysics::OnActorContact(PhysicsBody* body, PhysicsActorController* controller, ContactInfoIterator& contactIt)
{
	mLinearVelocity.SetZero();

	mContactThisFrame = true;

	//if (!mContactLastFrame)
	//	mLinearVelocity.SetZero();

	// align to newly hit surface
	Vector4 normal = contactIt->normal;
	Vector4 position = contactIt->position;

	Vector4 xAxis = mTransform.GetZAxis().Cross(normal);
	xAxis.Normalize();

	Vector4 zAxis = normal.Cross(xAxis);
	zAxis.Normalize();

	xAxis = zAxis.Cross(normal);
	xAxis.Normalize();
	
	Matrix4 transform(MI_Identity);
	mTransform.SetYAxis(normal);
	mTransform.SetZAxis(zAxis);
	mTransform.SetXAxis(xAxis);
}