#ifndef _PHYSICSCOMPONENT_H_
#define _PHYSICSCOMPONENT_H_

class PlacementComponent;

class PhysicsComponent : public Component
{
public:

	RTTI_PARENT(Component)
};

//
// controls motion of actors
//
class ActorPhysics : public PhysicsComponent, public ContactReport
{
public:

	RTTI_PARENT(PhysicsComponent)

	typedef PhysicsComponent Parent;

	virtual void		Init(Entity* owner);

	void				Update();
	virtual void		NotifyObservableChanged(const ObserverEvent* event);

	virtual void		OnContactNotify(PhysicsBody* body, PhysicsBody* body2, ContactInfoIterator& contactIt, ContactFlags event) {}
	virtual void		OnActorContact(PhysicsBody* body, PhysicsActorController* controller, ContactInfoIterator& contactIt);

	PhysicsActorController*	mActor;
	Vector4					mLinearVelocity;

	Matrix4					mTransform; // desired transform
	Matrix4					mViewTransform; // first person view transform
	Matrix4					mBodyTransform; //

	int						mContactFrameCount; // num frames in contact with a surface
	bool					mContactThisFrame;
	bool					mContactLastFrame;

	float					mMouseX;
	float					mMouseY;

	DirectorComponent*		mDirector;
	PlacementComponent*		mPlacement;		// its up to us to move the placement around
};

//
// controls motion of dnyamic bodies
//
class DynamicBodyPhysics : public PhysicsComponent
{
public:

	RTTI_PARENT(PhysicsComponent)

	PhysicsBody*	mBody;
};

//
// set up a static body
//
class StaticBodyPhysics : public PhysicsComponent
{
public:

	RTTI_PARENT(PhysicsComponent)

	PhysicsBody*	mBody;
};

#endif
