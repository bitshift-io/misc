#include "StdAfx.h"
#include "RenderComponent.h"

SceneComponent::SceneComponent() :
	mScene(0)
{
}

void SceneComponent::Init(Entity* owner)
{
	Parent::Init(owner);

	if (mScene)
	{
		gGame.mWorld->mRenderMgr->InsertScene(mScene, RT_Default);
	}
	else
	{
		// create a null scene so Set and GetTransform still work, this could be temporary
		mScene = gGame.mRenderFactory->CreateScene();
	}
}

void SceneComponent::SetTransform(const Matrix4& transform)
{
	if (mScene)
		mScene->SetTransform(transform);
}

Matrix4 SceneComponent::GetTransform()
{
	if (!mScene)
		return Matrix4(MI_Identity);

	return mScene->GetTransform();
}