#include "StdAfx.h"
#include "CameraComponent.h"

bool CameraComponent::IsActive()
{
	return mEntity->mEntityMgr->mWorld->mRenderMgr->mActiveComponent == this;
}

void CameraComponent::Activate()
{
	mEntity->mEntityMgr->mWorld->mRenderMgr->mActiveComponent = this;
}




void FirstPersonCamera::Init(Entity* entity)
{
	Parent::Init(entity);
	mEntity->InsertObserver(this);
	mPlacement = (PlacementComponent*)mEntity->GetComponent(PlacementComponent::Type);

	mDirector = 0;
	mMouseX = 0.f;
	mMouseY = 0.f;
}

void FirstPersonCamera::NotifyObservableChanged(const ObserverEvent* event)
{
	if (event->observable == mEntity)
		Update();

	mDirector = (DirectorComponent*)mEntity->GetComponent(DirectorComponent::Type);
}

void FirstPersonCamera::Update()
{
	if (!mDirector || !IsActive())
		return;

	Director* director = mDirector->mDirector; // cuz the deref overloading aint working :-/
	Camera* camera = mEntity->mEntityMgr->mWorld->mRenderMgr->mCamera;

#ifdef FIRST_PERSON
	/*
	float mouseX = director->GetControlState(CT_LookX); //-gGame.mInput->GetState(MOUSE_AxisX);
	float mouseY = director->GetControlState(CT_LookY); //-gGame.mInput->GetState(MOUSE_AxisY);

	mMouseX += mouseX;
	mMouseY = Math::Clamp(-Math::PI * 0.5f, mMouseY - mouseY, Math::PI * 0.5f);

	Matrix4 yaw(MI_Identity);
	yaw.RotateY(mMouseX);

	Matrix4 pitch(MI_Identity);
	pitch.RotateX(mMouseY);

	Matrix4 rotation;
	rotation = pitch.Multiply3(yaw);

	Matrix4 actorTransform = mPlacement->GetTransform();
	Matrix4 cameraOrientation = actorTransform.Multiply3(rotation);

	Matrix4 worldView = cameraOrientation;
	worldView.SetTranslation(actorTransform.GetTranslation());
	camera->LookAt(worldView);
	*/

	camera->LookAt(mPlacement->GetTransform());
#else
	camera->LookAt(Vector4(0.f, 0.f, -15.f), Vector4(0.f, 0.f, 0.f));
#endif
}