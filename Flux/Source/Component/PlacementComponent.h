#ifndef _PLACEMENTCOMPONENT_H_
#define _PLACEMENTCOMPONENT_H_

//
// means an entity can be situated in the world
//
class PlacementComponent : public Component
{
public:

	RTTI_PARENT(Component)

	virtual void		SetTransform(const Matrix4& transform)		= 0;
	virtual Matrix4		GetTransform()								= 0;
};



#endif