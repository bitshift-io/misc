#ifndef _RENDERCOMPONENT_H_
#define _RENDERCOMPONENT_H_

//
// some renderable component
//
class RenderComponent : public PlacementComponent
{
public:

	typedef PlacementComponent Parent;
};

//
// the entity has a scene
//
class SceneComponent : public RenderComponent
{
public:

	typedef RenderComponent Parent;

	SceneComponent();

	virtual void		Init(Entity* owner);

	virtual void		SetTransform(const Matrix4& transform);
	virtual Matrix4		GetTransform();

	Scene*				mScene;
};

#endif