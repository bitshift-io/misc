#ifndef _GAME_H_
#define _GAME_H_

#include "StdAfx.h"

#define gGame Game::GetInstance()

//
// There can only be one game running per application
//
class Game : public Singleton<Game>, public StateMachine<GameState>
{
public:

	bool					Init(const CmdLine& cmdLine);
	void					Deinit();
	bool					UpdateGame();
	bool					UpdateNetwork();
	bool					UpdatePhysics();
	void					Render();
	bool					Run();
	void					Update();

	static int				Main(const char* cmdLine);

	CmdLine					mCmdLine;

	// render
	RenderFactory*			mRenderFactory;
	Window*					mWindow;
	Device*					mDevice;
	Input*					mInput;

#ifdef PHYSICS
	// physics
	PhysicsFactory*			mPhysicsFactory;
#endif

	// sound
	SoundFactory*			mSoundFactory;

#ifdef NETWORK
	// network
	NetworkFactory*			mNetworkFactory;

	MasterServer*			mMasterServer;
#endif

	// frame updates
	FrameUpdate				mGameUpdate;
	FrameUpdate				mNetworkUpdate;
	FrameUpdate				mPhysicsUpdate;

	Font*					mDebugFont;

	TestGame				mTestGame;

	World*					mWorld;

protected:

	bool					InitWindowAndDevice();
};

#endif
