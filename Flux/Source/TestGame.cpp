#include "StdAfx.h"
#include "TestGame.h"

const float cCameraHeight = 1.8f;
const float cSphereRadius = 1.f;

#define FIRST_PERSON 1

int Test(void* data)
{
	return 0;
}

#ifdef  FASTDLGT_MICROSOFT_MFP

#ifdef FASTDLGT_HASINHERITANCE_KEYWORDS
	// For Microsoft and Intel, we want to ensure that it's the most efficient type of MFP 
	// (4 bytes), even when the /vmg option is used. Declaring an empty class 
	// would give 16 byte pointers in this case....
	class __single_inheritance GenericClass;
#endif
	// ...but for Codeplay, an empty class *always* gives 4 byte pointers.
	// If compiled with the /clr option ("managed C++"), the JIT compiler thinks
	// it needs to load GenericClass before it can call any of its functions,
	// (compiles OK but crashes at runtime!), so we need to declare an 
	// empty class to make it happy.
	// Codeplay and VC4 can't cope with the unknown_inheritance case either.
	class GenericClass {};
#else
	class GenericClass;
#endif

void TestGame::Init()
{
	typedef void (GenericClass::*GenericMemFuncType)(); // arbitrary MFP.
	typedef void (*GenericFuncPtr)(); // arbitrary code pointer

	void* t = &Test;
	Test(0);
	//GenericFuncPtr p = &TestGame::Init;
	//GenericMemFuncType m_pFunction = &TestGame::Init;

	mSetTransform = false;
	/*
	mContactFrameCount = -1;
	mContact = false;*/
/*
	mRenderer = gGame.mRenderFactory->CreateRenderer();
	mCamera = (Camera*)gGame.mRenderFactory->Create(Camera::Type);
	mLevel = gGame.mRenderFactory->LoadScene(&ObjectLoadParams(gGame.mDevice, "TestLevel"));

	mRenderer->SetCamera(mCamera);
	mRenderer->InsertScene(mLevel);
*/
	/*
	mCamera->LookAt(Vector4(0.f, 0.f, 0.f), Vector4(0.f, 0.f, 1.f));
	mCamera->SetPerspective(70.f);
	*/
	mTransform = Matrix4(MI_Identity);

	mMouseX = 0.f;
	mMouseY = 0.f;
/*
	mPhysicsScene = gGame.mPhysicsFactory->CreatePhysicsScene();
	mPhysicsScene->Init(Vector4(0.f, 0.f, 0.f, 0.f));
*/

	mDirector = new Director;
	mDirector->Init();


	// new world
	gGame.mWorld = new World;
	gGame.mWorld->Init();	

	mCamera = gGame.mWorld->mRenderMgr->mCamera;

	// set up some manual entitys

	// static level entity
	{
		Entity* entity = new Entity;
		gGame.mWorld->mEntityMgr->InsertEntity(entity);


		SceneComponent* scene = new SceneComponent;
		entity->InsertComponent(scene);
		gGame.mWorld->mEntityMgr->InsertComponent(scene);

		scene->mScene = gGame.mRenderFactory->LoadScene(&ObjectLoadParams(gGame.mDevice, "TestLevel"));
		

		StaticBodyPhysics* physics = new StaticBodyPhysics;
		entity->InsertComponent(physics);
		gGame.mWorld->mEntityMgr->InsertComponent(physics);

		// generate physics for static collision
		Geometry* geometry = ((Mesh*)(*scene->mScene->GetRootNode()->Begin())->GetObject())->GetMeshMat(0).geometry;

		RenderBufferLock indexLock;
		geometry->GetIndexBuffer()->Lock(indexLock);

		RenderBufferLock positionLock;
		geometry->GetVertexFrame().mPosition->Lock(positionLock);

		TriMeshConstructor triMeshConst;
		triMeshConst.mIndex = indexLock.uintData;
		triMeshConst.mPosition = positionLock.vector4Data;
		triMeshConst.mIndexCount = geometry->GetIndexBuffer()->GetCount();
		triMeshConst.mPositionCount = geometry->GetVertexFrame().mPosition->GetCount();

		PhysicsBodyConstructor staticConst;
		staticConst.AddTriMesh(gGame.mPhysicsFactory, triMeshConst);

		physics->mBody = gGame.mPhysicsFactory->CreatePhysicsBody();
		physics->mBody->Init(gGame.mWorld->mPhysicsScene, staticConst);
		physics->mBody->SetTransform((*scene->mScene->GetRootNode()->Begin())->GetWorldTransform());
	}

	// pawn enetiy
	FirstPersonCamera* camera = new FirstPersonCamera;
	{
		Entity* entity = new Entity;
		gGame.mWorld->mEntityMgr->InsertEntity(entity);

		SceneComponent* scene = new SceneComponent;
		entity->InsertComponent(scene);
		gGame.mWorld->mEntityMgr->InsertComponent(scene);

		ActorPhysics* physics = new ActorPhysics;
		entity->InsertComponent(physics);
		gGame.mWorld->mEntityMgr->InsertComponent(physics);

		// generate physics for pawn
		PhysicsBodyConstructor pawnConst;
		pawnConst.AddSphere(cSphereRadius);
		pawnConst.SetDynamicBody(50.f);
/*
		physics->mBody = gGame.mPhysicsFactory->CreatePhysicsBody();
		physics->mBody->Init(gGame.mWorld->mPhysicsScene, pawnConst);

		
		physics->mBody->SetTransform(physicsBodyTransform);
		physics->mBody->Enable();
		physics->mBody->SetContactReport(physics);//this);
*/
		Matrix4 physicsBodyTransform = mTransform;
		physicsBodyTransform.Translate(mTransform.GetYAxis());

		PhysicsActorControllerConstructor actorConstructor;
		actorConstructor.mHeight = 0.001f;
		actorConstructor.mRadius = 0.5f;
		physics->mActor = gGame.mPhysicsFactory->CreatePhsicsActorController();
		physics->mActor->Init(gGame.mPhysicsFactory, gGame.mWorld->mPhysicsScene, actorConstructor);
		//physics->mActor->SetTransform(physicsBodyTransform);
		//physics->mActor->Enable();
		physics->mActor->SetContactReport(physics);//this);


		entity->InsertComponent(camera);
		gGame.mWorld->mEntityMgr->InsertComponent(camera);


		//mPawn = physics->mBody;

		mDirector->InsertEntity(entity); // make the director control this entity
	}

	gGame.mWorld->mEntityMgr->InitEntity();

	int type = PlacementComponent::Type;
	int actype = ActorPhysics::Type;

	// needs to know about its parent :-/
	camera->Activate();
}

void TestGame::Deinit()
{

}

void TestGame::Update()
{
	gGame.mWindow->HandleMessages();
	gGame.mInput->Update();
	gGame.mWorld->Update();
/*
	mPhysicsScene->Update(gGame.mGameUpdate.GetDeltaTime());
*/
	UpdatePawnPhysics();
	UpdateCamera();
	UpdateMovement();
}

void TestGame::UpdatePawnPhysics()
{/*
	if (mSetTransform)
	{
		mPawn->SetTransform(mTransform);
		mSetTransform = false;
	}*/
/*
	if (!mContact && mPawn->IsKinematic())
	{
		mPawn->SetKinematic(false);
		mPawn->SetLinearVelocity(Vector4(VI_Zero));
		mPawn->SetAngularVelocity(Vector4(VI_Zero));		
	}
*/
	// apply linear velocity
	//mTransform = mPawn->GetTransform();
	/*
	Vector4 linVel = Vector4(0.f, 1.f, 0.f); //mPawn->GetLinearVelocity();
	mTransform.Translate(linVel * gGame.mGameUpdate.GetDeltaTime());
	mPawn->SetTransform(mTransform);
*/
	/*
	if (mContactFrameCount == -1)
		mPawn->ApplyLinearForce(mTransform.GetYAxis() * -9.8f);
*/
	//mTransform.SetTranslation(mPawn->GetPosition() - mTransform.GetYAxis()); // set transform at base of sphere
/*
	mContact = false;*/
}

void TestGame::UpdateCamera()
{
	
/*
	mMouseX -= gGame.mInput->GetState(MOUSE_AxisX);
	mMouseY -= gGame.mInput->GetState(MOUSE_AxisY);

	Vector4 up = mTransform.GetYAxis();

	Matrix4 yaw(MI_Identity);
	yaw.RotateY(mMouseX);

	Matrix4 pitch(MI_Identity);
	pitch.RotateX(mMouseY);

	Matrix4 lookAt;

	// apply rotation to pawn
	Vector4 translation = mTransform.GetTranslation();
	lookAt = yaw.Multiply3(mTransform);
	lookAt = pitch.Multiply3(lookAt);
	lookAt.SetTranslation(translation + up * cCameraHeight); // move camera off ground 1.8 meters
	mCamera->LookAt(lookAt);*/

	/*
#ifdef FIRST_PERSON
	Matrix4 worldView = mCamera->GetWorld();

	float mouseX = -gGame.mInput->GetState(MOUSE_AxisX);
	float mouseY = -gGame.mInput->GetState(MOUSE_AxisY);

	mMouseX -= mouseX;
	mMouseY -= mouseY;

	Matrix4 yaw(MI_Identity);
	yaw.RotateY(mouseX);

	Matrix4 pitch(MI_Identity);
	pitch.RotateX(mouseY);

	worldView = pitch.Multiply3(worldView);
	worldView = yaw.Multiply3(worldView);
	

	worldView.SetTranslation(mPawn->GetTransform().GetTranslation());
	mCamera->LookAt(worldView);
/*
	Matrix4 lookAt;

	// apply rotation to pawn
	Vector4 translation = mPawn->GetTransform().GetTranslation();
	lookAt = yaw.Multiply3(mPawn->GetTransform());
	lookAt = pitch.Multiply3(lookAt);
	//lookAt.SetTranslation(translation + up * cCameraHeight); // move camera off ground 1.8 meters
	lookAt.SetTranslation(translation);
	mCamera->LookAt(lookAt);* /
#else
	mCamera->LookAt(Vector4(0.f, 0.f, -15.f), Vector4(0.f, 0.f, 0.f));
#endif*/
}

void TestGame::UpdateMovement()
{/*
	// jump
	if (gGame.mInput->GetState(KEY_Space)) //WasPressed(KEY_Space))
	{
		const float cJumpSpeed = 1000.f;
 		Matrix4 world = mCamera->GetWorld();
		Vector4 viewDir = world.GetZAxis();
		
		// push out of surface a bit
 		Matrix4 transform = mPawn->GetTransform();
		/*
 		transform.SetTranslation(transform.GetTranslation() * transform.GetYAxis() * (-cSphereRadius + 0.1f));
		mPawn->SetTransform(transform);
* /
#ifdef FIRST_PERSON
		mPawn->ApplyLinearForce(viewDir * cJumpSpeed);
#else
		mPawn->ApplyLinearForce(transform.GetYAxis() /*viewDir* / * cJumpSpeed);
#endif
		//mPawn->ApplyLinearForce(mTransform.GetYAxis() * -9.8f);

//		LinearSweepSphere(mPhysicsScene, transform.GetTranslation(), cSphereRadius, viewDir, 1000.f);
	}

	if (gGame.mInput->WasPressed(KEY_v))
	{
		mPawn->SetLinearVelocity(Vector4(0.f, 1.f, 0.f));
	}

	if (gGame.mInput->WasPressed(KEY_r))
	{
		Matrix4 transform(MI_Identity);
		mPawn->SetTransform(transform);
	}*/
}

void TestGame::OnContactNotify(PhysicsBody* body, PhysicsBody* body2, ContactInfoIterator& contactIt, ContactFlags event)
{/*
	// this occurs the frame after the on start touch, so we will be oriented correctly
	if ((event & CF_NotifyOnTouch) 
		&& (body == mPawn || body2 == mPawn))
	{
		mPawn->SetLinearVelocity(Vector4(VI_Zero));
		mPawn->SetAngularVelocity(Vector4(VI_Zero));

		mContact = true;

		if (mContactFrameCount > 2)
		{
			mContactFrameCount = 0;
			mPawn->SetKinematic(false);
		}

		++mContactFrameCount;
	}

	if ((event & CF_NotifyOnEndTouch) 
		&& (body == mPawn || body2 == mPawn))
	{
		mContactFrameCount = -1;
	}

	if ((event & CF_NotifyOnStartTouch) 
		&& (body == mPawn || body2 == mPawn))
	{
		mContact = true;

		// stop the movement
		mPawn->SetLinearVelocity(Vector4(VI_Zero));
		mPawn->SetAngularVelocity(Vector4(VI_Zero));

		// align to newly hit surface
		Vector4 normal = -contactIt->normal;
		Vector4 position = contactIt->position;

		Matrix4 curTransform = mPawn->GetTransform();

		Vector4 xAxis = curTransform.GetZAxis().Cross(normal);
		Vector4 zAxis = normal.Cross(xAxis);
		xAxis = zAxis.Cross(normal);
		
		Matrix4 transform(MI_Identity);
		transform.SetYAxis(normal);
		transform.SetZAxis(zAxis);
		transform.SetXAxis(xAxis);
		transform.SetTranslation(position + normal * (cSphereRadius + 0.1f));

		mPawn->SetKinematic(true);
		mPawn->SetTransform(transform);

		mContactFrameCount = 0;
	}
/*
	// this occurs the frame after the on start touch, so we will be oriented correctly
	if ((event & CF_NotifyOnEndTouch) 
		&& (body == mPawn || body2 == mPawn))
	{
		mPawn->SetKinematic(false);
	}*/
}

void TestGame::OnActorContact(PhysicsBody* body, PhysicsActorController* controller, ContactInfoIterator& contactIt)
{
}

bool TestGame::OnSweepHit(const SweepHit& hit)
{
	int nothing = 0;
	return false;
}

void TestGame::Render()
{
	gGame.mWorld->Render();
	/*
	gGame.mDevice->Begin();
	mRenderer->Render(gGame.mDevice);

	mCamera->Bind(gGame.mDevice);
	mPhysicsScene->Draw(gGame.mDevice);
	gGame.mDevice->End();*/
}
