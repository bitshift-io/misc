#include "StdAfx.h"
#include "Director.h"

void Director::Init()
{
	mDirectorComponent.mDirector = this;

	// metadata this!
	mInputControlMap[CT_LookX].inputControl = MOUSE_AxisX;
	mInputControlMap[CT_LookX].stateModifier = -1.f;

	mInputControlMap[CT_LookY].inputControl = MOUSE_AxisY;
	mInputControlMap[CT_LookY].stateModifier = -1.f;

	mInputControlMap[CT_Jump].inputControl = KEY_Space;
	mInputControlMap[CT_Jump].stateModifier = 1.f;

	mInputControlMap[CT_MoveForward].inputControl = KEY_w;
	mInputControlMap[CT_MoveForward].stateModifier = 1.f;

	mInputControlMap[CT_MoveBackward].inputControl = KEY_s;
	mInputControlMap[CT_MoveBackward].stateModifier = 1.f;

	mInputControlMap[CT_StrafeLeft].inputControl = KEY_a;
	mInputControlMap[CT_StrafeLeft].stateModifier = 1.f;

	mInputControlMap[CT_StrafeRight].inputControl = KEY_d;
	mInputControlMap[CT_StrafeRight].stateModifier = 1.f;
}

float Director::GetControlState(ControlType controlType)
{
	const InputControlMap& controlMap = mInputControlMap[controlType];
	return gGame.mInput->GetState(controlMap.inputControl) * controlMap.stateModifier;
}

bool Director::WasPressed(ControlType controlType)
{
	const InputControlMap& controlMap = mInputControlMap[controlType];
	return gGame.mInput->WasPressed(controlMap.inputControl);
}

void Director::InsertEntity(Entity* entity)
{
	entity->InsertComponent(&mDirectorComponent);
	mEntity.push_back(entity);
}

void Director::RemoveEntity(Entity* entity)
{
	entity->RemoveComponent(&mDirectorComponent);
	mEntity.remove(entity);
}