#ifndef _COMPONENT_H_
#define _COMPONENT_H_

class Entity;
class Component;


//
// many components make up an entity,
// components are things like, what the entity can do and its properties
// eg. health, can use weapons, visual component, physics component
// this can be thought of as an interface in a way
//
// i'm are using an observer pattern
// http://www.codeproject.com/cpp/observer.asp
//
class Component : public Observable
{
public:

	RTTI_PARENT(Observable)

	//
	// required functionality
	//
	virtual void Init(Entity* owner)
	{
		mEntity = owner;
	}

	virtual void Deinit()
	{
	}

	virtual void Resolve()
	{
	}

	virtual void Reset()
	{
	}

	Entity*				mEntity;
};

#endif