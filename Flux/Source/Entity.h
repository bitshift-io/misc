#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <vector>
#include "Reflectance/ClassInfo.h"

using namespace std;

class Component;
class EntityMgr;

//
// a list of components, thats it!
//
class Entity : public Observable
{
public:

	REFLECT()

	void		Init(EntityMgr* entityMgr);
	void		InsertComponent(Component* component);
	void		RemoveComponent(Component* component);

	void		Update();

	// we should be able to iterate over multiple components of the same type!
	Component*	GetComponent(unsigned int type, Component* previous = 0);

	vector<Component*>	mComponent;
	EntityMgr*			mEntityMgr;
};

#endif