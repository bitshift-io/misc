#ifndef _GAMESTATE_H_
#define _GAMESTATE_H_

enum GameStateType
{
	GST_Intro,
	GST_Menu,
	GST_InGame,
};

class GameState
{
public:

	virtual bool			Init()				{ return true; }
	virtual void			Deinit()			{}

	virtual void			StateEnter()		{}
	virtual void			StateExit()			{}

	virtual bool			UpdateGame()		{ return true; }
	virtual bool			UpdateNetwork()		{ return true; }
	virtual bool			UpdatePhysics()		{ return true; }
	virtual void			Render()			{}

	virtual GameStateType	GetType()			= 0;
};

#endif