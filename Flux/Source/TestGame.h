#ifndef _TESTGAME_H_
#define _TESTGAME_H_

#include "StdAfx.h"

//
// quick n dirty proto
//
class TestGame : public ContactReport, public CollisionReport
{
public:

	void Init();
	void Deinit();

	void Update();
	void Render();

	void UpdateCamera();
	void UpdatePawnPhysics();
	void UpdateMovement();

	void OnContactNotify(PhysicsBody* body, PhysicsBody* body2, ContactInfoIterator& contactIt, ContactFlags event);
	void OnActorContact(PhysicsBody* body, PhysicsActorController* controller, ContactInfoIterator& contactIt);

	virtual bool	OnSweepHit(const SweepHit& hit);
/*
	Scene*		mLevel;
	Renderer*	mRenderer;
	*/
	Camera*		mCamera;

	Matrix4		mTransform; // pawn transform
	bool		mSetTransform;
/*
	int			mContactFrameCount; // num frames in contact with a surface
	bool		mContact;
*/
	float		mMouseX;
	float		mMouseY;
/*
	PhysicsScene*	mPhysicsScene;
	PhysicsBody*	mStatic;*/
	PhysicsBody*	mPawn;

	Director*		mDirector;
};

#endif