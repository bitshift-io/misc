<?xml version="1.0" encoding="UTF-8" ?>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<style type="text/css">
${css}
		</style>
		<script type="text/javascript">
${js}
		</script>
	</head>
	<body>
${content}
	</body>
</html>
	