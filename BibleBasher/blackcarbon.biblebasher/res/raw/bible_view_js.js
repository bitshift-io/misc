function toggleVisibility(id) 
{
	var e = document.getElementById(id);
    if (e.style.display == 'block')
	{
		e.style.display = 'none';
	}
    else
	{
		e.style.display = 'block';
	}
}

function scriptureClicked(book, chapter, verse)
{
	var id = "vis_toggle_" + book + "_" + chapter + "_" + verse;
	toggleVisibility(id);
	app.scriptureClicked(book, chapter, verse);
}

function wordClicked(word, strongId)
{
	app.wordClicked(word, strongId);
}