package blackcarbon.biblebasher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.SparseArray;

/*
 * Scriptures are broken down in to strong id/word pairs
 * While this is not used for normal texts, I just store a sentence in a word with no strong id
 * For hebrew and greek texts use this feature so I can link to strongs references for each word
 */
public class Bible {

	private SQLiteDatabase m_database;
	
	private static final String TABLE_CURRENT_BOOK = "current_book";
    private static final String TABLE_BIBLE = "bible";
    private static final String KEY_BOOK = "book";
    private static final String KEY_CHAPTER = "chapter";
    private static final String KEY_VERSE = "verse";
    private static final String KEY_WORDIDX = "wordIdx";
    private static final String KEY_STRONGID = "strongId";
    private static final String KEY_WORD = "word";
    
    private static List<String> m_bookNames;
    
    static public class Word
    {
    	public Word(String word, String strongId)
    	{
    		m_word = word;
    		m_strongId = strongId;
    	}
    	
    	String m_strongId;
    	String m_word;
    }
    
    public static class Book
    {
    	Bible	mBible;
    	int 	mBookIndex;
    	int		mChapterCount;
    	
    	SparseArray<Chapter> mChapterMap = new SparseArray<Chapter>();

    	Book(Bible bible, int book)
    	{
    		mBible = bible;
    		mBookIndex = book;
    		
    		Cursor cursor = mBible.database().rawQuery("SELECT MAX(" + KEY_CHAPTER + ") FROM " + TABLE_BIBLE + " WHERE " + KEY_BOOK + "=?", new String[] { String.valueOf(book) });
        	cursor.moveToFirst();
            mChapterCount = cursor.getInt(0);
    	}
    	
    	int index()
    	{
    		return mBookIndex;
    	}
    	
    	int chapterCount()
    	{
    		return mChapterCount;
    	}
    	
    	Chapter chapter(int chapter)
    	{
    		Chapter value = mChapterMap.get(chapter);
    		if (value != null)
    		{
    			return value;
    		}
    		
    		value = new Chapter(this, chapter);
    		mChapterMap.append(chapter, value);
    		return value;
    	}

    	String tableName()
    	{
    		return Bible.TABLE_BIBLE;
    	}
    }
    
    public static class Chapter 
    {
    	Bible	mBible;
    	Book	mBook;
    	int 	mChapterIndex;
    	int		mVerseCount;
    	SparseArray<Verse> mVerseMap = new SparseArray<Verse>();
    	
    	Chapter(Book book, int chapter)
    	{
    		mBible = book.mBible;
    		mBook = book;
    		mChapterIndex = chapter;
    		
    		String tn = tableName();
    		
    		String createViewQuery = "CREATE TEMP TABLE " + tn + " AS SELECT * FROM " + Bible.TABLE_BIBLE + 
    				" WHERE " + Bible.TABLE_BIBLE + "." + Bible.KEY_BOOK + "=" + String.valueOf(book.index()) + " AND " + Bible.KEY_CHAPTER + "=" + String.valueOf(mChapterIndex);
    		mBible.database().execSQL(createViewQuery);
    		
    		Cursor cursor = mBible.database().rawQuery("SELECT MAX(" + KEY_VERSE + ") FROM " + tn + " WHERE " + KEY_BOOK + "=? AND " + KEY_CHAPTER + "=?", new String[] { String.valueOf(book.index()), String.valueOf(chapter) });
        	cursor.moveToFirst();
        	mVerseCount = cursor.getInt(0);
    	}
    	
    	int index()
    	{
    		return mChapterIndex;
    	}
    	
    	String tableName()
    	{
    		return Bible.bookName(mBook.index()) + "_" + String.valueOf(mChapterIndex);
    	}
    	        
        int verseCount()
        {
        	return mVerseCount;
        }
        
        Verse verse(int verse)
        {
        	Verse value = mVerseMap.get(verse);
    		if (value != null)
    		{
    			return value;
    		}
    		
    		value = new Verse(this, verse);
    		mVerseMap.append(verse, value);
    		return value;
        }
    }
    
    public static class Verse 
    {
    	Bible	mBible;
    	Chapter mChapter;
    	int 	mVerseIndex;
    	List<Word> mWordList;
    	
    	Verse(Chapter chapter, int verse)
    	{
    		mBible = chapter.mBible;
    		mChapter = chapter;
    		mVerseIndex = verse;
    		    		
    		Cursor cursor = mBible.database().rawQuery("SELECT " + KEY_STRONGID + "," + KEY_WORD + " FROM " + tableName() + " WHERE " + KEY_BOOK + "=? AND " + KEY_CHAPTER + "=? AND " + KEY_VERSE + "=?"
    					, new String[] { String.valueOf(mChapter.mBook.index()), String.valueOf(mChapter.index()), String.valueOf(verse) });

        	cursor.moveToFirst();
        	
        	mWordList = new ArrayList<Word>();
        	do
        	{
        		mWordList.add(new Word(cursor.getString(1), cursor.getString(0)));
        	} while (cursor.moveToNext());
    	}
    	
    	int index()
    	{
    		return mVerseIndex;
    	}
    	
    	String tableName()
    	{
    		return mChapter.tableName(); //Bible.bookName(mBook) + "_" + String.valueOf(mChapter);
    	}
    	
    	public List<Word> words()
        { 
    		return mWordList;
        }
    }
    
    SparseArray<Book> mBookMap = new SparseArray<Book>();
    	
	Bible(String path)
	{
		m_database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READONLY);
	}
	
	public SQLiteDatabase database()
	{
		return m_database;
	}
	
	Book book(int book)
	{
		Book value = mBookMap.get(book);
		if (value != null)
		{
			return value;
		}
		
		value = new Book(this, book);
		mBookMap.append(book, value);
		return value;
	}
	
    public static String bookName(int book)
    {
    	return m_bookNames.get(book - 1);
    }
    
    public static void setBookNames(String[] books)
    {	
    	m_bookNames = new ArrayList<String>(Arrays.asList(books));
    }
    
    public List<String> bookNames()
    {
    	return m_bookNames;
    }
}
