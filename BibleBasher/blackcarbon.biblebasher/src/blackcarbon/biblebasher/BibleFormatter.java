package blackcarbon.biblebasher;

import java.util.ArrayList;
import java.util.List;

import blackcarbon.biblebasher.Bible.Word;

public class BibleFormatter {

	ArrayList<Bible> m_bibleList = new ArrayList<Bible>();
	
	public BibleFormatter()
	{
		
	}
	
	Bible primaryBible()
	{
		return m_bibleList.get(0);
	}
	
	void addBible(Bible bible)
	{
		m_bibleList.add(bible);
	}
	
	String chapterAsHtml(int book, int chapter)
	{		
		StringBuilder s = new StringBuilder();
		int verseCount = primaryBible().book(book).chapter(chapter).verseCount();
		for (int verse = 1; verse <= verseCount; ++verse)
		{
			verseAsHtml(s, book, chapter, verse);
		}
		
		return s.toString();
	}
	
	void verseAsHtml(StringBuilder s, int bookIndex, int chapterIndex, int verseIndex)
	{
		for (int b = 0; b < m_bibleList.size(); ++b)
		{
			Bible.Chapter chapter = m_bibleList.get(b).book(bookIndex).chapter(chapterIndex);		
			
			// used to toggle visibility of parallels
			if (b == 0) // primary bible
			{
				s.append("<span onclick=\"scriptureClicked(" + bookIndex + "," + chapterIndex + "," + verseIndex +");\">\n");
			}
			else if (b == 1) // other parallels
			{
				String id = "vis_toggle_" + bookIndex + "_" + chapterIndex + "_" + verseIndex;
				s.append("<span class=\"vis_toggle\" id=\"" + id + "\">\n");	
			}
			
			
			// each verse on a new line
			// set an id so we can style them
			s.append("\t<span class=\"parallel" + (b + 1) + "\">\n");	
			
			if (b == 0) // first parallel (primary)
			{
				s.append("\t\t<span class=\"verse\">" + String.valueOf(verseIndex) + "</span> ");
				//s.append(scripture);
			}
			
			// compile the words in to sentences
			List<Word> wordList = chapter.verse(verseIndex).words();
			for (int w = 0; w < wordList.size(); ++w)
			{
				wordAsHtml(s, wordList.get(w));
			}

			s.append("\n\t</span>\n"); // parallel
			
			if (b == 0) // primary bible
			{
				s.append("</span>\n");
			}
		}
		
		s.append("</span>\n"); // end of vis_toggle_
		s.append("</span>\n");
	}
	
	void wordAsHtml(StringBuilder s, Bible.Word word)
	{
		if (word.m_strongId.isEmpty())
		{
			s.append(word.m_word); 
		}
		else
		{
			s.append("\t\t<span class=\"word\" onclick=\"wordClicked(this.innerText, '" + word.m_strongId + "');\">");
			s.append(word.m_word);
			s.append("</span> \n");
		}
	}
}
