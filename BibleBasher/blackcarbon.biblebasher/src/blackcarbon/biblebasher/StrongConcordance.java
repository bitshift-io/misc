package blackcarbon.biblebasher;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.widget.Toast;

public class StrongConcordance
{
	private SQLiteDatabase m_database;
	
	private static final String TABLE_DICTIONARY = "strongs";
	
    private static final String KEY_STRONGID = "id";
    private static final String KEY_WORD = "word";
	private static final String KEY_SOURCE = "source";
    private static final String KEY_MEANING = "meaning";
    private static final String KEY_USAGE = "usage";
   
	StrongConcordance(String path)
	{
		m_database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READONLY);
	}
	
	String definition(String strongId)
	{
		Cursor cursor = m_database.rawQuery("SELECT " + KEY_WORD + "," + KEY_SOURCE + "," + KEY_MEANING + "," + KEY_USAGE +
				" FROM " + TABLE_DICTIONARY + 
				" WHERE " + KEY_STRONGID + " LIKE '" + strongId + "'", null);
    	cursor.moveToFirst();
    	
    	int i = 0;
    	StringBuilder s = new StringBuilder();
    	do
    	{    		
    		if (i == 0)
    		{
	    		s.append("<span class=\"concordance_title\">");
	    		s.append(cursor.getString(0)); // title
	    		s.append("</span>");
    		}

    		s.append("<span class=\"concordance_strong_id\">");
    		s.append(strongId); // strong id
    		s.append("</span>");
    		
    		s.append("<span class=\"concordance_meaning\">");
    		s.append(cursor.getString(2)); // meaning
    		s.append("</span>");
    		
    		s.append("<span class=\"concordance_source\">");
    		s.append(cursor.getString(1)); // source
    		s.append("</span>");
    		
    		s.append("<span class=\"concordance_usage\">");
    		s.append(cursor.getString(3)); // usage
    		s.append("</span>");
    		
    		++i;
    	} while (cursor.moveToNext());
    	
        return s.toString();
	}
}
