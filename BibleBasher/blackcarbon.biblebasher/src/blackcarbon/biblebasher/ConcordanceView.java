package blackcarbon.biblebasher;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

// http://marakana.com/s/post/1250/android_fragments_tutorial
public class ConcordanceView extends DialogFragment 
{
	private BibleView m_webView = null;
	
	private static String TEXT = "text";

	public static ConcordanceView newInstance(String word, String strongId) 
	{
		String html = Globals.get().concordance().definition(strongId);
    	if (html.isEmpty())
    	{
    		return null;
    	}
    	
		ConcordanceView frag = new ConcordanceView();
        Bundle args = new Bundle();
        args.putString(TEXT, html);
        frag.setArguments(args);
        return frag;
    }
	
    public ConcordanceView() 
    {
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	super.onCreate(savedInstanceState);
    	setStyle(STYLE_NO_TITLE, 0);	
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {        
    	String text = getArguments().getString(TEXT);
        View view = inflater.inflate(R.layout.concordance_view, container);
        m_webView = (BibleView) view.findViewById(R.id.web_view);
        m_webView.setText(text);
        return view;
    }
}
