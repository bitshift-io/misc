package blackcarbon.biblebasher;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ChapterAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener
{
	int m_book;
	int m_chapterCount;
	int m_chapter;
	Context mContext;
	SparseArray<View> mViews = new SparseArray<View>(); // list of views so we can update, one for each corresponding group pos (pos, view)

	public ChapterAdapter(Context context)
	{
		mContext = context;
	}
	
	public void setBook(int book)
	{
		m_book = book;
		m_chapterCount = Globals.get().bibleFormatter().primaryBible().book(book).chapterCount();
		//notifyDataSetChanged();
		onPageSelected(0);
	}
	
	/*
	@Override
	public void notifyDataSetChanged()
	{
		for (int i = 0; i < mViews.size(); ++i)
		{
			int position = mViews.keyAt(i); 
			View view = mViews.get(position); 
			updateView(view, position);
		}
		super.notifyDataSetChanged();
	}*/

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) 
	{
		mViews.remove(position);
		container.removeView((View) object);
	}
	
	void updateView(View view, int position)
	{
		// update view
		BibleView mainView = (BibleView)view.findViewById(R.id.web_view);
		String html = Globals.get().bibleFormatter().chapterAsHtml(m_book, position + 1);
		mainView.setText(html);
	}
		
	@Override
	public Object instantiateItem(ViewGroup container, int position)
	{
		LayoutInflater inflater = LayoutInflater.from(mContext);
		View view = inflater.inflate(R.layout.chapter_view, null);
		mViews.put(position, container);
		container.addView(view, 0);
		updateView(view, position);
		return view;
	}

	@Override
	public int getCount() 
	{
		return m_chapterCount;
	}
	
	@Override
	public void onPageScrollStateChanged(int state) 
	{
		
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
	{
		
	}

	@Override
	public void onPageSelected(int position)
	{
		String bookName = Bible.bookName(m_book);
		MainActivity.get().actionBar().setTitle(bookName + ", Chapter " + String.valueOf(position + 1));
	}

	@Override
	public boolean isViewFromObject(View view, Object object) 
	{
		return view == object;
	}
}