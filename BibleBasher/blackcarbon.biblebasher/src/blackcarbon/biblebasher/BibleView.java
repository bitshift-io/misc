package blackcarbon.biblebasher;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v4.view.DirectionalViewPager;
import android.support.v4.view.ScrollListener;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.PopupWindow;

public class BibleView extends WebView
{
	ScrollListener m_scrollListener = null;
	String m_template;
	ViewPager m_pager = null;
	
	public BibleView(Context context) 
	{
		super(context);
		init(context);
	}
	
	public BibleView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init(context);
	}

	public BibleView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		init(context);
	}
	

    public void setScrollListener(ScrollListener listener) 
    {
    	m_scrollListener = listener;
    }
	
	@SuppressLint("SetJavaScriptEnabled")
	protected void init(Context context)
	{
		setBackgroundColor(0);
		
		// set up the template, this needs to be part of the bible formatter!
		String tpl = MainActivity.readRawTextFile(context, R.raw.bible_view_tpl);
		String css = MainActivity.readRawTextFile(context, R.raw.bible_view_css);
		String js = MainActivity.readRawTextFile(context, R.raw.bible_view_js);
		m_template = tpl;
		m_template = m_template.replace("${css}", css);
		m_template = m_template.replace("${js}", js);
		
		getSettings().setJavaScriptEnabled(true);

		// In this case "app" is the name that you will use from the Html to call 
		// your methods if is not too clear yet, please keep reading
		addJavascriptInterface(this, "app");
	
		/*
		// get pager if this webview is part of a pager
		ViewParent view = getParent();
		while (view != null)
		{
			if (view instanceof DirectionalViewPager)
			{
				m_pager = (ViewPager) this.getParent();
				break;
			}
			
			view = view.getParent();
		}*/
	}
	
	@JavascriptInterface
	public void scriptureClicked(final int book, final int chapter, final int verse)
	{
	}

	@JavascriptInterface
	public void wordClicked(final String word, final String strongId)
	{
		MainActivity.get().showConcordance(word, strongId);
	}
	
	void setText(String html)
	{
		String content = m_template.replace("${content}", html);
		
		// http://stackoverflow.com/questions/4096783/android-webview-1st-loaddata-works-fine-subsequent-calls-do-not-update-disp
		loadDataWithBaseURL("same://ur/l/tat/does/not/work", content, "text/html", "utf-8", null);
	}
	
	@Override
	protected void onScrollChanged(int x, int y, int oldX, int oldY)
	{
		if (m_scrollListener != null)
		{
			m_scrollListener.onScrollChanged(x, y, oldX, oldY);
		}
		/*
		int deltaY = oldY - y;
		
		if (y == 0)
		{
			MainActivity.get().showActionBar(MainActivity.ActionBarOperation.Show);	
		}
		else if (deltaY > 0)
		{
			MainActivity.get().showActionBar(MainActivity.ActionBarOperation.ShowAutoHide);
		}
		else
		{
			MainActivity.get().showActionBar(MainActivity.ActionBarOperation.Hide);
		}
		*/
		
		super.onScrollChanged(x, y, oldX, oldY);
	}
	
	@Override
	protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY)
	{
		if (m_scrollListener != null)
		{
			m_scrollListener.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
		}
		
		/*
		if (clampedY)
		{
			MainActivity.get().showActionBar(MainActivity.ActionBarOperation.Show);	
		}*/
		
		super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
	}
	
	@Override
    public boolean onTouchEvent(MotionEvent event) 
	{
		return super.onTouchEvent(event);
	}
}
