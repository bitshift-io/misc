package blackcarbon.biblebasher;

import android.webkit.WebView;

public class Globals {

	private static Globals m_instance = null;
	
	private BibleFormatter m_bibleFormatter = new BibleFormatter();
	private StrongConcordance m_concordance;
	
	protected Globals()
	{
		
	}
	
	public static Globals get()
	{
      if (m_instance == null)
      {
    	  m_instance = new Globals();
      }
      return m_instance;
	}
		
	public BibleFormatter bibleFormatter()
	{
		return m_bibleFormatter;
	}
	
	public StrongConcordance concordance()
	{
		return m_concordance;
	}
	
	public void setConcordance(StrongConcordance concordance)
	{
		m_concordance = concordance;
	}	
}
