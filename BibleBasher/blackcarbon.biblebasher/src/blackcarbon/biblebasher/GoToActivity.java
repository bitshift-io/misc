package blackcarbon.biblebasher;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.support.v4.app.NavUtils;
import android.widget.AdapterView;

public class GoToActivity extends Activity implements AdapterView.OnItemClickListener {

	ListView 	m_bookList;
	ListView	m_chapterList;
	ChapterAdapter	m_chapterAdapter;
	
	static class ChapterAdapter extends ArrayAdapter<String> implements AdapterView.OnItemClickListener
	{		
		int m_book = 1;
		
		public ChapterAdapter(Context context) {
			super(context, android.R.layout.simple_list_item_1);
			onItemClick(null, null, 0, 0); // fakey to get chapters for Genesis
		}
		
		public int selectedBook()
		{
			return m_book;
		}

		/*
		 * Called when the user presses on the book list
		 * as this Adapter is set as the click listener for the book list,
		 * it will then update the chapter list
		 */
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) 
		{
			m_book = position + 1;
			clear();
			
			int chapterCount = Globals.get().bibleFormatter().primaryBible().book(m_book).chapterCount();
			for (int i = 1; i <= chapterCount; ++i)
			{
				String string = String.valueOf(i);
				add(string);
			}
			
			notifyDataSetChanged();
		}	
	}
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_go_to);
		
		// http://projets2a.iutlan.etu.univ-rennes1.fr/Projet041011/browser/sources/ENT-Project/res/anim?rev=57
		// http://stackoverflow.com/questions/3389501/activity-transition-in-android
		
		//getWindow().setWindowAnimations(android.R.anim.slide_in_left);
		//getWindow().getAttributes().windowAnimations = R.style.SlideUpAnimation;
		
		// Show the Up button in the action bar.
		//getActionBar().setDisplayHomeAsUpEnabled(true);
		
		m_bookList = (ListView)findViewById(R.id.book_list);
		m_chapterList = (ListView)findViewById(R.id.chapter_list);
		
		m_chapterAdapter = new ChapterAdapter(this);
		m_chapterList.setAdapter(m_chapterAdapter);
		m_chapterList.setOnItemClickListener(this);
		m_bookList.setOnItemClickListener(m_chapterAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_go_to, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/*
	 * Called when the user presses on the chapter list
	 * so at that stage we need to close
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) 
	{
		int book = m_chapterAdapter.selectedBook();
		int chapter = position + 1;
		MainActivity.get().showChapter(book, chapter);
		finish();
	}
}
