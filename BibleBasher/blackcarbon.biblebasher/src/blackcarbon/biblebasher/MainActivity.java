package blackcarbon.biblebasher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ActionBar;
import android.support.v4.view.DirectionalViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

public class MainActivity extends FragmentActivity {
	
	ActionBar mActionBar = null;
	
	private final int HIDE_ACTION_BAR_TIME_MS = 1000;
	

	ChapterAdapter m_chapterFragmentAdapter = null;
	DirectionalViewPager mPager = null;
	
	
	//BibleView m_mainView;
	private static MainActivity m_instance;
	
	/*
	private Handler m_handler = new Handler();
	
	enum ActionBarOperation
	{
		ShowAutoHide,
		Show,
		Hide,
		AutoHide
	}

	
	private boolean m_hideActionRunning = false;
	private Runnable m_hideActionBarTask = new Runnable() {
		   public void run() 
		   {
		       getActionBar().hide();
		       m_hideActionRunning = false;
		   }
		};
		*/
		
	public static MainActivity get()
	{
      return m_instance;
	}
	
	public MainActivity()
	{
		super();
		m_instance = this;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		//getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FF000000")));
		
		
		setContentView(R.layout.activity_main);
		
		mActionBar = (ActionBar)findViewById(R.id.actionBar);
		mActionBar.setViewFromResource(R.layout.actionbar);
				

		//FrameLayout barView = (RelativeLayout) mInflater.inflate(R.layout.actionbar, null);
		//View testView = getActionBar().getCustomView();
		
		//m_mainView = (BibleView)findViewById(R.id.main_view);
				
		// set up book names
		try {
            Bible.setBookNames(getResources().getStringArray(R.array.books));
        } catch (Exception e) {
 
        }
		
		// TODO:
		Globals.get().bibleFormatter().addBible(new Bible("/sdcard/data/blackcarbon.biblebasher/bibles/kjv.db"));
		Globals.get().bibleFormatter().addBible(new Bible("/sdcard/data/blackcarbon.biblebasher/hebrew_bible.db"));
		
		Globals.get().setConcordance(new StrongConcordance("/sdcard/data/blackcarbon.biblebasher/strongs.db"));

		
		//Set up the pager
		//m_chapterFragmentAdapter = new ChapterAdapter(getApplicationContext());
		
		mPager = (DirectionalViewPager)findViewById(R.id.pager);
		//mPager.setAdapter(m_chapterFragmentAdapter);
		//mPager.setOnPageChangeListener(m_chapterFragmentAdapter);
        //pager.setOrientation(DirectionalViewPager.VERTICAL);
		//mPager.setPagingEnabled(true);

        
		showChapter(1, 1);
		//showActionBar(ActionBarOperation.Show);
	}
	
	/*
	 * Called when the user scrolls the webview up
	 * this will show the action bar for about 1 second before hiding itself
	 * /
	void showActionBar(ActionBarOperation operation)
	{
		switch (operation)
		{
		case ShowAutoHide:
			getActionBar().show();
			m_hideActionRunning = true;
			m_handler.removeCallbacks(m_hideActionBarTask);
			m_handler.postDelayed(m_hideActionBarTask, HIDE_ACTION_BAR_TIME_MS);			
			break;
			
		case Show:
			m_hideActionRunning = false;
			m_handler.removeCallbacks(m_hideActionBarTask);
			getActionBar().show();
			break;
			
		case Hide:
			m_hideActionRunning = false;
			m_handler.removeCallbacks(m_hideActionBarTask);
			getActionBar().hide();
			break;
			
		case AutoHide:
			if (getActionBar().isShowing() && !m_hideActionRunning)
			{
				m_hideActionRunning = true;
				m_handler.removeCallbacks(m_hideActionBarTask);
				m_handler.postDelayed(m_hideActionBarTask, HIDE_ACTION_BAR_TIME_MS);
			}
			break;
		}		
	}*/
		
	public ActionBar actionBar()
	{
		return mActionBar;
	}
	
	void showChapter(int book, int chapter)
	{
		String bookName = Bible.bookName(book);
		//getActionBar().setTitle(bookName + ", Chapter " + String.valueOf(chapter));
		//m_mainView.setText(Globals.get().bibleFormatter().chapterAsHtml(book, chapter));
		
		m_chapterFragmentAdapter = new ChapterAdapter(getApplicationContext());
		m_chapterFragmentAdapter.setBook(book);
		
		mPager.setAdapter(m_chapterFragmentAdapter);
		mPager.setOnPageChangeListener(m_chapterFragmentAdapter);
		
		//mPager.setAdapter(null);
		
		//mPager.setAdapter(m_chapterFragmentAdapter);
		mPager.setCurrentItem(chapter - 1);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		int id = item.getItemId();
		switch (id)
		{
		case R.id.menu_goto:
			gotoActivity();
			return true;
			
		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	/*
	 * Launch the "Go To" activity to allow users to select book/chapter
	 */
	void gotoActivity()
	{
		Intent intent = new Intent(this, GoToActivity.class);
		startActivity(intent);
	}
	
	public static String readRawTextFile(Context ctx, int resId)
    {
		InputStream inputStream = ctx.getResources().openRawResource(resId);
		
		InputStreamReader inputreader = new InputStreamReader(inputStream);
		BufferedReader buffreader = new BufferedReader(inputreader);
		String line;
		StringBuilder text = new StringBuilder();
		
		try {
			while (( line = buffreader.readLine()) != null) 
			{
				text.append(line);
				text.append('\n');
		    }
		 } catch (IOException e) {
			 return null;
		 }
         
         return text.toString();
    }

	public void showConcordance(String word, String strongId) 
	{
		ConcordanceView view = ConcordanceView.newInstance(word, strongId);
		if (view != null)
		{
			//	view.setConfirmationDialogFragmentListener(this);
			view.show(getFragmentManager(), null);
		}
	}
}
