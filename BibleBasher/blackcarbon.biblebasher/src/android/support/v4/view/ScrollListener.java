package android.support.v4.view;

public interface ScrollListener 
{
	public void onScrollChanged(int x, int y, int oldX, int oldY);
	public void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY);
}
