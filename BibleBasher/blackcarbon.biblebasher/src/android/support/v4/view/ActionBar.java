package android.support.v4.view;

import blackcarbon.biblebasher.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ActionBar extends FrameLayout {

	public ActionBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public void setViewFromResource(int id)
	{
		LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout barView = (LinearLayout) inflater.inflate(id, null);
	    addView(barView);
	}

	public void setTitle(String title)
	{
		TextView textView = (TextView)findViewById(R.id.title);
		textView.setText(title);
	}
}
