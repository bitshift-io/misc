# -*- coding: utf_8 -*-
#
# Copyright (c) 2013 Fabian Mathews
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, publish, distribute, 
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES 
# OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#
# Downloads and converts .bbl.mybible to format usable by my app
#

import os
import sqlite3
import re
import shutil

g_inBiblesDir = "input_bibles"
g_outBiblesDir = "bibles"
g_bbl_mybible_ext = ".bbl.mybible"
g_output_ext = ".db"
g_forceDownload = True

# Download the given URL and return a local file path
def download(url):	
    if not os.path.exists(os.path.basename(url)) or g_forceDownload == True:
        print("Retrieving %s" % url)
        try:
            urllib.urlretrieve(url, os.path.basename(url))
        except:
            print("Failed to retrieve resource.")
            sys.exit(1)
    else:
        print("%s already exists" % os.path.basename(url))
    return os.path.basename(url)

class BibleDB():
    def __init__(self, db_file):
		if os.path.exists(db_file):
			os.remove(db_file) # delete db if it exists

		self._conn = sqlite3.connect(db_file)
		self._cursor = self._conn.cursor()
		init_db_sql = "create table bible (book integer, chapter integer, verse integer, wordIdx integer, strongId text, word text)"
		self._cursor.execute(init_db_sql)
		self._conn.commit()
    
    def addWord(self, book, chapter, verse, wordIdx, strongId, word):
        add_row_sql = 'insert into bible values (?, ?, ?, ?, ?, ?)' 
        self._cursor.execute(add_row_sql, (book, chapter, verse, wordIdx, strongId, word))

    def commit(self):
        self._conn.commit()

    def finish(self):
        self._cursor.close()
	
class BblMyBibleParser:
	def __init__(self, db_file, bible_db):			
		self._conn = sqlite3.connect(db_file)
		self._cursor = self._conn.cursor()
		self.bibleDb = bible_db
		self.parse()
		self._conn.commit()
		self._cursor.close()
		
	def finish(self):
		self._cursor.close()
		
	def parse(self):
		self._cursor.execute("SELECT Book, Chapter, Verse, Scripture FROM Bible")
		rows = self._cursor.fetchall()
		for row in rows:
			self.bibleDb.addWord(row[0], row[1], row[2], 1, "", self.strip(row[3]))
			
		self.bibleDb.commit()
		
	# strip out unwanted stuff, these are strongs references or html etc
	# ideally we should split and add parts of sentences based on the strongs id's
	# but these never match anyway
	def strip(self, sentence):
		if sentence == None:
			return sentence
			
		#print(sentence.encode('utf-8', 'ignore'))
		sentence = re.sub(ur"(<[^>]+>)", ur"", sentence)
		return sentence
		
if __name__ == "__main__":

	# remove old files
	#try:
	#	shutil.rmtree(g_outBiblesDir)
	#except:
	#	pass	
	
	try:
		os.mkdir(g_outBiblesDir)
	except:
		print("Could not create: " + g_outBiblesDir + ", please create this dir")
	
	# pass bbl.mybible
	bibleFileNameList = os.listdir(g_inBiblesDir)
	for bibleFileName in bibleFileNameList:
		if bibleFileName.endswith(g_bbl_mybible_ext):
			outName = bibleFileName[0:-len(g_bbl_mybible_ext)] + g_output_ext
			print(outName)
			outDb = BibleDB(g_outBiblesDir + "/" + outName)
			inDb = BblMyBibleParser(g_inBiblesDir + "/" + bibleFileName, outDb)

	
	print("\nDone")
