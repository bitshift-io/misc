# -*- coding: utf_8 -*-
#
# Copyright (c) 2013 Fabian Mathews
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without restriction, 
# including without limitation the rights to use, copy, modify, merge, publish, distribute, 
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES 
# OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#
# This script downloads the appropriate files and generates the bible and strongs concordance into an sql lite database
#

import os
import sqlite3
import zipfile
import urllib
import shutil
import re
from xml.dom import minidom

# Globals
g_strongs_hebrew_xml_filename = "https://raw.github.com/openscriptures/HebrewLexicon/master/HebrewStrong.xml"
g_hebrew_bible_zip_filename = "https://github.com/openscriptures/morphhb/archive/master.zip"
g_heberew_bible_extracted_bible_xml_dir = "morphhb-master/wlc"

g_strongs_output_db_filename = 'strongs.db'
g_hebrew_bible_output_db_filename = "hebrew_bible.db"

g_forceDownload = True

# Download the given URL and return a local file path
def download(url):	
    if not os.path.exists(os.path.basename(url)) or g_forceDownload == True:
        print("Retrieving %s" % url)
        try:
            urllib.urlretrieve(url, os.path.basename(url))
        except:
            print("Failed to retrieve resource.")
            sys.exit(1)
    else:
        print("%s already exists" % os.path.basename(url))
    return os.path.basename(url)

class BibleDB():
    def __init__(self, db_file):
		if os.path.exists(db_file):
			os.remove(db_file) # delete db if it exists

		self._conn = sqlite3.connect(db_file)
		self._cursor = self._conn.cursor()
		init_db_sql = "create table bible (book integer, chapter integer, verse integer, wordIdx integer, strongId text, word text)"
		self._cursor.execute(init_db_sql)
		self._conn.commit()
    
    def addWord(self, book, chapter, verse, wordIdx, strongId, word):
        add_row_sql = 'insert into bible values (?, ?, ?, ?, ?, ?)' 
        self._cursor.execute(add_row_sql, (book, chapter, verse, wordIdx, strongId, word))

    def commit(self):
        self._conn.commit()

    def finish(self):
        self._cursor.close()
		
class HebrewBibleParser:
	def __init__(self, db):
		self.db = db
		
		self.books = ["Gen","Exod","Lev","Num", "Deut", "Josh", "Judg", "Ruth","1Sam","2Sam",
        "1Kgs","2Kgs","1Chr", "2Chr","Ezra","Neh","Esth","Job","Ps","Prov","Eccl","Song","Isa",
        "Jer","Lam","Ezek","Dan","Hos","Joel","Amos","Obad","Jonah","Mic","Nah","Hab","Zeph","Hag","Zech","Mal"]
		
		self.parse()
		
	def bookIndex(self, book):
		return self.books.index(book) + 1
		
	def parse(self):
		print("Parsing Hebrew Bible XML")
		local_zip_filename = download(g_hebrew_bible_zip_filename)

		zip = zipfile.ZipFile(local_zip_filename)
		zip.extractall()
		
		bibleBooksXmlFileNameList = os.listdir(g_heberew_bible_extracted_bible_xml_dir)
		for bibleBookXmlFileName in bibleBooksXmlFileNameList:
			print(bibleBookXmlFileName)
			self.parseBook(g_heberew_bible_extracted_bible_xml_dir + "/" + bibleBookXmlFileName)
		
		self.db.commit()
		
		# remove temp files
		try:
			shutil.rmtree('morphhb-master')
		except:
			pass			
		
	def parseBook(self, bookFileName):
		xmldoc = minidom.parse(bookFileName)
		verseList = xmldoc.getElementsByTagName("verse")
		
		for verse in verseList:
			self.parseVerse(verse)	
			
	def parseVerse(self, verseNode):
		osisID = ""
		try:
			osisID = verseNode.attributes["osisID"].value
		except:
			return # will occur for xml files that are NOT books of the bible
		
		osisIDList = osisID.split(".");
		self.book = self.bookIndex(osisIDList[0])
		self.chapter = osisIDList[1]
		self.verse = osisIDList[2]
		
		wordList = verseNode.getElementsByTagName("w")
		
		wordIdx = 1;
		for word in wordList:
			self.parseWord(word, wordIdx)
			wordIdx += 1
		
	def parseWord(self, wordNode, wordIdx):
		strongId = ""
		word = wordNode.childNodes[0].data
		
		try:
			strongId = wordNode.attributes["lemma"].value
			strongId = re.search("(\d+)", strongId).group(0) # strip out non digit characters
			strongId = "H" + strongId
		except:
			strongId = ""
		
		self.db.addWord(self.book, self.chapter, self.verse, wordIdx, strongId, word)
	
class StrongsDB():
    def __init__(self, db_file):
		if os.path.exists(db_file):
			os.remove(db_file) # delete db if it exists

		self._conn = sqlite3.connect(db_file)
		self._cursor = self._conn.cursor()
		init_db_sql = "create table strongs (id text, word text, source text, \
		meaning text, usage text)"
		self._cursor.execute(init_db_sql)
		self._conn.commit()
    
    def addRow(self, id, word, source, meaning, usage):
        add_row_sql = 'insert into strongs values (?, ?, ?, ?, ?)' 
        self._cursor.execute(add_row_sql, (id, word, source, meaning, usage))

    def commit(self):
        self._conn.commit()

    def finish(self):
        self._cursor.close()

class StrongsHebrewParser:
	def __init__(self, db):
		self.in_foreign = False
		self.note_depth = 0
		self.in_entry = False
		self.in_trans = False
		self.in_w = False
		self.db = db
		self.parse()

	def parse(self):
		print("Parsing Strong Hebrew XML")
		local_filename = download(g_strongs_hebrew_xml_filename)

		xmldoc = minidom.parse(local_filename)
		entryList = xmldoc.getElementsByTagName('entry') 

		for entry in entryList:
			self.parseEntry(entry)

		self.db.commit()
		
	def parseEntry(self, entry):
		id = entry.attributes['id'].value
		print(id)
		word = entry.getElementsByTagName('w')[0].childNodes[0].data
		
		meaning = ""
		try:
			meaning = self.childNodesAsData(entry.getElementsByTagName('meaning')[0].childNodes)
		except:
			pass
			
		usage = ""
		try:
			usage = self.childNodesAsData(entry.getElementsByTagName('usage')[0].childNodes)
		except:
			pass
			
		source = ""
		try:
			source = self.childNodesAsData(entry.getElementsByTagName('source')[0].childNodes)
		except:
			pass
			
		self.db.addRow(id, word, source, meaning, usage)
	
	def childNodesAsData(self, nodeList):
		result = ""
		for node in nodeList:
			if node.nodeType == node.TEXT_NODE:
				result += node.data
			else:
				#result += "<" + node.localName + ">"
				result += self.childNodesAsData(node.childNodes)
				#result += "</" + node.localName + ">"
			
		return result;
	
#
# ENTRY
#
if __name__ == "__main__":

	strongsDb = StrongsDB(g_strongs_output_db_filename)
	StrongsHebrewParser(strongsDb)
	
	hebrewBibleDb = BibleDB(g_hebrew_bible_output_db_filename)
	HebrewBibleParser(hebrewBibleDb)
	
	print("\nDone")
