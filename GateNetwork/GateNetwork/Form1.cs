﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GateNetwork
{
    public partial class Form1 : Form
    {
        GateNetwork m_gateNetwork = new GateNetwork();

        public Form1()
        {
            InitializeComponent();

            m_gateNetwork.Init(2, 1);

            float trainingCount = 1;

            // OR
            for (int i = 0; i < trainingCount; i++)
            {
                m_gateNetwork.Train(new float[] { 0, 0 }, new float[] { 0 });
                m_gateNetwork.Train(new float[] { 0, 1 }, new float[] { 1 });
                m_gateNetwork.Train(new float[] { 1, 0 }, new float[] { 1 });
                m_gateNetwork.Train(new float[] { 1, 1 }, new float[] { 1 });
            }
        }
    }
}
