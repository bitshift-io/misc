﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GateNetwork
{
    // Neural network are simply gates
    // they arrange them selves in to AND, OR, XOR, NAND gates
    // so why not simplify them
    class Gate
    {
        public enum Operation
        {
            AND,
            OR,
            XOR,
            NAND
        };


        public float input;
        public float output;
        public float expectedOutput;
        public Operation operation;
        public List<Link> parents;
        public List<Link> children;

        public void Process()
        {

        }
    }

    class Link
    {
        public float weight;
        public Gate parent;
        public Gate child;
    }

    class GateNetwork
    {
        protected List<Gate> m_inputList = new List<Gate>();
        protected List<Gate> m_outputList = new List<Gate>();
        protected List<Gate> m_hiddenList = new List<Gate>();

        public void Init(int inputCount, int outputCount)
        {
            for (int i = 0; i < inputCount; ++i)
            {
                Gate gate = new Gate();
                m_inputList.Add(gate);
            }

            for (int i = 0; i < outputCount; ++i)
            {
                Gate gate = new Gate();
                m_outputList.Add(gate);
            }
        }

        public List<Gate> InputList()
        {
            return m_inputList;
        }

        public List<Gate> OutputList()
        {
            return m_outputList;
        }

        public float[] Train(float[] input, float[] expectedOutput)
        {
            // some how need to learn and evolve the network here
            // how to work out if the gate should be changed?
            List<Gate> toBePrcoessed = new List<Gate>(m_inputList.Count + m_hiddenList.Count + m_outputList.Count);
            for (int i = 0; i < m_inputList.Count; ++i )
            {
                Gate gate = m_inputList[i];
                gate.input = input[i];
                toBePrcoessed.Add(gate);
            }

            while (toBePrcoessed.Count != 0)
            {
                Gate gate = toBePrcoessed.First();
                toBePrcoessed.RemoveAt(0);

                gate.Process();
                foreach (Link child in gate.children)
                {
                    toBePrcoessed.Add(child.child);
                }
            }

            return null;
        }
    }
}
