import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


public class Main extends JFrame implements WindowListener
{
	public WordPrediction mWordPrediction = new WordPrediction();
	public SpellChecker	mSpellChecker = new SpellChecker();
	public Dictionary	mDictionary = new Dictionary();
	public JTextArea 	mLabel;
	public AITextField 	mTextField;

	class AITextField extends JTextField implements DocumentListener, ActionListener
	{
		AITextField()
		{
			getDocument().addDocumentListener(this);
			addActionListener(this);
		}
		
		@Override
		public void changedUpdate(DocumentEvent e)
		{
			System.out.println("changedUpdate - "+getText());
		}

		@Override
		public void insertUpdate(DocumentEvent e)
		{
			// process input for ai, display ai results in mLabel
			
			// what i probably should do get around auto correction crap
			// is to underline words i think are incorrect
			// dont add words to ai dictionary till user hits accept
			// this gives them a chance to correct their words before going in to dictionary
			
			System.out.println("insertUpdate - "+getText());
			
			String labelText = "output: " + getText() + "\n";
			String[] result =  getText().split("\\s");
			String currentWord = result[result.length - 1];
			
			if (getText().endsWith(" "))
			{	
				{
					labelText += "spell checker:";
					String correction = mSpellChecker.correct(currentWord);
					labelText += correction;
					//for (int i = 0; i < predictions.size(); ++i)
					//{
					//	labelText += " " + predictions.get(i);
					//}
					
					labelText += "\n";
				}
				/*
				System.out.println("adding word to dictionary: - " + currentWord);
				mDictionary.addWord(currentWord);
				currentWord = "";
				
				labelText += "AI predictions:";
				List<String> predictions = mWordPrediction.getPredictions(result);
				for (int i = 0; i < predictions.size(); ++i)
				{
					labelText += " " + predictions.get(i);
				}
				
				labelText += "\n";*/
			}
			else
			{
				labelText += "predictions:";
				List<String> predictions = mDictionary.getCompletionPredictions(currentWord);
				for (int i = 0; i < predictions.size(); ++i)
				{
					labelText += " " + predictions.get(i);
				}
				
				labelText += "\n";
			}
			
			labelText += "current word: " + currentWord;
			
			mLabel.setText(labelText);
		}

		@Override
		public void removeUpdate(DocumentEvent e)
		{
			System.out.println("removeUpdate - "+getText());
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			System.out.println("actionPerformed - "+getText());
		}
	}
	
	public static void main(String[] args) 
	{
		new Main();
	}
	
	public Main()
	{
		if (!mDictionary.serializeFromFile("dictionary.ser"))
		{
			// first time initialization
			Dictionary.DictionaryList list = new Dictionary.DictionaryList();
			list.serializeFromFile("words.txt");
			mDictionary.add(list);
		}
		
		mSpellChecker.loadTrainingSet("big.txt");
		
		mWordPrediction.serializeFromFile("wordprediction.ser");
		
		addWindowListener(this);
		mLabel = new JTextArea("AI Stuff");
		mTextField = new AITextField();
		
		JPanel contentPane = new JPanel(new BorderLayout());
		contentPane.add(mLabel, BorderLayout.CENTER);
		contentPane.add(mTextField, BorderLayout.PAGE_END);
		setContentPane(contentPane);

		setSize(300, 200);
		setVisible(true);
		
		// test cases
		NNet2 n = new NNet2();
		n.test();
		
		//mWordPrediction.test3();
		//mWordPrediction.test1();
		//mWordPrediction.loadTrainingSet("ai_sentences.txt", mDictionary.getList());
		//mWordPrediction.loadTrainingSet("big.txt", mDictionary.getList());
		//mWordPrediction.test();
	}

	@Override
	public void windowActivated(WindowEvent e)
	{
	}

	@Override
	public void windowClosed(WindowEvent e)
	{
	}

	@Override
	public void windowClosing(WindowEvent e)
	{
		mWordPrediction.serializeToFile("wordprediction.ser");
		mDictionary.serializeToFile("dictionary.ser");
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent e)
	{
	}

	@Override
	public void windowDeiconified(WindowEvent e)
	{
	}

	@Override
	public void windowIconified(WindowEvent e)
	{
	}

	@Override
	public void windowOpened(WindowEvent e)
	{
	}

}
