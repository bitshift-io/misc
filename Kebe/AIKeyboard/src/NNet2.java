
import java.io.Serializable;



public class NNet2 implements Serializable
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3167354486031965781L;

	static class NeuronLayer implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -5922771301451682400L;

		NeuronLayer(int neuronCount)
		{
			neuron = new Neuron[neuronCount];
		}
		
		Neuron[]	neuron;
	}
	
	static class Neuron implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -7054399037139091773L;
		
		float[] weight;
		float	output;
		float	learnRate;
		float	error;
	}
	
	NeuronLayer[]	mLayer;
	float			mLearnRate = 0.5f;
		
	NNet2()
	{
	}
	
	void create(int inputCount, NeuronLayer[] layer)
	{
		create(inputCount, layer, -1.f, 1.f, -1.f, 1.f);
	}
	
	void create(int inputCount, NeuronLayer[] layer, float minWeight, float maxWeight)
	{
		create(inputCount, layer, minWeight, maxWeight, -1.f, 1.f);
	}
	
	
	void create(int inputCount, NeuronLayer[] layer, float minWeight, float maxWeight, float minLearnRate, float maxLearnRate)
	{	
		mLayer = layer;
			//new NeuronLayer[2];
		//mLayer[0] = new NeuronLayer(3);
		//mLayer[1] = new NeuronLayer(1);
		
		for (int l = 0; l < mLayer.length; ++l)
		{
			for (int n = 0; n < mLayer[l].neuron.length; ++n)
			{
				int previousLayerCount = (l == 0) ? inputCount : mLayer[l - 1].neuron.length;
				Neuron neuron = new Neuron();
				mLayer[l].neuron[n] = neuron;
				neuron.weight = new float[previousLayerCount + 1]; // +1 for threshold weight
				neuron.learnRate = (float)Math.random() * (maxLearnRate - minLearnRate) + minLearnRate;
				
				for (int w = 0; w < neuron.weight.length; ++w)
				{
					neuron.weight[w] = (float)Math.random() * (maxWeight - minWeight) + minWeight;
				}
			}
		}
	}
	
	float sigmoid(float num)
	{
		return num; //(float)(1.f/(1.f+Math.exp(-num)));
		//return ((float)(1.f/(1.f+Math.exp(-num))) - 0.5f);
	}
	
	float[] train(float[] input, float[] expectedOutput)
	{
		//float learnRate = 0.5f;
		float[] output = run(input);
		
		// go through and calculate errors
		for (int l = (mLayer.length - 1); l >= 0; --l)
		{
			for (int n = 0; n < mLayer[l].neuron.length; ++n)
			{
				Neuron neuron = mLayer[l].neuron[n];
				
				float error = 0; //neuron.output * (1 - neuron.output);
				if ((l + 1) == mLayer.length)
				{
					error += (expectedOutput[n] - neuron.output);
				}
				else
				{	
					for (int on = 0; on < mLayer[l + 1].neuron.length; ++on)
					{
						Neuron outputNeuron = mLayer[l + 1].neuron[on];
						error += outputNeuron.weight[n] * outputNeuron.error;
					}
				}
				
				neuron.error = error;
			}
		}
		
		// now alter the weights
		for (int l = 0; l < mLayer.length; ++l)
		{
			for (int n = 0; n < mLayer[l].neuron.length; ++n)
			{
				Neuron neuron = mLayer[l].neuron[n];
				
				for (int w = 0; w < neuron.weight.length; ++w)
				{
					if ((w + 1) == neuron.weight.length)
					{
						neuron.weight[w] += mLearnRate * neuron.error;
					}
					else if (l == 0)
					{
						neuron.weight[w] += mLearnRate * neuron.error * input[w];
					}
					else
					{
						Neuron previousNeuron = mLayer[l - 1].neuron[w];
						neuron.weight[w] += mLearnRate * neuron.error * previousNeuron.output;
					}
				}
			}
		}
		
		return output;
	}
	
	float[] run(float[] input)
	{
		for (int l = 0; l < mLayer.length; ++l)
		{
			for (int n = 0; n < mLayer[l].neuron.length; ++n)
			{
				Neuron neuron = mLayer[l].neuron[n];
				
				neuron.output = 0.f;
				for (int w = 0; w < neuron.weight.length; ++w)
				{
					if ((w + 1) == neuron.weight.length)
					{
						neuron.output += neuron.weight[w];
					}
					else if (l == 0)
					{
						neuron.output += neuron.weight[w] * input[w];
					}
					else
					{
						Neuron previousNeuron = mLayer[l - 1].neuron[w];
						neuron.output += neuron.weight[w] * previousNeuron.output;
					}
				}
				
				float output = sigmoid(neuron.output);
				neuron.output = output;
			}
		}
		
		// gather outputs and return them
		NeuronLayer outputLayer = mLayer[mLayer.length - 1];
		float[] output = new float[outputLayer.neuron.length];
		
		for (int n = 0; n < outputLayer.neuron.length; ++n)
		{
			output[n] = mLayer[mLayer.length - 1].neuron[n].output;
		}
		
		return output;
		
		/*
		// I just copied and pasted the code from the Train() function,
		// so see there for the necessary documentation.
		
		float net1, net2, i3, i4;
		
		net1 = 1 * m_fWeights[0][0] + i1 * m_fWeights[1][0] +
			  i2 * m_fWeights[2][0];
		net2 = 1 * m_fWeights[0][1] + i1 * m_fWeights[1][1] +
			  i2 * m_fWeights[2][1];

		i3 = Sigmoid(net1);
		i4 = Sigmoid(net2);

		net1 = 1 * m_fWeights[0][2] + i3 * m_fWeights[1][2] +
		   	  i4 * m_fWeights[2][2];
		return Sigmoid(net1);*/
	}
	
	void test()
	{
		NeuronLayer[] layer = new NeuronLayer[2];
		layer[0] = new NeuronLayer(3);
		layer[1] = new NeuronLayer(1);
		create(2, layer);
		
		
		float trainingCount = 2000;
		
		// XOR
		for (int i=0;i<trainingCount;i++)
		{
			train(new float[]{0, 0}, new float[]{0});
			train(new float[]{0, 1}, new float[]{1});
			train(new float[]{1, 0}, new float[]{1});
			train(new float[]{1, 1}, new float[]{0});
		}
/*
		
		// OR
		for (int i=0;i<trainingCount;i++)
		{
			Train(0,0,0);
			Train(0,1,1);
			Train(1,0,1);
			Train(1,1,1);
		}
*/
		/*
		// AND
		for (int i=0;i<trainingCount;i++)
		{
			Train(0,0,0);
			Train(0,1,0);
			Train(1,0,0);
			Train(1,1,1);
		}*/
		
		System.out.println("0,0 = " + run(new float[]{0, 0})[0]);
		System.out.println("0,1 = " + run(new float[]{0, 1})[0]);
		System.out.println("1,0 = " + run(new float[]{1, 0})[0]);
		System.out.println("1,1 = " + run(new float[]{1, 1})[0]);
	}
}
