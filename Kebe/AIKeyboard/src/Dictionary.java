import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

/**
 * 
 * @author Fabian Mathews
 * 
 * Creates a dictionary of terminology used by the user
 * stores this as a tree such that partial words
 * can be extrapolated in to potential words the user
 * is typing
 */
public class Dictionary
{
	static class Node implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 6377397956801565930L;
		
		static class NodeCountSort implements Comparator<Node>
		{
			@Override
			public int compare(Node n1, Node n2)
			{
				return n2.mCount - n1.mCount;
			}
		}
		
		Node()
		{
		}
		
		Node(Character character)
		{
			mCharacter = character;
		}
		
		public void add(String word, int index)
		{
			if (index >= word.length())
				return;
			
			char key = word.charAt(index);
			Node node = mChildMap.get(key);
			if (node == null)
			{
				node = new Node(key);
				mChildMap.put(key, node);
			}
			
			++node.mCount;
			++index;
			node.add(word, index);
		}
		
		public Node getNode(String word, int index)
		{
			if (index >= word.length())
				return this;
			
			char key = word.charAt(index);
			Node node = mChildMap.get(key);
			if (node == null)
				return null;
			
			++index;
			return node.getNode(word, index);
		}
		
		public void getCompletionPredictions(String prefix, List<String> predictionsList, Node root)
		{
			Collection<Node> nodeCollection = mChildMap.values();
			
			// only submit this sub-word as a possible prediction if its has multiple children
			// (i.e. this node is a branching point for multiple words)
			// how ever, leaf nodes (i.e. full words) should be appended
			if (root != this && (nodeCollection.size() > 1 || nodeCollection.size() == 0))
			{
				String prediction = prefix + mCharacter;
				predictionsList.add(prediction);
			}
			
			List<Node> nodeList = new ArrayList<Node>(nodeCollection);
			Collections.sort(nodeList, new NodeCountSort());
			
			String newPrefix = prefix;
			if (root != this)
				newPrefix += mCharacter;
			
			for (int i = 0; i < nodeList.size(); ++i)
			{
				Node node = nodeList.get(i);
				node.getCompletionPredictions(newPrefix, predictionsList, root);
			}
		}
		
		public void getDictionaryListFromTree(String prefix, DictionaryList list, Node root)
		{
			String newPrefix = prefix;
			if (root != this)
				newPrefix += mCharacter;
			
			if (root != this && mChildMap.size() <= 0)
			{
				list.add(newPrefix);
			}
			
			Iterator<Map.Entry<Character, Node>> it = mChildMap.entrySet().iterator();
		    while (it.hasNext())
		    {
		        Map.Entry<Character, Node> pairs = it.next();
		        pairs.getValue().getDictionaryListFromTree(newPrefix, list, root);
		    }
		}
		
		public Character			mCharacter		= null;
		public int					mCount			= 0;
		public Map<Character, Node> mChildMap		= new HashMap<Character, Node>();
	}
	
	static class DictionaryTree extends Node implements Serializable 
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 556787798817960695L;

		public void add(String word)
		{
			add(word, 0);
		}
		
		public List<String> getCompletionPredictions(String word)
		{
			List<String> predictions = new Vector<String>();
			
			Node node = getNode(word, 0);
			if (node == this || node == null)
				return predictions;
			
			node.getCompletionPredictions(word, predictions, node);
			return predictions;
		}
		
		public DictionaryList getDictionaryListFromTree()
		{
			DictionaryList list = new DictionaryList();
			getDictionaryListFromTree("", list, this);
			return list;
		}
		
		public void add(DictionaryList list)
		{
			Iterator<String> it = list.iterator();
			while (it.hasNext())
			{
				String word = it.next();
				add(word);
			}
		}
	}
	
	// should this also contain a number of uses/hash for the string?
	static class DictionaryList extends TreeSet<String>
	{	
		/**
		 * 
		 */
		private static final long serialVersionUID = -5960156620721423290L;

		public void add(DictionaryList list)
		{
			Iterator<String> it = list.iterator();
			while (it.hasNext())
			{
				String word = it.next();
				add(word);
			}
		}
		
		public void serializeFromFile(String filename)
		{	
			try
			{
				File file = new File(filename);
				if (!file.exists())
					return;
				
				FileInputStream fstream = new FileInputStream(file);
				DataInputStream in = new DataInputStream(fstream);
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String strLine;
				while ((strLine = br.readLine()) != null)
				{
					add(strLine);
				}

				in.close();
			}
			catch (Exception e)
			{
			  e.printStackTrace();
			}
		}
	}
	
	public boolean serializeFromFile(String filename)
	{
		try
		{
			File file = new File(filename);
			if (!file.exists())
				return false;
			
			FileInputStream fileIn = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			mTree = (DictionaryTree) in.readObject();
			mList = mTree.getDictionaryListFromTree();
			in.close();
			fileIn.close();
		}
		catch(Exception c)
        {
            c.printStackTrace();
            return false;
        }
		
		return true;
	}
	
	public boolean serializeToFile(String filename)
	{
		try
		{
			FileOutputStream fileOut = new FileOutputStream(filename);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(mTree);
			out.close();
			fileOut.close();
		}
		catch(Exception i)
		{
			i.printStackTrace();
			return false;
		}
		
		return true;
	}
		
	public DictionaryList getList()
	{
		return mList;
	}
	
	public void add(String word)
	{
		mTree.add(word);
		mList.add(word);
	}
	
	public void add(DictionaryList list)
	{
		mTree.add(list);
		mList.add(list);
	}
	
	public List<String> getCompletionPredictions(String word)
	{
		return mTree.getCompletionPredictions(word);
	}
	
	public boolean contains(String word)
	{
		return mList.contains(word);
	}
	
	protected DictionaryTree mTree = new DictionaryTree();
	protected DictionaryList mList = new DictionaryList();
}
