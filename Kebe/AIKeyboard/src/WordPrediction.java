import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Fabian Mathews
 *
 * Uses AI to predict whole words
 * based on previous words in the sentence the user is typing
 */
public class WordPrediction
{
	static class WordPredictionInternal implements Serializable 
	{
		protected NNet 			mNet;
		
		WordPredictionInternal()
		{
			mNet = new NNet();
			NNet.NeuronLayer[] layer = new NNet.NeuronLayer[2];
			layer[0] = new NNet.NeuronLayer(10); // input layer
			layer[1] = new NNet.NeuronLayer(100); // middle hidden layer
			mNet.create(10, layer);
		}
		
		public void train(String[] sentence, String result)
		{
			float[] inputs = new float[sentence.length];
			for (int i = 0; i < sentence.length; ++i)
			{
				inputs[i] = sentence[i].hashCode();
			}
			
			// not sure a single output is good, maybe have an array which of n outputs
			// where there are n words to be predicted
			float expectedResults[] = new float[1];
			expectedResults[0] = result.hashCode();
			
			float[] output = mNet.train(inputs, expectedResults);
			
			//System.out.println("\toutput: " + output + " expected: " + expectedResult + " error: " + (expectedResult - output));
		}
		
		public float[] test_getResultArray(String result, String[] dictionaryList)
		{
			float[] resultList = new float[dictionaryList.length];
			for (int i = 0; i < dictionaryList.length; ++i)
			{
				resultList[i] = 0.f;
				if (result == dictionaryList[i])
				{
					resultList[i] = 1.f;
				}
			}
			
			return resultList;
		}
		
		public float[] test_train(float[] inputs, float[] expectedOutputs)
		{
			float[] output = mNet.train(inputs, expectedOutputs);
			return output;
		}
		
		public float[] test_getInputArray(String[] inputs, String[] dictionaryList)
		{
			// complex input test
			if (complexInput_test)
			{
				float[] results = new float[10];
				for (int i = 0; i < inputs.length; ++i)
				{
					results[i] = inputs[i].hashCode();
				}
				return results;
			}
			else
			{
				// binary input test
				float[] results = new float[10 * dictionaryList.length];
				for (int i = 0; i < 10 * dictionaryList.length; ++i)
				{
					results[i] = 0.f;
				}
				
				for (int j = 0; j < inputs.length; ++j)
				{
					String str = inputs[inputs.length - (j + 1)];
					for (int i = 0; i < dictionaryList.length; ++i)
					{
						if (str == dictionaryList[i])
							results[(j * 10) + i] = 1.f;
					}
				}
				
				return results;
			}
		}
		
		public String test_interpretResults(float[] results, String[] dictionaryList)
		{
			int index = 0;
			float max = results[0];
			for (int i = 0; i < results.length; ++i)
			{
				if (results[i] > max)
				{
					index = i;
					max = results[i];
				}
			}
			
			return dictionaryList[index];
		}
		
		public void test()
		{
			String[] dictionaryList = {"the", "cat", "jumped", "over", "fat", "brown", "dog", "ate", "stupid"};
			Integer[] hashList = new Integer[dictionaryList.length];
			for (int i = 0; i < dictionaryList.length; ++i)
			{
				hashList[i] = dictionaryList[i].hashCode();
			}
			
			mNet = new NNet();
			if (complexInput_test)
			{
				NNet.NeuronLayer[] layer = new NNet.NeuronLayer[2];
				layer[0] = new NNet.NeuronLayer(10); // middle layer
				layer[1] = new NNet.NeuronLayer(dictionaryList.length); // output
				mNet.create(10, layer);
			}
			else
			{
				NNet.NeuronLayer[] layer = new NNet.NeuronLayer[2];
				layer[0] = new NNet.NeuronLayer(10 * dictionaryList.length); // middle layer
				layer[1] = new NNet.NeuronLayer(dictionaryList.length); // output
				mNet.create(10 * dictionaryList.length, layer);
			}
			
			// this takes binary inputs and returns binary outputs! damnit!
			// 10 words * 10 dictionarywords = 100 inputs
			int trainingIterations = 100;
			for (int t = 0; t < trainingIterations; ++t)
			{
				System.out.println("Training Iteration: " + t);
					
				String[] s1 = {"the", "fat", "cat", "jumped", "the", "brown"};
				float[] expectedResults = test_getResultArray("dog", dictionaryList);
				float[] results = test_train(test_getInputArray(s1, dictionaryList), expectedResults);
				System.out.println("Outputs: " + test_interpretResults(results, dictionaryList) + " -  dog");
				
				String[] s2 = {"the", "cat", "ate", "the"};
				expectedResults = test_getResultArray("dog", dictionaryList);
				results = test_train(test_getInputArray(s2, dictionaryList), expectedResults);
				System.out.println("Outputs: " + test_interpretResults(results, dictionaryList) + " -  dog");
				
				String[] s3 = {"dogs", "are", "like"};
				expectedResults = test_getResultArray("stupid", dictionaryList);
				results = test_train(test_getInputArray(s3, dictionaryList), expectedResults);
				System.out.println("Outputs: " + test_interpretResults(results, dictionaryList) + " -  stupid");
				
				String[] s4 = {"there"};
				expectedResults = test_getResultArray("are", dictionaryList);
				results = test_train(test_getInputArray(s4, dictionaryList), expectedResults);
				System.out.println("Outputs: " + test_interpretResults(results, dictionaryList) + " -  are");
			}
			
			int nothing = 0;
			++nothing;
		}
		
		
		public Float[] getDesiredResultArray(String result)
		{
			Float[] resultList = new Float[mList.size()];
			Iterator<String> it = mList.iterator();
			int i = 0;
			while (it.hasNext())
			{
				resultList[i] = 0.f;
				if (it.next() == result)
					resultList[i] = 1.f;
				
				++i;
			}
			
			return resultList;
		}
		
		public float[] trainSentence(float[] inputs, float[] desiredResults)
		{
			float[] output = mNet.train(inputs, desiredResults);
			return output;
		}
		
		// TODO: change to a list
		public String interpretResults(float[] results)
		{
			int index = 0;
			float max = results[0];
			for (int i = 0; i < results.length; ++i)
			{
				if (results[i] > max)
				{
					index = i;
					max = results[i];
				}
			}
			
			return (String) mList.toArray()[index];
		}
		
		public void learnSentence(String sentence)
		{
			// make lower case, remove all non alpha numeric characters
			// and tokenise by spaces
			sentence = sentence.toLowerCase();
			sentence = sentence.replaceAll("[^a-zA-Z0-9 ]", "");
			String[] result = sentence.split("\\s");
			
			if (!complexInput_test)
			{
				List<Float> inputs = new Vector<Float>(); // = new float[10 * mList.size()];
				for (int i = 1; i < result.length; ++i)
				{
					for (int j = 0; j < 10; ++j)
					{
						if ((i - (j + 1)) <= 0)
							break;
						
						Float[] inputResultAray = getDesiredResultArray(result[i - (j + 1)]);
						
						Collection<Float> appendResult = Arrays.asList(inputResultAray);
						inputs.addAll(appendResult);
						//inputs[j] = result[i - (j + 1)].hashCode();
					}
					
					// sentence can now be learned
					
					Float[] desiredResults = getDesiredResultArray(result[i]);
					float[] fudgedResults = new float[desiredResults.length];
					for (int f = 0; f < desiredResults.length; ++f)
					{
						fudgedResults[f] = desiredResults[f];
					}
					
					float[] fudgedInputs = new float[inputs.size()];
					for (int f = 0; f < inputs.size(); ++f)
					{
						fudgedInputs[f] = inputs.get(f);
					}
					
					float[] results = trainSentence(fudgedInputs, fudgedResults);
					String bestResult = interpretResults(results);
					System.out.println(bestResult + "\t\t" + result[i]);
				}
			}
			else
			{/*
				//int count = Math.min(result.length, 10);
				float[] inputs = new float[10];
				for (int i = 1; i < result.length; ++i)
				{
					for (int j = 0; j < 10; ++j)
					{
						if ((i - (j + 1)) <= 0)
							break;
						
						inputs[j] = result[i - (j + 1)].hashCode();
					}
					
					// sentence can now be learned
					float[] desiredResults = getDesiredResultArray(result[i]);
					float[] results = trainSentence(inputs, desiredResults);
					String bestResult = interpretResults(results);
					System.out.println(bestResult + "\t" + result[i]);
				}*/
			}
		}
		

		public Float[] getDesiredResultArray(String result, Set<String> dictionary)
		{
			Float[] resultList = new Float[dictionary.size()];
			Iterator<String> it = dictionary.iterator();
			int i = 0;
			while (it.hasNext())
			{
				resultList[i] = 0.f;
				if (it.next().equals(result))
					resultList[i] = new Float(1.f);
				
				++i;
			}
			
			return resultList;
		}
		
		public List<String> interpretResults(float[] results, Set<String> dictionary)
		{
			int index = -1;
			int index2 = -1;
			int index3 = -1;
			float max = Float.MIN_NORMAL;
			float max2 = max;
			float max3 = max;
			for (int i = 0; i < results.length; ++i)
			{
				if (results[i] >= max)
				{
					max3 = max2;
					index3 = index2;
					
					max2 = max;
					index2 = index;
					
					index = i;
					max = results[i];
				}
			}
			
			List<String> topList = new Vector<String>();
			
			if (index >= 0)
				topList.add((String) dictionary.toArray()[index]);
			
			if (index2 >= 0)
				topList.add((String) dictionary.toArray()[index2]);
			
			if (index3 >= 0)
				topList.add((String) dictionary.toArray()[index3]);
			
			return topList;
		}
		
		public void test1() throws Exception
		{
			List<String> sentenceList = new Vector<String>();
			Set<String> dictionary = new HashSet();
			//mList = new Dictionary.DictionaryList();
			String sentence = "";
			
			String file = "ai_sentences.txt";
			BufferedReader in = new BufferedReader(new FileReader(file));
			Pattern p = Pattern.compile("\\w+");
			for (String temp = ""; temp != null; temp = in.readLine())
			{
				int index = temp.indexOf('.');
				if (index < 0)
				{
					sentence += temp;
				}
				else
				{
					sentence += temp.substring(0, index);
					
					sentence = sentence.toLowerCase();
					sentence = sentence.replaceAll("[^a-zA-Z0-9 ]", "");
					
					// learn the sentence
					sentenceList.add(sentence);
					
					// add words to dictionary
					String[] result =  sentence.split("\\s");
					for (int i = 0; i < result.length; ++i)
					{
						//mList.add(result[i]);
						dictionary.add(result[i]);
					}
					
					// start the new sentence
					if (index < temp.length())
						sentence = temp.substring(index + 1, temp.length());
				}
			}
			in.close();
			
			int dictionarySize = dictionary.size();
			int maxSentenceLength = 10;
			int inputSize = dictionarySize * maxSentenceLength;
			
			/*
			// data has been loaded time for some training runs
			NNet.NeuronLayer[] layer = new NNet.NeuronLayer[2];
			layer[0] = new NNet.NeuronLayer(inputSize / 2); // middle layer
			layer[1] = new NNet.NeuronLayer(dictionary.size()); // output
			mNet.create(inputSize, layer);
			*/
			
			
			NNet.NeuronLayer[] layer = new NNet.NeuronLayer[1];
			layer[0] = new NNet.NeuronLayer(dictionary.size()); // output
			mNet.create(inputSize, layer);
			
			boolean reverseSentence = true;
			
			int correctCount = 0;
			int incorrectCount = 0;
			int traningIterations = 10;
			for (int ti = 0; ti < traningIterations; ++ti)
			{
				System.out.println("\n----------------Training Iteration " + ti + "-----------------------------------\n"); 
				// time for some learning
				for (int sli = 0; sli < sentenceList.size(); ++sli)
				{
					// make lower case, remove all non alpha numeric characters
					// and tokenise by spaces
					sentence = sentenceList.get(sli).toLowerCase();
					sentence = sentence.replaceAll("[^a-zA-Z0-9 ]", "");
					String[] result = sentence.split("\\s");
					
					List<Float> inputs = new Vector<Float>();
					for (int i = 1; i < result.length; ++i)
					{
						inputs.clear();
						String partialSentence = "";
						for (int j = 0; j < maxSentenceLength; ++j)
						{
							int wordIndex = (i - maxSentenceLength) + j;
							if (reverseSentence)
							{
								wordIndex = i - (j + 1);
							}
							
							if (wordIndex < 0 || wordIndex >= result.length)
							{
								continue;
							}
							
							partialSentence += result[wordIndex] + ",";
							
							Float[] inputResultAray = getDesiredResultArray(result[wordIndex], dictionary);
							
							Collection<Float> appendResult = Arrays.asList(inputResultAray);
							inputs.addAll(appendResult);
							//inputs[j] = result[i - (j + 1)].hashCode();
						}
						
						// sentence can now be learned
						
						Float[] desiredResults = getDesiredResultArray(result[i], dictionary);
						float[] fudgedResults = new float[desiredResults.length];
						for (int f = 0; f < desiredResults.length; ++f)
						{
							fudgedResults[f] = desiredResults[f];
						}
						
						float[] fudgedInputs = new float[inputSize];
						for (int f = 0; f < inputs.size(); ++f)
						{
							try
							{
								fudgedInputs[f] = inputs.get(f);
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
						}
						
						float[] results = trainSentence(fudgedInputs, fudgedResults);
						List<String> bestResults = interpretResults(results, dictionary);
						System.out.println(ti + "   --------------------------------------------------"); 
						System.out.println("      Sentence        : " + partialSentence); 
						System.out.println("      Next word       : " + result[i]);
						System.out.print("      AI prediction   : ");
						
						boolean correct = false;
						for (int bri = 0; bri < bestResults.size(); ++bri)
						{
							System.out.print(bestResults.get(bri) + ",");
							if (bestResults.get(bri).equals(result[i]))
							{
								correct = true;
							}
						}
						
						System.out.println("");
						
						if (correct)
						{
							++correctCount;
						}
						else
						{
							++incorrectCount;
						}
					}
				}
			}
			
			System.out.println("\n\nTraining complete"); 
			System.out.println("Incorrect count: " + incorrectCount); 
			System.out.println("Correct count: " + correctCount); 
			float percentCorrect = (float)(correctCount) / (float)(correctCount + incorrectCount) * 100.f;
			System.out.println("Correct percent: " + percentCorrect); 
		}
		

		public void test2() throws Exception
		{
			List<String> sentenceList = new Vector<String>();
			Set<String> dictionary = new HashSet();
			//mList = new Dictionary.DictionaryList();
			String sentence = "";
			
			dictionary.add("can");
			dictionary.add("i");
			dictionary.add("you");
			dictionary.add("we");
			dictionary.add("have");
			
			//sentenceList.add("can i");
			//sentenceList.add("can you");
			sentenceList.add("can we");
			//sentenceList.add("can have");
			
			//sentenceList.add("can i");
			//sentenceList.add("can you");
			//sentenceList.add("can we");
			
			//sentenceList.add("can i");
			//sentenceList.add("can you");
			
			//sentenceList.add("can i");
			
			
			int dictionarySize = dictionary.size();
			int maxSentenceLength = 1;
			int inputSize = dictionarySize * maxSentenceLength;
			

			
			NNet.NeuronLayer[] layer = new NNet.NeuronLayer[1];
			layer[0] = new NNet.NeuronLayer(dictionary.size()); // output
			mNet.create(inputSize, layer, 0.f, 0.f);
			
			boolean reverseSentence = true;
			
			int correctCount = 0;
			int incorrectCount = 0;
			int traningIterations = 10;
			for (int ti = 0; ti < traningIterations; ++ti)
			{
				System.out.println("\n----------------Training Iteration " + ti + "-----------------------------------\n"); 
				// time for some learning
				for (int sli = 0; sli < sentenceList.size(); ++sli)
				{
					// make lower case, remove all non alpha numeric characters
					// and tokenise by spaces
					sentence = sentenceList.get(sli).toLowerCase();
					sentence = sentence.replaceAll("[^a-zA-Z0-9 ]", "");
					String[] result = sentence.split("\\s");
					
					List<Float> inputs = new Vector<Float>();
					for (int i = 1; i < result.length; ++i)
					{
						inputs.clear();
						String partialSentence = "";
						for (int j = 0; j < maxSentenceLength; ++j)
						{
							int wordIndex = (i - maxSentenceLength) + j;
							if (reverseSentence)
							{
								wordIndex = i - (j + 1);
							}
							
							if (wordIndex < 0 || wordIndex >= result.length)
							{
								continue;
							}
							
							partialSentence += result[wordIndex] + ",";
							
							Float[] inputResultAray = getDesiredResultArray(result[wordIndex], dictionary);
							
							Collection<Float> appendResult = Arrays.asList(inputResultAray);
							inputs.addAll(appendResult);
							//inputs[j] = result[i - (j + 1)].hashCode();
						}
						
						// sentence can now be learned
						
						Float[] desiredResults = getDesiredResultArray(result[i], dictionary);
						float[] fudgedResults = new float[desiredResults.length];
						for (int f = 0; f < desiredResults.length; ++f)
						{
							fudgedResults[f] = desiredResults[f];
						}
						
						float[] fudgedInputs = new float[inputSize];
						for (int f = 0; f < inputs.size(); ++f)
						{
							try
							{
								fudgedInputs[f] = inputs.get(f);
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
						}
						
						float[] results = trainSentence(fudgedInputs, fudgedResults);
						List<String> bestResults = interpretResults(results, dictionary);
						System.out.println(ti + "   --------------------------------------------------"); 
						System.out.println("      Sentence        : " + partialSentence); 
						System.out.println("      Next word       : " + result[i]);
						System.out.print("      AI prediction   : ");
						
						boolean correct = false;
						for (int bri = 0; bri < bestResults.size(); ++bri)
						{
							System.out.print(bestResults.get(bri) + ",");
							if (bestResults.get(bri).equals(result[i]))
							{
								correct = true;
							}
						}
						
						System.out.println("");
						
						if (correct)
						{
							++correctCount;
						}
						else
						{
							++incorrectCount;
						}
					}
				}
			}
			
			System.out.println("\n\nTraining complete"); 
			System.out.println("Incorrect count: " + incorrectCount); 
			System.out.println("Correct count: " + correctCount); 
			float percentCorrect = (float)(correctCount) / (float)(correctCount + incorrectCount) * 100.f;
			System.out.println("Correct percent: " + percentCorrect); 
		}
		

		public void test3() throws Exception
		{
			List<String> sentenceList = new Vector<String>();
			Set<String> dictionary = new HashSet();
			//mList = new Dictionary.DictionaryList();
			String sentence = "";
			
			dictionary.add("can");
			dictionary.add("i");
			dictionary.add("you");
			dictionary.add("we");
			dictionary.add("have");
			
			//sentenceList.add("can i");
			//sentenceList.add("can you");
			sentenceList.add("can we");
			//sentenceList.add("can have");
			
			//sentenceList.add("can i");
			//sentenceList.add("can you");
			//sentenceList.add("can we");
			
			//sentenceList.add("can i");
			//sentenceList.add("can you");
			
			//sentenceList.add("can i");
			
			
			int dictionarySize = dictionary.size();
			int maxSentenceLength = 1;
			int inputSize = dictionarySize * maxSentenceLength;
			

			
			NNet.NeuronLayer[] layer = new NNet.NeuronLayer[1];
			layer[0] = new NNet.NeuronLayer(dictionary.size()); // output
			mNet.create(inputSize, layer, 0.f, 0.f);
			
			boolean reverseSentence = true;
			
			int correctCount = 0;
			int incorrectCount = 0;
			int traningIterations = 10;
			for (int ti = 0; ti < traningIterations; ++ti)
			{
				System.out.println("\n----------------Training Iteration " + ti + "-----------------------------------\n"); 
				// time for some learning
				for (int sli = 0; sli < sentenceList.size(); ++sli)
				{
					// make lower case, remove all non alpha numeric characters
					// and tokenise by spaces
					sentence = sentenceList.get(sli).toLowerCase();
					sentence = sentence.replaceAll("[^a-zA-Z0-9 ]", "");
					String[] result = sentence.split("\\s");
					
					List<Float> inputs = new Vector<Float>();
					for (int i = 1; i < result.length; ++i)
					{
						inputs.clear();
						String partialSentence = "";
						for (int j = 0; j < maxSentenceLength; ++j)
						{
							int wordIndex = (i - maxSentenceLength) + j;
							if (reverseSentence)
							{
								wordIndex = i - (j + 1);
							}
							
							if (wordIndex < 0 || wordIndex >= result.length)
							{
								continue;
							}
							
							partialSentence += result[wordIndex] + ",";
							
							Float[] inputResultAray = getDesiredResultArray(result[wordIndex], dictionary);
							
							Collection<Float> appendResult = Arrays.asList(inputResultAray);
							inputs.addAll(appendResult);
							//inputs[j] = result[i - (j + 1)].hashCode();
						}
						
						// sentence can now be learned
						
						Float[] desiredResults = getDesiredResultArray(result[i], dictionary);
						float[] fudgedResults = new float[desiredResults.length];
						for (int f = 0; f < desiredResults.length; ++f)
						{
							fudgedResults[f] = desiredResults[f];
						}
						
						float[] fudgedInputs = new float[inputSize];
						for (int f = 0; f < inputs.size(); ++f)
						{
							try
							{
								fudgedInputs[f] = inputs.get(f);
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
						}
						
						float[] results = trainSentence(fudgedInputs, fudgedResults);
						List<String> bestResults = interpretResults(results, dictionary);
						System.out.println(ti + "   --------------------------------------------------"); 
						System.out.println("      Sentence        : " + partialSentence); 
						System.out.println("      Next word       : " + result[i]);
						System.out.print("      AI prediction   : ");
						
						boolean correct = false;
						for (int bri = 0; bri < bestResults.size(); ++bri)
						{
							System.out.print(bestResults.get(bri) + ",");
							if (bestResults.get(bri).equals(result[i]))
							{
								correct = true;
							}
						}
						
						System.out.println("");
						
						if (correct)
						{
							++correctCount;
						}
						else
						{
							++incorrectCount;
						}
					}
				}
			}
			
			System.out.println("\n\nTraining complete"); 
			System.out.println("Incorrect count: " + incorrectCount); 
			System.out.println("Correct count: " + correctCount); 
			float percentCorrect = (float)(correctCount) / (float)(correctCount + incorrectCount) * 100.f;
			System.out.println("Correct percent: " + percentCorrect); 
		}
		
		public void loadTrainingSet(String file, Dictionary.DictionaryList list)
		{
			try
			{
				if (!complexInput_test)
				{
					NNet.NeuronLayer[] layer = new NNet.NeuronLayer[2];
					layer[0] = new NNet.NeuronLayer(10 * list.size()); // middle layer
					layer[1] = new NNet.NeuronLayer(list.size()); // output
					mNet.create(10 * list.size(), layer);
				}
				else
				{
					NNet.NeuronLayer[] layer = new NNet.NeuronLayer[2];
					layer[0] = new NNet.NeuronLayer(list.size()); // middle layer
					layer[1] = new NNet.NeuronLayer(list.size()); // output
					mNet.create(10, layer);
				}
				
				mList = list;
				list.serializeFromFile("words.txt");
				//String[] dictionary = list.toArray();
				String sentence = "";
				
				BufferedReader in = new BufferedReader(new FileReader(file));
				Pattern p = Pattern.compile("\\w+");
				for (String temp = ""; temp != null; temp = in.readLine())
				{
					int index = temp.indexOf('.');
					if (index < 0)
					{
						sentence += temp;
					}
					else
					{
						sentence += temp.substring(0, index);
						
						// learn the sentence
						learnSentence(sentence);
						
						// start the new sentence
						if (index < temp.length())
							sentence = temp.substring(index + 1, temp.length());
					}
					
					/*
					String[] result =  temp.split("\\s");
					
					Matcher m = p.matcher(temp.toLowerCase());
					while(m.find())
					{
						nWords.put((temp = m.group()), nWords.containsKey(temp) ? nWords.get(temp) + 1 : 1);
					}*/
				}
				in.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		
		
		public boolean complexInput_test = false;

		/**
		 * 
		 */
		private static final long serialVersionUID = -2989183936797868241L;
		
		protected Dictionary.DictionaryList mList;
	}
	
	protected WordPredictionInternal mInternal = new WordPredictionInternal();
	
	public void train(String[] sentence, String result)
	{
		mInternal.train(sentence, result);
	}
	
	public void loadTrainingSet(String filename, Dictionary.DictionaryList list)
	{
		mInternal.loadTrainingSet(filename, list);
	}
	
	public void test()
	{
		mInternal.test();
	}
	
	public void test1()
	{
		try {
			mInternal.test1();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public void test2()
	{
		try {
			mInternal.test2();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	


	public void test3()
	{
		try {
			mInternal.test3();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<String> getPredictions(String[] words)
	{
		for (int i = 0; i < words.length; ++i)
		{
			int hash = words[i].hashCode();
		}
		return null;
	}
	
	public void serializeFromFile(String filename)
	{
		try
		{
			FileInputStream fileIn = new FileInputStream(filename);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			mInternal = (WordPredictionInternal) in.readObject();
			in.close();
			fileIn.close();
		}
		catch(Exception c)
        {
            c.printStackTrace();
            return;
        }
	}
	
	public void serializeToFile(String filename)
	{
		try
		{
			FileOutputStream fileOut = new FileOutputStream(filename);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(mInternal);
			out.close();
			fileOut.close();
		}
		catch(Exception i)
		{
			i.printStackTrace();
		}
	}
}
