package Kebe.Android;

import java.io.BufferedReader;
import java.util.List;
import java.util.Vector;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.ImageButton;

public class Key
{
	enum SystemKey
	{
		UseKeyText,
		DeleteReturn,
		SpaceComma, // this should be comma or ?
		EmoteSpeech
	}
	
	enum State
	{
		Up,
		Down,
	}
	
	public Key()
	{
		
	}
	
	public Key(Button button)
	{
		mButton = button;
		mSystemKey = SystemKey.UseKeyText;
	}
	
	public Key(ImageButton imageButton, SystemKey systemKey)
	{
		mImageButton = imageButton;
		mSystemKey = systemKey;
	}
	
	public SystemKey getSystemKey()
	{
		return mSystemKey;
	}
	
	public CharSequence getText()
	{
		if (mButton == null)
			return null;
		
		return mButton.getText();
	}
	
	public void draw(RectF rect, Canvas canvas, Paint paint)
	{
		Rect internalRect = new Rect();
		internalRect.top = (int) ((mRect.top * rect.bottom) + rect.top);
		internalRect.left = (int) ((mRect.left * rect.right) + rect.left);
		internalRect.bottom = (int) ((mRect.bottom * rect.bottom) + rect.top);
		internalRect.right = (int) ((mRect.right * rect.right) + rect.left);
		
		// this is unrelated to actual key zoom,
		// just a visual effect to help indicate what key i pressed
		if (mState == State.Down)
		{
			int halfZoomAmount = 4;
			internalRect.left -= halfZoomAmount;
			internalRect.top -= halfZoomAmount;
			internalRect.bottom += halfZoomAmount;
			internalRect.right += halfZoomAmount;
		}
		
		Theme.Bitmap.draw(mBackground[mState.ordinal()], internalRect, canvas, paint);

		if (!Theme.Bitmap.draw(mContent[mState.ordinal()], internalRect, canvas, paint))
		{
			int fontPontSize = 20;
			paint.setARGB(255, 255, 255, 255);
			paint.setTextSize(fontPontSize);
			//paint.setTypeface(Typeface.c); // sets font
			float midX = internalRect.left + ((internalRect.right - internalRect.left) / 2);
			float midY = internalRect.top + ((internalRect.bottom - internalRect.top) / 2) + (fontPontSize / 2);
			paint.setTextAlign(Paint.Align.CENTER);
			canvas.drawText(mPrimaryAction, midX, midY, paint);
			
			if (mSecondAction != null)
			{
				fontPontSize = 15;
				float x = midX + (midX / 2.f);
				float y = midY - (midY / 2.f);
				
				paint.setTextSize(fontPontSize);
				paint.setARGB(255, 128, 128, 128);
				canvas.drawText(mSecondAction, x, y, paint);
			}
		}
	}
	
	public void serializeFromFile(BufferedReader in) throws Exception
	{
		for (String line = ""; line != null; line = in.readLine())
		{
			line = line.trim();
			
			if (line.equals("}"))
				return;
			
			// manual split on first equals
			int equalsIndex = line.indexOf('=');
			if (equalsIndex == -1)
				continue;
			
			String[] keyValue = new String[2];
			keyValue[0] = line.substring(0, equalsIndex).toLowerCase();
			keyValue[1] = line.substring(equalsIndex + 1, line.length());
			
			if (keyValue.length != 2)
				continue;
			
			String key = keyValue[0].trim();
			String value = keyValue[1].trim();
			
			if (key.equals("primary_action"))
				mPrimaryAction = value;
			
			if (key.equals("second_action"))
				mSecondAction = value;
			
			if (key.equals("x"))
				mRect.left = Integer.parseInt(value);
			
			if (key.equals("y"))
				mRect.top = Integer.parseInt(value);
			
			if (key.equals("width"))
				mRect.right = mRect.left + Integer.parseInt(value);
			
			if (key.equals("height"))
				mRect.bottom = mRect.top + Integer.parseInt(value);
			
			if (key.equals("background_up"))
				mBackground[State.Up.ordinal()] = new Theme.Bitmap(value);
			
			if (key.equals("background_down"))
				mBackground[State.Down.ordinal()] = new Theme.Bitmap(value);
			
			if (key.equals("content_up"))
				mContent[State.Up.ordinal()] = new Theme.Bitmap(value);
			
			if (key.equals("content_down"))
				mContent[State.Down.ordinal()] = new Theme.Bitmap(value);
		}
	}
	
	public void setTheme(Theme theme)
	{
		for (int i = 0; i < 2; ++i)
		{
			if (mBackground[i] != null)
				mBackground[i].setTheme(theme);
			
			if (mContent[i] != null)
				mContent[i].setTheme(theme);
		}
	}
	
	List<String> suggestionViewList()
	{
		Vector<String> list = new Vector<String>();
		if (mPrimaryAction != null)
		{
			list.add(mPrimaryAction);
		}
		if (mSecondAction != null)
		{
			list.add(mSecondAction);
		}
		return list;
		//return m_suggestionViewList;
	}
	
	public Button				mButton;
	public ImageButton			mImageButton;
	public SystemKey 			mSystemKey;
	
	public String				mPrimaryAction;
	public String				mSecondAction;
	public Theme.Bitmap[]		mBackground			= new Theme.Bitmap[2];
	public Theme.Bitmap[]		mContent			= new Theme.Bitmap[2];
	
	public State				mState				= State.Up;
	public Rect					mRect				= new Rect();
	public boolean				mZoom				= false;
	
	//public List<String>			m_suggestionViewList = new Vector<String>();
}
