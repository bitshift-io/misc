package Kebe.Android;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;

import Kebe.Android.Key.State;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;

public class Keyboard 
{	
	public Keyboard(KebeIMS service)
	{
		mService = service;
	}
	
	public void serializeFromFile(BufferedReader in) throws Exception
	{
		for (String line = ""; line != null; line = in.readLine())
		{
			line = line.toLowerCase().trim();
			
			if (line.equals("}"))
				return;
			
			if (line.equals("[key]"))
			{
				line = in.readLine();
				line = line.toLowerCase().trim();
				
				if (line.equals("{"))
				{
					Key key = new Key();
					key.serializeFromFile(in);
					mKeys.add(key);
				}
			}
			
			String[] keyValue = line.split("=");
			if (keyValue.length != 2)
				continue;
			
			String key = keyValue[0].trim();
			String value = keyValue[1].trim();
			
			if (key.equals("candidate_height"))
				mCandidateHeight = Integer.parseInt(value);
			
			if (key.equals("width"))
				mWidth = Integer.parseInt(value);
			
			if (key.equals("height"))
				mHeight = Integer.parseInt(value);
			
			if (key.equals("background"))
				mBackground = new Theme.Bitmap(value);
			
			if (key.equals("candidate_background"))
				mCandidateBackground = new Theme.Bitmap(value);
		}
	}
	
	public void setTheme(Theme theme)
	{
		if (mBackground != null)
			mBackground.setTheme(theme);
		
		if (mCandidateBackground != null)
			mCandidateBackground.setTheme(theme);
		
		for (int i = 0; i < mKeys.size(); ++i)
		{
			Key key = mKeys.get(i);
			key.setTheme(theme);
		}
	}
	
	public void drawKeyboard(Canvas canvas, Paint paint)
	{
		// top and left indicate offset, right and bottom represent scale
		RectF scale = new RectF();
		scale.top = mViewRect.top;
		scale.left = mViewRect.left;
		scale.right = (float)mViewRect.width() / (float)mWidth;
		scale.bottom = (float)mViewRect.height() / (float)mHeight;
		
		Theme.Bitmap.draw(mBackground, mViewRect, canvas, paint);
		
		for (int i = 0; i < mKeys.size(); ++i)
		{
			mKeys.get(i).draw(scale, canvas, paint);
		}
	}
	
	public void drawCandidates(Canvas canvas, Paint paint, List<String> candidates)
	{
		Theme.Bitmap.draw(mCandidateBackground, mViewRect, canvas, paint);
	}
	
	public Key getKey(MotionEvent event)
	{
		float x = event.getX();
		float y = event.getY();
		
		//convert from android view space to keyboard space
		x *= (float)mWidth / (float)mViewRect.width();
		y *= (float)mHeight / (float)mViewRect.height();
		
		// test zoomed keys first,
		// this doesn't affect the visuals as that looks ugly
		for (int i = 0; i < mKeys.size(); ++i)
		{
			Key key = mKeys.get(i);

			if (!key.mZoom)
				continue;
			
			Rect rect = key.mRect;
			
			int halfZoomAmount = 4;
			if (x > (rect.left - halfZoomAmount)  && x < (rect.right +halfZoomAmount) && y > (rect.top - halfZoomAmount) && y < (rect.bottom + halfZoomAmount))
				return key;
		}
		
		// now test normal keys
		for (int i = 0; i < mKeys.size(); ++i)
		{
			Key key = mKeys.get(i);
			
			if (key.mZoom)
				continue;
			
			Rect rect = key.mRect;
			if (x > rect.left && x < rect.right && y > rect.top && y < rect.bottom)
				return key;
		}
		
		return null;
	}
	
	public Key getKey(String primaryAction)
	{
		for (int i = 0; i < mKeys.size(); ++i)
		{
			Key key = mKeys.get(i);
			if (key.mPrimaryAction.equals(primaryAction))
				return key;
		}
		
		return null;
	}
	
	public void setViewRect(Rect rect)
	{
		mViewRect = rect;
	}
	
	public float aspectRatio()
	{
		return (float)mWidth / (float)mHeight;
	}
	
	public List<Key> 		mKeys = new Vector<Key>();
	public Theme.Bitmap		mBackground;
	public Theme.Bitmap		mCandidateBackground;
	public int				mCandidateHeight;
	
	// width and height are keyboard units
	// view width and height are actual android units
	// i scale automatically between the two
	// to seperate keyboard layout from androids
	// scale
	public int				mHeight;
	public int				mWidth;
	
	public Rect				mViewRect;

	
	public KebeIMS			mService;
}
