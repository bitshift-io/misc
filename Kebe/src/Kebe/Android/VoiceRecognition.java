package Kebe.Android;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;

// http://developer.android.com/resources/samples/ApiDemos/src/com/example/android/apis/app/VoiceRecognition.html
// http://developer.android.com/reference/android/speech/SpeechRecognizer.html#createSpeechRecognizer(android.content.Context)
public class VoiceRecognition implements RecognitionListener
{
	protected KebeIMS			mService;
	
	SpeechRecognizer	mRecognizer;
	
	ArrayList<String> mWordList;
	
	public VoiceRecognition(KebeIMS service)
	{
		mService = service;
		/*
		// Check to see if a recognition activity is present
        PackageManager pm = mService.getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(
                new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (activities.size() != 0) {
            speakButton.setOnClickListener(this);
        } else {
            speakButton.setEnabled(false);
            speakButton.setText("Recognizer not present");
        }

        // Most of the applications do not have to handle the voice settings. If the application
        // does not require a recognition in a specific language (i.e., different from the system
        // locale), the application does not need to read the voice settings.
        refreshVoiceSettings();*/
		
		mRecognizer = SpeechRecognizer.createSpeechRecognizer(mService.getApplicationContext());
		mRecognizer.setRecognitionListener(this);
		
		int nothing = 0;
		++nothing;
	}
	
	/**
     * Fire an intent to start the speech recognition activity.
     */
    public void start() 
    {
    	if (mRecognizer == null)
    	{
    		return;
    	}
    	
    	mRecognizer.startListening(new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH));
    	
    	/*
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        // Specify the calling package to identify your application
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass().getPackage().getName());

        // Display an hint to the user about what he should say.
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speech recognition demo");

        // Given an hint to the recognizer about what the user is going to say
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        // Specify how many results you want to receive. The results will be sorted
        // where the first result is the one with higher confidence.
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);

        // Specify the recognition language. This parameter has to be specified only if the
        // recognition has to be done in a specific language and not the default one (i.e., the
        // system locale). Most of the applications do not have to set this parameter.
        if (!mSupportedLanguageView.getSelectedItem().toString().equals("Default")) {
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                    mSupportedLanguageView.getSelectedItem().toString());
        }

        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);*/
    }
    
    public void stop()
    {
    	mRecognizer.stopListening();
    }

	public void onBeginningOfSpeech() 
	{
		// TODO Auto-generated method stub
		
	}

	public void onBufferReceived(byte[] buffer) 
	{
		// TODO Auto-generated method stub
		
	}

	public void onEndOfSpeech()
	{
		// TODO Auto-generated method stub
		
	}

	public void onError(int error) 
	{
		// TODO Auto-generated method stub
		
	}

	public void onEvent(int eventType, Bundle params) 
	{
		// TODO Auto-generated method stub
		
	}

	public void onPartialResults(Bundle partialResults) 
	{
		// TODO Auto-generated method stub
		
	}

	public void onReadyForSpeech(Bundle params) 
	{
		// TODO Auto-generated method stub
		
	}

	public void onResults(Bundle results) 
	{
		// TODO Auto-generated method stub
		
		mWordList = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
		
		int nothing = 0;
		++nothing;
	}

	public void onRmsChanged(float rmsdB) 
	{
		// TODO Auto-generated method stub
		
	}
}
