package Kebe.Android;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;

import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.text.method.MetaKeyKeyListener;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class KebeIMS extends InputMethodService implements KeyboardView.Listener
{
	class MessageHandler extends Handler
	{
		@Override
        public void handleMessage(Message msg)
        {
	        switch (msg.what)
	        {
            // Schedule a long press. This will be canceled if:
            //  * gesture finishes (ACTION_UP)
            //  * gesture is canceled (ACTION_CANCEL)
            //  * we move outside of our 'slop' range
            case MSG_LONG_PRESS:
            	mLongKeyPressed = true;
				longKeyPress(mCurrentKeyDown);
            	break;
 
            default:
            	throw new RuntimeException("handleMessage: unknown message " + msg);
	        }
        }
	}
	
	public Dictionary			mDictionary = new Dictionary();
	private static final int 	MSG_LONG_PRESS = 1;
	
	public KeyboardView 		mView;
	public Theme				mTheme;
	
	//public CandidateView mCandidateView;
	//private StringBuilder mComposingText = new StringBuilder();
	private char 				mPreviousDeletedCharacter = 0;
	protected long 				mMultiCharKeyTime = 300; // ms
	protected long 				mLongKeyTime = 300; // ms // this could be set per key
	protected long 				mKeyRepeatTime = 500; // msthis could be set per key
	protected boolean 			mLongKeyPressed = false;
	protected boolean 			mSwipeOccured = false;
	protected float 			mSwipeDistance = 10; // 50 pixels is 1 button
	
	protected Key 				mCurrentKeyDown;
	protected MotionEvent 		mTouchDownEvent;
	
	protected long				mPreviousReleasedKeyTime;
	protected Key				mPreviousReleasedKey;
	
	private MessageHandler 		mMessageHandler = new MessageHandler(); 
	private Vibrator 			mVibrator;
	
	private boolean				mDebug = false;//true;
	
	private int 				mKeyboardSwipeDistance = 200;
	
	protected long 				mVibrateTime = 0; //50;// ms
	
	protected VoiceRecognition	mVoiceRecognition;

	
	public KebeIMS()
	{
	}
	
	void waitForDebugger()
	{
		if (mDebug)
		{
			System.out.println("Waiting for debugger to attach...");
			android.os.Debug.waitForDebugger();
			System.out.println("Debugger attached");
			//mDebug = false;
		}
	}
	
	@Override
	public void onExtractedSelectionChanged(int start, int end)
	{
		waitForDebugger();
		
		//set the input cursor position to this selected position
		getCurrentInputConnection().setSelection(start, end);
	}
	/*
	@Override
	protected void startExtractingText()
	{
		
	}*/
	
	void longKeyPress(Key key)
	{
		if (key.mPrimaryAction.equals("delete"))
		{
			// iterate till we remove all spaces
	    	CharSequence existingText = getCurrentInputConnection().getTextBeforeCursor(100, 0);
	    	if (existingText.length() <= 0)
	    		return;
	    	
	    	int charsToDelete = 0;
	    	int endIndex = existingText.length() - 1;
	    	for (; endIndex >= 0; --endIndex)
	    	{
	    		if (existingText.charAt(endIndex) == ' ')
	    			break;

	    		++charsToDelete;
	    	}
	    	
	    	for (; endIndex >= 0; --endIndex)
	    	{
	    		if (existingText.charAt(endIndex) != ' ')
	    			break;

	    		++charsToDelete;
	    	}
			
			getCurrentInputConnection().deleteSurroundingText(charsToDelete, 0);
			
			// TODO: THIS appear not to be working
			// queue long key press check
			//mMessageHandler.removeMessages(MSG_LONG_PRESS);
			//mMessageHandler.sendEmptyMessageAtTime(MSG_LONG_PRESS,
	        //                System.currentTimeMillis() + mKeyRepeatTime);
			
			boolean result = mMessageHandler.sendEmptyMessageDelayed(MSG_LONG_PRESS,  mKeyRepeatTime);
			if (!result)
			{
				int nothing = 0; // error
			}
		}
	}
	
	public String wordBeforeCursor(boolean ignoreEndNonLetterChars)
	{
		CharSequence charSequence = getCurrentInputConnection().getTextBeforeCursor(100, 0);
		int end = charSequence.length() - 1;
		if (ignoreEndNonLetterChars)
		{
			for (; end >= 0; --end)
			{
				Character c = charSequence.charAt(end);
				if (Character.isLetter(c))
				{
					break;
				}
			}
		}
		++end;
		
		int start = end - 1;
		for (; start >= 0; --start)
		{
			Character c = charSequence.charAt(start);
			if (!Character.isLetter(c))
			{
				break;
			}
		}
		++start;
		
		String currentWord = charSequence.subSequence(start, end).toString();
		return currentWord;
	}
	
	public void updatePredictions()
	{
		Kebe.Android.Keyboard keyboard = mView.getCurrentKeyboard();
		if (keyboard == null)
			return;
		
		// reset zoomed keys
		for (int i = 0; i < keyboard.mKeys.size(); ++i)
		{
			Key key = keyboard.mKeys.get(i);
			key.mZoom = false;
		}
		
		// find predictions ONLY if the cursor is at the end of the line
		CharSequence afterCharSequence = getCurrentInputConnection().getTextAfterCursor(100, 0);
		if (afterCharSequence != null && afterCharSequence.length() <= 0)
		{
			String currentWord = wordBeforeCursor(false);
			
			// should this be passed in?
			// check for punctuation/special keys
			if (mCurrentKeyDown != null)
			{
				currentWord += mCurrentKeyDown.mPrimaryAction;
			}
			List<String> suggestions = mCurrentKeyDown.suggestionViewList();
			List<String> predictions = mDictionary.getCompletionPredictions(currentWord);
			suggestions.addAll(predictions);
			
			mView.getSuggestionView().setSuggestions(suggestions);
			
			int nextCharacterIndex = currentWord.length();
			
			// zoom keys based on what characters come next
			for (int z = 0; z < predictions.size(); ++z)
			{
				String predition = predictions.get(z);
				String characterToZoom = predition.substring(nextCharacterIndex, nextCharacterIndex + 1);
				Key key = keyboard.getKey(characterToZoom);
				if (key == null)
					continue;
				
				key.mZoom = true;
			}
		}
	}
	
	public void vibrate()
	{
		if (mVibrator != null && mVibrateTime > 0)
			mVibrator.vibrate(mVibrateTime);
	}
	
	// popups dont work in landscape mode, which is typically fullscreen mode
	// so make sure we dont go fullscreen in landscape
	@Override
	public boolean onEvaluateFullscreenMode()
	{
		return false;
	}
	
	public boolean onTouch(Key key, MotionEvent event)
	{
		//
		// I have numerous methods to determine which key is pressed
		// on multikeys
		//
		// 1. swipe left or right from the key press
		// 2. long key press chooses second key, tap does first key
		// 3. tap/double tap, tap for first, double for second (doesnt work on system keys)
		//
		
		if (event.getAction() == MotionEvent.ACTION_UP)
		{
			mMessageHandler.removeMessages(MSG_LONG_PRESS);
			
			if (mLongKeyPressed || mSwipeOccured)
			{
				mSwipeOccured = false;
				mLongKeyPressed = false;
				return true;
			}
		}
		else if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			mTouchDownEvent = event.obtain(event);
		}
		else if (event.getAction() == MotionEvent.ACTION_MOVE)
		{
			if (mLongKeyPressed || mSwipeOccured)
				return true;

			// swipe gesture recognition 
			float deltaX = event.getX() - mTouchDownEvent.getX();
			if (Math.abs(deltaX) > mKeyboardSwipeDistance)
			{
				mSwipeOccured = true;
				if (deltaX > 0)
					mView.changeToNextKeyboard();
				else
					mView.changeToPreviousKeyboard();
				
				updatePredictions(); // adjust zoom
			}
		}
		
		if (key == null)
			return true;
		
		if (event.getAction() == MotionEvent.ACTION_UP)
		{
			if (key.mPrimaryAction.equals("next_keyboard"))
			{
				mView.changeToNextKeyboard();
			}
			else if (key.mPrimaryAction.equals("delete"))
			{
				// TODO: on long keypress, delete entire word, and do repeat
				getCurrentInputConnection().deleteSurroundingText(1, 0);
			}
			else if (key.mPrimaryAction.equals("space"))
			{
				getCurrentInputConnection().commitText(" ", 1);
			}
			else if (key.mPrimaryAction.equals("enter"))
			{
				getCurrentInputConnection().sendKeyEvent(
		                new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
			}
			else if (key.mPrimaryAction.equals("voice_recognition"))
			{
				mVoiceRecognition.start();
			}
			else
			{
				// if previous character is a space, assume the user has spelt the last word correctly
				// BUG: how to handle the last word the user enters?
				CharSequence charSequence = getCurrentInputConnection().getTextBeforeCursor(1, 0);
				if (charSequence.length() > 0 && charSequence.charAt(0) == ' ')
				{
					mDictionary.add(wordBeforeCursor(true));
				}
				
				getCurrentInputConnection().commitText(key.mPrimaryAction, 1);
			}
			
			updatePredictions();
		}
		else if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			vibrate();
			mCurrentKeyDown = key;
			updatePredictions();
			
			if (key.mPrimaryAction.equals("delete"))
			{
				// queue long key press check
				mMessageHandler.removeMessages(MSG_LONG_PRESS);
				mMessageHandler.sendEmptyMessageAtTime(MSG_LONG_PRESS,
		                        event.getDownTime() + mLongKeyTime);
			}
		}
		
		return true;
	}

	
	@Override
	public void onStartInputView(EditorInfo attribute, boolean restarting)
	{
		waitForDebugger();
		
        super.onStartInputView(attribute, restarting);
	}
	
	/**
     * This is the main point where we do our initialization of the input method
     * to begin operating on an application.  At this point we have been
     * bound to the client, and are now receiving all of the detailed information
     * about the target of our edits.
     */
    @Override 
    public void onStartInput(EditorInfo attribute, boolean restarting)
    {
    	waitForDebugger();
    	
        super.onStartInput(attribute, restarting);
        
        //startComposingText(false);
        
        /*
        // Reset our state.  We want to do this even if restarting, because
        // the underlying state of the text editor could have changed in any way.
        ExtractedTextRequest extractedText = new ExtractedTextRequest();
        getCurrentInputConnection().getExtractedText(extractedText, 0);
        mText.setLength(0);
        mText.append(extractedText.)
        
        // We are now going to initialize our state based on the type of
        // text being edited.
        switch (attribute.inputType & EditorInfo.TYPE_MASK_CLASS)
        {
            case EditorInfo.TYPE_CLASS_NUMBER:
            case EditorInfo.TYPE_CLASS_DATETIME:
                break;
                
            case EditorInfo.TYPE_CLASS_PHONE:
                break;
                
            case EditorInfo.TYPE_CLASS_TEXT:
                // This is general text editing.  We will default to the
                // normal alphabetic keyboard, and assume that we should
                // be doing predictive text (showing candidates as the
                // user types).

                
                // We now look for a few special variations of text that will
                // modify our behavior.
                int variation = attribute.inputType &  EditorInfo.TYPE_MASK_VARIATION;
                if (variation == EditorInfo.TYPE_TEXT_VARIATION_PASSWORD ||
                        variation == EditorInfo.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) 
                {
                    // Do not display predictions / what the user is typing
                    // when they are entering a password.

                }
                
                if (variation == EditorInfo.TYPE_TEXT_VARIATION_EMAIL_ADDRESS 
                        || variation == EditorInfo.TYPE_TEXT_VARIATION_URI
                        || variation == EditorInfo.TYPE_TEXT_VARIATION_FILTER)
                {
                    // Our predictions are not useful for e-mail addresses
                    // or URIs.

                }
                
                if ((attribute.inputType&EditorInfo.TYPE_TEXT_FLAG_AUTO_COMPLETE) != 0)
                {
                    // If this is an auto-complete text view, then our predictions
                    // will not be shown and instead we will allow the editor
                    // to supply their own.  We only show the editor's
                    // candidates when in fullscreen mode, otherwise relying
                    // own it displaying its own UI.
                }
                
                // We also want to look at the current state of the editor
                // to decide whether our alphabetic keyboard should start out
                // shifted.
                break;
                
            default:
                // For all unknown input types, default to the alphabetic
                // keyboard with no special features.
        }*/
    }
    
	/**
     * This is called when the user is done editing a field.  We can use
     * this to reset our state.
     */
    @Override 
    public void onFinishInput() 
    {
    	waitForDebugger();
    	
        //if (getCurrentInputConnection() != null)
        //	getCurrentInputConnection().commitText(mComposingText, 1);
        
		//mComposingText.setLength(0);
		
		
		// test
		//mDictionary.serializeFromFile("/sdcard/kebe/dictionary.ser");
		
		super.onFinishInput();
    }
    
    @Override
    public void	onFinishInputView(boolean finishingInput)
    {
    	waitForDebugger();
    	
    	//if (getCurrentInputConnection() != null)
       // 	getCurrentInputConnection().commitText(mComposingText, 1);
        
		//mComposingText.setLength(0);
    	
		// STACK OVERFLOW ISSUE!
    	/*
        try
        {
			// not really a good spot to do this, maybe put it in a thread?
			mDictionary.serializeToFile("/sdcard/kebe/dictionary.ser");
        }
        catch (Exception ex)
        {
        	ex.printStackTrace();
        }*/
		
		super.onFinishInputView(finishingInput);
    }
	
	/**
     * Called by the framework when your view for creating input needs to
     * be generated.  This will be called the first time your input method
     * is displayed, and every time it needs to be re-created such as due to
     * a configuration change.
     */
    @Override 
    public View onCreateInputView()
    {
    	waitForDebugger();
    	
    	/*
    	// onCreateView gets recreated when you rotate the screen,
    	// no point reloading form file, the view handles this automatically
    	if (mView != null)
    	{
    		return mView;
    	}
    	*/
    	
    	mView = new KeyboardView(this);
    	//mView.serializeFromFile(new Path("Kebe.Android:raw/layout.txt"));
		mView.serializeFromFile(new Path("Kebe.Android:raw/layout_default.txt"));
    	mView.setListener(this);
    	
    	mTheme = new Theme(this);
    	mTheme.serializeFromFile(new Path("Kebe.Android:raw/theme.txt"));
    	//mTheme.serializeFromFile(new Path("Kebe.Android:raw/theme_default.txt"));
    	mView.setTheme(mTheme);
    	
    	mVoiceRecognition = new VoiceRecognition(this);
    	
    	// stack overflow, need to write my own serialization
    	/*
    	try
    	{
	    	// load standard dictionary then load/save a user version
	    	mDictionary.serializeFromFile("/sdcard/kebe/dictionary.ser");
    	}
    	catch (Exception ex)
    	{
    		ex.printStackTrace();
    	}*/
    	
    	return mView;
    }
    
    /**
     * Called by the framework when your view for showing candidates needs to
     * be generated, like {@link #onCreateInputView}.
     * /
    @Override 
    public View onCreateCandidatesView()
    {
        mCandidateView = new CandidateView(this);
        return mCandidateView;
    }*/
    
    @Override 
    public void onInitializeInterface()
    {
    	waitForDebugger();
    	
    	mVibrator = ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE));
    }
    
    
   
	public Bitmap loadBitmap(Path path)
	{
		Bitmap bitmap = null;
		int resId = getResourcesId(path);
		if (resId > 0)
		{
			bitmap = BitmapFactory.decodeResource(getResources(), resId);
		}
		
		if (bitmap == null)
		{
			bitmap = BitmapFactory.decodeFile(path.path);
		}
		
		return bitmap;
	}
	
	public BufferedReader loadTextFile(Path path)
	{	
		int resId = getResourcesId(path);
		if (resId > 0)
		{
			Resources res = getResources();
			InputStream is = res.openRawResource(resId);
			if (is != null)
			{
				InputStreamReader in = new InputStreamReader(is);
				return new BufferedReader(in);
			}
		}
		
		try 
		{
			return new BufferedReader(new FileReader(path.path));
		} 
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public int getResourcesId(Path path)
	{
		Resources res = getResources();
		
		String resourceDir = path.directory; //"Kebe.Android:raw";
		String fileName = path.fileName;
		if (resourceDir != null && resourceDir.length() > 0)
			fileName = resourceDir + "/" + fileName;
		
		int resID = res.getIdentifier(fileName, null, null);
		return resID;
	}
	
}
