package Kebe.Android;

import java.util.List;
import java.util.Vector;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.widget.Button;
import android.widget.LinearLayout;

public class CandidateView extends RowLayout
{
	public CandidateView(Context context)
	{
		super(context);
		/*
		
		switch (context.getResources().getConfiguration().orientation)
		{
		case Configuration.ORIENTATION_PORTRAIT:
			{
				// move to a serialize method
				Bitmap backgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.candidate_background_portrait);
		    	BitmapDrawable backgroundDrawable = new BitmapDrawable(this.getResources(), backgroundBitmap);
		    	setBackgroundDrawable(backgroundDrawable);
				break;
			}
			
		case Configuration.ORIENTATION_LANDSCAPE:
			{
				// move to a serialize method
				Bitmap backgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.candidate_background_landscape);
		    	BitmapDrawable backgroundDrawable = new BitmapDrawable(this.getResources(), backgroundBitmap);
		    	setBackgroundDrawable(backgroundDrawable);
				break;
			}
		}
		
		*/
	}
	

	void setSuggestions(List<String> stringList)
	{
		removeAllViews();
		
		if (stringList != null)
		{	
			int maxChars = 0;
			switch (getContext().getResources().getConfiguration().orientation)
			{
			case Configuration.ORIENTATION_PORTRAIT:
				maxChars = 40;
				break;
				
			case Configuration.ORIENTATION_LANDSCAPE:
				maxChars = 75;
				break;
			}
				
			for (int i = 0; i < stringList.size(); ++i)
			{
				// takes up too much space, maybe make it a image
				/*
				if (i != 0)
				{
					// add separator button
					Button button = new Button(getContext());
			    	button.setText("|");
			    	button.setTextColor(Color.rgb(255, 255, 255));
			    	button.setBackgroundDrawable(null);
			    	LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(10, LayoutParams.WRAP_CONTENT, 0);
			    	button.setLayoutParams(params);
			    	mSuggestions.addView(button);
			    	button.setGravity(Gravity.CENTER);
			    	button.setTextSize(15.f);
				}*/
				
				maxChars -= stringList.get(i).length() + 3; // + n characters worth of padding atm :(
				
				if (maxChars < 0)
					break;
				
				Button button = new Button(getContext());
		    	button.setText(stringList.get(i));
		    	button.setTextColor(Color.rgb(255, 255, 255));
		    	button.setBackgroundDrawable(null);
		    	LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 0);
		    	button.setLayoutParams(params);
		    	addView(button);
		    	button.setGravity(Gravity.CENTER);
		    	button.setTextSize(15.f);
			}
		}
	}

}
