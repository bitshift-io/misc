package Kebe.Android;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import Kebe.Android.Key.State;
import Kebe.Android.KeyboardView.Orientation;

public class Theme
{
	static public class Bitmap
	{
		public static boolean draw(Bitmap bitmap, Rect rect, Canvas canvas, Paint paint)
		{
			if (bitmap == null)
				return false;
			
			bitmap.draw(rect, canvas, paint);
			return true;
		}
		
		public Bitmap(String name)
		{
			this.name = name;
		}
		
		public void setTheme(Theme theme)
		{
			resource = theme.get(name);
		}
		
		public void draw(Rect rect, Canvas canvas, Paint paint)
		{
			if (resource == null)
				return;
			
			canvas.drawBitmap(resource, null, rect, paint);
		}
		
		String 					name;
		android.graphics.Bitmap	resource;
	}
	
	public Theme(KebeIMS service)
	{
		mService = service;
	}
	
	public boolean serializeFromFile(Path path)
	{
		try
		{
			mPath = path;
			BufferedReader in = mService.loadTextFile(path);
			for (String line = ""; line != null; line = in.readLine())
			{
				line = line.toLowerCase().trim();
				
				// manual split on first equals
				int equalsIndex = line.indexOf('=');
				if (equalsIndex == -1)
					continue;
				
				String[] keyValue = new String[2];
				keyValue[0] = line.substring(0, equalsIndex).toLowerCase();
				keyValue[1] = line.substring(equalsIndex + 1, line.length());
				
				if (keyValue.length != 2)
					continue;
				
				String key = keyValue[0].trim();
				String value = keyValue[1].trim();
				
				android.graphics.Bitmap bitmap = mService.loadBitmap(new Path(path.directory, value));
				mMap.put(key, bitmap);
			}
		}
		catch (Exception c)
        {
            c.printStackTrace();
        }
		
		return true;
	}

	public android.graphics.Bitmap get(String key)
	{
		// if null try to load based on key?
		android.graphics.Bitmap bitmap = mMap.get(key);
		if (bitmap == null)
		{
			bitmap = mService.loadBitmap(new Path(mPath.directory, key));
			mMap.put(key, bitmap);
		}
		return bitmap;
	}
	
	public Map<String, android.graphics.Bitmap> 	mMap 	= new HashMap<String, android.graphics.Bitmap>();
	public KebeIMS 				mService;
	public Path					mPath;
}
