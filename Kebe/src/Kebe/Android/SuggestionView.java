package Kebe.Android;

import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.IBinder;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

// http://developer.android.com/reference/android/widget/PopupWindow.html
// Might need to use a "PopupWindow"
public class SuggestionView extends LinearLayout // implements View.OnTouchListener, PopupWindow.OnDismissListener
{
	enum State
	{
		FadeIn,
		FadeOut,
	}
	
	KeyboardView 				mKeyboardView;
	private final WindowManager mWindowManager;
	private boolean 			mIsShowing = false;
	List<String> 				mSuggestions;
	float 						mX;
	float 						mY;
	float						mAlpha = 1.0f;
	int							mLineHeight = 50;
	PopupWindow					mPopupWindow;
	Point						mPopupPosition = null;
	
	int							mSelectedIndex = 0;
	
	
	public SuggestionView(KeyboardView keyboardView, Context context)
	{
		super(context);
		mKeyboardView = keyboardView;
		//setOnTouchListener(this);
		
		mWindowManager = (WindowManager)context.getSystemService(
                Context.WINDOW_SERVICE);
		
		mPopupWindow = new PopupWindow(context);
    	mPopupWindow.setContentView(this);
    	//mPopupWindow.setTouchInterceptor(this);
	}
	
	public Rect rectForSuggestion(int index)
	{
		float y = (index * mLineHeight);
		Rect r = new Rect(0, (int)y, getWidth(), (int)y + mLineHeight);
		return r;
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		if (mSuggestions == null/* || mSuggestions.size() <= 0*/)
			return;
		
		//int canvasWidth = canvas.getWidth();
		//int canvasHeight = canvas.getHeight();
		
		
		//canvas.drawColor(Color.argb(50, 0, 0, 0));
		
		
		Paint paint = new Paint();
		
		int suggestionCount = mSuggestions.size();
		
		paint.setTextSize((float)mLineHeight * 0.8f); // reduce font to fit line height
		paint.setTextAlign(Paint.Align.LEFT);
				
		paint.setColor(Color.argb((int)(128.0f * mAlpha), 255, 255, 255));
		paint.setStrokeWidth(3.f);
		
		for (int i = 0; i < suggestionCount; ++i)
		{
			Rect r = rectForSuggestion(i);
			if (i == mSelectedIndex)
			{
				paint.setStyle(Style.STROKE);
				canvas.drawRect(r, paint);
			}

			paint.setStyle(Style.FILL);
			canvas.drawText(mSuggestions.get(i), r.left + 5, r.top + (mLineHeight / 2) + 5, paint);
		}
		
		// circular view - dont think this will work when selecting
		/*
		PointF center = new PointF((float)canvas.getWidth() / 2.f, (float)canvas.getHeight() / 2.f);
		
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(3.f);
		paint.setColor(Color.argb((int)(128.0f * mAlpha), 255, 255, 255));
		canvas.drawCircle(center.x, center.y, 150.f, paint);
		
		paint.setStyle(Style.FILL);
		paint.setColor(Color.argb((int)(128.0f * mAlpha), 0, 0, 0));
		canvas.drawCircle(center.x, center.y, 150.f, paint);
				
		paint.setTextSize(20.f);
		paint.setTextAlign(Paint.Align.CENTER);
		
		float dToR = (float) (Math.PI / 180.f);
		float startAngle = 0.f * dToR;
		float radius = 100;		
		int count = mSuggestions.size(); //Math.min(5, mSuggestions.size());
		float arc = (180.f / count) * dToR;
		for (int i = 0; i < count; ++i)
		{
			float x = center.x - (float)Math.sin(startAngle - (arc * i)) * radius;
			float y = center.y - (float)Math.cos(startAngle - (arc * i)) * radius;
			
			//paint.setColor(Color.argb(128, 0, 255, 0));
			//canvas.drawCircle(x, y, 10.f, paint);
			
			paint.setColor(Color.argb((int)(255.0f * mAlpha), 255, 255, 255));
			canvas.drawText(mSuggestions.get(i), x, y, paint);
			y += paint.getFontSpacing();
		}*/
	}
	
	public void setSuggestions(List<String> suggestions)
	{
		mSuggestions = suggestions;
		invalidate();
	}
	
	 /**
     * <p>
     * Display the content view in a popup window at the specified location. If the popup window
     * cannot fit on screen, it will be clipped. See {@link android.view.WindowManager.LayoutParams}
     * for more information on how gravity and the x and y parameters are related. Specifying
     * a gravity of {@link android.view.Gravity#NO_GRAVITY} is similar to specifying
     * <code>Gravity.LEFT | Gravity.TOP</code>.
     * </p>
     * 
     * @param parent a parent view to get the {@link android.view.View#getWindowToken()} token from
     * @param gravity the gravity which controls the placement of the popup window
     * @param x the popup's x location offset
     * @param y the popup's y location offset
     */
    public void showAtLocation(View parent, int gravity, float x, float y)
    {
    	if (mIsShowing)
    		return;
    	
    	if (mSuggestions == null)
    	{
    		return;
    	}
    	
    	mIsShowing = true;
    	
    	mX = x;
    	mY = y - 30; // account for notification bar, hrmm need a better way to do this
    	/*
    	WindowManager.LayoutParams p = createPopupLayout(parent.getWindowToken());
    	invokePopup(p);
    	*/
    	

    	setFocusable(true);
    	/*
    	mPopupWindow.setFocusable(true);
    	mPopupWindow.setWidth(300);
    	mPopupWindow.setHeight(300);
    	mPopupWindow.showAtLocation(parent, Gravity.NO_GRAVITY, (int)x - 150, (int)y - 150);
    	*/
    	
    	// should pass in a button rect
    	// so we can align correctly
    	int width = 150;
    	int height = 300;
    	
    	int suggestionCount = mSuggestions.size();
    	height = mLineHeight * suggestionCount;
    	
    	
    	mPopupWindow.setFocusable(true);
    	mPopupWindow.setWidth(width);
    	mPopupWindow.setHeight(height);
    	
    	mPopupPosition = new Point((int)x, (int)y - (height + 30));// +offset to account for button rect till this is done properly
    	mPopupWindow.showAtLocation(parent, Gravity.NO_GRAVITY, mPopupPosition.x, mPopupPosition.y); 
    	
    	//mPopupWindow.
    	
    	//setFocusableInTouchMode(true);
    	
    	boolean isFoc = isFocusable();
    	boolean isFocT = isFocusableInTouchMode();
    	boolean gotFocus = requestFocus();
    	if (gotFocus)
    	{
    		int nothing = 0;
    		++nothing;
    	}
    	
    	mPopupWindow.update();
    
    	
    	/*
        if (isShowing() || mContentView == null) {
            return;
        }

        unregisterForScrollChanged();

        mIsShowing = true;
        mIsDropdown = false;

        WindowManager.LayoutParams p = createPopupLayout(parent.getWindowToken());
        p.windowAnimations = computeAnimationResource();
       
        preparePopup(p);
        if (gravity == Gravity.NO_GRAVITY) {
            gravity = Gravity.TOP | Gravity.LEFT;
        }
        p.gravity = gravity;
        p.x = x;
        p.y = y;
        invokePopup(p);*/
    }
    
	/**
     * <p>Invoke the popup window by adding the content view to the window
     * manager.</p>
     *
     * <p>The content view must be non-null when this method is invoked.</p>
     *
     * @param p the layout parameters of the popup's content view
     * /
	private void invokePopup(WindowManager.LayoutParams p)
	{
        mWindowManager.addView(this, p);
    }
	
	/**
     * <p>Generate the layout parameters for the popup window.</p>
     *
     * @param token the window token used to bind the popup's window
     *
     * @return the layout parameters to pass to the window manager
     * /
    private WindowManager.LayoutParams createPopupLayout(IBinder token)
    {
        // generates the layout parameters for the drop down
        // we want a fixed size view located at the bottom left of the anchor
        WindowManager.LayoutParams p = new WindowManager.LayoutParams();
        // these gravity settings put the view at the top left corner of the
        // screen. The view is then positioned to the appropriate location
        // by setting the x and y offsets to match the anchor's bottom
        // left corner
        p.gravity = Gravity.LEFT | Gravity.TOP;
        p.width = 533; //mLastWidth = mWidth;
        p.height = 320; //mLastHeight = mHeight;
        p.format = PixelFormat.TRANSLUCENT;
        
        p.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED;
        /*
        if (mBackground != null) {
            p.format = mBackground.getOpacity();
        } else {
            p.format = PixelFormat.TRANSLUCENT;
        }* /
        //p.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
        	//computeFlags(p.flags);
        p.type = WindowManager.LayoutParams.TYPE_APPLICATION_PANEL;
        p.token = token;
        p.setTitle("PopupWindow:" + Integer.toHexString(hashCode()));

        return p;
    }*/
    /*
    private int computeFlags(int curFlags) 
    {
        curFlags &= ~(
                WindowManager.LayoutParams.FLAG_IGNORE_CHEEK_PRESSES |
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE |
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH |
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        /*
        if(mIgnoreCheekPress) {
            curFlags |= WindowManager.LayoutParams.FLAG_IGNORE_CHEEK_PRESSES;
        }
        if (!mFocusable) {
            curFlags |= WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
            if (mInputMethodMode == INPUT_METHOD_NEEDED) {
                curFlags |= WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
            }
        } else if (mInputMethodMode == INPUT_METHOD_NOT_NEEDED) {
            curFlags |= WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
        }
        if (!mTouchable) {
            curFlags |= WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
        }
        if (mOutsideTouchable) {
            curFlags |= WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH;
        }
        if (!mClippingEnabled) {
            curFlags |= WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        }* /
        return curFlags;
    }*/
    
    /**
     * <p>Dispose of the popup window. This method can be invoked only after
     * {@link #showAsDropDown(android.view.View)} has been executed. Failing that, calling
     * this method will have no effect.</p>
     *
     * @see #showAsDropDown(android.view.View) 
     */
    public void dismiss() 
    {
    	mIsShowing = false;
    	
    	if (mPopupWindow.isShowing()) 
    	{
    		mPopupWindow.dismiss();
    	}
    	/*
    	mWindowManager.removeView(this);
    	/*
        if (isShowing() && mPopupView != null) {
            unregisterForScrollChanged();

            mWindowManager.removeView(this);
            if (mPopupView != mContentView && mPopupView instanceof ViewGroup) {
                ((ViewGroup) mPopupView).removeView(mContentView);
            }
            mPopupView = null;
            mIsShowing = false;

            if (mOnDismissListener != null) {
                mOnDismissListener.onDismiss();
            }
        }*/
    }
    
    /**
     * Updates the state of the popup window, if it is currently being displayed,
     * from the currently set state.  This include:
     * {@link #setClippingEnabled(boolean)}, {@link #setFocusable(boolean)},
     * {@link #setIgnoreCheekPress()}, {@link #setInputMethodMode(int)},
     * {@link #setTouchable(boolean)}, and {@link #setAnimationStyle(int)}.
     * /
    public void update()
    {
    	WindowManager.LayoutParams p = (WindowManager.LayoutParams)this.getLayoutParams();
    	
    	 mWindowManager.updateViewLayout(this, p);
    	 
    	/*
        if (!isShowing() || mContentView == null) {
            return;
        }
        
        WindowManager.LayoutParams p = (WindowManager.LayoutParams)
                mPopupView.getLayoutParams();
        
        boolean update = false;
        
        final int newAnim = computeAnimationResource();
        if (newAnim != p.windowAnimations) {
            p.windowAnimations = newAnim;
            update = true;
        }

        final int newFlags = computeFlags(p.flags);
        if (newFlags != p.flags) {
            p.flags = newFlags;
            update = true;
        }
        
        if (update) {
            mWindowManager.updateViewLayout(mPopupView, p);
        }* /
    }*/
	
	@Override
    public boolean dispatchTouchEvent(MotionEvent ev) 
	{
		//View viewRoot = this.getRootView();
		//if (viewRoot != null)
		//	viewRoot.dispatchTouchEvent(ev);
		
		// make this event relative to this view
		ev.offsetLocation(-mPopupPosition.x, -mPopupPosition.y);
		return super.dispatchTouchEvent(ev);
		//return false;
	}
	
	public boolean onTouchEvent(MotionEvent event)
	{
		System.out.println("SuggestionView - onTouchEvent");
		switch (event.getAction())
		{
		case MotionEvent.ACTION_CANCEL:
			System.out.println("ACTION_CANCEL");
			break;
		case MotionEvent.ACTION_DOWN:
			System.out.println("ACTION_DOWN");
			break;
		case MotionEvent.ACTION_UP:
			System.out.println("ACTION_UP");
			break;
		default:
			System.out.println("Unknown Action");
			break;
		};
		
		// update selection
		// what we want to do is select by having our finder under the button
		int suggestionCount = mSuggestions.size();
		for (int i = 0; i < suggestionCount; ++i)
		{
			Rect r = rectForSuggestion(i);
			
			// convert the point to relative to this popup
			float x = event.getX();
			float y = event.getY();// - mLineHeight; // so we select from under the button

			System.out.println("touch(" +  x + "," + y + ")");
			
			if (r.contains((int)x,  (int)y))
			{
				mSelectedIndex = i;
				break;
			}
		}
		
		if (event.getAction() == MotionEvent.ACTION_UP)
    	{
			dismiss();
    	}	
		
		// forward it onto keyboard if unhandled
		return true; //mKeyboardView.onTouch(v, event);
	}

	public void onDismiss() {
		// TODO Auto-generated method stub
		int nothing = 0;
		++nothing;
	}
}

