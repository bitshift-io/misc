package Kebe.Android;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

public class RowLayout extends LinearLayout
{
	public RowLayout(Context context)
	{
		super(context);
		setOrientation(LinearLayout.HORIZONTAL);
		setPadding(0, 0, 0, 0);
	}
	
	@Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		/*
		int count = getChildCount();
		for (int i = 0; i < count; i++) 
	    {
	    
	    }
		
		setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight() / 2);
		*/
		
		/*
        int count = getChildCount();

        int maxHeight = 0;
        int maxWidth = 0;

        // Find out how big everyone wants to be
        measureChildren(widthMeasureSpec, heightMeasureSpec);

        // Find rightmost and bottom-most child
        for (int i = 0; i < count; i++) 
        {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE)
            {
                int childRight;
                int childBottom;

                childRight = child.getMeasuredWidth();
                childBottom = child.getMeasuredHeight();

                maxWidth = Math.max(maxWidth, childRight);
                maxHeight = Math.max(maxHeight, childBottom);
            }
        }

        // Account for padding too
        maxWidth += getPaddingLeft() + getPaddingRight();
        maxHeight += getPaddingTop() + getPaddingBottom();

        // Check against minimum height and width
        maxHeight = Math.max(maxHeight, getSuggestedMinimumHeight());
        maxWidth = Math.max(maxWidth, getSuggestedMinimumWidth());
        
        setMeasuredDimension(resolveSize(maxWidth, widthMeasureSpec),
                resolveSize(maxHeight, heightMeasureSpec));*/
    }
}