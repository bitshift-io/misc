package Kebe.Android;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ViewFlipper;

/**
 * 
 * @author Fabian
 *
 */
public class KeyboardView extends View //implements View.OnTouchListener
{
	enum Orientation
	{
		Portrait,
		Landscape,
	}
	
	public interface Listener
	{
		public boolean onTouch(Key key, MotionEvent event);
	}
	
	public KeyboardView(KebeIMS service)
	{		
		super(service.getApplicationContext());
		mService = service;
		//setOnTouchListener(this);
		
		//mSuggestionWindow = new SuggestionWindow(this, context);
		mSuggestionView = new SuggestionView(this, service.getApplicationContext());
	}
	
	public void setTheme(Theme theme)
	{
		// iterate over all keyboard and set the appropriate theme
		for (int i = 0; i < mKeyboards.size(); ++i)
		{
			Vector<Keyboard> orientationKeyboard = mKeyboards.get(i);
			for (int k = 0; k < orientationKeyboard.size(); ++k)
			{
				Keyboard keyboard = orientationKeyboard.get(k);
				keyboard.setTheme(theme);
			}
		}
	}

	public void serializeFromFile(Kebe.Android.Path path)
	{
		try
		{
			mKeyboards.add(new Vector<Keyboard>());
			mKeyboards.add(new Vector<Keyboard>());
			
			BufferedReader in = mService.loadTextFile(path);
			for (String line = ""; line != null; line = in.readLine())
			{
				line = line.toLowerCase().trim();
				
				if (line.equals("[portrait]"))
				{
					line = in.readLine();
					line = line.toLowerCase().trim();
					
					if (line.equals("{"))
					{
						Vector<Keyboard> keyboards = mKeyboards.get(Orientation.Portrait.ordinal());
						serializeFromFile(in, keyboards);
					}
				}
				
				if (line.equals("[landscape]"))
				{
					line = in.readLine();
					line = line.toLowerCase().trim();
					
					if (line.equals("{"))
					{
						Vector<Keyboard> keyboards = mKeyboards.get(Orientation.Landscape.ordinal());
						serializeFromFile(in, keyboards);
					}
				}
			}
			
			updateCurrentKeyboard();
		}
		catch (Exception c)
        {
            c.printStackTrace();
        }
	}
	
	public void serializeFromFile(BufferedReader in, Vector<Keyboard> keyboards) throws Exception
	{
		for (String line = ""; line != null; line = in.readLine())
		{
			line = line.toLowerCase().trim();
			
			if (line.equals("}"))
				return;
			
			if (line.equals("[keyboard]"))
			{
				line = in.readLine();
				line = line.toLowerCase().trim();
				
				if (line.equals("{"))
				{
					Keyboard keyboard = new Keyboard(mService);
					keyboard.serializeFromFile(in);
					keyboards.add(keyboard);
				}
			}
		}
	}
	
	void updateCurrentKeyboard()
	{
		switch (getContext().getResources().getConfiguration().orientation)
		{
		case Configuration.ORIENTATION_PORTRAIT:
			{
				mOrientation = Orientation.Portrait;
				break;
			}
			
		case Configuration.ORIENTATION_LANDSCAPE:
			{
				mOrientation = Orientation.Landscape;
				break;
			}
		}
	}
	
	void changeToNextKeyboard()
	{
		updateCurrentKeyboard();
		
		++mCurrentKeyboardIndex;
		if (mCurrentKeyboardIndex >= mKeyboards.get(mOrientation.ordinal()).size()) 
			mCurrentKeyboardIndex = 0;
	}
	
	void changeToPreviousKeyboard()
	{
		updateCurrentKeyboard();
		
		--mCurrentKeyboardIndex;
		if (mCurrentKeyboardIndex < 0) 
			mCurrentKeyboardIndex = mKeyboards.get(mOrientation.ordinal()).size() - 1;
	}
	
	Keyboard getCurrentKeyboard()
	{
		if (mOrientation.ordinal() >= mKeyboards.size())
			return null;
		
		Vector<Keyboard> keyboards = mKeyboards.get(mOrientation.ordinal());
		if (mCurrentKeyboardIndex >= keyboards.size())
			return null;
		
		return keyboards.get(mCurrentKeyboardIndex);
	}
	
	@Override
    public boolean dispatchTouchEvent(MotionEvent ev) 
	{	
		// TODO: handle when the view is dismissed, forward the key event to here or the IMS
		if (mSuggestionView != null && mSuggestionView.isShown())
		{
			return mSuggestionView.dispatchTouchEvent(ev);
		}

		return super.dispatchTouchEvent(ev);
	}
	
	public void dismiss() 
    {
		int nothing = 0;
		++nothing;
    }
	
	/*
	public boolean onInterceptTouchEvent (MotionEvent ev)
	{
		boolean r = super.onInterceptTouchEvent(ev);
		return r;
	}*/
	
	public boolean onTouchEvent(MotionEvent event)
	{
		System.out.println("KeyboardView - onTouchEvent");
		switch (event.getAction())
		{
		case MotionEvent.ACTION_CANCEL:
			System.out.println("ACTION_CANCEL");
			break;
		case MotionEvent.ACTION_DOWN:
			System.out.println("ACTION_DOWN");
			break;
		case MotionEvent.ACTION_UP:
			System.out.println("ACTION_UP");
			break;
		default:
			System.out.println("Unknown Action");
			break;
		};
		/*
		// this seems to eb the problem that always closes our windows
		// dont know where this bollocks is comming from
		if (event.getAction() == MotionEvent.ACTION_CANCEL)
    	{
			int nothing = 0;
			++nothing;
			return true;
    	}*/
		
		
		
		/*
		// occurs when the popup window is poped up,
		// if we let this throug it closes the keyboard view
		int action = event.getAction();
		if (action == MotionEvent.ACTION_CANCEL)
		{
			System.out.println("where is cancel coming from?");
			return false;
		}*/
		
		if (event.getAction() == MotionEvent.ACTION_DOWN)
    	{
			/*
			PopupWindow mPopupKeyboard = new PopupWindow(getContext());
	        mPopupKeyboard.setBackgroundDrawable(null);
	        
	        mPopupKeyboard.setContentView(mMiniKeyboardContainer);
            mPopupKeyboard.setWidth(mMiniKeyboardContainer.getMeasuredWidth());
            mPopupKeyboard.setHeight(mMiniKeyboardContainer.getMeasuredHeight());
            mPopupKeyboard.showAtLocation(this, Gravity.NO_GRAVITY, x, y);
	        */
						
			/*
			if (!mSuggestionWindow.isShowing())
			{
				mSuggestionWindow.setWidth(533); // screen width
				mSuggestionWindow.setHeight(320);
				mSuggestionWindow.showAtLocation(this, Gravity.NO_GRAVITY, 0, 0); // offset around touch till view handles this
			}*/
			
			
    		mPath = new Path();
    		mPath.moveTo(event.getX(), event.getY());
    		mPath.lineTo(event.getX(), event.getY());
    	}
    	else if (event.getAction() == MotionEvent.ACTION_MOVE)
    	{
    		if (mPath != null)
    		{
    			mPath.lineTo(event.getX(), event.getY());
    		}
    	}
    	else if (event.getAction() == MotionEvent.ACTION_UP)
    	{
    		//if (mSuggestionWindow.isShowing())
    			mSuggestionView.dismiss();
    		
    		if (mPath != null)
    		{
	    		mPath.lineTo(event.getX(), event.getY());
	    		//mGraphics.add(mPath);
	    		mPath = null;
	    		// when a path is complete, check for OCR matches
	        	//mOCRThread.RecogniseFrom(mGraphics);
    		}
    	}
		
		if (mLastKey != null && event.getAction() == MotionEvent.ACTION_UP)
		{
			mLastKey.mState = Key.State.Up;
			invalidate();
		}
		
		Keyboard keyboard = getCurrentKeyboard();
		if (keyboard == null)
			return false;
		

		// adjust to account for visible candidate view
		if (mCandidatesVisible)
		{
			event.setLocation(event.getX(), event.getY() - keyboard.mCandidateHeight);
		}
		
		Key key = keyboard.getKey(event);
			
		if (mLastKey != null && mLastKey != key)
		{
			mLastKey.mState = Key.State.Up;
			invalidate();
		}
		
		mLastKey = key;
		
		if (key != null)
		{
			if (event.getAction() == MotionEvent.ACTION_UP)
			{
				key.mState = Key.State.Up;
				invalidate();
			}
			else if (event.getAction() == MotionEvent.ACTION_DOWN)
			{
				if (mSuggestionViewEnabled)
				{
					// done in KebeIMS atm
					//mSuggestionView.setSuggestions(key.suggestionViewList());
					mSuggestionView.showAtLocation(this, Gravity.NO_GRAVITY, event.getX(), event.getY()); // offset around touch till view handles this
				}
				
				key.mState = Key.State.Down;
				invalidate();
			}
		}
		
		if (mListener == null)
			return false;
		
		return mListener.onTouch(key, event);
		//return true;
	}

	public void setListener(Listener listener)
	{
		mListener = listener;
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		mPaint.setAntiAlias(true);
		
		int canvasWidth = canvas.getWidth();
		int canvasHeight = canvas.getHeight();
		
		Keyboard keyboard = getCurrentKeyboard();
		if (keyboard == null)
			return;
		
		/*
		if (mCandidatesVisible)
		{
			keyboardRect.top = keyboard.mCandidateHeight;
			
			Rect candidatesRect = new Rect();
			candidatesRect.top = 0;
			candidatesRect.bottom = keyboard.mCandidateHeight;
			candidatesRect.left = 0;
			candidatesRect.right = canvasWidth;
			
			keyboard.drawCandidates(candidatesRect, canvas, mPaint, mCandidates);
		}*/
		
		keyboard.drawKeyboard(canvas, mPaint);
		
		// draw swipe history
		/*
		mPaint.setDither(true);
        mPaint.setColor(0xFFFFFF00);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(3);
        */
        
        if (mPath != null)
        {
        	mPaint.setARGB(255, 255, 255, 255);
        	mPaint.setStyle(Paint.Style.STROKE);
			canvas.drawPath(mPath, mPaint);
        }
        // for hand writing recognition
        /*
		RectF combinedBounds = new RectF();
		for (Path path : mGraphics)
		{
			mPaint.setARGB(255, 255, 255, 255);
			canvas.drawPath(path, mPaint);
			
			//int curColour = mPaint.getColor();
			//mPaint.setARGB(255, 0, 0, 128);
			
			RectF bounds = new RectF();
			path.computeBounds(bounds, false);
			combinedBounds.union(bounds);
			//canvas.drawRect(bounds, mPaint);
			
			//mPaint.setColor(curColour);
		}
		
		mPaint.setARGB(255, 0, 0, 128);
		canvas.drawRect(combinedBounds, mPaint);*/
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		Keyboard keyboard = getCurrentKeyboard();
		keyboard.setViewRect(new Rect(0, 0, w, h));
		super.onSizeChanged(w, h, oldw, oldh);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		WindowManager wm = (WindowManager) mService.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		Point size = new Point();
		display.getSize(size);

		Keyboard keyboard = getCurrentKeyboard();
		int width = size.x;
		int height = (int)((float)width / keyboard.aspectRatio());
		
		this.setMeasuredDimension(width, height);
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		//super.onLayout(changed, l, t, r, b);
	}
	
	public void setCandidatesVisible(boolean visible)
	{
		mCandidatesVisible = visible;
		invalidate();
	}
	

	public SuggestionView getSuggestionView()
	{
		return mSuggestionView;
	}
	
	protected Vector<Vector<Keyboard>>	mKeyboards 			= new Vector<Vector<Keyboard>>();
	
	protected boolean			mSuggestionViewEnabled		= true; //true; // make true to show candidate view
	protected boolean			mCandidatesVisible			= false;
	protected Paint 			mPaint						= new Paint();
	protected int				mCurrentKeyboardIndex	 	= 0;
	protected Orientation		mOrientation 				= Orientation.Portrait;
	protected Listener 			mListener;
	protected Key				mLastKey;
	protected List<String>		mCandidates					= new Vector<String>();
	private Path 				mPath;
	private SuggestionView		mSuggestionView;
	protected KebeIMS			mService;
}
