package Kebe.Android;

// Breaks up a String path
public class Path 
{
	public Path(String path)
	{
		this.path = path;
		
		// resources are of the form {directory}/{filename}.{extension}
		int lastSlash = path.lastIndexOf('/');
		int lastDot = path.lastIndexOf('.');
		
		// no extension
		if (lastSlash > lastDot || lastDot == -1)
			lastDot = path.length();
		
		directory = path.substring(0, lastSlash);
		fileName = path.substring(lastSlash + 1, lastDot);
		
		if (lastDot < path.length())
			extension = path.substring(lastDot + 1);
	}
	
	public Path(String directory, String fileName, String extension)
	{
		this.directory = directory;
		this.fileName = fileName;
		this.extension = extension;
		path = directory + "/" + fileName + "." + extension;
	}
	
	public Path(String directory, String fileNameAndExtension)
	{
		this(directory + "/" + fileNameAndExtension);
	}
	
	public String directory;
	public String fileName;
	public String extension;
	public String path;
}
