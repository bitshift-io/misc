#include "StdAfx.h"
#include "CodeBlock.h"

void CodeBlock::Evaluate()
{
	// set all vars to default
	{
		vector<Variable*>::iterator it;
		for (it = mVariable.begin(); it != mVariable.end(); ++it)
		{
			(*it)->mCurrentValue = (*it)->mDefault;
		}
	}

	// evaluate each line of code
	{
		vector<Node*>::iterator it;		
		for (it = mNode.begin(); it != mNode.end(); ++it)
		{
			Node* node = (*it);
			node->Evaluate(0);
		}
	}
}

Variable* CodeBlock::FindVariable(const string& name)
{
	vector<Variable*>::iterator it;
	for (it = mVariable.begin(); it != mVariable.end(); ++it)
	{
		if (strcmp((*it)->mName.c_str(), name.c_str()) == 0)
			return *(it);
	}

	return 0;
}