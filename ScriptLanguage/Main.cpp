#include "StdAfx.h"
#include "Main.h"

//
// if i decide im bored with my own parser:
// http://www.codeproject.com/vcpp/stl/introduction_spirit.asp
//

static const char *sBuild = __DATE__ " " __TIME__;
								
int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	char cmdLine[256];
	WideCharToMultiByte(CP_ACP, 0, lpCmdLine, -1, cmdLine, 256, NULL, NULL);
	return 	gScriptLanguage.Main(cmdLine);;
}

int ScriptLanguage::Main(const char* cmdLine)
{
	Parser p;
	p.Open("test.sc");


	Variable param1(6);
	Variable param2(2);
	Variable* result = p.EvaluateFunction("Main", "Main", 2, &param1, &param2);
	int outputReference = param1.mCurrentValue.mInt;
	int outputReturn = result->mCurrentValue.mVariable->mCurrentValue.mInt;
	return 0;
}




