#ifndef _OPERATOR_H_
#define _OPERATOR_H_

// http://msdn2.microsoft.com/en-us/library/x04xhy0h(VS.80).aspx

// sorted by order of precedance
enum OperatorType
{
	OT_Assign,

	// locical
	OT_Logic_And,
	OT_Logic_Or,

	// Relational and Equality
	OT_Equality,
	OT_LessThanOrEqual,
	OT_GreaterThenOrEqual,
	OT_LessThan,
	OT_GreaterThan,
	OT_NotEqual,

	// multiplacive
	OT_Multiply,
	OT_Divide,
	OT_Modulus,

	// additive
	OT_Add,
	OT_Subtract,


/*
	// postfix
	OT_Decrement,
	OT_Increment,

	// bitwise
	OT_Bitwise_Or,
	OT_Bitwise_And,
	OT_Bitwise_Xor,
	//OT_Not,

	// assignment
	OT_AddAssign,
	OT_SubtractAssign,
	OT_MultiplyAssign,
	OT_DivideAssign,



	// shift
	OT_LeftShift,
	OT_RightShift,
*/
	OT_Max,
};

extern const char* operatorList[];

class ParseNodeVisitor;

class Node
{
public:

	virtual bool	Evaluate(Variable* var) = 0;
	virtual bool	Assign(Variable* var) = 0;
};

class OperatorNode : public Node
{
public:

	virtual bool	Evaluate(Variable* var);
	virtual bool	Assign(Variable* var)		{ return false; }

	OperatorType	mOperator;	

	Node*			mLeft;
	Node*			mRight;
};

class FunctionCallNode : public Node
{
public:

	virtual bool	Evaluate(Variable* var);
	virtual bool	Assign(Variable* var)		{ return false; }

	Function*		mFunction;
};

class VariableNode : public Node
{
public:

	virtual bool	Evaluate(Variable* var);
	virtual bool	Assign(Variable* var);

	Variable*		mVariable;
};

class ConstVariableNode : public Node
{
public:

	virtual bool	Evaluate(Variable* var);
	virtual bool	Assign(Variable* var)		{ return false; }

	Variable		mVariable;
};

class BranchNode : public Node
{
public:

	BranchNode() : mNext(0), mCode(0), mExpression(0)
	{
	}

	virtual bool	Evaluate(Variable* var);
	virtual bool	Assign(Variable* var)		{ return false; }

	Node*			mExpression;
	CodeBlock*		mCode;
	Node*			mNext;
};

class CodeBlockNode : public Node
{
public:

	virtual bool	Evaluate(Variable* var);
	virtual bool	Assign(Variable* var)		{ return false; }

	CodeBlock*		mCode;
};

#endif