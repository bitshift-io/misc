#ifndef _FUNCTION_H_
#define _FUNCTION_H_

#include <string>
#include <vector>

using namespace std;

class Variable;
class CodeBlock;
class Class;

class Function
{
public:

	Function()
	{
		mReturnArgument = 0;
	}

	Variable*			Evaluate(vector<Variable*> args);
	Variable*			FindVariable(const string& name);

	string				mName;
	vector<Variable*>	mArgument;
	Variable*			mReturnArgument;
	CodeBlock*			mCodeBlock;
	Class*				mClass;
};

#endif