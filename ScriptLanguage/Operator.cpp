#include "StdAfx.h"
#include "Operator.h"

const char* operatorList[] =
{
	"=",

	"and",
	"or",

	"==",
	"<=",
	">=",
	"<",
	">",
	"!=",

	"\\*",
	"/",
	"%",
	"\\+",
	"-",
};

#define APPLY_OPERATOR_INT(_left, _right, _operator)					\
	_left.mCurrentValue.mInt _operator _right.mCurrentValue.mInt;		\
	_left.mCurrentValue.mUInt _operator _right.mCurrentValue.mUInt;

#define APPLY_OPERATOR_REAL(_left, _right, _operator)					\
	_left.mCurrentValue.mReal _operator _right.mCurrentValue.mReal;		\
	_left.mCurrentValue.mUReal _operator _right.mCurrentValue.mUReal;	\


#define APPLY_LEFT_OPERATOR_INT(_left, _right, _operator)					\
	_left.mCurrentValue.mInt = _left.mCurrentValue.mInt _operator _right.mCurrentValue.mInt;		\
	_left.mCurrentValue.mUInt = _left.mCurrentValue.mUInt _operator _right.mCurrentValue.mUInt;

#define APPLY_LEFT_OPERATOR_REAL(_left, _right, _operator)					\
	_left.mCurrentValue.mReal = _left.mCurrentValue.mReal _operator _right.mCurrentValue.mReal;		\
	_left.mCurrentValue.mUReal = _left.mCurrentValue.mUReal _operator _right.mCurrentValue.mUReal;	\

bool OperatorNode::Evaluate(Variable* var)
{
	Variable left;
	Variable right;

	mLeft->Evaluate(&left);
	mRight->Evaluate(&right);

	switch (mOperator)
	{
	case OT_Assign:
		mLeft->Assign(&right);
		return true;

	case OT_Logic_And:
		APPLY_LEFT_OPERATOR_INT(left, right, &&)
		APPLY_LEFT_OPERATOR_REAL(left, right, &&)
		*var = left;
		break;

	case OT_Logic_Or:
		APPLY_LEFT_OPERATOR_INT(left, right, ||)
		APPLY_LEFT_OPERATOR_REAL(left, right, ||)
		*var = left;
		break;

	case OT_Equality:
		APPLY_LEFT_OPERATOR_INT(left, right, ==)
		APPLY_LEFT_OPERATOR_REAL(left, right, ==)
		*var = left;
		break;

	case OT_LessThanOrEqual:
		APPLY_LEFT_OPERATOR_INT(left, right, <=)
		APPLY_LEFT_OPERATOR_REAL(left, right, <=)
		*var = left;
		break;

	case OT_GreaterThenOrEqual:
		APPLY_LEFT_OPERATOR_INT(left, right, >=)
		APPLY_LEFT_OPERATOR_REAL(left, right, >=)
		*var = left;
		break;

	case OT_LessThan:
		APPLY_LEFT_OPERATOR_INT(left, right, <)
		APPLY_LEFT_OPERATOR_REAL(left, right, <)
		*var = left;
		break;

	case OT_GreaterThan:
		APPLY_LEFT_OPERATOR_INT(left, right, >)
		APPLY_LEFT_OPERATOR_REAL(left, right, >)
		*var = left;
		break;

	case OT_NotEqual:
		APPLY_LEFT_OPERATOR_INT(left, right, !=)
		APPLY_LEFT_OPERATOR_REAL(left, right, !=)
		*var = left;
		break;

	case OT_Multiply:
		APPLY_OPERATOR_INT(left, right, *=)
		APPLY_OPERATOR_REAL(left, right, *=)
		*var = left;
		return true;

	case OT_Divide:
		APPLY_OPERATOR_INT(left, right, /=)
		APPLY_OPERATOR_REAL(left, right, /=)
		*var = left;
		return true;

	case OT_Modulus:
		APPLY_OPERATOR_INT(left, right, %=)
		*var = left;
		return true;

	case OT_Add:
		left.mCurrentValue.mString += right.mCurrentValue.mString;
		APPLY_OPERATOR_INT(left, right, +=)
		APPLY_OPERATOR_REAL(left, right, +=)
		*var = left;
		return true;

	case OT_Subtract:
		APPLY_OPERATOR_INT(left, right, -=)
		APPLY_OPERATOR_REAL(left, right, -=)
		*var = left;
		return true;
	}

	return false;
}

bool FunctionCallNode::Evaluate(Variable* var)
{
	return false;
}

bool VariableNode::Evaluate(Variable* var)
{
	*var = *mVariable;
	return true;
}

bool VariableNode::Assign(Variable* var)
{
	*mVariable = *var;
	return true;
}

bool ConstVariableNode::Evaluate(Variable* var)
{
	*var = mVariable;
	return true;
}

bool BranchNode::Evaluate(Variable* var)
{
	Variable variable;
	mExpression->Evaluate(&variable);

	bool branchPassed = false;
	switch (variable.mType)
	{
	case VT_UInt:
		branchPassed = variable.mCurrentValue.mUInt;
		break;

	case VT_UReal:
		branchPassed = variable.mCurrentValue.mUReal;
		break;

	case VT_Int:
		branchPassed = variable.mCurrentValue.mInt;
		break;

	case VT_Real:
		branchPassed = variable.mCurrentValue.mReal;
		break;

	case VT_String:
		branchPassed = variable.mCurrentValue.mString.length();
		break;

	case VT_Any:
		break;
	}

	if (branchPassed)
	{
		mCode->Evaluate();
	}
	else
	{
		return mNext->Evaluate(var);
	}

	return true;
}

bool CodeBlockNode::Evaluate(Variable* var)
{
	mCode->Evaluate();
	return true;
}