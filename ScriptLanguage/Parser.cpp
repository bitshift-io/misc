#include "StdAfx.h"
#include "Parser.h"
#include "File/ScriptFile.h"

using namespace boost;

bool Parser::Open(const string& fileName)
{
	File file;
	if (!file.Open(fileName.c_str(), "rt"))
		return false;

	Process(file);

	file.Close();
	return true;
}

void Parser::Process(File& in)
{
	TokenType tokenType;
	do
	{
		tokenType = ReadToNextToken(in);

		switch (tokenType)
		{
		case TT_Class:
		case TT_Namespace:
			ReadClass(in, tokenType);
			break;
		}

	} while (tokenType != TT_EndOfFile);
}

void Parser::ReadClass(File& in, TokenType tokenType)
{
	char line[1024];
	in.ReadLine(line, 1024);

	// http://www.fileformat.info/tool/regex.htm
	string name;
	string templateArg;
	string extends;

	// extract out the name + template info and the extends list here
	cmatch matches;
	regex expression("^ ((.*) : (.*)|(.*))$");
	if (regex_match(line, matches, expression))
	{
		int nonEmptyCount = 0;
		for (int i = 0; i < matches.size(); i++)
		{
			string match(matches[i].first, matches[i].second);

			if (nonEmptyCount == 2)
				name = match;

			if (nonEmptyCount == 3)
				extends = match;

			if (match.length() > 0)
				++nonEmptyCount;
		}
	}

	
	// now extract any template arguments
	expression = regex("^(.*)<(.*)>$");
	if (regex_match(name.c_str(), matches, expression))
	{
		templateArg = string(matches[2].first, matches[2].second);
		name = string(matches[1].first, matches[1].second);
	}

	// extract template args
	vector<string> templateArgList = ScriptFile::Tokenize(templateArg, ",", true);

	// extract the extends list
	vector<string> extendsList = ScriptFile::Tokenize(extends, ",", true);

	Class* pClass = new Class();
	mClass.push_back(pClass);

	pClass->mType = (tokenType == TT_Class ? CT_Class : CT_Namespace);
	pClass->mName = name;

	vector<string>::iterator it;
	for (it = extendsList.begin(); it != extendsList.end(); ++it)
	{
		pClass->mExtend.push_back(FindClass(*it));
	}

	/*
	vector<string> it;
	for (it = templateArgList.begin(); it != templateArgList.end(); ++it)
	{
		pClass->mExtend.push_back(FindClass(*it));
	}*/

	tokenType = ReadToNextToken(in);
	if (tokenType != TT_BlockBegin)
	{
		int nothing = 0;
		//cout << "error, expected block begin" << endl;
	}

	do
	{
		tokenType = ReadToNextToken(in);

		switch (tokenType)
		{
		case TT_Class:
		case TT_Namespace:
			ReadClass(in, tokenType);
			break;

		case TT_Function:
			ReadFunction(in, pClass);
			break;

		case TT_Variable:
			ReadVariable(in, pClass);
			break;


		case TT_BlockEnd:
			return;
		}

	} while (tokenType != TT_EndOfFile);
}

void Parser::ReadFunction(File& in, Class* pClass)
{	
	// a function header can use up to 2 lines
	char line[1024];
	in.ReadLine(line, 1024);

	int filePos = in.GetPosition();

	char line2[1024];
	in.ReadLine(line2, 1024);
	strcpy(line2, ScriptFile::StripWhitespace(line2).c_str());

	if (strcmpi(line2, "(") == 0)
	{
		in.Seek(filePos);
	}
	else
	{
		strcat(line, " ");
		strcat(line, line2);
	}

	string name;
	string arg;
	string returnArg;
	string whereArg;
	string templateArg;

	// extract out the name, arguments, and where clause list
	cmatch matches;
	regex expression("^( (.*)\\((.*)\\) returns \\((.*)\\) where \\((.*)\\)| (.*)\\((.*)\\) returns \\((.*)\\))$");
	if (regex_match(line, matches, expression))
	{
		int first = 2;

		string name1(matches[first + 0].first, matches[first + 0].second);
		string name2(matches[first + 4].first, matches[first + 4].second);
		name = name1.length() ? name1 : name2;

		string arg1(matches[first + 1].first, matches[first + 1].second);
		string arg2(matches[first + 5].first, matches[first + 5].second);
		arg = arg1.length() ? arg1 : arg2;

		string returnArg1(matches[first + 2].first, matches[first + 2].second);
		string returnArg2(matches[first + 6].first, matches[first + 6].second);
		returnArg = returnArg1.length() ? returnArg1 : returnArg2;

		whereArg = string(matches[first + 3].first, matches[first + 3].second);
	}

	// now extract any template arguments
	expression = regex("^(.*)<(.*)>$");
	if (regex_match(name.c_str(), matches, expression))
	{
		templateArg = string(matches[2].first, matches[2].second);
		name = string(matches[1].first, matches[1].second);
	}

	// extract template args
	vector<string> templateArgList = ScriptFile::Tokenize(templateArg, ",", true);

	// extract the argument list
	vector<string> argList = ScriptFile::Tokenize(arg, ",", true);

	// extract the where list
	vector<string> whereList = ScriptFile::Tokenize(whereArg, ",", true);

	Function* func = new Function();
	func->mName = name;
	func->mClass = pClass;

	if (strcmpi(returnArg.c_str(), "void") != 0)
	{
		func->mReturnArgument = ProcessVariable(returnArg.c_str());
	}

	vector<string>::iterator it;
	for (it = argList.begin(); it != argList.end(); ++it)
	{
		Variable* var = ProcessVariable((*it).c_str());
		func->mArgument.push_back(var);
	}

	pClass->mFunction.push_back(func);

	// read past the (
	in.ReadLine(line, 1024);

	func->mCodeBlock = ReadCodeBlock(in, func, 0);
}

Variable* Parser::ProcessVariable(const char* line)
{
	string size;
	string name;
	string type;
	string defaultValue;

	// extract the type, size and name, and default value of this arg
	cmatch matches;
	regex expression("^((.*?)([0123456789]*) (.*) = (.*)|(.*?)([0123456789]*) (.*))$");
	//regex expression("^((.*) (.*) = (.*)|(.*) (.*))$");
	if (regex_match(line, matches, expression))
	{
		int first = 2;

		string type1(matches[first + 0].first, matches[first + 0].second);
		string type2(matches[first + 4].first, matches[first + 4].second);
		type = type1.length() ? type1 : type2;

		string size1(matches[first + 1].first, matches[first + 1].second);
		string size2(matches[first + 5].first, matches[first + 5].second);
		size = type1.length() ? size1 : size2;

		string name1(matches[first + 2].first, matches[first + 2].second);
		string name2(matches[first + 6].first, matches[first + 6].second);
		name = name1.length() ? name1 : name2;

		defaultValue = string(matches[first + 3].first, matches[first + 3].second);	
	}

	Variable* var = new Variable();
	var->mName = name;
	var->mSize = atoi(size.c_str());

	if (strcmpi(type.c_str(), "any") == 0)
		var->mType = VT_Any;

	if (strcmpi(type.c_str(), "int") == 0)
		var->mType = VT_Int;

	if (strcmpi(type.c_str(), "uint") == 0)
		var->mType = VT_UInt;

	if (strcmpi(type.c_str(), "real") == 0)
		var->mType = VT_Real;

	if (strcmpi(type.c_str(), "ureal") == 0)
		var->mType = VT_UReal;

	if (strcmpi(type.c_str(), "string") == 0)
		var->mType = VT_String;

	switch (var->mType)
	{
	case VT_UInt:
		var->mDefault.mUInt = atoi(defaultValue.c_str());
		break;

	case VT_UReal:
		var->mDefault.mUReal = atof(defaultValue.c_str());
		break;

	case VT_Int:
		var->mDefault.mInt = atoi(defaultValue.c_str());
		break;

	case VT_Real:
		var->mDefault.mReal = atof(defaultValue.c_str());
		break;

	case VT_String:
		var->mDefault.mString = defaultValue;
		break;

	case VT_Any:
		var->mDefault.mString = defaultValue;
		break;
	}

	var->SetDefault();

	return var;
}

CodeBlock* Parser::ReadCodeBlock(File& in, Function* pFunc, CodeBlock* pParent)
{
	char line[1024];

	CodeBlock* code = new CodeBlock();
	code->mFunction = pFunc;
	code->mParent = pParent;
	
	do
	{
		// if this line an if line?
		if (ReadToNextIfToken(in, TT_If) == TT_If)
		{
			Node* node = ProcessCodeBlockCondition(in, code);
			code->mNode.push_back(node);
		}
		else if (ReadToNextIfToken(in, TT_Return) == TT_Return)
		{
			in.ReadLine(line, 1024);
			strcpy(line, ScriptFile::StripWhitespace(line).c_str());

			Variable* returnVar = pFunc->FindVariable(line);
			if (returnVar)
			{
				// we should verify the return type fits the current type of the return parameter
				pFunc->mReturnArgument->mType = VT_Pointer;
				pFunc->mReturnArgument->mCurrentValue.mVariable = returnVar;
				pFunc->mReturnArgument->mDefault.mVariable = returnVar;
			}
			else // must be a constant or an error
			{
				delete pFunc->mReturnArgument;
				pFunc->mReturnArgument = ProcessVariable(line);
			}
		}
		else
		{
			in.ReadLine(line, 1024);
			strcpy(line, ScriptFile::StripWhitespace(line).c_str());

			if (strcmpi(line, "(") == 0 || strlen(line) <= 0)
				continue;

			if (strcmpi(line, ")") == 0)
				return code;

			Node* node = ProcessCodeBlockExpression(line, code);
			code->mNode.push_back(node);
		}
		
	} while (!in.EndOfFile());

	return 0;
}

Node* Parser::ProcessCodeBlockCondition(File& in, CodeBlock* code)
{/*
	int startPos = in.GetPosition();

	if (ReadToNextToken(in, TT_BlockBegin) != TT_BlockBegin)
		return 0;

	int endPos = in.GetPosition();

	in.Seek(startPos);

	char nextLine[1024];
	memset(nextLine, 0, 1024);
	in.Read(nextLine, endPos - startPos);

	string strNextLine = nextLine;
	int last = strNextLine.find_last_of('(');
	nextLine[last] = '\0';
*/
	// if can only span 1 line atm, may need to end the line with 'then'
	char line[1024];
	in.ReadLine(line, 1024);
	string strLine = ScriptFile::StripWhitespace(line);
/*
	if (strLine.length() <= 0)
	{
		in.ReadLine(line, 1024);
		strLine = ScriptFile::StripWhitespace(line);
	}
*/
	ReadToNextToken(in, TT_BlockBegin);

	cmatch matches;
	if (regex_match(strLine.c_str(), matches, regex("^\\((.*)\\)$")))
	{
		string match(matches[1].first, matches[1].second);

		Node* boolResultNode = ProcessCodeBlockExpression(match.c_str(), code);

		//Node* boolResultNode = ProcessCodeBlockBooleanExpression(match.c_str(), code);
		/*
		while (1)
		{
			string match(matches[1].first, matches[1].second);
			if (regex_match(strLine, matches, expression("^(.*?) (and|or) (.*)$")))
			{
				string expMatch(matches[1].first, matches[1].second);
				string boolMatch(matches[2].first, matches[2].second);
			}
		}*/

		BranchNode* node = new BranchNode();
		node->mExpression = boolResultNode;
		node->mCode = ReadCodeBlock(in, code->mFunction, code);

		if (ReadToNextIfToken(in, TT_Else) == TT_Else)
		{
			node->mNext = ProcessCodeBlockCondition(in, code);
		}

		return node;
	}

	CodeBlockNode* node = new CodeBlockNode();
	node->mCode = ReadCodeBlock(in, code->mFunction, code);
	return node;
}
/*
Node* Parser::ProcessCodeBlockBooleanExpression(const char* line, CodeBlock* code)
{
	string match(matches[1].first, matches[1].second);
	if (regex_match(strLine, matches, expression("^(.*?) (and|or) (.*)$")))
	{
		string expMatchLeft(matches[1].first, matches[1].second);
		string boolMatch(matches[2].first, matches[2].second);
		string expMatchRight(matches[3].first, matches[3].second);

		Node* lhs = ProcessCodeBlockExpression(expMatchLeft, code);
		Node* rhs = ProcessCodeBlockBooleanExpression(expMatchRight, code);
	}

	int nothing = 0;
}*/

Node* Parser::ProcessCodeBlockExpression(const char* line, CodeBlock* code)
{
	// this needs to happen in reverse order
	//for (int i = OT_Max; i >= 0; --i)
	for (int i = 0; i < OT_Max; ++i)
	{
		cmatch matches;
		char expStr[1024];
		sprintf(expStr, "^(.*) %s (.*)$", operatorList[i]);
		regex expression(expStr);
		if (regex_match(line, matches, expression))
		{
			string lhs(matches[1].first, matches[1].second);
			string rhs(matches[2].first, matches[2].second);

			OperatorNode* n = new OperatorNode();
			n->mOperator = (OperatorType)i;

			n->mLeft = ProcessCodeBlockExpression(lhs.c_str(), code);
			n->mRight = ProcessCodeBlockExpression(rhs.c_str(), code);
			return n;
		}
	}

	// couldnt subdivie this any more, so now we need to work out if we are a function call or what....
	
	// check if its a string
	cmatch matches;
	if (regex_match(line, matches, regex("^'(.*)'$")))
	{
		ConstVariableNode* n = new ConstVariableNode();
		n->mVariable.mType = VT_String;
		n->mVariable.mCurrentValue.mString = line;
		return n;
	}	

	// check if its a float
	if (regex_match(line, matches, regex("^[0123456789]*\\.[0123456789]*$")))
	{
		ConstVariableNode* n = new ConstVariableNode();
		n->mVariable.mType = VT_Real;
		n->mVariable.mCurrentValue.mReal = atof(line);
		return n;
	}	

	// see if its a integer
	if (regex_match(line, matches, regex("^[0123456789]*$")))
	{
		ConstVariableNode* n = new ConstVariableNode();
		n->mVariable.mType = VT_Int;
		n->mVariable.mCurrentValue.mInt = atoi(line);
		return n;
	}
	
	// else, it must be a variable or a function
	VariableNode* n = new VariableNode();
	n->mVariable = code->FindVariable(line);
	if (!n->mVariable)
	{
		n->mVariable = code->mFunction->FindVariable(line);
		if (!n->mVariable)
		{
			n->mVariable = code->mFunction->mClass->FindVariable(line);
			if (!n->mVariable)
			{
				// must be a function call?
				int noting = 0;
			}
		}
	}
	return n;
}

void Parser::ReadVariable(File& in, Class* pClass)
{

}

Class* Parser::FindClass(const string& name)
{
	vector<Class*>::iterator it;
	for (it = mClass.begin(); it != mClass.end(); ++it)
	{
		if (strcmpi(name.c_str(), (*it)->mName.c_str()) == 0)
			return (*it);
	}

	return 0;
}

Parser::TokenType Parser::ReadToNextToken(File& in)
{
	TokenType tokenType = TT_Unknown;
	//int filePos = in.GetPosition();
	//int rewindLen = 0;
	char line[1024];
	in.ReadString("%s", line);
	strcpy(line, ScriptFile::StripWhitespace(line).c_str());

	//rewindLen = strlen(line);

	tokenType = GetToken(in, line);

	if (tokenType == TT_Unknown)
		return ReadToNextToken(in);

	// rewind to begining of the token
	//in.Seek(rewindLen, File::Current);

	return tokenType;
}

Parser::TokenType Parser::ReadToNextToken(File& in, TokenType tokenType)
{
	TokenType lastTokenType;
	while ((lastTokenType = ReadToNextToken(in)) != tokenType)
	{
		if (lastTokenType == TT_EndOfFile)
			return lastTokenType;
	}

	return lastTokenType;
}

Parser::TokenType Parser::ReadToNextIfToken(File& in, TokenType tokenType)
{
	int filePos = in.GetPosition();
	char str[1024] = "\0";
	in.ReadString("%s", str);

	if (ScriptFile::StripWhitespace(str).length() <= 0)
		return ReadToNextIfToken(in, tokenType);

	if (GetToken(in, ScriptFile::StripWhitespace(str).c_str()) == tokenType)
		return tokenType;

	in.Seek(filePos);
	return TT_Unknown;
}

Parser::TokenType Parser::GetToken(File& in, const char* line)
{
	TokenType tokenType = TT_Unknown;

	if (in.EndOfFile())
		return TT_EndOfFile;

	if (strcmpi(line, "class") == 0)
		tokenType = TT_Class;

	if (strcmpi(line, "namespace") == 0)
		tokenType = TT_Namespace;

	if (strcmpi(line, "func") == 0)
		tokenType = TT_Function;

	if (strcmpi(line, "var") == 0)
		tokenType = TT_Variable;

	if (strcmpi(line, "if") == 0)
		tokenType = TT_If;

	if (strcmpi(line, "else") == 0)
		tokenType = TT_Else;

	if (strcmpi(line, "return") == 0)
		tokenType = TT_Return;

	// block begin and end dont rewind
	if (strcmpi(line, "(") == 0)
		return TT_BlockBegin;

	if (strcmpi(line, ")") == 0)
		return TT_BlockEnd;

	return tokenType;
}

CommentType Parser::ProcessComment(char* line)
{
	// check for block comment
	{
		cmatch match;
		regex expression("(.*);(.*");

		if (regex_match(line, match, expression))
		{
			line[string(match[1].first).length() - string(match[1].second).length()] = '\0';
			return CT_Block;
		}
	}

	// check for single line comment
	{
		cmatch match;
		regex expression("(.*);.*");

		if (regex_match(line, match, expression))
		{
			line[string(match[1].first).length() - string(match[1].second).length()] = '\0';
			return CT_Line;
		}
	}

	return CT_None;
}

void Parser::StripBlockComment(File& in)
{

}

Variable* Parser::EvaluateFunction(const string& className, const string& functionName, int count, ...)
{
	Class* pClass = FindClass(className);
	Function* pFunc = pClass->FindFunction(functionName);

	vector<Variable*> vars;

	va_list ap;
  	va_start(ap, count);
	for (int i = 0; i < count; ++i)
	{
		vars.push_back(va_arg(ap, Variable*));
	}
  	va_end(ap);

	Variable* ret = pFunc->Evaluate(vars);
	int nothing = 0;
	return ret;
}
