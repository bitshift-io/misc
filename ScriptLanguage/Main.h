#ifndef _MAIN_H_
#define _MAIN_H_

#include "StdAfx.h"

using namespace std;

#define gScriptLanguage ScriptLanguage::GetInstance()

//
// There can only be one game running per application
//
class ScriptLanguage : public Singleton<ScriptLanguage>
{
public:

	int				Main(const char* cmdLine);

protected:

};

#endif
