#ifndef _VARIABLE_H_
#define _VARIABLE_H_

enum VariableType
{
	VT_UInt,
	VT_UReal,
	VT_Int,
	VT_Real,
	VT_String,
	VT_Any,
	VT_Pointer,
};

class Variable
{
public:

	Variable()
	{
	}

	Variable(const string& value)
	{
		mType = VT_String;
		mSize = 32;
		mCurrentValue.mString = value; 
	}

	Variable(float value)
	{
		mType = VT_Real;
		mSize = 32;
		mCurrentValue.mReal = value;
	}

	Variable(int value)
	{
		mType = VT_Int;
		mSize = 32;
		mCurrentValue.mInt = value;
	}

	void			SetDefault();

	string			mName;
	int				mSize;
	VariableType	mType;

	struct Value
	{
		Value() :
			mUReal(0.f),
			mUInt(0),
			mReal(0.f),
			mInt(0),
			mVariable(0)
		{
		}

		unsigned float	mUReal;
		unsigned int	mUInt;
		float			mReal;
		int				mInt;
		string			mString;
		Variable*		mVariable;
	};

	Value			mCurrentValue;
	Value			mDefault;
};

#endif