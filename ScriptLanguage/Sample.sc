include somefile.sc

; the following is a block comment:
;(
 type keywords:
 int
 real
 any
 str
 
 post fix:
 indicates the size of the type, this is optional
 eg.
 int32
 int64

 pointer/reference:
 these have been merged and are post fixed after the type using the $ operator

 valid variables:
 a function can be used to valid any assignment of a variable
 using the : operator. A comma seperated of checking functions follows
   		
 all funtions and variables must be a member of either a namespace or a class
)

; sample type declaration:
int32		mVar

; sample pointer/reference, can be null
int32$		mPointer

; sample variable, with a validator function, as well as assigning an initial value
float32		mVar = 4 	where	ValuePositive, ValuePowerOfTwo

;  another validator, we can simply put in a code function
float32		mVar = 4 	where	(myVal >= 4)

; functions also take validators, validators must return boolean values
; these are to catch errors at compile time
void FunctionValidated(int x, int y) where (x > y), (x != 0)
(

)


;(
 strctures/classes:
 these are one and the same,
 
 namespaces:
 static classes/namespaces are the same
)

;(
	arrays:
	an array of elements, is encapsulated in the ' marker
	and is comma sepetrated (execept for strings)
	for multi demensional array the array mus be enclosed by brackets
	eg.
	
	str			stringArray		= 'my string'
	int[4]		intArray		= '2,4,5,6'
	int8[][]	intArray		= ''a, c, h', '5, 6, 7''
	
	special arrays:
	if you dont want an array of void$'s you can have an array of values
	array
	
)

namespace MyNamespace
(
	; namespace is a static class, so has a constructor and destructor when the app loads/closes
	construct()
	{
		myVal = 3
	}
	
	void StaticLikeFunction(any unknownValue)
	{
	
	}
		
	class ClassT
	(
		; shows of cunstructors and destructors, they have no return type
		construct()
		{
		
		}
		
		construct(int value)
		(
		
		)
		
		destruct()
		{
		
		}
		
		; not a constructor as it has a return type
		void Create()
		{
		
		}
		
		; this show a function can take an array of any values
		void Print(str text, any[] args)
		(
			loop (int i = 0, i < args.length, ++i)
			(
				any$ value = args[i]
				; use value here
			)
		)
	)
	
	void SetMyVal(int val)
	(
	)
	
	int GetMyVal()
	(
		return myVal
	)

	int myVal = 2
)

; sample class
class ClassA
(
	; a function
	void$	Foo(int$ i, real$ d)
	(
		; samples of loops
		loop (int i = 0, i < 4, ++i)
		(
			; we can check is a class has a function, how ever, if it does not exist no errors will occur
			if (mClassVariable.Print != null)
				mClassVariable.Print('what t3h fuck', '2, 3, 4', 3)
				
			; we can do a more specific function check:
			if (mClassVariable.Print(string, array) != null)
				mClassVariable.Print('what t3h fuck', '2, 3, 4')
			
		)
		
		loop (i < 4, ++i)
		(
		)
		
		loop (int i = 0, i < 4)
		(
		)
		
		; descision making
		if (i < 4)
		(
		
		)
		else if (i > 5)
		(
		
		)
		else
		(
		
		)
		
		
	)
	
	void TestTypeCheck(any unknownValue)
	(
		if (unknownValue.size > 32)
		(
			; find something of size more than 32, eg. float64, int64
		)
		
		if (unknownValue.type == int)
		(
			; found an integer, eg. int32, int64	
		)
		
		; we can also get an array of the values function checks
			
	)
	
	; here we are testing nameing of code blocks, and resuing them in another function
	void ExtendFunctionTest()
	(
		code block1
		(
			int test = 4
			Print('in block1')
		)
		
		code block2
		(
			Print('in block2')
		)
	)
	
	void ExtendFunctionTest2()
	{
		use code ExtendFunctionTest.block1
		use code ExtendFunctionTest.block2		
	}
	
	; the following two functions show of default parameters
	; and how to call a function using default parameters
	void DefaultFunction(int val = 4, int val2 = 5, real val3, real val5 = 2.3)
	(
	
	)
	
	void CallDefaultFunc()
	{
		DefaultFunc(default, default, 4.0, default)
	}
	
	; is such a feature needed:
	void VarFunc()
	(
		int def = 4
		def = 5
		def = default ; this will assign 4 to def, its the initial or default value
	)

	
	-- variable
	void$	mVariable1
	ClassT	mClassVariable
)

class ClassB : MyNamespace.ClassT
(

)

class ClassC : ClassA, ClassB
(

)


;(
	Templates:
	how can we validate data types in a clean way? : is already used for extends,
	should : be replaecd with extends while : is struictly reserved for validators?	
	namespaces can also be templated
)

int TemplateFunc <type t, type d> (t val1, d$ val2)
(
	return val1 * val2	
)

class ClassD <type t> where (t.type != int)
(
	void Func(t$ val1)
	(
		myVar = val1
	)
	
	t$ myVar
)

;(
	Application Entry:
	this will load main.main(command line aruguments)
)


namespace Main
(
	construct(any[] args)
	(
			
	)
	
	destruct()
	(
	
	)
	
	;
	; What the fuck?!
	;
	int	Main(any[] args)
	(
		; create a c on the stack hrmmm, all classes on the heap?
		MyNamespace.ClassT t = MyNamespace.ClassT(4)
		t.Print('woohoo')
		
		MyNamespace.StaticLikeFunction(4)
		
		; create a c on the heap
		ClassC$ c = ClassC()
		
		; test 
		if (c.extends(ClassA))
		(
			ClassA a = c
		)
		
		ClassD<real32> d
		d.Func(3.4)
		
	)
)


;(
	ideas
)

class T
(
	func template GreaterThanZero<type T>(T val) returns (bool)
	(
		return val > 0
	)
	
	func Foo(inout int a, inout int b, int c) returns (int t)
		where (a > b and > c), (t != 0)
	(
		return 5
	)
	
	func Foo2(inout int a, inout int b) returns (void)
		where GreaterThanZero<int>(a)
	(
		int t = T.Foo(3, 5, 6)
	)
	
	; operator key words mean we can use this function in the following way:
	; x = T * P 
	; the compiler will change this to:
	; x = T.premul(P.postmul())
	preoperator *(inout )
	(
	
	)
	
	
	; stealing a page out of d
	; these accessors will be called if the following occurs in code:
	; T.test = 4 or val = T.test
	test(in uint64 val) returns (void)
	(
		test = val
	)
	
	test() return (uint64)
	(
		return test
	)	
	
	var uint64 test
)