--###############################################################################
--## Script Description
--## This script will automatically setup/configure layers(Ala layer manager)
--## Bronson Mathews 2007, gibbz1@yahoo.com
--###############################################################################

global mLayerManagerLastSelection = #()

--###############################################################################
--## Functions
--## The shiz!
--###############################################################################

--## Create initial Layers
fn fnCreateLayer valName = 
(
	mLayer = LayerManager.getLayerFromName valName
	if mLayer == undefined do
		mLayer = LayerManager.newLayerFromName valName
		
	return mLayer
)

--## Create initial Layers
fn fnResetLayers =
(
	-- rename defulat layer
	mLayerDefault = LayerManager.getlayer 0
	
	
	-- setup geometry layer
	mLayer = fnCreateLayer "Geometry"
	
	mLayer.on = true
	mLayer.lock = true
	mLayer.ishidden = false
	mLayer.isfrozen = false	
	mLayer.wireColor = color 200 200 200 255
	mLayer.renderable = true
	mLayer.isGIExcluded = false
	
	
	-- setup Collision/Physics layer
	mLayer = fnCreateLayer "Physics"
	
	mLayer.on = true
	mLayer.lock = true	
	mLayer.ishidden = false
	mLayer.isfrozen = false
	mLayer.wireColor = color 214 229 166 255
	mLayer.renderable = false
	mLayer.isGIExcluded = true	
	
	
	-- setup Sound layer
	mLayer = fnCreateLayer "Sounds"
	
	mLayer.on = true
	mLayer.lock = true
	mLayer.ishidden = false
	mLayer.isfrozen = false
	mLayer.wireColor = color 224 143 87 255
	mLayer.renderable = false
	mLayer.isGIExcluded = true		
	
	
	-- setup Helper layer
	mLayer = fnCreateLayer "Helpers"
	
	mLayer.on = true
	mLayer.lock = true
	mLayer.ishidden = false
	mLayer.isfrozen = false
	mLayer.wireColor = color 153 228 153 255
	mLayer.renderable = false
	mLayer.isGIExcluded = true	


	-- setup Bones layer
	mLayer = fnCreateLayer "Bones"
	
	mLayer.on = true
	mLayer.lock = true
	mLayer.ishidden = false
	mLayer.isfrozen = false
	mLayer.wireColor = color 88 199 225 255
	mLayer.renderable = false
	mLayer.isGIExcluded = true	


	-- setup Cameras layer
	mLayer = fnCreateLayer "Cameras"
	
	mLayer.on = true
	mLayer.lock = true
	mLayer.ishidden = false
	mLayer.isfrozen = false
	mLayer.wireColor = color  141 7 58 255
	mLayer.renderable = false
	mLayer.isGIExcluded = true		
	
	
	-- setup Lights layer
	mLayer = fnCreateLayer "Lights"
	
	mLayer.on = true
	mLayer.lock = true
	mLayer.ishidden = false
	mLayer.isfrozen = false
	mLayer.wireColor = color 228 184 153 255
	mLayer.renderable = false
	mLayer.isGIExcluded = true	

	-- setup Particles layer
	mLayer = fnCreateLayer "Particles"
	
	mLayer.on = true
	mLayer.lock = true
	mLayer.ishidden = false
	mLayer.isfrozen = false
	mLayer.wireColor = color 228 184 153 255
	mLayer.renderable = false
	mLayer.isGIExcluded = true	
	
	
	-- setup Decals layer
	mLayer = fnCreateLayer "Decals"
	
	mLayer.on = true
	mLayer.lock = true
	mLayer.ishidden = false
	mLayer.isfrozen = false
	mLayer.wireColor = color 228 184 153 255
	mLayer.renderable = false
	mLayer.isGIExcluded = true	
	
	
	-- setup Projectors layer
	mLayer = fnCreateLayer "Projectors"
	
	mLayer.on = true
	mLayer.lock = true
	mLayer.ishidden = false
	mLayer.isfrozen = false
	mLayer.wireColor = color 228 184 153 255
	mLayer.renderable = false
	mLayer.isGIExcluded = true	
	
		
)

--## Add to layer
fn fnAddToLayer valLayer valObj =
(
	-- check if layer exists
	mLayer = LayerManager.getLayerFromName valLayer
	if mLayer == undefined do
	(
		fnResetLayers()
		
		-- now try again :)
		mLayer = LayerManager.getLayerFromName valLayer
		if mLayer == undefined do
			format "Error copying % to Layer %!\n" valObj valLayer -- you FAIL!
	)
	
	-- now move to layer
	mLayer.addnode valObj
	
	-- now inherit the layer color!
	valObj.wirecolor = valObj.INodeLayerProperties.layer.wirecolor

	redrawViews()	
	
)

--## main layer manager
-- valNameArray  = old name, new name, and node
fn fnManageLayers valSelection =
(
	-- need a bit of a hack to force update if we need to!
	if valSelection != 0 do
		mLayerManagerLastSelection = valSelection as array
		
	-- this is 1 selection behind the current selection, sneaky shiz :)
	for i = 1 to mLayerManagerLastSelection.count do
	(
		mObj = mLayerManagerLastSelection[i] 
		mNewName = mObj.name
		
		mFilterString = filterString mNewName "_"
		
		-- Add to mesh/geometry layer
		if mFilterString[1] == "M" or mFilterString[1] == "G" do
			fnAddToLayer "Geometry" mObj
	
		-- Add to collision/physics layer
		if mFilterString[1] == "C" or mFilterString[1] == "PX" or mFilterString[1] == "CX" do
			fnAddToLayer "Physics" mObj
	
		-- Add to helper layer
		if mFilterString[1] == "R" or mFilterString[1] == "Z" or mFilterString[1] == "H" or (superclassof mObj) == helper do
			fnAddToLayer "Helpers" mObj
			
		-- Add to bones layer
		if mFilterString[1] == "B" or (classof mObj) == bone do
			fnAddToLayer "Bones" mObj
			
		-- Add to projectors layer
		if mFilterString[1] == "P" do
			fnAddToLayer "Projectors" mObj
			
		-- Add to particles layer
		if mFilterString[1] == "P" do
			fnAddToLayer "Particles" mObj			

		-- Add to decals layer
		if mFilterString[1] == "D"  do
			fnAddToLayer "Decals" mObj			
			
		-- Add to Cameras
		if (superclassof mObj) == camera do
			fnAddToLayer "Cameras" mObj	
	
		-- Add to Lights
		if (superclassof mObj) == light do
			fnAddToLayer "Lights" mObj	

	)
	
	-- hacky hacky
	if valSelection == 0 do
		mLayerManagerLastSelection = selection as array

)


--## this will delete all existing/unused layers
fn fnClearLayers =
(
	mLayerDefault = LayerManager.getlayer 0
	mLayerDefault.current = true
	
	for mObjects in objects do
		mLayerDefault.addnode mObjects

	-- delete backwards!
	for i = (layermanager.count - 1) to 1 by -1 do
	(
		mLayer = LayerManager.getlayer i
		mBool = layermanager.deleteLayerByName (mLayer.name)
	)

)

--## This will filter a existing scene into the correct layers :D
fn fnFilterScene =
(
	-- clear out old layers
	fnClearLayers()
	fnResetLayers()
	
	mObjects = objects as array
	fnManageLayers mObjects
)


--## Run this when max is closed! Or cleanup!
fn fnReset =
(
	callbacks.removeScripts #nodeNameSet id:#callbackLayerManager
	callbacks.removeScripts #selectionSetChanged id:#callbackUpdateLayers
)

--###############################################################################
--## Rollouts and Shiz
--## Main UI here with buttons and stuff...
--###############################################################################

clearListener()
fnReset()

-- use a callback when the node is renamed :)
-- callbacks.addScript #nodeNameSet "mMoveToLayerList = callbacks.notificationParam()" id:#callbackLayerManager

-- used to move the last updated object to its layer
callbacks.addScript #selectionSetChanged "fnManageLayers 0" id:#callbackUpdateLayers


-- callback to kill the script when max closes

-- TODO, use the dropdown menu script to add a button for this to the menubar, which will allow 
-- the user to reset the layers manually :) using : fnFilterScene()
