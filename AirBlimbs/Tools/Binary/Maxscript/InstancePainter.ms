-- Instance Painter
-- Bronson Mathews 2006

if mRolloutFloater != undefined do (closeRolloutFloater mRolloutFloater)

-- GLOBALS
global mImportFileName = undefined
global mExportFileName = undefined
global mInstanceBlade = undefined
global mStrokeNodes = #()

-- FUNCTIONS
fn fnGetValidNodes valArray = -- this cannot be mapped?!
(
	mValidNodes = #()
	for i = 1 to valArray.count do
	(
		if (isValidNode valArray[i]) do
			append mValidNodes valArray[i]
	)
	return mValidNodes 
)

fn fnRoundNumber valFloat valDigits = --round off
(
	mMultiply = 1.0
	for i = 1 to valDigits do
		mMultiply *= 10

	mRounded = (valFloat * mMultiply) as integer
	mRounded = mRounded as float
	mRounded = (mRounded / mMultiply) 
	return mRounded
)

fn fnCreateTriMesh = -- function creates a mesh, with center of triangle as pivot point!
(
	mTriMesh = mesh vertices:#([0,10,0],[8.6603,-5,0],[-8.6603,-5,0]) faces:#([1,3,2]) -- 10cm tri to make it easier to see...
	mTriMesh.material = standardMaterial()
	mTriMesh.name = "TriMesh"	
	convertToMesh mTriMesh
	return mTriMesh
)

fn fnGetGroups = --collects a array of groups
(
	mGroups = #()
	for i in helpers do
	(
		if (isGroupHead i) do
			append mGroups i
	)
	return mGroups
)

fn fnGetGroupChildren valGroupNode = --collect the nodes in a group
(
	mChildNodes = #()
	mChildren = valGroupNode.children
	for i in mChildren do
	(
		append mChildNodes i
	)
	return mChildNodes
)

fn fnGetGroupNode valGroupName = -- gets a group Node from group name
(
	mGroups = #() -- possibly multiple groups?!
	mGroupNode = undefined
	mGroupNodes = fnGetGroups()

	for i in mGroupNodes do
		if i.name == valGroupName do
			append mGroups i
			
	if mGroups.count > 1 then --if multiple groups with same name, lets merger them!
	(
		mChildren = #()
		for i in mGroups do
		(
			append mChildren (fnGetGroupChildren i)
			explode i -- delete the group
		) 
		mGroupNode = group mChildren name:valGroupName select:false --make our new group!
	)
	else
		mGroupNode = mGroups[1]

	return mGroupNode
)

fn fnEndPaintSession =
(
	mStrokeNodes = fnGetValidNodes mStrokeNodes
	if mStrokeNodes.count != 0 do
	(
		mPrefix = rolloutInstance.txtGroupName.text
		thePainterInterface.endPaintSession()
		mName = mPrefix + "." + mInstanceBlade.name --this is the name of the new group node
		mGroup = fnGetGroupNode mName -- check to see if the group exists...
		if mGroup != undefined do --append the othergroups children to out nodes!
		(
			mChildren = fnGetGroupChildren mGroup
			mStrokeNodes = mStrokeNodes + mChildren 
			ungroup mGroup -- delete the old group
		)
		
		mStrokeNodes = fnGetValidNodes mStrokeNodes
		group mStrokeNodes name:mName 
	
		mStrokeNodes = #()
	)
)

mapped fn fnGetVariationNormal valNormal valVariationAngle = -- function takes normal variation(0-1) and a normal
(
	mNMatrix = matrixFromNormal valNormal 

	-- pick a RANDOM direction for our normal
	mRandZ = random 0.0 360.0
	mZMatrix = rotateZMatrix mRandZ 
	
	-- now bend that away by random amount
	mRandY = random 0.0 valVariationAngle 
	mYMatrix = rotateYMatrix mRandY

	mTranformMatrix =  mYMatrix * mZMatrix
	mReturnNormal = mTranformMatrix * mNMatrix
	mReturnNormal = mReturnNormal.row3
	
	return mReturnNormal
)

mapped fn fnCalculateSpacing valObj = -- returns min spacing value for even spacing
(
	mDistance = #()
	mMin = valObj.min
	mMax = valObj.max
	mDistance[1] = abs (mMin.X - mMax.X)
	mDistance[2] = abs (mMin.Y - mMax.Y)
	mMinSpacing = amax mDistance
	return mMinSpacing
)

mapped fn fnMatrixEngineToMax valMatrix = --takes a string output a matrix
(
	matrix = filterString valMatrix ","
	for i = 1 to matrix.count do (matrix[i] = matrix[i] as float)
	
	matrixX = [matrix[1],matrix[3],matrix[2]]
	matrixY = [matrix[9],matrix[11],matrix[10]] --swap x and y
	matrixZ = [matrix[5],matrix[7],matrix[6]] 
	matrixTanslation = [matrix[13],matrix[15],matrix[14]]

	newMatrix = matrix3 matrixX matrixY matrixZ matrixTanslation
	return newMatrix 
)

mapped fn fnMatrixMaxToEngine valMatrix = --takes a matrix output a string
(
	mRow1 = valMatrix.row1
	mRow2 = valMatrix.row2
	mRow3 = valMatrix.row3
	mTranslation = valMatrix.translation

	mString = (mRow1.X as string) + ", " + (mRow1.Z as string) + ", " + (mRow1.Y as string) + ", 0, " + (mRow3.X as string) + ", " + (mRow3.Y as string) + ", " + (mRow3.Z as string) + ", 0, " + (mRow2.X as string) + ", " + (mRow2.Y as string) + ", " + (mRow2.Z as string) + ", 0, " + (mTranslation.X as string) + ", " + (mTranslation.Z as string) + ", " + (mTranslation.Y as string) + ", 1"
	return mString
)

fn fnExportINI valFile valSelection =
(
	numBlade = 0
	for i = 1 to valSelection.count do
	(
		obj = valSelection[i]
		
		if i < 10 do (strBlade = "Blade_0000" + i as string)
		if i >= 10 and i < 100 do (strBlade = "Blade_000" + i as string)		
		if i >= 100 and i < 1000 do (strBlade = "Blade_00" + i as string)
		if i >= 1000 and i < 10000 do (strBlade = "Blade_0" + i as string)	
		if i >= 10000 and i < 100000 do (strBlade = "Blade_" + i as string)			
		
		mMatrix = fnMatrixMaxToEngine obj.transform
		setINISetting valFile strBlade "matrix" mMatrix
		
		mColor = "1 ,1 ,1 ,1"
		setINISetting valFile strBlade "color" mColor		
	)
)

fn fnImportINI valFile &valMatrixArray &valColorArray = 
(
	-- find out how many blade models in the ini file...
	file = openFile valFile
	numBlade = 0
	while not eof file do 
	(
		test = skipToString file "Blade_"
		if test != undefined do (numBlade += 1)
	)
	close file
	
	--read the ini file into the arrays
	for i = 1 to numBlade do
	(
		if i < 10 do (strBlade = "Blade_0000" + i as string)
		if i >= 10 and i < 100 do (strBlade = "Blade_000" + i as string)		
		if i >= 100 and i < 1000 do (strBlade = "Blade_00" + i as string)
		if i >= 1000 and i < 10000 do (strBlade = "Blade_0" + i as string)	
		if i >= 10000 and i < 100000 do (strBlade = "Blade_" + i as string)	
		
		matrix = getINISetting valFile strBlade "matrix"
		matrix = fnMatrixEngineToMax matrix
		append valMatrixArray matrix 
		
		color = getINISetting valFile strBlade "color"
		append valColorArray color
	)
	
)

fn fnSpawnBlade valBladeModel valMatrixArray valColorArray =
(
	mInstanceNodes = #()
	for i = 1 to valMatrixArray.count do
	(
		--instance
		mInstance = instance valBladeModel 
		mInstance.transform *= inverse mInstance.transform --reset transform(ie center it at world)
		mInstance.transform = mInstance.transform *  valMatrixArray[i]	
		append mInstanceNodes mInstance
	)
	return mInstanceNodes
)

fn fnCreateCollapsedMesh valBladeModel valMatrixArray valColorArray =
(
	mInstanceNodes = #()
	mScale = #()
	for i = 1 to valMatrixArray.count do
	(
		--instance
		mInstance = copy valBladeModel 
		append mInstanceNodes mInstance		
		mInstance.transform *= inverse mInstance.transform --reset transform(ie center it at world)
		mInstance.transform = mInstance.transform *  valMatrixArray[i]
		convertToMesh mInstance	
		append mScale (mInstance.scale[1])
	)
	
	-- quick hack to add vertex alpha!
	mScaleMin = amin mScale 
	mScaleMax = amax mScale 
	mScaleAverage = (mScaleMin + mScaleMax) / 2
	mMultiply = 255 / mScaleAverage
	for i = 1 to valMatrixArray.count do
	(
		if mScale[i] <= mScaleAverage  do 
		(
			mScale[i] *= mMultiply	
			meshOp.setVertColor mInstanceNodes[i] -2 #{1..3} (color mScale[i] mScale[i] mScale[i])
		)
	)
	
	mObj = mInstanceNodes[1]
	for i = 2 to mInstanceNodes.count do
		attach mObj mInstanceNodes[i]

	return mObj 
)

mapped fn fnDistanceCheck valPosition valCompareArray valDistance =
(
	mBool = true
	for i in valCompareArray do
	(
		mDist = distance valPosition i.pos
		if mDist < valDistance do
		(
			mBool = false
			exit
		)
	)
	return mBool
)

fn fnStrokeStart =
(
	mStrokeNodes = fnGetValidNodes mStrokeNodes -- check for valid nodes
	thePainterInterface.undoStart()
)

mapped fn fnStrokePaint =
(
	mLocalHit = Point3 0 0 0
	mLocalNormal = Point3 0 0 0
	mWorldHit = Point3 0 0 0
	mWorldNormal = Point3 0 0 0
	mStr = 0.0f
	mRadius = 0.0f
	
	mPressure = 1.0f -- what range is this?! Assume this is 0-1
	mShift = false
	mCtrl = false
	mAlt = false
	
	--read in ui stuff
	mMinimumSpacing = rolloutPainter.spinMinSpacing.value
	mNormalBlendAmount = rolloutPainter.slideNormalBlend.value / 100.0 -- makes this 0 to 1
	mNormalVariationAmount = rolloutPainter.slideNormalVariation.value -- 0 - 180 degrees
	mNormalDirection = rolloutPainter.dropNormalDirection.selected
	mRotationVariationAmount = rolloutPainter.slideRotationVariation.value -- 0 - 360 degrees
	mScaleMethod = rolloutPainter.dropScaleMethod.selected 
	mMinimumScale = rolloutPainter.slideMinimumScale.value
	mMaximumScale = rolloutPainter.slideMaximumScale.value	
	
	--did we hit something?
	mHit = thePainterInterface.getIsHit -1
 
 	if mHit do
	(
		thePainterInterface.getHitPointData &mLocalHit &mLocalNormal &mWorldHit &mWorldNormal &mRadius &mStr 0 --tabIndex
		thePainterInterface.getHitPressureData &mShift &mCtrl &mAlt &mPressure 0 --<integer>tabIndex
		
		-- do a check against all other nodes to see if the distance is greater than min distance, returns true.
		mDist = fnDistanceCheck mWorldHit mStrokeNodes mMinimumSpacing

		if mDist do
		(
			mInstanceMesh = instance mInstanceBlade
			append mStrokeNodes mInstanceMesh -- use this to compare distance/spacing
			mInstanceMesh.backFaceCull = false -- yeah for grass
	
			-- Normal part
			if mNormalDirection == "Surface" do
			(
				mNewNormal = mWorldNormal
				mNewNormal = fnGetVariationNormal mNewNormal mNormalVariationAmount -- function takes normal variation(0-1) and a normal
			)
			
			if mNormalDirection == "Up" do
			(
				mNormal = [0,0,1] -- up normal
				mSurfaceNormal = mWorldNormal
				mNormal = mNormal * (1 - mNormalBlendAmount)
				mSurfaceNormal = mSurfaceNormal  * mNormalBlendAmount
	
				mNewNormal = normalize(mNormal + mSurfaceNormal)
				mNewNormal = fnGetVariationNormal mNewNormal mNormalVariationAmount -- function takes normal variation(0-1) and a normal
			)	

			-- make a transform matrix for our object
			mTransform = matrixFromNormal mNewNormal

			-- position part
			mTransform.translation = mWorldHit

			-- rotation part
			mRandZ = random 0.0 mRotationVariationAmount 
			mRotationMatrixZ = rotateZMatrix mRandZ 
			mTransform = mRotationMatrixZ * mTransform 	
		
			-- scale part
			if mScaleMethod == "Pen Pressure" do
			(
				mPressure = ((mMaximumScale - mMinimumScale) * mPressure) + mMinimumScale -- makes mpressure in the range of min-max
				mScaleMatrix = scaleMatrix [mPressure,mPressure,mPressure] 
			)
			if mScaleMethod == "Random" do
			(
				mRandomScale = random mMinimumScale mMaximumScale 
				mScaleMatrix = scaleMatrix [mRandomScale, mRandomScale, mRandomScale]
			)
			if mScaleMethod == "Combination" do
			(
				mRandomScale = random mMinimumScale mMaximumScale 
				mPressure = ((mMaximumScale - mMinimumScale) * mPressure) + mMinimumScale -- makes mpressure in the range of min-max
				mPressure = (mPressure + mPressure + mRandomScale)  / 3 --get the average, weighted for pressure
				mScaleMatrix = scaleMatrix [mPressure,mPressure,mPressure] 			
			)			
			mTransform = mScaleMatrix * mTransform

			-- setup our final transform \:D/
			mInstanceMesh.transform = mTransform 
		)	
	
	)
)

fn fnStrokeEnd =
(
	thePainterInterface.undoAccept()
)

fn fnStrokeCancel =
(
	print "cancel"
	thePainterInterface.undoCancel()
)

fn fnStrokeSystemEnd =
(
	print "system end"
)


-- PAINTER ROLLOUT
rollout rolloutPainter "Painter"
(
	local uiX = 5
	local uiY = 5

	button btnPaintStart "Paint" width:50 height:20 pos:[uiX,uiY] tooltip:""
	button btnPaintOptions "Brush" width:50 height:20 pos:[uiX+50,uiY] tooltip:""
	button btnPaintPlacment "Place" width:50 height:20 pos:[uiX+100,uiY] tooltip:""
	button btnPaintEnd "End" width:50 height:20 pos:[uiX+150,uiY] tooltip:""	

	dropdownlist dropNormalDirection "Normal Direction:" items:#("Surface", "Up", "Down", "Sides") pos:[uiX,uiY+=20] width:200
	slider slideNormalBlend "Normal Blend: 10%    " orient:#horizontal ticks:10 range:[0,100,10] type:#integer pos:[uiX,uiY+=40] width:200
	slider slideNormalVariation "Normal Variation: 5    " orient:#horizontal ticks:10 range:[0,180,5] type:#integer pos:[uiX,uiY+=50] width:200

	dropdownlist dropScatterPattern "Scatter Pattern:" items:#("Random", "Hexagon", "Grid") pos:[uiX,uiY+=50] width:200 enabled:false
	dropdownlist dropStrokeNormal "Stroke Normal:" items:#("Fixed", "Paint Direction") pos:[uiX,uiY+=40] width:200 enabled:false
	slider slideScatterVariation "Scatter Variation: 0%    " orient:#horizontal ticks:10 range:[0,100,0] type:#integer pos:[uiX,uiY+=50] width:200
	
	--dropdownlist dropRotation "Rotation:" items:#("Random", "Fixed", "Paint Direction") pos:[uiX,uiY+=50] width:200 enabled:false
	slider slideRotationVariation "Rotation Variation: 360" orient:#horizontal ticks:10 range:[0,360,360] type:#integer pos:[uiX,uiY+=50] width:200
	
	dropdownlist dropScaleMethod "Scale Method:" items:#("Combination", "Pen Pressure", "Random") pos:[uiX,uiY+=50] width:200
	slider slideMinimumScale "Scale Minimum: 1x            " orient:#horizontal ticks:10 range:[0,5,1] type:#float pos:[uiX,uiY+=50] width:200 
	slider slideMaximumScale "Scale Maximum: 1.5x          " orient:#horizontal ticks:10 range:[0,5,1] type:#float pos:[uiX,uiY+=50] width:200
	
	spinner spinMinSpacing "Minimum spacing:" range:[0,1000,10] pos:[uiX,uiY+=50] type:#worldunits --width:200 type:#integer
	
	
	on slideNormalBlend changed val do
		slideNormalBlend.text = "Normal Blend: " + (val as string) + "%"
		
	on slideNormalVariation changed val do
		slideNormalVariation.text = "Normal Variation: " + (val as string)		
		
	on slideScatterVariation changed val do
		slideScatterVariation.text = "Scatter Variation: " + (val as string) + "%"			
		
	on slideRotationVariation changed val do
		slideRotationVariation.text = "Rotation Variation: " + (val as string)		

	on slideMinimumScale changed val do
	(
		if slideMinimumScale.value > slideMaximumScale.value do --cant be larger than max!
			val = slideMaximumScale.value
		val = fnRoundNumber val 1 --round off
		slideMinimumScale.text = "Scale Minimum: " + (val as string) + "x"
		slideMinimumScale.value = val		
	)
		
	on slideMaximumScale changed val do
	(
		if slideMaximumScale.value < slideMinimumScale.value  do --cant be smaller than min!
			val = slideMinimumScale.value	
		val = fnRoundNumber val 1 --round off
		slideMaximumScale.text = "Scale Maximum: " + (val as string) + "x"
		slideMaximumScale.value = val
	)	
	
	on btnPaintOptions pressed do
		thePainterInterface.paintOptions()
	
	on btnPaintEnd pressed do
	(
		fnEndPaintSession()
	)
	
	on btnPaintStart pressed do
	(
		if mInstanceBlade!= undefined do -- if this exists
		(
		
			mPrefix = rolloutInstance.txtGroupName.text
			mName = mPrefix + "." + mInstanceBlade.name --this is the name of the new group node
			mGroupNode = fnGetGroupNode mName -- get group node
			print mGroupNode
			if mGroupNode != undefined do
			(
				mStrokeNodes = fnGetGroupChildren mGroupNode
			)
			
			thePainterInterface.ScriptFunctions fnStrokeStart fnStrokePaint fnStrokeEnd fnStrokeCancel fnStrokeSystemEnd 	
			nodeList = selection
			thePainterInterface.initializeNodes 0 nodeList
			thePainterInterface.drawTrace = false
			thePainterInterface.minSize	= 500 -- doesnt work?!	
			thePainterInterface.minSize	= 500 -- doesnt work?!
			thePainterInterface.offMeshHitType = 0
			thePainterInterface.startPaintSession()
		)
	)	
)

-- IMPORT/EXPORT ROLLOUT
rollout rolloutIO "Import/Export"
(
	local uiX = 5
	local uiY = 5

	button btnImportFile "Select Import Grass file..." width:200 height:20 pos:[uiX,uiY] tooltip:""	
	button btnExportFile "Export Grass file..." width:200 height:20 pos:[uiX,uiY=25] tooltip:""		

	button btnImport "Import" width:50 height:20 pos:[uiX,uiY+=20] tooltip:""	
	button btnExport "Export" width:50 height:20 pos:[uiX + 150,uiY] tooltip:""	
	
	on btnExport pressed do
	(
		fnExportINI mExportFileName selection
	)
	
	on btnExportFile pressed do
	(
		mExportFileName = getSaveFileName caption:"Export File:"	filename:"d:/src/hellboydata/patchfiles/" types:"Grass(*.grass)|*.grass"
		if mExportFileName != undefined do
			btnExportFile.text = getFilenameFile mExportFileName			
	)
	
	on btnImport pressed do
	(
		matrixArray = #()
		colorArray = #()
		fnImportINI mImportFileName &matrixArray &colorArray
		mInstanceNodes = fnSpawnBlade mInstanceBladematrixArray colorArray
		-- name it the same as the .grass file
		mName = getFilenameFile mImportFileName
		mName = filterString mName  "."
		mName = mName[1]
		group mInstanceNodes name:mName 
	)
		
	on btnImportFile pressed do
	(
		mImportFileName = getOpenFileName caption:"Open File:" filename:"d:/src/hellboydata/build/pc/" types:"Grass(*.grass)|*.grass"
		if mImportFileName != undefined do
			btnImportFile.text = getFilenameFile mImportFileName	
	)	

)

-- INSTANCE ROLLOUT
rollout rolloutInstance "Instance"
(
	local uiX = 5
	local uiY = 5

	pickbutton pickBladeModel "Pick Instance Mesh..." width:200 height:20 pos:[uiX,uiY] tooltip:""	
	
	dropdownlist dropDisplayType "Display Type:" items:#("Mesh" , "Triangles") pos:[uiX,uiY+=20] Width:200
	
	edittext txtGroupName "Prefix:" Width:200 labelOnTop:false pos:[uiX,uiY+=45]
	
	on dropDisplayType selected val do
	(
		mDisplayMesh = #()
		
		-- get matracies
		mMatrixArray = #()
		for i = 1 to selection.count do
			append mMatrixArray selection[i].transform
			
		if val == 1 do
		(
			mDisplayMesh = mInstanceBlade
			
			mNewSelection = fnSpawnBlade mDisplayMesh mMatrixArray #()
			-- group and select
			mName = "Instance"
			group mNewSelection name:mName 		
			select mNewSelection			
		)
		
		if val == 2 do --tri's we want to position, then collapse + set vertex alpha
		(
			mDisplayMesh = fnCreateTriMesh()
			
			mNewSelection = fnCreateCollapsedMesh mDisplayMesh mMatrixArray #()
			
			select mNewSelection
			delete mDisplayMesh			
		
		)	
	)
	
	on pickBladeModel picked obj do
	(
		if obj != undefiend do
		(
			mInstanceBlade= obj
			rolloutInstance.pickBladeModel.text = obj.name
			mSpacing = fnCalculateSpacing obj
			rolloutPainter.spinMinSpacing.value = mSpacing
		)
	)
	
	on rolloutInstance open do
	(
		clearListener()
		gc()	
	)
	on rolloutInstance close do
	(
		if thePainterInterface.inPaintMode() do
			fnEndPaintSession()
	)
)


mRolloutFloater = newRolloutFloater "Instance Painter" 225 770

addRollout rolloutInstance mRolloutFloater 
addRollout rolloutPainter mRolloutFloater
addRollout rolloutIO mRolloutFloater 



-- createDialog rolloutInstance width:250 height:300

