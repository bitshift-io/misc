--###############################################################################
--## Script Description
--## Renders active frames to a animation frame
--## Bronson Mathews 2009, gibbz1@yahoo.com
--###############################################################################


--###############################################################################
--## Define Globals
--## Without Globals we cany use functions here there and everywhere!
--###############################################################################




--###############################################################################
--## Functions
--##The shiz!
--###############################################################################

--## Render the active frames
fn fnRender mFrameRange =
(
	mBitmapArray = #()
	for r = mFrameRange[1] to mFrameRange[2] do
	(
		mRender = render frame:r vfb:false
		append mBitmapArray mRender
	)
	return mBitmapArray
)

--## Combine the frames and return a bitmap
fn fnCombineBitmap mBitmapArray =
(
	mBitmapWidth = mBitmapArray[1].width
	mBitmapHeight = mBitmapArray[1].height
	mBitmap = bitmap (mBitmapWidth * mBitmapArray.count) mBitmapHeight color:white
	
	for b = 1 to mBitmapArray.count do
	(
		for h = 0 to mBitmapHeight do
		(
			mRow = getPixels mBitmapArray[b] [0,h] mBitmapWidth
			setPixels mBitmap [(mBitmapWidth * b) - mBitmapWidth,h] mRow
		)
	)
	return mBitmap
)

--###############################################################################
--## Rollouts and Shiz
--## Main UI here with buttons and stuff...
--###############################################################################
rollout mAnimSequence "Anim Sequence"
( 
	local uiY = 5
	local uiX = 5
	
	button btnRender "Render" height:20 width:100 pos:[uiX,uiY] tooltip:""
	
	-- Vertex Color
	on btnRender pressed do
	(
		-- get render settings
		mAnimationRange = #(animationRange.start,animationRange.end)
		mBitmapArray = fnRender mAnimationRange
		mOutput = fnCombineBitmap mBitmapArray
		display mOutput
	)
)

--###############################################################################
--## Create Dialogs & Rollouts
--## These put the UI onscreen!
--###############################################################################
options.printAllElements = true
if mAnimSequence != undefined do (DestroyDialog mAnimSequence)
createdialog mAnimSequence 110 30
clearListener()




