--###############################################################################
--## Script Description
--## UI XML Creation
--## Bronson Mathews 2009, gibbz1@yahoo.com
--###############################################################################


--###############################################################################
--## Functions
--##The shiz!
--###############################################################################

--## Create a base frame to work with
fn fnCreateBaseFrame mDimensions =
(
	mBaseFrame = rectangle width:mDimensions.x length:mDimensions.y name:"UI_BaseFrame"
	max tool maximize
	viewport.setType #view_top
	viewport.setGridVisibility #all false
	max create mode
)

--## recursive loop.. mii brain!
fn fnCollectChildren mNode &mXMLString mTabIndex =
(
	-- tabbing for neatness
	local mTabString =""
	for t = 1 to mTabIndex do
		mTabString += "\t"
	
	-- get node data in space relation to the root node
	local mPos =  (mNode.pos.x as string) + "," + (mNode.pos.y as string)
	local mWidth = mNode.width as string
	local mHeight = mNode.length as string
	
	-- user defined values, fix the formatting (each new line is a new value x="value")
	local mUserDefined = getUserPropBuffer mNode
	mUserDefined = filterString mUserDefined "\n"
	local mUserDefinedString = ""
	for u = 1 to mUserDefined.count do
	(
		local mFiler = filterstring mUserDefined[u] "="
		mUserDefinedString += " " + mFiler[1] + "=\""  + mFiler[2] + "\""
	)

	mXMLString += mTabString + "<UIComponent position=\"" + mPos + "\" width=\"" + mWidth + "\" height=\"" + mHeight + "\"" + mUserDefinedString + ">\n"
	
	-- find children
	for c in mNode.children do
		fnCollectChildren c &mXMLString (mTabIndex + 1)
	
	mXMLString += mTabString +"</UIComponent>\n"
)
	
--## collect ui data and export to xml
fn fnExportXML mFileName =
(
	local mRootNode -- root node
	local mshapes = #() -- all other shapes
	
	-- find our root node (UI_BaseFrame)
	for mObj in shapes do
		if mObj.name == "UI_BaseFrame" then
			mRootNode = mObj
		else
			append mShapes mObj
		
	-- get object with no parent and link them to the base frame
	for s in mShapes do
		if s.parent == undefined do
			s.parent = mRootNode
	
	-- now we loop through all children and shove it into some xml format
	local mXMLString = "" -- sting containning our complete xml data with formating
	fnCollectChildren mRootNode &mXMLString 0
	
	-- create our file and dump data to it
	local mXMLFileStream = createFile mFileName
	format mXMLString to:mXMLFileStream
	close mXMLFileStream

)

--###############################################################################
--## Rollouts and Shiz
--## Main UI here with buttons and stuff...
--###############################################################################
if mUIEditor != undefined do (DestroyDialog mUIEditor)
rollout mUIEditor "UI Editor"
( 
	local uiY = 5
	local uiX = 5
	
	spinner spnBaseWidth "Width:" height:20 width:80 pos:[uiX,uiY]  scale:1 type:#integer range:[1,9999,100] align:#left tooltip:""
	spinner spnBaseHeight "Height:" height:20 width:80 pos:[uiX+100,uiY] scale:1 type:#integer range:[1,9999,100] align:#left tooltip:""
	button btnCreateBaseFrame "Create Base Frame" height:20 width:200 pos:[uiX,uiY+=20] tooltip:""
	button btnExportXML "Export to XML" height:20 width:200 pos:[uiX,uiY+=20] tooltip:""
	
	on btnCreateBaseFrame pressed do
	(
		local mDimensions = point2 mUIEditor.spnBaseWidth.value mUIEditor.spnBaseHeight.value
		fnCreateBaseFrame mDimensions
	)
	
	on btnExportXML pressed do
	(
		local mFileName = getSaveFileName caption:"Save As:" types:"XML(*.xml)|*.xml"
		if mFileName != undefined then
			fnExportXML mFileName
		else
			format "Error! Select a file to save!"
	)
)

--###############################################################################
--## Create Dialogs & Rollouts
--## These put the UI onscreen!
--###############################################################################
options.printAllElements = true
createdialog mUIEditor 210 80
clearListener()




