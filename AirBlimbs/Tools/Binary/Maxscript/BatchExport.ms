--###############################################################################
--## Script Description
--## This script will do a batch export!
--## Bronson Mathews 2007, gibbz1@yahoo.com
--###############################################################################


--###############################################################################
--## Define Globals
--## Without Globals we cany use functions here there and everywhere!
--###############################################################################
global mIniFileName = "g://Config.ini"



--###############################################################################
--## Functions
--##The shiz!
--###############################################################################

--## Return selection sets for export in the scene as an array (E_ and Export_)
fn fnGetExportSelectionSets =
(
	mNumSets = getNumNamedSelSets()
	mSetsToExport = #()
	for i = 1 to mNumSets do
	(
		mNumSetNodes = getNamedSelSetItemCount i
		
		-- if no nodes, skip to next set
		if mNumSetNodes <= 0 do
			continue
		
		mSetName = getNamedSelSetName i
		mFilter = filterstring mSetName "_, "
		
		if mFilter[1] == "E" or mFilter[1] == "Export" do
		(
			mFind = findstring mSetName  "_"
			mName = substring mSetName (mFind + 1) (mSetName.count)
			mTempArray = #()
			mTempArray[1] = i
			mTempArray[2] = mName	
			append mSetsToExport mTempArray		
		)
	)
	
	return mSetsToExport
)

--## Gets an array of all the objects in the selectionset
fn fnGetSetNodes valIndex =
(	
	mNumSetNodes = getNamedSelSetItemCount valIndex
	mSelection = #()
	
	for i = 1 to mNumSetNodes do
		append mSelection (getNamedSelSetItem valIndex i)
		
	return mSelection
)

--## Gets the project lists names as an array
fn fnGetINIProjectNameList =
(
	mProjectNames = #()
	
	mInt = 01
	--getINISetting "G:\Config.ini" -- return an array of headings?
	
		mProject = "Project" + (mInt as string)
		mIniString = getINISetting mIniFileName mProject "Name"
	print mIiniString
		if mIniString != undefined do
			append mProjectNames mIniString
		mInt += 1

		
		
	print mProjectNames
	return mProjectNames
)

--###############################################################################
--## Rollouts and Shiz
--## Main UI here with buttons and stuff...
--###############################################################################


rollout mRolloutProject "Project"
( 
	local uiY = 2
	local uiX = 7
	
	dropdownlist dropProject "" items:#("") width:200 height:20 pos:[uiX,uiY]
	button btnBatchExport "Export" width:100 height:20 pos:[uiX,uiY+=20]
	
	on btnBatchExport pressed do
		fnBatchExport()
		
	-- update the dropProject droplist
	on mRolloutProject open do
	(
		mProjectNames = #()
		mProjectNames = fnGetINIProjectNameList()
		--append mProjctNames ("All" as string)
		dropProject.items = mProjectNames
	)
)

--###############################################################################
--## Create Dialogs & Rollouts
--## These put the UI onscreen!
--###############################################################################

options.printAllElements = true
if mBatchExportFloater != undefined do (closeRolloutFloater mBatchExportFloater)
mBatchExportFloater = newRolloutFloater "Batch Export" 240 300
addRollout mRolloutProject mBatchExportFloater 
clearListener()




