-- Script used to make waypoint system
-- Bronson Mathews 2007


-- GLOBALS
global mWaypointNodes = #()
global mSelectedWaypoints = #()
global mWaypointName = ""
global mSuffix = ".waypoint"

global mUpdate = false
global mPreviousSelection = #()

global mLinkMesh = undefined


-- FUNCTIONS
fn fnMatrixToEngine valMatrix = --make a matrix to a string
(
	mPosX = (valMatrix[4][1] * 0.0253999) as string --magic number :)
	mPosY = (valMatrix[4][2] * 0.0253999) as string
	mPosZ = (valMatrix[4][3] * 0.0253999) as string
	
	mNewMatrix = (valMatrix[1][1] as string) + "," + (valMatrix[1][3] as string) + "," + (valMatrix[1][2] as string) + ",0, " + (valMatrix[2][1] as string) + "," + (valMatrix[2][3] as string) + "," + (valMatrix[2][2] as string) + ",0, " + (valMatrix[3][1] as string) + "," + (valMatrix[3][3] as string)  + "," + (valMatrix[3][2] as string) + ",0, " + mPosX + "," + mPosZ + "," + mPosY + ",1" 
	return mNewMatrix
)

fn fnExport valSelection = --export!
(
	mOutputFile = mRolloutIO.txtWaypointName.text + mWaypointName + mSuffix + ".mdd"
	mOutputFile = createFile mOutputFile
	
	-- initial crap
	format "[: WayPointGraph]\n" to:mOutputFile 
	format "(\n" to:mOutputFile 
	format "	mEntity =\n" to:mOutputFile 
	format "	(\n" to:mOutputFile 


	for i = 1 to valSelection.count do --spit out the waypoints...
	(
		mNode = valSelection[i]

		format "		[: WayPoint]\n" to:mOutputFile 
		format "		(\n" to:mOutputFile 		
		format "			mName = %\n" mNode.name to:mOutputFile 	
		format "			mTransform = %\n" (fnMatrixToEngine mNode.transform) to:mOutputFile 				
		format "		)\n\n" to:mOutputFile 				

	)
	
	format "\n" to:mOutputFile 
	mLinks = fnGetLinkedNodes valSelection --get linked nodes
	
	for i = 1 to mLinks.count do --spit out the links...
	(
		mNode = mLinks[i][1]
		mChildren = mLinks[i][2]	
		print mChildren

		for k = 1 to mChildren.count do
		(
			mChild = mChildren[k]
	
			format "		[: WayPointPath]\n" to:mOutputFile 
			format "		(\n" to:mOutputFile 		
			format "			mWayPointName1 = %\n" mNode.name to:mOutputFile 	
			format "			mWayPointName2 = %\n" mChild.name to:mOutputFile 				
			format "		)\n\n" to:mOutputFile 		
		)
	)

	
	-- end crap
	format "	)\n" to:mOutputFile 
	format ")\n" to:mOutputFile 
	
	close mOutputFile 
)

fn fnGetLinkedNodes valSelection = --gets linked nodes
(
	mLinks = #()
	
	for i = 1 to valSelection.count do --get the names of the nodes
	(
		mEnabled = #()
		for k = 1 to valSelection.count do --loop over and see if user properties exists...
		(
			mUserProp = getUserProp valSelection[i] ("WaypointEnabled_" + valSelection[k].name)
			if mUserProp == true do -- if its true, we have a link!
				append mEnabled valSelection[k]
		)
		
		mTemp = #()
		mTemp[1] = valSelection[i]
		mTemp[2] = mEnabled
		mLinks[i] = mTemp 
	)
	return mLinks	
)

fn fnDrawTri valTargetPos valSourcePos = --draws a tri from point to target
(
	mNormal = normalize(valTargetPos - valSourcePos)
	mCross = normalize(cross mNormal [0,0,1]) --get the 3rd vector
	
	mDist = 5
	
	mPointA = valSourcePos + (mDist * mCross)
	mPointB = valSourcePos - (mDist * mCross)
	
	mTriMesh = mesh vertices:#(valTargetPos, mPointA, mPointB) faces:#([1,3,2]) -- 10cm tri to make it easier to see...
	mTriMesh.wireColor = (color 255 0 0)
	mTriMesh.name = "TriMesh"
	return mTriMesh
)

fn fnDrawLinks valArray = -- darws links!
(
	if mLinkMesh != undefined do --delete the old mesh
		delete mLinkMesh
	
	mTriMesh = #()

	for i = 1 to valArray.count do
	(
		mLink = valArray[i]
		mParent = mLink[1]
		mChildren = mLink[2]
		
		for k = 1 to mChildren.count do
		(
			append mTriMesh (fnDrawTri mParent.pos mChildren[k].pos) --collect all our meshes
		)
	)
	
	mLinkMesh = mTriMesh[1]
	for i = 2 to mTriMesh.count do
		attach mLinkMesh mTriMesh[i]
)

fn fnResetUI = --clear the ui to original state
(
	mRolloutPropertiesA.labelA.text = ""		
	mRolloutPropertiesB.labelB.text = ""
		
	mRolloutPropertiesA.checkLinkEnabledA.checked = false 
	mRolloutPropertiesA.radioWeightA.state = 2 
	mRolloutPropertiesA.radioAIStateA.state = 2
	
	mRolloutPropertiesB.checkLinkEnabledB.checked = false 
	mRolloutPropertiesB.radioWeightB.state = 2 
	mRolloutPropertiesB.radioAIStateB.state = 2	
)

fn fnUpdateUI valSelection = --updated the ui when we have two nodes selected
(
	if valSelection.count == 2 do
	(
		mNodeA = valSelection[1]
		mNodeB = valSelection[2]
		
		mRolloutPropertiesA.labelA.text = mNodeA.name + " -> " + mNodeB.name		
		mRolloutPropertiesB.labelB.text = mNodeB.name + " -> " + mNodeA.name

		--get data for node A, this will be sorted alphabetically...
		mEnabledA = getUserProp mNodeA ("WaypointEnabled_" + mNodeB.name)
		mRadiusA = getUserProp mNodeA ("WaypointRadius_" + mNodeB.name)
		mWeightA = getUserProp mNodeA ("WaypointWeight_" + mNodeB.name)	
		mAIStateA = getUserProp mNodeA ("WaypointAIState_" + mNodeB.name)	
		
		mEnabledB = getUserProp mNodeB ("WaypointEnabled_" + mNodeA.name)
		mRadiusB = getUserProp mNodeB ("WaypointRadius_" + mNodeA.name)
		mWeightB = getUserProp mNodeB ("WaypointWeight_" + mNodeA.name)	
		mAIStateB = getUserProp mNodeB ("WaypointAIState_" + mNodeA.name)
	
		if mEnabledA != undefined do
		(
			mRolloutPropertiesA.checkLinkEnabledA.checked = mEnabledA 
			mRolloutPropertiesA.radioWeightA.state = mWeightA 
			mRolloutPropertiesA.radioAIStateA.state = mAIStateA
		)
		
		if mEnabledB != undefined do
		(
			mRolloutPropertiesB.checkLinkEnabledB.checked = mEnabledB 
			mRolloutPropertiesB.radioWeightB.state = mWeightB 
			mRolloutPropertiesB.radioAIStateB.state = mAIStateB
		)	
	)
)

fn fnWriteUserDefined valSelection = --updated the ui when we have two nodes selected
(
	if valSelection.count == 2 do
	(
		mNodeA = valSelection[1]
		mNodeB = valSelection[2]
	
		--read in the ui	
		mEnabledA = mRolloutPropertiesA.checkLinkEnabledA.checked 
		mWeightA = mRolloutPropertiesA.radioWeightA.state
		mAIStateA = mRolloutPropertiesA.radioAIStateA.state
	
		mEnabledB = mRolloutPropertiesB.checkLinkEnabledB.checked 
		mWeightB = mRolloutPropertiesB.radioWeightB.state
		mAIStateB = mRolloutPropertiesB.radioAIStateB.state
	
		--set data up!
		setUserProp mNodeA ("WaypointEnabled_" + mNodeB.name) mEnabledA
		setUserProp mNodeA ("WaypointRadius_" + mNodeB.name) mRadiusA 
		setUserProp mNodeA ("WaypointWeight_" + mNodeB.name) mWeightA 
		setUserProp mNodeA ("WaypointAIState_" + mNodeB.name) mAIStateA 
		
		setUserProp mNodeB ("WaypointEnabled_" + mNodeA.name) mEnabledB 
		setUserProp mNodeB ("WaypointRadius_" + mNodeA.name) mRadiusB 
		setUserProp mNodeB ("WaypointWeight_" + mNodeA.name) mWeightB 	
		setUserProp mNodeB ("WaypointAIState_" + mNodeA.name) mAIStateB 
	)
)

fn fnGetGroups = --collects a array of groups
(
	mGroups = #()
	for i in helpers do
	(
		if (isGroupHead i) do
			append mGroups i
	)
	return mGroups
)

fn fnGetGroupChildren valGroupNode = --collect the nodes in a group
(
	mChildNodes = #()
	mChildren = valGroupNode.children
	for i in mChildren do
	(
		append mChildNodes i
	)
	return mChildNodes
)

fn fnGetGroupNode valGroupName = -- gets a group Node from group name
(
	mGroups = #() -- possibly multiple groups?!
	mGroupNode = undefined
	mGroupNodes = fnGetGroups()

	for i in mGroupNodes do
		if i.name == valGroupName do
			append mGroups i

	if mGroups.count > 1 then --if multiple groups with same name, lets merger them!
	(
		mChildren = #()
		for i in mGroups do
		(
			mChildren = mChildren + (fnGetGroupChildren i)
			explodeGroup i -- delete the group
		) 
		mGroupNode = group mChildren name:valGroupName select:false --make our new group!
	)
	else
		mGroupNode = mGroups[1]

	return mGroupNode
)

fn fnGetValidNodes valArray = -- this cannot be mapped?!
(
	mValidNodes = #()
	for i = 1 to valArray.count do
	(
		if (isValidNode valArray[i]) do
			append mValidNodes valArray[i]
	)
	return mValidNodes 
)

-- TOOL
tool toolCreate
(
	on mousePoint valClick do
	(
		if valClick > 1 do --Very nasty hack!
		(
			mWaypointNodes = fnGetValidNodes mWaypointNodes
			mDummy = dummy pos:worldPoint
			mDummy.boxsize = [10, 10, 10]	
			mDummy.name = uniqueName "Waypoint_"
			append mWaypointNodes mDummy

			mName = mWaypointName + mSuffix			
			mGroupNode = fnGetGroupNode mName
			mChildren = #()
			if mGroupNode != undefind do
			(
				mChildren = fnGetGroupChildren mGroupNode
				mWaypointNodes += mChildren
				explodegroup mGroupNode
			)
			mGroup = group mWaypointNodes name:mName select:false
			
		)
	)
)

-- ROLLOUTS
rollout mRolloutWaypointSystem "Waypoint System"
(
	local uiX = 5
	local uiY = 5
	edittext txtWaypointName "Name" text:"" labelOnTop:false pos:[uiX,uiY] width:200
	button btnCreateNode "New Node" width:100 pos:[uiX,uiY+=20]
	button btnEdit "Edit" width:50 pos:[uiX+100,uiY]
	button btnClose "Apply" width:50 pos:[uiX+150,uiY]
	radiobuttons radioRadius "Radius:" labels:#("2m", "10m", "20m", "50m") default:3 pos:[uiX,uiY+=30] 

	on mRolloutWaypointSystem close do
	(
		--delete tri mesh
		if mLinkMesh != undefined do
			delete mLinkMesh 
	)
	
	on btnClose pressed do
	(
		mName = mWaypointName + mSuffix	
		mGroupNode = fnGetGroupNode mName
		setGroupOpen mGroupNode false
		select mGroupNode
	)

	on btnEdit pressed do
	(
		mName = mWaypointName + mSuffix	
		mGroupNode = fnGetGroupNode mName
		print mGroupNode 
		setGroupOpen mGroupNode true
		hide mGroupNode --hide the head node!
	)

	on txtWaypointName changed val do
		mWaypointName = val
	
	on btnCreateNode pressed do
		startTool toolCreate
		
	-- Selection changed Timmer
	timer mClock "" interval:200
	on mClock tick do
	(
		mCurrentSelection = selection as array
		
		-- check for selection change
		if mCurrentSelection.count != mPreviousSelection.count then
		(
			mUpdate = true
			mPreviousSelection = #()
			mPreviousSelection = mCurrentSelection
		)
		else
		(
			for i = 1 to mCurrentSelection.count do
			(
				if mCurrentSelection[i] != mPreviousSelection[i] then
				(
					mUpdate = true
					mPreviousSelection = #()
					mPreviousSelection = mCurrentSelection	
					exit			
				)
				else
					mUpdate = false
			)
		)



		if mUpdate do --update ui
		(
			if mCurrentSelection.count == 2 then --out 2 nodes!
			(
				print "2x selection"
				mSelectedWaypoints = selection as array
				fnResetUI() --clear the ui first to make shure :)				
				fnUpdateUI mSelectedWaypoints
			)
			else -- not 2 nodes
			(
				print "reset"
				fnResetUI()
			)
			
			-- get and draw links! wee!
			mLinks = fnGetLinkedNodes mCurrentSelection
			fnDrawLinks mLinks
			
			mUpdate = false
		)
	)		
)

rollout mRolloutPropertiesA "Properties A -> B"
(
	local uiX = 5
	local uiY = 5
	
	edittext labelA labelOnTop:false pos:[uiX-5,uiY] width:200 readonly:true bold:true
	checkbox checkLinkEnabledA "Link Enabled" checked:false pos:[uiX,uiY+=20]
	radiobuttons radioWeightA "Waypoint Weight:" labels:#("Low", "Standard", "High") default:2 pos:[uiX,uiY+=20]
	radiobuttons radioAIStateA "AI State:" labels:#("Move", "Vehicle", "Jump") default:2 pos:[uiX,uiY+=40]
	
	on checkLinkEnabledA changed val do
	(
		fnWriteUserDefined mSelectedWaypoints
		mLinks = fnGetLinkedNodes mSelectedWaypoints
		fnDrawLinks mLinks 
	)
		
	on radioWeightA changed val do
		fnWriteUserDefined mSelectedWaypoints
		
	on radioAIStateA changed val do
		fnWriteUserDefined mSelectedWaypoints	
)

rollout mRolloutPropertiesB "Properties B -> A"
(
	local uiX = 5
	local uiY = 5
	
	edittext labelB labelOnTop:false pos:[uiX-5,uiY] width:200 readonly:true bold:true
	checkbox checkLinkEnabledB "Link Enabled" checked:false pos:[uiX,uiY+=20]
	radiobuttons radioWeightB "Waypoint Weight:" labels:#("Low", "Standard", "High") default:2 pos:[uiX,uiY+=20]
	radiobuttons radioAIStateB "AI State:" labels:#("Move", "Vehicle", "Jump") default:2 pos:[uiX,uiY+=40]
	
	on checkLinkEnabledB changed val do
	(
		fnWriteUserDefined mSelectedWaypoints
		mLinks = fnGetLinkedNodes mSelectedWaypoints
		fnDrawLinks mLinks 
	)
		
	on radioWeightB changed val do
		fnWriteUserDefined mSelectedWaypoints
		
	on radioAIStateB changed val do
		fnWriteUserDefined mSelectedWaypoints	
)

rollout mRolloutIO "Import/Export"
(
	local uiX = 5
	local uiY = 5
	edittext txtWaypointName "Folder:" text:"D:\\" labelOnTop:false pos:[uiX,uiY] width:180
	button btnFilePath "..." width:20 pos:[uiX+180,uiY] height:18

	button btnImport "Import" width:100 pos:[uiX,uiY+=20]
	button btnExport "Export" width:100 pos:[uiX+100,uiY]
	
	on btnExport pressed do
	(
		fnExport selection
	)
	
	on btnFilePath pressed do
	(
		mPath = getSavePath caption:"Select Folder" initialDir:"D:/"	
		txtWaypointName.text = mPath 
	)

)

-- FLOATER
if mNewFloater != undefined do (closeRolloutFloater mNewFloater)
clearListener()
mNewFloater = newRolloutFloater "Waypoint Editor" 220 550

addRollout mRolloutWaypointSystem mNewFloater 
addRollout mRolloutPropertiesA mNewFloater 
addRollout mRolloutPropertiesB mNewFloater 
addRollout mRolloutIO mNewFloater 
