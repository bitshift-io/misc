#include "MaxMesh.h"
#include "MaxGeometry.h"
#include "Exporter.h"
#include "Render/RenderFactory.h"
#include "Render/Mesh.h"
#include "Render/Skin.h"
#include "Render/Geometry.h"
#include "Render/Material.h"
#include "Render/Export/ExpRenderBuffer.h"
#include "MaxMaterial.h"
#include <set>


#define BIPBONE_CONTROL_CLASS_ID    Class_ID(0x9125,0)

// this is the class for all biped controllers except the root and the footsteps
#define BIPSLAVE_CONTROL_CLASS_ID Class_ID(0x9154,0)

// this is the class for the center of mass, biped root controller ("Bip01")
#define BIPBODY_CONTROL_CLASS_ID  Class_ID(0x9156,0)

// this is the class for the biped footstep controller ("Bip01 Footsteps")
#define FOOTPRINT_CLASS_ID Class_ID(0x3011,0)  

struct FaceMat
{
	Tab<FaceEx*>	faces;
	IGameMaterial*	iMat;
	Material*		mat;
};

MaxMesh::MaxMesh(IGameNode* pGameNode) : MaxObject(pGameNode)
{
}

bool MaxMesh::IsSkinned()
{
	return mGameObject->IsObjectSkinned();
}

bool MaxMesh::Convert(Mesh* mesh)
{
	if (!mGameNode || !mGameObject)
	{
		gExporter->Log("There is no object or node!");
		return false;
	}

	if (!mesh)
	{
		gExporter->Log("Mesh is invalid");
		return false;
	}

	IGameMesh* pGameMesh = 0;
	try
	{
		pGameMesh = static_cast<IGameMesh*>(mGameObject);
		pGameMesh->SetCreateOptimizedNormalList();
		if (!pGameMesh->InitializeData())
		{
			gExporter->Log("Failed to convert to IGameMesh");
			return false;
		}
	}
	catch (...)
	{
		gExporter->Log("Failed to convert to IGameMesh");
		return false;
	}

	bool skinned = mGameObject->IsObjectSkinned();
	IGameSkin* pSkin = 0;
	if (skinned)
	{
		pSkin = mGameObject->GetIGameSkin();
		ProcessSkin(mesh);
	}

	Matrix4 matScale(MI_Identity);
	matScale.SetScale(MAX_TO_METERS);

	unsigned int vertexFormat = GetVertexFormat(pGameMesh);

	bool localSpace = true;
	INode* pMaxNode = mGameNode->GetMaxNode();
	CStr stringTransform;
	bool found = pMaxNode->GetUserPropString("#transform", stringTransform);
	if (found)
	{
		if (strstr(stringTransform.data(), "world"))
			localSpace = false;
	}

	// get the list of materials on this geometry
	list<FaceMat> matList;
	list<FaceMat>::iterator matIt;
	Tab<int> matIds = pGameMesh->GetActiveMatIDs();
	for (int m = 0; m < matIds.Count(); ++m)
	{
		Tab<FaceEx*> faces = pGameMesh->GetFacesFromMatID(matIds[m]);
		IGameMaterial* mat = pGameMesh->GetMaterialFromFace(faces[0]);

		bool found = false;
		for (matIt = matList.begin(); matIt != matList.end(); ++matIt)
		{
			if (matIt->iMat == mat)
			{
				matIt->faces.Append(faces.Count(), faces.Addr(0));
				found = true;
				break;
			}
		}

		if (!found)
		{
			FaceMat faceMat;
			faceMat.iMat = mat;
			faceMat.mat = CreateMaterial(mat);
			faceMat.faces = faces;

			matList.push_back(faceMat);
		}
	}

	//for (int m = 0; m < matIds.Count(); ++m)
	//{
	for (matIt = matList.begin(); matIt != matList.end(); ++matIt)
	{
		// get the list of faces for each material...
		//Tab<FaceEx*> faces = pGameMesh->GetFacesFromMatID(matIds[m]);
		//Geometry* geometry = CreateGeometry(pGameMesh, faces, vertexFormat, localSpace, pSkin);
		//Material* material = CreateMaterial(pGameMesh->GetMaterialFromFace(faces[0]));
		Geometry* geometry = CreateGeometry(pGameMesh, matIt->faces, vertexFormat, localSpace, pSkin);
		mesh->AddMeshMat(MeshMat(matIt->mat, geometry));
	}

	mesh->SetFlags(GetFlags());

	Box3 bb;
	Box bounds;
	pGameMesh->GetBoundingBox(bb); 

	GMatrix maxWorldToLocal = mGameNode->GetWorldTM();
	maxWorldToLocal = maxWorldToLocal.Inverse();
	Matrix4 worldToLocal = Helper::ConvertMatrix4(maxWorldToLocal, true);

	bounds.SetMin(worldToLocal * Helper::ConvertVector4(bb.Min(), true));
	bounds.SetMax(worldToLocal * Helper::ConvertVector4(bb.Max(), true));

	mesh->SetLocalBoundBox(bounds);
	return true;
}

int MaxMesh::GetFlags()
{
	int flags = 0;
	INode* pMaxNode = mGameNode->GetMaxNode();
	TSTR stringFlags;
	bool found = pMaxNode->GetUserPropString("#flags", stringFlags);
	if (found)
	{
		if (strstr(stringFlags.data(), "nonrenderable"))
			flags |= MF_NonRenderable;
	}

	return flags;
}

unsigned int MaxMesh::GetVertexFormat(IGameMesh* pGameMesh)
{
	Tab<int> mapChan = pGameMesh->GetActiveMapChannelNum();

	int numTexCoordChan = 0;
	for (int t = 0; t < mapChan.Count(); ++t)
	{
		int channel = mapChan[t];
		if (channel < 1)
			continue;

		++numTexCoordChan;
	}
	numTexCoordChan = min(MAX_TEXCOORD, numTexCoordChan);

	int numNormal		= pGameMesh->GetNumberOfNormals();
	int numTangent		= pGameMesh->GetNumberOfTangents();
	int numBinormal		= pGameMesh->GetNumberOfBinormals();
	int numColour		= pGameMesh->GetNumberOfColorVerts();
	int numAlpha		= pGameMesh->GetNumberOfAlphaVerts();

	int vertexFormat = VF_Position;

	INode* pMaxNode = mGameNode->GetMaxNode();

	TSTR stringFormat;
	bool customFormat = pMaxNode->GetUserPropString("#vertexformat", stringFormat);
	if (customFormat)
	{
		// parse the format
		if (strstr(stringFormat.data(), "position"))
			vertexFormat |= VF_Position;
		if (strstr(stringFormat.data(), "normal"))
			vertexFormat |= VF_Normal;
		if (strstr(stringFormat.data(), "colour"))
			vertexFormat |= VF_Colour;
		if (strstr(stringFormat.data(), "binormal"))
			vertexFormat |= VF_Binormal;
		if (strstr(stringFormat.data(), "tangent"))
			vertexFormat |= VF_Tangent;
		if (strstr(stringFormat.data(), "bonedata"))
			vertexFormat |= VF_BoneData;

		if (strstr(stringFormat.data(), "texcoord") && numTexCoordChan)
		{
			int i = 0;
			for (; i < numTexCoordChan; ++i)
			{
				vertexFormat |= (VF_TexCoord0 << i);
			}
		}
/*
		for (int i = 0; i < MAX_TEXCOORD; ++i)
		{
			char texBuffer[256];
			sprintf(texBuffer, "texcoord%i", i);

			if (strstr(stringFormat.data(), texBuffer))
				vertexFormat |= (VF_TexCoord0 << i);
		}*/
	}
	else
	{
		if (numNormal)
			vertexFormat |= VF_Normal;

		if (numTangent)
			vertexFormat |= VF_Tangent;

		if (numBinormal)
			vertexFormat |= VF_Binormal;

		//if (numColour)
			vertexFormat |= VF_Colour;

		//if (numAlpha)
		//	vertexFormat |= VF_Colour;

		if (mGameObject->IsObjectSkinned())
			vertexFormat |= VF_BoneData;

		if (numTexCoordChan)
		{
			int i = 0;
			for (; i < numTexCoordChan; ++i)
			{
				vertexFormat |= (VF_TexCoord0 << i);
			}
		}
	}

	// generate a string
	char buffer[256];
	strcpy(buffer, "position");

	if (vertexFormat & VF_Normal)
		strcat(buffer, " | normal");

	if (vertexFormat & VF_Tangent)
		strcat(buffer, " | tangent");

	if (vertexFormat & VF_Binormal)
		strcat(buffer, " | binormal");

	if (vertexFormat & VF_Colour)
		strcat(buffer,  " | colour");

	if (vertexFormat & VF_BoneData)
		strcat(buffer,  " | bonedata");

	bool texCoord = false;
	for (int i = 0; i < numTexCoordChan; ++i)
	{
		if (vertexFormat & (VF_TexCoord0 << i))
			texCoord = true;
	}
	if (texCoord)
		strcat(buffer,  " | texcoord");		

	gExporter->Log(buffer);
	return vertexFormat;
}

Material* MaxMesh::CreateMaterial(IGameMaterial* pGameMaterial)
{
	MaxMaterial obj(mGameNode);
	Material* material = gExporter->mRenderFactory->Create<Material>();
	MaterialBase* materialBase = gExporter->mRenderFactory->Create<MaterialBase>();
	material->SetMaterialBase(materialBase);
	obj.Convert(material, pGameMaterial);	
	return material;
}

Geometry* MaxMesh::CreateGeometry(IGameMesh* pGameMesh, const Tab<FaceEx*>& faces, int vertexFormat, bool localSpace, IGameSkin* pSkin)
{
	MaxGeometry obj(mGameNode, this);
	Geometry* geometry = (Geometry*)gExporter->mRenderFactory->Create(Geometry::Type);
	obj.Convert(geometry, pGameMesh, faces, vertexFormat, localSpace, pSkin);	
	return geometry;
}

void MaxMesh::ProcessSkin(Mesh* mesh)
{
	IGameSkin* pSkin = mGameObject->GetIGameSkin();
	std::set<IGameNode*> boneSet;

	// go through each vertex, compile a list of bones
	for (int v = 0; v < pSkin->GetNumOfSkinnedVerts(); ++v)
	{
		for (int b = 0; b < pSkin->GetNumberOfBones(v); ++b)
		{
			IGameNode* boneNode = pSkin->GetIGameBone(v, b);
			const char* boneName = boneNode->GetName();
			if (boneNode && !boneNode->IsNodeHidden())
				boneSet.insert(boneNode);
		}
	}

	// now we have a unique set of bones, we need to find the root bone
	std::set<IGameNode*>::iterator boneIt;
	std::set<IGameNode*>::iterator secBoneIt;

	if (boneSet.empty())
		return;

	IGameNode* root = (*boneSet.begin());

	IGameNode* parent = root->GetNodeParent();;
	
	bool rootFound = false;
	while (parent && !rootFound)
	{
		const char* parentName = root->GetName();

		if (!parent || !parent->GetIGameObject() || parent->IsNodeHidden())
		{
			rootFound = true;
		}
		else
		{
			root = parent;
			parent = root->GetNodeParent();
		}
	}
	
	const char* boneName = root->GetName();
	AddBone(root, 0, 0);

	Bone* bones = new Bone[mGameBones.size()];
	for (int i = 0; i < mGameBones.size(); ++i)
	{
		bones[i].mSkinToBone = mGameBones[i]->skinToBone;

		// bit of a nasty hack
		// create a node just to hold the name
		SceneNode* boneSceneNode = gExporter->mScene->CreateNode();
		boneSceneNode->SetName(mGameBones[i]->boneName);
		bones[i].mSceneNode = boneSceneNode;
	}

	SkinMesh* skinMesh = static_cast<SkinMesh*>(mesh);
	skinMesh->SetBone(bones, mGameBones.size());
}

int MaxMesh::GetBoneIdx(IGameNode* bone)
{
	for (int i = 0; i < mGameBones.size(); ++i)
	{
		if (bone == mGameBones[i]->gameNode)
			return i;
	}

	return -1;
}

bool MaxMesh::IsBone(IGameNode* bone)
{
	if (!bone)
		return false;

	INode* node = bone->GetMaxNode();
	Object* obj = bone->GetIGameObject()->GetMaxObject();
	Control* ctrl = node->GetTMController(); 

    // check if this is part of the biped hierarchy
    if(ctrl)
    {
        Class_ID nodeControllerClassId = ctrl->ClassID();
        Class_ID objectClassId = obj->ClassID();

        // make sure its not a nub
        if (objectClassId == Class_ID(DUMMY_CLASS_ID, 0x0))
			return false;

        // this is a bone, we should not include it in the hierarchy
        // instead we allow the dexcharacter to process it         
        if ((objectClassId == BIPBONE_CONTROL_CLASS_ID) ||
            (objectClassId == Class_ID(BONE_CLASS_ID, 0)) ||
            (ctrl && nodeControllerClassId == BIPSLAVE_CONTROL_CLASS_ID))
                return true;
    }

	// check if bone properties are turned on
	if (node->GetBoneNodeOnOff())
		return true;

	return false;
}

void MaxMesh::AddBone(IGameNode* bone, IGameNode* parent, int parentIdx)
{
	if (!bone || bone->IsNodeHidden() || !IsBone(bone))
		return;

	//Vector4 vScale(MAX_TO_METERS, MAX_TO_METERS, MAX_TO_METERS);//FMNOTE: this should be 0.01f;

	MaxBone* pBone = new MaxBone();
	MaxBone& b = *pBone;
	b.parentIdx = parentIdx;
	b.boneName = bone->GetName();
	b.gameNode = bone;

	IGameSkin* pSkin = mGameObject->GetIGameSkin();
	GMatrix mat, parentMat;
	pSkin->GetInitBoneTM(bone, mat);

//	if (parent)
//	{
//		pSkin->GetInitBoneTM(parent, parentMat);
//	}

//	GMatrix parentMatInv = parentMat; //GMatrix(Inverse(parentMat.ExtractMatrix3()));
//	parentMatInv.Inverse();

	// inverse out the parent 
//	b.defaultPose = Helper::ConvertMatrix4(mat * parentMatInv);

//	GMatrix initialSkinTM;
//	pSkin->GetInitSkinTM(initialSkinTM);

	
//	GMatrix boneSpace = parentMatInv * mat;

	
/*
	GMatrix parentMatrix;
	//parentMatrix.SetIdentity();
	IGameNode* parentBone = bone->GetNodeParent();
	while (parentBone)
	{
		GMatrix parentMat;
		pSkin->GetInitBoneTM(parentBone, parentMat);
		parentMatrix = GMatrix(Inverse(parentMat.ExtractMatrix3())) * parentMatrix;

		parentBone = parentBone->GetNodeParent();
	}
	b.defaultPose = mat * parentMatrix;
*/

//	Vector4 scaledTranslation = b.defaultPose.GetTranslation();
//	scaledTranslation = scaledTranslation * vScale;
//	b.defaultPose.Translate(scaledTranslation);


	
	b.skinToBone = Helper::ConvertMatrix4(mat); //b.defaultPose;
	b.skinToBone.Inverse();

	// set up bone bounds
	Box3 bb;
	IGameObject* obj = bone->GetIGameObject();
	obj->GetBoundingBox(bb);
	Matrix4 invDefaultPose = b.defaultPose;
	invDefaultPose.Inverse();
	b.bounds.SetMin(invDefaultPose * Helper::ConvertVector4(bb.pmin));
	b.bounds.SetMax(invDefaultPose * Helper::ConvertVector4(bb.pmax));

	int idx = mGameBones.size();
	mGameBones.push_back(pBone);

	// iterate through children bones
	int count = bone->GetChildCount();
	for (int i = 0; i < count; ++i)
	{
		AddBone(bone->GetNodeChild(i), bone, idx);
	}

	int nothing = 0;
}