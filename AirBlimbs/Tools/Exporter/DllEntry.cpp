#include <windows.h>
#include "resource.h"

#include "PCH.h"

//#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
//#else
//    #define DLL_EXPORT
//#endif

extern ClassDesc2* GetExporterDesc();

HINSTANCE hInstance;
int controlsInit = FALSE;


BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	hInstance = hinstDLL;				// Hang on to this DLL's instance handle.

	if (!controlsInit) {
		controlsInit = TRUE;
		InitCustomControls(hInstance);	// Initialize MAX's custom controls
		InitCommonControls();			// Initialize Win95 controls
	}

	return (TRUE);
}

TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if (hInstance)
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	return NULL;
}

DLL_EXPORT const TCHAR* LibDescription()
{
	return GetString(STR_LIBDESCRIPTION);
}

//TODO: Must change this number when adding a new class
DLL_EXPORT int LibNumberClasses()
{
	return 1;
}

DLL_EXPORT ClassDesc* LibClassDesc(int i)
{
	switch(i) {
		case 0: return GetExporterDesc();
		default: return 0;
	}
}

DLL_EXPORT ULONG LibVersion()
{
	return VERSION_3DSMAX;
}


