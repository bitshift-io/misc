#ifndef _MAXSKELETON_H_
#define _MAXSKELETON_H_

#include "MaxObject.h"
#include "Math/Quaternion.h"
#include "Render/Animation.h"
#include <vector>

using namespace std;

class Skeleton;

class MaxSkeleton : public MaxObject
{
public:

	MaxSkeleton(IGameNode* pGameNode);

	bool				Convert();

	virtual void		AddBone(IGameNode* bone, IGameNode* parent, int parentIdx);

	IGameSkin*			GetSkin();

	// given a bone, we can calculate its index if this skeleton has been updated
	virtual int			GetBoneIdx(IGameNode* bone);

	static bool			IsBone(IGameNode* bone);

	Matrix4				GetWorldToObjectTransform();

protected:

	Quaternion			CalculateParentOffset(IGameSkin* pSkin, IGameNode* bone, IGameNode* parent);

	Skeleton*			mSkeleton;
	vector<IGameNode*>	mGameBone;
	Animation*			mAnim;
};

#endif
