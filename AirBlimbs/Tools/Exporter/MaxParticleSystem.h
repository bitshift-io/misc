#ifndef _MAX_PARTICLESYSTEM_H_
#define _MAX_PARTICLESYSTEM_H_

#include "MaxObject.h"

class Scene;
class ParticleActionGroup;

class MaxParticleSystem : public MaxObject
{
public:

	MaxParticleSystem(IGameNode* pGameNode);

	bool	Convert(ParticleActionGroup* actionGroup, Scene* scene);

protected:

};

#endif
