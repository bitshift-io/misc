#ifndef _MAX_OBJECT_H_
#define _MAX_OBJECT_H_

#include "PCH.h"

class MaxObject
{
public:

	MaxObject(IGameNode* pGameNode);
	~MaxObject();

	virtual IGameObject::ObjectTypes	GetGameType() const			{ return mGameObject->GetIGameType(); }
	virtual const char*					GetName() const				{ return mGameNode->GetName(); }

protected:

	IGameNode*		mGameNode;
	IGameObject*	mGameObject;
};

#endif