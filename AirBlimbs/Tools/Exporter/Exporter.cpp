#include <windows.h>

#undef CreateWindow

#include "Max.h"
#include "Exporter.h"
#include "MaxGeometry.h"
#include "MaxSpline.h"
#include "MaxMesh.h"
#include "MaxLight.h"
#include "MaxCamera.h"
#include "MaxControl.h"
#include "MaxShape.h"
#include "MaxParticleSystem.h"
#include "Render/Export/ExpRenderFactory.h"
#include "Render/Scene.h"
#include "Render/Geometry.h"
#include "Render/Mesh.h"
#include "Render/Skin.h"
#include "Render/Spline.h"
#include "Render/AnimationController.h"
#include "Render/Light.h"
#include "Render/Camera.h"
#include "Render/Shape.h"
#include "Render/ParticleSystem.h"
#include "File/ScriptFile.h"
#include <string>

using namespace std;

Exporter*			Exporter::sInstance = NULL;

static ExporterClassDesc	sExporterDesc;
ClassDesc2*					GetExporterDesc()					{ return &sExporterDesc; }

#define GI_LIGHT_CLASS_ID Class_ID(0x4b241b11, 0x64e8527d)

BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND hwndTab;
	static HWND hwndDisplay;
	static HINSTANCE hinst = (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE);

	switch(message)
	{
	case WM_INITDIALOG:
		{
			CenterWindow(hWnd, GetParent(hWnd));
			gExporter->mProgress = GetDlgItem(hWnd, TXT_LOG);
			gExporter->mLog = GetDlgItem(hWnd, PRO_PROGRESS);
		}
		return TRUE;

	case WM_COMMAND:
		{
			switch (LOWORD(wParam))
			{
			case BTN_CLOSE:
				EndDialog(hWnd,1);
				return TRUE;
			}
		}
		return TRUE;

	case WM_CLOSE:
		EndDialog(hWnd, 1);
		return TRUE;
	}

	return FALSE;
}


//=========================================================================
// Exporter
//=========================================================================
Exporter::Exporter()
{
	sInstance = this;
	mRenderFactory = new ExpRenderFactory(RenderFactoryParameter());

	MemTracker::mEnableLog = false;
}

Exporter::~Exporter()
{
	delete mRenderFactory;
}


const TCHAR* Exporter::Ext(int n)
{
	switch (n)
	{
	case 0:
		return _T("sce");
	}

	return _T("");
}

TSTR Exporter::GetCfgFilename()
{
	TSTR filename;

	filename += GetCOREInterface()->GetDir(APP_PLUGCFG_DIR);
	filename += "\\";
	filename += "Exporter.cfg";

	return filename;
}

bool Exporter::ReadConfig()
{
//	FILE* pFile = fopen( GetCfgFilename(), "rb" );
//	fclose( pFile );
	return true;
}

bool Exporter::WriteConfig()
{
//	FILE* pFile = fopen( GetCfgFilename(), "wb" );
//	fclose( pFile );
	return true;
}

void Exporter::Log(const char* pText)
{
	int existingLen = GetWindowTextLength(GetDlgItem(mDialog, TXT_LOG));
	int len = existingLen + strlen(pText) + 2;
	char* pTemp = new char[len];
	existingLen = GetDlgItemText(mDialog, TXT_LOG, (LPSTR)pTemp, len);
	
	// append new line only if existing text
	if (existingLen > 0)
	{
		strcpy((char*)pTemp + existingLen, (LPSTR)"\r\n"); 
		strcpy((char*)pTemp + existingLen + 2, (LPSTR)pText); 
	}
	else
	{
		strcpy((char*)pTemp + existingLen, (LPSTR)pText);
	}

	SetDlgItemText(mDialog, TXT_LOG, (LPSTR)pTemp);
	RedrawWindow(mDialog, NULL, NULL, RDW_UPDATENOW);
	delete[] pTemp;
}

int	Exporter::DoExport(const TCHAR* name, ExpInterface* ei, Interface* i, BOOL suppressPrompts, DWORD options)
{
	mInterface			= GetCOREInterface();
	mName				= name;
	mExportSelected		= (options & SCENE_EXPORT_SELECTED) ? true : false;
	mIGameScene			= GetIGameInterface();
	mInterface			= i;
	mSuppressPrompts	= suppressPrompts == TRUE ? true : false;
	mOptions			= options;

	IGameConversionManager* cm = GetConversionManager();
	cm->SetCoordSystem(IGameConversionManager::IGAME_D3D); //IGAME_D3D);	
	mIGameScene->InitialiseIGame(mExportSelected);
	mIGameScene->SetStaticFrame(0);

	ReadConfig();

	mDialog = CreateDialogParam(hInstance,
				MAKEINTRESOURCE(DLG_EXPORT),
				mInterface->GetMAXHWnd(),
				DlgProc,
				(LPARAM)this);

	ShowWindow(mDialog, SW_SHOW);

	char buffer[256];
	sprintf(buffer, "Exporting to file: %s", name); 

	mName = name;
	string stringName = string(name);

	int nameLen = stringName.length();
	strcpy(&stringName[nameLen - 3], "sce"); // convert extension to lower case

	Log(buffer);

	sprintf(buffer, "IGame version: %.2f%", GetIGameVersion()); 
	Log(buffer);

	sprintf(buffer, "3ds max compatible version %.2f%", GetSupported3DSVersion()); 
	Log(buffer);

	mDevice = mRenderFactory->CreateDevice();
	mScene = mRenderFactory->CreateScene();
	mAnimation = (Animation*)mRenderFactory->Create(Animation::Type);


	ProcessNodes();
	ProcessParticleSystems();

	mScene->Save(stringName);

	if (mAnimation->GetNumNode())
	{
		string animName = stringName;
		strcpy(&animName[nameLen - 3], "anm");
		Log("Exporting animation data");
		File out(animName.c_str(), FAT_Write | FAT_Binary);
		mAnimation->Save(out);
	}

	mRenderFactory->ReleaseScene(&mScene);
	mRenderFactory->Release((SceneObject**)&mAnimation);

	mIGameScene->ReleaseIGame();
	WriteConfig();

	Log("Export Complete");

	return TRUE;
}

bool Exporter::ProcessNodes()
{
	if (mIGameScene->GetTopLevelNodeCount() <= 0)
		return false;

	SendMessage(mProgress, PBM_SETRANGE, 0, MAKELPARAM(0, mIGameScene->GetTopLevelNodeCount()));
	SendMessage(mProgress, PBM_SETSTEP, (WPARAM)1, 0);

	for (int i = 0; i < mIGameScene->GetTopLevelNodeCount(); ++i)
	{
		SceneNode* pSceneNode = mScene->CreateNode();
		
		IGameNode* pGameNode = mIGameScene->GetTopLevelNode(i);
		if (!ProcessNode(pGameNode, pSceneNode))
		{
			mScene->DestroyNode(pSceneNode);
			continue;
		}

		mScene->GetRootNode()->InsertChild(pSceneNode);
		SendMessage(mProgress, PBM_STEPIT, 0, 0); 
	}

	return true;
}

bool Exporter::ProcessNodeProperties(IGameNode* pGameNode, SceneNode* pSceneNode)
{	
	INode* pMaxNode = pGameNode->GetMaxNode();
	TSTR userProperties;
	pMaxNode->GetUserPropBuffer(userProperties);
	string strUser = userProperties.data();

	int offset = 0;
	int endOfLine = 0;
	int length = strUser.length();
	do
	{
		endOfLine = strUser.find_first_of("\n", offset);
		string line = strUser.substr(offset, endOfLine - offset);
		offset = endOfLine + 1;

		if (line.length() <= 0)// || line[0] != '#')
			continue;

		// we now need to split the string in to a key and value
		int divider = line.find_first_of("=");
		int initialOffset = line[0] == '#' ? 1 : 0;
		string key = line.substr(initialOffset, divider - 1);
		string value = line.substr(divider + 1, strUser.length());

		key = ScriptFile::StripWhitespace(key);
		value = ScriptFile::StripWhitespace(value);

		Property property;
		property.mKey = key;
		property.mValue = value;

		// some properties are handled by the exporter only, and shouldnt be exported
		if (ExportSpecificProperty(property))
			continue;

		pSceneNode->AddProperty(property);
		
	} while (endOfLine != -1);

	return true;
}

bool Exporter::ProcessNodeControl(IGameNode* pGameNode, SceneNode* pSceneNode)
{
	MaxControl control;
	control.Convert(pGameNode, pSceneNode);
	return true;
}

bool Exporter::ExportSpecificProperty(const Property& property)
{
	if (property.mKey.compare("vertexformat") == 0
	 || property.mKey.compare("flags") == 0
	 || property.mKey.compare("transform") == 0)
		return true;

	return false;
}

bool Exporter::ProcessNode(IGameNode* pGameNode, SceneNode* pSceneNode)
{
	if (!pGameNode || pGameNode->IsNodeHidden() || IgnoreNode(pGameNode))
		return false;

	const char* pName = pGameNode->GetName();
	pSceneNode->SetName(pName);
	ProcessNodeProperties(pGameNode, pSceneNode);
	Log(pName);
	
	// set node transform
	bool localSpace = true;
	INode* pMaxNode = pGameNode->GetMaxNode();
	CStr stringTransform;
	bool found = pMaxNode->GetUserPropString("#transform", stringTransform);
	if (found)
	{
		if (strstr(stringTransform.data(), "world"))
			localSpace = false;
	}

	CStr stringFlags;
	found = pMaxNode->GetUserPropString("#flags", stringFlags);
	if (found)
	{
		// TODO: tokenoize with |
		if (strstr(stringFlags.data(), "visible"))
			pSceneNode->SetFlags(SNF_Visible);
	}

	Matrix4 transform(MI_Identity);
	if (localSpace)
	{
		IGameNode* pParent = pGameNode->GetNodeParent();
		GMatrix localTransform = pParent ? pGameNode->GetWorldTM() * pParent->GetWorldTM().Inverse() : pGameNode->GetWorldTM(); 
		transform = Helper::ConvertMatrix4(localTransform, true);
	}
	pSceneNode->SetLocalTransform(transform);

	ProcessNodeControl(pGameNode, pSceneNode);

	SceneObject* pSceneObject = 0;
	IGameObject* pGameObject = pGameNode->GetIGameObject();
	if (!pGameObject)
		return false;

	Class_ID classId = pGameObject->GetMaxObject()->ClassID();
	SClass_ID superClassId = pGameObject->GetMaxObject()->SuperClassID();

	// special case
	if (classId == PLANE_CLASS_ID)
	{
		pSceneObject = ProcessHelper(pGameNode);
	}
	else
	{
		switch (pGameObject->GetIGameType())
		{
		case IGameObject::IGAME_MESH:
			pSceneObject = ProcessMesh(pGameNode);
			break;

		case IGameObject::IGAME_HELPER:
			pSceneObject = ProcessHelper(pGameNode);
			break;

		case IGameObject::IGAME_LIGHT:
			pSceneObject = ProcessLight(pGameNode);
			break;

		case IGameObject::IGAME_SPLINE:
			pSceneObject = ProcessSpline(pGameNode);
			break;

		case IGameObject::IGAME_CAMERA:
			pSceneObject = ProcessCamera(pGameNode);
			break;

		case IGameObject::IGAME_BONE:
			pSceneObject = ProcessBone(pSceneNode, pGameNode);
			break;

		case IGameObject::IGAME_IKCHAIN:
			Log("IK Chain not yet supported");
			break;

		case IGameObject::IGAME_UNKNOWN:
			Log("unknown");
			pSceneObject = ProcessHelper(pGameNode);
			break;
		}
	}

	pGameNode->ReleaseIGameObject();

	//if (!pSceneObject)
	//	return false;

	pSceneNode->SetObject(pSceneObject);

	for (int i = 0; i < pGameNode->GetChildCount(); ++i)
	{
		SceneNode* pChildSceneNode = mScene->CreateNode();
		
		IGameNode* pChildGameNode = pGameNode->GetNodeChild(i);
		if (!ProcessNode(pChildGameNode, pChildSceneNode))
		{
			mScene->DestroyNode(pChildSceneNode);
			continue;
		}

		pSceneNode->InsertChild(pChildSceneNode);
	}

	return true;
}

SceneObject* Exporter::ProcessMesh(IGameNode* pGameNode)
{
	INode* pMaxNode = pGameNode->GetMaxNode();
	CStr stringExport;
	if (pMaxNode->GetUserPropString("#export", stringExport))
	{
		if (strstr(stringExport.data(), "shape"))
		{
			MaxShape obj(pGameNode);
			Shape* shape = (Shape*)mRenderFactory->Create(Shape::Type);
			obj.Convert(shape);
			return (SceneObject*)shape;
		}
	}

	ObjectState os = pMaxNode->EvalWorldState(0);
	Object* maxObj = os.obj;
	ParticleObject* particleObj = GetParticleInterface(maxObj);
	if (particleObj)
		return 0;

	MaxMesh obj(pGameNode);
	if (obj.IsSkinned())
	{
		SkinMesh* mesh = mRenderFactory->Create<SkinMesh>();
		obj.Convert(mesh);
		return (SceneObject*)mesh;
	}

	Mesh* mesh = mRenderFactory->Create<Mesh>();
	obj.Convert(mesh);
	return (SceneObject*)mesh;
}

SceneObject* Exporter::ProcessHelper(IGameNode* pGameNode)
{
	MaxShape obj(pGameNode);
	Shape* shape = (Shape*)mRenderFactory->Create(Shape::Type);
	obj.Convert(shape);
	return (SceneObject*)shape;
}

SceneObject* Exporter::ProcessLight(IGameNode* pGameNode)
{
	// certain lights are pure evil, check for them now
	INode* pMaxNode = pGameNode->GetMaxNode();
	/*
	Object* pMaxObj = pMaxNode->GetObjectRef();
	Class_ID classId = pMaxObj->ClassID();

	const char* name = pGameNode->GetName();
	if (classId == GI_LIGHT_CLASS_ID)
		return 0;*/

	MaxLight obj(pGameNode);
	Light* light = (Light*)mRenderFactory->Create(Light::Type);
	if (!obj.Convert(light))
	{
		mRenderFactory->Release((SceneObject**)&light);
		return 0;
	}

	return (SceneObject*)light;
}

SceneObject* Exporter::ProcessSpline(IGameNode* pGameNode)
{
	MaxSpline obj(pGameNode);
	Spline* spline = (Spline*)mRenderFactory->Create(Spline::Type);
	obj.Convert(spline);
	return (SceneObject*)spline;
}

SceneObject* Exporter::ProcessCamera(IGameNode* pGameNode)
{
	MaxCamera obj(pGameNode);
	Camera* camera = (Camera*)mRenderFactory->Create(Camera::Type);
	obj.Convert(camera);
	return (SceneObject*)camera;
}

SceneObject* Exporter::ProcessBone(SceneNode* pSceneNode, IGameNode* pGameNode)
{
	return 0;
}

bool Exporter::ProcessParticleSystems()
{
	IPFActionListPool* actionListPool = GetPFActionListPool();
	for (int i = 0; i < actionListPool->NumActionLists(); ++i)
	{
		INode* actionListNode = actionListPool->GetActionList(i);
		IGameNode* gameNode = mIGameScene->GetIGameNode(actionListNode);
		SceneObject* pSceneObject = ProcessParticleSystem(gameNode);

		if (pSceneObject)
		{
			SceneNode* pChildSceneNode = mScene->CreateNode();
			pChildSceneNode->SetName(actionListNode->GetName());
			pChildSceneNode->SetObject(pSceneObject);
			mScene->GetRootNode()->InsertChild(pChildSceneNode);
		}
	}

	return true;
}

SceneObject* Exporter::ProcessParticleSystem(IGameNode* pGameNode)
{
	// TODO: particle systems need to be done last as they reference nodes pointers that need to be resolved
	MaxParticleSystem obj(pGameNode);
	ParticleActionGroup* system = (ParticleActionGroup*)mRenderFactory->Create(ParticleActionGroup::Type);
	obj.Convert(system, mScene);
	return system;
}

bool Exporter::IgnoreNode(IGameNode* pGameNode)
{
	Class_ID classId = pGameNode->GetMaxNode()->GetObjectRef()->ClassID();
	if (classId.PartB() == PFSubClassID_PartB 
		|| classId.PartB() == PFSubClassID_PartB_MXS_NonCreatable
		|| classId.PartB() == ParticleChannelClassID_PartB 
		|| classId.PartB() == PFActionClassID_PartB
		|| classId.PartB() == PFMaterialClassID_PartB
		|| classId.PartA() == 0x50320c9a	// PF source
		|| classId.PartA() == 0x74f93b01	// PF Engine
		|| classId.PartA() == 0x74f93b02	// PF source -> Event
		|| classId.PartA() == 0x74f93b07    // Particle view
		|| classId.PartA() == 0x1eb34300	// FP Arrow 02
		|| classId.PartA() == 0x1eb34300	// FP Arrow 1
		|| classId.PartB() == 0x1eb34300	// FP Arrows
		)
		return true;

	IPFActionListPool* actionListPool = GetPFActionListPool();
	for (int i = 0; i < actionListPool->NumActionLists(); ++i)
	{
		INode* actionListNode = actionListPool->GetActionList(i);
		if (pGameNode->GetMaxNode() == actionListNode)
			return true;

		IPFActionList* actionList = PFActionListInterface(actionListNode);
		for (int k = 0; k < actionList->NumActions(); ++k)
		{
			INode* actionNode = actionList->GetAction(k);
			if (pGameNode->GetMaxNode() == actionNode)
				return true;
		}
	}

	return false;
}