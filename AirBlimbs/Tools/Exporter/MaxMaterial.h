#ifndef _MAX_MATERIAL_H_
#define _MAX_MATERIAL_H_

#include "MaxObject.h"
#include <string>

using namespace std;

class Material;

class MaxMaterial : public MaxObject
{
public:

	MaxMaterial(IGameNode* pGameNode);

	bool	Convert(Material* material, IGameMaterial* pGameMaterial);

	string	ConvertToPath(const TCHAR* matName);

protected:

};

#endif
