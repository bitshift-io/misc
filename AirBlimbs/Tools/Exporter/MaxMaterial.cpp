#include "MaxMaterial.h"
#include "Exporter.h"
#include "Render/Material.h"

MaxMaterial::MaxMaterial(IGameNode* pGameNode) : MaxObject(pGameNode)
{
}

bool MaxMaterial::Convert(Material* material, IGameMaterial* pGameMaterial)
{
	TCHAR* matName = pGameMaterial ? pGameMaterial->GetMaterialName() : "null";
	material->SetName(matName);
	string filename = ConvertToPath(matName);
/*
	if (!pGameMaterial)
		return true;

	IGameFX* pGameFX = pGameMaterial->GetIGameFX();	
	if (!pGameFX)
		return true;

	File out(filename.c_str(), FAT_Write | FAT_Text);
	out.WriteString("effect\t%s\r\n", pGameFX->GetEffectFileName());
	out.WriteString("renderorder\t0\r\n");
	out.WriteString("\r\n");
	out.WriteString("pass\r\n{\r\n");

	for (int i = 0; i < pGameFX->GetNumberOfProperties(); ++i)
	{
		IGameFXProperty* pGameFXProperty = pGameFX->GetIGameFXProperty(i);
		out.WriteString("%s\t", pGameFXProperty->GetPropertyName());

		const TCHAR *pType = pGameFXProperty->GetPropertyType();

		 IGameProperty* pGameProperty = pGameFXProperty->GetIGameProperty();

		if (strcmp(pType, "float4") == 0)
			out.WriteString("%f %f %f %f\t", 0.f, 0.f, 0.f, 0.f);

		out.WriteString("\r\n");
	}

	out.WriteString("}\r\n");*/
	return true;
}

string MaxMaterial::ConvertToPath(const TCHAR* matName)
{
	string exportName = gExporter->mName;
	int idx = exportName.find_last_of('/');
	if (idx == -1)
		idx = exportName.find_last_of('\\');
	string exportPath = exportName.substr(0, idx + 1);
	return exportPath + string(matName) + string(".mat");
}