#include "MaxSpline.h"
#include "Exporter.h"
#include "Render/Spline.h"

MaxSpline::MaxSpline(IGameNode* pGameNode) : MaxObject(pGameNode)
{
}

bool MaxSpline::Convert(Spline* spline)
{
	if (!mGameNode || !mGameObject)
	{
		gExporter->Log("There is no object or node!");
		return false;
	}

	if (!spline)
	{
		gExporter->Log("ExpGeometry is invalid! FUCK");
		return false;
	}

	IGameSpline* pGameSpline = 0;
	try
	{
		pGameSpline = static_cast<IGameSpline*>(mGameObject);
		pGameSpline->InitializeData();
	}
	catch (...)
	{
		gExporter->Log("Failed to convert to IGameSpline");
		return false;
	}

	// convert to game spline
	spline->mSpline.resize(pGameSpline->GetNumberOfSplines());
	for (int i = 0; i < pGameSpline->GetNumberOfSplines(); ++i)
	{
		IGameSpline3D* pGameSpline3d = pGameSpline->GetIGameSpline3D(i);
		spline->mSpline[i].Init(pGameSpline3d->GetIGameKnotCount());

		for (int j = 0; j < pGameSpline3d->GetIGameKnotCount(); ++j)
		{
			IGameKnot* pGameKnot = pGameSpline3d->GetIGameKnot(j);

			Knot3 knot;
			knot.mType = KnotType(pGameKnot->GetKnotType());
			knot.mIn = Helper::ConvertVector4(pGameKnot->GetInVec());
			knot.mOut = Helper::ConvertVector4(pGameKnot->GetOutVec());
			knot.mPosition = Helper::ConvertVector4(pGameKnot->GetKnotPoint(), true);

			// max is wrong
			float temp = knot.mPosition.y;
			knot.mPosition.y = knot.mPosition.z;
			knot.mPosition.z = temp;

			spline->mSpline[i].mKnot[j] = knot;
		}
	}

	return true;
}
