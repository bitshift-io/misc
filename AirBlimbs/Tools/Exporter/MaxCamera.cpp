#include "MaxCamera.h"
#include "Render/Camera.h"

MaxCamera::MaxCamera(IGameNode* pGameNode) : MaxObject(pGameNode)
{
}

bool MaxCamera::Convert(Camera* camera)
{
	IGameCamera* pGameCamera = static_cast<IGameCamera*>(mGameObject);
	pGameCamera->InitializeData();

	float fov;
	float zFar;
	float zNear;

	pGameCamera->GetCameraFOV()->GetPropertyValue(fov);
	pGameCamera->GetCameraFarClip()->GetPropertyValue(zFar);
	pGameCamera->GetCameraNearClip()->GetPropertyValue(zNear);

	// convert max fov to my engine fov
	fov = fov * (1.f / 1.33f);

	camera->SetPerspective(Math::RtoD(fov), zNear, zFar);

	GMatrix nodeMatrix = mGameNode->GetWorldTM();
	Vector4 position = Helper::ConvertVector4(nodeMatrix.Translation(), true);
	Vector4 target(-nodeMatrix.GetColumn(2).x, -nodeMatrix.GetColumn(2).z, -nodeMatrix.GetColumn(2).y); // st0pid max
	if (pGameCamera->GetCameraTarget())
		target = Helper::ConvertVector4(pGameCamera->GetCameraTarget()->GetWorldTM().Translation(), true) - position;

	target.Normalize();

	camera->LookAt(position, position + target);
	return true;
}
