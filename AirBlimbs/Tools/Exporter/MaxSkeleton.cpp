#include "MaxSkeleton.h"
#include "Render/Skeleton.h"
#include "MaxAnimation.h"
#include <set>

using namespace std;

#define BIPBONE_CONTROL_CLASS_ID    Class_ID(0x9125,0)

// this is the class for all biped controllers except the root and the footsteps
#define BIPSLAVE_CONTROL_CLASS_ID Class_ID(0x9154,0)

// this is the class for the center of mass, biped root controller ("Bip01")
#define BIPBODY_CONTROL_CLASS_ID  Class_ID(0x9156,0)

// this is the class for the biped footstep controller ("Bip01 Footsteps")
#define FOOTPRINT_CLASS_ID Class_ID(0x3011,0)  

MaxSkeleton::MaxSkeleton(IGameNode* pGameNode) : MaxObject(pGameNode)
{

}

bool MaxSkeleton::Convert()
{/*
	// http://cvs.sourceforge.net/viewcvs.py/neoengine/neoexporters/3DSMax/exporter.cpp?rev=1.6

	IGameSkin* pSkin = mGameObject->GetIGameSkin();

	set<IGameNode*> boneSet;

	// go through each vertex, compile a list of bones
	for (int v = 0; v < pSkin->GetNumOfSkinnedVerts(); ++v)
	{
		for (int b = 0; b < pSkin->GetNumberOfBones(v); ++b)
		{
			IGameNode* boneNode = pSkin->GetIGameBone(v, b);
			const char* boneName = boneNode->GetName();
			if (boneNode && !boneNode->IsNodeHidden())
				boneSet.insert(boneNode);
		}
	}

	// now we have a unique set of bones, we need to find the root bone
	std::set<IGameNode*>::iterator boneIt;
	std::set<IGameNode*>::iterator secBoneIt;

	if (boneSet.empty())
		return true;

	IGameNode* root = (*boneSet.begin());

	IGameNode* parent = root->GetNodeParent();;
	
	bool rootFound = false;
	while (parent && !rootFound)
	{
		const char* parentName = root->GetName();

		if (!parent || !parent->GetIGameObject() || parent->IsNodeHidden())
		{
			rootFound = true;
		}
		else
		{
			root = parent;
			parent = root->GetNodeParent();
		}
	}

	const char* boneName = root->GetName();

	// create an animation group
	mAnim = new Animation();
	AddBone(root, 0, 0);*/
	return true;
}

IGameSkin* MaxSkeleton::GetSkin()
{
	return mGameObject->GetIGameSkin();
}

Matrix4 MaxSkeleton::GetWorldToObjectTransform()
{
	// Fabian's essay on max matrices:
	// we are using IGameInterface, so ALL meshes
	// are in world space
	// we need to convert characters back to thier initial pose space
	//
	// MaxController::GetObjectTM will return a matrix to convert
	// from the hidden/invisible pivot point of the mesh (this can be seen
	// by clicking the hierachey tab, and clicking reset pivot) to the visible 
	// pivot point of the mesh (this is seen if you dont reset the pivot point)
	//
	// GetNodeTM will return a matrix, that converts from the visible pivot point
	// to world space.

	// note: max treats skin differently from physique
	// objectTM = parentTM * nodeTM * offsetTM
	// IPhysique->GetInitNodeTM(..) returns objectTM
	// while ISkin->GetInitNodeTM(..) returns nodeTM
	// this fixes the max inconsistency
	INode* node = mGameNode->GetMaxNode();
	Matrix4 worldToObj(MI_Identity);
/*
	if (GetSkin()->GetSkinType() == IGameSkin::IGAME_SKIN)
	{
		// convert FROM world TO visible pivot point
		Max::Matrix3 nodeTM = node->GetNodeTM(0); // pivot to node
		Max::Matrix3 worldToPivot = Inverse(nodeTM); // go from world to pivot space

		Point3 p = worldToPivot.GetRow(0);
		worldToObj[0] = p[0];
		worldToObj[1] = p[1];
		worldToObj[2] = p[2];

		p = worldToPivot.GetRow(1);
		worldToObj[5] = p[0];
		worldToObj[6] = p[1];
		worldToObj[7] = p[2];

		p = worldToPivot.GetRow(2);
		worldToObj[9] = p[0];
		worldToObj[10] = p[1];
		worldToObj[11] = p[2];

		p = worldToPivot.GetRow(3);
		worldToObj[13] = p[0];
		worldToObj[14] = p[1];
		worldToObj[15] = p[2];
	}
*/
	return worldToObj;
}

void MaxSkeleton::AddBone(IGameNode* bone, IGameNode* parent, int parentIdx)
{/*
	if (!bone || bone->IsNodeHidden())
		return;

	const char* boneName = bone->GetName();

	Vector3 vScale(MAX_TO_METERS, MAX_TO_METERS, MAX_TO_METERS);//FMNOTE: this should be 0.01f;

	Bone b;
	b.mParentIdx = parentIdx;
	b.SetName(boneName);

	IGameSkin* pSkin = mGameObject->GetIGameSkin();
	GMatrix mat, parentMat;
	pSkin->GetInitBoneTM(bone, mat);

	if (parent)
	{
		pSkin->GetInitBoneTM(parent, parentMat);
	}

	// inverse out the parent 
	b.mDefaultPose = Helper::ConvertMatrix4(mat * Inverse(parentMat.ExtractMatrix3()));

	GMatrix initialSkinTM;
	pSkin->GetInitSkinTM(initialSkinTM);

	GMatrix boneSpace = GMatrix(Inverse(parentMat.ExtractMatrix3())) * mat;



	Vector3 scaledTranslation = b.mDefaultPose.GetTranslation();
	scaledTranslation = scaledTranslation * vScale;
	b.mDefaultPose.Translate(scaledTranslation);


	
	b.mSkinToBone = Helper::ConvertMatrix4(mat, false); //b.defaultPose;

	// dont forget to scale position
	scaledTranslation = b.mSkinToBone.GetTranslation();
	scaledTranslation = scaledTranslation * vScale;
	b.mSkinToBone.Translate(scaledTranslation);

	b.mSkinToBone.Inverse();


	// set up bone bounds
	Box3 bb;
	IGameObject* obj = bone->GetIGameObject();
	obj->GetBoundingBox(bb);
	Matrix4 invDefaultPose = b.mDefaultPose;
	invDefaultPose.Inverse();

	//mat = mat * Inverse(parentMat.ExtractMatrix3());
	Point4 parentTrans4 = parentMat.GetRow(3);
	Point4 trans4 = mat.GetRow(3) - parentTrans4;	
	Vector3 trans = Vector3(trans4[0], trans4[1], trans4[2]); // swap z and y
	b.mOffset = trans * MAX_TO_METERS; //we need to scale bones accordingly

	vector<AnimationStream>& animations = mAnim->GetStreams();
	MaxAnimation anim(bone);
	AnimationStream stream;
	anim.Convert(parent ? &(animations[b.mParentIdx]) : 0, &stream);
	animations.push_back(stream);

	int idx = mSkeleton->GetBones().size();
	mSkeleton->GetBones().push_back(b);
	mGameBone.push_back(bone);

	// iterate through children bones
	int count = bone->GetChildCount();
	for (int i = 0; i < count; ++i)
	{
		AddBone(bone->GetNodeChild(i), bone, idx);
	}
*/
	int nothing = 0;
}

Quaternion MaxSkeleton::CalculateParentOffset(IGameSkin* pSkin, IGameNode* bone, IGameNode* parent)
{
	Quaternion animOffset(Vector3(0,0,0), 0);
	//return animOffset;

	//if (!parent)
	//	return animOffset;

	GMatrix mat, parentMat;
	pSkin->GetInitBoneTM(bone, mat);

	if (parent)
		pSkin->GetInitBoneTM(parent, parentMat);

	Matrix4 testMat = Helper::ConvertMatrix4(mat), testParent = Helper::ConvertMatrix4(parentMat);
	testParent.Inverse();
	testMat = testMat * testParent;

	Quaternion ret;
	ret.CreateFromMatrix(testMat);
	return ret;
}

int MaxSkeleton::GetBoneIdx(IGameNode* bone)
{
	for (int i = 0; i < mGameBone.size(); ++i)
	{
		if (bone == mGameBone[i])
			return i;
	}

	return -1;
}

bool MaxSkeleton::IsBone(IGameNode* bone)
{
	INode* node = bone->GetMaxNode();
	Object* obj = bone->GetIGameObject()->GetMaxObject();
	Control* ctrl = node->GetTMController(); 

    // check if this is part of the biped hierarchy
    if(ctrl)
    {
        Class_ID nodeControllerClassId = ctrl->ClassID();
        Class_ID objectClassId = obj->ClassID();

        // make sure its not a nub
        if (objectClassId == Class_ID(DUMMY_CLASS_ID, 0x0))
			return false;

        // this is a bone, we should not include it in the hierarchy
        // instead we allow the dexcharacter to process it         
        if ((objectClassId == BIPBONE_CONTROL_CLASS_ID) ||
            (objectClassId == Class_ID(BONE_CLASS_ID, 0)) ||
            (ctrl && nodeControllerClassId == BIPSLAVE_CONTROL_CLASS_ID))
                return true;
    }

	// check if bone properties are turned on
	if (node->GetBoneNodeOnOff())
		return true;

	return false;
}