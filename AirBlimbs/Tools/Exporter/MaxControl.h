#ifndef _MAXCONTROL_H_
#define _MAXCONTROL_H_

#include "PCH.h"
#include "Math/Matrix.h"

class SceneNode;
class IGameNode;
class IGameControl;

class MaxControl
{
public:

	bool	Convert(IGameNode* pGameNode, SceneNode* pSceneNode);

	bool	GetTimeValue(IGameControl* gameControl, Tab<TimeValue>& times, IGameControlType type);
	Matrix4	GetLocalTransform(IGameNode* gameNode, const TimeValue& time);
};

#endif
