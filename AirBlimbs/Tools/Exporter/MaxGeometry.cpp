#include "MaxGeometry.h"
#include "Math/Matrix.h"
#include "Render/Export/ExpRenderBuffer.h"
#include "Exporter.h"
#include "Render/Geometry.h"
#include "MaxMesh.h"

#include <hash_set>
/*
struct eqstr
{
  bool operator()(const char* s1, const char* s2) const
  {
    return strcmp(s1, s2) == 0;
  }
};

//hash_multimap<const char*, int, hash_compare<const char*, const char*>/*, eqstr>* / map_type;
*/
MaxGeometry::MaxGeometry(IGameNode* pGameNode, MaxMesh* maxMesh) : MaxObject(pGameNode)
{
	mMaxMesh = maxMesh;
}

bool MaxGeometry::Convert(Geometry* geometry, IGameMesh* pGameMesh, const Tab<FaceEx*>& faces, int vertexFormat, bool localSpace, IGameSkin* pSkin)
{
	geometry->Init(gExporter->mDevice, vertexFormat);
	ConvertFaces(geometry, pGameMesh, faces, vertexFormat, pSkin);

	if (localSpace)
		ConvertToLocal(geometry);

	return true;
}

void MaxGeometry::ConvertToLocal(Geometry* geometry)
{
	VertexFrame& frame = geometry->GetVertexFrame(0);
	ExpRenderBuffer* position = static_cast<ExpRenderBuffer*>(frame.mPosition);

	GMatrix maxWorldToLocal = mGameNode->GetWorldTM();
	maxWorldToLocal = maxWorldToLocal.Inverse();
	Matrix4 worldToLocal = Helper::ConvertMatrix4(maxWorldToLocal, true);

	for (int i = 0; i < position->GetSize(); ++i)
	{
		// w affects xyz so back and restore illum for this operation
		Vector4& posWorld =	position->GetVector4(i);
		float illum = posWorld.w;
		posWorld.w = 1.f;
		posWorld = worldToLocal * posWorld;
		posWorld.w = illum;
	}
}

void MaxGeometry::ConvertFaces(Geometry* geometry, IGameMesh* pGameMesh, const Tab<FaceEx*>& faces, unsigned int vertexFormat, IGameSkin* pSkin)
{
	Tab<int> mapChan = pGameMesh->GetActiveMapChannelNum();

	int numTexCoordChan = 0;
	for (int t = 0; t < mapChan.Count(); ++t)
	{
		int channel = mapChan[t];
		if (channel < 1)
			continue;

		++numTexCoordChan;
	}
	numTexCoordChan = min(MAX_TEXCOORD, numTexCoordChan);

	int numNormal		= pGameMesh->GetNumberOfNormals();
	int numTangent		= pGameMesh->GetNumberOfTangents();
	int numBinormal		= pGameMesh->GetNumberOfBinormals();
	int numColour		= pGameMesh->GetNumberOfColorVerts();
	int numAlpha		= pGameMesh->GetNumberOfAlphaVerts();

	for (int f = 0; f < faces.Count(); ++f)
	{
		FaceEx* face = faces[f];
		//for(int i = 2; i >= 0; --i) // this fixes face winding to be clock wise!
		for (int i = 0; i < 3; ++i)
		{
			// generate a vertex
			Vertex vertex;

			// Texture coordinates
			int texCoordChan = 0;
			for (int t = 0; t < mapChan.Count(); ++t)
			{
				int channel = mapChan[t];
				if (channel < 1)
					continue;

				DWORD index[3];
				pGameMesh->GetMapFaceIndex(channel, face->meshFaceIndex, index); 
				Point3 texCoord = pGameMesh->GetMapVertex(channel, index[i]);
				Vector4 vTexCoord = Helper::ConvertVector4(texCoord);

				if (vertexFormat & (VF_TexCoord0 << texCoordChan))
				{
					vertex.mTexCoord[texCoordChan] = vTexCoord;
					vertex.mTexCoordSet[texCoordChan] = true;
				}

				++texCoordChan;
				if (texCoordChan >= MAX_TEXCOORD)
					break;
			}

			if (numNormal && (vertexFormat & VF_Normal))
			{
				vertex.mNormalSet = true;
				vertex.mNormal = Helper::ConvertVector4(pGameMesh->GetNormal(face->norm[i]));
			}

			if (vertexFormat & VF_Colour)
			{
				Vector4 vColour = Vector4(0.5f, 0.5f, 0.5f, 1.0f);
					
				if (numColour)
				{
					vColour = Helper::ConvertVector4(pGameMesh->GetColorVertex(face->color[i]));
					//Math::Swap(vColour.y, vColour.z); // make ur fucking mind up n00bs!
				}

				if (numAlpha)
					vColour.w = pGameMesh->GetAlphaVertex(face->alpha[i]);

				vertex.mColour = vColour;
				vertex.mColourSet = true;
			}

			// skinning
			if (pSkin)
			{
				Vector4 weights(0.0f, 0.0f, 0.0f, 0.0f);
				Vector4 bones(-1.f, -1.f, -1.f, -1.f);

				int numBones = pSkin->GetNumberOfBones(face->vert[i]);
				if (numBones > 4)
					gExporter->Log("A vertex is assigned to too many bones. Max bones supported is 4.");

				numBones = Math::Min(4, numBones);

				float totalWeight = 0.0f;
				for (int b = 0; b < numBones; ++b)
				{
					bones[b] = mMaxMesh->GetBoneIdx(pSkin->GetIGameBone(face->vert[i], b));
					if (bones[b] == -1)
						gExporter->Log("Vert assigned to invalid bone.");

					weights[b] = pSkin->GetWeight(face->vert[i], b);
					totalWeight += weights[b];
				}

				/*
				// normalize bone weights
				float weightAdjust = 1.f / totalWeight;
				weights *= weightAdjust;

				// verify bone weights are correct
				totalWeight = 0.f;
				for (int b = 0; b < 4; ++b)
					totalWeight += weights[b];

				if (totalWeight < (1.f - Math::Epsilon) || totalWeight > (1.f + Math::Epsilon))
				{
					gExporter->Log("Error calculating bone weight.");
				}*/

				vertex.mBoneWeight = weights;
				vertex.mBoneIndex = bones;
				vertex.mSkinSet = true;
			}

			int faceId = -1;
			for (int j = 0; j < pGameMesh->GetNumberOfFaces(); ++j)
			{
				if (face == pGameMesh->GetFace(j))
				{
					faceId = j;
					break;
				}
			}

			if (numTangent && (vertexFormat & VF_Tangent))
			{
				vertex.mTangentSet = true;
				vertex.mTangent = Helper::ConvertVector4(pGameMesh->GetTangent(pGameMesh->GetFaceVertexTangentBinormal(faceId, i)));
			}

			if (numBinormal && numTangent && (vertexFormat & VF_Binormal))
			{
				vertex.mBinormalSet = true;
				vertex.mBinormal = Helper::ConvertVector4(pGameMesh->GetBinormal(pGameMesh->GetFaceVertexTangentBinormal(faceId, i)));
			}
			/*
			{
				Vector4 vBinormal = vertex.mTangent[0].Cross(vertex.mNormal[0]);
				vertex.mBinormal.push_back(vBinormal);
			}*/

			vertex.mPointSet = true;
			vertex.mPoint = Helper::ConvertVector4(pGameMesh->GetVertex(face->vert[i]), true);

			// illuminosity gets packed in to the w component of position
			for (int t = 0; t < mapChan.Count(); ++t)
			{
				int channel = mapChan[t];
				if (channel != -1)
					continue;

				DWORD index[3];
				pGameMesh->GetMapFaceIndex(channel, face->meshFaceIndex, index); 
				Point3 texCoord = pGameMesh->GetMapVertex(channel, index[i]);
				Vector4 vTexCoord = Helper::ConvertVector4(texCoord);
				vertex.mPoint.w = vTexCoord.r;
			}

			int index = 0;
			if ((index = GetIndex(geometry, vertex)) == -1)
			{
				index = InsertVertex(geometry, vertex);
			}

			InsertIndex(geometry, index);
		}
	}
}

unsigned int MaxGeometry::GetIndex(Geometry* geometry, const Vertex& vertex)
{	
	float hash = vertex.Hash();
	pair<VertexHash::const_iterator, VertexHash::const_iterator> p = mVertexHash.equal_range(hash);
	for (VertexHash::const_iterator it = p.first; it != p.second; ++it)
	{
		if (vertex.Equals(it->second))
			return it->second.mIndex;
	}

	return -1;

/*
	unsigned int index = -1;

	VertexFrame& frame = geometry->GetVertexFrame(0);
	ExpRenderBuffer* position = static_cast<ExpRenderBuffer*>(frame.mPosition);
	ExpRenderBuffer* normal = static_cast<ExpRenderBuffer*>(frame.mNormal);
	ExpRenderBuffer* tangent = static_cast<ExpRenderBuffer*>(frame.mTangent);
	ExpRenderBuffer* binormal = static_cast<ExpRenderBuffer*>(frame.mBinormal);
	ExpRenderBuffer* colour = static_cast<ExpRenderBuffer*>(frame.mColour);
	vector<RenderBuffer*> texCoord = frame.mTexCoord;

	// see if this vertex already exists
	for (unsigned int i = 0; i < position->GetSize(); ++i)
	{
		if (position->GetVector4(i) != vertex.mPoint[0])
			continue;

		bool cont = false;
		for (int t = 0; t < vertex.mTexCoord.size(); ++t)
		{
			if (static_cast<ExpRenderBuffer*>(texCoord[t])->GetVector4(i) != vertex.mTexCoord[t])
			{
				cont = true;
				break;
			}
		}

		if (cont)
			continue;


		if (normal && normal->GetSize() && normal->GetVector4(i) != vertex.mNormal[0])
			continue;

		if (colour && colour->GetSize() && colour->GetVector4(i) != vertex.mColour[0])
			continue;

		if (tangent && tangent->GetSize() && tangent->GetVector4(i) != vertex.mTangent[0])
			continue;

		if (binormal && binormal->GetSize() && binormal->GetVector4(i) != vertex.mBinormal[0])
			continue;

		index = i;
		break;
	}

	return index;*/
}

void MaxGeometry::InsertIndex(Geometry* geometry, unsigned int index)
{
	ExpRenderBuffer* buffer = static_cast<ExpRenderBuffer*>(geometry->GetIndexBuffer());
	buffer->Insert(index);
}

unsigned int MaxGeometry::InsertVertex(Geometry* geometry, const Vertex& vertex)
{
	VertexFrame& frame = geometry->GetVertexFrame(0);
	ExpRenderBuffer* position = static_cast<ExpRenderBuffer*>(frame.mPosition);
	ExpRenderBuffer* normal = static_cast<ExpRenderBuffer*>(frame.mNormal);
	ExpRenderBuffer* tangent = static_cast<ExpRenderBuffer*>(frame.mTangent);
	ExpRenderBuffer* binormal = static_cast<ExpRenderBuffer*>(frame.mBinormal);
	ExpRenderBuffer* colour = static_cast<ExpRenderBuffer*>(frame.mColour);
	ExpRenderBuffer* boneWeight = static_cast<ExpRenderBuffer*>(frame.mBoneWeight);
	ExpRenderBuffer* boneIndex = static_cast<ExpRenderBuffer*>(frame.mBoneIndex);
	RenderBuffer** texCoord = frame.mTexCoord;

	unsigned int index = position->GetSize();

	position->Insert(vertex.mPoint);

	if (vertex.mNormalSet)
		normal->Insert(vertex.mNormal);

	if (vertex.mTangentSet)
		tangent->Insert(vertex.mTangent);

	if (vertex.mBinormalSet)
		binormal->Insert(vertex.mBinormal);

	if (vertex.mColourSet)
		colour->Insert(vertex.mColour);

	if (vertex.mSkinSet)
	{
		boneWeight->Insert(vertex.mBoneWeight);
		boneIndex->Insert(vertex.mBoneIndex);
	}

	for (int t = 0; t < MAX_TEXCOORD; ++t)
		if (vertex.mTexCoordSet[t])
			static_cast<ExpRenderBuffer*>(texCoord[t])->Insert(vertex.mTexCoord[t]);

	// insert in to hash
	VertexHash::iterator it = mVertexHash.insert(VertexHash::value_type(vertex.Hash(), vertex));
	it->second.mIndex = index;

	return index;
}