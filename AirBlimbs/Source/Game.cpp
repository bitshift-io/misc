#include "StdAfx.h"
#include "Game.h"

static const char *sBuild = __DATE__ " " __TIME__;

#ifdef WIN32
int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	char cmdLine[256];
	WideCharToMultiByte(CP_ACP, 0, lpCmdLine, -1, cmdLine, 256, NULL, NULL);
	return Game::Main(cmdLine);
}
#else
int main(int argc, char* argv[])
{
    string cmdLine;
    for (int i = 0; i < argc; ++i)
    {
        cmdLine += string(argv[i]);
    }
    return Game::Main(cmdLine.c_str());
}

#endif

bool Game::Init(const CmdLine& cmdLine)
{
	MemTracker::PushMemoryGroup("Game::Init");
	//MemTracker::SetBreakOnAllocNumber(61686);

	int numThread = -1;
	cmdLine.GetToken("numThread", numThread);
	gThreadPool.Init(numThread);

	string game = "Data"; // default game is called "Data"
	cmdLine.GetToken("game", game);

	gFilesystem.SetBaseDirectory(gFilesystem.GetBaseDirectory() + "/" + game);

	Log::Print("Build: %s\n", sBuild);
	Log::Print("Running game: %s\n", game.c_str());

	// build package mode!, build the package and quit
	vector<string> tokens;
	if (cmdLine.GetToken("package", tokens))
	{
		Log::Print("Building package: %s\n", tokens[0].c_str());
		Log::Print("Packaging files: %s\n", tokens[1].c_str());

		Package p;
		p.Open(tokens[0].c_str(), FAT_Write);
		p.AddDirectoryContents(gFilesystem.GetBaseDirectory(), true, tokens[1]);
		p.Close();
		return false;
	}


	// we should search for all packages in a directory instead of hard coding
	gFilesystem.RegisterPackage("game.pkg");
	gFilesystem.RegisterPackage("patch.pkg");
	gFilesystem.RegisterDirectoryContents(gFilesystem.GetBaseDirectory(), true);

	mCmdLine = cmdLine;

	// attempt to create device
	// render
	RenderFactoryParameter param;
	param.mCacheDir = "Cache";

	mDevice = 0;

#ifdef DX10

	if (!cmdLine.GetToken("opengl") && !cmdLine.GetToken("software"))
	{
		mRenderFactory = new DX10RenderFactory(param);
		if (InitWindowAndDevice())
		{
			Log::Print("[Game::Init] Using DirectX 10");
		}
		else
		{
			delete mRenderFactory;
			mRenderFactory = 0;
		}
	}

#endif

#ifdef OGL

	if (!mDevice && !cmdLine.GetToken("software"))
	{
		mRenderFactory = new OGLRenderFactory(param);
		if (InitWindowAndDevice())
		{
			Log::Print("[Game::Init] Using OpenGL");
		}
		else
		{
			delete mRenderFactory;
			mRenderFactory = 0;
		}
	}

#endif

	if (!mDevice)
	{
		Log::Error("[Game::Init] Failed to create a Device");
		return false;
	}

	mInput = new Input();
#ifdef DEBUG
	mInput->Init(mWindow, true);
#else
	mInput->Init(mWindow);
#endif

	// sound
	mSoundFactory = new SoundFactory();
	if (!mSoundFactory->Init(cmdLine.GetToken("nosound")))
		return false;

	// double the volume
	mSoundFactory->SetListenerGain(2.f);

#ifdef NETWORK
	// network
	mNetworkFactory = new NetworkFactory();
	mNetworkFactory->Init();

	// master server
	mMasterServer = new MasterServer;
	mMasterServer->Init(mNetworkFactory, mGameName.c_str(), mMasterServerName);
#endif

#ifdef PHYSICS
	// physics
	mPhysicsFactory = new PhysicsFactory;
	mPhysicsFactory->Init();
#endif

	mGameUpdate.SetUpdateRate(30);
	mNetworkUpdate.SetUpdateRate(1);
	mPhysicsUpdate.SetUpdateRate(1);

	// script mgr
	mScriptMgr = new ScriptMgr;

	// world
	mWorld = new World;
	mWorld->Init();

	// game mode mgr
	mGameModeMgr = new GameModeMgr();
	mGameModeMgr->Init();


	// set up game states
	Intro* intro = new Intro();
	intro->Init();
	InsertState(intro);
	Menu* menu = new Menu();
	menu->Init();
	InsertState(menu);
	InGame* ingame = new InGame();
	ingame->Init();
	InsertState(ingame);
	SetState(intro);

	mTestGame.Init();

	Log::Success("[Game::Init] Complete\n");

	MemTracker::PopMemoryGroup();
	MemTracker::PushMemoryGroup("Game::Update");
	return true;
}

bool Game::InitWindowAndDevice()
{
	mWindow = mRenderFactory->CreateWindow();
	WindowResolution res;
	res.bits = 32;
	res.width = 640;
	res.height = 480;
	mWindow->Init("AirBlimbs", &res, false);

	mDevice = mRenderFactory->CreateDevice();
	if (!mDevice->Init(mRenderFactory, mWindow))
	{
		mWindow->Deinit();
		mRenderFactory->ReleaseWindow(&mWindow);
		mRenderFactory->ReleaseDevice(&mDevice);
		mDevice = 0;
		return false;
	}

#ifdef DEBUG
	mDevice->SetClearColour(Vector4(0.f, 0.125f, 0.3f, 1.f));
#endif

	// show and clear to a dark colour so we dont hurt our precious eyes!
	mWindow->Show();
	mDevice->Begin();
	mDevice->End();
	return true;
}

void Game::Deinit()
{
	gFilesystem.OutputUsageStats();

	mTestGame.Deinit();

#ifdef PHYSICS
	// physics
	mPhysicsFactory->Deinit();
	delete mPhysicsFactory;
#endif

	// sound
	mSoundFactory->Deinit();
	delete mSoundFactory;

	mInput->Deinit();
	delete mInput;

	// render
	mDevice->Deinit(mRenderFactory, mWindow);
	mRenderFactory->ReleaseDevice(&mDevice);

	mWindow->Deinit();
	mRenderFactory->ReleaseWindow(&mWindow);

	gThreadPool.Deinit();

	MemTracker::PopMemoryGroup();
}

bool Game::UpdateGame()
{
	// alt enter - toggle fullscreen
	if (gGame.mInput->GetStateBool(KEY_RAlt) && gGame.mInput->WasPressed(KEY_Enter))
	{
		bool fullScreen = !mWindow->IsFullScreen();
		mDevice->Deinit(mRenderFactory, mWindow);
		mWindow->Deinit();
		mInput->Deinit();

		WindowResolution res;
		mWindow->GetWindowResolution(res);
		mWindow->Init(mWindow->GetTitle(), &res, fullScreen);
		mDevice->Init(mRenderFactory, mWindow);
		mInput->Init(mWindow);
		mWindow->Show();
		mRenderFactory->ReloadResource(mDevice);
	}

	// quit
	if (gGame.mInput->GetStateBool(KEY_Esc))
	{
		return false;
	}

	mTestGame.Update();
	return true;
}

bool Game::UpdateNetwork()
{
	return true;
}

bool Game::UpdatePhysics()
{
	return true;
}

void Game::Render()
{
	mTestGame.Render();
}

bool Game::Run()
{

/*
	int updateCount = 0;

	bool playing = true;
	while (playing)
	{
		mGameUpdate.Update();
		mNetworkUpdate.Update();
		mPhysicsUpdate.Update();

		// debug mode we dont care about frame rate independance!
		// might needs some tweaks to do at least 1 update and no more than 5 updates per loop,
		// ie push some of the updates forwards or backwards so game runs slightly faster/slower for a while
#ifdef _DEBUG

		Sleep(10); // bring fps down

		if (!(playing = UpdateGame()))
			return true;

		if (!(playing = UpdateNetwork()))
			return true;

		if (!(playing = UpdatePhysics()))
			return true;

#else

		for (i = 0; i < mGameUpdate.GetNumUpdates(); i++)
		{
			++updateCount;
			if (!(playing = UpdateGame()))
                return true;
		}

		for (i = 0; i < mNetworkUpdate.GetNumUpdates(); i++)
		{
			if (!(playing = UpdateNetwork()))
                 return true;
		}

		for (i = 0; i < mPhysicsUpdate.GetNumUpdates(); i++)
		{
			if (!(playing = UpdatePhysics()))
                return true;
		}
#endif

		Render();
	}
*/


	

	bool playing = true;
	while (playing)
	{
		unsigned long startTime = Time::GetMilliseconds();

		if (!(playing = UpdateGame()))
			return true;

		if (!(playing = UpdateNetwork()))
			return true;

		if (!(playing = UpdatePhysics()))
			return true;

		Render();

		unsigned long endTime = Time::GetMilliseconds();
		unsigned long deltaTime = endTime - startTime;
		unsigned long idealFrameTime = 1000 / 60; // fps
		unsigned long sleepTime = (deltaTime < idealFrameTime) ? (idealFrameTime - deltaTime) : 0;
		Sleep(sleepTime);
	}


	return true;
}

int Game::Main(const char* cmdLine)
{
	if (gGame.Init(CmdLine(cmdLine)))
	{
		gGame.Run();
		gGame.Deinit();
	}

	return 0;
}




