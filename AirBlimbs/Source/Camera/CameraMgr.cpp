#include "StdAfx.h"
#include "CameraMgr.h"

void CameraMgr::Init(World* world)
{
	mWorld = world;
	mTarget = 0;
	mActive = 0;

	// add cameras
	InsertCamera(new FirstPersonCamera);

	mActive = mGameCamera.front();
}

void CameraMgr::Deinit()
{
	// free cameras
	list<GameCamera*>::iterator it;
	for (it = mGameCamera.begin(); it != mGameCamera.end(); ++it)
	{
		//(*it)->Deinit();
		delete *it;
	}	
}

void CameraMgr::InsertCamera(GameCamera* camera)
{
	mGameCamera.push_back(camera);
}

void CameraMgr::Update()
{
	mActive->Update();
}
