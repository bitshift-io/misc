#include "StdAfx.h"
#include "Camera.h"


bool GameCamera::IsActive()
{
	return gGame.mWorld->mCameraMgr->mActive == this;
}

void GameCamera::Activate()
{
	gGame.mWorld->mCameraMgr->mActive = this;
}




void FirstPersonCamera::Init(Entity* entity)
{
	Parent::Init(entity);
	entity->InsertObserver(this);
	//Assert(entity->GetClassInfo() == GetStaticClassInfo(Actor));
}

void FirstPersonCamera::NotifyObservableChanged(ObserverEvent* event)
{
	if (event->observable == mParent)
		Update();
}

void FirstPersonCamera::Update()
{
	if (!IsActive())
		return;

	Entity* entity = gGame.mWorld->mCameraMgr->GetTarget();
	ClassDesc* pDesc = ::GetClassDesc<Actor>();
	if (entity->GetClassDesc()->InheritsFrom(pDesc))
	{
		Actor* actor = static_cast<Actor*>(entity);
		Camera* camera = gGame.mWorld->mRenderMgr->mCamera;
		camera->LookAt(actor->GetView());
	}
}