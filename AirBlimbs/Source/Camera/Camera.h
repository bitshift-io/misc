#ifndef _GAMECAMERA_H_
#define _GAMECAMERA_H_

//
// controls the camera
//
class GameCamera : public Entity
{
public:

	bool	IsActive();
	void	Activate();
};

class FirstPersonCamera : public GameCamera
{
public:

	typedef GameCamera Parent;

	virtual void			Init(Entity* entity);
	virtual void			NotifyObservableChanged(ObserverEvent* event);
	void					Update();
};

#endif