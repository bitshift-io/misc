#ifndef _CAMERAMGR_H_
#define _CAMERAMGR_H_

#include <list>

using namespace std;

class World;

class CameraMgr
{
public:

	void				Init(World* world);
	void				Deinit();
	void				Update();

	void				SetTarget(Entity* target)			{ mTarget = target; }
	Entity*				GetTarget()							{ return mTarget; }

//protected:

	void				InsertCamera(GameCamera* camera);

	World*				mWorld;

	Entity*				mTarget;
	GameCamera*			mActive;

	list<GameCamera*>	mGameCamera;	
};

#endif


