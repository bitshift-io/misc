#ifndef _GAMEMODEMGR_H_
#define _GAMEMODEMGR_H_

class Director;
class GameMode;

//
// manages game modes and game mode clients (ie directors)
//
class GameModeMgr
{
public:

	REFLECT_CLASS_M
	(
		GameModeMgr, 
		(
			FUNC_VOID(InsertDirector),
			FUNC(CreateDirector),
			FUNC_VOID(ReleaseDirector)
		)
	)

	typedef vector<Director*>::iterator DirectorIterator;

	void				Init();
	void				Deinit();

	void				Update();

	DirectorIterator	DirectorBegin()								{ return mDirector.begin(); }
	DirectorIterator	DirectorEnd()								{ return mDirector.end(); }
	void				InsertDirector(Director* director)			{ mDirector.push_back(director); }

	Director*			CreateDirector(const char* name);
	void				ReleaseDirector(Director** director);

	GameMode*			GetGameMode()								{ return mActive; }
	void				SetGameMode(GameMode* gameMode)				{ mActive = gameMode; }

	void				InsertGameMode(GameMode* gameMode);

protected:

	vector<Director*>	mDirector;
	vector<GameMode*>	mGameMode;
	GameMode*			mActive;
};

#endif