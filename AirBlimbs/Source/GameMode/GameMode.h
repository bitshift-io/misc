#ifndef _GAMEMODE_H_
#define _GAMEMODE_H_

//
// GameMode
// such as death match, TDM, tag team
// treasure hunt etc...
//
class GameMode
{
public:

	virtual Actor*			CreateActor(Director* director);
	virtual void			ReleaseActor(Actor** actor);

};

#endif
