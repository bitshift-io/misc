#include "StdAfx.h"
#include "GameModeMgr.h"

REGISTER_CLASS(GameModeMgr)

void GameModeMgr::Init()
{
	mActive = 0;
	gGame.mScriptMgr->Register<GameModeMgr>(this);
}

void GameModeMgr::Deinit()
{
}

void GameModeMgr::Update()
{
	DirectorIterator directorIt;
	for (directorIt = DirectorBegin(); directorIt != DirectorEnd(); ++directorIt)
	{
		(*directorIt)->Update();
	}
}

Director* GameModeMgr::CreateDirector(const char* name)
{
	string file = string(name) + string(".ref");
	ReflectScript script;
	list<void*> director;
	bool loaded = script.Open(file.c_str(), &director);
	if (!loaded || director.size() != 1)
		return 0;

	Director* dir = (Director*)director.front();
	dir->Init();
	return dir;
}

void GameModeMgr::ReleaseDirector(Director** director)
{
	if (!*director)
		return;

	(*director)->Deinit();
	delete *director;
	*director = 0;
}

void GameModeMgr::InsertGameMode(GameMode* gameMode)
{
	mGameMode.push_back(gameMode);
	if (!mActive)
		mActive = gameMode;
}