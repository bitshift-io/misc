#include "StdAfx.h"
#include "GameMode.h"

Actor* GameMode::CreateActor(Director* director)											
{ 
	list<void*> entityList;
	ReflectScript script;
	if (!script.Open("Actor.ref", &entityList, &CreateEntityCallback))
		return false;

	Actor* actor = (Actor*)entityList.front();
	actor->Init(0);
	gGame.mWorld->mEntityMgr->InsertEntity(actor);
	director->InsertActor(actor);
	return actor;
}

void GameMode::ReleaseActor(Actor** actor)
{
	if (!*actor)
		return;

	(*actor)->Deinit();
	delete *actor;
	*actor = 0;
}
