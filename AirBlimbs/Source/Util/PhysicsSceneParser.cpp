#include "StdAfx.h"
#include "Math/Vector.h"
#include "PhysicsSceneParser.h"

enum PhysicsType
{
	PT_DynamicRigidBody = 1,
	PT_KinematicRigidBody,
	PT_StaticRigidBody,
	PT_NonPhysical,
	PT_Undefined,
	PT_Cloth,
};

enum GeometryType
{
	GT_Automatic = 1,
	GT_Sphere,
	GT_Box,
	GT_Capsule,
	GT_Convex,
	GT_TriangleMesh,
};

struct PhysicsProperties
{
	PhysicsProperties() :
		collisionGroup(1),
		physicsType(-1)
	{
	}

	int		physicsType;
	float	mass;
	bool	isConcave;
	int		geometryType;
	int		materialType;
	float	friction;
	float	staticFriction;
	float	restitution;
	Vector4	initialVelocity;
	Vector4	initialSpin;
	int		collisionGroup;
};

#define PROPERTY_F(x, y)										\
{																\
	const Property* p = node->GetProperty(x);					\
	if (p)														\
		y = p->AsFloat();										\
}

#define PROPERTY_I(x, y)										\
{																\
	const Property* p = node->GetProperty(x);					\
	if (p)														\
		y = p->AsInt();											\
}


void PhysicsSceneParser::Init(PhysicsFactory* factory, PhysicsScene* physicsScene, SceneNode* node)
{
	mPhysicsScene = physicsScene;
	mFactory = factory;

	PhysicsProperties physProperties;
	PROPERTY_I("PhysicsType", physProperties.physicsType);
	if (physProperties.physicsType>= 0)
	{
		PROPERTY_F("Mass", physProperties.mass);
		PROPERTY_I("IsConcave", physProperties.isConcave);
		PROPERTY_I("GeometryType", physProperties.geometryType);
		PROPERTY_I("MaterialType", physProperties.materialType);
		PROPERTY_F("Friction", physProperties.friction);
		PROPERTY_F("StaticFriction", physProperties.staticFriction);
		PROPERTY_F("Restitution", physProperties.restitution);
		PROPERTY_F("InitialVelocityX", physProperties.initialVelocity.x);
		PROPERTY_F("InitialVelocityY", physProperties.initialVelocity.y);
		PROPERTY_F("InitialVelocityZ", physProperties.initialVelocity.z);
		PROPERTY_F("InitialSpinX", physProperties.initialSpin.x);
		PROPERTY_F("InitialSpinY", physProperties.initialSpin.y);
		PROPERTY_F("InitialSpinZ", physProperties.initialSpin.z);
		PROPERTY_I("CollisionGroup", physProperties.collisionGroup);

		SceneObject* obj = node->GetObject();
		if (obj)
		{
			int type = obj->GetType();
			switch (type)
			{
			case Mesh::Type:
				{
					ProcessMesh(node, static_cast<Mesh*>(obj), &physProperties);
				}
				break;
			}
		}
	}

	SceneNode::Iterator childIt;
	for (childIt = node->Begin(); childIt != node->End(); ++childIt)
		Init(factory, physicsScene, *childIt);
}

void PhysicsSceneParser::ProcessMesh(SceneNode* node, Mesh* mesh, PhysicsProperties* properties)
{
	//PhysicsBody* body = 0;

	// dynamic tri meshes can only get moved to follow the convex hull
	//if (properties->geometryType == GT_TriangleMesh)
	//	properties->physicsType = PT_StaticRigidBody;


	Geometry* geometry = mesh->GetMeshMat(0).geometry;

	PhysicsBodyConstructor constructor;
	switch (properties->geometryType)
	{
	case GT_Sphere:
		{
			RenderBufferLock posLock;
			geometry->GetVertexFrame(0).mPosition->Lock(posLock, RBLF_ReadOnly);
			float radius = posLock.vector4Data[0].Mag3();
			geometry->GetVertexFrame(0).mPosition->Unlock(posLock);

			constructor.AddSphere(radius, properties->collisionGroup);
			break;
		}

	case GT_Box:
		{
			RenderBufferLock posLock;
			geometry->GetVertexFrame(0).mPosition->Lock(posLock, RBLF_ReadOnly);

			Vector4 dimensions(VI_Zero);
			for (int i = 0; i < posLock.count; ++i)
			{
				dimensions.x = Math::Max(dimensions.x, Math::Abs(posLock.vector4Data[i].x));
				dimensions.y = Math::Max(dimensions.y, Math::Abs(posLock.vector4Data[i].y));
				dimensions.z = Math::Max(dimensions.z, Math::Abs(posLock.vector4Data[i].z));
			}
			geometry->GetVertexFrame(0).mPosition->Unlock(posLock);	

			constructor.AddBox(dimensions, properties->collisionGroup);
			break;
		}

	case GT_TriangleMesh:
	case GT_Convex:
		{
			TriMeshConstructor triMesh;
			triMesh.mIndexCount = geometry->GetIndexBuffer()->GetCount();
			triMesh.mPositionCount = geometry->GetVertexFrame(0).mPosition->GetCount();

			RenderBufferLock idxLock;
			geometry->GetIndexBuffer()->Lock(idxLock, RBLF_ReadOnly);
			triMesh.mIndex = idxLock.uintData;
			geometry->GetIndexBuffer()->Unlock(idxLock);

			RenderBufferLock posLock;
			geometry->GetVertexFrame(0).mPosition->Lock(posLock, RBLF_ReadOnly);
			triMesh.mPosition = posLock.vector4Data;
			geometry->GetVertexFrame(0).mPosition->Unlock(posLock);

			if (properties->geometryType == GT_Convex)
				constructor.AddConvexMesh(mFactory, triMesh, properties->collisionGroup);
			else
				constructor.AddTriMesh(mFactory, triMesh, properties->collisionGroup);

			break;
		}

	default:
		{
			WARN("Unsupported physics geometryType");
			return;
		}
	}

	if (properties->physicsType == PT_DynamicRigidBody)
		constructor.SetDynamicBody(properties->mass);

	PhysicsBody* body = mFactory->CreatePhysicsBody();
	body->Init(mPhysicsScene, constructor);
	body->SetTransform(node->GetWorldTransform());

	if (properties->physicsType == PT_DynamicRigidBody)
		body->EnableGravity(true);

	if (properties->physicsType == PT_KinematicRigidBody)
		body->SetKinematic(true);
	

	/*

	switch (properties->physicsType)
	{
	case PT_DynamicRigidBody:
		{
			switch (properties->geometryType)
			{
			case GT_Sphere:
				{
					// assume its centered on the matrix, and its a perfect sphere
					Geometry* geometry = mesh->GetMeshMat(0).geometry;

					RenderBufferLock posLock;
					geometry->GetVertexFrame(0).mPosition->Lock(posLock, RBLF_ReadOnly);
					float radius = posLock.vector4Data[0].Mag3();
					geometry->GetVertexFrame(0).mPosition->Unlock(posLock);				

					body = mFactory->CreatePhysicsBody();
					PhysicsBodyConstructor constructor;
					constructor.AddSphere(radius);
					constructor.SetDynamicBody(properties->mass);
					body->Init(mPhysicsScene, constructor);
					body->SetTransform(node->GetWorldTransform());
					body->EnableGravity(true);
				}
				break;

			case GT_Box:
				{
					// assume its centered on the matrix, and its a perfect sphere
					Geometry* geometry = mesh->GetMeshMat(0).geometry;

					RenderBufferLock posLock;
					geometry->GetVertexFrame(0).mPosition->Lock(posLock, RBLF_ReadOnly);

					Vector4 dimensions(VI_Zero);
					for (int i = 0; i < posLock.count; ++i)
					{
						dimensions.x = Math::Max(dimensions.x, Math::Abs(posLock.vector4Data[i].x));
						dimensions.y = Math::Max(dimensions.y, Math::Abs(posLock.vector4Data[i].y));
						dimensions.z = Math::Max(dimensions.z, Math::Abs(posLock.vector4Data[i].z));
					}
					geometry->GetVertexFrame(0).mPosition->Unlock(posLock);				

					body = mFactory->CreatePhysicsBody();
					PhysicsBodyConstructor constructor;
					constructor.AddBox(dimensions);
					constructor.SetDynamicBody(properties->mass);
					body->Init(mPhysicsScene, constructor);
					body->SetTransform(node->GetWorldTransform());
					body->EnableGravity(true);
				}
				break;

			case GT_Convex:
				{
					Geometry* geometry = mesh->GetMeshMat(0).geometry;

					body = mFactory->CreatePhysicsBody();
					TriMeshConstructor triMesh;
					triMesh.mIndexCount = geometry->GetIndexBuffer()->GetCount();
					triMesh.mPositionCount = geometry->GetVertexFrame(0).mPosition->GetCount();

					RenderBufferLock idxLock;
					geometry->GetIndexBuffer()->Lock(idxLock, RBLF_ReadOnly);
					triMesh.mIndex = idxLock.uintData;
					geometry->GetIndexBuffer()->Unlock(idxLock);

					RenderBufferLock posLock;
					geometry->GetVertexFrame(0).mPosition->Lock(posLock, RBLF_ReadOnly);
					triMesh.mPosition = posLock.vector4Data;
					geometry->GetVertexFrame(0).mPosition->Unlock(posLock);

					PhysicsBodyConstructor constructor;
					//if (properties->geometryType == GT_Convex)
						constructor.AddConvexMesh(mFactory, triMesh); //, properties->collisionGroup);
					//else
					//	constructor.AddTriMesh(mFactory, triMesh, properties->collisionGroup);

					constructor.SetDynamicBody(properties->mass);
					body->Init(mPhysicsScene, constructor);
					body->SetTransform(node->GetWorldTransform());
					body->EnableGravity(true);
				}
				break;

			case GT_TriangleMesh:
				{
					WARN("TriangleMeshshould be set as a static body");
				}
				break;
			}
		}
		break;

	case PT_KinematicRigidBody:
		{
			int nothing = 0;
			++nothing;
		}
		break;

	case PT_StaticRigidBody:
		{
			switch (properties->geometryType)
			{
			case GT_TriangleMesh:
				{
					Geometry* geometry = mesh->GetMeshMat(0).geometry;

					body = mFactory->CreatePhysicsBody();
					TriMeshConstructor triMesh;
					triMesh.mIndexCount = geometry->GetIndexBuffer()->GetCount();
					triMesh.mPositionCount = geometry->GetVertexFrame(0).mPosition->GetCount();

					RenderBufferLock idxLock;
					geometry->GetIndexBuffer()->Lock(idxLock, RBLF_ReadOnly);
					triMesh.mIndex = idxLock.uintData;
					geometry->GetIndexBuffer()->Unlock(idxLock);

					RenderBufferLock posLock;
					geometry->GetVertexFrame(0).mPosition->Lock(posLock, RBLF_ReadOnly);
					triMesh.mPosition = posLock.vector4Data;
					geometry->GetVertexFrame(0).mPosition->Unlock(posLock);

					PhysicsBodyConstructor constructor;
					constructor.AddTriMesh(mFactory, triMesh);
					body->Init(mPhysicsScene, constructor);
					body->SetTransform(node->GetWorldTransform());
				}
				break;

			default:
				break;
			}
		}
		break;

	case PT_NonPhysical:
		break;

	case PT_Undefined:
		break;

	case PT_Cloth:
		break;
	}
*/
	if (body)
	{
		PhysicsBodyNode bodyNode;
		bodyNode.body = body;
		bodyNode.node = node;
		mBodyNode.push_back(bodyNode);
	}
}