#ifndef _PHYSICSSCENEPARSER_H_
#define _PHYSICSSCENEPARSER_H_

struct PhysicsProperties;

//
// Parses the scene file to generate any physics requirements
//
struct PhysicsBodyNode
{
	PhysicsBody*	body;
	SceneNode*		node;
};

class PhysicsSceneParser
{
public:

	void Init(PhysicsFactory* factory, PhysicsScene* physicsScene, SceneNode* node);

//protected:

	void ProcessMesh(SceneNode* node, Mesh* mesh, PhysicsProperties* properties);

	vector<PhysicsBodyNode>	mBodyNode;
	PhysicsFactory*			mFactory;
	PhysicsScene*			mPhysicsScene;
};

#endif