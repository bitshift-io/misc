#ifndef _CMD_LINE_H_
#define _CMD_LINE_H_

#include <string>
#include <vector>

using namespace std;

typedef vector<pair<string, vector<string> > > CmdTokenPair;

class CmdLine
{
public:

	CmdLine()
	{
	}

	CmdLine(const string& cmdLine)
	{
		GenerateTokenPairs(cmdLine);
	}

	int ToInt(const string& name) const
	{
		int value = -1;
		const char* cStr = name.c_str();
		sscanf(cStr, "%i", &value);
		return value;
	}

	bool GetToken(const string& name, int& value) const
	{
		string str;
		if (!GetToken(name, str))
			return false;

		const char* cStr = str.c_str();
		sscanf(cStr, "%i", &value);
		return true;
	}

	bool GetToken(const string& name) const
	{
		CmdTokenPair::const_iterator it;
		for (it = mTokens.begin(); it != mTokens.end(); ++it)
		{
			if (name.compare(it->first) == 0)
			{
				return true;
			}
		}

		return false;
	}

	bool GetToken(const string& name, string& str) const
	{
		CmdTokenPair::const_iterator it;
		for (it = mTokens.begin(); it != mTokens.end(); ++it)
		{
			if (name.compare(it->first) == 0)
			{
				str = it->second[0];
				return true;
			}
		}

		return false;
	}

	bool GetToken(const string& name, vector<string>& strArray) const
	{
		CmdTokenPair::const_iterator it;
		for (it = mTokens.begin(); it != mTokens.end(); ++it)
		{
			if (name.compare(it->first) == 0)
			{
				strArray = it->second;
				return true;
			}
		}

		return false;
	}

	//
	// get the directory this exe is running from
	//
	const string& GetDirectory() const
	{
		return mDirectoy;
	}

	void GenerateTokenPairs(const string& cmdLine)
	{
		vector<string> tokens = Tokenize(cmdLine, "-");
		vector<string>::iterator it;
		for (it = tokens.begin(); it != tokens.end(); ++it)
		{
			vector<string> tokenCmd = Tokenize(*it, " ");

			vector<string> tokenList;
			for (int i = 1; i < tokenCmd.size(); ++i)
			{
				tokenList.push_back(tokenCmd[i]);
			}

			mTokens.push_back(pair<string, vector<string> >(tokenCmd[0], tokenList));
		}
	}

protected:

	vector<string> Tokenize(const string& str, const string& delimiters)
	{
		vector<string> tokens;

		// skip delimiters at beginning.
    	string::size_type lastPos = str.find_first_not_of(delimiters, 0);

		// find first "non-delimiter".
		string::size_type pos = str.find_first_of(delimiters, lastPos);

		while (string::npos != pos || string::npos != lastPos)
		{
    		// found a token, add it to the vector.
    		tokens.push_back(str.substr(lastPos, pos - lastPos));

    		// skip delimiters.  Note the "not_of"
    		lastPos = str.find_first_not_of(delimiters, pos);

    		// find next "non-delimiter"
    		pos = str.find_first_of(delimiters, lastPos);
		}

		return tokens;
	}

	CmdTokenPair	mTokens;
	string			mDirectoy;
};

#endif
