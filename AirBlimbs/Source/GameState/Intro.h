#ifndef _INTRO_H_
#define _INTRO_H_

class Intro : public GameState
{
public:

	virtual bool			Init();
	virtual void			Deinit();

	virtual void			StateEnter()		{}
	virtual void			StateExit()			{}

	virtual bool			UpdateGame();
	virtual bool			UpdateNetwork()		{ return true; }
	virtual bool			UpdatePhysics()		{ return true; }
	virtual void			Render()			{}

	virtual GameStateType	GetType()			{ return GST_Intro; }

protected:

	Flash					mFlash;
};

#endif