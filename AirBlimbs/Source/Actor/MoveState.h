#ifndef _MOVESTATE_H_
#define _MOVESTATE_H_

#include "Actor/ActorState.h"

//
// Im wondering if this should be more abstract/generic
// as movment system is really seperate from this state system
// the movememtn system controls the state system/anims
//
// this class should contain a list of ActorAction's
// when a controller action is pressed it is mapped to an actor action in this class
// which determines what to do with the key press
//

//
// General walking state
//
class MoveState : public ActorState, public CollisionReport
{
	typedef ActorState Parent;
public:

	MoveState();

	virtual void			Init(Entity* parent);

	virtual void			StateEnter();
	virtual void			StateExit();
	virtual void			StateUpdate();

	virtual ActorStateType	GetType()			{ return AST_Move; }

	virtual void			NotifyObservableChanged(ObserverEvent* event);
	virtual bool			OnRaycastHit(const RaycastHit& hit);

	float					mStrafeSpeed;
	float					mForwardSpeed;
	float					mBackwardSpeed;
	float					mGravity;

	PhysicsBody*			mFloor;
	float					mDistToFloor;
};


#endif
