#ifndef _ACTOR_H_
#define _ACTOR_H_

#include "ActorState.h"

//
// Erm, an actor :) complex entity
//
class Actor : public Entity, public StateMachine<ActorState>
{
	typedef Entity Parent;

public:

	REFLECT_CLASS
	(
		Actor, 
	)

	virtual void		Init(Entity* parent);
	virtual void		Update();
	Matrix4				GetTransform();
	void				SetTransform(const Matrix4& transform);
	Director*			GetDirector();

	Matrix4				GetView();

	void				Spawn();

protected:

};

#endif