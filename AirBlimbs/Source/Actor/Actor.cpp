#include "StdAfx.h"
#include "Actor.h"

REGISTER_CLASS_P(Actor, Entity)

void Actor::Init(Entity* parent)
{
	Parent::Init(parent);

	InsertState(new MoveState);
	InsertState(new UsePlacementState);
	InsertState(new JumpJetState);

	vector<ActorState*>::iterator it;
	for (it = mState.begin(); it != mState.end(); ++it)
	{
		(*it)->Init(this);
	}
	SetState(GetStateByIdx(0));

	Spawn();
}

void Actor::Update()
{
	if (GetState())
		GetState()->StateUpdate();

	vector<ActorState*>::iterator it;
	for (it = mState.begin(); it != mState.end(); ++it)
	{
		(*it)->Update();
	}

	Parent::Update();
}

Matrix4 Actor::GetTransform()
{
	ActorPhysics* physicsComponent = GetEntity<ActorPhysics>();
	if (physicsComponent)
		return (*physicsComponent)->GetTransform();

	SceneComponent* sceneComponent = GetEntity<SceneComponent>();
	if (sceneComponent)
		return sceneComponent->GetTransform();

	return Matrix4(MI_Identity);
}

void Actor::SetTransform(const Matrix4& transform)
{
	ActorPhysics* physicsComponent = GetEntity<ActorPhysics>();
	if (physicsComponent)
	{
		(*physicsComponent)->SetPosition(transform.GetTranslation());
		//(*physicsComponent)->SetTransform(transform);
	}

	SceneComponent* sceneComponent = GetEntity<SceneComponent>();
	if (sceneComponent)
		sceneComponent->SetTransform(transform);
}

Director* Actor::GetDirector()
{
	return GetEntity<Director>();
}

Matrix4 Actor::GetView()
{
	if (!GetDirector())
		return Matrix4(MI_Identity);

	Vector4 up(0.f, 1.f, 0.f);
	Vector4 view = GetDirector()->GetView();
	view.Normalize();
	Vector4 right = view.Cross(up);
	right.Normalize();
	up = right.Cross(view);

	Matrix4 viewMatrix(MI_Identity);

	viewMatrix.SetYAxis(up);
	viewMatrix.SetXAxis(right);
	viewMatrix.SetZAxis(view);

	const float neckLength = 0.2f;
	const float eyeHeight = 1.7f;

	Vector4 pos = GetTransform().GetTranslation();
	Vector4 camPos = pos + Vector4(0, eyeHeight - neckLength, 0);
	camPos += up * neckLength;
	viewMatrix.SetTranslation(camPos);
	return viewMatrix;
}

void Actor::Spawn()
{
	// just set the first for the time being
	EntityMgr::EntityIterator it;
	for (it = gGame.mWorld->mEntityMgr->EntityBegin<SpawnPoint>(); it != gGame.mWorld->mEntityMgr->EntityEnd<SpawnPoint>(); ++it)
	{
		ActorPhysics* physicsComponent = GetEntity<ActorPhysics>();
		//bool kinematic = (*physicsComponent)->IsKinematic();
		//(*physicsComponent)->SetKinematic(false);

		SpawnPoint* spawn = static_cast<SpawnPoint*>(*it);
		SetTransform(spawn->mTransform);

		//(*physicsComponent)->SetKinematic(kinematic);

		Matrix4 transform = GetTransform(); // for debugging
		break;
	}
}