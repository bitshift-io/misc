#ifndef _JUMPJETSTATE_H_
#define _JUMPJETSTATE_H_

#include "Actor/ActorState.h"

//
// Im wondering if this should be more abstract/generic
// as movment system is really seperate from this state system
// the movememtn system controls the state system/anims
//
// this class should contain a list of ActorAction's
// when a controller action is pressed it is mapped to an actor action in this class
// which determines what to do with the key press
//

//
// General walking state
//
class JumpJetState : public ActorState
{
	typedef ActorState Parent;
public:

	enum State
	{
		S_Jump,
		S_Jet,
		S_Fall,
	};

	JumpJetState();

	virtual void			Init(Entity* parent);

	virtual void			StateEnter();
	virtual void			StateExit();
	virtual void			StateUpdate();

	virtual void			Update();

	virtual ActorStateType	GetType()			{ return AST_JumpJet; }

	virtual void			NotifyObservableChanged(ObserverEvent* event);

	State					mState;
	Vector4					mVelocity;
	float					mTimeInState;
	float					mTimeToJet;
	float					mLerpTime;
	float					mPercentSpeedForward;
	float					mJumpSpeed;
	float					mStrafeSpeed;
	float					mForwardSpeed;
	float					mBackwardSpeed;
	float					mGravity;

	float					mMaxEnergy;
	float					mEnergy;
	float					mReplenishSpeed;
	float					mUseSpeed;

	PhysicsBody*			mFloor;
	float					mDistToFloor;
};


#endif
