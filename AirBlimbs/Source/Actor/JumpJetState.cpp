#include "StdAfx.h"
#include "JumpJetState.h"

JumpJetState::JumpJetState() :
	mStrafeSpeed(10.f),
	mForwardSpeed(10.f),
	mBackwardSpeed(10.f),
	mLerpTime(1.f),
	mJumpSpeed(5.f),
	mTimeInState(0.f),
	mTimeToJet(1.f),
	mEnergy(100.f),
	mMaxEnergy(100.f),
	mReplenishSpeed(10.f),
	mUseSpeed(10.f)
{
}

void JumpJetState::Init(Entity* parent)
{
	Parent::Init(parent);

	// notify us of collisions
	ActorPhysics* physics = parent->GetEntity<ActorPhysics>();
	physics->InsertObserver(this);
}

void JumpJetState::StateEnter()
{
	mPercentSpeedForward = 0.f;
	mGravity = 0.f;
	mFloor = 0;
	mTimeInState = 0.f;
	mState = S_Jump;

	// do jump
	Director* director = GetActor()->GetDirector();
	ActorPhysics* actorPhysics = GetActor()->GetEntity<ActorPhysics>();
	PhysicsActorController* physicsController = actorPhysics->mBody;

	// remove any upward movement from the view
	Vector4 up(0.f, 1.f, 0.f);
	Vector4 view = director->GetView();
	Vector4 right = view.Cross(up);
	right.Normalize();

	view = up.Cross(right);
	view.Normalize();

	mVelocity = Vector4(0.f, mJumpSpeed, 0.f);

	if (director->GetControlState(CT_MoveForward) > 0.5f)
		mVelocity += view * mForwardSpeed;

	if (director->GetControlState(CT_MoveBackward) > 0.5f)
		mVelocity -= view * mBackwardSpeed;

	if (director->GetControlState(CT_StrafeLeft) > 0.5f)
		mVelocity -= right * mStrafeSpeed;

	if (director->GetControlState(CT_StrafeRight) > 0.5f)
		mVelocity += right * mStrafeSpeed;

	// move physics and scene
	Vector4 finalMovement = mVelocity * gGame.mGameUpdate.GetDeltaTime();
	physicsController->Move(finalMovement);

	Vector4 pos = physicsController->GetPosition();

	Matrix4 transform(MI_Identity);
	transform.SetTranslation(pos);
	transform.SetZAxis(view);
	transform.SetXAxis(right);
	transform.SetYAxis(up);

	SceneComponent* sceneComponent = GetActor()->GetEntity<SceneComponent>();
	if (sceneComponent)
		sceneComponent->SetTransform(transform);
}

void JumpJetState::StateExit()
{

}

void JumpJetState::NotifyObservableChanged(ObserverEvent* event)
{
	if (GetActor()->GetState() != this)
		return;

	// return to move state once we hit the ground and jump not being pressed
	Director* director = GetActor()->GetDirector();
	if (director->GetControlState(CT_Jump) <= 0.5f
		&& event->GetClassDesc() == ::GetClassDesc<ContactReportEvent>())
	{
		GetActor()->SetState(GetActor()->GetStateByType(AST_Move));
	}
}

void JumpJetState::StateUpdate()
{
	mTimeInState += gGame.mGameUpdate.GetDeltaTime();

	Director* director = GetActor()->GetDirector();
	ActorPhysics* actorPhysics = GetActor()->GetEntity<ActorPhysics>();
	PhysicsActorController* physicsController = actorPhysics->mBody;

	Vector4 gravity(0.f, -9.8f, 0.f);

	// remove any upward movement from the view
	Vector4 up(0.f, 1.f, 0.f);
	Vector4 view = director->GetView();
	Vector4 right = view.Cross(up);
	right.Normalize();
	up = right.Cross(view);

	// begin jetting
	if (mVelocity.y <= 0.f && mState == S_Jump)
	{
		// holding down forward means go to jet
		if (director->GetControlState(CT_Jump) > 0.5f)
		{
			mVelocity.y = 0.f;
			mState = S_Jet;
		}
		else
		{
			mState = S_Fall;
		}
	}

	if (mState == S_Jet)
	{
		mEnergy -= mUseSpeed * gGame.mGameUpdate.GetDeltaTime();
		mEnergy = Math::Max(0.f, mEnergy);

  		mVelocity = view * mForwardSpeed;
		if (director->GetControlState(CT_Jump) < 0.5 || mEnergy <= 0.f)
		{
			mState = S_Fall;
		}
	}
	else if (mState != S_Jet)
	{
		mVelocity.y += gravity.y * gGame.mGameUpdate.GetDeltaTime();
	}

	// move physics and scene
	Vector4 finalMovement = mVelocity * gGame.mGameUpdate.GetDeltaTime();
	physicsController->Move(finalMovement);

	Vector4 pos = physicsController->GetPosition();

	Matrix4 transform(MI_Identity);
	transform.SetTranslation(pos);
	transform.SetZAxis(view);
	transform.SetXAxis(right);
	transform.SetYAxis(up);

	SceneComponent* sceneComponent = GetActor()->GetEntity<SceneComponent>();
	if (sceneComponent)
		sceneComponent->SetTransform(transform);
}

void JumpJetState::Update()
{
	Parent::Update();

	if (GetActor()->GetState() == this)
		return;

	mEnergy += mReplenishSpeed * gGame.mGameUpdate.GetDeltaTime();
	mEnergy = Math::Min(mMaxEnergy, mEnergy);
}