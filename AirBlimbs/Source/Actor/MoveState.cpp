#include "StdAfx.h"
#include "MoveState.h"

MoveState::MoveState() :
	mStrafeSpeed(10.f),
	mForwardSpeed(10.f),
	mBackwardSpeed(10.f)
{
}

void MoveState::Init(Entity* parent)
{
	Parent::Init(parent);

	// notify us of collisions
	ActorPhysics* physics = parent->GetEntity<ActorPhysics>();
	physics->InsertObserver(this);
}

void MoveState::StateEnter()
{
	mGravity = 0.f;
	mFloor = 0;
}

void MoveState::StateExit()
{

}

void MoveState::NotifyObservableChanged(ObserverEvent* event)
{
	if (GetActor()->GetState() != this)
		return;

	if (event->GetClassDesc() == ::GetClassDesc<ContactReportEvent>())
	{
		ContactReportEvent* contactReport = (ContactReportEvent*)event;
		mFloor = contactReport->body;
		mDistToFloor = 0.f;
	}
}

bool MoveState::OnRaycastHit(const RaycastHit& hit)
{
	ActorPhysics* actorPhysics = GetActor()->GetEntity<ActorPhysics>();
	PhysicsActorController* physicsController = actorPhysics->mBody;

	// ignore our own actors capsule/phsyics
	if (hit.body == physicsController)
		return true;

	//mGravity = 
	mFloor = hit.body;
	mDistToFloor = hit.distance;
 	return false;
}

void MoveState::StateUpdate()
{
	Director* director = GetActor()->GetDirector();
	ActorPhysics* actorPhysics = GetActor()->GetEntity<ActorPhysics>();
	PhysicsActorController* physicsController = actorPhysics->mBody;

	Vector4 gravity(0.f, -9.8f, 0.f);
	Vector4 jumpVelocity(0.f, 5.0f, 0.f);
	Vector4 movement(0.f, 0.f, 0.f);

	// remove any upward movement from the view
	Vector4 up(0.f, 1.f, 0.f);
	Vector4 view = director->GetView();
	Vector4 right = view.Cross(up);
	right.Normalize();

	view = up.Cross(right);
	view.Normalize();

	// if we are standing on a body, we should inherit its velocity
	PhysicsBody* floor = physicsController->GetFloor();
	Vector4 floorVelocity(VI_Zero);
	Vector4 pawnPos = physicsController->GetTransform().GetTranslation();

	// no floor? ray cast down a bit just in case we are on a ship thats moving down for example
	if (!floor)
	{
		mDistToFloor = 0.f;
		mFloor = 0;
		unsigned int group = -1; //(0x1 << 1) | (0x1 << 2) | (0x1 << 3) | (0x1 << 5);
		Raycast(gGame.mWorld->mPhysicsScene, pawnPos + Vector4(0.f, 0.5f, 0.f), Vector4(0.f, -1.0f, 0.f), 3.0f, group);
		floor = mFloor;
		mDistToFloor -= 0.5f;

		// if really close to the floor, adjust gravity so we hit the floor
		// should help some jerking around occasionally
		if (mFloor && mDistToFloor <= 0.1f)
			mGravity = -mDistToFloor * (1.0f / gGame.mGameUpdate.GetDeltaTime());
		else
			mGravity += gravity.y * gGame.mGameUpdate.GetDeltaTime();
	}
	else
	{
		mGravity = 0.f;
	}

	if (floor)
		floorVelocity = floor->GetPointVelocity(pawnPos);		

	// todo replace this to depend on the view vector
	if (director->GetControlState(CT_MoveForward) > 0.5f)
		movement += view * mForwardSpeed;

	if (director->GetControlState(CT_MoveBackward) > 0.5f)
		movement -= view * mBackwardSpeed;

	if (director->GetControlState(CT_StrafeLeft) > 0.5f)
		movement -= right * mStrafeSpeed;

	if (director->GetControlState(CT_StrafeRight) > 0.5f)
		movement += right * mStrafeSpeed;

	if (/*floor && */director->GetControlState(CT_Jump) > 0.5f)
	{
		GetActor()->SetState(GetActor()->GetStateByType(AST_JumpJet));
		//mGravity = jumpVelocity.y;
	}

	// move physics and scene
	Vector4 finalMovement = (floorVelocity + movement + Vector4(0.f, mGravity, 0.f)) * gGame.mGameUpdate.GetDeltaTime();
	physicsController->Move(finalMovement);

	Vector4 pos = physicsController->GetPosition();

	Matrix4 transform(MI_Identity);
	transform.SetTranslation(pos);
	transform.SetZAxis(view);
	transform.SetXAxis(right);
	transform.SetYAxis(up);

	SceneComponent* sceneComponent = GetActor()->GetEntity<SceneComponent>();
	if (sceneComponent)
		sceneComponent->SetTransform(transform);

	// check for use placements
	UsePlacementState* usePlacement = static_cast<UsePlacementState*>(GetActor()->GetStateByType(AST_UsePlacement));
	usePlacement->CheckUsePlacement();
}