#ifndef _USEPLACEMENTSTATE_H_
#define _USEPLACEMENTSTATE_H_

#include "Actor/ActorState.h"

//
// Im wondering if this should be more abstract/generic
// as movment system is really seperate from this state system
// the movememtn system controls the state system/anims
//
// this class should contain a list of ActorAction's
// when a controller action is pressed it is mapped to an actor action in this class
// which determines what to do with the key press
//

//
// General walking state
//
class UsePlacementState : public ActorState, public CollisionReport
{
public:

	UsePlacementState();


	virtual void			StateEnter();
	virtual void			StateExit();
	virtual void			StateUpdate();

	virtual ActorStateType	GetType()			{ return AST_UsePlacement; }

	virtual bool			OnHit(const RaycastHit& hit);

	virtual bool			CheckUsePlacement();
	virtual void			UsePlacement(Mount* mount);

	Mount*					mMount;
};


#endif
