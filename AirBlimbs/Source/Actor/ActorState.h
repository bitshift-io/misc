#ifndef _ACTORSTATE_H_
#define _ACTORSTATE_H_

class Actor;

enum ActorStateType
{
	AST_Dead,
	AST_Move,
	AST_JumpJet,
	AST_Idle,
	AST_UsePlacement,
};

class ActorState : public Entity
{
	typedef Entity Parent;
public:

	virtual void			Deinit()			{}

	virtual void			StateEnter()		{}
	virtual void			StateExit()			{}
	virtual void			StateUpdate()		{}

	virtual ActorStateType	GetType()			= 0;

	inline Actor*			GetActor()			{ return (Actor*)mParent; }

protected:
};

#endif