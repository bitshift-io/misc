#include "StdAfx.h"
#include "MoveState.h"

UsePlacementState::UsePlacementState()
{
}

void UsePlacementState::StateEnter()
{
}

void UsePlacementState::StateExit()
{
}

bool UsePlacementState::CheckUsePlacement()
{
	Director* director = GetActor()->GetDirector();
	if (director->WasPressed(CT_Use))
	{
		// just set the first for the time being
		EntityMgr::EntityIterator it;
		for (it = gGame.mWorld->mEntityMgr->EntityBegin<Placement>(); it != gGame.mWorld->mEntityMgr->EntityEnd<Placement>(); ++it)
		{
			Placement* placement = static_cast<Placement*>(*it);
			Sphere test(GetActor()->GetTransform().GetTranslation(), 1.f);

			Mount* m = placement->GetMount(test);
			if (m)
			{
				UsePlacement(m);
				return true;
			}
		}
	}

	return false;
}

void UsePlacementState::UsePlacement(Mount* mount)
{
	mMount = mount;

	if (mount->GetMountType() == Mount::MT_Use)
	{
		mount->Use(GetActor());
	}
	else
	{
		// mount if its free to use
		if (mMount->MountActor(GetActor()))
			GetActor()->SetState(this);
	}
}

bool UsePlacementState::OnHit(const RaycastHit& hit)
{
 	return false;
}

void UsePlacementState::StateUpdate()
{
	Director* director = GetActor()->GetDirector();
	ActorPhysics* actorPhysics = GetActor()->GetEntity<ActorPhysics>();
	PhysicsActorController* physicsController = actorPhysics->mBody;

	if (director->WasPressed(CT_Use))
	{
		mMount->DismountActor(GetActor());
		GetActor()->SetState(GetActor()->GetStateByType(AST_Move));
	}

	// move actor to mount node position
	SceneNode* node = mMount->GetMountNode();
	Matrix4 transform = node->GetWorldTransform();
	GetActor()->SetTransform(transform);
}