#include "StdAfx.h"
#include "Math.h"

void Matrix4TypeDesc::GetTypeName(char* buf)
{
	strcpy(buf, "Matrix4");
}

bool Matrix4TypeDesc::LoadString(void* variable, const char* strValue, int index) const
{
	vector<string> values = String::Tokenize(strValue, ",", true);
	vector<string>::const_iterator it;
	int i = 0;
	for (it = values.begin(); it != values.end(); ++it, ++i)
	{
		float value = 0.f;
		sscanf(it->c_str(), "%f", &value);

		float* floatArray = static_cast<float*>(variable);
		floatArray[i] = value;
	}

	return true;
}

bool Matrix4TypeDesc::LoadBinary(void* variable, void* data, int index) const
{
	return true;
}






void Vector4TypeDesc::GetTypeName(char* buf)
{
	strcpy(buf, "Vector4");
}

bool Vector4TypeDesc::LoadString(void* variable, const char* strValue, int index) const
{
	vector<string> values = String::Tokenize(strValue, ",", true);
	vector<string>::const_iterator it;
	int i = 0;
	for (it = values.begin(); it != values.end(); ++it, ++i)
	{
		float value = 0.f;
		sscanf(it->c_str(), "%f", &value);

		float* floatArray = static_cast<float*>(variable);
		floatArray[i] = value;
	}

	return true;
}

bool Vector4TypeDesc::LoadBinary(void* variable, void* data, int index) const
{
	return true;
}