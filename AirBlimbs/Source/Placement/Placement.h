#ifndef _PLACEMENT_H_
#define _PLACEMENT_H_

class SceneComponent;

enum RotationAxis
{
	RA_X,
	RA_Y,
	RA_Z,
};

REFLECT_ENUM(RotationAxis);

// should this extend from entity?
class PlacementConstraint : public Entity
{
	typedef Entity Parent;

public:

	REFLECT_CLASS_V
	(
		PlacementConstraint,
		(
			VAR_PTR(mNodeName),
			VAR(mSpeed),
			VAR_ENUM(RotationAxis, mRotationAxis),
			VAR(mCurRotation),
			VAR(mMaxRotation),
			VAR(mMinRotation),
			VAR(mLerpSpeed),
			VAR_PTR(mDirectorEntityName),
			VAR_ENUM(ControlType, mControlType)
		)
	)

	PlacementConstraint();
	~PlacementConstraint();

	virtual void			Init(Entity* parent);
	virtual void			Update();

	float					mSpeed;				// degrees per second
	const char*				mNodeName;
	SceneNode*				mNode;

	RotationAxis			mRotationAxis;
	float					mCurRotation;
	float					mMaxRotation;
	float					mMinRotation;
	float					mLerpSpeed;	// lerp speed, if not -1, this will lerp to default pose

	ControlType				mControlType;

	const char*				mDirectorEntityName;
	Entity*					mDirectorEntity;
};

class Mount : public Entity
{
	typedef Entity Parent;

public:

	enum MountType
	{
		MT_Use,
		MT_Mount,
	};

	REFLECT_CLASS_V
	(
		Mount,
		(
			VAR_PTR(mUseNodeName),
			VAR_PTR(mMountNodeName),
			VAR(mRadius)
		)
	)

	Mount();
	~Mount();

	virtual void			Init(Entity* parent);

	virtual bool			GetMount(const Sphere& test);
	virtual	MountType		GetMountType();
	virtual void			Use(Actor* actor)					{}
	virtual bool			MountActor(Actor* actor);
	virtual void			DismountActor(Actor* actor);
	virtual Actor*			GetMountedActor();
	SceneNode*				GetMountNode();

	// if no mount node its treated as a use node
	const char*				mMountNodeName;
	SceneNode*				mMountNode;
	const char*				mUseNodeName;
	SceneNode*				mUseNode;
	float					mRadius;
};

//
// means an entity can be situated in the world
//
class Placement : public Entity
{
	typedef Entity Parent;

public:

	REFLECT_CLASS_V
	(
		Placement,
		(
			//VAR_PTR_STL(mConstraint),
			//VAR_PTR_STL(mUsePoint),
			VAR_PTR(mAttachNodeName),
			VAR(mTransform)
		)
	)

	Placement();
	~Placement();

	virtual void		Init(Entity* owner);

	//virtual void		NotifyObservableChanged(const ObserverEvent* event);
	virtual void		Update();

	// get location to mount to, returns null if non in area of sphere
	virtual Mount*		GetMount(const Sphere& test);

	virtual SceneComponent*	GetSceneComponent();

	//vector<PlacementConstraint*>	mConstraint;
	//vector<UseSphere*>				mUsePoint;

	Matrix4				mTransform;
	const char*			mAttachNodeName;
	SceneNode*			mAttachNode;
};



#endif