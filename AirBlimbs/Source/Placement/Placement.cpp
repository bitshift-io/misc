#include "StdAfx.h"
#include "Placement.h"

REFLECT_ENUM_BEGIN(RotationAxis)
ENUM(RA_X)
ENUM(RA_Y)
ENUM(RA_Z)
REFLECT_ENUM_END(RotationAxis)

REGISTER_CLASS_P(Placement, Entity)
REGISTER_CLASS_P(PlacementConstraint, Entity)
REGISTER_CLASS_P(Mount, Entity)


PlacementConstraint::PlacementConstraint() :
	mNodeName(0),
	mNode(0),
	mDirectorEntityName(0),
	mSpeed(90),
	mCurRotation(0.f),
	mMaxRotation(-1.f),
	mMinRotation(-1.f),
	mLerpSpeed(0.f)
{
}

PlacementConstraint::~PlacementConstraint()
{
	free((void*)mNodeName);
	free((void*)mDirectorEntityName);
}

void PlacementConstraint::Init(Entity* parent)
{
	Parent::Init(parent);

	mDirectorEntity = mParent->GetEntity(mDirectorEntityName);
	mNode = mParent->GetEntity<SceneComponent>()->mScene->GetNodeByName(mNodeName);
	if (!mNode)
	{
		Log::Print("[PlacementConstraint::Init] Failed to find node '%s'\n", mNodeName);
	}
}

void PlacementConstraint::Update()
{
	Parent::Update();

	Actor* actor = mDirectorEntity->GetEntity<Actor>();
	if (!actor)
		return;

	Director* director = actor->GetDirector();

	Matrix4 local = mNode->GetLocalTransform();
	Matrix4 matRotation(MI_Identity);

	float direction = director->GetControlState(mControlType);
	if (direction != 0.f)
	{
		float rotation = mSpeed * direction * gGame.mGameUpdate.GetDeltaTime();

		// apply rotation limiting
		float newRotation = mCurRotation + Math::DtoR(rotation);
		if ((mMinRotation == -1.f || newRotation > Math::DtoR(mMinRotation)) 
			&& (mMaxRotation == -1.f || newRotation < Math::DtoR(mMaxRotation)))
		{
			mCurRotation = newRotation;
			
			switch (mRotationAxis)
			{
			case RA_X: 
				matRotation.RotateX(Math::DtoR(rotation));
				break;

			case RA_Y:
				matRotation.RotateY(Math::DtoR(rotation));
				break;

			case RA_Z:
				matRotation.RotateZ(Math::DtoR(rotation));
				break;
			}

			local = matRotation * local;
			mNode->SetLocalTransform(local);
		}
	}
	else if (mLerpSpeed > 0.f)
	{
		// lerp to origonal pose
		float rotation = mLerpSpeed * gGame.mGameUpdate.GetDeltaTime();
		if (mCurRotation > 0.01f)
		{
			mCurRotation -= Math::DtoR(rotation);
			rotation = -rotation;
		}
		else if (mCurRotation < -0.01f)
		{
			mCurRotation += Math::DtoR(rotation);
		}
		else
		{
			mCurRotation = 0.f;
			rotation = 0.f;
		}

		if (rotation != 0.f)
		{
			switch (mRotationAxis)
			{
			case RA_X: 
				matRotation.RotateX(Math::DtoR(rotation));
				break;

			case RA_Y:
				matRotation.RotateY(Math::DtoR(rotation));
				break;

			case RA_Z:
				matRotation.RotateZ(Math::DtoR(rotation));
				break;
			}
			
			local = matRotation * local;
			mNode->SetLocalTransform(local);
		}
	}
}

Mount::Mount() :
	mUseNodeName(0),
	mMountNodeName(0),
	mRadius(-1.f),
	mUseNode(0),
	mMountNode(0)
{
}

Mount::~Mount()
{
	free((void*)mUseNodeName);
	free((void*)mMountNodeName);
}

void Mount::Init(Entity* parent)
{
	Parent::Init(parent);

	if (mUseNodeName)
		mUseNode = static_cast<Placement*>(parent)->GetSceneComponent()->mScene->GetNodeByName(mUseNodeName);

	if (mMountNodeName)
		mMountNode = static_cast<Placement*>(parent)->GetSceneComponent()->mScene->GetNodeByName(mMountNodeName);
}

Mount::MountType Mount::GetMountType()
{
	if (mMountNode)
		return MT_Mount;

	return MT_Use;
}

Actor* Mount::GetMountedActor()
{
	return GetEntity<Actor>();
}

bool Mount::MountActor(Actor* actor)
{
	if (GetMountedActor())
		return false;

	InsertEntity(actor);
	return true;
}

void Mount::DismountActor(Actor* actor)
{
	RemoveEntity(actor);
}

bool Mount::GetMount(const Sphere& test)
{
	Sphere useSphere;
	Shape* shape = static_cast<Shape*>(mUseNode->GetObject());
	if (shape->mType == ST_Sphere && mRadius < 0.f)
	{
		useSphere = shape->GetSphere(mUseNode);
	}
	else
	{
		useSphere.position = mUseNode->GetWorldTransform().GetTranslation();
		useSphere.radius = mRadius;
	}

	return useSphere.Intersect(test) >= 0.f;
}

SceneNode* Mount::GetMountNode()
{
	return mMountNode;
}





Placement::Placement() :
	mAttachNodeName(0),
	mTransform(MI_Identity)
{
}

Placement::~Placement()
{
	free((void*)mAttachNodeName);
}

void Placement::Init(Entity* owner)
{
	Parent::Init(owner);

	SceneComponent* sceneComponent = GetSceneComponent();
	if (sceneComponent)
	{
		sceneComponent->mScene->SetTransform(mTransform);
		
		if (mParent && mParent->GetClassDesc() == ::GetClassDesc<Placement>())
		{
			Placement* parentPlacement = (Placement*)mParent;
			SceneComponent* parentSceneComponent = parentPlacement->GetSceneComponent();
			if (mAttachNodeName)
				mAttachNode = parentSceneComponent->mScene->GetNodeByName(mAttachNodeName);
			else
				mAttachNode = parentSceneComponent->mScene->GetRootNode();

			mAttachNode->InsertChild(sceneComponent->mScene->GetRootNode());
		}
	}
}

SceneComponent*	Placement::GetSceneComponent()
{
	return GetEntity<SceneComponent>();
}
/*
void Placement::NotifyObservableChanged(const ObserverEvent* event)
{
	if (event->observable == mParent)
		Update();
}
*/
void Placement::Update()
{
	Parent::Update();
	/*
	Director* director = mEntity->GetComponent<DirectorComponent>()->mDirector;
	if (!director)
		return;

	vector<PlacementConstraint*>::iterator it;
	for (it = mConstraint.begin(); it != mConstraint.end(); ++it)
	{
		PlacementConstraint* constraint = *it;

		float state = 0.f;
		vector<ControlType>::iterator controlIt;
		for (controlIt = constraint->controlType.begin(); controlIt != constraint->controlType.end(); ++controlIt)
			state += director->GetControlState(*controlIt);

		Matrix4 local = mNode->GetLocalTransform();
		Matrix4 matRotation(MI_Identity);

		if (direction != 0.f)
		{
			float rotation = mSpeed * state * gGame.mGameUpdate.GetDeltaTime();

			// apply rotation limiting
			float newRotation = mCurRotation + Math::DtoR(rotation);
			if ((mMinRotation == -1.f || newRotation > Math::DtoR(mMinRotation)) 
				&& (mMaxRotation == -1.f || newRotation < Math::DtoR(mMaxRotation)))
			{
				mCurRotation = newRotation;
				
				switch (mRotationAxis)
				{
				case RA_X: 
					matRotation.RotateX(Math::DtoR(rotation));
					break;

				case RA_Y:
					matRotation.RotateY(Math::DtoR(rotation));
					break;

				case RA_Z:
					matRotation.RotateZ(Math::DtoR(rotation));
					break;
				}

				local = matRotation * local;
				mNode->SetLocalTransform(local);
			}
		}
		else if (mLerpSpeed > 0.f)
		{
			// lerp to origonal pose
			float rotation = mLerpSpeed * gGame.mGameUpdate.GetDeltaTime();
			if (mCurRotation > 0.01f)
			{
				mCurRotation -= Math::DtoR(rotation);
				rotation = -rotation;
			}
			else if (mCurRotation < -0.01f)
			{
				mCurRotation += Math::DtoR(rotation);
			}
			else
			{
				mCurRotation = 0.f;
				rotation = 0.f;
			}

			if (rotation != 0.f)
			{
				switch (mRotationAxis)
				{
				case RA_X: 
					matRotation.RotateX(Math::DtoR(rotation));
					break;

				case RA_Y:
					matRotation.RotateY(Math::DtoR(rotation));
					break;

				case RA_Z:
					matRotation.RotateZ(Math::DtoR(rotation));
					break;
				}
				
				local = matRotation * local;
				constraint->node->SetLocalTransform(local);
			}
		}
	}*/
}

Mount* Placement::GetMount(const Sphere& test)
{
	Mount* mount = GetEntity<Mount>();
	while (mount)
	{
		if (mount->GetMount(test))
			return mount;

		mount = GetEntity<Mount>(mount);
	}

	Placement* placement = GetEntity<Placement>();
	while (placement)
	{
		if ((mount = placement->GetMount(test)))
			return mount;

		placement = GetEntity<Placement>(placement);
	}

	return 0;
}