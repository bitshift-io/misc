#include "StdAfx.h"
#include "WeaponMount.h"

REFLECT_ENUM_BEGIN(WeaponMountFlags)
ENUM(WMF_Shoot)
ENUM(WMF_Reload)
ENUM(WMF_Aim)
REFLECT_ENUM_END(WeaponMountFlags)

REGISTER_CLASS_P(WeaponMount, Mount)

WeaponMount::WeaponMount() :
	mWeaponName(0)
{
}

WeaponMount::~WeaponMount()
{
	free((void*)mWeaponName);
}

void WeaponMount::Init(Entity* parent)
{
	Parent::Init(parent);

	mWeapon = static_cast<Weapon*>(mParent->GetEntity(mWeaponName));
	//mWeapon = static_cast<Weapon*>(GetRootEntity()->GetEntity(mWeaponName));
}

void WeaponMount::Update()
{
	Parent::Update();
	UseInternal(GetEntity<Actor>(), true);	
}

void WeaponMount::Use(Actor* actor)
{
	UseInternal(actor, false);
}

void WeaponMount::UseInternal(Actor* actor, bool calledFromUpdate)
{
	if (!actor)
		return;

	Director* director = actor->GetDirector();
	if (!director)
		return;

	if ((mFlags & WMF_Shoot) && director->WasPressed(CT_FirePrimary) > 0.5f)
	{
		mWeapon->Fire();
	}

	if (mFlags & WMF_Aim)
	{
		int nnothing = 0;
	}

	if (mFlags & WMF_Reload)
	{
		if (!calledFromUpdate ||
			(calledFromUpdate && director->WasPressed(CT_Reload) > 0.5f))
		{
			mWeapon->Reload();
		}
	}
}
