#ifndef _WEAPONMOUNT_H_
#define _WEAPONMOUNT_H_

class Weapon;

enum WeaponMountFlags
{
	WMF_Shoot	= 1 << 0,
	WMF_Reload	= 1 << 1,
	WMF_Aim		= 1 << 2,
};

REFLECT_ENUM(WeaponMountFlags);

class WeaponMount : public Mount
{
	typedef Mount Parent;

public:

	REFLECT_CLASS_V
	(
		WeaponMount,
		(
			VAR_ENUM(WeaponMountFlags, mFlags),
			VAR_PTR(mWeaponName)
		)
	)

	WeaponMount();
	~WeaponMount();

	virtual void			Init(Entity* parent);
	virtual void			Update();
	virtual void			Use(Actor* actor);

protected:

	virtual void			UseInternal(Actor* actor, bool calledFromUpdate);

	WeaponMountFlags		mFlags;
	Weapon*					mWeapon;
	const char*				mWeaponName;
};

#endif