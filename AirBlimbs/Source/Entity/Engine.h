#ifndef _ENGINE_H_
#define _ENGINE_H_

class EngineConstraint : public Entity
{
	typedef Entity Parent;

public:

	REFLECT_CLASS_V
	(
		EngineConstraint,
		(
			VAR_PTR(mNodeName),
			VAR(mDefaultForce),
			VAR(mMaxForce),
			VAR(mMinForce),
			VAR(mWorldSpace),
			VAR_PTR(mDirectorEntityName),
			VAR_ENUM(ControlType, mControlType)
		)
	)

	EngineConstraint();
	~EngineConstraint();

	virtual void			Init(Entity* parent);
	virtual void			Update();

	const char*				mNodeName;
	SceneNode*				mNode;

	Vector4					mDefaultForce;
	Vector4					mMaxForce;
	Vector4					mMinForce;
	Vector4					mCurrentForce;
	bool					mWorldSpace;

	ControlType				mControlType;

	const char*				mDirectorEntityName;
	Entity*					mDirectorEntity;
};

class WingConstraint : public Entity
{
	typedef Entity Parent;

public:

	REFLECT_CLASS_V
	(
		WingConstraint,
		(
			VAR_PTR(mLeftNodeName),
			VAR_PTR(mRightNodeName),
			VAR(mDefaultForce),
			VAR(mMaxForce),
			VAR(mMinForce),
			VAR(mWorldSpace),
			VAR(mForwardToLiftMultiplier),
			VAR_PTR(mDirectorEntityName),
			VAR_ENUM(ControlType, mControlType)
		)
	)

	WingConstraint();
	~WingConstraint();

	virtual void			Init(Entity* parent);
	virtual void			Update();

	const char*				mLeftNodeName;
	SceneNode*				mLeftNode;

	const char*				mRightNodeName;
	SceneNode*				mRightNode;

	float					mForwardToLiftMultiplier;
	Vector4					mDefaultForce;
	Vector4					mMaxForce;
	Vector4					mMinForce;
	Vector4					mCurrentLeftForce;
	Vector4					mCurrentRightForce;
	bool					mWorldSpace;

	ControlType				mControlType;

	const char*				mDirectorEntityName;
	Entity*					mDirectorEntity;
};

class BaloonEngine : public Entity
{
	typedef Entity Parent;

public:

	REFLECT_CLASS_V
	(
		BaloonEngine,
		(
			VAR_PTR(mNodeName),
			VAR(mDefaultForce),
			VAR(mMaxForce),
			VAR(mMinForce),
			VAR(mWorldSpace),
			VAR_PTR(mDirectorEntityName),
			VAR_ENUM(ControlType, mControlType)
		)
	)

	BaloonEngine();
	~BaloonEngine();

	virtual void			Init(Entity* parent);
	virtual void			Update();

	const char*				mNodeName;
	SceneNode*				mNode;

	Vector4					mDefaultForce;
	Vector4					mMaxForce;
	Vector4					mMinForce;
	Vector4					mCurrentForce;
	bool					mWorldSpace;

	ControlType				mControlType;

	const char*				mDirectorEntityName;
	Entity*					mDirectorEntity;
};

#endif