#ifndef _RENDERCOMPONENT_H_
#define _RENDERCOMPONENT_H_

//
// some renderable component
//
class RenderComponent : public Entity
{
public:

	REFLECT_CLASS
	(
		RenderComponent
	)

	typedef Entity Parent;
};

//
// the entity has a scene
//
class SceneComponent : public RenderComponent
{
public:

	REFLECT_CLASS_VM
	(
		SceneComponent, 
		(
			VAR_PTR(mSceneName),
			VAR_PTR(mRenderLayer)
		),
		(
			FUNC_VOID(Init),
			FUNC_VOID(SetTransform),
			FUNC(GetTransform)
		)
	)

	typedef RenderComponent Parent;

	SceneComponent();
	~SceneComponent();

	virtual Entity*		Clone();
	virtual void		Init(Entity* owner);

	virtual void		SetTransform(const Matrix4& transform);
	virtual Matrix4		GetTransform();

	int					mRenderLayer;
	const char*			mSceneName;
	Scene*				mScene;
};

#endif