#include "StdAfx.h"
#include "Spawn.h"

REGISTER_CLASS_P(SpawnPoint, Entity)

SpawnPoint::SpawnPoint() :
	mEnabled(true),
	mTeamName(0),
	mTransform(MI_Identity)
{
}

SpawnPoint::~SpawnPoint()
{
	free((void*)mTeamName);
}