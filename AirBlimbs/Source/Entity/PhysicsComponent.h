#ifndef _PHYSICSCOMPONENT_H_
#define _PHYSICSCOMPONENT_H_

//#include "Template/Observer.h"

class PhysicsComponent;
/*
class CollisionReportEvent : public ObserverEvent
{
public:

	REFLECT_CLASS
	(
		CollisionReportEvent
	)

	enum CollisionReportType
	{
		CRT_Raycast,
		CRT_Sweep,
	};

	CollisionReportEvent(PhysicsComponent* component, const SweepHit& hit) :
		ObserverEvent((Observable*)component),
		sweepHit(hit),
		type(CRT_Sweep)
	{
	}

	CollisionReportEvent(PhysicsComponent* component, const RaycastHit& hit) :
		ObserverEvent((Observable*)component),
		raycastHit(hit),
		type(CRT_Sweep)
	{
	}

	CollisionReportType type;
	RaycastHit	raycastHit;
	SweepHit	sweepHit;
};
*/
class ContactReportEvent : public ObserverEvent
{
public:

	REFLECT_CLASS
	(
		ContactReportEvent
	)

	enum ContactReportType
	{
		CRT_ContactNotify,
		CRT_ActorContact,
	};

	ContactReportEvent()
	{
	}

	ContactReportEvent(PhysicsComponent* component, PhysicsBody* _body, PhysicsBody* _body2, ContactInfoIterator& _contactIt, ContactFlags _event) :
		ObserverEvent((Observable*)component),
		body(_body),
		body2(_body2),
		contactIt(&_contactIt),
		event(_event),
		type(CRT_ContactNotify)
	{
	}

	ContactReportEvent(PhysicsComponent* component, PhysicsBody* _body, PhysicsActorController* _controller, ContactInfoIterator& _contactIt) :
		ObserverEvent((Observable*)component),
		body(_body),
		controller(_controller),
		contactIt(&_contactIt),
		type(CRT_ActorContact)
	{
	}

	ContactReportType type;

	PhysicsBody* body;
	PhysicsBody* body2;
	ContactInfoIterator* contactIt;
	ContactFlags event;
	PhysicsActorController* controller;
};

class PhysicsComponent : public Entity, public ContactReport
{
	typedef Entity Parent;

public:

	REFLECT_CLASS
	(
		PhysicsComponent
	)

    virtual void OnContactNotify(PhysicsBody* body, PhysicsBody* body2, ContactInfoIterator& contactIt, ContactFlags event);
	virtual void OnActorContact(PhysicsBody* body, PhysicsActorController* controller, ContactInfoIterator& contactIt);
};

class ActorPhysics : public PhysicsComponent
{
	typedef PhysicsComponent Parent;

public:

	REFLECT_CLASS_V
	(
		ActorPhysics,
		(
			VAR(mRadius),
			VAR(mHeight)
		)
	)

	virtual void Init(Entity* parent);
	virtual void Deinit();

	PhysicsActorController* operator*()
	{
		return mBody;
	}

	PhysicsActorController* operator->()
	{
		return mBody;
	}

	PhysicsActorController*	mBody;
	float					mRadius;
	float					mHeight;
};

// FUTURE NOTE:
// for contact reports we are going to need components to subscribe to this class to be notified of the contact
// so 1 class doesnt simply steal the contact report
class BodyPhysics : public PhysicsComponent
{
	typedef PhysicsComponent Parent;

public:

	typedef vector<PhysicsBodyNode>::iterator Iterator;

	REFLECT_CLASS_V
	(
		BodyPhysics,
		(
			VAR_PTR(mSceneEntity),
			VAR_PTR(mSceneName)
		)
	)

	BodyPhysics();
	~BodyPhysics();

	virtual void Init(Entity* parent);
	virtual void Deinit();
	virtual void Resolve();
	virtual void Update();

	const char*				mSceneEntity; // this could be done with a Entity* typedesc
	const char*				mSceneName;

	vector<PhysicsBodyNode>	mBodyNode;
};

#endif
