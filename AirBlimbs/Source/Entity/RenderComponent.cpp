#include "StdAfx.h"
#include "RenderComponent.h"

REGISTER_CLASS_P(RenderComponent, Entity)
REGISTER_CLASS_P(SceneComponent, RenderComponent)

SceneComponent::SceneComponent() :
	mScene(0),
	mRenderLayer(RT_Default),
	mSceneName(0)
{
}

SceneComponent::~SceneComponent()
{
	if (mSceneName)
	{
		free((void*)mSceneName);
		mSceneName = 0;
	}

	if (mScene)
	{
		//gGame.mWorld->mRenderMgr->RemoveScene(mScene);
		gGame.mRenderFactory->ReleaseScene(&mScene);
	}
}


void SceneComponent::Init(Entity* owner)
{
	Parent::Init(owner);

	mScene = gGame.mRenderFactory->LoadScene(&ObjectLoadParams(gGame.mDevice, mSceneName));
	if (mScene)
		gGame.mWorld->mRenderMgr->InsertScene(mScene, mRenderLayer);
	else
		Log::Error("[SceneComponent::Init] Failed to load scene '%s'\n", mSceneName);
}

Entity* SceneComponent::Clone()
{
	return Parent::Clone();
}

void SceneComponent::SetTransform(const Matrix4& transform)
{
	if (mScene)
		mScene->SetTransform(transform);
}

Matrix4 SceneComponent::GetTransform()
{
	if (!mScene)
		return Matrix4(MI_Identity);

	return mScene->GetTransform();
}