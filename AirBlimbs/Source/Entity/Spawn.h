#ifndef _SPAWN_H_
#define _SPAWN_H_

class SpawnPoint : public Entity
{
public:

	REFLECT_CLASS_V
	(
		SpawnPoint,
		(
			VAR(mEnabled),
			VAR_PTR(mTeamName),
			VAR(mTransform)
		)
	)

	SpawnPoint();
	~SpawnPoint();

	bool				mEnabled;
	const char*			mTeamName;
	Matrix4				mTransform;
};

#endif
