#include "StdAfx.h"
#include "Engine.h"

REGISTER_CLASS_P(EngineConstraint, Entity)
REGISTER_CLASS_P(WingConstraint, Entity)
REGISTER_CLASS_P(BaloonEngine, Entity)

EngineConstraint::EngineConstraint() :
	mNodeName(0),
	mNode(0),
	mDirectorEntityName(0),
	mDirectorEntity(0)
{
}

EngineConstraint::~EngineConstraint()
{
	free((void*)mNodeName);
	free((void*)mDirectorEntityName);
}

void EngineConstraint::Init(Entity* parent)
{
	Parent::Init(parent);

	mDirectorEntity = mParent->GetEntity(mDirectorEntityName);
	mNode = mParent->GetEntity<SceneComponent>()->mScene->GetNodeByName(mNodeName);
	if (!mNode)
	{
		Log::Print("[EngineConstraint::Init] Failed to find node '%s'\n", mNodeName);
	}
}

void EngineConstraint::Update()
{
	Parent::Update();

	Actor* actor = mDirectorEntity->GetEntity<Actor>();
	BodyPhysics* bodyPhysics = GetRootEntity()->GetEntity<BodyPhysics>();
	PhysicsBody* mBody = bodyPhysics->mBodyNode[0].body;

	mCurrentForce = mDefaultForce;

	if (actor)
	{
		Director* director = actor->GetDirector();
		float direction = director->GetControlState(mControlType);
		

		if (direction > 0.5f)
			mCurrentForce = mMaxForce;

		if (direction < -0.5f)
			mCurrentForce = mMinForce;
	}

	Vector4 forceDir = mCurrentForce;
	if (!mWorldSpace)
	{
		Matrix4 localTransform = mNode->GetLocalTransform();
		localTransform.TransformRotation3(forceDir, &forceDir);
	}

	if (mNode)
		mBody->ApplyLinearForce(forceDir, mNode->GetWorldTransform().GetTranslation(), mWorldSpace ? PhysicsBody::World : PhysicsBody::Local);
	else
		mBody->ApplyLinearForce(forceDir, mWorldSpace ? PhysicsBody::World : PhysicsBody::Local);
}



WingConstraint::WingConstraint() :
	mLeftNodeName(0),
	mLeftNode(0),
	mRightNodeName(0),
	mRightNode(0),
	mDirectorEntityName(0),
	mDirectorEntity(0),
	mForwardToLiftMultiplier(1.f)
{
}

WingConstraint::~WingConstraint()
{
	free((void*)mLeftNodeName);
	free((void*)mRightNodeName);
	free((void*)mDirectorEntityName);
}

void WingConstraint::Init(Entity* parent)
{
	Parent::Init(parent);

	mDirectorEntity = mParent->GetEntity(mDirectorEntityName);
	mLeftNode = mParent->GetEntity<SceneComponent>()->mScene->GetNodeByName(mLeftNodeName);
	if (!mLeftNode)
	{
		Log::Print("[WingConstraint::Init] Failed to find node '%s'\n", mLeftNodeName);
	}
	mRightNode = mParent->GetEntity<SceneComponent>()->mScene->GetNodeByName(mRightNodeName);
	if (!mRightNode)
	{
		Log::Print("[WingConstraint::Init] Failed to find node '%s'\n", mRightNode);
	}
}

void WingConstraint::Update()
{
	Parent::Update();

	Actor* actor = mDirectorEntity->GetEntity<Actor>();
	BodyPhysics* bodyPhysics = GetRootEntity()->GetEntity<BodyPhysics>();
	PhysicsBody* mBody = bodyPhysics->mBodyNode[0].body;

	Matrix4 transform = mBody->GetTransform();
	Vector4 velocity = mBody->GetLinearVelocity();
	Vector4 forwards = transform.GetZAxis();
	float forwardVelocity = forwards.ProjectMag3(velocity);

	//if (velocity.Mag3() > 10.f)
	//{
	//	int nothing = 0;
	//}

	if (forwardVelocity > 10.f || forwardVelocity < -10.f)
	{
		int ntohig = 0;
	}

	float liftVelocity = forwardVelocity * mForwardToLiftMultiplier;
	Vector4 liftVec(0.f, 0.f, liftVelocity);

	mCurrentLeftForce = mDefaultForce + liftVec;
	mCurrentRightForce = mDefaultForce + liftVec;

	if (actor)
	{
		Director* director = actor->GetDirector();
		float direction = director->GetControlState(mControlType);
		
		if (direction > 0.5f)
		{
			mCurrentLeftForce = mMaxForce + liftVec;
			mCurrentRightForce = mMinForce + liftVec;
		}

		if (direction < -0.5f)
		{
			mCurrentRightForce = mMaxForce + liftVec;
			mCurrentLeftForce = mMinForce + liftVec;
		}
	}

	Vector4 leftForceDir = mCurrentLeftForce;
	Vector4 rightForceDir = mCurrentRightForce;
	if (!mWorldSpace)
	{
		Matrix4 localTransform = mLeftNode->GetLocalTransform();
		localTransform.TransformRotation3(leftForceDir, &leftForceDir);

		localTransform = mRightNode->GetLocalTransform();
		localTransform.TransformRotation3(rightForceDir, &rightForceDir);
	}

	if (mLeftNode && mRightNode)
	{
		mBody->ApplyLinearForce(leftForceDir, mLeftNode->GetWorldTransform().GetTranslation(), mWorldSpace ? PhysicsBody::World : PhysicsBody::Local);
		mBody->ApplyLinearForce(rightForceDir, mRightNode->GetWorldTransform().GetTranslation(), mWorldSpace ? PhysicsBody::World : PhysicsBody::Local);
	}
	else
	{
		mBody->ApplyLinearForce(leftForceDir, mWorldSpace ? PhysicsBody::World : PhysicsBody::Local);
		mBody->ApplyLinearForce(rightForceDir, mWorldSpace ? PhysicsBody::World : PhysicsBody::Local);
	}
}





BaloonEngine::BaloonEngine() :
	mNodeName(0),
	mNode(0),
	mDirectorEntityName(0),
	mDirectorEntity(0)
{
}

BaloonEngine::~BaloonEngine()
{
	free((void*)mNodeName);
	free((void*)mDirectorEntityName);
}

void BaloonEngine::Init(Entity* parent)
{
	Parent::Init(parent);

	mDirectorEntity = mParent->GetEntity(mDirectorEntityName);
	mNode = mParent->GetEntity<SceneComponent>()->mScene->GetNodeByName(mNodeName);
	if (!mNode)
	{
		Log::Print("[EngineConstraint::Init] Failed to find node '%s'\n", mNodeName);
	}
}

void BaloonEngine::Update()
{
	Parent::Update();

	Actor* actor = mDirectorEntity->GetEntity<Actor>();
	BodyPhysics* bodyPhysics = GetRootEntity()->GetEntity<BodyPhysics>();
	PhysicsBody* mBody = bodyPhysics->mBodyNode[0].body;
	mBody->SetKinematic(true); //doin resolve

	mCurrentForce = mDefaultForce;

	float rotation = 0.f;
	if (actor)
	{
		Director* director = actor->GetDirector();
		float direction = director->GetControlState(mControlType);

		if (direction > 0.5f)
			mCurrentForce = mMaxForce;

		if (direction < -0.5f)
			mCurrentForce = mMinForce;

		rotation = director->GetControlState(CT_Strafe) * gGame.mGameUpdate.GetDeltaTime() * Math::DtoR(10.f);
	}

	Vector4 forceDir = mCurrentForce;
	if (!mWorldSpace)
	{
		Matrix4 localTransform = mNode->GetWorldTransform();
		localTransform.TransformRotation3(forceDir, &forceDir);
	}

	Matrix4 transform = mBody->GetTransform();

	Matrix4 rotationTransform(MI_Identity);
	rotationTransform.RotateY(rotation);
	transform = transform.Multiply3(rotationTransform);
	//mNode->SetLocalTransform(localTransform);

	transform.SetTranslation(transform.GetTranslation() + forceDir * gGame.mGameUpdate.GetDeltaTime());
	mBody->SetTransform(transform);
/*
	if (mNode)
		mBody->ApplyLinearForce(forceDir, mNode->GetWorldTransform().GetTranslation(), mWorldSpace ? PhysicsBody::World : PhysicsBody::Local);
	else
		mBody->ApplyLinearForce(forceDir, mWorldSpace ? PhysicsBody::World : PhysicsBody::Local);*/
}