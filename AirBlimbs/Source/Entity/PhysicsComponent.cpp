#include "StdAfx.h"
#include "PhysicsComponent.h"

REGISTER_CLASS_P(ContactReportEvent, ObserverEvent)
//REGISTER_CLASS_P(CollisionReportEvent, ObserverEvent)

REGISTER_CLASS_P(PhysicsComponent, Entity)
REGISTER_CLASS_P(ActorPhysics, PhysicsComponent)
REGISTER_CLASS_P(BodyPhysics, PhysicsComponent)


void PhysicsComponent::OnContactNotify(PhysicsBody* body, PhysicsBody* body2, ContactInfoIterator& contactIt, ContactFlags event)
{
	ContactReportEvent contactEvent(this, body, body2, contactIt, event);
	NotifyObserver(&contactEvent);
}

void PhysicsComponent::OnActorContact(PhysicsBody* body, PhysicsActorController* controller, ContactInfoIterator& contactIt)
{
	ContactReportEvent contactEvent(this, body, controller, contactIt);
	NotifyObserver(&contactEvent);
}

void ActorPhysics::Init(Entity* parent)
{
	mBody = gGame.mPhysicsFactory->CreatePhsicsActorController();
	PhysicsActorControllerConstructor constructor;
	constructor.mHeight = mHeight;
	constructor.mRadius = mRadius;

	mBody->Init(gGame.mPhysicsFactory, gGame.mWorld->mPhysicsScene, constructor);
	mBody->SetCollisionGroup(6);
	mBody->SetContactReport(this);
	Parent::Init(parent);
}

void ActorPhysics::Deinit()
{
	Parent::Deinit();
}

BodyPhysics::BodyPhysics() :
	mSceneEntity(0),
	mSceneName(0)
{
}

BodyPhysics::~BodyPhysics()
{
	free((void*)mSceneEntity);
	free((void*)mSceneName);
}

void BodyPhysics::Init(Entity* parent)
{
	Parent::Init(parent);
}

void BodyPhysics::Resolve()
{
	Parent::Resolve();

	Scene* scene = 0;
	if (mSceneEntity)
	{
		SceneComponent* sceneComponent = static_cast<SceneComponent*>(GetRootEntity()->GetEntity(mSceneEntity));
		PhysicsSceneParser parser;
		parser.Init(gGame.mPhysicsFactory, gGame.mWorld->mPhysicsScene, sceneComponent->mScene->GetRootNode());
		
		Iterator it;
		for (it = parser.mBodyNode.begin(); it != parser.mBodyNode.end(); ++it)
		{
			mBodyNode.push_back(*it);
			it->body->SetContactReport(this);
		}
	}
}

void BodyPhysics::Deinit()
{
	Parent::Deinit();
}

void BodyPhysics::Update()
{
	Parent::Update();

	Iterator it;
	for (it = mBodyNode.begin(); it != mBodyNode.end(); ++it)
	{
		// set the parents transform, as physics is grouped with the visual
		// trimesh (which is static) takes its transform from the visual... 
		// this way it follows the convex hull which drives the visual,which drives any static physics
		if (it->body->IsDynamic())
		{
			//SceneNode* parent = it->node->GetParent();
			//SceneNode* node = parent ? parent : it->node;
			//node->SetWorldTransform(it->body->GetTransform());

			it->node->SetWorldTransform(it->body->GetTransform());
		}
		else //if(it->body->IsKinematic())
		{
			it->body->SetTransform(it->node->GetWorldTransform());
		}
	}
}