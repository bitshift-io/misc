#ifndef _PLAYBACKSTREAM_H_
#define _PLAYBACKSTREAM_H_

class Playback;

class PlaybackStream
{
public:

	PlaybackStream(Playback* playback);

	void	Write(void* data, unsigned int size);
	void	Read(void* data, unsigned int size);

	void	Save(File& file);
	void	Load(File& file);

	Playback*	GetPlayback()						{ return mPlayback; }

protected:

	unsigned int	mCursor;
	vector<char>	mData;
	Playback*		mPlayback;
};

template <class T>
PlaybackStream& operator &(PlaybackStream& stream, T& data)
{
	switch (stream.GetPlayback()->GetPlaybackType())
	{
	case PB_Record:
		stream.Write(&data, sizeof(T));
		break;

	case PB_Play:
		stream.Read(&data, sizeof(T));
		break;
	}

    return stream;
}

#endif
