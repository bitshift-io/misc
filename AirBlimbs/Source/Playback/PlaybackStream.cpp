#include "StdAfx.h"
#include "PlaybackStream.h"

PlaybackStream::PlaybackStream(Playback* playback) :
	mPlayback(playback),
	mCursor(0)
{
}

void PlaybackStream::Write(void* data, unsigned int size)
{
	unsigned int curSize = mData.size();
	mData.resize(curSize + size);
	memcpy(&mData[curSize], data, size);
}

void PlaybackStream::Read(void* data, unsigned int size)
{
	if (mCursor + size > mData.size())
		return;

	memcpy(data, &mData[mCursor], size);
	mCursor += size;
}

void PlaybackStream::Save(File& file)
{
	unsigned int size = mData.size();
	file.Write(&size, sizeof(unsigned int));
	file.Write(&mData[0], size);
}

void PlaybackStream::Load(File& file)
{
	unsigned int size = 0;
	file.Read(&size, sizeof(unsigned int));
	mData.resize(size);
	file.Read(&mData[0], size);
}