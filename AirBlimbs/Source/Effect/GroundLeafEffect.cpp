#include "StdAfx.h"
#include "GroundLeafEffect.h"
/*
CLASS_BEGIN(GroundLeafEffect)
	PARENT(Entity)
CLASS_END()

GroundLeafEffect::GroundLeafEffect() :
	mSceneName("leaf"),
	mMinOffset(0.5f),
	mMaxOffset(1.0f),
	mMovementAmount(0.1f, 0.1f, 0.f),
	mMovementSpeed(10.f, 1.f, 10.f)
{
}

void GroundLeafEffect::Init(Entity* entity)
{
	Parent::Init(entity);
	mParent->InsertObserver(this);
	SceneComponent* sceneComponent = mEntity->GetComponent<SceneComponent>();
	if (!sceneComponent)
		return;

	InitSceneNode(sceneComponent->mScene->GetRootNode());

	// insert scenes to be rendered
	vector<Scene*>::iterator sceneIt;
	for (sceneIt = mScene.begin(); sceneIt != mScene.end(); ++sceneIt)
	{
		gGame.mWorld->mRenderMgr->InsertScene(*sceneIt, RT_Default);
	}
}

void GroundLeafEffect::InitSceneNode(SceneNode* node)
{
	SceneObject* object = node->GetObject();
	if (object)
	{
		switch (object->GetType())
		{
		case Mesh::Type:
			{
				Matrix4 world = node->GetWorldTransform();

				Mesh* mesh = static_cast<Mesh*>(object);
				Geometry* geometry = mesh->GetMeshMat(0).geometry;

				VertexFrame& frame = geometry->GetVertexFrame(0);

				RenderBufferLock posLock;
				frame.mPosition->Lock(posLock, RBLF_ReadOnly);

				// FMNOTE: TODO - use normals to put leaves only above terrain
				// OR calculate a face normal

				for (int i = 0; i < posLock.count; )
				{
					Matrix4 transform(MI_Identity);
					transform.SetTranslation((world * posLock.vector4Data[i]) + Vector4(0.f, Math::Rand(mMinOffset, mMaxOffset), 0.f));
					Scene* scene = gGame.mRenderFactory->LoadScene(&ObjectLoadParams(gGame.mDevice, mSceneName));
					scene->SetTransform(transform);
					mScene.push_back(scene);

					i += 1; //Math::Rand(5, 10);
				}

				frame.mPosition->Unlock(posLock);
			}
			break;
		}
	}

	SceneNode::Iterator childIt;
	for (childIt = node->Begin(); childIt != node->End(); ++childIt)
	{
		InitSceneNode(*childIt);
	}
}

void GroundLeafEffect::Deinit()
{

}

void GroundLeafEffect::NotifyObservableChanged(const ObserverEvent* event)
{
	if (event->observable == mEntity)
		Update();
}

void GroundLeafEffect::Update()
{
	// update position and rotations
	float i = 0.f;
	vector<Scene*>::iterator sceneIt;
	for (sceneIt = mScene.begin(); sceneIt != mScene.end(); ++sceneIt, i += 34.4f)
	{
		Scene* scene = *sceneIt;
		Matrix4 transform = scene->GetTransform();
		Vector4 pos = transform.GetTranslation();

		pos.x += mMovementAmount.x * sinf(Time::GetSeconds() * i * mMovementSpeed.x);
		pos.y += mMovementAmount.y * sinf(Time::GetSeconds() * i * mMovementSpeed.y);
		pos.z += mMovementAmount.z * sinf(Time::GetSeconds() * i * mMovementSpeed.z);

		transform.SetTranslation(pos);
		scene->SetTransform(transform);
	}
}*/