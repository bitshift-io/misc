#include "StdAfx.h"
#include "World.h"

void CreateEntityCallback(void* instance, const ScriptClass* scriptClass, const ClassDesc* classDesc)
{
	if (classDesc->InheritsFrom(GetClassDesc<Entity>()))
	{
		Entity* entity = static_cast<Entity*>(instance);
		entity->SetName(scriptClass->GetName().c_str());
	}
}

void World::Init()
{
	mEntityMgr = new EntityMgr;
	mEntityMgr->Init(this);

	mRenderMgr = new RenderMgr;
	mRenderMgr->Init(this);

	mPhysicsScene = gGame.mPhysicsFactory->CreatePhysicsScene();
	if (mPhysicsScene->Init())
	{
		// this should be moved to metadata
		//
		// group:
		// 1 - static player interaction
		// 2 - dynamic pushable by player
		// 3 - dynamic nonpushable player interaction
		// 4 - dynamic (convex) no player interaction
		// 5 - dynamic (trimesh) player interaction no interaction with group 1
		// 6 - players
		mPhysicsScene->SetCollisionGroupFlags(2, CGF_Pushable);
		mPhysicsScene->EnableCollisionBetweenGroup(6, 4, false);
		mPhysicsScene->EnableCollisionBetweenGroup(5, 1, false);
		mPhysicsScene->EnableCollisionBetweenGroup(4, 5, false);
		mPhysicsScene->EnableCollisionBetweenGroup(4, 3, false);
		mPhysicsScene->EnableCollisionBetweenGroup(4, 2, false);
	}

	mCameraMgr = new CameraMgr;
	mCameraMgr->Init(this);
}

void World::Deinit()
{
	Unload();

	mPhysicsScene->Deinit();
	gGame.mPhysicsFactory->ReleasePhysicsScene(&mPhysicsScene);

	//mRenderMgr->Deinit();
	delete mRenderMgr;

	//mEntityMgr->Deinit();
	delete mEntityMgr;

	//mCameraMgr->Deinit();
	delete mCameraMgr;
}

unsigned long PhysicsTask(void* data, Thread* thread)
{
	World* world = (World*)data;
	world->mPhysicsScene->Update(gGame.mGameUpdate.GetDeltaTime());
	return 0;
}

void World::Update()
{
	gThreadPool.Wait();
	gThreadPool.AddTasksToAdd();	

	// we should really do this at load time, remove the task from the que when we dont need updating
	{
		Task* task = gThreadPool.CreateTask(TF_DeleteWhenDone);
		task->SetTask(&PhysicsTask, this);
		gThreadPool.AddTask(task);
	}

	// sync all tasks up until this point
	{
		gThreadPool.Wait();

		// this doesnt work so well :-/
		//Task* task = gThreadPool.CreateTask(TF_DeleteWhenDone);
		//task->SetTask(&WaitTask, this);
		//gThreadPool.AddTask(task);
	}

	//mPhysicsScene->Update(gGame.mGameUpdate.GetDeltaTime());
	mEntityMgr->UpdateEntity();
	mCameraMgr->Update();
	mRenderMgr->Update();
}

void World::Render()
{
	mRenderMgr->Render();
}

bool World::Load(const char* file)
{
	list<void*> entityList;
	ReflectScript script;
	if (!script.Open(file, &entityList, &CreateEntityCallback))
		return false;

	list<void*>::iterator entityIt;
	for (entityIt = entityList.begin(); entityIt != entityList.end(); ++entityIt)
	{
		Entity* entity = reinterpret_cast<Entity*>(*entityIt);
		mEntityMgr->InsertEntity(entity);
	}

	mEntityMgr->InitEntity();
	return true;
}

void World::Unload()
{
	mEntityMgr->DeinitEntity();

	EntityMgr::EntityIterator it;
	while (it != mEntityMgr->EntityEnd())
	{
		it = mEntityMgr->EntityRemove(it);
		delete (*it); // should each entity clean up its components? or should it be cleaned up by the ReflectScript?
	}
}
