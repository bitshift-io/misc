#ifndef _ENTITYMGR_H_
#define _ENTITYMGR_H_

//
// handles entity tracking, broadcasting finding of etc...
//
class EntityMgr
{
public:

	REFLECT_CLASS_M
	(
		EntityMgr,
		(
			FUNC_VOID(InsertEntity),
			FUNC_VOID(RemoveEntity),
			FUNC(GetEntityByName)
		)
	)

	typedef list<Entity*>::iterator		EntityIterator;
	typedef list<Component*>::iterator	ComponentIterator;

	void				Init(World* world);

	void				InitEntity();
	void				DeinitEntity();
	void				ResolveEntity();
	void				UpdateEntity();

	void				InsertEntity(Entity* entity);
	void				RemoveEntity(Entity* entity);

	EntityIterator		EntityBegin()								{ return mEntity.begin(); }
	EntityIterator		EntityEnd()									{ return mEntity.end(); }
	EntityIterator		EntityRemove(EntityIterator it)				{ return mEntity.erase(it); }

	EntityIterator		EntityBegin(unsigned int type)				{ return mEntityTable[type].begin(); }
	EntityIterator		EntityEnd(unsigned int type)				{ return mEntityTable[type].end(); }

	template <class T>
	EntityIterator		EntityBegin()								{ return EntityBegin(::GetClassDesc<T>()->uniqueId); }
	template <class T>
	EntityIterator		EntityEnd()									{ return EntityEnd(::GetClassDesc<T>()->uniqueId); }

	Entity*				GetEntityByName(const char* name);

	map<unsigned int, list<Entity*> >	mEntityTable;

	list<Entity*>				mEntity;
	World*						mWorld;
};

#endif