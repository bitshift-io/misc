#ifndef _RENDERMGR_H_
#define _RENDERMGR_H_

//
// flags describing how the scene should be rendered
// eg. should it be included in water reflections
// should it cast/receive shadows etc...
//
enum RenderType
{
	RT_Default = 0x1 << 0,
};

//
// handles rendering of the world
//
class RenderMgr
{
public:

	void		Init(World* world);
	void		InsertScene(Scene* scene, int renderType = RT_Default);

	void		Render();
	void		Update();

	Renderer*	mRenderer;
	Camera*		mCamera;
	World*		mWorld;
	bool		mDebugPhysics;
};

#endif
