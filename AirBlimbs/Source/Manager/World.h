#ifndef _WORLD_H_
#define _WORLD_H_

class EntityMgr;
class RenderMgr;
class CameraMgr;

void CreateEntityCallback(void* instance, const ScriptClass* scriptClass, const ClassDesc* classDesc);

//
// instance of a world
//
class World
{
public:

	void			Init();
	void			Deinit();
	void			Update();
	void			Render();

	bool			Load(const char* file);
	void			Unload();

	EntityMgr*		mEntityMgr;
	RenderMgr*		mRenderMgr;
	CameraMgr*		mCameraMgr;
	PhysicsScene*	mPhysicsScene;
};

#endif