#ifndef _SCRIPTMGR_H_
#define _SCRIPTMGR_H_

class ScriptMgr
{
public:

	struct Object
	{
		void*		object;
		ClassDesc*	classDesc;
	};

	bool	Execute(const char* command);

	template <class T>
	void Register(T* object);

protected:

	bool	ExecuteInternal(void* result, const char* command);

	vector<Object>	mObject;
};

template <class T>
void ScriptMgr::Register(T* object)
{
	Object obj;
	obj.object = object;
	obj.classDesc = object->GetClassDesc();
	mObject.push_back(obj);
}

#endif