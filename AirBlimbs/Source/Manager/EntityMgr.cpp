#include "StdAfx.h"
#include "EntityMgr.h"

#define NO_MT

REGISTER_CLASS(EntityMgr)

void EntityMgr::Init(World* world)
{
	mWorld = world;

	gGame.mScriptMgr->Register<EntityMgr>(this);
}

void EntityMgr::InitEntity()
{
	EntityIterator it;
	for (it = EntityBegin(); it != EntityEnd(); ++it)
	{
		(*it)->Init(0);
	}

	ResolveEntity();
}

void EntityMgr::DeinitEntity()
{
	EntityIterator it;
	for (it = EntityBegin(); it != EntityEnd(); ++it)
	{
		(*it)->Deinit();
	}
}

void EntityMgr::ResolveEntity()
{
	EntityIterator it;
	for (it = EntityBegin(); it != EntityEnd(); ++it)
	{
		(*it)->Resolve();
	}
}

unsigned long UpdateEntityTask(void* data, Thread* thread)
{
	Entity* entity = (Entity*)data;
	entity->Update();
	return 0;
}

void EntityMgr::UpdateEntity()
{
	// we should really do this at load time, remove the task from the que when we dont need updating
	EntityIterator it;
	for (it = EntityBegin(); it != EntityEnd(); ++it)
	{
#ifdef NO_MT
		(*it)->Update();
#else
		Task* task = gThreadPool.CreateTask(TF_DeleteWhenDone);
		task->SetTask(&UpdateEntityTask, *it);
		gThreadPool.AddTask(task);
#endif
	}
}

void EntityMgr::InsertEntity(Entity* entity)
{
	mEntity.push_back(entity);

	unsigned int type = entity->GetClassDesc()->uniqueId;
	map<unsigned int, list<Entity*> >::iterator entitytList = mEntityTable.find(type);

	if (entitytList == mEntityTable.end())
	{
		mEntityTable[type] = list<Entity*>();
		entitytList = mEntityTable.find(type);
	}

	entitytList->second.push_back(entity);
}

void EntityMgr::RemoveEntity(Entity* entity)
{
	mEntity.remove(entity);

	map<unsigned int, list<Entity*> >::iterator entitytList = mEntityTable.find(entity->GetClassDesc()->uniqueId);
	if (entitytList != mEntityTable.end())
		entitytList->second.remove(entity);
}

Entity* EntityMgr::GetEntityByName(const char* name)
{
	if (!name)
		return 0;

	EntityIterator it;
	for (it = EntityBegin(); it != EntityEnd(); ++it)
	{
		if (strcmpi((*it)->GetName(), name) == 0)
			return (*it);
	}

	return 0;
}