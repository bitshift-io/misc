#include "StdAfx.h"
#include "ScriptMgr.h"

bool ScriptMgr::Execute(const char* command)
{
	vector<string> commands = ScriptFile::Tokenize(command, ";", true);
	vector<string>::iterator it;
	for (it = commands.begin(); it != commands.end(); ++it)
	{
		void* result = 0;
		if (!ExecuteInternal(result, it->c_str()))
			return false;
	}

	return true;
}

bool ScriptMgr::ExecuteInternal(void* result, const char* command)
{
	char receiverName[256];
	char methodName[256];
	char parameters[2048] = {'\0'};

	// get the receiver name
	int len = strlen(command);
	int i = 0;
	for (; i < len; ++i)
	{
		if (command[i] == '.')
		{
			memcpy(&receiverName, command, i);
			receiverName[i] = '\0';
			break;
		}
	}
	if (i == len)
		return false;

	// get the method name
	++i;
	int j = i;
	for (; j < len; ++j)
	{
		if (command[j] == '(')
			break;
	}

	memcpy(&methodName, command + i, j - i);
	methodName[j - i] = '\0';

	// get the parameters
	vector<string> params;
	if (j < len)
	{
		++j;
		memcpy(&parameters, command + j, len - 1);
		parameters[(len - 1) - j] = '\0';
		params = ScriptFile::Tokenize(parameters, ",", true);
	}

	// invoke message
	vector<Object>::iterator objIt;
	for (objIt = mObject.begin(); objIt != mObject.end(); ++objIt)
	{
		if (strcmpi(objIt->classDesc->name, receiverName) == 0)
		{
			MethodDesc* methodDesc = objIt->classDesc->GetMethodDesc(methodName);
			if (!methodDesc)
				return false;

			// TODO: need to parse parameters based on thier type
			int p = 0;
			void* parameters[5];
			vector<string>::iterator paramIt;
			for (paramIt = params.begin(); paramIt != params.end(); ++paramIt, ++p)
			{
				parameters[p] = (void*)(paramIt->c_str());
			}

			Entity* temp = 0;
			methodDesc->Invoke(&temp, objIt->object, parameters);
			return true;
		}
	}

	return true;
}