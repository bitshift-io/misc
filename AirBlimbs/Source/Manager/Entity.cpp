#include "StdAfx.h"
#include "Entity.h"

REGISTER_CLASS(Entity)

Entity::Entity() :
	mName(0),
	mParent(0)
{
}

void Entity::Init(Entity* parent)
{
	mParent = parent;

	// remove null entitys, this occurs if bad data has been setup
	vector<Entity*>::iterator it;
	for (it = mEntity.begin(); it != mEntity.end(); )
	{
		if (*it)
		{
			(*it)->Init(this);
			++it;
		}
		else
		{
			it = mEntity.erase(it);
		}
	}
}

void Entity::Deinit()
{
	SetName(0);

	// delete child entitys
}

Entity* Entity::Clone()
{
	ClassDesc* desc = GetClassDesc();
	Entity* clone = (Entity*)gReflectSystem.Clone(this, desc);

	// clone children also
	vector<Entity*>::iterator it;
	for (it = mEntity.begin(); it != mEntity.end(); ++it)
		clone->mEntity.push_back((*it)->Clone());

	return clone;
}

void Entity::Resolve()
{
	vector<Entity*>::iterator it;
	for (it = mEntity.begin(); it != mEntity.end(); ++it)
	{
		(*it)->Resolve();
	}
}

void Entity::SetName(const char* name)
{
	if (mName)
	{
		free((void*)mName);
		mName = 0;
	}

	if (!name)
		return;

	mName = strdup(name);
}

void Entity::InsertEntity(Entity* entity)
{
	mEntity.push_back(entity);
}

void Entity::RemoveEntity(Entity* entity)
{
	vector<Entity*>::iterator it;
	for (it = mEntity.begin(); it != mEntity.end(); ++it)
	{
		if (*it == entity)
		{
			mEntity.erase(it);
			return;
		}
	}
}

Entity* Entity::GetEntity(ClassDesc* classDesc, Entity* previous)
{
	if (classDesc == 0)
		return 0;

	vector<Entity*>::iterator it;
	it = mEntity.begin();

	if (previous)
	{
		for (; it != mEntity.end(); ++it)
		{
			if (*it == previous)
			{
				++it;
				break;
			}
		}
	}	

	for (; it != mEntity.end(); ++it)
	{
		Entity* entity = (*it);

		ClassDesc* desc = entity->GetClassDesc();
		if (desc->InheritsFrom(classDesc))
			return (*it);
	}

	return 0;
}

void Entity::Update()
{
	// only update our own children!
	vector<Entity*>::iterator it;
	for (it = mEntity.begin(); it != mEntity.end(); ++it)
	{
		if ((*it)->mParent == this)
			(*it)->Update();
	}

	//NotifyObserver(0); //reinterpret_cast<void*>(&Entity::Update));
}

Entity* Entity::GetRootEntity()
{
	if (mParent)
		return mParent->GetRootEntity();

	return this;
}

Entity* Entity::GetEntity(const char* name)
{
	if (mName && strcmpi(name, mName) == 0)
		return this;

	vector<Entity*>::iterator it;
	for (it = mEntity.begin(); it != mEntity.end(); ++it)
	{
		Entity* entity = (*it)->GetEntity(name);
		if (entity)
			return entity;
	}

	return 0;
}