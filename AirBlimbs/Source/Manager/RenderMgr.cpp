#include "StdAfx.h"
#include "RenderMgr.h"

void RenderMgr::Init(World* world)
{
	mDebugPhysics = false;
	if (gGame.mCmdLine.GetToken("debugphysics"))
		mDebugPhysics = true;

	mCamera = (Camera*)gGame.mRenderFactory->Create(Camera::Type);
	mCamera->LookAt(Vector4(0.f, 0.f, 0.f), Vector4(0.f, 0.f, 1.f));
	mCamera->SetPerspective(80.f);

	mWorld = world;
	mRenderer = gGame.mRenderFactory->CreateRenderer();
	mRenderer->SetCamera(mCamera);
}

void RenderMgr::InsertScene(Scene* scene, int renderType)
{
	if (!scene)
		return;

	mRenderer->InsertScene(scene);
}

//#include <GL/gl.h>
//#include "Render/OGL/Cg/cg.h"
//#include "Render/OGL/OGLExtension.h"

void RenderMgr::Render()
{
	gGame.mDevice->Begin();

	if (!mDebugPhysics)
		mRenderer->Render(gGame.mDevice);

	if (mDebugPhysics)
	{
		//glDisable(GL_FRAGMENT_PROGRAM_ARB);
		//glDisable(GL_VERTEX_PROGRAM_ARB);
		//cgResetPassState(0);
		//cgSetPassState(0);
		gGame.mDevice->SetMatrix(Matrix4(MI_Identity), MM_Model);
		mCamera->SetAspectRatio(gGame.mDevice->GetWindow()->GetAspectRatio());
		mCamera->Bind(gGame.mDevice);
		mWorld->mPhysicsScene->Draw(gGame.mDevice);
	}

	gGame.mDevice->End();
}

void RenderMgr::Update()
{
	if (gGame.mInput->WasPressed(KEY_p))
		mDebugPhysics = !mDebugPhysics;
}