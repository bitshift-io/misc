#ifndef _OBSERVER_H_
#define _OBSERVER_H_

//
// Implementation of Observer pattern
//

#include <vector>

using namespace std;

class Observable;

//
// events sent by observable to obsrvers for them to handle
//
class ObserverEvent
{
public:

	REFLECT_CLASS
	(
		ObserverEvent
	)

	ObserverEvent(Observable* observable = 0, int eventId = -1) :
		observable(observable),
		eventId(eventId)
	{
	}

	Observable*				observable;
	int						eventId;
};

//
// notified of changes to any thing we are observing
//
class Observer
{
public:

	virtual void	NotifyObservableChanged(ObserverEvent* event);
};

//
// can be observed by components
//
class Observable : public Observer
{
public:

	//
	// notify all observers we have changed
	// were are using the observer pattern as described
	// http://www.codeproject.com/cpp/observer.asp
	//
	// takes a void* to some event/event data
	//
	void			NotifyObserver(int eventId = -1);
	void			NotifyObserver(ObserverEvent* event);
	
	void			InsertObserver(Observer* observer);
	void			RemoveObserver(Observer* observer);

	vector<Observer*>	mObserver;
};

#endif