#include "StdAfx.h"
#include "Weapon.h"

REFLECT_ENUM_BEGIN(WeaponFlags)
	ENUM(WF_None)
	ENUM(WF_AutomaticFire)
	ENUM(WF_AutomaticReload)
REFLECT_ENUM_END(WeaponFlags)

REGISTER_CLASS_P(Weapon, Entity)

Weapon::Weapon() :
	mMuzzleNodeName(0),
	mTimeSinceFire(0.f),
	mActiveProjectile(0),
	mWeaponFlags(WF_None)
{
}

Weapon::~Weapon()
{
	free((void*)mMuzzleNodeName);
}

void Weapon::Init(Entity* parent)
{
	Parent::Init(parent);

	if (mMuzzleNodeName)
		mMuzzleNode = parent->GetEntity<SceneComponent>()->mScene->GetNodeByName(mMuzzleNodeName);

	// init the pool of projectiles
	{
		mProjectilePool.resize(mProjectile.size());
		vector<Projectile*>::iterator it;
		vector<Pool<Projectile> >::iterator poolIt = mProjectilePool.begin();
		for (it = mProjectile.begin(); it != mProjectile.end(); ++poolIt)
		{
			poolIt->Init((*it)->mPoolSize, false);

			vector<Projectile*>::iterator projIt;
			for (projIt = poolIt->mPool.begin(); projIt != poolIt->mPool.end(); ++projIt)
			{
				*projIt = (Projectile*)(*it)->Clone();
				mEntity.push_back(*projIt); // add projectiles to entity list for updating and resolving etc...
				(*projIt)->Init(this);
				(*projIt)->Resolve();
				//Projectile* p = *projIt;
				//p->Init(*it);
				
			}

			// free the temporary projectile loaded from metadata
			//(*it)->Deinit();
			delete *it;
			it = mProjectile.erase(it);
		}
	}
}

void Weapon::Deinit()
{
	// deinit projectile pool
	{
		vector<Pool<Projectile> >::iterator poolIt;
		for (poolIt = mProjectilePool.begin(); poolIt != mProjectilePool.end(); )
		{
			vector<Projectile*>::iterator it;
			for (it = poolIt->mPool.begin(); it != poolIt->mPool.end(); ++it)
			{
				(*it)->Deinit();
			}

			poolIt->Deinit();
			poolIt = mProjectilePool.erase(poolIt);
		}
	}

	Parent::Deinit();
}

void Weapon::Update()
{
	Parent::Update();
/*
	vector<Pool<Projectile> >::iterator poolIt;
	for (poolIt = mProjectilePool.begin(); poolIt != mProjectilePool.end(); ++poolIt)
	{
		vector<Projectile*>::iterator it;
		for (it = poolIt->mPool.begin(); it != poolIt->mPool.end(); ++it)
		{
			(*it)->Update();
		}
	}
*/
	mTimeSinceFire += gGame.mGameUpdate.GetDeltaTime();
}

bool Weapon::Fire()
{
	if (mTimeSinceFire < mFireDelay)
		return false;

	mTimeSinceFire = 0.f;

	Matrix4 transform;
	if (!mMuzzleNode)
		transform = mParent->GetEntity<SceneComponent>()->mScene->GetTransform();
	else
		transform = mMuzzleNode->GetWorldTransform();

	Pool<Projectile>& pool = mProjectilePool[mActiveProjectile];
	Projectile* proj = pool.Create();
	if (proj)
	{/*
		if (mFireSound)
		{
			mFireSound->SetPosition(transform.GetTranslation());
			mFireSound->Play();
		}
*/
		proj->Fire(&pool, 0, transform);
	}

	return true;
}

bool Weapon::Reload()
{
	return true;
}