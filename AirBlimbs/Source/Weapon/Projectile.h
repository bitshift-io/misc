#ifndef _PROJECTILE_H_
#define _PROJECTILE_H_

//
// A weapon fires a projectile
//
class Projectile : public Entity
{
	typedef Entity Parent;

public:

	REFLECT_CLASS_V
	(
		Projectile,
		(
			VAR(mPoolSize),
			VAR(mTimeToLive),
			VAR(mFaceCamera),
			VAR(mDamage),
			VAR(mMaxDeviation),
			VAR(mMuzzleVelocity)
		)
	)

	Projectile();

	virtual Entity*		Clone();
	virtual void		Init(Entity* parent);
	virtual void		Deinit();

	virtual void		Fire(Pool<Projectile>* pool, Actor* instigator, const Matrix4& transform);
	virtual void		ReturnToPool();

	virtual void		UpdateInternal(Matrix4& transform);

	virtual void		CauseDamage(Entity* entity);

//protected:

	float				mMaxDeviation;
	float				mMuzzleVelocity;
	float				mDamage;
	float				mTimeToLive;
	bool				mFaceCamera;
	Actor*				mInstigator; // who fired the weapon
	float				mLife;
	Pool<Projectile>*	mPool;
	int					mPoolSize;
};

class RaycastProjectile : public Projectile, public CollisionReport
{
	typedef Projectile Parent;

public:

	REFLECT_CLASS_V
	(
		RaycastProjectile,
		(
			VAR(mLength)
		)
	)	

	virtual void		Init(Entity* parent);

	virtual void		Fire(Pool<Projectile>* pool, Actor* instigator, const Matrix4& transform);
	virtual void		Update();

	virtual bool		OnRaycastHit(const RaycastHit& hit);

protected:

	float				mLength;
	Matrix4				mTransform;
};

class PhysicsBodyProjectile : public Projectile
{
	typedef Projectile Parent;

public:

	REFLECT_CLASS_V
	(
		RaycastProjectile,
		(
			VAR(mApplyGravity),
			VAR(mMass),
			VAR(mMuzzleAngularVelocity),
			VAR(mAngularDamping),
			VAR(mLinearDamping)
		)
	)

	PhysicsBodyProjectile() : mApplyGravity(true), mPhysicsEnableDelay(-1.0f), mLinearDamping(0.f), mAngularDamping(0.f)
	{
	}

	//virtual Projectile*	Clone();
	virtual void		Init(Entity* parent);
	virtual void		Deinit();
	virtual void		Resolve();

	virtual void		Fire(Pool<Projectile>* pool, Actor* instigator, const Matrix4& transform);
	virtual void		ReturnToPool();

	virtual void		Update();

	virtual void		NotifyObservableChanged(ObserverEvent* event);	
	virtual void		OnContactNotify(PhysicsBody* body, PhysicsBody* body2, ContactInfoIterator& contactIt, ContactFlags event);
	virtual void		OnActorContact(PhysicsBody* body, PhysicsActorController* controller, ContactInfoIterator& contactIt);

protected:

	float				mPhysicsEnableTimer;
	bool				mApplyGravity;
	float				mPhysicsEnableDelay;
	float				mMass;
	float				mMuzzleAngularVelocity;
	float				mAngularDamping;
	float				mLinearDamping;
	//PhysicsBody*		mPhysicsBody;
};

#endif
