#ifndef _WEAPON_H_
#define _WEAPON_H_

class Projectile;

enum WeaponFlags
{
	WF_None					= 0,
	WF_AutomaticFire		= 1 << 0,
	WF_AutomaticReload		= 1 << 1,
};

REFLECT_ENUM(WeaponFlags);

class Weapon : public Entity
{
	typedef Entity Parent;

public:

	REFLECT_CLASS_V
	(
		Weapon,
		(
			VAR(mFireDelay),
			VAR_PTR(mMuzzleNodeName),
			VAR_PTR_STL(mProjectile),
			VAR_ENUM(WeaponFlags, mWeaponFlags)
		)
	)

	Weapon();
	~Weapon();

	virtual void	Init(Entity* parent);
	virtual void	Deinit();
	bool			Fire();
	bool			Reload();
	virtual void	Update();

	SceneNode*		mMuzzleNode;
	const char*		mMuzzleNodeName;
	float			mFireDelay;
	float			mTimeSinceFire;
	int				mActiveProjectile;
	WeaponFlags		mWeaponFlags;

	vector<Projectile*>			mProjectile;
	vector<Pool<Projectile> >	mProjectilePool;
};

#endif