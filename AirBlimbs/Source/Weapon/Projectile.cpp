#include "StdAfx.h"
#include "Projectile.h"

REGISTER_CLASS_P(Projectile, Entity)
REGISTER_CLASS_P(RaycastProjectile, Projectile)
REGISTER_CLASS_P(PhysicsBodyProjectile, Projectile)

Projectile::Projectile() : 
	mFaceCamera(false),
	mPool(0),
	mPoolSize(0),
	mMuzzleVelocity(0.f)
{
}

Entity*	Projectile::Clone()
{
	return Parent::Clone();
}

void Projectile::Init(Entity* parent)
{/*
	mMaxDeviation = clone->mMaxDeviation;
	mMuzzleVelocity = clone->mMuzzleVelocity;
	mDamage = clone->mDamage;
	mTimeToLive = clone->mTimeToLive;
	mFaceCamera = clone->mFaceCamera;
	mSceneName = clone->mSceneName;
	*/

	Parent::Init(parent);

	if (GetEntity<SceneComponent>())
		GetEntity<SceneComponent>()->mScene->SetFlags(SNF_Hidden);
}

void Projectile::Deinit()
{
	Parent::Deinit();
}

void Projectile::Fire(Pool<Projectile>* pool, Actor* instigator, const Matrix4& transform)
{
	mInstigator = instigator;
	mPool = pool;
	mLife = 0.f;

	if (GetEntity<SceneComponent>())
		GetEntity<SceneComponent>()->mScene->ClearFlags(SNF_Hidden);
}

void Projectile::ReturnToPool()
{
	if (!mPool)
		return;

	if (GetEntity<SceneComponent>())
		GetEntity<SceneComponent>()->mScene->SetFlags(SNF_Hidden);

	mPool->ReturnToPool(this);
	mPool = 0;
}

void Projectile::UpdateInternal(Matrix4& transform)
{
	// check life and return to pool if needed
	mLife += gGame.mGameUpdate.GetDeltaTime();
	if (mLife >= mTimeToLive)
	{
		ReturnToPool();
	}

	if (mFaceCamera)
	{
		// x-axis to face camera
		const Matrix4& camWorld = gGame.mWorld->mRenderMgr->mCamera->GetWorld();
		Vector4 camPos = camWorld.GetTranslation();
		Vector4 pos = transform.GetTranslation();
		Vector4 toCam = (camPos - pos);
		toCam.Normalize();

		Vector4 up = toCam.Cross(transform.GetZAxis());
		up.Normalize();

		Vector4 right = -transform.GetZAxis().Cross(up);
		right.Normalize();

		transform.SetYAxis(up);
		transform.SetXAxis(right);
	}

	if (GetEntity<SceneComponent>())
		GetEntity<SceneComponent>()->mScene->SetTransform(transform);

	Parent::Update();
}

void Projectile::CauseDamage(Entity* entity)
{
	if (!entity)
		return;

}


// ==============================================================

void RaycastProjectile::Init(Entity* parent)
{
	Parent::Init(parent);

	//RaycastProjectile* localClone = static_cast<RaycastProjectile*>(clone);
	//mLength = localClone->mLength;
}

void RaycastProjectile::Fire(Pool<Projectile>* pool, Actor* instigator, const Matrix4& transform)
{
	mTransform = transform;
	Parent::Fire(pool, instigator, transform);
}

void RaycastProjectile::Update()
{
	if (!mPool)
		return;

	unsigned int group = (0x1 << 1) | (0x1 << 2) | (0x1 << 3) | (0x1 << 5) | (0x1 << 6);
	//RaycastReport raycast;
	int hitCount = /*raycast.*/Raycast(gGame.mWorld->mPhysicsScene, mTransform.GetTranslation(), mTransform.GetZAxis(), mLength, group);

	if (hitCount)
	{
		ReturnToPool();
		return;
	}

	mTransform.SetTranslation(mTransform.GetTranslation() + mTransform.GetZAxis() * mMuzzleVelocity * gGame.mGameUpdate.GetDeltaTime());
	Parent::UpdateInternal(mTransform);
}

bool RaycastProjectile::OnRaycastHit(const RaycastHit& hit)
{
	CauseDamage(static_cast<Entity*>(hit.body->GetUserData()));
	return false;
}

// ==============================================================



void PhysicsBodyProjectile::Init(Entity* parent)
{
	Parent::Init(parent);
/*
	PhysicsBodyProjectile* localClone = static_cast<PhysicsBodyProjectile*>(clone);
	mMass = localClone->mMass;
	mMuzzleAngularVelocity = localClone->mMuzzleAngularVelocity;
	mPhysicsEnableDelay = localClone->mPhysicsEnableDelay;
	mApplyGravity = localClone->mApplyGravity;
	mLinearDamping = localClone->mLinearDamping;
	mAngularDamping = localClone->mAngularDamping;
* /
	mPhysicsBody = gGame.mPhysicsFactory->CreatePhysicsBody();

	// we should have a special class to construct physics from a scene,
	// which is not an entity!
	// TODO - move hardcoding out
	PhysicsBodyConstructor constructor;
	constructor.AddSphere(0.3, 3);
	//constructor.AddBox(Vector4(1,1,1));
	constructor.SetDynamicBody(mMass, mLinearDamping, mAngularDamping);
	mPhysicsBody->Init(gGame.GetEnvironment()->mPhysicsScene, constructor);
	mPhysicsBody->Disable();
	mPhysicsBody->EnableGravity(mApplyGravity);
	mPhysicsBody->SetContactReport(this);
	*/
}

void PhysicsBodyProjectile::Deinit()
{
	// release physics body

	Parent::Deinit();
}

void PhysicsBodyProjectile::Resolve()
{
	Parent::Resolve();
	GetEntity<BodyPhysics>()->InsertObserver(this);
}

void PhysicsBodyProjectile::Fire(Pool<Projectile>* pool, Actor* instigator, const Matrix4& transform)
{
	PhysicsBody* b = GetEntity<BodyPhysics>()->mBodyNode[0].body;
	mPhysicsEnableTimer = mPhysicsEnableDelay;

	b->SetTransform(transform);
	b->SetLinearVelocity(transform.GetZAxis() * mMuzzleVelocity);
	//mPhysicsBody->SetTransform(transform);
	//mPhysicsBody->SetLinearVelocity(transform.GetZAxis() * mMuzzleVelocity);

	if (mPhysicsEnableDelay <= 0.0f)
		b->Enable();

	// transform this angular velocity to world space
	float angularVelocity = rand() % 2 == 0 ? mMuzzleAngularVelocity : -mMuzzleAngularVelocity;
	b->SetAngularVelocity(transform *(Vector4(0, 0, 1) * angularVelocity));
	//mPhysicsBody->SetAngularVelocity(transform.Transform3(Vector4(0, 0, 1) * angularVelocity));

	Parent::Fire(pool, instigator, transform);
}

void PhysicsBodyProjectile::ReturnToPool()
{
	if (!mPool)
		return;

	Parent::ReturnToPool();
	GetEntity<BodyPhysics>()->mBodyNode[0].body->Disable();
}

void PhysicsBodyProjectile::Update()
{
	PhysicsBody* b = GetEntity<BodyPhysics>()->mBodyNode[0].body;

	// check to see if physics needs to be enabled
	float prevPhysicsEnableTimer = mPhysicsEnableTimer;
	mPhysicsEnableTimer -= gGame.mGameUpdate.GetDeltaTime();
	if (mPhysicsEnableTimer <= 0.0f && prevPhysicsEnableTimer > 0.0f)
		b->Enable();

	Matrix4 transform = b->GetTransform();
	GetEntity<SceneComponent>()->mScene->SetTransform(transform);
	Parent::UpdateInternal(transform);
}

void PhysicsBodyProjectile::OnContactNotify(PhysicsBody* body, PhysicsBody* body2, ContactInfoIterator& contactIt, ContactFlags event)
{
	PhysicsBody* b = GetEntity<BodyPhysics>()->mBodyNode[0].body;
	if (body == b)
		CauseDamage(static_cast<Entity*>(body2->GetUserData()));
	else
		CauseDamage(static_cast<Entity*>(body->GetUserData()));
}

void PhysicsBodyProjectile::OnActorContact(PhysicsBody* body, PhysicsActorController* controller, ContactInfoIterator& contactIt)
{

}

void PhysicsBodyProjectile::NotifyObservableChanged(ObserverEvent* event)
{
	if (event->GetClassDesc() == ::GetClassDesc<ContactReportEvent>())
	{
		ContactReportEvent* contactEvent = (ContactReportEvent*)event;
		switch (contactEvent->type)
		{
		case ContactReportEvent::CRT_ContactNotify:
			OnContactNotify(contactEvent->body, contactEvent->body2, *contactEvent->contactIt, contactEvent->event);
			break;

		case ContactReportEvent::CRT_ActorContact:
			OnActorContact(contactEvent->body, contactEvent->controller, *contactEvent->contactIt);
			break;
		}
	}
}