#include "StdAfx.h"
#include "Director.h"

REFLECT_ENUM_BEGIN(ControlType)
ENUM(CT_LookX)
ENUM(CT_LookY)
ENUM(CT_Jump)
ENUM(CT_MoveForward)
ENUM(CT_MoveBackward)
ENUM(CT_Move)
ENUM(CT_StrafeLeft)
ENUM(CT_StrafeRight)
ENUM(CT_Strafe)
ENUM(CT_Use)
REFLECT_ENUM_END(ControlType)

REGISTER_CLASS(ControlState)
REGISTER_CLASS(InputControlState)
REGISTER_CLASS(Director)
//REGISTER_CLASS_P(DirectorComponent, Entity)

void Director::Init()
{
	//mDirectorComponent.mDirector = this;

	// set up for players, load from data
	// AI doesnt need input control map
	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = MOUSE_AxisX;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_LookX;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = MOUSE_AxisY;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_LookY;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = KEY_Space;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_Jump;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = KEY_w;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_MoveForward;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = KEY_s;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_MoveBackward;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = KEY_a;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_StrafeLeft;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = KEY_d;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_StrafeRight;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = KEY_e;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_Use;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = KEY_r;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_Reload;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = MOUSE_Left;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_FirePrimary;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = MOUSE_Right;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_FireSecondary;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	// special control states
	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = InputControl_Max;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_Move;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}

	{
		InputControlMap* m = new InputControlMap;
		m->stateModifier = 1.f;
		m->deadZone = 0.f;
		m->inputControl = InputControl_Max;
		m->pressed = false;

		InputControlState* s = new InputControlState;
		s->type = CT_Strafe;
		s->state = 0.f;
		s->controlMap.push_back(m);

		mControlState.push_back(s);
	}
}

void Director::Deinit()
{
	// clean up
}

ControlState* Director::GetControlStateInternal(ControlType controlType)
{
	vector<ControlState*>::iterator it;
	for (it = mControlState.begin(); it != mControlState.end(); ++it)
	{
		if ((*it)->type == controlType)
			return (*it);
	}

	return 0;
}

float Director::GetControlState(ControlType controlType)
{
	ControlState* controlState = GetControlStateInternal(controlType);
	if (!controlState)
		return 0.f;

	return controlState->state;
}

bool Director::WasPressed(ControlType controlType)
{
	ControlState* controlState = GetControlStateInternal(controlType);
	if (!controlState)
		return false;

	if (controlState->state <= 0.5f && controlState->lastState > 0.5f)
		return true;

	return false;
	/*
	const InputControlMap& controlMap = mInputControlMap[controlType];
	return 
	*/

}

void Director::InsertActor(Actor* actor)
{
	actor->InsertEntity(this); //&mDirectorComponent);
	mActor.push_back(actor);
}

void Director::RemoveActor(Actor* actor)
{
	actor->RemoveEntity(this); //&mDirectorComponent);
	mActor.remove(actor);
}

void Director::UpdateViewFromState()
{
	float x = GetControlState(CT_LookX);
	float y = GetControlState(CT_LookY);

	Quaternion rotY(Vector4(0.f, 1.f, 0.f), x);
	Quaternion rotX(Vector4(1.f, 0.f, 0.f), y);
	mView = (rotY * rotX) * Vector4(0.f, 0.f, 1.f);
}

const Vector4& Director::GetView()
{
	return mView;
}

Actor* Director::GetActor(int index)
{
	return mActor.front();
}

void Director::Update()
{
	// update input controls
	vector<ControlState*>::iterator controlStateIt;
	for (controlStateIt = mControlState.begin(); controlStateIt != mControlState.end(); ++controlStateIt)
	{
		ClassDesc* classDesc = ::GetClassDesc<InputControlState>();
		if ((*controlStateIt)->GetClassDesc() != classDesc)
			continue;

		InputControlState* controlState = static_cast<InputControlState*>(*controlStateIt);
		controlState->lastState = controlState->state;
		
		float resultState = 0.f;
		vector<InputControlMap*>::iterator controlMapIt;
		for (controlMapIt = controlState->controlMap.begin(); controlMapIt != controlState->controlMap.end(); ++controlMapIt)
		{
			InputControlMap* controlMap = *controlMapIt;
			float controlState = gGame.mInput->GetState(controlMap->inputControl);
			if (controlState < 0.f)
				controlState = Math::Min(controlState + controlMap->deadZone, 0.f);
			else
				controlState = Math::Max(controlState - controlMap->deadZone, 0.f);

			controlState *= controlMap->stateModifier;

			if (Math::Abs(controlState) > Math::Abs(resultState))
				resultState = controlState;
		}

		switch (controlState->type)
		{
		case CT_LookX:
			controlState->state += resultState;
			break;

		case CT_LookY:
			controlState->state = Math::Clamp(Math::DtoR(-85.f), controlState->state + resultState, Math::DtoR(85.f));
			break;

		default:
			controlState->state = resultState;
			break;
		}
	}

	// update special controls
	for (controlStateIt = mControlState.begin(); controlStateIt != mControlState.end(); ++controlStateIt)
	{
		InputControlState* controlState = static_cast<InputControlState*>(*controlStateIt);
		InputControlState* control[2];

		switch (controlState->type)
		{
		case CT_Strafe:
			control[0] = static_cast<InputControlState*>(GetControlStateInternal(CT_StrafeLeft));
			control[1] = static_cast<InputControlState*>(GetControlStateInternal(CT_StrafeRight));
			break;

		case CT_Move:
			control[0] = static_cast<InputControlState*>(GetControlStateInternal(CT_MoveForward));
			control[1] = static_cast<InputControlState*>(GetControlStateInternal(CT_MoveBackward));
			break;

		default:
			continue;
		}

		controlState->lastState = controlState->state;
		controlState->state = control[0]->state;
		controlState->state -= control[1]->state;
	}


	UpdateViewFromState(); //really need an update function
}