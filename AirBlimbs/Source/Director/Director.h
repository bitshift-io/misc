#ifndef _DIRECTOR_H_
#define _DIRECTOR_H_

class Actor;

#include "Manager/Entity.h"

enum ControlType
{
	CT_LookX,
	CT_LookY,
	CT_Jump,
	CT_MoveForward,
	CT_MoveBackward,
	CT_Move,
	CT_StrafeLeft,
	CT_StrafeRight,
	CT_Strafe,
	CT_Use,
	CT_Reload,
	CT_FirePrimary,
	CT_FireSecondary,

	CT_Max,
};

REFLECT_ENUM(ControlType);

struct ControlState
{
	REFLECT_CLASS
	(
		ControlState
	)

	ControlType		type;
	float			state;
	float			lastState;
};

//
// struct used to convert a ControlType to a InputType
//
struct InputControlMap
{
	InputControl	inputControl;
	float			stateModifier;
	float			deadZone;
	bool			pressed;			// set state to 1 for a single frame on release
};

struct InputControlState : public ControlState
{
	REFLECT_CLASS
	(
		InputControlState
	)

	vector<InputControlMap*>	controlMap;
};
/*
class Director;
//
// gets added to entitys when they are controlled by a director so other components can get access to the director
//
class DirectorComponent 
{
public:

	REFLECT_CLASS
	(
		DirectorComponent
	)

	Director* operator*()
	{
		return mDirector;
	}

	Director* operator->()
	{
		return mDirector;
	}

	Director* mDirector;
};
*/
//
// controls actors
//
class Director : public Entity
{
public:

	REFLECT_CLASS_VM
	(
		Director, 
		(
			VAR_PTR_STL(mControlState)
		),
		(
			FUNC_VOID(InsertActor),
			FUNC_VOID(RemoveActor),
			//FUNC(GetControlState),
			//FUNC(WasPressed),
			FUNC(GetActor)
		)
	)

	void					Init();
	void					Deinit();

	void					InsertActor(Actor* actor);
	void					RemoveActor(Actor* actor);

	float					GetControlState(ControlType controlType);
	bool					WasPressed(ControlType controlType);

	const Vector4&			GetView();

	Actor*					GetActor(int index = 0);

	void					Update();

protected:

	ControlState*			GetControlStateInternal(ControlType controlType);

	void					UpdateViewFromState();

	Vector4					mView;

	list<Actor*>			mActor; // list of entitys being controlled
	vector<ControlState*>	mControlState;

//	DirectorComponent		mDirectorComponent;
};

#endif