//-----------------------------------------------------------------------------
// Parameters
//-----------------------------------------------------------------------------

float4x4		worldViewProj			: WorldViewProjection;
float4x4		cameraWorld				: Camera;
float			overbright = 2.0;

Texture2D		diffuseTexture;
Texture2D		depthTexture;

SamplerState	samLinear
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};

SamplerState	samPoint
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Wrap;
    AddressV = Wrap;
};

//-----------------------------------------------------------------------------
// Structures
//-----------------------------------------------------------------------------

struct VS_INPUT
{
	float3 position 	: POSITION;
	float4 colour		: COLOR0;
	float2 uv			: TEXCOORD0;	
};

struct VS_OUTPUT
{
	float4 hPosition	: SV_POSITION;
	float4 colour		: COLOR0;
	float2 uv			: TEXCOORD0;
	float4 uvScreen		: TEXCOORD1;
};

struct POut
{
	float4 color	: SV_Target;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------

VS_OUTPUT VS(VS_INPUT IN)
{
	VS_OUTPUT OUT;
	float4 hPos = mul(float4(IN.position + cameraWorld[3], 1.0), worldViewProj);
	OUT.hPosition = hPos;
	OUT.uvScreen = hPos;
	OUT.colour = IN.colour;
	OUT.uv = IN.uv;
    return OUT;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

POut PS(VS_OUTPUT IN)
{
	POut OUT;

	// only write skybox where depth is 1
	float2 screenTex = 0.5 * ((IN.uvScreen.xy / IN.uvScreen.w) + float2(1,1));
	screenTex.y = 1.0 - screenTex.y;

	float depthSample = depthTexture.Sample(samPoint, screenTex).r;
	clip(depthSample - 1.0);

	OUT.color = float4((IN.colour * diffuseTexture.Sample(samLinear, IN.uv)).rgb * overbright, 1.0);
	return OUT;
}


//--------------------------------------------------------------------------------------
technique10 Render
{
    pass P0
    {
        SetVertexShader(CompileShader(vs_4_0, VS()));
        SetGeometryShader(NULL);
        SetPixelShader(CompileShader(ps_4_0, PS()));
    }
}
