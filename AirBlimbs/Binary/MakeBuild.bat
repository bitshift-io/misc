@ECHO OFF

ECHO Building packages
DogFighters -package game.pkg mat,sce,fx,cg,anm,swf,ogg,wav,mdd,fnt,tga

ECHO Removing current build
rd /s /q Build 

ECHO Creating required directory structure
md Build
cd Build
md Data
cd Data
md Cache
cd ..
cd ..

ECHO Copying build files
copy *.dll Build
copy *.exe Build
copy Data\*.pkg Build\Data
copy Data\Profile.mdd Build\Data
copy Data\Controls.mdd Build\Data

ECHO Removing temp files
del Data\*.pkg

ECHO Build complete
PAUSE