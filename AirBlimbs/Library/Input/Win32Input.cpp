#include "Win32Input.h"
#include "Render/Window.h"
#include "Render/Win32/Win32Window.h"
#include "File/Log.h"
#include "math.h"

#include <wbemidl.h>
#include <oleauto.h>
//#include <wmsstd.h>

//#pragma comment(lib, "dinput.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "XInput.lib")

#define SAFE_RELEASE(p) { if(p) { (p)->Release(); (p)=NULL; } }

struct LookupTable
{
	int inputControlIdx;
	int keyCode;
} lookupTable[]	=
{
	KEY_Space, DIK_SPACE,

	// key pad
	KEY_KP0, DIK_NUMPAD0,
	KEY_KP1, DIK_NUMPAD1,
	KEY_KP2, DIK_NUMPAD2,
	KEY_KP3, DIK_NUMPAD3,
	KEY_KP4, DIK_NUMPAD4,
	KEY_KP5, DIK_NUMPAD5,
	KEY_KP6, DIK_NUMPAD6,
	KEY_KP7, DIK_NUMPAD7,
	KEY_KP8, DIK_NUMPAD8,
	KEY_KP9, DIK_NUMPAD9,

	KEY_0, DIK_0,
	KEY_1, DIK_1,
	KEY_2, DIK_2,
	KEY_3, DIK_3,
	KEY_4, DIK_4,
	KEY_5, DIK_5,
	KEY_6, DIK_6,
	KEY_7, DIK_7,
	KEY_8, DIK_8,
	KEY_9, DIK_9,

	KEY_a, DIK_A,
	KEY_b, DIK_B,
	KEY_c, DIK_C,
	KEY_d, DIK_D,
	KEY_e, DIK_E,
	KEY_f, DIK_F,
	KEY_g, DIK_G,
	KEY_h, DIK_H,
	KEY_i, DIK_I,
	KEY_j, DIK_J,
	KEY_k, DIK_K,
	KEY_l, DIK_L,
	KEY_m, DIK_M,
	KEY_n, DIK_N,
	KEY_o, DIK_O,
	KEY_p, DIK_P,
	KEY_q, DIK_Q,
	KEY_r, DIK_R,
	KEY_s, DIK_S,
	KEY_t, DIK_T,
	KEY_u, DIK_U,
	KEY_v, DIK_V,
	KEY_w, DIK_W,
	KEY_x, DIK_X,
	KEY_y, DIK_Y,
	KEY_z, DIK_Z,

	KEY_F1, DIK_F1,
	KEY_F2, DIK_F2,
	KEY_F3, DIK_F3,
	KEY_F4, DIK_F4,
	KEY_F5, DIK_F5,
	KEY_F6, DIK_F6,
	KEY_F7, DIK_F7,
	KEY_F8, DIK_F8,
	KEY_F9, DIK_F9,
	KEY_F10, DIK_F10,
	KEY_F11, DIK_F11,
	KEY_F12, DIK_F12,

	KEY_Esc, DIK_ESCAPE,
	KEY_Enter, DIK_RETURN,
	KEY_BackSpace, DIK_BACKSPACE,
	KEY_Tab, DIK_TAB,
	KEY_Pause, DIK_PAUSE,
	KEY_ScrollLock, DIK_SCROLL,
	KEY_PrintScreen, DIK_SYSRQ,
	KEY_Delete, DIK_DELETE,

	KEY_LShift, DIK_LSHIFT,
	KEY_RShift, DIK_RSHIFT,
	KEY_LControl, DIK_LCONTROL,
	KEY_RControl, DIK_RCONTROL,
	KEY_CapsLock, DIK_CAPSLOCK,
	KEY_LAlt, DIK_LALT,
	KEY_RAlt, DIK_RALT,

	KEY_Home, DIK_HOME,
	KEY_Left, DIK_LEFT,
	KEY_Up, DIK_UP,
	KEY_Down, DIK_DOWN,
	KEY_Right, DIK_RIGHT,
	KEY_PageUp, DIK_PRIOR,
	KEY_PageDown, DIK_NEXT,
	KEY_End, DIK_END,
	KEY_Begin, DIK_HOME,

	InputControl_KeyEnd, 0, // dont use this! its internal!

	// mouse
	MOUSE_Left, 0,
	MOUSE_Right, 1,
	MOUSE_Middle, 2,
	
	MOUSE_AxisX, 0,
	MOUSE_AxisY, 1,
	MOUSE_Wheel, 2,

	// joystick 0
	JOYSTICK0_AxisX, 0,
	JOYSTICK0_AxisY, 1,
	JOYSTICK0_AxisZ, 2,

	JOYSTICK0_Axis0, 0,
	JOYSTICK0_Axis1, 1,
	JOYSTICK0_Axis2, 2,
	JOYSTICK0_Axis3, 3,

	JOYSTICK0_Button1, 0,
	JOYSTICK0_Button2, 1,
	JOYSTICK0_Button3, 2,
	JOYSTICK0_Button4, 3,
	JOYSTICK0_Button5, 4,
	JOYSTICK0_Button6, 5,
	JOYSTICK0_Button7, 6,
	JOYSTICK0_Button8, 7,
	JOYSTICK0_Button9, 8,
	JOYSTICK0_Button10, 9,
	JOYSTICK0_Button11, 10,
	JOYSTICK0_Button12, 11,

	InputControl_Max, 0,
};

void Win32JoystickEffect::CreateEffect(LPDIRECTINPUTDEVICE8 pDevice)
{/*
	// create the effect and get the interface to it
	lpdijoy->CreateEffect(GUID_Square,  // standard GUID
                 &diEffect,      // where the data is
                 &lpdieffect,    // where to put interface pointer
                 NULL);          // no aggregation*/
}

void Win32JoystickEffect::Update()
{
	// emulate force feedback effect here
}

void Win32Joystick::ApplyForce(const Vector4& dir, float force)
{
	if (xboxIdx != -1)
	{
		float leftVal = dir.Dot3(Vector4(0.f, 0.f, 1.f));
		float rightVal = 1.f - leftVal;

		leftVal *= (force * 65535.f);
		rightVal *= (force * 65535.f);

		// Create a Vibraton State
		XINPUT_VIBRATION vibration;

		// Zeroise the Vibration
		ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));

		// Set the Vibration Values
		vibration.wLeftMotorSpeed = (int)leftVal;
		vibration.wRightMotorSpeed = (int)rightVal;

		// Vibrate the controller
		DWORD error = XInputSetState(xboxIdx, &vibration);
		int nothing = 0;
		//ERROR_DEVICE_NOT_CONNECTED
		//	ERROR_SUCCESS
	}
	else
	{
	}
}

void Win32Joystick::InsertEffect(JoystickEffect* _effect)
{
	Win32JoystickEffect* win32Effect = (Win32JoystickEffect*)_effect;
	win32Effect->CreateEffect(pDevice);
	effect.push_back(win32Effect);
}

int Win32Joystick::GetFlags()
{
	int flags = 0;

	if (caps.dwFlags & DIDC_FORCEFEEDBACK)
		flags |= JF_ForceFeedback;

	if (xboxIdx != -1)
		flags |= JF_XBox;
	
	return flags;
}

void Win32Joystick::Update()
{
	vector<Win32JoystickEffect*>::iterator it;
	for (it = effect.begin(); it != effect.end(); ++it)
	{
		(*it)->Update();
	}
}

inline float JoystickAxisToFloat(long axis, float deadZone, float max = 32767.f)
{
	float percent = float(axis - max) / max;
	float rescaledPercent;
	if (percent < 0.f)
		rescaledPercent = min(0.f, (percent + deadZone)) / (1.f - deadZone);
	else
		rescaledPercent = max(0.f, (percent - deadZone)) / (1.f - deadZone);

	return rescaledPercent;
}


Win32Input::Win32Input() : 
	mKeyboardDevice(0), 
	mDI(0), 
	mMouseDevice(0), 
	mMouseVisibility(true), 
	mWindow(0),
	mMouseSensitivity(0.01f)
{/*
	memset(&mKey, 0x0, 256);
	memset(&mKeyLock, 0x0, 256);
	memset(&mMouseButtonLock, 0x0, 8);
	memset(&mMouse, 0x0, sizeof(DIMOUSESTATE2));*/

	for (int i = 0; i < InputControl_Max; ++i)
	{
		mState[i].pressed = false;
		mState[i].state = 0.f;
	}
}

bool Win32Input::Init(Window* window, bool background)
{
#ifdef DEBUG
	// enable background while in debug mode
	//background = true;
#endif

	mWindow = static_cast<Win32Window*>(window);
	mBackground = background;

	// Set the cooperative level 
	DWORD flags = 0;
	if (window->IsFullScreen())
	{
		flags = DISCL_EXCLUSIVE | DISCL_FOREGROUND;
	}
	else
	{
		flags = DISCL_NONEXCLUSIVE;
		flags |= background ? DISCL_BACKGROUND : DISCL_FOREGROUND;
	}

	HRESULT hr; 
	hr = DirectInput8Create(mWindow->GetHInstance(), DIRECTINPUT_VERSION, 
			IID_IDirectInput8, (void**)&mDI, NULL); 
	if FAILED(hr) 
		return false; 

	// 
	// Keyboard
	//
	hr = mDI->CreateDevice(GUID_SysKeyboard, &mKeyboardDevice, NULL); 
	if FAILED(hr) 
		return false; 

	hr = mKeyboardDevice->SetDataFormat(&c_dfDIKeyboard); 
	if FAILED(hr) 
		return false; 

	hr = mKeyboardDevice->SetCooperativeLevel(mWindow->GetHandle(), flags); 
	if FAILED(hr) 
	{
		Log::Error("Keyboard SetCooperativeLevel Failed");
		return false; 
	}

	mKeyboardDevice->Acquire(); 

	//
	// Mouse
	// 
	hr = mDI->CreateDevice(GUID_SysMouse, &mMouseDevice, NULL); 
	if FAILED(hr)
		return false;

	hr = mMouseDevice->SetDataFormat(&c_dfDIMouse2); 
	if FAILED(hr) 
		return false;

	hr = mMouseDevice->SetCooperativeLevel(mWindow->GetHandle(), 
			flags); 
	if FAILED(hr) 
		return false; 

	mMouseDevice->Acquire(); 

	//
	// XBox controllers
	//
	for (int i = 0; i < 4; ++i)
	{
		EnumXBoxController(i);
	}

	//
	// Joysticks
	//
	mDI->EnumDevices(DI8DEVCLASS_GAMECTRL, Win32Input::EnumJoystickCallback,
		this, DIEDFL_ATTACHEDONLY);

	return true;
}

//-----------------------------------------------------------------------------
// Enum each PNP device using WMI and check each device ID to see if it contains 
// "IG_" (ex. "VID_045E&PID_028E&IG_00").  If it does, then it's an XInput device
// Unfortunately this information can not be found by just using DirectInput 
//-----------------------------------------------------------------------------
BOOL IsXInputDevice(const GUID* pGuidProductFromDirectInput)
{
	// http://msdn.microsoft.com/en-us/library/bb173051(VS.85).aspx
    IWbemLocator*           pIWbemLocator  = NULL;
    IEnumWbemClassObject*   pEnumDevices   = NULL;
    IWbemClassObject*       pDevices[20]   = {0};
    IWbemServices*          pIWbemServices = NULL;
    BSTR                    bstrNamespace  = NULL;
    BSTR                    bstrDeviceID   = NULL;
    BSTR                    bstrClassName  = NULL;
    DWORD                   uReturned      = 0;
    bool                    bIsXinputDevice= false;
    UINT                    iDevice        = 0;
    VARIANT                 var;
    HRESULT                 hr;

    // CoInit if needed
    hr = CoInitialize(NULL);
    bool bCleanupCOM = SUCCEEDED(hr);

    // Create WMI
    hr = CoCreateInstance( __uuidof(WbemLocator),
                           NULL,
                           CLSCTX_INPROC_SERVER,
                           __uuidof(IWbemLocator),
                           (LPVOID*) &pIWbemLocator);
    if( FAILED(hr) || pIWbemLocator == NULL )
        goto LCleanup;

    bstrNamespace = SysAllocString( L"\\\\.\\root\\cimv2" );if( bstrNamespace == NULL ) goto LCleanup;        
    bstrClassName = SysAllocString( L"Win32_PNPEntity" );   if( bstrClassName == NULL ) goto LCleanup;        
    bstrDeviceID  = SysAllocString( L"DeviceID" );          if( bstrDeviceID == NULL )  goto LCleanup;        
    
    // Connect to WMI 
    hr = pIWbemLocator->ConnectServer( bstrNamespace, NULL, NULL, 0L, 
                                       0L, NULL, NULL, &pIWbemServices );
    if( FAILED(hr) || pIWbemServices == NULL )
        goto LCleanup;

    // Switch security level to IMPERSONATE. 
    CoSetProxyBlanket( pIWbemServices, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL, 
                       RPC_C_AUTHN_LEVEL_CALL, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE );                    

    hr = pIWbemServices->CreateInstanceEnum( bstrClassName, 0, NULL, &pEnumDevices ); 
    if( FAILED(hr) || pEnumDevices == NULL )
        goto LCleanup;

    // Loop over all devices
    for( ;; )
    {
        // Get 20 at a time
        hr = pEnumDevices->Next( 10000, 20, pDevices, &uReturned );
        if( FAILED(hr) )
            goto LCleanup;
        if( uReturned == 0 )
            break;

        for( iDevice=0; iDevice<uReturned; iDevice++ )
        {
            // For each device, get its device ID
            hr = pDevices[iDevice]->Get( bstrDeviceID, 0L, &var, NULL, NULL );
            if( SUCCEEDED( hr ) && var.vt == VT_BSTR && var.bstrVal != NULL )
            {
                // Check if the device ID contains "IG_".  If it does, then it's an XInput device
				    // This information can not be found from DirectInput 
                if( wcsstr( var.bstrVal, L"IG_" ) )
                {
                    // If it does, then get the VID/PID from var.bstrVal
                    DWORD dwPid = 0, dwVid = 0;
                    WCHAR* strVid = wcsstr( var.bstrVal, L"VID_" );
                    if( strVid && swscanf( strVid, L"VID_%4X", &dwVid ) != 1 )
                        dwVid = 0;
                    WCHAR* strPid = wcsstr( var.bstrVal, L"PID_" );
                    if( strPid && swscanf( strPid, L"PID_%4X", &dwPid ) != 1 )
                        dwPid = 0;

                    // Compare the VID/PID to the DInput device
                    DWORD dwVidPid = MAKELONG( dwVid, dwPid );
                    if( dwVidPid == pGuidProductFromDirectInput->Data1 )
                    {
                        bIsXinputDevice = true;
                        goto LCleanup;
                    }
                }
            }   
            SAFE_RELEASE( pDevices[iDevice] );
        }
    }

LCleanup:
    if(bstrNamespace)
        SysFreeString(bstrNamespace);
    if(bstrDeviceID)
        SysFreeString(bstrDeviceID);
    if(bstrClassName)
        SysFreeString(bstrClassName);
    for( iDevice=0; iDevice<20; iDevice++ )
        SAFE_RELEASE( pDevices[iDevice] );
    SAFE_RELEASE( pEnumDevices );
    SAFE_RELEASE( pIWbemLocator );
    SAFE_RELEASE( pIWbemServices );

    if( bCleanupCOM )
        CoUninitialize();

    return bIsXinputDevice;
}

BOOL CALLBACK Win32Input::EnumJoystickCallback(const DIDEVICEINSTANCE* pdidInstance, VOID* pContext)
{
	// skip xbox controllers
	if (IsXInputDevice(&pdidInstance->guidProduct))
		return DIENUM_CONTINUE;

	Win32Input* pInput = (Win32Input*)pContext;
	HRESULT hr;

	Win32Joystick joystick;

	// Obtain an interface to the enumerated joystick.
	hr = pInput->mDI->CreateDevice(pdidInstance->guidInstance,  
		&joystick.pDevice, NULL);
	if (FAILED(hr))
		return DIENUM_CONTINUE;

	if (FAILED(hr = joystick.pDevice->SetDataFormat(&c_dfDIJoystick2)))
		return DIENUM_CONTINUE;

	// Set the cooperative level 
	DWORD flags = pInput->mBackground ? DISCL_BACKGROUND : DISCL_FOREGROUND;
	flags |= pInput->mWindow->IsFullScreen() ? DISCL_EXCLUSIVE : DISCL_NONEXCLUSIVE; //(background ? DISCL_NONEXCLUSIVE : DISCL_EXCLUSIVE);

	if (FAILED(hr = joystick.pDevice->SetCooperativeLevel(pInput->mWindow->GetHandle(), flags)))
		return hr;

	// get caps
	DIDEVCAPS diDevCaps;
	diDevCaps.dwSize = sizeof(DIDEVCAPS);
	if (FAILED(hr = joystick.pDevice->GetCapabilities(&diDevCaps)))
		return hr;

	joystick.caps = diDevCaps;

	pInput->mJoystick.push_back(joystick);
	
	return DIENUM_CONTINUE;
}

void Win32Input::EnumXBoxController(int index)
{
	Win32Joystick joystick;

	XINPUT_CAPABILITIES caps;
	if (ERROR_DEVICE_NOT_CONNECTED == XInputGetCapabilities(index, 0, &caps))
		return;

	joystick.xboxIdx = index;

	mJoystick.push_back(joystick);
}

void Win32Input::Deinit()
{
	if (mKeyboardDevice)
	{
		mKeyboardDevice->Unacquire();
		mKeyboardDevice->Release();
		mKeyboardDevice = 0;
	}

	if (mMouseDevice)
	{
		mMouseDevice->Unacquire();
		mMouseDevice->Release();
		mMouseDevice = 0;
	}

	if (mDI)
	{
		mDI->Release();
		mDI = 0;
	}
}

void Win32Input::Update()
{ 
	if (!mWindow)
		return;

	// turn off all locks
	bool wasPressed[InputControl_Max];
	for (int i = 0; i < InputControl_Max; ++i)
	{
		mState[i].pressed = false;
		wasPressed[i] = GetStateBool(InputControl(i));
	}

	HRESULT  hr; 

	//
	// Keyboard
	//
	char key[256];
	memset(&key, 0x0, 256);
    hr = mKeyboardDevice->GetDeviceState(256,(LPVOID)&key); 
	if FAILED(hr) 
	{ 
		// If input is lost then acquire and keep trying 
        hr = mKeyboardDevice->Acquire();
        while (hr == DIERR_INPUTLOST) 
            hr = mKeyboardDevice->Acquire();

		memset(&key, 0x0, 256);
	}

	//
	// Mouse
	//
	DIMOUSESTATE2 mouse;
	memset(&mouse, 0x0, sizeof(DIMOUSESTATE2));
	hr = mMouseDevice->GetDeviceState(sizeof(DIMOUSESTATE2), &mouse);
	if FAILED(hr) 
	{ 
		// If input is lost then acquire and keep trying 
        hr = mMouseDevice->Acquire();
        while (hr == DIERR_INPUTLOST) 
            hr = mMouseDevice->Acquire();

		memset(&mouse, 0x0, sizeof(DIMOUSESTATE2));
	}

	//
	// Joystick
	//
	for (unsigned int i = 0; i < mJoystick.size(); ++i)
	{	
		Win32Joystick& joystick = mJoystick[i];
		joystick.Update();

		int joystickIdx = InputControl_JoyStick0Start + (InputControl_JoyStick0End - InputControl_JoyStick0Start) * i;

		if (joystick.xboxIdx < 0)
		{
			LPDIRECTINPUTDEVICE8& joystickDevice = joystick.pDevice;

			hr = joystickDevice->Poll(); 
			if (FAILED(hr)) 
			{
				// If input is lost then acquire and keep trying 
				hr = joystickDevice->Acquire();
				while(hr == DIERR_INPUTLOST) 
					hr = joystickDevice->Acquire();
			}

			hr = joystickDevice->GetDeviceState(sizeof(DIJOYSTATE2), &joystick.state);
				
			//#define AXIS_TOFLOAT(_t_) fabs(((float)(_t_ - 32767)) / 32767.f) < joystick.deadZone ? 0.f : (((float)(_t_ - 32767)) / 32767.f);// * joystick.sensitivity
			//#define AXIS_TOFLOAT(_t_) max(0.f, (fabs(float(_t_ - 32767) / 32767.f) - joystick.deadZone) / (1.f - joystick.deadZone);

			// xyz axis
			mState[joystickIdx + 1].state = JoystickAxisToFloat(joystick.state.lX, joystick.deadZone);
			mState[joystickIdx + 2].state = JoystickAxisToFloat(joystick.state.lY, joystick.deadZone);
			mState[joystickIdx + 3].state = JoystickAxisToFloat(joystick.state.lZ, joystick.deadZone);

			// other unknown axis
			mState[joystickIdx + 4].state = JoystickAxisToFloat(joystick.state.lRx, joystick.deadZone);	// JOYSTICK0_Axis0
			mState[joystickIdx + 5].state = JoystickAxisToFloat(joystick.state.lRy, joystick.deadZone);	// JOYSTICK0_Axis1
			mState[joystickIdx + 6].state = JoystickAxisToFloat(joystick.state.lRz, joystick.deadZone);	// JOYSTICK0_Axis2
			mState[joystickIdx + 7].state = JoystickAxisToFloat(joystick.state.rglSlider[0], joystick.deadZone);	// JOYSTICK0_Axis3
			mState[joystickIdx + 8].state = JoystickAxisToFloat(joystick.state.rglSlider[1], joystick.deadZone);	// JOYSTICK0_Axis4	

			// the buttons
			joystickIdx += (JOYSTICK0_Button1 - InputControl_JoyStick0Start);
			for (int j = 0; j < (InputControl_JoyStick0End - JOYSTICK0_Button1); ++j)
			{
				mState[joystickIdx + j].state = joystick.state.rgbButtons[j] & 0x80 ? 1.f : 0.f;
			}
		}
		else
		{
			XINPUT_STATE state;
			if (XInputGetState(joystick.xboxIdx, &state) == ERROR_SUCCESS)
			{
				// axis
				mState[joystickIdx + 1].state = JoystickAxisToFloat(state.Gamepad.sThumbLX + 32768, joystick.deadZone);
				mState[joystickIdx + 2].state = -JoystickAxisToFloat(state.Gamepad.sThumbLY + 32768, joystick.deadZone);
				mState[joystickIdx + 3].state = -JoystickAxisToFloat(state.Gamepad.bLeftTrigger + 128, joystick.deadZone, 128.f) + JoystickAxisToFloat(state.Gamepad.bRightTrigger + 128, joystick.deadZone, 128.f);

				// the buttons
				joystickIdx += (JOYSTICK0_Button1 - InputControl_JoyStick0Start);
				mState[joystickIdx + 0].state = state.Gamepad.wButtons & XINPUT_GAMEPAD_A ? 1.f : 0.f;
				mState[joystickIdx + 1].state = state.Gamepad.wButtons & XINPUT_GAMEPAD_B ? 1.f : 0.f;
				mState[joystickIdx + 2].state = state.Gamepad.wButtons & XINPUT_GAMEPAD_X ? 1.f : 0.f;
				mState[joystickIdx + 3].state = state.Gamepad.wButtons & XINPUT_GAMEPAD_Y ? 1.f : 0.f;
				mState[joystickIdx + 4].state = state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER ? 1.f : 0.f;
				mState[joystickIdx + 5].state = state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER ? 1.f : 0.f;
				mState[joystickIdx + 6].state = state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK ? 1.f : 0.f;
				mState[joystickIdx + 7].state = state.Gamepad.wButtons & XINPUT_GAMEPAD_START ? 1.f : 0.f;
			}
			else
			{
				// axis
				mState[joystickIdx + 1].state = 0.f;
				mState[joystickIdx + 2].state = 0.f;
				mState[joystickIdx + 3].state = 0.f;

				// the buttons
				joystickIdx += (JOYSTICK0_Button1 - InputControl_JoyStick0Start);
				mState[joystickIdx + 0].state = 0.f;
				mState[joystickIdx + 1].state = 0.f;
				mState[joystickIdx + 2].state = 0.f;
				mState[joystickIdx + 3].state = 0.f;
			}
		}
	}

	// update the input controls:
	for (int i = 0; i < InputControl_KeyEnd; ++i)
	{
		mState[i].state = key[lookupTable[i].keyCode] & 0x80 ? 1.f : 0.f;

		if (mState[i].state == 1.f)
			int nohitng =0 ;
	}

	// mouse buttons
	for (int i = MOUSE_Left; i <= MOUSE_Middle; ++i)
	{
		mState[i].state = mouse.rgbButtons[i - MOUSE_Left] & 0x80 ? 1.f : 0.f;

		if (mState[i].state != 0.f)
			int nothing = 0;
	}

	// mouse axis
	mState[MOUSE_AxisX].state = (float)mouse.lX * mMouseSensitivity;
	mState[MOUSE_AxisY].state = (float)mouse.lY * mMouseSensitivity;
	mState[MOUSE_Wheel].state = ((float)mouse.lZ) / 120.f;

	if (mState[MOUSE_Wheel].state != 0.f)
		int nothing = 0;
/*
	// joysticks
	for (unsigned int i = 0; i < GetJoystickCount(); ++i)
	{
		Win32Joystick& joystick = mJoystick[i];

		int joystickIdx = InputControl_JoyStick0Start + (InputControl_JoyStick0End - InputControl_JoyStick0Start) * i;

		//#define AXIS_TOFLOAT(_t_) fabs(((float)(_t_ - 32767)) / 32767.f) < joystick.deadZone ? 0.f : (((float)(_t_ - 32767)) / 32767.f);// * joystick.sensitivity
		//#define AXIS_TOFLOAT(_t_) max(0.f, (fabs(float(_t_ - 32767) / 32767.f) - joystick.deadZone) / (1.f - joystick.deadZone);

		// xyz axis
		mState[joystickIdx + 1].state = JoystickAxisToFloat(joystick.state.lX, joystick.deadZone);
		mState[joystickIdx + 2].state = JoystickAxisToFloat(joystick.state.lY, joystick.deadZone);
		mState[joystickIdx + 3].state = JoystickAxisToFloat(joystick.state.lZ, joystick.deadZone);

		// other unknown axis
		mState[joystickIdx + 4].state = JoystickAxisToFloat(joystick.state.lRx, joystick.deadZone);	// JOYSTICK0_Axis0
		mState[joystickIdx + 5].state = JoystickAxisToFloat(joystick.state.lRy, joystick.deadZone);	// JOYSTICK0_Axis1
		mState[joystickIdx + 6].state = JoystickAxisToFloat(joystick.state.lRz, joystick.deadZone);	// JOYSTICK0_Axis2
		mState[joystickIdx + 7].state = JoystickAxisToFloat(joystick.state.rglSlider[0], joystick.deadZone);	// JOYSTICK0_Axis3
		mState[joystickIdx + 8].state = JoystickAxisToFloat(joystick.state.rglSlider[1], joystick.deadZone);	// JOYSTICK0_Axis4	

		// the buttons
		joystickIdx += (JOYSTICK0_Button1 - InputControl_JoyStick0Start);
		for (int j = 0; j < (InputControl_JoyStick0End - JOYSTICK0_Button1); ++j)
		{
			mState[joystickIdx + j].state = joystick.state.rgbButtons[j] & 0x80 ? 1.f : 0.f;
		}
	}*/

	// more pressed stuff
	for (int i = 0; i < InputControl_Max; ++i)
	{
		if (wasPressed[i] && !GetStateBool(InputControl(i)))
			mState[i].pressed = true;
	}
}

bool Win32Input::GetStateBool(InputControl control)
{
	return mState[control].state > 0.f ? true : false;
}

float Win32Input::GetState(InputControl control)
{
	return mState[control].state;
}

bool Win32Input::WasPressed(InputControl control)
{
	return mState[control].pressed;
}

Joystick* Win32Input::GetJoystick(int idx)
{
	if (idx < 0 || idx >= mJoystick.size())
		return 0;

	return &mJoystick[idx];
}

/*
void Input::GetMouseState(int& x, int& y)
{
	x = mMouse.lX;
	y = mMouse.lY;
}

bool Input::MouseDown(int button)
{
	return mMouse.rgbButtons[button] & 0x80 ? true : false;
}

bool Input::MousePressed(int button)
{
	if (MouseDown(button) && mMouseButtonLock[button] == false)
	{
		mMouseButtonLock[button] = true;
		return true;
	}

	if (!MouseDown(button))
	{
		mMouseButtonLock[button] = false;
	}

	return false;
}

void Input::JoystickAxisState(int joystick, int axisIndex, float* xAxis, float* yAxis, float* zAxis)
{
	Joystick& joy = mJoystick[joystick];

	long x = 0.f;
	long y = 0.f;
	long z = 0.f;

	switch (axisIndex)
	{
	case 0:
		x = joy.state.lX;
		y = joy.state.lY;
		z = joy.state.lZ;
		break;

	case 1:
		x = joy.state.lRx;
		y = joy.state.lRy;
		z = joy.state.lRz;
		break;
	}

	*xAxis = ((float)(x - 32767)) / 32767.f;
	*yAxis = -((float)(y - 32767)) / 32767.f;

	if (fabs(*xAxis) < joy.deadZone)
			*xAxis = 0.f;

	if (fabs(*yAxis) < joy.deadZone)
			*yAxis = 0.f;

	if (zAxis)
	{
		*zAxis = ((float)(z - 32767)) / 32767.f;
		if (fabs(*zAxis) < joy.deadZone)
			*zAxis = 0.f;
	}
}

bool Input::JoystickButtonDown(int joystick, int button)
{
	return mJoystick[joystick].state.rgbButtons[button] & 0x80 ? true : false;
}

bool Input::JoystickButtonPressed(int joystick, int button)
{
	Joystick& joy = mJoystick[joystick];

	if (JoystickButtonDown(joystick, button) && joy.buttonLock[button] == false)
	{
		joy.buttonLock[button] = true;
		return true;
	}

	if (!JoystickButtonDown(joystick, button))
	{
		joy.buttonLock[button] = false;
	}

	return false;
}

bool Input::KeyDown(int key)
{
	switch (key)
	{
	case JOYSTICK0_Axis0Up:
		{
			float xAxis, yAxis;
			JoystickAxisState(0, 0, &xAxis, &yAxis);
			return yAxis > 0.f;
		}
	case JOYSTICK0_Axis0Down:
		{
			float xAxis, yAxis;
			JoystickAxisState(0, 0, &xAxis, &yAxis);
			return yAxis < 0.f;
		}
	case JOYSTICK0_Axis0Left:
		{
			float xAxis, yAxis;
			JoystickAxisState(0, 0, &xAxis, &yAxis);
			return xAxis < 0.f;
		}
	case JOYSTICK0_Axis0Right:
		{
			float xAxis, yAxis;
			JoystickAxisState(0, 0, &xAxis, &yAxis);
			return xAxis > 0.f;
		}
	}

	if (key >= JOYSTICK0_Button0)
		return JoystickButtonDown(0, key - JOYSTICK0_Button0);

	if (key >= MOUSE_Left)
		return MouseDown(key - MOUSE_Left);

	return mKey[key] & 0x80 ? true : false;
}

bool Input::KeyPressed(int key)
{
	if (key >= JOYSTICK0_Button0)
		return JoystickButtonPressed(0, key - JOYSTICK0_Button0);

	if (key >= MOUSE_Left)
		return MousePressed(key - MOUSE_Left);

	if (KeyDown(key) && mKeyLock[key] == false)
	{
		mKeyLock[key] = true;
		return true;
	}

	if (!KeyDown(key))
		mKeyLock[key] = false;

	return false;
}
*/

