#ifndef _INPUT_H_
#define _INPUT_H_

#include "Math/Vector.h"

class Window;

#ifdef WIN32
	class Win32Input;
#else
	class LinuxInput;
#endif


enum InputControl
{
	KEY_Space,

	// key pad
	KEY_KP0,
	KEY_KP1,
	KEY_KP2,
	KEY_KP3,
	KEY_KP4,
	KEY_KP5,
	KEY_KP6,
	KEY_KP7,
	KEY_KP8,
	KEY_KP9,

	KEY_0,
	KEY_1,
	KEY_2,
	KEY_3,
	KEY_4,
	KEY_5,
	KEY_6,
	KEY_7,
	KEY_8,
	KEY_9,

	KEY_a,
	KEY_b,
	KEY_c,
	KEY_d,
	KEY_e,
	KEY_f,
	KEY_g,
	KEY_h,
	KEY_i,
	KEY_j,
	KEY_k,
	KEY_l,
	KEY_m,
	KEY_n,
	KEY_o,
	KEY_p,
	KEY_q,
	KEY_r,
	KEY_s,
	KEY_t,
	KEY_u,
	KEY_v,
	KEY_w,
	KEY_x,
	KEY_y,
	KEY_z,

	KEY_F1,
	KEY_F2,
	KEY_F3,
	KEY_F4,
	KEY_F5,
	KEY_F6,
	KEY_F7,
	KEY_F8,
	KEY_F9,
	KEY_F10,
	KEY_F11,
	KEY_F12,

	KEY_Esc,
	KEY_Enter,
	KEY_BackSpace,
	KEY_Tab,
	KEY_Pause,
	KEY_ScrollLock,
	KEY_PrintScreen,
	KEY_Delete,

	KEY_LShift,
	KEY_RShift,
	KEY_LControl,
	KEY_RControl,
	KEY_CapsLock,
	KEY_LAlt,
	KEY_RAlt,

	KEY_Home,
	KEY_Left,
	KEY_Up,
	KEY_Down,
	KEY_Right,
	KEY_PageUp,
	KEY_PageDown,
	KEY_End,
	KEY_Begin,

	InputControl_KeyEnd, // dont use this! its internal!

	// mouse
	MOUSE_Left,
	MOUSE_Right,
	MOUSE_Middle,

	MOUSE_AxisX,
	MOUSE_AxisY,
	MOUSE_Wheel,

	InputControl_JoyStick0Start,

	// joystick 0
	JOYSTICK0_AxisX,
	JOYSTICK0_AxisY,
	JOYSTICK0_AxisZ,

	JOYSTICK0_Axis0,
	JOYSTICK0_Axis1,
	JOYSTICK0_Axis2,
	JOYSTICK0_Axis3,
	JOYSTICK0_Axis4,

	JOYSTICK0_Button1,
	JOYSTICK0_Button2,
	JOYSTICK0_Button3,
	JOYSTICK0_Button4,
	JOYSTICK0_Button5,
	JOYSTICK0_Button6,
	JOYSTICK0_Button7,
	JOYSTICK0_Button8,
	JOYSTICK0_Button9,
	JOYSTICK0_Button10,
	JOYSTICK0_Button11,
	JOYSTICK0_Button12, 

	// xbox specific
	JOYSTICK0_Start = JOYSTICK0_Button8,
	JOYSTICK0_Back = JOYSTICK0_Button9,

	InputControl_JoyStick0End,
	InputControl_JoyStick1Start = InputControl_JoyStick0End,

	// joystick 1
	JOYSTICK1_AxisX,
	JOYSTICK1_AxisY,
	JOYSTICK1_AxisZ,

	JOYSTICK1_Axis0,
	JOYSTICK1_Axis1,
	JOYSTICK1_Axis2,
	JOYSTICK1_Axis3,
	JOYSTICK1_Axis4,

	JOYSTICK1_Button1,
	JOYSTICK1_Button2,
	JOYSTICK1_Button3,
	JOYSTICK1_Button4,
	JOYSTICK1_Button5,
	JOYSTICK1_Button6,
	JOYSTICK1_Button7,
	JOYSTICK1_Button8,
	JOYSTICK1_Button9,
	JOYSTICK1_Button10,
	JOYSTICK1_Button11,
	JOYSTICK1_Button12, 

	// xbox specific
	JOYSTICK1_Start = JOYSTICK1_Button8,
	JOYSTICK1_Back = JOYSTICK1_Button9,

	InputControl_JoyStick1End,
	InputControl_JoyStick2Start = InputControl_JoyStick1End,

	// joystick 2
	JOYSTICK2_AxisX,
	JOYSTICK2_AxisY,
	JOYSTICK2_AxisZ,

	JOYSTICK2_Axis0,
	JOYSTICK2_Axis1,
	JOYSTICK2_Axis2,
	JOYSTICK2_Axis3,
	JOYSTICK2_Axis4,

	JOYSTICK2_Button1,
	JOYSTICK2_Button2,
	JOYSTICK2_Button3,
	JOYSTICK2_Button4,
	JOYSTICK2_Button5,
	JOYSTICK2_Button6,
	JOYSTICK2_Button7,
	JOYSTICK2_Button8,
	JOYSTICK2_Button9,
	JOYSTICK2_Button10,
	JOYSTICK2_Button11,
	JOYSTICK2_Button12, 

	// xbox specific
	JOYSTICK2_Start = JOYSTICK2_Button8,
	JOYSTICK2_Back = JOYSTICK2_Button9,

	InputControl_JoyStick2End,
	InputControl_JoyStick3Start = InputControl_JoyStick2End,

	// joystick 3
	JOYSTICK3_AxisX,
	JOYSTICK3_AxisY,
	JOYSTICK3_AxisZ,

	JOYSTICK3_Axis0,
	JOYSTICK3_Axis1,
	JOYSTICK3_Axis2,
	JOYSTICK3_Axis3,
	JOYSTICK3_Axis4,

	JOYSTICK3_Button1,
	JOYSTICK3_Button2,
	JOYSTICK3_Button3,
	JOYSTICK3_Button4,
	JOYSTICK3_Button5,
	JOYSTICK3_Button6,
	JOYSTICK3_Button7,
	JOYSTICK3_Button8,
	JOYSTICK3_Button9,
	JOYSTICK3_Button10,
	JOYSTICK3_Button11,
	JOYSTICK3_Button12, 

	// xbox specific
	JOYSTICK3_Start = JOYSTICK3_Button8,
	JOYSTICK3_Back = JOYSTICK3_Button9,

	InputControl_JoyStick3End,

	InputControl_Max,
};

class JoystickEffect
{
public:

	float			totalEffectTime;

	// envelope settings
	float			attackLevel;
	float			attackTime;

	float			fadeLevel;
	float			fadeTime;

	InputControl	triggerControl; // what button causes the effect to activate
};

enum JoystickFlags
{
	JF_ForceFeedback,
	JF_XBox,
};
class Joystick
{
public:

	virtual void	ApplyForce(const Vector4& dir, float force) = 0;
	virtual void	InsertEffect(JoystickEffect* effect) = 0;
	virtual int		GetFlags() = 0;

	//virtual void SetDeadZone(float deadZone) = 0;
	//virtual void SetSensitivity(float sensitivity) = 0;
};

class Input
{
	friend class Window;

public:

	Input();

	bool			Init(Window* window, bool background = false);
	void			Deinit();

	void			Update();

	//
	// 0 is up, 1 is down
	// 
	//
	bool			GetStateBool(InputControl control);
	float			GetState(InputControl control);
	bool			WasPressed(InputControl control);

	// use these for alt-tab
	void			AcquireMouse();
	void			UnaquireMouse();

	int				GetJoystickIndex(InputControl control);
	Joystick*		GetJoystick(int idx);
	int				GetJoystickCount();
	void			SetMouseSensitivity(float sensitivity);

protected:
	
#ifdef WIN32
	Win32Input*				mInput;
#else
	LinuxInput*				mInput;
#endif
};

#endif
