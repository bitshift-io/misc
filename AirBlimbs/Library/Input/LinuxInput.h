#ifndef _LINUXINPUT_H_
#define _LINUXINPUT_H_

#include "Input.h"

class Window;

class LinuxInput
{
	friend class Window;

public:

	LinuxInput();

	bool			Init(Window* window, bool background = false);
	void			Deinit();

	void			Update();

	//
	// 0 is up, 1 is down
	// 
	//
	bool			GetStateBool(InputControl control);
	float			GetState(InputControl control);
	bool			WasPressed(InputControl control);

	// use these for alt-tab
	void			AcquireMouse();
	void			UnaquireMouse();

	unsigned int	GetJoystickCount();
	void			SetMouseSensitivity(float sensitivity);

protected:

};

#endif
