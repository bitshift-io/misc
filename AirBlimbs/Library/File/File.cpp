#include "Memory/NoMemory.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdarg.h>
#include <memory.h>
#include <assert.h>

#include "File.h"
#include "PhysicalFile.h"
#include "PackageFile.h"
#include "MemoryFile.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

//-------------------------------------------------------------------

File& operator >> (File& file, char* const& data)
{
	if (file.GetAccess() & FAT_Text)
	{
		file.ReadString("%s", data);
	}
	else
	{
		// read length then the text
		unsigned int len = 0;
		file & len;
		file.Read(data, len);
		data[len] = '\0';
	}

    return file;
}

//template <>
//File& operator << <char*>(File& file, char* const& data)
File& operator << (File& file, const char* const& data)
{
	if (file.GetAccess() & FAT_Text)
	{
		file.Write(data, strlen(data));
	}
	else
	{
		// write length then the text
		unsigned int len = strlen(data);
		file & len;
		file.Write(data, len);
	}

    return file;
}

//-------------------------------------------------------------------

File::File() : mFile(0)//, buffer(1024)
{
}

File::File(const char* file, int access) : mFile(0)
{
	Open(file, access);
}

File::~File()
{
	Close();
}

void File::Close()
{
	if (mFile)
	{
		mFile->Close();
		delete mFile;
		mFile = 0;
	}
}

bool File::Open(IFile* file, int access)
{
	if (!file)
		return false;

	mAccess = access;
	mFile = file;
	return true;
}

bool File::Open(const char* file, int access)
{
	if (!file || mFile)
		return false;

	mAccess = access;
	mFile = 0;

	string fileName = gFilesystem.GetFileNameFromPath(file);
	FileTableEntry* fileTableEntry = gFilesystem.GetFileTableEntry(fileName); // do i need to sore this here?

	// if no entry found, assume physical file
	if (!fileTableEntry)
	{
		// TODO: check if a full path is already given (this is windows specific ATM! what about nix, mac and network drives?)
		string fullFilename = file;
		if (fullFilename.length() < 3 || !(file[1] == ':' && (file[2] == '\\' || file[2] == '/')))
			fullFilename = gFilesystem.GetBaseDirectory() + "/" + file;

		mFile = new PhysicalFile();
		if (!mFile->Open(fullFilename.c_str(), access))
		{
			delete mFile;
			mFile = 0;
		}
	}
	else
	{
		++fileTableEntry->numTimesLoaded;

		switch (fileTableEntry->location)
		{
		case FL_Memory:
			{
				mFile = new MemoryFile();
				if (!mFile->Open(fileTableEntry->filename.c_str(), access))
				{
					delete mFile;
					mFile = 0;
				}
			}
			break;

		case FL_Package:
			{
				mFile = new PackageFile();
				if (!mFile->Open(fileName.c_str(), access))
				{
					delete mFile;
					mFile = 0;
				}
			}
			break;

		case FL_Physical:
			{
				mFile = new PhysicalFile();
				if (!mFile->Open(fileTableEntry->path.c_str(), access))
				{
					delete mFile;
					mFile = 0;
				}
			}
			break;
		}
	}

	return (mFile != 0);
}

bool File::EndOfFile() const
{
	return mFile->EndOfFile();
}

bool File::Valid() const
{
	return (mFile != 0);
}

unsigned int File::Write(const void* data, unsigned int size) const
{
	return mFile->Write(data, size);
}

unsigned int File::Read(void* data, unsigned int size) const
{
	return mFile->Read(data, size);
}

unsigned int File::WriteString(const char* str, ...)
{/*
	va_list ap;
	char newText[1024];
  	va_start(ap, str);
  	unsigned int count = vsprintf(newText, str, ap);
  	va_end(ap);

	mFile->Write(newText, strlen(newText));

	return count;*/

	//return 0;

	//va_start
	//ap = (va_list)_ADDRESSOF(v) + _INTSIZEOF(v)

	va_list ap;
	va_start(ap, str);
	unsigned int ret = mFile->WriteString(str, ap);
	va_end(ap);

	return ret;
}

unsigned int File::ReadString(const char* text, ...)
{/*
 	if( text == 0 )
    	return 0;

	va_list ap;
  	va_start(ap, text);
	int result = vfscanf(file, text, ap);
  	va_end(ap);

	return result;*/
	//return 0;

	va_list ap;
	va_start(ap, text);
	unsigned int ret = mFile->ReadString(text, ap);
	va_end(ap);

	return ret;
}

unsigned int File::Seek(long offset, FileSeekType type)
{
	return mFile->Seek(offset, type);
}

unsigned int File::GetPosition() const
{
	return mFile->GetPosition();
}

char* File::ReadLine(char* buffer, unsigned int size)
{
	return mFile->ReadLine(buffer, size);
}

unsigned int File::Size()
{
	return mFile->Size();
}

const char* File::GetName()
{
	return mFile->GetName();
}
