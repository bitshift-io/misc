#ifndef _FILE_H_
#define _FILE_H_

#include <stdio.h>
#include <string>
#include "Filesystem.h"

using namespace std;

#define endl	'\n'

enum FileAccessType
{
	FAT_Read	= 1 << 0,
	FAT_Write	= 1 << 1,
	FAT_Binary	= 1 << 2,
	FAT_Text	= 1 << 3,
};

enum FileSeekType
{
	FST_Begin,
	FST_Current,
	FST_End
};

class IFile
{
public:

	virtual bool			Open(const char* file, int access) = 0;
	virtual void			Close() = 0;
	virtual bool			EndOfFile() const = 0;

	virtual bool			Valid() const = 0;

	virtual unsigned int	Write(const void* data, unsigned int size) = 0;
	virtual unsigned int	Read(void* data, unsigned int size) = 0;

	virtual unsigned int	WriteString(const char* str, ...) = 0;
	virtual unsigned int	ReadString(const char* str, ...) = 0;

	virtual unsigned int	WriteString(const char* str, va_list ap) = 0;
	virtual unsigned int	ReadString(const char* str, va_list ap) = 0;

	virtual unsigned int	Seek(long offset, FileSeekType type = FST_Begin) = 0;
	virtual unsigned int	GetPosition() const = 0;

	virtual unsigned int	Size() = 0;

	virtual const char*		GetName() = 0;

	// fgets
	virtual char*			ReadLine(char* buffer, unsigned int size) = 0;
};

class File
{
public:

	
	File();
	File(const char* file, int access); // FileAccessType
	~File();

	bool			Open(IFile* file, int access);

	bool			Open(const char* file, int access); // FileAccessType
	void			Close();
	bool			EndOfFile() const;

	bool			Valid() const;
	
	unsigned int	Write(const void* data, unsigned int size) const;
	unsigned int	Read(void* data, unsigned int size) const;

	unsigned int	WriteString(const char* str, ...);
	unsigned int	ReadString(const char* str, ...);

	unsigned int	Seek(long offset, FileSeekType type = FST_Begin);
	unsigned int	GetPosition() const;

	unsigned int	Size();

	const char*		GetName();

	// fgets
	char*			ReadLine(char* buffer, unsigned int size);

	int				GetAccess()											{ return mAccess; }

protected:

	IFile*			mFile;
	int				mAccess;
};

//
// the auto read/write operator
//
template <class T>
File& operator &(File& file, T& data)
{
	if (file.GetAccess() & FAT_Read)
		return operator>>(file, data);
	else if (file.GetAccess() & FAT_Write)
		return operator<<(file, data);

    return file;
}
/*
template <>
File& operator &<string>(File& packet, string& data);

template <>
File& operator &<char*>(File& packet, char*& data);
*/
// read operator
template <class T>
File& operator >>(File& file, T& data)
{
	file.Read(&data, sizeof(T));
    return file;
}

File& operator >> (File& file, char* const& data);

// write operator
template <class T>
File& operator <<(File& file, const T& data)
{
	file.Write(&data, sizeof(T));
    return file;
}

//template <>
//File& operator << <const char*>(File& file, const char* const& data);


File& operator << (File& file, const char* const& data);

#endif
