#ifndef _MEMORYFILE_H_
#define _MEMORYFILE_H_

#include "File.h"

class MemoryFileContent
{
public:

	MemoryFileContent() :
		data(0),
		size(0)
	{
	}

	MemoryFileContent(const string& name, unsigned char* data, unsigned int size) :
		data(data),
		name(name),
		size(size)
	{
	}

	unsigned char*	data;
	string			name;
	unsigned int	size;
};

class MemoryFile : public IFile
{
public:

	MemoryFile();
	MemoryFile(const char* file, int access); // FileAccessType
	MemoryFile(MemoryFileContent* content);
	~MemoryFile();

	// make sure you clean this up when your done!
	virtual bool			Open(MemoryFileContent* content);
	virtual bool			Open(const char* file, int access); // FileAccessType
	virtual void			Close();
	virtual bool			EndOfFile() const;

	virtual bool			Valid() const;

	virtual unsigned int	Write(const void* data, unsigned int size);
	virtual unsigned int	Read(void* data, unsigned int size);

	virtual unsigned int	WriteString(const char* str, ...);
	virtual unsigned int	ReadString(const char* str, ...);

	virtual unsigned int	WriteString(const char* str, va_list ap);
	virtual unsigned int	ReadString(const char* str, va_list ap);

	virtual unsigned int	Seek(long offset, FileSeekType type = FST_Begin);
	virtual unsigned int	GetPosition() const;

	virtual unsigned int	Size();

	const char*				GetName();

	// fgets
	virtual char*			ReadLine(char* buffer, unsigned int size);

protected:

	MemoryFileContent*		mContent;
	unsigned int			mCursor;
};

#endif
