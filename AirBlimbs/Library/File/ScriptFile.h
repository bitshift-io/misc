#ifndef _SCRIPTFILE_H_
#define _SCRIPTFILE_H_

#include <string>
#include <vector>
#include <list>
#include "File.h"
#include "ScriptClass.h"
#include "ScriptVariable.h"

using namespace std;

enum ScriptType
{
	ST_Unknown,
	ST_Ignore,
	ST_Class,
	ST_Parameter,
	ST_Preprocessor,
	ST_BlockBegin,
	ST_BlockEnd,
};

enum PreprocessorType
{
	PT_Unknown,
	PT_Include,
};

class ScriptBase
{
public:



protected:

	string		mName;
	ScriptType	mType;
};

//
// The only one we process is the include statement
// This are outside of class definitions and are of the form:
//
// (something : something)
//
class ScriptPreprocessor
{
public:

	ScriptPreprocessor(const string& name, const string& line) :
	  mName(name),
	  mLine(line)
	{
	}

protected:

	string			mName;
	string			mLine;
};

//
// We simply load this data from file, its up to the user
// to parse the data how they see fit,
// this way we can use this file format for INI files,
// materials, and metadata
//
// note comments are of the form --
//
class ScriptFile
{
public:

	typedef list<ScriptClass>::const_iterator				ClassIterator;
	typedef list<ScriptPreprocessor>::const_iterator		PreprocessorIterator;

	ScriptFile()										{}
	ScriptFile(const string& file);

	bool					Open(const string& file);
	bool					Save(const string& file) const;
	bool					Valid() const				{ return mValid; }

	//const ScriptClass*		GetScriptClass(const string& name) const;
	ScriptClass*			GetScriptClass(const string& name);
	ScriptClass*			CreateScriptClass(const char* name);

	ClassIterator			ClassBegin() const			{ return mClass.begin(); }
	ClassIterator			ClassEnd() const			{ return mClass.end(); }

	PreprocessorIterator	PreprocessorBegin() const	{ return mPreprocessor.begin(); }
	PreprocessorIterator	PreprocessorEnd() const		{ return mPreprocessor.end(); }

	// this really should go in a string helper class
	static vector<string>	Tokenize(const string& str, const string& delimiters, bool stripWhitespace);
	static vector<string>	TokenizeSpecial(const string& str, const string& delimiters, bool stripWhitespace);
	static string			StripWhitespace(const string& str);

protected:

	void					WriteTab(File& out, int tabCount) const;

	bool					ReadFile(const string& file, File& in);
	bool					WriteFile(const string& file, File& out) const;

	bool					ReadClass(ScriptClass* classInfo, File& in);
	bool					WriteClass(const ScriptClass* classInfo, File& out, int tabCount) const;

	bool					WriteVariable(const ScriptVariable& variable, const ScriptClass* classInfo, File& out, int tabCount) const;

	bool					ReadPreprocessor(const string& file, char* line, File& in);
	bool					ReadClassVariable(ScriptClass* classInfo, File& in, const char* line);
	bool					ReadValues(ScriptVariable& variable, ScriptClass* classInfo, File& in);

	PreprocessorType		ClassifyPreprocessor(char* line, char* name, char* inner);
	
	bool					CreateClass(const char* line, ScriptClass& scriptClass);
	ScriptClass*			CreateAndAddClass(const char* line, ScriptClass* outterClass = 0);

	ScriptType				ClassifyLine(const char* line, char* inner);
	bool					StripComment(char* line);


	list<ScriptClass>			mClass;
	list<ScriptPreprocessor>	mPreprocessor;
	bool						mValid;
};

#endif
