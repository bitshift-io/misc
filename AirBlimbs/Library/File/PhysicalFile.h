#ifndef _PHYSICALFILE_H_
#define _PHYSICALFILE_H_

#include "File.h"

class PhysicalFile : public IFile
{
public:

	PhysicalFile();
	PhysicalFile(const char* file, int access); // FileAccessType
	~PhysicalFile();

	virtual bool			Open(const char* file, int access); // FileAccessType
	virtual void			Close();
	virtual bool			EndOfFile() const;

	virtual bool			Valid() const;

	virtual unsigned int	Write(const void* data, unsigned int size);
	virtual unsigned int	Read(void* data, unsigned int size);

	virtual unsigned int	WriteString(const char* str, ...);
	virtual unsigned int	ReadString(const char* str, ...);

	virtual unsigned int	WriteString(const char* str, va_list ap);
	virtual unsigned int	ReadString(const char* str, va_list ap);

	virtual unsigned int	Seek(long offset, FileSeekType type = FST_Begin);
	virtual unsigned int	GetPosition() const;

	virtual unsigned int	Size();

	const char*				GetName();

	// fgets
	virtual char*			ReadLine(char* buffer, unsigned int size);

protected:

	FILE*		file;
	string		name;
};

#endif
