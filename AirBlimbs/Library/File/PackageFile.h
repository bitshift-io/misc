#ifndef _PACKAGEFILE_H_
#define _PACKAGEFILE_H_

#include "File.h"
#include "PhysicalFile.h"

class PackageFile : public PhysicalFile
{
public:

	typedef PhysicalFile Super;

	virtual bool			Open(const char* file, int access); // FileAccessType
	virtual bool			EndOfFile() const;

	virtual unsigned int	Write(const void* data, unsigned int size);
	virtual unsigned int	Read(void* data, unsigned int size);

	virtual unsigned int	WriteString(const char* str, ...);
	virtual unsigned int	ReadString(const char* str, ...);

	virtual unsigned int	WriteString(const char* str, va_list ap);
	virtual unsigned int	ReadString(const char* str, va_list ap);

	virtual unsigned int	Seek(long offset, FileSeekType type = FST_Begin);
	virtual unsigned int	GetPosition() const;

	virtual unsigned int	Size();

	const char*				GetName();

	// fgets
	virtual char*			ReadLine(char* buffer, unsigned int size);

protected:

	unsigned int			mOffset; // start offset in the physical file for the "virtual" file
	unsigned int			mSize;
	string					mVirtualFileName; // name of the "virtual" file

};

#endif
