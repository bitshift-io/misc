#ifndef _PATH_H_
#define _PATH_H_

#include <string>

using namespace std;

class DirectoryIterator
{
public:

	DirectoryIterator(const Path& path);

	
	string	String();
	string	Leaf();
};

class Path
{
public:


protected:

};

#endif