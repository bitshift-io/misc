#include "ScriptClass.h"
#include "ScriptVariable.h"


ScriptClass::ScriptClass(const string& name, const string& extend, const vector<string>& classProperty) :
	mName(name),
	mExtend(extend)
{
	for (unsigned int i = 0; i < classProperty.size(); ++i)
		mProperty.push_back(classProperty[i]);
}

ScriptClass::ScriptClass(const ScriptClass& rhs)
{
	mName = rhs.mName;
	mExtend = rhs.mExtend;

	PropertyIterator propertyIt;
	for (propertyIt = rhs.PropertyBegin(); propertyIt != rhs.PropertyEnd(); ++propertyIt)
		mProperty.push_back(*propertyIt);

	VariableIterator variableIt;
	for (variableIt = rhs.VariableBegin(); variableIt != rhs.VariableEnd(); ++variableIt)
		mVariable.push_back(*variableIt);
}

ScriptClass* ScriptClass::AppendClass(const ScriptClass& scriptClass)
{
	mClass.push_back(scriptClass);
	return &mClass.back();
}

const ScriptClass* ScriptClass::GetScriptClass(const string& name) const
{
	ClassIterator it;
	for (it = ClassBegin(); it != ClassEnd(); ++it)
	{
		if (it->GetName().compare(name) == 0)
			return &(*it);
	}

	return 0;
}

ScriptVariable* ScriptClass::GetScriptVariable(const char* name)
{
	list<ScriptVariable>::iterator it;
	for (it = mVariable.begin(); it != mVariable.end(); ++it)
	{
		if (it->GetName().compare(name) == 0)
			return &(*it);
	}

	return 0;
}




