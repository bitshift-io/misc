#ifndef _SCRIPTVARIABLE_H_
#define _SCRIPTVARIABLE_H_

#include <string>
#include <vector>
#include <list>
#include "ScriptClass.h"

using namespace std;

//
// Parameters are contained with in a class
// and are the form:
//
// some_string = text here
//
class ScriptVariable
{
public:

	enum VariableValueType
	{
		VVT_String,
		VVT_Instance,
		VVT_Pointer,
	};

	struct VariableValue
	{
		string				mString;
		const ScriptClass*	mScriptClassPtr;
		ScriptClass			mScriptClass;
		VariableValueType	mType;
	};

	typedef vector<VariableValue>::iterator ValueIterator;
	typedef vector<VariableValue>::const_iterator ValueConstIterator;

	ScriptVariable(const string& name);
	ScriptVariable(const ScriptVariable& rhs);

	void AppendValue(const string& value);
	void AppendValue(const ScriptClass* value);
	void AppendValue(const ScriptClass& value);

	ValueConstIterator	ValueConstBegin() const			{ return mValue.begin(); }
	ValueConstIterator	ValueConstEnd() const			{ return mValue.end(); }

	ValueIterator	ValueBegin() 						{ return mValue.begin(); }
	ValueIterator	ValueEnd() 							{ return mValue.end(); }
	unsigned int	ValueSize() const					{ return (unsigned int)mValue.size(); }

	const string&	GetName() const						{ return mName; }


protected:

	string					mName;
	vector<VariableValue>	mValue;
};

#endif
