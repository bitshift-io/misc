#ifndef _PACKAGE_H_
#define _PACKAGE_H_

#include "Memory/NoMemory.h"
#include <string>
#include <vector>

using namespace std;

class File;

struct PackageFileEntry
{
	string			filename;
	unsigned int	offset;
	unsigned int	size;
	File*			file;
};

//
// a bunch of files packaged in to a single file, can be encrypted and/or compressed
//
class Package
{
public:

	typedef vector<PackageFileEntry>::iterator Iterator;

	Package();

	bool			Open(const char* file, int access);
	void			Close();

	// extensionInclusionList is a , sperated list of extensions to add, or just "" for all extensions
	bool			AddDirectoryContents(const string& directory, bool bAddSubdirectory = true, const string& extensionInclusionList = "");

	Iterator		Begin();
	Iterator		End();

	const char*		GetName();

	PackageFileEntry* FindFile(const char* file);

protected:

	bool			AddDirectoryContents(const string& directory, bool bAddSubdirectory, const vector<string>& extensionList);
	bool			Read(const char* file);

	vector<PackageFileEntry>	mFileTable;
	string						mName;
	int							mAccess;
};

#endif
