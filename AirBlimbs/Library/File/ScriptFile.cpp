#include "ScriptFile.h"
#include "File.h"
#include "Log.h"
#include "Template/String.h"
#include "Memory/NoMemory.h"
#include "Regex/Regex.h"
#include "Memory/Memory.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

using namespace boost;

ScriptFile::ScriptFile(const string& file)
{
	Open(file);
}

bool ScriptFile::Open(const string& file)
{
	mValid = false;
	File in(file.c_str(), FAT_Read | FAT_Text);
	if (!in.Valid())
	{
		Log::Print("[ScriptFile::Open] Failed to open '%s'\n", file.c_str());
		return false;
	}

	mValid = ReadFile(file, in);
	return mValid;
}

bool ScriptFile::ReadFile(const string& file, File& in)
{
	ScriptClass* lastClass = 0;
	char line[1024];
	char inner[256];
	
	do
	{
		line[0] = '\0';
		in.ReadLine(line, 1024);
		StripComment(line);
		switch (ClassifyLine(line, inner))
		{
		case ST_Class:
			{
				ScriptClass* info = CreateAndAddClass(inner);
				lastClass = info;
			}
			break;

		case ST_Preprocessor:
			{
				if (!ReadPreprocessor(file, inner, in))
					return false;
			}
			break;

		case ST_BlockBegin:
			{
				if (!ReadClass(lastClass, in))
					return false;

				lastClass = 0;
			}
			break;

		case ST_BlockEnd:
			Log::Print("Unexpected } command found\n");
			return false;

		case ST_Unknown:
		case ST_Ignore:
		default:
			break;
		}

	} while (!in.EndOfFile());

	return true;
}


bool ScriptFile::ReadClass(ScriptClass* classInfo, File& in)
{
	ScriptClass* lastClass = 0;
	char line[1024];

	do
	{
		in.ReadLine(line, 1024);

		char inner[256];
		StripComment(line);
		switch (ClassifyLine(line, inner))
		{
		case ST_Class:
			{
				ScriptClass* info = CreateAndAddClass(inner, classInfo);
				lastClass = info;
			}
			break;

		case ST_BlockBegin:
			{
				if (!ReadClass(lastClass, in))
					return false;

				lastClass = 0;
			}
			break;

		case ST_BlockEnd:
			return true;

		case ST_Preprocessor: // we get this when we use () as part of the string of a variable
		case ST_Unknown:
			ReadClassVariable(classInfo, in, line);
			break;

		case ST_Ignore:
		default:
			break;
		}
	} while (!in.EndOfFile());

	Log::Print("End of file found, expected }\n");
	return false;
}

bool ScriptFile::CreateClass(const char* line, ScriptClass& scriptClass)
{
	// get the class name
	char name[256] = "";
	cmatch match;
	regex expression("([^:\\|]*)");
	if (regex_search(line, match, expression))
	{
		strcpy(name, match[1].first);
		name[match[1].second - match[1].first] = '\0';
		strcpy(name, StripWhitespace(name).c_str());
	}
	else
	{
		Log::Print("Failed to extract name for class in: %s\n", line);
		return false;
	}
	
	// find out if we extend
	char extend[256] = "";
	expression = regex(": ([^\\|]*)");
	if (regex_search(line, match, expression))
	{
		strcpy(extend, match[1].first);
		extend[match[1].second - match[1].first] = '\0';
		strcpy(extend, StripWhitespace(extend).c_str());
	}

	// extract propertys
	char properties[256] = "";
	expression = regex("\\|(.*)");
	if (regex_search(line, match, expression))
	{
		strcpy(properties, match[1].first);
		properties[match[1].second - match[1].first] = '\0';
		strcpy(properties, StripWhitespace(properties).c_str());
	}

	vector<string> classProperty = Tokenize(properties, "|", true);
	scriptClass = ScriptClass(name, extend, classProperty);
	return true;
}

ScriptClass* ScriptFile::CreateAndAddClass(const char* line, ScriptClass* outterClass)
{
	ScriptClass scriptClass;
	if (!CreateClass(line, scriptClass))
		return 0;

	// this class is inside the outer class, so only add it to the outter class
	if (outterClass)
		return outterClass->AppendClass(scriptClass);

	mClass.push_back(scriptClass);
	return &mClass.back();
}

bool ScriptFile::ReadClassVariable(ScriptClass* classInfo, File& in, const char* line)
{
	char lhs[256];
	cmatch match;
	regex expression("(.*) =", regex::icase);
	if (!regex_search(line, match, expression))
		return false;

	// okay so we found the line of metadata
	strcpy(lhs, match[1].first);
	lhs[match[1].second - match[1].first] = '\0';

	string strLhs = StripWhitespace(lhs);
	string strRhs;
	char rhs[256] = {'\0'};
	expression = regex(" = (.*)", regex::icase);
	if (regex_search(line, match, expression))
	{
		//return false;

		// okay so we found the line of metadata
		strcpy(rhs, match[1].first);
		rhs[match[1].second - match[1].first] = '\0';

		// strip white space
		strRhs = StripWhitespace(rhs);
	}

	ScriptVariable variable(strLhs);

	// no parameters spcified, so we are expecting a { and } block
	if (strRhs.length() <= 0)
	{
		char line[1024];
		in.ReadLine(line, 1024);

		char inner[256];
		StripComment(line);
		switch (ClassifyLine(line, inner))
		{
		case ST_BlockBegin:
			{
				if (!ReadValues(variable, classInfo, in))
					return false;
			}
			break;

		default:
			Log::Print("Expected { on the line after %s in class %s\n", strLhs.c_str(), classInfo->GetName().c_str());
			return false;
		}		
	}
	else
	{
		variable.AppendValue(strRhs);
	}

	classInfo->AppendVariable(variable);
	return true;
}

bool ScriptFile::ReadValues(ScriptVariable& variable, ScriptClass* classInfo, File& in)
{
	//
	// so if we get here,
	// we are probably reading in an array of values
	//
	//ScriptClass* lastClass = 0;
	char line[1024];
	ScriptClass lastClass;
	bool lastClassValid = false;

	do
	{
		in.ReadLine(line, 1024);

		char inner[256];
		StripComment(line);
		switch (ClassifyLine(line, inner))
		{
		case ST_Class:
			{
				// we came across a pointer to a class
				// as we already have a valid class!
				if (lastClassValid)
				{
					// give preference to those int he scope of this class
					const ScriptClass* scriptClass = classInfo->GetScriptClass(lastClass.GetName());
					if (!scriptClass)
						scriptClass = GetScriptClass(lastClass.GetExtendsName());

					variable.AppendValue(scriptClass);
				}

				if (!CreateClass(inner, lastClass))
					return false;

				lastClassValid = true;
			}
			break;

		case ST_BlockBegin:
			{
				// here comes the definition of this class
				if (!ReadClass(&lastClass, in))
					return false;
		
				variable.AppendValue(lastClass);
				lastClassValid = false;
			}
			break;

		case ST_BlockEnd:
			{
				// we came across a pointer to a class
				if (lastClassValid)
				{
					// give preference to those int he scope of this class
					const ScriptClass* scriptClass = classInfo->GetScriptClass(lastClass.GetName());
					if (!scriptClass)
						scriptClass = GetScriptClass(lastClass.GetExtendsName());

					variable.AppendValue(scriptClass);
				}
			}
			return true;

		case ST_Preprocessor:
		case ST_Unknown:
			{
				if (lastClassValid)
				{
					Log::Print("Cant have classes mixed with values. Error in class %s\n", classInfo->GetName());
					return false;
				}

				string value = StripWhitespace(line);
				if (value.length())
					variable.AppendValue(value);
			}
			break;

		case ST_Ignore:
		default:
			break;
		}
	} while (!in.EndOfFile());

	Log::Print("End of file found, expected }\n");
	return false;	
}

PreprocessorType ScriptFile::ClassifyPreprocessor(char* line, char* name, char* inner)
{
	strcpy(name, line);

	cmatch match;
	regex expression("^(.*?) : .*$");
	if (regex_search(line, match, expression))
	{
		strcpy(inner, match[1].first);
		name[match[1].second - match[1].first] = '\0';
	}

	expression = regex("^.* : (.*)$");
	if (regex_search(line, match, expression))
	{
		strcpy(inner, match[1].first);
		inner[match[1].second - match[1].first] = '\0';
	}

	if (strcmpi("include", name) == 0)
		return PT_Include;

	return PT_Unknown;
}

bool ScriptFile::ReadPreprocessor(const string& file, char* line, File& in)
{
	char name[256];
	char inner[256];
	switch (ClassifyPreprocessor(line, name, inner))
	{
	case PT_Include:
		{
			int idx = file.find_last_of("/");
			if (idx == -1)
				idx = file.find_last_of("\\");

			string newFile = file;
			newFile = newFile.substr(0, idx == -1 ? 0 : idx + 1);
			newFile.append(inner);

			File newIn(newFile.c_str(), FAT_Read | FAT_Text);
			if (!newIn.Valid())
			{
				Log::Print("[ScriptFile::ReadPreprocessor] Failed to open file: %s\n", file.c_str());
				return false;
			}

			mPreprocessor.push_back(ScriptPreprocessor(name, inner));
			return ReadFile(newFile, newIn);
		}
		break;

	default:
		mPreprocessor.push_back(ScriptPreprocessor(name, inner));
		break;
	}

	return true;
}

ScriptType ScriptFile::ClassifyLine(const char* line, char* inner)
{	
	if (StripWhitespace(line).length() <= 0)
		return ST_Ignore;

	// http://www.boost.org/libs/regex/doc/syntax_perl.html
	cmatch match;
	regex expression("\\(([^\\)]*)\\)"); 
	if (regex_search(line, match, expression))
	{
		strcpy(inner, match[1].first);
		inner[match[1].second - match[1].first] = '\0';
		return ST_Preprocessor;
	}

	expression = regex("\\[([^\\]]*)\\]");
	if (regex_search(line, match, expression))
	{
		strcpy(inner, match[1].first);
		inner[match[1].second - match[1].first] = '\0';
		return ST_Class;
	}

	expression = regex("\\{");
	if (regex_search(line, match, expression))
	{
		strcpy(inner, match[1].first);
		inner[match[1].second - match[1].first] = '\0';
		return ST_BlockBegin;
	}

	expression = regex("\\}");
	if (regex_search(line, match, expression))
	{
		strcpy(inner, match[1].first);
		inner[match[1].second - match[1].first] = '\0';
		return ST_BlockEnd;
	}

	return ST_Unknown;
}

bool ScriptFile::StripComment(char* line)
{
	int len = strlen(line);
	if (len < 2)
		return false;

	for (int i = 0; i < (len - 1); ++i)
	{
		if (line[i] == '-' && line[i + 1] == '-')
		{
			line[i] = '\0';
			return true;
		}
	}
	/*
	cmatch match;
	regex expression("(.*)--.*");

	if (regex_match(line, match, expression))
	{
		line[string(match[1].first).length() - string(match[1].second).length()] = '\0';
		return true;
	}
*/
	return false;
}

vector<string> ScriptFile::Tokenize(const string& str, const string& delimiters, bool stripWhitespace)
{
	return String::Tokenize(str, delimiters, stripWhitespace);
	/*
	vector<string> tokens;
    	
	// skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    	
	// find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos)
	{
		// found a token, add it to the vector.
		if (stripWhitespace)
			tokens.push_back(StripWhitespace(str.substr(lastPos, pos - lastPos)));
		else
			tokens.push_back(str.substr(lastPos, pos - lastPos));
	
		// skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
	
		// find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

	return tokens;*/
}

vector<string> ScriptFile::TokenizeSpecial(const string& str, const string& delimiters, bool stripWhitespace)
{
	return String::TokenizeSpecial(str, delimiters, stripWhitespace);
	/*
	vector<string> tokens;
    	
	// skip delimiters at beginning.
	string::size_type lastPos = 0; //str.find_first_not_of(delimiters, 0);
    	
	// find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos + 1);

	while (string::npos != pos || string::npos != lastPos)
	{
		// found a token, add it to the vector.
		if (stripWhitespace)
			tokens.push_back(StripWhitespace(str.substr(lastPos, pos - lastPos)));
		else
			tokens.push_back(str.substr(lastPos, pos - lastPos));
	
		if (string::npos == pos)
			break;

		// skip delimiters.  Note the "not_of"
		lastPos = pos + 1; //str.find_first_of(delimiters, pos);
	
		// find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

	return tokens;*/
}

string ScriptFile::StripWhitespace(const string& str)
{
	return String::StripWhitespace(str);
	/*
	string temp = str;
	int len = strlen(temp.c_str()); //temp.length();
	int i = 0;
	for (; i < len; ++i)
	{
		if (temp[i] != ' ' && temp[i] != '\n' && temp[i] != '\t' && temp[i] != '\r' && temp[i] != '\f')
		{
			if (i != 0)
				temp = temp.substr(i, len);

			break;
		}
	}

	if (i >= len)
		return "";

	len = strlen(temp.c_str()); //temp.length();
	for (i = len - 1; i >= 0; --i)
	{
		if (temp[i] != ' ' && temp[i] != '\n' && temp[i] != '\t' && temp[i] != '\r' && temp[i] != '\f')
		{
			if (i != len)
				temp = temp.substr(0, i + 1);

			break;
		}
	}

	return temp;*/
}
/*
const ScriptClass* ScriptFile::GetScriptClass(const string& name) const
{
	if (name.length() <= 0)
		return 0;

	ClassIterator it;
	for (it = ClassBegin(); it != ClassEnd(); ++it)
	{
		if (strcmpi(it->GetName().c_str(), name.c_str()) == 0)
			return &(*it);
	}

	return 0;
}
*/
ScriptClass* ScriptFile::GetScriptClass(const string& name)
{
	if (name.length() <= 0)
		return 0;

	list<ScriptClass>::iterator it;
	for (it = mClass.begin(); it != mClass.end(); ++it)
	{
		if (strcmpi(it->GetName().c_str(), name.c_str()) == 0)
			return &(*it);
	}

	return 0;
}

ScriptClass* ScriptFile::CreateScriptClass(const char* name)
{
	ScriptClass scriptClass(name, "", vector<string>());
	mClass.push_back(scriptClass);
	return &mClass.back();
}

bool ScriptFile::Save(const string& file) const
{
	File out(file.c_str(), FAT_Write | FAT_Text);
	if (!out.Valid())
	{
		Log::Print("[ScriptFile::Save] Failed to save '%s'\n", file.c_str());
		return false;
	}

	return WriteFile(file, out);;
}

void ScriptFile::WriteTab(File& out, int tabCount) const
{
	for (int i = 0; i < tabCount; ++i)
		out.WriteString("\t");
}

bool ScriptFile::WriteFile(const string& file, File& out) const
{
	ClassIterator classIt;
	for (classIt = mClass.begin(); classIt != ClassEnd(); ++classIt)
	{
		WriteClass(&(*classIt), out, 0);
	}

	return true;
}

bool ScriptFile::WriteClass(const ScriptClass* classInfo, File& out, int tabCount) const
{
	WriteTab(out, tabCount);
	if (classInfo->GetName().size())
	{
		if (classInfo->GetExtendsName().size())
			out.WriteString("[%s : %s", classInfo->GetName().c_str(), classInfo->GetExtendsName().c_str());
		else
			out.WriteString("[%s", classInfo->GetName().c_str());
	}
	else
	{
		out.WriteString("[: %s", classInfo->GetName().c_str(), classInfo->GetExtendsName().c_str());
	}

	// save out propertys
	ScriptClass::PropertyIterator propertyIt;
	for (propertyIt = classInfo->PropertyBegin(); propertyIt != classInfo->PropertyEnd(); ++propertyIt)
		out.WriteString("| %s", propertyIt->c_str());

	out.WriteString("]\n{\n");

	// write out any sub classes
	ClassIterator classIt;
	for (classIt = classInfo->ClassBegin(); classIt != classInfo->ClassEnd(); ++classIt)
	{
		WriteClass(&(*classIt), out, tabCount + 1);
	}

	// write variables
	ScriptClass::VariableIterator varIt;
	for (varIt = classInfo->VariableBegin(); varIt != classInfo->VariableEnd(); ++varIt)
	{
		WriteVariable(*varIt, classInfo, out, tabCount + 1);
	}

	out.WriteString("}\n\n");
	return true;
}

bool ScriptFile::WriteVariable(const ScriptVariable& variable, const ScriptClass* classInfo, File& out, int tabCount) const
{
	WriteTab(out, tabCount);
	out.WriteString("%s = ", variable.GetName().c_str());

	ScriptVariable::ValueConstIterator valueIt;
	if (variable.ValueSize() <= 1)
	{
		for (valueIt = variable.ValueConstBegin(); valueIt != variable.ValueConstEnd(); ++valueIt)
		{
			// TODO
			switch (valueIt->mType)
			{
			case ScriptVariable::VVT_String:
				out.WriteString("%s\n", valueIt->mString.c_str());
				break;
				
			default:
				Log::Error("[ScriptFile::WriteVariable] Type not yet supported");
				break;
			}
		}
	}
	else
	{
		WriteTab(out, tabCount);
		out.WriteString("\n{\n", variable.GetName().c_str());

		// TODO

		WriteTab(out, tabCount);
		out.WriteString("\n}\n", variable.GetName().c_str());
	}

	return true;
}
