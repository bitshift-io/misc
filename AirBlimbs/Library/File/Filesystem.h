#ifndef _FILESYSTEM_H_
#define _FILESYSTEM_H_

#include "Memory/NoMemory.h"
#include <vector>
#include "Template/Singleton.h"

using namespace std;

class Package;
class MemoryFileContent;

// file table is a table of contents
enum FileLocation
{
	FL_Package,		// in a package
	FL_Physical,	// on the physical media
	FL_Memory,		// memory file
};

struct FileTableEntry
{
	string			filename;
	int				numTimesLoaded;
	FileLocation	location;
	string			path;

	union
	{
		Package*				package;
		MemoryFileContent*		memoryFileContent;
	};
};

#define gFilesystem Filesystem::GetInstance()

//
// holds a list of all files which may be opened
// this is a singleston, because i dont want to have
// this as a factory to create files :-/
//
// ie. a file nows about a filesystem, but what ever is using the file, doesnt need to know about it
// should make it easy to remove the need for this should the need ever occur
//
class Filesystem : public Singleton<Filesystem>
{
public:

	typedef	vector<FileTableEntry>::iterator Iterator;

	Filesystem();
	~Filesystem();

	void					Deinit();

	MemoryFileContent*		RegisterMemoryFileContent(const char* name, unsigned char* data, unsigned int size);
	bool					ReleaseMemoryFileContent(MemoryFileContent** file);

	Package*		RegisterPackage(const string& package);
	bool			RegisterDirectoryContents(const string& directory, bool bAddSubdirectory = true);

	// this is the projects root directory
	void			SetBaseDirectory(const string& directory);
	string			GetBaseDirectory();

	FileTableEntry*	GetFileTableEntry(const string& name);

	Iterator		Begin()												{ return mFileTable.begin(); }
	Iterator		End()												{ return mFileTable.end(); }

	void			OutputUsageStats();

	string			GetFileNameFromPath(const string& path);

	// requires dlls to be delay loaded
	bool			SetDLLDirectory(const string& path);

protected:

	vector<FileTableEntry>		mFileTable;		// all files that can be loaded
	vector<Package*>			mPackage;		// a list of loaded packages
	vector<MemoryFileContent*>	mMemoryFileContent;	// list of all memory file contents

	string						mCurrentDirectory;
};

#endif
