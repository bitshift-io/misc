#include "Log.h"
#include "Filesystem.h"
#include <string>
#include "Memory/Memory.h"

using namespace std;

FILE* Log::pLog = 0;

void Log::CheckLog()
{
	if (!pLog)
	{
		string path = gFilesystem.GetBaseDirectory() + "/log.html";
		if (!SetLog(path.c_str()))
		{
			path = gFilesystem.GetBaseDirectory() + "/log.html";
			SetLog(path.c_str());
		}
	}
}

void Log::Print(const char* text, ... )
{
	va_list ap;

	CheckLog();
	
	if (!text)
		return;

	char newText[8192] = "";

	va_start(ap, text);
	vfprintf(pLog, text, ap);
	vprintf(text, ap);
	vsprintf(newText, text, ap);
	va_end(ap);

	fprintf(pLog, "<br>");

#ifdef WIN32
	WCHAR wideText[8192];
	unsigned int len = (unsigned int)strlen(newText);
	wideText[len] = '\0';		
	MultiByteToWideChar(CP_ACP, 0, newText, len, wideText, len);

	OutputDebugStringW((LPCWSTR)wideText);
#endif

	fflush(pLog);
}

void Log::Warning(const char* text, ... )
{
	va_list ap;

	CheckLog();

	if (!text)
		return;

	char newText[8192] = "";

	fprintf(pLog, "<font color=blue>");

	va_start(ap, text);
	vfprintf(pLog, text, ap);
	vprintf(text, ap);
	vsprintf(newText, text, ap);
	va_end(ap);

	fprintf(pLog, "</font><br>");

#ifdef WIN32
	WCHAR wideText[8192];
	unsigned int len = (unsigned int)strlen(newText);
	wideText[len] = '\0';		
	MultiByteToWideChar(CP_ACP, 0, newText, len, wideText, len);

	OutputDebugStringW((LPCWSTR)wideText);
#endif

	fflush(pLog);
}

void Log::Error(const char* text, ... )
{
	va_list ap;

	CheckLog();

	if (!text)
		return;

	char newText[16384] = "";

	fprintf(pLog, "<font color=red>");

	va_start(ap, text);
	vfprintf(pLog, text, ap);
	vprintf(text, ap);
	vsprintf(newText, text, ap);
	va_end(ap);

	fprintf(pLog, "</font><br>");

#ifdef WIN32
	WCHAR wideText[8192];
	unsigned int len = (unsigned int)strlen(newText);
	wideText[len] = '\0';		
	MultiByteToWideChar(CP_ACP, 0, newText, len, wideText, len);

	OutputDebugStringW((LPCWSTR)wideText);
#endif

	fflush(pLog);

//#ifdef WIN32
//	_wassert(wideText, _CRT_WIDE(__FILE__), __LINE__);
//#else
	// assert so we know there was an error!
	// go up the stack on level to see why
	static bool bSingleAssert = false;
	if (!bSingleAssert)
	{
		bSingleAssert = true;
		Assert(false);
	}
//#endif
}

void Log::Success(const char* text, ... )
{
	va_list ap;

	CheckLog();

	if (!text)
		return;

	char newText[8192] = "";

	fprintf(pLog, "<font color=green>");

	va_start(ap, text);
	vfprintf(pLog, text, ap);
	vprintf(text, ap);
	vsprintf(newText, text, ap);
	va_end(ap);

	fprintf(pLog, "</font><br>");

#ifdef WIN32
	WCHAR wideText[8192];
	unsigned int len = (unsigned int)strlen(newText);
	wideText[len] = '\0';		
	MultiByteToWideChar(CP_ACP, 0, newText, len, wideText, len);

	OutputDebugStringW((LPCWSTR)wideText);
#endif

	fflush(pLog);
}
