#ifndef _SOUNDEFFECTSLOT_H_
#define _SOUNDEFFECTSLOT_H_

class SoundEffectSlot
{
	friend class SoundFactory;
	friend class SoundEffect;

public:

	bool	Valid()									{ return mAlEffectSlot != 0; }

	bool	InUse()									{ return mInUse; }
	void	SetInUse(bool inUse)					{ mInUse = inUse; }

protected:

	SoundEffectSlot(SoundFactory* factory);
	~SoundEffectSlot();

	unsigned int			mAlEffectSlot;
	bool					mInUse;
};

#endif