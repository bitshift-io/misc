#ifndef _SOUNDBUFFER_H_
#define _SOUNDBUFFER_H_

#include <string>
#include "al.h"
#include "vorbis/vorbisfile.h"

using namespace std;

class File;
class SoundSource;

class SoundBuffer
{
public:

	SoundBuffer();

	void			Destroy();
	bool			Load(const string& name, bool stream = false);

	// functionality for streaming
	bool			IsStreaming();
	bool			Update(SoundSource* source);
	bool			Stream(unsigned int sampleSet);
	bool			Restart();

	bool			LoadOGG(const char* name, bool stream);
	bool			LoadWAV(const char* name, bool stream);

	// filename, format, data, size, freqency, looping
	static void		LoadOGGFile(const ALbyte* file, ALenum* format, ALvoid** data, ALsizei* size, ALsizei* freq, ALboolean* loop);

	const char*		GetName()									{ return mName.c_str(); }

	struct StreamData
	{
		File*			file;
		vorbis_info*	pInfo;
		OggVorbis_File	oggFile;
		ov_callbacks	callbacks;
	};

	ALenum			mAlFormatBuffer;
	ALsizei			mAlFreqBuffer;
	ALuint			mAlBufferLen;
	unsigned int	mAlSampleSet[2]; // 2 is needed if streaming
	string			mName;
	StreamData*		mStreamData;
};

#endif