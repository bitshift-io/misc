#include "Sound.h"
#include "SoundFactory.h"
#include "SoundBuffer.h"
#include "SoundSource.h"
#include "SoundEffect.h"

Sound::Sound(SoundFactory* factory) :
	mSource(0),
	mBuffer(0),
	mRadius(0.f),
	mPosition(0.f, 0.f, 0.f),
	mVelocity(0.f, 0.f, 0.f),
	mLooping(false),
	mPitch(1.f),
	mGain(1.f),
	mRelative(false),
	mFactory(factory)
{	

}

Sound::~Sound()
{
	Stop();
}

void Sound::SetRadius(float radius)
{
	mRadius = radius;

	if (mSource)
		return mSource->SetRadius(radius);
}

void Sound::SetPosition(const Vector4& position)
{
	mPosition = position;

	if (mSource)
		return mSource->SetPosition(position);
}

void Sound::SetVelocity(const Vector4& velocity)
{
	mVelocity = velocity;

	if (mSource)
		return mSource->SetVelocity(velocity);
}

void Sound::SetSourceRelative(bool relative)
{
	mRelative = relative;

	if (mSource)
		return mSource->SetSourceRelative(relative);
}	

void Sound::SetLooping(bool looping)
{
	mLooping = looping;

	if (mSource)
		return mSource->SetLooping(looping);
}

void Sound::SetPitch(float pitch)
{
	mPitch = pitch;

	if (mSource)
		return mSource->SetPitch(pitch);
}

void Sound::SetGain(float gain)
{
	mGain = gain;

	if (mSource)
		return mSource->SetGain(gain);
}

float Sound::GetPitch()
{
	return mGain;
}

bool Sound::Load(const char* name, bool stream)
{
	mBuffer = mFactory->GetBuffer(name, stream);
	return mBuffer != 0;
}

void Sound::Play()
{
	// obtain a new source, notify soundmgr
	if (!mSource)
		mSource = mFactory->GetSource(); 

	if (mSource)
	{
		EffectIterator effectIt;
		for (effectIt = mEffect.begin(); effectIt != mEffect.end(); ++effectIt)
			(*effectIt)->Apply();

		if (mBuffer && mBuffer->IsStreaming())
			mBuffer->Restart();

		mSource->SetBuffer(mBuffer);
		mSource->SetLooping(mLooping);
		mSource->SetPitch(mPitch);
		mSource->SetRadius(mRadius);
		mSource->SetSourceRelative(mRelative);
		mSource->SetPosition(mPosition);
		mSource->SetVelocity(mVelocity);
		mSource->SetGain(mGain);
		mSource->Play();
	}
}

void Sound::Stop()
{
	if (!mSource)
		return;

	mSource->Stop();

	if (mSource->IsStreaming())
		mSource->SetBuffer(0);
		
	// release source, notify soundmgr
	mFactory->ReleaseSource(&mSource);
}

bool Sound::IsPlaying()
{
	if (mSource)
		return mSource->IsPlaying();

	return false;
}

void Sound::InsertEffect(SoundEffect* effect)
{
	// TODO: check not already in group
	mEffect.push_back(effect);
}

void Sound::RemoveEffect(SoundEffect* effect)
{
	// TODO
}