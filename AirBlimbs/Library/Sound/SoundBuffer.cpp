#include "SoundBuffer.h"
#include "SoundFactory.h"
#include "SoundSource.h"
#include "File/Log.h"
#include "File/File.h"
#include "al.h"
#include "alc.h"
#include "WAV.h"

#define BUFFER_SIZE     (4096 * 32)


//
// OGG callback functions for file io
//
size_t OGGReadFunc(void *ptr, size_t size, size_t nmemb, void *datasource)
{
	File* file = (File*)datasource;
	return file->Read(ptr, nmemb);
}

int OGGSeekFunc(void *datasource, ogg_int64_t offset, int whence)
{
	File* file = (File*)datasource;
	return file->Seek(offset, FileSeekType(whence));
}

int OGGCloseFunc(void *datasource)
{
	File* file = (File*)datasource;
	file->Close();
	return 0;
}

long OGGTellFunc(void *datasource)
{
	File* file = (File*)datasource;
	return file->GetPosition();
}

//
// back to the class code
//
SoundBuffer::SoundBuffer() :
	mAlFormatBuffer(0),
	mAlFreqBuffer(0),
	mAlBufferLen(0),
	mStreamData(0)
{
	mAlSampleSet[0] = 0;
	mAlSampleSet[1] = 0;
}

void SoundBuffer::Destroy()
{
	if (mStreamData)
	{
		alDeleteBuffers(2, mAlSampleSet);
		fn_ov_clear(&mStreamData->oggFile);
		delete mStreamData->file;
		delete mStreamData;
	}
	else
	{
		alDeleteBuffers(1, &mAlSampleSet[0]);
	}
}

bool SoundBuffer::Load(const string& name, bool stream)
{
	mName = name;

	//load our sound
	Log::Print("Loading sound: %s\n", name.c_str());

	bool oggExtension = true;
	string actualName = name + string(".ogg");
	File f;
	if (!f.Open(actualName.c_str(), FAT_Read | FAT_Binary))
	{
		oggExtension = false;
		actualName = name + string(".wav");
		if (!f.Open(actualName.c_str(), FAT_Read | FAT_Binary))
		{
			if (!f.Open("null.ogg", FAT_Read | FAT_Binary))
			{
				if (!f.Open("null.wav", FAT_Read | FAT_Binary))
					return false;
			}
		}
	}
	
	actualName = f.GetName();

	ALint error;
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		WARN("Unknown openal error: %s\n", alGetString(error));
	}

	if (!oggExtension)
	{
		if (!LoadWAV(actualName.c_str(), stream))
			return false;
	}
	else
	{
		if (!LoadOGG(actualName.c_str(), stream))
			return false;
	}

	error = alGetError();
	if (error != AL_NO_ERROR)
	{
		WARN("OpenAL Error: %s\n", alGetString(error));
	}

	return true;
}

bool SoundBuffer::LoadWAV(const char* name, bool stream)
{
	char* alBuffer = 0;
	WAVEID waveID;
	WAV wav;

	if (!SUCCEEDED(wav.LoadWaveFile(name, &waveID)))
	{
		Log::Error("Failed to load WAV: %s\n", name);
		return false;
	}

	if ((SUCCEEDED(wav.GetWaveSize(waveID, (unsigned long*)&mAlBufferLen))) &&
			(SUCCEEDED(wav.GetWaveData(waveID, (void**)&alBuffer))) &&
			(SUCCEEDED(wav.GetWaveFrequency(waveID, (unsigned long*)&mAlFreqBuffer))) &&
			(SUCCEEDED(wav.GetWaveALBufferFormat(waveID, &alGetEnumValue, (unsigned long*)&mAlFormatBuffer))))
	{
		int nothing = 0;			
	}
	else
	{
		Log::Error("Failed to load WAV: %s\n", name);
		return false;
	}

	alGenBuffers(1, &mAlSampleSet[0]);
	alBufferData(mAlSampleSet[0], mAlFormatBuffer, alBuffer, mAlBufferLen, mAlFreqBuffer);

	wav.DeleteWaveFile(waveID);
	return true;
}

bool SoundBuffer::LoadOGG(const char* name, bool stream)
{
	StreamData* streamData = new StreamData;
	streamData->file = new File(name, FAT_Read | FAT_Binary);
	if (!streamData->file->Valid())
	{
		Log::Error("Failed to load OGG: %s\n", name);
		return false;
	}
	
	// Try opening the given file
	streamData->callbacks.read_func = OGGReadFunc;
	streamData->callbacks.seek_func = OGGSeekFunc;
	streamData->callbacks.close_func = OGGCloseFunc;
	streamData->callbacks.tell_func = OGGTellFunc;
	if (fn_ov_open_callbacks(streamData->file, &streamData->oggFile, NULL, 0, streamData->callbacks) != 0)
		return false;

	// Get some information about the OGG file
	streamData->pInfo = fn_ov_info(&streamData->oggFile, -1);

	// Check the number of channels... always use 16-bit samples
	if (streamData->pInfo->channels == 1)
		mAlFormatBuffer = AL_FORMAT_MONO16;
	else
		mAlFormatBuffer = AL_FORMAT_STEREO16;

	// The frequency of the sampling rate
	mAlFreqBuffer = streamData->pInfo->rate;

	int endian = 0;                    // 0 for Little-Endian, 1 for Big-Endian
	int bitStream;

	unsigned int bufferSize = BUFFER_SIZE;
	char* buffer = new char[bufferSize];
    int  result;
 
    do
    {
        result = fn_ov_read(&streamData->oggFile, buffer + mAlBufferLen, bufferSize - mAlBufferLen, endian, 2, 1, &bitStream);
			
		if (mAlBufferLen < 0)
		{
			fn_ov_clear(&streamData->oggFile);
			delete streamData->file;
			delete streamData;
			Log::Error("Error decoding file");
			delete[] buffer;
			return false;
		}

        if (result > 0)
		{
            mAlBufferLen += result;

			// load the whole sound fx, no streaming!
			if (mAlBufferLen >= bufferSize && !stream)
			{
				char* newBuffer = new char[bufferSize * 2];
				memcpy(newBuffer, buffer, bufferSize);
				delete[] buffer;
				buffer = newBuffer;
				bufferSize = bufferSize * 2;
			}
		}

    } while (mAlBufferLen < bufferSize && result != 0);

	// there is too much data for the buffers, so make this a streaming sound
	if (mAlBufferLen >= bufferSize)
	{
		mStreamData = streamData;
		alGenBuffers(2, mAlSampleSet);
		alBufferData(mAlSampleSet[0], mAlFormatBuffer, buffer, mAlBufferLen, mAlFreqBuffer);
		Stream(mAlSampleSet[1]);
	}
	else
	{
		fn_ov_clear(&streamData->oggFile);
		delete streamData->file;
		delete streamData;
		mStreamData = 0;

		alGenBuffers(1, &mAlSampleSet[0]);
		alBufferData(mAlSampleSet[0], mAlFormatBuffer, buffer, mAlBufferLen, mAlFreqBuffer);		
	}
	
	delete[] buffer;
	return true;
}


void SoundBuffer::LoadOGGFile(const ALbyte *file, ALenum *format, ALvoid **data, ALsizei *size, ALsizei *freq, ALboolean *loop)
{

}

bool SoundBuffer::IsStreaming()
{
	return mStreamData != 0;
}

bool SoundBuffer::Update(SoundSource* source)
{
	if (!mStreamData)
		return true;

	int processed = 0;
    bool active = true;
 
    alGetSourcei(source->mAlSource, AL_BUFFERS_PROCESSED, &processed);
 
    while (processed--)
    {
        ALuint buffer;
        alSourceUnqueueBuffers(source->mAlSource, 1, &buffer);
        ALenum error = alGetError();
		if (error != AL_NO_ERROR)
		{
			WARN("OpenAL Error: %s\n", alGetString(error));
		}
	 
        active = Stream(buffer);
 
        alSourceQueueBuffers(source->mAlSource, 1, &buffer);
        error = alGetError();
		if (error != AL_NO_ERROR)
		{
			WARN("OpenAL Error: %s\n", alGetString(error));
		}
    }

	// restart the song if looping
	if (!active)
	{
		if (source->IsLooping())
		{
			source->Stop(false);
			source->SetBuffer(0, false);
			Restart();
			source->SetBuffer(this, false);
			source->Play(false);
		}
		else
		{
			source->Stop(false);
			source->SetBuffer(0, false);
			Restart();
			source->SetBuffer(this, false);

			// test
			//source->SetBuffer(0);
			//source->SetBuffer(this);
		}
	}
 
    return active;
}

bool SoundBuffer::Restart()
{
	// restart the ogg
	mStreamData->file->Seek(0);
	if (fn_ov_open_callbacks(mStreamData->file, &mStreamData->oggFile, NULL, 0, mStreamData->callbacks) != 0)
		return false;

	Stream(mAlSampleSet[0]);
	Stream(mAlSampleSet[1]);
	return true;
}

bool SoundBuffer::Stream(unsigned int sampleSet)
{
	char buffer[BUFFER_SIZE];
    int  size = 0;
    int  section;
    int  result;

	do
    {
        result = fn_ov_read(&mStreamData->oggFile, buffer + size, BUFFER_SIZE - size, 0, 2, 1, &section);

		if (size < 0)
		{
			fn_ov_clear(&mStreamData->oggFile);
			Log::Error("Error decoding file");
			return false;
		}

        if (result > 0)
            size += result;
    } while (size < BUFFER_SIZE && result != 0);
    
    if (size == 0)
        return false;
 
    alBufferData(sampleSet, mAlFormatBuffer, buffer, size, mAlFreqBuffer);
    ALenum error = alGetError();
	if (error != AL_NO_ERROR)
	{
		WARN("OpenAL Error: %s\n", alGetString(error));
	}

	return true;
}