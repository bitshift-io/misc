#ifndef _PACKET_H_
#define _PACKET_H_

#include "NetworkFactory.h"

enum StreamOperation
{
	SO_Read,
	SO_Write,
};

class Packet
{
public:

	Packet(StreamOperation operation = SO_Read) : mBuffer(0), mSize(0), mCursor(0), mOperation(operation)							{ }
	~Packet();

	int					Append(const void* data, unsigned int size);
	int					Read(void* data, unsigned int size);
	int					Peek(void* data, unsigned int size, unsigned int offset = 0) const;

	unsigned int 		ReadLine(char* buffer, unsigned int size);

	unsigned int		GetCursor() const								{ return mCursor; }
	char*				GetBuffer()	const								{ return mBuffer; }
	unsigned int		GetSize() const									{ return mSize; }
	void				SetSize(unsigned int size);

	void				SetSockAddress(const SocketAddr& addr);
	const SocketAddr&	GetSockAddress() const							{ return mSocketAddress; }

	StreamOperation		GetStreamOperation();
	void				SetStreamOperation(StreamOperation operation);

	bool				EndOfStream()									{ return mCursor >= mSize; }

protected:

	char*				mBuffer;
	unsigned int		mSize;
	unsigned int		mCursor;

	SocketAddr			mSocketAddress;
	StreamOperation		mOperation;
};

// add << and >> operators then & calls  << and >> operators

template <class T>
Packet& operator &(Packet& packet, T& data)
{
	switch (packet.GetStreamOperation())
	{
	case SO_Read:
		packet.Read(&data, sizeof(T));
		break;

	case SO_Write:
		packet.Append(&data, sizeof(T));
		break;
	}

    return packet;
}

template <>
Packet& operator &<string>(Packet& packet, string& data);

#endif
