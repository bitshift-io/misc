#ifndef _RDPMESSAGE_H_
#define _RDPMESSAGE_H_

class Packet;

// http://www.faqs.org/rfcs/rfc908.html
enum RDPMessageType
{
	MT_None				= 0,
	MT_State			= 0x1 << 0,
	MT_Acknowledge		= 0x1 << 1,
	MT_Protocol			= 0x1 << 2,
	MT_Request			= 0x1 << 3,
	MT_Reliable			= 0x1 << 4,	
	MT_PacketId			= 0x1 << 5,
	MT_Ping				= 0x1 << 6,
};

struct RDPHead
{
	unsigned char	messageType;

	union
	{
		unsigned int	packetId;
		unsigned int	state;
	};
};

//
// Helper functions to generate, get packet header
//
class RDPMessage
{
public:

	static RDPHead PeekHeader(const Packet* p);
};

#endif