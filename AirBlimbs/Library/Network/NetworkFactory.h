#ifndef _NETWORKFACTORY_H_
#define _NETWORKFACTORY_H_

#include "Template/Singleton.h"

#ifdef WIN32
    #define WIN32_LEAN_AND_MEAN
    #define NOMINMAX
    #include <windows.h>

    #include <winsock.h>
#else
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <netdb.h>
#endif

#include <iostream>
#include <list>

using namespace std;

//#undef GetMessage

typedef sockaddr_in SocketAddr;

using namespace std;
ostream& operator <<(ostream& os, const SocketAddr& addr);

class RDPStream;
class UDPStream;
class TCPStream;
class HTTP;

#define gNetworkFactory NetworkFactory::GetInstance()

class NetworkFactory : public Singleton<NetworkFactory>
{
public:

	NetworkFactory();
	~NetworkFactory();

	unsigned long	ResolveAddress(const char* addr);
	SocketAddr		GetAddress(const char* addr, unsigned int port); //takes a stringed IP and gives a sockaddr
	SocketAddr		GetBroadcastAddress(unsigned int port);
	SocketAddr		GetAnyAddress(unsigned int port);
	SocketAddr		GetInvalidAddress();

	bool			ParseURL(const char* URL, string* protocol, string* host, int* port, string* path, string* args);
	string			ConvertStringToURL(const char* str); // urlencode in php
	string			StripWhitespace(const string& str);

	bool			ConvertToString(const SocketAddr& addr, string& ip, unsigned int& port);
	bool			Equals(const SocketAddr& a, const SocketAddr& b);

	char*			Base64Encode(char *dst, const char *src, unsigned int len);

	int				GetLastError();

	RDPStream*		CreateRDPStream();
	void			Release(RDPStream** stream);

	UDPStream*		CreateUDPStream();
	void			Release(UDPStream** stream);

	TCPStream*		CreateTCPStream();
	void			Release(TCPStream** stream);

	HTTP*			CreateHTTP();
	void			Release(HTTP** http);

protected:

	list<RDPStream*>	mRDPStream;
	list<UDPStream*>	mUDPStream;
	list<TCPStream*>	mTCPStream;

};

#endif
