#include "Packet.h"
#include "Memory/Memory.h"

template <>
Packet& operator &<string>(Packet& packet, string& data)
{
	switch (packet.GetStreamOperation())
	{
	case SO_Read:
		{
			int len;
			packet & len;
			char buffer[1024] = "\0";
			packet.Read(buffer, len);
			data = buffer;
		}
		break;

	case SO_Write:
		{
			unsigned int len = data.length();
			packet & len;
			packet.Append(data.c_str(), len);
		}
		break;
	}

    return packet;
}

Packet::~Packet()
{
	delete[] mBuffer;
}

int Packet::Append(const void* data, unsigned int size)
{
	unsigned int newSize = mSize + size;
	char* newBuffer = new char[newSize];
	memcpy(newBuffer, mBuffer, mSize);
	memcpy(newBuffer + mSize, data, size);
	delete[] mBuffer;
	mBuffer = newBuffer;
	mSize = newSize;
	return size;
}

unsigned int Packet::ReadLine(char* buffer, unsigned int size)
{
	unsigned int offset = mCursor;
	while (mBuffer[offset] != '\n')
	{
		++offset;
		if (offset >= mSize)
			break;
	}

	int readSize = offset - mCursor;
	if (mBuffer[offset - 1] == '\r')
		readSize -= 1;

	if (readSize > size)
	{
		readSize = size;
		offset = mCursor + size;
	}

	memcpy(buffer, mBuffer + mCursor, readSize);
	buffer[readSize] = '\0';

	mCursor = offset + 1;
	if (mCursor > mSize)
		mCursor = mSize;

	return readSize;
}

int Packet::Read(void* data, unsigned int size)
{
	if (mCursor >= mSize)
		return 0;

	int actualSize = min(size, mSize - mCursor);
	if (actualSize <= 0)
		return 0;

	memcpy(data, mBuffer + mCursor, actualSize);
	mCursor += size;
	return actualSize;
}

int Packet::Peek(void* data, unsigned int size, unsigned int offset) const
{
	if ((mCursor + offset) >= mSize)
		return 0;

	int sizeToRead = min(mSize - (mCursor + offset), size);

	memcpy(data, mBuffer + (mCursor + offset), sizeToRead);
	return sizeToRead;
}

void Packet::SetSockAddress(const SocketAddr& addr)			
{ 
	mSocketAddress = addr;
}

void Packet::SetSize(unsigned int size)
{
	if (size < mSize)
		mSize = size;

	// TODO: make bigger
}

StreamOperation Packet::GetStreamOperation()
{
	return mOperation;
}

void Packet::SetStreamOperation(StreamOperation operation)
{
	mOperation = operation;
}
