#include "RDPPeer.h"
#include "RDPMessage.h"
#include "RDPStream.h"
#include "Packet.h"
#include "Template/Time.h"

RDPPeer::RDPPeer(RDPStream* stream) :
	mStream(stream),
	mState(PS_Closed),
	mSendPacketId(1),
	mReceivePacketId(0),
	mSendBytesPerUpdate(4096),
	mPing(0),
	mPingSendTime(0)
{

}

void RDPPeer::Connect(const SocketAddr& toAddr, bool bHost)
{
	mConnectAddr = toAddr;
	if (bHost)
		mState = PS_SyncReceived;
	else
		mState = PS_SyncSent;
}

void RDPPeer::Accept(const SocketAddr& fromAddr)
{
	mConnectAddr = fromAddr;
	mState = PS_SyncReceived;
}

void RDPPeer::SendPingPacket()
{
	// send a ping packet
	RDPHead head;
	head.messageType = MT_Ping | MT_Protocol | MT_Reliable;
	head.packetId = mSendPacketId;
	
	++mSendPacketId;
	mPingSendTime = Time::GetMilliseconds();

	Packet* p = new Packet();
	p->Append(&head, sizeof(head));
	mSendList.push_back(p);
}

void RDPPeer::ReceiveReliableProtocol(Packet* packet)
{
	RDPHead newHead = RDPMessage::PeekHeader(packet);

	// we received a ping packet (not acknowledged - bounce back
	if ((newHead.messageType & (MT_Ping | MT_Acknowledge)) == MT_Ping)
	{
		// send packet internally
		RDPHead head;
		head.messageType = MT_Ping | MT_Acknowledge | MT_Protocol | MT_Reliable;
		head.packetId = mSendPacketId;

		++mSendPacketId;

		Packet* p = new Packet();
		p->Append((const char*)&head, sizeof(RDPHead));
		mSendList.push_back(p);
	}

	// we received our origonal ping packet back, calculate ping
	// and fire off another ping packet
	if ((newHead.messageType & (MT_Ping | MT_Acknowledge)) == (MT_Ping | MT_Acknowledge))
	{
		unsigned long currentTime = Time::GetMilliseconds();
		mPing = currentTime - mPingSendTime;
		mPing /= 2; // round full trip time to half as ping is one way time
		SendPingPacket();
	}
}

void RDPPeer::ReceiveProtocol(Packet* packet)
{
	// do an ordered insert
	RDPHead newHead = RDPMessage::PeekHeader(packet);

	switch (mState)
	{
	case PS_SyncReceived:
		{
			// other side sending us thier state
			if ((newHead.messageType & MT_State) == MT_State && newHead.state == PS_Open)
				mState = PS_Open;
		}
		break;

	case PS_SyncSent:
		{
			// other side sending us thier state
			if ((newHead.messageType & MT_State) == MT_State && newHead.state == PS_SyncReceived)
				mState = PS_Open;
		}
		break;

	case PS_Open:
		{
			// other side may not be in the open state yet, bounce it back
			if ((newHead.messageType & MT_State) == MT_State)
			{
				switch (newHead.state)
				{
				case PS_SyncReceived:
				case PS_SyncSent:
					{
						// send sync ack packet
						Packet packet;
						RDPHead head;
						head.messageType = MT_State | MT_Protocol;
						head.state = PS_Open;
						packet.Append((const char*)&head, sizeof(RDPHead));
						int bytes = SendInternal(&packet);
					}
					break;

				case PS_WaitClose:
					{
						mState = PS_WaitClose;

						// send sync ack packet
						Packet packet;
						RDPHead head;
						head.messageType = MT_State | MT_Protocol;
						head.state = PS_WaitClose;
						packet.Append((const char*)&head, sizeof(RDPHead));
						int bytes = SendInternal(&packet);
					}
					break;
				}
			}


			// okay we can clear out all packets from the sent list up till this point 'newHead.packetId'
			if ((newHead.messageType & MT_PacketId) == MT_PacketId)
			{
				list<Packet*>::iterator it;
				for (it = mSendList.begin(); it != mSendList.end(); )
				{
					RDPHead head = RDPMessage::PeekHeader(*it);
					if (head.packetId <= newHead.packetId)
					{
						delete (*it);
						it = mSendList.erase(it);
						++mPacketsSentThisUpdate;
					}
					else
					{
						break;
					}
				}
			}			
		}
		break;
	}
}

void RDPPeer::ReceiveInternal(Packet* packet)
{
	// do an ordered insert
	RDPHead newHead = RDPMessage::PeekHeader(packet);

	// handle unreliable protocol packets
	// reliable protocol packets go in to the receive list and get processed once the client attempts to receive packets
	// only insert non protocol related packets in to the receive list
	if ((newHead.messageType & MT_Protocol) == MT_Protocol && (newHead.messageType & MT_Reliable) == MT_None)
	{
		ReceiveProtocol(packet);
		delete packet;
		return;
	}

	// we can discard packets older than 'mReceivePacketId'
	if (newHead.packetId <= mReceivePacketId)
	{
		delete packet;
		return;
	}
	
	int missingPacketCount = 0;

	RDPHead head;
	RDPHead previousHead;
	previousHead.packetId = mReceivePacketId;

	list<Packet*>::iterator it;
	list<Packet*>::iterator prevIt = mReceiveList.begin();
	for (it = mReceiveList.begin(); it != mReceiveList.end(); ++it)
	{
		head = RDPMessage::PeekHeader(*it);

		if (prevIt != mReceiveList.begin())
			previousHead = RDPMessage::PeekHeader(*prevIt);

		// we have already got this packet
		if (head.packetId == newHead.packetId)
		{
			delete packet;
			return;
		}

		// while we are inserting, count how many missing packets there are
		if ((previousHead.packetId + 1) != head.packetId && it != mReceiveList.begin())
		{
			++missingPacketCount;
		}

		// when we get to the location where we insert this new packet...
		if (newHead.packetId < head.packetId)
		{
			// if there are no missing packets, let the peer know we can receive all packets up to and including this packet
			if (missingPacketCount == 0)
				mReceivePacketId = newHead.packetId;

			mReceiveList.insert(prevIt, packet);
			break;
		}

		previousHead = head;
		prevIt = it;
	}

	// ops not inserted!
	if (it == mReceiveList.end())
	{
		if ((previousHead.packetId + 1) == newHead.packetId)
			mReceivePacketId = newHead.packetId;

		mReceiveList.insert(it, packet);
	}
}

StreamState RDPPeer::GetState()
{
	if (GetConnectionState() == CS_Closed)
		return eError;

	if (mReceiveList.size() == 0)
		return eNone;

	// see if there are packets to be read in, we can receive all packets up to and including 'mReceivePacketId' 
	// ignore protocol packets
	list<Packet*>::iterator packetIt;
	for (packetIt = mReceiveList.begin(); packetIt != mReceiveList.end(); ++packetIt)
	{
		RDPHead head = RDPMessage::PeekHeader(*packetIt);
		if ((head.messageType & MT_Protocol) != MT_Protocol)
		{
			if (head.packetId <= mReceivePacketId)
				return eRead;

			return eNone;
		}
	}

	return eNone;
}

RDPConnectionState RDPPeer::GetConnectionState()
{
	return mConnectionState;
}

void RDPPeer::Update()
{
	mPacketsSentThisUpdate = 0;

	switch (mState)
	{
	case PS_SyncSent:
	case PS_SyncReceived:
		{
			// send a sync packet
			Packet packet;
			RDPHead head;
			head.messageType = MT_State | MT_Protocol;
			head.state = mState;
			packet.Append((const char*)&head, sizeof(RDPHead));
			int bytes = SendInternal(&packet);
		}
		break;

	case PS_Open:
		{
			// send the other side all packets we know about
			Packet packet;
			RDPHead head;
			head.messageType = MT_PacketId | MT_Protocol;
			head.packetId = mReceivePacketId;
			packet.Append((const char*)&head, sizeof(RDPHead));
			int bytes = SendInternal(&packet);
			
			// send packets
			unsigned int bytesSent = 0;
			list<Packet*>::iterator it;
			for (it = mSendList.begin(); it != mSendList.end(); ++it)
			{
				// for debugging
				//RDPHead head = RDPMessage::PeekHeader(*it);
				//cout << "sending packet: " << head.packetId << endl;

				bytesSent += SendInternal(*it);
				if (bytesSent > mSendBytesPerUpdate) // outgoing speed throttle
					break;
			}
		}
		break;

	case PS_WaitClose:
		{
		}
		break;
	}

	// update connection state
	switch (mState)
	{
	case PS_SyncSent:
	case PS_SyncReceived:
		{
			mConnectionState = CS_Connecting;
		}
		break;

	case PS_Open:
		{
			if (mConnectionState == CS_Connecting)
			{
				mConnectionState = CS_Connected;
				SendPingPacket();
			}
			else
			{
				mConnectionState = CS_Open;
			}
		}
		break;

	default:
		mConnectionState = CS_Closed;
		break;
	}
}

int RDPPeer::SendInternal(Packet* packet)
{
	return mStream->mUDPStream->Send(mConnectAddr, packet);
}

int RDPPeer::Send(Packet* packet)
{
	if (GetConnectionState() != CS_Open && GetConnectionState() != CS_Connected)
		return 0;

	RDPHead head;
	head.messageType = MT_Reliable;
	head.packetId = mSendPacketId;
	++mSendPacketId;

	Packet* p = new Packet();
	p->Append(&head, sizeof(head));
	p->Append(packet->GetBuffer(), packet->GetSize());

	mSendList.push_back(p);
	return packet->GetSize();
}

int RDPPeer::Receive(Packet* packet)
{
	if (!(GetState() & eRead))
		return 0;

	// find the first non-protocol packet
	// ignore protocol packets
	Packet* p = 0;
	while (mReceiveList.size())
	{
		p = mReceiveList.front();
		mReceiveList.pop_front();

		RDPHead head = RDPMessage::PeekHeader(p);

		if ((head.messageType & MT_Protocol) != MT_Protocol)
			break;

		ReceiveReliableProtocol(p);
		delete p;
	}

	packet->Append(p->GetBuffer() + sizeof(RDPHead), p->GetSize() - sizeof(RDPHead));
	packet->SetSockAddress(p->GetSockAddress());

	delete p;
	return packet->GetSize();
}