#ifndef _RDPSTREAM_H_
#define _RDPSTREAM_H_

//#include "RDP/RDPChannel.h"
#include "NetworkFactory.h"
#include "UDPStream.h"
#include "RDPPeer.h"

class RDPState;
class Packet;

//
// Reliable Data Protocol Stream
//
class RDPStream
{
	friend class RDPPeer;

public:

	typedef list<RDPPeer*>::iterator Iterator;

	RDPStream(NetworkFactory* factory);
	~RDPStream();

	bool				Bind(const SocketAddr& onAddr);

	//
	// This is non blocking, call update to update the status of this connection
	// server must set bHost to true, if this is a nat punch through connection
	//
	RDPPeer*			Connect(const SocketAddr& toAddr, bool bHost);

	Iterator			Begin()												{ return mPeer.begin(); }
	Iterator			End()												{ return mPeer.end(); }

	int					GetNumPeer()										{ return mPeer.size(); }

	//
	// broke this in to 2 parts so we can receive at the start of the frame
	// and send at the end of the frame for minimizing ping
	//
	void				UpdateReceive();
	void				UpdateSend();

	int					Send(Packet* packet);

protected:

	NetworkFactory*		mFactory;
	SocketAddr			mBindAddr;
	UDPStream*			mUDPStream;
	list<RDPPeer*>		mPeer;
};

#endif
