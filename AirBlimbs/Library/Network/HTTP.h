#ifndef HTTP_H
#define HTTP_H

#include <string>
#include <vector>

using namespace std;

class NetworkFactory;
class TCPStream;
class Packet;

//
// HTTP/1.1 Protocol implementation
// http://www.w3.org/Protocols/rfc2616/rfc2616.html
//
class HTTP
{
public:

	HTTP(NetworkFactory* factory);

	bool Connect(const char* host, int port = 80);
	void Close();

	// send a page
	bool Post(const char* URI, const char* contentType, Packet* send, Packet* receive);

	// receive a page
	bool Get(const char* URI, Packet* receive);

	// eg.
	// "x-flash-version: 9,0,45,0"
	// "User-Agent: Shockwave Flash"
	void InsertHTTPArg(const char* arg);

protected:

	bool CheckAndStripHeader(Packet* received);
	void AppendHTTPArg(Packet* send);

	NetworkFactory* mFactory;
	TCPStream*		mStream;
	string			mHost;
	int				mPort;
	vector<string>	mHTTPArg;	// additional http args
};

#endif