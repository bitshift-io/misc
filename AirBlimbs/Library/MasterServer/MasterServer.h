#ifndef _MASTERSERVER_H_
#define _MASTERSERVER_H_

#include <string>
#include <vector>
#include "HTTP.h"
#include "Network/Packet.h"

using namespace std;

class Packet;
class Server;
class Client;
class TCPStream;

class ServerInfo
{
public:

	string			mServerName;
	string			mIP;
	unsigned int	mPort;
	int				mNumPlayers;
	string			mGameMode;
	string			mMapName;
	unsigned int	mServerHandle;
	string			mVersion;
	string			mGameName;
};

class ClientInfo
{
public:

	string			mClientName;
	string			mIP;
	unsigned int	mPort;
	string			mVersion;
	string			mGameName;
};

//template <>
//Packet& operator &<ServerInfo>(Packet& packet, ServerInfo& data);

template <>
Packet& operator &<ServerInfo>(Packet& packet, ServerInfo& data);

//Packet& operator &(Packet& packet, ServerInfo& data);

class NetworkFactory;

/*

*/
//
// An interface to the master server to register game online,
// join online games, NAT punch through etc...
//
class MasterServer
{
public:

	bool	Init(NetworkFactory* factory, const string& gameName, const string& serverPath, int port = 80);
	void	Deinit();

	bool	JoinServer(unsigned int serverHandle, int clientPort);
	void	GetInternetServerList(vector<ServerInfo>& server);
	void	GetLANServerList(vector<ServerInfo>& server);
	bool	GetJoinClientList(unsigned int serverHandle, vector<ClientInfo>& client);

	// returns a server handle
	unsigned int	RegisterServer(const string& serverName, int port, const string& mapName, const string& gameMode);
	unsigned int	UpdateServer(unsigned int serverHandle, const string& serverName, int port, const string& mapName, const string& gameMode);
	void			RemoveServer(unsigned int serverHandle);

	bool	HttpGet(Packet* packet, const char* text = 0, ...);
	int		ReadLine(Packet* packet, string& line);
	bool	ReadServerInfo(ServerInfo& info, const string& line);
	bool	ReadClientInfo(ClientInfo& info, const string& line);

	NetworkFactory* GetNetworkFactory()		{ return mFactory; }

	string	mGameName;
	string	mCompleteServerPath;
	string	mServerURL;
	string	mServerPath;
	int		mPort;

	NetworkFactory*	mFactory;
	HTTP			mHTTP;
};

#endif
