#ifndef _HTTP_H_
#define _HTTP_H_

#include <string>

using namespace std;

class NetworkFactory;
class Packet;
class TCPStream;

class HTTP
{
public:

	void	Init(NetworkFactory* factory, const char* host, int port = 80);
	void	Deinit();

	bool	Connect();
	void	Close();

	bool	Get(const char* url, Packet* page);
	void	Post(const char* url, Packet* page);

protected:

	NetworkFactory*		mFactory;
	TCPStream*			mStream;
	string				mHost;
	int					mPort;
};

#endif
