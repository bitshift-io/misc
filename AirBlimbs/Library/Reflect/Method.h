#ifndef _METHOD_H_
#define _METHOD_H_

class ClassDesc;

#include "Type.h"
/*
class MethodType
{
public:

	virtual void Invoke(void* result, void* obj, void* parameters[]) = 0;

	//RTTIType*  returnType;
    int        numParam;
    //RTTIType** paramTypes;
    ClassDesc* methodClass;
    //bool       isStatic;
};
*/
class MethodDesc
{
public:

    MethodDesc(char const* _name, int _flags, MethodTypeDesc* _type);
    MethodDesc& operator,(MethodDesc& method);

	void Invoke(void* result, void* obj, void* parameters[]);

	char const*     name;
	MethodTypeDesc*	type;
    int             flags; 
  
    MethodDesc*		next;
    MethodDesc**	chain;
};

#define FUNC(x) *(new MethodDesc(#x, 0, FuncTypeOf(&self::x)))
#define FUNC_F(x, flags) *(new MethodDesc(#x, flags, FuncTypeOf(&self::x)))

#define FUNC_VOID(x) *(new MethodDesc(#x, 0, ProcTypeOf(&self::x)))
#define FUNC_VOID_F(x, flags) *(new MethodDesc(#x, flags, ProcTypeOf(&self::x)))

template<class __RT, class __C>
class FuncType0 : public MethodTypeDesc 
{ 
 public:

    typedef __RT (__C::*fptr)();

    FuncType0(fptr f) 
	{ 
		this->f = f;
		methodClass = GetClassDesc<__C>();
		//returnType = RTTITypeOfPtr((__RT*)0);
		numParam = 0;
		//paramType = NULL;
    }

    void Invoke(void* result, void* obj, void* params[]) 
	{ 
		*(__RT*)result = (((__C*)obj)->*f)();
    }

protected:

    fptr f;
};


template<class __RT, class __C, class __P1>
class FuncType1 : public MethodTypeDesc
{ 
 public:

    typedef __RT (__C::*fptr)(__P1);

    FuncType1(fptr f) 
	{ 
		this->f = f;
		methodClass = GetClassDesc<__C>();
		//returnType = RTTITypeOfPtr((__RT*)0);
		numParam = 1;
		//paramTypes = new RTTIType*[1];
		//paramTypes[0] = RTTITypeOfPtr((__P1*)0);
    }

    void Invoke(void* result, void* obj, void* params[])
	{ 
		// might need to hold a result value here, so we can return a result ptr :-/
		//result = (((__C*)obj)->*f)(params ? (__P1)params[0] : 0);
		*(__RT*)result = (((__C*)obj)->*f)(params ? (__P1)params[0] : 0);
		//*(__RT*)result = (((__C*)obj)->*f)(params ? *(__P1*)params[0] : 0);
    }

	fptr f;
};


template<class __RT, class __C>
inline MethodTypeDesc* FuncTypeOf(__RT (__C::*f)()) 
{ 
    return new FuncType0<__RT, __C>(f);
}

template<class __RT, class __C, class __P1>
inline MethodTypeDesc* FuncTypeOf(__RT (__C::*f)(__P1)) 
{ 
    return new FuncType1<__RT, __C, __P1>(f);
}
/*
template<class __RT, class __C, class __P1, class __P2>
inline MethodType* FuncTypeOf(__RT (__C::*f)(__P1, __P2))
{ 
    return new FuncType2<__RT, __C, __P1, __P2>(f);
}

template<class __RT, class __C, class __P1, class __P2, class __P3>
inline RTTIMethodType* RTTIFuncTypeOf(__RT (__C::*f)(__P1, __P2, __P3)) { 
    return new RTTIFuncType3<__RT, __C, __P1, __P2, __P3>(f);
}

template<class __RT, class __C, class __P1, class __P2, class __P3, class __P4>
inline RTTIMethodType* RTTIFuncTypeOf(__RT (__C::*f)(__P1, __P2, __P3, __P4)) { 
    return new RTTIFuncType4<__RT, __C, __P1, __P2, __P3, __P4>(f);
}

template<class __RT, class __C, class __P1, class __P2, class __P3, class __P4, class __P5>
inline RTTIMethodType* RTTIFuncTypeOf(__RT (__C::*f)(__P1, __P2, __P3, __P4, __P5)) { 
    return new RTTIFuncType5<__RT, __C, __P1, __P2, __P3, __P4, __P5>(f);
}
*/





template<class __C>
class ProcType0 : public MethodTypeDesc 
{ 
 public:

    typedef void (__C::*fptr)();

    ProcType0(fptr f) 
	{ 
		this->f = f;
		methodClass = GetClassDesc<__C>();
		//returnType = &voidType;
		numParam = 0;
		//paramTypes = NULL;
    }

    void Invoke(void*, void* obj, void* params[]) 
	{ 
		(((__C*)obj)->*f)();
    }

    fptr f;
};

template<class __C, class __P1>
class ProcType1 : public MethodTypeDesc
{ 
 public:

    typedef void (__C::*fptr)(__P1);

    ProcType1(fptr f) 
	{ 
		this->f = f;
		methodClass = GetClassDesc<__C>();
		//returnType = &voidType;
		numParam = 1;
		//paramTypes = new RTTIType*[1];
		//paramTypes[0] = RTTITypeOfPtr((__P1*)0);
    }

    void Invoke(void*, void* obj, void* params[])
	{ 
		// FMNOTE: ptr is being dereferenced here, need to check against type for this
		(((__C*)obj)->*f)((__P1)params[0]);
		//(((__C*)obj)->*f)(*(__P1*)params[0]);
    }

	fptr f;
};

template<class __C>
inline MethodTypeDesc* ProcTypeOf(void (__C::*f)()) 
{ 
    return new ProcType0<__C>(f);
}

template<class __C, class __P1>
inline MethodTypeDesc* ProcTypeOf(void (__C::*f)(__P1)) 
{ 
    return new ProcType1<__C, __P1>(f);
}
/*
template<class __C, class __P1, class __P2>
inline RTTIMethodType* RTTIProcTypeOf(void (__C::*f)(__P1, __P2)) { 
    return new RTTIProcType2<__C, __P1, __P2>(f);
}

template<class __C, class __P1, class __P2, class __P3>
inline RTTIMethodType* RTTIProcTypeOf(void (__C::*f)(__P1, __P2, __P3)) { 
    return new RTTIProcType3<__C, __P1, __P2, __P3>(f);
}

template<class __C, class __P1, class __P2, class __P3, class __P4>
inline RTTIMethodType* RTTIProcTypeOf(void (__C::*f)(__P1, __P2, __P3, __P4)) { 
    return new RTTIProcType4<__C, __P1, __P2, __P3, __P4>(f);
}

template<class __C, class __P1, class __P2, class __P3, class __P4, class __P5>
inline RTTIMethodType* RTTIProcTypeOf(void (__C::*f)(__P1, __P2, __P3, __P4, __P5)) { 
    return new RTTIProcType5<__C, __P1, __P2, __P3, __P4, __P5>(f);
}*/

#endif