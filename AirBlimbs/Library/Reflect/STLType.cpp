#include "STLType.h"

void STLTypeDesc::GetTypeName(char* buf)
{
	switch (stlType)
	{
	case STLT_Vector:
		buf += sprintf(buf, "vector<");
		break;

	case STLT_List:
		buf += sprintf(buf, "list<");
		break;
	}

	templateType->GetTypeName(buf);
	buf += strlen(buf);
	buf += sprintf(buf, ">");
	*buf = '\0';
}
