#ifndef _TYPE_H_
#define _TYPE_H_

#include "File/Log.h"

#ifdef LoadString
#undef LoadString
#endif

class ClassDesc;

enum Type
{
	T_Unknown,
	T_BasicType,
	T_Enum,
	T_Pointer,
	T_Array,
	T_Class,
	T_Method,
	T_STL,
};

class TypeDesc
{
public:

	virtual void			GetTypeName(char* buf);

	// convert a string to binary and put it in the variable at the appropriate index
	virtual bool			LoadString(void* variable, const char* strValue, int index = 0) const		{ return false; }
	virtual bool			LoadBinary(void* variable, void* data, int index = 0) const					{ return false; }

	virtual void*			Create() const																{ return 0; }
	virtual void			Destroy(void** inst) const													{}

	virtual Type			GetType() const																{ return T_Unknown; }
	virtual unsigned int	GetSize() const																{ return 0; }
};


class BasicTypeDesc : public TypeDesc
{
public:

	enum BasicType
	{
		BT_Void,
		BT_Char,
		BT_UChar,
		BT_SChar,
		BT_Short,
		BT_UShort,
		BT_Int,
		BT_UInt,
		BT_Long,
		BT_ULong,
		BT_Float,
		BT_Double,
		BT_Bool,
	};

	BasicTypeDesc(BasicType _basicType) :
		basicType(_basicType)
	{
	}

	virtual void			GetTypeName(char* buf);
	virtual Type			GetType() const																{ return T_BasicType; }

	BasicType basicType;
};

template<class T>
class BasicTypeDescTemplate : public BasicTypeDesc
{
public:

	BasicTypeDescTemplate(BasicType _basicType) :
		BasicTypeDesc(_basicType)
	{
	}

	virtual bool			LoadString(void* variable, const char* strValue, int index = 0) const;
	virtual bool			LoadBinary(void* variable, void* data, int index = 0) const;

	virtual void*			Create() const																{ return new T; }
	virtual void			Destroy(void** inst) const													{ delete ((T*)(*inst)); }

	virtual unsigned int	GetSize() const																{ return sizeof(T); }
};

template<class T>
bool BasicTypeDescTemplate<T>::LoadString(void* variable, const char* strValue, int index = 0) const
{
	const char* format = 0;
	switch (basicType)
	{
	case BT_UChar:
	case BT_UShort:
	case BT_UInt:
		format = "%u";
		break;

	case BT_Short:
	case BT_Char:
	case BT_SChar:
	case BT_Int:
		format = "%d";
		break;

	case BT_Long:
	case BT_ULong:
		format = "%l";
		break;

	case BT_Float:
		format = "%f";
		break;

	case BT_Double:
		Log::Error("Loading T_Double not yet supported");
		break;

	case BT_Bool:
		{
			bool bVal = false;
			if (strcmpi(strValue, "true") == 0 || strcmpi(strValue, "1") == 0)
				bVal = true;

			*((char*)variable) = bVal;
			return true;
		}
		break;
	}
	
	if (!format)
		return false;

	return sscanf(strValue, format, variable) == 1;
}

template<class T>
bool BasicTypeDescTemplate<T>::LoadBinary(void* variable, void* data, int index) const
{
	*((T*)variable) = *((T*)data);
	return true;
}

class PointerTypeDesc : public TypeDesc 
{     
public:

	PointerTypeDesc(TypeDesc* _ptrType) : 
		ptrType(_ptrType)
	{
	}

    void					GetTypeName(char* buf);

	virtual bool			LoadString(void* variable, const char* strValue, int index = 0) const;
	virtual bool			LoadBinary(void* variable, void* data, int index = 0) const;

	virtual Type			GetType() const																{ return T_Pointer; }

    TypeDesc*				ptrType;
};


class  ArrayTypeDesc : public TypeDesc 
{     
public:

    ArrayTypeDesc(TypeDesc* _elemType, int _numElem) : 
		elemType(_elemType),
		numElem(_numElem)
	{
    }

    void					GetTypeName(char* buf);

	virtual Type			GetType() const																{ return T_Array; }

    TypeDesc*				elemType;
    int						numElem;
};

/*
class DerivedTypeDesc : public TypeDesc 
{     
public:
    DerivedTypeDesc(ClassDesc* _baseClass) : 
		TypeDesc(T_Derived, 0),
		baseClass(_baseClass)
	{
		this->baseClass = baseClass;
    }
    
    ClassDesc*	GetBaseClass()					{ return baseClass; }

    void		GetTypeName(char* buf);

protected:

    ClassDesc* baseClass;
};*/


class MethodTypeDesc : public TypeDesc 
{ 
	friend class MethodDescriptor;

public:

    virtual ~MethodTypeDesc() 
	{ 
		delete[] paramTypes; 
	}

	virtual Type			GetType() const																{ return T_Method; }

    void					GetTypeName(char* buf);

    void					GetMethodDeclaration(char* buf, char const* name);
    virtual void			Invoke(void* result, void* obj, void* parameters[]) = 0;

    TypeDesc*				returnType;
    int						numParam;
    TypeDesc**				paramTypes;
    ClassDesc*				methodClass;
    bool					isStatic;
};
	



const int MAX_PARAMETERS = 5;


template<class __T>
inline TypeDesc* TypeOf(__T&) 
{ 
	return GetClassDesc<__T>();
}



template<>
inline TypeDesc* TypeOf(char&) 
{ 
	static BasicTypeDescTemplate<char> basicType(BasicTypeDesc::BT_Char);
    return &basicType;
}

template<>
inline TypeDesc* TypeOf(unsigned char&) 
{
	static BasicTypeDescTemplate<unsigned char> basicType(BasicTypeDesc::BT_UChar);
    return &basicType;
}

template<>
inline TypeDesc* TypeOf(signed char&) 
{
	static BasicTypeDescTemplate<signed char> basicType(BasicTypeDesc::BT_SChar);
    return &basicType;
}

template<>
inline TypeDesc* TypeOf(short&) 
{
	static BasicTypeDescTemplate<short> basicType(BasicTypeDesc::BT_Short);
    return &basicType;
}

template<>
inline TypeDesc* TypeOf(unsigned short&) 
{ 
	static BasicTypeDescTemplate<unsigned short> basicType(BasicTypeDesc::BT_UShort);
    return &basicType;
}

template<>
inline TypeDesc* TypeOf(int&) 
{
	static BasicTypeDescTemplate<int> basicType(BasicTypeDesc::BT_Int);
    return &basicType;
}

template<>
inline TypeDesc* TypeOf(unsigned int&) 
{ 
	static BasicTypeDescTemplate<unsigned int> basicType(BasicTypeDesc::BT_UInt);
    return &basicType;
}

template<>
inline TypeDesc* TypeOf(long&) 
{ 
	static BasicTypeDescTemplate<long> basicType(BasicTypeDesc::BT_Long);
    return &basicType;
}

template<>
inline TypeDesc* TypeOf(unsigned long&) 
{ 
	static BasicTypeDescTemplate<unsigned long> basicType(BasicTypeDesc::BT_ULong);
    return &basicType;
}

template<>
inline TypeDesc* TypeOf(float&) 
{ 
    static BasicTypeDescTemplate<float> basicType(BasicTypeDesc::BT_Float);
    return &basicType;
}

template<>
inline TypeDesc* TypeOf(double&)
{ 
    static BasicTypeDescTemplate<double> basicType(BasicTypeDesc::BT_Double);
    return &basicType;
}

template<>
inline TypeDesc* TypeOf(bool&) 
{ 
    static BasicTypeDescTemplate<bool> basicType(BasicTypeDesc::BT_Bool);
    return &basicType;
}

inline TypeDesc* TypeOfPtr(char* t)
{ 
    return TypeOf(*t);
}

inline TypeDesc* TypeOfPtr(unsigned char* t) 
{
    return TypeOf(*t);
}

inline TypeDesc* TypeOfPtr(signed char* t)
{
    return TypeOf(*t);
}

inline TypeDesc* TypeOfPtr(short* t)
{
    return TypeOf(*t);
}

inline TypeDesc* TypeOfPtr(unsigned short* t) 
{ 
    return TypeOf(*t);
}

inline TypeDesc* TypeOfPtr(int* t)
{
    return TypeOf(*t);
}

inline TypeDesc* TypeOfPtr(unsigned int* t)
{ 
    return TypeOf(*t);
}

inline TypeDesc* TypeOfPtr(long* t) 
{ 
    return TypeOf(*t);
}

inline TypeDesc* TypeOfPtr(unsigned long* t)
{ 
    return TypeOf(*t);
}

inline TypeDesc* TypeOfPtr(float* t)
{ 
    return TypeOf(*t);
}

inline TypeDesc* TypeOfPtr(double* t) 
{ 
    return TypeOf(*t);
}

inline TypeDesc* TypeOfPtr(bool* t) 
{ 
    return TypeOf(*t);
}


template<class __P>
inline TypeDesc* TypeOfPtr(__P const*const*) 
{ 
    return new PointerTypeDesc(&__P::sClassDesc);
}

template<>
inline TypeDesc* TypeOfPtr(char const*const* t) 
{ 
	char temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<>
inline TypeDesc* TypeOfPtr(unsigned char const*const* t) 
{
	unsigned char temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<>
inline TypeDesc* TypeOfPtr(signed char const*const* t)
{
	signed char temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<>
inline TypeDesc* TypeOfPtr(short const*const* t)
{
	short temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<>
inline TypeDesc* TypeOfPtr(unsigned short const*const* t) 
{ 
	unsigned short temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<>
inline TypeDesc* TypeOfPtr(int const*const* t)
{
	int temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<>
inline TypeDesc* TypeOfPtr(unsigned int const*const* t)
{ 
	unsigned int temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<>
inline TypeDesc* TypeOfPtr(long const*const* t) 
{ 
	long temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<>
inline TypeDesc* TypeOfPtr(unsigned long const*const* t) 
{ 
	unsigned long temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<>
inline TypeDesc* TypeOfPtr(float const*const* t) 
{ 
	float temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<>
inline TypeDesc* TypeOfPtr(double const*const* t) 
{ 
	double temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<>
inline TypeDesc* TypeOfPtr(bool const*const* t) 
{ 
	bool temp;
    return new PointerTypeDesc(TypeOf(temp));
}

template<class __P>
inline TypeDesc* TypeOfPtrToPtr(__P const*const*const*) 
{ 
    return new PointerTypeDesc(new PointerTypeDesc(&__P::sClassDesc));
}


#endif