#ifndef _CLASS_H_
#define _CLASS_H_

#include "Variable.h"
#include "Method.h"

typedef void* (*CreateFn)();
typedef void (*DestroyFn)(void**);

template <class T>
void* Creator()
{
	return new T();
}

template <class T>
void Destroyer(void** inst)
{
	delete ((T*)*inst);
}

class ClassDesc : public TypeDesc
{
public:

	typedef VariableDesc*	(*VariableDescFn)();
    typedef MethodDesc*		(*MethodDescFn)();

	ClassDesc(char const* _name, ClassDesc* _parent, unsigned int _size, VariableDescFn varDescFn, MethodDescFn methodDescFn, CreateFn createFn, DestroyFn destroyFn, int _flags);

	MethodDesc*				GetMethodDesc(const char* name) const;
	VariableDesc*			GetVariableDesc(const char* name) const;
	ClassDesc*				InheritsFrom(ClassDesc* potentialParent) const;
	virtual void*			Create() const;
	virtual void			Destroy(void** inst) const;
	void					GetTypeName(char* buf);

	virtual unsigned int	GetSize() const																{ return size; }
	virtual Type			GetType() const																{ return T_Class; }

	unsigned int			uniqueId;			// unique id
	const char*				name;
	ClassDesc*				parent;
	VariableDesc*			variable;
	MethodDesc*				method;
	
	unsigned int			flags;
	unsigned int			size;

protected:

	DestroyFn				destroy;
	CreateFn				create;
};

/*
    static ClassDesc* GetClassDesc() { \
		return &sClassDesc; \
    } \
	*/

#define REFLECT_CLASS_VM(T, vars, methods) \
    static ClassDesc sClassDesc; \
	virtual ClassDesc* GetClassDesc() { return &sClassDesc; } \
    VariableDesc* GetVariableDesc() { return &vars; } \
    MethodDesc* GetMethodDesc() { return &methods; } \
	typedef T self;

#define REFLECT_CLASS_M(T, methods) \
    static ClassDesc sClassDesc; \
	virtual ClassDesc* GetClassDesc() { return &sClassDesc; } \
    VariableDesc* GetVariableDesc() { return 0; } \
    MethodDesc* GetMethodDesc() { return &methods; } \
	typedef T self;

#define REFLECT_CLASS_V(T, vars) \
    static ClassDesc sClassDesc; \
	virtual ClassDesc* GetClassDesc() { return &sClassDesc; } \
    VariableDesc* GetVariableDesc() { return &vars; } \
	MethodDesc* GetMethodDesc() { return 0; } \
	typedef T self;

#define REFLECT_CLASS(T) \
    static ClassDesc sClassDesc; \
	virtual ClassDesc* GetClassDesc() { return &sClassDesc; } \
	VariableDesc* GetVariableDesc() { return 0; } \
	MethodDesc* GetMethodDesc() { return 0; } \
	typedef T self;


#define REGISTER_CLASS(T)	\
	static VariableDesc* T##VariableDesc() { return T().GetVariableDesc(); } \
	static MethodDesc* T##MethodDesc() { return T().GetMethodDesc(); } \
    ClassDesc T::sClassDesc(#T, 0, sizeof(T), &T##VariableDesc, &T##MethodDesc, &Creator<T>, &Destroyer<T>, 0);

#define REGISTER_CLASS_P(T, P)	\
	static VariableDesc* T##VariableDesc() { return T().GetVariableDesc(); } \
	static MethodDesc* T##MethodDesc() { return T().GetMethodDesc(); } \
	ClassDesc T::sClassDesc(#T, ::GetClassDesc<P>(), sizeof(T), &T##VariableDesc, &T##MethodDesc, &Creator<T>, &Destroyer<T>, 0);

#define REGISTER_CLASS_F(T, flags)	\
	static VariableDesc* T##VariableDesc() { return T().GetVariableDesc(); } \
	static MethodDesc* T##MethodDesc() { return T().GetMethodDesc(); } \
    ClassDesc T::sClassDesc(#T, 0, sizeof(T), &T##VariableDesc, &T##MethodDesc, &Creator<T>, &Destroyer<T>, flags);


#define REGISTER_CLASS_PF(T, P, flags)	\
	static VariableDesc* T##VariableDesc() { return T().GetVariableDesc(); } \
	static MethodDesc* T##MethodDesc() { return T().GetMethodDesc(); } \
	ClassDesc T::sClassDesc(#T, ::GetClassDesc<P>(), sizeof(T), &T##VariableDesc, &T##MethodDesc, &Creator<T>, &Destroyer<T>, flags);

template <class T>
ClassDesc* GetClassDesc()
{
	return &T::sClassDesc;
}

#endif