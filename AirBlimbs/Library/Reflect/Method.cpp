#include "Method.h"

MethodDesc::MethodDesc(char const* _name, int _flags, MethodTypeDesc* _type) :
	name(_name),
	flags(_flags),
	type(_type),
	next(0),
	chain(&next)
{ 
}

MethodDesc& MethodDesc::operator,(MethodDesc& method) 
{
	*chain = &method;
	chain = &method.next;
	return *this;
}

void MethodDesc::Invoke(void* result, void* obj, void* parameters[])
{
	type->Invoke(result, obj, parameters);
}