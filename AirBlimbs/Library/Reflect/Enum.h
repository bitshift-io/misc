#ifndef _ENUM_H_
#define _ENUM_H_

#include "Type.h"

#define REFLECT_ENUM(e)							\
	extern EnumTypeDesc e##EnumTypeDesc;

#define REFLECT_ENUM_BEGIN(e)					\
	EnumEntry e##EnumEntry[] =					\
	{											\

#define REFLECT_ENUM_END(e)						\
		0, 0, 0, 0								\
	};											\
	EnumTypeDesc e##EnumTypeDesc(#e, e##EnumEntry);

#define ENUM(v)					\
	#v, (int)v,

#define VAR_ENUM(e, x) \
	*(new VariableDesc(#x, (char*)&x-(char*)this, sizeof(x), 0, &e##EnumTypeDesc))

struct EnumEntry
{
	const char* 		name;
	int					value;
};

class EnumTypeDesc : public TypeDesc
{
public:

	EnumTypeDesc(char const* _name, EnumEntry* _enumEntry);

	virtual void			GetTypeName(char* buf);

	// convert a string to binary and put it in the variable at the appropriate index
	virtual bool			LoadString(void* variable, const char* strValue, int index = 0) const;
	virtual bool			LoadBinary(void* variable, void* data, int index = 0) const;

	virtual Type			GetType() const																{ return T_Enum; }

	const char*				name;
	EnumEntry*				enumEntry;
};


#endif