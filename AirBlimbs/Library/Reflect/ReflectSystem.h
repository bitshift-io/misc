#ifndef _REFLECTSYSTEM_H_
#define _REFLECTSYSTEM_H_

// Run Time Type Info
// heavily influenced from:
// http://www.garret.ru/~knizhnik/cppreflection/docs/reflect.html

#include "Template/Singleton.h"
#include <list>

using namespace std;

class ClassDesc;

#define gReflectSystem ReflectSystem::GetInstance()


class ReflectSystem : public Singleton<ReflectSystem>
{
public:

	typedef list<const ClassDesc*>::iterator ClassDescIterator;

	void					Register(const ClassDesc* classDesc);

	ClassDescIterator		Begin()			{ return mClassDesc.begin(); }
	ClassDescIterator		End()			{ return mClassDesc.end(); }

	const ClassDesc*		GetClassDescByName(const char* name);

	void*					Clone(void* instance, ClassDesc* desc);

protected:

	list<const ClassDesc*>		mClassDesc;
};

#endif