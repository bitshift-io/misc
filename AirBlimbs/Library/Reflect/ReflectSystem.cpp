#include "ReflectSystem.h"
#include "Class.h"

void ReflectSystem::Register(const ClassDesc* classDesc)
{
	mClassDesc.push_back(classDesc);
}

const ClassDesc* ReflectSystem::GetClassDescByName(const char* name)
{
	ClassDescIterator it;
	for (it = Begin(); it != End(); ++it)
	{
		if (strcmpi(name, (*it)->name) == 0)
			return (*it);
	}

	return 0;
}

void* ReflectSystem::Clone(void* instance, ClassDesc* desc)
{
	void* clone = desc->Create();

	while (desc)
	{
		VariableDesc* varDesc = desc->variable;
		while (varDesc)
		{
			if (varDesc->type->GetType() != T_STL) // HACK!
				varDesc->type->LoadBinary((unsigned char*)clone + varDesc->offset, (unsigned char*)instance + varDesc->offset);
			varDesc = varDesc->next;
		}

		desc = desc->parent;
	}

	return clone;
}