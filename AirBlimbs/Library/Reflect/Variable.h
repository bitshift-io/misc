#ifndef _VARIABLE_H_
#define _VARIABLE_H_

class TypeDesc;

class VariableDesc
{
public:

	VariableDesc(char const* _name, long int _offset, unsigned int _size, unsigned int _flags, TypeDesc* _type);
    VariableDesc& operator,(VariableDesc& field);

	TypeDesc*		type;
	const char*		name;	
	long int		offset;
	unsigned int	flags;
	unsigned int	size;

	VariableDesc*	next;
    VariableDesc**	chain;
};

#define VAR(x) \
	*(new VariableDesc(#x, (char*)&x-(char*)this, sizeof(x), 0, TypeOf(x)))

#define VAR_F(x, flags) \
	*(new VariableDesc(#x, (char*)&x-(char*)this, sizeof(x), flags, TypeOf(x)))

#define VAR_PTR(x) \
    *(new VariableDesc(#x, (char*)&x-(char*)this, sizeof(x), 0, TypeOfPtr(&x)))

#define VAR_PTR_F(x, flags) \
    *(new VariableDesc(#x, (char*)&x-(char*)this, sizeof(x), flags, TypeOfPtr(&x)))

#endif