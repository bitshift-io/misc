#include "RenderHandler.h"
#include "Render/RenderFactory.h"
#include "Render/Device.h"
#include "Render/Geometry.h"
#include "Render/Material.h"
#include "Render/Window.h"
#include "Render/Texture.h"
#include "Render/Camera.h"
#include "File/Log.h"

//#define FORCEFLUSHTEST

const unsigned int cMaxBufferSize = 15000;

enum TechniqueID
{
	TID_Texture,
	TID_Colour,
};

void FillStyle::set_bitmap(const gameswf::bitmap_info* bi, const gameswf::matrix& m, gameswf::render_handler::bitmap_wrap_mode wm, const gameswf::cxform& color_transform)
{
	mBitmapInfo = (BitmapInfo*)bi;

	float invWidth = 1.0f / mBitmapInfo->mWidth;
	float invHeight = 1.0f / mBitmapInfo->mHeight;

	mMatrix.SetZero();
	mMatrix.m[0] = m.m_[0][0];// * invWidth;
	mMatrix.m[1] = m.m_[1][0];// * invWidth;
	mMatrix.m[4] = m.m_[0][1];// * invHeight;
	mMatrix.m[5] = m.m_[1][1];// * invHeight;
	mMatrix.m[10] = 1.f;
	mMatrix.m[12] = m.m_[0][2];// * invWidth;
	mMatrix.m[13] = m.m_[1][2];// * invHeight;
	mMatrix.m[15] = 1.f;
}




bool RenderHandler::Init(RenderFactory* factory, Device* device)
{
	mFactory = factory;

	mMaskEnabled = false;
	mFlashToEngine.SetIdentity();
	mMatrix.SetIdentity();
	//mCXForm.s

	mDevice = device;
	GenerateGeometryBuffer(factory, device, cMaxBufferSize); // cMaxBufferSize polys at most

	// load the material
	mMaterial = factory->Load<Material>(&ObjectLoadParams(factory, device, "Flash"));
	//mMaterial = static_cast<Material*>(params->mFactory->Create(Material::Type));
	if (!mMaterial) //->Load(params, 0))
	{
		Log::Print("Failed to load flash material: %s\n", "Flash");
		return false;
	}

	// set up technique id's so we dont need to do strcmpi's while rendering ;)
	Material::TechniqueIterator techIt;	
	for (techIt = mMaterial->TechniqueBegin(); techIt != mMaterial->TechniqueEnd(); ++techIt)
	{
		if (strcmpi(techIt->GetName(), "Texture") == 0)
		{
			mTextureParam = techIt->AddParameterByName("diffuseTexture");
			mTextureMatrixParam = techIt->AddParameterByName("textureMatrix");
			mColourParam[TID_Texture] = techIt->AddParameterByName("colour");
			if (mColourParam[TID_Texture])
				mColourParam[TID_Texture]->SetValue(&Vector4(1.f, 1.f, 1.f, 1.f));

			techIt->SetID(TID_Texture);
		}
		else
		{
			mColourParam[TID_Colour] = techIt->AddParameterByName("colour");
			if (mColourParam[TID_Colour])
				mColourParam[TID_Colour]->SetValue(&Vector4(1.f, 1.f, 1.f, 1.f));

			techIt->SetID(TID_Colour);
		}
	}

	return true;
}

void RenderHandler::GenerateGeometryBuffer(RenderFactory* factory, Device* device, int size)
{
	mGeometry = (Geometry*)factory->Create(Geometry::Type);
	mGeometry->Init(device, VF_Position /*| VF_Colour | VF_TexCoord0*/, size * 4, size * 6, RBU_Dynamic);
	VertexFrame& frame = mGeometry->GetVertexFrame();
	RenderBuffer* idxBuffer = mGeometry->GetIndexBuffer();

	// set up indices
	RenderBufferLock lock;
	idxBuffer->Lock(lock);
	for (int i = 0; i < size; ++i)
		lock.uintData[i] = i;

	idxBuffer->Unlock(lock);

	// lock the position buffer, this is always locked, its only unlocked to render!
	frame.mPosition->Lock(mPositionLock);
	mPositionLock.count = 0;
	mBufferPolyCount = 0;
}

void RenderHandler::Deinit(RenderFactory* factory)
{
	factory->Release((SceneObject**)&mMaterial);
	factory->Release((SceneObject**)&mGeometry);
}

gameswf::bitmap_info* RenderHandler::create_bitmap_info_empty() 
{ 
	return new BitmapInfo;
}

gameswf::bitmap_info* RenderHandler::create_bitmap_info_alpha(int w, int h, unsigned char* data) 
{
	return new BitmapInfo;
}

gameswf::bitmap_info* RenderHandler::create_bitmap_info_rgb(image::rgb* im) 
{
	BitmapInfo* bmp = new BitmapInfo;
	bmp->mWidth = im->m_width;
	bmp->mHeight = im->m_height;
	//bmp->m_suspended_image = im;

	Texture* texture = (Texture*)mFactory->Create(Texture::Type);
	bmp->texture = texture;
	if (!texture->Init(mDevice, im->m_data, im->m_type == image::image_base::RGB ? 3 : 4, im->m_width, im->m_height, 0))
	{
		Log::Error("[RenderHandler::create_bitmap_info_rgba] failed to init texture");
		return 0;
	}

	return bmp;
}

gameswf::bitmap_info* RenderHandler::create_bitmap_info_rgba(image::rgba* im) 
{
	BitmapInfo* bmp = new BitmapInfo;
	bmp->mWidth = im->m_width;
	bmp->mHeight = im->m_height;
	//bmp->m_suspended_image = im;

	Texture* texture = (Texture*)mFactory->Create(Texture::Type);
	bmp->texture = texture;
	if (!texture->Init(mDevice, im->m_data, im->m_type == image::image_base::RGB ? 3 : 4, im->m_width, im->m_height, 0))
	{
		Log::Error("[RenderHandler::create_bitmap_info_rgba] failed to init texture");
		return 0;
	}

	return bmp;
}

gameswf::video_handler*	RenderHandler::create_video_handler()
{
	return 0;
}

void RenderHandler::draw_bitmap(
		const gameswf::matrix&		m,
		gameswf::bitmap_info*	bi,
		const gameswf::rect&		coords,
		const gameswf::rect&		uv_coords,
		gameswf::rgba			color)
{
	int nothing = 0;
}

void RenderHandler::begin_display(
		gameswf::rgba background_color,
		int viewport_x0, int viewport_y0,
		int viewport_width, int viewport_height,
		float x0, float x1, float y0, float y1)
{
	// need to change this so it centers the flash even with stretched screen
	WindowResolution res;
	mDevice->GetWindow()->GetWindowResolution(res);
	float actualAspectRatio = mDevice->GetWindow()->GetAspectRatio();
	float flashAspectRatio = mFlashWidth / mFlashHeight;

	mFlashToEngine.SetIdentity();
	mFlashToEngine.SetScale(2.f / (x1 - x0), -2.f / (y1 - y0), 1.f);
	mFlashToEngine.m[0] *= (flashAspectRatio / actualAspectRatio);

	mFlashToEngine.SetTranslation(-1.f, 1.f, 0.f);
	mFlashToEngine.m[12] *= (flashAspectRatio / actualAspectRatio);
}

void RenderHandler::end_display()
{
	FlushRenderBuffer();
}

void RenderHandler::set_matrix(const gameswf::matrix& m)
{
	/// matrix change results in buffer flush
	Matrix4 newMatrix(MI_Zero);
	newMatrix.m[0] = m.m_[0][0];
	newMatrix.m[1] = m.m_[1][0];
	newMatrix.m[4] = m.m_[0][1];
	newMatrix.m[5] = m.m_[1][1];
	newMatrix.m[10] = 1.f;
	newMatrix.m[12] = m.m_[0][2];
	newMatrix.m[13] = m.m_[1][2];
	newMatrix.m[15] = 1.f;

	mMatrix = newMatrix;
}

void RenderHandler::set_cxform(const gameswf::cxform& cx)
{
	bool bSame = true;
	for (int j = 0; j < 4; ++j)
	{
		for (int i = 0; i < 2; ++i)		
		{
			if (cx.m_[j][i] != mCXForm.m_[j][i])
			{
				bSame = false;
				break;
			}
		}
	}

	if (bSame)
		return;

	FlushRenderBuffer();
	mCXForm = cx;
}

void RenderHandler::draw_line_strip(const void* coords, int vertex_count)
{
#ifdef FORCEFLUSHTEST
	FlushRenderBuffer(); // test
#endif

	Sint16* vertCoords = (Sint16*)coords;
	Window* window = mDevice->GetWindow();

	MaterialTechnique* colourTechnique = mMaterial->GetTechniqueByID(TID_Colour);
	if (mMaterial->GetCurrentTechnique() != colourTechnique)
		FlushRenderBuffer();

	mMaterial->SetCurrentTechnique(colourTechnique);

	gameswf::rgba col = mFillStyle[LINE_STYLE].get_color();

	// no alpha? no drawies! wee!
	if (!mMaskEnabled && col.m_a == 0)
		return;

	if (mMaskEnabled)
		col.m_a = 0;

	Vector4 colour = Vector4((float)col.m_r / 255.f, (float)col.m_g / 255.f, (float)col.m_b / 255.f, (float)col.m_a / 255.f);

	Vector4 oldColour;
	mColourParam[TID_Colour]->GetValue(&oldColour);
	if (oldColour != colour)
		FlushRenderBuffer();

	mColourParam[TID_Colour]->SetValue(&colour);


	Vector4 firstVert(vertCoords[0], vertCoords[1], 0.f);

	// determine if the last vertex connects to the first, in which case we have a looping line,
	// and we need to calulate the last line angle
	Vector4 lastLineCross;
	bool lastVertsConnect = false;
	Vector4 lastVert(vertCoords[(vertex_count - 1) * 2], vertCoords[(vertex_count - 1) * 2 + 1], 0.f);
	if (lastVert == firstVert)
	{
		lastVertsConnect = true;

		// closed line shape, so calculate last cross dir
		Vector4 nextVert(vertCoords[1 * 2], vertCoords[1 * 2 + 1], 0.f);
		Vector4 secondLastVert(vertCoords[(vertex_count - 2) * 2], vertCoords[(vertex_count - 2) * 2 + 1], 0.f);

		Vector4 vLastDir = lastVert - secondLastVert; // dis right?
		vLastDir.Normalize();

		Vector4 vDir = nextVert - firstVert;
		vDir.Normalize();

		lastLineCross = vDir - vLastDir;
		lastLineCross.Normalize();
		
		float angle = vDir.Angle(lastLineCross);
		float length = mFillStyle[LINE_STYLE].mLineWidth / sinf(angle * 0.5f);
		lastLineCross *= length;

		lastLineCross = -lastLineCross; // due to vLastDir being reversed
	}
	else
	{
		// not a closed line shape, so the start of the line, is square
		Vector4 finalVert(vertCoords[1 * 2], vertCoords[1 * 2 + 1], 0.f);

		Vector4 vDir = finalVert - firstVert;
		vDir.Normalize();

		lastLineCross = vDir.Cross(Vector4(0.f, 0.f, 1.f));
		lastLineCross *= mFillStyle[LINE_STYLE].mLineWidth;
	}

	//lastLineCross.x *= 1.f / float(window->GetWidth())/* * mFlashToEngine.m[0]*/; 
	//lastLineCross.y *= 1.f / float(window->GetHeight())/* * -mFlashToEngine.m[5]*/;

	//int t = 0;
	for (int v = 1; v < vertex_count; ++v, mBufferPolyCount += 6)
	{
		Vector4 nextVert(vertCoords[v * 2], vertCoords[v * 2 + 1], 0.f);

		// generate a quad from these 2 verts
		Vector4 vDir = nextVert - firstVert;
		vDir.Normalize();

		// what we need to do is to get the angle between this line and the next line, 
		Vector4 lineCross;
		if (v < (vertex_count - 1))
		{
			Vector4 nextNextVert(vertCoords[(v + 1) * 2], vertCoords[(v + 1) * 2 + 1], 0.f);

			Vector4 vNextDir = nextNextVert - nextVert;
			vNextDir.Normalize();

			lineCross = vDir - vNextDir;
			lineCross.Normalize();

			// we need to know if we are rotating to the left or right of the current
			// line so we can invert the line length
			Vector4 vRightDir = vDir.Cross(Vector4(0.f, 0.f, 1.f, 1.f));
			float rotatingDirection = vRightDir.Dot(vNextDir) < 0.f ? 1.f : -1.f;

			float angle = vDir.Angle(lineCross);
			float length = mFillStyle[LINE_STYLE].mLineWidth / sinf(angle);
			lineCross *= length * rotatingDirection;
		}
		else if (lastVertsConnect)
		{
			// wrap to first vert
			Vector4 nextNextVert(vertCoords[1 * 2], vertCoords[1 * 2 + 1], 0.f);

			Vector4 vNextDir = nextNextVert - nextVert;
			vNextDir.Normalize();

			lineCross = vDir - vNextDir;
			lineCross.Normalize();

			// we need to know if we are rotating to the left or right of the current
			// line so we can invert the line length
			Vector4 vRightDir = vDir.Cross(Vector4(0.f, 0.f, 1.f, 1.f));
			float rotatingDirection = vRightDir.Dot(vNextDir) < 0.f ? 1.f : -1.f;

			float angle = vDir.Angle(lineCross);
			float length = mFillStyle[LINE_STYLE].mLineWidth / sinf(angle);
			lineCross *= length * rotatingDirection;
		}
		else
		{
			// end of line vert, neds to be square
			lineCross = vDir.Cross(Vector4(0.f, 0.f, 1.f));
			lineCross *= mFillStyle[LINE_STYLE].mLineWidth;
		}

		//lineCross.x *= 1.f / float(window->GetWidth())/* * mFlashToEngine.m[0]*/;
		//lineCross.y *= 1.f / float(window->GetHeight())/* * -mFlashToEngine.m[5]*/;

		Vector4 quadVert[4];
		quadVert[0] = firstVert + lastLineCross;
		quadVert[1] = firstVert - lastLineCross;
		quadVert[2] = nextVert - lineCross;
		quadVert[3] = nextVert + lineCross;

		// first tri
		SetBufferPosition(mBufferPolyCount + 0, quadVert[0]);
		SetBufferPosition(mBufferPolyCount + 1, quadVert[1]);
		SetBufferPosition(mBufferPolyCount + 2, quadVert[2]);

		// second tri
		SetBufferPosition(mBufferPolyCount + 3, quadVert[0]);
		SetBufferPosition(mBufferPolyCount + 4, quadVert[2]);
		SetBufferPosition(mBufferPolyCount + 5, quadVert[3]);

		lastLineCross = lineCross;
		firstVert = nextVert;

		// we filled the buffer
		// so end this batch, render it and relock ro start filling from scratch
		if ((mBufferPolyCount + 6) >= cMaxBufferSize)
			FlushRenderBuffer();
	}
}

void RenderHandler::draw_mesh_strip(const void* coords, int vertex_count)
{
#ifdef FORCEFLUSHTEST
	FlushRenderBuffer(); // test
#endif

	Sint16* vertCoords = (Sint16*)coords;

	// Set up current style.
	Vector4 colour(1.0f, 1.f, 1.f, 1.f);
	EffectParameterValue* colourParam = 0;
	switch (mFillStyle[LEFT_STYLE].mMode)
	{
	case FillStyle::M_Disabled:
		return;

	case FillStyle::M_Bitmap:
		{
			Texture* oldTexture = 0;
			if (mTextureParam)				
				mTextureParam->GetValue(&oldTexture);

			BitmapInfo* bmp = (BitmapInfo*)mFillStyle[LEFT_STYLE].mBitmapInfo;
			MaterialTechnique* textureTechnique = mMaterial->GetTechniqueByID(TID_Texture);

			// this seems wrong being dependant only on texture size? wheres the x2 from? my ass?
			float	invWidth = (1.0f / bmp->mWidth);// * 2.f;
			float	invHeight = (1.0f / bmp->mHeight);// * 2.f;

			Matrix4 texMat(MI_Identity);
			Matrix4& styleMat = mFillStyle[LEFT_STYLE].mMatrix;
			texMat = styleMat;// * mFlashToEngine; // inverse out the current matrix

			// because we transform the verts on the cpu for performance reasons
			// we need to take that in to account for the texture matrix
			Matrix4 worldToLocal = mMatrix;
			worldToLocal.Inverse();
			texMat = worldToLocal * texMat;


			texMat.m[0] *= invWidth;
			texMat.m[1] *= invWidth;
			texMat.m[2] *= invWidth;

			texMat.m[4] *= invHeight;
			texMat.m[5] *= invHeight;
			texMat.m[6] *= invHeight;

			texMat.m[12] *= invWidth;
			texMat.m[13] *= invHeight;

			Matrix4 oldMatrix;
			if (mTextureMatrixParam)
				mTextureMatrixParam->GetValue(&oldMatrix);
			
			if (mMaterial->GetCurrentTechnique() != textureTechnique
				|| oldTexture != bmp->texture
				|| oldMatrix != texMat)
				FlushRenderBuffer();

			mMaterial->SetCurrentTechnique(textureTechnique);
			
			// if no texture param we still draw, but it will proably just draw as vertex coloured box
			if (mTextureParam)
				mTextureParam->SetValue(&(bmp->texture));

			if (mTextureMatrixParam)
				mTextureMatrixParam->SetValue(&texMat);

			colourParam = mColourParam[TID_Texture];
			
			//mFillStyle[LEFT_STYLE].set_color(gameswf::rgba(255, 255, 255, 255));
			fill_style_color(0, gameswf::rgba(255, 255, 255, 255));
		}
		break;

	case FillStyle::M_Colour:
		{
			MaterialTechnique* colourTechnique = mMaterial->GetTechniqueByID(TID_Colour);
			if (mMaterial->GetCurrentTechnique() != colourTechnique)
				FlushRenderBuffer();

			mMaterial->SetCurrentTechnique(colourTechnique);
			//mTextureParam->SetValue((Texture*)0);
			colourParam = mColourParam[TID_Colour];
		}
		break;
	}

	gameswf::rgba col = mFillStyle[LEFT_STYLE].get_color();

	// no alpha? no drawies! wee!
	if (!mMaskEnabled && col.m_a == 0)
		return;

	if (mMaskEnabled)
		col.m_a = 0;

	colour = Vector4((float)col.m_r / 255.f, (float)col.m_g / 255.f, (float)col.m_b / 255.f, (float)col.m_a / 255.f);

	Vector4 oldColour;
	if (colourParam)
		colourParam->GetValue(&oldColour);

	if (oldColour != colour)
		FlushRenderBuffer();

	if (colourParam)
		colourParam->SetValue(&colour);

	Vector4 firstVert(vertCoords[0], vertCoords[1], 0.f);
	Vector4 secondVert(vertCoords[2], vertCoords[3], 0.f);


	bool bWind = true;
	for (int v = 2; v < vertex_count; ++v, mBufferPolyCount += 3)
	{
		Vector4 finalVert(vertCoords[v * 2], vertCoords[v * 2 + 1], 0.f);

		if (bWind)
		{
			SetBufferPosition(mBufferPolyCount + 0, firstVert);
			SetBufferPosition(mBufferPolyCount + 1, secondVert);
			SetBufferPosition(mBufferPolyCount + 2, finalVert);
		}
		else
		{
			SetBufferPosition(mBufferPolyCount + 2, firstVert);
			SetBufferPosition(mBufferPolyCount + 1, secondVert);
			SetBufferPosition(mBufferPolyCount + 0, finalVert);
		}

		bWind = !bWind;

		firstVert = secondVert;
		secondVert = finalVert;

		// we filled the buffer
		// so end this batch, render it and relock ro start filling from scratch
		if ((mBufferPolyCount + 3) >= cMaxBufferSize)
			FlushRenderBuffer();
	}
}

void RenderHandler::fill_style_bitmap(int fill_side, gameswf::bitmap_info* bi, const gameswf::matrix& m, bitmap_wrap_mode wm) 
{
	/// colour bitmap results in buffer flush
	assert(fill_side >= 0 && fill_side < 2);
/*
	if (mFillStyle[fill_side].mMode == FillStyle::M_Bitmap && mFillStyle[fill_side].mBitmapInfo == (BitmapInfo*)bi)
		return;

	FlushRenderBuffer();
*/
	mFillStyle[fill_side].mMode = FillStyle::M_Bitmap;
	mFillStyle[fill_side].set_bitmap(bi, m, wm, mCXForm);
}

void RenderHandler::fill_style_color(int fill_side, const gameswf::rgba& color)
{
	gameswf::rgba newColour = mCXForm.transform(color);
	/*
	if (mFillStyle[fill_side].get_color().m_a == newColour.m_a 
		&& mFillStyle[fill_side].get_color().m_r == newColour.m_r
		&& mFillStyle[fill_side].get_color().m_g == newColour.m_g
		&& mFillStyle[fill_side].get_color().m_b == newColour.m_b
		&& mFillStyle[fill_side].mMode == FillStyle::M_Colour)
		return;

	FlushRenderBuffer();*/

	mFillStyle[fill_side].mMode = FillStyle::M_Colour;
	mFillStyle[fill_side].set_color(newColour);
}

void RenderHandler::fill_style_disable(int fill_side)
{
	mFillStyle[fill_side].mMode = FillStyle::M_Disabled;
}

void RenderHandler::line_style_disable()
{
	mFillStyle[LINE_STYLE].mMode = FillStyle::M_Disabled;
}

void RenderHandler::line_style_color(gameswf::rgba color)
{
	/// colour change results in buffer flush
	gameswf::rgba newColour = mCXForm.transform(color);
	/*
	if (mFillStyle[LINE_STYLE].get_color().m_a == newColour.m_a 
		&& mFillStyle[LINE_STYLE].get_color().m_r == newColour.m_r
		&& mFillStyle[LINE_STYLE].get_color().m_g == newColour.m_g
		&& mFillStyle[LINE_STYLE].get_color().m_b == newColour.m_b
		&& mFillStyle[LINE_STYLE].mMode == FillStyle::M_Colour)
		return;

	FlushRenderBuffer();
*/
	mFillStyle[LINE_STYLE].mMode = FillStyle::M_Colour;
	mFillStyle[LINE_STYLE].set_color(newColour);
}

void RenderHandler::line_style_width(float width) 
{
	mFillStyle[LINE_STYLE].mLineWidth = width * 0.5f; // * 200;/// * 100000;// / 20; // why divide by 20? flash funk
}

void RenderHandler::FlushRenderBuffer()
{
	if (mBufferPolyCount <= 0)
		return;

	// unlock the buffers
	VertexFrame& frame = mGeometry->GetVertexFrame();
	mPositionLock.count = mBufferPolyCount;
	frame.mPosition->Unlock(mPositionLock);
	//mGeometry->Unlock(VF_Position, 0, mBufferPolyCount);

	Matrix4 transform = /*mMatrix * */mFlashToEngine;
	Window* window = mDevice->GetWindow();
	Camera cam;
	cam.SetOrthographic(2.f, 2.f); //window->GetWidth(), window->GetHeight());
	cam.SetAspectRatio(mDevice->GetWindow()->GetAspectRatio());

	mMaterial->Begin();
	//mMaterial->BeginPass(mDevice, 0);

	VertexFrame& buffer = mGeometry->GetVertexFrame();

	// draw
	mMaterial->BeginPass(0);
	mMaterial->GetEffect()->SetMatrixParameters(cam.GetProjection(), cam.GetView(), cam.GetWorld(), transform); //
	mMaterial->SetPassParameter();
	mGeometry->Draw(0, mBufferPolyCount);
	mMaterial->EndPass();
	mMaterial->End();	

	// lock the buffers
	frame.mPosition->Lock(mPositionLock);
	mPositionLock.count = 0;

	mBufferPolyCount = 0;
}

void RenderHandler::begin_submit_mask() 
{
	// NOTE: this doesnt been programmed to handle multiple masks in 1 scene
	// dont think it will work correctly!
	FlushRenderBuffer();
	mMaskEnabled = true;

	Material::TechniqueIterator techIt;
	for (techIt = mMaterial->TechniqueBegin(); techIt != mMaterial->TechniqueEnd(); ++techIt)
	{
		MaterialTechnique::PassIterator passIt;
		for (passIt = techIt->PassBegin(); passIt != techIt->PassEnd(); ++passIt)
		{
			DeviceState& state = passIt->mDeviceState;
			state.stencilEnable = true;
			state.stencilReadMask = -1;
			state.stencilWriteMask = -1;
			for (int i = 0; i < 2; ++i)
			{
				state.stencilFace[i].compare = C_Always;
				state.stencilFace[i].pass = O_Increment;
				state.stencilFace[i].fail = O_Increment;
				state.stencilFace[i].depthFail = O_Increment;
			}
		}
	}
}

void RenderHandler::end_submit_mask() 
{
	FlushRenderBuffer();

	mMaskEnabled = false;

	Material::TechniqueIterator techIt;
	for (techIt = mMaterial->TechniqueBegin(); techIt != mMaterial->TechniqueEnd(); ++techIt)
	{
		MaterialTechnique::PassIterator passIt;
		for (passIt = techIt->PassBegin(); passIt != techIt->PassEnd(); ++passIt)
		{
			DeviceState& state = passIt->mDeviceState;
			state.stencilReadMask = 1;
			state.stencilWriteMask = -1;
			for (int i = 0; i < 2; ++i)
			{
				state.stencilFace[i].compare = C_Equal;
				state.stencilFace[i].pass = O_Keep;
				state.stencilFace[i].fail = O_Keep;
				state.stencilFace[i].depthFail = O_Keep;
			}
		}
	}
}

void RenderHandler::disable_mask() 
{
	FlushRenderBuffer();

	Material::TechniqueIterator techIt;
	for (techIt = mMaterial->TechniqueBegin(); techIt != mMaterial->TechniqueEnd(); ++techIt)
	{
		MaterialTechnique::PassIterator passIt;
		for (passIt = techIt->PassBegin(); passIt != techIt->PassEnd(); ++passIt)
		{
			DeviceState& state = passIt->mDeviceState;
			state.stencilEnable = false;
		}
	}
}