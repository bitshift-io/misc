#ifndef _SEMAPHORE_H_
#define _SEMAPHORE_H_

#ifdef WIN32
    #define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
    #define NOMINMAX
    //#define UNICODE
    #include <windows.h>
#else
    #include <pthread.h>
#endif

class Semaphore
{
public:

	Semaphore() :
		mHandle(0)
	{
	}

	void Init(int count = 1, int initialReleased = -1);
	void Deinit();

	bool Release(int count = 1);
	void Acquire(unsigned long timeout = -1);

protected:

	 #ifdef WIN32
        HANDLE mHandle;
    #else
        pthread_mutex_t mHandle;
    #endif
};

typedef Semaphore Mutex;

#endif
