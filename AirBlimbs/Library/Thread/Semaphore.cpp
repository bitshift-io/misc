#include "Semaphore.h"

void Semaphore::Init(int count, int initialReleased)
{
	if (mHandle)
		return;

	if (initialReleased == -1)
		initialReleased = count;

	mHandle = CreateSemaphore(0, initialReleased, count, 0);
}

void Semaphore::Deinit()
{
	CloseHandle(mHandle);
}

bool Semaphore::Release(int count)
{
	return ReleaseSemaphore(mHandle, count, NULL) != FALSE;
}

void Semaphore::Acquire(unsigned long timeout)
{
	WaitForSingleObject(mHandle, timeout == -1 ? INFINITE : timeout);
}
