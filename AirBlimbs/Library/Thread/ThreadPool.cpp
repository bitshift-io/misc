#include "ThreadPool.h"
#include "File/Log.h"
#include "Math/Math.h"

//unsigned long WaitTask(void* ptr, Thread* thread)
//{
//	gThreadPool.Wait();
//	return 0;
//}

unsigned long ThreadPool::StaticTaskFn(void* ptr, Thread* thread)
{
	return gThreadPool.TaskFn(ptr, thread);
}



void ThreadPool::Init(int numThreads)
{
	// check not already init'ed
	if (mThreads.size())
		return;

	#ifdef WIN32
		SYSTEM_INFO info;
		GetSystemInfo(&info);
		mNumLogicalCores = info.dwNumberOfProcessors;
	#else
		mNumLogicalCores = 1;
	#endif
	
	if (numThreads == -1)
		numThreads = GetNumCPULogicalCores();
 
	mActiveThread = 0;
	mSemaphore.Init(Math::IntMax, 0);
	mActiveThreadMutex.Init(1);

	mDefaultThreadTask = Task(ThreadPool::StaticTaskFn, 0);

	mThreads.resize(numThreads);
	for (int i = 0; i < numThreads; ++i)
	{
		mThreads[i].Init();
		mThreads[i].SetTask(&mDefaultThreadTask);
		mThreads[i].Resume();
	}
}

void ThreadPool::Deinit()
{
	for (unsigned int i = 0; i < mThreads.size(); ++i)
	{
		mThreads[i].Suspend();
		mThreads[i].Deinit();
	}

	while (mTaskPool.size())
	{
		delete mTaskPool.front();
		mTaskPool.pop();
	}

	mSemaphore.Deinit();
}

void ThreadPool::AddTasksToAdd()
{
	while (mTasksToAdd.size())
	{
		Task* task = mTasksToAdd.front();
		mTasksToAdd.pop();
		AddTask(task);
	}
}

void ThreadPool::Wait()
{
	while (mTasks.size() || mActiveThread > 0)
	{
		Sleep(0); // switch to active thread
	}
}

Task* ThreadPool::CreateTask(unsigned int taskFlags)
{
	if (mTaskPool.size() <= 0)
	{
		Task* task = new Task;
		task->mFlags = taskFlags;
		return task;
	}

	mActiveThreadMutex.Acquire();
	Task* task = mTaskPool.front();
	task->mFlags = taskFlags;
	mTaskPool.pop();
	mActiveThreadMutex.Release();
	return task;
}

void ThreadPool::AddTask(Task* task)
{
	mActiveThreadMutex.Acquire();
	mTasks.push(task);
	mSemaphore.Release();
	mActiveThreadMutex.Release();
}

Task* ThreadPool::PopLastTask()
{
	if (mTasks.size() <= 0)
		return 0;

	Task* task = mTasks.front();
	mTasks.pop();
	return task;
}

unsigned int ThreadPool::GetNumCPULogicalCores()
{
	return mNumLogicalCores;
}

unsigned long ThreadPool::TaskFn(void* ptr, Thread* thread)
{
	for (;;)
	{
		mSemaphore.Acquire();

		mActiveThreadMutex.Acquire();
		++mActiveThread;
		Task* task = PopLastTask();
		mActiveThreadMutex.Release();
		
		task->mTaskFn(task->mPtr, thread);

		mActiveThreadMutex.Acquire();
		if (task->mFlags & TF_AddWhenDone)
			mTasksToAdd.push(task);
		else if (task->mFlags & TF_DeleteWhenDone)
			mTaskPool.push(task);

		--mActiveThread;
		mActiveThreadMutex.Release();
	}
	return 0;
}
