#ifndef _THREAD_H_
#define _THREAD_H_

#ifdef WIN32
    #define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
    #define NOMINMAX
    //#define UNICODE
    #include <windows.h>
#else
    #include <pthread.h>
#endif

class Thread;

enum ThreadPriority
{
	TP_RealTime,
	TP_Highest,
	TP_High,
	TP_Normal,
	TP_Low,
	TP_Lowest,
	TP_Background,
};

enum TaskFlags
{
	TF_AddWhenDone		= 0x1 << 0,
	TF_DeleteWhenDone	= 0x1 << 1,
};

class Task
{
public:
	typedef unsigned long (*TaskFn)(void*, Thread*);

	Task() : mTaskFn(StaticDoTask), mFlags(0)
	{
		mPtr = this;
	}

	Task(TaskFn task, void* ptr = 0)
	{
		mTaskFn = task;
		mPtr = ptr;
	}

	static unsigned long StaticDoTask(void* ptr, Thread* thread)
	{
		Task* task = (Task*)ptr;
		return task->DoTask();
	}

	virtual unsigned long DoTask()
	{
		return 0;
	}

	void SetTask(TaskFn fn, void* ptr = 0)
	{
		mTaskFn = fn;
		mPtr = ptr;
	}

	TaskFn			mTaskFn;
	void*			mPtr;
	unsigned int	mFlags;
};

class Thread
{
    #ifndef WIN32
        friend void* ThreadFn(void* ptr);
    #endif

public:

	void Init();
	void Deinit();

	void SetPriority(ThreadPriority priority);

	void SetTask(Task* task = 0);
	bool Wait(unsigned long timeout = -1); // returns true if timed out, false otherwise

	void Suspend();
	bool Resume(); // returns true if resumed

	bool EndThread();

//protected:
	virtual unsigned long Update();

    #ifdef WIN32
        static DWORD WINAPI ThreadFn(LPVOID ptr);
    #else
        static void* ThreadFn(void* ptr);
    #endif

	bool			mEndThread;
	Task*			mTask;

	#ifdef WIN32
        HANDLE			mThreadHandle;
    #else
        pthread_t       mThreadHandle;
    #endif
};

#endif
