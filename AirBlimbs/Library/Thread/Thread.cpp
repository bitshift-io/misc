#include "Thread.h"

#ifdef WIN32
DWORD WINAPI Thread::ThreadFn(LPVOID ptr)
{
	Thread* thread = static_cast<Thread*>(ptr);
	unsigned long result;
	while ((result = thread->Update()) != -1 && !thread->EndThread())
	{
	}

	return result;
}
#else
void* ThreadFn(void* ptr)
{
    Thread* thread = static_cast<Thread*>(ptr);
	thread->Update();
	return 0;
}
#endif

void Thread::Init()
{
	mEndThread = false;
	mThreadHandle = 0;
	mTask = 0;

	#ifdef WIN32
        mThreadHandle = CreateThread(0, 0, Thread::ThreadFn, this, CREATE_SUSPENDED, 0);
    #else
        pthread_create(&mThreadHandle, NULL, Thread::ThreadFn, this);
    #endif
}

void Thread::Deinit()
{
	mEndThread = true;
	#ifdef WIN32
		Wait();
		BOOL result = CloseHandle(mThreadHandle);
	#else
	#endif
}

void Thread::SetPriority(ThreadPriority priority)
{
	int iPriority = 0;
	switch (priority)
	{
	case TP_RealTime:
		iPriority = THREAD_PRIORITY_TIME_CRITICAL;
		break;

	case TP_Highest:
		iPriority = THREAD_PRIORITY_HIGHEST;
		break;

	case TP_High:
		iPriority = THREAD_PRIORITY_ABOVE_NORMAL;
		break;

	case TP_Normal:
		iPriority = THREAD_PRIORITY_NORMAL;
		break;

	case TP_Low:
		iPriority = THREAD_PRIORITY_BELOW_NORMAL;
		break;

	case TP_Lowest:
		iPriority = THREAD_PRIORITY_LOWEST;
		break;

	case TP_Background:
		iPriority = THREAD_MODE_BACKGROUND_BEGIN;
		break;
	}

	SetThreadPriority(mThreadHandle, iPriority);

}

void Thread::SetTask(Task* task)
{
	mTask = task;
}

void Thread::Suspend()
{
#ifdef WIN32
	DWORD result = SuspendThread(mThreadHandle);
#endif
}

bool Thread::Resume()
{
#ifdef WIN32
	return ResumeThread(mThreadHandle) == 1;
#endif	
}

bool Thread::EndThread()
{
	return mEndThread;
}

bool Thread::Wait(unsigned long timeout)
{
#ifdef WIN32
	return WaitForSingleObject(mThreadHandle, timeout == -1 ? INFINITE : timeout) == WAIT_TIMEOUT;
#endif
}

unsigned long Thread::Update()
{
	unsigned long ret = mTask->mTaskFn(mTask->mPtr, this);
	return ret;
}
