#ifndef _TIME_H__
#define _TIME_H__

#include <ctime> // this is std c++, might work under linux and windows :D

namespace Time
{
	// Get Ticks returns time program has been running
	// in ms
	
	inline unsigned long GetMilliseconds()
	{
		unsigned long seconds = clock() / (CLOCKS_PER_SEC / 1000);
		return seconds;
	}

	inline float GetSeconds()
	{
		return float(GetMilliseconds()) / 1000.0f;
	}
};

#endif
