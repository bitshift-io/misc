#ifndef _FRAMEUPDATE_H_
#define _FRAMEUPDATE_H_

//
// The purpose of this class is used to make sure we get a consitent update called
// n time per frame
//
// eg. we have a game which renders every frame, then we want to run some game code
// at 30 time per second regardless of how many fps we are getting
// so we use this class to achieve that
//
class FrameUpdate
{
public:

	FrameUpdate();

	void SetUpdateRate(long time)	{ updateRate = time; }
	long GetUpdateRate()			{ return updateRate; }

	// use this to convert from units per second to units per update
	inline float GetDeltaTime()	{ return 1.f / float(updateRate); }

	// returns the number of times a loop should be called
	// to catch up the physics or game
	long GetNumUpdates()			{ return numUpdatesThisFrame; }

	void Update();
	void Reset();

protected:

	long updateRate;
	long numUpdates;
	long numUpdatesThisFrame;
};

#endif
