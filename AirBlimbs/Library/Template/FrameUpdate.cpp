#include "FrameUpdate.h"
#include "Time.h"
#include "File/Log.h"

FrameUpdate::FrameUpdate()
{
	updateRate = 30;
	numUpdates = 0;
	numUpdatesThisFrame = 0;
}

void FrameUpdate::Reset()
{
	long deltaTime = Time::GetMilliseconds();
	long lastUpdates = numUpdates;
	numUpdates = long(float(deltaTime) / (1000.0f / float(updateRate)));
	numUpdatesThisFrame = 0;
}

void FrameUpdate::Update()
{
	long deltaTime = Time::GetMilliseconds();
	long lastUpdates = numUpdates;
	numUpdates = long(float(deltaTime) / (1000.0f / float(updateRate)));
	numUpdatesThisFrame = numUpdates - lastUpdates;
}