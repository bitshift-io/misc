#include "OGLRenderFactory.h"
#include "OGLDevice.h"
#include "OGLEffect.h"
#include "OGLTexture.h"
#include "OGLRenderTexture.h"
#include "Memory/Memory.h"

#ifdef WIN32
	#include "Render/Win32/Win32Window.h"
	#undef CreateWindow
#elif __APPLE__
    #include "Render/Mac/MacWindow.h"
#else
	#include "Render/Linux/LinuxWindow.h"
#endif

OGLRenderFactory::OGLRenderFactory(const RenderFactoryParameter& parameter) :
	RenderFactory(parameter)
{
	Register(ObjectCreator<OGLEffect>,			Effect::Type);
	Register(ObjectCreator<OGLTexture>,			Texture::Type);
	Register(ObjectCreator<OGLRenderTexture>,	RenderTexture::Type);
}

void OGLRenderFactory::Init()
{

}

void OGLRenderFactory::Deinit()
{

}

Window* OGLRenderFactory::CreateWindow()
{
#ifdef WIN32
	return new Win32Window;
#elif __APPLE__
    return new MacWindow;
#else
	return new LinuxWindow;
#endif
}

Device*	OGLRenderFactory::CreateDevice()
{
	return new OGLDevice;
}

void OGLRenderFactory::ReleaseWindow(Window** window)
{
#ifdef WIN32
	Win32Window* win = static_cast<Win32Window*>(*window);
#elif __APPLE__
    MacWindow* win = static_cast<MacWindow*>(*window);
#else
	LinuxWindow* win = static_cast<LinuxWindow*>(*window);
#endif

    delete win;
	*window = 0;
}

void OGLRenderFactory::ReleaseDevice(Device** device)
{
	OGLDevice* dev = static_cast<OGLDevice*>(*device);
	delete dev;
	*device = 0;
}

void OGLRenderFactory::ReloadResource(Device* device)
{
	OGLEffect::StaticReloadResource();
	RenderFactory::ReloadResource(device);
}
