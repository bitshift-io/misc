#ifndef _OGL_TEXTURE_H_
#define _OGL_TEXTURE_H_

#include "../Texture.h"
#include "OGLDevice.h"

class TextureResource;

class OGLTexture : public Texture
{
	friend class OGLRenderTexture;

public:

	void			Deinit();

	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node = 0);

	virtual bool	Init(Device* device, unsigned char* image, int channels, int width, int height, int depth, int flags = TF_2D | TF_LowDynamicRange);
	virtual bool	Update(unsigned char* data);

	GLint			GetHandle()														{ return mTexture; }

	virtual void	ReloadResource();
	virtual bool	IsInstance(const ObjectLoadParams* params, SceneObject* loaded);

	void			DebugDraw(float x, float y, float size);

	// TODO
	virtual bool	Lock(TextureSubresource& resource, int lockFlags)				{ return false; }
	virtual void	Unlock() {}

	virtual bool	GetSubresource(TextureSubresource& resource)					{ return false; }

	const char*		GetName()														{ return mName.c_str(); }

protected:

	virtual void Create(RenderFactory* factory);

	bool	GenerateTexture(OGLDevice* device, TextureResource* texRes, int flags = 0);
	bool	GenerateCubeTexture(OGLDevice* device, TextureResource* texRes, int flags = 0);

	//bool	GenerateTexture(char* image, int width, int height, GLint internalFormat, GLint format, GLenum type = GL_UNSIGNED_BYTE, GLint target = GL_TEXTURE_2D, int flags = 0);
	//bool	GenerateCubeTexture(char* image, int width, int height, int bpp, GLint internalFormat, GLint format, GLenum type = GL_UNSIGNED_BYTE, GLint target = GL_TEXTURE_2D, int flags = 0);

	// this could be moved back to Texture
	void	FlipV(char* image, int width, int height, int bpp);

	GLint			mTarget;
	GLuint			mTexture;
	string			mName;
	RenderFactory*	mFactory;
	Device*			mDevice;
};

#endif
