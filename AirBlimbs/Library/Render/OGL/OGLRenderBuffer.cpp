#include "OGLRenderBuffer.h"
#include "File/File.h"
#include "Math/Vector.h"
#include "File/Log.h"

#define BUFFER_OFFSET(i) ((char *)0 + (i))

OGLRenderBuffer::OGLRenderBuffer() :
	mSize(0),
	mData(0),
	mBuffer(-1)
{
}

void OGLRenderBuffer::Init(RenderBufferType type, unsigned int size, RenderBufferUseage useage)
{
	RenderBuffer::Init(type, size, useage);
	mSize = -1;
	mUseage = useage;

	if (size != -1)
	{
		unsigned int bufferSize = 0;
		switch (mType)
		{
		case RBT_Matrix4:
			bufferSize = size * sizeof(Matrix4);
			break;

		case RBT_Vector4:
			bufferSize = size * sizeof(Vector4);
			break;

		case RBT_UnsignedInt:
			bufferSize = size * sizeof(unsigned int);
			break;
		}

		mSize = bufferSize;
		mData = _aligned_malloc(bufferSize, 16);
	}


	// this forces the buffer to be created if its dnyamic
	if (useage == RBU_Dynamic)
		GenerateBuffer(mBuffer, mData, mSize, mType == RBT_UnsignedInt);
}

void OGLRenderBuffer::Deinit()
{
	// clean up buffers
	glDeleteBuffersARB(1, &mBuffer);

	if (mData)
		_aligned_free(mData);

	mData = 0;
}

bool OGLRenderBuffer::Save(const File& out)
{
	return true;
}

bool OGLRenderBuffer::Load(Device* device, const File& in)
{
	// read the type
	in.Read(&mType, sizeof(RenderBufferType));

	// read the data
	if ((mSize = LoadVector(in, &mData)) == -1)
		return false;

	GenerateBuffer(mBuffer, mData, mSize, mType == RBT_UnsignedInt);
	return true;
}


void OGLRenderBuffer::Bind(VertexFormat channel, int index)
{
	mChannel = channel;
	mIndex = index;

	switch (mUseage)
	{
	case RBU_Dynamic:
	case RBU_Static:
		{
			switch (channel)
			{
			case VF_Position:
				BindInternal(GL_VERTEX_ARRAY);
				glVertexPointer(3, GL_FLOAT, sizeof(Vector4), 0);
				break;

			case VF_Normal:
				BindInternal(GL_NORMAL_ARRAY);
				glNormalPointer(GL_FLOAT, sizeof(Vector4), 0);
				break;

			case VF_Tangent:
			case VF_Binormal:
				glClientActiveTexture(GL_TEXTURE0_ARB + index);
				glBindBufferARB(GL_ARRAY_BUFFER_ARB, mBuffer);
				glTexCoordPointer(3, GL_FLOAT, sizeof(Vector4), 0);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				break;

			case VF_BoneData:
				glClientActiveTexture(GL_TEXTURE0_ARB + index);
				glBindBufferARB(GL_ARRAY_BUFFER_ARB, mBuffer);
				glTexCoordPointer(4, GL_FLOAT, sizeof(Vector4), 0);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				break;

			case VF_TexCoord0:
			case VF_TexCoord1:
			case VF_TexCoord2:
			case VF_TexCoord3:
				glClientActiveTexture(GL_TEXTURE0_ARB + index);
				glBindBufferARB(GL_ARRAY_BUFFER_ARB, mBuffer);
				glTexCoordPointer(2, GL_FLOAT, sizeof(Vector4), 0);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				break;

			case VF_Colour:
				BindInternal(GL_COLOR_ARRAY);
				glColorPointer(4, GL_FLOAT, sizeof(Vector4), 0);
				break;

			case VF_Index:
				BindInternal(GL_INDEX_ARRAY, true);
				break;

			default:
				return;
			}
		}
		break;
/*
	case RBU_Dynamic:
		{
			switch (channel)
			{
			case VF_Position:
				glEnableClientState(GL_VERTEX_ARRAY);
				glVertexPointer(3, GL_FLOAT, sizeof(Vector4), &mVec3[0]);
				break;

			case VF_Normal:
				glEnableClientState(GL_NORMAL_ARRAY);
				glNormalPointer(GL_FLOAT, sizeof(Vector4), &mVec3[0]);
				break;

			case VF_Tangent:
			case VF_Binormal:
				glClientActiveTexture(GL_TEXTURE0_ARB + index);
				glTexCoordPointer(3, GL_FLOAT, sizeof(Vector4), &mVec3[0]);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				break;

			case VF_TexCoord0:
			case VF_TexCoord1:
			case VF_TexCoord2:
			case VF_TexCoord3:
				glClientActiveTexture(GL_TEXTURE0_ARB + index);
				glTexCoordPointer(2, GL_FLOAT, sizeof(Vector4), &mVec2[0]);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				break;

			case VF_Colour:
				glEnableClientState(GL_COLOR_ARRAY);
				glColorPointer(4, GL_FLOAT, sizeof(Vector4), &mVec4[0]);
				break;

			default:
				return;
			}
		}
		break;*/
	}
}

void OGLRenderBuffer::Unbind()
{
	switch (mChannel)
	{
	case VF_Position:
		UnbindInternal(GL_VERTEX_ARRAY);
		break;

	case VF_Normal:
		UnbindInternal(GL_NORMAL_ARRAY);
		break;

	case VF_Tangent:
		//UnbindInternal(GL_TEXTURE0_ARB + mIndex);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		break;

	case VF_Binormal:
		//UnbindInternal(GL_TEXTURE0_ARB + mIndex);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		break;

	case VF_BoneData:
		//UnbindInternal(GL_TEXTURE0_ARB + mIndex);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		break;

	case VF_TexCoord0:
	case VF_TexCoord1:
	case VF_TexCoord2:
	case VF_TexCoord3:
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		//UnbindInternal(GL_TEXTURE0_ARB + mIndex);
		break;

	case VF_Colour:
		UnbindInternal(GL_COLOR_ARRAY);
		break;

	case VF_Index:
		UnbindInternal(GL_INDEX_ARRAY, true);
		break;

	default:
		return;
	}
}

void OGLRenderBuffer::Draw(unsigned int startIndex, unsigned int numIndices)
{
	unsigned int size = numIndices == -1 ? GetCount() : numIndices;
	switch (mUseage)
	{
	case RBU_Dynamic:
	case RBU_Static:
		{
			glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, BUFFER_OFFSET(startIndex * sizeof(unsigned int)));
		}
		break;
/*
	case RBU_Dynamic:
		{
			glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, &mUInt[0]);
		}
		break;*/
	}
}

void OGLRenderBuffer::BindInternal(GLenum channel, bool indexBuffer)
{
	glEnableClientState(channel);
	glBindBufferARB(indexBuffer ? GL_ELEMENT_ARRAY_BUFFER_ARB : GL_ARRAY_BUFFER_ARB, mBuffer);
}

void OGLRenderBuffer::UnbindInternal(GLenum channel, bool indexBuffer)
{
	glDisableClientState(channel);
}

bool OGLRenderBuffer::Lock(RenderBufferLock& lock, int lockFlags)
{
	if (lockFlags | RBLF_ReadOnly)
		lock.count = GetCount();

	lock.data = mData;
	lock.flags = lockFlags;
	return lock.data != 0;
}

void OGLRenderBuffer::Unlock(RenderBufferLock& lock)
{
	if (lock.flags &= RBLF_ReadOnly)
		return;

	switch (mUseage)
	{
	case RBU_Static:
		{
			if (mBuffer != -1)
			{
				Log::Error("[OGLRenderBuffer::Unlock] Trying to modify a static render buffer\n");
				return;
			}

			GenerateBuffer(mBuffer, mData, mSize, mType == RBT_UnsignedInt);
		}
		break;

	case RBU_Dynamic:
		CopyBuffer(mBuffer, mData, (lock.count == -1) ? mSize : (lock.count * GetTypeSize(mType)), lock.startIndex * GetTypeSize(mType), mType == RBT_UnsignedInt);
		break;
	}
}

void OGLRenderBuffer::ReloadResource(Device* device)
{
	GenerateBuffer(mBuffer, mData, mSize, mType == RBT_UnsignedInt);
}

void OGLRenderBuffer::GenerateBuffer(GLuint& buffer, void* vec, unsigned int size, bool indexBuffer)
{
	glGenBuffersARB(1, &buffer);
	glBindBufferARB(indexBuffer ? GL_ELEMENT_ARRAY_BUFFER_ARB : GL_ARRAY_BUFFER_ARB, buffer);
	glBufferDataARB(indexBuffer ? GL_ELEMENT_ARRAY_BUFFER_ARB : GL_ARRAY_BUFFER_ARB, size, vec, mUseage == RBU_Static ? GL_STATIC_DRAW_ARB : GL_DYNAMIC_DRAW_ARB); //GL_STREAM_DRAW_ARB
}

void OGLRenderBuffer::CopyBuffer(GLuint& buffer, void* vec, unsigned int size, unsigned int offset, bool indexBuffer)
{
	glBindBufferARB(indexBuffer ? GL_ELEMENT_ARRAY_BUFFER_ARB : GL_ARRAY_BUFFER_ARB, buffer);
	//glBufferDataARB(indexBuffer ? GL_ELEMENT_ARRAY_BUFFER_ARB : GL_ARRAY_BUFFER_ARB, 0, NULL, GL_DYNAMIC_DRAW_ARB);	// hrmmm
	char* ptr = (char*)glMapBufferARB(indexBuffer ? GL_ELEMENT_ARRAY_BUFFER_ARB : GL_ARRAY_BUFFER_ARB, GL_WRITE_ONLY_ARB);
	memcpy(ptr + offset, (unsigned char*)vec + offset, size);
	glUnmapBufferARB(indexBuffer ? GL_ELEMENT_ARRAY_BUFFER_ARB : GL_ARRAY_BUFFER_ARB);
}
