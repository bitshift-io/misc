#ifndef _OGLDEVICE_H_
#define _OGLDEVICE_H_

#ifdef WIN32
    #define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
    #define NOMINMAX
    #include <windows.h>
    #undef SendMessage
	#undef CreateWindow
#endif

#include "../Device.h"



#if defined(WIN32) || defined(UNIX)

    #include <GL/gl.h>
    #include <GL/glu.h>

#elif __APPLE__

    #include "Memory/NoMemory.h"
    #include <Carbon/Carbon.h>
    #include <AGL/agl.h>
    #include <OpenGL/glext.h>
    #include <OpenGL/gl.h>
    #include "Memory/Memory.h"

#endif

#include <list>

#include "Cg/cg.h"
#include "Cg/cgGL.h"

#include "OGLExtension.h"

using namespace std;

class Window;

#ifdef WIN32
	class Win32Window;
#elif __APPLE__
	class MacWindow;
#else
	class LinuxWindow;
#endif

class OGLDevice : public Device
{
public:

	virtual bool			Init(RenderFactory* factory, Window* window, int multisample = -1);
	virtual void			Deinit(RenderFactory* factory, Window* window);

	virtual int				GetMultisample()									{ return mMultisample; }

	virtual void			Begin();
	virtual void			End();

	virtual RenderBuffer*	CreateRenderBuffer();
	virtual void			DestroyRenderBuffer(RenderBuffer** buffer);

	virtual bool			Draw(Geometry* geometry, unsigned int startIndex = 0, unsigned int numIndices = -1, int instanceCount = DT_NotInstanced);

	//virtual void			Draw(int instanceCount, Material* material, Geometry* geometry, int vertexFormat = 0, unsigned int startIndex = 0, unsigned int numIndices = -1);
	virtual void			SetMatrix(const Matrix4& matrix, MatrixMode mode);

	int						ExtensionSupported(const char* extension);
	void*					GetExtensionAddress(const char* extension);

	virtual void			DrawPoint(const Vector4& p, const Vector4& colour);
	virtual void			DrawLine(const Vector4& p1, const Vector4& p2, const Vector4& colour);

	virtual bool			SetDeviceState(const DeviceState& state, bool force = false);
	virtual bool			GetDeviceState(DeviceState& state);

	virtual void			DrawStencil();
	virtual void			SaveStencil();

	virtual Window*			GetWindow();
	virtual void			ReloadResource();

	virtual void			SetGeometryTopology(GeometryTopology topology)		{}

	virtual void			CheckError();

	virtual Vector4			GetClearColour();
	virtual void			SetClearColour(const Vector4& colour);

	// for debugging, pix in windows, some gl extension in gl
	virtual void			BeginDebugEvent(const string& name, const Vector4& colour);
	virtual void			EndDebugEvent();

	CGcontext				GetContext()										{ return mContext; }

protected:

	bool					SetDeviceStateInternal(bool force = false);
	bool					InitInternal(RenderFactory* factory, Window* window, int pixelFormat = -1);
	void					ResetDevice(int width, int height);

	int						FindExtension(const char* extension, const char* extensions);
	bool					InitExtension();

#ifdef WIN32
	int						GetMultisamplePixelFormat(int multisample);

	Win32Window*			mWindow;

	HDC						mHDC;		// Private GDI Device Context
	HGLRC					mHRC;		// Permanent Rendering Context
#elif __APPLE__
    MacWindow*              mWindow;

    AGLDrawable             mDrawable;
    AGLContext              mOGLContext;
    CFDictionaryRef         mOriginalMode;
#else
	LinuxWindow*			mWindow;

#endif

	Matrix4					mWorld;
	Matrix4					mView;
	Matrix4					mProjection;

	list<RenderBuffer*>		mRenderBuffer;

	vector<string>			mDebugString;
	CGcontext				mContext;

	int						mMultisample;
	DeviceState				mCurrentDeviceState;
	DeviceState				mDesiredDeviceState;
};

#endif
