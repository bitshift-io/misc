#include "OGLRenderTexture.h"
#include "OGLTexture.h"
#include "Render/RenderFactory.h"
#include "File/Log.h"

bool OGLRenderTexture::Init(Device* device, RenderFactory* factory, int width, int height, unsigned int flags, int targetCount)
{
	// http://opengl.org/documentation/extensions/EXT_framebuffer_object.txt
	// http://oss.sgi.com/projects/ogl-sample/registry/ARB/texture_float.txt

	// http://www.gamedev.net/reference/programming/features/fbo2/page5.asp

	mWidth = width;
	mHeight = height;

	//GLuint maxBuffers;
	//glGetIntergeri(GL_MAX_COLOR_ATTACHMENTS, &maxBuffers);
	//mTargetCount = Math::Min(maxBuffers, Math::Min(targetCount, MAX_TARGET));

	mTargetCount = 1;//Math::Min(targetCount, MAX_TARGET);

	mClearDepth = 1.f;

	glGenFramebuffersEXT(1, &mFrameBuffer);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, mFrameBuffer);

	// initialize texture
	if (flags & RTF_CubeMap)
	{/*
		texture = new TextureCube();
		glGenTextures(1, &texture->texture);

		target = GL_TEXTURE_CUBE_MAP;
		texture->target = target;

		glEnable(target);
		glBindTexture(target, texture->texture);

		for (int i = 0; i < 6; ++i)
		{
			GLenum face = GL_TEXTURE_CUBE_MAP_POSITIVE_X + i;

			glTexImage2D(face, 0, GL_RGBA8, width, height, 0,
				GL_RGBA, GL_UNSIGNED_BYTE, NULL);

			glTexParameterf(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameterf(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterf(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
				GL_COLOR_ATTACHMENT0_EXT, face, texture->texture, 0);
		}*/
	}
	else if (flags & RTF_Colour)
	{


		mColour = (OGLTexture*)factory->Create(Texture::Type);
		glGenTextures(1, &mColour->mTexture);

		mTarget = GL_TEXTURE_2D;
		mColour->mTarget = mTarget;

		//glEnable(mTarget);
		glBindTexture(mTarget, mColour->mTexture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, mColour->mTexture, 0);



		/*
		texture = new Texture();
		glGenTextures(1, &texture->texture);

		target = GL_TEXTURE_2D; //GL_TEXTURE_RECTANGLE_NV; //GL_TEXTURE_2D;
		texture->target = GL_TEXTURE_RECTANGLE_NV;//target;

		glEnable(target);
		glBindTexture(target, texture->texture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glTexImage2D(GL_TEXTURE_RECTANGLE_NV, 0, GL_FLOAT_RGB32_NV, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_RECTANGLE_NV, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
			GL_COLOR_ATTACHMENT0_EXT,
			GL_TEXTURE_RECTANGLE_NV, texture->texture, 0);*/
	}
	else
	{/*
		// No color buffer to draw to or read from
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);*/
	}


	GLuint depthBuffer;			// Our handle to the depth render buffer

	// Create the render buffer for depth
	glGenRenderbuffersEXT(1, &depthBuffer);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depthBuffer);
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, width, height);

	// Attach the depth render buffer to the FBO as it's depth attachment
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depthBuffer);

	// initialize depth renderbuffer
	//if (flags & RTF_DepthTexture)
	//{
	/*
		depth = new Texture();
		glGenTextures(1, &depth->texture);

		depth->target = GL_TEXTURE_2D;

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, depth->texture);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE_ARB, GL_LUMINANCE);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0,
			GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);

		Device::GetInstance().CheckErrors();

		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
			GL_DEPTH_ATTACHMENT_EXT,
			GL_TEXTURE_2D, depth->texture, 0);*/
	//}
	//else

	if (flags & RTF_Depth)
	{/*
		// Create the render buffer for depth
		glGenRenderbuffersEXT(1, &depthBuffer);
		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depthBuffer);
		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, width, height);

		mDepth[0] = (OGLTexture*)factory->Create(Texture::Type);
		glGenTextures(1, &mDepth[0]->mTexture);

		mTarget = GL_TEXTURE_2D;
		mColour->mTarget = mTarget;

		glEnable(mTarget);
		glBindTexture(mTarget, mColour[i]->mTexture);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0,
			GL_RGBA, GL_UNSIGNED_BYTE, NULL);

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT + i, GL_TEXTURE_2D, mColour[i]->mTexture, 0);

		/*
		depth = new Texture();
		glGenRenderbuffersEXT(1, &depth->texture);

		depth->target = GL_TEXTURE_2D;

		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depth->texture);

		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,
			GL_DEPTH_COMPONENT24, width, height);

		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,
			GL_DEPTH_ATTACHMENT_EXT,
			GL_RENDERBUFFER_EXT, depth->texture);*/
	}

	// initialize stencil renderbuffer
	if (flags & RTF_Stencil)
	{/*
		stencil = new Texture();
		glGenRenderbuffersEXT(1, &stencil->texture);

		stencil->target = GL_TEXTURE_2D;

		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, stencil->texture);

		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,
			GL_STENCIL_INDEX, width, height);

		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,
			GL_STENCIL_ATTACHMENT_EXT,
			GL_RENDERBUFFER_EXT, stencil->texture);*/
	}

	CheckFramebufferStatus();
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	return true;
}

void OGLRenderTexture::Deinit()
{

}

int OGLRenderTexture::Begin()
{
	//glEnable(mTarget);
	//glBindTexture(mTarget, 0);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, mFrameBuffer);
	glPushAttrib(GL_VIEWPORT_BIT | GL_COLOR_BUFFER_BIT);

	return (mTarget == GL_TEXTURE_2D) ? 1 : 6;
}

void OGLRenderTexture::End()
{
	glPopAttrib();
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

void OGLRenderTexture::BindTarget(int index, bool clear, int flags)
{/*
	if (mTarget != GL_TEXTURE_2D)
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_CUBE_MAP_POSITIVE_X+index, mColour[index]->mTexture, 0);
	else
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, mColour[index]->mTexture, 0);
*/
	glViewport(0, 0, mWidth, mHeight);

	// Set the render target
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT + index);

	glClearDepth(mClearDepth);
	glClearColor(mClearColour.x, mClearColour.y, mClearColour.z, mClearColour.w);


	if (clear)
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void OGLRenderTexture::SetClearColour(const Vector4& colour)
{
	mClearColour = colour;
}

void OGLRenderTexture::SetClearDepth(float depth)
{
	mClearDepth = depth;
}

void OGLRenderTexture::SetClearStencil(unsigned int stencil)
{
	mClearStencil = stencil;
}

Texture* OGLRenderTexture::GetTexture(RenderTextureFlags type, int lockFlags)
{
	switch (type)
	{
	case RTF_Colour:
		return mColour;
	}

	return 0;
}

void OGLRenderTexture::CheckFramebufferStatus()
{
    GLenum status;
    status = (GLenum) glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
    switch(status)
	{
        case GL_FRAMEBUFFER_COMPLETE_EXT:
			//Log::Error("Framebuffer complete\n");
            break;
        case GL_FRAMEBUFFER_UNSUPPORTED_EXT:
            Log::Error("Unsupported framebuffer format\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
            Log::Error("Framebuffer incomplete, missing attachment\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT_EXT:
            Log::Error("Framebuffer incomplete, duplicate attachment\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
            Log::Error("Framebuffer incomplete, attached images must have same dimensions\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
            Log::Error("Framebuffer incomplete, attached images must have same format\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
            Log::Error("Framebuffer incomplete, missing draw buffer\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
            Log::Error("Framebuffer incomplete, missing read buffer\n");
            break;
        default:
            Log::Error("Uh oh!");
    }
}
