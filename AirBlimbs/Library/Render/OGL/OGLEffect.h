#ifndef _OGLEFFECT_H_
#define _OGLEFFECT_H_

#include "Render/Effect.h"
#include "Render/Device.h"
#include "OGLTexture.h"
#include "OGLDevice.h"
#include <map>

using namespace std;

class OGLEffect : public Effect
{
	friend class Material;

public:

	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node = 0);
	virtual void	ReloadResource();
	static void		StaticReloadResource();

	virtual int		Begin();
	virtual void	BeginPass(int idx);
	virtual void	SetPassParameter();
	virtual void	EndPass();
	virtual void	End();

/*
/*
	void			Deinit();

	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node = 0);

	CGtype			GetParameterType(CGparameter param) const						{ return cgGetParameterType(param); }
	CGannotation	GetAnnotationByName(CGparameter param, const char* name) const	{ return cgGetNamedParameterAnnotation(param, name); }
*/

/*
	virtual void	SetProjectorParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world);
	virtual void	SetMatrixParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world);
	virtual void	SetLightParameters(Light* lights[], unsigned int numLights);
	virtual void	SetTimeParameters(float time);


	virtual EffectParamHandle GetParameterByName(const char* name) { return (EffectParamHandle)cgGetNamedEffectParameter(mEffect, name); }


	// TODO:
	virtual void			SetTechnique(EffectTechnique technique) {}
	virtual int				GetTechniqueCount()						{ return 0; }

	virtual EffectTechnique	GetTechnique(int idx)					{ return 0; }
	virtual EffectTechnique	GetTechnique(const char* name)			{ return 0; }

	virtual EffectTechnique	GetCurrentTechnique()					{ return 0; }

	virtual int				GetVertexFormat(EffectTechnique technique) { return 0; }
	// END TODO

	virtual void	ReloadResource(RenderFactory* factory, Device* device);

	static void		ReloadResource();

protected:

	void					ProcessParameters();
	void					ProcessStructs();

	virtual EffectParam*	ReadParameter(Material* material, RenderFactory* factory, Device* device, const char* name, File* in);
	virtual void			DestroyParameter(RenderFactory* factory, EffectParam* param);

	vector<pair<EffectParamID, EffectParamHandle> > mParameter;
*/

protected:

	virtual void			SetValue(EffectParameter* param, const Matrix4* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, const float* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, Texture** value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, const Vector4* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, const int* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, void* buffer, unsigned int size);



	bool					ProcessTechniques(Device* device);
	void					ProcessParameters(EffectParameter* parent = 0);

	CGeffect	mEffect;
/*
	CGtechnique mTechnique;
	int			mNumPasses;
	*/
	bool		mOrigonal;				// temporary fix to go with the map below
/*
	bool		mProjector;
	bool		mSkin;
	bool		mLight;
	bool		mTime;
*/
	static int			sPassIdx;
	static CGpass		sPass;

	static map<string, CGeffect>	sEffect; // a temporary fix to speed up load times till we make this just a handle

};

#endif
