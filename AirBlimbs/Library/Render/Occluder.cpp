#include "Occluder.h"
#include "Scene.h"
#include "Mesh.h"
#include "Camera.h"
#include "Shape.h"
#include "File/Log.h"
#include "Device.h"

bool Occluder::InsertScene(Scene* scene)
{
	return InsertSceneNode(scene->GetRootNode());
}

bool Occluder::RemoveScene(Scene* scene)
{
	return RemoveSceneNode(scene->GetRootNode());
}

bool Occluder::InsertSceneNode(SceneNode* node)
{
	if (!node)
		return true;

	SceneObject* object = node->GetObject();
	if (object)
	{
		switch (object->GetType())
		{
		case Mesh::Type:
			mVisibilityNode.insert(VisibilityHash::value_type(node, VisibilityNode(node))); 
			break;
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
		InsertSceneNode(*it);

	return true;
}

bool Occluder::RemoveSceneNode(SceneNode* node)
{
	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
		RemoveSceneNode(*it);

	return true;
}

bool Occluder::InsertOccluder(SceneNode* shape)
{
	mOccluderNode.insert(OccluderHash::value_type(shape, OccluderNode(shape))); 
	return true;
}

bool Occluder::RemoveOccluder(SceneNode* shape)
{
	return true;
}

Visibility Occluder::GetVisibility(SceneNode* node)
{
	pair<VisibilityHash::const_iterator, VisibilityHash::const_iterator> p = mVisibilityNode.equal_range(node);
	for (VisibilityHash::const_iterator it = p.first; it != p.second; ++it)
	{
		if (node == it->first)
			return it->second.visibility;
	}

	return V_Visible;
}

bool Occluder::SetVisibility(SceneNode* node, Visibility)
{
	return true;
}

void Occluder::CalculateVisibility()
{
	const Frustum& frustum = mCamera->GetFrustum();

	//
	// 1. cull occluders using camera frustum
	//
	{
		OccluderHash::iterator it;
		for (it = mOccluderNode.begin(); it != mOccluderNode.end(); ++it)
		{
			SceneNode* node = it->second.node;
			Visibility& visibility = it->second.visibility;

			Box bound;
			node->GetObject()->GetLocalBoundBox(bound);
			bound.SetTransform(node->GetWorldTransform());

			Frustum::State frustumVisibility = frustum.ContainsBox(bound);
			if (frustumVisibility == Frustum::Outside)
				visibility = V_Culled;
			else
				visibility = V_Visible;
		}
	}
	
	//
	// 2. check for occluders occluding occluders
	//
	{
		// iterate over visible occluders
		OccluderHash::iterator occluderIt;
		for (occluderIt = mOccluderNode.begin(); occluderIt != mOccluderNode.end(); ++occluderIt)
		{
			SceneNode* occluderNode = occluderIt->second.node;
			Visibility& occluderVisibility = occluderIt->second.visibility;

			if (occluderVisibility == V_Culled)
				continue;

			Shape* occluderShape = static_cast<Shape*>(occluderNode->GetObject());

			switch (occluderShape->mType)
			{
			case ST_Plane:
				PlaneOccludeOccluder(occluderIt, occluderNode, occluderShape);
				break;

			default:
				Log::Error("[Occluder::CalculateVisibility] occluder shape not supported");
				break;
			}
		}
	}

	//
	// 3. frustum cull scene nodes
	//
	{
		VisibilityHash::iterator it;
		for (it = mVisibilityNode.begin(); it != mVisibilityNode.end(); ++it)
		{
			SceneNode* node = it->second.node;
			Visibility& visibility = it->second.visibility;

			Box bound;
			node->GetObject()->GetLocalBoundBox(bound);
			bound.SetTransform(node->GetWorldTransform());

			Frustum::State frustumVisibility = frustum.ContainsBox(bound);
			if (frustumVisibility == Frustum::Outside)
				visibility = V_Culled;
			else
				visibility = V_Visible;
		}
	}

	//
	// 4. occlusion cull scene nodes
	//
	{
		// iterate over visible occluders
		OccluderHash::iterator occluderIt;
		for (occluderIt = mOccluderNode.begin(); occluderIt != mOccluderNode.end(); ++occluderIt)
		{
			SceneNode* occluderNode = occluderIt->second.node;
			Visibility& occluderVisibility = occluderIt->second.visibility;

			if (occluderVisibility == V_Culled)
				continue;

			Shape* occluderShape = static_cast<Shape*>(occluderNode->GetObject());

			switch (occluderShape->mType)
			{
			case ST_Plane:
				PlaneOccludeSceneNode(occluderNode, occluderShape);
				break;

			default:
				Log::Error("[Occluder::CalculateVisibility] occluder shape not supported");
				break;
			}
		}
	}
}

Frustum Occluder::GeneratePlaneOccluderFrustum(SceneNode* occluderNode, Shape* occluderShape)
{
	//
	// generate a frustum from a plane
	// the near plane is the occluder plane, need to make the normal point away from the camera
	// all normals need to face intowards the center of the frstum
	//

	Matrix4 occluderWorldTransform = occluderNode->GetWorldTransform();
	Vector4 normal = occluderWorldTransform.GetYAxis(); // y because a plan by default is on the x and z axis
	float cameraDotPlaneNormal = normal.Dot(mCamera->GetWorld().GetZAxis()) > 0 ? 1.f : -1.f;

	// get the 4 points of the plane in world space
	Vector4 farRight(occluderShape->mPlane.width * 0.5f, 0.f, occluderShape->mPlane.length * 0.5f);
	Vector4 nearRight(occluderShape->mPlane.width * 0.5f, 0.f, -occluderShape->mPlane.length * 0.5f);
	Vector4 nearLeft(-occluderShape->mPlane.width * 0.5f, 0.f, -occluderShape->mPlane.length * 0.5f);
	Vector4 farLeft(-occluderShape->mPlane.width * 0.5f, 0.f, occluderShape->mPlane.length * 0.5f);

	farRight = occluderWorldTransform * farRight;
	farLeft = occluderWorldTransform * farLeft;
	nearLeft = occluderWorldTransform * nearLeft;
	nearRight = occluderWorldTransform * nearRight;

	Frustum occludeFrustum = mCamera->GetFrustum();
	occludeFrustum.sides[Frustum::Near] = Plane(occluderWorldTransform.GetTranslation(), normal * cameraDotPlaneNormal);

	// left plane
	Vector4 leftDir = farLeft - mCamera->GetPosition(); 
	leftDir.Normalize();

	Vector4 leftAlternate = farLeft - nearLeft;
	leftAlternate.Normalize();

	Vector4 leftNormal = leftAlternate.Cross(leftDir) * cameraDotPlaneNormal;
	occludeFrustum.sides[Frustum::Left] = Plane(farLeft, leftNormal);

	// right plane
	Vector4 rightDir = farRight - mCamera->GetPosition(); 
	rightDir.Normalize();

	Vector4 rightAlternate = farRight - nearRight;
	rightAlternate.Normalize();

	Vector4 rightNormal = -rightAlternate.Cross(rightDir) * cameraDotPlaneNormal;
	occludeFrustum.sides[Frustum::Right] = Plane(nearRight, rightNormal);

	// top (far) plane
	Vector4 topDir = farLeft - mCamera->GetPosition(); 
	topDir.Normalize();

	Vector4 topAlternate = farRight - farLeft;
	topAlternate.Normalize();

	Vector4 topNormal = topAlternate.Cross(topDir) * cameraDotPlaneNormal;
	occludeFrustum.sides[Frustum::Top] = Plane(farLeft, topNormal);

	// bottom (near) plane
	Vector4 bottomDir = nearLeft - mCamera->GetPosition(); 
	bottomDir.Normalize();

	Vector4 bottomAlternate = nearRight - nearLeft;
	bottomAlternate.Normalize();

	Vector4 bottomNormal = -bottomAlternate.Cross(bottomDir) * cameraDotPlaneNormal;
	occludeFrustum.sides[Frustum::Bottom] = Plane(nearLeft, bottomNormal);

	return occludeFrustum;
}

void Occluder::PlaneOccludeSceneNode(SceneNode* occluderNode, Shape* occluderShape)
{
	Frustum occludeFrustum = GeneratePlaneOccluderFrustum(occluderNode, occluderShape);

	//
	// iterate over visible scene nodes
	//

	VisibilityHash::iterator it;
	for (it = mVisibilityNode.begin(); it != mVisibilityNode.end(); ++it)
	{
		SceneNode* node = it->second.node;
		Visibility& visibility = it->second.visibility;

		if (visibility == V_Culled)
			continue;

		// check for visibility
		Box bound;
		node->GetObject()->GetLocalBoundBox(bound);
		bound.SetTransform(node->GetWorldTransform());

		Frustum::State frustumVisibility = occludeFrustum.ContainsBox(bound);
		if (frustumVisibility == Frustum::Inside)
			visibility = V_Culled;
	}
}

void Occluder::PlaneOccludeOccluder(OccluderHash::iterator occluderIt, SceneNode* occluderNode, Shape* occluderShape)
{
	Frustum occludeFrustum = GeneratePlaneOccluderFrustum(occluderNode, occluderShape);

	//
	// iterate over remaning occluders
	//

	OccluderHash::iterator it = occluderIt;
	++it;
	for (; it != mOccluderNode.end(); ++it)
	{
		SceneNode* node = it->second.node;
		Visibility& visibility = it->second.visibility;

		if (visibility == V_Culled)
			continue;

		// check for visibility
		Box bound;
		node->GetObject()->GetLocalBoundBox(bound);
		bound.SetTransform(node->GetWorldTransform());

		Frustum::State frustumVisibility = occludeFrustum.ContainsBox(bound);
		if (frustumVisibility == Frustum::Inside)
			visibility = V_Culled;
	}
}

void Occluder::DebugDraw(Device* device)
{
	// draw visible occluders
	OccluderHash::iterator occluderIt;
	for (occluderIt = mOccluderNode.begin(); occluderIt != mOccluderNode.end(); ++occluderIt)
	{
		SceneNode* occluderNode = occluderIt->second.node;
		Visibility& occluderVisibility = occluderIt->second.visibility;

		if (occluderVisibility == V_Culled)
			continue;

		Shape* occluderShape = static_cast<Shape*>(occluderNode->GetObject());

		switch (occluderShape->mType)
		{
		case ST_Plane:
			{
				Matrix4 occluderWorldTransform = occluderNode->GetWorldTransform();
				Vector4 normal = occluderWorldTransform.GetYAxis(); // y because a plan by default is on the x and z axis
				float cameraDotPlaneNormal = normal.Dot(mCamera->GetWorld().GetZAxis()) > 0 ? 1.f : -1.f;
				
				// get the 4 points of the plane in world space
				Vector4 farRight(occluderShape->mPlane.width * 0.5f, 0.f, occluderShape->mPlane.length * 0.5f);
				Vector4 nearRight(occluderShape->mPlane.width * 0.5f, 0.f, -occluderShape->mPlane.length * 0.5f);
				Vector4 nearLeft(-occluderShape->mPlane.width * 0.5f, 0.f, -occluderShape->mPlane.length * 0.5f);
				Vector4 farLeft(-occluderShape->mPlane.width * 0.5f, 0.f, occluderShape->mPlane.length * 0.5f);

				farRight = occluderWorldTransform * farRight;
				farLeft = occluderWorldTransform * farLeft;
				nearLeft = occluderWorldTransform * nearLeft;
				nearRight = occluderWorldTransform * nearRight;

				device->DrawLine(farLeft, nearLeft);
				device->DrawLine(nearLeft, nearRight);
				device->DrawLine(nearRight, farRight);
				device->DrawLine(farRight, farLeft);
			}
			break;
		}
	}
}
