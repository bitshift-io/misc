#ifndef _SHAPE_H_
#define _SHAPE_H_

#include "SceneObject.h"
#include "SceneNode.h"
#include "Math/Matrix.h"
#include "Math/Sphere.h"

enum ShapeType
{
	ST_Box,
	ST_Sphere,
	ST_Cylinder,
	ST_Plane,
};

//
// width = x
// length = z
// height = y
//

struct PlaneShape
{
	float	width;
	float	length;
};

struct BoxShape
{
	float	width;
	float	height;
	float	length;
};

struct SphereShape
{
	float	radius;
};

struct CylinderShape
{
	float	radius;
	float	height;
};

//
// precedural shapes
//
class Shape : public SceneObject
{
public:

	TYPE(13)

	virtual bool			Save(const File& out, SceneNode* node);
	virtual bool			Load(const ObjectLoadParams* params, SceneNode* node);

	virtual bool			GetLocalBoundBox(Box& box);

	Sphere					GetSphere(SceneNode* node);

	ShapeType				mType;

	PlaneShape				mPlane;
	BoxShape				mBox;
	SphereShape				mSphere;
	CylinderShape			mCylinder;
};

#endif
