#include "TGA.h"
#include "File/File.h"
#include "Memory/Memory.h"

#define DESC_ABITS      0x0f
#define DESC_HORIZONTAL 0x10
#define DESC_VERTICAL   0x20
	/*
TGA::TGA() : image(0)
{

}

TGA::~TGA()
{
	delete[] image;
}

unsigned char* TGA::GetPixels()
{
	return (unsigned char*)image;
}

int TGA::GetWidth()
{
	return header.width;
}

int TGA::GetHeight()
{
	return header.height;
}

int TGA::GetBPP()
{
	return header.bpp / 8;
}
*/
bool TGA::Save(const char* file, const void* data, int width, int height, int bpp)
{
	TGAHead header;
	memset(&header, 0x0, sizeof(TGAHead));
	header.width = width;
	header.height = height;
	header.bpp = bpp;
	header.imageType = 2; // uncompressed

	File out(file, FAT_Write | FAT_Binary);
	if (!out.Valid())
		return false;

	out.Write(&header, sizeof(TGAHead));
	out.Write(data, width * height * header.bpp / 8);

	return true;
}


bool TGA::Load(const char* file)
{
	File in(file, FAT_Read | FAT_Binary);
	if (!in.Valid())
		return false;

	TGAHead header;
	in.Read(&header, sizeof(TGAHead));

	TextureSubresource* resource = CreateResourceInfo(header.bpp == 24 ? TF_RGB : TF_RGBA, header.width, header.height);

	bool ret = false;
	if (header.imageType == IT_Compressed)
		ret = LoadCompresed(in, header, resource);
	else
		ret = LoadUncompresed(in, header, resource);

	bool flipHorizontal = (header.descritor & DESC_HORIZONTAL) == DESC_HORIZONTAL;
	bool flipVertical = (header.descritor & DESC_VERTICAL) == DESC_VERTICAL;
	char alphaBits = (header.descritor & DESC_ABITS) == DESC_ABITS;

	if (flipVertical)
		FlipVertical(resource);

	return ret;
}

bool TGA::LoadUncompresed(File& in, TGAHead& header, TextureSubresource* resource)
{
	unsigned int bytesPerPixel = header.bpp / 8;
	int numPixels = header.width * header.height;
	int imageSize = numPixels * bytesPerPixel;
	in.Read(resource->pixel, imageSize);
	int curByte = 0;

	// swap colour channels
	unsigned char* cPixel = (unsigned char*)resource->pixel;
	for (int i = 0; i < numPixels; ++i)
	{
		unsigned char pixel = cPixel[curByte + 0];
		cPixel[curByte + 0] = cPixel[curByte + 2];
		cPixel[curByte + 2] = pixel;
		curByte += bytesPerPixel;
	}

	return true;
}

bool TGA::LoadCompresed(File& in, TGAHead& header, TextureSubresource* resource)
{
	unsigned int curByte = 0;
	unsigned int bytesPerPixel = header.bpp / 8;
	unsigned int numPixels = header.width * header.height;
	unsigned int imageSize = numPixels * bytesPerPixel;
	unsigned char chunkHead;
	unsigned int pixelsRead = 0;
	unsigned char buffer[4]; // buffer used for decompression

	do
	{
		in.Read(&chunkHead, sizeof(char));
		if (chunkHead < 128) // RAW - not compressed
		{
			++chunkHead; //increase by one to determine how many pixels follow
			pixelsRead += chunkHead;

			unsigned char* cPixel = (unsigned char*)resource->pixel;
			in.Read(&cPixel[curByte], chunkHead * bytesPerPixel);

			// swap colour channels and move in to the resource
			for (int i = 0; i < chunkHead; ++i)
			{
				char temp = cPixel[curByte + 0];
				cPixel[curByte + 0] = cPixel[curByte + 2];
				cPixel[curByte + 2] = temp;

				curByte += bytesPerPixel;
			}
		}
		else //RLE - compressed
		{
			chunkHead -= 127;
			pixelsRead += chunkHead;

			in.Read(buffer, bytesPerPixel);

			char temp = buffer[0];
			buffer[0] = buffer[2];
			buffer[2] = temp;

			unsigned char* cPixel = (unsigned char*)resource->pixel;
			for (int i = 0; i < chunkHead; ++i)
			{
				memcpy(&cPixel[curByte], buffer, bytesPerPixel);
				curByte += bytesPerPixel;
			}
		}

	} while (pixelsRead < numPixels);

	return true;
}
/*
bool TGA::ConvertToGreyScale()
{
	int width = GetWidth();
	int height = GetHeight();
	int bpp = GetBPP();
	char* temp = image;

	image = new char[width * height * sizeof(unsigned int)];

	unsigned int* intPtr = (unsigned int*)image;
	for (int i = 0; i < width * height; ++i)
		intPtr[i] = (unsigned int)1.0f;

	#define RES 256
	float frequency = 20.0f;
	unsigned char *data = new unsigned char[RES*RES];
	unsigned char *dp = data;
	for (int i = 0; i < RES; ++i) {
		for (int j = 0; j < RES; ++j) {
			float u = float(i) / float(RES);
			float v = float(j) / float(RES);
			u *= frequency;
			v *= frequency;
			if ((int(u) + int(v)) & 1) {
				*dp++ = (unsigned char)1.0f;
				//*dp++ = color0[1];
				//*dp++ = color0[2];
			}
			else {
				*dp++ = (unsigned char)0.0f;
				//*dp++ = color1[1];
				//*dp++ = color1[2];
			}
			//*dp++ = 1.;
		}
	}

	image = (char*)data;

/*
	for (int w = 0; w < width; ++w)
	{
		for (int h = 0; h < height; ++h)
		{
			image[w + h * width] = temp[(w + h * width) * bpp];
		}
	}* /

	delete[] temp;
	return true;
}

bool TGA::ConvertToRGBA()
{
	if (GetBPP() == 4)
		return true;

	int width = GetWidth();
	int height = GetHeight();
	int bpp = GetBPP();
	char* temp = image;

	image = new char[width * height * (sizeof(char) * 4)];
	char pixel[4];
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			memcpy(pixel, &temp[(y * width + x) * 3], sizeof(char) * 3);
			pixel[3] = 1.f;
			memcpy(&image[(y * width + x) * 4], pixel, sizeof(char) * 4);
		}
	}

	header.bpp = 32;

/*
	unsigned int* intPtr = (unsigned int*)image;
	for (int i = 0; i < width * height; ++i)
		intPtr[i] = (unsigned int)1.0f;

	#define RES 256
	float frequency = 20.0f;
	unsigned char *data = new unsigned char[RES*RES];
	unsigned char *dp = data;
	for (int i = 0; i < RES; ++i) {
		for (int j = 0; j < RES; ++j) {
			float u = float(i) / float(RES);
			float v = float(j) / float(RES);
			u *= frequency;
			v *= frequency;
			if ((int(u) + int(v)) & 1) {
				*dp++ = (unsigned char)1.0f;
				//*dp++ = color0[1];
				//*dp++ = color0[2];
			}
			else {
				*dp++ = (unsigned char)0.0f;
				//*dp++ = color1[1];
				//*dp++ = color1[2];
			}
			//*dp++ = 1.;
		}
	}

	image = (char*)data;

/*
	for (int w = 0; w < width; ++w)
	{
		for (int h = 0; h < height; ++h)
		{
			image[w + h * width] = temp[(w + h * width) * bpp];
		}
	}* /

	delete[] temp;
	return true;
}

void TGA::FlipV()
{
	// flip texture vertically
	int width = GetWidth();
	int height = GetHeight();
	int bpp = GetBPP();

	int totalSize = width * height * GetBPP();
	char* newImage = new char[totalSize];	

	for (int y = 0; y < height; ++y)
	{
		int pitch = sizeof(char) * GetBPP() * width;
		int offset = pitch * y;
		memcpy(newImage + totalSize - (offset + pitch), image + offset, pitch);
	}

	delete[] image;
	image = newImage;
}

bool TGA::CreateCubeFace(TGA& tga, int idx)
{
	int faceWidth = GetWidth() / 6;
	tga.header.width = faceWidth;
	tga.header.height = GetHeight();
	tga.header.bpp = GetBPP() * 8;

	tga.image = new char[faceWidth * GetHeight() * GetBPP()];

	unsigned int offset = faceWidth * GetBPP() * idx;
	for (int y = 0; y < GetHeight(); ++y)
	{
		memcpy(&tga.image[y * faceWidth * GetBPP()], &image[offset], faceWidth * GetBPP() * sizeof(char));
		offset += GetWidth() * GetBPP();
	}

	return true;
}

void TGA::GetMipmap(TGA& tga, int level)
{
	int origWidth = GetWidth();
	int width = GetWidth();
	int height = GetHeight();
	int bpp = GetBPP();

	width >>= level;
	height >>= level;

	tga.header.width = width;
	tga.header.height = height;
	tga.header.bpp = bpp * 8;

	tga.image = new char[width * height * bpp];
	memset(tga.image, 0, sizeof(char) * width * height * bpp);

	char pixel[4];
	int curPixel = 0;
	int curMipPixel = 0;
	int skipCount = 1 << level;

	for (int y = 0, y0 = 0; y < height; ++y, y0 += skipCount)
	{
		for (int x = 0, x0 = 0; x < width; ++x, x0 += skipCount)
		{
			unsigned char pixel[4];
			GetBilinearPixel(x0, y0, pixel);
			memcpy(&tga.image[((y * width) + x) * bpp], pixel, bpp * sizeof(unsigned char));
		}
	}

	int nothing = 0;
}

struct BilinearFilter
{
	int		x;
	int		y;
	float	weight;
};

BilinearFilter bilinearFilter[9] =
{
	{0, 0, 0.5f},
	{-1, 1, 0.0625f},
	{0, 1, 0.0625f},
	{1, 1, 0.0625f},
	{1, 0, 0.0625f},
	{1, -1, 0.0625f},
	{0, -1, 0.0625f},
	{-1, -1, 0.0625f},
	{-1, 0, 0.0625f},
};

void TGA::GetBilinearPixel(int x, int y, unsigned char* data)
{
	int bpp = GetBPP();
	float floatData[4] = {0,0,0,0};

	for (int i = 0; i < 9; ++i)
	{
		unsigned char pixel[4];
		GetPixel(x + bilinearFilter[i].x, y + bilinearFilter[i].y, pixel);
		for (int j = 0; j < bpp; ++j)
		{
			floatData[j] += pixel[j] * bilinearFilter[i].weight;
		}
	}

	for (int j = 0; j < bpp; ++j)
		data[j] = floatData[j];
}

void TGA::GetPixel(int x, int y, unsigned char* data)
{
	int width = GetWidth();
	int height = GetHeight();
	int bpp = GetBPP();

	if (x >= width)
		x = 0;

	if (y >= height)
		y = 0;

	if (x < 0)
		x = width - 1;

	if (y < 0)
		y = height - 1;

	memcpy(data, &image[((y * width) + x) * bpp], bpp * sizeof(char));
}
*/

bool TGA::Save(const char* file)
{
	return false;
}