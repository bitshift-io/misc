#ifndef _TEXTUREFILE_H_
#define _TEXTUREFILE_H_

#include <vector>

using namespace std;

//
// replace channels with this :)
//
enum TextureFormat
{
	TF_A,
	TF_RGB,
	TF_RGBA,
	TF_HDR_A,
	TF_HDR_RGB,
	TF_HDR_RGBA,
};

struct TextureSubresource
{
	void*			pixel;
	TextureFormat	format;
	int				width;
	int				height;
	int				depth;
};


class TextureResource
{
public:

	struct PixelFilter
	{
		int		x;
		int		y;
		float	weight;
	};

	static PixelFilter BilinearFilter[];


	virtual bool			Load(const char* file) = 0;
	virtual bool			Save(const char* file) = 0;

	virtual void			Deinit();

	static int				GetChannelCount(TextureFormat format); // bit depth, bytes per pixel
	static int				GetChannelSize(TextureFormat format);  // number of bytes a single channel takes up

	int						GetAutoMipmapCount();
	void					GenerateMipmap(int count);
	void					GenerateCubemap();

	TextureSubresource*	CreateResourceInfo(TextureFormat format, int width, int height, int depth = 1);
	void					ReleaseResourceInfo(TextureSubresource** resource);

	int						GetResourceInfoCount();
	TextureSubresource*	GetResourceInfo(int index);

	bool					ConvertToFormat(TextureFormat format);

	void					FlipVertical(TextureSubresource* resource);

	template <class T>
	void					GetFilterPixel(TextureSubresource* resource, int x, int y, T* data, TextureResource::PixelFilter* filter = TextureResource::BilinearFilter);

	template <class T>
	void					GetPixel(TextureSubresource* resource, int x, int y, T* data);

protected:

	vector<TextureSubresource*>	mResource;
};

template <class T>
void TextureResource::GetFilterPixel(TextureSubresource* resource, int x, int y, T* data, TextureResource::PixelFilter* filter)
{
    TextureFormat format = resource->format;
	int bpp = GetChannelCount(format);
	float floatData[4] = {0.f,0.f,0.f,0.f};

	T* cData = (T*)data;
	int i = 0;
	while (filter[i].weight != -1.0f)
	{
		GetPixel<T>(resource, x + filter[i].x, y + filter[i].y, cData);
		for (int j = 0; j < bpp; ++j)
		{
			floatData[j] += cData[j] * filter[i].weight;
		}

		++i;
	}

	for (int j = 0; j < bpp; ++j)
		cData[j] = (T)floatData[j];
}

template <class T>
void TextureResource::GetPixel(TextureSubresource* resource, int x, int y, T* data)
{
	int width = resource->width;
	int height = resource->height;
	int bpp = GetChannelCount(resource->format);

	if (x >= width)
		x = 0;

	if (y >= height)
		y = 0;

	if (x < 0)
		x = width - 1;

	if (y < 0)
		y = height - 1;

	T* cPixel = (T*)resource->pixel;
	memcpy(data, &cPixel[((y * width) + x) * bpp], bpp * sizeof(T));
}


#endif
