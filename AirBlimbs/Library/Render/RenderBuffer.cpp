#include "RenderBuffer.h"


bool RenderBuffer::SaveVector(const File& out, void* data, unsigned int size)
{
	// write the length of the array
	if (out.Write(&size, sizeof(unsigned int)) != sizeof(unsigned int))
		return false;

	out.Write(data, size);
	return true;
}

unsigned int RenderBuffer::LoadVector(const File& in, void** data)
{
	unsigned int size = -1;
	if (in.Read(&size, sizeof(unsigned int)) != sizeof(unsigned int))
		return -1;

	*data = _aligned_malloc(size, 16);
	in.Read(*data, size);
	return size;
}

unsigned int RenderBuffer::GetTypeSize(RenderBufferType type)
{
	switch (type)
	{
	case RBT_Vector4:
		return sizeof(Vector4);

	case RBT_UnsignedInt:
		return sizeof(unsigned int);

	case RBT_Matrix4:
		return sizeof(Matrix4);
	}

	return -1;
}

unsigned int RenderBuffer::GetCount()
{
	return GetSize() / GetTypeSize(GetType());
}