#ifndef _FONT_H_
#define _FONT_H_

#include "Texture.h"
#include <list>
#include "SceneObject.h"

class RenderBuffer;
class Device;
class Geometry;
class Material;

enum FontAlignment
{
	FA_Center,
	FA_Left,
	FA_Right,
};

class Font : public SceneObject
{
public:

	TYPE(12)

	Font() :
		mGeometry(0),
		mMaterial(0),
		mFactory(0)
	{
	}

	bool	Save(const File& out, SceneNode* node)														{ return true; }
	bool	Load(const ObjectLoadParams* params, SceneNode* node);

	bool	Print(float x, float y, float scale, const char* text, ...);
	bool	RenderText(float x, float y, float scale, const char* text, ...);
	bool	RenderText(const Matrix4& transform, const char* text, ...);

	int		GetLineHeight() const											{ return mHeight; }

	// TODO: handle new line character, return the longest
	int		GetTextWidth(const char* text, ...);

	void	Render();
	void	ClearText();

	virtual bool	IsInstance(const ObjectLoadParams* params, SceneObject* loaded);

	// advanced rendering, this doesnt use the mText array, its up to you to manually render
	void	RenderBegin();
	void	RenderEnd();

	bool	RenderTextInternal(float x, float y, float scale, const char* text);
	bool	RenderTextInternal(const Matrix4& transform, const char* text);

	virtual void	AddReference();
	virtual bool	ReleaseReference();

protected:

	struct Text
	{
		char	text[256];
		float	x;
		float	y;
		float	scale;

		Text() { }
		Text(const char* text, float x, float y, float scale) : x(x), y(y), scale(scale)
			{ strcpy(this->text, text);	}
	};

	struct CharDesc
	{
		bool			valid;

		unsigned short	x;
		unsigned short	y;

		unsigned short	width;
		unsigned short	height;

		short	xOffset;
		short	yOffset;

		short	xAdvance;

		// render data
		Vector4			position[4];
		Vector4			texCoord[4];

		CharDesc() : width(0), height(0), xOffset(0), yOffset(0), xAdvance(0), valid(false) { }
	};

	
	void	GenerateRenderList(RenderFactory* factory, Device* device);

	void	GenerateGeometryBuffer(RenderFactory* factory, Device* device, int size);

	virtual void Destroy(RenderFactory* factory);

	Material*		mMaterial;
	vector<Text>	mText;
	CharDesc		mCharacter[256];
	int				mHeight;
	int				mTexWidth;
	int				mTexHeight;
	Geometry*		mGeometry;
	RenderFactory*	mFactory;
};

#endif

