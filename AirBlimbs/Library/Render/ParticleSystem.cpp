#include "ParticleSystem.h"
#include "Device.h"
#include "Camera.h"
#include "Scene.h"
#include "RenderFactory.h"
#include "Template/Time.h"
#include <algorithm>

#undef GetObject


class SortComparison
{
public:

	bool operator () (Particle* part1, Particle* part2)
	{
		return part1->mDistToPointSqrd > part2->mDistToPointSqrd;
	}
};



void Particle::Update(ParticleSystem* system)
{
	mLife += system->GetDeltaTime();
	mTransform.SetTranslation(mTransform.GetTranslation() + mVelocity * system->GetDeltaTime());

	ParticleActionGroup::Iterator it;
	for (it = mActionGroup->Begin(); it != mActionGroup->End(); ++it)
	{
		if ((*it)->Update(system, this) == false)
			break;
	}
}

// =====================================================

void ParticleActionGroup::AddAction(ParticleAction* action)
{
	action->mActionGroup = this;
	mAction.push_back(action);
}

SceneObject* ParticleActionGroup::Clone(SceneNode* owner)
{
	ParticleActionGroup* group = (ParticleActionGroup*)owner->GetOwner()->GetRenderFactory()->Create(ParticleActionGroup::Type);

	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		group->AddAction((*it)->Clone(group, owner));
	}
	group->mSceneNode = owner;
	return group;
}

void ParticleActionGroup::Update(ParticleSystem* system)
{
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		if (!system->IgnoreAction(*it))
			(*it)->Update(system);
	}
}
/*
void ParticleActionGroup::Update(ParticleSystem* system, Particle* particle)
{
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		(*it)->Update(system, particle);
	}
}*/

int ParticleActionGroup::GetRemainingActionCount(ParticleSystem* system, int actionType)
{
	int count = 0;
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		if ((*it)->GetType() == actionType && !system->IgnoreAction(*it))
			++count;
	}

	return count;
}

bool ParticleActionGroup::Save(const File& out, SceneNode* node)
{
	// write the count
	unsigned int count = mAction.size();
	out.Write(&count, sizeof(unsigned int));

	// write the actions
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		// write the type
		int type = (*it)->GetType();
		out.Write(&type, sizeof(int));

		(*it)->Save(out, node);
	}

	return true;
}

bool ParticleActionGroup::Load(const ObjectLoadParams* params, SceneNode* node)
{
	// read the count
	unsigned int count = 0;
	params->mFile->Read(&count, sizeof(unsigned int));

	for (unsigned int i = 0; i < count; ++i)
	{
		// read the type
		int type = -1;
		params->mFile->Read(&type, sizeof(int));

		// read the object
		ParticleAction* action = CreateAction(type);
		action->Load(params, node);
		AddAction(action);
	}

	mSceneNode = node;
	return true;
}

ParticleAction*	ParticleActionGroup::CreateAction(int type)
{
	switch (type)
	{
	case ParticleBirth::Type:
		return new ParticleBirth;
	case ParticleSpeed::Type:
		return new ParticleSpeed;
	case ParticlePosition::Type:
		return new ParticlePosition;
	case ParticleDeath::Type:
		return new ParticleDeath;
	case ParticleForce::Type:
		return new ParticleForce;
	case ParticleWind::Type:
		return new ParticleWind;
	}

	return 0;
}

void ParticleActionGroup::ReleaseAction(ParticleAction** action)
{
	if (!*action)
		return;

	delete *action;
	*action = 0;
}

// =====================================================

ParticleContainer::ParticleContainer()
{
	mSeperator = 0;
	//mSeperator = mLiveList.end();
}

Particle* ParticleContainer::Create(ParticleActionGroup* actionGroup)
{
	Particle* particle = new Particle();
	return particle;
}

void ParticleContainer::Reset()
{
	Clear();
}

void ParticleContainer::SetLizeListSize(unsigned int size)
{
	// move to the dead list
	if (size > mLiveList.size())
	{
		Iterator it;
		for (it = mDeadList.begin(); it != mDeadList.end(); )
		{
			if (size == mLiveList.size())
				return;

			it = mDeadList.erase(it);
			mLiveList.push_back(*it);
		}

		while (mLiveList.size() < size)
		{
			mLiveList.push_back(Create(0));
		}
	}

	// live list is to big!
	else if (size < mLiveList.size())
	{
		Iterator it = mLiveList.begin() + size;
		for (it = mLiveList.begin(); it != mLiveList.end(); )
		{
			it = mLiveList.erase(it);
			mDeadList.push_back(*it);
		}
	}
}

Particle* ParticleContainer::AddParticle(ParticleActionGroup* actionGroup)
{
	// not enough particles, so increase the pool size
	if (mDeadList.size() <= 0)
	{
		for (int i = 0; i < 10; ++i)
		{
			mDeadList.push_back(Create(actionGroup));
		}
	}

	Particle* particle = mDeadList[mDeadList.size() - 1];// mDeadList.front();
	mDeadList.pop_back(); //pop_front();
	particle->Init();
	particle->mActionGroup = actionGroup;

/*
	if (GetSeperator() == mLiveList.end())
	{
		mLiveList.push_back(particle);
		mSeperator = mLiveList.size(); //mLiveList.insert(mLiveList.end(), particle);
	}
	else*/
	{
		// add a dead particle to the alive list
		// set life to 0, if life is zero we can tell if its a new particle
		mLiveList.push_back(particle);
	}

	return particle;
}

ParticleContainer::Iterator ParticleContainer::GetSeperator()
{
	Iterator it = mLiveList.begin();
	std::advance(it, mSeperator);
	return it;
}

void ParticleContainer::SortByDistance(ParticleSystem* system, const Camera* camera)
{
	// get cam pos and convert to particle system local space
	const Matrix4 camWorld = camera->GetWorld();
	Vector4 camPos = camera->GetPosition();
	Matrix4 mWorldToLocal = system->GetTransform();
	mWorldToLocal.QuickInverse();
	camPos = mWorldToLocal * camPos;
	Plane viewPlane(camPos, camWorld.GetZAxis());

	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		(*it)->mDistToPointSqrd = viewPlane.DistanceToPoint((*it)->mTransform.GetTranslation());
	}

	// sort by depth
	SortComparison compare;
	std::sort(mLiveList.begin(), mLiveList.end(), compare);
}

ParticleSystem::Iterator ParticleContainer::Erase(Iterator it)							
{ 
	if (GetSeperator() >= it)
		--mSeperator;

	mDeadList.push_back(*it);
	return mLiveList.erase(it); 
}

void ParticleContainer::Clear()										
{ 
	Iterator it;
	for (it = mLiveList.begin(); it != mLiveList.end(); ++it)
	{
		mDeadList.push_back(*it);
	}

	mLiveList.clear(); 
	//mSeperator = mLiveList.begin(); 
	mSeperator = 0;
}

void ParticleContainer::Update(ParticleSystem* system)
{
	//mSeperator = mLiveList.end();
	mSeperator = mLiveList.size();

	// update
	Iterator it;
	for (it = ExistingBegin(); it != ExistingEnd(); )
	{
		(*it)->Update(system);

		if ((*it)->mLife == -1.f)
			it = Erase(it);
		else
			++it;
	}
}

// =====================================================

void ParticleSystem::Init(Scene* scene, ParticleContainer* container)
{
	mScene = scene;
	mTransform.SetIdentity();
	mContainer = container;

	Reset();
}

void ParticleSystem::Reset()
{
	mTime = 0.f;
	mIgnoreList.clear();
	mContainer->Reset();
}

void ParticleSystem::Draw(Device* device, Camera* camera)
{
	camera->Bind(device);

	Sphere sphere;
	sphere.position = mTransform.GetTranslation();
	sphere.radius = 1.f;
	device->DrawSphere(sphere);

	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		const Particle* particle = *it;
		device->DrawMatrix(mTransform * particle->mTransform);
	}
}

void ParticleSystem::Update(float deltaTime)
{
	mTime += deltaTime;
	mContainer->Update(this);

	mDeltaTime = deltaTime;
	Update(deltaTime, mScene->GetRootNode());
/*
	// update particles
	Iterator it;
	for (it = ExistingBegin(); it != ExistingEnd(); ++it)
	{
		(*it)->Update(deltaTime);
	}*/
}

void ParticleSystem::Update(float deltaTime, SceneNode* node)
{
	if (node && node->GetObject() && node->GetObject()->GetType() == ParticleActionGroup::Type)
	{
		ParticleActionGroup* group = static_cast<ParticleActionGroup*>(node->GetObject());
		group->Update(this);
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
	{
		// only update nodes from the same scene
		if (node->GetOwner() == (*it)->GetOwner())
			Update(deltaTime, *it);
	}
}

void ParticleSystem::AddToIgnoreList(ParticleAction* action)
{
	mIgnoreList.push_back(action);
}

bool ParticleSystem::IgnoreAction(ParticleAction* action)
{
	list<ParticleAction*>::iterator it;
	for (it = mIgnoreList.begin(); it != mIgnoreList.end(); ++it)
	{
		if (action == *it)
			return true;
	}

	return false;
}

Particle* ParticleSystem::AddParticle(ParticleActionGroup* actionGroup)	
{ 
	return mContainer->AddParticle(actionGroup); 
}

bool ParticleSystem::IsFinished()
{
	if (mContainer->GetExistingCount() == 0 && GetRemainingActionCount(mScene->GetRootNode(), ParticleBirth::Type) == 0)
		return true;

	return false;
}

int ParticleSystem::GetRemainingActionCount(SceneNode* node, int actionType)
{
	int count = 0;
	if (node && node->GetObject() && node->GetObject()->GetType() == ParticleActionGroup::Type)
	{
		ParticleActionGroup* group = static_cast<ParticleActionGroup*>(node->GetObject());
		group->GetRemainingActionCount(this, actionType);
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
	{
		count += GetRemainingActionCount(*it, actionType);
	}

	return count;
}

void ParticleSystem::SortByDistance(const Camera* camera)
{
	mContainer->SortByDistance(this, camera);
}