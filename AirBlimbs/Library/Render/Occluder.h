#ifndef _OCCLUDER_H_
#define _OCCLUDER_H_

#ifdef WIN32
	#include <map>
	using namespace std;
#else
	#include <multimap.h>
#endif

#include "Math/Frustum.h"

class Camera;
class Scene;
class SceneNode;
class Device;
class Shape;

enum Visibility
{
	V_Visible,
	V_Culled,
	V_Occluded,
};

struct VisibilityNode
{
	VisibilityNode() { node = 0; visibility = V_Visible; }

	VisibilityNode(SceneNode* _node)	{ node = _node; visibility = V_Visible; }

	Visibility			visibility;
	SceneNode*			node;
};

//
// occluders can be culled to!
//
struct OccluderNode
{
	OccluderNode() { node = 0; visibility = V_Visible; }
	OccluderNode(SceneNode* _node)	{ node = _node; visibility = V_Visible; }

	Visibility			visibility;
	SceneNode*			node;
};

//
// handles culling/occlusion of scene
//
class Occluder
{
public:

	typedef  multimap<void*, OccluderNode>		OccluderHash;
	typedef  multimap<void*, VisibilityNode>	VisibilityHash;


	bool			InsertScene(Scene* scene);
	bool			RemoveScene(Scene* scene);

	bool			InsertSceneNode(SceneNode* node);
	bool			RemoveSceneNode(SceneNode* node);

	bool			InsertOccluder(SceneNode* shape);
	bool			RemoveOccluder(SceneNode* shape);

	void			SetCamera(Camera* camera)						{ mCamera = camera; }
	Camera*			GetCamera() const								{ return mCamera; }

	Visibility		GetVisibility(SceneNode* node);
	bool			SetVisibility(SceneNode* node, Visibility);

	void			CalculateVisibility();
	void			DebugDraw(Device* device);

protected:

	void			PlaneOccludeSceneNode(SceneNode* occluderNode, Shape* occluderShape);
	void			PlaneOccludeOccluder(OccluderHash::iterator occluderIt, SceneNode* occluderNode, Shape* occluderShape);

	Frustum			GeneratePlaneOccluderFrustum(SceneNode* occluderNode, Shape* occluderShape);

	Camera*				mCamera;
	OccluderHash		mOccluderNode;
	VisibilityHash		mVisibilityNode;
};

#endif
