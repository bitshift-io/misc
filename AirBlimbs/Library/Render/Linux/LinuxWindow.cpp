#include "LinuxWindow.h"
//#include "File/Log.h"

#ifdef UNIX


bool LinuxWindow::Init(const char* title, int width, int height, bool fullscreen, int bits)
{
	mTitle = title;
	mFullScreen = fullscreen;
	mExit = false;
	mWidth = width;
	mHeight = height;


    using namespace Linux;


    XVisualInfo         *vi;
    Colormap             cmap;
    XSetWindowAttributes swa;
    GLXContext           cx;
    XEvent               event;
    GLboolean            needRedraw = GL_FALSE, recalcModelView = GL_TRUE;
    int                  dummy;
    GLboolean           doubleBuffer = GL_TRUE;

    int snglBuf[] = {GLX_RGBA, GLX_DEPTH_SIZE, 16, 0};
    int dblBuf[]  = {GLX_RGBA, GLX_DEPTH_SIZE, 16, GLX_DOUBLEBUFFER, 0};

    /*** (1) open a connection to the X server ***/

    mDisplay = XOpenDisplay(NULL);
    if (mDisplay == NULL)
    {
        //Log::Error("could not open display");
        return false;
    }

    /*** (2) make sure OpenGL's GLX extension supported ***/

    if(!glXQueryExtension(mDisplay, &dummy, &dummy))
    {
        //Log::Error("X server has no OpenGL GLX extension");
        return false;
    }

    /*** (3) find an appropriate visual ***/

    /* find an OpenGL-capable RGB visual with depth buffer */
    vi = glXChooseVisual(mDisplay, DefaultScreen(mDisplay), dblBuf);
    if (vi == NULL)
    {
        vi = glXChooseVisual(mDisplay, DefaultScreen(mDisplay), snglBuf);
        if (vi == NULL)
        {
            //Log::Error("no RGB visual with depth buffer");
            return false;
        }

        doubleBuffer = GL_FALSE;
    }
/*
    if (vi->class != TrueColor)
    {
        //Log::Error("TrueColor visual required for this program");
        return true;
    }
*/
    /*** (4) create an OpenGL rendering context  ***/

    /* create an OpenGL rendering context */
    cx = glXCreateContext(mDisplay, vi, /* no shared dlists */ 0,/* direct rendering if possible */ GL_TRUE);
    if (cx == NULL)
    {
        //Log::Error("could not create rendering context");
        return false;
    }

    /*** (5) create an X window with the selected visual ***/

    /* create an X colormap since probably not using default visual */
    cmap = XCreateColormap(mDisplay, RootWindow(mDisplay, vi->screen), vi->visual, AllocNone);

    swa.colormap = cmap;
    swa.border_pixel = 0;
    swa.event_mask = KeyPressMask | ExposureMask | ButtonPressMask | StructureNotifyMask;

    mWindow = XCreateWindow(mDisplay, RootWindow(mDisplay, vi->screen), 0, 0,
                width, height, 0, vi->depth, InputOutput, vi->visual,
                CWBorderPixel | CWColormap | CWEventMask, &swa);

    XSetStandardProperties(mDisplay, mWindow, title, title, None, NULL, 0, NULL);
                //argv, argc, NULL);

    /*** (6) bind the rendering context to the window ***/

    glXMakeCurrent(mDisplay, mWindow, cx);

    return true;
}

void LinuxWindow::Deinit()
{

}

bool LinuxWindow::Show()
{
    /*** (7) request the X window to be displayed on the screen ***/

    XMapWindow(mDisplay, mWindow);

	return true;
}

bool LinuxWindow::HandleMessages()
{
    /*** (9) dispatch X events ***/
    using namespace Linux;
    XEvent               event;
/*
    do
    {
        XNextEvent(mDisplay, &event);
        switch (event.type)
        {
        case ConfigureNotify:
            glViewport(0, 0, event.xconfigure.width,
                event.xconfigure.height);
            break;
        }
    } while(XPending(mDisplay));
*/
	return false;
}

bool LinuxWindow::GetCursorPosition(int& x, int& y)
{
	return true;
}

int	LinuxWindow::SetCursorVisibility(bool visible)
{
	return 0;
}

void LinuxWindow::CenterCursor()
{

}

bool LinuxWindow::IsCursorVisible()
{
	return false;
}

bool LinuxWindow::IsResolutionSupported(int width, int height, int bits)
{
	return false;
}

bool LinuxWindow::HasFocus()
{
	return false;
}

const char*	LinuxWindow::GetTitle()
{
	return mTitle.c_str();
}

#endif
