#ifndef _RENDERER_H_
#define _RENDERER_H_

#include <vector>
#include <list>
#include "SceneObject.h"
#include "Effect.h"
#include "Math/Vector.h"

using namespace std;

class Material;
class MaterialBase;
class Mesh;
class Geometry;
class Camera;
class Light;
class RenderTexture;
class RenderBuffer;
class Occluder;

enum InstanceNodeFlags
{
	INF_None = 0,
	INF_Culled = 1 << 0,
};

struct InstanceNode
{
	InstanceNode(SceneNode*	_node, Material* _material, Mesh* _mesh)	{ node = _node; flags = INF_None; material = _material; mesh = _mesh; }

	SceneNode*		node;
	Material*		material;
	Mesh*			mesh;

	// cached info
	int				flags;
	vector<Light*>	closestLight;
};

struct GeometryNode
{
	GeometryNode(Geometry* _geometry, const InstanceNode& instance)	{ geometry = _geometry; instanceNode.push_back(instance); }

	Geometry*				geometry;
	list<InstanceNode>		instanceNode;
	vector<RenderBuffer*>	instanceBuffer;
};
/*
struct MaterialNode
{
	MaterialNode(Material* _material)	{ material = _material; }

	//MaterialBaseNode*	materialBaseNode;
	Material*			material;
	list<MeshNode>		meshNode;
};
*/
struct MaterialBaseNode
{
	MaterialBaseNode(MaterialBase* _materialBase)	{ materialBase = _materialBase; }

	MaterialBase*			materialBase;
	//list<MaterialNode>		materialNode;
	list<GeometryNode>		geometryNode;
};

//
// Render the contained scene nodes
// and renders the child renderers
//
class Renderer
{
public:

	Renderer();

	bool			AddChildRenderer(Renderer* child);
	bool			RemoveChildRenderer(Renderer* child);

	bool			InsertScene(Scene* scene);
	bool			RemoveScene(Scene* scene);

	bool			InsertSceneNode(SceneNode* node);
	bool			RemoveSceneNode(SceneNode* node);

	bool			RemoveMesh(SceneNode* node, Mesh* mesh);
	bool			InsertMesh(SceneNode* node, Mesh* mesh);

	bool			RemoveLight(Light* light);
	bool			InsertLight(Light* light);

	virtual bool	Render(Device* device);

	void			SetCamera(Camera* camera)						{ mCamera = camera; }
	Camera*			GetCamera() const								{ return mCamera; }

	bool			InsertProjector(Camera* projector);
	bool			RemoveProjector(Camera* projector);

	int				GetClosestLights(const Vector4& position, Light* lightList[], int maxLights);

	void			SetRenderTexture(RenderTexture* renderTexture)	{ mRenderTexture = renderTexture; }

	void			SetTime(float time)								{ mTime = time; }

	void			SetOccluder(Occluder* occluder)					{ mOccluder = occluder; }
	Occluder*		GetOccluder()									{ return mOccluder; }

	void			SetMaterialTechniqueID(unsigned int id)			{ mMaterialTechniqueID = id; }

protected:

	typedef list<Renderer*>::iterator			RendererIterator;
	typedef list<Light*>::iterator				LightIterator;
//	typedef list<MaterialNode>::iterator		MaterialNodeIterator;
	typedef list<MaterialBaseNode>::iterator	MaterialBaseNodeIterator;
	typedef list<GeometryNode>::iterator		GeometryNodeIterator;
	typedef list<InstanceNode>::iterator		InstanceNodeIterator;

	// returns true if the material was bound
	virtual bool	RenderGeometryNodePass(Device* device, MaterialBase* material, GeometryNode& geometryNode, int pass, bool materialBound);
	virtual bool	RenderPassOverride(Device* device, MaterialBase* material, GeometryNode& geometryNode, int pass);
	virtual bool	RenderEffectOverride(Device* device, MaterialBase* material, GeometryNode& geometryNode);

	// upload data to shader for instances
	bool			SetEffectInstanceData(Device* device, InstanceNode& instanceNode, MaterialBase* materialBase);
	int				SetEffectInstanceData(Device* device, list<InstanceNode>& instanceNode, MaterialBase* materialBase, InstanceNodeIterator& instNodeIt);
	void			SetInstanceProjectorParameters(EffectParameter* effectParam, const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world, char* buffer);

	bool			IsVisible(InstanceNode* instNode);

	// if you need to upload special effect parameters extend this class then modify these
	virtual void	HandleCustomParameter(Effect* effect, GeometryNode& geometryNode, InstanceNode& instanceNode, const Matrix4& world)							{}
	virtual void	HandleCustomInstanceParameter(Effect* effect, EffectParameter* effectInstMemberParam, InstanceNode& instanceNode, const Matrix4& world, char* buffer, int instIdx)						{}

	//virtual MaterialNode*		InsertMaterial(Material* material);
	virtual MaterialBaseNode*	InsertMaterialBase(MaterialBase* materialBase);

	//list<MaterialNode>		mMaterialNode;
	list<MaterialBaseNode>	mMaterialBaseNode;
	list<Light*>			mLight;
	list<Renderer*>			mChild;
	Camera*					mCamera;
	vector<Camera*>			mProjector;
	RenderTexture*			mRenderTexture;
	Occluder*				mOccluder;
	float					mTime;
	unsigned int			mMaterialTechniqueID; // what material tehcnique ID should we be using to render with?
};

#endif
