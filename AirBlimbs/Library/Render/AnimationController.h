#ifndef _CONTROL_H_
#define _CONTROL_H_

#include "Math/Vector.h"
#include "Math/Matrix.h"
#include "Math/Quaternion.h"
#include "File/File.h"
#include "SceneObject.h"
#include <string>
#include <vector>

using namespace std;

class ObjectLoadParams;
class Animation;
class Scene;
class SceneNode;

enum StreamType
{
	ST_Position,
	ST_Rotation,
	ST_Scale,
	ST_Alpha,
	ST_Float,
	ST_Vector4,
	ST_Matrix4,
	ST_String,

	ST_MAX,
};

class AnimationStream 
{
	friend class AnimationNode;

public:

	AnimationStream();

	virtual bool		Save(const File& out);
	virtual bool		Load(const ObjectLoadParams* params);

	void				SetType(StreamType type)									{ mType = type; }
	StreamType			GetType()													{ return mType; }

	void				GetValue(Matrix4& value, float time) const;
	void				GetValue(float& value, float time) const;
	void				GetValue(string& value, float time)	const;
	void				GetValue(Quaternion& value, float time)	const;
	void				GetValue(Vector4& value, float time) const;

	void				AppendValue(Quaternion& value, float time);
	void				AppendValue(Vector4& value, float time);
	void				AppendValue(float value, float time);

	unsigned int		GetTypeSize(StreamType type);
	float				GetTime(unsigned int index)									{ return mTime[index]; }
	unsigned int		GetCount()													{ return mCount; }

protected:

	void				IncreaseSize(unsigned int size);
	void				GetKeyIndex(float time, unsigned int& firstKey, unsigned int& secondKey, float& blend) const;

	void				Destroy(RenderFactory* factory);
	
	StreamType			mType;
	float*				mTime;
	void*				mData;
	unsigned int		mCount;
	unsigned int		mMaxCount; // animation streams can grow like a stl::vector
};

//
// corresponds to a scene node, names must match!
// 
class AnimationNode
{
	friend class Animation;

public:

	typedef vector<AnimationStream>::iterator Iterator;

	Iterator			Begin()														{ return mStream.begin(); }
	Iterator			End()														{ return mStream.end(); }

	const char*			GetName() const												{ return mName.c_str(); }
	void				SetName(const char* name)									{ mName = name; }

	virtual bool		Save(const File& out);
	virtual bool		Load(const ObjectLoadParams* params);

	Animation*			GetAnimation()												{ return mOwner; }

	AnimationStream*	GetStream(StreamType streamType);
	AnimationStream*	CreateStream();

protected:

	AnimationNode(Animation* owner) : mOwner(owner)
	{
	}

	void		Destroy(RenderFactory* factory);

	vector<AnimationStream>		mStream;
	string						mName;
	Animation*					mOwner;
};

//
// animation that can be applied to a scene
//
class Animation : public SceneObject
{
public:

	TYPE(18)

	typedef vector<AnimationNode*>::iterator Iterator;

	Iterator			Begin()														{ return mNode.begin(); }
	Iterator			End()														{ return mNode.end(); }

	bool				Clone(ObjectLoadParams* params, Scene* copy);

	virtual bool		Save(const File& out, SceneNode* node = 0);
	virtual bool		Load(const ObjectLoadParams* params, SceneNode* node = 0);

	const char*			GetName() const												{ return mName.c_str(); }
	void				SetName(const char* name)									{ mName = name; }

	AnimationNode*		CreateNode()												{ return new AnimationNode(this); }
	void				DestroyNode(AnimationNode* node)							{ delete node; }
	void				InsertNode(AnimationNode* node)								{ mNode.push_back(node); }

	AnimationNode*		GetAnimationNodeByName(const char* name);

	unsigned int		GetNumNode()												{ return (unsigned int)mNode.size(); }

	void				SetLength(float length)										{ mLength = length; }
	float				GetLength()													{ return mLength; }

	virtual bool		IsInstance(const ObjectLoadParams* params, SceneObject* loaded);

protected:

	virtual void		Destroy(RenderFactory* factory);

	vector<AnimationNode*>			mNode;
	string							mName;
	float							mLength;
};

class SceneAnimationNode
{
public:

	void			InsertAnimationNode(AnimationNode* node)		{ mAnimNode.push_back(node); }

	void			ClearBlend()									{ mBlend.clear(); }
	void			InsertBlend(AnimationNode* node, float weight = 1.f, float time = 0.f);
	AnimationNode*	GetAnimationNode(Animation* anim);
	Matrix4			GetTransform();
	void			ProgressTime(float deltaTime, Animation* animation = 0);
	void			SetTime(float time, Animation* animation = 0);

protected:

	struct Blend
	{
		float			weight;
		float			time;
		AnimationNode*	node;
	};

	vector<Blend>					mBlend;
	SceneNode*						mSceneNode;
	vector<AnimationNode*>			mAnimNode;
};
/*
class SceneAnimation
{
public:

	Scene*							mScene;
	vector<Animation*>				mAnimation;
	vector<SceneAnimationNode*>		mNode;
};
/*
//
// list of animations + animation blending, applies anim to scene
//
class Animator
{
public:

	enum AnimatorFlags
	{
		AF_None,
		AF_Loop,
	};

	typedef vector<Animation*>::iterator Iterator;
/*
	unsigned int		GetNumAnimation() const										{ return (unsigned int)mAnimation.size(); }
	Iterator			Begin()														{ return mAnimation.begin(); }
	Iterator			End()														{ return mAnimation.end(); }

	Animation*			GetAnimation(const char* name);
	Animation*			CreateAnimation();

	virtual bool		Save(const File& out);
	virtual bool		Load(const ObjectLoadParams* params);

	void				SetAnimation(Animation* anim, float weight = 1.0f);
	void				SetTime(float time);

	Matrix4				GetTransform();
* /

	void				ProgressTime(float timeDelta);
	void				Apply(Scene* scene);

protected:

	struct Blend
	{
		int				flags;
		float			weight;
		float			time;
		Animation*		anim;
	};

	vector<Animation*>	mAnimation;
	vector<Blend>		mBlend;
};
*/
#endif
