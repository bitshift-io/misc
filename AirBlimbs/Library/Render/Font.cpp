#include "Font.h"
#include "Texture.h"
#include "RenderFactory.h"
#include "Geometry.h"
#include "RenderBuffer.h"
#include "Math/Vector.h"
#include "File/Log.h"
#include "Render/Device.h"
#include "Render/Window.h"
#include "Material.h"
#include "Camera.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

const int cMaxChars = 5120;

void Font::AddReference()
{
	if (mGeometry)
		mGeometry->AddReference();

	if (mMaterial)
		mMaterial->AddReference();

	SceneObject::AddReference();
}

bool Font::ReleaseReference()
{
	mGeometry = mFactory->Release<Geometry>(&mGeometry);
	mMaterial = mFactory->Release<Material>(&mMaterial);
	return SceneObject::ReleaseReference();
}

void Font::Destroy(RenderFactory* factory)
{
//	factory->Release((SceneObject**)&mGeometry);
//	factory->Release((SceneObject**)&mMaterial);
}

bool Font::IsInstance(const ObjectLoadParams* params, SceneObject* loaded)
{
	if (strcmpi(params->mName.c_str(), mMaterial->GetName().c_str()) == 0)
		return true;

	return false;
}


bool Font::Load(const ObjectLoadParams* params, SceneNode* node)
{
	mFactory = params->mFactory;

	string actualName = params->mName + ".fnt";

	// load the material
	mMaterial = static_cast<Material*>(params->mFactory->Load(Material::Type, params));
	//mMaterial = static_cast<Material*>(params->mFactory->Create(Material::Type));
	if (!mMaterial) //->Load(params, 0))
	{
		Log::Error("Failed to load font material: %s\n", params->mName.c_str());
		return false;
	}

	File fntFile(actualName.c_str(), FAT_Read | FAT_Text);
	if (!fntFile.Valid())
	{
		Log::Error("Failed to load font: %s\n", actualName.c_str());
		return false;
	}

	//
	// read the head:
	//
	// info face="Arial" size=60 bold=0 italic=0 charset="ANSI" stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=1,1
	// common lineHeight=60 base=48 scaleW=256 scaleH=256 pages=1

	char fntName[256];
	int size;
	int bold;
	int italic;
	char charSet[256];
	int stretchH;
	int smooth;
	int aa;
	int padding[4];
	int spacing[2];
	int unicode;
	int outline;

	// convert space in between " marks
	// to underscores for easy parsing
	char buffer[256];
	fntFile.ReadLine(buffer, 256);
	int lineLen = strlen(buffer);
	bool firstSpeechMark = false;
	for (int i = 0; i < lineLen; ++i)
	{
		if (buffer[i] == '\"')
			firstSpeechMark = !firstSpeechMark;

		if (firstSpeechMark && buffer[i] == ' ')
			buffer[i] = '_';
	}

	int count = sscanf(buffer, "info face=%s size=%i bold=%i italic=%i charset=%s unicode=%i stretchH=%i smooth=%i aa=%i padding=%i,%i,%i,%i spacing=%i,%i outline=%i",
		&fntName, &size, &bold, &italic, &charSet, &unicode, &stretchH, &smooth, &aa, &padding[0], &padding[1], &padding[2], &padding[3], &spacing[0], &spacing[1], &outline);
	if (count != 16)
	{
		Log::Error("Failed to parse font info: %s\n", actualName.c_str());
		return false;
	}

	int lineHeight;
	int base;
	int scaleW;
	int scaleH;
	int pages;

	// have to do a funky here o.0
	fntFile.ReadString("%s", &buffer);
	fntFile.ReadString(" lineHeight=%i base=%i scaleW=%i scaleH=%i pages=%i",
		&lineHeight, &base, &scaleW, &scaleH, &pages);

	mHeight = lineHeight + spacing[1];
	mTexWidth = scaleW;
	mTexHeight = scaleH;

	while (!fntFile.EndOfFile())
	{
		char lineType[256];
		fntFile.ReadString("%s", lineType);

		// read in the char description
		if (strcmp(lineType, "char") == 0)
		{
			//  id=32   x=0     y=0     width=1     height=0     xoffset=0     yoffset=60    xadvance=14    page=0
			int id;
			int x;
			int y;
			int width;
			int height;
			int xOffset;
			int yOffset;
			int xAdvance;
			int page;

			char buffer[256];
			fntFile.ReadLine(buffer, 256);
			int count = sscanf(buffer, " id=%i   x=%i     y=%i     width=%i     height=%i     xoffset=%i     yoffset=%i    xadvance=%i    page=%i",
				&id, &x, &y, &width, &height, &xOffset, &yOffset, &xAdvance, &page);

			if (count != 9 || id < 0 || id >= 256)
			{
				Log::Error("Failed to parse font character: %s\n", actualName.c_str());
				return false;
			}

			CharDesc& charDesc = mCharacter[id];
			charDesc.valid = true;
			charDesc.x = x;
			charDesc.y = y;
			charDesc.width = width;
			charDesc.height = height;
			charDesc.xOffset = xOffset;
			charDesc.yOffset = yOffset;
			charDesc.xAdvance = xAdvance + spacing[0];
		}
	}

	GenerateRenderList(params->mFactory, params->mDevice);
	GenerateGeometryBuffer(params->mFactory, params->mDevice, cMaxChars); // make sure we have enough for up to 5120 characters per string
	return true;
}

void Font::GenerateRenderList(RenderFactory* factory, Device* device)
{
	int validCount = 0;
	for (int i = 0; i < 256; ++i)
	{
		if (mCharacter[i].valid)
			++validCount;
	}

	for (int i = 0; i < 256; ++i)
	{
		CharDesc& charDesc = mCharacter[i];
		if (charDesc.valid == false)
			continue;

		// top left
		charDesc.position[0] = Vector4(float(charDesc.xOffset), -float(charDesc.yOffset + charDesc.height), 0.f);
		charDesc.texCoord[0] = Vector4(float(charDesc.x) / float(mTexWidth), 1.0f - float(charDesc.y + charDesc.height) / float(mTexHeight));

		// bottom left
		charDesc.position[1] = Vector4(float(charDesc.xOffset), -float(charDesc.yOffset), 0.f);
		charDesc.texCoord[1] = Vector4(float(charDesc.x) / float(mTexWidth), 1.0f - float(charDesc.y) / float(mTexHeight));

		// bottom right
		charDesc.position[2] = Vector4(float(charDesc.xOffset + charDesc.width), -float(charDesc.yOffset), 0.f);
		charDesc.texCoord[2] = Vector4(float(charDesc.x + charDesc.width) / float(mTexWidth), 1.0f - float(charDesc.y) / float(mTexHeight));

		// top right
		charDesc.position[3] = Vector4(float(charDesc.xOffset + charDesc.width), -float(charDesc.yOffset + charDesc.height), 0.f);
		charDesc.texCoord[3] = Vector4(float(charDesc.x + charDesc.width) / float(mTexWidth), 1.0f - float(charDesc.y + charDesc.height) / float(mTexHeight));
	}
}

void Font::GenerateGeometryBuffer(RenderFactory* factory, Device* device, int size)
{
	// generate and lock the buffers
	mGeometry = static_cast<Geometry*>(factory->Create(Geometry::Type));
	mGeometry->Init(device, VF_Position | VF_TexCoord0, size * 4, size * 6, RBU_Dynamic); //RBU_Static); //RBU_Dynamic);
	VertexFrame& buffer = mGeometry->GetVertexFrame();
	RenderBuffer* idxBuffer = mGeometry->GetIndexBuffer();

	RenderBufferLock lock;
	idxBuffer->Lock(lock);

	int idx = 0;
	int vertIdx = 0;
	for (int i = 0; i < size; ++i)
	{
		// set up indices
		lock.uintData[idx + 0] = vertIdx + 0;
		lock.uintData[idx + 1] = vertIdx + 2;
		lock.uintData[idx + 2] = vertIdx + 1;

		lock.uintData[idx + 3] = vertIdx + 0;
		lock.uintData[idx + 4] = vertIdx + 3;
		lock.uintData[idx + 5] = vertIdx + 2;

		idx += 6;
		vertIdx += 4;
	}

	idxBuffer->Unlock(lock);
}

int Font::GetTextWidth(const char* text, ...)
{
	va_list ap;
	char newText[cMaxChars];
 	if( text == 0 )
    	return false;

  	va_start(ap, text);
  	vsprintf(newText, text, ap);
  	va_end(ap);

	int len = strlen(newText);

	int textWidth = 0;
	for (int i = 0; i < len; ++i)
	{
		textWidth += /*mCharacter[newText[i]].width +*/ mCharacter[newText[i]].xAdvance;
	}

	return textWidth;
}

bool Font::Print(float x, float y, float scale, const char* text, ...)
{
	va_list ap;
	char newText[cMaxChars];
 	if( text == 0 )
    	return false;

  	va_start(ap, text);
  	vsprintf(newText, text, ap);
  	va_end(ap);

	if (strlen(newText) <= 0)
		return true;

	mText.push_back(Text(newText, x, y, scale));
	return true;
}

bool Font::RenderText(const Matrix4& transform, const char* text, ...)
{
	va_list ap;
	char newText[cMaxChars];
 	if (text == 0)
    	return false;

  	va_start(ap, text);
  	vsprintf(newText, text, ap);
  	va_end(ap);

	if (strlen(newText) <= 0)
		return true;

	RenderBegin();
	RenderTextInternal(transform, newText);
	RenderEnd();
	return true;
}

bool Font::RenderText(float x, float y, float scale, const char* text, ...)
{
	va_list ap;
	char newText[cMaxChars];
 	if (text == 0)
    	return false;

  	va_start(ap, text);
  	vsprintf(newText, text, ap);
  	va_end(ap);

	if (strlen(newText) <= 0)
		return true;

	RenderBegin();
	RenderTextInternal(x, y, scale, newText);
	RenderEnd();
	return true;
}

void Font::RenderBegin()
{
	BEGINDEBUGEVENT(mMaterial->GetDevice(), "Font", Vector4(1.f, 1.f, 1.f, 1.f));
	mMaterial->Begin();
	mMaterial->BeginPass(0);
}

void Font::RenderEnd()
{
	mMaterial->EndPass();
	mMaterial->End();
	ENDDEBUGEVENT(mMaterial->GetDevice());
}

bool Font::RenderTextInternal(const Matrix4& baseTransform, const char* text)
{
	float x = 0.f;
	float y = 0.f;
	float scale = 1.f;

	WindowResolution res;
	mMaterial->GetDevice()->GetWindow()->GetWindowResolution(res);
	float invWidth = (1.f / (float)res.height);
	Vector4 actualScale(scale * invWidth * 2.f * (1.f / mMaterial->GetDevice()->GetWindow()->GetAspectRatio()), scale * invWidth * 2.f, 1.f);

	float xOrig = x;

	Matrix4 transform(MI_Identity);
	transform.SetScale(actualScale);
	Camera cam;
	cam.SetOrthographic(2.f, 2.f);
	cam.SetAspectRatio(mMaterial->GetDevice()->GetWindow()->GetAspectRatio());

	// lock the buffers
	VertexFrame& buffer = mGeometry->GetVertexFrame();

	RenderBufferLock texCoordLock;
	buffer.mTexCoord[0]->Lock(texCoordLock);

	RenderBufferLock posLock;
	buffer.mPosition->Lock(posLock);

	int vertIdx = 0;
	unsigned int len = strlen(text);

	if (len > cMaxChars)
	{
		Log::Error("[Font::RenderTextInternal] Trying to print to many characters, up the size of cMaxChars\n");
		return false;
	}

	for (unsigned int i = 0 ; i < len; ++i)
	{
		CharDesc& desc = mCharacter[text[i]];
		if (text[i] == '\n')
		{
			y -= GetLineHeight() * actualScale.y;
			x = xOrig;
		}
		else
		{
			transform.SetTranslation(x, y, 0.f);
			Matrix4 finalTransform = transform * baseTransform;

			// calculate text pos and put quad into buffer
			for (int i = 0; i < 4; ++i)
			{
				Vector4 finalPosition = finalTransform * desc.position[i];
				posLock.vector4Data[vertIdx + i] = finalPosition;
				texCoordLock.vector4Data[vertIdx + i] = desc.texCoord[i];
			}

			vertIdx += 4;
			x += desc.xAdvance * actualScale.x;
		}
	}

	// unlock the buffers
	buffer.mTexCoord[0]->Unlock(texCoordLock);
	buffer.mPosition->Unlock(posLock);

	// draw
	mMaterial->GetEffect()->SetMatrixParameters(cam.GetProjection(), cam.GetView(), cam.GetWorld(), Matrix4(MI_Identity));
	mMaterial->SetPassParameter();
	//device->Draw(DT_NotInstanced, mMaterial, mGeometry, mMaterial->GetVertexFormat(0), 0, len * 6);
	mGeometry->Draw(0, len * 6);

	return true;
}

bool Font::RenderTextInternal(float x, float y, float scale, const char* text)
{
	WindowResolution res;
	mMaterial->GetDevice()->GetWindow()->GetWindowResolution(res);
	float invWidth = (1.f / (float)res.height);
	Vector4 actualScale(scale * invWidth * 2.f * (1.f / mMaterial->GetDevice()->GetWindow()->GetAspectRatio()), scale * invWidth * 2.f, 1.f);

	float xOrig = x;

	Matrix4 transform(MI_Identity);
	transform.SetScale(actualScale);
	Camera cam;
	cam.SetOrthographic(2.f, 2.f);
	cam.SetAspectRatio(mMaterial->GetDevice()->GetWindow()->GetAspectRatio());

	// lock the buffers
	VertexFrame& buffer = mGeometry->GetVertexFrame();

	RenderBufferLock texCoordLock;
	buffer.mTexCoord[0]->Lock(texCoordLock);

	RenderBufferLock posLock;
	buffer.mPosition->Lock(posLock);

	int vertIdx = 0;
	unsigned int len = strlen(text);

	if (len > cMaxChars)
	{
		Log::Error("[Font::RenderTextInternal] Trying to print to many characters, up the size of cMaxChars\n");
		return false;
	}

	for (unsigned int i = 0; i < len; ++i)
	{
		CharDesc& desc = mCharacter[text[i]];
		if (text[i] == '\n')
		{
			y -= GetLineHeight() * actualScale.y;
			x = xOrig;
		}
		else
		{
			transform.SetTranslation(x, y, 0.f);
			Matrix4 finalTransform = transform;

			// calculate text pos and put quad into buffer
			for (int i = 0; i < 4; ++i)
			{
				Vector4 finalPosition = finalTransform * desc.position[i];
				posLock.vector4Data[vertIdx + i] = finalPosition;
				texCoordLock.vector4Data[vertIdx + i] = desc.texCoord[i];
			}

			vertIdx += 4;
			x += desc.xAdvance * actualScale.x;
		}
	}

	// unlock the buffers
	buffer.mTexCoord[0]->Unlock(texCoordLock);
	buffer.mPosition->Unlock(posLock);

	// draw
	mMaterial->GetEffect()->SetMatrixParameters(cam.GetProjection(), cam.GetView(), cam.GetWorld(), Matrix4(MI_Identity));
	mMaterial->SetPassParameter();
	//device->Draw(DT_NotInstanced, mMaterial, mGeometry, mMaterial->GetVertexFormat(0), 0, len * 6);
	mGeometry->Draw(0, len * 6);

	return true;
}

void Font::Render()
{
	// TODO: this can all be done as a single draw call!
	RenderBegin();

	unsigned int textSize = mText.size();
	for (unsigned int i = 0; i < textSize; ++i)
	{
		Text& txt = mText[i];
		RenderTextInternal(txt.x, txt.y, txt.scale, txt.text);
	}

	RenderEnd();
}

void Font::ClearText()
{
	mText.clear();
}
