#ifndef _POSTEFFECT_H_
#define _POSTEFFECT_H_

class Material;
class RenderBuffer;
class Device;
class RenderFactory;
class Geometry;

//
// These arent created via factory as you probably need specialized versions!
//
class PostEffect
{
public:

	virtual void		Init(RenderFactory* factory, Device* device, Material* material = 0);
	virtual void		Deinit(RenderFactory* factory, Device* device);
	virtual void		Render(Device* device);
	virtual void		SetTime(float time)														{ mTime = time; }
	virtual Material*	GetMaterial()															{ return mMaterial; }

	static Geometry*	InitQuad(RenderFactory* factory, Device* device, float width, float height);
	static void			DeinitQuad(RenderFactory* factory, Geometry* geometry);
	static void			RenderScreenQuad(Device* device, Geometry* geometry, Material* material, float time = 0.f);

protected:

	float				mTime;
	Geometry*			mGeometry;
	Material*			mMaterial;
};

#endif
