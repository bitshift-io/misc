#ifndef _SKIN_H_
#define _SKIN_H_

#include "SceneObject.h"
#include "Math/Matrix.h"
#include "Mesh.h"

class Bone
{
public:
	bool		Save(const File& out);
	bool		Load(const ObjectLoadParams* params, SceneNode* node);

//protected:

	Matrix4		mSkinToBone;
	SceneNode*	mSceneNode;
};

class SkinMesh : public Mesh
{
public:

	typedef Mesh Parent;

	TYPE(11)

	SkinMesh();

	virtual bool		Save(const File& out, SceneNode* node);
	virtual bool		Load(const ObjectLoadParams* params, SceneNode* node);

	void				SetBone(Bone* bone, unsigned int boneCount);
	void				ApplySkinPalette();
	unsigned int		GetBoneCount()												{ return mBoneCount; }
	Matrix4*			GetSkinPalette()											{ return mSkinPalette; }

protected:

	virtual void		Destroy(RenderFactory* factory);

	//Matrix4*			mSkinToBone;
	SceneNode*			mSceneNode;
	Bone*				mBone;
	Matrix4*			mSkinPalette; // matrix aray to get uploaded to shader
	unsigned int		mBoneCount;
};

#endif