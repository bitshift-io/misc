#ifndef __MATERIAL_H_
#define __MATERIAL_H_

#include "SceneObject.h"
#include "Effect.h"
#include "Math/Matrix.h"
#include "Math/Vector.h"
#include "Device.h"
#include <map>
#include <list>
#include <algorithm>

class Device;
class EffectParam;
class Projector;
class Light;
class EffectParam;
class MaterialBase;
class Material;

using namespace std;

enum MaterialFlags
{
	MF_OverridePass		= 1 << 0,
	MF_OverrideEffect	= 1 << 1,
	MF_Hidden			= 1 << 2,
};

class MaterialPass
{
public:

	typedef vector<EffectParameterValue*>::iterator EffectParameterValueIterator;

	MaterialPass()
	{
	}

	EffectParameterValueIterator		EffectParameterValueBegin()							{ return mParameterData.begin(); }
	EffectParameterValueIterator		EffectParameterValueEnd()							{ return mParameterData.end(); }

	bool							Equals(MaterialPass& rhs);

	vector<EffectParameterValue*>	mParameterData;
	DeviceState						mDeviceState;
};

class MaterialTechnique
{
	friend class Material;
	friend class MaterialBase;

public:

	typedef vector<MaterialPass>::iterator PassIterator;

	MaterialTechnique();

	PassIterator			PassBegin()											{ return mPass.begin(); }
	PassIterator			PassEnd()											{ return mPass.end(); }

	virtual void			SetTechnique(EffectTechnique* technique)			{ mTechnique = technique; }

	Effect*					GetEffect() const									{ return mEffect; }

	bool					operator<(MaterialTechnique& rhs)					{ return mRenderOrder < rhs.mRenderOrder; }

	EffectParameterValue*	GetParameterByUserString(const char* userString);
	EffectParameterValue*	GetParameterByName(const char* name);
	EffectParameterValue*	AddParameterByName(const char* name, int pass = 0);
	bool					AddParameter(EffectParameterValue* effectParamData, int pass = 0);

	bool					Equals(MaterialTechnique& rhs);


	int						Begin();
	void					BeginPass(int idx);
	void					SetPassParameter(); // call this after pass, and after you've set the parameters, but before you render
	void					EndPass();
	void					End();

	const char*				GetName()											{ return mName.c_str(); }
	void					SetID(unsigned int id)								{ mID = id; }

protected:

	MaterialBase*			mMaterial; // owner
	string					mName;
	unsigned int			mID;
	Effect*					mEffect;
	int						mRenderOrder;
	int						mCurPass;

	EffectTechnique*		mTechnique;
	vector<MaterialPass>	mPass;
};

class MaterialBase : public SceneObject
{
	friend class RenderFactory;
	friend class MaterialTechnique;
	friend class Material;

public:

	TYPE(3)

	typedef vector<MaterialTechnique>::iterator TechniqueIterator;
	typedef list<Material*>::iterator MaterialIterator;

	MaterialBase();
	~MaterialBase()													{}

	virtual bool				Save(const File& out, SceneNode* node);
	virtual bool				Load(const ObjectLoadParams* params, SceneNode* node);

	virtual SceneObject*		Clone(SceneNode* owner = 0);

	virtual bool				IsInstance(const ObjectLoadParams* params, SceneObject* loaded);

	void						SetName(const string& name);
	const string&				GetName()											{ return mName; }

	MaterialTechnique*			GetTechniqueByID(unsigned int id);

	TechniqueIterator			TechniqueBegin()											{ return mTechnique.begin(); }
	TechniqueIterator			TechniqueEnd()												{ return mTechnique.end(); }

	virtual void				AddReference();
	virtual bool				ReleaseReference();

	void						SetCurrentTechnique(MaterialTechnique* technique);
	MaterialTechnique*			GetCurrentTechnique();
	

	// this modify the current material technique
	int							Begin();
	void						BeginPass(int idx);
	void						SetPassParameter(); // call this after pass, and after you've set the parameters, but before you render
	void						EndPass();
	void						End();

	Effect*						GetEffect();
	Device*						GetDevice();

	MaterialIterator			MaterialBegin()												{ return mMaterial.begin(); }
	MaterialIterator			MaterialEnd()												{ return mMaterial.end(); }

protected:

	int							GetCurrentTechniqueIndex()									{ return mTechniqueIdx; }

	void						InsertMaterial(Material* material);
	void						RemoveMaterial(Material* material);

	static bool					ReadLine(File* file, char* value);
	static bool					ReadString(File* file, char* value);
	static bool					ReadVector4(File* file, Vector4& value);
	static bool					ReadFloat(File* file, float& value);
	static bool					ReadInt(File* file, int& value);
	static bool					ReadBool(File* file, bool& value);

	BlendMode					ConvertBlendMode(const char* value);

	void						InterpretString(char* line);

	virtual void				Destroy(RenderFactory* factory);

	bool						ReadTechnique(const ObjectLoadParams* params, const string& name);
	bool						ReadPass(const ObjectLoadParams* params, MaterialTechnique& technique);
	bool						ReadVertexFormat(File* file, int& value);

	int							mTechniqueIdx;
	vector<MaterialTechnique>	mTechnique;
	list<Material*>				mMaterial;			// list of all materials that reference this base material
	RenderFactory*				mFactory;
	Device*						mDevice;
	string						mName;

};

//
// Hides material base interface, and provides overriding of the base material
//
class Material : public SceneObject
{
	friend class MaterialBase;

public:

	TYPE(17)

	typedef MaterialBase::TechniqueIterator TechniqueIterator;

	Material();

	virtual bool				Save(const File& out, SceneNode* node);
	virtual bool				Load(const ObjectLoadParams* params, SceneNode* node);

	virtual SceneObject*		Clone(SceneNode* owner = 0);

	virtual bool				IsInstance(const ObjectLoadParams* params, SceneObject* loaded);

	void						SetName(const string& name);
	const string&				GetName();

	MaterialTechnique*			GetTechniqueByID(unsigned int id);

	TechniqueIterator			TechniqueBegin();
	TechniqueIterator			TechniqueEnd();

	virtual void				AddReference();
	virtual bool				ReleaseReference();

	void						SetCurrentTechnique(MaterialTechnique* technique);
	MaterialTechnique*			GetCurrentTechnique();

	// this modify the current material technique
	int							Begin();
	void						BeginPass(int idx);
	void						SetPassParameter(); // call this after pass, and after you've set the parameters, but before you render
	void						EndPass();
	void						End();

	Effect*						GetEffect();

	MaterialBase*				GetMaterialBase();
	void						SetMaterialBase(MaterialBase* materialBase);

	// if you want to edit material properties it needs to be locked and unlocked
	bool						Lock();
	void						Unlock();

	// reset to default base material parameters
	void						Reset();
/*
	// for determining what level this material overrides of the material base
	bool						OverridesEffect();
	bool						OverridesPass();
			*/
	unsigned int				GetFlags()										{ return mFlags; }
	void						SetFlags(unsigned int flags);
	void						ClearFlags(unsigned int flags);

	Device*						GetDevice();

protected:

	//int							mTechniqueIdx;
	bool						mLocked;
	MaterialBase*				mMaterialBase;
	vector<MaterialTechnique>	mTechniqueOverride;
	unsigned int				mFlags;
};

#endif
