#ifndef __LIGHT_H_
#define __LIGHT_H_

#include "SceneObject.h"
#include "Math/Vector.h"

enum ELightType
{
	LT_Omni,
	LT_Spot,
	LT_Directional
};

enum ELightDecayType
{
	LDT_None,
	LDT_Inverse,
	LDT_InverseSquare,
};

class Light : public SceneObject
{
public:

	TYPE(8);

	virtual bool	Save(const File& out, SceneNode* node);
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node);

	void			SetDecayType(ELightDecayType decayType)						{ mDecayType = decayType; }
	ELightDecayType	GetDecayType()												{ return mDecayType; }

	const Vector4&	GetDiffuseColour()											{ return mDiffuse; }
	void			SetDiffuseColour(const Vector4& colour)						{ mDiffuse = colour; }

	void			SetLightType(ELightType type)								{ mType = type; }
	ELightType		GetLightType()												{ return mType; }

	bool			Enabled()													{ return mEnabled; }
	void			SetEnabled(bool enabled)									{ mEnabled = enabled; }

	void			SetBrightness(float brightness)								{ mBrightness = brightness; }
	float			GetBrightness()												{ return mBrightness; }

	// we might hcange this to a shadow type
	// ie. none, stencil, shadow map
	bool			CastShadow()												{ return mCastShadow; }
	void			SetCastShadow(bool castShadow)								{ mCastShadow = castShadow; }

	void			SetPosition(const Vector4& position)						{ mPosition = position; }
	const Vector4&	GetPosition()												{ return mPosition; }

	void			SetDirection(const Vector4& direction)						{ mDirection = direction; }
	const Vector4&	GetDirection()												{ return mDirection; }

	bool			InRange(const Vector4& position);

	// only for debugging please
	void			Draw(Device* device);

protected:

	Vector4			mPosition;
	Vector4			mDirection;

	ELightDecayType	mDecayType;
	Vector4			mDiffuse;
	ELightType		mType;
	float			mBrightness;
	bool			mCastShadow;
	bool			mEnabled;
};

#endif