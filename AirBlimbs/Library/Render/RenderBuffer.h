#ifndef _RENDERBUFFER_H_
#define _RENDERBUFFER_H_

#include "Memory/NoMemory.h"
#include <vector>
#include "Memory/Memory.h"
#include "File/File.h"
#include "Math/Vector.h"
#include "Math/Matrix.h"

using namespace std;

class Device;

enum RenderBufferType
{
	RBT_Vector4,
	RBT_UnsignedInt,
	RBT_Matrix4,
};

#define MAX_TEXCOORD 4

enum RenderBufferUseage
{
	RBU_Static,
	RBU_Dynamic,
};

// this should be something like "rendberbufferuseage"
enum VertexFormat
{
	VF_Position		= 0x1 << 0,
	VF_Normal		= 0x1 << 1,
	VF_Tangent		= 0x1 << 2,
	VF_Binormal		= 0x1 << 3,
	VF_Colour		= 0x1 << 4,
	VF_Index		= 0x1 << 5,
	VF_TexCoord0	= 0x1 << 6,
	VF_TexCoord1	= 0x1 << 7,
	VF_TexCoord2	= 0x1 << 8,
	VF_TexCoord3	= 0x1 << 9,
	VF_BoneData		= 0x1 << 10,
	VF_InstanceId	= 0x1 << 12, // for instancing

	VF_Unknown		= 0x1 << 13,
};

enum RenderBufferLockFlags
{
	RBLF_ReadOnly	= 0x1 << 0,
};

//
// info pertaning to a locked render buffer
//
struct RenderBufferLock
{
	RenderBufferLock() : 
		startIndex(0), 
		count(-1),
		data(0),
		flags(0)
	{
	}

	union
	{
		void*					data;		// points to start of buffer!
		Matrix4*				matrixData;
		Vector4*				vector4Data;
		unsigned int*			uintData;
	};

	RenderBufferUseage		useage;
	VertexFormat			channel;
	unsigned int			startIndex;
	unsigned int			count;
	unsigned int			flags;
};

//
// Render buffer
//
class RenderBuffer
{
public:

	//
	// disk io
	//
	virtual bool			Save(const File& out)	= 0;
	virtual bool			Load(Device* device, const File& in)	= 0;

	//
	// for manual use
	//
	virtual void			Init(RenderBufferType type, unsigned int size = -1, RenderBufferUseage useage = RBU_Static)		{ mType = type; }
	virtual void			Deinit()																	= 0;

	//
	// Bind for rendering
	//
	virtual void			Bind(VertexFormat channel, int index = 0)		= 0;
	virtual void			Unbind()										= 0;

	//
	// locking for modification
	//
	virtual bool			Lock(RenderBufferLock& lock, int lockFlags = 0)					= 0;
	virtual void			Unlock(RenderBufferLock& lock)									= 0;

	//
	// get the data type held by this buffer
	//
	RenderBufferType		GetType()								{ return mType; }

	//
	// size on bytes per data in bytes
	//
	unsigned int			GetTypeSize(RenderBufferType type);
	
	//
	// get total buffer size in bytes
	//
	virtual unsigned int	GetSize()								= 0;

	//
	// get number of data this holds
	//
	virtual unsigned int	GetCount();

	virtual void			ReloadResource(Device* device)			= 0;

	virtual RenderBufferUseage	GetUseage()							= 0;
	virtual void				Draw(unsigned int startIndex = 0, unsigned int numIndices = -1)		= 0;

protected:

	bool			SaveVector(const File& out, void* data, unsigned int size);
	unsigned int	LoadVector(const File& in, void** data);

	RenderBufferType mType;
};

#endif
