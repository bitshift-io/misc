#ifndef _MACWINDOW_H_
#define _MACWINDOW_H_

#ifdef __APPLE__

#include <Carbon/Carbon.h>
#include "../Window.h"

class MacWindow : public Window
{
	friend class OGLDevice;

public:

    virtual bool	Init(const char* title, const WindowResolution* resolution = 0, bool fullscreen = false);
	virtual void	Deinit();
	virtual bool	Show();

	virtual bool						SetWindowResolution(const WindowResolution* resolution);
	virtual bool						GetWindowResolution(WindowResolution& resolution) const;
	virtual bool						GetDesktopResolution(WindowResolution& resolution) const;

	// returns true if quit message recived
	virtual bool	HandleMessages();
	virtual bool	IsFullScreen();


	// returns cursor position relative to client area
	// returns true if in the client area
	virtual bool	SetCursorPosition(int x, int y);
	virtual bool	GetCursorPosition(int& x, int& y);
	virtual int		SetCursorVisibility(bool visible);
	virtual bool	IsCursorVisible();
	virtual void	CenterCursor();

	virtual bool	IsResolutionSupported(int width, int height, int bits);
	virtual bool	HasFocus();

	virtual const char*	GetTitle();

	virtual int		MessageBox(const char* text, const char* title, int flags);

    WindowRef       GetWindowRef()      { return mWindow; }

//protected:

    WindowRef       mWindow;
	bool			mExit;
	bool			mFocus;
	bool			mFullScreen;
	string			mTitle;
};

#endif

#endif
