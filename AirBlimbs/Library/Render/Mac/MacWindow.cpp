#include "MacWindow.h"
//#include "File/Log.h"

#ifdef __APPLE__

const CFStringRef kAboutNibFile = CFSTR("GameWindow");
const CFStringRef kGameWindowNibName = CFSTR("GameWindow");
const CFStringRef kGameWindowName = CFSTR("Game Window");
const PropertyTag kGSGameWindowTag = 'GAME';

bool MacWindow::Init(const char* title, const WindowResolution* resolution, bool fullscreen)
{
	mTitle = title;
	mFullScreen = fullscreen;
	mExit = false;

	IBNibRef nibRef;
	OSStatus status = CreateNibReference(CFSTR("main"), &nibRef);
	status = CreateWindowFromNib(nibRef, CFStringRef(title), &mWindow);
	if (status != noErr)
        return false;

    SetWindowTitleWithCFString(mWindow, CFStringRef(title));
    status = SetWindowProperty(mWindow, PropertyCreator("1234"), kGSGameWindowTag, sizeof(MacWindow), (const void*)this);
    return status != noErr;
}

void MacWindow::Deinit()
{

}

bool MacWindow::SetWindowResolution(const WindowResolution* resolution)
{
    return false;
}

bool MacWindow::GetWindowResolution(WindowResolution& resolution) const
{
    return false;
}

bool MacWindow::GetDesktopResolution(WindowResolution& resolution) const
{
    return false;
}

bool MacWindow::Show()
{
    ShowWindow(mWindow);
	return true;
}

bool MacWindow::HandleMessages()
{
	return false;
}

bool MacWindow::GetCursorPosition(int& x, int& y)
{
	return true;
}

int	MacWindow::SetCursorVisibility(bool visible)
{
	return 0;
}

void MacWindow::CenterCursor()
{

}

bool MacWindow::IsCursorVisible()
{
	return false;
}

bool MacWindow::IsResolutionSupported(int width, int height, int bits)
{
	return false;
}

bool MacWindow::IsFullScreen()
{
    return false;
}

bool MacWindow::HasFocus()
{
	return false;
}

const char*	MacWindow::GetTitle()
{
	return mTitle.c_str();
}

int MacWindow::MessageBox(const char* text, const char* title, int flags)
{
    return 0;
}

#endif
