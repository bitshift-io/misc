#include "Skin.h"
#include "Scene.h"
#include "SceneNode.h"

bool Bone::Save(const File& out)
{	
	// write the node name
	const string& name = mSceneNode->GetName();
	int len = name.length();
	out.Write(&len, sizeof(int));
	out.Write(name.c_str(), sizeof(char) * len);

	// write bone matrices
	out.Write(&mSkinToBone, sizeof(Matrix4));

	return true;
}

bool Bone::Load(const ObjectLoadParams* params, SceneNode* node)
{
	File* in = params->mFile;

	// read the node name
	char buffer[1024];
	int len = 0;
	in->Read(&len, sizeof(int));
	in->Read(buffer, sizeof(char) * len);
	buffer[len] = '\0';
	mSceneNode = node->GetOwner()->GetNodeByName(buffer);

	// read the bone matrix
	in->Read(&mSkinToBone, sizeof(Matrix4));
	return true;
}



SkinMesh::SkinMesh() :
	mBone(0),
	mBoneCount(0),
	mSkinPalette(0),
	mSceneNode(0)
{
}

void SkinMesh::Destroy(RenderFactory* factory)
{
	if (mBone)
		_aligned_free(mBone);
}

bool SkinMesh::Save(const File& out, SceneNode* node)
{
	// write bone count
	out.Write(&mBoneCount, sizeof(unsigned int));

	// write bones
	for (int i = 0; i < mBoneCount; ++i)
	{
		mBone[i].Save(out);
	}

	return Parent::Save(out, node);
}

bool SkinMesh::Load(const ObjectLoadParams* params, SceneNode* node)
{
	File* in = params->mFile;

	mSceneNode = node;

	// read bone count
	in->Read(&mBoneCount, sizeof(unsigned int));

	// read bones
	mBone = (Bone*)_aligned_malloc(sizeof(Bone) * mBoneCount, 16);
	mSkinPalette = (Matrix4*)_aligned_malloc(sizeof(Matrix4) * mBoneCount, 16);
	for (int i = 0; i < mBoneCount; ++i)
	{
		mSkinPalette[i].SetIdentity();
		mBone[i].Load(params, node);
	}

	return Parent::Load(params, node);
}

void SkinMesh::SetBone(Bone* bone, unsigned int boneCount)
{
	mBone = bone;
	mBoneCount = boneCount;
}

void SkinMesh::ApplySkinPalette()
{
	Matrix4 world = mSceneNode->GetWorldTransform();
	Matrix4 worldInverse = world;//mSceneNode->GetWorldTransform();
	worldInverse.Inverse();
	for (int i = 0; i < mBoneCount; ++i)
	{
		Matrix4 boneWorldTransform = mBone[i].mSceneNode->GetWorldTransform();

		if (i == 2)
		{
			int nothing = 0;
		}
		//if (i == 0)
		//{
		//mSkinPalette[i] = mBone[i].mSkinToBone * boneWorldTransform;
			mSkinPalette[i] = world * (mBone[i].mSkinToBone * (boneWorldTransform * worldInverse));
		//	boneWorldTransform = worldInverse * boneWorldTransform;
		//}
		//else
		//{
		//	mSkinPalette[i] = (mBone[i].mSkinToBone * boneWorldTransform);// * world;
		//}
		//mSkinPalette[i] = (mBone[i].mSkinToBone * boneWorldTransform) * worldInverse;

		//if (i > 2)
		//	mSkinPalette[i] = Matrix4(MI_Identity);

		int nothing = 0;
	}
}