#include "TextureResource.h"

TextureResource::PixelFilter TextureResource::BilinearFilter[] =
{
	{0, 0, 0.5f},
	{-1, 1, 0.0625f},
	{0, 1, 0.0625f},
	{1, 1, 0.0625f},
	{1, 0, 0.0625f},
	{1, -1, 0.0625f},
	{0, -1, 0.0625f},
	{-1, -1, 0.0625f},
	{-1, 0, 0.0625f},
	{-1, 0, -1.0f},
};

void TextureResource::Deinit()
{
	vector<TextureSubresource*>::iterator it;
	for (it = mResource.begin(); it != mResource.end(); ++it)
	{
		delete[] (*it)->pixel;
		(*it)->pixel = 0;
	}

	mResource.clear();
}

TextureSubresource* TextureResource::CreateResourceInfo(TextureFormat format, int width, int height, int depth)
{
	TextureSubresource* resource = new TextureSubresource();
	mResource.push_back(resource);
	resource->format = format;
	resource->width = width;
	resource->height = height;
	resource->depth = depth;
	resource->pixel = new unsigned char[GetChannelSize(format) * GetChannelCount(format) * width * height * depth];
	return resource;
}

void TextureResource::ReleaseResourceInfo(TextureSubresource** resource)
{
	if (!*resource)
		return;

	delete[] (*resource)->pixel;
	delete *resource;
}

int TextureResource::GetAutoMipmapCount()
{
	TextureSubresource* info = GetResourceInfo(0);

	int height = info->height;
	if (height <= 32)
		return 1;

	int numMip = 0;
	while (height > 32)
	{
		height >>= 1;
		++numMip;
	}

	return numMip;
}

int TextureResource::GetResourceInfoCount()
{
	return mResource.size();
}

TextureSubresource* TextureResource::GetResourceInfo(int index)
{
	return mResource[index];
}

int	TextureResource::GetChannelCount(TextureFormat format)
{
	switch (format)
	{
	case TF_A:
		return 1;

	case TF_RGB:
		return 3;

	default:
	case TF_RGBA:
		return 4;

	case TF_HDR_A:
		return 1;

	case TF_HDR_RGB:
		return 3;

	case TF_HDR_RGBA:
		return 4;
	}
}

int	TextureResource::GetChannelSize(TextureFormat format)
{
	switch (format)
	{
	default:
	case TF_A:
	case TF_RGB:
	case TF_RGBA:
		return sizeof(unsigned char);

	case TF_HDR_A:
	case TF_HDR_RGB:
	case TF_HDR_RGBA:
		return sizeof(float);
	}
}

bool TextureResource::ConvertToFormat(TextureFormat format)
{
	TextureSubresource* resource = GetResourceInfo(0);
	if (resource->format == format)
		return true;

	TextureFormat oldFormat = resource->format;
	resource->format = format;

	unsigned char* newPixelBuffer = new unsigned char[resource->width * resource->height * GetChannelCount(format) * GetChannelSize(format)];

	switch (GetChannelSize(oldFormat))
	{
	default:
	case sizeof(unsigned char):
		{
			switch (GetChannelSize(format))
			{
			default:
			case sizeof(unsigned char): // convert from char to char type
				{
					unsigned char* oldPixel = (unsigned char*)resource->pixel;
					unsigned char* newPixel = newPixelBuffer;
					int oldBpp = GetChannelCount(oldFormat);
					int newBpp = GetChannelCount(format);
					int minBpp = min(oldBpp, newBpp);

					for (int i = 0; i < resource->width * resource->height; ++i)
					{
						int j = 0;
						for (; j < minBpp; ++j)
							newPixel[i * newBpp + j] = oldPixel[i * oldBpp + j];

						for (; j < newBpp; ++j)
							newPixel[i * newBpp + j] = 255;
					}
				}
				break;

			case sizeof(float): // convert from char to float type
				{

				}
				break;
			}
		}
		break;

	case sizeof(float):
		{
			switch (GetChannelSize(format))
			{
			default:
			case sizeof(unsigned char): // convert from float to char type
				{

				}
				break;

			case sizeof(float): // convert from float to float
				{

				}
				break;
			}
		}
		break;
	}

	delete[] resource->pixel;
	resource->pixel = newPixelBuffer;

	return true;
}

void TextureResource::GenerateMipmap(int count)
{
	TextureSubresource* prevResource = GetResourceInfo(0);
	int size = GetChannelSize(prevResource->format);
	int bpp = GetChannelCount(prevResource->format);

	int skipCount = 2;
	for (int i = 0; i < count; ++i)
	{
		int width = prevResource->width >> 1;
		int height = prevResource->height >> 1;
		TextureSubresource* curResource = CreateResourceInfo(prevResource->format, width, height);

		for (int y = 0, y0 = 0; y < height; ++y, y0 += skipCount)
		{
			for (int x = 0, x0 = 0; x < width; ++x, x0 += skipCount)
			{
				switch (GetChannelSize(prevResource->format))
				{
				default:
				case sizeof(unsigned char):
					{
						unsigned char pixel[4];
						GetFilterPixel<unsigned char>(prevResource, x0, y0, pixel);
						unsigned char* cPixel = (unsigned char*)(curResource->pixel);
						memcpy(&cPixel[((y * width) + x) * bpp], pixel, bpp * size);
					}
					break;

				case sizeof(float):
					{
						float pixel[4];
						GetFilterPixel<float>(prevResource, x0, y0, pixel);
						float* fPixel = (float*)(curResource->pixel);
						memcpy(&fPixel[((y * width) + x) * bpp], pixel, bpp * size);
					}
					break;
				}
			}
		}


		prevResource = curResource;
	}

	/*
	int origWidth = resource->width;
	int width = resource->width;
	int height = resource->height;
	int bpp = GetChannelCount(resource->format);

	width >>= level;
	height >>= level;
* /


	tga.image = new char[width * height * bpp];
	memset(tga.image, 0, sizeof(char) * width * height * bpp);

	char pixel[4];
	int curPixel = 0;
	int curMipPixel = 0;
	int skipCount = 1 << level;

	for (int y = 0, y0 = 0; y < height; ++y, y0 += skipCount)
	{
		for (int x = 0, x0 = 0; x < width; ++x, x0 += skipCount)
		{
			unsigned char pixel[4];
			GetBilinearPixel(x0, y0, pixel);
			memcpy(&tga.image[((y * width) + x) * bpp], pixel, bpp * sizeof(unsigned char));
		}
	}*/
}

void TextureResource::FlipVertical(TextureSubresource* resource)
{/*
		unsigned int bytesPerPixel = header.bpp / 8;
		int lineLength = header.width * bytesPerPixel;
		char* oldImage = image;
		image = new char[header.width * header.height * bytesPerPixel];

		int hNew = header.height - 1;
		for (int hOld = 0; hOld < header.height; ++hOld, --hNew)
			memcpy(&image[hNew * lineLength], &oldImage[hOld * lineLength], lineLength);

		delete[] oldImage;*/
}

void TextureResource::GenerateCubemap()
{
	TextureSubresource* resource = mResource[0];
	mResource.clear();
	int width = resource->width;

	unsigned char* cPixel = (unsigned char*)resource->pixel;

	int size = GetChannelSize(resource->format);
	int bpp = GetChannelCount(resource->format);
	int faceWidth = resource->width / 6;
	for (int i = 0; i < 6; ++i)
	{
		TextureSubresource* face = CreateResourceInfo(resource->format, faceWidth, resource->height);
		unsigned char* cFacePixel = (unsigned char*)face->pixel;

		unsigned int offset = faceWidth * bpp * i;
		for (int y = 0; y < resource->height; ++y)
		{
			memcpy(&cFacePixel[y * faceWidth * bpp * size], &cPixel[offset], faceWidth * bpp * size);
			offset += width * bpp * size;
		}
	}

	ReleaseResourceInfo(&resource);
}
