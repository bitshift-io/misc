#include "Mesh.h"
#include "Device.h"
#include "File/File.h"
#include "Geometry.h"
#include "Material.h"
#include "RenderFactory.h"

Mesh::Mesh() :
	mName(""),
	mFlags(0)
{
}

void Mesh::Destroy(RenderFactory* factory)
{
	//MeshMatIterator it;
	//for (it = Begin(); it != End(); ++it)
	//{
	//	factory->Release((SceneObject**)&(it->material));
	//	factory->Release((SceneObject**)&(it->geometry));
	//}
}

void Mesh::AddReference()
{
	MeshMatIterator it;
	for (it = Begin(); it != End(); ++it)
	{
		//it->material = (Material*)mFactory->Load(Material::Type, &ObjectLoadParams(mFactory, mDevice, it->material->GetName()));
		it->material->AddReference();
		it->geometry->AddReference();
	}

	SceneObject::AddReference();
}

bool Mesh::ReleaseReference()
{
	MeshMatIterator it;
	for (it = Begin(); it != End(); ++it)
	{
		//it->material->ReleaseReference(factory);
		//it->geometry->ReleaseReference(factory);
		mFactory->Release((SceneObject**)&it->material); 
		mFactory->Release((SceneObject**)&it->geometry);
	}

	return SceneObject::ReleaseReference();
}


bool Mesh::Save(const File& out, SceneNode* node)
{
	// write flags
	out.Write(&mFlags, sizeof(unsigned int));

	// write bounds
	out.Write(&mBoundBox, sizeof(Box));

	// write number of mesh mats
	unsigned int size = GetNumMeshMat();
	out.Write(&size, sizeof(unsigned int));

	// write out the mesh mats
	MeshMatIterator it;
	for (it = Begin(); it != End(); ++it)
	{
		// write geometry
		it->geometry->Save(out, node);

		// write the material
		it->material->Save(out, node);
	}

	return true;
}

bool Mesh::Load(const ObjectLoadParams* params, SceneNode* node)
{
	mDevice = params->mDevice;
	mFactory = params->mFactory;

	File* in = params->mFile;

	// read flags
	in->Read(&mFlags, sizeof(unsigned int));

	// read bounds
	in->Read(&mBoundBox, sizeof(Box));

	// read the size of mesh mats
	unsigned int size = 0;
	in->Read(&size, sizeof(unsigned int));
	mMeshMat.resize(size);

	// read in the mesh mats
	for (unsigned int i = 0; i < size; ++i)
	{
		Geometry* geometry = (Geometry*)params->mFactory->Create(Geometry::Type);
		geometry->Load(params, node);

		Material* material = (Material*)params->mFactory->Load(Material::Type, params);
			//(Material*)params->mFactory->Create(Material::Type);
		//material->Load(params);

		mMeshMat[i] = MeshMat(material, geometry);
	}

	return true;
}

SceneObject* Mesh::Clone(SceneNode* owner)
{
	// duplicate this in to the clone
	Mesh* clone = new Mesh();
	*clone = *this;
	clone->mReferenceCount = 1;

	MeshMatIterator it;
	for (it = Begin(); it != End(); ++it)
	{
		it->material = mFactory->Clone<Material>(it->material);
		//it->material->AddReference(); // = (Material*)mFactory->Load(Material::Type, &ObjectLoadParams(mFactory, mDevice, it->material->GetName()));
		it->geometry->AddReference();
	}

	return clone;
}

bool Mesh::Render(Device* device, unsigned int meshMatIdx, int vertexFormat)
{
	//device->Draw(1, mMeshMat[meshMatIdx].material, mMeshMat[meshMatIdx].geometry, vertexFormat, 0, -1);
	mMeshMat[meshMatIdx].geometry->Draw();
	return true;
}

void Mesh::Debug(Device* device, const Matrix4& world)
{/*
	Matrix4 worldIT = world;
	worldIT.Inverse();
	worldIT.Transpose();


	//normal = worldIT * normal;
	//gGame.mDevice->DrawLine(Vector4(0,0,0), normal);


	MeshMatIterator it;
	for (it = Begin(); it != End(); ++it)
	{
		VertexFrame& frame = it->geometry->GetVertexFrame();
		RenderBuffer* pos = frame.mPosition;
		RenderBuffer* norm = frame.mNormal;

		if (!pos || !norm)
			continue;

		for (unsigned int i = 0; i < pos->GetSize(); ++i)
		{
			Vector4 n = norm->GetVector4()[i] * 0.1;
			Vector4 p = pos->GetVector4()[i];

			n = worldIT * n;
			p = world * p;
			device->DrawLine(p, p + n);
		}
	}*/
}

bool Mesh::GetLocalBoundBox(Box& box)
{
	box = mBoundBox;
	return true;
}

void Mesh::SetLocalBoundBox(Box& box)
{
	mBoundBox = box;
}

