#include "DX10Effect.h"
#include "../Device.h"
#include "../Effect.h"
#include "../RenderFactory.h"
#include "../Material.h"
#include "../Texture.h"
#include "../RenderBuffer.h"
#include "DX10Texture.h"
#include "File/Log.h"

map<string, ID3D10Effect*> DX10Effect::sEffect;

class IncludeFile : public ID3D10Include
{
public:

	virtual HRESULT STDMETHODCALLTYPE Close(LPCVOID pData)
	{
		char* buffer = (char*)pData;
		delete[] buffer;
		return S_OK;
	}

	virtual HRESULT STDMETHODCALLTYPE Open(D3D10_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes)
	{
		File file;
		if (!file.Open(pFileName, FAT_Read | FAT_Binary))
			return E_FAIL;

		unsigned int size = file.Size();
		char* buffer = new char[size];
		file.Read(buffer, size);		
		*ppData = buffer;
		*pBytes = size;

		return S_OK;
	}
};

bool DX10Effect::Load(const ObjectLoadParams* params, SceneNode* node)
{
	mDevice = params->mDevice;
	mFactory = params->mFactory;
	mName = params->mName;
	mEffect = sEffect[mName];
	mOrigonal = false;
	if (!mEffect)
	{
		HRESULT hr;
		mOrigonal = true;

		DWORD dwShaderFlags = D3D10_SHADER_ENABLE_STRICTNESS;
		
		if (params->mFactory->GetParameter().mDebugMode)
		{
			// Set the D3D10_SHADER_DEBUG flag to embed debug information in the shaders.
			// Setting this flag improves the shader debugging experience, but still allows 
			// the shaders to be optimized and to run exactly the way they will run in 
			// the release configuration of this program.
			dwShaderFlags |= D3D10_SHADER_DEBUG;
		}

		// check for a cached version of the file, if non exists, compile and add it to the cache
		bool bLoadedCache = false;

		string path;
		params->mFactory->GetParameter().GetExistingFile(mName, "fx", path);

		string cachePath;
		File cacheFile;
		/* FMNOTE: FIX ME!
		if (params->mFactory->GetParameter().mUseCache && params->mFactory->GetParameter().GetExistingFile(mName, "fx.cache", cachePath))
		{
			// see if this cached file is older than the non-cached version,
			// if so well dont use the cached version!
			std::time_t cacheFileTime = last_write_time(boost::filesystem::path(cachePath.c_str(), native));
			std::time_t fileTime = last_write_time(boost::filesystem::path(path.c_str(), native));

			if (fileTime < cacheFileTime)
			{
				bLoadedCache = true;

				cacheFile.Open(cachePath.c_str(), FAT_Read | FAT_Binary);
				unsigned int size = cacheFile.Size();
				char* blobBuffer = new char[size];
				memset(blobBuffer, 0, size);
				cacheFile.Read(blobBuffer, size);

				ID3D10Blob* pShader;
				hr = D3D10CreateBlob(size, &pShader);
				memcpy(pShader->GetBufferPointer(), blobBuffer, size);
				delete[] blobBuffer;

				if (FAILED(hr))
				{
					Log::Error("[DX10Effect::Load] D3D10CreateBlob failed with %X\n", hr);
					return false;
				}

				hr = D3D10CreateEffectFromMemory(pShader->GetBufferPointer(), pShader->GetBufferSize(), 0, 
						static_cast<const DX10Device*>(params->mDevice)->GetD3DDevice(), NULL, &mEffect);

				if (FAILED(hr))
				{
					Log::Error("[DX10Effect::Load] D3D10CreateEffectFromMemory failed with %X\n", hr);
					return false;
				}
			}
		}*/

		if (!bLoadedCache)
		{
			File file;
			if (!file.Open(path.c_str(), FAT_Read | FAT_Binary))
				return false;

			unsigned int size = file.Size();
			char* effectBuffer = new char[size];
			memset(effectBuffer, 0, size);
			file.Read(effectBuffer, size);

			ID3D10Blob* pErrors;
			ID3D10Blob* pShader;
			IncludeFile include;

			hr = D3DX10CompileFromMemory(effectBuffer, size, path.c_str(), NULL, &include, NULL, "fx_4_0", dwShaderFlags, 0, NULL, &pShader, &pErrors, NULL);

			delete[] effectBuffer;

			if (FAILED(hr))
			{
				if (pErrors)
				{
					char* error = (char*)pErrors->GetBufferPointer();
					Log::Error("%s\n", error);
				}
				return false;
			}

			// create a cached version so we can load faster next load :D
			string cacheFilename = params->mFactory->GetParameter().mCacheDir + "/" + mName + ".fx.cache";
			if (cacheFile.Open(cacheFilename.c_str(), FAT_Write | FAT_Binary))
			{
				cacheFile.Write(pShader->GetBufferPointer(), pShader->GetBufferSize());
			}

			hr = D3D10CreateEffectFromMemory(pShader->GetBufferPointer(), pShader->GetBufferSize(), 0, 
					static_cast<const DX10Device*>(params->mDevice)->GetD3DDevice(), NULL, &mEffect);

			if (FAILED(hr))
			{
				Log::Error("[DX10Effect::Load] D3D10CreateEffectFromMemory failed with %X\n", hr);
				return false;
			}
		}

		sEffect[mName] = mEffect;
	}

	ProcessTechniques(params->mDevice);
    mCurrentTechnique = *BeginTechnique(); //GetTechniqueByName("Render");
	ProcessParameters();
	return true;
}

bool DX10Effect::ProcessTechniques(Device* device)
{
	D3D10_EFFECT_DESC effectDesc;
	mEffect->GetDesc(&effectDesc);
	for (unsigned int i = 0; i < effectDesc.Techniques; ++i)
	{
		ID3D10EffectTechnique* pTechnique = mEffect->GetTechniqueByIndex(i);

		D3D10_TECHNIQUE_DESC techniqueDesc;
		pTechnique->GetDesc(&techniqueDesc);

		DX10EffectTechnique* technique = new DX10EffectTechnique();
		technique->mVertexFormat = 0;
		technique->mHandle = pTechnique;
		technique->mName = techniqueDesc.Name;
		technique->mVertexLayout = CreateInputLayout(device, pTechnique, technique->mVertexFormat);	
		mTechnique.push_back(technique);
	}

	return true;
}

ID3D10InputLayout* DX10Effect::CreateInputLayout(Device* device, ID3D10EffectTechnique* technique, int& vertexFormat)
{
	ID3D10InputLayout* pVertexLayout = 0;
	ID3D10EffectPass* pPass = technique->GetPassByIndex(0);

	//
	// What we do here is to iterate over each vertex input to generate D3D10_INPUT_ELEMENT_DESC structure
	//
	D3D10_PASS_SHADER_DESC vertexShaderDesc;
	HRESULT hr = pPass->GetVertexShaderDesc(&vertexShaderDesc);
	if (FAILED(hr))
        return false;

	D3D10_EFFECT_SHADER_DESC effectShaderDesc;
	hr = vertexShaderDesc.pShaderVariable->GetShaderDesc(0, &effectShaderDesc);

	// create shader reflection object
	ID3D10ShaderReflection* pIShaderReflection = 0;
	D3D10_SHADER_DESC shaderDesc;
	hr = D3D10ReflectShader(effectShaderDesc.pBytecode, effectShaderDesc.BytecodeLength, &pIShaderReflection);
    if (FAILED(hr))
        return 0;

	pIShaderReflection->GetDesc(&shaderDesc);
/*
	int matrixCount = 0;
	for (int i = 0; i < shaderDesc.InputParameters; ++i)
	{
		D3D10_SIGNATURE_PARAMETER_DESC signatureParameterDesc;
		hr = pIShaderReflection->GetInputParameterDesc(i, &signatureParameterDesc);

		if (strcmpi("WORLDVIEWPROJECTION", signatureParameterDesc.SemanticName) == 0)
			++matrixCount;
	}*/


	int uvIdx = 0;
	int inputSlot = 0;
	int reservedCount = 0;
	D3D10_INPUT_ELEMENT_DESC* pLayout = new D3D10_INPUT_ELEMENT_DESC[shaderDesc.InputParameters /*+ (matrixCount * 3)*/];
	for (unsigned int i = 0; i < shaderDesc.InputParameters; ++i)
	{
		D3D10_SIGNATURE_PARAMETER_DESC signatureParameterDesc;
		hr = pIShaderReflection->GetInputParameterDesc(i, &signatureParameterDesc);

		memset(&pLayout[i], 0, sizeof(D3D10_INPUT_ELEMENT_DESC));

		pLayout[i].SemanticName = signatureParameterDesc.SemanticName;
		pLayout[i].SemanticIndex = signatureParameterDesc.SemanticIndex;
		pLayout[i].InputSlot = inputSlot; //signatureParameterDesc.Register;
		pLayout[i].InstanceDataStepRate = D3D10_INPUT_PER_VERTEX_DATA;

		//pLayout[i].InputSlotClass = D3D10_INPUT_PER_INSTANCE_DATA;
		//pLayout[i].InstanceDataStepRate = 1;

		if (strcmpi("POSITION", pLayout[i].SemanticName) == 0
			|| strcmpi("POS", pLayout[i].SemanticName) == 0)
		{
			pLayout[i].Format = DXGI_FORMAT_R32G32B32_FLOAT;
			vertexFormat |= VF_Position;
			++inputSlot;
		}
		else if (strcmpi("COLOR", pLayout[i].SemanticName) == 0)
		{
			pLayout[i].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
			vertexFormat |= VF_Colour;
			++inputSlot;
		}
		else if (strcmpi("TEXCOORD", pLayout[i].SemanticName) == 0)
		{
			pLayout[i].Format = DXGI_FORMAT_R32G32_FLOAT;
			vertexFormat |= (VF_TexCoord0 << uvIdx);
			++uvIdx;
			++inputSlot;
		}
		else if (strcmpi("NORMAL", pLayout[i].SemanticName) == 0)
		{
			pLayout[i].Format = DXGI_FORMAT_R32G32B32_FLOAT;
			vertexFormat |= VF_Normal; 
			++inputSlot;
		}
		else if (strcmpi("TANGENT", pLayout[i].SemanticName) == 0)
		{
			pLayout[i].Format = DXGI_FORMAT_R32G32B32_FLOAT;
			vertexFormat |= VF_Tangent;
			++inputSlot;
		}
		else if (strcmpi("BINORMAL", pLayout[i].SemanticName) == 0)
		{
			pLayout[i].Format = DXGI_FORMAT_R32G32B32_FLOAT;
			vertexFormat |= VF_Binormal;
			++inputSlot;
		}
		else if (strcmpi("InstanceID", pLayout[i].SemanticName) == 0)
		{
			vertexFormat |= VF_InstanceId;

			pLayout[i].Format = DXGI_FORMAT_R32_UINT;
			pLayout[i].InputSlotClass = D3D10_INPUT_PER_INSTANCE_DATA;
			pLayout[i].InstanceDataStepRate = 1;
			++inputSlot;
		}
		else if (strstr(pLayout[i].SemanticName, "SV_") != 0)
		{
			// added in by hardware :D
			++reservedCount;
		}
		else
		{
			Log::Print("[DX10Effect::CreateInputLayout] Invalid vertex parameter");
			delete[] pLayout;
			return 0;
		}
	}

	UINT numElements = shaderDesc.InputParameters - reservedCount;

    D3D10_PASS_DESC passDesc;
    hr = pPass->GetDesc(&passDesc);

	hr = static_cast<const DX10Device*>(device)->GetD3DDevice()->CreateInputLayout(pLayout, numElements, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &pVertexLayout);

	// release layout
	delete[] pLayout;

    if (FAILED(hr) || !pVertexLayout)
	{
		Log::Print("[DX10Effect::CreateInputLayout] CreateInputLayout failed");
        return 0;
	}

	return pVertexLayout;
}

void DX10Effect::ProcessParameters(EffectParameter* parent)
{
	D3D10_EFFECT_DESC desc;
	HRESULT hr = GetEffect()->GetDesc(&desc);

	for (unsigned int i = 0; i < desc.GlobalVariables; ++i)
	{
		ID3D10EffectVariable* d3dParam = GetEffect()->GetVariableByIndex(i);
		ID3D10EffectType* d3dType = d3dParam->GetType();

		mParameter.push_back(ProcessParameter(d3dParam, d3dType));
	}
}

void DX10Effect::ProcessStructureParameters(EffectParameter* parent)
{
	ID3D10EffectVariable* d3dParam = (ID3D10EffectVariable*)parent->mHandle;
	ID3D10EffectType* d3dType = d3dParam->GetType();

	D3D10_EFFECT_TYPE_DESC typeDesc;
	HRESULT hr = d3dType->GetDesc(&typeDesc);
	if (FAILED(hr))
	{
		Log::Error("[DX10Effect::ProcessParameters] Failed to get effect type descriptor for '%s'", parent->mName.c_str());
		return;
	}

	for (unsigned int i = 0; i < typeDesc.Members; ++i)
	{
		ID3D10EffectVariable* d3dMemberParam = d3dParam->GetMemberByIndex(i);
		ID3D10EffectType* d3dMemberType = d3dType->GetMemberTypeByIndex(i);

		EffectParameter* param = ProcessParameter(d3dMemberParam, d3dMemberType);
		parent->mChild.push_back(param);

		param->mStructOffset = parent->mSize;
		parent->mSize += param->mSize;
	}

	// for an array
	parent->mSize *= typeDesc.Elements == 0 ? 1 : typeDesc.Elements;
	parent->mArraySize = typeDesc.Elements;
}

EffectParameter* DX10Effect::ProcessParameter(ID3D10EffectVariable* d3dParam, ID3D10EffectType* d3dType)
{
	D3D10_EFFECT_VARIABLE_DESC paramDesc;
	HRESULT hr = d3dParam->GetDesc(&paramDesc);

	const char* semantic = paramDesc.Semantic;

	int parameterID = GetParameterIDByName(semantic);
	EffectParameter* effectParam = new EffectParameter();
	
	effectParam->mName = paramDesc.Name;
	effectParam->mEffect = this;
	effectParam->mHandle = (EffectParameterHandle)d3dParam;
	effectParam->mID = (EffectParameterID)parameterID;
	effectParam->mType = EPT_Unknown;
	effectParam->mBufferOffset = paramDesc.BufferOffset;
	effectParam->mSemantic = semantic ? semantic : "";
	effectParam->mArraySize = 0;
	effectParam->mSize = 0;
	effectParam->mStructOffset = 0;

	D3D10_EFFECT_TYPE_DESC typeDesc;
	hr = d3dType->GetDesc(&typeDesc);
	if (FAILED(hr))
	{
		Log::Error("[DX10Effect::ProcessParameters] Failed to get effect type descriptor for '%s'", effectParam->mName.c_str());
		return effectParam;
	}

	switch (typeDesc.Type)
	{
	case D3D10_SVT_VOID: // structure!
		{
			if (typeDesc.Class == D3D10_SVC_STRUCT)
			{
				effectParam->mType = EPT_Struct;
				ProcessStructureParameters(effectParam);
			}
		}
		break;

	case D3D10_SVT_TEXTURE:
		effectParam->mType = EPT_Texture;
		break;

	case D3D10_SVT_TEXTURE2D:
		effectParam->mType = EPT_Texture2D;
		break;

	case D3D10_SVT_TEXTURE3D:
		effectParam->mType = EPT_Texture3D;
		break;

	case D3D10_SVT_TEXTURECUBE:
		effectParam->mType = EPT_TextureCube;
		break;

	case D3D10_SVT_UINT:
	case D3D10_SVT_INT:
		effectParam->mType = EPT_Int;
		break;

	case D3D10_SVT_FLOAT:
		{
			if (typeDesc.Class == D3D10_SVC_MATRIX_ROWS || typeDesc.Class == D3D10_SVC_MATRIX_COLUMNS)
			{
				effectParam->mType = EPT_Matrix4;
			}
			else
			{
				switch (typeDesc.Columns)
				{
				case 0:
				case 1:
					effectParam->mType = EPT_Float;
					break;

				case 2:
					effectParam->mType = EPT_Vector4;
					break;

				case 3:
					effectParam->mType = EPT_Vector4;
					break;

				case 4:
					effectParam->mType = EPT_Vector4;
					break;
				}
			}
		}
		break;

	default:
		effectParam->mType = EPT_Unknown;
		break;
	}

	// alignemnt should be 16 byte size, so should that go here? or just at the struct level?
	switch (effectParam->mType)
	{
	case EPT_Float:
		effectParam->mSize = 4;
		break;

	case EPT_Vector4:
		effectParam->mSize = sizeof(float) * 4;
		break;

	case EPT_Int:
		effectParam->mSize = sizeof(int);
		break;

	case EPT_Matrix4:
		effectParam->mSize = sizeof(float) * 16;
		break;
	}

	return effectParam;
}


void DX10Effect::SetValue(EffectParameter* param, const Matrix4* value, unsigned int count)
{
	ID3D10EffectMatrixVariable* matrixVar = ((ID3D10EffectVariable*)param->mHandle)->AsMatrix();
	HRESULT hr = S_OK;
	if (count <= 1)
		hr = matrixVar->SetMatrix((float*)&(value[0].m[0]));
	else
		hr = matrixVar->SetMatrixArray((float*)value, 0, count);

	Assert(hr == S_OK);
}

void DX10Effect::SetValue(EffectParameter* param, const float* value, unsigned int count)
{
	ID3D10EffectScalarVariable* vectorVar = ((ID3D10EffectVariable*)param->mHandle)->AsScalar();
	HRESULT hr = S_OK;
	if (count <= 1)
		hr = vectorVar->SetFloat(value[0]);
	else
		hr = vectorVar->SetFloatArray((float*)value, 0, count);

	Assert(hr == S_OK);
}

void DX10Effect::SetValue(EffectParameter* param, Texture** value, unsigned int count)
{
	ID3D10EffectShaderResourceVariable* resourceVar = ((ID3D10EffectVariable*)param->mHandle)->AsShaderResource();

	HRESULT hr = S_OK;
	if (count <= 1)
	{
		if (value && value[0])
			hr = resourceVar->SetResource(static_cast<DX10Texture*>(value[0])->GetTextureRV());
		else
			hr = resourceVar->SetResource(0);

		if (mFactory->GetParameter().mDebugMode && value)
		{
			ID3D10ShaderResourceView* pRes = 0;
			resourceVar->GetResource(&pRes);
			if (!pRes)
			{
				Log::Error("[DX10Effect::SetPassParameter] Resource not set in effect '%s'\n", 
					mName.c_str());
			}
		}
	}
	else
	{
		Assert(false);
		//hr = resourceVar->SetResourceArray(
	}

	Assert(hr == S_OK);
}

void DX10Effect::SetValue(EffectParameter* param, const Vector4* value, unsigned int count)
{
	ID3D10EffectVectorVariable* vectorVar = ((ID3D10EffectVariable*)param->mHandle)->AsVector();
	HRESULT hr = S_OK;
	if (count <= 1)
		hr = vectorVar->SetFloatVector((float*)&(value[0].vec[0]));
	else
		hr = vectorVar->SetFloatVectorArray((float*)value, 0, count);

	Assert(hr == S_OK);
}

void DX10Effect::SetValue(EffectParameter* param, const int* value, unsigned int count)
{
	ID3D10EffectScalarVariable* vectorVar = ((ID3D10EffectVariable*)param->mHandle)->AsScalar();
	HRESULT hr = S_OK;
	if (count <= 1)
		hr = vectorVar->SetInt(value[0]);
	else
		hr = vectorVar->SetIntArray((int*)value, 0, count);

	Assert(hr == S_OK);
}

void DX10Effect::SetValue(EffectParameter* param, void* buffer, unsigned int size)
{
	HRESULT hr = ((ID3D10EffectVariable*)param->mHandle)->SetRawValue(const_cast<void*>(buffer), 0, size);
	Assert(hr == S_OK);
}

	/*

void DX10Effect::SetInt(EffectParameterHandle handle, const int& value) 
{
	ID3D10EffectScalarVariable* vectorVar = ((ID3D10EffectVariable*)handle)->AsScalar();
	HRESULT hr = vectorVar->SetInt(value);
	int nothing = 0;
}

void DX10Effect::SetFloat(EffectParameterHandle handle, const float& value) 
{
	ID3D10EffectScalarVariable* vectorVar = ((ID3D10EffectVariable*)handle)->AsScalar();
	HRESULT hr = vectorVar->SetFloat(value);
	int nothing = 0;
}

void DX10Effect::SetVector4(EffectParameterHandle handle, const Vector4& value)
{
	ID3D10EffectVectorVariable* vectorVar = ((ID3D10EffectVariable*)handle)->AsVector();
	HRESULT hr = vectorVar->SetFloatVector((float*)&(value.vec[0]));
	int nothing = 0;
}

void DX10Effect::SetVector4Array(EffectParameterHandle handle, const Vector4* value, int count)
{
	ID3D10EffectVectorVariable* vectorVar = ((ID3D10EffectVariable*)handle)->AsVector();
	HRESULT hr = vectorVar->SetRawValue((void*)value, 0, sizeof(Vector4) * count);
	//HRESULT hr = vectorVar->SetFloatVectorArray(
	int nothing = 0;
}

void DX10Effect::SetTexture(EffectParameterHandle handle, Texture* texture)
{
	ID3D10EffectShaderResourceVariable* resourceVar = ((ID3D10EffectVariable*)handle)->AsShaderResource();

	HRESULT hr;
	if (texture)
		hr = resourceVar->SetResource(static_cast<DX10Texture*>(texture)->GetTextureRV());
	else
		hr = resourceVar->SetResource(0);

	if (mFactory->GetParameter().mDebugMode && texture)
	{
		ID3D10ShaderResourceView* pRes = 0;
		resourceVar->GetResource(&pRes);
		if (!pRes)
		{
			Log::Error("[DX10Effect::SetPassParameter] Resource not set in effect '%s'\n", 
				mName.c_str());
		}
	}
}

void DX10Effect::SetMatrix4(EffectParameterHandle handle, const Matrix4& matrix)
{
	ID3D10EffectMatrixVariable* matrixVar = ((ID3D10EffectVariable*)handle)->AsMatrix();
	HRESULT hr = matrixVar->SetMatrix((float*)&(matrix.m[0]));
	int nothing = 0;
}

void DX10Effect::SetMatrix4Array(EffectParameterHandle handle, const Matrix4* matrix, int count) 
{
	ID3D10EffectMatrixVariable* matrixVar = ((ID3D10EffectVariable*)handle)->AsMatrix();

	HRESULT hr = matrixVar->SetMatrixArray((float*)matrix, 0, count);
	//HRESULT hr = matrixVar->SetRawValue((float*)matrix, 0, sizeof(Matrix4) * count);
	int nothing = 0;
}

void DX10Effect::SetRaw(EffectParameterHandle handle, const void* value, int count)
{
	HRESULT hr = ((ID3D10EffectVariable*)handle)->SetRawValue(const_cast<void*>(value), 0, count);
	int nothing = 0;
}*/

int	DX10Effect::Begin() 
{ 
	D3D10_TECHNIQUE_DESC techDesc;
    ((ID3D10EffectTechnique*)GetCurrentTechnique()->mHandle)->GetDesc(&techDesc);
	return techDesc.Passes; 
}

void DX10Effect::BeginPass(int idx) 
{
	mCurPass = idx;
}

void DX10Effect::SetPassParameter()
{
	Effect::SetPassParameter();

#ifdef DEBUG
	for (unsigned int i = 0; i < mParameter.size(); ++i)
	{
		EffectParameter* effectParam = mParameter[i];
		if (effectParam->mType == EPT_Texture
			|| effectParam->mType == EPT_Texture2D
			|| effectParam->mType == EPT_TextureCube
			|| effectParam->mType == EPT_Texture3D)
		{
			ID3D10EffectShaderResourceVariable* resourceVar = ((ID3D10EffectVariable*)effectParam->mHandle)->AsShaderResource();

			ID3D10ShaderResourceView* pRes = 0;
			resourceVar->GetResource(&pRes);
			if (!pRes)
			{
				Log::Error("[DX10Effect::SetPassParameter] Resource not set in effect '%s' parameter '%s'\n", 
					mName.c_str(), effectParam->mName.c_str());
			}
		}
	}
#endif

	DeviceState state;
	mDevice->GetDeviceState(state);
	state.vertexFormat = GetCurrentVertexFormat();
	mDevice->SetDeviceState(state);
	static_cast<DX10Device*>(mDevice)->SetInputLayout(GetCurrentVertexLayout());

	((ID3D10EffectTechnique*)GetCurrentTechnique()->mHandle)->GetPassByIndex(mCurPass)->Apply(0);
}

void DX10Effect::EndPass()
{
	((ID3D10EffectTechnique*)GetCurrentTechnique()->mHandle)->GetPassByIndex(mCurPass)->Apply(0);
}

void DX10Effect::End()
{

}