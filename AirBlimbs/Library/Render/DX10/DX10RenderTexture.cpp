#include "DX10RenderTexture.h"
#include "DX10Texture.h"
#include "../RenderFactory.h"
#include "File/Log.h"

bool DX10RenderTexture::Init(Device* device, RenderFactory* factory, int width, int height, unsigned int flags, int targetCount)
{
	mWidth = width;
	mHeight = height;

	mClearColour = Vector4(0.f, 0.f, 0.f, 0.f);

	mClearDepth = 1.f;
	mClearStencil = 0;

	mOrigRTV = 0;
	mOrigDSV = 0;

	mRender = 0;
	mDepthStencil = 0;
	mDepthStencilView = 0;
	mRenderTargetView = 0;

	mRenderStaging = 0;
	mDepthStencilStaging = 0;

	mDevice = static_cast<DX10Device*>(device);
	ID3D10Device* d3dDevice = mDevice->GetD3DDevice();

	HRESULT hr = S_OK;

	if (flags & RTF_Colour)
	{
		mRender = (DX10Texture*)factory->Create(Texture::Type);

		//
		// Create render texture
		//
		D3D10_TEXTURE2D_DESC descTex;
		ZeroMemory(&descTex, sizeof(D3D10_TEXTURE2D_DESC));
		descTex.Width = mWidth;
		descTex.Height = mHeight;
		descTex.MipLevels = 1;
		descTex.Format = (flags & RTF_LowDynamicRange) ? DXGI_FORMAT_R8G8B8A8_UNORM : DXGI_FORMAT_R16G16B16A16_FLOAT;
		descTex.SampleDesc.Count = (flags & RTF_Multisample) ? 8 : 1;
		descTex.SampleDesc.Quality = 0;
		descTex.Usage = D3D10_USAGE_DEFAULT;
		descTex.BindFlags = D3D10_BIND_RENDER_TARGET | D3D10_BIND_SHADER_RESOURCE;
		descTex.CPUAccessFlags = 0;
		descTex.MiscFlags = 0;
		descTex.ArraySize = targetCount;

		if (flags & RTF_Write)
		{
			descTex.CPUAccessFlags |= D3D10_CPU_ACCESS_WRITE;
			descTex.Usage = D3D10_USAGE_DYNAMIC;
		}

		if (flags & RTF_Read)
		{
			//
			// Create staging resource
			//
			mRenderStaging = (DX10Texture*)factory->Create(Texture::Type);
			D3D10_TEXTURE2D_DESC stagingDescTex = descTex;
			stagingDescTex.CPUAccessFlags = D3D10_CPU_ACCESS_READ;
			stagingDescTex.Usage = D3D10_USAGE_STAGING;
			stagingDescTex.BindFlags = 0;
			hr = d3dDevice->CreateTexture2D(&stagingDescTex, NULL, &mRenderStaging->mTexture);
			if (FAILED(hr))
				return false;
		}

		hr = d3dDevice->CreateTexture2D(&descTex, NULL, &mRender->mTexture);
		if (FAILED(hr))
			return false;

		//
		// Create the render target view
		//
		D3D10_RENDER_TARGET_VIEW_DESC rtDesc;
		ZeroMemory(&rtDesc, sizeof(D3D10_RENDER_TARGET_VIEW_DESC));
		rtDesc.Format = descTex.Format;

		D3D10_RTV_DIMENSION viewDimension = (flags & RTF_Multisample) ? D3D10_RTV_DIMENSION_TEXTURE2DMS : D3D10_RTV_DIMENSION_TEXTURE2D;
		if (targetCount > 1)
			viewDimension = (flags & RTF_Multisample) ? D3D10_RTV_DIMENSION_TEXTURE2DMSARRAY : D3D10_RTV_DIMENSION_TEXTURE2DARRAY;

		rtDesc.ViewDimension = viewDimension;
		rtDesc.Texture2DArray.ArraySize = targetCount;
		rtDesc.Texture2DArray.FirstArraySlice = 0;
		rtDesc.Texture2D.MipSlice = 0;
		hr = d3dDevice->CreateRenderTargetView(mRender->mTexture, &rtDesc, &mRenderTargetView);
		if (FAILED(hr))
			return false;

		//
		// Create the shader resource view (color)
		//
		D3D10_SHADER_RESOURCE_VIEW_DESC descRV;
		ZeroMemory(&descRV, sizeof(D3D10_SHADER_RESOURCE_VIEW_DESC));
		descRV.Format = descTex.Format;
		descRV.ViewDimension = (flags & RTF_Multisample) ? D3D10_SRV_DIMENSION_TEXTURE2DMS : D3D10_SRV_DIMENSION_TEXTURE2D;
		descRV.Texture2D.MipLevels = 1;
		descRV.Texture2D.MostDetailedMip = 0;
		hr = d3dDevice->CreateShaderResourceView(mRender->mTexture, &descRV, &mRender->mTextureRV);
		if (FAILED(hr))
			return false;
	}

	if (flags & RTF_Depth || flags & RTF_Stencil)
	{
		mDepthStencil = (DX10Texture*)factory->Create(Texture::Type);

		//
		// Create depth-stencil texture
		//
		D3D10_TEXTURE2D_DESC descDepth;
		ZeroMemory(&descDepth, sizeof(D3D10_TEXTURE2D_DESC));
		descDepth.Width = mWidth;
		descDepth.Height = mHeight;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = targetCount;
		descDepth.Format = (flags & RTF_Stencil) ? DXGI_FORMAT_R24G8_TYPELESS : DXGI_FORMAT_R32_TYPELESS;
		descDepth.SampleDesc.Count = (flags & RTF_Multisample) ? 8 : 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D10_USAGE_DEFAULT;
		descDepth.BindFlags = D3D10_BIND_DEPTH_STENCIL | D3D10_BIND_SHADER_RESOURCE;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;

		if (flags & RTF_Write)
		{
			descDepth.CPUAccessFlags |= D3D10_CPU_ACCESS_WRITE;
			descDepth.Usage = D3D10_USAGE_DYNAMIC;
		}

		if (flags & RTF_Read)
		{
			//
			// Create staging resource
			//
			mDepthStencilStaging = (DX10Texture*)factory->Create(Texture::Type);
			D3D10_TEXTURE2D_DESC stagingDescDepth = descDepth;
			stagingDescDepth.CPUAccessFlags = D3D10_CPU_ACCESS_READ;
			stagingDescDepth.Usage = D3D10_USAGE_STAGING;
			stagingDescDepth.BindFlags = 0;
			hr = d3dDevice->CreateTexture2D(&stagingDescDepth, NULL, &mDepthStencilStaging->mTexture);
			if (FAILED(hr))
				return false;
		}

		hr = d3dDevice->CreateTexture2D(&descDepth, NULL, &mDepthStencil->mTexture);
		if (FAILED(hr))
			return false;


		//
		// Create the depth stencil view
		//
		D3D10_DEPTH_STENCIL_VIEW_DESC descDSV;
		ZeroMemory(&descDSV, sizeof(D3D10_DEPTH_STENCIL_VIEW_DESC));
		descDSV.Format = (flags & RTF_Stencil) ? DXGI_FORMAT_D24_UNORM_S8_UINT : DXGI_FORMAT_D32_FLOAT;

		D3D10_DSV_DIMENSION viewDimension = (flags & RTF_Multisample) ? D3D10_DSV_DIMENSION_TEXTURE2DMS : D3D10_DSV_DIMENSION_TEXTURE2D;
		if (targetCount > 1)
			viewDimension = (flags & RTF_Multisample) ? D3D10_DSV_DIMENSION_TEXTURE2DMSARRAY : D3D10_DSV_DIMENSION_TEXTURE2DARRAY;

		descDSV.Texture2DArray.FirstArraySlice = 0;
		descDSV.Texture2DArray.ArraySize = targetCount;
		descDSV.ViewDimension = viewDimension;
		descDSV.Texture2D.MipSlice = 0;
		hr = d3dDevice->CreateDepthStencilView(mDepthStencil->mTexture, &descDSV, &mDepthStencilView);
		if (FAILED(hr))
			return false;

		//
		// Create the shader resource view (depth)
		//
		D3D10_SHADER_RESOURCE_VIEW_DESC descRV;
		ZeroMemory(&descRV, sizeof(D3D10_SHADER_RESOURCE_VIEW_DESC));
		descRV.Format = (flags & RTF_Stencil) ? DXGI_FORMAT_X24_TYPELESS_G8_UINT : DXGI_FORMAT_R32_FLOAT;	// Make the shaders see this as R32_FLOAT instead of typeless
		descRV.ViewDimension = (flags & RTF_Multisample) ? D3D10_SRV_DIMENSION_TEXTURE2DMS : D3D10_SRV_DIMENSION_TEXTURE2D;
		descRV.Texture2D.MipLevels = 1;
		descRV.Texture2D.MostDetailedMip = 0;
		hr = d3dDevice->CreateShaderResourceView(mDepthStencil->mTexture, &descRV, &mDepthStencil->mTextureRV);
		if (FAILED(hr))
			return false;
	}

	return true;
}

void DX10RenderTexture::Deinit()
{

}

void DX10RenderTexture::SetViewport(const Vector4& topLeft, const Vector4& bottomRight, const Vector4& nearFarDepth)
{
	// SetYAxis the viewport
    D3D10_VIEWPORT vp;
    vp.Width = (bottomRight.x - topLeft.x) * mWidth;
    vp.Height = (bottomRight.y - topLeft.y) * mHeight;
    vp.MinDepth = nearFarDepth.x;
    vp.MaxDepth = nearFarDepth.y;
    vp.TopLeftX = topLeft.x * mWidth;
    vp.TopLeftY = topLeft.y * mHeight;
	mDevice->GetD3DDevice()->RSSetViewports(1, &vp);
}

int DX10RenderTexture::Begin()
{
	// back up old view port
	unsigned int count = 1;
	mDevice->GetD3DDevice()->RSGetViewports(&count, &mOrigVP);

	// SetYAxis the viewport
    D3D10_VIEWPORT vp;
    vp.Width = mWidth;
    vp.Height = mHeight;
    vp.MinDepth = 0;
    vp.MaxDepth = 1;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
	mDevice->GetD3DDevice()->RSSetViewports(1, &vp);

// Store off original render targets
    mDevice->GetD3DDevice()->OMGetRenderTargets( 1, &mOrigRTV, &mOrigDSV );

	return 1;
}

void DX10RenderTexture::End()
{
	// rebind origonal
    ID3D10RenderTargetView* aRTViews[1] = { mOrigRTV };
    mDevice->GetD3DDevice()->OMSetRenderTargets(1, aRTViews, mOrigDSV);

	// restore view port
	mDevice->GetD3DDevice()->RSSetViewports(1, &mOrigVP);
}

void DX10RenderTexture::BindTarget(int index, bool clear, int flags)
{
	ID3D10RenderTargetView* aRTViews[1] = { 0 };
	
	if (flags & RTF_Colour)
		aRTViews[0] = mRenderTargetView;

	mDevice->GetD3DDevice()->OMSetRenderTargets(1, aRTViews, (flags & RTF_Depth || flags & RTF_Stencil) ? mDepthStencilView : 0);

	if (clear)
	{
		if (mRenderTargetView)
			mDevice->GetD3DDevice()->ClearRenderTargetView(mRenderTargetView, (float*)&mClearColour);

		unsigned int clearFlags = flags & RTF_Depth ? D3D10_CLEAR_STENCIL : 0;
		clearFlags |= flags & RTF_Stencil ? D3D10_CLEAR_DEPTH : 0;
		if (mDepthStencilView)
			mDevice->GetD3DDevice()->ClearDepthStencilView(mDepthStencilView, clearFlags, mClearDepth, mClearStencil);
	}
}

void DX10RenderTexture::SetClearColour(const Vector4& colour)
{
	mClearColour = colour;
}

void DX10RenderTexture::SetClearDepth(float depth)
{
	mClearDepth = depth;
}

void DX10RenderTexture::SetClearStencil(unsigned int stencil)
{
	mClearStencil = stencil;
}

Texture* DX10RenderTexture::GetTexture(RenderTextureFlags type, int lockFlags)
{
	// TODO: use dirty flags so we dont keep copying each time this is called!
	// or should staging copy be done after rendering?

	switch (type)
	{
	case RTF_Colour:
		{
			// copy to staging resource and return it
			if (lockFlags & RTF_Read)
			{
				mDevice->GetD3DDevice()->CopyResource(mRenderStaging->mTexture, mRender->mTexture);
				return mRenderStaging;
			}

			return mRender;
		}

	case RTF_Stencil:
	case RTF_Depth:
		{
			// copy to staging resource and return it
			if (lockFlags & RTF_Read)
			{
				mDevice->GetD3DDevice()->CopyResource(mDepthStencilStaging->mTexture, mDepthStencil->mTexture);
				return mDepthStencilStaging;
			}

			return mDepthStencil;
		}
	}

	return 0;
}

int DX10RenderTexture::GetWidth()
{
	return mWidth;
}

int DX10RenderTexture::GetHeight()
{
	return mHeight;
}
