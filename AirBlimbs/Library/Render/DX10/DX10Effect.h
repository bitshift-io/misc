#ifndef _DX10EFFECT_H_
#define _DX10EFFECT_H_

#include "../Effect.h"
#include "DX10Device.h"
#include <map>

class DX10EffectTechnique : public EffectTechnique
{
public:

	ID3D10InputLayout*			mVertexLayout;
};

class DX10Effect : public Effect
{
public:

	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node = 0);

	virtual int		Begin();
	virtual void	BeginPass(int idx);
	virtual void	SetPassParameter();
	virtual void	EndPass();
	virtual void	End();

/*
	virtual void	SetProjectorParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world);
	virtual void	SetMatrixParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world);
	virtual void	SetLightParameters(Light* lights[], unsigned int numLights) {}
	virtual void	SetTimeParameters(float time);

	virtual EffectParamHandle GetParameterByName(const char* name);


/*
	void			SetMatrixParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& model, const Vector4& eyePosition);
	void			SetLightParameters(Light* lights[], unsigned int numLights);
	void			SetProjectorParameters(const Matrix4& model, Projector* projectors[], unsigned int numProjectors);
	void			SetSkinParameters(const Matrix4 bones[], unsigned int numBones);
	void			SetTimeParameter(float time);* /
*/
	ID3D10Effect*			GetEffect()						{ return mEffect; }
	ID3D10InputLayout*		GetCurrentVertexLayout()		{ return ((DX10EffectTechnique*)mCurrentTechnique)->mVertexLayout; }
	int						GetCurrentVertexFormat()		{ return mCurrentTechnique->mVertexFormat; }
/*

	virtual void			SetTechnique(EffectTechnique technique);
	virtual int				GetTechniqueCount();

	virtual EffectTechnique	GetTechnique(int idx);
	virtual EffectTechnique	GetTechnique(const char* name);

	virtual int				GetVertexFormat(EffectTechnique technique);

	virtual EffectTechnique	GetCurrentTechnique();

protected:

	struct Technique
	{
		ID3D10InputLayout*									mVertexLayout;
		ID3D10EffectTechnique*								mTechnique;
		int													mVertexFormat;
	};

	const Technique* 		GetTechniqueInternal(EffectTechnique technique);
*/

private:

	virtual void			SetValue(EffectParameter* param, const Matrix4* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, const float* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, Texture** value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, const Vector4* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, const int* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, void* buffer, unsigned int size);

	bool					ProcessTechniques(Device* device);
	void					ProcessParameters(EffectParameter* parent = 0);
	void					ProcessStructureParameters(EffectParameter* parent);
	EffectParameter*		ProcessParameter(ID3D10EffectVariable* d3dParam, ID3D10EffectType* d3dType);

	ID3D10InputLayout*		CreateInputLayout(Device* device, ID3D10EffectTechnique* technique, int& vertexFormat);	
/*
	virtual EffectParam*	ReadParameter(Material* material, RenderFactory* factory, Device* device, const char* name, File* in);
	virtual void			DestroyParameter(RenderFactory* factory, EffectParam* param)		{}
*/
	ID3D10Effect*           mEffect;
	
	int						mCurPass;
/*
	vector<pair<EffectParamID, EffectParamHandle> > mParameter;

	vector<Technique>		mTechnique;
	int						mCurTechnique;

	bool					mProjector;
	bool					mSkin;
	bool					mLight;
	bool					mTime;
*/
	bool					mOrigonal;				// temporary fix to go with the map below
	RenderFactory*			mFactory;

	static map<string, ID3D10Effect*>	sEffect; // a temporary fix to speed up load times till we make this just a handle
	
};

#endif