#ifndef _DX10RENDERBUFFER_H_
#define _DX10RENDERBUFFER_H_

#include "../RenderBuffer.h"
#include "DX10Device.h"

class D3DDevice;

//
// Render buffer
//
class DX10RenderBuffer : public RenderBuffer
{
public:

	explicit DX10RenderBuffer(DX10Device* device);

	virtual void	Init(RenderBufferType type, unsigned int size, RenderBufferUseage useage);
	virtual void	Deinit();

	virtual bool	Save(const File& out);
	virtual bool	Load(Device* device, const File& in);

	virtual void	Bind(VertexFormat channel, int index = 0);
	virtual void	Unbind();

	virtual bool	Lock(RenderBufferLock& lock, int lockFlags = 0);
	virtual void	Unlock(RenderBufferLock& lock);

	virtual unsigned int	GetSize()				{ return mSize; }

	virtual RenderBufferUseage	GetUseage()								{ return mUseage; }
	virtual void				Draw(unsigned int startIndex = 0, unsigned int numIndices = -1);

	virtual void		ReloadResource(Device* device);

	ID3D10Buffer*			GetBuffer()									{ return mBuffer; }
	int						GetStride();

protected:

	void			GenerateBuffer(Device* device, ID3D10Buffer** buffer, void* vec, unsigned int size, bool indexBuffer = false);

	void			CopyBuffer(Device* device, ID3D10Buffer** buffer, void* vec, unsigned int size, unsigned int offset = 0, bool indexBuffer = false);

	RenderBufferUseage		mUseage;
	int						mSize;

	ID3D10Buffer*			mBuffer;
	DX10Device*				mDevice;

	void*					mData;

};

#endif