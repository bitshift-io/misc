#ifndef _DX10TEXTURE_H_
#define _DX10TEXTURE_H_

#include "../TGA.h"
#include "../HDR.h"
#include "../Texture.h"
#include "DX10Device.h"

class DX10Texture : public Texture
{
	friend class DX10RenderTexture;
	friend class DX10Device;

public:

	DX10Texture() :
		mTexture(0),
		mTextureRV(0),
		mTexture3D(0)
	{
	}

	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node = 0);

	virtual bool	Init(Device* device, unsigned char* image, int channels, int width, int height, int depth, int flags = TF_2D | TF_LowDynamicRange);
	//virtual bool	Update(unsigned char* image);

	virtual bool	Lock(TextureSubresource& resource, int lockFlags);
	virtual void	Unlock();

	virtual bool	GetSubresource(TextureSubresource& resource);


	ID3D10Texture2D*			GetTexture()				{ return mTexture; }
	ID3D10ShaderResourceView*	GetTextureRV()				{ return mTextureRV; }

	//virtual int		GetWidth();
	//virtual int		GetHeight();


	virtual bool	IsInstance(const ObjectLoadParams* params, SceneObject* loaded);

protected:

	bool	GenerateTexture(ID3D10Device* d3dDevice, TextureResource* texRes, int flags = 0);
	bool	GenerateCubeTexture(ID3D10Device* d3dDevice, TextureResource* texRes, int flags = 0);


	ID3D10Texture3D*			mTexture3D;
	ID3D10Texture2D*			mTexture;
	ID3D10ShaderResourceView*   mTextureRV;
	string						mName;
};

#endif