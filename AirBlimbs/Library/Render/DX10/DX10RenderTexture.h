#ifndef _DX10_RENDERTEXTURE_H_
#define _DX10_RENDERTEXTURE_H_

#include "../RenderTexture.h"
#include "DX10Device.h"

class DX10Texture;

class DX10RenderTexture : public RenderTexture
{
	friend class DX10Device;

public:

	virtual bool		Init(Device* device, RenderFactory* factory, int width, int height, unsigned int flags, int targetCount = 1);
	virtual void		Deinit();

	virtual int			Begin();
	virtual void		End();

	virtual void		SetViewport(const Vector4& topLeft, const Vector4& bottomRight, const Vector4& nearFarDepth = Vector4(0.f, 1.f));
	virtual void		BindTarget(int index, bool clear = true, int flags = RTF_Colour | RTF_Depth | RTF_Stencil);
	virtual void		SetClearColour(const Vector4& colour);
	virtual void		SetClearDepth(float depth);
	virtual void		SetClearStencil(unsigned int stencil);

	virtual Texture*	GetTexture(RenderTextureFlags type, int lockFlags = 0);

	virtual int			GetWidth();
	virtual int			GetHeight();

protected:

	DX10Texture*			mRender;
	DX10Texture*			mDepthStencil;

	ID3D10RenderTargetView*	mRenderTargetView;
	ID3D10DepthStencilView*	mDepthStencilView;

	ID3D10RenderTargetView* mOrigRTV;
    ID3D10DepthStencilView* mOrigDSV;
	D3D10_VIEWPORT			mOrigVP;

	DX10Texture*			mRenderStaging;
	DX10Texture*			mDepthStencilStaging;


	int			mWidth;
	int			mHeight;

	Vector4		mClearColour;
	float		mClearDepth;
	unsigned int mClearStencil;

	DX10Device* mDevice;
};

#endif