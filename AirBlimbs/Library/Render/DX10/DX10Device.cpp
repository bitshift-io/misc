#include "DX10Device.h"
#include "../Device.h"
#include "../Window.h"
#include "../Material.h"
#include "../Geometry.h"
#include "../RenderBuffer.h"
#include "../RenderFactory.h"
#include "../Win32/Win32Window.h"
#include "DX10RenderBuffer.h"
#include "DX10Texture.h"
#include "DX10RenderTexture.h"
#include "DX10Effect.h"
#include "File/Log.h"

#if defined(DEBUG)
	// for PIX
	#include <d3d9.h>
	#pragma comment(lib, "d3d9.lib")
#endif

//#pragma comment(lib, "delayimp.lib")

#pragma comment(lib, "dxerr.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "d3d10.lib")
//#pragma comment(linker, "/DELAYLOAD:d3d10.dll")

#if defined(DEBUG)
	#pragma comment(lib, "d3dx10d.lib")
	//#pragma comment(linker, "/DELAYLOAD:d3dx10d.dll")
#else
	#pragma comment(lib, "d3dx10.lib")
	//#pragma comment(linker, "/DELAYLOAD:d3dx10.dll")
#endif

#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "comctl32.lib")

bool DX10Device::Init(RenderFactory* factory, Window* window, int multisample)
{
	mMultisample = multisample;
	mWindow = window;

	// check for dx10 dll's
#ifdef DEBUG
	HINSTANCE dllInst = LoadLibrary(L"d3d10d.dll");
#else
	HINSTANCE dllInst = LoadLibrary(L"d3d10.dll");
#endif

	bool loadedDll = (dllInst != 0);
	FreeLibrary(dllInst);

	if (!loadedDll)
	{
		Log::Error("[DX10Device::Init] Failed to load DX10 dll's");
		return false;
	}

	HRESULT hr;

	WindowResolution res;
	window->GetWindowResolution(res);
    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory( &sd, sizeof(sd) );
    sd.BufferCount = 1;
	sd.BufferDesc.Width = res.width;
	sd.BufferDesc.Height = res.height;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; //DXGI_FORMAT_R16G16B16A16_FLOAT; //DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;// | D3D10_BIND_RENDER_TARGET | D3D10_BIND_SHADER_RESOURCE;
    sd.OutputWindow = static_cast<Win32Window*>(window)->GetHandle();
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;

	UINT flags = 0;
	if (factory->GetParameter().mDebugMode)
	{
		flags |= D3D10_CREATE_DEVICE_DEBUG;
	}

	mD3DDevice = 0;
	mSwapChain = 0;
    hr = D3D10CreateDeviceAndSwapChain(NULL, D3D10_DRIVER_TYPE_HARDWARE, 
                                       NULL, flags, D3D10_SDK_VERSION, &sd,
                                       &mSwapChain, &mD3DDevice);
    if (FAILED(hr))
	{
		if (flags & D3D10_CREATE_DEVICE_DEBUG)
			Log::Error("[DX10Device::Init] D3D10CreateDeviceAndSwapChain failed, Enable debugging for directx in the control panel.");
		else
			Log::Error("[DX10Device::Init] D3D10CreateDeviceAndSwapChain failed");

        return false;
	}

	// Assuming pDevice was created with D3D10_CREATE_DEVICE_DEBUG
	mD3DDevice->QueryInterface(__uuidof(ID3D10InfoQueue), (void**)&mInfoQueue);

    // Create a render target view
	hr = mSwapChain->GetBuffer(0, __uuidof( ID3D10Texture2D ), (LPVOID*)&mRender);
    if (FAILED(hr))
        return false;

	hr = mD3DDevice->CreateRenderTargetView(mRender, NULL, &mRenderTargetView);
    if (FAILED(hr))
        return false;

	//
	// Create depth-stencil texture
	//
	mDepthStencilView = 0;

    D3D10_TEXTURE2D_DESC descDepth;
	descDepth.Width = res.width;
	descDepth.Height = res.height;
    descDepth.MipLevels = 1;
    descDepth.ArraySize = 1;
    descDepth.Format = DXGI_FORMAT_D32_FLOAT;
    descDepth.SampleDesc.Count = 1;
    descDepth.SampleDesc.Quality = 0;
    descDepth.Usage = D3D10_USAGE_DEFAULT;
    descDepth.BindFlags = D3D10_BIND_DEPTH_STENCIL;
    descDepth.CPUAccessFlags = 0;
    descDepth.MiscFlags = 0;
    hr = mD3DDevice->CreateTexture2D(&descDepth, NULL, &mDepthStencil);
    if (FAILED(hr))
        return false;

	//
	// Create the depth-stencil state
	//
	mDepthStencilDesc.DepthEnable = true;
	mDepthStencilDesc.DepthWriteMask = D3D10_DEPTH_WRITE_MASK_ALL;
	mDepthStencilDesc.DepthFunc = D3D10_COMPARISON_LESS;

	mDepthStencilDesc.StencilEnable = true;
	mDepthStencilDesc.StencilReadMask = (UINT8)0xFFFFFFFF;
	mDepthStencilDesc.StencilWriteMask = (UINT8)0xFFFFFFFF;

	mDepthStencilDesc.FrontFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	mDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D10_STENCIL_OP_INCR;
	mDepthStencilDesc.FrontFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	mDepthStencilDesc.FrontFace.StencilFunc = D3D10_COMPARISON_ALWAYS;

	mDepthStencilDesc.BackFace.StencilFailOp = D3D10_STENCIL_OP_KEEP;
	mDepthStencilDesc.BackFace.StencilDepthFailOp = D3D10_STENCIL_OP_DECR;
	mDepthStencilDesc.BackFace.StencilPassOp = D3D10_STENCIL_OP_KEEP;
	mDepthStencilDesc.BackFace.StencilFunc = D3D10_COMPARISON_ALWAYS;

	//
    // Create the depth stencil view
	//
    D3D10_DEPTH_STENCIL_VIEW_DESC descDSV;
    descDSV.Format = descDepth.Format;
    descDSV.ViewDimension = D3D10_DSV_DIMENSION_TEXTURE2D;
    descDSV.Texture2D.MipSlice = 0;
    hr = mD3DDevice->CreateDepthStencilView(mDepthStencil, &descDSV, &mDepthStencilView);
    if (FAILED(hr))
        return false;

    //mD3DDevice->OMSetRenderTargets(1, &mRenderTexture->mRenderTargetView, mDepthStencilView);
	mD3DDevice->OMSetRenderTargets(1, &mRenderTargetView, mDepthStencilView);

    // SetYAxis the viewport
    D3D10_VIEWPORT vp;
	vp.Width = res.width;
	vp.Height = res.height;
    vp.MinDepth = 0;
    vp.MaxDepth = 1;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
	mD3DDevice->RSSetViewports(1, &vp);


	// set up culling and face orders
    mRasterizerState.FillMode = D3D10_FILL_SOLID;
    mRasterizerState.CullMode = D3D10_CULL_NONE;		// TODO
    mRasterizerState.FrontCounterClockwise = true;
    mRasterizerState.DepthBias = false;
    mRasterizerState.DepthBiasClamp = 0;
    mRasterizerState.SlopeScaledDepthBias = 0;
    mRasterizerState.DepthClipEnable = true;
    mRasterizerState.ScissorEnable = false;
    mRasterizerState.MultisampleEnable = false;
    mRasterizerState.AntialiasedLineEnable = false;

	//
	// Set up blend state
	//
	mBlendDesc.AlphaToCoverageEnable = false;
	for (int i = 0; i < 8; ++i)
	{
		mBlendDesc.BlendEnable[i] = false;
		mBlendDesc.RenderTargetWriteMask[i] = D3D10_COLOR_WRITE_ENABLE_ALL;
	}

	mBlendDesc.BlendOpAlpha = D3D10_BLEND_OP_ADD;
	mBlendDesc.BlendOp = D3D10_BLEND_OP_ADD;
	mBlendDesc.DestBlend = D3D10_BLEND_ZERO;
	mBlendDesc.DestBlendAlpha = D3D10_BLEND_ZERO;
	mBlendDesc.SrcBlend = D3D10_BLEND_ONE; //D3D10_BLEND_ZERO;//D3D10_BLEND_ONE;
	mBlendDesc.SrcBlendAlpha = D3D10_BLEND_ONE; //D3D10_BLEND_ZERO;//D3D10_BLEND_ONE;

	mRasterizerStateDirty = true;
	mDepthStencilDirty = true;
	mBlendStateDirty = true;

	mGeometryTopology = D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	mGeometryTopologyDirty = true;

	// this clear is temporary till vc compiles CheckErrors correctly
	if (mInfoQueue)
		mInfoQueue->ClearStoredMessages();
	
	UpdateStates();
	SetClearColour(Vector4(0.f, 0.f, 0.f, 0.f));
	SetDeviceState(DeviceState(), true);
	CheckErrors();

    return true;
}

void DX10Device::Deinit(RenderFactory* factory, Window* window)
{
    if (mRenderTargetView) mRenderTargetView->Release();
    if (mSwapChain) mSwapChain->Release();
    if (mD3DDevice) mD3DDevice->Release();
}

void DX10Device::Begin()
{
	Device::Begin();
    mD3DDevice->ClearRenderTargetView(mRenderTargetView, mClearColour.vec); //ClearRenderTarGetZAxis(mRenderTarGetZAxis, clearColor);
	mD3DDevice->ClearDepthStencilView(mDepthStencilView, D3D10_CLEAR_DEPTH, 1.0f, 0);
}

void DX10Device::End()
{
	mSwapChain->Present(0, 0);
}

void DX10Device::SetClearColour(const Vector4& colour)
{
	mClearColour = colour;
}

RenderBuffer* DX10Device::CreateRenderBuffer()
{
	return new DX10RenderBuffer(this);
}

void DX10Device::DestroyRenderBuffer(RenderBuffer** buffer)
{
	if (*buffer == 0)
		return;

	delete *buffer;
	*buffer = 0;
}

//void DX10Device::Draw(int instanceCount, Material* material, Geometry* geometry, int vertexFormat, unsigned int startIndex, unsigned int numIndices)
bool DX10Device::Draw(Geometry* geometry, unsigned int startIndex, unsigned int numIndices, int instanceCount)
{
	SetGeometryTopology(geometry->GetTopology());

	//DX10Effect* pEffect = static_cast<DX10Effect*>(material->GetEffect());

	int effectVertexFormat = mDeviceState.vertexFormat; //pEffect->GetCurrentVertexFormat();
	VertexFrame& frame = geometry->GetVertexFrame(0);

	DX10RenderBuffer* pos = static_cast<DX10RenderBuffer*>(frame.mPosition);
	DX10RenderBuffer* norm = static_cast<DX10RenderBuffer*>(frame.mNormal);
	DX10RenderBuffer* binorm = static_cast<DX10RenderBuffer*>(frame.mBinormal);
	DX10RenderBuffer* tang = static_cast<DX10RenderBuffer*>(frame.mTangent);
	DX10RenderBuffer* col = static_cast<DX10RenderBuffer*>(frame.mColour);
	DX10RenderBuffer* tex0 = static_cast<DX10RenderBuffer*>(frame.mTexCoord[0]);
	DX10RenderBuffer* idxBuffer = static_cast<DX10RenderBuffer*>(geometry->GetIndexBuffer());

	int		instIdx = 0;
	int		bufferIdx = 0;
	UINT	strides[15];
	UINT	offsets[15];
	ID3D10Buffer* pBuffers[15];

	memset(offsets, 0, sizeof(UINT) * 15);

	if (pos && (effectVertexFormat & VF_Position))
	{
		effectVertexFormat &= ~VF_Position;
		pBuffers[bufferIdx] = pos->GetBuffer();
		strides[bufferIdx] = pos->GetStride();
		++bufferIdx;
	}

	if (norm && (effectVertexFormat & VF_Normal))
	{
		effectVertexFormat &= ~VF_Normal;
		pBuffers[bufferIdx] = norm->GetBuffer();
		strides[bufferIdx] = norm->GetStride();
		++bufferIdx;
	}

	if (tang && (effectVertexFormat & VF_Tangent))
	{
		effectVertexFormat &= ~VF_Tangent;
		pBuffers[bufferIdx] = tang->GetBuffer();
		strides[bufferIdx] = tang->GetStride();
		++bufferIdx;
	}

	if (binorm && (effectVertexFormat & VF_Binormal))
	{
		effectVertexFormat &= ~VF_Binormal;
		pBuffers[bufferIdx] = binorm->GetBuffer();
		strides[bufferIdx] = binorm->GetStride();
		++bufferIdx;
	}

	if (col && (effectVertexFormat & VF_Colour))
	{
		effectVertexFormat &= ~VF_Colour;
		pBuffers[bufferIdx] = col->GetBuffer();
		strides[bufferIdx] = col->GetStride();
		++bufferIdx;
	}

	if (tex0 && (effectVertexFormat & VF_TexCoord0))
	{
		effectVertexFormat &= ~VF_TexCoord0;
		pBuffers[bufferIdx] = tex0->GetBuffer();
		strides[bufferIdx] = tex0->GetStride();
		++bufferIdx;
	}

	DX10RenderBuffer* instanceIdBuffer = 0;
	if (instanceCount != DT_NotInstanced && (effectVertexFormat & VF_InstanceId))
	{
		instanceIdBuffer = (DX10RenderBuffer*)CreateRenderBuffer();
		instanceIdBuffer->Init(RenderBufferType::RBT_UnsignedInt, instanceCount, RenderBufferUseage::RBU_Dynamic);

		RenderBufferLock lock;
		instanceIdBuffer->Lock(lock);
		for (int i = 0; i < instanceCount; ++i)
		{
			lock.uintData[i] = i;
			//instanceIdBuffer->SetUnsignedInt(i, i);
		}
		lock.count = instanceCount;

		instanceIdBuffer->Unlock(lock);

		effectVertexFormat &= ~VF_InstanceId;
		pBuffers[bufferIdx] = instanceIdBuffer->GetBuffer();
		strides[bufferIdx] = instanceIdBuffer->GetStride();
		++bufferIdx;
		++instIdx;
	}

	// only attempt to render if uploaded buffers match the vertex format or we get bad errors!
	if (effectVertexFormat != 0)
	{
		Log::Print("[DX10Device::Draw] Failed to render due to vertex format incompatibility\n");
		return false;
	}

	UpdateStates();

	UINT numVBsSet = bufferIdx;
	mD3DDevice->IASetVertexBuffers(0, numVBsSet, pBuffers, strides, offsets);
	mD3DDevice->IASetIndexBuffer(idxBuffer->GetBuffer(), DXGI_FORMAT_R32_UINT, 0);
	
	unsigned int size = numIndices == -1 ? idxBuffer->GetSize() : numIndices;

	//material->SetPassParameter();

	if (instanceCount == DT_NotInstanced)
		mD3DDevice->DrawIndexed(size, startIndex, 0);
	else
		mD3DDevice->DrawIndexedInstanced(size, instanceCount, startIndex, 0, 0);

	if (instanceIdBuffer)
	{
		instanceIdBuffer->Deinit();
		DestroyRenderBuffer((RenderBuffer**)&instanceIdBuffer);
	}

	return Device::Draw(geometry, startIndex, numIndices == -1 ? idxBuffer->GetSize() : numIndices, instanceCount);
}

void DX10Device::SetInputLayout(ID3D10InputLayout* pLayout)
{
	//ID3D10InputLayout* pLayout = pEffect->GetCurrentVertexLayout();
	mD3DDevice->IASetInputLayout(pLayout);
}

void DX10Device::UpdateStates()
{
	if (mRasterizerStateDirty)
	{
		// create new state
		HRESULT hr = mD3DDevice->CreateRasterizerState(&mRasterizerState, &mRasterState);
		if (FAILED(hr))
		{
			Log::Print("[DX10Device::Draw] CreateRasterizerState failed\n");
			return;
		}

		// bind
		mD3DDevice->RSSetState(mRasterState);
		mRasterizerStateDirty = false;
	}

	//
    // Update the Depth Stencil States (non-FX method)
    //
	if (mDepthStencilDirty)
	{
		// create new state
		HRESULT hr = mD3DDevice->CreateDepthStencilState(&mDepthStencilDesc, &mDepthStencilState);
		if (FAILED(hr))
		{
			Log::Print("[DX10Device::Draw] CreateDepthStencilState failed");
			return;
		}

		// Bind depth stencil state
		mD3DDevice->OMSetDepthStencilState(mDepthStencilState, 0);
		mDepthStencilDirty = false;
	}

	// update blend state if dirty
	if (mBlendStateDirty)
	{
		// create new blend state
		HRESULT hr = mD3DDevice->CreateBlendState(&mBlendDesc, &mBlendState);
		if (FAILED(hr))
		{
			Log::Print("[DX10Device::Draw] CreateBlendState failed");
			return;
		}

		// bind blend state
		FLOAT blendFactor[4] = { 1.f, 1.f, 1.f, 1.f };
		mD3DDevice->OMSetBlendState(mBlendState, blendFactor, 0xffffffff);
		mBlendStateDirty = false;
	}

	/// set primitive draw type
	if (mGeometryTopologyDirty)
	{
		mD3DDevice->IASetPrimitiveTopology(mGeometryTopology);
		mGeometryTopologyDirty = false;
	}
}

void DX10Device::SetMatrix(const Matrix4& matrix, MatrixMode mode)
{

}

void DX10Device::DrawPoint(const Vector4& p, const Vector4& colour)
{

}

void DX10Device::DrawLine(const Vector4& p1, const Vector4& p2, const Vector4& colour)
{

}

int DX10Device::CheckErrors() const
{
	ID3D10InfoQueue* queue = mInfoQueue;
	if (!queue)
		return 0;

	int i = 0;
	UINT64 count = queue->GetNumStoredMessagesAllowedByRetrievalFilter();
	for (; i < count; ++i)
	{
		D3D10_MESSAGE message;
		SIZE_T messageByteLength;
		queue->GetMessage(i, &message, &messageByteLength);
		int nothing = 0;
	}

	queue->ClearStoredMessages();
	return i;
}

void DX10Device::SetGeometryTopology(GeometryTopology topology)
{
	D3D10_PRIMITIVE_TOPOLOGY newTopology = D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	switch (topology)
	{
	case GT_PointList:
		newTopology = D3D10_PRIMITIVE_TOPOLOGY_POINTLIST;
		break;

	case GT_TriangleList:
		newTopology = D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
		break;

	case GT_TriangleListAdjacent:
		newTopology = D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST_ADJ;
		break;
	}

	if (mGeometryTopology == newTopology)
		return;

	mGeometryTopology = newTopology;
	mGeometryTopologyDirty = true;
}

void DX10Device::BeginDebugEvent(const string& name, const Vector4& colour)
{
#if defined(DEBUG) || defined(_DEBUG)
	unsigned int len = name.length();
	WCHAR wideName[256];
	wideName[len] = '\0';
	MultiByteToWideChar(CP_ACP, 0, name.c_str(), len, wideName, len);

	D3DCOLOR col = D3DCOLOR_COLORVALUE(colour.x, colour.y, colour.z, colour.w);
 	D3DPERF_BeginEvent(col, wideName);
#endif
}

void DX10Device::EndDebugEvent()
{
#if defined(DEBUG) || defined(_DEBUG)
	D3DPERF_EndEvent();
#endif
}

Vector4	DX10Device::GetClearColour()
{
	return mClearColour;
}

bool DX10Device::SetDeviceState(const DeviceState& state, bool force)
{
	//Assert(false); // LOOK AT OPENGL SetDeviceState and copy that to defer setting of device states

	// matches enum Compare
	D3D10_COMPARISON_FUNC compareLookup[] = 
	{
		D3D10_COMPARISON_LESS,
		D3D10_COMPARISON_LESS_EQUAL,
		D3D10_COMPARISON_EQUAL,
		D3D10_COMPARISON_GREATER,
		D3D10_COMPARISON_GREATER_EQUAL,
		D3D10_COMPARISON_ALWAYS,
		D3D10_COMPARISON_NEVER,
		D3D10_COMPARISON_NOT_EQUAL,
	};

	// matches Operation enum
	D3D10_STENCIL_OP operationLookup[] =
	{
		D3D10_STENCIL_OP_ZERO,
		D3D10_STENCIL_OP_KEEP,
		D3D10_STENCIL_OP_REPLACE,
		D3D10_STENCIL_OP_INCR,
		D3D10_STENCIL_OP_DECR,
		D3D10_STENCIL_OP_INVERT,
	};

	// matches blendmode enum
	D3D10_BLEND blendModeLookup[] =
	{
		D3D10_BLEND_ZERO,
		D3D10_BLEND_ONE,
		D3D10_BLEND_DEST_COLOR,
		D3D10_BLEND_SRC_COLOR,
		D3D10_BLEND_INV_DEST_COLOR,
		D3D10_BLEND_INV_SRC_COLOR,
		D3D10_BLEND_SRC_ALPHA,
		D3D10_BLEND_INV_SRC_ALPHA,
		D3D10_BLEND_DEST_ALPHA,
		D3D10_BLEND_INV_DEST_ALPHA,
		D3D10_BLEND_SRC_ALPHA_SAT,
	};

	D3D10_CULL_MODE cullFaceLookup[] =
	{
		D3D10_CULL_FRONT,
		D3D10_CULL_BACK,
		D3D10_CULL_NONE,
	};

	DeviceState& prevState = mDeviceState;

	if (force || state.cullFace != prevState.cullFace)
	{
		mRasterizerState.CullMode = cullFaceLookup[state.cullFace];
		mRasterizerStateDirty = true;
	}

	// alpha
	if (force || state.alphaToCoverageEnable != prevState.alphaToCoverageEnable)
	{
		mBlendDesc.AlphaToCoverageEnable = state.alphaToCoverageEnable;
		mBlendStateDirty = true;
	}

	if (force || state.alphaBlendEnable != prevState.alphaBlendEnable)
	{
		for (int i = 0; i < 8; ++i)
			mBlendDesc.BlendEnable[i] = state.alphaBlendEnable;

		mBlendStateDirty = true;
	}

	if (force || state.alphaBlendEnable)
	{
		if (force || state.alphaBlendSource != prevState.alphaBlendSource || state.alphaBlendDest != prevState.alphaBlendDest)
		{
			mBlendDesc.SrcBlend = blendModeLookup[state.alphaBlendSource];
			mBlendDesc.DestBlend = blendModeLookup[state.alphaBlendDest];
			mBlendStateDirty = true;
		}
	}

	// depth
	if (force || state.depthCompare != prevState.depthCompare)
	{
		mDepthStencilDesc.DepthFunc = compareLookup[state.depthCompare];
		mDepthStencilDirty = true;
	}

	if (force || state.depthTestEnable != prevState.depthTestEnable)
	{
		mDepthStencilDesc.DepthEnable = state.depthTestEnable;
		mDepthStencilDirty = true;
	}

	if (force || state.depthWriteEnable != prevState.depthWriteEnable)
	{
		D3D10_DEPTH_WRITE_MASK writeMask = state.depthWriteEnable ? D3D10_DEPTH_WRITE_MASK_ALL : D3D10_DEPTH_WRITE_MASK_ZERO;
		mDepthStencilDesc.DepthWriteMask = writeMask;
		mDepthStencilDirty = true;
	}

	// stencil
	if (force || prevState.stencilEnable != state.stencilEnable)
	{
		mDepthStencilDesc.StencilEnable = prevState.stencilEnable;
		mDepthStencilDirty = true;
	}

	if (force || state.stencilEnable)
	{
		// bah, front and back are reversed in dx10
		if (force || state.stencilFace[CF_Front].compare != prevState.stencilFace[CF_Front].compare
			|| state.stencilReadMask != prevState.stencilReadMask
			|| state.stencilWriteMask != prevState.stencilWriteMask
			|| state.stencilFace[CF_Front].fail != prevState.stencilFace[CF_Front].fail
			|| state.stencilFace[CF_Front].depthFail != prevState.stencilFace[CF_Front].depthFail
			|| state.stencilFace[CF_Front].pass != prevState.stencilFace[CF_Front].pass)
		{
			mDepthStencilDesc.FrontFace.StencilFunc = compareLookup[state.stencilFace[CF_Back].compare];
			mDepthStencilDesc.FrontFace.StencilFailOp = operationLookup[state.stencilFace[CF_Back].fail];
			mDepthStencilDesc.FrontFace.StencilDepthFailOp = operationLookup[state.stencilFace[CF_Back].depthFail];
			mDepthStencilDesc.FrontFace.StencilPassOp = operationLookup[state.stencilFace[CF_Back].pass];

			mDepthStencilDesc.BackFace.StencilFunc = compareLookup[state.stencilFace[CF_Front].compare];
			mDepthStencilDesc.BackFace.StencilFailOp = operationLookup[state.stencilFace[CF_Front].fail];
			mDepthStencilDesc.BackFace.StencilDepthFailOp = operationLookup[state.stencilFace[CF_Front].depthFail];
			mDepthStencilDesc.BackFace.StencilPassOp = operationLookup[state.stencilFace[CF_Front].pass];

			mDepthStencilDesc.StencilReadMask = state.stencilReadMask;
			mDepthStencilDesc.StencilWriteMask = state.stencilWriteMask;
			mDepthStencilDirty = true;
		}
	}

	mDeviceState = state;
	return true;
}

bool DX10Device::GetDeviceState(DeviceState& state)
{
	state = mDeviceState;
	return true;
}

