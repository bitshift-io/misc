#ifndef _SCENE_NODE_H_
#define _SCENE_NODE_H_

#include <vector>
#include "File/File.h"
#include "Math/Matrix.h"
#include "Math/Box.h"

using namespace std;

class Device;
class RenderFactory;
class ObjectLoadParams;
class Scene;
class SceneObject;
class SceneAnimationNode;

#undef GetObject

//
// Custom data
//
class Property
{
public:

	int		AsInt() const		{ int value = -1; sscanf(mValue.c_str(), "%i", &value); return value; }
	float	AsFloat() const		{ float value = -1.f; sscanf(mValue.c_str(), "%f", &value); return value; }

	string mKey;
	string mValue;	
};

enum SceneNodeFlags
{
	SNF_None	= 0,
	SNF_Hidden	= 1 << 0,
	SNF_Visible	= 1 << 1,
};

class SceneNode
{
	friend class Scene;
	friend class Renderer;

public:
	
	typedef vector<SceneNode*>::iterator	Iterator;
	typedef vector<Property>::iterator		PropertyIterator;

	void				InsertChild(SceneNode* child)					{ child->SetParent(this); mChild.push_back(child); }
	void				RemoveChild(SceneNode* child);

	void				SetObject(SceneObject* object)					{ mObject = object; }
	SceneObject*		GetObject()										{ return mObject; }

	Iterator			Begin() 										{ return mChild.begin(); }
	Iterator			End()											{ return mChild.end(); }
	unsigned int		GetNumChildNode()								{ return (unsigned int)mChild.size(); }

	const Property*		GetProperty(const string& key);
	void				AddProperty(const Property& property)			{ mProperty.push_back(property); }

	void				SetName(const string& name)						{ mName = name; }
	const string&		GetName()										{ return mName; }

	const Matrix4&		GetOrigonalLocalTransform() const				{ return mOrigonalTransform; }
	void				SetLocalTransform(const Matrix4& matrix);
	Matrix4				GetLocalTransform() const;
	Matrix4				GetWorldTransform() const;
	void				SetWorldTransform(const Matrix4& matrix);

	PropertyIterator	PropertyBegin() 								{ return mProperty.begin(); }
	PropertyIterator	PropertyEnd()									{ return mProperty.end(); }
	unsigned int		GetNumProperty()								{ return (unsigned int)mProperty.size(); }

	void				SetParent(SceneNode* parent)					{ mParent = parent; }
	SceneNode*			GetParent()										{ return mParent; }

	SceneNode*			GetNodeByName(const string& name);

	bool				Clone(ObjectLoadParams* params, SceneNode* copy, Scene* scene);
	bool				Load(ObjectLoadParams* params, Scene* scene);
	bool				LoadObject(ObjectLoadParams* params, Scene* scene);
	bool				Save(File& out);
	bool				SaveObject(File& out);

	Scene*				GetOwner()										{ return mOwner; }

	unsigned int		GetFlags()										{ return mFlags; }

	// if owner is supplied, will only be applied to those nodes that belong to the scene
	void				SetFlags(unsigned int flags, bool applyToChildren = false, Scene* owner = 0);
	void				ClearFlags(unsigned int flags, bool applyToChildren = false, Scene* owner = 0);

	SceneAnimationNode*	GetSceneAnimationNode()							{ return mSceneAnimationNode; }
	void				SetSceneAnimationNode(SceneAnimationNode* node)	{ mSceneAnimationNode = node; }

protected:

	SceneNode(Scene* owner)	: mTransform(MI_Identity), mObject(0), mParent(0), mOwner(owner), mFlags(0), mSceneAnimationNode(0) { }

	SceneObject*			mObject;
	vector<SceneNode*>		mChild;
	SceneNode*				mParent;
	vector<Property>		mProperty;
	string					mName;
	Scene*					mOwner;
	unsigned int			mFlags;
	Matrix4					mTransform;
	Matrix4					mOrigonalTransform;
	SceneAnimationNode*		mSceneAnimationNode;
};

inline const Property* SceneNode::GetProperty(const string& key)
{
	vector<Property>::iterator it;
	for (it = mProperty.begin(); it != mProperty.end(); ++it)
	{
		if (_stricmp(key.c_str(), it->mKey.c_str()) == 0)
			return &*it;
	}

	return 0;
}

#endif
