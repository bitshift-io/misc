#ifndef _SCENE_H_
#define _SCENE_H_

#include <string>
#include "SceneObject.h"
#include "SceneNode.h"
#include "File/File.h"
#include "Memory/Memory.h"

using namespace std;

class Animation;

typedef SceneObject* (*FnObjectCreate)();

template <class T>
SceneObject* ObjectCreator()
{
	return new T();
}

//
// The uber engine file format, encapsulates scene nodes
// any attachments (eg, arrays of particle systems, animations etc..)
//
class Scene
{
	friend class RenderFactory;

public:

	typedef vector<SceneNode*>::iterator	Iterator;

	bool			Clone(ObjectLoadParams* params, Scene* copy);
	bool			Load(ObjectLoadParams* params);
	bool			Save(const string& file);

	SceneNode*		CreateNode()									{ return new SceneNode(this); }
	void			DestroyNode(SceneNode* node)					{ delete node; }

	SceneNode*		GetRootNode()									{ return mRoot; }

	SceneNode*		GetNodeByName(const string& name);

	Matrix4			GetTransform() const							{ return mRoot->GetWorldTransform(); }
	void			SetTransform(const Matrix4& matrix) const		{ return mRoot->SetLocalTransform(matrix); }

	const string&	GetName()										{ return mName; }
	void			SetName(const string& name)						{ mName = name; }

	// TODO
	// reset all nodes to origonal transforms
	//void			Reset();

	// apply sto ALL children of the supplied node
	void			SetFlags(unsigned int flags, SceneNode* node = 0, bool affectOwnedOnly = true);
	void			ClearFlags(unsigned int flags, SceneNode* node = 0, bool affectOwnedOnly = true);

	void			DebugBoundBox(Device* device, SceneNode* node = 0);
	void			DebugNode(Device* device, SceneNode* node = 0, float scale = 1.0f);
/*
	// hrmm how are we to set up blend layers and such this way? this mighht be temporary
	// might need a speerate animation instance class to handle things? instance specifics?
	void			SetAnimation(const char* name, SceneNode* node = 0);
	void			SetAnimationTime(float time, SceneNode* node = 0);
*/
	// adds an animation with a scene so it knows its usable
	void			InsertAnimation(Animation* anim, SceneNode* node = 0, bool firstCall = true);
	Animation*		GetAnimation(const char* name);
	void			SetAnimation(Animation* anim, SceneNode* node = 0);
	void			ApplyAnimation(SceneNode* node = 0, bool bFirstCall = true);

	// progress/set anim time on all anims (if NULL) or a specific anim
	void			ProgressAnimation(float deltaTime, Animation* animation = 0, SceneNode* node = 0);
	void			SetAnimationTime(float time, Animation* animation = 0, SceneNode* node = 0);

	RenderFactory*	GetRenderFactory()								{ return mFactory; }

protected:

	Scene()															{}

	void			ApplySkinPalette(SceneNode* node = 0);
	virtual void	Create(RenderFactory* factory)			{ mFactory = factory; mRoot = CreateNode(); mRoot->SetName("root"); }
	virtual void	Destroy(const RenderFactory* factory);
	void			Destroy(const RenderFactory* factory, SceneNode* node);

	//bool			Load(const ObjectLoadParams* params, SceneNode* node);
	//bool			Save(const File& out, SceneNode* node);

	SceneNode*		mRoot;
	string			mName;

	vector<Animation*>	mAnimation;
	RenderFactory*		mFactory;
};

#endif
