#ifndef _PARTICLESYSTEM_H_
#define _PARTICLESYSTEM_H_

#include "SceneObject.h"
#include "ParticleAction.h"
#include <list>

using namespace std;

class ParticleActionGroup;
class ParticleSystem;
class Camera;
class Scene;

class Particle
{
public:

	virtual void Init()
	{
		mLife = 0.f;
		mColour.SetOne();
		mTransform.SetIdentity();
		mVelocity.SetZero();
		mActionGroup = 0;
	}

	virtual void Update(ParticleSystem* system);

	float					mLife;
	Vector4					mColour;
	Matrix4					mTransform;
	Vector4					mVelocity;
	ParticleActionGroup*	mActionGroup;
	float					mDistToPointSqrd;
};

class ParticleActionGroup : public SceneObject
{
public:

	TYPE(15)

	typedef list<ParticleAction*>::iterator Iterator;

	void				AddAction(ParticleAction* action);

	Iterator			Begin()										{ return mAction.begin(); }
	Iterator			End()										{ return mAction.end(); }

	virtual SceneObject*	Clone(SceneNode* owner);
	

	void				Update(ParticleSystem* system);
//	void				Update(Particle* particle);

	int					GetRemainingActionCount(ParticleSystem* system, int actionType);

	virtual bool		Save(const File& out, SceneNode* node = 0);
	virtual bool		Load(const ObjectLoadParams* params, SceneNode* node = 0);

	ParticleAction*		CreateAction(int type);
	void				ReleaseAction(ParticleAction** action);

	SceneNode*			GetSceneNode()								{ return mSceneNode; }

	list<ParticleAction*>	mAction;
	SceneNode*				mSceneNode;
};

//
// Extends this class to be able to supply your own particles
//
class ParticleContainer
{
public:

	typedef vector<Particle*>::iterator Iterator;

	ParticleContainer();

	virtual Particle* Create(ParticleActionGroup* actionGroup);

	void			Update(ParticleSystem* system);
	void			Reset();

	// iterate over all particles
	Iterator		Begin()										{ return mLiveList.begin(); }
	Iterator		End()										{ return mLiveList.end(); }

	// iterate over existing particles
	Iterator		ExistingBegin()								{ return mLiveList.begin(); }
	Iterator		ExistingEnd()								{ return GetSeperator(); }

	// iterate over new particles
	Iterator		NewBegin()									{ return GetSeperator(); }
	Iterator		NewEnd()									{ return mLiveList.end(); }

	Iterator		Erase(Iterator it);
	void			Clear();

	virtual Particle*		AddParticle(ParticleActionGroup* actiongroup);

	virtual void			SortByDistance(ParticleSystem* system, const Camera* camera);

	unsigned int	GetExistingCount()							{ return (unsigned int)mLiveList.size(); }
	unsigned int	GetTotalCount()								{ return (unsigned int)mLiveList.size() + mDeadList.size(); }

	Iterator		GetSeperator();

	void			SetLizeListSize(unsigned int size);

	vector<Particle*>		mLiveList;
	vector<Particle*>		mDeadList;
	//Iterator				mSeperator;
	int						mSeperator;
};

class ParticleSystem : public SceneObject
{
public:

	TYPE(14)

	typedef	ParticleContainer::Iterator Iterator;

	void			Init(Scene* scene, ParticleContainer* container);

	virtual bool	Save(const File& out, SceneNode* node = 0)					{ return true; }
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node = 0)	{ return true; }

	// reset the system to its initial state
	void			Reset();

	void			Update(float deltaTime);

	// iterate over all particles
	Iterator		Begin()										{ return mContainer->Begin(); }
	Iterator		End()										{ return mContainer->End(); }

	// iterate over existing particles
	Iterator		ExistingBegin()								{ return mContainer->ExistingBegin(); }
	Iterator		ExistingEnd()								{ return mContainer->ExistingEnd(); }

	// iterate over new particles
	Iterator		NewBegin()									{ return mContainer->NewBegin(); }
	Iterator		NewEnd()									{ return mContainer->NewEnd(); }

	Iterator		Erase(Iterator it)							{ return mContainer->Erase(it); }
	void			Clear()										{ mContainer->Clear(); }
	Particle*		AddParticle(ParticleActionGroup* actionGroup);

	// only for debugging please
	void			Draw(Device* device, Camera* camera);

	const Matrix4&	GetTransform()								{ return mTransform; }
	void			SetTransform(const Matrix4& transform)		{ mTransform = transform; }

	void			AddToIgnoreList(ParticleAction* action);
	bool			IgnoreAction(ParticleAction* action);

	float			GetDeltaTime()								{ return mDeltaTime; }
	float			GetTime()									{ return mTime; }

	bool			IsFinished();

	void			SortByDistance(const Camera* camera);

protected:

	void			Update(float deltaTime, SceneNode* node);

	int				GetRemainingActionCount(SceneNode* node, int actionType);

	Matrix4					mTransform;
	Scene*					mScene;
	list<ParticleAction*>	mIgnoreList;
	ParticleContainer*		mContainer;
	ParticleSystem*			mSystem;
	float					mDeltaTime;
	float					mTime;
};

#endif
