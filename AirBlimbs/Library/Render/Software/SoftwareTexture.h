#ifndef _SOFTWARETEXTURE_H_
#define _SOFTWARETEXTURE_H_

#include "Render/Texture.h"

class SoftwareTextureShader
{
public:

	virtual Vector4 Tex2D(const Vector4& coord) = 0;
	virtual Vector4 Tex3D(const Vector4& coord) = 0;
};

class SoftwareTexture : public Texture
{
public:

	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node = 0);

	virtual bool	Init(Device* device, unsigned char* image, int channels, int width, int height, int depth, int flags = TF_2D | TF_LowDynamicRange);

	virtual bool	Lock(TextureSubresource& resource, int lockFlags) { return false; }
	virtual void	Unlock() {}

	virtual bool	GetSubresource(TextureSubresource& resource) { return false; }

	virtual bool	IsInstance(const ObjectLoadParams* params, SceneObject* loaded);

	// functions for shaders
	inline Vector4			Tex2D(const Vector4& uvCoord);

protected:

	bool	GenerateTexture(TextureResource* texRes, int flags = 0);
	bool	GenerateCubeTexture(TextureResource* texRes, int flags = 0);

	virtual void	Destroy(RenderFactory* factory);

	unsigned char*		mTexture;
	unsigned int		mWidth;
	unsigned int		mHeight;
	unsigned int		mChannels;

	string				mName;
};


inline Vector4 SoftwareTexture::Tex2D(const Vector4& uvCoord)
{
	// NOTE:
	// maybe store textures in float format?
	Vector4 result;

	int x = float(mWidth) * uvCoord.x;
	int y = float(mHeight) * uvCoord.y;

	// repeat
	x = x % mWidth;
	y = y % mHeight;

	unsigned char* pixel = &mTexture[((y * mWidth) + x) * mChannels];
	result.x = float(pixel[0]) / 255.f;
	result.y = float(pixel[1]) / 255.f;
	result.z = float(pixel[2]) / 255.f;
	if (mChannels >= 4)
		result.w = float(pixel[3]) / 255.f;

	return result;
}

#endif