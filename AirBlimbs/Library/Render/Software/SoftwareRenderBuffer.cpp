#include "SoftwareRenderBuffer.h"
#include "File/File.h"
#include "Math/Vector.h"
#include "File/Log.h"

#define BUFFER_OFFSET(i) ((char *)0 + (i))

SoftwareRenderBuffer::SoftwareRenderBuffer() :
	mSize(0),
	mData(0)
{
}

void SoftwareRenderBuffer::Init(RenderBufferType type, unsigned int size, RenderBufferUseage useage)
{
	RenderBuffer::Init(type, size, useage);
	mSize = -1;

	if (size != -1)
	{
		unsigned int bufferSize = 0;
		switch (mType)
		{
		case RBT_Matrix4:
			bufferSize = size * sizeof(Matrix4);
			break;

		case RBT_Vector4:
			bufferSize = size * sizeof(Vector4);
			break;

		case RBT_UnsignedInt:
			bufferSize = size * sizeof(unsigned int);
			break;
		}

		mSize = bufferSize;
		mData = _aligned_malloc(bufferSize, 16);
	}

	mUseage = useage;
}

void SoftwareRenderBuffer::Deinit()
{
	if (mData)
		_aligned_free(mData);

	mData = 0;
}

bool SoftwareRenderBuffer::Save(const File& out)
{
	return true;
}

bool SoftwareRenderBuffer::Load(Device* device, const File& in)
{
	// read the type
	in.Read(&mType, sizeof(RenderBufferType));

	// read the data
	if ((mSize = LoadVector(in, &mData)) == -1)
		return false;

	return true;
}


void SoftwareRenderBuffer::Bind(VertexFormat channel, int index)
{
	mChannel = channel;
	mIndex = index;
}

void SoftwareRenderBuffer::Unbind()
{
}

void SoftwareRenderBuffer::Draw(unsigned int startIndex, unsigned int numIndices)
{
	unsigned int size = numIndices == -1 ? GetSize() : numIndices;
}

bool SoftwareRenderBuffer::Lock(RenderBufferLock& lock, int lockFlags)
{
	lock.count = mSize;
	lock.data = mData;
	return lock.data != 0;
}

void SoftwareRenderBuffer::Unlock(RenderBufferLock& lock)
{
	lock.data = 0;
}

void SoftwareRenderBuffer::ReloadResource(Device* device)
{
}