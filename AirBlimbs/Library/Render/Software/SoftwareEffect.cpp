#include "SoftwareEffect.h"
#include "SoftwareRenderFactory.h"
#include "File/Log.h"

void SoftwareEffect::Destroy(RenderFactory* factory)
{
	EffectParameterIterator paramIt;
	for (paramIt = BeginParameter(); paramIt != EndParameter(); ++paramIt)
	{
		delete *paramIt;
		*paramIt = 0;
	}

	mParameter.resize(0);
}

bool SoftwareEffect::Load(const ObjectLoadParams* params, SceneNode* node)
{/*
	// i should have a software shader file .sw which lists techniqus in a particular shader
	// so we dont just need a single technique :)
	SoftwareRenderFactory* renderFactory = (SoftwareRenderFactory*)params->mFactory;

	SoftwareEffectTechnique* technique = renderFactory->GetEffectTechnique(params->mName);
	if (!technique)
	{
		technique = renderFactory->GetEffectTechnique("null");
		if (!technique)
			return false;
	}

	mTechnique.push_back(technique);
	mCurrentTechnique = technique;*/

	Effect::Load(params, node);

	// find an existing effect to clone
	SoftwareRenderFactory* renderFactory = (SoftwareRenderFactory*)params->mFactory;
	SoftwareEffect* effect = renderFactory->GetEffectByName(params->mName.c_str());

	if (!effect)
	{
		Log::Error("[SoftwareEffect::Load] Attempting to load a shader that does not exist '%s'", params->mName.c_str());
		return false;
	}

	EffectTechniqueIterator techniqueIt;
	for (techniqueIt = effect->BeginTechnique(); techniqueIt != effect->EndTechnique(); ++techniqueIt)
	{
		SoftwareEffectTechnique* effectTechnique = (SoftwareEffectTechnique*)*techniqueIt;
		if (effect != this)
			mTechnique.push_back(effectTechnique);		
	}

	mCurrentTechnique = 0;
	if (mTechnique.size())
		mCurrentTechnique = mTechnique.front();

	ParameterData* parameterData = GetParameterData(); //effectTechnique->mParameterData;
	ParameterDataLayout* parameterDataLayout = parameterData->GetParameterDataLayout();

	int p = 0;
	while (parameterDataLayout[p].ID != EPID_Max)
	{
		EffectParameter* effectParam = new EffectParameter();
		effectParam->mEffect = this;
		effectParam->mID = parameterDataLayout[p].ID;
		effectParam->mType = parameterDataLayout[p].type;
		effectParam->mBufferOffset = parameterDataLayout[p].offset;
		effectParam->mSize = parameterDataLayout[p].size;	
		effectParam->mName = parameterDataLayout[p].name;
		effectParam->mHandle = effectParam;

		mParameter.push_back(effectParam);
		++p;
	}

	return false;
}

SoftwareVertexShader* SoftwareEffect::GetVertexShader()		
{ 
	return ((SoftwareEffectTechnique*)mCurrentTechnique)->mVertexShader; 
}

SoftwarePixelShader* SoftwareEffect::GetPixelShader()		
{ 
	return ((SoftwareEffectTechnique*)mCurrentTechnique)->mPixelShader; 
}

ParameterData* SoftwareEffect::GetParameterData()
{
	return ((SoftwareEffectTechnique*)mCurrentTechnique)->mParameterData; 
}

void SoftwareEffect::SetName(const char* name)
{
	mName = name;
}

void SoftwareEffect::AddTechnique(SoftwareEffectTechnique* technique)
{
	// first technique becomes the current technique
	if (!mCurrentTechnique)
		mCurrentTechnique = technique;

	mTechnique.push_back(technique);	
}

void SoftwareEffect::SetPassParameter()
{
	Effect::SetPassParameter();
}

void SoftwareEffect::SetValue(EffectParameter* param, const Matrix4* value, unsigned int count)
{
	SetValue(param, (void*)value, sizeof(Matrix4) * count);
}

void SoftwareEffect::SetValue(EffectParameter* param, const float* value, unsigned int count)
{
	SetValue(param, (void*)value, sizeof(float) * count);
}

void SoftwareEffect::SetValue(EffectParameter* param, Texture** value, unsigned int count)
{
	SetValue(param, (void*)value, sizeof(Texture*) * count);
}

void SoftwareEffect::SetValue(EffectParameter* param, const Vector4* value, unsigned int count)
{
	SetValue(param, (void*)value, sizeof(Vector4) * count);
}

void SoftwareEffect::SetValue(EffectParameter* param, const int* value, unsigned int count)
{
	SetValue(param, (void*)value, sizeof(int) * count);
}

void SoftwareEffect::SetValue(EffectParameter* param, void* buffer, unsigned int size)
{
	SoftwareEffectTechnique* technique = (SoftwareEffectTechnique*)mCurrentTechnique;
	unsigned char* parameterData = (unsigned char*)technique->mParameterData;

	EffectParameter* effectParam = (EffectParameter*)param->mHandle;
	memcpy(parameterData + effectParam->mBufferOffset, buffer, size);
}

int SoftwareEffect::Begin()
{ 
	static_cast<SoftwareDevice*>(mDevice)->SetEffect(this);
	return 1; 
}

void SoftwareEffect::End()						
{
	static_cast<SoftwareDevice*>(mDevice)->SetEffect(0);
}