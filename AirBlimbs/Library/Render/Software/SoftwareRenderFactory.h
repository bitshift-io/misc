#ifndef _SOFTWARERENDERFACTORY_H_
#define _SOFTWARERENDERFACTORY_H_

#include "Render/RenderFactory.h"

#undef CreateWindow

class Window;
//class SoftwareEffectTechnique;
class SoftwareEffect;

class SoftwareRenderFactory : public RenderFactory
{
public:

	SoftwareRenderFactory(const RenderFactoryParameter& parameter);

	virtual void			Init();
	virtual void			Deinit();

	virtual Window*			CreateWindow();
	virtual void			ReleaseWindow(Window** window);

	virtual Device*			CreateDevice();
	virtual void			ReleaseDevice(Device** device);

	//void						RegisterEffectTechnique(SoftwareEffectTechnique* technique);
	//SoftwareEffectTechnique*	GetEffectTechnique(const string& name);

	SoftwareEffect*			GetEffectByName(const char* name);

	virtual RenderFactoryType	GetRenderFactoryType()			{ return RFT_Software; }

protected:

	//vector<SoftwareEffectTechnique*>	mEffectTechnique;
};

#endif
