#include "SoftwareDevice.h"
#include "SoftwareRenderBuffer.h"
#include "SoftwareEffect.h"
#include "Render/Material.h"
#include "File/Log.h"
#include "Render/Geometry.h"
#include "Math/Line.h"
#include "Math/Ray.h"
#include "Render/Camera.h"
#include "Memory/Memory.h"
#include "Thread/ThreadPool.h"

#ifdef WIN32
	#include "Render/Win32/Win32Window.h"
#else
	#include "Render/Linux/LinuxWindow.h"
	#include <malloc.h>
#endif

#include <algorithm>

// http://www.codeproject.com/cpp/sseintro.asp
// linux needs -msse compile option
#include <xmmintrin.h>

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLu32.lib")
//#pragma comment(lib, "GLaux.lib")

#ifndef WIN32
    #define __align16  __attribute__((align(16) ))
	#define inline __forceinline
#else
    #define __align16 __declspec(align(16) )
#endif

//#define MULTITHREAD

const unsigned int cMaxClipList = 10;

//
// task for multithreading the drawing
//
class DrawTask : public Task
{
public:

	virtual unsigned long DoTask()
	{
		device->DrawInternal(effect, geometry, startIndex, numIndices, instanceCount, memory);
		return 0;
	}

	SoftwareDevice* device;
	int				instanceCount;
	SoftwareEffect*	effect;
	Geometry*		geometry;
	int				vertexFormat;
	unsigned int	startIndex;
	unsigned int	numIndices;

	unsigned char*	memory;
};

bool SoftwareDevice::Init(RenderFactory* factory, Window* window, int multisample)
{
	mInterlace = false;

	mColourBuffer = 0;
	mDepthBuffer = 0;
	mStencilBuffer = 0;

	mWidth = 0;
	mHeight = 0;

	SetClearColour(Vector4(0.f, 0.f, 0.f, 0.f));

	mCullFace = CF_Back;

#ifdef WIN32
	mWindow = (Win32Window*)window;

	static PIXELFORMATDESCRIPTOR pfd =				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		32,											// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		16,											// 16Bit Z-Buffer (Depth Buffer)
		1,											// Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};

	if (!(mHDC = GetDC(mWindow->GetHandle())))							// Did We Get A Device Context?
	{
		MessageBoxW(NULL, (LPCWSTR)"Can't Create A GL Device Context.", (LPCWSTR)"ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;
	}

	int pixelFormat = -1;
	if (pixelFormat == -1)
	{
		if (!(pixelFormat = ChoosePixelFormat(mHDC, &pfd)))	// Did Windows Find A Matching Pixel Format?
		{
			MessageBoxW(NULL, (LPCWSTR)"Can't Find A Suitable PixelFormat.", (LPCWSTR)"ERROR", MB_OK|MB_ICONEXCLAMATION);
			return FALSE;								// Return FALSE
		}
	}

	if (!SetPixelFormat(mHDC, pixelFormat, &pfd))		// Are We Able To Set The Pixel Format?
	{
		MessageBoxW(NULL, (LPCWSTR)"Can't Set The PixelFormat.", (LPCWSTR)"ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(mHRC = wglCreateContext(mHDC)))				// Are We Able To Get A Rendering Context?
	{
		MessageBoxW(NULL, (LPCWSTR)"Can't Create A GL Rendering Context.", (LPCWSTR)"ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!wglMakeCurrent(mHDC, mHRC))					// Try To Activate The Rendering Context
	{
		MessageBoxW(NULL, (LPCWSTR)"Can't Activate The GL Rendering Context.", (LPCWSTR)"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}
#else
    mWindow = (LinuxWindow*)window;
#endif

	gThreadPool.Init();

	mMemoryPool = 0;
	mMemoryPoolSize = 0;

	// set default state
	SetDeviceState(DeviceState(), true);
	return true;
}

void SoftwareDevice::Deinit(RenderFactory* factory, Window* window)
{
	if (mMemoryPool)
	{
		_aligned_free(mMemoryPool);
		mMemoryPoolSize = 0;
	}

	// free frame buffer textures
	if (mColourBuffer)
		_aligned_free(mColourBuffer);

	if (mDepthBuffer)
		_aligned_free(mDepthBuffer);

	if (mStencilBuffer)
		_aligned_free(mStencilBuffer);

#ifdef WIN32
	if (mHRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBoxW(NULL,(LPCWSTR)"Release Of DC And RC Failed.",(LPCWSTR)"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(mHRC))						// Are We Able To Delete The RC?
		{
			MessageBoxW(NULL,(LPCWSTR)"Release Rendering Context Failed.",(LPCWSTR)"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		mHRC = NULL;										// Set RC To NULL
	}

	if (mHDC && !ReleaseDC(mWindow->GetHandle(), mHDC))					// Are We Able To Release The DC
	{
		MessageBoxW(NULL,(LPCWSTR)"Release Device Context Failed.",(LPCWSTR)"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		mHDC = NULL;										// Set DC To NULL
	}
#else

#endif
}

void SoftwareDevice::IncreaseMemoryPoolSize(unsigned int newSize)
{
	if (newSize <= mMemoryPoolSize)
		return;

	if (mMemoryPool)
		_aligned_free(mMemoryPool);

	mMemoryPoolSize = Math::Max(newSize, mMemoryPoolSize);
	mMemoryPool = (unsigned char*)_aligned_malloc(mMemoryPoolSize, 16);
}

void SoftwareDevice::Begin()
{
	Device::Begin();
	WindowResolution res;
	mWindow->GetWindowResolution(res);
	ResetDevice(res.width, res.height);

	glClearStencil(0);
	glClear(GL_STENCIL_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	// clear buffers to appropriate values
	std::fill_n((int*)mColourBuffer, mWidth * mHeight, (int)mClearColour); 
	std::fill_n(mDepthBuffer, mWidth * mHeight, 1.f); 
/*
#ifndef WIN32
	for (int i = 0; i < (mWidth * mHeight * 4); i += 4)
		memcpy(&mColourBuffer[i], mClearColour, 4 * sizeof(unsigned char));

	for (int i = 0; i < (mWidth * mHeight); ++i)
		mDepthBuffer[i] = 1.f;
#else
	// clear colour buffer
	{
		int count = (int)floorf(float(mWidth * mHeight * 4) / 16.f);
		__m128* colourBuf = (__m128*)mColourBuffer;
		__m128 clearColour = _mm_set_ps1(*(float*)mClearColour);
		int i = 0;
		for (int c = 0; c < count; ++c, i += 16)
		{
			_mm_stream_ps((float*)colourBuf, clearColour);
//			*colourBuf = clearColour;
			++colourBuf;
		}
		//for (; i < (mWidth * mHeight * 4); i += 4)
		//	memcpy(&mColourBuffer[i], mClearColour, 4 * sizeof(unsigned char));
	}

	// clear depth buffer
	{
		int count = (int)floorf(float(mWidth * mHeight) / 4.f);
		__m128* depthBuf = (__m128*)mDepthBuffer;
		__m128 clearDepth = _mm_set_ps1(1.f);
		int i = 0;
		for (int c = 0; c < count; ++c, i += 4)
		{
			_mm_stream_ps((float*)depthBuf, clearDepth);
//			*depthBuf = clearDepth;
			++depthBuf;
		}
		//for (; i < (mWidth * mHeight); ++i)
		//	mDepthBuffer[i] = 1.f;
	}
#endif
*/
	memset(mStencilBuffer, 0, mWidth * mHeight * sizeof(unsigned char));

	CheckError();
}

void SoftwareDevice::End()
{
	// blit pixels to screen
	glDrawPixels(mWidth, mHeight, GL_RGBA, GL_UNSIGNED_BYTE, mColourBuffer);

	glDepthMask(GL_TRUE); // hack, needs to be enabled for the clear to work 0.o driver bug?

#ifdef WIN32
	SwapBuffers(mHDC);
#else
    glXSwapBuffers(mWindow->mDisplay, mWindow->mWindow);
#endif
}

void SoftwareDevice::ResetDevice(int width, int height)
{
    // should this go in the camera code also?
	glViewport(0, 0, width, height);

	// do we need to resize the frame buffer
	if (mWidth == width && height == height)
		return;

	mWidth = width;
	mHeight = height;

	if (mColourBuffer)
		_aligned_free(mColourBuffer);

	if (mDepthBuffer)
		_aligned_free(mDepthBuffer);

	if (mStencilBuffer)
		_aligned_free(mStencilBuffer);

	mColourBuffer = (unsigned char*)_aligned_malloc(4 * width * height * sizeof(unsigned char), 16); //new unsigned char[4 * width * height];
	mDepthBuffer = (float*)_aligned_malloc(width * height * sizeof(float), 16); //new float[width * height];
	mStencilBuffer = (unsigned char*)_aligned_malloc(width * height * sizeof(unsigned char), 16); //new unsigned char[width * height];
}

RenderBuffer* SoftwareDevice::CreateRenderBuffer()
{
	RenderBuffer* buff = new SoftwareRenderBuffer;
	mRenderBuffer.push_back(buff);
	return buff;
}

void SoftwareDevice::DestroyRenderBuffer(RenderBuffer** buffer)
{
	if (*buffer == 0)
		return;

	(*buffer)->Deinit();
	mRenderBuffer.remove(*buffer);

	delete *buffer;
	*buffer = 0;
}

void SoftwareDevice::CheckError()
{
	GLenum err = glGetError();
    while (err != GL_NO_ERROR)
	{
		Log::Error("OGL Error: %s", (char *)gluErrorString(err));
        err = glGetError();
    }
}

Window* SoftwareDevice::GetWindow()
{
	return (Window*)mWindow;
}

Vector4	SoftwareDevice::GetClearColour()
{
	return Vector4(mClearColour[0], mClearColour[1], mClearColour[2], mClearColour[3]) / 255.f;
}

void SoftwareDevice::SetClearColour(const Vector4& colour)
{
	mClearColour[0] = colour.x * 255.f;
	mClearColour[1] = colour.y * 255.f;
	mClearColour[2] = colour.z * 255.f;
	mClearColour[3] = colour.w * 255.f;
}

DWORD WINAPI Test(LPVOID ptr)
{
	return 0;
}

//void SoftwareDevice::Draw(int instanceCount, Material* material, Geometry* geometry, int vertexFormat, unsigned int startIndex, unsigned int numIndices)
bool SoftwareDevice::Draw(Geometry* geometry, unsigned int startIndex, unsigned int numIndices, int instanceCount)
{
	gThreadPool.Wait();

	// calculate how much memory we need in the memory pool and allocate it
	unsigned int requiredMemory = 0;
	SoftwareEffect* effect = mCurrentEffect;
	SoftwareVertexShader* vertexShader = effect->GetVertexShader();
	SoftwarePixelShader* pixelShader = effect->GetPixelShader();
	ParameterData* parameterData = effect->GetParameterData();

	requiredMemory += vertexShader->GetVertexInputSize() * 3;
	requiredMemory += pixelShader->GetPixelInputSize() * (3 + (2 * cMaxClipList + 6));
	requiredMemory *= gThreadPool.GetNumCPULogicalCores();
	IncreaseMemoryPoolSize(requiredMemory);

#ifdef MULTITHREAD
	RenderBuffer* indexBuffer = geometry->GetIndexBuffer();
	unsigned int indexCount = (numIndices == -1) ? indexBuffer->GetCount() : numIndices;
	unsigned int numTasks = gTaskMgr.GetNumCPULogicalCores();

	unsigned int taskStartIndex = startIndex;
	unsigned int taskIndexCount = indexCount / numTasks;
	unsigned int taskMemorySize = requiredMemory / gTaskMgr.GetNumCPULogicalCores();
	
	DrawTask* tasks = new DrawTask[numTasks];
	int i = 0;
	for (; i < (numTasks - 1); ++i)
	{
		DrawTask* pTask = &tasks[i];
		pTask->device = this;
		pTask->instanceCount = instanceCount;
		pTask->effect = mCurrentEffect;
		pTask->geometry = geometry;
		pTask->vertexFormat = vertexFormat;
		pTask->startIndex = taskStartIndex;
		pTask->numIndices = taskIndexCount;
		pTask->memory = mMemoryPool + (taskMemorySize * i);
		gTaskMgr.AddTask(pTask);

		taskStartIndex += taskIndexCount;
	}

	// add the task task
	DrawTask* pTask = &tasks[i];
	pTask->device = this;
	pTask->instanceCount = instanceCount;
	pTask->effect = mCurrentEffect;
	pTask->geometry = geometry;
	pTask->vertexFormat = vertexFormat;
	pTask->startIndex = taskStartIndex;
	pTask->numIndices = indexCount - taskIndexCount;
	pTask->memory = mMemoryPool + (taskMemorySize * i);
	gTaskMgr.AddTask(pTask);

	taskStartIndex += taskIndexCount;

	gTaskMgr.Wait();
	delete[] tasks;
#else
	DrawInternal(mCurrentEffect, geometry, startIndex, numIndices, instanceCount, mMemoryPool);
#endif
/*
	DrawTask task;
	DrawTask* pTask = &task;
	pTask->device = this;
	pTask->instanceCount = instanceCount;
	pTask->material = material;
	pTask->geometry = geometry;
	pTask->vertexFormat = vertexFormat;
	pTask->startIndex = startIndex;
	pTask->numIndices = numIndices;

	gTaskMgr.AddTask(pTask);
	gTaskMgr.Wait();*/
	return true;
}

//void SoftwareDevice::DrawInternal(int instanceCount, Material* material, Geometry* geometry, int vertexFormat, unsigned int startIndex, unsigned int numIndices, unsigned char* memory)
void SoftwareDevice::DrawInternal(SoftwareEffect* effect, Geometry* geometry, unsigned int startIndex, unsigned int numIndices, int instanceCount, unsigned char* memory)
{
	// get current pixel and vertex shader from effect
	SoftwareVertexShader* vertexShader = effect->GetVertexShader();
	SoftwarePixelShader* pixelShader = effect->GetPixelShader();
	ParameterData* parameterData = effect->GetParameterData();

	// iterate over all vertices and get them processed
	RenderBuffer* indexBuffer = geometry->GetIndexBuffer();
	VertexFrame& frame = geometry->GetVertexFrame();

	RenderBufferLock indexLock;
	indexBuffer->Lock(indexLock);
	indexLock.uintData += startIndex; // offset for start index

	RenderBufferLock positionLock;
	frame.mPosition->Lock(positionLock);

	RenderBufferLock colourLock;
	if (frame.mColour)
		frame.mColour->Lock(colourLock);

	RenderBufferLock texCoordLock;
	if (frame.mTexCoord[0])
		frame.mTexCoord[0]->Lock(texCoordLock);

	unsigned int indexCount = (numIndices == -1) ? indexBuffer->GetCount() : numIndices;
	unsigned int pixelInputSize = pixelShader->GetPixelInputSize();

	VertexInput* vertexInput[3];
	PixelInput* pixelInput[3];
	for (int i = 0; i < 3; ++i)
	{
		vertexInput[i] = (VertexInput*)memory;
		memory += vertexShader->GetVertexInputSize();

		pixelInput[i] = (PixelInput*)memory;
		memory += pixelInputSize;
	}

	PixelInput* clipList[2][cMaxClipList];
	for (int i = 0; i < cMaxClipList; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			clipList[j][i] = (PixelInput*)memory;
			memory += pixelInputSize;
		}
	}

	PixelInput* interpolators[6];
	for (int i = 0; i < 6; ++i)
	{
		interpolators[i] = (PixelInput*)memory;
		memory += pixelInputSize;
	}
/*
	for (int i = 0; i < 3; ++i)
	{
		vertexInput[i] = vertexShader->CreateVertexInput();

		#ifdef WIN32
            pixelInput[i] = (PixelInput*)_aligned_malloc(pixelInputSize, 16);
        #else
            pixelInput[i] = (PixelInput*)_mm_malloc(pixelInputSize, 16);
        #endif
	}

	PixelInput* clipList[2][cMaxClipList]; // clipper can generate at most 4 points to generate 2 tris
	for (int i = 0; i < cMaxClipList; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			#ifdef WIN32
				clipList[j][i] = (PixelInput*)_aligned_malloc(pixelInputSize, 16);
			#else
				clipList[j][i] = (PixelInput*)_mm_malloc(pixelInputSize, 16);
			#endif
		}
	}

	PixelInput* interpolators[6];
	for (int i = 0; i < 6; ++i)
	{
	    #ifdef WIN32
            interpolators[i] = (PixelInput*)_aligned_malloc(pixelInputSize, 16);
        #else
            interpolators[i] = (PixelInput*)_mm_malloc(pixelInputSize, 16);
        #endif
	}
*/

	VertexInputLayout* vertexInputLayout = vertexShader->GetVertexInputLayout();

	// this can be sped up using a T&L cache like what the gpu does :D
	for (unsigned int i = 0; i < indexCount; i += 3)
	{
		// draw first poly only
		//if (i > 0)
		//	return;

		unsigned int idx[3];
		idx[0] = indexLock.uintData[i];
		idx[1] = indexLock.uintData[i+1];
		idx[2] = indexLock.uintData[i+2];

		for (int v = 0; v < 3; ++v)
		{
			VertexInput* vInput = vertexInput[v];
			PixelInput* pInput = pixelInput[v];

			// set up the structure
			int l = 0;
			while (vertexInputLayout[l].dataType != VF_Unknown)
			{
				unsigned char* data = (unsigned char*)vInput + vertexInputLayout[l].offset;
				switch (vertexInputLayout[l].dataType)
				{
				case VF_Position:
					{
						Vector4* position = (Vector4*)((unsigned char*)vInput + vertexInputLayout[l].offset);
						*position = positionLock.vector4Data[idx[v]]; // frame.mPosition->GetVector4()[idx[v]];
					}
					break;

				case VF_Colour:
					{
						Vector4* colour = (Vector4*)((unsigned char*)vInput + vertexInputLayout[l].offset);

						if (frame.mColour)
							*colour = colourLock.vector4Data[idx[v]];
						else
							*colour = Vector4(0.f, 0.f, 0.f, 0.f);
					}
					break;

				case VF_TexCoord0:
					{
						Vector4* texCoord = (Vector4*)((unsigned char*)vInput + vertexInputLayout[l].offset);

						if (frame.mTexCoord[0])
							*texCoord = texCoordLock.vector4Data[idx[v]];
						else
							*texCoord = Vector4(0.f, 0.f);
					}
					break;
				}
				++l;
			}

			// apply vertex shader
			vertexShader->Apply(parameterData, vInput, pInput);

			// do the appropriate divide for position
			pInput->position /= Math::Abs(pInput->position.w); // abs fixed projections through the near plane
			//pInput->position.x /= pInput->position.w;
			//pInput->position.y /= pInput->position.w;
			//pInput->position.z /= Math::Abs(pInput->position.w);
		}

		// apply backface culling (clockwise faces are discarded)
		Vector4 vec0to1 = pixelInput[1]->position - pixelInput[0]->position;
		Vector4 vec0to2 = pixelInput[2]->position - pixelInput[0]->position;
		Vector4 faceNormal = vec0to1.Cross(vec0to2);

		switch (mCullFace)
		{
		case CF_Back:
			{
				if (faceNormal.z < 0.f)
					continue;
			}
			break;

		case CF_Front:
			{
				if (faceNormal.z > 0.f)
					continue;
			}
			break;

		case CF_None:
			break;
		}

		// check if clipping needs to be applied and do it if reuqired!
		// at most this should generate 1 extra vert, other verts will need to be adjusted how ever
		// owch this could be a killer!

		bool clipLeft = pixelInput[0]->position.x <= -1.f || pixelInput[1]->position.x <= -1.f || pixelInput[2]->position.x <= -1.f;
		bool clipRight = pixelInput[0]->position.x >= 1.f || pixelInput[1]->position.x >= 1.f || pixelInput[2]->position.x >= 1.f;

		bool clipTop = pixelInput[0]->position.y >= 1.f || pixelInput[1]->position.y >= 1.f || pixelInput[2]->position.y >= 1.f;
		bool clipBottom = pixelInput[0]->position.y <= -1.f || pixelInput[1]->position.y <= -1.f || pixelInput[2]->position.y <= -1.f;

		bool clipBack = pixelInput[0]->position.z >= 1.f || pixelInput[1]->position.z >= 1.f || pixelInput[2]->position.z >= 1.f;
		bool clipFront = pixelInput[0]->position.z <= -1.f || pixelInput[1]->position.z <= -1.f || pixelInput[2]->position.z <= -1.f;

		if (clipLeft || clipRight || clipTop || clipBottom || clipFront || clipBack)
		{			
			// we keep toggling clip lists
			unsigned int clipListSize[2];
			int curClipList = 0;

			// populate clip list with origonal triangle
			CopyInterpolant(pixelInputSize, pixelInput[0], clipList[0][0]);
			CopyInterpolant(pixelInputSize, pixelInput[1], clipList[0][1]);
			CopyInterpolant(pixelInputSize, pixelInput[2], clipList[0][2]);
			clipListSize[0] = 3;

			if (clipLeft)
			{
				// first clip list is generated from pixelInput, this is the origonal triangle
				Plane leftClipPlane(Vector4(-1.f, 0.f, 0.f), Vector4(1.f, 0.f, 0.f));
				curClipList = !curClipList;
				Clip(pixelInputSize, leftClipPlane, clipList[!curClipList], clipListSize[!curClipList], clipList[curClipList], &clipListSize[curClipList]);			
			}

			if (clipRight)
			{
				Plane rightClipPlane(Vector4(1.f, 0.f, 0.f), Vector4(-1.f, 0.f, 0.f));
				curClipList = !curClipList;
				Clip(pixelInputSize, rightClipPlane, clipList[!curClipList], clipListSize[!curClipList], clipList[curClipList], &clipListSize[curClipList]);				
			}

			if (clipBottom)
			{
				Plane bottomClipPlane(Vector4(0.f, -1.f, 0.f), Vector4(0.f, 1.f, 0.f));
				curClipList = !curClipList;
				Clip(pixelInputSize, bottomClipPlane, clipList[!curClipList], clipListSize[!curClipList], clipList[curClipList], &clipListSize[curClipList]);					
			}

			if (clipTop)
			{
				Plane topClipPlane(Vector4(0.f, 1.f, 0.f), Vector4(0.f, -1.f, 0.f));
				curClipList = !curClipList;
				Clip(pixelInputSize, topClipPlane, clipList[!curClipList], clipListSize[!curClipList], clipList[curClipList], &clipListSize[curClipList]);				
			}

			if (clipFront)
			{
				Plane frontClipPlane(Vector4(0.f, 0.f, -1.f), Vector4(0.f, 0.f, 1.f));
				curClipList = !curClipList;
				Clip(pixelInputSize, frontClipPlane, clipList[!curClipList], clipListSize[!curClipList], clipList[curClipList], &clipListSize[curClipList]);				
			}

			if (clipBack)
			{
				Plane backClipPlane(Vector4(0.f, 0.f, 1.f), Vector4(0.f, 0.f, -1.f));
				curClipList = !curClipList;
				Clip(pixelInputSize, backClipPlane, clipList[!curClipList], clipListSize[!curClipList], clipList[curClipList], &clipListSize[curClipList]);				
			}

			// poly fully clipped
			if (clipListSize[curClipList] < 3)
				continue;

			// make sure our clipper is working correctly
#ifdef _DEBUG
			for (unsigned int i = 0; i < (clipListSize[curClipList] - 1); ++i)
			{
				PixelInput* pInput = pixelInput[0];
				if (Math::Abs(pInput->position.x) > 1.f
					|| Math::Abs(pInput->position.y) > 1.f
					|| Math::Abs(pInput->position.z) > 1.f)
				{
					//Assert(false);
					int nothing = 0;
				}
			}
#endif

			pixelShader->SetPixelShader(this);

			// first vert of all tris is always vert 0
			// we fan out to all verts from vert 0
			CopyInterpolant(pixelInputSize, clipList[curClipList][0], pixelInput[0]);

			// clamp & convert to texture space
			PixelInput* pInput = pixelInput[0];
			pInput->position.x = Math::Clamp(-1.f, pInput->position.x, 1.f);
			pInput->position.y = Math::Clamp(-1.f, pInput->position.y, 1.f);

			pInput->position.x = (pInput->position.x * 0.5f + 0.5f) * float(mWidth - 1);
			pInput->position.y = (pInput->position.y * 0.5f + 0.5f) * float(mHeight - 1);

			for (unsigned int i = 1; i < (clipListSize[curClipList] - 1); ++i)
			{				
				CopyInterpolant(pixelInputSize, clipList[curClipList][i], pixelInput[1]);
				CopyInterpolant(pixelInputSize, clipList[curClipList][i + 1], pixelInput[2]);

				// clamp & convert to texture space
				for (int v = 1; v < 3; ++v)
				{
					PixelInput* pInput = pixelInput[v];
					pInput->position.x = Math::Clamp(-1.f, pInput->position.x, 1.f);
					pInput->position.y = Math::Clamp(-1.f, pInput->position.y, 1.f);

					pInput->position.x = (pInput->position.x * 0.5f + 0.5f) * float(mWidth - 1);
					pInput->position.y = (pInput->position.y * 0.5f + 0.5f) * float(mHeight - 1);
				}

				(this->*mRasterizeFunction)(pixelInput, interpolators, pixelInputSize, pixelShader, parameterData);
			}
		}
		else
		{
			// convert to texture space
			for (int v = 0; v < 3; ++v)
			{
				PixelInput* pInput = pixelInput[v];
				pInput->position.x = (pInput->position.x * 0.5f + 0.5f) * float(mWidth - 1);
				pInput->position.y = (pInput->position.y * 0.5f + 0.5f) * float(mHeight - 1);
			}

			pixelShader->SetPixelShader(this);
			(this->*mRasterizeFunction)(pixelInput, interpolators, pixelInputSize, pixelShader, parameterData);
		}
	}

/*
	// release vertex structures
	for (int i = 0; i < 3; ++i)
	{
		vertexShader->ReleaseVertexInput(&vertexInput[i]);

		#ifdef WIN32
            _aligned_free(pixelInput[i]);
        #else
            _mm_free(pixelInput[i]);
        #endif
	}

	// release clip list
	for (int i = 0; i < cMaxClipList; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			#ifdef WIN32
				_aligned_free(clipList[j][i]);
			#else
				_mm_free(clipList[j][i]);
			#endif
		}
	}

	// release interpolators
	for (int i = 0; i < 6; ++i)
	{
	    #ifdef WIN32
            _aligned_free(interpolators[i]);
		#else
            _mm_free(interpolators[i]);
        #endif
	}
*/
	indexBuffer->Unlock(indexLock);
	frame.mPosition->Unlock(positionLock);

	if (frame.mColour)
		frame.mColour->Unlock(colourLock);

	if (frame.mTexCoord[0])
		frame.mTexCoord[0]->Unlock(texCoordLock);

	Device::Draw(geometry, startIndex, indexCount, instanceCount);
}

void SoftwareDevice::Clip(unsigned int pixelInputSize, const Plane& clipPlane, PixelInput** verts, unsigned int numVerts, PixelInput** clippedVerts, unsigned int* clippedNumVerts)
{
	unsigned int newClippedNumVerts = 0;

	// for each vert, check which side of the plane it lies on,
	// if a line segment intersects the plane, we need to clip it
	for (unsigned int i = 0; i < numVerts; ++i)
	{
		// need to know if the first vert is inside the clipd region or outside
		bool insideOrOnBorder = clipPlane.DistanceToPoint(verts[i]->position) >= 0.f;

		// vert is inside or on the border, so simply add this vert to the list, no need to clip
		if (insideOrOnBorder)
		{
			CopyInterpolant(pixelInputSize, verts[i], clippedVerts[newClippedNumVerts]);
			++newClippedNumVerts;
		}
		
		//if (!insideOrOnBorder || i == (numVerts - 1))
		{	
			int nextVert = (i + 1) % numVerts;

			Vector4 edgeDir = (verts[nextVert]->position - verts[i]->position);
			float edgeLen = edgeDir.Normalize3();
			Ray edge(verts[i]->position, edgeDir);
			float intersectTime = clipPlane.Intersect(edge);
			if (intersectTime > 0.f && intersectTime < edgeLen)
			{
				float percent = intersectTime / edgeLen;
/*
				// this pushes the vert slightly outside of the screen edge, it will get clamped later on
				// this stops edge flickering
				if (insideOrOnBorder)
					percent *= 1.000001f;
				else
					percent *= 0.999999f;
*/
				//percent = 0.5f; // testing


				InterpInterpolant(pixelInputSize, verts[i], verts[nextVert], clippedVerts[newClippedNumVerts], percent);
				++newClippedNumVerts;
			}
			else
			{
				int wtf = 0; // what does this mean?
			}
		}
	}

	*clippedNumVerts = newClippedNumVerts;
}

bool SoftwareDevice::SetDeviceState(const DeviceState& state, bool force)			
{ 
	mCullFace = state.cullFace;
	mDeviceState = state;
	return true; 
}

bool SoftwareDevice::GetDeviceState(DeviceState& state)
{ 
	state = mDeviceState;
	return true; 
}