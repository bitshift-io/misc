#ifndef _RENDERTEXTURE_H_
#define _RENDERTEXTURE_H_

#include "SceneObject.h"

class Texture;

enum RenderTextureFlags
{
	RTF_None			= 0x0,
	RTF_Depth			= 0x1 << 1,
	RTF_CubeMap			= 0x1 << 2,
	RTF_Stencil			= 0x1 << 3,
	RTF_Colour			= 0x1 << 4,
	RTF_Multisample		= 0x1 << 5,
	RTF_LowDynamicRange = 0x1 << 6,
	RTF_Read			= 0x1 << 7, // cpu read write access
	RTF_Write			= 0x1 << 8, // cpu read write access
};

//
// A render target with a number of textures to render to
//
class RenderTexture : public SceneObject
{
public:

	TYPE(6)

	virtual bool		Save(const File& out, SceneNode* node = 0)									{ return true; }
	virtual bool		Load(const ObjectLoadParams* params, SceneNode* node = 0)					{ return true; }

	virtual bool		Init(Device* device, RenderFactory* factory, int width, int height, unsigned int flags, int targetCount = 1)	= 0;
	virtual void		Deinit()												= 0;

	virtual int			Begin()													= 0;
	virtual void		End()													= 0;

	virtual void		SetViewport(const Vector4& topLeft, const Vector4& bottomRight, const Vector4& nearFarDepth = Vector4(0.f, 1.f))	= 0;
	virtual void		BindTarget(int index, bool clear = true, int flags = RTF_Colour | RTF_Depth | RTF_Stencil)				= 0;
	virtual void		SetClearColour(const Vector4& colour)					= 0;
	virtual void		SetClearDepth(float depth)								= 0;
	virtual void		SetClearStencil(unsigned int stencil)					= 0;

	virtual Texture*	GetTexture(RenderTextureFlags type, int lockFlags = 0)	= 0;

	virtual int			GetWidth()												= 0;
	virtual int			GetHeight()												= 0;
};

#endif
