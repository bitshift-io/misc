#ifndef _EXP_RENDERBUFFER_H_
#define _EXP_RENDERBUFFER_H_

#include <vector>
#include "../RenderBuffer.h"
#include "../../Math/Vector.h"

using namespace std;

class File;
class Device;

class ExpRenderBuffer : public RenderBuffer
{
public:

	ExpRenderBuffer();

	virtual void	Deinit()																	{}

	virtual bool	Save(const File& out);
	virtual bool	Load(Device* device, const File& in)		{	return true; }

	virtual void	Bind(VertexFormat channel, int index = 0)	{}
	virtual void	Unbind()									{}

	virtual bool	Lock(RenderBufferLock& lock, int lockFlags = 0)					{ return false; }
	virtual void	Unlock(RenderBufferLock& lock)									{}

	void			Insert(unsigned int value);
	void			Insert(const Vector4& value);

	Vector4&				GetVector4(int idx);
	unsigned int			GetUnsignedInt(int idx);
	
	virtual unsigned int	GetSize();
	virtual void			ReloadResource(Device* device)		{}

	virtual RenderBufferUseage	GetUseage()						{ return RBU_Dynamic; }
	virtual void				Draw(unsigned int startIndex = 0, unsigned int numIndices = -1)		{}

protected:

	void					IncreaseSize(unsigned int size);

	void*					mData;
	unsigned int			mMaxSize;
	unsigned int			mSize;
};

#endif