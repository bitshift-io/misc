#include "ExpRenderFactory.h"
//#include "ExpGeometry.h"
#include "ExpDevice.h"

ExpRenderFactory::ExpRenderFactory(const RenderFactoryParameter& parameter) : RenderFactory(parameter)
{
	//Register(ObjectCreator<ExpGeometry>, Geometry::Type);
}

void ExpRenderFactory::Init()
{
	//Register(ObjectCreator<ExpGeometry>, Geometry::Type);
}

void ExpRenderFactory::Deinit()
{

}

Window* ExpRenderFactory::CreateWindow()
{
	return 0;
}

Device*	ExpRenderFactory::CreateDevice()
{
	return new ExpDevice;
}

void ExpRenderFactory::ReleaseDevice(Device** device)
{
	if (!*device)
		return;

	ExpDevice* dev = static_cast<ExpDevice*>(*device);
	delete dev;
	*device = 0;
}
