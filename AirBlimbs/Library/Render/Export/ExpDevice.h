#ifndef _EXP_DEVICE_H_
#define _EXP_DEVICE_H_

#include "../Device.h"

class Window;

class ExpDevice : public Device
{
public:

	virtual bool			Init(RenderFactory* factory, Window* window, int multisample = 0)				{ return true; }
	virtual void			Deinit(RenderFactory* factory, Window* window)									{}

	virtual void			Begin()																			{}
	virtual void			End()																			{}

	virtual int				GetMultisample()																{ return 0; }

	virtual bool			Draw(Geometry* geometry, unsigned int startIndex = 0, unsigned int numIndices = -1, int instanceCount = DT_NotInstanced)	{ return false; }
	virtual void			SetMatrix(const Matrix4& matrix, MatrixMode mode)								{}

	virtual RenderBuffer*	CreateRenderBuffer();
	virtual void			DestroyRenderBuffer(RenderBuffer** buffer);

	virtual void			DrawPoint(const Vector4& p, const Vector4& colour)								{}
	virtual void			DrawLine(const Vector4& p1, const Vector4& p2, const Vector4& colour)			{}

	virtual Window*			GetWindow()																		{ return 0; }

	virtual bool			SetDeviceState(const DeviceState& state, bool force = false)					{ return false; }
	virtual bool			GetDeviceState(DeviceState& state)												{ return false; }

	virtual void			SetGeometryTopology(GeometryTopology topology)									{}

	virtual void			DrawStencil()																	{}
	virtual void			SaveStencil()																	{}
	virtual void			ReloadResource()																{}

	virtual void			SetClearColour(const Vector4& colour)											{}
	virtual Vector4			GetClearColour()																{ return Vector4(0.f, 0.f, 0.f, 0.f); }

};

#endif