#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include "TextureResource.h"
#include "SceneObject.h"

class Device;

enum TextureFlags
{
	TF_None				= 0x0,
	TF_2D				= 0x1 << 1,
	TF_CubeMap			= 0x1 << 2,
	TF_3D				= 0x1 << 3,
	TF_GenerateMipmap	= 0x1 << 4,
	TF_LowDynamicRange	= 0x1 << 5,
	TF_VertexTexture	= 0x1 << 6,
	TF_Read				= 0x1 << 7, // cpu read write access
	TF_Write			= 0x1 << 8, // cpu read write access
};

class Texture : public SceneObject
{
public:

	TYPE(4)

	virtual bool	Save(const File& out, SceneNode* node = 0)													{ return true; }

	virtual bool	Init(Device* device, unsigned char* image, int channels, int width, int height, int depth, int flags = TF_2D | TF_LowDynamicRange) = 0;
	//virtual bool	Update(unsigned char* image) = 0; // modify the texture

	virtual bool	Lock(TextureSubresource& resource, int lockFlags) = 0;
	virtual void	Unlock() = 0;

	virtual bool	GetSubresource(TextureSubresource& resource) = 0;

protected:

};

#endif
