#include "Geometry.h"
#include "RenderBuffer.h"
#include "Device.h"
#include "File/Log.h"

//=========================================================================
// VertexFrame
//=========================================================================
VertexFrame::VertexFrame() :
	mPosition(0),
	mNormal(0),
	mTangent(0),
	mBinormal(0),
	mColour(0),
	mBoneWeight(0),
	mBoneIndex(0)
{
	for (int i = 0; i < MAX_TEXCOORD; ++i)
		mTexCoord[i] = 0;
}

int VertexFrame::GetVertexFormat() const
{
	int format = 0;

	if (mPosition)			format |= VF_Position;
	if (mNormal)			format |= VF_Normal;
	if (mTangent)			format |= VF_Tangent;
	if (mBinormal)			format |= VF_Binormal;
	if (mColour)			format |= VF_Colour;

	if (mBoneWeight || mBoneIndex)		
		format |= VF_BoneData;

	int texCoordCount = 0;
	for (int i = 0; i < MAX_TEXCOORD; ++i)
	{
		if (mTexCoord[i] != 0)
		{
			format |= (VF_TexCoord0 << texCoordCount);
			++texCoordCount;
		}
	}

	return format;
}

bool VertexFrame::Save(const File& out)
{
	// write the vertex format
	int format = GetVertexFormat();
	out.Write(&format, sizeof(int));

	// write the buffers
	if (mPosition)	mPosition->Save(out);
	if (mNormal)	mNormal->Save(out);
	if (mTangent)	mTangent->Save(out);
	if (mBinormal)	mBinormal->Save(out);
	if (mColour)	mColour->Save(out);
	if (mBoneWeight)mBoneWeight->Save(out);
	if (mBoneIndex)	mBoneIndex->Save(out);

	for (int i = 0; i < MAX_TEXCOORD; ++i)
	{
		if (mTexCoord[i] != 0)
			mTexCoord[i]->Save(out);
	}

	return true;
}

bool VertexFrame::Load(Device* device, const File& in)
{
	// read the vertex format
	int format = 0;
	in.Read(&format, sizeof(int));

	// init with the vertex format
	Init(device, format);

	// load in the buffers
	if (mPosition)	mPosition->Load(device, in);
	if (mNormal)	mNormal->Load(device, in);
	if (mTangent)	mTangent->Load(device, in);
	if (mBinormal)	mBinormal->Load(device, in);
	if (mColour)	mColour->Load(device, in);
	if (mBoneWeight)mBoneWeight->Load(device, in);
	if (mBoneIndex)	mBoneIndex->Load(device, in);

	for (int i = 0; i < MAX_TEXCOORD; ++i)
	{
		if (mTexCoord[i] != 0)
			mTexCoord[i]->Load(device, in);
	}

	return true;
}

void VertexFrame::Init(Device* device, int vertexFormat, int numVerts, RenderBufferUseage useage)
{
	Deinit(device);

	if (vertexFormat & VF_Position)	
	{
		mPosition = device->CreateRenderBuffer();
		mPosition->Init(RBT_Vector4, numVerts, useage);
	}

	if (vertexFormat & VF_Normal)	
	{
		mNormal = device->CreateRenderBuffer();
		mNormal->Init(RBT_Vector4, numVerts, useage);
	}

	if (vertexFormat & VF_Tangent)	
	{
		mTangent = device->CreateRenderBuffer();
		mTangent->Init(RBT_Vector4, numVerts, useage);
	}

	if (vertexFormat & VF_Binormal)
	{
		mBinormal = device->CreateRenderBuffer();
		mBinormal->Init(RBT_Vector4, numVerts, useage);
	}

	if (vertexFormat & VF_Colour)	
	{
		mColour = device->CreateRenderBuffer();
		mColour->Init(RBT_Vector4, numVerts, useage);
	}

	if (vertexFormat & VF_BoneData)
	{
		mBoneWeight = device->CreateRenderBuffer();
		mBoneWeight->Init(RBT_Vector4, numVerts, useage);
		mBoneIndex = device->CreateRenderBuffer();
		mBoneIndex->Init(RBT_Vector4, numVerts, useage);
	}

	int texCoordCount = 0;
	for (int i = 0; i < MAX_TEXCOORD; ++i)
	{
		if (vertexFormat & (VF_TexCoord0 << i))	
		{
			RenderBuffer* renderBuffer = device->CreateRenderBuffer();
			renderBuffer->Init(RBT_Vector4, numVerts, useage);
			mTexCoord[texCoordCount] = renderBuffer; 			
			++texCoordCount;
		}
	}	
}

void VertexFrame::Deinit(Device* device)
{
	device->DestroyRenderBuffer(&mPosition);
	device->DestroyRenderBuffer(&mNormal);
	device->DestroyRenderBuffer(&mTangent);
	device->DestroyRenderBuffer(&mBinormal);
	device->DestroyRenderBuffer(&mColour);
	device->DestroyRenderBuffer(&mBoneWeight);
	device->DestroyRenderBuffer(&mBoneIndex);

	for (int i = 0; i < MAX_TEXCOORD; ++i)
		device->DestroyRenderBuffer(&mTexCoord[i]);
}

//=========================================================================
// Geometry
//=========================================================================
void Geometry::Destroy(RenderFactory* factory)
{
	mDevice->DestroyRenderBuffer(&mIndex);

	for (unsigned int i = 0; i < mVertexFrame.size(); ++i)
		mVertexFrame[i].Deinit(mDevice);

	mVertexFrame.clear();
	std::vector<VertexFrame>(mVertexFrame).swap(mVertexFrame);
}

bool Geometry::Save(const File& out, SceneNode* node)
{
	mIndex->Save(out);

	// write size
	unsigned int size = mVertexFrame.size();
	out.Write(&size, sizeof(unsigned int));

	// write vertex frames
	for (unsigned int i = 0; i < size; ++i)
		mVertexFrame[i].Save(out);

	return true;
}

bool Geometry::Load(const ObjectLoadParams* params, SceneNode* node)
{
	File* in = params->mFile;

	mTopology = GT_TriangleList;
	mDevice = params->mDevice;
	mIndex = mDevice->CreateRenderBuffer();
	mIndex->Init(RBT_UnsignedInt);
	mIndex->Load(mDevice, *in);

	// read size
	unsigned int size = 0;
	in->Read(&size, sizeof(unsigned int));
	mVertexFrame.resize(size);

	// read vertex frames
	for (unsigned int i = 0; i < size; ++i)
		mVertexFrame[i].Load(mDevice, *in);

	return true;
}

void Geometry::Init(Device* device, unsigned int vertexFormat, unsigned int numVertexFrame)
{
	mTopology = GT_TriangleList;
	mDevice = device;
	mIndex = device->CreateRenderBuffer();
	mIndex->Init(RBT_UnsignedInt);
	mVertexFrame.resize(numVertexFrame);

	for (unsigned int i = 0; i < numVertexFrame; ++i)
		mVertexFrame[i].Init(device, vertexFormat);
}

void Geometry::Init(Device* device, unsigned int vertexFormat, unsigned int numVerts, unsigned int numIndices, RenderBufferUseage useage, unsigned int numVertexFrame)
{
	mTopology = GT_TriangleList;
	mDevice = device;
	mIndex = device->CreateRenderBuffer();
	mIndex->Init(RBT_UnsignedInt, numIndices, useage);
	mVertexFrame.resize(numVertexFrame);

	for (unsigned int i = 0; i < numVertexFrame; ++i)
		mVertexFrame[i].Init(device, vertexFormat, numVerts, useage);
}

unsigned int Geometry::GetVertexFrameCount() const
{
	return mVertexFrame.size();
}

VertexFrame& Geometry::GetVertexFrame(unsigned int idx)
{
	if (idx >= mVertexFrame.size())
	{
		Log::Error("[Geometry::GetVertexFrame] index '%i' is larger than number of vertex frames '%i', buffer overrun will occur", idx, mVertexFrame.size());
	}

	return mVertexFrame[idx];
}

void Geometry::Draw(unsigned int startIndex, unsigned int numIndices, int instanceCount)
{
	mDevice->Draw(this, startIndex, numIndices, instanceCount);
}
