#include "PhysicsReport.h"
#include "PhysicsFactory.h"
#include "PhysicsScene.h"

ContactInfoIterator::ContactInfoIterator(NxConstContactStream& stream) :
	mStreamIterator(stream),
	mEnd(-1)
{
	mStreamIterator.goNextPair();
	mStreamIterator.goNextPatch();
	mStreamIterator.goNextPoint();
	operator++();
}

ContactInfoIterator::ContactInfoIterator() :
	mStreamIterator(NxConstContactStream())
{
}

ContactInfo* ContactInfoIterator::operator*()
{
	if (mEnd == 0)
		return 0;

	return &mInfo;
}

ContactInfo* ContactInfoIterator::operator->()
{
	if (mEnd == 0)
		return 0;

	return &mInfo;
}

void ContactInfoIterator::operator++()
{
	if (mEnd == 0)
		return;

	--mEnd;
	mInfo.position = Vector4(mStreamIterator.getPoint().x, mStreamIterator.getPoint().y, mStreamIterator.getPoint().z);
	mInfo.normal = Vector4(mStreamIterator.getPatchNormal().x, mStreamIterator.getPatchNormal().y, mStreamIterator.getPatchNormal().z);

	if (!mStreamIterator.goNextPoint())
	{
		if (!mStreamIterator.goNextPatch())
		{
			if (!mStreamIterator.goNextPair())
				mEnd = 1;
		}
	}
}





class InternalSweepHitReport : public NxUserEntityReport<NxSweepQueryHit>
{
public:

	virtual bool onEvent(NxU32 numHit, NxSweepQueryHit* hit)
	{
		SweepHit sweepHit;
		for (unsigned int i = 0; i < numHit; ++i)
		{
			sweepHit.body = (PhysicsBody*)hit[i].hitShape->getActor().userData;
			sweepHit.distance = hit[i].t;

			if (!mReport->OnSweepHit(sweepHit))
				return false;
		}

		return true;
	}

	CollisionReport* mReport;
};

class InternalRaycastReport : public NxUserRaycastReport
{
public:

	virtual bool onHit(const NxRaycastHit& hit)
	{
		RaycastHit rayHit;
		//NxActor* actortest = dynamic_cast<NxActor*>(hit.shape->getActor());
		//NxController* test = dynamic_cast<NxController*>(hit.shape->getActor());
		rayHit.body = (PhysicsBody*)hit.shape->getActor().userData;
		rayHit.distance = hit.distance;
		//rayHit.faceIdx = hit.
		return mReport->OnRaycastHit(rayHit);
	}

	CollisionReport* mReport;
};

int CollisionReport::Raycast(PhysicsScene* scene, const Vector4& point, const Vector4& direction, float distance, unsigned int group)
{
	InternalRaycastReport report;
	report.mReport = this;
	NxU32 count = scene->GetHandle()->raycastAllShapes(NxRay(point.vec, direction.vec), report, NX_ALL_SHAPES, group, distance == -1 ? NX_MAX_F32 : distance);
	return count;
}

int	CollisionReport::LinearSweepBody(PhysicsScene* scene, PhysicsBody* body, const Vector4& direction, float distance)
{/*
	InternalSweepHitReport report;
	report.mReport = this;
	NxU32 count = scene->GetHandle()->linearSweep();
	*/
	return -1;
}

int	CollisionReport::LinearSweepSphere(PhysicsScene* scene, const Vector4& position, float radius, const Vector4& direction, float distance, unsigned int group)
{
	// DIS NOT WORKING!
	InternalSweepHitReport report;
	report.mReport = this;
	Vector4 motion = direction * (distance == -1 ? NX_MAX_F32 : distance);
	NxU32 count = scene->GetHandle()->linearCapsuleSweep(NxCapsule(NxSegment(position.vec, position.vec), radius), NxVec3(motion.vec), 0, 0, 0, 0, &report, -1, NULL/*&group*/);
	return count;
}