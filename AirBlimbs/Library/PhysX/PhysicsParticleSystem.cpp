#include "PhysicsParticleSystem.h"
#include "PhysicsScene.h"
#include "NxSimpleTypes.h"
#include "NxBounds3.h"
#include "fluids/NxParticleData.h"
#include "fluids/NxParticleIdData.h"
#include "fluids/NxFluidDesc.h"
#include "NxScene.h"

// temp
#include "NxActorDesc.h" // temp
#include "NxBoxShapeDesc.h" // temp

bool gUseHW = false;

const  int              NUM_VERTS = 400000;

static NxU16            gIndices[NUM_VERTS];
static int              gWidth= 512;
static int              gHeight= 512;
static NxU32            gNumVertices;
static NxU32            gNumIndices;

static float            gParticleVertices[NUM_VERTS * 3];
static float            gParticleNormals[NUM_VERTS * 3];
static NxU16            gParticleIndices[NUM_VERTS];
static NxU32            gParticleNumVertices;
static NxU32            gParticleNumIndices;



#define REST_PARTICLES_PER_METER 10.f
#define KERNEL_RADIUS_MULTIPLIER 1.8f
//#define MOTION_LIMIT_MULTIPLIER (3*KERNEL_RADIUS_MULTIPLIER)
#define MOTION_LIMIT_MULTIPLIER 3
#define PACKET_SIZE_MULTIPLIER 8
#define COLLISION_DISTANCE_MULTIPLIER 0.18f


void PhysicsParticleEmitter::SetTransform(const Matrix4& transform)
{
	NxMat34 nxTransform;
	nxTransform.setColumnMajor44(transform.m);
	mEmitter->setGlobalPose(nxTransform);
}



void PhysicsParticleSystem::Init(PhysicsScene* scene)
{
	unsigned int maxParticle = 2103;
	mParticleBuffer = new PhysicsParticle[maxParticle];


	//Create a set of particles
	Vector4 pos(0,1.5,0);
	float distance = 0.1f;
	int gParticleBufferNum = 0;
	unsigned sideNum = 16;
	float rad = sideNum*distance*0.5f;
	for (unsigned i=0; i<sideNum; i++)
		for (unsigned j=0; j<sideNum; j++)
			for (unsigned k=0; k<sideNum; k++)
			{
				Vector4 p = Vector4(i*distance,j*distance,k*distance);
				if (p.Distance(Vector4(rad,rad,rad)) < rad)
				{
					p += pos;
					mParticleBuffer[gParticleBufferNum++].pos = p;
					mParticleBuffer[gParticleBufferNum].life = 0.0f;
				}
			}

	NxParticleData particles;
//	particles.maxParticles				= maxParticle;
	particles.numParticlesPtr			= &maxParticle;
	particles.bufferPos					= &mParticleBuffer[0].pos.x;
	particles.bufferDensity				= &mParticleBuffer[0].density;
	particles.bufferId					= &mParticleBuffer[0].id;
	particles.bufferLife				= &mParticleBuffer[0].life;
	particles.bufferPosByteStride		= sizeof(PhysicsParticle);
	particles.bufferDensityByteStride	= sizeof(PhysicsParticle);
	particles.bufferIdByteStride		= sizeof(PhysicsParticle);
	particles.bufferLifeByteStride		= sizeof(PhysicsParticle);

	//Set structure to receive IDs of created particles every simulation step
	NxParticleIdData createdIds;
//	createdIds.maxIds				= maxParticle;
	createdIds.numIdsPtr			= &maxParticle;
	createdIds.bufferId				= &mParticleBuffer[0].createdId;
	createdIds.bufferIdByteStride	= sizeof(PhysicsParticle);

	//Create a fluid descriptor
	NxFluidDesc fluidDesc;
//    fluidDesc.maxParticles                  = particles.maxParticles;
    fluidDesc.kernelRadiusMultiplier		= KERNEL_RADIUS_MULTIPLIER;
    fluidDesc.restParticlesPerMeter			= REST_PARTICLES_PER_METER;
	fluidDesc.motionLimitMultiplier			= MOTION_LIMIT_MULTIPLIER;
	fluidDesc.packetSizeMultiplier			= PACKET_SIZE_MULTIPLIER;
    fluidDesc.collisionDistanceMultiplier   = COLLISION_DISTANCE_MULTIPLIER ;
    fluidDesc.stiffness						= 50.0f;
    fluidDesc.viscosity						= 22.0f;
	fluidDesc.restDensity					= 1000.0f;
    fluidDesc.damping						= 0.0f;
//    fluidDesc.staticCollisionRestitution	= 0.4f;
//	fluidDesc.staticCollisionAdhesion		= 0.03f;
	fluidDesc.simulationMethod				= NX_F_SPH; //NX_F_NO_PARTICLE_INTERACTION;

	fluidDesc.initialParticleData			= particles;
	fluidDesc.particlesWriteData			= particles;
	fluidDesc.particleCreationIdWriteData   = createdIds;

	if (!gUseHW)
    {
        fluidDesc.flags &= ~NX_FF_HARDWARE;
        fluidDesc.flags |= (1 << 20);
    }

	//if (fluidDesc.maxParticles < gParticleBufferNum)
	//	gParticleBufferNum = fluidDesc.maxParticles;

	mFluid = scene->GetHandle()->createFluid(fluidDesc);
	assert(mFluid != NULL);

//	mFluid->removeAllParticles();
}

void PhysicsParticleSystem::Deinit()
{
	delete[] mParticleBuffer;
}

PhysicsParticleEmitter*	PhysicsParticleSystem::CreateParticleEmitter(const PhysicsParticleEmitterConstructor& constructor)
{
	NxFluidEmitterDesc desc;
	desc.maxParticles = constructor.mMaxParticles;
	desc.particleLifetime = constructor.mParticleLife;
	desc.rate = constructor.mRate;

	PhysicsParticleEmitter* emitter = new PhysicsParticleEmitter();
	emitter->mEmitter = mFluid->createEmitter(desc);
	return emitter;
}

void PhysicsParticleSystem::ReleaseParticleEmitter(PhysicsParticleEmitter** emitter)
{
	if (!*emitter)
		return;

	delete *emitter;
	*emitter = 0;
}
