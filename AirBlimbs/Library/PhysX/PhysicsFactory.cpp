#include "PhysicsFactory.h"
#include "PhysicsScene.h"
#include "PhysicsBody.h"
#include "PhysicsCloth.h"
#include "PhysicsJoint.h"
#include "PhysicsActorController.h"
#include "PhysicsParticleSystem.h"
#include "File/Log.h"

#pragma comment(lib, "NxCharacter.lib")
#pragma comment(lib, "NxCooking.lib")
#pragma comment(lib, "PhysXLoader.lib")

bool PhysicsFactory::Init()
{
	mPhysicsHandle = NxCreatePhysicsSDK(NX_PHYSICS_SDK_VERSION);
	if (!mPhysicsHandle)
	{
		Log::Error("Failed to get physx handle, Make sure your physx driver is installed\n");
		return false;
	}

	mPhysicsHandle->setParameter(NX_SKIN_WIDTH, 0.01f);
//	mPhysicsHandle->setParameter(NX_MIN_SEPARATION_FOR_PENALTY, -0.05f);

    // Set the debug visualization parameters
	mPhysicsHandle->setParameter(NX_VISUALIZATION_SCALE, 1.0f);
	mPhysicsHandle->setParameter(NX_VISUALIZE_COLLISION_SHAPES, 1.f);
    mPhysicsHandle->setParameter(NX_VISUALIZE_ACTOR_AXES, 1.f);
	return true;
}

void PhysicsFactory::Deinit()
{
	NxReleasePhysicsSDK(mPhysicsHandle);
}

PhysicsScene* PhysicsFactory::CreatePhysicsScene()
{
	PhysicsScene* scene = new PhysicsScene(this);
	return scene;
}

void PhysicsFactory::ReleasePhysicsScene(PhysicsScene** scene)
{
	delete *scene;
	*scene = 0;
}

PhysicsParticleSystem* PhysicsFactory::CreatePhysicsParticleSystem()
{
	return new PhysicsParticleSystem;
}

PhysicsJoint* PhysicsFactory::CreatePhysicsJoint()
{
	return new PhysicsJoint;
}

PhysicsBody* PhysicsFactory::CreatePhysicsBody()
{
	return new PhysicsBody;
}

PhysicsCloth* PhysicsFactory::CreatePhysicsCloth()
{
	return new PhysicsCloth;
}

PhysicsActorController*	PhysicsFactory::CreatePhsicsActorController()
{
	return new PhysicsActorController;
}

void PhysicsFactory::Update()
{
	mControllerMgr.updateControllers();
}
