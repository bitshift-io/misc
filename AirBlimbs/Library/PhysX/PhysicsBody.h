#ifndef _PHYSICSBODY_H_
#define _PHYSICSBODY_H_

#include "PhysicsFactory.h"
#include "PhysicsReport.h"
#include "Math/Vector.h"
#include "Math/Matrix.h"
#include <list>
#include <vector>

using namespace std;

class NxShapeDesc;

class TriMeshConstructor
{
public:

	void Init(unsigned int numIndex, unsigned int numPosition);
	void Deinit();

	unsigned int*	mIndex;
	Vector4*		mPosition;

	unsigned int	mIndexCount;
	unsigned int	mPositionCount;
};

class PhysicsBodyConstructor
{
	friend class PhysicsBody;

public:

	PhysicsBodyConstructor();
	~PhysicsBodyConstructor();

	bool		Load(const PhysicsFactory* factory, const string& name);

	void		AddTriMesh(const PhysicsFactory* factory, const TriMeshConstructor& meshData, int collisionGroup = 1, const Matrix4* local = 0);
	void		AddConvexMesh(const PhysicsFactory* factory, const TriMeshConstructor& meshData, int collisionGroup = 1, const Matrix4* local = 0);
	void		AddBox(const Vector4& dimensions, int collisionGroup = 1);
	void		AddPlane(int collisionGroup = 1);
	void		AddSphere(float radius, int collisionGroup = 1);
	void		AddCapsule(float radius, float height, int collisionGroup = 1);

	void		SetDynamicBody(float mass, float linearDamping = 0.f, float angularDamping = 0.f);
	void		SetTrigger();

protected:

	void		SetCollisionGroup(NxShapeDesc* shape, int collisionGroup);

	struct ShapeInfo
	{
		NxShapeDesc*	shapeDesc;
	};

	NxActorDesc			mActorDesc;
	NxBodyDesc			mBodyDesc;
	list<ShapeInfo>		mShapeInfo;
};

//
// A group of smaller primitives to create a body
//
class PhysicsBody
{
public:

	enum Space
	{
		World,
		Local,
	};

	PhysicsBody() : 
		mUserData(0),
		mContactReport(0),
		mActor(0)
	{
	}

	void		Init(PhysicsScene* scene, const PhysicsBodyConstructor& constructor);
	//void		Init(PhysicsScene* scene, PhysicsActorController* ctrl);

	void		ApplyLinearForce(const Vector4& force, Space space = World);
	void		ApplyAngularForce(const Vector4& force, Space space = World);

	void		ApplyLinearForce(const Vector4& force, const Vector4& point, Space forceSpace = World, Space pointSpace = World);
	void		ApplyAngularForce(const Vector4& force, const Vector4& point, Space forceSpace = World, Space pointSpace = World);

	void		SetLinearVelocity(const Vector4& velocity);
	void		SetAngularVelocity(const Vector4& velocity);

	Vector4		GetAngularVelocity();
	Vector4		GetLinearVelocity();
	Vector4		GetPointVelocity(const Vector4& point);

	void		SetPosition(const Vector4& position);
	Vector4		GetPosition();

	Matrix4		GetTransform();
	void		SetTransform(const Matrix4& transform);
	void		SetRotation(const Matrix4& transform);

	void		EnableGravity(bool enable);

	void		SetCollisionGroup(int collisionGroup);

	void		Enable();
	void		Disable();

	void		SetUserData(void* data)							{ mUserData = data; }
	void*		GetUserData()									{ return mUserData; }

	bool		IsDynamic();
	bool		IsKinematic();
	void		SetKinematic(bool kinematic);

	NxActor*	GetHandle()										{ return mActor; }

	void			SetContactReport(ContactReport* report);
	ContactReport*	GetContactReport()							{ return mContactReport; }

	virtual PhysicsActorController*	GetActorController()		{ return 0; }

protected:

	NxActor*		mActor;	// a group of bodys in physx speak :P
	void*			mUserData;
	ContactReport*	mContactReport;
};

#endif
