#ifndef _PHYSICSCLOTH_H_
#define _PHYSICSCLOTH_H_
/*
#include "NxSimpleTypes.h"
#include "Common/Stream.h"
#include "NxPhysics.h"
*/
#include "PhysicsFactory.h"
#include "PhysicsReport.h"
#include "PhysicsBody.h"
#include "Math/Vector.h"
#include "Math/Matrix.h"
#include "NxuStream2/NXU_Streaming.h"

using namespace NXU;
//class MemoryReadBuffer;

class PhysicsClothConstructor
{
	friend class PhysicsCloth;

public:

	PhysicsClothConstructor() :
	  mReadBuffer(0)
	{
	}

	~PhysicsClothConstructor();

	void			AddTriMesh(const PhysicsFactory* factory, const TriMeshConstructor& meshData, int collisionGroup = 1, const Matrix4* local = 0);
	void			SetProperties(float thickness = 0.2f);

protected:

	NxClothMeshDesc		mClothMeshDesc;
	NxClothDesc			mDesc;
	MemoryReadBuffer*	mReadBuffer;
};

class PhysicsCloth
{
public:

	void			Init(PhysicsScene* scene, const PhysicsClothConstructor& constructor);
	void			Deinit();
	void			AttachToBody(PhysicsBody* body);

protected:

	NxCloth*		mCloth;
	NxClothMesh*	mClothMesh;
	NxMeshData		mReceiveBuffers;
};

#endif
