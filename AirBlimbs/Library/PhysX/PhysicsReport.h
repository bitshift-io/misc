#ifndef _PHYSICS_REPORT_
#define _PHYSICS_REPORT_

#include "Math/Vector.h"
#include "NxUserContactReport.h"

class PhysicsBody;
class PhysicsScene;
class PhysicsActorController;

struct RaycastHit
{
	Vector4			worldPoint;
	Vector4			worldNormal;
	unsigned int	faceIdx;
	float			distance;
	Vector4			texCoord;
	unsigned short	materialIdx;
	PhysicsBody*	body;
};

struct SweepHit
{
	Vector4			worldPoint;
	Vector4			worldNormal;
	unsigned int	faceIdx;
	float			distance;
	PhysicsBody*	body;
};

//
// raycasts, sweeps etc...
//
class CollisionReport
{
public:

	// raycast collision check
	int				Raycast(PhysicsScene* scene, const Vector4& point, const Vector4& direction, float distance = -1, unsigned int group = -1);
	virtual bool	OnRaycastHit(const RaycastHit& hit) { return true; }

	// sweeps a body checking for collisions
	int				LinearSweepBody(PhysicsScene* scene, PhysicsBody* body, const Vector4& direction, float distance = -1);
	int				LinearSweepSphere(PhysicsScene* scene, const Vector4& position, float radius, const Vector4& direction, float distance = -1, unsigned int group = -1);

	virtual bool	OnSweepHit(const SweepHit& hit) { return true; }
};

enum ContactFlags
{
	CF_NotifyOnStartTouch	= 1 << 0,
	CF_NotifyOnEndTouch		= 1 << 1,
	CF_NotifyOnTouch		= 1 << 2,
	CF_NotifyOnImpact		= 1 << 3,
	CF_NotifyOnRoll			= 1 << 4,
	CF_NofityOnSlide		= 1 << 5,
};

struct ContactInfo
{
	Vector4 normal;
	Vector4 position;
};

class ContactInfoIterator
{
	friend class InternalContactReport;
	friend class HitReport;

public:

	ContactInfo* operator*();
	ContactInfo* operator->();
	void operator++();

protected:

	ContactInfoIterator(NxConstContactStream& stream);
	ContactInfoIterator();

	NxContactStreamIterator mStreamIterator;
	ContactInfo				mInfo;
	int						mEnd;
};

class ContactReport
{
public:

    virtual void OnContactNotify(PhysicsBody* body, PhysicsBody* body2, ContactInfoIterator& contactIt, ContactFlags event) = 0;
	virtual void OnActorContact(PhysicsBody* body, PhysicsActorController* controller, ContactInfoIterator& contactIt) = 0;
};

#endif
