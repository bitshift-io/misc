#include "PhysicsJoint.h"
#include "PhysicsScene.h"
#include "PhysicsBody.h"

void PhysicsJointConstructor::SetSphere(float twistSpring, float swingSpring)
{
	mJointType = JT_Sphere;

	mSphereDesc.flags |= NX_SJF_TWIST_LIMIT_ENABLED;
    mSphereDesc.twistLimit.low.value = -(NxReal)1.0*NxPi;
    mSphereDesc.twistLimit.high.value = (NxReal)1.0*NxPi;

    mSphereDesc.flags = 0; //|= NX_SJF_TWIST_SPRING_ENABLED;
    NxSpringDesc ts;
    ts.spring = twistSpring;
    ts.damper = 10.0;
    ts.targetValue = 0;
    mSphereDesc.twistSpring = ts;

    mSphereDesc.flags = 0; //|= NX_SJF_SWING_LIMIT_ENABLED;
    mSphereDesc.swingLimit.value = (NxReal)2.0*NxPi;

    mSphereDesc.flags =0; //|= NX_SJF_SWING_SPRING_ENABLED;
    NxSpringDesc ss;
    ss.spring = 1000.f; //swingSpring;
    ss.damper = 10.0;
    ss.targetValue = 0;
    mSphereDesc.swingSpring = ss;
}

void PhysicsJointConstructor::JoinBody(PhysicsBody* body1, PhysicsBody* body2, const Vector4& globalAnchor, const Vector4& globalAxis)
{
	NxJointDesc* jointDesc = 0;
	switch (mJointType)
	{
	case JT_Cylinder:
		jointDesc = &mCylinderDesc;
		break;

	case JT_Sphere:
		jointDesc = &mSphereDesc;
		break;
	}

	jointDesc->actor[0] = body1 == 0 ? 0 : body1->GetHandle();
	jointDesc->actor[1] = body2 == 0 ? 0 : body2->GetHandle();
	jointDesc->setGlobalAnchor(NxVec3(globalAnchor.vec));
	jointDesc->setGlobalAxis(NxVec3(globalAxis.vec));
}

void PhysicsJoint::Init(PhysicsScene* scene, const PhysicsJointConstructor& constructor)
{
	NxScene* nxScene = scene->GetHandle();
	switch (constructor.mJointType)
	{
	case JT_Cylinder:
		{
			mJoint = nxScene->createJoint(constructor.mCylinderDesc);
		}
		break;

	case JT_Sphere:
		{
			bool valid = constructor.mSphereDesc.isValid();
			mJoint = nxScene->createJoint(constructor.mSphereDesc);
		}
		break;
	}
}