#ifndef _PHYSICSPARTICLESYSTEM_H_
#define _PHYSICSPARTICLESYSTEM_H_

#include "Math/Vector.h"
#include "Math/Matrix.h"

class PhysicsScene;
class NxFluid;
class NxImplicitScreenMesh;
class NxFluidEmitter;

struct PhysicsParticle
{
	Vector4			pos;
	float			life;
	unsigned int	id;
	float			density;
	unsigned int	createdId;
};

class PhysicsParticleEmitterConstructor
{
public:

	float			mRate;
	unsigned int	mMaxParticles;
	float			mParticleLife;
};

class PhysicsParticleEmitter
{
	friend class PhysicsParticleSystem;

public:

	void SetTransform(const Matrix4& transform);

protected:

	NxFluidEmitter*	mEmitter;
};


class PhysicsParticleSystem
{
public:

	void		Init(PhysicsScene* scene);
	void		Deinit();

	int						GetParticleCount()	{ return 2103; }
	PhysicsParticle*		GetParticle()		{ return mParticleBuffer; }

	PhysicsParticleEmitter*	CreateParticleEmitter(const PhysicsParticleEmitterConstructor& constructor);
	void					ReleaseParticleEmitter(PhysicsParticleEmitter** emitter);

protected:

	//vector<PhysicsParticleEmitter*>	mEmitter;

	PhysicsParticle*		mParticleBuffer;
	NxFluid*				mFluid;
	int						mParticleCount;
};

#endif
