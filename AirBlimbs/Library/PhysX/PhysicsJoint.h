#ifndef _PHYSICSJOINT_H_
#define _PHYSICSJOINT_H_

#include "PhysicsFactory.h"
#include "Math/Vector.h"

class PhysicsBody;

enum JointType
{
	JT_Cylinder,
	JT_Sphere,
};

class PhysicsJointConstructor
{
	friend class PhysicsJoint;

public:

	void SetSphere(float twistSpring = 100, float swingSpring = 100);

	void JoinBody(PhysicsBody* body1, PhysicsBody* body2, const Vector4& globalAnchor, const Vector4& globalAxis);

protected:

	JointType				mJointType;
	NxRevoluteJointDesc		mRevoluteDesc;
	NxCylindricalJointDesc	mCylinderDesc;
	NxSphericalJointDesc	mSphereDesc;
};

class PhysicsJoint
{
public:

	void		Init(PhysicsScene* scene, const PhysicsJointConstructor& constructor);

protected:

	NxJoint*	mJoint;
};

#endif
