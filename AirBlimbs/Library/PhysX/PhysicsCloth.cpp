#include "PhysicsCloth.h"
#include "PhysicsScene.h"
#include "Cloth/NxClothMeshDesc.h"
#include "NxCooking.h"

#define TEAR_MEMORY_FACTOR 2

PhysicsClothConstructor::~PhysicsClothConstructor()
{
	delete mReadBuffer;
}

void PhysicsClothConstructor::SetProperties(float thickness)
{
	mDesc.thickness = thickness;
	mDesc.friction = 0.5f;
	mDesc.flags |= NX_CLF_BENDING;
	mDesc.flags |= NX_CLF_COLLISION_TWOWAY;
}

void PhysicsClothConstructor::AddTriMesh(const PhysicsFactory* factory, const TriMeshConstructor& meshData, int collisionGroup, const Matrix4* local)
{
	mClothMeshDesc.numVertices				= meshData.mPositionCount;
	mClothMeshDesc.numTriangles				= meshData.mIndexCount / 3;
	mClothMeshDesc.pointStrideBytes			= sizeof(Vector4);
	mClothMeshDesc.triangleStrideBytes		= 3 * sizeof(unsigned int);
	mClothMeshDesc.points					= meshData.mPosition;
	mClothMeshDesc.triangles				= meshData.mIndex;
	mClothMeshDesc.flags					= 0;

	// the cooked mesh will not be simulated on the PPU, only in software
	// the software only version is smaller and takes less time to cook
//	mClothMeshDesc.flags = NX_CLOTH_MESH_SOFTWARE;

	// we cook the mesh on the fly through a memory stream
	// we could also use a file stream and pre-cook the mesh
	MemoryWriteBuffer wb;
	if (!NxCookClothMesh(mClothMeshDesc, wb)) 
		return;

	mReadBuffer = new MemoryReadBuffer(wb.data);
}


void PhysicsCloth::Init(PhysicsScene* scene, const PhysicsClothConstructor& constructor)
{
	mCloth = scene->GetHandle()->createCloth(constructor.mDesc);
	mClothMesh = scene->GetHandle()->getPhysicsSDK().createClothMesh(*constructor.mReadBuffer);

	// here we setup the buffers through which the SDK returns the dynamic cloth data
	// we reserve more memory for vertices than the initial mesh takes
	// because tearing creates new vertices
	// the SDK only tears cloth as long as there is room in these buffers
	NxU32 maxVertices = TEAR_MEMORY_FACTOR * constructor.mClothMeshDesc.numVertices;
	mReceiveBuffers.verticesPosBegin = (NxVec3*)malloc(sizeof(NxVec3)*maxVertices);
	mReceiveBuffers.verticesNormalBegin = (NxVec3*)malloc(sizeof(NxVec3)*maxVertices);
	mReceiveBuffers.verticesPosByteStride = sizeof(NxVec3);
	mReceiveBuffers.verticesNormalByteStride = sizeof(NxVec3);
	mReceiveBuffers.maxVertices = maxVertices;
	mReceiveBuffers.numVerticesPtr = (NxU32*)malloc(sizeof(NxU32));

	// the number of triangles is constant, even if the cloth is torn
	NxU32 maxIndices = 3 * constructor.mClothMeshDesc.numTriangles;
	mReceiveBuffers.indicesBegin = (NxU32*)malloc(sizeof(NxU32)*maxIndices);
	mReceiveBuffers.indicesByteStride = sizeof(NxU32);
	mReceiveBuffers.maxIndices = maxIndices;
	mReceiveBuffers.numIndicesPtr = (NxU32*)malloc(sizeof(NxU32));
	//mReceiveBuffers.numTrianglesPtr = (NxU32*)malloc(sizeof(NxU32));

	// the parent index information would be needed if we used textured cloth
	NxU32 maxParentIndices = maxVertices;
	mReceiveBuffers.parentIndicesBegin = (NxU32*)malloc(sizeof(NxU32)*maxParentIndices);
	mReceiveBuffers.parentIndicesByteStride = sizeof(NxU32);
	mReceiveBuffers.maxParentIndices = maxParentIndices;
	mReceiveBuffers.numParentIndicesPtr = (NxU32*)malloc(sizeof(NxU32));

	// init the buffers in case we want to draw the mesh 
	// before the SDK as filled in the correct values
	*mReceiveBuffers.numVerticesPtr = 0;
	//*mReceiveBuffers.numTrianglesPtr = 0;
	*mReceiveBuffers.numIndicesPtr = 0;
}

void PhysicsCloth::Deinit()
{
	NxVec3* vp;
	NxU32* up; 
	vp = (NxVec3*)mReceiveBuffers.verticesPosBegin; free(vp);
	vp = (NxVec3*)mReceiveBuffers.verticesNormalBegin; free(vp);
	up = (NxU32*)mReceiveBuffers.numVerticesPtr; free(up);

	up = (NxU32*)mReceiveBuffers.indicesBegin; free(up);
	up = (NxU32*)mReceiveBuffers.numIndicesPtr; free(up);
	//up = (NxU32*)mReceiveBuffers.numTrianglesPtr; free(up);

	up = (NxU32*)mReceiveBuffers.parentIndicesBegin; free(up);
	up = (NxU32*)mReceiveBuffers.numParentIndicesPtr; free(up);
}

void PhysicsCloth::AttachToBody(PhysicsBody* body)
{
	// TODO: itertor
	mCloth->attachToShape(*body->GetHandle()->getShapes(), NX_CLOTH_ATTACHMENT_TWOWAY);
}