#ifndef _PHYSICS_H_
#define _PHYSICS_H_


#undef	new
#undef	delete
#undef	malloc
#undef	calloc
#undef	realloc
#undef	free

#include "NxSimpleTypes.h"
#include "NxPhysics.h"
#include "ControllerManager.h"

class PhysicsJoint;
class PhysicsScene;
class PhysicsBody;
class PhysicsActorController;
class PhysicsCloth;
class PhysicsParticleSystem;

class PhysicsFactory
{
public:

	bool			Init();
	void			Deinit();

	PhysicsActorController*	CreatePhsicsActorController();

	PhysicsParticleSystem*	CreatePhysicsParticleSystem();

	PhysicsJoint*	CreatePhysicsJoint();
	PhysicsBody*	CreatePhysicsBody();
	PhysicsCloth*	CreatePhysicsCloth();
	PhysicsScene*	CreatePhysicsScene();
	void			ReleasePhysicsScene(PhysicsScene** scene);

	NxPhysicsSDK*	GetHandle() const								{ return mPhysicsHandle; }
	ControllerManager& GetActorController()							{ return mControllerMgr; }

	void			Update();

protected:

	NxPhysicsSDK*		mPhysicsHandle;
	ControllerManager	mControllerMgr;
};

#endif
