#ifndef _SPHERE_H_
#define _SPHERE_H_

#include <math.h>
#include "Math.h"
#include "Vector.h"

class Sphere
{
public:

	Sphere();
	Sphere(const Vector4& _position, float _radius);

	void		CreateFromTri(const Vector4& p0, const Vector4& p1, const Vector4& p2);

	void		SetRadius(float radius)					{ this->radius = radius; }
	void		SetPosition(const Vector4& position)	{ this->position = position; }

	const float&	GetRadius() const					{ return radius; }
	const Vector4&	GetPosition() const					{ return position; }

	// returns distance between spheres or -1 if no intersection
	float		Intersect(const Sphere& other) const;

	Vector4		position;
	float		radius;
};

#include "Sphere.inl"

#endif
