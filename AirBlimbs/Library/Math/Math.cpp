#include "Math.h"
#include "Vector.h"

const float Math::Epsilon = 0.00000001f;
const float Math::PI = 3.1415f;
const int Math::IntMax = 0x7FFFFFFF;
const unsigned int Math::UIntMax = 0xFFFFFFFF;


//
// From "Texturing and Modeling A Procedural Approach"
//
// Chapter 6 by Ken Perlin
//
// Taken from max sdk so particle systems work in the same fashion
//

/*
#define random()	rand()


float bias(float a, float b)
{
	return pow((double)a, log((double)b) / log(0.5));
}

float gain(float a, float b)
{
	float p = log(1. - b) / log(0.5);

	if (a < .001)
		return 0.;
	else if (a > .999)
		return 1.;
	if (a < 0.5)
		return pow(2 * a, p) / 2;
	else
		return 1. - pow(2.0 * (1. - a), (double)p) / 2;
}

float noise1(float arg);
float noise2(float vec[]);
float noise3(float vec[]);

float noise(float vec[], int len)
{
	switch (len) {
	case 0:
		return 0.;
	case 1:
		return noise1(vec[0]);
	case 2:
		return noise2(vec);
	default:
		return noise3(vec);
	}
}

float turbulence(float *v, float freq)
{
	float t, vec[3];

	for (t = 0. ; freq >= 1. ; freq /= 2) {
		vec[0] = freq * v[0];
		vec[1] = freq * v[1];
		vec[2] = freq * v[2];
		t += fabs(noise3(vec)) / freq;
	}
	return t;
}
*/
/* noise functions over 1, 2, and 3 dimensions */

#define B 0x100
#define BM 0xff

#define N 0x1000
#define NP 12   /* 2^N */
#define NM 0xfff

static int p[B + B + 2];
static Vector4 g3[B + B + 2];
static Vector4 g2[B + B + 2];
static float g1[B + B + 2];
static int start = 1;

static void init(void);

int Perm(int v)
	{
	return p[v&BM];
	}

#define s_curve(t) ( t * t * (3. - 2. * t) )

#define lerp(t, a, b) ( a + t * (b - a) )

#define setup(i,b0,b1,r0,r1)\
	t = vec.vec[i] + N;\
	b0 = ((int)t) & BM;\
	b1 = (b0+1) & BM;\
	r0 = t - (int)t;\
	r1 = r0 - 1.;


float Math::Noise1(float arg)
{
	int bx0, bx1;
	float rx0, rx1, sx, t, u, v;
	Vector4 vec(arg, 0.f, 0.f, 0.f);

	if (start) 
	{
		start = 0;
		init();
	}

	setup(0, bx0,bx1, rx0,rx1);

	sx = s_curve(rx0);

	u = rx0 * g1[ p[ bx0 ] ];
	v = rx1 * g1[ p[ bx1 ] ];

	return lerp(sx, u, v);
}

float Math::Noise2(const Vector4& vec)
{
	int bx0, bx1, by0, by1, b00, b10, b01, b11;
	float rx0, rx1, ry0, ry1, *q, sx, sy, a, b, t, u, v;
	register int i, j;

	if (start) 
	{
		start = 0;
		init();
	}

	setup(0, bx0,bx1, rx0,rx1);
	setup(1, by0,by1, ry0,ry1);

	i = p[ bx0 ];
	j = p[ bx1 ];

	b00 = p[ i + by0 ];
	b10 = p[ j + by0 ];
	b01 = p[ i + by1 ];
	b11 = p[ j + by1 ];

	sx = s_curve(rx0);
	sy = s_curve(ry0);

#define at2(rx,ry) ( rx * q[0] + ry * q[1] )

	u = g2[ b00 ].Dot(Vector4(rx0,ry0, 0.f, 0.f));
	v = g2[ b10 ].Dot(Vector4(rx1,ry0, 0.f, 0.f));
	//q = g2[ b00 ] ; u = at2(rx0,ry0);
	//q = g2[ b10 ] ; v = at2(rx1,ry0);
	a = lerp(sx, u, v);

	u = g2[ b01 ].Dot(Vector4(rx0,ry1, 0.f, 0.f));
	v = g2[ b11 ].Dot(Vector4(rx1,ry1, 0.f, 0.f));
	//q = g2[ b01 ] ; u = at2(rx0,ry1);
	//q = g2[ b11 ] ; v = at2(rx1,ry1);
	b = lerp(sx, u, v);

	return lerp(sy, a, b);
}

float Math::Noise3(const Vector4& vec)
{
	int bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11;
	float rx0, rx1, ry0, ry1, rz0, rz1, *q, sy, sz, a, b, c, d, t, u, v;
	register int i, j;

	if (start) 
	{
		start = 0;
		init();
	}

	setup(0, bx0,bx1, rx0,rx1);
	setup(1, by0,by1, ry0,ry1);
	setup(2, bz0,bz1, rz0,rz1);

	i = p[ bx0 ];
	j = p[ bx1 ];

	b00 = p[ i + by0 ];
	b10 = p[ j + by0 ];
	b01 = p[ i + by1 ];
	b11 = p[ j + by1 ];

	t  = s_curve(rx0);
	sy = s_curve(ry0);
	sz = s_curve(rz0);

#define at3(rx,ry,rz) ( rx * q[0] + ry * q[1] + rz * q[2] )

	u = g3[ b00 + bz0 ].Dot(Vector4(rx0,ry0,rz0));
	v = g3[ b10 + bz0 ].Dot(Vector4(rx1,ry0,rz0));
	//q = g3[ b00 + bz0 ] ; u = at3(rx0,ry0,rz0);
	//q = g3[ b10 + bz0 ] ; v = at3(rx1,ry0,rz0);
	a = lerp(t, u, v);

	u = g3[ b01 + bz0 ].Dot(Vector4(rx0,ry1,rz0));
	v = g3[ b11 + bz0 ].Dot(Vector4(rx1,ry1,rz0));
	//q = g3[ b01 + bz0 ] ; u = at3(rx0,ry1,rz0);
	//q = g3[ b11 + bz0 ] ; v = at3(rx1,ry1,rz0);
	b = lerp(t, u, v);

	c = lerp(sy, a, b);

	u = g3[ b00 + bz1 ].Dot(Vector4(rx0,ry0,rz1));
	v = g3[ b10 + bz1 ].Dot(Vector4(rx1,ry0,rz1));
	//q = g3[ b00 + bz1 ] ; u = at3(rx0,ry0,rz1);
	//q = g3[ b10 + bz1 ] ; v = at3(rx1,ry0,rz1);
	a = lerp(t, u, v);

	u = g3[ b01 + bz1 ].Dot(Vector4(rx0,ry1,rz1));
	v = g3[ b11 + bz1 ].Dot(Vector4(rx1,ry1,rz1));
	//q = g3[ b01 + bz1 ] ; u = at3(rx0,ry1,rz1);
	//q = g3[ b11 + bz1 ] ; v = at3(rx1,ry1,rz1);
	b = lerp(t, u, v);

	d = lerp(sy, a, b);

	return lerp(sz, c, d);
}

static void init(void)
{
	int i, j, k;
	srand(0);

	for (i = 0 ; i < B ; i++) 
	{
		p[i] = i;

		g1[i] = (float)((rand() % (B + B)) - B) / B;

		for (j = 0 ; j < 2 ; j++)
			g2[i].vec[j] = (float)((rand() % (B + B)) - B) / B;
		g2[i].Normalize();

		for (j = 0 ; j < 3 ; j++)
			g3[i].vec[j] = (float)((rand() % (B + B)) - B) / B;
		g3[i].Normalize();
	}

	while (--i) 
	{
		k = p[i];
		p[i] = p[j = rand() % B];
		p[j] = k;
	}

	for (i = 0 ; i < B + 2 ; i++) 
	{
		p[B + i] = p[i];
		g1[B + i] = g1[i];
		for (j = 0 ; j < 2 ; j++)
			g2[B + i].vec[j] = g2[i].vec[j];

		for (j = 0 ; j < 3 ; j++)
			g3[B + i].vec[j] = g3[i].vec[j];
	}
}



/*
 * Procedural fBm evaluated at "point"; returns value stored in "value".
 *
 * Copyright 1994 F. Kenton Musgrave 
 * 
 * Parameters:
 *    ``H''  is the fractal increment parameter
 *    ``lacunarity''  is the gap between successive frequencies
 *    ``octaves''  is the number of frequencies in the fBm
 * /

// RB:
// Modified to be evaluated with a scalar.

#define TRUE    1
#define FALSE   0
#define MAX_OCTAVES	50

double
fBm1( double point, double H, double lacunarity, double octaves )
{
	  static double     exponent_array[MAX_OCTAVES+1];
      static double		lastH;
      double            value, frequency, remainder, Noise3();
      int               i;
      static int        first = TRUE;
      

      /* precompute and store spectral weights * /
      if (first || H!= lastH) {
	  		lastH = H;
            frequency = 1.0;
            for (i=0; i<=octaves; i++) {
                  /* compute weight for each frequency * /
                  exponent_array[i] = pow( frequency, -H );
                  frequency *= lacunarity;
            }
            first = FALSE;
      }

      value = 0.0;            /* initialize vars to proper values * /
      frequency = 1.0;

      /* inner loop of spectral construction * /
      for (i=0; i<octaves; i++) {
            value += Noise1( point ) * exponent_array[i];
            point *= lacunarity;            
      } /* for * /

      remainder = octaves - (int)octaves;
      if ( remainder )      /* add in ``octaves''  remainder * /
            /* ``i''  and spatial freq. are preset in loop above * /
            value += remainder * Noise1( point ) * exponent_array[i];

      return( value );

} /* fBm() */
