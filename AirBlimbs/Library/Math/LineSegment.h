#ifndef _LINESEGMENT_H_
#define _LINESEGMENT_H_

class LineSegment
{
public:

	LineSegment() {}
	LineSegment(const Vector4& a, const Vector4& b);

	Vector4	ClosestPoint(const Vector4& point, float* time = 0);
	float	DistanceToPoint(const Vector4& _point);

	// the shortest line segment (result)  between two line segments (this and other)
	// returns false if no solution
	// result.p[0] always lies on this segment
	// result.p[1] always lies on other segment
	bool ShortestSegmentBetweenSegment(const LineSegment& other, LineSegment* result, float* time0 = 0, float* time1 = 0);

	float	Length() const;
	Ray		AsRay() const;

	Vector4 p[2];
};

#include "LineSegment.inl"

#endif