#ifndef _FRUSTUM_H_
#define _FRUSTUM_H_

#include "Sphere.h"
#include "Plane.h"
#include "Box.h"

class Frustum
{
public:

	enum Sides
	{
		Top,
		Bottom,
		Left,
		Right,
		Near,
		Far
	};

	enum Points
	{
		TopLeftNear,
		TopLeftFar,
		TopRightNear,
		TopRightFar,

		BottomLeftNear,
		BottomLeftFar,
		BottomRightNear,
		BottomRightFar
	};

	enum State
	{
		Inside,
		Intersect,
		Outside
	};


	inline State ContainsBox(const Box& box) const
	{
		Vector4 vMin = box.GetTransform() * box.GetMin();
		Vector4 vMax = box.GetTransform() * box.GetMax();

		Sphere sphere;
		Vector4 vRadius = (vMax - vMin) * 0.5f;
		float radius = vRadius.Mag();
		sphere.SetPosition(vMin + vRadius); 
		sphere.SetRadius(radius);

		// TODO: more detailed check
		return ContainsSphere(sphere);
	}

	State	ContainsSphere(const Sphere& cullSphere) const;
	void	Init(const Matrix4& transform, float fovy, float aspect, float zNear, float zFar);
	void	Init(const Matrix4& worldViewProjection);


	Sphere	sphere;
	Plane	sides[6];
	Vector4 points[8];

	float	angle;
	float	length;
	Vector4 foward;
	Vector4 position;

	// should we also keep conical frustrum info for quick culling?
};

#include "Frustum.inc"

#endif
