
inline Triangle::Triangle(const Vector4& a, const Vector4& b, const Vector4& c)
{
	p[0] = a;
	p[1] = b;
	p[2] = c;
}

inline LineSegment Triangle::GetEdgeAsSegment(int index)
{
	return LineSegment(p[index], p[(index+1) % 3]);
}