#ifndef _AFFINE_H_
#define _AFFINE_H_

#include "Matrix.h"
#include "Quaternion.h"

// http://www.cse.ucsc.edu/~pang/160/f98/Gems/GemsIV/polar_decomp/Decompose.h 
// http://www.cse.ucsc.edu/~pang/160/f98/Gems/GemsIV/polar_decomp/Decompose.c

//
// This class converts a transform matrix in to its individual parts
//
class AffineParts
{
public:

	void		DecomposeAffine(const Matrix4& matrix);
	//void		InvertAffine();

	//float		DecomposePolar(const Matrix4& m, Matrix4& q, Matrix4& s);
	//Vector4		DecomposeSpect(Matrix4& s, Matrix4& u);
	//Quaternion	Snuggle(Quaternion& q, Vector4& k);

	Vector4		translation;
	Quaternion	rotation;
	Quaternion	stretchRotation;
	Vector4		stretchFactor;
	float		determinant;
};


#endif