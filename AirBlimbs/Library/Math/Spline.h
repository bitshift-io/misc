#ifndef _MATH_SPLINE_H_
#define _MATH_SPLINE_H_

#include "Vector.h"
#include <vector>

using namespace std;

enum KnotType
{
	KT_Auto,
	KT_Corner,
	KT_Bezier,
	KT_BezierCorner,
};

class Knot3
{
public:

	Vector4		mIn;
	Vector4		mOut;
	Vector4		mPosition;
	KnotType	mType;
};

class Spline3
{
public:

	Spline3();

	void	Init(int count);
	void	Deinit();

	Vector4 Evaluate(float time);

	Knot3*			mKnot;
	unsigned int	mCount;
};

#endif
