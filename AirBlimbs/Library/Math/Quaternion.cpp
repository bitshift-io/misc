#include "Quaternion.h"

void Quaternion::CreateFromMatrix(const Matrix4& mat)
{
	// http://skal.planet-d.net/demo/matrixfaq.htm#Q55
	float t = 1 + mat[0] + mat[5] + mat[10];

	if (t > Math::Epsilon) // should this be less than epsilon also?
	{
		float s = sqrtf(t) * 2;
		x = (mat[9] - mat[6]) / s;
		y = (mat[2] - mat[8]) / s;
		z = (mat[4] - mat[1]) / s;
		w = 0.25f * s;
	}
	else // if t == 0
	{
		if (mat[0] > mat[5] && mat[0] > mat[10]) // col1
		{
			float s = sqrtf(1.0f + mat[0] - mat[5] - mat[10]) * 2;
			x = 0.25f * s;
			y = (mat[4] + mat[1]) / s;
			z = (mat[2] + mat[8]) / s;
			w = (mat[9] - mat[6]) / s;
		}
		else if (mat[5] > mat[10]) // col2
		{
			float s = sqrtf(1.0f + mat[5] - mat[0] - mat[10]) * 2;
			x = (mat[4] + mat[1]) / s;
			y = 0.25f * s;
			z = (mat[9] + mat[6]) / s;
			w = (mat[2] - mat[8]) / s;
		}
		else //col3
		{
			float s = sqrtf(1.0f + mat[10] - mat[0] - mat[5]) * 2;
			x = (mat[2] - mat[8]) / s;
			y = (mat[9] - mat[6]) / s;
			z = 0.25f * s;
			w = (mat[4] - mat[1]) / s;
		}
	}
}

void Quaternion::CreateFromAxisAngle(const Vector4& axis, const float angle)
{
	Vector4 temp = axis * sinf(angle * 0.5f);
	x = temp.x;
	y = temp.y;
	z = temp.z;
	w = cosf(angle * 0.5f);

	Normalize();
}

void Quaternion::GetAxisAngle(Vector4& axis, float& angle)
{
	float cosAng = w;
	angle = acosf(cosAng) * 2;

	float sinAng = sqrtf(1.0f - cosAng * cosAng);
	if (fabsf( sinAng ) < 0.0005f)
		sinAng = 1;

	axis.x = x / sinAng;
	axis.y = y / sinAng;
	axis.z = z / sinAng;
}

float Quaternion::Magnitude() const
{
	return sqrtf(x*x + y*y + z*z + w*w);
}

float Quaternion::AngleBetween(const Quaternion& rhs) const
{
	float dot = Dot(rhs);
	return acosf(dot);
}

Quaternion Quaternion::Interpolate(const Quaternion& to, float weight) const
{
	// http://www.gamasutra.com/features/19980703/quaternions_01.htm

	Quaternion temp;
    float omega, cosom, sinom, scale0, scale1;

    // calc cosine
    cosom = Dot(to);

    // adjust signs (if necessary)
    if (cosom < 0.0)
	{
		cosom = -cosom;
		temp.x = -to.x;
		temp.y = -to.y;
		temp.z = -to.z;
		temp.w = -to.w;
    }
	else
	{
		temp.x = to.x;
		temp.y = to.y;
		temp.z = to.z;
		temp.w = to.w;
    }

    // calculate coefficients
   if ((1.0f - cosom) > 0.0001f)
   {
        // standard case (slerp)
        omega = acosf(cosom);
        sinom = sinf(omega);
        scale0 = sinf((1.0f - weight) * omega) / sinom;
        scale1 = sinf(weight * omega) / sinom;
   }
   else
   {
		// "from" and "to" quaternions are very close
		//  ... so we can do a linear interpolation
        scale0 = 1.0f - weight;
        scale1 = weight;
    }

	Quaternion res;

	// calculate final values
	res.x = scale0 * x + scale1 * temp.x;
	res.y = scale0 * y + scale1 * temp.y;
	res.z = scale0 * z + scale1 * temp.z;
	res.w = scale0 * w + scale1 * temp.w;

	return res;
}
