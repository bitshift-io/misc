#include "Sphere.h"
#include "Plane.h"

inline LineSegment::LineSegment(const Vector4& a, const Vector4& b)
{
	p[0] = a;
	p[1] = b;
}

inline Vector4	LineSegment::ClosestPoint(const Vector4& point, float* time)
{
	// this could be optimized lots, probably a better formula as well
	Vector4 dir = p[1] - p[0];
	float dist = dir.Normalize3();
	Ray r(p[0], dir);

	Vector4 closestPoint = r.ClosestPoint(point);
	if (time)
	{
		Vector4 closestPointToP0 = p[0] - closestPoint;
		float distClosestPointToP0 = closestPointToP0.Normalize3();
		*time = distClosestPointToP0 / dist;

		if (dir.Dot3(closestPointToP0) < 0.f)
			*time = -*time;
	}

	return closestPoint;
}

inline float LineSegment::DistanceToPoint(const Vector4& _point)
{
	return 0.f;
}

inline bool LineSegment::ShortestSegmentBetweenSegment(const LineSegment& other, LineSegment* result, float* time0, float* time1)
{/*
	// http://www.netcomuk.co.uk/~jenolive/skew.html
	// http://www.mycplus.com/tutorial.asp?TID=81
	Ray otherRay = other.AsRay();
	Ray thisRay = AsRay();

	Vector4 planeNormal = otherRay.direction.Cross(thisRay.direction);
	// what if two segments have the same direction (ie parallel)?
	if (Math::Abs(planeNormal.MagSquared3() <= Math::Epsilon))
	{
		// TODO
		return -1.f;
	}

	planeNormal.Normalize3();

	Plane otherPlane(otherRay.origin, planeNormal);
	Plane thisPlane(thisRay.origin, planeNormal);

	float distanceBetweenPlane = otherPlane.DistanceToPoint(thisRay.origin);
	return Math::Abs(distanceBetweenPlane); // does sign really matter here?
	*/

	Vector4 p1 = p[0];
	Vector4 p2 = p[1];
	Vector4 p3 = other.p[0];
	Vector4 p4 = other.p[1];

	Vector4 p13 = p1 - p3;
	Vector4 p43 = p4 - p3;

	using namespace Math;

	if (Abs(p43.x) < Epsilon && Abs(p43.y) < Epsilon && Abs(p43.z) < Epsilon)
      return false;

	Vector4 p21 = p2 - p1;

	if (Abs(p21.x) < Epsilon && Abs(p21.y) < Epsilon && Abs(p21.z) < Epsilon)
      return false;

	float d1343 = p13.Dot3(p43);
	float d4321 = p43.Dot3(p21);
	float d1321 = p13.Dot3(p21);
	float d4343 = p43.Dot3(p43);
	float d2121 = p21.Dot3(p21);

	float denom = d2121 * d4343 - d4321 * d4321;
	if (Abs(denom) < Epsilon)
		return false;
	float numer = d1343 * d4321 - d1321 * d4343;

	float mua = numer / denom;
	float mub = (d1343 + d4321 * mua) / d4343;

	if (time0)
		*time0 = mua;

	if (time1)
		*time1 = mub;

	// clamp locks to end points of line segments
	Vector4 pa = p1 + p21 * Clamp(0.f, mua, 1.f);
	Vector4 pb = p3 + p43 * Clamp(0.f, mub, 1.f);

	result->p[0] = pa;
	result->p[1] = pb;
	return true;
}

inline float LineSegment::Length() const
{
	float length = (p[0] - p[1]).Mag3();
	return length;
}

inline Ray LineSegment::AsRay() const
{
	Vector4 dir = p[1] - p[0];
	dir.Normalize3();
	return Ray(p[0], dir);
}