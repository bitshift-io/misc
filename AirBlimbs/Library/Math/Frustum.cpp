#include "Frustum.h"

Frustum::State Frustum::ContainsSphere(const Sphere& cullSphere) const
{
	//if (sphere.Intersect(cullSphere) == -1)
	//	return Outside;

	float dist;

	// calculate our distances to each of the planes
	for(int i = 0; i < 6; ++i)
	{
		// find the distance to this plane
		dist = sides[i].DistanceToPoint(cullSphere.GetPosition());
			
		if (dist < -cullSphere.GetRadius())
			return Outside;
			
		// intersect
		if (fabs(dist) < cullSphere.GetRadius())
			return Intersect;
	}

	// otherwise we are fully in view
	return Inside;
}

void Frustum::Init(const Matrix4& transform, float fovy, float aspect, float zNear, float zFar)
{
	/*
  D3DXVector4 vZ=vTarget-vSource;
  D3DXVec3Normalize(&vZ,&vZ);

  D3DXVector4 vX;
  D3DXVec3Cross(&vX,&vUp,&vZ);
  D3DXVec3Normalize(&vX,&vX);

  D3DXVector4 vY;
  D3DXVec3Cross(&vY,&vZ,&vX);
*/
  
  

  	Vector4 view = transform.GetZAxis();
	Vector4 right = transform.GetXAxis();
	Vector4 up = transform.GetYAxis();

	float fNearPlaneHeight = tanf(Math::DtoR(fovy) * 0.5f) * zNear;
	float fNearPlaneWidth = fNearPlaneHeight * aspect;

	float fFarPlaneHeight = tanf(Math::DtoR(fovy) * 0.5f) * zFar;
	float fFarPlaneWidth = fFarPlaneHeight * aspect;

	Vector4 vNearPlaneCenter = transform.GetTranslation() + view * zNear;
	Vector4 vFarPlaneCenter = transform.GetTranslation() + view * zFar;

	points[BottomLeftNear]=vNearPlaneCenter - right*fNearPlaneWidth - up*fNearPlaneHeight;
	points[TopLeftNear]=vNearPlaneCenter - right*fNearPlaneWidth + up*fNearPlaneHeight;
	points[TopRightNear]=vNearPlaneCenter + right*fNearPlaneWidth + up*fNearPlaneHeight;
	points[BottomRightNear]=vNearPlaneCenter + right*fNearPlaneWidth - up*fNearPlaneHeight;

	points[BottomLeftFar]=vFarPlaneCenter - right*fFarPlaneWidth - up*fFarPlaneHeight;
	points[TopLeftFar]=vFarPlaneCenter - right*fFarPlaneWidth + up*fFarPlaneHeight;
	points[TopRightFar]=vFarPlaneCenter + right*fFarPlaneWidth + up*fFarPlaneHeight;
	points[BottomRightFar]=vFarPlaneCenter + right*fFarPlaneWidth - up*fFarPlaneHeight;

	/*
	float tanFov = tanf(Math::DtoR(fovy) / 2.f);

	float heightNear = tanFov * zNear;
	float widthNear = heightNear * aspect;

	float heightFar = tanFov * zFar;
	float widthFar = heightFar * aspect;

	Vector4 view = transform.GetZAxis();
	Vector4 right = transform.GetXAxis();
	Vector4 up = transform.GetYAxis();

	points[TopLeftNear] = view * zNear - right * widthNear + up * heightNear;
	points[BottomLeftNear] = view * zNear - right * widthNear - up * heightNear;
	points[BottomRightNear] = view * zNear + right * widthNear - up * heightNear;
	points[TopRightNear] = view * zNear + right * widthNear + up * heightNear;
	points[TopLeftFar] = view * zFar - right * widthFar + up * heightFar;
	points[TopRightFar] = view * zFar + right * widthFar + up * heightFar;
	points[BottomLeftFar] = view * zFar - right * widthFar - up * heightFar;
	points[BottomRightFar] = view * zFar + right * widthFar - up * heightFar;

	for (int i = 0; i < 8; ++i)
	{
		points[i] += transform.GetTranslation();
	}
	*/
	sides[Near] = Plane(transform.GetTranslation() + view * zNear, view);
	sides[Far] = Plane(transform.GetTranslation() + view * zFar, -view);

	Vector4 rightNorm = points[TopRightFar] - points[TopRightNear];
	rightNorm.Normalize();
	rightNorm = up.Cross(rightNorm);

	Vector4 topNorm = points[TopRightFar] - points[TopRightNear];
	topNorm.Normalize();
	topNorm = topNorm.Cross(right);

	Vector4 leftNorm = points[TopLeftFar] - points[TopLeftNear];
	leftNorm.Normalize();
	leftNorm = leftNorm.Cross(up);

	Vector4 bottomNorm = points[BottomRightFar] - points[BottomRightNear];
	bottomNorm.Normalize();
	bottomNorm = right.Cross(bottomNorm);

	sides[Top] = Plane(transform.GetTranslation(), topNorm);
	sides[Bottom] = Plane(transform.GetTranslation(), bottomNorm);
	sides[Left] = Plane(transform.GetTranslation(), leftNorm);
	sides[Right] = Plane(transform.GetTranslation(), rightNorm);
}
