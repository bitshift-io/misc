#include "Vector.h"
#include "Sphere.h"

inline Ray::Ray(const Vector4& _origin, const Vector4& _direction) :
	origin(_origin),
	direction(_direction)
{
}

inline Vector4 Ray::ClosestPoint(const Vector4& p) const
{
	Vector4 projectVec = p - origin;
	return origin + direction.Project3(projectVec);
}

inline bool Ray::Intersect(const Sphere& sphere) const
{
	// http://www.devmaster.net/wiki/Ray-sphere_intersection
	Vector4 dest = sphere.position - origin;
	float dot = dest.Dot(dest);
	if (dot < sphere.radius)
		return true;

	float tca = dest.Dot(direction);
	if (tca < 0.f) // points away from the sphere
		return false;

	float l2hc = sphere.radius - dot + (tca * tca);
	return l2hc > 0;
}

inline bool	Ray::Intersect(const Vector4& p, float* time) const
{
	// origin * direction * t = p
	// o.x * d.x * t = p.x
	// o.y * d.y * t = p.y
	// o.z * d.z * t = p.z
}

inline bool Ray::IntersectTri(const Vector4& p0, const Vector4& p1, const Vector4& p2, bool cull, TriIntersect* intersect) const
{
	Vector4 edge1 = p1 - p0;
	Vector4 edge2 = p2 - p0;

	Vector4 pVec = direction.Cross(edge2);
	float det = edge1.Dot(pVec);

	if (det > -Math::Epsilon && det < Math::Epsilon)
		return false;

	if (cull && det > 0.f)
		return false;

	float invDet = 1.f / det;

	// distance from ray origin to vert0
	Vector4 tVec = origin - p0;

	// calulate u
	float u = invDet * tVec.Dot(pVec);
	if (u < 0.f || u > 1.f)
		return false;

	// calulate v
	Vector4 qVec = tVec.Cross(edge1);
	float v = invDet * direction.Dot(qVec);
	if (v < 0.f || u + v > 1.f)
		return false;

	// calculate intersection point
	float time = invDet * edge2.Dot(qVec);
	if (time > Math::Epsilon) // ray intersection
	{
		if (intersect)
		{
			intersect->intersection.x = u;
			intersect->intersection.y = v;
			intersect->rayDistance = time;
		}
		return true;
	}

	return false;
}


