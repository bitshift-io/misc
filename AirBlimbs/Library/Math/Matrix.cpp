#include "Matrix.h"

void Matrix4::SetOrthographic(float width, float height, float znear, float zfar)
{
	float halfWidth = width * 0.5f;
	float halfHeight = height * 0.5f;
	SetOrthographic(-halfWidth, halfWidth, halfHeight, -halfHeight, znear, zfar);
}

void Matrix4::SetOrthographic(float left, float right, float top, float bottom, float znear, float zfar)
{
	SetIdentity();

	m[0] = 2.f / (right - left);
	m[5] = 2.f / (top - bottom);
	m[10] = -1.f / (zfar - znear); // m[10] = -2.f / (zfar - znear);

	//m[12] = -(right + left) / (right - left);
	//m[13] = -(top + bottom) / (top - bottom);
	//m[14] = (znear + zfar) / (znear - zfar);

	m[14] = znear / (znear - zfar);

/*
	2/w  0    0           0
	0    2/h  0           0
	0    0    1/(zf-zn)   zn/(zn-zf)
	0    0    0			  1
	*/
}

void Matrix4::SetPerspective(float fovy, float aspect, float znear, float zfar)
{
	SetZero();

	float yScale = 1.0f / tan(Math::DtoR(fovy / 2.0f));
	float xScale = yScale / aspect;

	m[0]  = xScale;
	m[5]  = yScale;
	m[10] = zfar / (znear - zfar);
	//m[10] = (zfar + znear) / (znear - zfar);

	m[11] = -1.0f;
	m[14] = (zfar * znear) / (znear - zfar);
	//m[14] = (2.0f * zfar * znear) / (znear - zfar);
}

void Matrix4::SetRotateY(float angle)
{
	SetRotationIdentity();

	float c = cosf(angle);
	float s = sinf(angle);

	m[0] = c;
	m[2] = s;
	m[8] = -s;
	m[10] = c;
}

void Matrix4::SetRotateX(float angle)
{
	SetRotationIdentity();

	float c = cosf(angle);
	float s = sinf(angle);

	m[5] = c;
	m[6] = -s;
	m[9] = s;
	m[10] = c;
}

void Matrix4::SetRotateZ(float angle)
{
	SetRotationIdentity();

	float c = cosf(angle);
	float s = sinf(angle);

	m[0] = c;
	m[1] = -s;
	m[4] = s;
	m[5] = c;
}

void Matrix4::SetRotateAxis(float angle, const Vector4& axis)
{
	float c = (float)cos(angle);
	float s = (float)sin(angle);

	m[0] = c + ((axis.x * axis.x) * (1 - c));
	m[1] = (axis.x * axis.y * (1 - c)) + (axis.z * s);
	m[2] = (axis.x * axis.z * (1 - c)) - (axis.y * s);

	m[4] = (axis.x * axis.y * (1 - c)) - (axis.z * s);
	m[5] = c + (axis.y * axis.y * (1 - c));
	m[6] = (axis.y * axis.z * (1 - c)) + (axis.x * s);

	m[8] = (axis.x * axis.z * (1 - c)) + (axis.y * s);
	m[9] = (axis.y * axis.z * (1 - c)) - (axis.z * s);
	m[10]= c + (axis.z * axis.z * (1 - c));
}
