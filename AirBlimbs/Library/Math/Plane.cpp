#include "Plane.h"
#include "Line.h"
#include "Ray.h"

float Plane::Normalize()
{
	float mag;
	mag = sqrtf(normal.x*normal.x + normal.y*normal.y + normal.z*normal.z);

	normal.x /= mag;
	normal.y /= mag;
	normal.z /= mag;
	distance /= mag;

	return mag;
}
/*
float Plane::Intersect(const Line& line) const
{
	float normDotDir = normal.Dot3(line.direction);
	if (normDotDir == 0.f)
		return 0.f;

	Vector4 pointOnPlane = normal * distance;
	return normal.Dot3(pointOnPlane - line.point) / normDotDir;
}
*/
float Plane::Intersect(const Ray& ray) const
{
	float normDotDir = normal.Dot3(ray.direction);
	if (normDotDir == 0.f)
		return 0.f;

	Vector4 pointOnPlane = normal * distance;
	return normal.Dot3(pointOnPlane - ray.origin) / normDotDir;
}