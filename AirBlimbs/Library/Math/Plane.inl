
inline Plane::Plane(const Vector4& _point, const Vector4& _normal) :
	normal(_normal)
{
	distance = normal.Dot3(_point);
}

inline float Plane::DistanceToPoint(const Vector4& _point) const
{
	return normal.Dot3(_point) - distance;

//	return normal.Dot(_point - point);
}

inline float Plane::DistanceToPlane(const Plane& other) const
{
	return 0.f;
}

/*
inline void Plane::ExtractLeft(const Matrix4& transform)
{
	normal.x = transform[3] + transform[0];
	normal.y = transform[7] + transform[4];
	normal.z = -transform[11] + transform[8];
	distance = transform[15] + transform[12];
}

inline void Plane::ExtractRight(const Matrix4& transform)
{
	/*
	normal.x = transform[3] - transform[0];
	normal.y = transform[7] - transform[4];
	normal.z = -transform[11] - transform[8];
	distance = transform[15] - transform[12];
	* /
}

inline void Plane::ExtractTop(const Matrix4& transform)
{/*
	normal.x = transform[3] - transform[1];
	normal.y = transform[7] - transform[5];
	normal.z = -transform[11] - transform[9];
	distance = transform[15] - transform[13];
	* /
}

inline void Plane::ExtractBottom(const Matrix4& transform)
{
	/*
	normal.x = transform[3] + transform[1];
	normal.y = transform[7] + transform[5];
	normal.z = -transform[11] + transform[9];
	distance = transform[15] + transform[13];
	* /
}

inline void Plane::ExtractNear(const Matrix4& transform)
{
	/*
	normal.x = transform[12] + transform[8];
	normal.y = transform[13] + transform[9];
	normal.z = transform[14] + transform[10];
	distance = transform[15] - transform[14]; //transform[15] + transform[11];
/*
	normal.x = transform[3] - transform[2];
	normal.y = transform[7] - transform[6];
	normal.z = -transform[11] - transform[10];
	distance = transform[15] - transform[14];* /
}

inline void Plane::ExtractFar(const Matrix4& transform)
{
	/*
	normal.x = transform[12] - transform[8];
	normal.y = transform[13] - transform[9];
	normal.z = transform[14] - transform[10];
	distance = transform[15] + transform[14]; //transform[15] - transform[11];

	/*
	normal = transform.GetView();
	distance = transform[15] + transform[11];
	/*
	normal.x = transform[3] + transform[2];
	normal.y = transform[7] + transform[6];
	normal.z = -transform[11] + transform[10];
	distance = transform[15] + transform[14];* /
}*/

inline Vector4 Plane::Intersect3(Plane& p1, Plane& p2)
{
	Vector4& n1 = normal;
	Vector4& n2 = p1.normal;
	Vector4& n3 = p2.normal;
	float& d1 = distance;
	float& d2 = p1.distance;
	float& d3 = p2.distance;

	//http://astronomy.swin.edu.au/~pbourke/geometry/3planes/
	Vector4 point = ((n2.Cross(n3) * d1) + (n1.Cross(n3) * d2) + (n1.Cross(n2) * d3)) / n1.Dot(n2.Cross(n3));
	return point;
}

inline Plane Plane::Transform(const Matrix4& transform) const
{
	Plane ret;

	// not sure if this works
	ret.normal.x = (transform[0] * normal.x) + (transform[4] * normal.y) + (transform[8] * normal.z)  + (transform[12] * distance);
	ret.normal.y = (transform[1] * normal.x) + (transform[5] * normal.y) + (transform[9] * normal.z)  + (transform[13] * distance);
	ret.normal.z = (transform[2] * normal.x) + (transform[6] * normal.y) + (transform[10] * normal.z) + (transform[14] * distance);
	ret.distance = (transform[3] * normal.x) + (transform[7] * normal.y) + (transform[11] * normal.z) + (transform[15] * distance);

	return ret;
}
