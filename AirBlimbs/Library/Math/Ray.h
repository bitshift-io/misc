#ifndef _RAY_H_
#define _RAY_H_

class Sphere;

struct TriIntersect
{
	Vector4 intersection;
	float	rayDistance;
};

class Ray
{
public:

	Ray() {}
	Ray(const Vector4& _origin, const Vector4& _direction);

	Vector4 ClosestPoint(const Vector4& p) const;
	bool	Intersect(const Sphere& sphere) const;
	bool	IntersectTri(const Vector4& p0, const Vector4& p1, const Vector4& p2, bool cull = true, TriIntersect* intersect = 0) const;
	bool	Intersect(const Vector4& p, float* time = 0) const; // see if this ray intersects the point p

	Vector4	origin;
	Vector4 direction;
	//float	distance;  // end distance
};

#include "Ray.inl"

#endif