#ifdef WIN32
	#define inline __forceinline
#endif

//=========================================================================
// Matrix3
//=========================================================================
inline float& Matrix3::operator[](const int index)
{
	return m[index];
}

inline float Matrix3::GetDeterminant() const
{
	return	(m[0] * (m[4] * m[8] - m[5] * m[7])) -
			(m[3] * (m[1] * m[8] - m[2] * m[7])) +
			(m[6] * (m[1] * m[5] - m[4] * m[2]));
}

//=========================================================================
// Matrix4
//=========================================================================
inline Matrix4::Matrix4()
{
}

inline Matrix4::Matrix4(const float* data)
{
	m[0] = data[0];
	m[1] = data[1];
	m[2] = data[2];
	m[3] = data[3];
	m[4] = data[4];
	m[5] = data[5];
	m[6] = data[6];
	m[7] = data[7];
	m[8] = data[8];
	m[9] = data[9];
	m[10] = data[10];
	m[11] = data[11];
	m[12] = data[12];
	m[13] = data[13];
	m[14] = data[14];
	m[15] = data[15];
}

inline Matrix4::Matrix4(MatrixInit type)
{
	if (type == MI_Identity)
		SetIdentity();
	else if (type == MI_Zero)
		SetZero();
	else
		SetMirror();
}

inline void Matrix4::SetScale(float scale)
{
	SetScale(scale, scale, scale);
}

inline void Matrix4::SetScale(float x, float y, float z)
{
	Column(0).Normalize3();
	Column(0) *= x;

	Column(1).Normalize3();
	Column(1) *= y;

	Column(2).Normalize3();
	Column(2) *= z;
}

inline void	Matrix4::SetScale(const Vector4& scale)
{
	mat[0][0] = scale.x;
	mat[1][1] = scale.y;
	mat[2][2] = scale.z;
}

inline void Matrix4::SetIdentity()
{
	m[0] = m[5]  = m[10] = m[15] = 1.0f;
	m[1] = m[2]  = m[3]  = m[4]  = 0.0f;
	m[6] = m[7]  = m[8]  = m[9]  = 0.0f;
	m[11]= m[12] = m[13] = m[14] = 0.0f;
}

inline void Matrix4::SetZero()
{
	m[0] = m[5]  = m[10] = m[15] = 0.0f;
	m[1] = m[2]  = m[3]  = m[4]  = 0.0f;
	m[6] = m[7]  = m[8]  = m[9]  = 0.0f;
	m[11]= m[12] = m[13] = m[14] = 0.0f;
}

inline void Matrix4::SetMirror()
{
	m[5] = -1.0f;
	m[0] = m[10] = m[15] = 1.0f;
	m[1] = m[2]  = m[3]  = m[4]  = 0.0f;
	m[6] = m[7]  = m[8]  = m[9]  = 0.0f;
	m[11]= m[12] = m[13] = m[14] = 0.0f;
}

inline void	Matrix4::SetRotationIdentity()
{
	m[0] = m[5]  = m[10] = 1.0f;
	m[1] = m[2]  = m[3]  = 0.0f;
	m[6] = m[7]  = m[8]  = 0.0f;
}

inline void Matrix4::SetTranslation(float x, float y, float z)
{
	m[12] = x;
	m[13] = y;
	m[14] = z;
}

inline void Matrix4::SetTranslation(const Vector4& trans)
{
	m[12] = trans.x;
	m[13] = trans.y;
	m[14] = trans.z;
}

inline float& Matrix4::operator[](const int index)
{
	return m[index];
}

inline const float& Matrix4::operator[](const int index) const
{
	return m[index];
}

inline void Matrix4::Translate(const Vector4& trans)
{
	Matrix4 n(MI_Identity);
	n.SetTranslation(trans);
	(*this) = (*this) * n;
}

inline void Matrix4::SetRotationZero()
{
	m[0] = m[1] = m[2] = 0.f;
	m[4] = m[5] = m[6] = 0.f;
	m[8] = m[9] = m[10] = 0.f;
}

inline void Matrix4::SetTranslationZero()
{
	m[12] = m[13] = m[14] = 0.f;
}

inline void Matrix4::RotateY(float angle)
{
	Matrix4 n(MI_Identity);
	n.SetRotateY(angle);
	(*this) = (*this) * n;
}

inline void Matrix4::RotateX(float angle)
{
	Matrix4 n(MI_Identity);
	n.SetRotateX(angle);
	(*this) = (*this) * n;
}

inline void Matrix4::RotateZ(float angle)
{
	Matrix4 n(MI_Identity);
	n.SetRotateZ(angle);
	(*this) = (*this) * n;
}

inline void Matrix4::RotateAxis(float angle, const Vector4& axis)
{
	Matrix4 n(MI_Identity);
	n.SetRotateAxis(angle, axis);
	(*this) = (*this) * n;
}

inline Vector4 Matrix4::operator*(const Vector4& vector) const
{
	Vector4 result;
	Transform(vector, &result);
	return result;
}

inline void Matrix4::Transform3(const Vector4& vector, Vector4* result) const
{
	Vector4 ret;
	ret.x = (m[0] * vector.x) + (m[4] * vector.y) + (m[8] * vector.z) + m[12];
	ret.y = (m[1] * vector.x) + (m[5] * vector.y) + (m[9] * vector.z) + m[13];
	ret.z = (m[2] * vector.x) + (m[6] * vector.y) + (m[10] * vector.z) + m[14];
	ret.w = 1.f;
	*result = ret;
}

inline void Matrix4::TransformRotation3(const Vector4& vector, Vector4* result) const
{
	Vector4 ret;
	ret.x = (m[0] * vector.x) + (m[4] * vector.y) + (m[8] * vector.z);
	ret.y = (m[1] * vector.x) + (m[5] * vector.y) + (m[9] * vector.z);
	ret.z = (m[2] * vector.x) + (m[6] * vector.y) + (m[10] * vector.z);
	ret.w = 1.f;
	*result = ret;
}

inline void Matrix4::Transform(const Vector4& vector, Vector4* result) const
{
#ifndef WIN32
	result->x = (m[0] * vector.x) + (m[4] * vector.y) + (m[8] * vector.z) + (m[12] * vector.w);
	result->y = (m[1] * vector.x) + (m[5] * vector.y) + (m[9] * vector.z) + (m[13] * vector.w);
	result->z = (m[2] * vector.x) + (m[6] * vector.y) + (m[10] * vector.z) + (m[14] * vector.w);
	result->w = (m[3] * vector.x) + (m[7] * vector.y) + (m[11] * vector.z) + (m[15] * vector.w);
#else
	// http://www.cortstratton.org/articles/HugiCode.html#bm1
	__asm 
	{
		//mov         esi, [esi]vector.vec
		mov         esi, vector.vec
		mov         edi, result

		// load columns of matrix into xmm4-7
		mov         edx, this
		movups   xmm4, [edx]
		movups   xmm5, [edx+0x10]
		movups   xmm6, [edx+0x20]
		movups   xmm7, [edx+0x30]

		// load v into xmm0.
		movups   xmm0, [esi]

		// we'll store the final result in xmm2; initialize it
		// to zero
		xorps      xmm2, xmm2

		// broadcast x into xmm1, multiply it by the first
		// column of the matrix (xmm4), and add it to the total
		movups   xmm1, xmm0
		shufps   xmm1, xmm1, 0x00
		mulps      xmm1, xmm4
		addps      xmm2, xmm1

		// repeat the process for y, z and w
		movups   xmm1, xmm0
		shufps   xmm1, xmm1, 0x55
		mulps      xmm1, xmm5
		addps      xmm2, xmm1
		movups   xmm1, xmm0
		shufps   xmm1, xmm1, 0xAA
		mulps      xmm1, xmm6
		addps      xmm2, xmm1
		movups   xmm1, xmm0
		shufps   xmm1, xmm1, 0xFF
		mulps      xmm1, xmm7
		addps      xmm2, xmm1

		// write the results to vout
		movups   [edi], xmm2
	}
#endif
}

inline Matrix4 Matrix4::operator*(const Matrix4& rhs) const
{
	return Multiply(rhs);
}

inline Matrix4 Matrix4::operator*(const float rhs) const
{
	Matrix4 ret;

	ret.m[0] = m[0] * rhs;
	ret.m[1] = m[1] * rhs;
	ret.m[2] = m[2] * rhs;
	ret.m[3] = m[3] * rhs;

	ret.m[4] = m[4] * rhs;
	ret.m[5] = m[5] * rhs;
	ret.m[6] = m[6] * rhs;
	ret.m[7] = m[7] * rhs;

	ret.m[8] = m[8] * rhs;
	ret.m[9] = m[9] * rhs;
	ret.m[10] = m[10] * rhs;
	ret.m[11] = m[11] * rhs;

	ret.m[12] = m[12] * rhs;
	ret.m[13] = m[13] * rhs;
	ret.m[14] = m[14] * rhs;
	ret.m[15] = m[15] * rhs;

	return ret;
}

inline Matrix4 Matrix4::MirrorMatrix(const Matrix4& matrixToMirror) const
{
	// NOTE: try changing mirroring around y to mirroring around z!
	Matrix4 mirror(MI_Mirror);
	Matrix4 temp = (mirror * (*this));
	Matrix4 temp2 = matrixToMirror * temp; // * temp;
	return temp2;
}

inline Matrix4 Matrix4::Multiply3(const Matrix4& rhs) const
{
	Matrix4 ret;

	ret.m[0] = (rhs.m[0] * m[0]) + (rhs.m[4] * m[1]) + (rhs.m[8] * m[2]);
	ret.m[1] = (rhs.m[1] * m[0]) + (rhs.m[5] * m[1]) + (rhs.m[9] * m[2]);
	ret.m[2] = (rhs.m[2] * m[0]) + (rhs.m[6] * m[1]) + (rhs.m[10] * m[2]);
	ret.m[3] = (rhs.m[3] * m[0]) + (rhs.m[7] * m[1]) + (rhs.m[11] * m[2]);

	ret.m[4] = (rhs.m[0] * m[4]) + (rhs.m[4] * m[5]) + (rhs.m[8] * m[6]);
	ret.m[5] = (rhs.m[1] * m[4]) + (rhs.m[5] * m[5]) + (rhs.m[9] * m[6]);
	ret.m[6] = (rhs.m[2] * m[4]) + (rhs.m[6] * m[5]) + (rhs.m[10] * m[6]);
	ret.m[7] = (rhs.m[3] * m[4]) + (rhs.m[7] * m[5]) + (rhs.m[11] * m[6]);

	ret.m[8] = (rhs.m[0] * m[8]) + (rhs.m[4] * m[9]) + (rhs.m[8] * m[10]);
	ret.m[9] = (rhs.m[1] * m[8]) + (rhs.m[5] * m[9]) + (rhs.m[9] * m[10]);
	ret.m[10] = (rhs.m[2] * m[8]) + (rhs.m[6] * m[9]) + (rhs.m[10] * m[10]);
	ret.m[11] = (rhs.m[3] * m[8]) + (rhs.m[7] * m[9]) + (rhs.m[11] * m[10]);

	ret.m[12] = m[12];
	ret.m[13] = m[13];
	ret.m[14] = m[14];
	ret.m[15] = m[15];

	return ret;
}

inline Matrix4 Matrix4::Multiply(const Matrix4& rhs) const
{
	Matrix4 ret;

	ret.m[0] = (rhs.m[0] * m[0]) + (rhs.m[4] * m[1]) + (rhs.m[8] * m[2]) + (rhs.m[12] * m[3]);
	ret.m[1] = (rhs.m[1] * m[0]) + (rhs.m[5] * m[1]) + (rhs.m[9] * m[2]) + (rhs.m[13] * m[3]);
	ret.m[2] = (rhs.m[2] * m[0]) + (rhs.m[6] * m[1]) + (rhs.m[10] * m[2]) + (rhs.m[14] * m[3]);
	ret.m[3] = (rhs.m[3] * m[0]) + (rhs.m[7] * m[1]) + (rhs.m[11] * m[2]) + (rhs.m[15] * m[3]);

	ret.m[4] = (rhs.m[0] * m[4]) + (rhs.m[4] * m[5]) + (rhs.m[8] * m[6]) + (rhs.m[12] * m[7]);
	ret.m[5] = (rhs.m[1] * m[4]) + (rhs.m[5] * m[5]) + (rhs.m[9] * m[6]) + (rhs.m[13] * m[7]);
	ret.m[6] = (rhs.m[2] * m[4]) + (rhs.m[6] * m[5]) + (rhs.m[10] * m[6]) + (rhs.m[14] * m[7]);
	ret.m[7] = (rhs.m[3] * m[4]) + (rhs.m[7] * m[5]) + (rhs.m[11] * m[6]) + (rhs.m[15] * m[7]);

	ret.m[8] = (rhs.m[0] * m[8]) + (rhs.m[4] * m[9]) + (rhs.m[8] * m[10]) + (rhs.m[12] * m[11]);
	ret.m[9] = (rhs.m[1] * m[8]) + (rhs.m[5] * m[9]) + (rhs.m[9] * m[10]) + (rhs.m[13] * m[11]);
	ret.m[10] = (rhs.m[2] * m[8]) + (rhs.m[6] * m[9]) + (rhs.m[10] * m[10]) + (rhs.m[14] * m[11]);
	ret.m[11] = (rhs.m[3] * m[8]) + (rhs.m[7] * m[9]) + (rhs.m[11] * m[10]) + (rhs.m[15] * m[11]);

	ret.m[12] = (rhs.m[0] * m[12]) + (rhs.m[4] * m[13]) + (rhs.m[8] * m[14]) + (rhs.m[12] * m[15]);
	ret.m[13] = (rhs.m[1] * m[12]) + (rhs.m[5] * m[13]) + (rhs.m[9] * m[14]) + (rhs.m[13] * m[15]);
	ret.m[14] = (rhs.m[2] * m[12]) + (rhs.m[6] * m[13]) + (rhs.m[10] * m[14]) + (rhs.m[14] * m[15]);
	ret.m[15] = (rhs.m[3] * m[12]) + (rhs.m[7] * m[13]) + (rhs.m[11] * m[14]) + (rhs.m[15] * m[15]);

	return ret;
}

inline bool Matrix4::operator==(const Matrix4& rhs) const
{
	return rhs.m[0] == m[0] && rhs.m[1] == m[1] && rhs.m[2] == m[2] && rhs.m[3] == m[3]
			&& rhs.m[4] == m[4] && rhs.m[5] == m[5] && rhs.m[6] == m[6] && rhs.m[7] == m[7]
			&& rhs.m[8] == m[8] && rhs.m[9] == m[9] && rhs.m[10] == m[10] && rhs.m[11] == m[11]
			&& rhs.m[12] == m[12] && rhs.m[13] == m[13] && rhs.m[14] == m[14] && rhs.m[15] == m[15];
}

inline bool Matrix4::operator!=(const Matrix4& rhs) const
{
	return !operator==(rhs);
}

inline void Matrix4::operator=(const Matrix4& rhs)
{
	m[0] = rhs.m[0];
	m[1] = rhs.m[1];
	m[2] = rhs.m[2];
	m[3] = rhs.m[3];
	m[4] = rhs.m[4];
	m[5] = rhs.m[5];
	m[6] = rhs.m[6];
	m[7] = rhs.m[7];
	m[8] = rhs.m[8];
	m[9] = rhs.m[9];
	m[10] = rhs.m[10];
	m[11] = rhs.m[11];
	m[12] = rhs.m[12];
	m[13] = rhs.m[13];
	m[14] = rhs.m[14];
	m[15] = rhs.m[15];
}

inline const Vector4& Matrix4::Column(int idx) const
{
	return *(Vector4*)mat[idx];
}

inline Vector4& Matrix4::Column(int idx)
{
	return *(Vector4*)mat[idx];
}

inline const Vector4& Matrix4::GetTranslation() const
{
	return *(Vector4*)mat[3];
}

inline void Matrix4::CreateLookAt(const Vector4& pos, const Vector4& target)
{
	Vector4 up = Vector4(0.f, 1.f, 0.f, 0.f);
	Vector4 view = target - pos;
	view.Normalize();

	Vector4 right = view.Cross(up);
	right.Normalize();

	up = right.Cross(view);
	up.Normalize();

	Column(0) = right;
	Column(1) = up;
	Column(2) = view;
	SetTranslation(pos);
}

inline void Matrix4::CreateView(const Matrix4& world)
{
	Vector4 right = world.GetXAxis();
	Vector4 up = world.GetYAxis();
	Vector4 view = -world.GetZAxis();
	Vector4 position = world.GetTranslation();

	SetIdentity();

	// set the camera view matrix
	m[ 0] = right.x;
	m[ 4] = right.y;
	m[ 8] = right.z;

	m[ 1] = up.x;
	m[ 5] = up.y;
	m[ 9] = up.z;

	m[ 2] = view.x;
	m[ 6] = view.y;
	m[10] = view.z;

	m[12] = -position.Dot(right);
	m[13] = -position.Dot(up);
	m[14] = -position.Dot(view);

}

inline Matrix4	Matrix4::GetTranspose() const
{
	Matrix4 temp = (*this);
	temp[4] = m[1];
	temp[1] = m[4];

	temp[8] = m[2];
	temp[2] = m[8];

	temp[9] = m[6];
	temp[6] = m[9];

	temp[12] = m[3];
	temp[3] = m[12];

	temp[13] = m[7];
	temp[7] = m[13];

	temp[14] = m[11];
	temp[11] = m[14];

	return temp;
}

inline void	Matrix4::Transpose()
{
	*this = GetTranspose();
}

inline Matrix4 Matrix4::GetQuickInverse() const
{
	Vector4 position = Vector4(m[12], m[13], m[14]);
	Vector4 invPos = Vector4(
			-((position.x * m[0]) + (position.y * m[4]) + (position.z * m[8])),
			-((position.x * m[1]) + (position.y * m[5]) + (position.z * m[9])),
			-((position.x * m[2]) + (position.y * m[6]) + (position.z * m[10])));

	Matrix4 ret = GetTranspose();
	ret.SetTranslation(invPos);
	return ret;
}

inline void Matrix4::QuickInverse()
{
	*this = GetQuickInverse();
}

inline Matrix4 Matrix4::GetInverse() const
{
	Matrix4 ret;
	Matrix4 adjoint = GetAdjoint();
	float invDet = 1.0f / GetDeterminant();

	for (int i = 0; i < 16; ++i)
		ret[i] = invDet * adjoint[i]; //adjoint[i] * det;

	return ret;
}

inline void Matrix4::Inverse()
{
	*this = GetInverse();
}

inline float Matrix4::GetOneNorm() const
{
    float sum = 0.f;
    float max = 0.f;
    for (int i = 0; i < 3; ++i) 
	{
		sum = fabs(mat[i][0]) + fabs(mat[i][1]) + fabs(mat[i][2]);

		if (max < sum) 
			max = sum;
    }

    return max;
}

inline float Matrix4::GetInfiniteNorm() const
{
	float sum = 0.f;
    float max = 0.f;
    for (int i = 0; i < 3; ++i) 
	{
		sum = fabs(mat[0][i]) + fabs(mat[1][i]) + fabs(mat[2][i]);

		if (max < sum) 
			max = sum;
    }

	return max;
}

inline float Matrix4::GetDeterminant() const
{
	// http://www.cvl.iis.u-tokyo.ac.jp/~miyazaki/tech/teche23.html

	return (mat[0][0] * mat[1][1] * mat[2][2] * mat[3][3]) + (mat[0][0] * mat[1][2] * mat[2][3] * mat[3][1]) + (mat[0][0] * mat[1][3] * mat[2][1] * mat[3][2])
		 + (mat[0][1] * mat[1][0] * mat[2][3] * mat[3][2]) + (mat[0][1] * mat[1][2] * mat[2][0] * mat[3][3]) + (mat[0][1] * mat[1][3] * mat[2][2] * mat[3][0])
		 + (mat[0][2] * mat[1][0] * mat[2][1] * mat[3][3]) + (mat[0][2] * mat[1][1] * mat[2][3] * mat[3][0]) + (mat[0][2] * mat[1][3] * mat[2][0] * mat[3][1])
		 + (mat[0][3] * mat[1][0] * mat[2][2] * mat[3][1]) + (mat[0][3] * mat[1][1] * mat[2][0] * mat[3][2]) + (mat[0][3] * mat[1][2] * mat[2][1] * mat[3][0])
		 - (mat[0][0] * mat[1][1] * mat[2][3] * mat[3][2]) - (mat[0][0] * mat[1][2] * mat[2][1] * mat[3][3]) - (mat[0][0] * mat[1][3] * mat[2][2] * mat[3][1])
		 - (mat[0][1] * mat[1][0] * mat[2][2] * mat[3][3]) - (mat[0][1] * mat[1][2] * mat[2][3] * mat[3][0]) - (mat[0][1] * mat[1][3] * mat[2][0] * mat[3][2])
		 - (mat[0][2] * mat[1][0] * mat[2][3] * mat[3][1]) - (mat[0][2] * mat[1][1] * mat[2][0] * mat[3][3]) - (mat[0][2] * mat[1][3] * mat[2][1] * mat[3][0])
		 - (mat[0][3] * mat[1][0] * mat[2][1] * mat[3][2]) - (mat[0][3] * mat[1][1] * mat[2][2] * mat[3][0]) - (mat[0][3] * mat[1][2] * mat[2][0] * mat[3][1]);
	/*
	// http://www.cs.virginia.edu/~cs551dl/assignment3/matinv.c

	Matrix3 a = GetMatrix3(0, 0);
	Matrix3 b = GetMatrix3(0, 1);
	Matrix3 c = GetMatrix3(0, 2);
	Matrix3 d = GetMatrix3(0, 3);

	return (m[0] * a.Determinant()) - (m[4] * b.Determinant()) + (m[8] * c.Determinant()) - (m[12] * d.Determinant());*/
}

inline Matrix4 Matrix4::GetAdjoint() const
{/*
	Matrix4 ret;
	
	ret.mat[0][0] = (mat[1][1] * mat[2][2] * mat[3][3]) + (mat[1][2] * mat[2][3] * mat[3][1]) + (mat[1][3] * mat[2][1] * mat[3][2]) - (mat[1][1] * mat[2][3] * mat[3][2]) - (mat[1][2] * mat[2][1] * mat[3][3]) - (mat[1][3] * mat[2][2] * mat[3][1]);
	ret.mat[0][1] = (mat[0][1] * mat[2][3] * mat[2][3]) + (mat[0][2] * mat[2][1] * mat[3][3]) + (mat[0][1] * mat[2][2] * mat[3][3]) - (mat[0][2] * mat[2][2] * mat[3][3]) - (mat[0][2] * mat[2][3] * mat[3][1]) - (mat[0][3] * mat[2][1] * mat[3][2]);
	ret.mat[0][2] = (mat[0][1] * mat[1][2] * mat[3][3]) + (mat[0][2] * mat[1][3] * mat[3][1]) + (mat[0][3] * mat[1][1] * mat[3][2]) - (mat[0][1] * mat[1][3] * mat[3][2]) - (mat[0][2] * mat[1][1] * mat[3][3]) - (mat[0][3] * mat[1][2] * mat[3][1]);
	ret.mat[0][3] = (mat[0][1] * mat[1][3] * mat[2][2]) + (mat[0][2] * mat[1][1] * mat[2][3]) + (mat[0][3] * mat[1][2] * mat[2][1]) - (mat[0][1] * mat[1][2] * mat[2][3]) - (mat[0][2] * mat[1][3] * mat[2][1]) - (mat[0][3] * mat[1][1] * mat[2][2]);
	ret.mat[1][0] = (mat[1][0] * mat[2][3] * mat[3][2]) + (mat[1][2] * mat[2][0] * mat[3][3]) + (mat[1][3] * mat[2][2] * mat[3][0]) - (mat[1][0] * mat[2][2] * mat[3][3]) - (mat[1][2] * mat[2][3] * mat[3][0]) - (mat[1][3] * mat[2][0] * mat[3][2]);
	ret.mat[1][1] = (mat[0][0] * mat[2][2] * mat[3][3]) + (mat[0][2] * mat[2][3] * mat[3][0]) + (mat[0][3] * mat[2][0] * mat[3][2]) - (mat[0][0] * mat[2][3] * mat[3][2]) - (mat[0][2] * mat[2][0] * mat[3][3]) - (mat[0][3] * mat[2][2] * mat[3][0]);
	ret.mat[1][2] = (mat[0][0] * mat[1][3] * mat[3][2]) + (mat[0][2] * mat[1][0] * mat[3][3]) + (mat[0][3] * mat[1][2] * mat[3][0]) - (mat[0][0] * mat[1][2] * mat[3][3]) - (mat[0][2] * mat[1][3] * mat[3][0]) - (mat[0][3] * mat[1][0] * mat[3][2]);
	ret.mat[1][3] = (mat[0][0] * mat[1][2] * mat[2][3]) + (mat[0][2] * mat[1][3] * mat[2][0]) + (mat[0][3] * mat[1][0] * mat[2][2]) - (mat[0][0] * mat[1][3] * mat[2][2]) - (mat[0][2] * mat[1][0] * mat[2][3]) - (mat[0][3] * mat[1][2] * mat[2][0]);
	ret.mat[2][0] = (mat[1][0] * mat[2][1] * mat[3][3]) + (mat[1][1] * mat[2][3] * mat[3][0]) + (mat[1][3] * mat[2][0] * mat[3][1]) - (mat[1][0] * mat[2][3] * mat[3][1]) - (mat[1][1] * mat[2][0] * mat[3][3]) - (mat[1][3] * mat[2][1] * mat[3][0]);
	ret.mat[2][1] = (mat[0][0] * mat[2][3] * mat[3][1]) + (mat[0][1] * mat[2][0] * mat[3][3]) + (mat[0][3] * mat[2][1] * mat[3][0]) - (mat[0][0] * mat[2][1] * mat[3][3]) - (mat[0][1] * mat[2][3] * mat[3][0]) - (mat[0][3] * mat[2][0] * mat[3][1]);
	ret.mat[2][2] = (mat[0][0] * mat[1][1] * mat[3][3]) + (mat[0][1] * mat[1][3] * mat[3][0]) + (mat[0][3] * mat[1][0] * mat[3][1]) - (mat[0][0] * mat[1][3] * mat[3][1]) - (mat[0][1] * mat[1][0] * mat[3][3]) - (mat[0][3] * mat[1][1] * mat[3][0]);
	ret.mat[2][3] = (mat[0][0] * mat[1][3] * mat[2][1]) + (mat[0][1] * mat[1][0] * mat[2][3]) + (mat[0][3] * mat[1][1] * mat[2][0]) - (mat[0][0] * mat[1][1] * mat[2][3]) - (mat[0][1] * mat[1][3] * mat[2][0]) - (mat[0][3] * mat[1][0] * mat[2][1]);
	ret.mat[3][0] = (mat[1][0] * mat[2][2] * mat[3][1]) + (mat[1][1] * mat[2][0] * mat[3][2]) + (mat[1][2] * mat[2][1] * mat[3][0]) - (mat[1][0] * mat[2][1] * mat[3][2]) - (mat[1][1] * mat[2][2] * mat[3][0]) - (mat[1][2] * mat[2][0] * mat[3][1]);
	ret.mat[3][1] = (mat[0][0] * mat[2][1] * mat[3][2]) + (mat[0][1] * mat[2][2] * mat[3][0]) + (mat[0][2] * mat[2][0] * mat[3][1]) - (mat[0][0] * mat[2][2] * mat[3][2]) - (mat[0][1] * mat[2][0] * mat[3][2]) - (mat[0][2] * mat[2][1] * mat[3][0]);
	ret.mat[3][2] = (mat[0][0] * mat[1][2] * mat[3][1]) + (mat[0][1] * mat[1][0] * mat[3][2]) + (mat[0][2] * mat[1][1] * mat[3][0]) - (mat[0][0] * mat[1][1] * mat[3][2]) - (mat[0][1] * mat[1][2] * mat[3][0]) - (mat[0][2] * mat[1][0] * mat[3][1]);
	ret.mat[3][3] = (mat[0][0] * mat[1][1] * mat[2][2]) + (mat[0][1] * mat[1][2] * mat[2][0]) + (mat[0][2] * mat[1][0] * mat[2][1]) - (mat[0][0] * mat[1][2] * mat[2][1]) - (mat[0][1] * mat[1][0] * mat[2][2]) - (mat[0][2] * mat[1][1] * mat[2][0]);
*/

	
	// http://www.cs.virginia.edu/~cs551dl/assignment3/matinv.c

	Matrix4 ret;
	Matrix3 mat3;

	// we are effectivly doing a transpose here as well

	// column 0
	mat3 = GetMatrix3(0, 0);
	ret[0] = mat3.GetDeterminant();

	mat3 = GetMatrix3(1, 0);
	ret[4] = -mat3.GetDeterminant();

	mat3 = GetMatrix3(2, 0);
	ret[8] = mat3.GetDeterminant();

	mat3 = GetMatrix3(3, 0);
	ret[12] = -mat3.GetDeterminant();

	// column 1
	mat3 = GetMatrix3(0, 1);
	ret[1] = -mat3.GetDeterminant();

	mat3 = GetMatrix3(1, 1);
	ret[5] = mat3.GetDeterminant();

	mat3 = GetMatrix3(2, 1);
	ret[9] = -mat3.GetDeterminant();

	mat3 = GetMatrix3(3, 1);
	ret[13] = mat3.GetDeterminant();

	// column 2
	mat3 = GetMatrix3(0, 2);
	ret[2] = mat3.GetDeterminant();

	mat3 = GetMatrix3(1, 2);
	ret[6] = -mat3.GetDeterminant();

	mat3 = GetMatrix3(2, 2);
	ret[10] = mat3.GetDeterminant();

	mat3 = GetMatrix3(3, 2);
	ret[14] = -mat3.GetDeterminant();

	// column 3
	mat3 = GetMatrix3(0, 3);
	ret[3] = -mat3.GetDeterminant();

	mat3 = GetMatrix3(1, 3);
	ret[7] = mat3.GetDeterminant();

	mat3 = GetMatrix3(2, 3);
	ret[11] = -mat3.GetDeterminant();

	mat3 = GetMatrix3(3, 3);
	ret[15] = mat3.GetDeterminant();

	return ret;
}

inline Matrix3 Matrix4::GetMatrix3(int row, int col) const
{
	Matrix3 mat;

	for (int c = 0, i = 0; c < 4; ++c, ++i)
	{
		if (c == col)
		{
			++c;

			if (c >= 4)
				break;
		}

		for (int r = 0, j = 0; r < 4; ++r, ++j)
		{
			if (r == row)
			{
				++r;

				if (r >= 4)
					break;
			}

			mat[i * 3 + j] = m[c * 4 + r];
		}
	}

	return mat;
}

inline void	Matrix4::SetColumn(int idx, const Vector4& vec)
{
	mat[idx][0] = vec.x;
	mat[idx][1] = vec.y;
	mat[idx][2] = vec.z;
}

inline Vector4	Matrix4::GetRow(int idx) const
{
	return Vector4(mat[0][idx], mat[1][idx], mat[2][idx], mat[3][idx]);
}

inline void Matrix4::SetRow(int idx, const Vector4& vec)
{
	mat[0][idx] = vec.x;
	mat[1][idx] = vec.y;
	mat[2][idx] = vec.z;
	mat[3][idx] = vec.w;
}

inline Matrix4 Matrix4::operator-() const
{
	Matrix4 ret;
	for (int i = 0; i < 16; ++i)
		ret[i] = -m[i];

	return ret;
}