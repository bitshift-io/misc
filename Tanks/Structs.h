#ifndef _STRUCTS
#define _STRUCTS

#include <windows.h>
#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include "Timming.h"

//-----------------------------------------------------------------------------
// Name: Structures
//-----------------------------------------------------------------------------
typedef struct COLOR
{
	int red;
	int green;
	int blue;
	int alpha;
	COLOR(int iRed, int iGreen, int iBlue, int iAlpha)
	{	
		red = iRed;
		green = iGreen;
		blue = iBlue;
		alpha = iAlpha;
	}
}COLOR;

typedef struct BMPDATA
{
	LPDIRECT3DSURFACE8 pBitmap;
	int width;
	int height;
}BMPDATA;



//-----------------------------------------------------------------------------
// FNT struct for use in CFONT
//-----------------------------------------------------------------------------
typedef struct FNT
{
	LPDIRECT3DSURFACE8 textSurf;
	long timeAdded;
	long timeToLive;
	RECT* srcRect;
	POINT* dest;
	FNT()
	{
		srcRect = new RECT;
		dest = new POINT;
		timeAdded = (long)CTime::GetTime(); 
		textSurf = 0;
		timeToLive = 0;
	}
}FNT;

//-----------------------------------------------------------------------------
// Class stuffs
//-----------------------------------------------------------------------------

#endif
