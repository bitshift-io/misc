#ifndef _PARTICLE
#define _PARTICLE

// the particle system,
// just contains a list of sprites
// and updates them

#include "Sprite.h"
#include "Object3D.h"

class CParticle : public CObject3D
{

public:
	CParticle( int numParticles, int particlesPerSecond );
	~CParticle();
	void Render();
	void Update();
	virtual void Remove();

	CSprite* particles;
	CSprite* sprite; //lets keep a copy of our sprite and its settings

private:
	int maxParticles;
	int particlesPerSecond;
	int noParticles;
	double gravity;

};


#endif


