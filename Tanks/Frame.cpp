#include "Frame.h"
#include "AdvMesh.h"
#include "CollisionDetection.h"
#include "ErrorLogging.h"

CFrame::CFrame()
{
	pObjList = 0;
	masterFrame = false;
}

void CFrame::CollisionCheck(CObject* pAdvMeshList)
{
	bool createdList = false;

	CObject* meshList = (CObject*)pAdvMeshList;

	//if its null, we are the world/base frame
	if( pAdvMeshList == NULL)
	{
		//create a new list then
		CObject* meshList = NULL;
		createdList = true;

	}

	//list thu all objects
	CObject* unknownObj = pObjList;

	while( unknownObj != NULL )
	{
		if( unknownObj->GetType() == ADVMESH)
		{
			//now add it to the mehsList
			if( meshList == NULL )
			{
				meshList = (CObject*)unknownObj;
			}
			else
			{

				CAdvMesh* listEnd = (CAdvMesh*)meshList;

				while( listEnd->colNext != NULL )
				{
					listEnd = (CAdvMesh*)listEnd->colNext;
				}
				listEnd->colNext = unknownObj;

			}
		}
		if( unknownObj->GetType() == FRAME)
		{
			//Error("ehg?");
			//we found a frame, so call it
			//send it mesh list, this will fill
			// up the list with thier meshes
			//CFrame* nextFrame = (CFrame*)unknownObj;
			//nextFrame->CollisionCheck( meshList );
		}

		unknownObj = unknownObj->next;
	}


	//if we are the function that created the list, its our job to call
	//the "cheack all the collsisions" function in CollisionDetection.h
	if( createdList )
	{
		CheckAllCollisions( meshList );
	}

	//now for the all important killing of the list
	CAdvMesh* listPrev = (CAdvMesh*)meshList;
	CAdvMesh* listEnd = (CAdvMesh*)meshList;

	while( listEnd->colNext != NULL )
	{
		listPrev = listEnd;
		listEnd = (CAdvMesh*)listEnd->colNext;
		listPrev->colNext = NULL;
	}
	//listEnd->colNext = unknownObj;


}

void CFrame::AddObject( CObject *pNewObject )
{
	pNewObject->SetParent( this );

	CObject* newNode;
	newNode = pNewObject;

	if( pObjList == NULL )
	{
		pObjList = newNode;
	}
	else
	{
		CObject* curNode = pObjList;

		while( curNode->next != NULL )
		{
			curNode = curNode->next;
		}
		curNode->next = newNode;
	}
}

void CFrame::RemoveObject( CObject *pNewObject )
{
	if( pObjList == NULL )
		return;

	CObject* curNode = pObjList;
	CObject* temp;

    while( curNode->next != pNewObject && curNode->next != NULL )
	{
		curNode = curNode->next;
	}

	if( curNode->next == 0 )
		return;

	if( curNode->next )
	{
		if( curNode->next->next != 0)
		{
			curNode->next = curNode->next->next;
		}
		else
		{
			curNode->next = 0;
		}

		//delete pNewObject; //not like this
	}

}

void CFrame::RemoveDestroy()
{
	if( pObjList == NULL )
		return;

	CObject* curNode = pObjList;
    CObject* prevNode = curNode;
	CObject* delNode = 0;

	while( curNode != NULL )
	{
		if( curNode->bDestroy )
		{
			if( curNode->next != 0 )
			{
				prevNode->next = curNode->next;
				delNode = curNode;
				curNode = curNode->next;
			}
			else
			{
				prevNode->next = 0;
				delNode = curNode;
			}
			
			delete delNode;
		}

		prevNode = curNode;
		curNode = curNode->next;
	}
}

void CFrame::Update()
{
	CObject3D::Update();
	RemoveDestroy();
}

void CFrame::Render()
{
	Update();

	//if( masterFrame )
	//	CollisionCheck(NULL);

	CObject* tempObj = pObjList;

	while( tempObj != NULL )
	{
		tempObj->Render();
		tempObj = tempObj->next;
	}
}

void CFrame::Remove()
{
	//clean up code, removeing is killing, if u want
	//to change parents dont call this! call SetParent

	//for layers and frames, we have to "kill" off the children first :)
	CObject* curNode = pObjList;
	CObject* prevNode = pObjList;

	while( curNode != 0 )
	{
		prevNode = curNode;
		curNode = curNode->next;

		prevNode->Remove();
	}

	//then clean up our own stuff
	CallbackFn = 0;
	next = 0;
	parent = 0;

	delete pObjList;
	pObjList = NULL;

	delete this;
}
