md build\data
copy data build\data\ 

md build\models
copy models\*.X build\models\ 
copy models\*.3ds build\models\ 
copy models\*.bmp build\models\

md build\sounds
copy sounds\ build\sounds\ 

md build\textures
copy textures\ build\textures\ 

md build\docs

copy debug\*.exe build\


pause