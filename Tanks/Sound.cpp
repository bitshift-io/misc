#include "Sound.h"
#include <dsound.h>
#include "DirectSound.h"
#include "ErrorLogging.h"

CSound::CSound(char* fileName)
{
	soundBuffer=LoadWAV(fileName,g_pSound);
}

void CSound::Play()
{
	soundBuffer->Play(0,0,0);
}

void CSound::PlayLooping()
{
	soundBuffer->Play(0,0,DSBPLAY_LOOPING);
}

void CSound::Stop()
{
}

bool CSound::IsPlaying()
{
	/*
	LPDWORD pdwStatus; 
	soundBuffer->GetStatus(pdwStatus);

	//eg: 0x001001 & 0x000001 == 0x000001
	if( *pdwStatus & DSBSTATUS_PLAYING == DSBSTATUS_PLAYING )
	{
		return true;
	}

	//Error("we are palying a sound");*/
	return false;
}
