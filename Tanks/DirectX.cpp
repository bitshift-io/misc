
#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include "Tanks.h"
#include "DirectX.h"
#include "ErrorLogging.h"
#include "DataManip.h"
#include "Windows.h"


LPDIRECT3DSURFACE8 g_pBackSurf = 0;
//LPDIRECT3DSURFACE8 g_pBmpSurf = 0;
LPDIRECT3D8             g_pD3D       = NULL; // Used to create the D3DDevice
LPDIRECT3DDEVICE8       g_pd3dDevice = NULL; // Our rendering device
LPDIRECT3DVERTEXBUFFER8 g_pVB        = NULL; // Buffer to hold vertices

LPDIRECT3DTEXTURE8 g_pDefaultTexture = 0;
DWORD g_copyFilter = D3DX_FILTER_LINEAR | D3DX_FILTER_DITHER;
DWORD g_loadBmp = D3DX_FILTER_LINEAR | D3DX_FILTER_DITHER;

//GLOBALS
int g_deviceWidth = 640;
int g_deviceHeight = 480;
bool g_fullScreen = true;
bool g_screenSaver = false; //make it so all keys quit

//paths to data stuffs
char* g_meshPath;
char* g_meshTexturePath; 
char* g_texturesPath;
char* g_soundsPath;
char* g_dataPath;
char* g_imageDumpsPath;


//-----------------------------------------------------------------------------
// ShutdownD3D
//-----------------------------------------------------------------------------
VOID ShutdownD3D()
{
    if( g_pVB != NULL )
        g_pVB->Release();

    if( g_pd3dDevice != NULL )
        g_pd3dDevice->Release();

    if( g_pD3D != NULL )
        g_pD3D->Release();
}

//-----------------------------------------------------------------------------
// InitD3D
//-----------------------------------------------------------------------------
HRESULT InitD3D( HWND hWnd )
{
	//Error("should be here!");
	HRESULT r=0;

    // Create the D3D object.
    if( NULL == ( g_pD3D = Direct3DCreate8( D3D_SDK_VERSION ) ) )
	{
		Error("cant create D3D object");
        return E_FAIL;
	}

    // Get the current desktop display mode, so we can set up a back
    // buffer of the same format
    D3DDISPLAYMODE d3ddm;
    if( FAILED( g_pD3D->GetAdapterDisplayMode( D3DADAPTER_DEFAULT, &d3ddm ) ) )
	{
		Error("Failed to get display mode");
        return E_FAIL;
	}

    // Set up the structure used to create the D3DDevice
    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory( &d3dpp, sizeof(d3dpp) );
    d3dpp.Windowed = g_fullScreen ? FALSE : TRUE;
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.MultiSampleType = D3DMULTISAMPLE_NONE;
	d3dpp.FullScreen_PresentationInterval = g_fullScreen ? D3DPRESENT_INTERVAL_IMMEDIATE : 0;
    d3dpp.BackBufferFormat = d3ddm.Format;
    d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.BackBufferFormat = g_fullScreen ? SURFACEFORMAT : d3ddm.Format;
	d3dpp.BackBufferCount = 1;
	d3dpp.BackBufferWidth = (UINT)g_deviceWidth;
	d3dpp.BackBufferHeight = (UINT)g_deviceHeight;

	d3dpp.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;

    // Create the D3DDevice
    if( FAILED( g_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
                                      D3DCREATE_SOFTWARE_VERTEXPROCESSING,
                                      &d3dpp, &g_pd3dDevice ) ) )
    {
		Error("can't create device");
        return E_FAIL;
    }

	// Get backbuffer
	r = g_pd3dDevice->GetBackBuffer(0,D3DBACKBUFFER_TYPE_MONO,&g_pBackSurf);
	if(FAILED(r))
	{
		Error("Failed to get back buffer");
		MessageBox(hWnd, "back buf","error",MB_OK);
	}


    g_pd3dDevice->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_ONE);
    g_pd3dDevice->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_ONE);

	//g_pd3dDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	//g_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	//g_pd3dDevice->SetRenderState(D3DRS_ALPHAREF, (DWORD)0x000000FF);
	//g_pd3dDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

	g_pd3dDevice->SetRenderState( D3DRS_DITHERENABLE, TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_ZENABLE, D3DZB_TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_SPECULARENABLE, TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_NORMALIZENORMALS, TRUE );

	g_pd3dDevice->SetRenderState( 	D3DRS_EDGEANTIALIAS, TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_AMBIENT, 0x6f6f6f6f );


	g_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
    g_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW ); //temporary disable culling
    g_pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR  );
    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_MINFILTER, D3DTEXF_LINEAR  );




	//g_pd3dDevice->SetRenderState( D3DRS_FILLMODE, D3DFILL_WIREFRAME );




    return S_OK;
}



HRESULT InitD3D()
{
	HRESULT r=0;

    // Create the D3D object.
    if( NULL == ( g_pD3D = Direct3DCreate8( D3D_SDK_VERSION ) ) )
        return E_FAIL;

    // Get the current desktop display mode, so we can set up a back
    // buffer of the same format
    D3DDISPLAYMODE d3ddm;
    if( FAILED( g_pD3D->GetAdapterDisplayMode( D3DADAPTER_DEFAULT, &d3ddm ) ) )
        return E_FAIL;

    // Set up the structure used to create the D3DDevice
    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory( &d3dpp, sizeof(d3dpp) );
    d3dpp.Windowed = g_fullScreen ? FALSE : TRUE;
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.MultiSampleType = D3DMULTISAMPLE_NONE;
	d3dpp.FullScreen_PresentationInterval = g_fullScreen ? D3DPRESENT_INTERVAL_IMMEDIATE : 0;
    d3dpp.BackBufferFormat = d3ddm.Format;
    d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.BackBufferFormat = g_fullScreen ? SURFACEFORMAT : d3ddm.Format;
	d3dpp.BackBufferCount = 1;
	d3dpp.BackBufferWidth = (UINT)g_deviceWidth;
	d3dpp.BackBufferHeight = (UINT)g_deviceHeight;

	d3dpp.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;

    // Create the D3DDevice
    if( FAILED( g_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, g_hWnd,
                                      D3DCREATE_SOFTWARE_VERTEXPROCESSING,
                                      &d3dpp, &g_pd3dDevice ) ) )
    {
		Error("ops");
        return E_FAIL;
    }

	// Get backbuffer
	r = g_pd3dDevice->GetBackBuffer(0,D3DBACKBUFFER_TYPE_MONO,&g_pBackSurf);
	if(FAILED(r))
	{
		MessageBox(g_hWnd, "back buf","error",MB_OK);
	}


    g_pd3dDevice->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_ONE);
    g_pd3dDevice->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_ONE);

	g_pd3dDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	g_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	g_pd3dDevice->SetRenderState(D3DRS_ALPHAREF, (DWORD)0x000000FF);
	g_pd3dDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

	g_pd3dDevice->SetRenderState( D3DRS_DITHERENABLE, TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_ZENABLE, D3DZB_TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_SPECULARENABLE, TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_NORMALIZENORMALS, TRUE );

	g_pd3dDevice->SetRenderState( 	D3DRS_EDGEANTIALIAS, TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_AMBIENT, 0x6f6f6f6f );


	g_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
    g_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW ); //temporary disable culling
    g_pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR  );
    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_MINFILTER, D3DTEXF_LINEAR  );




	//g_pd3dDevice->SetRenderState( D3DRS_FILLMODE, D3DFILL_WIREFRAME );




    return S_OK;
}

//-----------------------------------------------------------------------------
// Name: CopySurface
//-----------------------------------------------------------------------------
HRESULT CopySurface(LPDIRECT3DSURFACE8 *pSrcSurf, RECT *srcRect,
				 LPDIRECT3DSURFACE8 *pDestSurf, RECT *destRect)
{
	HRESULT r = 0;
	r = D3DXLoadSurfaceFromSurface( *pDestSurf, NULL, destRect, *pSrcSurf,
			NULL, srcRect, g_copyFilter, NULL);
	if(FAILED(r))
	{
		MessageBox(g_hWnd, "copy surface","error",MB_OK);
	}


	return S_OK;
}

HRESULT CopySurfaceFast(LPDIRECT3DSURFACE8 *pSrcSurf, RECT *srcRect,
						LPDIRECT3DSURFACE8 *pDestSurf, POINT *destPoint)
{
	HRESULT r = 0;
	r = g_pd3dDevice->CopyRects(*pSrcSurf,srcRect, 1, *pDestSurf ,destPoint);
	if(FAILED(r))
	{
		MessageBox(g_hWnd,
		"copy surface fast\n make sure srcRect and destPoint are not NULL"
		,"error",MB_OK);
		return S_FALSE;
	}

	return S_OK;
}

//-----------------------------------------------------------------------------
// Name: LoadBmp
//-----------------------------------------------------------------------------
HRESULT LoadBmp(char *path, BMPDATA *pBmpData, LPDIRECT3DSURFACE8 *pDestSurf)
{
	HRESULT r;
	HBITMAP hBitmap;
	BITMAP Bitmap;

	//now add the textures dir to the path
	path = (char*)StringCat(g_texturesPath, path);


	hBitmap = (HBITMAP)LoadImage(NULL, path, IMAGE_BITMAP,
		0, 0, LR_LOADFROMFILE | LR_CREATEDIBSECTION);

	if( hBitmap == NULL )
	{
		Error("Unable to load bitmap");
		//return FAIL;
	}

	GetObject(hBitmap, sizeof(BITMAP), &Bitmap);
	DeleteObject(hBitmap);

	//if the user doesnt want us to fill out the BMPDATA
	//we wont
	if( pBmpData != NULL )
	{
		pBmpData->height = Bitmap.bmHeight;
		pBmpData->width = Bitmap.bmWidth;

		r = g_pd3dDevice->CreateImageSurface( Bitmap.bmWidth, Bitmap.bmHeight,
			SURFACEFORMAT, &pBmpData->pBitmap);
		if( FAILED(r) )
		{
			Error("Unable to create surface for bitmap load");
			//return FAIL;
		}

		r = D3DXLoadSurfaceFromFile(pBmpData->pBitmap, NULL, NULL, path,
			NULL, g_loadBmp, g_colorKey, NULL);
		if( FAILED(r) )
		{
			Error("Unable to load file to surface");
			//return FAIL;
		}
	}

	//if we are given a surface fill it, or let the user
	// do what they want
	if( pDestSurf != NULL)
	{
		r = g_pd3dDevice->CreateImageSurface( Bitmap.bmWidth, Bitmap.bmHeight,
		SURFACEFORMAT, pDestSurf);
		if( FAILED(r) )
		{
			Error("Unable to load file to surface");
			//return FAIL;
		}

		r = D3DXLoadSurfaceFromFile(*pDestSurf, NULL, NULL, path,
			NULL, g_loadBmp, g_colorKey, NULL);
		if( FAILED(r) )
		{
			Error("Unable to load file to surface");
			//return FAIL;
		}
	}

	return S_OK;
}
