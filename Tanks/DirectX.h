#ifndef _DIRECTX
#define _DIRECTX

#include <windows.h>
#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include "Structs.h"

//-----------------------------------------------------------------------------
// Global variables
//-----------------------------------------------------------------------------
#define SURFACEFORMAT D3DFMT_A8R8G8B8

extern LPDIRECT3DSURFACE8 g_pBackSurf;
//LPDIRECT3DSURFACE8 g_pBmpSurf = 0;
extern LPDIRECT3D8             g_pD3D; // Used to create the D3DDevice
extern LPDIRECT3DDEVICE8       g_pd3dDevice; // Our rendering device
extern LPDIRECT3DVERTEXBUFFER8 g_pVB; // Buffer to hold vertices

extern LPDIRECT3DTEXTURE8 g_pDefaultTexture;
extern DWORD g_copyFilter;
extern DWORD g_loadBmp;

extern int g_deviceWidth;
extern int g_deviceHeight;
extern bool g_fullScreen;
extern bool g_screenSaver; //make it so all keys quit

//paths to data stuffs
extern char* g_meshPath;
extern char* g_meshTexturePath; 
extern char* g_texturesPath;
extern char* g_soundsPath;
extern char* g_dataPath;
extern char* g_imageDumpsPath;

#define g_colorKey 0xFFFF00FF

// A structure for our custom vertex type
#pragma pack(push, 1)
typedef struct CUSTOMVERTEX
{
	D3DXVECTOR3 point;
    //FLOAT x, y, z; // The transformed position for the vertex
	//D3DXVECTOR3 normal;
	//FLOAT nx, ny, nz;
	FLOAT tu, tv;   // The texture coordinates.

} CUSTOMVERTEX;
#pragma pack(pop)

// Our custom FVF, which describes our custom vertex structure
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ | D3DFVF_TEX1 )
//#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZRHW | D3DFVF_TEX1 )

// This is for collision detect
#pragma pack(push, 1)
typedef struct COLLISIONVERTEX
{
    FLOAT x, y, z;
} COLLISIONVERTEX;
#pragma pack(pop)

#define D3DFVF_COLLISIONVERTEX (D3DFVF_XYZ)

//-----------------------------------------------------------------------------
// ShutdownD3D
//-----------------------------------------------------------------------------
VOID ShutdownD3D();

//-----------------------------------------------------------------------------
// InitD3D
//-----------------------------------------------------------------------------
HRESULT InitD3D( HWND hWnd );



HRESULT InitD3D();

//-----------------------------------------------------------------------------
// Name: CopySurface
//-----------------------------------------------------------------------------
HRESULT CopySurface(LPDIRECT3DSURFACE8 *pSrcSurf, RECT *srcRect,
				 LPDIRECT3DSURFACE8 *pDestSurf, RECT *destRect);

HRESULT CopySurfaceFast(LPDIRECT3DSURFACE8 *pSrcSurf, RECT *srcRect,
						LPDIRECT3DSURFACE8 *pDestSurf, POINT *destPoint);

//-----------------------------------------------------------------------------
// Name: LoadBmp
//-----------------------------------------------------------------------------
HRESULT LoadBmp(char *path, BMPDATA *pBmpData, LPDIRECT3DSURFACE8 *pDestSurf);

#endif