#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "Dinput8.lib")
#pragma comment(lib, "Dxguid.lib")
#pragma comment(lib, "D3dx8.lib") 

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include <windows.h>

//#include "DirectX.h"

#include "AdvMesh.h" 
#include "ErrorLogging.h"

HRESULT CAdvMesh::LoadXFile( char* pstrPathName )
{
	HRESULT r = 0;

	//lets call loadXfile for mesh
	this->CMesh::LoadXFile( pstrPathName );

	//now we make a pointer to the vertex buffer


	//lets see if we can get direct access to the polys...
	//for colission detection....
	// NOTE: dont think we are working with index buffers

	//TODO:clone the mesh using an appropriate FVF, then we know the format
	// and can load the data into structs :) done in parent class Mesh

	//NOTE: 1. Get Model, multiply by world transform matrix
	// 2. Then get FVF data, this will be transformed, data.
	// Implmentation:
	// 1. D3DXMATRIX mLocal;




	//CUSTOMVERTEX* vertexInfo;

	r = pMesh->LockVertexBuffer( D3DLOCK_READONLY, (BYTE**)&vertData );
	if( FAILED(r) )
		Error("lock failed");

	vertexData = (CUSTOMVERTEX*)&vertData; //so it dont shit out, when it all works i gotta fix some stuff


	byte* curVert = vertData;
	CUSTOMVERTEX *data;

	//lets make an array of d3dxvector4's
	D3DXVECTOR4 *transModel = (D3DXVECTOR4*)malloc( sizeof(D3DXVECTOR4) * pMesh->GetNumVertices() );
	D3DXVECTOR4 *result = transModel;


	for( int i=0; i < (signed int)pMesh->GetNumVertices() ; i++)
	{
		curVert += sizeof(CUSTOMVERTEX);
		data = (CUSTOMVERTEX*)curVert;

		D3DXVECTOR4* vertex = new D3DXVECTOR4(data->point.x,data->point.y,data->point.z, 1);
		D3DXVec4Transform( result, vertex, &mLocal );

		result += 1; //where 1 should be sizeof(D3DXVECTOR4), have to cheack it tho

		char strMessage[255] = {0};
		sprintf(strMessage, "v: %f %f %f\n", data->point.x,data->point.y,data->point.z);
	}

	r = pMesh->UnlockVertexBuffer();
	if( FAILED(r) )
		Error("unlock failed");

	return S_OK;
}

void CAdvMesh::OnCollision( CObject3D* pObjCollision )
{
	//here we should do a proper collision detection?

	if( OnCollisionFn != 0 )
		OnCollisionFn(this, pObjCollision);
}

void CAdvMesh::SetOnCollisionFn( void (*OnCollisionFn)( CObject3D* owner, CObject3D* pObjCollision ) )
{
	this->OnCollisionFn = OnCollisionFn;
}

void CAdvMesh::GetVertexData(BYTE** vertexData, long* noVertices)
{
	*noVertices = pMesh->GetNumVertices();

	vertexData = (BYTE**)this->vertexData;
}
