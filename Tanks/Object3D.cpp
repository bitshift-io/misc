#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include "DirectX.h"
#include "Object3D.h"

CObject3D::CObject3D()
{
	CallbackFn = 0;

	D3DXMatrixIdentity( &mLocal );

	vPosition = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	vVelocity = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	xRotUpdate = 0;
	yRotUpdate = 0;
	zRotUpdate = 0;

	xRot = 0;
	yRot = 0;
	zRot = 0;

	next = NULL;
	parent = NULL;
	ID = 0;
}

CObject3D::CObject3D(long ID)
{
	next = NULL;
	parent = NULL;
	this->ID = ID;
}

void CObject3D::SetCallbackFn( void (*CallbackFn)(CObject3D* owner) )
{
	this->CallbackFn = CallbackFn;
}

void CObject3D::Update()
{
	if( CallbackFn != NULL )
		CallbackFn( (CObject3D*)this );

	// Create some temporary matrices for the
	// rotation and translation transformations
	D3DXMATRIX mRotX, mRotY, mRotZ, mTrans, mRotTemp;

	// Update the position by the velocity
	vPosition.x += vVelocity.x;
	vPosition.y += vVelocity.y;
	vPosition.z += vVelocity.z;

	// Update rotations
	xRot += xRotUpdate;
	yRot += yRotUpdate;
	zRot += zRotUpdate;

	// Set the translation matrix
	D3DXMatrixTranslation( &mTrans, vPosition.x, vPosition.y, vPosition.z );

	D3DXMatrixRotationX( &mRotX, xRot );
	D3DXMatrixRotationY( &mRotY, yRot );
	D3DXMatrixRotationZ( &mRotZ, zRot );

	// Concatenate the y axis and x axis rotation matrices
	D3DXMatrixMultiply( &mRotTemp, &mRotX, &mRotY );
	// Concatenate the xy axes and z axis rotation matrices
	D3DXMatrixMultiply( &mRotTemp, &mRotZ, &mRotTemp );
	// Concatenate the xyz axes and translation matrices
	D3DXMatrixMultiply( &mTrans, &mRotTemp, &mTrans );

	// Update the copy of the local matrix
	//D3DXMATRIX mLocal;
	mLocal = mTrans;


	if( GetParent() )
	{
		D3DXMATRIX *mParent;

		mParent = &((CObject3D*)GetParent())->mLocal;

		D3DXMatrixMultiply( &mLocal, &mLocal, mParent);
	}

	// Set the world matrix
	g_pd3dDevice->SetTransform( D3DTS_WORLD, &mLocal );
}

void CObject3D::Render()
{

}

void CObject3D::Remove()
{
	//clean up code, removeing is killing, if u want
	//to change parents dont call this! call SetParent
	CallbackFn = NULL;
	next = NULL;
	parent = NULL;

	delete this;
}
