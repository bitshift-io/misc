#ifndef _TIMMING
#define _TIMMING


#include <time.h>

//-----------------------------------------------------------------------------
// Name: Time class that also tracks frames per second
//-----------------------------------------------------------------------------
class CTime
{
public:
	CTime();

	static long GetTime();
	static char* GetDate();
	static char* GetClockTime();
	int FrameCount();

private:
	long current;
	long secondCounter;
	int frameCount;
};

#endif