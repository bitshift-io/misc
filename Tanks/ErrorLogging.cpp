#include <stdio.h>
#include <string.h>
#include <windows.h>
#include "Windows.h"
#include "ErrorLogging.h"

char* lastPath;
FILE* pFile = fopen("system.log", "w"); //default file

//-----------------------------------------------------------------------------
// Name: Error
//-----------------------------------------------------------------------------
void Error(char *text)
{
	MessageBox(g_hWnd, text, "Error", MB_OK);
}

//-----------------------------------------------------------------------------
// Name: Log
//-----------------------------------------------------------------------------

void LogSetFile(char* path)
{
	if( lastPath != 0 )
	{
		if( strcmp( lastPath, path ) == 0 )
			return;
	}

	lastPath = path;

	pFile = fopen(path, "w");

	fseek(pFile, 0, SEEK_END);
	fputs("\n--- NEW SESSION ---\n", pFile);
}

void Log(char* text)
{
	fseek(pFile, 0, SEEK_END);
	fputs(text, pFile);;
}

