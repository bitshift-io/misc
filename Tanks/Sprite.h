#ifndef _SPRITE
#define _SPRITE

#include <windows.h>

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include <math.h>

#include "Object3D.h"


#pragma pack(push, 1)
typedef struct SPRITEVERTEX
{
    float x, y, z; // The transformed position for the vertex
	float tu, tv;   // The texture coordinates.

} SPRITEVERTEX;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct SPRITEVERTEXPROCESSED
{
    float x, y, z, rhw; // The transformed position for the vertex
	float tu, tv;   // The texture coordinates.

} SPRITEVERTEXPROCESSED;
#pragma pack(pop)

// Our custom FVF, which describes our custom vertex structure
#define D3DFVF_SPRITEVERTEX (D3DFVF_XYZ | D3DFVF_TEX1 )
#define D3DFVF_SPRITEVERTEXPROCESSED (D3DFVF_XYZRHW | D3DFVF_TEX1 )

class CSprite : public CObject3D
{
	friend class CParticle;
public:
	CSprite();
	CSprite(char* path);
	void LoadTexture( char* path );
	void Render();
	void Update();

	D3DXVECTOR2 translation;
	D3DXVECTOR2 scale;
	D3DXVECTOR2 rotationCenter;
	D3DCOLOR color;

protected:
	float size;
	float mass;
	CSprite* nextSprite; // this will eventauuly be for the particl system to link the sprites

private:
	LPDIRECT3DTEXTURE8 texture;
	RECT srcRect;
	SPRITEVERTEX sprite[6];
	unsigned char *vb_vertices;
	IDirect3DVertexBuffer8 *g_vb;
};


#endif