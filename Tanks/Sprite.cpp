#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "Dinput8.lib")
#pragma comment(lib, "Dxguid.lib")
#pragma comment(lib, "D3dx8.lib") 

#include <windows.h>
#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "Sprite.h"
#include "ErrorLogging.h"
#include "directx.h"
#include "DataManip.h"



CSprite::CSprite()
{
	nextSprite = 0;

	size = 5.0;

	SPRITEVERTEX sprite_verts[6] ={
    {-size,  -size, 0.0f, 0.0f, 1.0f}, // x, y, z, colour, tu, tv
    {-size,   size, 0.0f, 0.0f, 0.0f},
    { size,  -size, 0.0f, 1.0f, 1.0f},

	{-size,   size, 0.0f, 0.0f, 0.0f},
	{ size,   size, 0.0f, 1.0f, 0.0f},
    { size,  -size, 0.0f, 1.0f, 1.0f}
	};

	memcpy( sprite, sprite_verts, sizeof(sprite_verts) );

	translation.x = 0;
	translation.y = 0;

	scale.x = 1;
	scale.y = 1;

	rotationCenter.x = 0;
	rotationCenter.y = 0;

	color = D3DCOLOR_XRGB(255, 255, 255);

	texture = NULL;

	HRESULT r = 0;

	r = g_pd3dDevice->CreateVertexBuffer(6*sizeof(D3DFVF_SPRITEVERTEX), //Size of memory to be allocated
                                                            // Number of vertices * size of a vertex
                                       D3DUSAGE_WRITEONLY, // We never need to read from it so
                                                           // we specify write only, it's faster
                                       D3DFVF_SPRITEVERTEX, //Our custom vertex specifier (coordinates & a colour)
                                       D3DPOOL_MANAGED,   //Tell DirectX to manage the memory of this resource
                                       &g_vb);      //Pointer to our vb, after this call
                                                          //It will point to a valid vertex buffer
   if(FAILED(r)){
      Error("Error creating vertex buffer");
   }


   //Now we go through the same process to fill in our VB for the square.
   r=g_vb->Lock(0, //Offset, we want to start at the beginning
                     0, //SizeToLock, 0 means lock the whole thing
                     &vb_vertices, //If successful, this will point to the data in the VB
                     D3DLOCK_DISCARD);  //Flags, nothing special
   if(FAILED(r)){
      Error("Error Locking vertex buffer");
   }

   memcpy(vb_vertices, sprite, sizeof(sprite) );

   g_vb->Unlock();

}

CSprite::CSprite(char* path)
{
	HRESULT r = 0;
	this->CSprite::CSprite();

	path = (char*)StringCat(g_texturesPath, path);

	/*
    if( FAILED( D3DXCreateTextureFromFile( g_pd3dDevice,
                            path, &texture ) ) )
    {
		Error("cant load texture");
        texture = NULL;
    }*/

 //D3DXCreateTextureFromFileEx takes a lot of paramters.
   //We pass it our device and the file name of our texture.
   //We set Width & Height to D3DX_DEFAULT so it uses the values in the file.
   //We want an alpha channel so we tell it to use D3DFMT_A8R8G8B8 because it's a
   //    very common texture format, it should work on just about everything.
   //We set our Colour Key to opaque black (0xFF000000, watch the alpha component!)
   r = D3DXCreateTextureFromFileEx(g_pd3dDevice, //Our D3D Device
                                  path,    //Filename of our texture
                                  D3DX_DEFAULT, //Width:D3DX_DEFAULT = Take from file
                                  D3DX_DEFAULT, //Height:D3DX_DEFAULT = Take from file
                                  1,            //MipLevels
                                  0,            //Usage, Is this to be used as a Render Target? 0 == No
                                  SURFACEFORMAT, //32-bit with Alpha, everything should support this
                                  D3DPOOL_MANAGED,//Pool, let D3D Manage our memory
                                  D3DX_DEFAULT, //Filter:Default filtering
                                  D3DX_DEFAULT, //MipFilter, used for filtering mipmaps
                                  0xFFFF00FF,   //ColourKey
                                  NULL,         //SourceInfo, returns extra info if we want it (we don't)
                                  NULL,         //Palette:We're not using one
                                  &texture);  // Our texture goes here.
   if(FAILED(r)){
      Error("Error Loading Texture");
   }

/*
	r = g_pd3dDevice->CreateVertexBuffer(6*sizeof(SPRITEVERTEX), //Size of memory to be allocated
                                                            // Number of vertices * size of a vertex
                                       D3DUSAGE_WRITEONLY, // We never need to read from it so
                                                           // we specify write only, it's faster
                                       D3DFVF_SPRITEVERTEX, //Our custom vertex specifier (coordinates & a colour)
                                       D3DPOOL_MANAGED,   //Tell DirectX to manage the memory of this resource
                                       &spriteVertBuff);      //Pointer to our vb, after this call
                                                          //It will point to a valid vertex buffer
   if(FAILED(r)){
      Error("Error creating vertex buffer");
   }


	r = spriteVertBuff->Lock(0, //Offset, we want to start at the beginning
                     0, //SizeToLock, 0 means lock the whole thing
                     &vb_vertices, //If successful, this will point to the data in the VB
                     D3DLOCK_DISCARD);  //Flags, nothing special
   if(FAILED(r)){
      Error("Error Locking vertex buffer");
   }

   memcpy(vb_vertices, sprite, sizeof(sprite) );

   spriteVertBuff->Unlock();

   // Create the processed vertex buffer
	g_pd3dDevice->CreateVertexBuffer(sizeof(SPRITEVERTEXPROCESSED)*6, NULL,
		D3DFVF_SPRITEVERTEXPROCESSED, D3DPOOL_MANAGED, &spriteVertBuffProcessed);

*/




}

void CSprite::LoadTexture( char* path )
{
	HRESULT r = 0;
	this->CSprite::CSprite();

	path = (char*)StringCat(g_texturesPath, path);

	/*
    if( FAILED( D3DXCreateTextureFromFile( g_pd3dDevice,
                            path, &texture ) ) )
    {
		Error("cant load texture");
        texture = NULL;
    }*/

	//D3DXCreateTextureFromFileEx takes a lot of paramters.
   //We pass it our device and the file name of our texture.
   //We set Width & Height to D3DX_DEFAULT so it uses the values in the file.
   //We want an alpha channel so we tell it to use D3DFMT_A8R8G8B8 because it's a
   //    very common texture format, it should work on just about everything.
   //We set our Colour Key to opaque black (0xFF000000, watch the alpha component!)
   r = D3DXCreateTextureFromFileEx(g_pd3dDevice, //Our D3D Device
                                  path,    //Filename of our texture
                                  D3DX_DEFAULT, //Width:D3DX_DEFAULT = Take from file
                                  D3DX_DEFAULT, //Height:D3DX_DEFAULT = Take from file
                                  1,            //MipLevels
                                  0,            //Usage, Is this to be used as a Render Target? 0 == No
                                  SURFACEFORMAT, //32-bit with Alpha, everything should support this
                                  D3DPOOL_MANAGED,//Pool, let D3D Manage our memory
                                  D3DX_DEFAULT, //Filter:Default filtering
                                  D3DX_DEFAULT, //MipFilter, used for filtering mipmaps
                                  0xFFFF00FF,   //ColourKey
                                  NULL,         //SourceInfo, returns extra info if we want it (we don't)
                                  NULL,         //Palette:We're not using one
                                  &texture);  // Our texture goes here.
   if(FAILED(r)){
      Error("Error Loading Texture");
   }
}

void CSprite::Render()
{
	HRESULT r = 0;
	Update();


	/*
	verts[0]=D3DLVERTEX(loc-rightVect, color, 0, 0.0f, 0.0f);
	verts[1]=D3DLVERTEX(loc+upVect, color, 0, 0.0f, 1.0f);
	verts[2]=D3DLVERTEX(loc-upVect, color, 0, 1.0f, 0.0f);
	verts[3]=D3DLVERTEX(loc+rightVect, color, 0, 1.0f, 1.0f);
	*/


/*
	// Tell it to use our texture
	g_pd3dDevice->SetTexture(0, texture);

	// Set the source vertex buffer to our processed vertices
	g_pd3dDevice->SetStreamSource(0, spriteVertBuffProcessed, sizeof(SPRITEVERTEXPROCESSED));

	// Tell it to process the source vertex buffer using the CProcessedVertex FVF
	g_pd3dDevice->SetVertexShader(D3DFVF_SPRITEVERTEXPROCESSED);

	// Draw the source vertex buffer
	g_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 4);

*/


		D3DXMATRIX location_matrix;
		D3DXMATRIX temp_matrix;
		D3DXMATRIX world_matrix;
		D3DXMATRIX view_matrix;
		D3DXMATRIX trans_matrix;
		//DWORD colour;

      g_pd3dDevice->SetTexture(0,texture);

      g_pd3dDevice->SetVertexShader(D3DFVF_SPRITEVERTEX);

      g_pd3dDevice->SetStreamSource(0,g_vb,sizeof(SPRITEVERTEX));


         //First we perform all of our calculations to determine the star's
         //position in 3D space.

         D3DXMatrixIdentity(&location_matrix);

		 //this->vPosition.y = 0;
		 //this->vPosition.z = 0;
		 //this->vPosition.x = 10.0f;
         //Move star out from center
		 //int x = vPosition.x = 10;
		 //D3DXMatrixTranslation( &temp_matrix, 10, 0, 0 );
         D3DXMatrixTranslation(&temp_matrix, vPosition.x ,vPosition.y, vPosition.z);
         D3DXMatrixMultiply(&location_matrix,&location_matrix,&temp_matrix);


         //Revolve around center
         D3DXMatrixRotationZ(&temp_matrix,0.00);
         D3DXMatrixMultiply(&location_matrix,&location_matrix,&temp_matrix);

         //Tilt on Y axis
         D3DXMatrixRotationX(&temp_matrix, 0 );
         D3DXMatrixMultiply(&location_matrix,&location_matrix,&temp_matrix);

         //Zoom into Screen
         D3DXMatrixTranslation(&temp_matrix,0,0,0);
         D3DXMatrixMultiply(&location_matrix,&location_matrix,&temp_matrix);


         //Now we know the star's position.  Since each star is actually a flat
         //quad we have to make it face the camera.  This is known as a billboard.
         //To do this we Transpose the View matrix, which gives us a matrix that
         //is oriented towards the camera.
         g_pd3dDevice->GetTransform(D3DTS_VIEW,&view_matrix);
         D3DXMatrixTranspose(&trans_matrix,&view_matrix);

         //Now we have the position of our star in the location_matrix
         //and the orientation in the trans_matrix

         //We copy the trans matrix (we need to maintain it's values for the
         //Twinkle effect which follows).
         world_matrix=trans_matrix;

         //Now we plug in the location values into our world matrix.
         world_matrix._41 = location_matrix._41;
         world_matrix._42 = location_matrix._42;
         world_matrix._43 = location_matrix._43;


         g_pd3dDevice->SetTransform(D3DTS_WORLD,&world_matrix );

         //We calculate the colour and then set it as the TFACTOR (TEXTUREFACTOR)
         //This causes the star to be drawn in that colour since we're modulating
         //the texture (which is grayscale) by this colour.
         //colour=D3DCOLOR_ARGB(0xFF,g_stars[count].r,g_stars[count].g,g_stars[count].b);

         //g_pd3dDevice->SetRenderState(D3DRS_TEXTUREFACTOR,colour);

         g_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST,0,2);


}


void CSprite::Update()
{

	vPosition.x += vVelocity.x;
	vPosition.y += vVelocity.y;
	vPosition.z += vVelocity.z;
/*
	D3DMATRIX mat;

	g_pd3dDevice->GetTransform(D3DTS_VIEW,&mat);

	D3DXVECTOR3 right = D3DXVECTOR3(mat._11,mat._21,mat._31);
	D3DXVECTOR3 up = D3DXVECTOR3(mat._12,mat._22,mat._32);
	D3DXVECTOR3 look = D3DXVECTOR3(mat._13,mat._23,mat._33);

	D3DXVec3Normalize( &right, &right );
	D3DXVec3Normalize( &up, &up );
	D3DXVec3Normalize( &look, &look );

	//trnasform our untransformed shape....

	g_pd3dDevice->SetVertexShader( D3DFVF_SPRITEVERTEX );

	g_pd3dDevice->SetStreamSource(0, spriteVertBuff,sizeof(SPRITEVERTEX));

	// Tell it to process the vertices, starting at vertex 0 in both the source and destination
	// Buffers, processing all four vertices, and putting them into the processed vertices buffer.
	g_pd3dDevice->ProcessVertices(0, 0, 6, spriteVertBuffProcessed, NULL);

	// now append it :)

	/*	x1 = x * rx + y * ux + z * lx;
	y1 = x * ry + y * uy + z * uz;
	z1 = x * rz + y * uz + z * uz;* /

	size = 15.75;
	procSprite[0].x = (vPosition.x * right.x + vPosition.y * up.x + vPosition.z * look.x) - right.x;
	procSprite[0].y = (vPosition.x * right.y + vPosition.y * up.x + vPosition.z * look.y) - right.y;
	procSprite[0].z = (vPosition.x * right.z + vPosition.y * up.x + vPosition.z * look.z) - right.z;

	procSprite[1].x = (vPosition.x * right.x + vPosition.y * up.x + vPosition.z * look.x) + up.x;
	procSprite[1].y = (vPosition.x * right.y + vPosition.y * up.x + vPosition.z * look.y) + up.y;
	procSprite[1].z = (vPosition.x * right.z + vPosition.y * up.x + vPosition.z * look.z) + up.z;

	procSprite[2].x = (vPosition.x * right.x + vPosition.y * up.x + vPosition.z * look.x) - up.x;
	procSprite[2].y = (vPosition.x * right.y + vPosition.y * up.x + vPosition.z * look.y) - up.y;
	procSprite[2].z = (vPosition.x * right.z + vPosition.y * up.x + vPosition.z * look.z) - up.z;

	procSprite[3].x = (vPosition.x * right.x + vPosition.y * up.x + vPosition.z * look.x) + up.x;
	procSprite[3].y = (vPosition.x * right.y + vPosition.y * up.x + vPosition.z * look.y) + up.x;
	procSprite[3].z = (vPosition.x * right.z + vPosition.y * up.x + vPosition.z * look.z) + up.x;

	procSprite[4].x = (vPosition.x * right.x + vPosition.y * up.x + vPosition.z * look.x) + right.x;
	procSprite[4].y = (vPosition.x * right.y + vPosition.y * up.x + vPosition.z * look.y) + right.x;
	procSprite[4].z = (vPosition.x * right.z + vPosition.y * up.x + vPosition.z * look.z) + right.x;

	procSprite[5].x = (vPosition.x * right.x + vPosition.y * up.x + vPosition.z * look.x) - up.x;
	procSprite[5].y = (vPosition.x * right.y + vPosition.y * up.x + vPosition.z * look.y) - up.x;
	procSprite[5].z = (vPosition.x * right.z + vPosition.y * up.x + vPosition.z * look.z) - up.x;


	HRESULT r =0;

	r = spriteVertBuffProcessed->Lock(0, //Offset, we want to start at the beginning
                     0, //SizeToLock, 0 means lock the whole thing
                     &vb_vertices, //If successful, this will point to the data in the VB
                     D3DLOCK_DISCARD);

	memcpy(vb_vertices, procSprite, sizeof(procSprite) );

	spriteVertBuff->Unlock();
	*/
}

/*
void CSprite::Update()
{

	if( CallbackFn != NULL )
		CallbackFn( (CObject3D*)this );

	// Create some temporary matrices for the
	// rotation and translation transformations
	D3DXMATRIX mRotX, mRotY, mRotZ, mTrans, mRotTemp;

	// Update the position by the velocity
	vPosition.x += vVelocity.x;
	vPosition.y += vVelocity.y;
	vPosition.z += vVelocity.z;

		// Set the translation matrix
	D3DXMatrixTranslation( &mTrans, vPosition.x, vPosition.y, vPosition.z );


// THIS PART WORKS SORT OF......
	/*
			D3DXVECTOR3 lookAtCamera = -(g_camera.vPosition - this->vPosition);
	float lengthLookVector = sqrt( pow(lookAtCamera.x,2) + pow(lookAtCamera.y,2) + pow(lookAtCamera.z,2) );
	D3DXVec3Normalize( &lookAtCamera, &lookAtCamera );


    // Set up a rotation matrix to orient the billboard towards the camera.
    D3DXVECTOR3 vDir = lookAtCamera;//g_camera.lookAt - g_camera.vPosition;//;vLookatPt - vEyePt;

	if( vDir.x > 0.0f )
		yRot = -atanf(vDir.z/vDir.x)+D3DX_PI/2;
    else
		yRot = -atanf(vDir.z/vDir.x)-D3DX_PI/2;


		zRot = acosf( vDir.y / lengthLookVector )-D3DX_PI/2;


	D3DXMatrixRotationX( &mRotX, xRot );
	D3DXMatrixRotationY( &mRotY, yRot );
	D3DXMatrixRotationZ( &mRotZ, zRot );

	// Concatenate the y axis and x axis rotation matrices
	D3DXMatrixMultiply( &mRotTemp, &mRotX, &mRotY );
	// Concatenate the xy axes and z axis rotation matrices
	D3DXMatrixMultiply( &mRotTemp, &mRotZ, &mRotTemp );
	// Concatenate the xyz axes and translation matrices
	D3DXMatrixMultiply( &mTrans, &mRotTemp, &mTrans );
	//* /

	//D3DXMatrixMultiply( &mTrans, &mRot, &mTrans);




	//Now we know the star's position.  Since each star is actually a flat
         //quad we have to make it face the camera.  This is known as a billboard.
         //To do this we Transpose the View matrix, which gives us a matrix that
         //is oriented towards the camera.

//	D3DXMATRIX view_matrix, trans_matrix, world_matrix;

 //        g_pd3dDevice->GetTransform(D3DTS_VIEW,&view_matrix);
 //        D3DXMatrixTranspose(&trans_matrix,&view_matrix);

         //Now we have the position of our star in the location_matrix
         //and the orientation in the trans_matrix

         //We copy the trans matrix (we need to maintain it's values for the
         //Twinkle effect which follows).
 //        world_matrix=trans_matrix;

         //Now we plug in the location values into our world matrix.
 //        world_matrix._41 = mTrans._41;
 //        world_matrix._42 = mTrans._42;
 //        world_matrix._43 = mTrans._43;


  //       g_pd3dDevice->SetTransform(D3DTS_WORLD,&mTrans );







		 mLocal = mTrans;

	if( GetParent() )
	{
		D3DXMATRIX *mParent;

		mParent = &((CObject3D*)GetParent())->mLocal;

		D3DXMatrixMultiply( &mLocal, &mLocal, mParent);
	}

	// Set the world matrix
	g_pd3dDevice->SetTransform( D3DTS_WORLD, &mLocal );
}*/
