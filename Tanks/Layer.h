#ifndef _LAYER
#define _LAYER

#include "Object.h"
#include "Image.h"

class CLayer : public CImage
{
public:
	CLayer();
	void Render();
	virtual void Remove();
	void AddObject( CObject *pNewObject );
	void RemoveObject( CObject *pObjToRemove );
	int GetSize(){ return sizeof( *this ); }
	void SetVisible( bool visibility );
	bool IsVisible(){ return visible; };
	virtual OBJECT_TYPE GetType() const {return LAYER;}

private:
	bool visible;
	CLayer* parent;
	CObject* pObjList;
};


#endif