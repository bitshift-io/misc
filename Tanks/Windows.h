#ifndef _EWINDOWS
#define _EWINDOWS

#include <windows.h>

extern HWND g_hWnd;
extern WNDCLASSEX *g_wc;
extern HINSTANCE g_hInstance;

//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: The window's message handler
//-----------------------------------------------------------------------------
LRESULT WINAPI MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
#endif