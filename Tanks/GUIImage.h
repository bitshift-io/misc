#ifndef _GUIIMAGE
#define _GUIIMAGE

#include "Image.h"
//GUI Image.h
//this are buttons text boxes etc..

class CGUIImage : public CImage
{
public:
	CGUIImage();
	void Remove();
	virtual void OnMouseOver();
	virtual void OnMouseOut();
	virtual void OnMouseClick();
	void Update();
	void Render();
	bool HasFocus(){return focus;}
	void SetMouseZone(RECT* zone);
	virtual OBJECT_TYPE GetType() const {return GUIIMAGE;}

	void (*MouseOverFn)(CImage* owner);
	void (*MouseOutFn)(CImage* owner);
	void (*MouseClickFn)(CImage* owner);

	RECT mouseZone;

private:

protected:
	bool focus;
	bool wasOver;
};

#endif