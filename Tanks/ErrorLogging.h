#ifndef _ERRORLOGGING
#define _ERRORLOGGING

#include <stdio.h>
#include <string.h>

//-----------------------------------------------------------------------------
// Name: Error
//-----------------------------------------------------------------------------
void Error(char *text);

//-----------------------------------------------------------------------------
// Name: Log
//-----------------------------------------------------------------------------
extern char* lastPath;
extern FILE* pFile; //default file


void LogSetFile(char* path);


void Log(char* text);

#endif
