#ifndef _IMAGE
#define _IMAGE

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>
#include <math.h>

#include <windows.h>
#include "Object.h"

class CImage : public CObject
{
public:
	CImage();
	void LoadImage(char* path);
	void SetImage(void* image);
	void Render();
	void Remove();
	void Update();
	void SetSourceRect(RECT* rect);
	void SetDestRect(RECT* rect);
	void SetDestPoint(POINT* point);
	void StretchImage(long width, long height);
	void SetAbsolute(bool relOrAbs){this->absolute = relOrAbs;}
	virtual OBJECT_TYPE GetType() const {return IMAGE;}

	LPDIRECT3DSURFACE8 image;
	int width;
	int height;
	RECT srcRect;
	POINT* destPoint;
	POINT realDestPoint;
	bool absolute; //relative or absolute positioning - mouse would be absolute,
	//pretty much ene thing else would be relative to its parent
};


#endif
