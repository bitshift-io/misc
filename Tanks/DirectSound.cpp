#include "DirectSound.h"
#include "ErrorLogging.h"

//http://www.myran.com/enilno/tuts/Tutorial5.htm

LPDIRECTSOUND8 g_pSound = NULL;
LPDIRECTSOUNDBUFFER g_pSoundBuffer = NULL;

bool InitialiseDirectAudio()
{
	//CoInitialize(NULL);

	if( FAILED(DirectSoundCreate8( NULL, &g_pSound, 0)) )
	{
		Error("failed to get sound interface");
		return false;
	}

	if( FAILED(g_pSound->SetCooperativeLevel(g_hWnd, DSSCL_PRIORITY )) )
	{
		Error("failed to set sound cooperation");
		return false;
	}

	DSBUFFERDESC bufferDesc;
	ZeroMemory(&bufferDesc,sizeof(DSBUFFERDESC));
	bufferDesc.dwSize = sizeof(DSBUFFERDESC);
	bufferDesc.dwFlags=DSBCAPS_PRIMARYBUFFER|DSBCAPS_CTRLVOLUME|DSBCAPS_CTRLPAN;
	bufferDesc.dwBufferBytes=0;
	bufferDesc.lpwfxFormat=NULL;

	g_pSound->CreateSoundBuffer(&bufferDesc, &g_pSoundBuffer , NULL);

	//set play format now we have our sound buffer
	WAVEFORMATEX waveFmt;
	ZeroMemory(&waveFmt,sizeof(WAVEFORMATEX));
	waveFmt.wFormatTag=WAVE_FORMAT_PCM;
	waveFmt.nChannels=2;//stereo
	waveFmt.nSamplesPerSec=22050;
	waveFmt.wBitsPerSample=16;
	waveFmt.nBlockAlign=waveFmt.wBitsPerSample/8*waveFmt.nChannels;
	waveFmt.nAvgBytesPerSec=waveFmt.nSamplesPerSec*waveFmt.nBlockAlign;

	g_pSoundBuffer->SetFormat(&waveFmt);
	g_pSoundBuffer->Play(0,0,DSBPLAY_LOOPING); //plays
	g_pSoundBuffer->Stop(); //stops playing



    return true;
}


LPDIRECTSOUNDBUFFER8 CreateFileSoundBuffer(FILE* pFile,WAVEHEADER *hdr , LPDIRECTSOUND8 dSound)
{
   LPDIRECTSOUNDBUFFER    TempSB;//we will query SOUNDBUFFER8 with this
   LPDIRECTSOUNDBUFFER8   SBuffer;//actual BUFFER8 we will return
   DSBUFFERDESC           Sdesc;//description of our SoundBuffer
   WAVEFORMATEX           Wfex; 

   //read in the data from the beginning of the WAV
   fseek(pFile,0,SEEK_SET);
   fread(hdr,1,sizeof(WAVEHEADER),pFile);

   //make sure all the signature fields are present.  If they are not, 
   //this is not a valid WAV file
   if(memcmp(hdr->RiffSignature,"RIFF",4)||
      memcmp(hdr->WaveSignature,"WAVE",4)||
      memcmp(hdr->FormatSignature,"fmt ",4)||//notice the space after fmt, we need this because of the FormatSignature[4] array!
      memcmp(hdr->DataSignature,"data",4))
              return NULL;

   ZeroMemory(&Wfex,sizeof(WAVEFORMATEX));

   //copy over our Wave Info into the WAVEFORMATEX structure
   Wfex.wFormatTag=WAVE_FORMAT_PCM;
   Wfex.nChannels=hdr->Channels;
   Wfex.nSamplesPerSec=hdr->SampleRate;
   Wfex.wBitsPerSample=hdr->BitsPerSample;

   //shrink our sample block allignment to 8 bits per channel
   Wfex.nBlockAlign=Wfex.wBitsPerSample/8*Wfex.nChannels;

   //average our bytes per second
   Wfex.nAvgBytesPerSec=Wfex.nSamplesPerSec*Wfex.nBlockAlign;

   //create the temp buffer with this information
   ZeroMemory(&Sdesc,sizeof(Sdesc));
   Sdesc.dwSize=sizeof(DSBUFFERDESC);

   //set our buffer so we can  control volume, pan, and frequency.
   //If we raise the frequency, our wave file will play faster, and at a higher pitch
   Sdesc.dwFlags=DSBCAPS_CTRLVOLUME|DSBCAPS_CTRLPAN|DSBCAPS_CTRLFREQUENCY;
   Sdesc.dwBufferBytes=hdr->DataSize;

   //we need to pass our WaveformatEX here
   Sdesc.lpwfxFormat=&Wfex;

   if(FAILED(dSound->CreateSoundBuffer(&Sdesc,&TempSB,NULL)))
               return NULL;

   //make our soundbuffer the current interface supported by the sound card.
   if(FAILED(TempSB->QueryInterface(IID_IDirectSoundBuffer8,(LPVOID*)&SBuffer)))
   {
               TempSB->Release();
               return NULL;
   }
 
   TempSB->Release();

   //return our fully created sound buffer, the size of the Wave file
   return SBuffer;
}

bool LoadSoundIntoBuffer(LPDIRECTSOUNDBUFFER8 Buffer,long Position,FILE* pFile,long Size)
{
            //pointers to our sound data
            BYTE  *P1,*P2;

            //sizes of our chunks
            DWORD Size1,Size2;

            if(FAILED(Buffer->Lock(Position,Size,(void**)&P1,&Size1,(void**)&P2,&Size2,0)))
                        return FALSE;

            //read in the data from our file
            fread(P1,1,Size1,pFile);

            //if we have a second pointer, read that in also
            if(P2!=NULL)
                       fread(P2,1,Size2,pFile);

            //unlock our Sound Buffer
            Buffer->Unlock(P1,Size1,P2,Size2);

            //everything went fine
            return TRUE;
}


LPDIRECTSOUNDBUFFER8  LoadWAV(char* Filename, LPDIRECTSOUND8 DSound)
{
            LPDIRECTSOUNDBUFFER8 Buffer;
            WAVEHEADER  hdr;
            FILE *pFile;

            if((pFile=fopen(Filename,"rb"))==NULL)
                        return NULL;

            if((Buffer=CreateFileSoundBuffer(pFile,&hdr,DSound))==NULL)
            {
                        fclose(pFile);
                        return NULL;
            }

            //read all the data into the buffer
            fseek(pFile,sizeof(WAVEHEADER),SEEK_SET);
            LoadSoundIntoBuffer(Buffer,0,pFile,hdr.DataSize);

            fclose(pFile);
            return Buffer;
}

