#include <stdlib.h>
#include <string.h>
#include "Particle.h"
#include "Sprite.h"
#include "ErrorLogging.h"


CParticle::CParticle( int maxParticles, int particlesPerSecond )
{
	gravity = 9.8;
	noParticles = 0;
	this->particlesPerSecond = particlesPerSecond;
	this->maxParticles = maxParticles;
	particles = (CSprite*)calloc( maxParticles, sizeof(CSprite));

	sprite = new CSprite("star.png");
}

void CParticle::Render()
{
	Update();

	if(particles == 0)
		return;

	for(int i=0; i < noParticles; i++)
	{
		particles[i].Render();
	}
}

void CParticle::Update()
{
	//fill up the particles array with some particles
	if( noParticles < maxParticles )
		for( int i=0; i < particlesPerSecond; i++ )
		{
			sprite->vVelocity.x = (float)(-5 + rand()%10);
			sprite->vVelocity.y = (float)(-5 + rand()%10);
			sprite->vVelocity.z = (float)(-5 + rand()%10);
			memcpy((void*)&particles[noParticles], sprite, sizeof(CSprite));
			noParticles++;

			if( noParticles == maxParticles )
				break;
		}

	//now update positions etc, and gravity etc...
}

void CParticle::Remove()
{
	Error("hey");
};

CParticle::~CParticle()
{
	Error("oh ya");
	//delete particles;
	free( particles );
	free( sprite );
}


