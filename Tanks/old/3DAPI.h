#ifndef _3DAPI
#define _3DAPI

// this is the grapgics api interface/core
enum API { DIRECTX,
			OPENGL};


class C3DAPI
{
public:

	COLOR* clearColor;

	C3DAPI();

	virtual void Init3DAPI( HWND hWnd, bool fullScreen, int deviceWidth, int deviceHeight ) = 0;
	virtual void Shutdown3DAPI() = 0;

	virtual void BeginScene() = 0;
	virtual void EndScene() = 0;

	void SetClearColor( COLOR* pColor );
};

C3DAPI::C3DAPI()
{
	clearColor = NULL;
}


void C3DAPI::SetClearColor( COLOR* pColor )
{
	clearColor = pColor;
}

#endif

