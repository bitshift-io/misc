#ifndef _ERRORLOGGING
#define _ERRORLOGGING

#include "Vertices.h"

#include <stdio.h>
#include <string.h>

//-----------------------------------------------------------------------------
// Name: Error
//-----------------------------------------------------------------------------
void Error(char *text)
{
	MessageBox(g_hWnd, text, "Error", MB_OK);
}

//-----------------------------------------------------------------------------
// Name: Log
//-----------------------------------------------------------------------------
FILE* pFile = fopen("system.log", "w"); //default file
char* lastPath = "null";

void LogSetFile(char* path)
{
	if( strcmp( lastPath, path ) == 0 )
		return;

	lastPath = path;

	pFile = fopen(path, "w");

	fseek(pFile, 0, SEEK_END);
	fputs("\n--- NEW SESSION ---\n", pFile);
}

void Log(char* text)
{
	fseek(pFile, 0, SEEK_END);
	fputs(text, pFile);;
}

#endif
