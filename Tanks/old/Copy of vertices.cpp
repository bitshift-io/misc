//-----------------------------------------------------------------------------
//  Fabian Mathews
//	Engine
//-----------------------------------------------------------------------------
#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "Dinput8.lib")
#pragma comment(lib, "Dxguid.lib")
#pragma comment(lib, "D3dx8.lib")   

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>
#include <math.h>

//-----------------------------------------------------------------------------
// Nesisary Globals
//-----------------------------------------------------------------------------
HWND g_hWnd;
WNDCLASSEX *g_wc;
HINSTANCE g_hInstance;

//-----------------------------------------------------------------------------
// Custom Nesisary Globals
//-----------------------------------------------------------------------------
#define PI 3.1415
bool inMenu = false;

int g_deviceWidth = 640;
int g_deviceHeight = 480;
bool g_fullScreen = false;
bool g_screenSaver = false; //make it so all keys quit

//paths to data stuffs
char* g_meshPath;
char* g_meshTexturePath; 
char* g_texturesPath;
char* g_soundsPath;
char* g_dataPath;
char* g_imageDumpsPath;

//-----------------------------------------------------------------------------
// Custom Include files
//-----------------------------------------------------------------------------
#include "Engine.h";



//-----------------------------------------------------------------------------
// Custom Classes/Structs
//---------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Custom Globals
//-----------------------------------------------------------------------------

CINI g_ini("data/config.ini");
static CTime g_timer;
CFont *g_pFont = NULL;


/// GAME CODE ///////////
int cameraTimer = 0;


CFrame g_world;
CAdvMesh g_skybox;
CAdvMesh g_plane;
CAdvMesh g_ball6;

CAdvMesh g_ball1;
CAdvMesh g_ball2;
CAdvMesh g_ball3;
CAdvMesh g_ball4;
CAdvMesh g_ball5;
CMesh g_3dsmodel;

CSprite* g_sprite;
CParticle* g_particleEmitter;
CGUITextbox g_textbox;

CLayer g_menu;
CGUIImage g_restart;
CImage* g_logo = new CImage();
CImage* g_mouse = new CImage();


bool g_newScene = true;
float g_timmer = 0;

//-----------------------------------------------------------------------------
// Other Custom Include files
//-----------------------------------------------------------------------------
#include "WinInput.h"

void ResetOver( CImage* owner )
{
		RECT rect = {0,30,80,60};
		owner->SetSourceRect( &rect );
}

void ResetOut( CImage* owner )
{
		RECT rect = {0,0,80,30};
		owner->SetSourceRect( &rect );
}

void ResetClick( CImage* owner )
{
	g_ball1.vVelocity.x = 0.05;
	g_ball1.vVelocity.y = -0.05;
	g_ball1.vVelocity.z = 0.0;
	g_ball1.vPosition.x = -100;
	g_ball1.vPosition.y = 100;

	g_ball2.vVelocity.x = -0.05;
	g_ball2.vVelocity.y = 0.0;
	g_ball2.vVelocity.z = 0.0;
	g_ball2.vPosition.x = 100;
	g_ball2.vPosition.y = 0;

	g_ball3.vPosition.x = 50;
	g_ball3.vPosition.y = 0;
	g_ball3.vPosition.y = -80;
	g_ball3.vVelocity.y = 0.1;
	g_ball3.vVelocity.x = 0.0;

	g_ball4.vPosition.x = -100;
	g_ball4.vPosition.y = 0;
	g_ball4.vVelocity.x = 0.04;
	g_ball4.vVelocity.y = 0.0;

	g_ball5.vPosition.x = 120;
	g_ball5.vPosition.y = 30;
	g_ball5.vPosition.z = -100;
	g_ball5.vVelocity.x = -0.04;
	g_ball5.vVelocity.y = -0.01;
	g_ball5.vVelocity.z = 0.04;
}


void Camera1( CObject3D* owner)
{
	//g_newScene = false;
	
	//g_camera.lookAt = g_sprite->vPosition;

	float theta = (CTime::GetTime()/D3DX_PI)/1000;
	//owner->vPosition.x = 10;
	//owner->vPosition.z = 10;
	//owner->vPosition.x = (float)(180 * cos( theta ));
	//owner->vPosition.z = (float)(180 * sin( theta ));
	//owner->vPosition.y = (float)(150 * cos( theta ));
}

void ball2col(CObject3D* owner, CObject3D* colObj)
{	
	//what are we going to do????
	//get the position of the other object, 
	//determine what angle to bounce off at
/*
	//calculate the normal, then the angle between 
	//normal and volicity, change it around
	// the normal
	CAdvMesh* pObjA = (CAdvMesh*)owner;
	CAdvMesh* pObjB = (CAdvMesh*)colObj;

	//calculate normal
	float aX = pObjA->vPosition.x;
	aX = aX < 0 ? -aX : aX;
	float aY = pObjA->vPosition.y;
	aY = aY < 0 ? -aY : aY;
	float aZ = pObjA->vPosition.z;
	aZ = aZ < 0 ? -aZ : aZ;

	float bX = pObjB->vPosition.x;
	bX = bX < 0 ? -bX : bX;
	float bY = pObjB->vPosition.y;
	bY = bY < 0 ? -bY : bY;
	float bZ = pObjB->vPosition.z;
	bZ = bZ < 0 ? -bZ : bZ;

	float nX = (aX + bX);
	float nY = (aY + bY);
	float nZ = (aZ + bZ);//<---- thats the normal

	//now for the angle
	// theta = cos-1(a.b)/(|a|x|b|)
	double adotb = (nX * aX) + (nY * aY) + (nZ + aZ);
	double lengtha = sqrt(pow(aX,2) + pow(aY,2) + pow(aZ,2));
	double lengthb = sqrt(pow(nX,2) + pow(nY,2) + pow(nZ,2));
	double theta = cosf( adotb )/(lengtha * lengthb);

	// double the angle?
	aX *= -cos( theta * 2 );
	aY *= -cos( theta * 2 );
	aZ *= -cos( theta * 2 );

	pObjA->vVelocity.x += cos( PI - (theta * 2) );
    pObjA->vVelocity.y += sin( PI - (theta * 2) );
	//pObjA->vVelocity.z *= -cos( theta * 2 );

*/
//	owner->vVelocity.x = -(owner->vVelocity.x);
//	owner->vVelocity.y = -(owner->vVelocity.y);
//	owner->vVelocity.z = -(owner->vVelocity.z);
}

void ball1col(CObject3D* owner, CObject3D* colObj)
{
	Error("waht u want?");
	//swap vectors, saves a bit of maths, 
	//looks pretty realistic too ;)
	static bool taken = false;

	if( taken == false )
	{
		D3DXVECTOR3* temp = new D3DXVECTOR3(owner->vVelocity.x, owner->vVelocity.y, owner->vVelocity.z);
		owner->vVelocity.x = (colObj->vVelocity.x);
		owner->vVelocity.y = (colObj->vVelocity.y);
		owner->vVelocity.z = (colObj->vVelocity.z);

		colObj->vVelocity.x = temp->x;
		colObj->vVelocity.y = temp->y;
		colObj->vVelocity.z = temp->z;
	}
	
	if(taken)
		taken = false;
	else
		taken = true;

}


//-----------------------------------------------------------------------------
// Name: Init()
//-----------------------------------------------------------------------------
HRESULT Init()
{
	char *retValue = NULL;

	//read important device settings
	g_ini.SetSection("system");
	retValue = g_ini.ReadKey("fullScreen");
	g_fullScreen = (bool)StringToNumber(retValue);
	retValue = g_ini.ReadKey("deviceWidth");
	g_deviceWidth = StringToNumber(retValue);
	retValue = g_ini.ReadKey("deviceHeight");
	g_deviceHeight = StringToNumber(retValue);


	// read in the paths, if not found, should have defaults?
	g_ini.SetSection("paths");
	g_meshPath = g_ini.ReadKey("meshes");
	g_meshTexturePath = g_ini.ReadKey("meshTextures");
	g_texturesPath = g_ini.ReadKey("textures");
	g_soundsPath = g_ini.ReadKey("sounds");		
	g_imageDumpsPath =  g_ini.ReadKey("imageDumps");

	InitD3D( NULL );
	g_pd3dDevice->SetRenderState( D3DRS_AMBIENT, 0xffffffff );


	//load font
	g_pFont = new CFont("FontSmall.bmp",10,10);	

	InitInput();

	//some 3ds ene 1?
	g_3dsmodel.Load3DSFile( "earth.3ds" );


	//set up our scene
	g_sprite = new CSprite("star.png");
	//g_sprite->vPosition.x = 10;
	g_world.AddObject((CObject3D*)g_sprite);

	g_particleEmitter = new CParticle(1000, 10);
	g_world.AddObject((CObject3D*)g_particleEmitter);

	//we always want to do collision detection with the level
	// so make a massive bound shphere

	/*
	g_skybox.LoadXFile("cube.X");
	g_skybox.boundSphereRadius = 60000;
	g_world.AddObject((CObject3D*)&g_skybox);*/

	
	g_plane.LoadXFile("poly.X");
	g_plane.boundSphereRadius = 10000;
	g_plane.yRot = -D3DX_PI;
	//g_plane.xRot = D3DX_PI/2;
	g_plane.vPosition.z = 0;
	g_plane.vPosition.x = -100;
	g_plane.vPosition.y = 0;
	g_world.AddObject((CObject3D*)&g_plane);

	g_ball6.LoadXFile("earth.X");
	g_ball6.SetOnCollisionFn( ball1col );
	g_ball6.vPosition.x = 50;
	g_ball6.vPosition.y = 0;
	g_ball6.vVelocity.x = -0.06;
	g_ball6.vVelocity.y = 0.0;
	g_ball6.boundSphereRadius = 5.0;
	g_world.AddObject((CObject3D*)&g_ball6);

/*
	g_ball1.LoadXFile("earth.X");
	g_ball1.SetOnCollisionFn( ball1col );
	g_ball1.vPosition.x = -100;
	g_ball1.vPosition.y = 100;
	g_ball1.vVelocity.x = 0.06;
	g_ball1.vVelocity.y = -0.06;
	g_ball1.boundSphereRadius = 5.0;
	g_world.AddObject((CObject3D*)&g_ball1);

	g_ball3.LoadXFile("venus.X");
	g_ball3.SetOnCollisionFn( ball1col );
	g_ball3.vPosition.x = 50;
	g_ball3.vPosition.y = -80;
	g_ball3.vVelocity.y = 0.1;
	g_ball3.boundSphereRadius = 5.0;
	g_world.AddObject((CObject3D*)&g_ball3);

	g_ball2.LoadXFile("mars.X");
	g_ball2.SetOnCollisionFn( ball1col );
	g_ball2.vPosition.x = 100;
	g_ball2.vVelocity.x = -0.06;
	g_ball2.boundSphereRadius = 5.0;
	g_world.AddObject((CObject3D*)&g_ball2);

	g_ball4.LoadXFile("mars.X");
	g_ball4.SetOnCollisionFn( ball1col );
	g_ball4.vPosition.x = -100;
	g_ball4.vVelocity.x = 0.04;
	g_ball4.boundSphereRadius = 5.0;
	g_world.AddObject((CObject3D*)&g_ball4);

	g_ball5.LoadXFile("earth.X");
	g_ball5.SetOnCollisionFn( ball1col );
	g_ball5.vPosition.x = 120;
	g_ball5.vPosition.y = 30;
	g_ball5.vPosition.z = -100;
	g_ball5.vVelocity.x = -0.04;
	g_ball5.vVelocity.y = -0.01;
	g_ball5.vVelocity.z = 0.04;
	g_ball5.boundSphereRadius = 5.0;
	g_world.AddObject((CObject3D*)&g_ball5);*/

	//set the camera position and call back
	g_camera.vPosition = D3DXVECTOR3(00,0,-130);
	g_camera.SetCallbackFn( Camera1 );


	////////////// NOW FOR THE MENU STUFF //////////////////////////////
	RECT* rect = new RECT;
	POINT* point = new POINT;

	g_restart.LoadImage( "btnRun.bmp" );
	rect = new RECT;
	rect->top = 0;
	rect->left = 0;
	rect->bottom = 30;
	rect->right = 80;
	g_restart.SetSourceRect( rect );
	g_restart.SetMouseZone(NULL);
	point = new POINT;
	point->x = 0;
	point->y = 5;	
	g_restart.SetDestPoint( point );
	g_restart.MouseClickFn = ResetClick;
	g_restart.MouseOutFn = ResetOut;
	g_restart.MouseOverFn = ResetOver;
	g_menu.AddObject((CObject*)&g_restart);

	point = new POINT;
	g_logo = new CImage();
	g_logo->LoadImage("siphon.bmp");
	g_logo->StretchImage(300, 40);
	point->x = 120;
	point->y = 0;
	g_logo->SetDestPoint(point);
	g_menu.AddObject((CObject*)g_logo);

	//can we try text boexes, surte thing
	point = new POINT;
	point->x = 10;
	point->y = 100;
	g_textbox.SetAbsolute(true);
	g_textbox.LoadImage("textbox20.bmp");
	g_textbox.SetDestPoint(point);
	g_textbox.SetMouseZone(NULL);
	g_textbox.SetMaxChars(20);
	g_textbox.SetFont( g_pFont ); //what font do we want to use?
	g_menu.AddObject((CObject*)&g_textbox);


	g_mouse->LoadImage("cursor.bmp");
	point = new POINT;
	point->x = 0;
	point->y = 0;
	g_mouse->SetDestPoint(point);
	g_mouse->SetAbsolute(true);
	g_mouseData.height = g_mouse->height;
	g_mouseData.width = g_mouse->width;
	g_menu.AddObject((CObject*)g_mouse);

	point = new POINT;
	point->x = g_deviceWidth - 500;
	point->y = g_deviceHeight - 50;
	g_menu.SetDestPoint( point );




	// WE MUST SET THE MASTER FRAME
	// FOR COLLISION DETECTION TO WORK!!!!
	g_world.masterFrame = true;

    return S_OK;
}

//-----------------------------------------------------------------------------
// Name: Cleanup()
// Desc: Releases all previously initialized objects
//-----------------------------------------------------------------------------
VOID Cleanup()
{
	g_world.Remove();

	ShutdownD3D();
}

//-----------------------------------------------------------------------------
// Name: GameLoop()
// Desc: Draws the scene
//-----------------------------------------------------------------------------
VOID GameLoop()
{

	//lets try som DInput
	char* text;
	text = g_textbox.GetLastText();
	if( text != NULL )
	{
		g_pFont->PrintText( text, 10,120, 5000);
	}


	int frames = g_timer.FrameCount();
	if( frames != 0 )
	{
		char *frameCount = NumberToString(frames);
		g_pFont->PrintText( frameCount,10,10,CLOCKS_PER_SEC);
	}

	char strMessage[255] = {0};
	sprintf(strMessage, "VPos.x: %f!", g_sprite->vPosition.x);
	g_pFont->PrintText( strMessage ,120,10,CLOCKS_PER_SEC);

    g_pd3dDevice->Clear( 0, NULL, (D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER), 
		D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

    // Begin the scene
    g_pd3dDevice->BeginScene();

	g_camera.Update();
	g_world.Render();

    // End the scene
    g_pd3dDevice->EndScene();

	//update mouse position
	POINT mousePos = {g_mouseData.x, g_mouseData.y};
	g_mouse->SetDestPoint(&mousePos);
	g_menu.Render();
	g_pFont->Render();			

    // Present the backbuffer contents to the display
    g_pd3dDevice->Present( NULL, NULL, NULL, NULL );
}

//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: The window's message handler
//-----------------------------------------------------------------------------
LRESULT WINAPI MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
    switch( msg )
    {
        case WM_DESTROY:
			Error("hey!");
            PostQuitMessage( 0 );
            return 0;
			break;

		case WM_QUIT:
			Error("hey");
			return 0;
			break;

		//case WM_ACTIVATE:
		//	AcquireInput();
			//return 0;
    }

    return DefWindowProc( hWnd, msg, wParam, lParam );
}

//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: The application's entry point
//-----------------------------------------------------------------------------
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR, INT )
{
	g_hInstance = hInst;

    // Register the window class
    WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                      GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                      "D3D Tutorial", NULL };
	g_wc = &wc;
    RegisterClassEx( g_wc );

	// Create the application's window
    g_hWnd = CreateWindow( "D3D Tutorial", "Fabs stogy engine",
                              WS_OVERLAPPEDWINDOW, 100, 100, g_deviceWidth, g_deviceHeight,
                              GetDesktopWindow(), NULL, g_wc->hInstance, NULL );

	SetCursor(NULL);

    // Initialize Direct3D
    if( SUCCEEDED( Init() ) )
    {
            // Show the window
            ShowWindow( g_hWnd, SW_SHOWDEFAULT );
            UpdateWindow( g_hWnd );

            // Enter the message loop
            MSG msg;
            ZeroMemory( &msg, sizeof(msg) );
            while( msg.message != WM_QUIT )
            {
				SetCursor(NULL);

                if( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
                {
                    TranslateMessage( &msg );
                    DispatchMessage( &msg );
					
					CheckInput();
					CheckSaverInput( &msg );
                }
                else
				{
					CheckDInput();
					GameLoop();
				}
            }
    }

	Error("EY!!!");
    // Clean up everything and exit the app
    Cleanup();
    UnregisterClass( "D3D Tutorial", g_wc->hInstance );
    return 0;
}
