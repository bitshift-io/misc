#ifndef _DIRECT3D
#define _DIRECT3D

#define SURFACEFORMAT D3DFMT_A8R8G8B8

//LPDIRECT3DSURFACE8 g_pBackSurf = 0;
//LPDIRECT3DSURFACE8 g_pBmpSurf = 0;
//LPDIRECT3D8             g_pD3D       = NULL; // Used to create the D3DDevice
//LPDIRECT3DDEVICE8       g_pd3dDevice = NULL; // Our rendering device
//LPDIRECT3DVERTEXBUFFER8 g_pVB        = NULL; // Buffer to hold vertices

class CDirect3D : public C3DAPI
{
public:
	CDirect3D();

	virtual void Init3DAPI( HWND hWnd, bool fullScreen, int deviceWidth, int deviceHeight );
	virtual void Shutdown3DAPI();

	virtual void BeginScene();
	virtual void EndScene();

	//virtual void SetClearColor( COLOR* pColor );

private:
	LPDIRECT3DDEVICE8 pd3dDevice;
	LPDIRECT3DSURFACE8 pBackSurf;
};

CDirect3D::CDirect3D()
{

}

void CDirect3D::Init3DAPI( HWND hWnd, bool fullScreen, int deviceWidth, int deviceHeight )
{
	HRESULT r=0;


	//LPDIRECT3D8 pD3D = NULL; // Used to create the D3DDevice
	//pd3dDevice = NULL;
	//pBackSurf = NULL;

	

    // Create the D3D object.
    if( NULL == ( g_pD3D = Direct3DCreate8( D3D_SDK_VERSION ) ) )
	{
		Error("cant create D3D object");
        return;// E_FAIL;
	}	

    // Get the current desktop display mode, so we can set up a back
    // buffer of the same format
    D3DDISPLAYMODE d3ddm;
    if( FAILED( g_pD3D->GetAdapterDisplayMode( D3DADAPTER_DEFAULT, &d3ddm ) ) )
	{
		Error("Failed to get display mode");
        return;// E_FAIL;
	}

		

    // Set up the structure used to create the D3DDevice
    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory( &d3dpp, sizeof(d3dpp) );
    d3dpp.Windowed = fullScreen ? FALSE : TRUE;
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.MultiSampleType = D3DMULTISAMPLE_NONE;
	d3dpp.FullScreen_PresentationInterval = fullScreen ? D3DPRESENT_INTERVAL_IMMEDIATE : 0;
    d3dpp.BackBufferFormat = d3ddm.Format;
    d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.BackBufferFormat = fullScreen ? SURFACEFORMAT : d3ddm.Format;
	d3dpp.BackBufferCount = 1;
	d3dpp.BackBufferWidth = deviceWidth;
	d3dpp.BackBufferHeight = deviceHeight;

	d3dpp.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;

	if( hWnd == NULL )
		Error(NumberToString(fullScreen));

    // Create the D3DDevice
	r = g_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, g_hWnd,
                                      D3DCREATE_SOFTWARE_VERTEXPROCESSING,
                                      &d3dpp, &g_pd3dDevice );
    if( FAILED( r ) )
    {
		switch( r )
		{
		case D3DERR_INVALIDCALL:
			Error("inv call");
			break;
		case D3DERR_NOTAVAILABLE:
			Error("not avai");
			break;
		case D3DERR_OUTOFVIDEOMEMORY:
			Error("out of mem");
			break;
		}
		Error("can't create device");
        return;// E_FAIL;
    }
		
	// Get backbuffer
	r = g_pd3dDevice->GetBackBuffer(0,D3DBACKBUFFER_TYPE_MONO,&g_pBackSurf);
	if(FAILED(r))
	{
		Error("Failed to get back buffer");
		MessageBox(hWnd, "back buf","error",MB_OK);
	}	


    g_pd3dDevice->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_ONE);
    g_pd3dDevice->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_ONE);

	//g_pd3dDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);	
	//g_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	//g_pd3dDevice->SetRenderState(D3DRS_ALPHAREF, (DWORD)0x000000FF);
	//g_pd3dDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
	
	g_pd3dDevice->SetRenderState( D3DRS_DITHERENABLE, TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_ZENABLE, D3DZB_TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_SPECULARENABLE, TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_NORMALIZENORMALS, TRUE );

	g_pd3dDevice->SetRenderState( 	D3DRS_EDGEANTIALIAS, TRUE );
    g_pd3dDevice->SetRenderState( D3DRS_AMBIENT, 0x6f6f6f6f );

	
	g_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
    g_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW ); //temporary disable culling
    g_pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR  );
    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_MINFILTER, D3DTEXF_LINEAR  );


	//g_pBackSurf = pBackSurf;
	//g_pD3D = pD3D; // Used to create the D3DDevice
	//g_pd3dDevice = pd3dDevice; // Our rendering device
}

void CDirect3D::Shutdown3DAPI()
{

}

void CDirect3D::BeginScene()
{
	// Clear device
    pd3dDevice->Clear( 0, NULL, (D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER), 
		D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

    // Begin the scene
    pd3dDevice->BeginScene();
}

void CDirect3D::EndScene()
{
	// End the scene
    pd3dDevice->EndScene();

    // Present the backbuffer contents to the display
    pd3dDevice->Present( NULL, NULL, NULL, NULL );
}

#endif
