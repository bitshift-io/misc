#ifndef _OBJECT
#define _OBJECT

//for sum run time information
enum OBJECT_TYPE{OBJECT, 
	IMAGE, GUIIMAGE, LAYER,		//2d stuffs
	OBJECT3D, FRAME, MESH, LIGHT, CAMERA, ADVMESH,	//3d stuffs
	OTHER
	}; //sounds stuffs


//this will eventually be the core object for everything!
class CObject
{
public:
	CObject();
	virtual void Render();
	void Update();	
	virtual void Remove();
	virtual OBJECT_TYPE GetType() const = 0; 

	void SetNext( CObject* next){ this->next = next; }
	void* GetNext(){ return next; }
	void SetParent( CObject* parent){ this->parent = parent; }
	void* GetParent(){ return parent; }
	unsigned int GetSize(){ return sizeof( *this ); }
	void SetCallbackFn( void (*CallbackFn)(CObject* owner));
	

	void (*CallbackFn)(CObject* owner);
	CObject* next;
	long ID;
	char* name;
	CObject* parent;

private:

};


OBJECT_TYPE CObject::GetType() const
{
	return OBJECT;
}

void CObject::SetCallbackFn( void (*CallbackFn)(CObject* owner) )
{
	this->CallbackFn = CallbackFn;
}

CObject::CObject()
{
	CallbackFn = NULL;
	next = NULL;
	parent = NULL;
	ID = 0;
}

void CObject::Render()
{

}

void CObject::Remove()
{
	//clean up code, removeing is killing, if u want 
	//to change parents dont call this! call SetParent
	CallbackFn = NULL;
	next = NULL;
	parent = NULL;
	delete this;
}

#endif
