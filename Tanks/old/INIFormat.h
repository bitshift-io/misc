#ifndef _INIFORMAT
#define _INIFORMAT

/* *********************************************
Fabian Mathews
2002

  INI File loader/saver
  requires DataManip.h

********************************************* */

/*

  ;comment

  [title]
  setting=variable

  */

#include <stdio.h>

class CINI
{
public:
	CINI();
	~CINI();
	CINI(char *path);

	void SetPath(char *path);
	void SetSection(char *section);
	char *ReadKey(char *key);
	void WriteKey(char *key);

private:
	FILE *pFile;
	char *path;
	char *section;
};

CINI::CINI()
{
	section = NULL;
	path = NULL;
	pFile = NULL;
}

CINI::CINI(char *path)
{
	section = NULL;
	this->path = path;	
	pFile = fopen(path,"r");
}

CINI::~CINI()
{
	if(pFile != NULL)
		fclose(pFile);

	path = NULL;
	section = NULL;
}

void CINI::SetPath(char *path)
{
	this->path = path;
	
	pFile = fopen(path,"r");
}

void CINI::SetSection(char *section)
{
	this->section = section;
}

char *CINI::ReadKey(char *key)
{
	//search for the section
	//read in a line at a time
	//chop it up
	// dont forget to rewind the filepointer
	if( pFile == NULL )
		return NULL;

	if( section == NULL )
		return NULL;

	bool sectionFound = false;
	bool keyFound = false;
	bool fail = false;
	char *retValue = (char*)malloc(sizeof(char)*100);
	char *left = (char*)malloc(sizeof(char)*100);

	char *pFileLine = (char*)malloc(sizeof(char)*100);

	while( sectionFound != true && !feof(pFile) )
	{
		fgets(pFileLine, 100, pFile);
		sectionFound = StringSearch(section, pFileLine);		
	}

	if( feof(pFile) )
		return NULL;

	if( sectionFound == true)
		while( keyFound != true && !feof(pFile) )
		{
			fgets(pFileLine, 100, pFile);
			keyFound = StringSearch(key, pFileLine);

			if( strcmp(pFileLine,"\n")==0 )
				Error("Key not found under right Section");
		}

	if( keyFound == true )
	{
		//now replace the /n if it exists with /0
		int i = 0;
		while( pFileLine[i] != '\n' )
		{
			i++;
		}
		pFileLine[i] = '\0';

		StringDivide(pFileLine, left, retValue, '=');		
	}	

	rewind(pFile);
	return retValue;
}

void CINI::WriteKey(char *key)
{
	//search the file for the section
	//then place it at the end of it
	// dont forget to rewind the filepointer

	rewind(pFile);
}

#endif