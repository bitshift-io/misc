#ifndef _GUITEXTBOX
#define _GUITEXTBOX

//so we have a text box, it needs to get the key presses...
// it needs to store a string of letters
// which can be accesses by other classes etc...
//extern class CFont;

class CGUITextbox : public CGUIImage
{
public:
	CGUITextbox();
	void Update();
	void Render();
	void OnMouseClick();
	char* GetText(){ return text; } //returns current text
	char* GetLastText();
	void SetText( char* text ){ this->text = text; }
	void SetMaxChars( int num ){ maxChars = num; }
	void SetFont( CFont* font ){ this->font = font; }

private:
	RECT srcText;
	CFont* font;
	LPDIRECT3DSURFACE8 textImg;
	char* text;
	char* lastText; //a copy of last string when lost focus
	int maxChars; // no chars that will fit before it scrolls/gets cropped etc..
};

CGUITextbox::CGUITextbox()
{
	font = NULL;
	text = new char[256];
	text = ">";
	textImg = NULL;

	srcText.top = 0;
	srcText.left = 0;
	srcText.bottom = 0;
	srcText.right = 0;

	maxChars = 10;
}

char* CGUITextbox::GetLastText()
{
	//can only return the text when
	// we are finished i.e when we loss
	//focus

	//this is DIFFERENT to GetText()!!!!

	if( focus == true)
		return NULL;

	char* returnText = text;
	text = ">";
	return lastText;
}

void CGUITextbox::Render()
{
	Update();

	CopySurfaceFast(&image, &srcRect, &g_pBackSurf, &realDestPoint);
	CopySurfaceFast(&textImg, &srcText, &g_pBackSurf, &realDestPoint);
}

void CGUITextbox::Update()
{
	CGUIImage::Update();

	//cheack to see if a key has been pressed,
	// if we have focus
	// then append the string, and update the picture as text
	if( focus == true )
		for(int i=0; i < 256; i++ )
		{
			int value = g_keyboardData.key[i];

			if( value == 2)
			{
				

				//special characters to manipulate string
				switch( i )
				{
				case DIK_RETURN:
					lastText = text;
					text = ">";
					this->focus = false;
					break;
				}

				//make sure if we append another string, 
				//we do something special if it wont fit...
				if( (int)strlen(text) < maxChars )
					//theres gotta be a better way to do this?
					//should i even use DI??
					switch( i )
					{
					case DIK_A:
						text = StringCat(text,"a");
						break;
					case DIK_B:
						text = StringCat(text,"b");
						break;
					case DIK_C:
						text = StringCat(text,"c");
						break;
					case DIK_D:
						text = StringCat(text,"d");
						break;
					case DIK_E:
						text = StringCat(text,"e");
						break;
					case DIK_F:
						text = StringCat(text,"f");
						break;
					case DIK_G:
						text = StringCat(text,"g");
						break;
					case DIK_H:
						text = StringCat(text,"h");
						break;
					case DIK_I:
						text = StringCat(text,"i");
						break;
					case DIK_J:
						text = StringCat(text,"j");
						break;
					case DIK_K:
						text = StringCat(text,"k");
						break;
					case DIK_L:
						text = StringCat(text,"l");
						break;
					case DIK_M:
						text = StringCat(text,"m");
						break;
					case DIK_N:
						text = StringCat(text,"n");
						break;
					case DIK_O:
						text = StringCat(text,"o");
						break;
					case DIK_P:
						text = StringCat(text,"p");
						break;
					case DIK_Q:
						text = StringCat(text,"q");
						break;
					case DIK_R:
						text = StringCat(text,"r");
						break;
					case DIK_S:
						text = StringCat(text,"s");
						break;
					case DIK_T:
						text = StringCat(text,"t");
						break;
					case DIK_U:
						text = StringCat(text,"u");
						break;
					case DIK_V:
						text = StringCat(text,"v");
						break;
					case DIK_W:
						text = StringCat(text,"w");
						break;
					case DIK_X:
						text = StringCat(text,"x");
						break;
					case DIK_Y:
						text = StringCat(text,"y");
						break;
					case DIK_Z:
						text = StringCat(text,"z");
						break;
					case DIK_SPACE:
						text = StringCat(text," ");
						break;
					}
			}
		}	

	font->PrintText(text, 0,0, -1);
	textImg = (LPDIRECT3DSURFACE8)font->GetLastTextAsImage();

	D3DSURFACE_DESC desc;
	textImg->GetDesc( &desc );

	srcText.top = 0;
	srcText.left = 0;
	srcText.bottom = desc.Height;
	srcText.right = desc.Width;
}

void CGUITextbox::OnMouseClick()
{
	CGUIImage::OnMouseClick();

	focus = true;
}

#endif




