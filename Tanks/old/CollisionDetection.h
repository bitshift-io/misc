#ifndef _COLLISIONDETECTION
#define _COLLISIONDETECTION

//vertices list:
bool CheckCollision( CAdvMesh* pObj1, CAdvMesh* pObj2 );
bool CheckPolyCollision( CAdvMesh* pObj1, CAdvMesh* pObj2 );
bool CheckBoundsCollision( CAdvMesh* pObj1, CAdvMesh* pObj2 );

void CheckAllCollisions(  CObject* meshList )
{
	LogSetFile("collision.log");

	int count = 0;
	CAdvMesh* temp = (CAdvMesh*)meshList;
	while( temp != NULL )
	{
		count++;
		temp = (CAdvMesh*)temp->colNext;
	}
	if( count < 2)
		return;
	//Error(NumberToString(count));


	//now compare all meshes
	// and call the right functions
	CAdvMesh* primaryColObj = (CAdvMesh*)meshList;
	CAdvMesh* secondaryColObj = (CAdvMesh*)primaryColObj->colNext;

	while( primaryColObj->colNext != NULL )
	{		
		while( secondaryColObj != NULL )
		{
			bool result = CheckCollision(primaryColObj, secondaryColObj);
			if( result )
			{
				primaryColObj->OnCollision( secondaryColObj );
				secondaryColObj->OnCollision( primaryColObj );
			}			
			secondaryColObj = (CAdvMesh*)secondaryColObj->colNext;
		}		

		primaryColObj = (CAdvMesh*)primaryColObj->colNext;
		secondaryColObj = (CAdvMesh*)primaryColObj->colNext;
	}
	
	
}

bool CheckCollision( CAdvMesh* pObj1, CAdvMesh* pObj2 )
{
	bool result = false;
	result = CheckBoundsCollision(pObj1, pObj2);
	
	return result;
}

bool CheckBoundsCollision( CAdvMesh* pObj1, CAdvMesh* pObj2 )
{
	//check radius of each obj to see if they are in range
	bool result = false;

	//get the center of the shpheres
	// say (x1, y1, z1) and (x2, y2, z2)
	// then  (x1 - x2)^2 + (y1 + y2)^2 + (z1 + z2)^2 (^ is power), 
	// then square root that all, thats the length/deistance
	// between the shpheres, add thier radii together,
	// and check to to make sure the radiis are bigger
	// or we have a collision... eeek

	CAdvMesh* pObjA = pObj1;
	CAdvMesh* pObjB = pObj2;
	
	float aX = pObjA->vPosition.x;
	float aY = pObjA->vPosition.y;
	float aZ = pObjA->vPosition.z;
	float bX = pObjB->vPosition.x;
	float bY = pObjB->vPosition.y;
	float bZ = pObjB->vPosition.z;

	float x = (aX - bX);
	float y = (aY - bY);
	float z = (aZ - bZ);

	float distanceBetweenSpheres = (float)sqrt( pow(x,2)+pow(y,2)+pow(z,2) );

	//now whats the radii's added together?
	float minDistance = pObjA->boundSphereRadius + pObjB->boundSphereRadius;

	if( distanceBetweenSpheres <= minDistance  )
	{
		result = CheckPolyCollision( pObj1, pObj2 );
	}

	return result;
}

bool CheckPolyCollision( CAdvMesh* pObj1, CAdvMesh* pObj2 )
{
	HRESULT r = 0;

	//Error("per poly detection");
//	http://www.flipcode.com/tpractice/issue02.shtml
// http://www.geocities.com/SiliconValley/2151/math3d.html
// http://www.geometryalgorithms.com/Archive/algorithm_0105/algorithm_0105.htm#Triangle-Triangle
	
	long noVert1 = pObj1->pMesh->GetNumVertices();
	long noVert2 = pObj2->pMesh->GetNumVertices();

	CUSTOMVERTEX* vertData1 = pObj1->vertexData;
	CUSTOMVERTEX* vertData2 = pObj2->vertexData;


	//all planes will collide unless they are parallel, ie.e nroamls are parallel
	// cross product
	//use a X b = (ya * zb)+(za*yb)
	for(int i=0; i < noVert1/3; i++)
	{



		//construct a normal...dx doesnt do it the way we need it
		D3DXVECTOR3 normal1;
		D3DXVECTOR3 vector11, vector12;

		vector11.x = vertData1[i].point.x - vertData1[i+1].point.x;
		vector11.y = vertData1[i].point.y - vertData1[i+1].point.y;
		vector11.z = vertData1[i].point.z - vertData1[i+1].point.z;

		vector12.x = vertData1[i].point.x - vertData1[i+2].point.x;
		vector12.y = vertData1[i].point.y - vertData1[i+2].point.y;
		vector12.z = vertData1[i].point.z - vertData1[i+2].point.z;

		D3DXVec3Cross(&normal1, &vector11, &vector12); 

		Log("---model 1-----\n");
		for(int t=0; t<3; t++)
		{
			char logString[100];
			sprintf( logString, "v: %f ", vertData1[i+t].point.x);
			Log(logString);
			sprintf( logString, "%f ", vertData1[i+t].point.y);
			Log(logString);
			sprintf( logString, "%f ", vertData1[i+t].point.z);
			Log(logString);
			Log("\n");							
		}

		//eqaution of plane in triangle1, is...
		float k1 = (vertData1[i].point.x * normal1.x) +
					(vertData1[i].point.y * normal1.y) +
					(vertData1[i].point.z * normal1.z);


		Log("---model 2-----\n");
		for(int j=0; j < noVert2/3; j++)
		{


					for(int t=0; t<3; t++)
					{
						char logString[100];
						sprintf( logString, "v: %f ", vertData1[j+t].point.x);
						Log(logString);
						sprintf( logString, "%f ", vertData1[j+t].point.y);
						Log(logString);
						sprintf( logString, "%f ", vertData1[j+t].point.z);
						Log(logString);
						Log("\n");
					}

			//now computer objb tri plane....
			D3DXVECTOR3 normal2;
			D3DXVECTOR3 vector21, vector22;

			vector21.x = vertData2[j].point.x - vertData2[j+1].point.x;
			vector21.y = vertData2[j].point.y - vertData2[j+1].point.y;
			vector21.z = vertData2[j].point.z - vertData2[j+1].point.z;

			vector22.x = vertData2[j].point.x - vertData2[j+2].point.x;
			vector22.y = vertData2[j].point.y - vertData2[j+2].point.y;
			vector22.z = vertData2[j].point.z - vertData2[j+2].point.z;

			D3DXVec3Cross(&normal2, &vector21, &vector22);

			//do planes intersect, prolly....
			D3DXVECTOR3 crossOfNormals;
			D3DXVec3Cross( &crossOfNormals, &normal1, &normal2 );
			float lengthOfCross = D3DXVec3Length( &crossOfNormals );

			if( lengthOfCross != 0 )
			{
				//Error("planes intersect");
				//so the planes intersect, but do the polys?

				//time to test if the poly meet....
				// if one of the points is on the wrong side of the plane,
				// then the poly intersecs the plane

				//for each vertex, check to see what side of the plane its on
				// <0 is one side >0 is the other side
				
				//what side of plane?
				int sideOfPlane[3] = {0,0,0};

				for( int k=0; k < 3; k++ )
				{
					//so plugin in each vertex into the equation
					int result = (int)( k1 - 
							((vertData2[j+k].point.x * normal1.x) +
							(vertData2[j+k].point.y * normal1.y) +
							(vertData2[j+k].point.z * normal1.z)));

					
					if( result < 0 )
					{
						Error("eh?");
						sideOfPlane[k] = -1;
					}
					if( result > 0 )
					{
						//Error("hey!");
						sideOfPlane[k] = 1;
					}
					
					//Log(NumberToString(sideOfPlane[k]));
				}

				//if( sideOfPlane[0] != sideOfPlane[1] != sideOfPlane[2] )
				if( ( sideOfPlane[0] != sideOfPlane[1] ) || 
					( sideOfPlane[0] != sideOfPlane[2] ) ||
					( sideOfPlane[1] != sideOfPlane[2] ))
				{
					//this means triangle2 intersects triangle1's plane
					Error("we have a collision");
					//return true;
				}
			}			
		}
	}

	//vertexBuffer1->Unlock();

	return false;
}

#endif