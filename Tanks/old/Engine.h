#ifndef _ENGINE
#define _ENGINE

//This is where every thing comes together
// to make a nice engine class :)

//-----------------------------------------------------------------------------
// DIRECTX Include files
//-----------------------------------------------------------------------------
#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "Dinput8.lib")
#pragma comment(lib, "Dxguid.lib")
#pragma comment(lib, "D3dx8.lib")   

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>
#include <Dinput.h>

//-----------------------------------------------------------------------------
// Custom Include files
//-----------------------------------------------------------------------------
#include "ErrorLogging.h"

#include "Windows.h"
//#include "3ds.h"

#include "Timming.h"
#include "DataManip.h"
#include "Structs.h"
#include "3DAPI.h"
#include "DirectX.h"
#include "Direct3D.h" 
#include "DirectInput.h"

//#include "3DSFormat.h"
#include "TGAFormat.h"
#include "Object.h"
#include "Object3D.h"
#include "Image.h"
#include "Layer.h"
#include "GUIImage.h"

#include "Light.h"
#include "Camera.h"

CCamera g_camera; //this is dogey, need a game class with camera
					// and d3d device etc in it...

#include "Sprite.h"
#include "Particle.h"
#include "Mesh.h"
#include "AdvMesh.h"
#include "CollisionDetection.h"

#include "List.h"
#include "Font.h"
#include "Frame.h"

#include "GUITextbox.h"

#include "INIFormat.h"

#include "WinInput.h"

#include "Vertices.h"

//GLOABLAS =================================================
CINI g_ini("data/config.ini");



//=================================================

class CEngine 
{
public:
	CEngine( HINSTANCE hInstance );
	~CEngine();
	virtual void Init();
	virtual void MainLoop(); //runs till exit - false
	virtual void Shutdown();
	virtual void Set3DAPI( int API );

	CFrame world; //the main frame for the engine, everything 3d is added here
	HWND hWnd; //the handle to our app/window
	HINSTANCE hInstance; //this iis taken strait from WinMain
	WNDCLASSEX wc; //our windows class, prolly dont need to keep track of it

	//cal back f'ns
	bool (*MainLoopCallbk)(CEngine* pEngine);

	int deviceWidth;
	int deviceHeight;
	bool fullScreen;

	char* appName;

	C3DAPI* API;

private:

};

CEngine::CEngine( HINSTANCE hInstance )
{
	this->hInstance = hInstance;

	MainLoopCallbk = NULL;	

	deviceWidth = 600;
	deviceHeight = 800;
	fullScreen = true;

	appName = "Siphon Engine - Fabian Mathews";
}

CEngine::~CEngine()
{
	Shutdown();
}

void CEngine::Init()
{
	world.masterFrame = true;

	// Register the window class
    WNDCLASSEX tempWc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                      GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                      appName, NULL };

	wc = tempWc;
	
    RegisterClassEx( &wc );

	// Create the application's window
    hWnd = CreateWindow( appName, appName,
                              WS_OVERLAPPEDWINDOW, 100, 100, g_deviceWidth, g_deviceHeight,
                              GetDesktopWindow(), NULL, wc.hInstance, NULL );

	SetCursor(NULL);

	// Show the window
    ShowWindow( hWnd, SW_SHOWDEFAULT );
    UpdateWindow( hWnd );

	//this bit is temporoary ///
	g_hWnd = hWnd;
	g_wc = &wc;
	g_hInstance = hInstance;

	if( g_hWnd == NULL || g_wc == NULL || g_hInstance == NULL)
		Error("we in sh*t");
	///////////////////////////


	//CDirect3D* test = new CDirect3D();
	//test->Init3DAPI( hWnd, fullScreen, 600, 800);

	//API = new CDirect3D();
	//API->Init3DAPI( hWnd, fullScreen, deviceWidth, deviceHeight );	
	//InitD3D();

	InitD3D( hWnd );
	InitDInput();
			
}

void CEngine::MainLoop()
{
	bool r = 0;
	
	// Enter the message loop
    MSG msg;
    ZeroMemory( &msg, sizeof(msg) );
    while( msg.message != WM_QUIT )
    {
		SetCursor(NULL);

        if( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
        {
			TranslateMessage( &msg );
            DispatchMessage( &msg );
					
			CheckInput();
		}
        else
		{					
			CheckDInput();
			if( MainLoopCallbk != NULL )
			{
				r = MainLoopCallbk( this );
				if( r == false )
					break;
			}
		}
	}
}

void CEngine::Shutdown()
{
	UnregisterClass( appName, g_wc->hInstance );
}

//make this an enum l8r
void CEngine::Set3DAPI( int API )
{
/*
	switch( API )
	{
	case 1: //directx
		this->API = new CDirect3D();
		break;
	case 2: // ogl
		break;
	default: //directx
		//CDirect3D* newAPI = 
		this->API = new CDirect3D();
		break;
	}*/

	//this->API = new CDirect3D();

	//this->API->Init3DAPI( hWnd, fullScreen, deviceWidth, deviceHeight );	
}

#endif