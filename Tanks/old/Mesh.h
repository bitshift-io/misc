#ifndef _MESH
#define _MESH

// ************************************************
// ---------------- CMesh ------------------------
// This contains the mesh data, currenly loads
// X files.
// ************************************************
#include "3DSFormat.h"



class CMesh : public CObject3D
{
public:
	void Remove();
	HRESULT LoadXFile( char* pstrPathName );	//Loads a mesh from a file
	bool Load3DSFile( char* pstrPathName );
	void Render();							// Renders the mesh
	unsigned int GetSize(){ return sizeof( *this ); }
	virtual OBJECT_TYPE GetType() const {return MESH;}

	DWORD numMaterials;
	LPD3DXMESH pMesh;
	D3DMATERIAL8* pMaterials;
	LPDIRECT3DTEXTURE8* pTextures;

private:

};

bool CMesh::Load3DSFile( char* pstrPathName )
{
	pstrPathName = StringCat(g_meshPath, pstrPathName);

	float* vertexList = NULL;
	unsigned int* faceList = NULL;

	Load3DS( pstrPathName, vertexList, faceList );

	return true;
}

HRESULT CMesh::LoadXFile( char* pstrPathName )
{
	HRESULT r = 0;


	//add the meshPath to the model file
	//Error( StringCat("models/", pstrPathName) );
	pstrPathName = StringCat(g_meshPath, pstrPathName);

	// The buffer to hold the materials
	LPD3DXBUFFER pMaterialBuffer = 0;

	// Temporaryly hold mesh data
	LPD3DXMESH pTempMesh;

    // Load the mesh from the specified file
    if( FAILED( D3DXLoadMeshFromX( pstrPathName, D3DXMESH_SYSTEMMEM, 
                                   g_pd3dDevice, NULL, 
                                   &pMaterialBuffer, &numMaterials, 
                                   &pTempMesh ) ) )
    {
		Error("Cant Load Mesh");
        return E_FAIL;
    }

	//we do this if we eva need to get a list of polys :)
	//now we should clone out mesh using a FVF, into pMesh
	// D3DFVF_CUSTOMVERTEX
	r = pTempMesh->CloneMeshFVF( 0, D3DFVF_CUSTOMVERTEX, g_pd3dDevice, &pMesh );
	if( FAILED(r) )
	{
		Error("cant clone mesh to proper FVF");
	}

    // We need to extract the material properties and texture names from the 
    // pMaterialBuffer
    D3DXMATERIAL* d3dxMaterials = (D3DXMATERIAL*)pMaterialBuffer->GetBufferPointer();
    pMaterials = new D3DMATERIAL8[numMaterials];
    pTextures  = new LPDIRECT3DTEXTURE8[numMaterials];

    for( DWORD i=0; i<numMaterials; i++ )
    {
        // Copy the material
        pMaterials[i] = d3dxMaterials[i].MatD3D;

        // Set the ambient color for the material (D3DX does not do this)
        pMaterials[i].Ambient = pMaterials[i].Diffuse;

		//now append the model texture path
		d3dxMaterials[i].pTextureFilename = StringCat(g_meshTexturePath, 
										d3dxMaterials[i].pTextureFilename);
     
        // Create the texture
        if( FAILED( D3DXCreateTextureFromFile( g_pd3dDevice, 
                                               d3dxMaterials[i].pTextureFilename, 
                                               &pTextures[i] ) ) )
        {
			Error("cant load texture");
            pTextures[i] = NULL;
        }
    }

    // Done with the material buffer
    pMaterialBuffer->Release();	

	return S_OK;
}

void CMesh::Render()
{
	Update();
	
	// Loop for each material
	for( int i = 0 ; i < (int)numMaterials ; i++ )
	{


		g_pd3dDevice->SetMaterial( &pMaterials[i] );
        g_pd3dDevice->SetTexture( 0, pTextures[i] );

		// Render this subset of the mesh
		pMesh->DrawSubset(i);

		// Reset the vertex shader
		g_pd3dDevice->SetVertexShader( D3DFVF_CUSTOMVERTEX );
	}
}

void CMesh::Remove()
{
	//clean up code, removeing is killing, if u want 
	//to change parents dont call this! call SetParent
	CallbackFn = NULL;
	next = NULL;
	parent = NULL;

	pMesh->Release();
	delete pMaterials;
	pMaterials = NULL;
	delete pTextures;
	pTextures = NULL;
	
	delete this;
}

#endif



