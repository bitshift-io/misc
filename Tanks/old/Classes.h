void Error(char *text);

// ************************************************
// ---------------- CObject ------------------------
// This is the core of most classes in the engine.
// ************************************************
class CObject
{
public:
	CObject();
	~CObject();
	CObject( CObject& OtherObject );

// Functions
public:
	virtual HRESULT Render();

	void SetNext( CObject* pNext ){ m_pNext = pNext; }
	void* GetNext(){ return m_pNext; }

	void* GetParentFrame(){ return m_pParentFrame; }
	void SetParentFrame( void* pFrame ){ m_pParentFrame = pFrame; }

	virtual int GetSize(){ return sizeof( *this ); }
protected:
	

// Variables
public:
	char* m_strName;
	void* m_pParentFrame;
	
protected:
	CObject* m_pNext;

};

CObject::CObject( CObject& OtherObject )
{
	m_strName = OtherObject.m_strName;
	m_pParentFrame = OtherObject.m_pParentFrame;
	m_pNext = OtherObject.m_pNext;
}

CObject::CObject()
{
	m_strName = 0;

	m_pNext = 0;

	m_pParentFrame = NULL;
}

CObject::~CObject()
{
	if( m_strName )
		delete m_strName;
}

HRESULT CObject::Render()
{
	return S_OK;
}

// ************************************************
// ---------------- CFrame ------------------------
// Everthing has to be attached to a frame to be 
// rendered.
// ************************************************
class CFrame;
typedef int (*FRAME_MOVEMENT_CALLBACK)( CFrame* pFrame, void* Parameter );

class CFrame
{
public:
	CFrame();
	~CFrame();

// Variables
public:
	void* m_pParameter;

protected:
	// The local matrix for this frame
	D3DXMATRIX m_mLocal;

	// The position of this frame
	D3DXVECTOR3 m_vPosition;
	// The velocity of this frame
	D3DXVECTOR3 m_vVelocity;
	
	// The orientation of this frame
	float m_Yaw, m_Pitch, m_Roll;

	// The list of objects in this frame
	CObject* m_pObjectList;

	// Pointer to the next frame in the list
	CFrame* m_pNext;

	CFrame* m_pChildFrameList;
	CFrame* m_pParentFrame;

	FRAME_MOVEMENT_CALLBACK m_pfnCallback;
	BOOL m_bCallback;

	
// Functions
public:
	HRESULT SetCallback( FRAME_MOVEMENT_CALLBACK pfnCallback );
	
	void SetVelocity( float x, float y, float z );
	void GetVelocity( float& x, float& y, float& z );

	void SetPosition( float x, float y, float z );
	void GetPosition( float& x, float& y, float& z );

	// Returns the local matrix for this frame
	void GetLocal( D3DXMATRIX& pMatrix );

	void SetYaw( float Yaw ){ m_Yaw = Yaw;  }
	void GetYaw( float& Yaw){ Yaw = m_Yaw; }

	void SetPitch( float Pitch ){ m_Pitch = Pitch;  }
	void GetPitch( float& Pitch){ Pitch = m_Pitch; }

	void SetRoll( float Roll ){ m_Roll = Roll;  }
	void GetRoll( float& Roll){ Roll = m_Roll; }

	// Update the position of this objects
	void Update();	

	// Add an object to the frame
	HRESULT AddObject( CObject* pNewObject );

	// Render the objects
	HRESULT Render(); 

	// Set/Get the next pointer for use in the list
	void SetNext( CFrame* pNext ){ m_pNext = pNext; }
	CFrame* GetNext(){ return m_pNext; }

	
	HRESULT AddFrame( CFrame* pNewFrame );


protected:

	void SetParent( CFrame* pParent ){ m_pParentFrame = pParent; }
	CFrame* GetParent(){ return m_pParentFrame; }
};


CFrame::CFrame()
{
	// Set the orientation to 0
	m_Yaw = 0.0f;
	m_Pitch = 0.0f;
	m_Roll = 0.0f;

	// Set the position and velocity to 0
	m_vPosition = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	m_vVelocity = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );

	// Init the local matrix to an identity
	D3DXMatrixIdentity( &m_mLocal );

	// Zero out the object list
	m_pObjectList = 0;

	m_pNext = 0;
	m_pChildFrameList = 0;
	m_pParentFrame = 0;

	m_pfnCallback = 0;
	m_bCallback = FALSE;
}

CFrame::~CFrame()
{
	
	
	
}

HRESULT CFrame::SetCallback( FRAME_MOVEMENT_CALLBACK pfnCallback )
{
	if( !pfnCallback )
	{
		m_bCallback = FALSE;
		m_pfnCallback = NULL;
		return E_FAIL;
	}

	m_pfnCallback = pfnCallback;

	m_bCallback = TRUE;

	return S_OK;
}

HRESULT CFrame::AddFrame( CFrame* pNewFrame )
{
	// Make sure the new frame is valid
	if( !pNewFrame )
	{
		Error( "Failed in attempt to add an invalid child frame" );
		return E_FAIL;
	}

	pNewFrame->SetParent( this );
	
	if( !m_pChildFrameList )
	{
		m_pChildFrameList = pNewFrame;
	}
	else
	{
		CFrame* pTempFrame = m_pChildFrameList;

		while( pTempFrame->GetNext() )
			pTempFrame = pTempFrame->GetNext();

		pTempFrame->SetNext( pNewFrame );
	}
	
	return S_OK;
}


HRESULT CFrame::AddObject( CObject* pNewObject )
{
	// Return if the new object is bogus
	if( !pNewObject )
		return E_FAIL;

	// Tell the object it has a new parent frame
	pNewObject->SetParentFrame( this );
	
	// If the object list does not exist yet...
	if( !m_pObjectList )
	{
		// ...Set this object to the start of the list
		m_pObjectList = pNewObject;
	}
	else 
	{
		// ...The list has already been created
		// so add this object to the end of the list
		
		// Get a pointer to the start of the list
		CObject* pObject = m_pObjectList;

		// Find the last object in the list
		while( pObject->GetNext() )
			pObject = (CObject*)pObject->GetNext();

		// Add this to the last item in the list
		pObject->SetNext( pNewObject );
	}

	return S_OK;
}

HRESULT CFrame::Render()
{
	
	// Update the position and orientation of the frame
	// and set the new world transform matrix
	Update();

	CFrame* pFrame = m_pChildFrameList;

	while( pFrame )
	{
		pFrame->Render();
		pFrame = pFrame->GetNext();
	}
	
	// Return if this frame has no visuals to render
	if( !m_pObjectList )
		return S_OK;

	// Get a pointer to the start of the list
	CObject* pObject = m_pObjectList;

	// Reset the transform in case those pesky children modified it
	g_pd3dDevice->SetTransform( D3DTS_WORLD, &m_mLocal );

	// Loop for each object in the list
	while( pObject )
	{
		// Render the object
		pObject->Render();

		// Increment to the next object in the list
		pObject = (CObject*)pObject->GetNext();
	}	

	return S_OK;

}

void CFrame::Update()
{

	if( m_bCallback )
		m_pfnCallback( this, m_pParameter );
	
	// Create some temporary matrices for the
	// rotation and translation transformations
	D3DXMATRIX mRotX, mRotY, mRotZ, mTrans, mRotTemp;

	// Update the position by the velocity
	m_vPosition.x += m_vVelocity.x;
	m_vPosition.y += m_vVelocity.y;
	m_vPosition.z += m_vVelocity.z;

	// Set the translation matrix
	D3DXMatrixTranslation( &mTrans, m_vPosition.x, m_vPosition.y, m_vPosition.z );

	// Set the rotation around the x axis
	D3DXMatrixRotationX( &mRotX, m_Pitch );
	// Set the rotation around the y axis
	D3DXMatrixRotationY( &mRotY, m_Yaw );
	// Set the rotation around the z axis
	D3DXMatrixRotationZ( &mRotZ, m_Roll );

	// Concatenate the y axis and x axis rotation matrices
	D3DXMatrixMultiply( &mRotTemp, &mRotX, &mRotY );
	// Concatenate the xy axes and z axis rotation matrices
	D3DXMatrixMultiply( &mRotTemp, &mRotZ, &mRotTemp );
	// Concatenate the xyz axes and translation matrices
	D3DXMatrixMultiply( &mTrans, &mRotTemp, &mTrans );

	// Update the copy of the local matrix
	m_mLocal = mTrans;	

	if( GetParent() )
	{
		D3DXMATRIX mParent;
		GetParent()->GetLocal( mParent );

		D3DXMatrixMultiply( &m_mLocal, &m_mLocal, &mParent);
	}
	

	// Set the world matrix
	g_pd3dDevice->SetTransform( D3DTS_WORLD, &m_mLocal );

}

// Returns the local transform matrix
void CFrame::GetLocal( D3DXMATRIX& Matrix )
{
	Update();
	
	Matrix = m_mLocal;
}

// Returns the velocity of the frame
void CFrame::GetVelocity( float& x, float& y, float& z )
{
	x = m_vVelocity.x;
	y = m_vVelocity.y;
	z = m_vVelocity.z;
}

// Sets the velocity of the frame
void CFrame::SetVelocity( float x, float y, float z )
{
	m_vVelocity.x = x;
	m_vVelocity.y = y;
	m_vVelocity.z = z;
}

// Returns the position of the frame
void CFrame::GetPosition( float& x, float& y, float& z )
{
	x = m_vPosition.x;
	y = m_vPosition.y;
	z = m_vPosition.z;
}

// Sets the position of the frame
void CFrame::SetPosition( float x, float y, float z )
{
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z;
}

// ************************************************
// ---------------- CCamera ------------------------
// Encapsulation of the view matrix into the 
// camera class.
// ************************************************
class CCamera : public CObject
{
public:
	CCamera();
	~CCamera();
	CCamera( CCamera& OtherCamera );
// Functions:
public:
	
	// Sets/Gets the up vector
	void SetUp( float x, float y, float z );
	void GetUp( float& x, float& y, float& z );

	// Sets/Gets the right vector
	void SetRight( float x, float y, float z );
	void GetRight( float& x, float& y, float& z );

	// Sets/Gets the velocity
	void SetVelocity( float x, float y, float z );
	void GetVelocity( float& x, float& y, float& z );

	// Sets/Gets Position
	void SetPosition(float x, float y, float z );
	void GetPosition( float& x, float& y, float& z );

	// Sets/Gets the look point
	void SetLookPoint(float x, float y, float z );
	void GetLookPoint( float& x, float& y, float& z );

	// Updates the position of the camera
	void Update();

	// Moves the camera
	void Move(float x, float y, float z );

	// Sets the roll, pitch, or yaw
	void SetRoll( float Roll );
	void SetYaw( float Yaw );
	void SetPitch( float Pitch );

	// Returns the roll, pitch, or yaw
	void GetRoll( float& Roll );
	void GetYaw( float& Yaw );
	void GetPitch( float& Pitch );

	// Resets the camera back to the origin
	void Reset();

	HRESULT Render();

	// Returns the size (in bytes) of this object
	int GetSize(){ return sizeof( *this ); }
protected:


// Variables
public:
		
protected:
	
	// The roll, pitch, and yaw
	// for the camera's orientation

	float m_Roll;	
	float m_Pitch;
	float m_Yaw;

	// The position
	D3DXVECTOR3 m_Position;	
	// The look at vector
	D3DXVECTOR3 m_LookAt;
	// The up vector
	D3DXVECTOR3 m_Up;
	// The rigth vector
	D3DXVECTOR3 m_Right;

	// The cameras velocity
	D3DXVECTOR3 m_Velocity;
};

CCamera::CCamera( CCamera& OtherCamera )
{
	m_LookAt = OtherCamera.m_LookAt;
	m_Pitch = OtherCamera.m_Pitch;
	m_Position = OtherCamera.m_Position;
	m_Right = OtherCamera.m_Right;
	m_Roll = OtherCamera.m_Roll;
	m_Up = OtherCamera.m_Up;
	m_Velocity = OtherCamera.m_Velocity;
	m_Yaw = OtherCamera.m_Yaw;
}

// Constructor
CCamera::CCamera()
{
	// Set the positon
	m_Position	= D3DXVECTOR3( 0.0f, 0.0f,-1.0f );
	// Set the velocity to 0
	m_Velocity	= D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
	
	// Set the lookat vector to straight ahead
	m_LookAt	= D3DXVECTOR3( 0.0f, 0.0f,-1.0f );
	// Set the rigth vector to right
	m_Right		= D3DXVECTOR3( 1.0f, 0.0f, 0.0f );
	// Set the up vector to up
	m_Up		= D3DXVECTOR3( 0.0f, 1.0f, 0.0f );


	// Set the roll, pitch, and yaw to 0
	m_Roll = m_Pitch = m_Yaw = 0.0f;
}

// Destructor
CCamera::~CCamera()
{

}

// Moves the camera back to the origin
void CCamera::Reset()
{
	m_Position	= D3DXVECTOR3( 0.0f, 0.0f,-1.0f );
	
	m_LookAt	= D3DXVECTOR3( 0.0f, 0.0f, 1.0f ); // z
	m_Right		= D3DXVECTOR3( 1.0f, 0.0f, 0.0f ); // x
	m_Up		= D3DXVECTOR3( 0.0f, 1.0f, 0.0f ); // y


	m_Roll = m_Pitch = m_Yaw = 0.0f;
}

// Updates the Direct3D view matrix
void CCamera::Update()
{
	// Update the x position
	m_Position.x += m_Velocity.x * m_Right.x;
	m_Position.y += m_Velocity.x * m_Right.y;
	m_Position.z += m_Velocity.x * m_Right.z;

	// Update the y position
	m_Position.x += m_Velocity.y * m_Up.x;
	m_Position.y += m_Velocity.y * m_Up.y;
	m_Position.z += m_Velocity.y * m_Up.z;
	
	// Update the z position
	m_Position.x += m_Velocity.z * m_LookAt.x;
	m_Position.y += m_Velocity.z * m_LookAt.y;
	m_Position.z += m_Velocity.z * m_LookAt.z;
	
	D3DXMATRIX mPitch, mRoll, mYaw;
	
	// Normalize and Regenerate the Look, Right, and Up Vectors
	D3DXVec3Normalize( &m_LookAt, &m_LookAt );
	D3DXVec3Cross( &m_Right, &m_Up, &m_LookAt );
	D3DXVec3Normalize( &m_Right, &m_Right );
	D3DXVec3Cross( &m_Up, &m_LookAt, &m_Right );
	D3DXVec3Normalize( &m_Up, &m_Up );
	
	// Set up the y-axis rotation
	D3DXMatrixRotationAxis( &mYaw, &m_Up, m_Yaw );
	D3DXVec3TransformCoord( &m_LookAt, &m_LookAt, &mYaw );
	D3DXVec3TransformCoord( &m_Right, &m_Right, &mYaw );

	// Set up the x-axis rotation
	D3DXMatrixRotationAxis( &mPitch, &m_Right, m_Pitch );
	D3DXVec3TransformCoord( &m_LookAt, &m_LookAt, &mPitch );
	D3DXVec3TransformCoord( &m_Up, &m_Up, &mPitch );

	// Set up the z-axis rotation
	D3DXMatrixRotationAxis( &mRoll, &m_LookAt, m_Roll );
	D3DXVec3TransformCoord( &m_Right, &m_Right, &mRoll );
	D3DXVec3TransformCoord( &m_Up, &m_Up, &mRoll );

	D3DXMATRIX mView;

	// Init the view matrix to an identity
	D3DXMatrixIdentity( &mView );

	// Fill in the view matrix 
	mView(0,0) = m_Right.x;	
	mView(0,1) = m_Up.x;
	mView(0,2) = m_LookAt.x;

	mView(1,0) = m_Right.y;
	mView(1,1) = m_Up.y;
	mView(1,2) = m_LookAt.y;

	mView(2,0) = m_Right.z;
	mView(2,1) = m_Up.z;
	mView(2,2) = m_LookAt.z;

	mView(3,0) = - D3DXVec3Dot( &m_Position, &m_Right );
	mView(3,1) = - D3DXVec3Dot( &m_Position, &m_Up );
	mView(3,2) = - D3DXVec3Dot( &m_Position, &m_LookAt );

	// Set the view matrix
	g_pd3dDevice->SetTransform( D3DTS_VIEW, &mView );
}

// Can be used to render an object to represent the camera
HRESULT CCamera::Render()
{
	// Not implemented
	return S_OK;
}


// Moves the camera relative to its current position
void CCamera::Move( float x, float y, float z )
{

	m_Position.x += x;
	m_Position.y += y;
	m_Position.z += z;

}

// Sets the position of the camera
void CCamera::SetPosition( float x, float y, float z )
{
	m_Position.x = x;
	m_Position.y = y;
	m_Position.z = z;
}

// Gets the position for the camera
void CCamera::GetPosition( float& x, float& y, float& z )
{
	x = m_Position.x;
	y = m_Position.y;
	z = m_Position.z;
}

// Sets the roll of the camera
void CCamera::SetRoll( float Roll )
{
	m_Roll = Roll;
}

// Gets the roll for the camera
void CCamera::GetRoll( float& Roll )
{
	Roll = m_Roll;
}

// Gets the yaw for the camera
void CCamera::GetYaw( float& Yaw )
{
	Yaw = m_Yaw;
}

// Sets the yaw for the camera
void CCamera::SetYaw( float Yaw )
{
	m_Yaw = Yaw;
}

// Gets the pitch for the camera
void CCamera::GetPitch( float& Pitch )
{
	Pitch = m_Pitch;
}

// Sets the pitch for the camera
void CCamera::SetPitch( float Pitch )
{
	m_Pitch = Pitch;
}

// Sets the point for the camera to look at
void CCamera::SetLookPoint( float x, float y, float z )
{
	m_LookAt.x = x;
	m_LookAt.y = y;
	m_LookAt.z = z;
}

// Gets the look vector
void CCamera::GetLookPoint( float& x, float& y, float& z )
{
	x = m_LookAt.x;
	y = m_LookAt.y;
	z = m_LookAt.z;
}

// Sets the up direction
void CCamera::SetUp( float x, float y, float z )
{
	m_Up.x = x;
	m_Up.y = y;
	m_Up.z = z;
}

// Gets the up vector
void CCamera::GetUp( float& x, float& y, float& z )
{
	x = m_Up.x;
	y = m_Up.y;
	z = m_Up.z;
}

// Sets the velocity
void CCamera::SetVelocity( float x, float y, float z )
{
	m_Velocity.x = x;
	m_Velocity.y = y;
	m_Velocity.z = z;
}

// Gets the velocity
void CCamera::GetVelocity( float& x, float& y, float& z )
{
	x = m_Velocity.x;
	y = m_Velocity.y;
	z = m_Velocity.z;
}

// Sets the right vector
void CCamera::SetRight( float x, float y, float z )
{
	m_Right.x = x;
	m_Right.y = y;
	m_Right.z = z;
}

// Gets the right vector
void CCamera::GetRight( float& x, float& y, float& z )
{
	x = m_Right.x;
	y = m_Right.y;
	z = m_Right.z;
}

// ************************************************
// ---------------- CMaterial ---------------------
// Material class, every mesh needs a material
// ***********************************************
class CMaterial 
{
public:
	CMaterial();
	~CMaterial();

// Variables
public:
	D3DMATERIAL8 m_Material;
protected:

// Functions:
public:
	void SetDiffuse( float r, float g, float b );
	void SetAmbient( float r, float g, float b );
	void SetSpecular( float r, float g, float b, float Power );
	void SetEmissive(  float r, float g, float b );

	HRESULT Update();
protected:


};

CMaterial::CMaterial()
{
	ZeroMemory( &m_Material, sizeof( D3DMATERIAL8 ) );

	m_Material.Diffuse.r = 1.0f;
	m_Material.Diffuse.g = 1.0f;
	m_Material.Diffuse.b = 1.0f;

	m_Material.Ambient.r = 0.5f;
	m_Material.Ambient.g = 0.5f;
	m_Material.Ambient.b = 0.5f;
}

CMaterial::~CMaterial()
{

}

// Sets the Diffuse color
void CMaterial::SetDiffuse( float r, float g, float b )
{
	m_Material.Diffuse.r = r;
	m_Material.Diffuse.g = g;
	m_Material.Diffuse.b = b;
}

// Sets the ambient color
void CMaterial::SetAmbient( float r, float g, float b )
{
	m_Material.Ambient.r = r;
	m_Material.Ambient.g = g;
	m_Material.Ambient.b = b;
}

// Sets the emissive color
void CMaterial::SetEmissive( float r, float g, float b )
{
	m_Material.Emissive.r = r;
	m_Material.Emissive.g = g;
	m_Material.Emissive.b = b;
}

// Sets the specular color and power
void CMaterial::SetSpecular( float r, float g, float b, float Power )
{
	m_Material.Specular.r = r;
	m_Material.Specular.g = g;
	m_Material.Specular.b = b;
	m_Material.Power = Power;
}

// Set this material as active
HRESULT CMaterial::Update()
{
	return g_pd3dDevice->SetMaterial( &m_Material );
}

// ************************************************
// ---------------- CMesh ------------------------
// This contains the mesh data, currenly loads
// X files.
// ************************************************
class CMesh : public CObject
{
public:
	CMesh();
	~CMesh();
	CMesh( CMesh& OtherMesh );

// Variables:
public:
	int m_NumMats;

protected:
	LPD3DXMESH m_pMesh;	// The mesh


	LPDIRECT3DTEXTURE8* m_pTextures;	// The mesh's textures
	CMaterial* m_pMaterials;			// The mesh's materials

// Functions:
public:
	HRESULT LoadXFile( char* pstrPathName );	//Loads a mesh
	HRESULT Render();							// Renders the mesh

	void SetMaterial( CMaterial* pMaterial );

	int GetSize(){ return sizeof( *this ); }

protected:

};

CMesh::CMesh( CMesh& OtherMesh )
{
	m_NumMats = OtherMesh.m_NumMats;
	m_pMesh = OtherMesh.m_pMesh;

	m_pMesh->AddRef();

	m_pTextures = new LPDIRECT3DTEXTURE8[ m_NumMats ];

	CopyMemory( m_pTextures, OtherMesh.m_pTextures, sizeof( m_pTextures ) );

	for( int i = 0 ; i < m_NumMats ; i++ )
		m_pTextures[i]->AddRef();

	m_pMaterials = new CMaterial[ m_NumMats ];

	CopyMemory( m_pMaterials, OtherMesh.m_pMaterials, sizeof( m_pMaterials ) );

}	


CMesh::CMesh()
{
	m_pMesh = 0;
	m_NumMats = 0;

	m_pTextures = 0;
	m_pMaterials = 0;
}

CMesh::~CMesh()
{
	if( m_pMesh )
		m_pMesh->Release();


	if( m_pTextures )
		delete[] m_pTextures;

	if( m_pMaterials )
		delete[] m_pMaterials;
}

// Sets all the materials for the object to the specified material
void CMesh::SetMaterial( CMaterial* pMaterial )
{
	for( int i = 0 ; i < m_NumMats ; i++ )
	{
		CopyMemory( &m_pMaterials[i].m_Material, &pMaterial->m_Material, sizeof( D3DMATERIAL8 ) );
	}
}



HRESULT CMesh::LoadXFile( char* pstrPathName )
{
	HRESULT r = 0;

	// The buffer to hold the materials
	LPD3DXBUFFER pMaterialBuffer = 0;

	// Load the x file from disk
	r = D3DXLoadMeshFromX( pstrPathName, D3DXMESH_SYSTEMMEM, g_pd3dDevice, 0, 
							&pMaterialBuffer, (DWORD*)&m_NumMats, &m_pMesh );
	if( FAILED( r ) )
	{
		Error( "Failed to load XFile with file name:" );
		Error( pstrPathName );
		return E_FAIL;
	}

	// Create a new texture array
	m_pTextures = new LPDIRECT3DTEXTURE8[ m_NumMats ];
	// Create a new material array
	m_pMaterials = new CMaterial[ m_NumMats ];

	// Get a pointer to the start of the material buffer
	D3DXMATERIAL* pMaterials = (D3DXMATERIAL*)pMaterialBuffer->GetBufferPointer();

	// Loop for each material in the buffer
	for( int i = 0 ; i < m_NumMats ; i++ )
	{
		// Extract the material from the buffer
		m_pMaterials[i].m_Material = pMaterials[i].MatD3D;
		// Brighten the material
		m_pMaterials[i].m_Material.Ambient = m_pMaterials[i].m_Material.Diffuse;
		
		// If a texture is not set for this material
		/*
		if( !pMaterials[i].pTextureFilename )
		{
			// Set the texture to the default texture 
			m_pTextures[i] = g_pDefaultTexture;

			// Iterate to the next loop because there is no texture
			continue;
		}*/
		
		// Create a new texture from the filename supplied
		r = D3DXCreateTextureFromFile( g_pd3dDevice, pMaterials[i].pTextureFilename, &m_pTextures[i] );
		if( FAILED( r ) )
		{
			Error( "Unable to load texture for mesh with filname:" );
			Error( pMaterials[i].pTextureFilename );
			
			// If the texture load failed then set
			// it to the default texture
			m_pTextures[i] = g_pDefaultTexture;
		}

			
	}

	// Release the material buffer
	pMaterialBuffer->Release();

	return S_OK;

}

HRESULT CMesh::Render()
{
	HRESULT r = E_FAIL;
	
	// Loop for each material
	for( int i = 0 ; i < m_NumMats ; i++ )
	{
		// Set this material as active
		m_pMaterials[i].Update();
		// Set this texture as active
		g_pd3dDevice->SetTexture( 0, m_pTextures[i] );
		// Render this subset of the mesh
		r = m_pMesh->DrawSubset(i);

		// Reset the vertex shader
		g_pd3dDevice->SetVertexShader( VERTEX_TYPE );
	}

	// Return the result of the render operation
	return r;
}

// ************************************************
// ---------------- CLight ------------------------
// Light encapsulation.
// ************************************************
static UINT g_LightCounter = 0;

class CLight : public CObject
{
public:
	CLight();
	~CLight();
	CLight( CLight& OtherLight );
// Variables
public:
	D3DLIGHT8 m_Light;	

protected:	

	int m_ID;		// The lights index
	BOOL m_bIsOn;	// Is the light on?

// Functions:
public:
	// Sets the lights color properties
	void SetDiffuse( float r, float g, float b );
	void SetSpecular( float r, float g, float b );
	void SetAmbient( float r, float g, float b );

	// Turns the light on or off
	void Enable( BOOL bEnable );
	// Returns the status of the light
	BOOL IsOn(){ return m_bIsOn; }

	// Updates the status of the light
	HRESULT Render();

	// Returns the size of the light object(in bytes)
	int GetSize(){ return sizeof( *this ); }
protected:


};

CLight::CLight( CLight& OtherLight )
{
	m_bIsOn = OtherLight.m_bIsOn;
	m_ID = OtherLight.m_ID;
	m_Light = OtherLight.m_Light;

	CopyMemory( &m_Light, &OtherLight.m_Light, sizeof( D3DLIGHT8 ) );
}

CLight::CLight()
{
	// Zero out the D3DLIGHT8 structure
	ZeroMemory( &m_Light, sizeof( D3DLIGHT8 ) );

	// Set the initial type to point
	m_Light.Type = D3DLIGHT_POINT;
	
	// Set the initial color white
	m_Light.Diffuse.r = 1.0f;
	m_Light.Diffuse.g = 1.0f;
	m_Light.Diffuse.b = 1.0f;

	// Set the attenuation
	m_Light.Attenuation0 = 1.0f;

	// Set the initial range to 100 units
	m_Light.Range = 100.0f;
	
	// Set the index based on a static counter
	m_ID = g_LightCounter++;

	// Set the light status tracker to off
	m_bIsOn = FALSE;
}

CLight::~CLight()
{

}

// Sets the ambient color of the light
void CLight::SetAmbient( float r, float g, float b )
{
	m_Light.Ambient.r = r;
	m_Light.Ambient.g = g;
	m_Light.Ambient.b = b;
}	

// Sets the diffuse color of the light
void CLight::SetDiffuse( float r, float g, float b )
{
	m_Light.Diffuse.r = r;
	m_Light.Diffuse.g = g;
	m_Light.Diffuse.b = b;
}

// Sets the specular color of the light
void CLight::SetSpecular( float r, float g, float b )
{
	m_Light.Specular.r = r;
	m_Light.Specular.g = g;
	m_Light.Specular.b = b;
}

// Turns the light on or off
void CLight::Enable( BOOL bEnable )
{
	// Update the tracking variable
	m_bIsOn = bEnable;
	// Change the status of the light
	g_pd3dDevice->LightEnable( m_ID, bEnable );
}


// Puts the light at the same location as the parent frame
HRESULT CLight::Render()
{
	
	// The local matrix of the parent frame
	D3DXMATRIX ParentMatrix;
	// The position of this light = Center of frame.
	D3DXVECTOR3 Position = D3DXVECTOR3( 0, 0, 0 );
	
	if( m_pParentFrame )
	{
		// Get the position from the parent frame
		((CFrame*)m_pParentFrame)->GetLocal( ParentMatrix );		

		// Transform the lights position by the matrix
		D3DXVec3TransformCoord( &Position, &Position, &ParentMatrix );		

		// Update the position
		m_Light.Position = Position;

	}
	else
	{
		// Set the light to be at the origin
		m_Light.Position.x = 0;
		m_Light.Position.y = 0;
		m_Light.Position.z = 0;

		Error( "Light being rendered without a parent frame" );
	}
	

	// Update the light with Direct3D
	g_pd3dDevice->SetLight( m_ID, &m_Light );
	
	return S_OK;	
}

void SetAmbientLight( D3DCOLOR AmbientColor )
{
	// Set the ambient light 
	g_pd3dDevice->SetRenderState( D3DRS_AMBIENT, AmbientColor );
}

