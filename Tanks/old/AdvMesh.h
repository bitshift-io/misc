#ifndef _ADVMESH
#define _ADVMESH

// ************************************************
// ---------------- CAdvMesh ------------------------
// Advanced mesh... support bounding shpeheres and 
// collision detection eventually.....
// This contains the mesh data, currenly loads
// X files.
// ************************************************
class CAdvMesh : public CMesh
{
public:
	void OnCollision( CObject3D* pObjCollision ); //what did we collide with?
	void SetOnCollisionFn( void (*OnCollisionFn)( CObject3D* owner, CObject3D* pObjCollision ) );
	void GetVertexData(BYTE** vertexData, long* noVertices); 
	HRESULT CAdvMesh::LoadXFile( char* pstrPathName );
	//for collision detection, it gets the polys to detect with
	virtual OBJECT_TYPE GetType() const {return ADVMESH;}

	void (*OnCollisionFn)( CObject3D* owner, CObject3D* pObjCollision );
	float boundSphereRadius;
	float mass;
	CObject* colNext; //pointer for collision list
	CUSTOMVERTEX* vertexData; //pointer to vertex buffer
	byte* vertData; //this is the proper way!!!
};

HRESULT CAdvMesh::LoadXFile( char* pstrPathName )
{
	HRESULT r = 0;

	//lets call loadXfile for mesh
	this->CMesh::LoadXFile( pstrPathName );

	//now we make a pointer to the vertex buffer


	//lets see if we can get direct access to the polys...
	//for colission detection....
	// NOTE: dont think we are working with index buffers

	//TODO:clone the mesh using an appropriate FVF, then we know the format
	// and can load the data into structs :) done in parent class Mesh

	//NOTE: 1. Get Model, multiply by world transform matrix
	// 2. Then get FVF data, this will be transformed, data.
	// Implmentation:
	// 1. D3DXMATRIX mLocal;
	 

	

	//CUSTOMVERTEX* vertexInfo;  

	r = pMesh->LockVertexBuffer( D3DLOCK_READONLY, (BYTE**)&vertData );
	if( FAILED(r) )
		Error("lock failed");

	vertexData = (CUSTOMVERTEX*)&vertData; //so it dont shit out, when it all works i gotta fix some stuff


	byte* curVert = vertData;
	CUSTOMVERTEX *data;

	//lets make an array of d3dxvector4's
	D3DXVECTOR4 *transModel = (D3DXVECTOR4*)malloc( sizeof(D3DXVECTOR4) * pMesh->GetNumVertices() );
	D3DXVECTOR4 *result = transModel;


	for( int i=0; i < pMesh->GetNumVertices() ; i++)
	{
		curVert += sizeof(CUSTOMVERTEX);
		data = (CUSTOMVERTEX*)curVert;

		D3DXVECTOR4* vertex = new D3DXVECTOR4(data->point.x,data->point.y,data->point.z, 1);
		D3DXVec4Transform( result, vertex, &mLocal );

		result += 1; //where 1 should be sizeof(D3DXVECTOR4), have to cheack it tho

		char strMessage[255] = {0};
		sprintf(strMessage, "v: %f %f %f\n", data->point.x,data->point.y,data->point.z);
	}

	r = pMesh->UnlockVertexBuffer();
	if( FAILED(r) )
		Error("unlock failed");

	return S_OK;
}

void CAdvMesh::OnCollision( CObject3D* pObjCollision ) 
{
	//here we should do a proper collision detection?

	if( OnCollisionFn != NULL )
		OnCollisionFn(this, pObjCollision);
}

void CAdvMesh::SetOnCollisionFn( void (*OnCollisionFn)( CObject3D* owner, CObject3D* pObjCollision ) )
{
	this->OnCollisionFn = OnCollisionFn;
}

void CAdvMesh::GetVertexData(BYTE** vertexData, long* noVertices)
{
	*noVertices = pMesh->GetNumVertices();

	vertexData = (BYTE**)this->vertexData;	
}

#endif