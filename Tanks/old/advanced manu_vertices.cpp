//-----------------------------------------------------------------------------
//  Fabian Mathews
//	Engine
//-----------------------------------------------------------------------------
#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "Dinput8.lib")
#pragma comment(lib, "Dxguid.lib")
#pragma comment(lib, "D3dx8.lib")   

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

//-----------------------------------------------------------------------------
// Nesisary Globals
//-----------------------------------------------------------------------------
HWND g_hWnd;
WNDCLASSEX *g_wc;
HINSTANCE g_hInstance;

//-----------------------------------------------------------------------------
// Custom Nesisary Globals
//-----------------------------------------------------------------------------
bool inMenu = true;

int g_deviceWidth = 640;
int g_deviceHeight = 480;
bool g_fullScreen = false;
bool g_screenSaver = false; //make it so all keys quit

//paths to data stuffs
char* g_meshPath;
char* g_meshTexturePath; 
char* g_texturesPath;
char* g_soundsPath;
char* g_dataPath;
char* g_imageDumpsPath;

//-----------------------------------------------------------------------------
// Custom Include files
//-----------------------------------------------------------------------------
#include "ErrorLogging.h"
#include "Timming.h"
#include "DataManip.h"
#include "Structs.h"
#include "DirectX.h"
#include "DirectInput.h"

#include "TGAFormat.h"
#include "Object.h"
#include "Object3D.h"
#include "Image.h"
#include "GUIImage.h"
#include "Layer.h"
#include "Light.h"
#include "Sprite.h"
#include "Camera.h"
#include "Mesh.h"

#include "List.h"
#include "Font.h"
#include "Frame.h"


#include "INIFormat.h"

LPDIRECT3DSURFACE8 testi;
LPDIRECT3DSURFACE8 testi2;

//-----------------------------------------------------------------------------
// Custom Classes/Structs
//---------------------------------------------------------------------------
const int g_numAsteroids = 100;
int g_rxMin = 180;
int g_rxMax = 50;
int g_ryMin = 185;
int g_ryMax = 50;
int g_rzMin = 0;
int g_rzMax = 30;
int g_speedMin = 1000;
int g_speedMax = 100;
int g_curAsteroid = 0;

typedef struct ASTEROID
{
	int rx;
	int ry;
	int rz;
	int theta;
	int speed;
}ASTEROID;

class CAsteroid : public CMesh
{
public:
	int rx;
	int ry;
	int rz;
	int theta;
	int speed;
};

//-----------------------------------------------------------------------------
// Custom Globals
//-----------------------------------------------------------------------------

CINI g_ini("data/config.ini");
static CTime g_timer;
CFont *g_pFont = NULL;
LPDIRECT3DSURFACE8 g_png = 0;

///// MENU //////////////
LPDIRECT3DSURFACE8 testsurf;
CImage* g_test = new CImage();
CLayer g_menu;
CImage* g_background = new CImage();
CImage* g_mouse = new CImage(); 

CGUIImage* g_run;



/// GAME CODE ///////////
int cameraTimer = 0;
CCamera g_camera;
CFrame g_world;
CMesh* g_newMesh = new CMesh;
CFrame g_skyboxframe;
CFrame g_sunframe;
CFrame g_earthframe;
CFrame g_mercuryframe;
CFrame g_uranusframe;
CFrame g_moonframe;
CFrame g_marsframe;
CFrame g_jupiterframe;
CFrame g_saturnframe;
CFrame g_venusframe;
CFrame g_plutoframe;
CFrame g_ioframe;
CFrame g_europaframe;
CFrame g_ganymedeframe;
CFrame g_callistoframe;
CFrame g_asteroidframe;

ASTEROID g_asteroids[g_numAsteroids];
CMesh* g_asteroid = new CMesh;

CSprite* g_star;
CLight* g_sunlight;

CLayer g_hud;
CImage* g_temp;
CImage* g_proper;

bool g_newScene = true;
float g_timmer = 0;

//-----------------------------------------------------------------------------
// Other Custom Include files
//-----------------------------------------------------------------------------
#include "WinInput.h"

//-----------------------------------------------------------------------------
// Name: callback fn's
//-----------------------------------------------------------------------------
void MouseClick( CImage* owner )
{
	RECT rect = {0,60,80,90};
	owner->SetSourceRect(&rect);
}

void MouseOver( CImage* owner )
{
	RECT rect = {0,30,80,60};
	owner->SetSourceRect(&rect);
}

void MouseOut( CImage* owner )
{
	RECT rect = {0,00,80,30};
	owner->SetSourceRect(&rect);
}

void Camera1( CObject3D* owner)
{
	g_newScene = false;

	float theta = (CTime::GetTime()/D3DX_PI)/1000;
	owner->vPosition.x = (float)(180 * sin( theta ));
	owner->vPosition.z = (float)(150 * cos( theta ));
	owner->vPosition.y = (float)(150 * cos( theta ));
}

void Camera2( CObject3D* owner)
{
	g_newScene = false;

	float theta = (CTime::GetTime()/D3DX_PI)/1000;
	owner->vPosition.x = (float)(180 * sin( theta ));
	owner->vPosition.z = (float)(80 * cos( theta ));
	owner->vPosition.y = (float)(80 * cos( theta ));

	g_camera.lookAt = g_earthframe.vPosition;
}

void Camera3( CObject3D* owner)
{
	g_newScene = false;

	float theta = (CTime::GetTime()/D3DX_PI)/1000;
	owner->vPosition.x = (float)(120 * sin( theta ));
	owner->vPosition.z = (float)(100 * cos( theta ));
	owner->vPosition.y = (float)(120 * cos( theta ));

	g_camera.lookAt = g_jupiterframe.vPosition;
}


void Camera4( CObject3D* owner)
{
	if( g_newScene == true )
	{
		g_timmer = -150;
	}
	g_timmer += (float)0.08;

	g_newScene = false;

	owner->vPosition.x = 250 + g_timmer;
	owner->vPosition.z = 200;
	owner->vPosition.y = 120 + g_timmer/2;

	g_camera.lookAt.x = 0;
	g_camera.lookAt.y = g_timmer;
	g_camera.lookAt.z = g_timmer*2;
}

void Camera5( CObject3D* owner)
{
	owner->vPosition = D3DXVECTOR3(100,150,600);
	g_camera.lookAt = g_sunframe.vPosition;
}

void VenusFrame(CObject3D* owner)
{
	float theta = ((CTime::GetTime()-5000)/D3DX_PI)/1000;
	owner->vPosition.x = 70 * sin( theta );
	owner->vPosition.z = 75 * cos( theta );
	owner->vPosition.y = 0;
}

void SaturnFrame(CObject3D* owner)
{
	float theta = ((CTime::GetTime()-5000)/D3DX_PI)/2500;
	owner->vPosition.x = 250 * sin( theta );
	owner->vPosition.z = 255 * cos( theta );
	owner->vPosition.y = 0;
}

void PlutoFrame(CObject3D* owner)
{
	float theta = ((CTime::GetTime()-5000)/D3DX_PI)/4000;
	owner->vPosition.x = 390 * sin( theta );
	owner->vPosition.z = 395 * cos( theta );
	owner->vPosition.y = 2 * cos( theta );
}

void JupiterFrame(CObject3D* owner)
{
	float theta = ((CTime::GetTime()-5000)/D3DX_PI)/3000;
	owner->vPosition.x = 300 * sin( theta );
	owner->vPosition.z = 305 * cos( theta );
	owner->vPosition.y = 0;
}

void MarsFrame(CObject3D* owner)
{
	float theta = ((CTime::GetTime()+5000)/D3DX_PI)/2000;
	owner->vPosition.x = 150 * sin( theta );
	owner->vPosition.z = 150 * cos( theta );
	owner->vPosition.y = 0;
}

void UranusFrame(CObject3D* owner)
{
	float theta = (CTime::GetTime()/D3DX_PI)/3500;
	owner->vPosition.x = 350 * sin( theta );
	owner->vPosition.z = 355 * cos( theta );
	owner->vPosition.y = 0;
}

void MercuryFrame(CObject3D* owner)
{
	float theta = (CTime::GetTime()/D3DX_PI)/500;
	owner->vPosition.x = 30 * sin( theta );
	owner->vPosition.z = 35 * cos( theta );
	owner->vPosition.y = 0;
}

void SkyBox( CObject3D* owner)
{
	owner->vPosition = D3DXVECTOR3(0.0,-0.0, -0.0f);
	owner->yRotUpdate = -0.0000;
}

void SunFrame( CObject3D* owner)
{
	owner->vPosition = D3DXVECTOR3(0.0,0.0, 0.0f);
	owner->yRotUpdate = 0.006;
}

void EarthFrame(CObject3D* owner)
{
	float theta = (CTime::GetTime()/D3DX_PI)/1500;
	owner->vPosition.x = 110 * sin( theta );
	owner->vPosition.z = 115 * cos( theta );
	owner->vPosition.y = 0;
}

void MoonFrame(CObject3D* owner)
{
	float theta = (CTime::GetTime()/D3DX_PI)/100;
	owner->vPosition.x = 10 * sin( theta );
	owner->vPosition.z = 7 * cos( theta );
	owner->vPosition.y = 0;
}

void IOFrame(CObject3D* owner)
{
	float theta = (CTime::GetTime()/D3DX_PI)/100;
	owner->vPosition.x = 25 * sin( theta );
	owner->vPosition.z = 22 * cos( theta );
	owner->vPosition.y = 0;
}

void EuropaFrame(CObject3D* owner)
{
	float theta = (CTime::GetTime()/D3DX_PI)/110;
	owner->vPosition.x = 30 * sin( theta );
	owner->vPosition.z = 27 * cos( theta );
	owner->vPosition.y = 0;
}

void GanymedeFrame(CObject3D* owner)
{
	float theta = (CTime::GetTime()/D3DX_PI)/120;
	owner->vPosition.x = 35 * sin( theta );
	owner->vPosition.z = 35 * cos( theta );
	owner->vPosition.y = 0;
}

void CallistoFrame(CObject3D* owner)
{
	float theta = (CTime::GetTime()/D3DX_PI)/130;
	owner->vPosition.x = 40 * sin( theta );
	owner->vPosition.z = 40 * cos( theta );
	owner->vPosition.y = 5 * sin( theta );
}

void Asteroids(CObject3D* owner)
{	
	if( g_curAsteroid >= g_numAsteroids)
		g_curAsteroid = 0;
	else
		g_curAsteroid++;

	float theta = g_asteroids[g_curAsteroid].theta + (CTime::GetTime()/D3DX_PI)/
		g_asteroids[g_curAsteroid].speed;

	owner->vPosition.x = g_asteroids[g_curAsteroid].rx * sin( theta );
	owner->vPosition.z = g_asteroids[g_curAsteroid].ry * cos( theta );
	owner->vPosition.y = g_asteroids[g_curAsteroid].rz * sin( theta );
}

//-----------------------------------------------------------------------------
// Name: Init()
//-----------------------------------------------------------------------------
HRESULT Init()
{
	char *retValue = NULL;

	//read important device settings
	g_ini.SetSection("system");
	retValue = g_ini.ReadKey("screenSaver");
	g_screenSaver = StringToNumber(retValue);
	retValue = g_ini.ReadKey("fullScreen");
	g_fullScreen = StringToNumber(retValue);
	retValue = g_ini.ReadKey("deviceWidth");
	g_deviceWidth = StringToNumber(retValue);
	retValue = g_ini.ReadKey("deviceHeight");
	g_deviceHeight = StringToNumber(retValue);


	// read in the paths, if not found, should have defaults?
	g_ini.SetSection("paths");
	g_meshPath = g_ini.ReadKey("meshes");
	g_meshTexturePath = g_ini.ReadKey("meshTextures");
	g_texturesPath = g_ini.ReadKey("textures");
	g_soundsPath = g_ini.ReadKey("sounds");		
	g_imageDumpsPath =  g_ini.ReadKey("imageDumps");

	InitD3D();


	//load font
	g_pFont = new CFont("FontSmall.bmp",10,10);	

	InitInput();

	
	//skybox frame wont move
	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("skybox.X");
	g_skyboxframe.AddObject((CObject3D*)g_newMesh);
	g_world.AddObject((CObject3D*)g_newMesh);	
	
	//the sun rotates
	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("sun.X");
	//increase the ambience of the sun
	for(int i=0; i < g_newMesh->numMaterials; i++ )
	{
		g_newMesh->pMaterials[i].Ambient = D3DXCOLOR(255,255,255,255);
	}
	g_sunframe.SetCallbackFn( SunFrame );
	g_sunframe.AddObject((CObject*)g_newMesh);
	g_world.AddObject((CObject3D*)&g_sunframe);

	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("uranus.X");
	g_newMesh->yRotUpdate = -0.02;
	g_uranusframe.SetCallbackFn( UranusFrame );
	g_uranusframe.AddObject((CObject3D*)g_newMesh);
	g_world.AddObject((CObject3D*)&g_uranusframe);

	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("mercury.X");
	g_newMesh->yRotUpdate = -0.02;
	g_mercuryframe.SetCallbackFn( MercuryFrame );
	g_mercuryframe.AddObject((CObject3D*)g_newMesh);
	g_world.AddObject((CObject3D*)&g_mercuryframe);

	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("mars.X");
	g_newMesh->yRotUpdate = -0.02;
	g_marsframe.SetCallbackFn( MarsFrame );
	g_marsframe.AddObject((CObject3D*)g_newMesh);
	g_world.AddObject((CObject3D*)&g_marsframe);

	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("jupiter.X");
	g_newMesh->yRotUpdate = -0.02;
	g_jupiterframe.SetCallbackFn( JupiterFrame );
	g_jupiterframe.AddObject((CObject3D*)g_newMesh);
	g_world.AddObject((CObject3D*)&g_jupiterframe);

	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("io.X");
	g_ioframe.SetCallbackFn( IOFrame );
	g_ioframe.AddObject((CObject3D*)g_newMesh);
	g_jupiterframe.AddObject((CObject3D*)&g_ioframe);

	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("europa.X");
	g_europaframe.SetCallbackFn( EuropaFrame );
	g_europaframe.AddObject((CObject3D*)g_newMesh);
	g_jupiterframe.AddObject((CObject3D*)&g_europaframe);

	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("ganymede.X");
	g_ganymedeframe.SetCallbackFn( GanymedeFrame );
	g_ganymedeframe.AddObject((CObject3D*)g_newMesh);
	g_jupiterframe.AddObject((CObject3D*)&g_ganymedeframe);

	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("callisto.X");
	g_callistoframe.SetCallbackFn( CallistoFrame );
	g_callistoframe.AddObject((CObject3D*)g_newMesh);
	g_jupiterframe.AddObject((CObject3D*)&g_callistoframe);

	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("venus.X");
	g_newMesh->yRotUpdate = -0.02;
	g_venusframe.SetCallbackFn( VenusFrame );
	g_venusframe.AddObject((CObject3D*)g_newMesh);
	g_world.AddObject((CObject3D*)&g_venusframe);

	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("pluto.X");
	g_newMesh->yRotUpdate = -0.02;
	g_plutoframe.SetCallbackFn( PlutoFrame );
	g_plutoframe.AddObject((CObject3D*)g_newMesh);
	g_world.AddObject((CObject3D*)&g_plutoframe);
	
	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("saturn.X");
	g_newMesh->yRotUpdate = -0.02;
	g_saturnframe.SetCallbackFn( SaturnFrame );
	g_saturnframe.AddObject((CObject3D*)g_newMesh);
	g_world.AddObject((CObject3D*)&g_saturnframe);

	//the earthframe is added to the sun, it rotates
	//in an eliptical path around the SUN
	//but the sun frame rotates, so we add it to the world
	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("earth.X");
	g_newMesh->yRotUpdate = -0.02;
	g_earthframe.SetCallbackFn( EarthFrame );
	g_earthframe.AddObject((CObject3D*)g_newMesh);
	g_world.AddObject((CObject3D*)&g_earthframe);

	//finnaly the moon goes eliptically around the earth
	g_newMesh = new CMesh;
	g_newMesh->LoadXFile("moon.X");
	g_moonframe.SetCallbackFn( MoonFrame );
	g_moonframe.AddObject((CObject3D*)g_newMesh);
	g_earthframe.AddObject((CObject3D*)&g_moonframe);

	//set the camera position and call back
	g_camera.vPosition = D3DXVECTOR3(0,0,-150);
	g_camera.SetCallbackFn( Camera2 );

	g_star = new CSprite("fontSmall.bmp");
	//g_world.AddObject((CObject3D*)g_star);

	//set up the sun light
	g_sunlight = new CLight();
	g_sunlight->SetLightType( D3DLIGHT_POINT );
	g_sunframe.AddObject((CObject3D*)g_sunlight);

	POINT* point = new POINT;
	point->x = 0;
	point->y = 0;
	g_temp = new CImage();
	g_temp->LoadImage("widescreenstriptop.bmp");
	g_temp->StretchImage(g_deviceWidth, 50);
	g_temp->SetDestPoint(point);
	g_hud.AddObject((CObject*)g_temp);

	point = new POINT;
	g_temp = new CImage();
	g_temp->LoadImage("widescreenstripbottom.bmp");
	g_temp->StretchImage(g_deviceWidth, 50);	;
	point->x = 0;
	point->y = g_deviceHeight - 50;
	g_temp->SetDestPoint(point);
	g_hud.AddObject((CObject*)g_temp);   

	point = new POINT;
	g_temp = new CImage();
	g_temp->LoadImage("siphon.bmp");
	g_temp->StretchImage(300, 40);
	point->x = g_deviceWidth - 350;
	point->y = g_deviceHeight - 45;
	g_temp->SetDestPoint(point);
	g_hud.AddObject((CObject*)g_temp);

	//Asteroids are spacial :)
	//scew brons
	g_asteroid->LoadXFile("asteroid.X");
	for(i=0; i< g_numAsteroids ;i++)
	{
		g_asteroids[i].rx = g_rxMin + (rand()%g_rxMax);
		g_asteroids[i].ry = g_ryMin + (rand()%g_ryMax);
		g_asteroids[i].rz = g_rzMin + (rand()%g_rzMax);
		g_asteroids[i].speed = g_speedMin + (rand()%g_speedMax);
		g_asteroids[i].theta = rand()%360;
	}

	CMesh* g_asteroidMaster = new CMesh();
	g_asteroidMaster->LoadXFile("asteroid.X");
	g_asteroidMaster->SetCallbackFn( Asteroids );
	g_asteroidframe.AddObject((CObject3D*)g_asteroid);

	for(i=1; i < g_numAsteroids ;i++)
	{
		g_asteroid = new CMesh();
		memcpy(g_asteroid, g_asteroidMaster, sizeof(CMesh));
		//g_asteroid->SetCallbackFn( Asteroids );
		g_asteroidframe.AddObject((CObject3D*)g_asteroid);
	}

	g_world.AddObject((CObject3D*)&g_asteroidframe);
	
	////////////// NOW FOR THE MENU STUFF //////////////////////////////

	g_background->LoadImage("siphonsplash.bmp");
	g_menu.AddObject((CObject*)g_background);	


	LoadTGA("800x600.tga", &testsurf);
	g_test->SetImage(testsurf);
	g_menu.AddObject((CObject*)g_test);

	g_run = new CGUIImage();
	g_run->LoadImage("btnRun.bmp");
	g_run->MouseClickFn = MouseClick;
	g_run->MouseOutFn = MouseOut;
	g_run->MouseOverFn = MouseOver;
	point = new POINT;
	point->x = 100;
	point->y = 100;
	g_run->SetDestPoint( point );
	RECT* rect = new RECT;
	g_run->mouseZone.top = 0;
	g_run->mouseZone.left = 0;
	g_run->mouseZone.bottom = 30;
	g_run->mouseZone.right = 80;
	//g_run->mouseZone = {0,0,30,80};
	g_menu.AddObject((CObject*)g_run);

	g_mouse->LoadImage("cursor.bmp");
	point = new POINT;
	point->x = 0;
	point->y = 0;
	g_mouse->SetDestPoint(point);
	g_menu.AddObject((CObject*)g_mouse);


    return S_OK;
}


//-----------------------------------------------------------------------------
// Name: InitVB()
// Desc: Creates a vertex buffer and fills it with our vertices. The vertex
//       buffer is basically just a chuck of memory that holds vertices. After
//       creating it, we must Lock()/Unlock() it to fill it. For indices, D3D
//       also uses index buffers. The special thing about vertex and index
//       buffers is that the ycan be created in device memory, allowing some
//       cards to process them in hardware, resulting in a dramatic
//       performance gain.
//-----------------------------------------------------------------------------
HRESULT InitVB()
{
    // Initialize three vertices for rendering a triangle
    CUSTOMVERTEX g_Vertices[] =
    {
        { 150.0f,  50.0f, 0.5f, 1.0f, 0xffff0000, }, // x, y, z, rhw, color
        { 250.0f, 250.0f, 0.5f, 1.0f, 0xff00ff00, },
        {  50.0f, 250.0f, 0.5f, 1.0f, 0xff00ffff, },
    };

    // Create the vertex buffer. Here we are allocating enough memory
    // (from the default pool) to hold all our 3 custom vertices. We also
    // specify the FVF, so the vertex buffer knows what data it contains.
    if( FAILED( g_pd3dDevice->CreateVertexBuffer( 3*sizeof(CUSTOMVERTEX),
                                                  0, D3DFVF_CUSTOMVERTEX,
                                                  D3DPOOL_DEFAULT, &g_pVB ) ) )
    {
        return E_FAIL;
    }

    // Now we fill the vertex buffer. To do this, we need to Lock() the VB to
    // gain access to the vertices. This mechanism is required becuase vertex
    // buffers may be in device memory.
    VOID* pVertices;
    if( FAILED( g_pVB->Lock( 0, sizeof(g_Vertices), (BYTE**)&pVertices, 0 ) ) )
        return E_FAIL;
    memcpy( pVertices, g_Vertices, sizeof(g_Vertices) );
    g_pVB->Unlock();

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: Cleanup()
// Desc: Releases all previously initialized objects
//-----------------------------------------------------------------------------
VOID Cleanup()
{
	ShutdownD3D();
}


//-----------------------------------------------------------------------------
// Name: Menu()
// Desc: Draws the menu
//-----------------------------------------------------------------------------
void Menu()
{
	//update mouse position
	POINT mousePos = {g_mouseData.x, g_mouseData.y};
	g_mouse->SetDestPoint(&mousePos);

	g_pd3dDevice->Clear( 0, NULL, (D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER), 
		D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

	g_menu.Render();

	char *frameCount = NumberToString(g_mouseData.x);
		g_pFont->PrintText( frameCount,200,200,CLOCKS_PER_SEC);

	g_pFont->Render();

	g_pd3dDevice->Present( NULL, NULL, NULL, NULL );
}

//-----------------------------------------------------------------------------
// Name: GameLoop()
// Desc: Draws the scene
//-----------------------------------------------------------------------------
VOID GameLoop()
{
	int frames = g_timer.FrameCount();
	if( frames != 0 )
	{
		char *frameCount = NumberToString(frames);
		g_pFont->PrintText( frameCount,10,10,CLOCKS_PER_SEC);
	}


	//pick random time up to 20 ses to 10 secs
	if( cameraTimer <= 0 )
	{
		cameraTimer = 3000 + rand()%5000; 

		int i = rand()%6;

		g_newScene = true;

		switch(i)
		{
			case 0:
				g_camera.SetCallbackFn( Camera1 );
				break;
			case 1:
				g_camera.SetCallbackFn( Camera2 );
				break;
			case 2:
				g_camera.SetCallbackFn( Camera3 );
				break;
			case 3:
				g_camera.SetCallbackFn( Camera4 );
				break;
			case 4:
				g_camera.SetCallbackFn( Camera5 );
				break;
			default:
				g_camera.SetCallbackFn( Camera5 );
		}	
	}
	cameraTimer--;


	HRESULT r = 0;


    g_pd3dDevice->Clear( 0, NULL, (D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER), 
		D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

    // Begin the scene
    g_pd3dDevice->BeginScene();

    // Draw the triangles in the vertex buffer. This is broken into a few
    // steps. We are passing the vertices down a "stream", so first we need
    // to specify the source of that stream, which is our vertex buffer. Then
    // we need to let D3D know what vertex shader to use. Full, custom vertex
    // shaders are an advanced topic, but in most cases the vertex shader is
    // just the FVF, so that D3D knows what type of vertices we are dealing
    // with. Finally, we call DrawPrimitive() which does the actual rendering
    // of our geometry (in this case, just one triangle).
    //g_pd3dDevice->SetStreamSource( 0, g_pVB, sizeof(CUSTOMVERTEX) );
    //g_pd3dDevice->SetVertexShader( D3DFVF_CUSTOMVERTEX );
    //g_pd3dDevice->DrawPrimitive( D3DPT_TRIANGLELIST, 0, 1 );

	g_camera.Update();

	g_world.Render();

    // End the scene
    g_pd3dDevice->EndScene();
	
	g_hud.Render();
	g_pFont->Render();	



		

    // Present the backbuffer contents to the display
    g_pd3dDevice->Present( NULL, NULL, NULL, NULL );
}




//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: The window's message handler
//-----------------------------------------------------------------------------
LRESULT WINAPI MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
    switch( msg )
    {
        case WM_DESTROY:
            PostQuitMessage( 0 );
            return 0;

		//case WM_ACTIVATE:
		//	AcquireInput();
			//return 0;
    }

    return DefWindowProc( hWnd, msg, wParam, lParam );
}




//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: The application's entry point
//-----------------------------------------------------------------------------
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR, INT )
{
	g_hInstance = hInst;

    // Register the window class
    WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                      GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                      "D3D Tutorial", NULL };
	g_wc = &wc;
    RegisterClassEx( g_wc );

	// Create the application's window
    g_hWnd = CreateWindow( "D3D Tutorial", "Fabs stogy engine",
                              WS_OVERLAPPEDWINDOW, 100, 100, g_deviceWidth, g_deviceHeight,
                              GetDesktopWindow(), NULL, g_wc->hInstance, NULL );

	SetCursor(NULL);

    // Initialize Direct3D
    if( SUCCEEDED( Init() ) )
    {
        // Create the vertex buffer
        if( SUCCEEDED( InitVB() ) )
        {
            // Show the window
            ShowWindow( g_hWnd, SW_SHOWDEFAULT );
            UpdateWindow( g_hWnd );

            // Enter the message loop
            MSG msg;
            ZeroMemory( &msg, sizeof(msg) );
            while( msg.message != WM_QUIT )
            {
				SetCursor(NULL);

                if( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
                {
                    TranslateMessage( &msg );
                    DispatchMessage( &msg );

					CheckDInput();
					CheckInput();
					CheckSaverInput( &msg );
                }
                else
				{
					if( inMenu == true )
						Menu();
					if( inMenu == false )
						GameLoop();
				}
            }
        }
    }

    // Clean up everything and exit the app
    Cleanup();
    UnregisterClass( "D3D Tutorial", g_wc->hInstance );
    return 0;
}
