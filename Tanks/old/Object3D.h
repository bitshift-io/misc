#ifndef _OBJECT3D
#define _OBJECT3D

//class CFrame;

class CObject3D : public CObject
{
public:
	CObject3D();
	CObject3D(long ID);
	virtual void Render();
	virtual void Update();
	void Remove();

	//void SetNext( CObject3D* next){ this->next = next; }
	//void* GetNext(){ return next; }

	//void SetParent( CObject3D* parent){ this->parent = parent; }
	//void* GetParent(){ return parent; }

	//void SetCallbackFn( void (*CallbackFn)(CObject3D* owner));
	void SetCallbackFn( void (*CallbackFn)(CObject3D* owner));
	virtual OBJECT_TYPE GetType() const {return OBJECT3D;}

	void (*CallbackFn)(CObject3D* owner);

	unsigned int GetSize(){ return sizeof( *this ); }

	//CObject3D* next;

	D3DXVECTOR3 vPosition;
	D3DXVECTOR3 vVelocity;

	float xRotUpdate;
	float yRotUpdate;
	float zRotUpdate;

	float xRot;
	float yRot;
	float zRot;	

	D3DXMATRIX mLocal;

protected:	
	long ID;
	//CObject3D* parent;
	//void (*CallbackFn)(CObject3D* owner);
};

CObject3D::CObject3D()
{
	CallbackFn = NULL;

	D3DXMatrixIdentity( &mLocal );

	vPosition = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	vVelocity = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	
	xRotUpdate = 0;
	yRotUpdate = 0;
	zRotUpdate = 0;

	xRot = 0;
	yRot = 0;
	zRot = 0;

	next = NULL;
	parent = NULL;
	ID = 0;
}

CObject3D::CObject3D(long ID)
{
	next = NULL;
	parent = NULL;
	this->ID = ID;
}

void CObject3D::SetCallbackFn( void (*CallbackFn)(CObject3D* owner) )
{
	this->CallbackFn = CallbackFn;
}

void CObject3D::Update()
{
	if( CallbackFn != NULL )
		CallbackFn( (CObject3D*)this );

	// Create some temporary matrices for the
	// rotation and translation transformations
	D3DXMATRIX mRotX, mRotY, mRotZ, mTrans, mRotTemp;

	// Update the position by the velocity
	vPosition.x += vVelocity.x;
	vPosition.y += vVelocity.y;
	vPosition.z += vVelocity.z;

	// Update rotations
	xRot += xRotUpdate;
	yRot += yRotUpdate;
	zRot += zRotUpdate;

	// Set the translation matrix
	D3DXMatrixTranslation( &mTrans, vPosition.x, vPosition.y, vPosition.z );

	D3DXMatrixRotationX( &mRotX, xRot );
	D3DXMatrixRotationY( &mRotY, yRot );
	D3DXMatrixRotationZ( &mRotZ, zRot );

	// Concatenate the y axis and x axis rotation matrices
	D3DXMatrixMultiply( &mRotTemp, &mRotX, &mRotY );
	// Concatenate the xy axes and z axis rotation matrices
	D3DXMatrixMultiply( &mRotTemp, &mRotZ, &mRotTemp );
	// Concatenate the xyz axes and translation matrices
	D3DXMatrixMultiply( &mTrans, &mRotTemp, &mTrans );

	// Update the copy of the local matrix
	//D3DXMATRIX mLocal;
	mLocal = mTrans;	
	
	
	if( GetParent() )
	{
		D3DXMATRIX *mParent;

		mParent = &((CObject3D*)GetParent())->mLocal;		

		D3DXMatrixMultiply( &mLocal, &mLocal, mParent);
	}

	// Set the world matrix
	g_pd3dDevice->SetTransform( D3DTS_WORLD, &mLocal );
}

void CObject3D::Render()
{

}

void CObject3D::Remove()
{
	//clean up code, removeing is killing, if u want 
	//to change parents dont call this! call SetParent
	CallbackFn = NULL;
	next = NULL;
	parent = NULL;
	
	delete this;
}

#endif