#ifndef _GUIIMAGE
#define _GUIIMAGE

//GUI Image.h
//this are buttons text boxes etc..

class CGUIImage : public CImage
{
public:
	CGUIImage();
	void Remove();
	virtual void OnMouseOver();
	virtual void OnMouseOut();
	virtual void OnMouseClick();
	void Update();
	void Render();
	bool HasFocus(){return focus;}
	void SetMouseZone(RECT* zone);
	virtual OBJECT_TYPE GetType() const {return GUIIMAGE;}

	void (*MouseOverFn)(CImage* owner); 
	void (*MouseOutFn)(CImage* owner);
	void (*MouseClickFn)(CImage* owner);

	RECT mouseZone;

private:

protected:
	bool focus;
	bool wasOver;
};

CGUIImage::CGUIImage()
{
	//call this so it sets the proper srcRects etc...
	//MouseOutFn(this);	
}

void CGUIImage::SetMouseZone(RECT* zone)
{
	//if null is passed in, default to srcRect
	if( zone == NULL )
	{		
		mouseZone.bottom = srcRect.bottom;
		mouseZone.top = srcRect.top;
		mouseZone.left = srcRect.left;
		mouseZone.right = srcRect.right;
	}
	else
	{
		mouseZone.bottom = zone->bottom;
		mouseZone.left = zone->left;
		mouseZone.right = zone->right;
		mouseZone.top = zone->top;
	}
}

void CGUIImage::Render()
{
	Update();

	CopySurfaceFast(&image, &srcRect, &g_pBackSurf, &realDestPoint);
}

void CGUIImage::Update()
{
	CImage::Update();

	
	//before we render, we need to update the destpoints
	// as they are inherted from the parent....
	//this makes "this" relative to its parent
	//CImage* pParent = (CImage*)GetParent();
	//CLayer* pParent = (CLayer*)parent; 
	//realDestPoint.x = pParent->destPoint->x + destPoint->x; 
	//realDestPoint.y = pParent->destPoint->y + destPoint->y;

	//cheack to see if the mouseZone
	// and actual mouse position are 
	// over each other
	RECT realMouseZone;
	realMouseZone.left = mouseZone.left + realDestPoint.x;
	realMouseZone.right = mouseZone.right + realDestPoint.x;
	realMouseZone.top = mouseZone.top + realDestPoint.y;
	realMouseZone.bottom = mouseZone.bottom + realDestPoint.y;
	

	//wheres the mouse?
	if( (g_mouseData.x >= realMouseZone.left) &&
		(g_mouseData.x <= realMouseZone.right) &&
		(g_mouseData.y >= realMouseZone.top) &&
		(g_mouseData.y <= realMouseZone.bottom))
	{
		this->wasOver = true;

		OnMouseOver();

		if( g_mouseData.button[0] == -1)
		{
			OnMouseClick();
		}
	}
	//the mouse aint over, was it over?
	else
	{
		if( wasOver == true )
		{
			wasOver = false;
			OnMouseOut();
		}		
	}

	
};

void CGUIImage::OnMouseClick()
{
	if( MouseClickFn != NULL )
		MouseClickFn(this);	
}

void CGUIImage::OnMouseOut()
{
	if( MouseOutFn != NULL )
		MouseOutFn(this);	
}

void CGUIImage::OnMouseOver()
{
	if( MouseOverFn != NULL )
		MouseOverFn(this);	
}

void CGUIImage::Remove()
{
	//clean up code, removeing is killing, if u want 
	//to change parents dont call this! call SetParent
	CallbackFn = NULL;
	next = NULL;
	parent = NULL;

	/*
	delete image;
	image = NULL;*/
	image->Release();
	image = NULL;

	MouseOverFn = NULL; 
	MouseOutFn = NULL;
	MouseClickFn = NULL;

	delete destPoint;
	destPoint = NULL;	

	delete this;
}

#endif