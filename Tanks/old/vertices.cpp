
//


//-----------------------------------------------------------------------------
//  Fabian Mathews
//	Engine
//-----------------------------------------------------------------------------

#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "Dinput8.lib")
#pragma comment(lib, "Dxguid.lib")
#pragma comment(lib, "D3dx8.lib")   

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>
#include <math.h>

#define PI 3.1415

//-----------------------------------------------------------------------------
// Nesisary Globals
//-----------------------------------------------------------------------------





//-----------------------------------------------------------------------------
// Custom Include files
//-----------------------------------------------------------------------------
#include "Vertices.h"
#include "Engine.h"
#include "Projectile.h"
#include "Player.h"

//#include "Player.cpp"
//#include "Projectile.cpp"


HWND g_hWnd;
WNDCLASSEX *g_wc;
HINSTANCE g_hInstance;

//paths to data stuffs
char* g_meshPath;
char* g_meshTexturePath; 
char* g_texturesPath;
char* g_soundsPath;
char* g_dataPath;
char* g_imageDumpsPath;

/*
int g_deviceWidth = 640;
int g_deviceHeight = 480;
bool g_fullScreen = true;
bool g_screenSaver = false; //make it so all keys quit

//-----------------------------------------------------------------------------
// Custom Nesisary Globals
//-----------------------------------------------------------------------------

bool bInMenu = true;
bool bDisplayScore = false;
bool bInGame = false;
bool bEndGame = false;
const int mapBorderSize = 430;*/

//-----------------------------------------------------------------------------
// Custom Classes/Structs
//---------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Custom Globals
//-----------------------------------------------------------------------------

//CINI g_ini("data/config.ini");
static CTime g_timer;
CFont *g_pFont = NULL;


/// GAME CODE ///////////
int cameraTimer = 0;


CFrame g_world;

CLayer menu;
CLayer scoreboard;

CGUIImage play;


CAdvMesh map;
int const MAXPLAYERS = 8;
int noPlayers = 8;
int lastLivePlayer; //index of players array of the last surviving player
CPlayer* players[8]; //up to 8 players including AI


CAdvMesh g_skybox;
CAdvMesh g_plane;
CAdvMesh g_ball6;

CAdvMesh g_ball1;
CAdvMesh g_ball2;
CAdvMesh g_ball3;
CAdvMesh g_ball4;
CAdvMesh g_ball5;
CMesh g_3dsmodel;

CSprite* g_sprite;
CParticle* g_particleEmitter;
CGUITextbox g_textbox;

CLayer g_menu;
CGUIImage g_restart;
CImage* g_logo = new CImage();
CImage* g_mouse = new CImage();


bool g_newScene = true;
float g_timmer = 0;

//-----------------------------------------------------------------------------
// Other Custom Include files
//-----------------------------------------------------------------------------


void PlayOver( CImage* owner )
{
		RECT rect = {0,30,80,60};
		owner->SetSourceRect( &rect );
}

void PlayOut( CImage* owner )
{
		RECT rect = {0,0,80,30};
		owner->SetSourceRect( &rect );
}

void PlayClick( CImage* owner )
{
	bInMenu = false;
	bDisplayScore = false;
	bInGame = true;
}

void Camera1( CObject3D* owner)
{
	static int time = 0;

	//lock on 2 winning player
	if( bEndGame ) 
	{
		static_cast<CCamera*>(owner)->lookAt = players[lastLivePlayer]->vPosition;
		owner->vPosition.y = 100;
	}
	else if( bInMenu )
	{
		static int lookingAt;
		
		time--;

		float theta = (CTime::GetTime()/D3DX_PI)/2000;
		owner->vPosition.x = (float)(400 * cos( theta ));
		owner->vPosition.z = (float)(100 * sin( theta ));
		owner->vPosition.y = 150;

		if( time <= 0 )
		{
			int safetyBreak = 0;
			//do{
				lookingAt = rand()%noPlayers;
				safetyBreak++;
			//}while( !players[lookingAt]->IsDead() && safetyBreak < 40 );
			
            if( safetyBreak < 40 )
			{
				((CCamera*)owner)->lookAt = players[lookingAt]->vPosition;
				//Error("lets watch some 1");
			}
			else
			{
					((CCamera*)owner)->lookAt = g_world.vPosition;
					((CCamera*)owner)->lookAt.x += 50;
					((CCamera*)owner)->lookAt.y += 5;
			}

			time = 1000 + rand()%1000;
		}
	}
	else
	{
		((CCamera*)owner)->lookAt = g_world.vPosition;
		((CCamera*)owner)->lookAt.x += 50;
		((CCamera*)owner)->lookAt.y += 5;
		owner->vPosition = D3DXVECTOR3(650,950,50);

		time = 0;
	}
}




//-----------------------------------------------------------------------------
// Name: Init()
//-----------------------------------------------------------------------------
//HRESULT Init()
void Init(CEngine* pEngine)
{
	char *retValue = NULL;

	//read important device settings
	g_ini.SetSection("system");
	retValue = g_ini.ReadKey("fullScreen");
	g_fullScreen = (bool)StringToNumber(retValue);
	retValue = g_ini.ReadKey("deviceWidth");
	g_deviceWidth = (int)StringToNumber(retValue);
	retValue = g_ini.ReadKey("deviceHeight");
	g_deviceHeight = (int)StringToNumber(retValue);

	// read in the paths, if not found, should have defaults?
	g_ini.SetSection("paths");
	g_meshPath = g_ini.ReadKey("meshes");
	g_meshTexturePath = g_ini.ReadKey("meshTextures");
	g_texturesPath = g_ini.ReadKey("textures");
	g_soundsPath = g_ini.ReadKey("sounds");		
	g_imageDumpsPath =  g_ini.ReadKey("imageDumps");

	//InitD3D();	
	pEngine->Init();
	pEngine->Set3DAPI( 1 );
	g_pd3dDevice->SetRenderState( D3DRS_AMBIENT, 0xffffffff );

	//load font
	g_pFont = new CFont("FontSmall.bmp",10,10);	

	map.LoadXFile("map.x");
	g_world.AddObject((CObject*)&map);

	CLight *light = new CLight(); //light not working :?
	light->vPosition = D3DXVECTOR3(70,110,10);
	light->lookAt = map.vPosition;
	light->SetLightType( D3DLIGHT_POINT );
	g_world.AddObject((CObject*)light);

	for(int i=0; i<noPlayers; i++)
	{
		players[i] = new CPlayer(players, noPlayers);
		players[i]->Init(&g_world);
		g_world.AddObject((CObject*)players[i]);
	}

	//set the camera position and call back
	//g_camera.vPosition = D3DXVECTOR3(200,510,200);
	g_camera.SetCallbackFn( Camera1 );

	//some 3ds ene 1?
	//g_3dsmodel.Load3DSFile( "earth.3ds" );


	//set up our scene
	//g_sprite = new CSprite("star.png");
	//g_sprite->vPosition.x = 10;
	//g_world.AddObject((CObject3D*)g_sprite);

	//g_particleEmitter = new CParticle(1000, 10);
	//g_world.AddObject((CObject3D*)g_particleEmitter);

	//we always want to do collision detection with the level
	// so make a massive bound shphere

	/*
	g_skybox.LoadXFile("cube.X");
	g_skybox.boundSphereRadius = 60000;
	g_world.AddObject((CObject3D*)&g_skybox);*/

	
	//g_plane.LoadXFile("poly.X");
	//g_plane.boundSphereRadius = 10000;
	//g_plane.yRot = -D3DX_PI;
	//g_plane.xRot = D3DX_PI/2;
	//g_plane.vPosition.z = 0;
	//g_plane.vPosition.x = -100;
	//g_plane.vPosition.y = 0;
	//g_world.AddObject((CObject3D*)&g_plane);

	//g_ball6.LoadXFile("earth.X");
	//g_ball6.SetOnCollisionFn( ball1col );
	//g_ball6.vPosition.x = 50;
	//g_ball6.vPosition.y = 0;
	//g_ball6.vVelocity.x = (float)-0.06;
	//g_ball6.vVelocity.y = 0.0;
	//g_ball6.boundSphereRadius = 5.0;
	//g_world.AddObject((CObject3D*)&g_ball6);

/*
	g_ball1.LoadXFile("earth.X");
	g_ball1.SetOnCollisionFn( ball1col );
	g_ball1.vPosition.x = -100;
	g_ball1.vPosition.y = 100;
	g_ball1.vVelocity.x = 0.06;
	g_ball1.vVelocity.y = -0.06;
	g_ball1.boundSphereRadius = 5.0;
	g_world.AddObject((CObject3D*)&g_ball1);

	g_ball3.LoadXFile("venus.X");
	g_ball3.SetOnCollisionFn( ball1col );
	g_ball3.vPosition.x = 50;
	g_ball3.vPosition.y = -80;
	g_ball3.vVelocity.y = 0.1;
	g_ball3.boundSphereRadius = 5.0;
	g_world.AddObject((CObject3D*)&g_ball3);

	g_ball2.LoadXFile("mars.X");
	g_ball2.SetOnCollisionFn( ball1col );
	g_ball2.vPosition.x = 100;
	g_ball2.vVelocity.x = -0.06;
	g_ball2.boundSphereRadius = 5.0;
	g_world.AddObject((CObject3D*)&g_ball2);

	g_ball4.LoadXFile("mars.X");
	g_ball4.SetOnCollisionFn( ball1col );
	g_ball4.vPosition.x = -100;
	g_ball4.vVelocity.x = 0.04;
	g_ball4.boundSphereRadius = 5.0;
	g_world.AddObject((CObject3D*)&g_ball4);

	g_ball5.LoadXFile("earth.X");
	g_ball5.SetOnCollisionFn( ball1col );
	g_ball5.vPosition.x = 120;
	g_ball5.vPosition.y = 30;
	g_ball5.vPosition.z = -100;
	g_ball5.vVelocity.x = -0.04;
	g_ball5.vVelocity.y = -0.01;
	g_ball5.vVelocity.z = 0.04;
	g_ball5.boundSphereRadius = 5.0;
	g_world.AddObject((CObject3D*)&g_ball5);*/




	////////////// NOW FOR THE MENU STUFF //////////////////////////////
	RECT* rect = new RECT;
	POINT* point = new POINT;

	menu.SetVisible(bInMenu);
	scoreboard.SetVisible(bDisplayScore);


	play.LoadImage("btnRun.bmp");
	rect = new RECT;
	rect->top = 0;
	rect->left = 0;
	rect->bottom = 30;
	rect->right = 80;
	play.SetSourceRect( rect );
	play.SetMouseZone(NULL);
	point = new POINT;
	point->x = 0;
	point->y = 5;	
	play.SetDestPoint( point );
	play.MouseClickFn = PlayClick;
	play.MouseOutFn = PlayOut;
	play.MouseOverFn = PlayOver;
	menu.AddObject((CObject*)&play);

	point = new POINT;
	CImage *title = new CImage();
	title->LoadImage("siphon.bmp");
	title->StretchImage(300, 40);
	point->x = 120;
	point->y = 0;
	title->SetDestPoint(point);
	menu.AddObject((CObject*)title);


	//scoreboard
	point = new POINT;
	CImage *title2 = new CImage();
	title2->LoadImage("siphon.bmp");
	title2->StretchImage(300, 40);
	point->x = g_deviceWidth - 310;
	point->y = g_deviceHeight - 50;
	title2->SetDestPoint(point);
	scoreboard.AddObject((CObject*)title2);



	//g_restart.LoadImage( "btnRun.bmp" );
	//rect = new RECT;
	//rect->top = 0;
	//rect->left = 0;
	//rect->bottom = 30;
	//rect->right = 80;
	//g_restart.SetSourceRect( rect );
	//g_restart.SetMouseZone(NULL);
	//point = new POINT;
	//point->x = 0;
	//point->y = 5;	
	//g_restart.SetDestPoint( point );
	//g_restart.MouseClickFn = ResetClick;
	//g_restart.MouseOutFn = ResetOut;
	//g_restart.MouseOverFn = ResetOver;
	//g_menu.AddObject((CObject*)&g_restart);

	//point = new POINT;
	//g_logo = new CImage();
	//g_logo->LoadImage("siphon.bmp");
	//g_logo->StretchImage(300, 40);
	//point->x = 120;
	//point->y = 0;
	//g_logo->SetDestPoint(point);
	//g_menu.AddObject((CObject*)g_logo);

	//can we try text boexes, surte thing
	//point = new POINT;
	//point->x = 10;
	//point->y = 100;
	//g_textbox.SetAbsolute(true);
	//g_textbox.LoadImage("textbox20.bmp");
	//g_textbox.SetDestPoint(point);
	//g_textbox.SetMouseZone(NULL);
	//g_textbox.SetMaxChars(20);
	//g_textbox.SetFont( g_pFont ); //what font do we want to use?
	//g_menu.AddObject((CObject*)&g_textbox);


	g_mouse->LoadImage("cursor.bmp");
	point = new POINT;
	point->x = 0;
	point->y = 0;
	g_mouse->SetDestPoint(point);
	g_mouse->SetAbsolute(true);
	g_mouseData.height = g_mouse->height;
	g_mouseData.width = g_mouse->width;
	menu.AddObject((CObject*)g_mouse);

	point = new POINT;
	point->x = g_deviceWidth - 500;
	point->y = g_deviceHeight - 50;
	menu.SetDestPoint( point );




	// WE MUST SET THE MASTER FRAME
	// FOR COLLISION DETECTION TO WORK!!!!
	g_world.masterFrame = true;

    //return S_OK;
}

//-----------------------------------------------------------------------------
// Name: Cleanup()
// Desc: Releases all previously initialized objects
//-----------------------------------------------------------------------------
void Cleanup(CEngine* pEngine)
{
	g_world.Remove();

	ShutdownD3D();
}

//-----------------------------------------------------------------------------
// Name: DoMenu()
// Desc: Draws the menu
//-----------------------------------------------------------------------------
bool DoMenu(CEngine *pEngine)
{
	menu.SetVisible(true);
	//update mouse position
	POINT mousePos = {g_mouseData.x, g_mouseData.y};
	g_mouse->SetDestPoint(&mousePos);

	menu.Render();

	return true;
}

//-----------------------------------------------------------------------------
// Name: DisplayScore
// Desc: Draws the scoreboard at end of round
//-----------------------------------------------------------------------------
bool DisplayScore(CEngine *pEngine)
{
	int score;

	scoreboard.SetVisible(true);

	for(int i=0; i<noPlayers; i++)
	{
		score = players[i]->GetScore();

		char *charScore = NumberToString((float)score);
		g_pFont->PrintText( players[i]->GetName(),g_deviceWidth - 100,50*i,CLOCKS_PER_SEC);
		g_pFont->PrintText( charScore,g_deviceWidth - 80,(50*i)+20,CLOCKS_PER_SEC);
	}

	scoreboard.Render();

	return true;
}

//-----------------------------------------------------------------------------
// Name: DoGame
// Desc: Does all things here game related :)
//-----------------------------------------------------------------------------
bool DoGame(CEngine *pEngine)
{
	srand(CTime::GetTime()); //randomize averything ;P

	//check for dead players
	int noDeadPlayers = 0;

	for(int i=0; i<noPlayers; i++)
	{
		if( players[i]->IsDead() )
			noDeadPlayers++;
		else
			lastLivePlayer = i;
	}

	if( (noDeadPlayers + 1) >= noPlayers )
	{
		bEndGame = true;
		bDisplayScore = true;
		bInMenu = false;
	}

	//let players do thier keys with direct input
	for(int i=0; i<noPlayers; i++)
	{
		players[i]->CheckKey();
	}

	return true;
}

//-----------------------------------------------------------------------------
// Name: GameLoop()
// Desc: Draws the scene
//-----------------------------------------------------------------------------
bool GameLoop(CEngine *pEngine)
{
	int frames = g_timer.FrameCount();
	if( frames != 0 )
	{
		char *frameCount = NumberToString((float)frames);
		g_pFont->PrintText( frameCount,10,10,CLOCKS_PER_SEC);
	}

    g_pd3dDevice->Clear( 0, NULL, (D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER), 
		D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

    // Begin the scene
    g_pd3dDevice->BeginScene();

	g_camera.Update();
	g_world.Render();

	//game mode has 2 states
	if( bInMenu )
		DoMenu(pEngine);
	else if( bDisplayScore )
		DisplayScore(pEngine);
	else if( bInGame )
		DoGame(pEngine);

	g_pFont->Render();


	    // End the scene
    g_pd3dDevice->EndScene();	
	
	    // End the scene
    //g_pd3dDevice->EndScene();

    // Present the backbuffer contents to the display
    g_pd3dDevice->Present( NULL, NULL, NULL, NULL );

	return true;
}

//-----------------------------------------------------------------------------
// Name: CheckInput()
// Desc: checks all game inputs
//-----------------------------------------------------------------------------
void CheckInput()
{
	static bool bEscPressed = false;
	static bool bEnterPressed = false;

	if( GetAsyncKeyState(VK_ESCAPE))
	{
		if( !bEscPressed )
			bEscPressed = true;

		//if( !bDisplayScore )
			
		if( !bInMenu &&  bDisplayScore)
		{
			bInMenu = true;
			bDisplayScore = false;
		}
		else if( bInMenu )
			PostQuitMessage(0);

		bDisplayScore = true;
		bInGame = false;
		//
		//PostMessage(g_hWnd, WM_QUIT, 0, 0);
	}
	else
		bEscPressed = false;

	if( GetAsyncKeyState(VK_RETURN) )
	{
		if( !bEnterPressed )
			bEnterPressed = true;

		if( bInMenu || bDisplayScore )
		{
			bInMenu = false;
			bDisplayScore = false;
			bInGame = true;

			if( bEndGame )
			{
				bEndGame = false;
				for(int i=0; i<noPlayers; i++)
				{
					players[i]->Reset();
				}
			}
		}
	}
	else
		bEnterPressed = false;
}

//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: The application's entry point
//-----------------------------------------------------------------------------
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR, INT )
{
	CEngine* game = new CEngine( hInst );

	game->MainLoopCallbk = GameLoop;	

	Init( game );

	game->MainLoop();
	
	game->Shutdown();

    Cleanup( game );
    
    return 0;
}
