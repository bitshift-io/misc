#include "Engine.h"
#include "Player.h"
#include "Projectile.h"

OBJECT_TYPE CPlayer::GetType() const {return FRAME;}

	CPlayer::CPlayer(CPlayer* players[], int numPlayers)
	{
		allPlayers = players;
		noPlayers = numPlayers;

		radius = 40;

		CFrame::CFrame();

		bIsAI = false;

		switch(playerNumber) //maximum of 3 humans, the rest have to be AI 
		{
		case 0:
			name = "Player 1";
			controls[FIRE] = DIK_Z;
			controls[RIGHT] = DIK_G;
			controls[LEFT] = DIK_D;
			controls[UP] = DIK_R;
			controls[DOWN] = DIK_F;
			controls[BLEFT] = DIK_A;
			controls[BRIGHT] = DIK_Q;
			break;
		case 1:
			name = "Player 2";
			controls[FIRE] = DIK_SPACE;
			controls[RIGHT] = DIK_RIGHT;
			controls[LEFT] = DIK_LEFT;
			controls[UP] = DIK_UP;
			controls[DOWN] = DIK_DOWN;
			controls[BLEFT] = DIK_N;
			controls[BRIGHT] = DIK_M;
			break;
		case 2:
			name = "Player 3";
			controls[FIRE] = DIK_SPACE;
			controls[RIGHT] = DIK_RIGHT;
			controls[LEFT] = DIK_LEFT;
			controls[UP] = DIK_UP;
			controls[DOWN] = DIK_DOWN;
			controls[BLEFT] = DIK_N;
			controls[BRIGHT] = DIK_M;
			break;
		default:
			name = "AI";
            bIsAI = true;
			controls[FIRE] = 0;
			controls[RIGHT] = 0;
			controls[LEFT] = 0;
			controls[UP] = 0;
			controls[DOWN] = 0;
			controls[BLEFT] = 0;
			controls[BRIGHT] = 0;
		}

		fireTime = 500;
		nextFireTime = 0;
		speed = 1;
		rotation = 1;
		barrelrotation=1;
		score = 0;
		health = 100;
		bIsDead = false;

		barrel.LoadXFile("turret.x");
		barrel.vPosition = D3DXVECTOR3(0,8,0);
		//barrel.vPosition.y = 20;//needs tweaking
		barrel.SetParent(this);
		AddObject((CObject3D*)&barrel);
		

		body.LoadXFile("base.x");
		body.vPosition = D3DXVECTOR3(0,0,0);
		//body.vPosition.y = 10;//needs tweaking
		body.SetParent(this);
		AddObject((CObject3D*)&body);

		projectile.LoadXFile("bullet.x");

		playerNumber++;

        Reset();				
	}

	//respawns/resets all variables except score
	void CPlayer::Reset()
	{
		health = 100;
		bIsDead = false;

		//pick spawn spot
		vPosition.x = -mapBorderSize + rand()%(2*mapBorderSize);
		vPosition.z = -mapBorderSize + rand()%(2*mapBorderSize);
	}

	void CPlayer::SetKey(int definedKey, int button)
	{
		controls[definedKey] = button;
	}

	int CPlayer::GetScore(){return score;}
	char* CPlayer::GetName(){return name;}
	bool CPlayer::IsDead(){return bIsDead;}

	void CPlayer::CheckKey()
	{
		if( !bInGame )
			return;

		if( bIsAI ) //ingame and ai
			DoAI();

		D3DXVECTOR3 newPosition(vPosition);

		if( g_keyboardData.key[controls[LEFT]] > 0 )
		{
			yRot -= radian(rotation);
		}
		else if( g_keyboardData.key[controls[RIGHT]] > 0 )
		{
			yRot += radian(rotation);
		}

		if(g_keyboardData.key[controls[BRIGHT]] > 0)
		{
			barrel.yRot += radian(barrelrotation);
		}
		else if(g_keyboardData.key[controls[BLEFT]] > 0)
		{
			barrel.yRot -= radian(barrelrotation);
		}


		if( g_keyboardData.key[controls[UP]] > 0)
		{
			newPosition.x += (speed * sin( yRot ))/2;
			newPosition.z += (speed * cos( yRot ))/2;
		}
		else if( g_keyboardData.key[controls[DOWN]] > 0)
		{
			newPosition.x -= (speed * sin( yRot ))/2;
			newPosition.z -= (speed * cos( yRot ))/2;
		}

		//check to amke sure we are in the map
		if( newPosition.x > mapBorderSize || newPosition.x < -mapBorderSize )
			return;

		if( newPosition.z > mapBorderSize || newPosition.z < -mapBorderSize )
			return;

		//make sure we havent run into any one else
		for( int i=0; i< noPlayers; i++)
		{
			if( ( allPlayers[i]->vPosition.x <= (newPosition.x + radius) ) &&
				( allPlayers[i]->vPosition.x >= (newPosition.x - radius) ) &&
				( allPlayers[i]->vPosition.z <= (newPosition.z + radius) ) &&
				( allPlayers[i]->vPosition.z >= (newPosition.z - radius) ) &&
				allPlayers[i] != this )
			{
				//Log();
				return;
			}
		}

        vPosition = newPosition;	

		if( g_keyboardData.key[controls[FIRE]] > 0 && nextFireTime <= 0 )
		{
			CProjectile* newProj = new CProjectile(this, &projectile, *allPlayers, noPlayers);
			newProj->vPosition.y = 5;
			newProj->vPosition.x = vPosition.x + (10 * sin( barrel.yRot + yRot ))/2;
			newProj->vPosition.z = vPosition.z + (10 * cos( barrel.yRot + yRot ))/2;
			newProj->vVelocity.x = (speed*1.5 * sin( barrel.yRot + yRot ))/2;
			newProj->vVelocity.z = (speed*1.5 * cos( barrel.yRot + yRot ))/2;

			world->AddObject((CObject*)newProj);

			nextFireTime = fireTime;
		}
	}

	//load the standard player, called from init function
	void CPlayer::Init(CFrame *world)
	{
		this->world = world;
	}

	//do damage
	void CPlayer::TakeDamage(int damage, CPlayer* instigator)
	{
		health -= damage;

		if( health < 0 )
		{
			bIsDead = true;
			instigator->ScoreKill(this);
		}
	}

	void CPlayer::ScoreKill(CPlayer* killed)
	{
		score++;
	}

	void CPlayer::DoAI()
	{
		//D3DXVECTOR3 newPosition(vPosition);
		D3DXVECTOR3 checkObstacle(vPosition);

		static int doNextMove = 0; //time till we hsould do something else
		bool moveDirection[7]; //this contains information about legitimate moves
		static int fowardBackDirection; //this is the way we will move up or back, -1 for stopped
		static int leftRightDirection; //left or right, -1 for no movement
		static int barrelDirection; //left right, -1 for no direction
		bool bFire = false;
		static bool bLocked = false; //if something is in front or behind, we must choose a direction to rotate and stick with it
		bool bDoNextMove = false;
				
			for(int i=0; i < 7; i++)
			{
				moveDirection[i] = true; //true means we can choose any of the following
			}

			/*
			#define LEFT 0
			#define RIGHT 1
			#define UP 2
			#define DOWN 3
			*/

			//determine any obsticals then move
			//check to amke sure we are in the map
			checkObstacle = vPosition;
			checkObstacle.x += (10 * sin( yRot ))/2;
			checkObstacle.y += (10 * cos( yRot ))/2;	

			//if something is in front of us...
			if( (checkObstacle.x > mapBorderSize || checkObstacle.x < -mapBorderSize) ||
				( checkObstacle.z > mapBorderSize || checkObstacle.z < -mapBorderSize ))//|| or player
			{
				moveDirection[2] = false; //cant go fowards
				bDoNextMove = true; //recalculate moves
			}

			for( int i=0; i< noPlayers; i++)
			{
				if( ( allPlayers[i]->vPosition.x <= (checkObstacle.x + radius) ) &&
					( allPlayers[i]->vPosition.x >= (checkObstacle.x - radius) ) &&
					( allPlayers[i]->vPosition.z <= (checkObstacle.z + radius) ) &&
					( allPlayers[i]->vPosition.z >= (checkObstacle.z - radius) ) &&
					allPlayers[i] != this )
				{
					moveDirection[2] = false; //cant go fowards
					bDoNextMove = true; //recalculate moves
				}
			}

			//something behind us
			checkObstacle = vPosition;
			checkObstacle.x -= (10 * sin( yRot ))/2;
			checkObstacle.y -= (10 * cos( yRot ))/2;

			if( (checkObstacle.x > mapBorderSize || checkObstacle.x < -mapBorderSize) ||
				( checkObstacle.z > mapBorderSize || checkObstacle.z < -mapBorderSize ))//|| or player
			{
				moveDirection[3] = false; //cant go fowards
				bDoNextMove = true; //recalculate moves
			}

			for( int i=0; i< noPlayers; i++)
			{
				if( ( allPlayers[i]->vPosition.x <= (checkObstacle.x + radius) ) &&
					( allPlayers[i]->vPosition.x >= (checkObstacle.x - radius) ) &&
					( allPlayers[i]->vPosition.z <= (checkObstacle.z + radius) ) &&
					( allPlayers[i]->vPosition.z >= (checkObstacle.z - radius) ) &&
					allPlayers[i] != this )
				{
					moveDirection[3] = false; //cant go fowards
					bDoNextMove = true; //recalculate moves
				}
			}

			//moveing and timming to help AI act more human and not so 'random'
			if( doNextMove <= 0 || bDoNextMove )//change move
			{
				doNextMove = 1000 + rand()%1000;
				bDoNextMove = true;
			}
			doNextMove--;

			//if we cant go fowards or backwards, go left or right:
			if( !moveDirection[3] && !moveDirection[2])
			{
				if( bLocked )//dont hcnage direction
				{
					leftRightDirection = rand()%2; //number between 0 and 1
				}
				bLocked = true;
			}
			else
			{
				if( bDoNextMove ) //do a new move
				{
					if( bLocked )//have we become unstuck?
					{
						//leftRightDirection = -1; //lets not rotate any more
						moveDirection[0] = false;
						moveDirection[1] = false; //disable these moves
					}
					bLocked = false;

					bool legitMove = false; //can we do this

					do
					{
					int direction = -1 + rand()%3;  

					switch( direction )
					{
					case -1:
						fowardBackDirection = -1;
						legitMove = true;
						break;
					case 0:
						if( moveDirection[2] )//if legit move
						{
							fowardBackDirection = 0;
							legitMove = true;
						}
						break;
					case 1:
						if( moveDirection[3] ) 
						{
							fowardBackDirection = 1;
							legitMove = true;
						}
						break;
					default:
						fowardBackDirection = -1;
						legitMove = true;
						Error("wtf! - is this a legit foward backward move?");
					}
					}while( !legitMove );
				}
			}

			//now pick a direction from those available
			//go left or right
			if( moveDirection[0] && moveDirection[1] )
			{
				if( bDoNextMove ) //do a new move
				{

					bool legitMove = false; //can we do this

					do
					{
					int direction = -1 + rand()%3;  

					switch( direction )
					{
					case -1:
						leftRightDirection = -1;
						legitMove = true;
						break;
					case 0:
						if( moveDirection[0] )//if legit move
						{
							leftRightDirection = 0;
							legitMove = true;
						}
						break;
					case 1:
						if( moveDirection[1] ) 
						{
							leftRightDirection = 1;
							legitMove = true;
						}
						break;
					default:
						leftRightDirection = -1;
						legitMove = true;
						Error("wtf! - is this a legit left right rotation?");
					}
					}while( !legitMove );
				}
			}
			else
			{
				leftRightDirection = -1; //we have become unlocked so lets not rotate for a while
			}

		//determine players locations(closest player/angle to our barrel and rotate barrel

		//if player is in sights, try firing
		bFire = true; //always fire ASAP for the time being, we might get lucky ;P


		//make the movement stuff here
		D3DXVECTOR3 newPosition(vPosition);

		if( leftRightDirection == 0 )
		{
			yRot -= radian(rotation);
		}
		else if( leftRightDirection == 1 )
		{
			yRot += radian(rotation);
		}

		if(g_keyboardData.key[controls[BRIGHT]] > 0)
		{
			barrel.yRot += radian(barrelrotation);
		}
		else if(g_keyboardData.key[controls[BLEFT]] > 0)
		{
			barrel.yRot -= radian(barrelrotation);
		}


		if( fowardBackDirection == 0)//foward
		{
			newPosition.x += (speed * sin( yRot ))/2;
			newPosition.z += (speed * cos( yRot ))/2;
		}
		else if( fowardBackDirection == 1)//backward
		{
			newPosition.x -= (speed * sin( yRot ))/2;
			newPosition.z -= (speed * cos( yRot ))/2;
		}

		//check to amke sure we are in the map
		if( newPosition.x > mapBorderSize || newPosition.x < -mapBorderSize )
			return;

		if( newPosition.z > mapBorderSize || newPosition.z < -mapBorderSize )
			return;

		//make sure we havent run into any one else
		for( int i=0; i< noPlayers; i++)
		{
			if( ( allPlayers[i]->vPosition.x <= (newPosition.x + radius) ) &&
				( allPlayers[i]->vPosition.x >= (newPosition.x - radius) ) &&
				( allPlayers[i]->vPosition.z <= (newPosition.z + radius) ) &&
				( allPlayers[i]->vPosition.z >= (newPosition.z - radius) ) &&
				allPlayers[i] != this )
			{
				return;
			}
		}

        vPosition = newPosition;	

		if( bFire && nextFireTime <= 0 )//should we fire
		{
			CProjectile* newProj = new CProjectile(this, &projectile, *allPlayers, noPlayers);
			newProj->vPosition.y = 5;
			newProj->vPosition.x = vPosition.x + (10 * sin( barrel.yRot + yRot ))/2;
			newProj->vPosition.z = vPosition.z + (10 * cos( barrel.yRot + yRot ))/2;
			newProj->vVelocity.x = (speed*1.5 * sin( barrel.yRot + yRot ))/2;
			newProj->vVelocity.z = (speed*1.5 * cos( barrel.yRot + yRot ))/2;

			world->AddObject((CObject*)newProj);

			nextFireTime = fireTime;
		}
	}

	
	void CPlayer::Update()
	{
		CFrame::Update();
		nextFireTime--;
	}

	//return degrees as radians
	float CPlayer::radian(float degrees)
	{
		return degrees * DEG_RAD;
	}