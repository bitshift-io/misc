#include "Engine.h"
#include "Projectile.h"
#include "Player.h"
#include "Vertices.h"

OBJECT_TYPE CProjectile::GetType() const {return ADVMESH;}

	CProjectile::CProjectile(CPlayer* playerOwner, CAdvMesh* bullet, CPlayer* players, int numPlayers)
	{
		CAdvMesh::CAdvMesh();
		this->playerOwner = playerOwner;

		allPlayers = players;
		noPlayers = numPlayers;

		damage = 35;
		radius = 10;
		//LoadXFile("bullet.x");

		vertexData = bullet->vertexData; //pointer to vertex buffer
		vertData = bullet->vertData; //this is the proper way!!!

		numMaterials = bullet->numMaterials;
		pMesh = bullet->pMesh;
		pMaterials = bullet->pMaterials;
		pTextures = bullet->pTextures;
	}

	void CProjectile::Update()
	{
		static D3DXVECTOR3 velocity(vVelocity);

		//pause
		if( !bInGame )
			vVelocity = D3DXVECTOR3(0,0,0);
		else
			vVelocity = velocity;

		CAdvMesh::Update();

		//NEED TO ADD CODE TO REMOVE PORJECTILES FROM THE WORLD's LINKED LIST


		//check to amke sure we are in the map - need to fix remove code
		if( vPosition.x > mapBorderSize || vPosition.x < -mapBorderSize )
			return;

		if( vPosition.z > mapBorderSize || vPosition.z < -mapBorderSize )
			return;
/*
		//make sure we havent run into any one else
		for( int i=0; i< noPlayers; i++)
		{
			if( ( allPlayers[i]->vPosition.x <= (vPosition.x + radius) ) &&
				( allPlayers[i]->vPosition.x >= (vPosition.x - radius) ) &&
				( allPlayers[i]->vPosition.z <= (vPosition.z + radius) ) &&
				( allPlayers[i]->vPosition.z >= (vPosition.z - radius) ) && )
			{
				allPlayers[i].TakeDamage(damage, playerOwner);
				return;
			}
		}*/

		
	}