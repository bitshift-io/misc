#ifndef _FRAME
#define _FRAME

class CFrame : public CObject3D
{
public:
	CFrame();
	void Render();
	void Remove();
	void CollisionCheck(CObject* pAdvMesh);
	void AddObject( CObject *pNewObject );
	int GetSize(){ return sizeof( *this ); }
	D3DMATRIX* GetLocal(){ return &mLocal;}
	virtual OBJECT_TYPE GetType() const {return FRAME;}
	
	bool masterFrame;

protected:
	//D3DXMATRIX mLocalParent;

private:
	//D3DXVECTOR3 vPosition;
	//D3DXVECTOR3 vVelocity;

	CFrame* parent;
	CObject* pObjList;
};

CFrame::CFrame()
{
	pObjList = NULL;
	masterFrame = false;
}

void CFrame::CollisionCheck(CObject* pAdvMeshList)
{
	bool createdList = false;

	CObject* meshList = pAdvMeshList;

	//if its null, we are the world/base frame
	if( pAdvMeshList == NULL)
	{
		//create a new list then
		CObject* meshList = NULL;
		createdList = true;

	}

	//list thu all objects
	CObject* unknownObj = pObjList;

	while( unknownObj != NULL )
	{
		if( unknownObj->GetType() == ADVMESH)
		{			
			//now add it to the mehsList
			if( meshList == NULL )
			{
				meshList = unknownObj;				
			}
			else
			{
				
				CAdvMesh* listEnd = (CAdvMesh*)meshList;
				
				while( listEnd->colNext != NULL )
				{					
					listEnd = (CAdvMesh*)listEnd->colNext;
				}
				listEnd->colNext = unknownObj;
				
			}
		}
		if( unknownObj->GetType() == FRAME)
		{
			//Error("ehg?");
			//we found a frame, so call it
			//send it mesh list, this will fill
			// up the list with thier meshes
			//CFrame* nextFrame = (CFrame*)unknownObj;
			//nextFrame->CollisionCheck( meshList );
		}
		
		unknownObj = unknownObj->next;
	}

	
	//if we are the function that created the list, its our job to call
	//the "cheack all the collsisions" function in CollisionDetection.h
	if( createdList )
	{
		CheckAllCollisions( meshList );
	}

	//now for the all important killing of the list
	CAdvMesh* listPrev = (CAdvMesh*)meshList;
	CAdvMesh* listEnd = (CAdvMesh*)meshList;

	while( listEnd->colNext != NULL )
	{					
		listPrev = listEnd;
		listEnd = (CAdvMesh*)listEnd->colNext;
		listPrev->colNext = NULL;
	}
	//listEnd->colNext = unknownObj;


}

void CFrame::AddObject( CObject *pNewObject )
{
	pNewObject->SetParent( this );

	CObject* newNode; 
	newNode = pNewObject;

	if( pObjList == NULL )
	{
		pObjList = newNode;
	}
	else
	{
		CObject* curNode = pObjList;

		while( curNode->next != NULL )
		{
			curNode = curNode->next;
		}
		curNode->next = newNode;
	}	
}

void CFrame::Render()
{
	Update();

	//if( masterFrame )
	//	CollisionCheck(NULL);

	CObject* tempObj = pObjList;

	while( tempObj != NULL )
	{
		tempObj->Render();
		tempObj = tempObj->next;
	}
}

void CFrame::Remove()
{
	//clean up code, removeing is killing, if u want 
	//to change parents dont call this! call SetParent

	//for layers and frames, we have to "kill" off the children first :)
	CObject* curNode = pObjList;
	CObject* prevNode = pObjList;

	while( curNode != NULL )
	{
		prevNode = curNode;
		curNode = curNode->next;

		prevNode->Remove();
	}

	//then clean up our own stuff
	CallbackFn = NULL;
	next = NULL;
	parent = NULL;
	
	delete pObjList;
	pObjList = NULL;

	delete this;
}

#endif