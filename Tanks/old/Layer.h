#ifndef _LAYER
#define _LAYER

class CLayer : public CImage
{
public:
	CLayer();
	void Render();
	virtual void Remove();
	void AddObject( CObject *pNewObject );
	void RemoveObject( CObject *pObjToRemove );
	int GetSize(){ return sizeof( *this ); }
	void SetVisible( bool visibility );
	bool IsVisible(){ return visible; };
	virtual OBJECT_TYPE GetType() const {return LAYER;}

private:
	bool visible;
	CLayer* parent;
	CObject* pObjList;
};

CLayer::CLayer()
{
	visible = true;
}

void CLayer::SetVisible(bool visibility)
{
	visible = visibility;
}

void CLayer::RemoveObject( CObject *pObjToRemove )
{
	//go through the list and compare
	//the address passed in with
	CObject* curNode = pObjList;
	CObject* prevNode = pObjList;

	//head node....
	if( pObjToRemove == curNode )
	{
		pObjList = curNode->next;
	}
	else
	{
		while( curNode != NULL )
		{
			if( pObjToRemove == curNode )
			{
				prevNode->next = curNode->next;
				break;
			}

			prevNode = curNode;
			curNode = curNode->next;
		}
	}
	curNode->Remove();
}

void CLayer::AddObject( CObject *pNewObject )
{
	pNewObject->SetParent( this );

	CObject* newNode; 
	newNode = pNewObject;

	if( pObjList == NULL )
	{
		pObjList = newNode;
	}
	else
	{
		CObject* curNode = pObjList;

		while( curNode->next != NULL )
		{
			curNode = curNode->next;
		}
		curNode->next = newNode;
	}	
}

void CLayer::Render()
{
	//why update when its not visible?
	//if its not visible, dont do a thing!
	if( !visible )
		return;

	//Update();

	CObject* tempObj = pObjList;

	while( tempObj != NULL )
	{
		tempObj->Render();
		tempObj = tempObj->next;
	}
}

void CLayer::Remove()
{
	//clean up code, removeing is killing, if u want 
	//to change parents dont call this! call SetParent

	//for layers and frames, we have to "kill" off the children first :)
	CObject* curNode = pObjList;
	CObject* prevNode = pObjList;

	while( curNode != NULL )
	{
		prevNode = curNode;
		curNode = curNode->next;

		prevNode->Remove();
	}

	//then clean up our own stuff
	CallbackFn = NULL;
	next = NULL;
	parent = NULL;
	
	delete pObjList;
	pObjList = NULL;

	delete this;
}

#endif