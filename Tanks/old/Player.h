#ifndef _PLAYER
#define _PLAYER


#define DEG_RAD     0.01745329251994329576f

#define LEFT 0
#define RIGHT 1
#define UP 2
#define DOWN 3
#define BLEFT 4
#define BRIGHT 5
#define FIRE 6

static int playerNumber = 0;

//this class is resonsible for updateing the mesh also
class CPlayer : public CFrame //public CAdvMesh
{
public:
  CAdvMesh barrel; //head/barrel of tank
  int barrelrotation;

  CAdvMesh body; //tank body
  CAdvMesh projectile; //points to a projectile class to copy when we fire as not to load an x file all the time

  char* name;
  int speed;
  int rotation;

  int score;
  int health;
  int bIsDead;

  int nextFireTime;

  int fireTime;

  CFrame *world;
  CPlayer **allPlayers; //pointer to players for projectiles
  int noPlayers; //for projectile

  int radius;

  int bIsAI;

  int controls[7]; //players controls: up down left right berrel left, berrel right, fire

public:
	virtual OBJECT_TYPE GetType() const;

	CPlayer(CPlayer* players[], int numPlayers);

	int GetScore();
	char* GetName();
	bool IsDead();
	void Reset();
	void SetKey(int definedKey, int button);
	void Init(CFrame *world);
	void TakeDamage(int damage, CPlayer* instigator);
	void ScoreKill(CPlayer* killed);
	void Update();
	float radian(float degrees);
	void CheckKey();
	void DoAI();

};

#endif



