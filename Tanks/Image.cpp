#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>


#include "Image.h"
#include "Structs.h"
#include "ErrorLogging.h"
#include "directx.h"


CImage::CImage()
{
	srcRect.bottom = 0;
	srcRect.left = 0;
	srcRect.right = 0;
	srcRect.top = 0;

	destPoint = new POINT;
	destPoint->x = 0;
	destPoint->y = 0;

	realDestPoint.x = 0;
	realDestPoint.y = 0;

	absolute = false;
}

void CImage::StretchImage(long width, long height)
{
	HRESULT r = 0;
	LPDIRECT3DSURFACE8 temp = image;

	image = NULL;
	r = g_pd3dDevice->CreateImageSurface( width, height,
			SURFACEFORMAT, &image);

	if( FAILED(r) )
	{
		Error("Unable to re-create surface");
			//return FAIL;
	}

	CopySurface(&temp, &srcRect, &image, NULL);

	srcRect.bottom = height;
	srcRect.right = width;
}

void CImage::SetImage(void* image)
{
	HRESULT r = 0;

	LPDIRECT3DSURFACE8 temp = (LPDIRECT3DSURFACE8)image;
	D3DSURFACE_DESC imageDesc;
	temp->GetDesc(&imageDesc);

	r = g_pd3dDevice->CreateImageSurface( imageDesc.Width, imageDesc.Height,
		SURFACEFORMAT, &this->image);
	if( FAILED(r) )
	{
		Error("Unable to re-create/create surface");
	}

	this->image = (LPDIRECT3DSURFACE8)image;

	srcRect.bottom = imageDesc.Height;
	srcRect.right = imageDesc.Width;
}

void CImage::LoadImage(char* path)
{
	BMPDATA tempImage;

	LoadBmp(path, &tempImage, &image);
	width = tempImage.width;
	height = tempImage.height;

	srcRect.bottom = height;
	srcRect.left = 0;
	srcRect.right = width;
	srcRect.top = 0;
}

void CImage::Render()
{
	Update();

	CopySurfaceFast(&image, &srcRect, &g_pBackSurf, &realDestPoint);
}

void CImage::SetDestPoint(POINT* point)
{
	destPoint->x = point->x;
	destPoint->y = point->y;
}

void CImage::SetSourceRect(RECT* rect)
{
	srcRect.bottom = rect->bottom;
	srcRect.left = rect->left;
	srcRect.right = rect->right;
	srcRect.top = rect->top;
}

void CImage::Remove()
{
	//clean up code, removeing is killing, if u want
	//to change parents dont call this! call SetParent
	CallbackFn = NULL;
	next = NULL;
	parent = NULL;

	/*
	delete image;
	image = NULL;*/
	image->Release();
	image = NULL;

	delete destPoint;
	destPoint = NULL;


	delete this;
}

void CImage::Update()
{
	//before we render, we need to update the destpoints
	// as they are inherted from the parent....
	//this makes "this" relative to its parent
	//CImage* pParent = (CImage*)GetParent();
	//do a quick test to see if its relative or absolute
	if( !absolute )
	{
		CImage* pParent = (CImage*)parent;
		realDestPoint.x = pParent->destPoint->x + destPoint->x;
		realDestPoint.y = pParent->destPoint->y + destPoint->y;
	}
	else
	{
		realDestPoint.x = destPoint->x;
		realDestPoint.y = destPoint->y;
	}
}


