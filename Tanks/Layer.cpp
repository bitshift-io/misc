#include "Layer.h"


CLayer::CLayer()
{
	visible = true;
}

void CLayer::SetVisible(bool visibility)
{
	visible = visibility;
}

void CLayer::RemoveObject( CObject *pObjToRemove )
{
	//go through the list and compare
	//the address passed in with
	CObject* curNode = pObjList;
	CObject* prevNode = pObjList;

	//head node....
	if( pObjToRemove == curNode )
	{
		pObjList = curNode->next;
	}
	else
	{
		while( curNode != 0 )
		{
			if( pObjToRemove == curNode )
			{
				prevNode->next = curNode->next;
				break;
			}

			prevNode = curNode;
			curNode = curNode->next;
		}
	}
	curNode->Remove();
}

void CLayer::AddObject( CObject *pNewObject )
{
	pNewObject->SetParent( this );

	CObject* newNode;
	newNode = pNewObject;

	if( pObjList == 0 )
	{
		pObjList = newNode;
	}
	else
	{
		CObject* curNode = pObjList;

		while( curNode->next != 0 )
		{
			curNode = curNode->next;
		}
		curNode->next = newNode;
	}
}

void CLayer::Render()
{
	//why update when its not visible?
	//if its not visible, dont do a thing!
	if( !visible )
		return;

	//Update();

	CObject* tempObj = pObjList;

	while( tempObj != 0 )
	{
		tempObj->Render();
		tempObj = tempObj->next;
	}
}

void CLayer::Remove()
{
	//clean up code, removeing is killing, if u want
	//to change parents dont call this! call SetParent

	//for layers and frames, we have to "kill" off the children first :)
	CObject* curNode = pObjList;
	CObject* prevNode = pObjList;

	while( curNode != 0 )
	{
		prevNode = curNode;
		curNode = curNode->next;

		prevNode->Remove();
	}

	//then clean up our own stuff
	CallbackFn = 0;
	next = 0;
	parent = 0;

	delete pObjList;
	pObjList = 0;

	delete this;
}

