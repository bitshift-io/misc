#include "Object3D.h"

#ifndef _CAMERA
#define _CAMERA

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

class CCamera : public CObject3D
{
public:
	CCamera();
	void Update();
	void Render();
	void Remove();
	virtual OBJECT_TYPE GetType() const {return CAMERA;}

	D3DXVECTOR3 lookAt;
	D3DXVECTOR3 up;

private:

};

#endif