#include "Object.h"


OBJECT_TYPE CObject::GetType() const
{
	return OBJECT;
}

void CObject::SetCallbackFn( void (*CallbackFn)(CObject* owner) )
{
	this->CallbackFn = CallbackFn;
}

CObject::CObject()
{
	CallbackFn = 0;
	next = 0;
	parent = 0;
	ID = 0;
	bDestroy = false;
}

void CObject::Render()
{

}

void CObject::Remove()
{
	//clean up code, removeing is killing, if u want
	//to change parents dont call this! call SetParent
	CallbackFn = 0;
	next = 0;
	parent = 0;
	delete this;
}


