#ifndef _LIGHT
#define _LIGHT

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include "Object3D.h"

static int lightCount = 0;

class CLight : public CObject3D
{
public:
	CLight();
	void Update();
	void Render();
	void SetLightType( D3DLIGHTTYPE type );
	virtual OBJECT_TYPE GetType() const {return LIGHT;}

	D3DXVECTOR3 lookAt;
	D3DLIGHT8 light;
	bool enable;
	int lightNo;


private:

};


#endif