#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "Dinput8.lib")
#pragma comment(lib, "Dxguid.lib")
#pragma comment(lib, "D3dx8.lib") 

#include <windows.h>
#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include "DirectX.h"
#include "Mesh.h"
#include "DataManip.h"
#include "ErrorLogging.h"

/*
#include "Object.h"


OBJECT_TYPE CMesh::GetType() const
{
	return MESH;
}
*/

bool CMesh::Load3DSFile( char* pstrPathName )
{
	
	pstrPathName = (char*)StringCat(g_meshPath, pstrPathName);

	float* vertexList = NULL;
	unsigned int* faceList = NULL;

	//Load3DS( pstrPathName, vertexList, faceList );

	return true;
}

HRESULT CMesh::LoadXFile( char* pstrPathName )
{
	HRESULT r = 0;


	//add the meshPath to the model file
	//Error( StringCat("models/", pstrPathName) );
	pstrPathName = (char*)StringCat(g_meshPath, pstrPathName);

	// The buffer to hold the materials
	LPD3DXBUFFER pMaterialBuffer = 0;

	// Temporaryly hold mesh data
	LPD3DXMESH pTempMesh;

    // Load the mesh from the specified file
    if( FAILED( D3DXLoadMeshFromX( pstrPathName, D3DXMESH_SYSTEMMEM,
                                   g_pd3dDevice, 0,
                                   &pMaterialBuffer, &numMaterials,
                                   &pTempMesh ) ) )
    {
		Error("Cant Load Mesh");
        return E_FAIL;
    }

	//we do this if we eva need to get a list of polys :)
	//now we should clone out mesh using a FVF, into pMesh
	// D3DFVF_CUSTOMVERTEX
	r = pTempMesh->CloneMeshFVF( 0, D3DFVF_CUSTOMVERTEX, g_pd3dDevice, &pMesh );
	if( FAILED(r) )
	{
		Error("cant clone mesh to proper FVF");
	}

    // We need to extract the material properties and texture names from the
    // pMaterialBuffer
    D3DXMATERIAL* d3dxMaterials = (D3DXMATERIAL*)pMaterialBuffer->GetBufferPointer();
    pMaterials = new D3DMATERIAL8[numMaterials];
    pTextures  = new LPDIRECT3DTEXTURE8[numMaterials];

    for( DWORD i=0; i<numMaterials; i++ )
    {
        // Copy the material
        pMaterials[i] = d3dxMaterials[i].MatD3D;

		pMaterials[i].Ambient.a = 0;
		pMaterials[i].Ambient.r = 1;
		pMaterials[i].Ambient.g = 1;
		pMaterials[i].Ambient.b = 1;

		pMaterials[i].Specular.a = 0; 
		pMaterials[i].Specular.r = 1;
		pMaterials[i].Specular.g = 1;
		pMaterials[i].Specular.b = 1;
		pMaterials[i].Power = 1;


        // Set the ambient color for the material (D3DX does not do this)
        pMaterials[i].Ambient = pMaterials[i].Diffuse;

		//now append the model texture path
		d3dxMaterials[i].pTextureFilename = (LPSTR)StringCat(g_meshTexturePath,
										d3dxMaterials[i].pTextureFilename);

        // Create the texture
        if( FAILED( D3DXCreateTextureFromFile( g_pd3dDevice,
                                               d3dxMaterials[i].pTextureFilename,
                                               &pTextures[i] ) ) )
        {
			Error("cant load texture");
            pTextures[i] = NULL;
        }
    }

    // Done with the material buffer
    pMaterialBuffer->Release();

	return S_OK;
}

void CMesh::Render()
{
	Update();

	// Loop for each material
	for( int i = 0 ; i < (int)numMaterials ; i++ )
	{


		g_pd3dDevice->SetMaterial( &pMaterials[i] );
        g_pd3dDevice->SetTexture( 0, pTextures[i] );

		// Render this subset of the mesh
		pMesh->DrawSubset(i);

		// Reset the vertex shader
		g_pd3dDevice->SetVertexShader( D3DFVF_CUSTOMVERTEX );
	}
}

void CMesh::Remove()
{
	//clean up code, removeing is killing, if u want
	//to change parents dont call this! call SetParent
	CallbackFn = 0;
	next = 0;
	parent = 0;

	pMesh->Release();
	delete pMaterials;
	pMaterials = 0;
	delete pTextures;
	pTextures = 0;

	delete this;
}




