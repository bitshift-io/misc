#include <windows.h>

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include "Structs.h"
#include "Font.h"
#include "ErrorLogging.h"
#include "directx.h"
#include "Windows.h"

//-----------------------------------------------------------------------------
// CFONT
//-----------------------------------------------------------------------------


CFont::CFont(char *path, int lettersPerRow, int rows)
{
	fontImg = 0;

	this->lettersPerRow = lettersPerRow;
	this->rows = rows;

	BMPDATA bmpData;
	LoadBmp(path, &bmpData, &fontImg);

	width = bmpData.width;
	height = bmpData.height;

	letterWidth = width / lettersPerRow;
	letterHeight = height / rows;
}

/*
void* CFont::GetLastTextAsImage()
{
	return (void*)lastFnt->textSurf;
}*/


LPDIRECT3DSURFACE8 CFont::GetLastTextAsImage()
{
	return lastFnt->textSurf;
}

FNT *CFont::GetLastAsFont()
{
	return lastFnt;
}

FNT CFont::PrintText(char *text, int xPos, int yPos, int time)
{
	HRESULT r = 0;
	FNT pFont;
	pFont.timeToLive = time;
	pFont.timeAdded = (long)CTime::GetTime();
	int i = 0;

	//count the size of the string
	// and make the right size surface
	while( text[i] != '\0' )
		i++;

	r = g_pd3dDevice->CreateImageSurface((letterWidth)*i,(letterHeight)
		, SURFACEFORMAT, &pFont.textSurf);
	if(FAILED(r))
	{
		Error("cant create surface");
	}

	//now calculate the destination point
	pFont.dest->y = yPos;
	pFont.dest->x = xPos;

	//and the source rect
	pFont.srcRect->top = 0;
	pFont.srcRect->left = 0;
	pFont.srcRect->bottom = letterHeight;
	pFont.srcRect->right = letterWidth*i;

	i=0;
	while( text[i] != '\0')
	{
		char tempChar = text[i];
		tempChar -= 32;

		//rounding could cause prolems
		//reminder determines the x position of the letter
		int remainder = tempChar % lettersPerRow;

		//the result (ie. tempChar/g_FontLettersPerRow with out the decimal)
		// determines the y position
		int result = (tempChar / lettersPerRow) - (remainder/10);

		//the top left coords
		int x = remainder * letterWidth;
		int y = result * letterHeight;

		//bottom right coords
		int xOffset = x + letterWidth;
		int yOffset = y + letterHeight;

		//now copy the letter to the fnt struct surface
		// at the right position of course ;)
		// 1. create the src and dest RECT's
		RECT *srcRect = new RECT;
		srcRect->top = y;
		srcRect->left = x;
		srcRect->bottom = yOffset;
		srcRect->right = xOffset;

		//remmebr scaling
		RECT *destRect = new RECT;
		destRect->top = 0;
		destRect->left = i*letterWidth;
		destRect->bottom = letterHeight;
		destRect->right = (i+1)*letterWidth;

		POINT *destPoint = new POINT;
		destPoint->x = i*letterWidth;
		destPoint->y = 0;

		HRESULT r = 0;
		r = g_pd3dDevice->CopyRects(fontImg,srcRect, 1, pFont.textSurf ,destPoint);
		if(FAILED(r))
		{
			MessageBox(g_hWnd,"print text error","error",MB_OK);
		}

		i++;
	}

	//now add the FNT struct to the
	// list
	ListAdd(&fntList, &pFont, sizeof(pFont));

	//set the last text to the currently added text, this is so if we want to
	//strecth the texture, or if we want a pointer to he surface for more
	//poewrful stuff
	lastFnt = &pFont;

	return pFont;

}

void CFont::StretchImage(long width, long height)
{

	HRESULT r = 0;
	LPDIRECT3DSURFACE8 text = lastFnt->textSurf;
	LPDIRECT3DSURFACE8 temp = 0;

	//lastFnt->textSurf = NULL;

	r = g_pd3dDevice->CreateImageSurface( width, height,
			SURFACEFORMAT, &temp);

	if( FAILED(r) )
	{
		Error("Unable to re-create surface");
			//return FAIL;
	}

	CopySurface(&lastFnt->textSurf, NULL, &temp, NULL);
	//CopySurface(&temp, NULL, &lastFnt->textSurf, NULL);

	//srcRect.bottom = height;
	//srcRect.right = width;
}

void CFont::Render()
{
	Update();

	//go through the list and render each surface
	for(int i=0; i < fntList.size; i++)
	{
		FNT *retNode = (FNT*)ListRetrive(&fntList, i);

		HRESULT r = 0;
		r = CopySurfaceFast(&retNode->textSurf, retNode->srcRect,
				 &g_pBackSurf, retNode->dest);
		if( FAILED( r ))
		{
			Error("Font Print Text");
		}
	}
}

//go through and check times and remove out of date stuff :)
void CFont::Update()
{
	//go through the list get each node
	for(int i=0; i < fntList.size; i++)
	{
		FNT *retNode = (FNT*)ListRetrive(&fntList, i);

		//check time, make sure its not 0 - meanning infinite
		if( retNode->timeToLive != 0)
			if( (CTime::GetTime() - retNode->timeAdded) >= retNode->timeToLive )
			{
				ListDelete(&fntList, i);
			}
	}
}

void CFont::Remove()
{
	//clean up code, removeing is killing, if u want
	//to change parents dont call this! call SetParent

	//should i loop through the list to deltete them all?
	//delete fntList;
	delete lastFnt;
	lastFnt = NULL;
	fontImg->Release();
	fontImg = NULL;

	delete this;
}


