#ifndef _FRAME
#define _FRAME

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include "Object3D.h"

class CFrame : public CObject3D
{
public:
	CFrame();
	void Render();
	void Update();
	void Remove();
	void RemoveDestroy();
	void CollisionCheck(CObject* pAdvMesh);
	void AddObject( CObject *pNewObject );
	void RemoveObject( CObject *pNewObject );
	int GetSize(){ return sizeof( *this ); }
	D3DMATRIX* GetLocal(){ return &mLocal;}
	virtual OBJECT_TYPE GetType() const {return FRAME;}

	bool masterFrame;

protected:
	//D3DXMATRIX mLocalParent;

private:
	//D3DXVECTOR3 vPosition;
	//D3DXVECTOR3 vVelocity;

	CFrame* parent;
	CObject* pObjList;
};

#endif