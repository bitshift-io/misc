#ifndef _Sound
#define _Sound

#include "DirectSound.h"

//PLAYS WAVS ONLY
class CSound
{
public:
	CSound(char* fileName);
	void Play();
	void PlayLooping();
	void Stop();
	bool IsPlaying();

private:
	LPDIRECTSOUNDBUFFER8 soundBuffer;
};

#endif