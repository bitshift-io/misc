#ifndef _OBJECT
#define _OBJECT

//for sum run time information
enum OBJECT_TYPE{OBJECT,IMAGE,GUIIMAGE,LAYER,OBJECT3D,FRAME,MESH,LIGHT,CAMERA,ADVMESH,OTHER}; 

//this will eventually be the core object for everything!
class CObject
{
public:
	CObject();
	virtual void Render();
	void Update();
	virtual void Remove();
	virtual OBJECT_TYPE GetType() const = 0;

	void SetNext( CObject* next){ this->next = next; }
	void* GetNext(){ return next; }
	void SetParent( CObject* parent){ this->parent = parent; }
	void* GetParent(){ return parent; }
	unsigned int GetSize(){ return sizeof( *this ); }
	void SetCallbackFn( void (*CallbackFn)(CObject* owner));


	void (*CallbackFn)(CObject* owner);
	CObject* next;
	long ID;
	char* name;
	CObject* parent;
	bool bDestroy; //do we delete this object?

private:

};


#endif
