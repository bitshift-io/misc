#ifndef _DIRECTSOUND
#define _DIRECTSOUND

#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "strmiids.lib")

#include <dmusici.h>
#include <dsound.h>
#include "Windows.h"
#include <stdio.h>


typedef struct WAVEHEADER
{
    char RiffSignature[4];
    long WaveChunkSize;
    char WaveSignature[4];
    char FormatSignature[4];
    long FormatChunkSize;
    short FormatTag;
    short Channels;
    long SampleRate;
    long BytesPerSec;
    short BlockAlign;
    short BitsPerSample;
    char DataSignature[4];
    long DataSize;
} WAVEHEADER;


extern LPDIRECTSOUND8 g_pSound;
extern LPDIRECTSOUNDBUFFER g_pSoundBuffer;

bool InitialiseDirectAudio();
LPDIRECTSOUNDBUFFER8 CreateFileSoundBuffer(FILE* pFile,WAVEHEADER *hdr , LPDIRECTSOUND8 dSound);
bool LoadSoundIntoBuffer(LPDIRECTSOUNDBUFFER8 Buffer,long Position,FILE* pFile,long Size);
LPDIRECTSOUNDBUFFER8  LoadWAV(char* Filename, LPDIRECTSOUND8 DSound);
 
 

#endif