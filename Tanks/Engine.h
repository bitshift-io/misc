#ifndef _ENGINE
#define _ENGINE

//This is where every thing comes together
// to make a nice engine class :)

//-----------------------------------------------------------------------------
// DIRECTX Include files
//-----------------------------------------------------------------------------
#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "Dinput8.lib")
#pragma comment(lib, "Dxguid.lib")
#pragma comment(lib, "D3dx8.lib")

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>
#include <Dinput.h>

//-----------------------------------------------------------------------------
// Custom Include files
//-----------------------------------------------------------------------------
#include "ErrorLogging.h"

#include "Windows.h"
//#include "3ds.h"

#include "Timming.h"
#include "DataManip.h"
#include "Structs.h"
#include "3DAPI.h"
#include "DirectX.h"
#include "Direct3D.h"
#include "DirectInput.h"

#include "DirectSound.h"
#include "Sound.h"

//#include "3DSFormat.h"
#include "TGAFormat.h"
#include "Object.h"
#include "Object3D.h"
#include "Image.h"
#include "Layer.h"
#include "GUIImage.h"

#include "Light.h"
#include "Camera.h"



#include "Sprite.h"
#include "Particle.h"
#include "Mesh.h"
#include "AdvMesh.h"
#include "CollisionDetection.h"

#include "List.h"
#include "Font.h"
#include "Frame.h"

#include "GUITextbox.h"

#include "INIFormat.h"

#include "WinInput.h"


//GLOABLAS =================================================
extern CINI g_ini;

extern CCamera g_camera; //this is dogey, need a game class with camera
					// and d3d device etc in it...

//=================================================

class CEngine
{
public:
	CEngine( HINSTANCE hInstance );
	~CEngine();
	virtual void Init();
	virtual void MainLoop(); //runs till exit - false
	virtual void Shutdown();
	virtual void Set3DAPI( int API );

	CFrame world; //the main frame for the engine, everything 3d is added here
	HWND hWnd; //the handle to our app/window
	HINSTANCE hInstance; //this iis taken strait from WinMain
	WNDCLASSEX wc; //our windows class, prolly dont need to keep track of it

	//cal back f'ns
	bool (*MainLoopCallbk)(CEngine* pEngine);

	int deviceWidth;
	int deviceHeight;
	bool fullScreen;

	char* appName;

	C3DAPI* API;

private:

};

#endif