#include <stdlib.h>
#include <string.h>
#include "DataManip.h"

char* StringCat(char* strFirst, char* strSecond)
{
	char *returnString = new char[MAX_STRING_SIZE*2];

	int i=0;
	while( i < (int)strlen(strFirst) )
	{
		returnString[i] = strFirst[i];
		i++;
	}

	int j=0;
	while( j < (int)strlen(strSecond) )
	{
		returnString[i] = strSecond[j];
		i++,j++;
	}

	returnString[i++] = '\0';

	return returnString;
}

//these dont work with decimal points ATM
// or negatives!

float StringToNumber(char *string)
{
	float newNumber = 0;
	float tempNo = 0;

	int index = 0;

	while(string[index] != '\0')
	{
		if(newNumber != 0)
		{
			newNumber *= 10;
		}

		tempNo = (float)string[index] - '0';

		newNumber += tempNo;

		index++;
	}

	return newNumber;
}

char* NumberToString(float number)
{
	int index=0;
	char *returnString = (char*)malloc(100*sizeof(char));
	char tempString[100];
	char tempChar;
	float tempNo = number;
	int tempNoToChar;
	bool negative = 0;

	//firstly is it negative?
	if( tempNo < 0 )
	{
		tempNo = -tempNo;
		negative = 1;
	}

 	while( tempNo > 9 )
	{
		tempNoToChar = (int)tempNo % 10;
		tempChar = tempNoToChar + '0';
		tempString[index] = tempChar;
		tempNo /= 10;

		index++;
	}

	tempNoToChar = (int)tempNo % 10;
	tempChar = tempNoToChar + '0';
	tempString[index] = tempChar;

	if( negative )
	{
		tempString[index+1] = '-';
		index++;
	}

	tempString[index+1] = '\0';

	//now that we got the number into a string, it's
	// backwards! so reverse the string...
	int size = strlen(tempString);
	for(index = size; index > 0; index--)
	{
		returnString[size - index] = tempString[index-1];
		//returnString = tempString;
	}
	returnString[size] = '\0';

	return returnString;

}

//searches a string for a substring
bool StringSearch(char *searchVal, char *searchStr)
{
	int i=0, j=0, temp;

	//find the start chars to match
	while( searchStr[i] != '\0' )
	{
		if(searchStr[i] == searchVal[0])
		{
			temp = i;
			while((searchStr[i] == searchVal[j]))
			{
				if(searchVal[j+1] == '\0')
					return true;

				if(searchStr[i+1] == '\0')
					return false;

				i++, j++;
			}
			i=temp, j=0;
		}
		i++;
	}

	return false;
}

// string, the string to divide, left is left of the char, right
// is right of the dividing char
void StringDivide(char *string, char *left, char*right, char dividingChar)
{
	int i=0, j=0;
	while( string[i] != '\0' )
	{
		left[i] = string[i];

		if(string[i] == dividingChar)
		{
			i++;
			left[i] = '\0';
			while( string[i] != '\0' )
			{
				right[j] = string[i];
				i++, j++;
			}
			right[j++] = '\0';
			break;
		}
		i++;
	}
}
