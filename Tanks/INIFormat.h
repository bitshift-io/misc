#ifndef _INIFORMAT
#define _INIFORMAT

/* *********************************************
Fabian Mathews
2002

  INI File loader/saver
  requires DataManip.h

********************************************* */

/*

  ;comment

  [title]
  setting=variable

  */

#include <stdio.h>

class CINI
{
public:
	CINI();
	~CINI();
	CINI(char *path);

	void SetPath(char *path);
	void SetSection(char *section);
	char *ReadKey(char *key);
	void WriteKey(char *key);

private:
	FILE *pFile;
	char *path;
	char *section;
};

#endif