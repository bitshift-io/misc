#ifndef _GUITEXTBOX
#define _GUITEXTBOX

#include "Font.h"
#include "GUIImage.h"

//so we have a text box, it needs to get the key presses...
// it needs to store a string of letters
// which can be accesses by other classes etc...
//extern class CFont;

class CGUITextbox : public CGUIImage
{
public:
	CGUITextbox();
	void Update();
	void Render();
	void OnMouseClick();
	void OnMouseOver();
	void OnMouseOut();
	char* GetText(){ return text; } //returns current text
	char* GetLastText();
	void SetText( char* text ){ this->text = text; }
	void SetMaxChars( int num ){ maxChars = num; }
	void SetFont( CFont* font ){ this->font = font; }
	void SetAcceptInput( bool value){ bAcceptInput = value; }

	char* text;

private:
	RECT srcText;
	CFont* font;
	LPDIRECT3DSURFACE8 textImg;

	char* lastText; //a copy of last string when lost focus
	int maxChars; // no chars that will fit before it scrolls/gets cropped etc..
	bool bAcceptInput; //defaults to yes, we will tak input
};


#endif




