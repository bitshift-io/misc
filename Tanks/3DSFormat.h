#ifndef _3DSFORMAT
#define _3DSFORMAT

/*

#include <stdio.h>
#include <windows.h>
#include "ErrorLogging.h"


#define FILEID       		0x4D4D	//the file ID chunk
#define OBJECT1		 		0x4000 //object can have lights and cameras also
#define OBJECT_MESH			0x4100 //thats why there is object and object_mesh
#define VERTICES_LIST		0x4110 //a list of all vertices in the object
#define FACE_LIST			0x4120 //is a list of numbers refering to the 
									//verticies list


bool Load3DS( char* path, float* vertexList, unsigned int* faceList )
{

	//void *ptrVoid;
	int bytesRead = 0; //stores how many bytes of the chunk we have read
	unsigned short int chunkID; // 2 bytes
	unsigned int chunkLength; //4 bytes
	unsigned int fileLength; //same as above

	//printf("3DS FILE LOADER\n");

	FILE *ptrFile;
	//open file
	ptrFile = fopen(path, "rb");
	if(ptrFile == NULL)
	{
		Error("CANT OPEN 3ds FILE");
		char strMessage[255] = {0};
		sprintf(strMessage, "Unable to find the file: %s!", path);
		MessageBox(NULL, strMessage, "Error", MB_OK);
	}

	// This reads the chunkID which is 2 bytes
	//
	bytesRead = fread( &chunkID,1, 2, ptrFile);

	if( chunkID != FILEID)
	{
		Error("this file aint 3ds, or its fx0red");
		return false;
	}

	// Now read the next 4 bytes, the chunk length of the chunk,
	// or file if its the first chunk like we are doing now
	bytesRead += fread( &fileLength, 1, 4, ptrFile);

	//printf("FileSize:%i\n", fileLength);

	//keep reading chunks till we have read every chunk
	while(bytesRead < (int)fileLength)
	{
		//looking for chunkID
		bytesRead += fread( &chunkID,1, 2, ptrFile);

		if( feof(ptrFile) )
		{
			break;
		}

		if(chunkID == VERTICES_LIST)
		{
			Log("vertices list:");

			//is there a size?
			bytesRead += fread( &chunkLength, 1, 4, ptrFile);
			//printf("chunk size:%i\n", chunkLength);

			unsigned short noVertices;
			bytesRead += fread( &noVertices, 1, 2, ptrFile);
			
			//float vertexList[100][3]  = new float[noVertices][3];

			float vertices [3]; // x,y,z

			for(int i=0; i < noVertices; i++)
			{
				bytesRead += fread(&(vertices[0]),sizeof(float),1,ptrFile);
				bytesRead += fread(&(vertices[1]),sizeof(float),1,ptrFile);
				bytesRead += fread(&(vertices[2]),sizeof(float),1,ptrFile);
				
				/*
				vertexList[i][0] = vertices[0];
				vertexList[i][1] = vertices[1];
				vertexList[i][2] = vertices[2];*/

				/*
				Log(NumberToString(vertices[0]));
				Log(" ");
				Log(NumberToString(vertices[1]));
				Log(" ");
				Log(NumberToString(vertices[2]));
				Log("\n");* /

			}
		}

		if(	chunkID == FACE_LIST )
		{
			Log("poly list:");

			//lets read the num of polys....
			unsigned int numPolys;
			bytesRead += fread( &numPolys, sizeof(unsigned int), 1, ptrFile);

			unsigned int vertListNumbers[3];
			unsigned int unknown;

			for(int i=0; i < (int)numPolys; i++)
			{
				bytesRead += fread(&(vertListNumbers[0]),sizeof(unsigned int),1,ptrFile);
				bytesRead += fread(&(vertListNumbers[1]),sizeof(unsigned int),1,ptrFile);
				bytesRead += fread(&(vertListNumbers[2]),sizeof(unsigned int),1,ptrFile);
				bytesRead += fread(&(unknown),sizeof(unsigned int),1,ptrFile);

				/*
				Log(NumberToString(vertListNumbers[0]));
				Log(" ");
				Log(NumberToString(vertListNumbers[1]));
				Log(" ");
				Log(NumberToString(vertListNumbers[2]));
				Log("\n");* /
			}
		}
	}

	return true;
}

void ConvertListsToModel(float* vertexList, unsigned int* faceList, float* polyArray )
{
	//go through the face list, grab the value corresponding
	// from the vertex list, and copy it to polyArray
}

*/

#endif