#ifndef _OBJECT3D
#define _OBJECT3D

#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "Dinput8.lib")
#pragma comment(lib, "Dxguid.lib")
#pragma comment(lib, "D3dx8.lib") 

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

//class CFrame;
#include "Object.h"

class CObject3D : public CObject
{
public:
	CObject3D();
	CObject3D(long ID);
	virtual void Render();
	virtual void Update();
	void Remove();

	//void SetNext( CObject3D* next){ this->next = next; }
	//void* GetNext(){ return next; }

	//void SetParent( CObject3D* parent){ this->parent = parent; }
	//void* GetParent(){ return parent; }

	//void SetCallbackFn( void (*CallbackFn)(CObject3D* owner));
	void SetCallbackFn( void (*CallbackFn)(CObject3D* owner));
	virtual OBJECT_TYPE GetType() const {return OBJECT3D;}

	void (*CallbackFn)(CObject3D* owner);

	unsigned int GetSize(){ return sizeof( *this ); }

	//CObject3D* next;

	D3DXVECTOR3 vPosition;
	D3DXVECTOR3 vVelocity;

	float xRotUpdate;
	float yRotUpdate;
	float zRotUpdate;

	float xRot;
	float yRot;
	float zRot;

	D3DXMATRIX mLocal;

protected:
	long ID;
	//CObject3D* parent;
	//void (*CallbackFn)(CObject3D* owner);
};


#endif