#include "Light.h"
#include "DirectX.h"

CLight::CLight()
{
	lightNo = lightCount;
	lightCount++;
	ZeroMemory( &light, sizeof(D3DLIGHT8) );
	enable = true;
	light.Type       = D3DLIGHT_DIRECTIONAL;
	light.Range       = 600.0f;

	light.Diffuse.r  = 1.0f;
	light.Diffuse.g  = 1.0f;
	light.Diffuse.b  = 1.0f;

	light.Specular.r = 0.5f;
	light.Specular.g = 0.5f;
	light.Specular.b = 0.5f;

	light.Ambient.r = 0.01f;
	light.Ambient.g = 0.01f;
	light.Ambient.b = 0.01f;


	light.Position = D3DXVECTOR3(0,0,0);
	light.Direction = D3DXVECTOR3(0,0,1);

	g_pd3dDevice->SetLight( lightNo, &light );
	//g_pd3dDevice->SetRenderState
}

void CLight::SetLightType( D3DLIGHTTYPE type )
{
	light.Type = type;
}

void CLight::Render()
{
	Update();

	//g_pd3dDevice->SetRenderState( D3DRS_AMBIENT, 0x00202020 );
	g_pd3dDevice->SetLight( lightNo, &light );
}

void CLight::Update()
{
	if( enable == true )
		g_pd3dDevice->LightEnable( 0, TRUE);
	else
		g_pd3dDevice->LightEnable( 0, FALSE);

	D3DXVec3Normalize( (D3DXVECTOR3*)&light.Direction, &lookAt );

	if( GetParent() )
	{
		D3DXMATRIX *mParent;

		mParent = &((CObject3D*)GetParent())->mLocal;

		D3DXVec3TransformCoord( &vPosition, &vPosition, mParent );
		light.Position = vPosition;
	}
}
