#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include "GUIImage.h"
#include "DirectInput.h"
#include "DirectX.h"

CGUIImage::CGUIImage()
{
	//call this so it sets the proper srcRects etc...
	//MouseOutFn(this);

	MouseOverFn != NULL;
	MouseClickFn != NULL;
	MouseOutFn != NULL;

	srcRect.bottom = 0;
	srcRect.left = 0;
	srcRect.right = 0;
	srcRect.top = 0;
}

void CGUIImage::SetMouseZone(RECT* zone)
{
	//if null is passed in, default to srcRect
	if( zone == NULL )
	{
		mouseZone.bottom = srcRect.bottom;
		mouseZone.top = srcRect.top;
		mouseZone.left = srcRect.left;
		mouseZone.right = srcRect.right;
	}
	else
	{
		mouseZone.bottom = zone->bottom;
		mouseZone.left = zone->left;
		mouseZone.right = zone->right;
		mouseZone.top = zone->top;
	}
}

void CGUIImage::Render()
{
	Update();

	CopySurfaceFast(&image, &srcRect, &g_pBackSurf, &realDestPoint);
}

void CGUIImage::Update()
{
	CImage::Update();


	//before we render, we need to update the destpoints
	// as they are inherted from the parent....
	//this makes "this" relative to its parent
	//CImage* pParent = (CImage*)GetParent();
	//CLayer* pParent = (CLayer*)parent;
	//realDestPoint.x = pParent->destPoint->x + destPoint->x;
	//realDestPoint.y = pParent->destPoint->y + destPoint->y;

	//cheack to see if the mouseZone
	// and actual mouse position are
	// over each other
	RECT realMouseZone;
	realMouseZone.left = mouseZone.left + realDestPoint.x;
	realMouseZone.right = mouseZone.right + realDestPoint.x;
	realMouseZone.top = mouseZone.top + realDestPoint.y;
	realMouseZone.bottom = mouseZone.bottom + realDestPoint.y;


	//wheres the mouse?
	if( (g_mouseData.x >= realMouseZone.left) &&
		(g_mouseData.x <= realMouseZone.right) &&
		(g_mouseData.y >= realMouseZone.top) &&
		(g_mouseData.y <= realMouseZone.bottom))
	{
		this->wasOver = true;

		OnMouseOver();

		if( g_mouseData.button[0] == -1)
		{
			OnMouseClick();
		}
	}
	//the mouse aint over, was it over?
	else
	{
		if( wasOver == true )
		{
			wasOver = false;
			OnMouseOut();
		}
	}


};

void CGUIImage::OnMouseClick()
{
	if( MouseClickFn != NULL )
		MouseClickFn(this);
}

void CGUIImage::OnMouseOut()
{
	if( MouseOutFn != NULL )
		MouseOutFn(this);
}

void CGUIImage::OnMouseOver()
{
	if( MouseOverFn != NULL )
		MouseOverFn(this);
}

void CGUIImage::Remove()
{
	//clean up code, removeing is killing, if u want
	//to change parents dont call this! call SetParent
	CallbackFn = 0;
	next = 0;
	parent = 0;

	/*
	delete image;
	image = NULL;*/
	image->Release();
	image = 0;

	MouseOverFn = 0;
	MouseOutFn = 0;
	MouseClickFn = 0;

	delete destPoint;
	destPoint = 0;

	delete this;
}
