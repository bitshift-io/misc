#include "Engine.h"
#include "Projectile.h"
#include "Player.h"
#include "Tanks.h"

OBJECT_TYPE CProjectile::GetType() const {return ADVMESH;}

	CProjectile::CProjectile(CPlayer* playerOwner, CAdvMesh* bullet, CPlayer* players[], int numPlayers)
	{
		CAdvMesh::CAdvMesh();
		this->playerOwner = playerOwner;

		allPlayers = players;
		noPlayers = numPlayers;

		damage = 110; //amke sure they die!
		radius = 15;
		//LoadXFile("bullet.x");

		vertexData = bullet->vertexData; //pointer to vertex buffer
		vertData = bullet->vertData; //this is the proper way!!!

		numMaterials = bullet->numMaterials;
		pMesh = bullet->pMesh;
		pMaterials = bullet->pMaterials;
		pTextures = bullet->pTextures;

		lastTime = CTime::GetTime();
	}

	void CProjectile::Update()
	{
		long int newTime = CTime::GetTime();

		if( !bInGame ) //pause
			lastTime = newTime;

		float moveSpeedX = (float)((newTime - lastTime)*velocity.x);
		float moveSpeedZ = (float)((newTime - lastTime)*velocity.z);
		lastTime = newTime;

		vVelocity.x = moveSpeedX;
		vVelocity.z = moveSpeedZ;
		

		//NEED TO ADD CODE TO REMOVE PORJECTILES FROM THE WORLD's LINKED LIST


		//check to amke sure we are in the map - need to fix remove code
		if( vPosition.x > mapBorderSize || vPosition.x < -mapBorderSize )
		{
			((CFrame*)GetParent())->RemoveObject(this);
			bDestroy = true;
		}


		if( vPosition.z > mapBorderSize || vPosition.z < -mapBorderSize )
		{
			((CFrame*)GetParent())->RemoveObject(this);
			bDestroy = true;
		}



		//make sure we havent run into any one else
		for( int i=0; i< noPlayers; i++)
		{
			if( ( allPlayers[i]->vPosition.x <= (vPosition.x + radius) ) &&
				( allPlayers[i]->vPosition.x >= (vPosition.x - radius) ) &&
				( allPlayers[i]->vPosition.z <= (vPosition.z + radius) ) &&
				( allPlayers[i]->vPosition.z >= (vPosition.z - radius) ) &&
				allPlayers[i] != playerOwner && allPlayers[i]->bIsInGame)
			{
				allPlayers[i]->TakeDamage(damage, playerOwner);
				((CFrame*)GetParent())->RemoveObject(this);
				bDestroy = true;
			}
		}

		CAdvMesh::Update();

				
	}