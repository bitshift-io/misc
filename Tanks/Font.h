#ifndef _FONT
#define _FONT

#include "Structs.h"
#include "List.h"
// TODO: MAKE THE FONT USE CopyRects which is way faster

//-----------------------------------------------------------------------------
// CFONT
//-----------------------------------------------------------------------------
class CFont
{
public:
	CFont();
	void Remove();
	CFont(char *path,int lettersPerRow, int rows);

	void Update();
	FNT PrintText(char *text, int xPos, int yPos, int time);
	void StretchImage(long width, long height);
	LPDIRECT3DSURFACE8 GetLastTextAsImage();
	FNT* GetLastAsFont();
	void Render();

private:
	HEAD fntList;
	FNT* lastFnt;
	LPDIRECT3DSURFACE8 fontImg;
	int width;
	int height;
	int lettersPerRow;
	int rows;
	int letterWidth;
	int letterHeight;
};


#endif

