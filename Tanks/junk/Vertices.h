#ifndef _VERTICES
#define _VERTICES


extern HWND g_hWnd;
extern WNDCLASSEX *g_wc;
extern HINSTANCE g_hInstance;

//paths to data stuffs
extern char* g_meshPath;
extern char* g_meshTexturePath; 
extern char* g_texturesPath;
extern char* g_soundsPath;
extern char* g_dataPath;
extern char* g_imageDumpsPath;


int g_deviceWidth = 640;
int g_deviceHeight = 480;
bool g_fullScreen = true;
bool g_screenSaver = false; //make it so all keys quit

//-----------------------------------------------------------------------------
// Custom Nesisary Globals
//-----------------------------------------------------------------------------

bool bInMenu = true;
bool bDisplayScore = false;
bool bInGame = false;
bool bEndGame = false;
const int mapBorderSize = 430;




#endif