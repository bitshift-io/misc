#ifndef _DIRECT3D
#define _DIRECT3D

#include <windows.h>
#include "3DAPI.h"

#define SURFACEFORMAT D3DFMT_A8R8G8B8

//LPDIRECT3DSURFACE8 g_pBackSurf = 0;
//LPDIRECT3DSURFACE8 g_pBmpSurf = 0;
//LPDIRECT3D8             g_pD3D       = NULL; // Used to create the D3DDevice
//LPDIRECT3DDEVICE8       g_pd3dDevice = NULL; // Our rendering device
//LPDIRECT3DVERTEXBUFFER8 g_pVB        = NULL; // Buffer to hold vertices

class CDirect3D : public C3DAPI
{
public:
	CDirect3D();

	virtual void Init3DAPI( HWND hWnd, bool fullScreen, int deviceWidth, int deviceHeight );
	virtual void Shutdown3DAPI();

	virtual void BeginScene();
	virtual void EndScene();

	//virtual void SetClearColor( COLOR* pColor );

private:
	LPDIRECT3DDEVICE8 pd3dDevice;
	LPDIRECT3DSURFACE8 pBackSurf;
};

#endif
