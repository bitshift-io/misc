#ifndef _TGAFORMAT
#define _TGAFORMAT

#include <D3d8.h>
#include <stdio.h>


//__declspec( dllexport ) void LoadTGA(char *path, LPDIRECT3DSURFACE8* destSurf);
//__declspec( dllexport ) void SaveTGA(char *path, LPDIRECT3DSURFACE8* srcSurf);
// extern __declspec( dllimport )

//the #pragma gets ride of padding in the struct i guess
#pragma pack(push, 1)

struct TGA
{
	byte ident;
	byte colormap;
	byte type;
	unsigned short int colormaporigin;
	unsigned short int colormaplength;
	byte colormapsize;
	unsigned short int x;
	unsigned short int y;
	unsigned short int width;
	unsigned short int height;
	byte bpp;
	byte descriptor;
};

#pragma pack(pop)

void LoadTGA(char *path, LPDIRECT3DSURFACE8* destSurf);
void SaveTGA(char *path, LPDIRECT3DSURFACE8* srcSurf);
#endif
