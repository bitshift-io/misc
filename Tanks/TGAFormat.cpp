#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include <stdio.h>
#include "TGAFormat.h"
#include "directx.h"
#include "ErrorLogging.h"
#include "DataManip.h"


void LoadTGA(char *path, LPDIRECT3DSURFACE8* destSurf)
{
	HRESULT r =0;
	TGA tga;

	path = (char*)StringCat(g_texturesPath, path);

	FILE* pFile = fopen(path, "rb");
	if( pFile == NULL )
		Error("cant open file");

	//fseek(pFile,
	fseek(pFile, 0, SEEK_SET);
	rewind(pFile);

	//fread(&tga,1,sizeof(TGA),pFile);
	fread(&tga,sizeof(TGA),1,pFile);

	//now determine how much data there is,
	//read it all in
	unsigned int imageDataSize = tga.bpp * tga.width * tga.height;
	unsigned int* imageData = new unsigned int[imageDataSize];
	fread( imageData, 1, imageDataSize, pFile);

	LPDIRECT3DSURFACE8 image;
	r = g_pd3dDevice->CreateImageSurface( tga.width, tga.height,
		SURFACEFORMAT, &image);
	if( FAILED(r) )
	{
		Error("Unable to create surface for TGA load");
		return;
	}


	r = g_pd3dDevice->CreateImageSurface( tga.width, tga.height,
		SURFACEFORMAT, destSurf);
	if( FAILED(r) )
	{
		Error("Unable to create surface for TGA load");
		return;
	}

	//time to load in the data
	D3DLOCKED_RECT lockedArea;
	image->LockRect( &lockedArea, NULL, D3DLOCK_NOSYSLOCK);

	//unsigned short int  <-- 2 bytes
	//unsigned int <-- 4 bytes

	unsigned int pitch = lockedArea.Pitch;
	unsigned int sizeOfLine = (tga.width * (tga.bpp/8));

	unsigned int* pBits = (unsigned int*)lockedArea.pBits;
	unsigned int*  pImageBits = imageData;
	//the directX is upside down, so gopy from the end to
	//the front
	pImageBits +=  sizeOfLine/4 * tga.height;

	for(int i=0; i < tga.height ;i++)
	{
		memcpy(pBits, pImageBits, sizeOfLine);

		pBits += pitch/4;			//divide by 4 - 4 bytes
		pImageBits -= sizeOfLine/4;	//same as above, using -=
									//here to reverse the tga
	}

	image->UnlockRect();

	fclose(pFile);

	POINT point = {0,0};
	RECT src = {0,0, tga.width, tga.height};
	CopySurfaceFast(&image, &src, destSurf , &point);
}

void SaveTGA(char *path, LPDIRECT3DSURFACE8* srcSurf)
{
	TGA tga;

	path = (char*)StringCat(g_imageDumpsPath, path);

	//fill up the TGA structure
	D3DSURFACE_DESC desc;
	(*srcSurf)->GetDesc( &desc );

	tga.x = 0;
	tga.y = 0;
	tga.descriptor = 0;
	tga.ident = 0;
	tga.colormap = 0;
	tga.type = 2;
	tga.colormaplength = 0;
	tga.colormaporigin = 0;
	tga.colormapsize = 0;
	tga.bpp = 32;
	tga.width = desc.Width;
	tga.height = desc.Height;

	//now determine how much image data there is
	unsigned int imageDataSize = tga.bpp * tga.width * tga.height;
	unsigned int* imageData = new unsigned int[imageDataSize];

	FILE* pFile = fopen(path, "wb");
	if( pFile == NULL )
		Error("cant open file");

	//write headder info
	fwrite( &tga, sizeof(TGA), 1, pFile );

	//now dump the surface
	D3DLOCKED_RECT lockedArea;
	(*srcSurf)->LockRect( &lockedArea, NULL, D3DLOCK_NOSYSLOCK);

	//unsigned short int  <-- 2 bytes
	//unsigned int <-- 4 bytes

	unsigned int pitch = lockedArea.Pitch;
	unsigned int sizeOfLine = (tga.width * (tga.bpp/8));

	unsigned int* pBits = (unsigned int*)lockedArea.pBits;
	//unsigned int* pImageBits = imageData;
	//the directX is upside down, so gopy from the end to
	//the front
	unsigned int* pImageBits = imageData;
	pImageBits +=  sizeOfLine/4 * tga.height;

	for(int i=0; i < tga.height ;i++)
	{
		memcpy(pImageBits, pBits, sizeOfLine);

		pBits += pitch/4;			//divide by 4 - 4 bytes
		pImageBits -= sizeOfLine/4;	//same as above, using -=
									//here to reverse the tga
	}

	(*srcSurf)->UnlockRect();

	fwrite( imageData, imageDataSize, 1, pFile );

	fclose(pFile);
}
