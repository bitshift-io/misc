//***********************************************************************
//											
//	- "Talk to me like I'm a 3 year old!" Programming Lessons -		 
//                                                                       
//	$Author:	DigiBen		digiben@gametutorials.com	
//											
//	$Program:	3DS Loader			 
//											 
//	$Description:	Demonstrates how to load a .3ds file format		 
//											 
//	$Date:		10/6/01							
//											
//***********************************************************************


Files:  	Main.cpp   (The Source File containing the most worked with code)
		3DS.cpp    (The Source File containing the 3DS loading code)
		Init.cpp   (The Source File containing the rarely changed code)
		Main.h     (The header file that holds the global variables and prototypes)
		3DS.h      (The header file that holds the chunk definations and structures)
		3DS Format.rtf (The .3DS file format explained further)
		3DS Loader.dsp  (The Project File holding the project info)
		3DS Loader.dsw  (The Workspace File holding the workspace info)

Libraries:      opengl32.lib, glu32.lib, glaux.lib

Keys Used:	Left Mouse Button - Changes the Render mode from normal to wireframe.
		Right Mouse Button - Turns lighting On/Off
		Left Arrow Key - Spins the model to the left
		Right Arrow Key - Spins the model to the right
		Escape - Quits

Win32 App:	Remember, if using visual c/c++, if you want to create a windows
		application from scratch, and not a console application, you must 
		choose "Win32 App" in the projects tab when selecting "New" from
		the file menu in visual C.  Your program will not be able to run
		if you try and create a window inside of a console program.  Once
		again, if creating a new project, click on "Win32 Application" and
		not "Win32 Console Application" if you want to create a program for
		windows (not a DOS window).  This process is further explained at 
		www.GameTutorials.com

Instructions:	If you have visual studio c++ (around version 6) just click on the
		<Program Name>.dsw file.  This will open up up visual c++.  You will most
		likely see the code for <Program Name>.cpp.  If you don't, there should be
		a tab on the left window (workspace window) called "FileView".  Click the 
		tab and click on the plus sign in this tab if it isn't already open.
		There you should see 2 folders called "source" and "header".
		Double click on the "source" folder and you should see your source file with
		a .cpp extension after it.  Double click on the file and it will open
		up in your main window.  Hit Control-F5 to run the program.
		You will probably see a prompt to compile/build the project.  Click OK and
		a window should pop up with the program. :)

Ben Humphrey
Game Programmer
digiben@gametutorials.com
www.GameTutorials.com