#ifndef _DATAMANIP
#define _DATAMANIP

/* *********************************************
Fabian Mathews
2002

  String and number conversion/manipulation


********************************************* */
#define MAX_STRING_SIZE 50


char* StringCat(char* strFirst, char* strSecond);

//these dont work with decimal points ATM
// or negatives!

float StringToNumber(char *string);

char* NumberToString(float number);

//searches a string for a substring
bool StringSearch(char *searchVal, char *searchStr);

// string, the string to divide, left is left of the char, right
// is right of the dividing char
void StringDivide(char *string, char *left, char*right, char dividingChar);

#endif