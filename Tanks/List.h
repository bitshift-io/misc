#ifndef _LIST
#define _LIST


/* *********************************************
Fabian Mathews
2002

  Generic list stuff

********************************************* */
#include <stdio.h>
#include <stdlib.h>

void Error(char *text);

extern long IMGID; //image list
extern long FNTID; //font list
extern long NEXT;
extern long NEXT2;
extern long NEXT3; //remmebr its hex so thats 16

typedef struct NODE
{
	NODE *pNext;
	void *pNode;

	NODE()
	{
		pNext=NULL;
		pNode=NULL;
	}

} NODE;

typedef struct HEAD
{
	NODE *pNext;
	long ID;
	int size;

	HEAD()
	{
		ID = NULL;
		pNext = NULL;
		size = 0;
	}
}HEAD;

void ListAdd(HEAD *head, void *node, long size);
void *ListRetrive(HEAD *head, int pos);
void ListDelete(HEAD *head, int pos);

#endif



