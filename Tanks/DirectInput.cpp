

#include <DInput.h>
#include "DirectInput.h"
#include "ErrorLogging.h"
#include "DirectX.h"
#include "Windows.h"


LPDIRECTINPUT8 g_pDI;
LPDIRECTINPUTDEVICE8 g_pMouse;
LPDIRECTINPUTDEVICE8 g_pKeyboard;

KEYBOARD g_keyboardData;
MOUSE g_mouseData;

//-----------------------------------------------------------------------------
// InitInput
//-----------------------------------------------------------------------------
void InitDInput()
{
	HRESULT r = 0;

	    // Create the D3D object.
    r = DirectInput8Create( g_hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8A,
		 (void**)&g_pDI, NULL);
	if( FAILED( r ) )
		Error("DI failed 1");

	r = g_pDI->CreateDevice(GUID_SysMouse, &g_pMouse,NULL);
	if( FAILED( r ) )
		Error("DI failed 2");

	r = g_pDI->CreateDevice(GUID_SysKeyboard, &g_pKeyboard,NULL);
	if( FAILED( r ) )
		Error("DI failed 3");

	r = g_pMouse->SetDataFormat(&c_dfDIMouse2);
	if( FAILED( r ) )
		Error("DI failed 4");

	r = g_pKeyboard->SetDataFormat(&c_dfDIKeyboard);
	if( FAILED( r ) )
		Error("DI failed 5");

	r = g_pMouse->SetCooperativeLevel( g_hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE );
	if( FAILED( r ) )
		Error("DI failed 6");

	r = g_pKeyboard->SetCooperativeLevel( g_hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE );
	if( FAILED( r ) )
		Error("DI failed 7");

	r = g_pMouse->Acquire();
	if( FAILED( r ) )
		Error("DI failed 8");

	r = g_pKeyboard->Acquire();
	if( FAILED( r ) )
		Error("DI failed 9");
}

void AcquireInput()
{
	HRESULT r = 0;

	r = g_pMouse->Acquire();
	if( FAILED( r ) )
		Error("DI failed");

	r = g_pKeyboard->Acquire();
	if( FAILED( r ) )
		Error("DI failed");
}

void CheckDInput()
{
	HRESULT r = 0;

	///// MOUSE STUFFS HERE ///////////////////////////////////

	DIMOUSESTATE2 dims2;      // DirectInput mouse state structure

    if( NULL == g_pMouse )
        return;

    // Get the input's device state, and put the state in dims
    ZeroMemory( &dims2, sizeof(dims2) );
    r = g_pMouse->GetDeviceState( sizeof(DIMOUSESTATE2), &dims2 );
    if( FAILED(r) )
    {
        // DirectInput may be telling us that the input stream has been
        // interrupted.  We aren't tracking any state between polls, so
        // we don't have any special reset that needs to be done.
        // We just re-acquire and try again.

        // If input is lost then acquire and keep trying
        r = g_pMouse->Acquire();
        while( r == DIERR_INPUTLOST )
            r = g_pMouse->Acquire();
    }

	//there can be up to 8 buttons
	for(int i=0; i < 8; i++)
	{
		//button down....else its up
		if(dims2.rgbButtons[i] & 0x80)
		{
			// 1 means its down, 2 means it just went down
			if( g_mouseData.button[i] == 2 )
				g_mouseData.button[i] = 1;
			if( g_mouseData.button[i] <= 0)
				g_mouseData.button[i] = 2;

		}
		else
		{
			// 0 means its up, -1 means it just went up
			if( g_mouseData.button[i] == -1 )
				g_mouseData.button[i] = 0;
			if( g_mouseData.button[i] > 0 )
				g_mouseData.button[i] = -1;
		}

	}

	//cheack for axis changes, and make sure
	// the mouse wont be rendered outside of the screen or we get errors!
	if( ((g_mouseData.x + g_mouseData.width + dims2.lX) <= g_deviceWidth ) &&
		((g_mouseData.x + dims2.lX) >= 0 ))
		g_mouseData.x += dims2.lX;

	if( ((g_mouseData.y + g_mouseData.height + dims2.lY) <= g_deviceHeight ) &&
		((g_mouseData.y + dims2.lY) >= 0 ))
		g_mouseData.y += dims2.lY;

	///// KEYBOARD STUFFS HERE ///////////////////////////////////
	char keyBuffer[256];

    r = g_pKeyboard->GetDeviceState(sizeof(keyBuffer),(LPVOID)&keyBuffer);
    if( FAILED(r) )
    {
         // If it failed, the device has probably been lost.
         // Check for (hr == DIERR_INPUTLOST)
         // and attempt to reacquire it here.
		r = g_pKeyboard->Acquire();
        while( r == DIERR_INPUTLOST )
            r = g_pKeyboard->Acquire();
         //return;
    }

	for(i=0; i < 256; i++ )
	{
		//button down....else its up
		if( keyBuffer[i] & 0x80 )
		{
			//Error("w0000w");
			// 1 means its down, 2 means it just went down
			if( g_keyboardData.key[i] == 2 )
				g_keyboardData.key[i] = 1;
			if( g_keyboardData.key[i] <= 0 )
				g_keyboardData.key[i] = 2;
		}
		else
		{
			// 0 means its up, -1 means it just went up
			if( g_keyboardData.key[i] == -1 )
				g_keyboardData.key[i] = 0;
			if( g_keyboardData.key[i] > 0 )
				g_keyboardData.key[i] = -1;
		}
	}

}
