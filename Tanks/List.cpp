#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "List.h"

long IMGID = 0x00000001; //image list
long FNTID = 0x00000002; //font list
long NEXT = 0x00000004;
long NEXT2 = 0x00000008;
long NEXT3 = 0x00000010; //remmebr its hex so thats 16

//prolly wanna return a position
void ListAdd(HEAD *head, void *node, long size)
{
	NODE *newNode = new NODE;
	newNode->pNode = malloc(size);
	memcpy(newNode->pNode, node, size);

	if(head->pNext == NULL )
	{
		head->pNext = newNode;
	}
	else
	{
		NODE *curNode = head->pNext;

		while(curNode->pNext != NULL)
		{
			curNode = curNode->pNext;
		}
		curNode->pNext = newNode;
	}

	head->size++;

}

void *ListRetrive(HEAD *head, int pos)
{
	NODE *curNode = new NODE;
	curNode = head->pNext;

	for(int i=0; i < pos; i++)
	{
		curNode = curNode->pNext;
	}

	return (void*)curNode->pNode;
}

void ListDelete(HEAD *head, int pos)
{
	//make sure position is less than the max size
	if( pos > head->size)
	{
		Error("hmm");
		return;
	}

	NODE *prevNode = new NODE;
	NODE *curNode = new NODE;
	curNode->pNext = NULL;
	curNode = head->pNext;

	if( pos == 0)
	{
		head->pNext = curNode->pNext;
		delete curNode;

	}
	else
	{
		for(int i=0; i < pos; i++)
		{
			prevNode = curNode;
			curNode = curNode->pNext;
		}

		prevNode->pNext = curNode->pNext;
		delete curNode;
	}

	head->size--;
}




