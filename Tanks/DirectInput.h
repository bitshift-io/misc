#ifndef _DIRECTINPUT
#define _DIRECTINPUT


// reqirees Dxguid.lib Dinput8.lib

#include <DInput.h>

#define KEYDOWN(name, key) (name[key] & 0x80)

extern LPDIRECTINPUT8 g_pDI;
extern LPDIRECTINPUTDEVICE8 g_pMouse;
extern LPDIRECTINPUTDEVICE8 g_pKeyboard;

typedef struct KEYBOARD
{
	int key[256];
} KEYBOARD;

typedef struct MOUSE
{
	int button[8];
	long x;
	long y;
	int width;
	int height;
	MOUSE()
	{
		x = 0;
		y = 0;
	}
}MOUSE;

extern KEYBOARD g_keyboardData;
extern MOUSE g_mouseData;

void InitDInput();

void AcquireInput();

void CheckDInput();

#endif