#include "Engine.h"
#include "Player.h"
#include "Projectile.h"
#include "Tanks.h"

OBJECT_TYPE CPlayer::GetType() const {return FRAME;}

	CPlayer::CPlayer(CPlayer* players[], int numPlayers)
	{
		allPlayers = players;
		noPlayers = numPlayers;

		radius = 40;

		bIsInGame = true;

		CFrame::CFrame();

		bIsAI = false;

		playerTexture = 0;

		switch(playerNumber) //maximum of 3 humans, the rest have to be AI 
		{
		case 0:
			name = "Player 1";
			controls[FIRE] = DIK_Z;
			controls[RIGHT] = DIK_G;
			controls[LEFT] = DIK_D;
			controls[UP] = DIK_R;
			controls[DOWN] = DIK_F;
			controls[BLEFT] = DIK_A;
			controls[BRIGHT] = DIK_Q;

			playerTexture = new LPDIRECT3DTEXTURE8;
			if( FAILED( D3DXCreateTextureFromFile( g_pd3dDevice,
												"textures/human1.bmp",
												playerTexture ) ) )
				{
					Error("cant load texture");
					playerTexture = 0;
				}
			break;
		case 1:
			name = "Player 2";
			controls[FIRE] = DIK_RIGHT;
			controls[RIGHT] = DIK_NUMPAD3;
			controls[LEFT] = DIK_NUMPAD1;
			controls[UP] = DIK_NUMPAD5;
			controls[DOWN] = DIK_NUMPAD2;
			controls[BLEFT] = DIK_LEFT;
			controls[BRIGHT] = DIK_DOWN;

			playerTexture = new LPDIRECT3DTEXTURE8;
			if( FAILED( D3DXCreateTextureFromFile( g_pd3dDevice,
												"textures/human2.bmp",
												playerTexture ) ) )
				{
					Error("cant load texture");
					playerTexture = 0;
				}
			break;
		case 2:
			name = "Player 3";
			controls[FIRE] = DIK_SLASH;
			controls[RIGHT] = DIK_L;
			controls[LEFT] = DIK_J;
			controls[UP] = DIK_I;
			controls[DOWN] = DIK_K;
			controls[BLEFT] = DIK_APOSTROPHE;
			controls[BRIGHT] = DIK_RBRACKET;

			playerTexture = new LPDIRECT3DTEXTURE8;
			if( FAILED( D3DXCreateTextureFromFile( g_pd3dDevice,
												"textures/human3.bmp",
												playerTexture ) ) )
				{
					Error("cant load texture");
					playerTexture = 0;
				}
			break;
		default:
			name = "AI";
            bIsAI = true;
			controls[FIRE] = 0;
			controls[RIGHT] = 0;
			controls[LEFT] = 0;
			controls[UP] = 0;
			controls[DOWN] = 0;
			controls[BLEFT] = 0;
			controls[BRIGHT] = 0;
		}

		//default AI settings
		bLocked = false;
		doNextMove = 0;
		enemyIndex = -1;

		fireTime = 500;
		nextFireTime = 0;
		speed = 0.2; //unit per second
		rotation = 1;
		barrelrotation=1;
		score = 0;
		health = 100;
		bIsDead = false;

		lastTime = CTime::GetTime();

		
		barrel.vPosition = D3DXVECTOR3(0,8,0);
		//barrel.vPosition.y = 20;//needs tweaking
		barrel.SetParent(this);
		AddObject((CObject3D*)&barrel);
		

		body.LoadXFile("base.x");
		body.vPosition = D3DXVECTOR3(0,0,0);
		//body.vPosition.y = 10;//needs tweaking
		body.SetParent(this);
		AddObject((CObject3D*)&body);

		projectile.LoadXFile("bullet.x");

		playerNumber++;

		//load shrapnel and hide under map
		for(int i=0; i<4; i++)
		{
			shrapnel[i].LoadXFile("shrapnel.x");
			AddObject((CObject3D*)&shrapnel[i]);
		}

		destroyedTexture = new LPDIRECT3DTEXTURE8;

		// Create the blown-up texture
		//Error(g_texturesPath);

        if( FAILED( D3DXCreateTextureFromFile( g_pd3dDevice,
                                               "textures/destroyed.bmp",
                                               destroyedTexture ) ) )
        {
			//Error("cant load texture");
            destroyedTexture = 0;
        }

		//load sounds
		fireSound = new CSound("sounds/fire.wav");
		explosionSound = new CSound("sounds/explosion.wav");

		humanPlayerNumber == -1;

        Reset();				
	}

	void CPlayer::SetAI(bool value, int index = -1)
	{
		if( value == false )
		{
			bIsAI = false;

			switch(index) //change players texture here also :)
			{
			case 0:
				name = "Player 1";
				humanPlayerNumber = 0; 
				break;
			case 1:
				name = "Player 2";
				humanPlayerNumber = 1; 
				break;
			case 2:
				name = "Player 3";
				humanPlayerNumber = 2; 
				break;
			default:
				name = "Unknown";
				playerTexture = 0;
				break;
			}
		}
		else
		{
			name = "AI";
			bIsAI = true;
			playerTexture = 0;
		}
	}

	//respawns/resets all variables except score
	void CPlayer::Reset()
	{
		health = 100;
		bIsDead = false;

		enemyIndex = -1;

		//for relaoding textures
		body.LoadXFile("base.x");
		barrel.LoadXFile("turret.x");

		//reset shrapnel
		for(int i=0; i<4; i++)
		{
			shrapnel[i].vPosition.x = 0;
			shrapnel[i].vPosition.y = -20;
			shrapnel[i].vPosition.z = 0;
			shrapnel[i].vVelocity = D3DXVECTOR3(0,0,0);//going down
		}

		bIsExploding = false;

		//pick spawn spot
		bool bSpawnFound = false;
		int safteyCatch = 20;
		while( !bSpawnFound && safteyCatch > 0 )
		{
			vPosition.x = -mapBorderSize + (float)(rand()%(2*mapBorderSize));
			vPosition.z = -mapBorderSize + (float)(rand()%(2*mapBorderSize));
			safteyCatch--;
			bSpawnFound = true;

			//make sure our spawn spot doesnt collide with others
			for( int i=0; i < (playerNumber-1); i++)
			{
				if( ( allPlayers[i]->vPosition.x <= (vPosition.x + radius) ) &&
					( allPlayers[i]->vPosition.x >= (vPosition.x - radius) ) &&
					( allPlayers[i]->vPosition.z <= (vPosition.z + radius) ) &&
					( allPlayers[i]->vPosition.z >= (vPosition.z - radius) ) &&
					allPlayers[i] != this && allPlayers[i]->bIsInGame )
				{
					bSpawnFound = false;
				}
			}
		}


		yRot = 0;

		
		//change skins
		if( playerTexture != 0 )
		{
			for( int i=0; i < body.numMaterials-1; i++) //the tank wheels are the last skin, we dont wanna change them
			{
				(body.pTextures[i]) = *playerTexture;
			}

			for( i=0; i < barrel.numMaterials; i++)
			{
				(barrel.pTextures[i]) = *playerTexture;
			}
		}
	}

	void CPlayer::SetKey(int definedKey, int button)
	{
		controls[definedKey] = button;
	}

	int CPlayer::GetScore(){return score;}
	char* CPlayer::GetName(){return name;}

	bool CPlayer::IsDead()
	{
		return bIsDead;
	}

	void CPlayer::CheckKey()
	{
		if(!bIsInGame)
			return;

		if( !bInGame )
			return;

		if( bIsDead )
			return;

		if( bIsAI ) //ingame and ai
		{
			DoAI();
			return;
		}

		long int newTime = CTime::GetTime();
		float moveSpeed = (newTime - lastTime)*speed;
		lastTime = newTime;

		D3DXVECTOR3 newPosition(vPosition);

		if( g_keyboardData.key[controls[LEFT]] > 0 )
		{
			yRot -= radian((float)rotation);
		}
		else if( g_keyboardData.key[controls[RIGHT]] > 0 )
		{
			yRot += radian((float)rotation);
		}

		if(g_keyboardData.key[controls[BRIGHT]] > 0)
		{
			barrel.yRot += radian((float)barrelrotation);
		}
		else if(g_keyboardData.key[controls[BLEFT]] > 0)
		{
			barrel.yRot -= radian((float)barrelrotation);
		}


		if( g_keyboardData.key[controls[UP]] > 0)
		{
			newPosition.x += (float)(moveSpeed * sin( yRot ))/2;
			newPosition.z += (float)(moveSpeed * cos( yRot ))/2;
		}
		else if( g_keyboardData.key[controls[DOWN]] > 0)
		{
			newPosition.x -= (float)(moveSpeed * sin( yRot ))/2;
			newPosition.z -= (float)(moveSpeed * cos( yRot ))/2;
		}

		//check to amke sure we are in the map
		if( newPosition.x > mapBorderSize || newPosition.x < -mapBorderSize )
			return;

		if( newPosition.z > mapBorderSize || newPosition.z < -mapBorderSize )
			return;

		//make sure we havent run into any one else
		for( int i=0; i< noPlayers; i++)
		{
			if( ( allPlayers[i]->vPosition.x <= (newPosition.x + radius) ) &&
				( allPlayers[i]->vPosition.x >= (newPosition.x - radius) ) &&
				( allPlayers[i]->vPosition.z <= (newPosition.z + radius) ) &&
				( allPlayers[i]->vPosition.z >= (newPosition.z - radius) ) &&
				allPlayers[i] != this && allPlayers[i]->bIsInGame )
			{
				//Log();
				return;
			}
		}

        vPosition = newPosition;	

		if( g_keyboardData.key[controls[FIRE]] > 0 && nextFireTime <= 0 )
		{
			CProjectile* newProj = new CProjectile(this, &projectile, allPlayers, noPlayers);
			newProj->vPosition.y = 5;
			newProj->vPosition.x = vPosition.x + (float)(10 * sin( barrel.yRot + yRot ))/2;
			newProj->vPosition.z = vPosition.z + (float)(10 * cos( barrel.yRot + yRot ))/2;
			newProj->velocity.x = (float)(speed*5 * sin( barrel.yRot + yRot ))/2;
			newProj->velocity.z = (float)(speed*5 * cos( barrel.yRot + yRot ))/2;

			world->AddObject((CObject*)newProj);

			nextFireTime = fireTime;
			fireSound->Play();
		}
	}

	//load the standard player, called from init function
	void CPlayer::Init(CFrame *world)
	{
		this->world = world;
	}

	//do damage
	void CPlayer::TakeDamage(int damage, CPlayer* instigator)
	{
		if(!bIsInGame)
			return;

		health -= damage;

		if( health < 0 && !bIsDead )
		{
			bIsDead = true;
			instigator->ScoreKill(this);

			//change skins
			for( int i=0; i < body.numMaterials; i++)
			{
				(body.pTextures[i]) = *destroyedTexture;
			}

			for( i=0; i < barrel.numMaterials; i++)
			{
				(barrel.pTextures[i]) = *destroyedTexture;
			}

			//move shrapnel into players location
			for(i=0; i<4; i++)
			{
				shrapnel[i].vPosition.y = 0; //bring it up
				shrapnel[i].vVelocity.x = -1 + rand()%2;
				shrapnel[i].vVelocity.z = -1 + rand()%2;
				shrapnel[i].vVelocity.y = rand()%3;//going up
				shrapnel[i].xRotUpdate = rand()%1;
				shrapnel[i].yRotUpdate = rand()%1;
				shrapnel[i].zRotUpdate = rand()%1;
			}

			bIsExploding = true;

			explosionSound->Play();
			bShakeCamera = true;
		}
	}

	void CPlayer::ScoreKill(CPlayer* killed)
	{
		if(!bIsInGame)
			return;

		score++;
	}

	void CPlayer::DoAI()
	{

		//D3DXVECTOR3 newPosition(vPosition);
		D3DXVECTOR3 checkObstacle(vPosition);
		bool moveDirection[7]; //this contains information about legitimate moves
		bool bFire = false;
		bool bDoNextMove = false;


				
			for(int i=0; i < 7; i++)
			{
				moveDirection[i] = true; //true means we can choose any of the following
			}

			/*
			#define LEFT 0
			#define RIGHT 1
			#define UP 2
			#define DOWN 3
			*/

			//determine any obsticals then move
			//check to amke sure we are in the map
			checkObstacle = vPosition;
			checkObstacle.x += (float)(10 * sin( yRot ))/2;
			checkObstacle.y += (float)(10 * cos( yRot ))/2;	

			//if something is in front of us...
			if( (checkObstacle.x > mapBorderSize || checkObstacle.x < -mapBorderSize) ||
				( checkObstacle.z > mapBorderSize || checkObstacle.z < -mapBorderSize ))//|| or player
			{
				moveDirection[2] = false; //cant go fowards
				bDoNextMove = true; //recalculate moves
			}

			for( i=0; i< noPlayers; i++)
			{
				if( ( allPlayers[i]->vPosition.x <= (checkObstacle.x + radius) ) &&
					( allPlayers[i]->vPosition.x >= (checkObstacle.x - radius) ) &&
					( allPlayers[i]->vPosition.z <= (checkObstacle.z + radius) ) &&
					( allPlayers[i]->vPosition.z >= (checkObstacle.z - radius) ) &&
					allPlayers[i] != this && allPlayers[i]->bIsInGame)
				{
					moveDirection[2] = false; //cant go fowards
					bDoNextMove = true; //recalculate moves
				}
			}

			//something behind us
			checkObstacle = vPosition;
			checkObstacle.x -= (float)(10 * sin( yRot ))/2;
			checkObstacle.y -= (float)(10 * cos( yRot ))/2;

			if( (checkObstacle.x > mapBorderSize || checkObstacle.x < -mapBorderSize) ||
				( checkObstacle.z > mapBorderSize || checkObstacle.z < -mapBorderSize ))//|| or player
			{
				moveDirection[3] = false; //cant go fowards
				bDoNextMove = true; //recalculate moves
			}

			for( i=0; i< noPlayers; i++)
			{
				if( ( allPlayers[i]->vPosition.x <= (checkObstacle.x + radius) ) &&
					( allPlayers[i]->vPosition.x >= (checkObstacle.x - radius) ) &&
					( allPlayers[i]->vPosition.z <= (checkObstacle.z + radius) ) &&
					( allPlayers[i]->vPosition.z >= (checkObstacle.z - radius) ) &&
					allPlayers[i] != this && allPlayers[i]->bIsInGame)
				{
					moveDirection[3] = false; //cant go fowards
					bDoNextMove = true; //recalculate moves
				}
			}

			//moveing and timming to help AI act more human and not so 'random'
			if( doNextMove <= 0 || bDoNextMove )//change move
			{
				doNextMove = 300 + rand()%500;
				bDoNextMove = true;
			}
			doNextMove--;

			//if we cant go fowards or backwards, go left or right:
			if( !moveDirection[3] && !moveDirection[2])
			{
				if( bLocked )//dont hcnage direction
				{
					leftRightDirection = rand()%2; //number between 0 and 1
				}
				bLocked = true;
			}
			else
			{
				if( bDoNextMove ) //do a new move
				{
					if( bLocked )//have we become unstuck?
					{
						//leftRightDirection = -1; //lets not rotate any more
						moveDirection[0] = false;
						moveDirection[1] = false; //disable these moves
					}
					bLocked = false;

					bool legitMove = false; //can we do this

					do
					{
					int direction = -1 + rand()%3;  

					switch( direction )
					{
					case -1:
						fowardBackDirection = -1;
						legitMove = true;
						break;
					case 0:
						if( moveDirection[2] )//if legit move
						{
							fowardBackDirection = 0;
							legitMove = true;
						}
						break;
					case 1:
						if( moveDirection[3] ) 
						{
							fowardBackDirection = 1;
							legitMove = true;
						}
						break;
					default:
						fowardBackDirection = -1;
						legitMove = true;
						Error("wtf! - is this a legit foward backward move?");
					}
					}while( !legitMove );
				}
			}

			//now pick a direction from those available
			//go left or right
			if( moveDirection[0] && moveDirection[1] )
			{
				if( bDoNextMove ) //do a new move
				{

					bool legitMove = false; //can we do this

					do
					{
					int direction = -1 + rand()%4; //not much chnace of turning, 0, and 1 are the only directions  

					switch( direction )
					{
					case -1:
						leftRightDirection = -1;
						legitMove = true;
						break;
					case 0:
						if( moveDirection[0] )//if legit move
						{
							leftRightDirection = 0;
							legitMove = true;
						}
						break;
					case 1:
						if( moveDirection[1] ) 
						{
							leftRightDirection = 1;
							legitMove = true;
						}
						break;
					default:
						leftRightDirection = -1;
						legitMove = true;
						//Error("wtf! - is this a legit left right rotation?");
					}
					}while( !legitMove );
				}
			}
			else
			{
				leftRightDirection = -1; //we have become unlocked so lets not rotate for a while
			}

		//determine players locations(closest player/angle to our barrel and rotate barrel
		if( enemyIndex >= 0 && ( allPlayers[enemyIndex]->IsDead() ||  !allPlayers[enemyIndex]->bIsInGame ))
			enemyIndex = -1;

		int safteyCatch = 20;
		if( enemyIndex == -1 )
		{
			do{
				enemyIndex = rand()%(noPlayers);
				safteyCatch--;
			}while( (allPlayers[enemyIndex]->IsDead() || !allPlayers[enemyIndex]->bIsInGame ) && safteyCatch > 0);

		}

		D3DXVECTOR3 enemy;

		if( safteyCatch > 0 )
		{
			enemy = (vPosition - allPlayers[enemyIndex]->vPosition); //vector from me to enemy
		}

		D3DXVECTOR3 direction; //vector showing my barrel

		direction.x = vPosition.x - (vPosition.x + (float)(10 * sin( barrel.yRot + yRot ))/2);
		direction.z = vPosition.z - (vPosition.z + (float)(10 * cos( barrel.yRot + yRot ))/2);

		//normalize vectors -- does this do anything?!
		float length = sqrt(enemy.x * enemy.x + enemy.y * enemy.y + enemy.z * enemy.z);
		enemy.x /= length;
		enemy.y /= length;
		enemy.z /= length;

		length = sqrt(direction.x * direction.x + direction.y * direction.y + direction.z * direction.z);
		direction.x /= length;
		direction.y /= length;
		direction.z /= length;



		//now we have to vectors, angle between them is:
		//float angle = acos( ((direction.x * enemy.x) + (direction.y * enemy.y) + (direction.z * enemy.z)) / 
		//	(sqrt(direction.x*direction.x + direction.y*direction.y + direction.z*direction.z) *
		//	sqrt(enemy.x*enemy.x + enemy.y*enemy.y + enemy.z*enemy.z)));

		float angle = acos( ((direction.x * enemy.x) + (direction.z * enemy.z)) / 
			(sqrt(direction.x*direction.x + direction.z*direction.z) *
			sqrt(enemy.x*enemy.x + enemy.z*enemy.z)));

		if( safteyCatch <= 0 )
			angle = 0;

			
			if( angle >= radian(5) ) //is positive
				barrelDirection = 0;
			else if( angle < radian(-5) )
				barrelDirection = 1;
			else
				barrelDirection = -1;

			if( angle < radian(5) && angle > radian(-5) )
			{
				bFire = true;
				barrelDirection = -1;
			}

			//bFire = true;


		//make the movement stuff here
		long int newTime = CTime::GetTime();
		float moveSpeed = (newTime - lastTime)*speed;
		lastTime = newTime;

		D3DXVECTOR3 newPosition(vPosition);

		if( leftRightDirection == 0 )
		{
			yRot -= radian((float)rotation);
		}
		else if( leftRightDirection == 1 )
		{
			yRot += radian((float)rotation);
		}

		if( barrelDirection == 1)
		{
			barrel.yRot += radian((float)barrelrotation);
		}
		else if( barrelDirection == 0)
		{
			barrel.yRot -= radian((float)barrelrotation);
		}


		if( fowardBackDirection == 0)//foward
		{
			newPosition.x += (float)(moveSpeed * sin( yRot ))/2;
			newPosition.z += (float)(moveSpeed * cos( yRot ))/2;
		}
		else if( fowardBackDirection == 1)//backward
		{
			newPosition.x -= (float)(moveSpeed * sin( yRot ))/2;
			newPosition.z -= (float)(moveSpeed * cos( yRot ))/2;
		}

		//check to amke sure we are in the map
		if( newPosition.x > mapBorderSize || newPosition.x < -mapBorderSize )
		{
			doNextMove = 0;
			return;
		}

		if( newPosition.z > mapBorderSize || newPosition.z < -mapBorderSize )
		{
			doNextMove = 0;
			return;
		}

		//make sure we havent run into any one else
		for( i=0; i< noPlayers; i++)
		{
			if( ( allPlayers[i]->vPosition.x <= (newPosition.x + radius) ) &&
				( allPlayers[i]->vPosition.x >= (newPosition.x - radius) ) &&
				( allPlayers[i]->vPosition.z <= (newPosition.z + radius) ) &&
				( allPlayers[i]->vPosition.z >= (newPosition.z - radius) ) &&
				allPlayers[i] != this && allPlayers[i]->bIsInGame)
			{
				doNextMove = 0;
				return;
			}
		}

        vPosition = newPosition;	

		if( bFire && nextFireTime <= 0 )//should we fire
		{
			CProjectile* newProj = new CProjectile(this, &projectile, allPlayers, noPlayers);
			newProj->vPosition.y = 5;
			newProj->vPosition.x = vPosition.x + (float)(10 * sin( barrel.yRot + yRot ))/2;
			newProj->vPosition.z = vPosition.z + (float)(10 * cos( barrel.yRot + yRot ))/2;
			newProj->velocity.x = (float)(speed*5 * sin( barrel.yRot + yRot ))/2;
			newProj->velocity.z = (float)(speed*5 * cos( barrel.yRot + yRot ))/2;		

			world->AddObject((CObject*)newProj);

			nextFireTime = fireTime;
			fireSound->Play();
		}
	}

	
	void CPlayer::Update()
	{
		if(!bIsInGame)
			return;

		if( bIsExploding )
		{
			//long int newTime = CTime::GetTime();
			//int timeChange = newTime - shrapnelLastTime; 
			
			for(int i=0; i<4; i++)
			{
				shrapnel[i].vVelocity.x *= 0.95;
				shrapnel[i].vVelocity.z *= 0.95;
				shrapnel[i].vVelocity.y -= 0.01;

				//[a,b,c] * [i,j,k] = [
				//shrapnel[i].vVelocity.y = abs(shrapnel[i].vVelocity.y) - 1.3;//going down
			}
			//shrapnelLastTime = newTime;
		}

		CFrame::Update();
		nextFireTime--;
	}

	void CPlayer::Render()
	{
		if(!bIsInGame)
			return;

		//if( bIsExploding )
		//{
		/*
			for(int i=0; i<4; i++)
			{
				shrapnel[i].Render();
			}*/
		//}

		//if( bIsDead )
		//	return;

		CFrame::Render();
	}

	//return degrees as radians
	float CPlayer::radian(float degrees)
	{
		return degrees * DEG_RAD;
	}