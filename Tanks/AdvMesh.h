#ifndef _ADVMESH
#define _ADVMESH

#include <windows.h>
#include "Object3D.h"
#include "DirectX.h"
#include "Mesh.h"
// ************************************************
// ---------------- CAdvMesh ------------------------
// Advanced mesh... support bounding shpeheres and
// collision detection eventually.....
// This contains the mesh data, currenly loads
// X files.
// ************************************************
class CAdvMesh : public CMesh
{
public:
	void OnCollision( CObject3D* pObjCollision ); //what did we collide with?
	void SetOnCollisionFn( void (*OnCollisionFn)( CObject3D* owner, CObject3D* pObjCollision ) );
	void GetVertexData(BYTE** vertexData, long* noVertices);
	HRESULT CAdvMesh::LoadXFile( char* pstrPathName );
	//for collision detection, it gets the polys to detect with
	virtual OBJECT_TYPE GetType() const {return ADVMESH;}

	void (*OnCollisionFn)( CObject3D* owner, CObject3D* pObjCollision );
	float boundSphereRadius;
	float mass;
	CObject* colNext; //pointer for collision list
	CUSTOMVERTEX* vertexData; //pointer to vertex buffer
	BYTE* vertData; //this is the proper way!!!
};

#endif