#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "Dinput8.lib")
#pragma comment(lib, "Dxguid.lib")
#pragma comment(lib, "D3dx8.lib") 

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>
#include <DInput.h>

#include "DirectX.h"
#include "GUITextbox.h"
#include "DirectInput.h"
#include "DataManip.h"


CGUITextbox::CGUITextbox()
{
	CGUIImage::CGUIImage();

	font = 0;
	text = new char[256];
	text = ">";
	textImg = 0;

	srcText.top = 0;
	srcText.left = 0;
	srcText.bottom = 0;
	srcText.right = 0;

	maxChars = 10;

	bAcceptInput = true;
}

char* CGUITextbox::GetLastText()
{
	//can only return the text when
	// we are finished i.e when we loss
	//focus

	//this is DIFFERENT to GetText()!!!!

	if( focus == true)
		return NULL;

	char* returnText = text;
	text = ">";
	return lastText;
}

void CGUITextbox::Render()
{
	Update();

	CopySurfaceFast(&image, &srcRect, &g_pBackSurf, &realDestPoint);
	CopySurfaceFast(&textImg, &srcText, &g_pBackSurf, &realDestPoint);
}

void CGUITextbox::Update()
{

	CGUIImage::Update();

	//cheack to see if a key has been pressed,
	// if we have focus
	// then append the string, and update the picture as text
	if( focus == true && bAcceptInput )
		for(int i=0; i < 256; i++ )
		{
			int value = g_keyboardData.key[i];

			if( value == 2)
			{


				//special characters to manipulate string
				switch( i )
				{
				case DIK_RETURN:
					lastText = text;
					text = ">";
					this->focus = false;
					break;
				}

				//make sure if we append another string,
				//we do something special if it wont fit...
				if( (int)strlen(text) < maxChars )
					//theres gotta be a better way to do this?
					//should i even use DI??
					switch( i )
					{
					case DIK_A:
						text = (char*)StringCat(text,"a");
						break;
					case DIK_B:
						text = (char*)StringCat(text,"b");
						break;
					case DIK_C:
						text = (char*)StringCat(text,"c");
						break;
					case DIK_D:
						text = (char*)StringCat(text,"d");
						break;
					case DIK_E:
						text = (char*)StringCat(text,"e");
						break;
					case DIK_F:
						text = (char*)StringCat(text,"f");
						break;
					case DIK_G:
						text = (char*)StringCat(text,"g");
						break;
					case DIK_H:
						text = (char*)StringCat(text,"h");
						break;
					case DIK_I:
						text = (char*)StringCat(text,"i");
						break;
					case DIK_J:
						text = (char*)StringCat(text,"j");
						break;
					case DIK_K:
						text = (char*)StringCat(text,"k");
						break;
					case DIK_L:
						text = (char*)StringCat(text,"l");
						break;
					case DIK_M:
						text = (char*)StringCat(text,"m");
						break;
					case DIK_N:
						text = (char*)StringCat(text,"n");
						break;
					case DIK_O:
						text = (char*)StringCat(text,"o");
						break;
					case DIK_P:
						text = (char*)StringCat(text,"p");
						break;
					case DIK_Q:
						text = (char*)StringCat(text,"q");
						break;
					case DIK_R:
						text = (char*)StringCat(text,"r");
						break;
					case DIK_S:
						text = (char*)StringCat(text,"s");
						break;
					case DIK_T:
						text = (char*)StringCat(text,"t");
						break;
					case DIK_U:
						text = StringCat(text,"u");
						break;
					case DIK_V:
						text = (char*)StringCat(text,"v");
						break;
					case DIK_W:
						text = (char*)StringCat(text,"w");
						break;
					case DIK_X:
						text = (char*)StringCat(text,"x");
						break;
					case DIK_Y:
						text = (char*)StringCat(text,"y");
						break;
					case DIK_Z:
						text = (char*)StringCat(text,"z");
						break;
					case DIK_SPACE:
						text = (char*)StringCat(text," ");
						break;
					}
			}
		}

	textImg = font->PrintText(text, 0,0, -1).textSurf;
	//textImg = (LPDIRECT3DSURFACE8)font->GetLastTextAsImage();

	if( textImg != 0 )
	{
		D3DSURFACE_DESC desc;
		textImg->GetDesc( &desc );
	
		srcText.top = 0;
		srcText.left = 0;
		srcText.bottom = desc.Height;
		srcText.right = desc.Width;
	}
}

void CGUITextbox::OnMouseClick()
{
	//CGUIImage::OnMouseClick();

	focus = true;
}

void CGUITextbox::OnMouseOut()
{

}

void CGUITextbox::OnMouseOver()
{

}






