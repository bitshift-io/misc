#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>

#include "DirectX.h"
#include "Camera.h"
#include "Object3D.h"


CCamera::CCamera()
{
	up		= D3DXVECTOR3( 0.0f, 1.0f, 0.0f ); // y
	lookAt = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
	vPosition = D3DXVECTOR3( 0.0f, 0.0f,-1.0f);
}

void CCamera::Render()
{

}

void CCamera::Update()
{
	if( CallbackFn != 0 )
		CallbackFn( (CObject3D*)this );

	// Update the position by the velocity
	vPosition.x += vVelocity.x;
	vPosition.y += vVelocity.y;
	vPosition.z += vVelocity.z;

	// Update rotations
	xRot += xRotUpdate;
	yRot += yRotUpdate;
	zRot += zRotUpdate;

	//to encapsualte the below in a camera class

	    // For the projection matrix, we set up a perspective transform (which
    // transforms geometry from 3D view space to 2D viewport space, with
    // a perspective divide making objects smaller in the distance). To build
    // a perpsective transform, we need the field of view (1/4 pi is common),
    // the aspect ratio, and the near and far clipping planes (which define at
    // what distances geometry should be no longer be rendered).
    D3DXMATRIX matProj;
	D3DXMATRIX matView;
	D3DXMatrixLookAtLH( &matView,&vPosition,&lookAt,&up);
	g_pd3dDevice->SetTransform( D3DTS_VIEW, &matView );

    D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, 1.4f, 1.0f, 10000.0f );
    g_pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );

	// Set the view matrix
	//g_pd3dDevice->SetTransform( D3DTS_VIEW, &mView );
}

void CCamera::Remove()
{
	//clean up code, removeing is killing, if u want
	//to change parents dont call this! call SetParent
	CallbackFn = 0;
	next = 0;
	parent = 0;

	delete this;
}
