#ifndef _PROJECTILE
#define _PROJECTILE

#include "Player.h"
#include "AdvMesh.h"

//this class gets added to the world when fired, 
//it deltes it self once it hits something/bounds
class CProjectile : public CAdvMesh
{
public:
	CPlayer* playerOwner;
	CPlayer** allPlayers;
	int noPlayers;

	D3DXVECTOR3 velocity;

	int damage;
	int radius;

	virtual OBJECT_TYPE GetType() const;
	CProjectile(CPlayer* playerOwner, CAdvMesh* bullet, CPlayer* players[], int numPlayers);
	void Update();

private:
	long int lastTime;
};


/*
//this class gets added to the world when fired, 
//it deltes it self once it hits something/bounds
class CProjectile : public CAdvMesh
{
public:
	CPlayer* playerOwner;
	CPlayer* allPlayers;
	int noPlayers;

	int damage;
	int radius;
*/

	
//};

#endif