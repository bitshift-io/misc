
//


//-----------------------------------------------------------------------------
//  Fabian Mathews
//	Engine
//-----------------------------------------------------------------------------

#pragma comment(lib, "d3d8.lib")
#pragma comment(lib, "Dinput8.lib")
#pragma comment(lib, "Dxguid.lib")
#pragma comment(lib, "D3dx8.lib")   

#include <d3d8.h>
#include <D3dx8tex.h>
#include <D3d8types.h>
#include <math.h>

#define PI 3.1415

//-----------------------------------------------------------------------------
// Nesisary Globals
//-----------------------------------------------------------------------------





//-----------------------------------------------------------------------------
// Custom Include files
//-----------------------------------------------------------------------------
#include "Tanks.h"
#include "Engine.h"
#include "Projectile.h"
#include "Player.h"

//#include "Player.cpp"
//#include "Projectile.cpp"



/*
//paths to data stuffs
char* g_meshPath;
char* g_meshTexturePath; 
char* g_texturesPath;
char* g_soundsPath;
char* g_dataPath;
char* g_imageDumpsPath;
*/
/*
int g_deviceWidth = 640;
int g_deviceHeight = 480;
bool g_fullScreen = true;
bool g_screenSaver = false; //make it so all keys quit

//-----------------------------------------------------------------------------
// Custom Nesisary Globals
//-----------------------------------------------------------------------------

bool bInMenu = true;
bool bDisplayScore = false;
bool bInGame = false;
bool bEndGame = false;
const int mapBorderSize = 430;*/

//-----------------------------------------------------------------------------
// Custom Classes/Structs
//---------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Custom Globals
//-----------------------------------------------------------------------------
bool bInMenu = true;
bool bDisplayScore = false;
bool bRegetScores = true; //set this so we we get scores
bool bInGame = false;
bool bEndGame = false;
bool bShakeCamera = false;
const int mapBorderSize = 430;

FNT* playerScores[2][8]; //static function an array of stuff, for displaying fonts faster :)

//CINI g_ini("data/config.ini");
static CTime g_timer;
CFont *g_pFont = NULL;


/// GAME CODE ///////////
int cameraTimer = 0;


CFrame g_world;

CLayer menu;
CLayer controls;
CLayer scoreboard;
CLayer background; //this is the background for both the menu and the scoreboard
CLayer options;
CLayer *playerOptions[3]; //only up to 3 humans

CGUIImage play;


CAdvMesh map;
int const MAXPLAYERS = 8;
int noPlayers = 8;
int noHumanPlayers = 0; //gets changed to 1 before play
int noAIPlayers = 3; //gets increased by 1
int lastLivePlayer; //index of players array of the last surviving player
CPlayer* players[8]; //up to 8 players including AI

/*
CAdvMesh g_skybox;
CAdvMesh g_plane;
CAdvMesh g_ball6;

CAdvMesh g_ball1;
CAdvMesh g_ball2;
CAdvMesh g_ball3;
CAdvMesh g_ball4;
CAdvMesh g_ball5;
CMesh g_3dsmodel;

CSprite* g_sprite;
CParticle* g_particleEmitter;
CGUITextbox g_textbox;
*/


CLayer g_menu;
CGUIImage g_restart;
CImage* g_logo = new CImage();
CImage* g_mouse = new CImage();

/*
bool g_newScene = true;
float g_timmer = 0;
*/

//-----------------------------------------------------------------------------
// Other Custom Include files
//-----------------------------------------------------------------------------


void PlayOver( CImage* owner )
{
		RECT rect = {0,50,100,100};
		owner->SetSourceRect( &rect );
}

void PlayOut( CImage* owner )
{
		RECT rect = {0,0,100,50};
		owner->SetSourceRect( &rect );
}

void PlayClick( CImage* owner )
{
	int i;

	bInMenu = false;
	bDisplayScore = false;
	bInGame = true;

	//reset the game
	for(i=0; i < MAXPLAYERS; i++)
	{
		players[i]->Reset();
		players[i]->bIsInGame = false;
	}

	for(i=0; i < noHumanPlayers; i++)
	{
			players[i]->SetAI(false,i);
			players[i]->Reset();
			players[i]->bIsInGame = true;
			//players[i]->bIsAI = false;
	}
	for(; i < MAXPLAYERS; i++)
	{
		if( (i - noHumanPlayers) < noAIPlayers )
		{
			players[i]->SetAI(true,i);
			players[i]->Reset();
			players[i]->bIsInGame = true;
			//players[i]->bIsAI = true;
		}
	}
}

void OptionsOver( CImage* owner )
{
		RECT rect = {0,30,80,60};
		owner->SetSourceRect( &rect );
}

void OptionsOut( CImage* owner )
{
		RECT rect = {0,0,80,30};
		owner->SetSourceRect( &rect );
}

void OptionsClick( CImage* owner )
{
	options.SetVisible(true);
}

void HumansOver( CImage* owner )
{
}

void HumansOut( CImage* owner )
{
}

void HumansClick( CImage* owner )
{
	noHumanPlayers++;

	if( noHumanPlayers > 3 || (noAIPlayers+ noHumanPlayers) > MAXPLAYERS)
		noHumanPlayers = 0;

	RECT rect = {0,( noHumanPlayers*50 ),150,( noHumanPlayers*50 + 50 )};
	owner->SetSourceRect( &rect );
}

void AIOver( CImage* owner )
{
}

void AIOut( CImage* owner )
{
}

void AIClick( CImage* owner )
{
	noAIPlayers++;

	if( noAIPlayers > 8 || (noAIPlayers+ noHumanPlayers) > MAXPLAYERS)
		noAIPlayers = 0;

	RECT rect = {0,( noAIPlayers*50 ),150,( noAIPlayers*50 + 50 )};
	owner->SetSourceRect( &rect );
}

void Camera1( CObject3D* owner)
{
	static int time = 0;
	static int lookingAt;

	static long int shakeTime = 0;
	static bool bShaking = false;
	static D3DXVECTOR3 position; //used for camera shake

	//lock on 2 winning player
	if( bEndGame ) 
	{
		float theta = (CTime::GetTime()/D3DX_PI)/2000;
		owner->vPosition.x = (float)(400 * cos( theta ));
		owner->vPosition.z = (float)(100 * sin( theta ));
		owner->vPosition.y = 150;

		if( lookingAt == -1)
		{
			int safetyBreak = 20;

			do{
				lookingAt = rand()%noPlayers;
				safetyBreak--;
			}while( players[lookingAt]->IsDead() && !players[lookingAt]->bIsInGame && safetyBreak > 0 );

			if( safetyBreak > 0 )
			{
				((CCamera*)owner)->lookAt = players[lookingAt]->vPosition;
			}
			else
			{
					((CCamera*)owner)->lookAt = g_world.vPosition;
					((CCamera*)owner)->lookAt.x += 50;
					((CCamera*)owner)->lookAt.y += 5;
			}
		}
	}
	else if( bInMenu )
	{		
		time--;

		float theta = (CTime::GetTime()/D3DX_PI)/2000;
		owner->vPosition.x = (float)(400 * cos( theta ));
		owner->vPosition.z = (float)(100 * sin( theta ));
		owner->vPosition.y = 150;

		if( time <= 0 )
		{
			int safetyBreak = 20;
			do{
				lookingAt = rand()%noPlayers;
				safetyBreak--;
			}while( players[lookingAt]->IsDead() && !players[lookingAt]->bIsInGame && safetyBreak > 0 );
			
            if( safetyBreak > 0 )
			{
				((CCamera*)owner)->lookAt = players[lookingAt]->vPosition;
			}
			else
			{
					((CCamera*)owner)->lookAt = g_world.vPosition;
					((CCamera*)owner)->lookAt.x += 50;
					((CCamera*)owner)->lookAt.y += 5;
			}

			time = 1000 + rand()%1000;
		}
	}
	else
	{
		((CCamera*)owner)->lookAt = g_world.vPosition;
		((CCamera*)owner)->lookAt.x += 50;
		((CCamera*)owner)->lookAt.y += 5;
		owner->vPosition = D3DXVECTOR3(650,950,50);

		lookingAt = -1;
		time = 0;
	}

	//shake camera :)
	if( bShakeCamera )
	{
		shakeTime = CTime::GetTime() + 500; //half a second of shake when some 1 dies
		bShakeCamera = false;
		bShaking = true;
		position = owner->vPosition;
	}

	if( bShaking )
	{
		if( CTime::GetTime() >= shakeTime )
			bShaking = false;

		//do shaking here
		owner->vPosition.x = position.x + (rand()%8 - 4);
		owner->vPosition.y = position.y + (rand()%8 - 4);
		owner->vPosition.z = position.z + (rand()%8 - 4);
	}
}

//-----------------------------------------------------------------------------
// Name: Init()
//-----------------------------------------------------------------------------
//HRESULT Init()
void Init(CEngine* pEngine)
{
	char *retValue = NULL;

	//read important device settings
	g_ini.SetSection("system");
	retValue = g_ini.ReadKey("fullScreen");
	g_fullScreen = (bool)StringToNumber(retValue);
	retValue = g_ini.ReadKey("deviceWidth");
	g_deviceWidth = (int)StringToNumber(retValue);
	retValue = g_ini.ReadKey("deviceHeight");
	g_deviceHeight = (int)StringToNumber(retValue);

	// read in the paths, if not found, should have defaults?
	g_ini.SetSection("paths");
	g_meshPath = g_ini.ReadKey("meshes");
	g_meshTexturePath = g_ini.ReadKey("meshTextures");
	g_texturesPath = g_ini.ReadKey("textures");
	g_soundsPath = g_ini.ReadKey("sounds");		
	g_imageDumpsPath =  g_ini.ReadKey("imageDumps");

	//InitD3D();	
	pEngine->Init();
	pEngine->Set3DAPI( 1 );
	g_pd3dDevice->SetRenderState( D3DRS_AMBIENT, 0xffffffff );

	//load font
	g_pFont = new CFont("FontSmall.bmp",10,10);	

	map.LoadXFile("map.x");
	g_world.AddObject((CObject*)&map);

	CLight *light = new CLight(); //light not working :?
	light->vPosition = D3DXVECTOR3(30,30,30);
	light->lookAt = map.vPosition;
	light->SetLightType( D3DLIGHT_POINT );
	g_world.AddObject((CObject*)light);

	for(int i=0; i<noPlayers; i++)
	{
		players[i] = new CPlayer(players, noPlayers);
		players[i]->Init(&g_world);
		g_world.AddObject((CObject*)players[i]);
	}

	//set the camera position and call back
	g_camera.SetCallbackFn( Camera1 );


	////////////// NOW FOR THE MENU STUFF //////////////////////////////
	RECT* rect = new RECT;
	POINT* point = new POINT;

	controls.SetVisible(bInMenu);
	menu.SetVisible(bInMenu);
	scoreboard.SetVisible(bDisplayScore);

	if( bInMenu || bDisplayScore )
		background.SetVisible(true);
	else
		background.SetVisible(false);

	//MENU buttons
	play.LoadImage("play.bmp");
	rect = new RECT;
	rect->top = 0;
	rect->left = 0;
	rect->bottom = 50;
	rect->right = 100;
	play.SetSourceRect( rect );
	play.SetMouseZone(NULL);
	point = new POINT;
	
	point->x = g_deviceWidth - ((play.width/2)+(g_deviceWidth*0.15)/2);
	point->y = 20;	
	play.absolute = true;
	play.SetDestPoint( point );
	play.MouseClickFn = PlayClick;
	play.MouseOutFn = PlayOut;
	play.MouseOverFn = PlayOver;
	menu.AddObject((CObject*)&play);

	/*
	//do we need this button?
	CGUIImage *optionsBtn = new CGUIImage();
	optionsBtn->LoadImage("options.bmp");
	rect = new RECT;
	rect->top = 0;
	rect->left = 0;
	rect->bottom = 30;
	rect->right = 80;
	optionsBtn->SetSourceRect( rect );
	optionsBtn->SetMouseZone(NULL);
	point = new POINT;
	
	point->x = g_deviceWidth - ((play.width/2)+(g_deviceWidth*0.15)/2);
	point->y = 150;	
	optionsBtn->absolute = true;
	optionsBtn->SetDestPoint( point );
	optionsBtn->MouseClickFn = OptionsClick;
	optionsBtn->MouseOutFn = OptionsOut;
	optionsBtn->MouseOverFn = OptionsOver;
	menu.AddObject((CObject*)optionsBtn);
	
	options.SetVisible(bInMenu);
	menu.AddObject((CObject*)&options);*/

	//options menu settings:
	
	CGUIImage *noHumans = new CGUIImage();
	noHumans->LoadImage("human.bmp");
	rect = new RECT;
	rect->top = 0;
	rect->left = 0;
	rect->bottom = 50;
	rect->right = 150;
	noHumans->SetSourceRect( rect );
	noHumans->SetMouseZone(NULL);
	point = new POINT;
	
	point->x = 10;
	point->y = 50;	
	noHumans->absolute = true;
	noHumans->SetDestPoint( point );
	noHumans->MouseClickFn = HumansClick;
	noHumans->MouseOutFn = HumansOut;
	noHumans->MouseOverFn = HumansOver;
	HumansClick(noHumans);//call it so its all set properly
	menu.AddObject((CObject*)noHumans);
	//options.AddObject((CObject*)noHumans);

	CGUIImage *noAI = new CGUIImage();
	noAI->LoadImage("bot.bmp");
	rect = new RECT;
	rect->top = 0;
	rect->left = 0;
	rect->bottom = 50;
	rect->right = 150;
	noAI->SetSourceRect( rect );
	noAI->SetMouseZone(NULL);
	point = new POINT;
	
	point->x = 10;
	point->y = 120;	
	noAI->absolute = true;
	noAI->SetDestPoint( point );
	noAI->MouseClickFn = AIClick;
	noAI->MouseOutFn = AIOut;
	noAI->MouseOverFn = AIOver;
	AIClick(noAI);//call it so its all set properly
	menu.AddObject((CObject*)noAI);
	//options.AddObject((CObject*)noAI);

	//keyboard with controls
	point = new POINT;
	CImage *kboard = new CImage();
	kboard->LoadImage("keyboard.bmp");
	//kboard->StretchImage(g_deviceWidth, (g_deviceHeight*0.1267));
	point->x = 10;
	point->y = g_deviceHeight - (g_deviceHeight*0.1267 + 160);
	kboard->SetDestPoint(point);
	controls.AddObject((CObject*)kboard);


	point = new POINT;
	CImage *right = new CImage();
	right->LoadImage("right.bmp");
	right->StretchImage((g_deviceWidth*0.15), (g_deviceHeight*0.8733));
	point->x = g_deviceWidth - (g_deviceWidth*0.15);
	point->y = 0;
	right->SetDestPoint(point);
	background.AddObject((CObject*)right);

	point = new POINT;
	CImage *bottomLeft = new CImage();
	bottomLeft->LoadImage("bottom.bmp");
	bottomLeft->StretchImage(g_deviceWidth, (g_deviceHeight*0.1267));
	point->x = 0;
	point->y = g_deviceHeight - (g_deviceHeight*0.1267);
	bottomLeft->SetDestPoint(point);
	background.AddObject((CObject*)bottomLeft);

	g_mouse->LoadImage("cursor.bmp");
	point = new POINT;
	point->x = 0;
	point->y = 0;
	g_mouse->SetDestPoint(point);
	g_mouse->SetAbsolute(true);
	g_mouseData.height = g_mouse->height;
	g_mouseData.width = g_mouse->width;
	menu.AddObject((CObject*)g_mouse);

	point = new POINT;
	point->x = g_deviceWidth - 500;
	point->y = g_deviceHeight - 50;
	menu.SetDestPoint( point );

	for( i=0; i< 8; i++)
	{
		playerScores[0][i] = NULL;
		playerScores[1][i] = NULL;
	}


	// WE MUST SET THE MASTER FRAME
	// FOR COLLISION DETECTION TO WORK!!!!
	g_world.masterFrame = true;
}

//-----------------------------------------------------------------------------
// Name: Cleanup()
// Desc: Releases all previously initialized objects
//-----------------------------------------------------------------------------
void Cleanup(CEngine* pEngine)
{
	g_world.Remove();

	ShutdownD3D();
}

//-----------------------------------------------------------------------------
// Name: DoMenu()
// Desc: Draws the menu
//-----------------------------------------------------------------------------
bool DoMenu(CEngine *pEngine)
{
	menu.SetVisible(true);
	background.SetVisible(true);
	//update mouse position
	POINT mousePos = {g_mouseData.x, g_mouseData.y};
	g_mouse->SetDestPoint(&mousePos);

	background.Render();
	menu.Render();
	controls.Render();

	return true;
}

//-----------------------------------------------------------------------------
// Name: DisplayScore
// Desc: Draws the scoreboard at end of round
//-----------------------------------------------------------------------------
bool DisplayScore(CEngine *pEngine)
{
	int i;
	int score;
	
	scoreboard.SetVisible(true);
	background.SetVisible(true);

	if(bRegetScores == true)
	{
		for(i=0; i<noPlayers; i++)
		{
			if( players[i]->bIsInGame )
			{
				if( playerScores[0][i] != NULL )
					delete playerScores[0][i];

				if( playerScores[1][i] != NULL )
					delete playerScores[1][i];

				playerScores[0][i] = new FNT();
				playerScores[1][i] = new FNT();

				score = players[i]->GetScore();

				char *charScore = NumberToString((float)score);
				memcpy((void*)playerScores[0][i], (void*)&g_pFont->PrintText( players[i]->GetName(),g_deviceWidth - 100,50*i,-1), sizeof(FNT));
				memcpy((void*)playerScores[1][i], (void*)&g_pFont->PrintText( charScore,g_deviceWidth - 100,50*i + 20,-1), sizeof(FNT));
			}
		}

		bRegetScores = false; //only do this once
	}

	background.Render();
	scoreboard.Render();

	//render socres etc...
	for(i=0; i<noPlayers; i++)
	{
		if( players[i]->bIsInGame && playerScores[0][i] != NULL && playerScores[0][i]->textSurf != NULL && playerScores[1][i]->textSurf != NULL)
		{
			CopySurfaceFast(&playerScores[0][i]->textSurf, playerScores[0][i]->srcRect,&g_pBackSurf, playerScores[0][i]->dest);
			CopySurfaceFast(&playerScores[1][i]->textSurf, playerScores[1][i]->srcRect,&g_pBackSurf, playerScores[1][i]->dest);
		}
	}

	return true;
}

//-----------------------------------------------------------------------------
// Name: DoGame
// Desc: Does all things here game related :)
//-----------------------------------------------------------------------------
bool DoGame(CEngine *pEngine)
{
	srand(CTime::GetTime()); //randomize averything ;P

	//check for dead players
	int noDeadPlayers = 0;

	for(int i=0; i<noPlayers; i++)
	{
		if( players[i]->IsDead() || !players[i]->bIsInGame)
			noDeadPlayers++;
		else
			lastLivePlayer = i;
	}

	if( (noDeadPlayers + 1) >= noPlayers )
	{
		bEndGame = true;
		bDisplayScore = true;
		bInMenu = false;
	}

	//let players do thier keys with direct input
	for(i=0; i<noPlayers; i++)
	{
		players[i]->CheckKey();
	}

	return true;
}

//-----------------------------------------------------------------------------
// Name: GameLoop()
// Desc: Draws the scene
//-----------------------------------------------------------------------------
bool GameLoop(CEngine *pEngine)
{
	int frames = g_timer.FrameCount();
	if( frames != 0 )
	{
		char *frameCount = NumberToString((float)frames);
		g_pFont->PrintText( frameCount,10,10,CLOCKS_PER_SEC);
	}

    g_pd3dDevice->Clear( 0, NULL, (D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER), 
		D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

    // Begin the scene
    g_pd3dDevice->BeginScene();

	g_camera.Update();
	g_world.Render();

	//game mode has 2 states
	if( bInMenu )
	{
		bRegetScores = true;
		DoMenu(pEngine);
	}
	else if( bDisplayScore )
		DisplayScore(pEngine);
	else if( bInGame )
	{
		bRegetScores = true;
		DoGame(pEngine);
	}

	g_pFont->Render();


	    // End the scene
    g_pd3dDevice->EndScene();	
	
	    // End the scene
    //g_pd3dDevice->EndScene();

    // Present the backbuffer contents to the display
    g_pd3dDevice->Present( NULL, NULL, NULL, NULL );

	return true;
}

//-----------------------------------------------------------------------------
// Name: CheckInput()
// Desc: checks all game inputs
//-----------------------------------------------------------------------------
void CheckInput()
{
	static bool bEscPressed = false;
	static bool bEnterPressed = false;

	if( GetAsyncKeyState(VK_ESCAPE))
	{
		if( !bEscPressed )
		{
			bEscPressed = true;
			return;
		}

		//if( !bDisplayScore )
		//{
		//	bDisplayScore = true;
		//	bInMenu = false;
		//}
		if( bInGame == true )
		{
			bDisplayScore = true;
			bInMenu = false;
		}
		else if( !bInMenu &&  bDisplayScore)
		{
			bInMenu = true;
			bDisplayScore = false;
		}
		else if( bInMenu )
			PostQuitMessage(0);

		bDisplayScore = true;
		bInGame = false;
		//
		//PostMessage(g_hWnd, WM_QUIT, 0, 0);
	}
	else
		bEscPressed = false;

	if( GetAsyncKeyState(VK_RETURN) )
	{
		if( !bEnterPressed )
		{
			bEnterPressed = true;
			return;
		}

		if( bInMenu || bDisplayScore )
		{
			bInMenu = false;
			bDisplayScore = false;
			bInGame = true;

			if( bEndGame )
			{
				bEndGame = false;
				for(int i=0; i<noPlayers; i++)
				{
					players[i]->Reset();
				}
			}
		}
	}
	else
		bEnterPressed = false;
}

//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: The application's entry point
//-----------------------------------------------------------------------------
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR, INT )
{
	CEngine* game = new CEngine( hInst );

	game->MainLoopCallbk = GameLoop;	

	Init( game );

	game->MainLoop();
	
	game->Shutdown();

    Cleanup( game );
    
    return 0;
}
