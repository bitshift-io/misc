

#include <time.h>
#include "Timming.h"


CTime::CTime()
{
	current = clock();
	frameCount = 0;
	secondCounter = 0;
}

char* CTime::GetClockTime()
{
	char* sysTime;
	_strtime( sysTime );

	return sysTime;
}

char* CTime::GetDate()
{
	char* sysDate;
	_strdate( sysDate );

	return sysDate;
}

long CTime::GetTime()
{
	return clock();
}

int CTime::FrameCount()
{
	int returnFrame = 0;

	frameCount++;

	current = clock();

	if(current >= secondCounter + CLOCKS_PER_SEC)
	{
		returnFrame = frameCount;
		secondCounter = clock();
		frameCount = 0;
	}

	return returnFrame;
}
