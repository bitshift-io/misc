#ifndef _MESH
#define _MESH

// ************************************************
// ---------------- CMesh ------------------------
// This contains the mesh data, currenly loads
// X files.
// ************************************************
#include "3DSFormat.h"
#include "Object3D.h"

class CMesh : public CObject3D
{
public:
	void Remove();
	HRESULT LoadXFile( char* pstrPathName );	//Loads a mesh from a file
	bool Load3DSFile( char* pstrPathName );
	void Render();							// Renders the mesh
	unsigned int GetSize(){ return sizeof( *this ); }
	virtual OBJECT_TYPE GetType() const {return MESH;}

	DWORD numMaterials;
	LPD3DXMESH pMesh;
	D3DMATERIAL8* pMaterials;
	LPDIRECT3DTEXTURE8* pTextures;

private:

};

#endif



