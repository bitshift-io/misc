#ifndef _COLLISIONDETECTION
#define _COLLISIONDETECTION

#include "AdvMesh.h"

//vertices list:
void CheckAllCollisions(  CObject* meshList );
bool CheckCollision( CAdvMesh* pObj1, CAdvMesh* pObj2 );
bool CheckPolyCollision( CAdvMesh* pObj1, CAdvMesh* pObj2 );
bool CheckBoundsCollision( CAdvMesh* pObj1, CAdvMesh* pObj2 );


#endif
