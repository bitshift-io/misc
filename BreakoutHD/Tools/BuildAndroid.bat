REM
REM This file is used to build android project manually so i can have a clean folder layout
REM
REM
REM http://benlynn.blogspot.com/2009/01/developing-android-without-eclipse-or.html
REM

set PROJECT=SkiTeam
set SDKDIR=D:\BlackCarbon\SDK\android-sdk\platforms\android-1.5
set APKDIR=D:\BlackCarbon\SDK\android-sdk\tools
set PATH=%PATH%;%SDKDIR%/tools;%APKDIR%

cd ..


REM copy data to android data folder

md %CD%\Binary\Android\Data\raw
copy %CD%\Binary\Data %CD%\Binary\Android\Data\raw




REM Generate R.java, move it to a good location, delete garbage folder left behind

md %CD%\Source\SkiTeam_Android\gen

aapt p -m -J Source/SkiTeam_Android/gen -M Project/Android/AndroidManifest.xml -S Binary/Android/Data -I %SDKDIR%/android.jar


move /y %CD%\Source\SkiTeam_Android\gen\SkiTeam\Android\R.java %CD%\Source\SkiTeam_Android
rd /s /q %CD%\Source\SkiTeam_Android\gen




REM Compile resources

aapt p -f -M Project/Android/AndroidManifest.xml -S Binary/Android/Data -I %SDKDIR%/android.jar -F Binary/%PROJECT%.ap_







REM Compile code

md %CD%\Binary\classes

javac -encoding ascii -target 1.5 -d Binary/classes -sourcepath Source -bootclasspath %SDKDIR%/android.jar Source/SkiTeam_Android/*.java




REM Convert to byte code

call dx --dex --verbose --debug --output=%CD%/Binary/classes.dex %CD%/Binary/classes





REM Combine resources with compiled code

call apkbuilder %CD%/Binary/%PROJECT%.apk -z  %CD%/Binary/%PROJECT%.ap_ -f %CD%/Binary/classes.dex -rf %CD%/Source




REM clean up the android mess

del %CD%\Binary\%PROJECT%.ap_
del %CD%\Binary\classes.dex
rd /s /q %CD%\Binary\classes
rd /s /q %CD%\Binary\Android\Data\raw

cd Tools

pause