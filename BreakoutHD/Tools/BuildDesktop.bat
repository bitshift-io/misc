REM
REM This file is used to build desktop project manually
REM

set PROJECT=SkiTeam

cd ..



REM Compile code

md %CD%\Binary\classes

javac -encoding ascii -target 1.5 -d Binary/classes -sourcepath Source Source/Desktop/*.java




REM Create uncompressed jar file, load times are important

jar cfve0 Binary/Desktop/%PROJECT%.jar Main -C Binary/classes .




REM Clean up left over mess

rd /s /q %CD%\Binary\classes



cd Tools
pause
