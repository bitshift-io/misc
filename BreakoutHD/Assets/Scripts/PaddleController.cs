using UnityEngine;
using System.Collections;

public class PaddleController : SplineFollow
{
	public float 		movementSpeed = 1.0f; // percent per second
	
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	public override void Update() 
	{
		float leftMovement = Input.GetKey("a") ? -1.0f : 0.0f;
		float rightMovement = Input.GetKey("d") ? 1.0f : 0.0f;
		percent += (leftMovement + rightMovement) * Time.deltaTime * movementSpeed;
		
		base.Update();
		
		// Get camera and change the percentage also to match this percentage
		GameObject camera = GameObject.Find("Camera");
		SplineFollow splineFollow = camera.GetComponent<SplineFollow>();
		splineFollow.percent = percent;
	}
}
