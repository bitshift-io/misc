
var movementSpeed = 1000.0;
var impactSound : AudioSource;

private var velocity;

function Start()
{
	velocity = Vector3(1, 1, 0);
	velocity.Normalize();
	velocity *= movementSpeed * Time.deltaTime;
	rigidbody.velocity = velocity;
	
	Debug.Log("Start velocity: " + rigidbody.velocity);
}
/*
function Update() 
{
	// maintain velocity
	velocity = rigidbody.velocity;
	velocity.Normalize();
	velocity *= movementSpeed * Time.deltaTime;
	rigidbody.velocity = velocity;
}*/

function OnCollisionEnter(collision : Collision)
{
/*
	// simply reflect the velocity
	var averageNormal = Vector3(0.0, 0.0, 0.0);
	for (var contact : ContactPoint in collision.contacts)
	{
		 Debug.DrawRay(contact.point, contact.normal, Color.white);
		 
		averageNormal += contact.normal;
	}
	
	averageNormal.Normalize();
	
	velocity = Vector3.Reflect(velocity, averageNormal);
	velocity.Normalize();
	velocity *= movementSpeed * Time.deltaTime;
	rigidbody.velocity = velocity;
	
	Debug.Log("Reflect velocity: " + velocity);
	*/
	
	// maintain velocity
	velocity = rigidbody.velocity;
	velocity.Normalize();
	velocity *= movementSpeed * Time.deltaTime;
	rigidbody.velocity = velocity;
	
	impactSound.Play();
}