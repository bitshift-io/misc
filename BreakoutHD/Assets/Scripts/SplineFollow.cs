using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SplineFollow : MonoBehaviour 
{
	public float 		percent = 0.0f;
	public iTweenPath 	path;

	// Use this for initialization
	void Start() 
	{
	
	}
	
	// Update is called once per frame
	public virtual void Update() 
	{
		percent = Mathf.Clamp(percent, 0.0f, 1.0f);
		Vector3 position = iTween.PointOnPath(path.nodes.ToArray(), percent);
		transform.localPosition = position;
	}
}
