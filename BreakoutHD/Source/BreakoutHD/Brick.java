package BreakoutHD;

import java.util.Vector;

import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Memory;
import BlackCarbon.API.RenderBuffer;
import BlackCarbon.API.XMLSerialize;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;
import BlackCarbon.Math.Math;

public class Brick extends Entity
{
	enum State
	{
		Invisible,
		Spawn,
		Moving,
		Dying,
		Dead,
	}
	
	@Override
	public void Render()
	{
		if (mState == State.Invisible || mState == State.Dead)
			return;
		
		// modify colour
		Device device = Factory.Get().GetDevice();
		Device.DeviceState currentState = device.GetDeviceState(); // do not modify directly, should i return a copy?
		Device.DeviceState newState = Memory.Stack.New(Device.DeviceState.class);
		newState.Copy(currentState);
		newState.colour.Set(1.f, 0.f, 0.f, 1.f);
		device.SetDeviceState(newState);
		
		mSprite.Render();
		
		// reset colour
		newState.colour.Set(1.f, 1.f, 1.f, 1.f);
		device.SetDeviceState(newState);
		
		if (Game.Get().IsDebug())
			mCollision.RenderDebug();
	}
	
	
	public boolean CanCollide()
	{
		if (mState == State.Moving)
			return true;
		
		return false;
	}
	
	@Override
	public void Update()
	{
		Memory.Stack.StackLevel stack = Memory.Stack.Push();
		
		// update movement, occurs for all states
		Vector4 velocityThisFrame = Memory.Stack.New(Vector4.class);
		Vector4 translation = mSprite.mTransform.GetTranslation();
		velocityThisFrame.Copy(mVelocity);
		velocityThisFrame = velocityThisFrame.Multiply(Game.Get().GetDeltaTime());		
		translation = translation.Add(velocityThisFrame);
		mSprite.mTransform.SetTranslation(translation);
		mCollision.SetTransform(mSprite.mTransform);
		
		switch (mState)
		{
		case Invisible:
			Update_Invisible();
			break;
			
		case Spawn:
			Update_Spawn();
			break;
			
		case Moving:
			Update_Moving();
			break;
			
		case Dying:
			Update_Dying();
			break;
			
		case Dead:
			break;
		}
		
		Memory.Stack.Pop(stack);
	}
	
	public void Update_Invisible()
	{
		// this state is when we are moving on to the playing area,
		// when we reach a certain point we go to the spawn state and become collidable
		Vector4 scale = Memory.Stack.New(Vector4.class);
		scale.Set(0.f, 0.f, 1.f, 0.f);
		mSprite.mTransform.SetScale(scale);
		
		float spawnHeight = 340.f; // TODO: move to data
		Vector4 translation = mSprite.mTransform.GetTranslation();
		if (translation.GetY() < spawnHeight)
		{
			mTimeInState = 0.f;
			mState = State.Spawn;
		}
	}
	
	public void Update_Spawn()
	{
		// scale in over time
		mTimeInState += Game.Get().GetDeltaTime();
		
		float scalePercent = Math.Clamp(0.f, mTimeInState / mSpawnStateTime, 1.f);
		Vector4 scale = Memory.Stack.New(Vector4.class);
		scale.Set(scalePercent, scalePercent, 1.f, 0.f);
		mSprite.mTransform.SetScale(scale);
		
		if (mTimeInState >= mSpawnStateTime)
		{
			mTimeInState = 0.f;
			mState = State.Moving;
		}
		
		if (mHealth <= 0.f)
			mState = State.Dying;
	}
	
	public void Update_Dying()
	{
		// scale out then remove
		mTimeInState += Game.Get().GetDeltaTime();
		
		float scalePercent = 1.f - Math.Clamp(0.f, mTimeInState / mSpawnStateTime, 1.f);
		Vector4 scale = Memory.Stack.New(Vector4.class);
		scale.Set(scalePercent, scalePercent, 1.f, 0.f);
		mSprite.mTransform.SetScale(scale);
		
		if (mTimeInState >= mSpawnStateTime)
		{
			mTimeInState = 0.f;
			mState = State.Dead;
		}
	}
	
	public void Update_Moving()
	{
		Vector<Laser> laserList = Game.Get().GetLevel().GetEntityList(Laser.class);
		Laser laser = laserList.get(0);
		float laserHeight = laser.mOffsetY; //-100.f;
	
		Vector4 translation = mSprite.mTransform.GetTranslation();
		if (translation.GetY() < laserHeight)
		{
			// reduce actors score
			mState = State.Dying;
			laser.TakeDamage(0.1f); // PUSH TO DATA
		}
		
		if (mHealth <= 0.f)
		{
			// increase actors score, drop any powerups
			mState = State.Dying;
		}
		
		// shoot
		Level level = Game.Get().GetLevel();
		int randShoot = level.mRandom.nextInt(1000);
		if (randShoot == 0)
		{
			Vector4 velocity = Memory.Stack.New(Vector4.class);
			velocity.Set(0.f, -100.f, 0.f, 0.f);
			Bullet bullet = (Bullet)level.LoadFromTemplate(mBulletName);
			bullet.SetPosition(mSprite.mTransform.GetTranslation());
			bullet.Launch(velocity);
			level.InsertEntity(bullet);
		}
	}
	
	public void SetTransform(Matrix4 transform)
	{
		Memory.Stack.Push();
		
		Vector4 translation = transform.GetTranslation();
		translation.SetZ(mOffsetZ);
		mSprite.mTransform = transform;
		mSprite.mTransform.SetTranslation(translation);
		mCollision.SetTransform(mSprite.mTransform);
		
		Memory.Stack.Pop();
	}
	
	public void SetVelocity(Vector4 velocity)
	{
		mVelocity = velocity;
	}
	
	@Override
	public void Resolve(XMLSerialize serialize) 
	{
		// do this in data
		mCollision = Memory.Heap.New(Collision.class);
		Collision.Rect rect = Memory.Heap.New(Collision.Rect.class);
		rect.mWidth = 70.f;
		rect.mHeight = 30.f;
		mCollision.InsertShape(rect);
		
		//Device device = Factory.Get().GetDevice();
		//device.Begin();
		// experimental
		//RenderBuffer cube = Factory.Get().CreateRenderBuffer();
		//cube.SetUsage(RenderBuffer.Static);
		//cube.CreateUnitCube();
		//mSprite.mGeometry = cube;
		//device.End();
	}
	
	public void Damage(float damage, Entity damager)
	{
		mHealth -= damage;
		
		// score the player a point for destroying this brick
		if (damager instanceof Ball)
		{
			Ball ball = (Ball)damager;
			Actor actor = (Actor)ball.GetEntity();
			actor.GetScore().mScore += actor.GetScore().mScoreMultiplier;
		}
	}
	
	public Vector4 GetPosition()
	{
		Memory.Stack.Push();
		Vector4 position = mSprite.mTransform.GetTranslation();
		return Memory.Stack.PopAndReturn(position);
	}
	
	public EnemyPath	mPath;
	public Collision	mCollision;
	public Vector4 		mVelocity 			= null;
	public Sprite 		mSprite;
	public float		mHealth				= 1.f;
	public State		mState				= State.Invisible;
	public float		mSpawnStateTime		= 0.25f;
	public float		mDyingStateTime		= 0.1f;
	public float		mTimeInState		= 0.f;
	public float		mOffsetZ			= 0.f;
	public String		mBulletName			= "bullet";
}
