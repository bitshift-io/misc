package BreakoutHD;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import BlackCarbon.API.Font;
import BlackCarbon.API.Network;
import BlackCarbon.API.Camera;
import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Memory;
import BlackCarbon.API.RenderBuffer;
import BlackCarbon.API.Texture;
import BlackCarbon.API.XMLSerialize;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Math;
import BlackCarbon.API.Network.Connection;
import BlackCarbon.Math.Vector4;
import BreakoutHD.Sprite;
import BreakoutHD.Collision.Rect;

//test
class StarfieldBackground
{
	public void Init()
	{
		Memory.Stack.Push();
		
		Level level = Game.Get().GetLevel();
		
		mGeometry = Factory.Get().CreateRenderBuffer();
		mTexture = Factory.Get().AcquireTexture("star");

		// create stars
		int numStars = 40;
		Vector4 positions[] = new Vector4[numStars * 4];
		Vector4 uvs[] = new Vector4[numStars * 4];
		Vector4 colours[] = new Vector4[numStars * 4];
		byte indicies[] = new byte[numStars * 6]; // will need to make short!
		
		float scale = 10.f;
		for (int i = 0; i < numStars; ++i)
		{
			float x = (level.mRandom.nextFloat() - 0.5f) * (level.mBoardWidth + 50.f);
			float y = (level.mRandom.nextFloat() - 0.5f) * (level.mBoardHeight + 50.f);
			float z = (level.mRandom.nextFloat()) * 100.f;
			
			positions[i * 4 + 0] = new Vector4(x + (-0.5f * scale), y + (-0.5f * scale), z);
			positions[i * 4 + 1] = new Vector4(x + ( 0.5f * scale), y + (-0.5f * scale), z);
			positions[i * 4 + 2] = new Vector4(x + (-0.5f * scale), y + ( 0.5f * scale), z);
			positions[i * 4 + 3] = new Vector4(x + ( 0.5f * scale), y + ( 0.5f * scale), z);
			
			uvs[i * 4 + 0] = new Vector4(0,   1.f);
			uvs[i * 4 + 1] = new Vector4(1.f, 1.f);
			uvs[i * 4 + 2] = new Vector4(  0, 0.f);
			uvs[i * 4 + 3] = new Vector4(1.f, 0.f);
			
			float alpha = 1.0f - (z / 100.f);
			colours[i * 4 + 0] = new Vector4(alpha, alpha, alpha, alpha);
			colours[i * 4 + 1] = new Vector4(alpha, alpha, alpha, alpha);
			colours[i * 4 + 2] = new Vector4(alpha, alpha, alpha, alpha);
			colours[i * 4 + 3] = new Vector4(alpha, alpha, alpha, alpha);
			
			indicies[i * 6 + 0] = (byte) (0 + i * 4);
			indicies[i * 6 + 1] = (byte) (1 + i * 4);
			indicies[i * 6 + 2] = (byte) (3 + i * 4);
			indicies[i * 6 + 3] = (byte) (0 + i * 4);
			indicies[i * 6 + 4] = (byte) (3 + i * 4);
			indicies[i * 6 + 5] = (byte) (2 + i * 4);
		}
		
		Device device = Factory.Get().GetDevice();
		device.Begin();
		
		mGeometry.SetIndicies(indicies);
		mGeometry.SetPositions(positions);
		mGeometry.SetUVs(uvs);
		mGeometry.SetColours(colours);
		mGeometry.MakeStatic();
		
		device.End();
	
		Memory.Stack.Pop();
	}
	
	public void Update()
	{

	}
	
	public void Render()
	{
		Memory.Stack.Push();
		Device device = Factory.Get().GetDevice();
		
		Matrix4 identity = Memory.Stack.New(Matrix4.class);
		identity.SetIdentity();
		device.SetMatrix(identity, Device.MatrixMode.Model);
		device.SetTexture(mTexture);
		device.SetRenderBuffer(mGeometry);
		device.Render();
		/*
		mTexture.Bind();
		mGeometry.Bind();
		mGeometry.Render();
		mGeometry.Unbind();
		mTexture.Unbind();*/
		
		Memory.Stack.Pop();
	}
	
	Texture			mTexture;
	RenderBuffer 	mGeometry;
}

public class Level implements Network.Callback
{
	public enum State
	{
		Unloaded,
		Loaded,
		Loading
	}
	
	public Level()
	{
		
	}
	/*
	// converts screen space coordinates to world space coordinates
	public Vector4 ScreenToWorld(Vector4 screen)
	{
		Memory.Stack.StackLevel stack = Memory.Stack.Push();

		Matrix4 view = mCamera.GetView();
		Matrix4 projection = mCamera.GetProjection();
		
		Matrix4 viewProjection = view.Multiply(projection);	
		Matrix4 invViewProjection = viewProjection.GetInverse();
	
		Vector4 result = invViewProjection.Multiply4(screen);
		result = result.Divide(result.GetW());
		
		return Memory.Stack.PopAndReturn(stack, result);
	}*/
	
	// THIS SHOULD ONLY BE CALLED ONCE PER FRAME, OTHERWISE CHANGE IT TO CACHE THESE MATRIX MULTIPLES AND INVERSE
	// AS THEY ARE EXPENSIVE, MAYBE MOVE THEM TO BE CACHED BY THE CAMERA?
	// projects on to the XY plane in world space
	public Vector4 ScreenToWorldXYPlane(Vector4 screen)
	{
		Memory.Stack.StackLevel stack = Memory.Stack.Push();
		
		Matrix4 view = mCamera.GetView();
		Matrix4 projection = mCamera.GetProjection();
		
		Matrix4 viewProjection = view.Multiply(projection);	
		Matrix4 invViewProjection = viewProjection.GetInverse();
	
		//Vector4 result = invViewProjection.Multiply4(screen);
		//result = result.Divide(result.GetW());
		
		
		
		// temp1 = near point, temp2 = far point
		float screenZ = screen.GetZ();
		
		screen.SetZ(0.f);
		//Vector4 a = ScreenToWorld(screen);
		Vector4 a = invViewProjection.Multiply4(screen);
		a = a.Divide(a.GetW());
		
		screen.SetZ(1.f);
		//Vector4 b = ScreenToWorld(screen);
		Vector4 b = invViewProjection.Multiply4(screen);
		b = b.Divide(b.GetW());
		
		screen.SetZ(screenZ);
		
		// intercepts XY plane when z = 0
		// m = intercept point
		float t = (-a.GetZ() / (b.GetZ() - a.GetZ()));
		float mX = a.GetX() + (b.GetX() - a.GetX()) * t;
		float mY = a.GetY() + (b.GetY() - a.GetY()) * t;
		
		Vector4 result = Memory.Stack.New(Vector4.class);
		result.Set(mX, mY, 0.f, 0.f);
		return Memory.Stack.PopAndReturn(stack, result);
	}

	public void LoadGlobalResources()
	{
		Memory.Stack.StackLevel stack = Memory.Stack.Push();

		Factory factory = Factory.Get();
		Device device = factory.GetDevice();
		
		device.Begin();
		device.SetClearColour(new Vector4(0.f, 0.f, 0.f, 0.f));
		device.End();
		
		// load obstruction templates
		XMLSerialize objectTemplates = new XMLSerialize(factory, "template", "BreakoutHD");
		for (int i = 0; i < objectTemplates.GetSize(); ++i)
		{
			Object object = objectTemplates.Get(i);
			
			// if we come across a level, load the data in to this level
			if (object.getClass() == Level.class)
			{
				objectTemplates.Copy(object, this);
			}
			else
			{
				mTemplate.add(object);
			}
		}
		
		// set up any entity lists that are generated at runtime to stop this mEntiyMap being modified while its in use
		Vector<Entity> entityList = new Vector<Entity>();
		mEntityMap.put(Bullet.class, entityList);
		
		// TODO: add support in serializer class for this
		Collision.Rect rect = (Rect) mCollision.mShape.get(0);
		rect.mFlags = Collision.Inverted;
		
		/*
		mCollision = new Collision();
		Collision.Rect rect = new Collision.Rect();
		rect.mOffset = new Vector4(0.f, 0.f, 0.f, 0.f);
		rect.mWidth = mBoardWidth;
		rect.mHeight = mBoardHeight;
		rect.mFlags = Collision.Inverted;
		mCollision.InsertShape(rect);*/
		
		
		// set up standard bricks config
    	for (int x = 0; x < 5; ++x)
    	{
    		for (int y = 0; y < 5; ++y)
    		{
    			//float xPos = (x + 0.5f) * (70.f + 2.f);
    			//float yPos = y * (30.f + 2.f);
    			//world.SetTranslation(new Vector4(xPos, yPos, 0.f));
    			
    			Matrix4 world = new Matrix4(Matrix4.Init.Identity);
    	    	Vector4 pos = new Vector4(0.f, 0.f, 0.f);
    	    	
    	    	// TODO: MOVE TO DATA
    			pos.SetX((x * 80.f) - 175.f);
    			pos.SetY((y * 80.f) + 400.f);
    			world.SetTranslation(pos);
    			
    			
    			Brick brick = (Brick) LoadFromTemplate(Brick.class);
    			brick.SetTransform(world);
    			brick.SetVelocity(new Vector4(0.f, -10.f, 0.f));
    			InsertEntity(brick);
    		}
    	}

		Vector4 translation = Memory.Stack.New(Vector4.class);
		translation.Set(0.f, 0.f, mFrameOffsetZ, 0.f);
		mFrame.mTransform.SetTranslation(translation);
		translation.Set(0.f, 0.f, mCursorOffsetZ, 0.f);
		mCursor.mTransform.SetTranslation(translation);
		
		mFrame.Resolve(null);
		mCursor.Resolve(null);
		
		
		mBackground = new StarfieldBackground();
		mBackground.Init();
		
		UpdateCamera(0.f);
		
		// set up the laser
		InsertEntity(LoadFromTemplate(Laser.class));
		
		Memory.Stack.Pop(stack);
	}
	
	public void UpdateCamera(float desiredHoritonalPosition) //Director director)
	{
		Memory.Stack.StackLevel stack = Memory.Stack.Push();
		
		Device device = Factory.Get().GetDevice();
		
		float halfFovRad = Math.DegToRad(mFieldOfView) * 0.5f;
		float cameraDistance = (mBoardHeight * 0.5f) / Math.Tan(halfFovRad);
		
		float aspectRatio = device.GetAspectRatio();
		mCamera.SetPerspective(mFieldOfView, cameraDistance * 0.1f, cameraDistance * 2.f, aspectRatio);
		
		//float desiredHoritonalPosition = director.GetActionHeld(Director.Action.DesiredHorizontalPosition); //Percent);
		desiredHoritonalPosition /= mBoardWidth;
		
		float rotationRad = -desiredHoritonalPosition * Math.DegToRad(mCameraTilt); // how much camera can tilt based on input
		float s = Math.Sin(rotationRad);
		float c = Math.Cos(rotationRad);
		
		Vector4 cameraView = Memory.Stack.New(Vector4.class);
		cameraView.Set(-s, 0.f, c, 0.f);
		
		Vector4 cameraPos = Memory.Stack.New(Vector4.class);
		cameraPos.Set(s, 0.f, -c, 0.f);
		cameraPos = cameraPos.Multiply(cameraDistance + mCameraDistanceOffset);
		
		Vector4 up = Memory.Stack.New(Vector4.class);
		up.Set(0.f, 1.f, 0.f, 0.f);
		
		Matrix4 world = Memory.Stack.New(Matrix4.class);
		world.CreateFromForward(cameraView, up);
		world.SetTranslation(cameraPos);
		mCamera.SetWorld(world);
		
		Memory.Stack.Pop(stack);
	}
	
	public synchronized void Update()
	{	
		if (mState != State.Loaded)
			return;
		
		Memory.Stack.StackLevel stack = Memory.Stack.Push();
		
		// update entities		
		Collection<Vector<Entity>> map = mEntityMap.values();
		Iterator<Vector<Entity>> mapIt = map.iterator();
		Vector<Entity> entityList = null;
		while (mapIt.hasNext())
		{
			entityList = mapIt.next();
			for (int i = 0; i < entityList.size(); ++i)
			{
				Entity entity = entityList.get(i);
				entity.Update();
			}
		}
		
		mBackground.Update();
    	
    	Memory.Stack.Pop(stack);
	}
	
	public synchronized void Render()
	{
		if (mState != State.Loaded)
			return;
		
		Memory.Stack.StackLevel stack = Memory.Stack.Push();
		Device device = Factory.Get().GetDevice();
		Game game = Game.Get();
		
        mCamera.Bind();
        
    	// set device state - enable alpha blending
    	Device.DeviceState state = Memory.Stack.New(Device.DeviceState.class);
    	state.alphaBlendEnable = true;
    	state.alphaCompare = Device.Compare.Greater;
    	state.alphaBlendSource = Device.BlendMode.One;
    	state.alphaBlendDest = Device.BlendMode.One;
    	state.depthTestEnable = false; // TODO: MAKE ENGINE SUPPORT DPEHT TEST
    	device.SetDeviceState(state);
   
    	mBackground.Render();
    	mFrame.Render();
    	mCursor.Render();
    	
    	if (Game.Get().IsDebug())
			mCollision.RenderDebug();
   	
    	try
    	{
    		// render entities		
    		Collection<Vector<Entity>> map = mEntityMap.values();
    		Iterator<Vector<Entity>> mapIt = map.iterator();
    		Vector<Entity> entityList = null;
    		while (mapIt.hasNext())
    		{
    			entityList = mapIt.next();
    			for (int i = 0; i < entityList.size(); ++i)
    			{
    				Entity entity = entityList.get(i);
    				entity.Render();
    			}
    		}
    	}
    	catch (Exception e)
    	{
    		System.out.print(e.getLocalizedMessage());
			e.printStackTrace();
    	}
    	
    	// render text
    	
    	Vector4 translation = Memory.Stack.New(Vector4.class);
    	
    	// wtf?
    	if (Factory.Get().GetPlatform() == Factory.Platform.Android)
    	{
    		translation.Set(-200.f, 375.f, -10.f, 0.f);
    	}
    	else
    	{
    		translation.Set(-200.f, 345.f, -10.f, 0.f);
    	}
    			
    	
    	Vector4 scale = Memory.Stack.New(Vector4.class);
    	scale.Set(2.f, 2.f, 2.f, 1.f);
    	
    	Matrix4 transform = Memory.Stack.New(Matrix4.class);
    	transform.SetIdentity();
    	transform.SetScale(scale);
    	transform.SetTranslation(translation);
    	device.SetMatrix(transform, Device.MatrixMode.Model);
    	
    	Font font = game.GetFont();
    	
    	// FONT RENDERING IS SLLLOOOOOWWW!
    	//font.Render("Score: 10      FPS: " + game.mRenderThisSecond); // NEED TO INCREASE INDEX BUFFER FROM BYTE TO SHORT INT
    	
    	font.Render("" + game.mRenderThisSecond);
    	
    	// set device state - disable alpha blending
    	state.alphaBlendEnable = false;
    	device.SetDeviceState(state);
    	Memory.Stack.Pop(stack);
	}
	
	// we need a string version to take by entity name!
	@SuppressWarnings("unchecked")
	public Object LoadFromTemplate(Class type)
	{
		// TODO: this needs some work on the memory front
		//Memory.Stack.Push();
		
		for (int i = 0; i < mTemplate.size(); ++i)
		{
			Object obj = mTemplate.get(i);
			if (obj.getClass() == type)
			{
				XMLSerialize objectCloner = Memory.Heap.New(XMLSerialize.class); //new XMLSerialize();
				
				//Memory.Stack.Pop();
				return objectCloner.Clone(obj);
			}
		}
		
		//Memory.Stack.Pop();
		return null;
	}
	
	public Object LoadFromTemplate(String name)
	{
		// TODO: this needs some work on the memory front
		//Memory.Stack.Push();
		
		for (int i = 0; i < mTemplate.size(); ++i)
		{
			Object obj = mTemplate.get(i);
			if (obj instanceof Entity)
			{
				Entity entity = (Entity) obj;
				if (entity.mName.equalsIgnoreCase(name))
				{
					XMLSerialize objectCloner = Memory.Heap.New(XMLSerialize.class); //new XMLSerialize();
					
					//Memory.Stack.Pop();
					return objectCloner.Clone(obj);
				}
			}
		}
		
		//Memory.Stack.Pop();
		return null;
	}
	
	public void SetState(State state)
	{
		mState = state;
	}
	
	@Override
	public void NotifyAddConnection(Connection connection)
	{
		Memory.Stack.Push();
		System.out.println("A new client has connected");
		
		// create a new actor and director for this network player to control
		Director director = new Director(Director.State.Network);
		director.SetConnection(connection);
    	
    	Actor actor = (Actor)LoadFromTemplate(Actor.class);
    	actor.SetDirector(director);
    	InsertEntity(actor);
    	Memory.Stack.Pop();
	}
	
	@Override
	public void NotifyRemoveConnection(Connection connection)
	{
		Memory.Stack.Push();
		System.out.println("A client has disconnected");
		
		Vector<Actor> actorList = GetEntityList(Actor.class);
		
		for (int i = 0; i < actorList.size(); ++i)
		{
			try
	    	{
				Actor actor = actorList.get(i);
				Director director = actor.GetDirector();
	
				if (director != null && director.GetConnection() == connection)
				{
					actorList.remove(i);
				}
	    	}
	    	catch (Exception e)
	    	{
	    		// not a network director class
	    	}

		}
		
		Memory.Stack.Pop();
	}
	
	@SuppressWarnings("unchecked")
	public <T> Vector<T> GetEntityList(Class<T> type)
	{
		return (Vector<T>) mEntityMap.get(type);
	}
	
	public synchronized <T> void InsertEntity(T entity)
	{
		Vector<Entity> entityList = mEntityMap.get(entity.getClass());
		if (entityList == null)
		{
			entityList = new Vector<Entity>();
			mEntityMap.put(entity.getClass(), entityList);
		}
		
		entityList.add((Entity)entity);
	}
	
	public synchronized <T> void RemoveEntity(T entity)
	{
		Vector<Entity> entityList = mEntityMap.get(entity.getClass());
		entityList.remove(entity);
	}
	

	HashMap<Class<?>, Vector<Entity>>	mEntityMap	= new HashMap<Class<?>, Vector<Entity>>();
	
	Vector<Object> 			mTemplate 			= new Vector<Object>();
	State					mState				= State.Unloaded;
	Camera					mCamera				= new Camera();
	int						mRenderMode			= 1;

	Random 					mRandom 			= new Random(19580427);
	
	public Collision		mCollision;
	public Sprite			mFrame;
	public Sprite			mCursor;
	
	public float			mCameraDistanceOffset = 500.f;
	
	public float			mFieldOfView		= 10.f;
	public float			mCameraTilt			= 20.f;
	
	public float 			mBoardWidth 		= 480.f; //320.f; // sprite width
	public float 			mBoardHeight 		= 800.f; //480.f;
	
	public float			mFrameOffsetZ		= 1.f;
	public float			mCursorOffsetZ		= 0.f;
	
	public StarfieldBackground mBackground;
}
