package BreakoutHD.Android;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import BlackCarbon.API.Factory;
import BlackCarbon.Android.AndroidDevice;
import BlackCarbon.Android.AndroidFactory;
import BlackCarbon.Math.Math;
import BreakoutHD.Game;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;

public class Main extends Activity implements BlackCarbon.Android.GLSurfaceView.Renderer
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
    	try
    	{
	        super.onCreate(savedInstanceState);

			AndroidFactory.InitParam init = new AndroidFactory.InitParam();
			init.resourceDir = "BreakoutHD.Android:raw";
			init.activity = this;
			new AndroidFactory(init);
			new Game();
    	}
    	catch (Exception e)
    	{
    		System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
    	}
    }

    @Override
    protected void onPause() 
    {
        super.onPause();
        
        AndroidFactory factory = (AndroidFactory)Factory.Get();
        factory.OnPause();
    }

    @Override
    protected void onResume() 
    {
        super.onResume();
        
        AndroidFactory factory = (AndroidFactory)Factory.Get();
        factory.OnResume();
    }
    
    @Override 
    public boolean onKeyUp(int keyCode, KeyEvent event) 
    { 
        switch(keyCode) 
        { 
        case KeyEvent.KEYCODE_DPAD_CENTER: 
            finish();
            return true; 
        } 
        
        return false; 
    } 

	@Override
	public void onDrawFrame(GL10 gl)
	{
		try 
    	{
	    	Game.Get().Update();
	    	Game.Get().Render();
		} 
    	catch (Exception e) 
		{
    		System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
		}
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height)
	{
		Factory.Get().GetDevice().Resize(width, height);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		try 
    	{
	    	AndroidDevice device = (AndroidDevice)Factory.Get().GetDevice();
	    	if (gl instanceof GL11)
	    		device.mGL = (GL11)gl;
	    	
	    	Game.Get().Init();
    	} 
    	catch (Exception e) 
		{
    		System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
		}
	}
}
