package BreakoutHD;

import java.util.Vector;

import BlackCarbon.Math.Vector4;
import BlackCarbon.Math.Math;
import BlackCarbon.API.Memory;
import BlackCarbon.API.XMLSerialize;
import BreakoutHD.Sprite;
import BreakoutHD.Game;

//
// the representation of what your controlling in the world
//
public class Actor extends Entity
{
	public void SetDirector(Director director)
	{
		mDirector = director;
		mDirector.SetActor(this);
	}
	
	public Director GetDirector()
	{
		return mDirector;
	}
	
	public Score GetScore()
	{
		return mDirector.GetScore();
	}
	
	public void LaunchBall()
	{
		if (mBall == null)
			return;
		
		Memory.Stack.Push();
		
		Director director = GetDirector();
		Vector4 launchDirection = director.GetBallLaunchDirection();
		launchDirection.Normalize3();
		Vector4 launchVector = Memory.Stack.New(Vector4.class);
		launchVector = launchDirection.Multiply(mBallLaunchSpeed);
		
		mBall.Launch(launchVector);
		mBall = null;
		
		Memory.Stack.Pop();
	}
	
	public Vector4 GetAttachPoint()
	{
		Memory.Stack.Push();
		Vector4 attachPoint = mSprite.mTransform.GetTranslation();
		attachPoint.SetY(attachPoint.GetY() + mAttachPointYOffset);
		return Memory.Stack.PopAndReturn(attachPoint);
	}
	
	public void GiveBall(Ball ball)
	{
		if (mBall != null)
			return;
		
		mBall = ball;
		mBall.AttachToEntity(this);
	}
	
	@Override
	public void Update()
	{
		Memory.Stack.StackLevel stack = Memory.Stack.Push();
		Level level = Game.Get().GetLevel();
		
		Director director = GetDirector();
		director.Update();
		
		float desiredHoritonalPosition = director.GetActionHeld(Director.Action.DesiredHorizontalPosition);
		
		Vector4 translation = level.mCursor.mTransform.GetTranslation();
		translation.SetY(mOffsetY); // move down
		
		translation.SetX(Math.Clamp(-mMovementX, desiredHoritonalPosition, mMovementX)); // magnify movement
		translation.SetZ(mOffsetZ);
		mSprite.mTransform.SetTranslation(translation);
		
		mCollision.SetTransform(mSprite.mTransform);
		
		if (director.GetActionPressed(Director.Action.LaunchBall))
		{
			LaunchBall();
		}
		
		// manual update to make sure it follows us exactly with no lag
		if (mBall != null)
			mBall.Update_AttachedToEntity();
	
		Memory.Stack.Pop(stack);
	}

	public Vector4 GetPosition()
	{
		Memory.Stack.Push();
		Vector4 position = mSprite.mTransform.GetTranslation();
		return Memory.Stack.PopAndReturn(position);
	}
	
	@Override
	public void Render()
	{
		mSprite.Render();
		
		if (Game.Get().IsDebug())
			mCollision.RenderDebug();
		
		mDirector.Render();
	}
	
	@Override
	public void Resolve(XMLSerialize serialize) 
	{
		/*
		// do this in data
		mCollision = Memory.Heap.New(Collision.class);
		Collision.Rect rect = Memory.Heap.New(Collision.Rect.class);
		rect.mWidth = 100.f;
		rect.mHeight = 100.f;
		mCollision.InsertShape(rect);*/
	}
	
	// when the ball hits a paddle, it always emits in a certain predictable
	// angle
	public Vector4 GetBallReflectVector(Vector4 position, Vector4 vector)
	{
		Memory.Stack.Push();
		
		float magnitude = vector.Magnitude3();
		
		// set the balls new vector? or return a new vector?
		//Vector4 translation = ball.mSprite.mTransform.GetTranslation();
		float ballX = position.GetX();
		
		Vector4 translation = mSprite.mTransform.GetTranslation();
		float actorX = translation.GetX();
		
		float deltaX = ballX - actorX;
		float actorWidth = mSprite.mScaleX;
	
		// this percentage should be 0 if directly in centre of paddle,
		// -1 if to the left of the very left of the paddle,
		// 1 if to the very right of the paddle
		float percent = deltaX / (actorWidth * 0.5f);
		
		float reflectAngle = Math.DegToRad(45.f) * percent;
		float s = Math.Sin(reflectAngle);
		float c = Math.Cos(reflectAngle);

		if (IsAtTop())
			c = -c;
		
		Vector4 reflectVector = Memory.Stack.New(Vector4.class);
		reflectVector.Set(s, c, 0.f, 0.f);
		reflectVector = reflectVector.Multiply(magnitude);		
		return Memory.Stack.PopAndReturn(reflectVector);
	}
	
	// are we at the top of the playing field? ie. upside down
	boolean IsAtTop()
	{
		return mSprite.mScaleY < 0.f;
	}

	private Director 	mDirector;
	public Ball			mBall				= null;
	public Sprite 		mSprite;
	public Collision	mCollision;
	public float		mOffsetZ			= 0.f;
	public float		mAttachPointYOffset	= -100.f;
	public float		mOffsetY			= -300.f; 	// the y offset of the paddle
	public float		mMovementX			= 200.f;	// how much it can move from the origin
	public float		mBallLaunchSpeed	= 400.f;
}
