package BreakoutHD;

import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Font;
import BlackCarbon.API.Memory;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;

public class Score
{
	public void Render()
	{
		Memory.Stack.Push();
		
		Game game = Game.Get();
		Device device = Factory.Get().GetDevice();
		
		Vector4 translation = Memory.Stack.New(Vector4.class);
    	
    	// wtf?
    	if (Factory.Get().GetPlatform() == Factory.Platform.Android)
    	{
    		translation.Set(-200.f, 375.f, -10.f, 0.f);
    	}
    	else
    	{
    		translation.Set(-200.f, 345.f, -10.f, 0.f);
    	}
    	
    	translation.Set(-320.f, 295.f, -10.f, 0.f);
    			
    	
    	Vector4 scale = Memory.Stack.New(Vector4.class);
    	scale.Set(2.f, 2.f, 2.f, 1.f);
    	
    	Matrix4 transform = Memory.Stack.New(Matrix4.class);
    	transform.SetIdentity();
    	transform.SetScale(scale);
    	transform.SetTranslation(translation);
    	device.SetMatrix(transform, Device.MatrixMode.Model);
    	
    	Font font = game.GetFont();
    	
    	// FONT RENDERING IS SLLLOOOOOWWW!
    	//font.Render("Score: 10      FPS: " + game.mRenderThisSecond); // NEED TO INCREASE INDEX BUFFER FROM BYTE TO SHORT INT
    	
    	font.Render("score." + mScore + " multiplier." + mScoreMultiplier + " lives." + mLivesRemaining);
    	
    	Memory.Stack.Pop();
	}
	
	public int	mLivesRemaining			= 10;
	public int	mScore					= 0;
	public int	mScoreMultiplier		= 1;
}
