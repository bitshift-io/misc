package BreakoutHD;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.util.Vector;

import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Input;
import BlackCarbon.API.Memory;
import BlackCarbon.API.Network;
import BlackCarbon.API.Input.TouchState;
import BreakoutHD.Game;
import BlackCarbon.Math.Vector4;
import BlackCarbon.Math.Math;

//
// directs an actor
//
public class Director
{
	enum State
	{
		Player,
		AI,
		Network
	}
	
	enum Action
	{
		DesiredHorizontalPosition,
		//DesiredHorizontalPercent,
		LaunchBall,
		
		Left,
		Right,
		Accelerate,
		Brake,
		
		MAX,
	}
	
	public Director(State state)
	{
		mState = state;
		
		for (int i = 0; i < Action.MAX.ordinal(); ++i)
		{
			mActionState[i] = Memory.Heap.New(ActionState.class);
		}
	}
	
	public float GetActionHeld(Action action)
	{
		return mActionState[action.ordinal()].value;
	}
	
	public boolean GetActionPressed(Action action)
	{
		return mActionState[action.ordinal()].value > 0.5f && mActionState[action.ordinal()].lastValue <= 0.5f;
	}
	
	// DO NOT CALL THIS IN THE NetworkDirector, as it modifies the timestamp
	public void Update()
	{
		Memory.Stack.StackLevel stack = Memory.Stack.Push();
		
		++mTimeStamp;
		
		for (int i = 0; i < Action.MAX.ordinal(); ++i)
		{
			mActionState[i].lastValue = mActionState[i].value;
		}
		
		switch (mState)
		{
		case Player:
			Update_Player();
			break;
			
		case AI:
			Update_AI();
			break;
			
		case Network:
			Update_Network();
			break;
		}
		
		Memory.Stack.Pop(stack);
	}
	
	public void Update_Player()
	{
		Factory factory = Factory.Get();
		Device device = factory.GetDevice();
		Input input = factory.GetInput();
		Game game = Game.Get();

		Vector<Laser> laser = Game.Get().GetLevel().GetEntityList(Laser.class);
		float launchYHeight = laser.get(0).mOffsetY; //-100.f;
		
		float cursorX = 0.f;
		float cursorY = 0.f;
		
		boolean touchReleased = false;
		
		mTimeSinceInput += game.GetDeltaTime();
		if (mTimeSinceInput > mIdleSwitchToAITime)
		{
			mState = State.AI;
		}
		
		if (factory.GetPlatform() == Factory.Platform.Desktop)
		{
			cursorX = input.GetState(Input.Control.MouseAxisX);
			cursorX /= device.GetWidth();
			cursorX = (cursorX * 2.f) - 1.f;
			
			cursorY = input.GetState(Input.Control.MouseAxisY);
			cursorY /= device.GetHeight();
			cursorY = ((cursorY * 2.f) - 1.f);
			
			// should be 'waspressed'
			touchReleased = input.WasPressed(Input.Control.MouseLeft);
			
			// user is pressing a key
			if (input.GetState(Input.Control.MouseLeft) > 0.f)
				mTimeSinceInput = 0.f;
		}
		
		if (factory.GetPlatform() == Factory.Platform.Android)
		{
			// touch for Android
			TouchState touch = (TouchState) input.GetControlState(Input.Control.Touch);

			cursorX = touch.x;
			cursorX /= device.GetWidth();
			cursorX = (cursorX * 2.f) - 1.f;
			
			cursorY = touch.y;
			cursorY /= device.GetHeight();
			cursorY = ((1.f - cursorY) * 2.f) - 1.f;
			
			touchReleased = touch.pressed;
			
			// user is pressing a key
			if (touch.state > 0.f)
				mTimeSinceInput = 0.f;
		}
		
			
		Level level = Game.Get().GetLevel();

		Vector4 cursorScreenSpace = Memory.Stack.New(Vector4.class);
		cursorScreenSpace.Set(cursorX, cursorY, 0.f, 1.f);
		Vector4 cursorWorldSpace = level.ScreenToWorldXYPlane(cursorScreenSpace);
		level.mCursor.mTransform.SetTranslation(cursorWorldSpace);
		
		mActionState[Action.LaunchBall.ordinal()].value = 0.f;
		if (cursorWorldSpace.GetY() > launchYHeight)
		{
			Vector4 actorPosition = mActor.GetAttachPoint();
			if (touchReleased)
			{
				mActionState[Action.LaunchBall.ordinal()].value = 1.f;
				mBallLaunchDirection.Copy(cursorWorldSpace.Subtract(actorPosition));
				mBallLaunchDirection.SetZ(0.f);
			}
		}
		else
		{
			//ActionState desiredHorizontalPercent = mActionState[Action.DesiredHorizontalPercent.ordinal()];
			ActionState desiredHorizontalPosition = mActionState[Action.DesiredHorizontalPosition.ordinal()];
			
			// there was some change in position
			float delta = Math.Abs(desiredHorizontalPosition.value - cursorWorldSpace.GetX());
			if (delta > (Math.Epsilon * 10.f))
			{
				mTimeSinceInput = 0.f;
			}
			
			//desiredHorizontalPercent.value = cursorX;
			desiredHorizontalPosition.value = cursorWorldSpace.GetX(); //cursorX;
		}
		
		// let the level update the camera based on this director/actor
		level.UpdateCamera(cursorWorldSpace.GetX()); //this);
		
		if (mConnection != null)
		{
			// sent packet once every half a second, so we dont flood the network and slow the fps down
			mNetworkSendTimer += Game.Get().GetDeltaTime();
			if (mNetworkSendTimer <= 0.1f)
				return;
			
			mNetworkSendTimer = 0.f;
			
			// broadcast our data to all network connections
			try 
			{
				PacketData packetData = Memory.Stack.New(PacketData.class); //new PacketData();
				packetData.timeStamp = mTimeStamp;
				//packetData.state = mActor.GetState().getClass();
				//packetData.rotation = mActor.GetRotation();
				//packetData.translation = mActor.GetTranslation();
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos;
				oos = new ObjectOutputStream(baos);
				
				oos.writeObject(packetData);
				byte[] buf = baos.toByteArray();
				
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				Factory.Get().GetNetwork().Broadcast(packet);
			} 
			catch (Exception e)
			{
				System.out.print(e.getLocalizedMessage());
	    		e.printStackTrace();
			}
		}
	}
	
	public void Update_AI()
	{
		Factory factory = Factory.Get();
		Input input = factory.GetInput();
		Game game = Game.Get();
		Level level = Game.Get().GetLevel();
		Vector<Ball> ballList = level.GetEntityList(Ball.class);
		Ball ball = ballList.get(0);
		if (ball == null)
			return;
		
		// pick a new launch vector
		mLaunchBallTime -= game.GetDeltaTime();
		if (mLaunchBallTime <= 0.f)
		{
			mLaunchBallTime = mLaunchBallTimeMin + level.mRandom.nextFloat() * (mLaunchBallTimeMax - mLaunchBallTimeMin);
			
			mBallLaunchDirection.Set(0.707f, 0.707f, 0.f, 0.f);
			mActionState[Action.LaunchBall.ordinal()].value = 1.f;
		}
		else
		{
			mActionState[Action.LaunchBall.ordinal()].value = 0.f;
		}	
		
		mOffsetXTime -= game.GetDeltaTime();
		if (mOffsetXTime <= 0.f)
		{
			mOffsetXTime = mOffsetXTimeMin + level.mRandom.nextFloat() * (mOffsetXTimeMax - mOffsetXTimeMin);
			mDesiredOffsetX = ((level.mRandom.nextFloat() * 2.f) - 1.f) * (mActor.mSprite.mScaleX * 2.f); // increasing this makes the AI miss more
		}
		
		// lerp towards the desired xoffset
		float deltaOffset = mDesiredOffsetX - mOffsetX;
		if (deltaOffset < 0.f)
			deltaOffset = -1.f;
		else
			deltaOffset = 1.f;
		
		mOffsetX = mOffsetX + deltaOffset * mOffsetDeltaSpeed * game.GetDeltaTime();
		
		ActionState desiredHorizontalPosition = mActionState[Action.DesiredHorizontalPosition.ordinal()];
		desiredHorizontalPosition.value = ball.mSprite.mTransform.GetTranslation().GetX() + mOffsetX;
		
		Vector4 cursorWorldSpace = Memory.Stack.New(Vector4.class);
		cursorWorldSpace.Set(desiredHorizontalPosition.value, mActor.mSprite.mTransform.GetTranslation().GetY() - mActor.mSprite.mScaleY * 0.5f, 0.f, 0.f);
		level.mCursor.mTransform.SetTranslation(cursorWorldSpace);
		
		// let the level update the camera based on this director/actor
		level.UpdateCamera(cursorWorldSpace.GetX()); //this);
		
		// return to be human again?
		if (factory.GetPlatform() == Factory.Platform.Desktop)
		{
			// user is pressing a key
			if (input.WasPressed(Input.Control.MouseLeft))
			{
				mTimeSinceInput = 0.f;
				mState = State.Player;
			}
		}
		
		if (factory.GetPlatform() == Factory.Platform.Android)
		{
			// touch for Android
			TouchState touch = (TouchState) input.GetControlState(Input.Control.Touch);

			// user is pressing a key
			if (touch.pressed)
			{
				mTimeSinceInput = 0.f;
				mState = State.Player;
			}
		}
	}
	
	public void Update_Network()
	{
		try 
		{
			// read packet from network
			byte[] buf = new byte[1024];
			DatagramSocket udp = mConnection.GetUDPSocket();
			
			while (true)
		    {
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				udp.receive(packet);
				mConnection.udpBytesReceived += packet.getLength();
				mConnection.lastPacket = 0; // we received a packet, so clear this
				
				// deserialize from a byte array - can treat as an object, then query its class type
				ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(buf));
				PacketData packetData = (PacketData) in.readObject();
			    in.close();
				
			    // modify actor, if this is a newer packet than we have previously received
			    if (packetData.timeStamp > mTimeStamp)
			    {
			    	mTimeStamp = packetData.timeStamp;
			    
				    //mActor.SetState(packetData.state);
				    //mActor.SetTranslation(packetData.translation);
				    //mActor.SetRotation(packetData.rotation);
			    }
		    }
		    
		} 
		catch (SocketTimeoutException e) 
	    {
	    	// out of packets
	    }
		catch (Exception e)
		{
			System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
		}
	}
	
	public void Render()
	{
		mScore.Render();
	}
	
	public Score GetScore()
	{
		return mScore;
	}
	
	public void SetActor(Actor actor)
	{
		mActor = actor;
	}
	
	Vector4 GetBallLaunchDirection()
	{
		return mBallLaunchDirection;
	}
	
	public void SetConnection(Network.Connection connection)
	{
		mConnection = connection;
	}
	
	public Network.Connection GetConnection()
	{
		return mConnection;
	}
	
	
	
	public static class ActionState
	{
		float lastValue;
		float value;
	}
	
	public static class PacketData implements Serializable
	{
		private static final long serialVersionUID = -2328105976847146556L;
		
		public long 	timeStamp;			// for discarding out of order packets
	}
	
	ActionState 		mActionState[] 				= new ActionState[Action.MAX.ordinal()];
	Actor				mActor;
	Score				mScore						= Memory.Heap.New(Score.class);
	long				mTimeStamp					= 0;
	State				mState						= State.Player;
	
	float				mTimeSinceInput				= 0.f;
	float				mIdleSwitchToAITime			= 10.f;
	
	// network
	Network.Connection 	mConnection;

	// player state
	Vector4 			mBallLaunchDirection		= Memory.Heap.New(Vector4.class);
	float 				mNetworkSendTimer;
	
	// AI
	float				mLaunchBallTimeMax			= 2.f;
	float				mLaunchBallTimeMin			= 1.f;
	float				mLaunchBallTime;
	
	float				mOffsetXTimeMax				= 1.f;
	float				mOffsetXTimeMin				= 0.2f;
	float				mOffsetDeltaSpeed			= 50.f;	// metres per second
	float				mOffsetXTime;
	float 				mOffsetX;
	float 				mDesiredOffsetX;
}
