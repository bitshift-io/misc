package BreakoutHD;

import java.util.StringTokenizer;

import BlackCarbon.API.Device;
import BlackCarbon.API.Font;
import BlackCarbon.API.Camera;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Input;
import BlackCarbon.API.Memory;
import BlackCarbon.API.XMLSerialize;
import BlackCarbon.UI.UIComponent;
import BlackCarbon.UI.UICursor;
import BlackCarbon.UI.UIEvent;
import BreakoutHD.Game;

// extend ui component to receive notifications of events
public class UserInterface extends UIComponent
{
	public UserInterface()
	{
	}
	
	public void LoadGlobalResources()
	{
		Game game = Game.Get();
		mFont = Factory.Get().AcquireFont(game.GetPrimaryFontName());
		mFont.SetHorizontalAlign(Font.HorizontalAlign.Centre);
		mFont.SetVerticalAlign(Font.VerticalAlign.Centre);
		
		try
    	{
			//SetUI("main");
    		
			XMLSerialize xml = new XMLSerialize(Factory.Get(), "cursor", "BlackCarbon.UI");
    		mCursor = (UICursor)xml.Get(0);
    	}
    	catch (Exception e)
    	{
    		System.out.println("[UserInterface::UserInterface] Exception: " + e.getMessage());
    	}
	}
	
	public void Update()
	{
		if (!IsVisible())
			return;
		
		Input input = Factory.Get().GetInput();
		if (input.WasPressed(Input.Control.KeyEscape))
		{
			mVisible = true;
		}
		
		if (mCursor != null)
			mCursor.Update(GetActiveUI());
		
		super.Update(mCursor);
	}
	
	public void Render()
	{
		if (!IsVisible())
			return;
		
		Memory.Stack.Push();
		
		Device device = Factory.Get().GetDevice();
		device.Clear(Device.CF_Depth);
		
    	// draw the ui
    	Camera uiCamera = Memory.Stack.New(Camera.class);
    	uiCamera.mFactory = Factory.Get();
    	uiCamera.SetOrthographic(mChild.get(0).GetWidth(), mChild.get(0).GetHeight(), 1.f, 15.f);
    	uiCamera.Bind();
   
    	super.Render();
    	mCursor.Render();
    	
    	Memory.Stack.Pop();
	}
	
	public void NotifyEvent(UIEvent event)
	{
		String command = event.component.GetCommand();
		switch (event.type)
		{
		case CursorSelect:
			{
				ProcessCommand(command);
				break;
			}
		}
	}
	
	public void ProcessCommand(String command)
	{
		if (command == null)
			return;
		
		StringTokenizer st = new StringTokenizer(command, " ,()");
		String primaryCommand = st.nextToken();
		
		if (primaryCommand.equalsIgnoreCase("start"))
		{
			mVisible = false;
		}
		else if (primaryCommand.equalsIgnoreCase("quit"))
		{
			System.exit(0);
		}
		else if (primaryCommand.equalsIgnoreCase("showui"))
		{
			String file = st.nextToken();
			SetUI(file);
		}
	}
	
	public boolean ShowUI(String file)
	{
		if (!SetUI(file))
			return false;
		
		SetVisible(true);
		return true;
	}
	
	public boolean SetUI(String file)
	{
		try
    	{
			// remove last ui
			if (mChild.size() > 0)
				mChild.remove(0);
			
			XMLSerialize xml = new XMLSerialize(Factory.Get(), file, "BlackCarbon.UI");
			UIComponent ui = (UIComponent)xml.Get(0);
			mChild.add(ui);
			ui.SetParent(null, this);
    	}
		catch (Exception e)
		{
			return false;
		}
		
		return true;
	}
	
	public void SetVisible(boolean visible)
	{
		mVisible = visible;
	}
	
	public boolean IsVisible()
	{
		return mVisible;
	}
	
	public UIComponent GetActiveUI()
	{
		return mChild.get(0);
	}
	
	boolean		mVisible	= false;
	UICursor 	mCursor;
}
