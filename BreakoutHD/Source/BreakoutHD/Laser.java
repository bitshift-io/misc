package BreakoutHD;

import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Memory;
import BlackCarbon.API.XMLSerialize;
import BlackCarbon.Math.Math;
import BlackCarbon.Math.Vector4;

//
// This is a separator between the ball and the falling blocks
//
//
public class Laser extends Entity
{
	@Override
	public void Resolve(XMLSerialize serialize) 
	{	
		Vector4 translation = mLaserSprite.mTransform.GetTranslation();
		//translation.SetY(mOffsetY); // move down

		//float boardWidth = Game.Get().GetLevel().mBoardWidth;
		translation.Set(mOffsetX, mOffsetY, 0.f, 0.f);
		mLaserSprite.mTransform.SetTranslation(translation);
		
		translation.Set(0.f, mOffsetY, 0.f, 0.f);
		mBeamSprite.mTransform.SetTranslation(translation);
		mBeamSprite.mScaleX = mOffsetX * 2;
	}
	
	@Override
	public void Render()
	{
		Memory.Stack.Push();
		
		// draw left laser
		Vector4 translation = mLaserSprite.mTransform.GetTranslation();
		mLaserSprite.Render();
		
		// draw the right laser
		//Vector4 rightTranslation = Memory.Heap.New(Vector4.class);
		//rightTranslation.Set
		translation.SetX(-translation.GetX());
		mLaserSprite.mTransform.SetTranslation(translation);
		mLaserSprite.mScaleX = -mLaserSprite.mScaleX;
		mLaserSprite.Render();
		
		// restore
		translation.SetX(-translation.GetX());
		mLaserSprite.mScaleX = -mLaserSprite.mScaleX;
		mLaserSprite.mTransform.SetTranslation(translation);
		

		// modify colour
		Device device = Factory.Get().GetDevice();
		Device.DeviceState currentState = device.GetDeviceState(); // do not modify directly, should i return a copy?
		Device.DeviceState newState = Memory.Stack.New(Device.DeviceState.class);
		newState.Copy(currentState);
		newState.colour.Set(0.f, 0.f, 1.f, mHealth);
		device.SetDeviceState(newState);
		
		mBeamSprite.Render();
		
		// reset colour
		newState.colour.Set(1.f, 1.f, 1.f, 1.f);
		device.SetDeviceState(newState);
		
		Memory.Stack.Pop();
	}
	
	public float GetHealth()
	{
		return mHealth;
	}
	
	public void TakeDamage(float damage)
	{
		mHealth -= damage;
		if (mHealth < 0.f)
			mHealth = 0.f;
	}

	public Vector4 GetPosition()
	{
		Memory.Stack.Push();
		Vector4 position = mLaserSprite.mTransform.GetTranslation();
		return Memory.Stack.PopAndReturn(position);
	}
	
	public float	mHealth		= 1.f;
	public float	mOffsetY;
	public float	mOffsetX;
	public Sprite	mLaserSprite;
	public Sprite	mBeamSprite;
}
