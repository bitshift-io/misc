package BreakoutHD;

//
// rules for determining the rules of the game,
// setting up the players/directors
//
public class GameMode 
{
	enum Mode
	{
		SinglePlayer,
		Coop,
		Versus,
	}
	
	// for VS
	static public int BottomActor = 0;
	static public int TopActor = 1;
	
	// for coop and SP
	static public int FirstActor = 0;
	static public int SecondActor = 1;

	
	Mode GetMode()
	{
		return mMode;
	}
	
	void SetMode(Mode mode)
	{
		mMode = mode;
	}
	
	public void LoadGlobalResources()
	{
		switch (mMode)
		{
		case SinglePlayer:
			LoadGlobalResources_SinglePlayer();
			break;
			
		case Coop:
			LoadGlobalResources_Coop();
			break;
			
		case Versus:
			LoadGlobalResources_Versus();
			break;
		}
	}

	void LoadGlobalResources_SinglePlayer()
	{
		Level level = Game.Get().GetLevel();
		Actor actor = (Actor)level.LoadFromTemplate("PaddleBottom0");
    	if (actor != null)
    	{	
	    	actor.SetDirector(new Director(Director.State.Player));
	    	level.InsertEntity(actor);
    	}
    	
    	// give ball to player
    	Ball ball = (Ball) level.LoadFromTemplate(Ball.class);
    	level.InsertEntity(ball);
    	actor.GiveBall(ball);
	}
	
	void LoadGlobalResources_Coop()
	{
		Level level = Game.Get().GetLevel();
		LoadGlobalResources_SinglePlayer();
		
		Actor actor = (Actor)level.LoadFromTemplate("PaddleBottom1");
    	if (actor != null)
    	{	
	    	actor.SetDirector(new Director(Director.State.Player));
	    	level.InsertEntity(actor);
    	}
	}
	
	void LoadGlobalResources_Versus()
	{
		Level level = Game.Get().GetLevel();
		LoadGlobalResources_SinglePlayer();
		
    	Actor actor = (Actor)level.LoadFromTemplate("PaddleTop0");
    	if (actor != null)
    	{	
	    	actor.SetDirector(new Director(Director.State.Player));
	    	level.InsertEntity(actor);
    	}
	}
	
	void Update()
	{
		switch (mMode)
		{
		case SinglePlayer:
			Update_SinglePlayer();
			break;
			
		case Coop:
			Update_Coop();
			break;
			
		case Versus:
			Update_Versus();
			break;
		}
	}
	
	void Update_SinglePlayer()
	{
		
	}
	
	void Update_Coop()
	{
		Update_SinglePlayer();
	}
	
	void Update_Versus()
	{
		
	}
	
	Mode	mMode	= Mode.Versus;
}
