package BreakoutHD;

import BlackCarbon.API.XMLSerializable;
import BlackCarbon.API.XMLSerialize;

//
// Base class for game objects
//
public class Entity implements XMLSerializable
{
	public void Render()
	{
		
	}
	
	public void Update()
	{

	}
	
	@Override
	public XMLSerializable Clone()
	{
		return null;
	}

	@Override
	public void Resolve(XMLSerialize serialize) 
	{
		
	}
	
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder(mName);
		return builder.toString();
	}

	public String			mName 			= "";
}
