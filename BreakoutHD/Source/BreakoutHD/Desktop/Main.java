package BreakoutHD.Desktop;

import java.util.concurrent.TimeUnit;

import BlackCarbon.Desktop.DesktopFactory;
import BlackCarbon.Math.Math;
import BreakoutHD.Game;

public class Main
{
	public static void main(String[] args)
	{		
		DesktopFactory.InitParam init = new DesktopFactory.InitParam();
		init.width = 480;
		init.height = 800;
		init.resourceDir = "Data";
		new DesktopFactory(init); 
		
		Game game = new Game();

		boolean bRunning = game.Init();
		while (bRunning) 
        {
        	bRunning = game.Update();
        	game.Render();
        }
	}
}