package BreakoutHD;

import java.util.Vector;

import BlackCarbon.API.Memory;
import BlackCarbon.Math.Vector4;
import BreakoutHD.Ball.State;

public class Bullet extends Entity
{
	enum State
	{
		Moving,
		Dead,
	}
	
	public void Launch(Vector4 velocity)
	{
		mState = State.Moving;
		mVelocity.Copy(velocity);
	}
	
	public void SetPosition(Vector4 translation)
	{
		mSprite.mTransform.SetTranslation(translation);
		mCollision.SetTransform(mSprite.mTransform);
	}
	
	@Override
	public void Render()
	{
		if (mState == State.Dead)
			return;
		
		mSprite.Render();
		
		if (Game.Get().IsDebug())
			mCollision.RenderDebug();
	}
	
	@Override
	public void Update()
	{
		Memory.Stack.StackLevel stack = Memory.Stack.Push();
		
		switch (mState)
		{		
		case Moving:
			Update_Moving();
			break;
			
		case Dead:
			break;
		}
		
		Memory.Stack.Pop(stack);
	}
	
	public void Update_Moving()
	{
		// update movement, occurs for all states
		Vector4 velocityThisFrame = Memory.Stack.New(Vector4.class);
		Vector4 translation = mSprite.mTransform.GetTranslation();
		velocityThisFrame.Copy(mVelocity);
		velocityThisFrame = velocityThisFrame.Multiply(Game.Get().GetDeltaTime());		
		translation = translation.Add(velocityThisFrame);
		mSprite.mTransform.SetTranslation(translation);
		mCollision.SetTransform(mSprite.mTransform);
		
		// subtract radius from the half width/height to shrink the board 
		// so we can compare strait against ball translation and ignore its radius
		Collision.Sphere sphere = (Collision.Sphere) mCollision.mShape.get(0);
		
		// bottom of board collision check
		Level level = Game.Get().GetLevel();
		float halfBoardHeight = level.mBoardHeight * 0.5f - sphere.mSphere.GetW();
		float distanceOffBoard = translation.GetY() + halfBoardHeight;
		if (distanceOffBoard < 0.f)
		{
			mState = State.Dead;
			return;
		}
		
		// check collision with actor
		Vector<Actor> actorList = level.GetEntityList(Actor.class);
		for (int i = 0; i < actorList.size(); ++i)
    	{
    		Actor actor = actorList.get(i);
    		Collision.Rect rect = (Collision.Rect) actor.mCollision.mShape.get(0);
    		
    		Vector4 actorPosition = actor.GetPosition();
    		
    		// add radius to actor rectangle width to make this code easier
    		float halfActorCollisionWidth = rect.mWidth * 0.5f + sphere.mSphere.GetW();
    		float halfActorCollisionHeight = rect.mHeight * 0.5f + sphere.mSphere.GetW();
    		
    		// the ball is aligned in the x range to collide with the actor paddle
    		boolean betweenX = translation.GetX() < (actorPosition.GetX() + halfActorCollisionWidth) 
								&& translation.GetX() > (actorPosition.GetX() - halfActorCollisionWidth);


			float actorCollisionY = (i == GameMode.BottomActor) ? actorPosition.GetY() + halfActorCollisionHeight : actorPosition.GetY() - halfActorCollisionHeight;
			

			// push out based on if the actor is the top or bottom
			if (i == GameMode.BottomActor && translation.GetY() < actorCollisionY) // bottom actor
			{	
				// disable actor collision, we have missed the ball!
				if (betweenX)
				{
					// game over man!
					actor.GetScore().mLivesRemaining -= 1;
					mState = State.Dead;
				}
			}
    	}
	}
	
	public Vector4		mVelocity			= Memory.Heap.New(Vector4.class);;
	public Collision	mCollision;
	public Sprite 		mSprite;
	public State		mState				= State.Dead;
}
