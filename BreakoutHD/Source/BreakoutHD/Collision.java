package BreakoutHD;

import java.util.Vector;

import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Memory;
import BlackCarbon.Math.Line;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;
import BlackCarbon.Math.Math;

public class Collision 
{
	static public int Inverted = 1 << 0; // must be inside the shape for collision to not occur
	
	public static class Result
	{
		public Shape 	shape;
		public Vector4 	normal;
		public Vector4	position;
	}
	
	public static class Shape //implements Cloneable
	{
		// assumes both shapes are in the same space (ie. world space)
		Result CollidesWith(Shape other)
		{
			return null;
		}
		
		// to transform shape, to some temporary space for collision detection
		// the same space of the other shape being tested
		void ApplySpaceTransform(Matrix4 transform)
		{

		}
		
		void RenderDebug()
		{
			
		}

		public Result RayCollide(Line ray) 
		{
			return null;
		}
	}
	
	public static class Rect extends Shape
	{
		@Override
		public Result RayCollide(Line ray) 
		{
			Memory.Stack.Push();
			
			float halfWidth = mWidth * 0.5f;
			float halfHeight = mHeight * 0.5f;
			
			Vector4 topRight = Memory.Stack.New(Vector4.class);
			topRight.Set(mOffset.GetX() + halfWidth, mOffset.GetY() + halfHeight, mOffset.GetZ(), 0.f);
			
			Vector4 topLeft = Memory.Stack.New(Vector4.class);
			topLeft.Set(mOffset.GetX() - halfWidth, mOffset.GetY() + halfHeight, mOffset.GetZ(), 0.f);
			
			Vector4 bottomLeft = Memory.Stack.New(Vector4.class);
			bottomLeft.Set(mOffset.GetX() - halfWidth, mOffset.GetY() - halfHeight, mOffset.GetZ(), 0.f);
			
			Vector4 bottomRight = Memory.Stack.New(Vector4.class);
			bottomRight.Set(mOffset.GetX() + halfWidth, mOffset.GetY() - halfHeight, mOffset.GetZ(), 0.f);
			
			Line topSegment = Memory.Stack.New(Line.class);
			topSegment.Set(topLeft, topRight, Line.Type.Segment);
			Line.ShortestDistanceResult topSegmentResult = ray.ShortestDistance(topSegment);
			
			// negative time0 means its behind the start of the ray
			// time1 is the range on te line, so negative and above 1 is out of range to hit this
			// line segment
			if (topSegmentResult.time0 >= 0.f && topSegmentResult.distance <= Math.Epsilon &&
					topSegmentResult.time1 >= 0.f && topSegmentResult.time1 <= 1.f)
			{
				Result cr = Memory.Stack.New(Result.class);
				cr.shape = this;
				cr.position = ray.GetPositionAtTime(topSegmentResult.time0);
				return Memory.Stack.PopAndReturnAll(cr);
			}
			
			return Memory.Stack.PopAndReturn(null);
		}
		
		@Override
		public void ApplySpaceTransform(Matrix4 transform)
		{
			Memory.Stack.Push();
			
			if (transform == null)
				mOffsetSpace.Copy(mOffset);
			else
				mOffsetSpace.Copy(transform.Multiply3(mOffset));
			
			Memory.Stack.Pop();
		}
		
		@Override
		public void RenderDebug()
		{
			Memory.Stack.Push();
			
			Vector4 colour = Memory.Stack.New(Vector4.class);
			colour.Set(1.f, 0.f, 0.f, 1.f);
			
			float halfWidth = mWidth * 0.5f;
			float halfHeight = mHeight * 0.5f;
			Vector4 offset = Memory.Stack.New(Vector4.class);
			
			offset.Set(halfWidth, halfHeight, 0.f, 0.f);
			Vector4 start = mOffset.Add(offset);
			offset.Set(halfWidth, -halfHeight, 0.f, 0.f);
			Vector4 end = mOffset.Add(offset);
			Factory.Get().GetDevice().DrawLine(start, end, colour);
			
			offset.Set(-halfWidth, halfHeight, 0.f, 0.f);
			start = mOffset.Add(offset);
			offset.Set(-halfWidth, -halfHeight, 0.f, 0.f);
			end = mOffset.Add(offset);
			Factory.Get().GetDevice().DrawLine(start, end, colour);
			
			offset.Set(halfWidth, halfHeight, 0.f, 0.f);
			start = mOffset.Add(offset);
			offset.Set(-halfWidth, halfHeight, 0.f, 0.f);
			end = mOffset.Add(offset);
			Factory.Get().GetDevice().DrawLine(start, end, colour);
			
			offset.Set(halfWidth, -halfHeight, 0.f, 0.f);
			start = mOffset.Add(offset);
			offset.Set(-halfWidth, -halfHeight, 0.f, 0.f);
			end = mOffset.Add(offset);
			Factory.Get().GetDevice().DrawLine(start, end, colour);
			
			Memory.Stack.Pop();
		}
		
		public float 	mWidth;
		public float 	mHeight;
		public Vector4 	mOffset				= Memory.Heap.New(Vector4.class); // local space
		public int 		mFlags;
		
		public Vector4	mOffsetSpace 	= Memory.Heap.New(Vector4.class);
	}
	
	public static class Sphere extends Shape
	{
		@Override
		public Result RayCollide(Line ray) 
		{
			return null;
		}
		
		@Override
		public void ApplySpaceTransform(Matrix4 transform)
		{			
			Memory.Stack.Push();
			
			if (transform == null)
				mSphereSpace.Copy(mSphere);
			else
				mSphereSpace.Copy(transform.Multiply3(mSphere));
			
			Memory.Stack.Pop();
		}
		
		@Override
		void RenderDebug()
		{
			Memory.Stack.Push();
			
			Vector4 colour = Memory.Stack.New(Vector4.class);
			colour.Set(1.f, 0.f, 0.f, 1.f);
			Factory.Get().GetDevice().DrawSphere(mSphereSpace, colour);
			
			Memory.Stack.Pop();
		}
		
		@Override
		public Result CollidesWith(Shape other)
		{
			Memory.Stack.Push();
			
			if (other instanceof Sphere)
			{
				//Sphere sphere = (Sphere) other;
				return null;
			}
			
			if (other instanceof Rect)
			{
				Rect rect = (Rect) other;
				
				Vector4 relativeSphere = mSphereSpace.Subtract(rect.mOffsetSpace);
				
				//Vector4 relativeSphere = mSphere.Subtract(rect.mOffset);
				float halfWidth = rect.mWidth * 0.5f;
				float halfHeight = rect.mHeight * 0.5f;
				float radius = relativeSphere.GetW();
				
				// we must be inside rectangle for no collision
				if ((rect.mFlags & Inverted) != 0)
				{
					boolean bCollision = false;
					Result cr = Memory.Stack.New(Result.class);
					cr.shape = other;
					cr.normal = Memory.Stack.New(Vector4.class); 
					cr.normal.Set(0.f, 0.f, 0.f, 0.f);
					
					if (relativeSphere.GetX() > (halfWidth - radius))
					{
						bCollision = true;
						Vector4 normal = Memory.Stack.New(Vector4.class);
						normal.Set(-1.f, 0.f, 0.f, 0.f);
						cr.normal = cr.normal.Add(normal);
					}
					
					if (relativeSphere.GetX() < -(halfWidth - radius))
					{
						bCollision = true;
						Vector4 normal = Memory.Stack.New(Vector4.class);
						normal.Set(1.f, 0.f, 0.f, 0.f);
						cr.normal = cr.normal.Add(normal);
					}
					
					if (relativeSphere.GetY() > (halfHeight - radius))
					{
						bCollision = true;
						Vector4 normal = Memory.Stack.New(Vector4.class);
						normal.Set(0.f, -1.f, 0.f, 0.f);
						cr.normal = cr.normal.Add(normal);
					}
					
					if (relativeSphere.GetY() < -(halfHeight - radius))
					{
						bCollision = true;
						Vector4 normal = Memory.Stack.New(Vector4.class);
						normal.Set(0.f, 1.f, 0.f, 0.f);
						cr.normal = cr.normal.Add(normal);
					}
					
					if (bCollision)
					{
						cr.normal.Normalize3();
						return Memory.Stack.PopAndReturnAll(cr);
					}
				}
				else // we must be outside for no collision
				{
					if ((Math.Abs(relativeSphere.GetX()) - radius) < halfWidth && (Math.Abs(relativeSphere.GetY()) - radius) < halfHeight)
					{
						Result cr = Memory.Stack.New(Result.class);
						cr.shape = other;
						cr.normal = Memory.Stack.New(Vector4.class);
						cr.normal.Set(0.f, 0.f, 0.f, 0.f);
						
						if (relativeSphere.GetY() > halfHeight && (relativeSphere.GetY() - radius) < halfHeight)
						{
							Vector4 normal = Memory.Stack.New(Vector4.class);
							normal.Set(0.f, 1.f, 0.f, 0.f);
							cr.normal = cr.normal.Add(normal);
						}
						
						if (relativeSphere.GetY() < -halfHeight && (relativeSphere.GetY() + radius) > -halfHeight)
						{
							Vector4 normal = Memory.Stack.New(Vector4.class);
							normal.Set(0.f, -1.f, 0.f, 0.f);
							cr.normal = cr.normal.Add(normal);
						}
						
						if (relativeSphere.GetX() > halfWidth && (relativeSphere.GetX() - radius) < halfWidth)
						{
							Vector4 normal = Memory.Stack.New(Vector4.class);
							normal.Set(1.f, 0.f, 0.f, 0.f);
							cr.normal = cr.normal.Add(normal);
						}
						
						if (relativeSphere.GetX() < -halfWidth && (relativeSphere.GetX() + radius) > -halfWidth)
						{
							Vector4 normal = Memory.Stack.New(Vector4.class);
							normal.Set(-1.f, 0.f, 0.f, 0.f);
							cr.normal = cr.normal.Add(normal);
						}
						
						cr.normal.Normalize3();
						return Memory.Stack.PopAndReturnAll(cr);
					}
				}
				
				return Memory.Stack.PopAndReturn(null);
			}
			
			return Memory.Stack.PopAndReturn(null);
		}
		
		public Vector4 	mSphere					= Memory.Heap.New(Vector4.class); // local space
		public int 		mFlags;
		
		public Vector4 	mSphereSpace			= Memory.Heap.New(Vector4.class);
	}
	
	
	public Collision()
	{
		mTransform.SetIdentity();
	}
	
	public Result RayCollide(Vector4 position, Vector4 direction)
	{
		Memory.Stack.Push();
		
		// convert ray position and direction i to this collision objects
		// local space
		Matrix4 inverseTransform = mTransform.GetInverse();
		
		Line ray = Memory.Stack.New(Line.class);
		Vector4 position2D = inverseTransform.Multiply3(position);
		//position2D.Copy(position);
		position2D.SetZ(0.f);
		
		
		
		Vector4 direction2D = inverseTransform.MultiplyRotation3(direction);
		//direction2D.Copy(direction);
		direction2D.SetZ(0.f);
		direction2D.Normalize3();
		ray.Set(position2D, direction2D, Line.Type.Ray);
		
		
		// convert ray to be in shapes local space
		for (int t = 0; t < mShape.size(); ++t)
    	{
			Shape thisShape = mShape.get(t);
			Result result = thisShape.RayCollide(ray);
			if (result != null)
			{
				// convert position to world space
				result.position = mTransform.Multiply3(result.position);
				return Memory.Stack.PopAndReturnAll(result);
			}
    	}
		
		return Memory.Stack.PopAndReturn(null);
	}
	
	public Result CollidesWith(Collision other)
	{
		Memory.Stack.Push();
		
		// convert all shapes from this, to be in other's local space
		Matrix4 inverseOtherTransform = other.mTransform.GetInverse();
		Matrix4 spaceTransform = mTransform.Multiply(inverseOtherTransform);
		for (int t = 0; t < mShape.size(); ++t)
    	{
    		Shape thisShape = mShape.get(t);
    		thisShape.ApplySpaceTransform(spaceTransform);
	
    		for (int o = 0; o < other.mShape.size(); ++o)
        	{
    			Shape otherShape = other.mShape.get(o);
    			otherShape.ApplySpaceTransform(null);
    			
    			Result result = thisShape.CollidesWith(otherShape);
    			if (result != null)
    				return Memory.Stack.PopAndReturnAll(result);
        	}
    	}
		
		return Memory.Stack.PopAndReturn(null);
	}
	
	public void InsertShape(Shape shape)
	{
		mShape.add(shape);
	}
	
	public void SetTransform(Matrix4 transform)
	{
		mTransform.Copy(transform);
	}
	
	public void RenderDebug()
	{
		// waste of time on Android
		if (Factory.Get().GetPlatform() == Factory.Platform.Android)
			return;
		
		Memory.Stack.Push();
		
		Device device = Factory.Get().GetDevice();
	
		device.SetMatrix(mTransform, Device.MatrixMode.Model);
		for (int t = 0; t < mShape.size(); ++t)
    	{
			Shape shape = mShape.get(t);
			shape.ApplySpaceTransform(null);
			shape.RenderDebug();
    	}
		
		Memory.Stack.Pop();
	}
	
	public Vector<Shape>			mShape 						= new Vector<Shape>();
	public Matrix4					mTransform					= Memory.Heap.New(Matrix4.class);
}
