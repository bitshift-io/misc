package BreakoutHD;

import BlackCarbon.API.Memory;
import BlackCarbon.Math.Vector4;

// determines movement path of enemies
public class EnemyPath
{	
	public class LinearPath extends EnemyPath
	{
		public Vector4 Update()
		{
			// update movement, occurs for all states
			Vector4 velocityThisFrame = Memory.Stack.New(Vector4.class);
			velocityThisFrame.Copy(mVelocity);
			velocityThisFrame = velocityThisFrame.Multiply(Game.Get().GetDeltaTime());	
			
			Vector4 translation = mTranslation.Add(velocityThisFrame);
			mTranslation.Copy(translation);
			return mTranslation;
		}

		public Vector4 		mVelocity 			= null;
	}
	
	// update and return current translation
	public Vector4 Update()
	{
		mCurrentTime += Game.Get().GetDeltaTime();
		
		return mTranslation;
	}
	
	public boolean IsActive()
	{
		return mCurrentTime > mStartTime;
	}
	
	// initial delay to start this path
	public float		mStartTime		= 0.f;
	public Vector4 		mTranslation	= null;
	
	protected float		mCurrentTime	= 0.f;
}
