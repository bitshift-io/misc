package BreakoutHD;

import java.util.Vector;

import BlackCarbon.API.Camera;
import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Font;
import BlackCarbon.API.Memory;
import BlackCarbon.API.Network;
import BlackCarbon.API.Device.MatrixMode;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;

public class Game
{
	class LoadingThread extends Thread
	{
		public void run()
		{
			Memory.Stack.StackLevel stack = Memory.Stack.Push();
			
			Game game = (Game) Game.Get();
			Network network = Factory.Get().GetNetwork();
			Level level = game.GetLevel();
			
			//game.GetFactory().GetDevice().Begin();
			
			network.SetCallback(level);
			level.LoadGlobalResources();
			
			mGameMode.SetMode(GameMode.Mode.SinglePlayer);
			mGameMode.LoadGlobalResources();
			
			/*
			Actor actor = (Actor)level.LoadFromTemplate("PaddleBottom0");
	    	if (actor != null)
	    	{	
		    	actor.SetDirector(new Director(Director.State.Player));
		    	level.InsertEntity(actor);
	    	}
	    	
	    	actor = (Actor)level.LoadFromTemplate("PaddleTop0");
	    	if (actor != null)
	    	{	
		    	actor.SetDirector(new Director(Director.State.Player));
		    	level.InsertEntity(actor);
	    	}*/
	    	
	    	level.SetState(Level.State.Loaded);
	    	
	    	// loading complete, so disable UI and show the game
	    	UserInterface ui = game.GetUserInterface();
	    	ui.SetVisible(false);
	    	
	    	//game.GetFactory().GetDevice().End();

	    	Memory.Stack.Pop(stack);
		}
	}
	
	public Game()
	{
		mGame = this;
	}
	
	public float GetDeltaTime()
    {
    	return mLastFrameTimeDelta;
    }

	public boolean IsDebug()
	{
		return mDebug;
	}
	
    public synchronized boolean Init()
    {
    	// only enable debug mode on PC in debug mode
    	Memory.SetDebugMode(false);
    	if (IsDebug() && Factory.Get().GetPlatform() == Factory.Platform.Desktop)
    	{
    		Memory.SetDebugMode(true);
    	}
    	
    	Memory.Stack.StackLevel stack = Memory.Stack.Push();
    	System.out.println("Initializing Game...");
    	
    	/*
    	// testing network code
    	try {
    		
    		ServerSocket server = new ServerSocket(25);
    		server.setSoTimeout(1000);
    		
    		
			//Socket client = new Socket("localhost", 25);
			
			Socket self = server.accept();
			
			int nothing = 0;

			
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
    	 
    	
    	mDebug = true;
        mFont = Factory.Get().AcquireFont("mini");
        mFont.SetHorizontalAlign(Font.HorizontalAlign.Right);
        mFont.SetVerticalAlign(Font.VerticalAlign.Below);	
    	//Factory.Get().GetDevice().SetTexture(mDebugFont.mTexture);
    		
    	mGameMode = Memory.Heap.New(GameMode.class);
    	mLevel = Memory.Heap.New(Level.class);
    	mUserInterface = Memory.Heap.New(UserInterface.class);
    	
    	// set up the loading UI
    	if (mUserInterface != null)
    	{
	    	mUserInterface.LoadGlobalResources();
	    	mUserInterface.ShowUI("loading");
    	}
    	
    	// temp
    	Factory factory = Factory.Get();
    	factory.GetDevice().End();
    	
    	// set up a loading thread to load game
    	LoadingThread loadingThread = new LoadingThread();
    	
    	// is this causing textures to not work correctly?
    	//loadingThread.start();
    	loadingThread.run();
    	/*
    	*/
    	
    	//GetFactory().GetDevice().Begin();
    	/*
    	try 
    	{
    		TimeUnit.MILLISECONDS.sleep(1000 * 100);
		} 
    	catch (InterruptedException e) 
		{
			e.printStackTrace();
		}*/
    	
    	//GetFactory().GetDevice().End();
    	
    	//while (loadingThread.i)
    	//loadingThread.run();
    	
    	/*
    	Actor actor = (Actor)mWorld.LoadFromTemplate(Actor.class);
    	if (actor != null)
    	{	
	    	actor.SetDirector(Director.Create(Director.Type.Player));
	    	mWorld.InsertActor(actor);
    	}
    	
    	// debugging
    	//AndroidFactory factory = (AndroidFactory) GetFactory();
    	//AlertDialog alertDialog = new AlertDialog.Builder(factory.GetInitParam().activity).create();
    	//alertDialog.setTitle("Init complete");
    	//alertDialog.show();
    	*/
    	
    	System.out.println("Initializing Game\t\t[OK]");
    	
    	mLastFrameTimeDelta = 1;
    	mCurrentFrameTime = System.currentTimeMillis();
    	Memory.Stack.Pop(stack);
    	return true;
    }
    
    public synchronized boolean Update()
	{	
    	++mUpdateCount;
		long curTime = System.currentTimeMillis();
		if (curTime >= (mUpdateTimer + 1000))
		{
			mUpdateTimer = curTime;
			mUpdateThisSecond = mUpdateCount;
			mUpdateCount = 0;
		}
		
		mLastFrameTimeDelta = ((float)(curTime - mCurrentFrameTime) / 1000.f);
		mCurrentFrameTime = curTime;
		
		
    	Memory.Stack.StackLevel stack = Memory.Stack.Push();
    	
    	mGameMode.Update();
    	mLevel.Update();
    	
    	if (mUserInterface != null)
    		mUserInterface.Update();

    	Memory.Stack.Pop(stack);
		return Factory.Get().Update();
	}
	
	public synchronized void Render()
	{	
    	++mRenderCount;
		long curTime = System.currentTimeMillis();
		if (curTime >= (mRenderTimer + 1000))
		{
			mRenderTimer = curTime;
			mRenderThisSecond = mRenderCount;
			mRenderCount = 0;
		}
		
		
		
    	Memory.Stack.StackLevel stack = Memory.Stack.Push();
    	
    	Device device = Factory.Get().GetDevice();
    	device.Begin();
    	device.Clear(Device.CF_Colour | Device.CF_Depth);

    	mLevel.Render();    
    	
    	if (mUserInterface != null)
    		mUserInterface.Render();
/*
    	if (IsDebug())
    	{
	    	// render frame rate
	    	Camera uiCamera = Memory.Stack.New(Camera.class);
	    	uiCamera.SetOrthographic(2.f, 2.f, 1.f, 15.f);
	    	uiCamera.Bind();

			Matrix4 transform = Memory.Stack.New(Matrix4.class);
			transform.SetIdentity();
			float scale = 0.006f;
			
			Vector4 vScale = Memory.Stack.New(Vector4.class);
			vScale.Set(scale, scale, 1.f, 0.f);
			transform.SetScale(vScale);
			
			Vector4 translation = Memory.Stack.New(Vector4.class);
			translation.Set(-1.f, 0.f, 0.f, 1.f);
			transform.SetTranslation(translation);
			device.SetMatrix(transform, MatrixMode.Model);
			/*
			// set device state - enable alpha blending
	    	Device.DeviceState state = Memory.Stack.New(Device.DeviceState.class);
	    	state.alphaBlendEnable = true;
	    	device.SetDeviceState(state);* /
			
			// set device state - enable alpha blending
	    	Device.DeviceState state = Memory.Stack.New(Device.DeviceState.class);
	    	state.alphaBlendEnable = true;
	    	state.alphaCompare = Device.Compare.Greater;
	    	state.alphaBlendSource = Device.BlendMode.One;
	    	state.alphaBlendDest = Device.BlendMode.One;
	    	device.SetDeviceState(state);

	    	String debugString = "" + mRenderThisSecond; //"FPS " + mRenderThisSecond + "\nUPS " + mUpdateThisSecond;
	    	Vector<Network.Connection> connectionList = Factory.Get().GetNetwork().GetConnectionList();
	    	if (connectionList.size() > 0)
	    	{
	    		debugString += "\nNetCon " + connectionList.size();
	    		/*
	    		for (int i = 0; i < connectionList.size(); ++i)
	    		{
	    			Network.Connection connection = connectionList.get(i);
	    			debugString += "\n " + i + ". " + connection.GetAddress().toString() 
	    				+ "\n      in: " + Integer.toString(connection.udpBytesReceived)
	    				+ "\n      out: " + Integer.toString(connection.udpBytesSent);
	    		}* /
	    	}
			mFont.Render(debugString);
			/*
			// output accelerometer readings
			transform.SetTranslation(new Vector4(-1.f, 0.5f));
			device.SetMatrix(transform, MatrixMode.Model);
			
			try
			{
				float accelX = GetFactory().GetInput().GetState(Input.Control.AccelerometerX);
				float accelY = GetFactory().GetInput().GetState(Input.Control.AccelerometerY);
				float accelZ = GetFactory().GetInput().GetState(Input.Control.AccelerometerZ);
				
				mDebugFont.Render("Accel X " + accelX
						+ "\nAccel Y " + accelY
						+ "\nAccel Z " + accelZ);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			// disable alpha blending
			state.alphaBlendEnable = false;
	    	device.SetDeviceState(state);* /
    	}*/
    	
        device.End();
        Memory.Stack.Pop(stack);
	}
	
	GameMode GetGameMode()
	{
		return mGameMode;
	}
    
    Level GetLevel()
    {
    	return mLevel;
    }
    
    Font GetFont()
    {
    	return mFont;
    }
    
    UserInterface GetUserInterface()
    {
    	return mUserInterface;
    }
    
	public String GetPrimaryFontName()
	{
		return mPrimaryFontName;
	}

	static public Game Get()
	{
		return mGame;
	}
	
	private String			mPrimaryFontName	= "mini";
	
    public Font				mFont;
    private Level			mLevel;
    private UserInterface	mUserInterface;
    private GameMode		mGameMode;
    
	
	public boolean			mDebug					= false;
	
	public long				mRenderTimer			= 0;
	public int 				mRenderCount 			= 0;
	public int 				mRenderThisSecond 		= 0;
	
	public long				mUpdateTimer			= 0;
	public int 				mUpdateCount 			= 0;
	public int 				mUpdateThisSecond 		= 0;
	
	public long				mCurrentFrameTime		= 0;
	public float			mLastFrameTimeDelta		= 0;

	static public Game		mGame;
}