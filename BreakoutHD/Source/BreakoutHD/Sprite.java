package BreakoutHD;

import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Memory;
import BlackCarbon.API.XMLSerializable;
import BlackCarbon.API.XMLSerialize;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.API.RenderBuffer;
import BlackCarbon.API.Texture;
import BlackCarbon.Math.Vector4;

//
// simplifies handling of multiple frames of animation from a texture atlas
//
public class Sprite implements XMLSerializable
{
	// Animation flags
	static public int AF_None				= 0;
	static public int AF_Horizontal			= (1 << 0);
	static public int AF_Vertical			= (1 << 1);
	static public int AF_NoLoop				= (1 << 2);
	static public int AF_Paused				= (1 << 3);
	
	public Sprite() //String texture, int numWidth, int numHeight)
	{/*
		Factory factory = Game.Get().GetFactory();
		
		// set up a background
		mGeometry = factory.CreateRenderBuffer();
		mGeometry.CreateQuad(null);
		mTexture = factory.AcquireTexture(texture);
		
		mNumWidth = numWidth;
		mNumHeight = numHeight;
		*/
		mTransform.SetIdentity();
	}
	
	public void ResetAnimation()
	{
		mTime = 0.f;
		mCurWidth = 0;
		mCurHeight = 0;
		mAnimationFlags = mAnimationFlags & ~AF_Paused;
	}
	
	public void Update(float deltaTime)
	{
		if (mAnimationFlags == AF_None || (mAnimationFlags & AF_Paused) != 0)
			return;
		
		// deal with sprite animation
		mTime += deltaTime;
		
		float frameTime = 1.f / mFrameRate;
		if (mTime > frameTime)
		{
			mTime -= frameTime;
			
			if ((mAnimationFlags & AF_Horizontal) != 0)
			{
				if ((mCurWidth + 1) >= mNumWidth)
				{
					if ((mAnimationFlags & AF_NoLoop) != 0)
						mAnimationFlags |= AF_Paused;
					else
						mCurWidth = 0;
				}
				else
				{
					++mCurWidth;
				}
			}
				
			if ((mAnimationFlags & AF_Vertical) != 0)
			{
				if ((mCurHeight + 1) >= mNumHeight)
				{
					if ((mAnimationFlags & AF_NoLoop) != 0)
						mAnimationFlags |= AF_Paused;
					else
						mCurHeight = 0;
				}
				else
				{
					++mCurHeight;
				}
			}
		}
	}
	
	public void Render()
	{
		Memory.Stack.Push();
		
		Device device = Factory.Get().GetDevice();
		
		// set new texture matrix to scroll the texture
		Vector4 translation = Memory.Stack.New(Vector4.class);
		translation.Set((float) mCurWidth / (float)mNumWidth, (float)mCurHeight / (float)mNumHeight, 0.f, 1.f);
		Vector4 scale = Memory.Stack.New(Vector4.class);
		scale.Set(1.f / (float)mNumWidth, 1.f / (float)mNumHeight, 1.f, 0.f);
		Matrix4 t = Memory.Stack.New(Matrix4.class);
    	t.SetIdentity();
    	t.SetScale(scale);
    	t.SetTranslation(translation);
    	device.SetMatrix(t, Device.MatrixMode.Texture);
    	
    	// set the world matrix, apply sprite scale here
    	Matrix4 transform = Memory.Stack.New(Matrix4.class);
    	transform.Copy(mTransform);
    	Matrix4 modelScale = Memory.Stack.New(Matrix4.class);
    	modelScale.SetIdentity();
    	scale.Set(mScaleX, mScaleY, 1.f, 1.f);
    	modelScale.SetScale(scale);
    	transform = modelScale.MultiplyRotation(transform);
    	transform.SetTranslation(mTransform.GetTranslation());
    	device.SetMatrix(transform, Device.MatrixMode.Model);
    	/*
    	// set device state - enable alpha blending
    	Device.DeviceState state = device.new DeviceState();
    	state.alphaBlendEnable = true;
    	state.alphaCompare = Device.Compare.Greater;
    	device.SetDeviceState(state);*/
    	
    	// bind texture and draw
    	
    	device.SetTexture(mTexture);
    	device.SetRenderBuffer(mGeometry);
    	device.Render();
    	/*
    	mTexture.Bind();
    	mGeometry.Bind();
    	mGeometry.Render();
    	mGeometry.Unbind();
    	mTexture.Unbind();*/
		
		// restore original texture matrix
    	t.SetIdentity();
    	device.SetMatrix(t, Device.MatrixMode.Texture);
    	
    	Memory.Stack.Pop();
	}
	
	public void Resolve(XMLSerialize serialize)
	{
		if (mTextureName != null)
		{
			mTexture = Factory.Get().AcquireTexture(mTextureName);
			
			Device device = Factory.Get().GetDevice();
			device.Begin();
			
			mGeometry = Factory.Get().AcquireRenderBuffer("quad");
			if (mGeometry.GetIndexCount() <= 0)
			{
				mGeometry.CreateQuad(null);
				mGeometry.MakeStatic();
			}
/*
			mGeometry = Factory.Get().CreateRenderBuffer();
			mGeometry.SetUsage(RenderBuffer.Static); // TEMPORARY, DO NOT COMMIT
			mGeometry.CreateQuad(null);*/
			device.End();
		}
	}

	public XMLSerializable Clone()
	{
		return null;
	}
	
	public int		mAnimationFlags 		= AF_None;
	
	public float	mTime					= 0.f;
	public float	mFrameRate				= 12.f;
	
	public int		mCurWidth				= 0;
	public int		mCurHeight				= 0;
	
	public int		mNumWidth				= 1;
	public int		mNumHeight				= 1;
	
	public float	mScaleX					= 1.f;
	public float	mScaleY					= 1.f;
	
	public String			mTextureName;
	private Texture 		mTexture;
	public RenderBuffer 	mGeometry;
	
	public Matrix4			mTransform		= Memory.Heap.New(Matrix4.class);
}
