package BreakoutHD;

import java.util.Vector;

import BlackCarbon.API.Factory;
import BlackCarbon.API.Memory;
import BlackCarbon.API.Sound;
import BlackCarbon.API.XMLSerializable;
import BlackCarbon.API.XMLSerialize;
import BreakoutHD.Game;
import BreakoutHD.Sprite;
import BreakoutHD.Collision.Rect;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;

public class Ball extends Entity
{
	enum State
	{
		AttachedToEntity,
		Moving,
		Dead,
	}
	
	public static int CollideWithLevel 		= 1 << 0;
	public static int CollideWithActors 	= 1 << 1;
	public static int CollideWithEntites 	= 1 << 2;
	public static int CollideWithAll		= CollideWithLevel | CollideWithActors | CollideWithEntites;
	

	@Override
	public void Resolve(XMLSerialize serialize) 
	{			
		mImpactSound = Factory.Get().AquireSound("ballimpact"); // this should be moved to the factory
		/*
		// something weird with data here?
		// do this in data
		mCollision = Memory.Heap.New(Collision.class);
		Collision.Sphere sphere = Memory.Heap.New(Collision.Sphere.class);
		sphere.mSphere.Set(0.f, 0.f, 0.f, 15.f);
		mCollision.InsertShape(sphere);*/
	}
	
	public void AttachToEntity(Entity entity)
	{
		Memory.Stack.Push();
		mEntity = entity;
		mState = State.AttachedToEntity;
		Update_AttachedToEntity(); // teleport to attach position
		Memory.Stack.Pop();
	}
	
	public void Launch(Vector4 velocity)
	{
		mCollisionFlags = CollideWithAll;
		mVelocity.Copy(velocity);
		mState = State.Moving;
	}
	
	@Override
	public void Update()
	{
		Memory.Stack.StackLevel stack = Memory.Stack.Push();
		
		switch (mState)
		{
		case AttachedToEntity: // handled by actor when attached
			//result = Update_AttachedToEntity();
			break;
			
		case Moving:
			Update_Moving();
			break;
			
		case Dead:
			break;
		}
		
		Memory.Stack.Pop(stack);
	}
	
	public void Update_AttachedToEntity()
	{
		// get attach position from entity
		Actor actor = (Actor) mEntity;
		Vector4 translation = actor.GetAttachPoint();
		translation.SetZ(mOffsetZ);
		mSprite.mTransform.SetTranslation(translation);
		mCollision.SetTransform(mSprite.mTransform);
	}
	
	public void Update_Moving()
	{
		// update movement, occurs for all states
		Vector4 velocityThisFrame = Memory.Stack.New(Vector4.class);
		Vector4 translation = mSprite.mTransform.GetTranslation();
		velocityThisFrame.Copy(mVelocity);
		velocityThisFrame = velocityThisFrame.Multiply(Game.Get().GetDeltaTime());		
		translation = translation.Add(velocityThisFrame);
		/*
		Matrix4 transform = Memory.Stack.New(Matrix4.class);
		transform.Copy(mSprite.mTransform);
		transform.SetTranslation(translation);
		
		// set new matrix temporarily, check for collision
		// we set this properly after
		mCollision.SetTransform(transform);
		
		Vector4 collisionNormal = Memory.Stack.New(Vector4.class);
		collisionNormal.Set(0.f, 0.f, 0.f, 0.f);
		*/
		// check for collision with world		
		Level level = Game.Get().GetLevel();
		GameMode gameMode = Game.Get().GetGameMode();
		Vector<Actor> actorList = level.GetEntityList(Actor.class);
		
		if ((mCollisionFlags & CollideWithLevel) != 0)
		{
			// reflect velocity if we have gone off the board this frame
			// also adjust the translation to not be inside the collision
			
			// subtract radius from the half width/height to shrink the board 
			// so we can compare strait against ball translation and ignore its radius
			Collision.Sphere sphere = (Collision.Sphere) mCollision.mShape.get(0);
			
			// right of board
			float halfBoardWidth = level.mBoardWidth * 0.5f - sphere.mSphere.GetW();
			float distanceOffBoard = translation.GetX() - halfBoardWidth;
			if (distanceOffBoard > 0.f)
			{
				if (mImpactSound != null)
					mImpactSound.Play();
				translation.SetX(translation.GetX() - distanceOffBoard);
				mVelocity.SetX(-mVelocity.GetX());
			}
			
			// left of board
			distanceOffBoard = translation.GetX() + halfBoardWidth;
			if (distanceOffBoard < 0.f)
			{
				if (mImpactSound != null)
					mImpactSound.Play();
				translation.SetX(translation.GetX() - distanceOffBoard);
				mVelocity.SetX(-mVelocity.GetX());
			}
			

			// top of board
			float halfBoardHeight = level.mBoardHeight * 0.5f - sphere.mSphere.GetW();
			distanceOffBoard = translation.GetY() - halfBoardHeight;
			if (distanceOffBoard > 0.f)
			{
				translation.SetY(translation.GetY() - distanceOffBoard);
				mVelocity.SetY(-mVelocity.GetY());
				
				if (gameMode.GetMode() == GameMode.Mode.Versus)
				{
					// increase this players score
					Actor actor = actorList.get(GameMode.BottomActor);
					actor.GiveBall(this);
					
					//mState = State.Dead;
					return; // never remove! memory gc collection
				}
				
				if (mImpactSound != null)
					mImpactSound.Play();
			}
			
			// bottom of board
			distanceOffBoard = translation.GetY() + halfBoardHeight;
			if (distanceOffBoard < 0.f)
			{
				translation.SetY(translation.GetY() - distanceOffBoard);
				mVelocity.SetY(-mVelocity.GetY());
				
				if (gameMode.GetMode() == GameMode.Mode.Versus)
				{
					// increase this players score
					Actor actor = actorList.get(GameMode.TopActor);
					actor.GiveBall(this);
					
					//mState = State.Dead;
					return; // never remove! memory gc collection
				}
				else
				{
					Actor actor = actorList.get(GameMode.BottomActor);
					if (actor.GetScore().mLivesRemaining > 0)
					{
						actor.GiveBall(this);
						actor.GetScore().mLivesRemaining -= 1;
					}
					else
					{
						// end game
					}
					
					//mState = State.Dead;
					return; // never remove! memory gc collection
				}
			}
				
			
			/*
			
			Collision.Result cr = mCollision.CollidesWith(level.mCollision);
			if (cr != null)
			{
				// hit bottom of board, missed paddle, oh dear!
				if (gameMode.GetMode() == GameMode.Mode.Versus)
				{	
					// the bottom player missed the ball!
					if (cr.normal.GetY() > 0.f)
					{
						// increase this players score
						Actor actor = actorList.get(GameMode.TopActor);
						actor.GiveBall(this);
						
						//mState = State.Dead;
						return; // never remove! memory gc collection
					}
					
					// the top player missed the ball!
					if (cr.normal.GetY() < 0.f)
					{
						// increase this players score
						Actor actor = actorList.get(GameMode.BottomActor);
						actor.GiveBall(this);
						
						//mState = State.Dead;
						return; // never remove! memory gc collection
					}
				}
				else
				{
					if (cr.normal.GetY() > 0.f)
					{
						Actor actor = actorList.get(GameMode.BottomActor);
						if (actor.GetScore().mBallsRemaining > 0)
						{
							actor.GiveBall(this);
							actor.GetScore().mBallsRemaining -= 1;
						}
						else
						{
							// end game
						}
						
						//mState = State.Dead;
						return; // never remove! memory gc collection
					}
				}
				
				collisionNormal = collisionNormal.Add(cr.normal);
			}*/
		}
		
		// check for collision with actors
		//boolean willCollideWithActor = false;
		if ((mCollisionFlags & CollideWithActors) != 0)
		{
			for (int i = 0; i < actorList.size(); ++i)
	    	{
	    		Actor actor = actorList.get(i);
	    		Collision.Rect rect = (Collision.Rect) actor.mCollision.mShape.get(0);
	    		Collision.Sphere sphere = (Collision.Sphere) mCollision.mShape.get(0);
	    		
	    		Vector4 actorPosition = actor.GetPosition();
	    		
	    		// add radius to actor rectangle width to make this code easier
	    		float halfActorCollisionWidth = rect.mWidth * 0.5f + sphere.mSphere.GetW();
	    		float halfActorCollisionHeight = rect.mHeight * 0.5f + sphere.mSphere.GetW();
	    		
	    		// the ball is aligned in the x range to collide with the actor paddle
	    		boolean betweenX = translation.GetX() < (actorPosition.GetX() + halfActorCollisionWidth) 
    								&& translation.GetX() > (actorPosition.GetX() - halfActorCollisionWidth);


    			float actorCollisionY = (i == GameMode.BottomActor) ? actorPosition.GetY() + halfActorCollisionHeight : actorPosition.GetY() - halfActorCollisionHeight;
    			

    			
    			// push out based on if the actor is the top or bottom
    			if (i == GameMode.BottomActor && translation.GetY() < actorCollisionY) // bottom actor
    			{	
    				// disable actor collision, we have missed the ball!
    				if (!betweenX)
    				{
    					mCollisionFlags &= ~CollideWithActors;
    				}
    				else
    				{
    					if (mImpactSound != null)
    						mImpactSound.Play();
	    				translation.SetY(translation.GetY() + (actorCollisionY - translation.GetY())); 
	    				mVelocity.Copy(actor.GetBallReflectVector(mSprite.mTransform.GetTranslation(), mVelocity));
	    				mVelocity.SetY(Math.abs(mVelocity.GetY()));
	    				mEntity = actor;
    				}
    			}
    			else if (i == GameMode.TopActor && translation.GetY() > actorCollisionY) // top actor
    			{
    				// disable actor collision, we have missed the ball!
    				if (!betweenX)
    				{
    					mCollisionFlags &= ~CollideWithActors;
    				}
    				else
    				{	
    					if (mImpactSound != null)
    						mImpactSound.Play();
	    				translation.SetY(translation.GetY() - (actorCollisionY - translation.GetY()));
		    			mVelocity.Copy(actor.GetBallReflectVector(mSprite.mTransform.GetTranslation(), mVelocity));
	    				mVelocity.SetY(-Math.abs(mVelocity.GetY()));
	    				mEntity = actor;
    				}
    			}
    			
    			// work out x intercept at actorCollisionY, if this will hit the paddle, then draw the reflection vector
				// pos.y + time * velocity.y = actorCollisionY
				// time = (actorCollisionY - pos.y) / velocity.y
    			mRenderHitReflectIndicator = false;
				float interceptTime = (actorCollisionY - translation.GetY()) / mVelocity.GetY();
				if (interceptTime > 0.f)
				{
					// now we want to find x
					float xPositionAtIntercept = translation.GetX() + interceptTime * mVelocity.GetX();
					
					if (xPositionAtIntercept < (actorPosition.GetX() + halfActorCollisionWidth) 
					&& xPositionAtIntercept > (actorPosition.GetX() - halfActorCollisionWidth))
					{
						mRenderHitReflectIndicator = true;
						
	
		    			// set up the hit/incomming ball vector indicator
		    			Vector4 up = Memory.Stack.New(Vector4.class);
		    			up.Set(0.f, 0.f, 1.f, 0.f);
		    			
		    			Vector4 forward = Memory.Stack.New(Vector4.class);
		    			//forward.Copy(mVelocity);
		    			//forward.SetW(0.f);
		    			//forward.Normalize3();
		    			//forward = forward.Multiply(-1.f);
		    			
		    			//Vector4 right = forward.Cross3(up);
		    			//right.Normalize3();
		    			
		    			//Vector4 spriteTranslation = cr.position.Subtract(forward.Multiply(-mHitSprite.mScaleX * 0.5f));
		    			//spriteTranslation.SetZ(mSprite.mTransform.GetTranslation().GetZ() + 0.1f); // different depths to avoid z-fighting
		    			
		    			//mHitSprite.mTransform.SetColumn(0, forward);
		    			//mHitSprite.mTransform.SetColumn(1, right);
		    			//mHitSprite.mTransform.SetTranslation(spriteTranslation);
		    			
		    			Vector4 interceptPosition = Memory.Stack.New(Vector4.class);
		    			interceptPosition.Set(xPositionAtIntercept, actorCollisionY, 0.f, 0.f);
		    			
		    			// now set up the reflection indicator
		    			Vector4 reflectionVector = actor.GetBallReflectVector(interceptPosition, mVelocity);
		    			forward.Copy(reflectionVector);
		    			forward.SetW(0.f);
		    			forward.Normalize3();
		    			//forward = forward.Multiply(-1.f); // make the vector point in towards the actor to swap the UV's around
		    			
		    			Vector4 right = forward.Cross3(up);
		    			right.Normalize3();
		    			
		    			Vector4 spriteTranslation = interceptPosition.Subtract(forward.Multiply(-mReflectSprite.mScaleX * 0.5f));
		    			spriteTranslation.SetZ(mSprite.mTransform.GetTranslation().GetZ() + 0.2f);  // different depths to avoid z-fighting
		    			
		    			mReflectSprite.mTransform.SetColumn(0, forward);
		    			mReflectSprite.mTransform.SetColumn(1, right);
		    			mReflectSprite.mTransform.SetTranslation(spriteTranslation);
					}
				}
		
	    		
	    		
	    		/*
	    		Collision.Result cr = mCollision.CollidesWith(actor.mCollision);
	    		if (cr != null)
	    		{
	    			// if we did not hit the top of the paddle,
	    			// ignore further collisions with actor... so we cant get stuck
	    			// inside the actor if he moves across the ball
	    			//if (cr.normal.GetY() <= 0.f)
	    			//{
	    			//	mCollisionFlags &= ~CollideWithActors;
	    			//}
	    			//else
	    			{
		    			// velocity should be set by the paddle here
		    			mVelocity.Copy(actor.GetBallReflectVector(mSprite.mTransform.GetTranslation(), mVelocity));
		    			
		    			mEntity = actor;
	    			}
	    		}
	    		
	    		// check if we will collide with actor in future on current path
	    		mRenderHitReflectIndicator = false;
	    		cr = actor.mCollision.RayCollide(mSprite.mTransform.GetTranslation(), mVelocity);
	    		if (cr != null)
	    		{
	    			mRenderHitReflectIndicator = true;
	    			willCollideWithActor = true;
	    			
	    			// set up the hit/incomming ball vector indicator
	    			Vector4 up = Memory.Stack.New(Vector4.class);
	    			up.Set(0.f, 0.f, 1.f, 0.f);
	    			
	    			Vector4 forward = Memory.Stack.New(Vector4.class);
	    			//forward.Copy(mVelocity);
	    			//forward.SetW(0.f);
	    			//forward.Normalize3();
	    			//forward = forward.Multiply(-1.f);
	    			
	    			//Vector4 right = forward.Cross3(up);
	    			//right.Normalize3();
	    			
	    			//Vector4 spriteTranslation = cr.position.Subtract(forward.Multiply(-mHitSprite.mScaleX * 0.5f));
	    			//spriteTranslation.SetZ(mSprite.mTransform.GetTranslation().GetZ() + 0.1f); // different depths to avoid z-fighting
	    			
	    			//mHitSprite.mTransform.SetColumn(0, forward);
	    			//mHitSprite.mTransform.SetColumn(1, right);
	    			//mHitSprite.mTransform.SetTranslation(spriteTranslation);
	    			
	    			// now set up the reflection indicator
	    			Vector4 reflectionVector = actor.GetBallReflectVector(cr.position, mVelocity);
	    			forward.Copy(reflectionVector);
	    			forward.SetW(0.f);
	    			forward.Normalize3();
	    			//forward = forward.Multiply(-1.f); // make the vector point in towards the actor to swap the UV's around
	    			
	    			Vector4 right = forward.Cross3(up);
	    			right.Normalize3();
	    			
	    			Vector4 spriteTranslation = cr.position.Subtract(forward.Multiply(-mReflectSprite.mScaleX * 0.5f));
	    			spriteTranslation.SetZ(mSprite.mTransform.GetTranslation().GetZ() + 0.2f);  // different depths to avoid z-fighting
	    			
	    			mReflectSprite.mTransform.SetColumn(0, forward);
	    			mReflectSprite.mTransform.SetColumn(1, right);
	    			mReflectSprite.mTransform.SetTranslation(spriteTranslation);
	    		}*/
	    	}
		}
		
		// check for collision with entites
		if ((mCollisionFlags & CollideWithEntites) != 0)
		{
			boolean collisionWithBrickThisFrame = false;
			Vector<Brick> brickList = level.GetEntityList(Brick.class);
			
			for (int i = 0; i < brickList.size(); ++i)
	    	{
				Entity entity = brickList.get(i);
				
				if (entity instanceof Brick)
				{
		    		Brick brick = brickList.get(i);
		    		if (!brick.CanCollide())
		    			continue;
		    		
		    		Collision.Rect rect = (Collision.Rect) brick.mCollision.mShape.get(0);
		    		Collision.Sphere sphere = (Collision.Sphere) mCollision.mShape.get(0);
		    		
		    		Vector4 brickPosition = brick.GetPosition();
		    		
		    		// add radius to actor rectangle width to make this code easier
		    		float halfBrickCollisionWidth = rect.mWidth * 0.5f + sphere.mSphere.GetW();
		    		float halfBrickCollisionHeight = rect.mHeight * 0.5f + sphere.mSphere.GetW();
		    		
		    		// new position is inside a brick!
		    		if (translation.GetX() < (brickPosition.GetX() + halfBrickCollisionWidth)
		    			&& translation.GetX() > (brickPosition.GetX() - halfBrickCollisionWidth)
		    			&& translation.GetY() < (brickPosition.GetY() + halfBrickCollisionHeight)
		    			&& translation.GetY() > (brickPosition.GetY() - halfBrickCollisionHeight))
		    		{
		    			brick.Damage(1.f, this);
		    			
		    			if (collisionWithBrickThisFrame == false)
		    			{
		    				if (mImpactSound != null)
	    						mImpactSound.Play();
		    				
		    				collisionWithBrickThisFrame = true;
			    			mVelocity.SetX(-mVelocity.GetX());
			    			mVelocity.SetY(-mVelocity.GetY());
		    			}
		    		}
		    		
		    		/*
		    		Collision.Result cr = mCollision.CollidesWith(brick.mCollision);
		    		if (cr != null)
		    		{
		    			collisionNormal = collisionNormal.Add(cr.normal);
		    			//mVelocity = mVelocity.Reflect(cr.normal);
		    			
		    			// notify brick it has been destroyed!
		    			brick.Damage(1.f, this);
		    			
		    			//return Keep;
		    		}*/
				}
	    	}
		}
		
		/*
		// if we arent going to collide with an actor,
		// check where we will collide with the world
		if (!willCollideWithActor)
		{
			
		}
		
		float mag = collisionNormal.Normalize3();
		if (mag > 0.f)
			mVelocity.Copy(mVelocity.Reflect3(collisionNormal));

		// apply movement, we do this after the collision check to make sure we never end up
		// inside the collision
		translation = mSprite.mTransform.GetTranslation();
		velocityThisFrame.Copy(mVelocity);
		velocityThisFrame = velocityThisFrame.Multiply(Game.Get().GetDeltaTime());		
		translation = translation.Add(velocityThisFrame);
		
		mSprite.mTransform.SetTranslation(translation);
		mCollision.SetTransform(mSprite.mTransform);*/
		
		mSprite.mTransform.SetTranslation(translation);
		
	}
	
	@Override
	public void Render()
	{
		if (mState == State.Dead)
			return;
		
		mSprite.Render();
		
		if (mRenderHitReflectIndicator)
		{
			//mHitSprite.Render();
			mReflectSprite.Render();
		}
		
		if (Game.Get().IsDebug())
			mCollision.RenderDebug();
	}
	
	public Entity GetEntity()
	{
		return mEntity;
	}
	
	public State GetState()
	{
		return mState;
	}
	
	public Sound		mImpactSound;
	public int			mCollisionFlags 	= CollideWithAll;
	public Collision	mCollision;
	public Vector4 		mVelocity 			= Memory.Heap.New(Vector4.class);
	public Sprite 		mSprite;
	public State		mState				= State.Dead;
	public Entity		mEntity				= null;
	public float		mOffsetZ			= 0.f;

	// hit indicator
	boolean				mRenderHitReflectIndicator = false;
	//public Sprite		mHitSprite;
	public Sprite		mReflectSprite;
}
