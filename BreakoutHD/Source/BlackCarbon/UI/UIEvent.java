package BlackCarbon.UI;

public class UIEvent 
{
	public enum Type
	{
		CursorOver,
		CursorLeave,
		CursorSelect,
		CursorDeselect,
	}
	
	public Type			type;
	public UICursor		cursor;		// the cursor that possibly caused the event
	public UIComponent	component; 	// component the event was triggered by
}
