package BlackCarbon.UI;

import java.util.Vector;

import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Font;
import BlackCarbon.API.Memory;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.API.RenderBuffer;
import BlackCarbon.API.Texture;
import BlackCarbon.Math.Vector4;
import BlackCarbon.API.XMLSerializable;
import BlackCarbon.API.XMLSerialize;
import BlackCarbon.API.Device.MatrixMode;

public class UIComponent implements XMLSerializable
{
	public UIComponent()
	{
		mTransform.SetIdentity();
	}
	
	public void SetText(String text)
	{
		mText = text;
	}
	
	public boolean ParseAttribute(XMLSerialize serialize, String name, String value)
	{/*
		// saving having mPosition as a redundant vector
		if (name.toLowerCase().equals("position"))
		{
			mTransform.SetTranslation(new Vector4(value));
		}
		else if (name.toLowerCase().equals("background"))
		{
			mBackground = Factory.Get().AcquireTexture(value);
			if (mRenderBuffer == null)
			{
				mRenderBuffer = Factory.Get().CreateRenderBuffer();
				mRenderBuffer.CreateQuad(null);
			}
		}
		*/
		return false;
	}
	
	public void	SetParent(XMLSerialize serialize, Object parent)
	{
		mParent = (UIComponent)parent;
	}
	
	public void	InsertChild(XMLSerialize serialize, Object child)
	{
		mChild.add((UIComponent)child);
	}
	
	public void Resolve(XMLSerialize serialize)
	{
		mTransform.SetTranslation(mPosition);
		
		if (mBackgroundName != null)
		{
			mBackground = Factory.Get().AcquireTexture(mBackgroundName);
			if (mRenderBuffer == null)
			{
				mRenderBuffer = Factory.Get().CreateRenderBuffer();
				mRenderBuffer.CreateQuad(null);
			}
		}
		
		for (int i = 0; i < mChild.size(); ++i)
		{
			mChild.get(i).Resolve(serialize);
			mChild.get(i).SetParent(this);
		}
	}
	
	public void SetParent(UIComponent parent)
	{
		mParent = parent;
	}
	
	public XMLSerializable Clone()
	{
		return null;
	}
	
	public void Update(UICursor cursor)
	{
		if (cursor == null)
			return;
		
		Memory.Stack.Push();

		Vector4 translation = cursor.GetTranslation();
		Vector4 translationLastFrame = cursor.GetTranslationLastFrame();
		
		// convert cursor position in to buttons local space, then test to see if
		// its in the region
		Matrix4 transformInverse = GetTransform().GetInverse();

		Vector4 localSpaceTranslation = transformInverse.Multiply3(translation);
		Vector4 localSpaceTranslationLastFrame = transformInverse.Multiply3(translationLastFrame);
		
		boolean cursorOverLastFrame = Math.abs(localSpaceTranslationLastFrame.GetX()) < mWidth && Math.abs(localSpaceTranslationLastFrame.GetY()) < mHeight;
		boolean cursorOver = Math.abs(localSpaceTranslation.GetX()) < mWidth && Math.abs(localSpaceTranslation.GetY()) < mHeight;
		
		if (cursorOver && !cursorOverLastFrame)
		{
			UIEvent event = Memory.Stack.New(UIEvent.class);
			event.component = this;
			event.cursor = cursor;
			event.type = UIEvent.Type.CursorOver;
			NotifyEvent(event);
		}
		
		if (!cursorOver && cursorOverLastFrame)
		{
			UIEvent event = Memory.Stack.New(UIEvent.class);
			event.component = this;
			event.cursor = cursor;
			event.type = UIEvent.Type.CursorLeave;
			NotifyEvent(event);
		}
		
		if (cursorOver && cursor.GetActionPressed(UICursor.Action.LeftCursor))
		{
			UIEvent event = Memory.Stack.New(UIEvent.class);
			event.component = this;
			event.cursor = cursor;
			event.type = UIEvent.Type.CursorSelect;
			NotifyEvent(event);
		}
		
		for (int i = 0; i < mChild.size(); ++i)
		{
			mChild.get(i).Update(cursor);
		}
		
		Memory.Stack.Pop();
	}
	
	public void NotifyEvent(UIEvent event)
	{
		// pass to parent, the top level container can handle events
		if (mParent != null)
			mParent.NotifyEvent(event);
	}
	
	public void Render()
	{
		// not liking this method to get the device as it relys on using the game class :-/
		// maybe have the xmlserilaiser to hold some custom attributes we can access in the resolve
		// etc
		// some form of 
		// UITemplate
		// {
		//  device
		//  defaultFont
		// }
		Memory.Stack.Push();
		Device device = Factory.Get().GetDevice();
		
		Matrix4 transform = GetTransform();

		// set device state - enable alpha blending
    	Device.DeviceState state = Memory.Stack.New(Device.DeviceState.class);
    	state.alphaBlendEnable = true;
    	state.alphaCompare = Device.Compare.Greater;
    	device.SetDeviceState(state);
    	
		if (mBackground != null)
			RenderQuad(mBackground, transform);
		
		// as rendering quad does some scaling
		device.SetMatrix(transform, MatrixMode.Model);
		
		Font font = GetFont();
		if (font != null)
		{
			font.Render(mText);
		}
		
		// set device state - disable alpha blending, clear world matrix
    	state.alphaBlendEnable = false;
    	device.SetDeviceState(state);
    	
		RenderDebug();
		
		for (int i = 0; i < mChild.size(); ++i)
		{
			mChild.get(i).Render();
		}
		
		Memory.Stack.Pop();
	}
	
	public void RenderDebug()
	{
		Device device = Factory.Get().GetDevice();
		
		Matrix4 transform = GetTransform();
		
		float halfWidth = mWidth * 0.5f;
		float halfHeight = mHeight * 0.5f;
		
		device.SetMatrix(transform, MatrixMode.Model);
		
		// draw debug border
		Vector4 topLeft = new Vector4(-halfWidth, halfHeight);
		Vector4 topRight = new Vector4(halfWidth, halfHeight);
		Vector4 bottomLeft = new Vector4(-halfWidth, -halfHeight);
		Vector4 bottomRight = new Vector4(halfWidth, -halfHeight);
		
		device.DrawLine(topLeft, topRight, new Vector4(1.f, 0.f, 0.f, 1.f));
		device.DrawLine(topRight, bottomRight, new Vector4(1.f, 0.f, 0.f, 1.f));
		device.DrawLine(bottomRight, bottomLeft, new Vector4(1.f, 0.f, 0.f, 1.f));
		device.DrawLine(bottomLeft, topLeft, new Vector4(1.f, 0.f, 0.f, 1.f));
	}
	
	public void RenderQuad(Texture texture, Matrix4 transform)
	{
		if (mRenderBuffer == null)
			return;
		
		Memory.Stack.Push();
		
		Device device = Factory.Get().GetDevice();
		
		Matrix4 scale = Memory.Stack.New(Matrix4.class);
		scale.SetIdentity();
		
		Vector4 vScale = Memory.Stack.New(Vector4.class);
		vScale.Set(mWidth, mHeight, 0.f, 0.f);
		scale.SetScale(vScale);
		
		Matrix4 finalTransform = transform.Multiply(scale);
		device.SetMatrix(finalTransform, MatrixMode.Model);
		
		texture.Bind();
		mRenderBuffer.Bind();
		mRenderBuffer.Render();
		mRenderBuffer.Unbind();
		texture.Unbind();
		
		Memory.Stack.Pop();
	}
	
	public Font GetFont()
	{
		if (mFont != null)
			return mFont;
		
		if (mParent == null)
			return null;
		
		return mParent.GetFont();
	}
	
	public float GetWidth()
	{
		return mWidth;
	}
	
	public float GetHeight()
	{
		return mHeight;
	}
	
	public Vector4 GetTranslation()
	{
		Memory.Stack.Push();
		Vector4 translation = mTransform.GetTranslation();
		return Memory.Stack.PopAndReturn(translation);
	}
	
	public void SetTranslation(Vector4 translation)
	{
		mTransform.SetTranslation(translation);
	}
	
	public Matrix4 GetTransform()
	{
		return mTransform;
	}
	
	public String GetCommand()
	{
		return mCommand;
	}
	
	public int GetTabIndex()
	{
		return mTabIndex;
	}
	
	public RenderBuffer mRenderBuffer		= null;
	
	public Texture	mBackground				= null;
	public int		mTabIndex				= -1;
	public String	mCommand;
	public float	mWidth;
	public float	mHeight;
	public String	mText;
	public Matrix4	mTransform				= Memory.Heap.New(Matrix4.class);
	public Font		mFont					= null;
	
	public Vector4	mPosition				= null;
	public String	mBackgroundName			= null;
	
	public UIComponent 			mParent		= null;
	public Vector<UIComponent> 	mChild		= new Vector<UIComponent>();
}
