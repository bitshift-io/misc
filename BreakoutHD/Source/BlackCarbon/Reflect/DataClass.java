package BlackCarbon.Reflect;

import java.util.Vector;

//
//a data class
//has the form:
//
//[name : extends | property1 | property2]
//(
//)
//
public class DataClass
{
	protected String 				mName;
	protected String 				mExtendName;
	protected DataClass				mExtendClass;
	
	protected Vector<String>		mProperty;
	protected Vector<DataVariable>	mVariable;
	protected Vector<DataClass>		mClass;
}
