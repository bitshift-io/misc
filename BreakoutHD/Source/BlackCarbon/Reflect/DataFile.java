package BlackCarbon.Reflect;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.util.Vector;

public class DataFile
{
	// line means line of data file
	public enum LineType
	{
		Unknown,
		Ignore,
		Class,
		Parameter,
		Preprocessor,
		BlockBegin,
		BlockEnd,
	}
	
	public enum Token
	{
		Unknown,
		PreprocessorBegin,
		PreprocessorEnd,
		BlockBegin,
		BlockEnd,
		ClassHeaderBegin,
		ClassHeaderEnd,
	}
	
	public enum PreprocessorType
	{
		Unknown,
		Include,
	}
	
	public boolean Open(String filename)
	{
		File f = new File(filename);
		if (!f.exists())
			return false;
	
		//StringBufferInputStream stream = new StringBufferInputStream();
		//InputStream is = new InputStream(filename);
		//BufferedInputStream bis = new BufferedInputStream(new InputStream());
		
		return true;
	}
	
	protected LineType ClassifyLine(String line, String inner)
	{
		return LineType.Unknown;
	}
	
	protected DataClass CreateClassFromLine(String line)
	{
		return null;
	}
	
	protected boolean ReadVariable()
	{
		return false;
	}
	
	protected File				mFile;
	protected Vector<DataClass>	mClass;
}
