package BlackCarbon.Reflect;

import java.util.Vector;

//
//Parameters are contained with in a class
//and are the form:
//
//some_string = text here
//
public class DataVariable
{
	public enum ValueType
	{
		String,
		DataClassInstance,
		DataClassPointer
	}
	
	public class Value
	{
		String				mString;
		DataClass			mClass;
		ValueType			mType;
	}
	
	protected String			mName;
	protected Vector<Value>		mValue;
}
