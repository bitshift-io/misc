package BlackCarbon.Android;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import BlackCarbon.API.Device;
import BlackCarbon.API.Memory;
import BlackCarbon.API.Device.DeviceState;
import BlackCarbon.Android.GLSurfaceView.Renderer;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;

import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;


public class AndroidDevice extends Device
{
	public AndroidDevice(AndroidFactory.InitParam param)
	{		
		param.activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
		param.activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		DisplayMetrics dm = Memory.Heap.New(DisplayMetrics.class);
		param.activity.getWindowManager().getDefaultDisplay().getMetrics(dm); 
		mHeight = dm.heightPixels; 
		mWidth = dm.widthPixels; 
		
		mGLView = new BlackCarbon.Android.GLSurfaceView(param.activity);
		mGLView.setRenderer((Renderer) param.activity);
		param.activity.setContentView(mGLView);
		param.view = mGLView;
	}
	
	public GL11 GetGL()
	{
		return mGL;
	}
	
	public void Resize(int width, int height)
	{
		GL10 gl = GetGL();
		
		mWidth  = width;
		mHeight = height;
		
		gl.glViewport(0, 0, width, height);
	
		// improve performance at cost of quality
		gl.glDisable(GL10.GL_DITHER);
/*
        /*
         * Set our projection matrix. This doesn't have to be done
         * each time we draw, but usually a new projection needs to be set
         * when the viewport is resized.
         * /

        float ratio = (float)width / height;
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glFrustumf(-ratio, ratio, -1, 1, 2, 12);

        /*
         * By default, OpenGL enables features that improve quality
         * but reduce performance. One might want to tweak that
         * especially on software renderer.
         * /
        gl.glDisable(GL10.GL_DITHER);
        gl.glActiveTexture(GL10.GL_TEXTURE0);*/
	}
	
	@Override
	public void SetClearColour(Vector4 colour)
	{
		GL10 gl = GetGL();
		gl.glClearColor(colour.GetX(), colour.GetY(), colour.GetZ(), colour.GetW());
	}
	
	@Override
	public void Clear(int clearFlags)
	{
		GL11 gl = GetGL();
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
	}
	
	@Override
	public void Begin()
	{
		AcquireSemaphore();
		//Memory.Stack.Push();
		
		try 
		{
			mGLView.MakeCurrent();
		} 
		catch (Exception e)
		{
			System.out.println("[AndroidDevice::Begin] Exception: " + e.getMessage());
		}
		/*
		// restore default rendering
		Matrix4 identity = Memory.Stack.New(Matrix4.class);
		identity.SetIdentity();

		SetMatrix(identity, MatrixMode.Model);
		SetMatrix(identity, MatrixMode.Texture);
		SetMatrix(identity, MatrixMode.View);
		SetMatrix(identity, MatrixMode.Projection);
		
		Memory.Stack.Pop();*/
	}
	
	@Override
	public void End()
	{/*
		try
		{
			Display.releaseContext();
		} 
		catch (Exception e)
		{
			System.out.println("[AndroidDevice::End] Exception: " + e.getMessage());
		}*/
		
		ReleaseSemaphore();
	}
	
	@Override
	public void SetMatrix(Matrix4 matrix, MatrixMode mode)
	{
		Memory.Stack.Push();
		GL11 gl = GetGL();
		
		switch (mode)
		{
		case Texture:
		{
			mTextureMatrix.Copy(matrix);
			gl.glMatrixMode(GL11.GL_TEXTURE);
			LoadMatrix(mTextureMatrix);
			break;
		}
			
		case Model:
		{
			mWorld.Copy(matrix);
			gl.glMatrixMode(GL11.GL_MODELVIEW);
			Matrix4 worldView = mWorld.Multiply(mView);
			LoadMatrix(worldView);
			break;
		}
		
		case View:
		{
			mView.Copy(matrix);
			gl.glMatrixMode(GL11.GL_MODELVIEW);
			Matrix4 worldView = mWorld.Multiply(mView);
			LoadMatrix(worldView);
			break;
		}
		
		case Projection:
		{
			mProjection.Copy(matrix);
			gl.glMatrixMode(GL11.GL_PROJECTION);
			LoadMatrix(mProjection);
			break;
		}
		}
		
		Memory.Stack.Pop();
	}
	
	protected void LoadMatrix(Matrix4 matrix)
	{
		GL11 gl = GetGL();
		
		FloatBuffer fb = FloatBuffer.allocate(16);
		
		fb.put(matrix.m[0]);
		fb.put(matrix.m[1]);
		fb.put(matrix.m[2]);
		fb.put(matrix.m[3]);
		
		fb.position(0);
		
		gl.glLoadMatrixf(fb);
	}
	
	@Override
	public Matrix4 GetMatrix(MatrixMode mode)
	{
		switch (mode)
		{
		case Texture:
			return mTextureMatrix;
			
		case Model:
			return mWorld;
		
		case View:
			return mView;
		
		case Projection:
			return mProjection;
		}
		
		return null;
	}
	
	@Override
	public int GetWidth()
	{
		return mWidth;
	}
	
	@Override
	public int GetHeight()
	{
		return mHeight;
	}
	
	@Override
	public float GetAspectRatio()
	{
		return (float)(mWidth) / (float)(mHeight);
	}
	
	@Override
	public DeviceState GetDeviceState()
	{
		return mDeviceState;
	}
	
	/*
	@Override
	public void SetDeviceState(DeviceState state)
	{
		GL11 gl = GetGL();
		
		if (state.alphaBlendEnable)
			gl.glEnable(GL11.GL_BLEND);
		else
			gl.glDisable(GL11.GL_BLEND);
			
		int alphaBlendSource = GetBlendMode(state.alphaBlendSource);
		int alphaBlendDest = GetBlendMode(state.alphaBlendDest);
		gl.glBlendFunc(alphaBlendSource, alphaBlendDest);
		
		if (state.alphaCompare == Device.Compare.Always)
			gl.glDisable(GL11.GL_ALPHA_TEST);
		else
			gl.glEnable(GL11.GL_ALPHA_TEST);
		
		int alphaCompare = GetCompare(state.alphaCompare);
		gl.glAlphaFunc(alphaCompare, state.alphaCompareValue);
	}*/
	

	@Override
	public void Render()
	{
		GL11 gl = GetGL();
		
		// depth functionality
		if ((mDeviceState.dirtyFlags & DeviceState.DF_DepthTestEnable) != 0)
		{
			if (mDeviceState.depthTestEnable)
				gl.glEnable(GL11.GL_DEPTH_TEST);
			else
				gl.glDisable(GL11.GL_DEPTH_TEST);
		}
		
		// alpha functionality
		if ((mDeviceState.dirtyFlags & DeviceState.DF_AlphaBlendEnable) != 0)
		{
			if (mDeviceState.alphaBlendEnable)
				gl.glEnable(GL11.GL_BLEND);
			else
				gl.glDisable(GL11.GL_BLEND);
		}
		
		if ((mDeviceState.dirtyFlags & (DeviceState.DF_AlphaBlendSource | DeviceState.DF_AlphaBlendDest)) != 0)
		{
			int alphaBlendSource = GetBlendMode(mDeviceState.alphaBlendSource);
			int alphaBlendDest = GetBlendMode(mDeviceState.alphaBlendDest);
			gl.glBlendFunc(alphaBlendSource, alphaBlendDest);
		}
		
		if ((mDeviceState.dirtyFlags & (DeviceState.DF_AlphaCompare | DeviceState.DF_AlphaCompareValue)) != 0)
		{
			if (mDeviceState.alphaCompare == Device.Compare.Always)
				gl.glDisable(GL11.GL_ALPHA_TEST);
			else
				gl.glEnable(GL11.GL_ALPHA_TEST);
			
			int alphaCompare = GetCompare(mDeviceState.alphaCompare);
			gl.glAlphaFunc(alphaCompare, mDeviceState.alphaCompareValue);
		}
		
		if ((mDeviceState.dirtyFlags & DeviceState.DF_Colour) != 0)
		{
			gl.glColor4f(mDeviceState.colour.GetX(), mDeviceState.colour.GetY(), mDeviceState.colour.GetZ(), mDeviceState.colour.GetW());
		}
		
		if ((mDirtyFlags & DF_Texture) != 0)
		{
			if (mTexture != null)
			{
				mTexture.Bind();
			}
			else
			{
				gl.glDisable(GL11.GL_TEXTURE_2D);
				gl.glBindTexture(GL11.GL_TEXTURE_2D, -1);
			}
		}
		
		if ((mDirtyFlags & DF_RenderBuffer) != 0)
		{
			// need to unbind incase going from VBO to vertex arrays
			mRenderBuffer.Unbind();
			mRenderBuffer.Bind();
		}
		
		mRenderBuffer.Render();
		
		mDeviceState.dirtyFlags = 0;
		mDirtyFlags = 0;
	}
	
	int GetCompare(Compare value)
	{/*
		public enum Compare
		{
			Less,
			LessEqual,
			Equal,
			Greater,
			GreaterEqual,
			Always, // TODO: fix device enums
			Never,
			NotEqual,
		}*/	
		
		int lookup[] = {
				GL11.GL_LESS, 
				GL11.GL_LEQUAL,
				GL11.GL_EQUAL, 
				GL11.GL_GREATER, 
				GL11.GL_GEQUAL,
				GL11.GL_ALWAYS,
				GL11.GL_NEVER,
				GL11.GL_NOTEQUAL, 
		};
		
		return lookup[value.ordinal()];
	}
	
	int GetBlendMode(BlendMode value)
	{/*
		public enum BlendMode
		{
			Zero,
			One,
			DestColour,
			SrcColour,
			OneMinusDestColour,
			OneMinusSrcColour,
			SrcAlpha,
			OneMinusSrcAlpha,
			DestAlpha,
			OneMinusDestAlpha,
			SrcAlphaSaturate,
		}
		*/
		int lookup[] = {
				GL11.GL_ZERO, 
				GL11.GL_ONE,
				GL11.GL_DST_COLOR, 
				GL11.GL_SRC_COLOR, 
				GL11.GL_ONE_MINUS_DST_COLOR,
				GL11.GL_ONE_MINUS_SRC_COLOR,
				GL11.GL_SRC_ALPHA,
				GL11.GL_ONE_MINUS_SRC_ALPHA, 
				GL11.GL_DST_ALPHA,
				GL11.GL_ONE_MINUS_DST_ALPHA, 
				GL11.GL_SRC_ALPHA_SATURATE
		};
		
		return lookup[value.ordinal()];
	}
	
	int GetTopology(GeometryTopology value)
	{
		/*
		public enum GeometryTopology
	{
		LineList,
		PointList,
		TriangleList,
		TriangleListAdjacent,
	}
	
		 */
		int lookup[] = {
				GL11.GL_LINES, 
				-1,
				GL11.GL_TRIANGLES, 
				//GL11.GL_TRIANGLES_ADJACENCY_EXT, 
				GL11.GL_TRIANGLE_STRIP,
				-1,
		};
		
		return lookup[value.ordinal()];
	}
	
	public GL11 		mGL;

	private int			mWidth;
	private int			mHeight;
	
	private BlackCarbon.Android.GLSurfaceView 	mGLView;
}


