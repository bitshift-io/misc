package BlackCarbon.Android;

import BlackCarbon.API.Factory;
import BlackCarbon.API.Sound;

public class AndroidSound extends Sound
{
	@Override
	public void Play()
	{
		AndroidAudio audio = (AndroidAudio)Factory.Get().GetAudio();
		mStreamId = audio.mSoundPool.play(mId, 1.f, 1.f, 0, 0, 1.f);
	}
	
	@Override
	public void Stop()
	{
		AndroidAudio audio = (AndroidAudio)Factory.Get().GetAudio();
		audio.mSoundPool.stop(mStreamId);
	}
	
	public int mId 			= -1;
	public int mStreamId 	= -1;
}
