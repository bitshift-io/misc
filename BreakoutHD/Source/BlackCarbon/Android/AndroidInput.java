package BlackCarbon.Android;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Input;

public class AndroidInput extends Input implements SensorEventListener, OnTouchListener, OnKeyListener
{
	AndroidInput()
	{
		super();
		
		AndroidFactory factory = (AndroidFactory) Factory.Get();
		Activity activity = factory.GetInitParam().activity;
		View view = factory.GetInitParam().view;
		
		SensorManager sensorManager = (SensorManager)activity.getSystemService(Context.SENSOR_SERVICE);
		
		List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER); 
        if (sensors.size() > 0) 
        { 
        	Sensor sensor = sensors.get(0); 
        	sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
        } 
        
        view.setOnKeyListener(this);
        view.setOnTouchListener(this);
	}
	
	@Override
	public void Update()
	{
		super.Update(); 
		
		if (mAccelerometer != null)
		{
			mState[Input.Control.AccelerometerX.ordinal()].state = mAccelerometer.values[0];
			mState[Input.Control.AccelerometerY.ordinal()].state = mAccelerometer.values[1];
			mState[Input.Control.AccelerometerZ.ordinal()].state = mAccelerometer.values[2];
		}
		
		TouchState touchState = (TouchState)GetControlState(Input.Control.Touch);
		if (mTouchEvent != null)
		{
			touchState.state = 1.f;
			touchState.x = mTouchEvent.getX();
			touchState.y = mTouchEvent.getY();
			touchState.pressure = mTouchEvent.getPressure();
			touchState.size = mTouchEvent.getSize();
			/*
			float curState = touchState.state;
			float newState = Keyboard.getEventKeyState() ? 1.f : 0.f;
			mState[control.ordinal()].state = newState;
			if (curState != newState && newState <= 0.f)
				mState[control.ordinal()].pressed = true;
			
			mState[Input.Control.TouchX.ordinal()].state = mTouchEvent.getX();
			mState[Input.Control.TouchY.ordinal()].state = mTouchEvent.getY();
			mState[Input.Control.TouchPressure.ordinal()].state = mTouchEvent.getPressure();
			mState[Input.Control.TouchSize.ordinal()].state = mTouchEvent.getSize();*/
			
			mTouchEvent = null;
		}
		else
		{
			touchState.pressed = false;
			if (touchState.state > 0.f)
				touchState.pressed = true;
			
			touchState.state = 0.f;
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) 
	{
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) 
	{
		//mAccelerometerValues[0] = event.values[0];
		//mAccelerometerValues[1] = event.values[1];
		//mAccelerometerValues[2] = event.values[2];
		
		mAccelerometer = event;
	}
	
	@Override
	public boolean onTouch(View view, MotionEvent event) 
	{
		mTouchEvent = event;
		return true;
	}

	@Override
	public boolean onKey(View view, int arg1, KeyEvent event)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	SensorEvent	mAccelerometer;
	MotionEvent	mTouchEvent;

	
	/*
	public Control ControlLookup(int VK)
	{
		for (int i = 0; i < mControlLookup.length; ++i)
		{
			if (mControlLookup[i].VK == VK)
				return mControlLookup[i].control;
		}
		
		return Control.Unknown;
	}
	
	class ControlLookup
	{
		public ControlLookup(int _VK, Control _control)
		{
			VK = _VK;
			control = _control;
		}
		
		public int 		VK;
		public Control 	control;
	}
	
	protected ControlLookup[] mControlLookup =
	{
			new ControlLookup(Keyboard.KEY_ESCAPE, Control.KeyEscape),
			new ControlLookup(Keyboard.KEY_RETURN, Control.KeyEnter),
			new ControlLookup(Keyboard.KEY_LEFT, Control.KeyLeft),
			new ControlLookup(Keyboard.KEY_RIGHT, Control.KeyRight),
			new ControlLookup(Keyboard.KEY_UP, Control.KeyUp),
			new ControlLookup(Keyboard.KEY_DOWN, Control.KeyDown),
	};*/
}
