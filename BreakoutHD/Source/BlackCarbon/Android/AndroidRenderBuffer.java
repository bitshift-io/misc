package BlackCarbon.Android;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL11;

import BlackCarbon.API.Factory;
import BlackCarbon.API.RenderBuffer;
import BlackCarbon.Math.Vector4;

public class AndroidRenderBuffer extends RenderBuffer
{
	private void BindStatic()
	{
		GL11 gl = ((AndroidDevice)Factory.Get().GetDevice()).GetGL();
		if (mPositionID != -1)
		{
			gl.glEnableClientState(GL11.GL_VERTEX_ARRAY);
			gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, mPositionID);
			gl.glVertexPointer(3, GL11.GL_FLOAT, 0, 0);
		}
		else
		{
			gl.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		}
		
		if (mColourID != -1)
		{
			gl.glEnableClientState(GL11.GL_COLOR_ARRAY);
			gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, mColourID);
			gl.glColorPointer(4, GL11.GL_FLOAT, 0, 0);
		}
		else
		{
			gl.glDisableClientState(GL11.GL_COLOR_ARRAY);
		}
        
		if (mUVID != -1)
		{
			gl.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
			gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, mUVID);
			gl.glTexCoordPointer(2, GL11.GL_FLOAT, 0, 0);
		}
		else
		{
			gl.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		}
		
		if (mIndexID != -1)
		{
			gl.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, mIndexID);
		}
	}
	
	private void BindDynamic()
	{
		GL11 gl = ((AndroidDevice)Factory.Get().GetDevice()).GetGL();
		if (mPosition != null)
		{
			gl.glEnableClientState(GL11.GL_VERTEX_ARRAY);
			gl.glVertexPointer(3, GL11.GL_FLOAT, 0, mPosition);
		}
		else
		{
			gl.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		}
		
		if (mColour != null)
		{
			gl.glEnableClientState(GL11.GL_COLOR_ARRAY);
			gl.glColorPointer(4, GL11.GL_FLOAT, 0, mColour);
		}
		else
		{
			gl.glDisableClientState(GL11.GL_COLOR_ARRAY);
		}
        
		if (mUV != null)
		{
			gl.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
			gl.glTexCoordPointer(2, GL11.GL_FLOAT, 0, mUV);
		}
		else
		{
			gl.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		}
	}
	
	public void Bind()
	{
		if (mUsage == Dynamic)
			BindDynamic();
		else
			BindStatic();
	}
	
	public void Unbind()
	{	
		GL11 gl = ((AndroidDevice)Factory.Get().GetDevice()).GetGL();
		
		gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, 0);
		gl.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, 0);
		
		gl.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL11.GL_COLOR_ARRAY);
		gl.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
	}
	
	public void Render()
	{	
		GL11 gl = ((AndroidDevice)Factory.Get().GetDevice()).GetGL();
//		gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
//		gl.glDisable(GL11.GL_CULL_FACE); // double sided plz for a bit
//		gl.glFrontFace(GL11.GL_CW);

		try
		{
			if (mUsage == Dynamic)
				gl.glDrawElements(GL11.GL_TRIANGLES, mIndex.capacity(), GL11.GL_UNSIGNED_BYTE, mIndex);
			else
				gl.glDrawElements(GL11.GL_TRIANGLES, mIndexCount, GL11.GL_UNSIGNED_BYTE, 0);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private int CreateVBOWithData(FloatBuffer data, boolean fixed)
	{
		GL11 gl = ((AndroidDevice)Factory.Get().GetDevice()).GetGL();
		/*
		IntBuffer fixedData = null;
		if (fixed)
		{
			ByteBuffer bb = ByteBuffer.allocateDirect(data.capacity() * 4);
			bb.order(ByteOrder.nativeOrder());
		    fixedData = bb.asIntBuffer();
		    
			for (int d = 0; d < data.capacity(); ++d)
			{
				float f = data.get(d);
				
				int i = (int)(f * 65536);
				fixedData.put((int)i);
				data.put(d, f * 65536);
			}
		}*/

		IntBuffer buffer = ByteBuffer.allocateDirect(4).order(ByteOrder.nativeOrder()).asIntBuffer();
		gl.glGenBuffers(1, buffer);
		int id = buffer.get(0);
		
		gl.glBindBuffer(GL11.GL_ARRAY_BUFFER, id);
		gl.glBufferData(GL11.GL_ARRAY_BUFFER, data.capacity() * 4, /*fixed ? fixedData : */data, GL11.GL_STATIC_DRAW);

	    return id;
	}

	@Override
	public void MakeStatic()
	{
		mUsage = Static;
		
		AndroidDevice device = ((AndroidDevice)Factory.Get().GetDevice());
		device.Begin();
		
		// index buffer to VBO
		mIndexCount = mIndex.capacity();
		
		GL11 gl = ((AndroidDevice)Factory.Get().GetDevice()).GetGL();

		IntBuffer buffer = ByteBuffer.allocateDirect(4).order(ByteOrder.nativeOrder()).asIntBuffer();
		gl.glGenBuffers(1, buffer);
		mIndexID = buffer.get(0);
		
		gl.glBindBuffer(GL11.GL_ELEMENT_ARRAY_BUFFER, mIndexID);
		gl.glBufferData(GL11.GL_ELEMENT_ARRAY_BUFFER, mIndexCount, mIndex, GL11.GL_STATIC_DRAW);
		
		mIndex = null;
		
		if (mPosition != null)
		{
			mPositionID = CreateVBOWithData(mPosition, false);
			mPosition = null;
		}
		
		if (mColour != null)
		{
			mColourID = CreateVBOWithData(mColour, false);
			mColour = null;
		}
		
		if (mUV != null)
		{
			mUVID = CreateVBOWithData(mUV, false);
			mUV = null;
		}
		
		Unbind();
		device.End();
	}
	
	@Override
	public int GetIndexCount()
	{
		if (mUsage == Static)
			return mIndexCount;
		
		return super.GetIndexCount();
	}

	int 	mPositionID		= -1;
	int		mUVID			= -1;
	int		mColourID		= -1;
	int		mIndexID		= -1;
	
	int		mIndexCount		= 0;
}