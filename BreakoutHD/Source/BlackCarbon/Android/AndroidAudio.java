package BlackCarbon.Android;

import android.media.AudioManager;
import android.media.SoundPool;
import BlackCarbon.API.Audio;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Sound;

public class AndroidAudio extends Audio
{
	AndroidAudio()
	{
		mSoundPool = new SoundPool(16, AudioManager.STREAM_MUSIC, 0);
	}
	
	@Override
	public Sound LoadSound(String name)
	{
		AndroidFactory factory = (AndroidFactory)Factory.Get();
		
		int resourceID = factory.GetResourcesID(name, "snd");
		AndroidSound sound  = new AndroidSound();
		sound.mId = mSoundPool.load(factory.GetInitParam().activity.getApplicationContext(), resourceID, 1);
		return sound;
	}
	
	SoundPool	mSoundPool;
}
