package BlackCarbon.Android;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL11;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import BlackCarbon.API.Factory;
import BlackCarbon.API.Texture;

public class AndroidTexture extends Texture
{
	public void Load()
	{
		//TextureReader.Texture texture = 
        //    TextureReader.readTexture("demos/data/images/glass.png");
		
		AndroidDevice device = ((AndroidDevice)Factory.Get().GetDevice());
		GL11 gl = device.GetGL();
		
		int width = 0;
		int height = 0;
		int pixels[];
		
		try
		{/*
			String resourceDir = mFactory.GetInitParam().resourceDir;
			AndroidDevice device = (AndroidDevice)mFactory.GetDevice();
			
			
			File f = new File(resourceDir + "/" + mName + "_tex.png");
			if (!f.exists())
				return;
			
			BufferedImage bi = ImageIO.read(f);
			int width = bi.getWidth();
			int height = bi.getHeight();*/
			
			
			AndroidFactory factory = (AndroidFactory)mFactory;
			int resourceID = factory.GetResourcesID(mName, "tex");
			if (resourceID <= 0)
			{
				System.out.println("[AndroidTexture.Load] Resource '" + mName + "' does not exist in apk");
				return;
			}
			
			Bitmap bmp = BitmapFactory.decodeResource(factory.GetResources(), resourceID);
			width = bmp.getWidth();
			height = bmp.getHeight();
			
			
			pixels = new int[width * height];
			bmp.getPixels(pixels, 0, width, 0, 0, width, height);
			
			//pixels = bi.getRGB(0, 0, width, height, null, 0, width);  
			
			// this is in the format ARGB and i need to convert it 
			// to ABGR 
			for (int i = 0; i < pixels.length; ++i)
			{				
				int r = (pixels[i] >> 16) & 0x000000ff;
				int b = pixels[i] & 0x000000ff;
				
				pixels[i] &= 0xff00ff00;
				pixels[i] |= r;
				pixels[i] |= (b << 16);
			}
		}
		catch (Exception e)
		{
			System.out.println("[AndroidTexture::Load1] Exception: " + e.getMessage());
			return;
		}
		
		try
		{
			device.Begin();
			
			IntBuffer ib = ByteBuffer.allocateDirect(4).order(ByteOrder.nativeOrder()).asIntBuffer();
			
			gl.glEnable(GL11.GL_TEXTURE_2D);
			gl.glGenTextures(1, ib);
			gl.glBindTexture(GL11.GL_TEXTURE_2D, ib.get(0));
			mID = ib.get(0);
			
			IntBuffer tbuf = IntBuffer.wrap(pixels);
			tbuf.position(0);	
			
			//gl.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			//gl.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
			
			gl.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			gl.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);

			gl.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, tbuf);
			gl.glDisable(GL11.GL_TEXTURE_2D);
		}
		catch (Exception e)
		{
			System.out.println("[AndroidTexture::Load2] Exception: " + e.getMessage());
		}
		
		device.End();
	}
	
	public void Bind()
	{
		if (mID == -1)
			return;
		
		GL11 gl = ((AndroidDevice)Factory.Get().GetDevice()).GetGL();
		gl.glEnable(GL11.GL_TEXTURE_2D);
		gl.glBindTexture(GL11.GL_TEXTURE_2D, mID);
	}
	
	public void Unbind()
	{
		GL11 gl = ((AndroidDevice)Factory.Get().GetDevice()).GetGL();
		gl.glDisable(GL11.GL_TEXTURE_2D);
		gl.glBindTexture(GL11.GL_TEXTURE_2D, -1);
	}
	
	private int mID 	= 	-1;
}
