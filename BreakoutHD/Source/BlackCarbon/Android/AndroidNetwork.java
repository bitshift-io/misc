package BlackCarbon.Android;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import BlackCarbon.API.Network;


public class AndroidNetwork extends Network //BroadcastReceiver
{/*
	public class AndroidConnection extends Connection
	{
		InetAddress 	address;
		DatagramSocket 	udp;
	}
	
	//
	// a thread that
	// sits there discovering clients
	//
	class BroadcastThread extends Thread
	{
		public BroadcastThread(AndroidNetwork network)
		{
			mNetwork = network;
		}
		
		public void run()
		{
			while (true)
			{
				int DISCOVERY_PORT = 2525;
				int TIMEOUT_MS = 500;
				
				String data = "yay";
				DatagramSocket socket;
				try 
				{
					socket = new DatagramSocket(DISCOVERY_PORT);
					socket.setBroadcast(true);
					socket.setSoTimeout(TIMEOUT_MS);
					
					// send
					for (int i = 0; i < 10; ++i)
					{
						InetAddress inetAddr = GetBroadcastAddress();
						DatagramPacket packet = new DatagramPacket(data.getBytes(), data.length(),
								inetAddr, DISCOVERY_PORT);
						socket.send(packet);
					}
			
					// receive
					byte[] buf = new byte[1024];
				    try 
				    {
				      while (true)
				      {
				        DatagramPacket packet = new DatagramPacket(buf, buf.length);
				        socket.receive(packet);
				        String s = new String(packet.getData(), 0, packet.getLength());
				        //Log.d(TAG, "Received response " + s);
				        
				        boolean existingConnectionFound = false;
				        Vector<Connection> connection = mNetwork.GetConnectionList();
				        for (int i = 0; i < connection.size(); ++i)
				        {
				        	AndroidConnection androidConnection = (AndroidConnection) connection.get(i);
				        	if (androidConnection.address == packet.getAddress())
				        	{
				        		existingConnectionFound = true;
				        		break;
				        	}
				        	
				        }
				        
				        if (!existingConnectionFound)
				        {
					        AndroidConnection androidConnection = new AndroidConnection();
					        androidConnection.address = packet.getAddress();
					        //connection.udp = new 
					        connection.add(androidConnection);
				        }
				        
				      }
				    } 
				    catch (SocketTimeoutException e) 
				    {
				      //Log.d(TAG, "Receive timed out");
				    }
				    
					//byte[] buf = new byte[1024];
					//DatagramPacket packet2 = new DatagramPacket(buf, buf.length);
					//socket.receive(packet2);
					
				}
				catch (Exception e)
				{
					System.out.print(e.getLocalizedMessage());
		    		e.printStackTrace();
				}
			}
		}
		
		public AndroidNetwork mNetwork;
	}
	
	class Receiver extends BroadcastReceiver
	{
		public Receiver(AndroidNetwork network)
		{
			mNetwork = network;
		}
		
		@Override
		public void onReceive(Context context, Intent intent)
		{
			mNetwork.onReceive(context, intent);
		}
	
		AndroidNetwork mNetwork;
	}
	
	public AndroidNetwork(AndroidFactory.InitParam param)
	{
		mFactory = (AndroidFactory)param.factory;
		
		// http://marakana.com/forums/android/android_examples/50.html
		// http://developer.android.com/guide/topics/wireless/bluetooth.html
		// http://code.google.com/p/boxeeremote/wiki/AndroidUDP
		// http://code.google.com/p/boxeeremote/source/browse/trunk/Boxee+Remote/src/com/andrewchatham/Discoverer.java?spec=svn28&r=28
		/*
		// get bluetooth adapter
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) 
		{
		    // Device does not support Bluetooth
		}
		
		// enable bluetooth
		if (!mBluetoothAdapter.isEnabled())
		{
		    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}* /
		
		WifiManager wifi = (WifiManager) mFactory.GetInitParam().activity.getSystemService(Context.WIFI_SERVICE);
		
		// Get WiFi status
		WifiInfo info = wifi.getConnectionInfo();
		
		// List available networks
		List<WifiConfiguration> configs = wifi.getConfiguredNetworks();
		for (WifiConfiguration config : configs) 
		{
			//textStatus.append("\n\n" + config.toString());
			int nothing = 0;
		}
		
		// Register Broadcast Receiver
		//BroadcastReceiver receiver = null;
		//if (receiver == null)
		//	receiver = new WiFiScanReceiver(this);

		mFactory.GetInitParam().activity.registerReceiver(new Receiver(this), new IntentFilter(
				WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		
		
		int PORT = 1515;
		int DISCOVERY_PORT = 2525;
		int TIMEOUT_MS = 500;
		
		String data = "yay";
		DatagramSocket socket;
		try 
		{
			socket = new DatagramSocket(DISCOVERY_PORT);
			socket.setBroadcast(true);
			
			// send
			for (int i = 0; i < 100; ++i)
			{
				
				
				
				//socket.setSoTimeout(TIMEOUT_MS);
				InetAddress inetAddr = GetBroadcastAddress();
				DatagramPacket packet = new DatagramPacket(data.getBytes(), data.length(),
						inetAddr, DISCOVERY_PORT);
				socket.send(packet);
			}
	/*
			// receive
			byte[] buf = new byte[1024];
			DatagramPacket packet2 = new DatagramPacket(buf, buf.length);
			socket.receive(packet2);
			* /
		}
		catch (Exception e)
		{
			System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
		}
	}
	

	
	public void onReceive(Context context, Intent intent) 
	{
		/*
		Parcelable btParcel = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        BluetoothDevice btDevice = (BluetoothDevice) btParcel;
		String name = btDevice.getName();
		String address = btDevice.getAddress();*/
		
		/*
		List<ScanResult> results = wifiDemo.wifi.getScanResults();
	    ScanResult bestSignal = null;
	    for (ScanResult result : results) {
	      if (bestSignal == null
	          || WifiManager.compareSignalLevel(bestSignal.level, result.level) < 0)
	        bestSignal = result;
	    }

	    String message = String.format("%s networks found. %s is the strongest.",
	        results.size(), bestSignal.SSID);
	    Toast.makeText(wifiDemo, message, Toast.LENGTH_LONG).show();

	    Log.d(TAG, "onReceive() message: " + message);* /
		
		int nothing = 0;
	}

	AndroidFactory mFactory;*/
	
	public AndroidNetwork(AndroidFactory.InitParam param)
	{
		mFactory = (AndroidFactory) param.factory;
	}
	
	public InetAddress GetLocalAddress()
	{
		try 
		{
	        for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) 
	        {
	            NetworkInterface intf = en.nextElement();
	            for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();)
	            {
	                InetAddress inetAddress = enumIpAddr.nextElement();
	                if (!inetAddress.isLoopbackAddress())
	                	return inetAddress;
	            }
	        }
	    } 
		catch (Exception e)
		{
			System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
	    }
	    /*
		try 
		{
			WifiManager wifi = (WifiManager) mFactory.GetInitParam().activity.getSystemService(Context.WIFI_SERVICE);
			DhcpInfo dhcp = wifi.getDhcpInfo();
	
			int broadcast = dhcp.ipAddress;
		    byte[] quads = new byte[4];
		    for (int k = 0; k < 4; k++)
		      quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
		    
			return InetAddress.getByAddress(quads);
		} 
		catch (UnknownHostException e) 
		{
			System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
		}
		*/
		return null;
	}
	
	AndroidFactory mFactory;
}
