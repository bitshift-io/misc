package BlackCarbon.Math;

import BlackCarbon.API.Memory;

public class Line 
{
	public enum Type
	{
		Line,			// Ray = v0 + v1 * time
		Segment,		// Ray = v0 + (v1 - v0) * time
		Ray				// Ray = v0 + v1 * time (where t > 0)
	}
	
	public Line()
	{
		
	}
	
	public Line(Vector4 v0, Vector4 v1, Type _type)
	{
		Set(v0, v1, type);
	}
	
	public void Set(Vector4 v0, Vector4 v1, Type _type)
	{
		v[0] = v0;
		v[1] = v1;
		type = _type;
	}
	
	public Vector4 GetPositionAtTime(float time)
	{
		Memory.Stack.Push();
		
		Vector4 r = null;
		if (type == Type.Ray)
			r = v[0].Add(v[1].Multiply(time));
		
		return Memory.Stack.PopAndReturn(r);
	}
	
	public static class ShortestDistanceResult
	{
		public float 	time0;
		public float 	time1;
		public float 	distance;
	}
	
	public ShortestDistanceResult ShortestDistance(Line other)
	{
		// http://softsurfer.com/Archive/algorithm_0106/algorithm_0106.htm
		Memory.Stack.Push();
		
		ShortestDistanceResult result = Memory.Stack.New(ShortestDistanceResult.class);
		
		Vector4 _u = Memory.Stack.New(Vector4.class);
		if (type == Type.Segment)
			_u = v[1].Subtract(v[0]);
		else
			_u.Copy(v[1]);
		
		Vector4 _v = Memory.Stack.New(Vector4.class);
		if (other.type == Type.Segment)
			_v = other.v[1].Subtract(other.v[0]);
		else
			_v.Copy(other.v[1]);
		
		Vector4 _w = v[0].Subtract(other.v[0]);
		
		float a = _u.Dot3(_u);	// always >= 0
		float b = _u.Dot3(_v);
		float c = _v.Dot3(_v);	// always >= 0
		float d = _u.Dot3(_w);
		float e = _v.Dot3(_w);
		float D = (a * c) - (b * b); // always >= 0
		
		float sc; // this line time
		float tc; // other line time
		
		// compute the line parameters of the two closest points
		// if the lines are almost parallel
		if (D < Math.Epsilon)
		{
			sc = 0.f;
			tc = (b > c) ? (d / b) : (e / c); // use the largest denominator
		}
		else
		{
			sc = ((b * e) - (c * d)) / D;
			tc = ((a * e) - (b * d)) / D;
		}
		
		// get the difference of the two closest points
		Vector4 dP = _w.Add(_u.Multiply(sc)).Subtract(_v.Multiply(tc));
		
		result.distance = dP.Magnitude3();
		result.time0 = sc;
		result.time1 = tc;
		
		return Memory.Stack.PopAndReturn(result);
	}
	
	/*
	public Line ShortestSegmentBetweenTwoSegments(Line other, boolean clamp)
	{
		// http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline3d/
		Vector4 v1 = v[0];
		Vector4 v2 = v[1];
		Vector4 v3 = other.v[0];
		Vector4 v4 = other.v[1];

		Vector4 v13 = v1.Subtract(v3);
		Vector4 v43 = v4.Subtract(v3);

		if (Math.Abs(v43.GetX()) < Math.Epsilon && Math.Abs(v43.GetY()) < Math.Epsilon && Math.Abs(v43.GetZ()) < Math.Epsilon)
	      return null;

		Vector4 v21 = v2.Subtract(v1);

		if (Math.Abs(v21.GetX()) < Math.Epsilon && Math.Abs(v21.GetY()) < Math.Epsilon && Math.Abs(v21.GetZ()) < Math.Epsilon)
	      return null;

		float d1343 = v13.Dot3(v43);
		float d4321 = v43.Dot3(v21);
		float d1321 = v13.Dot3(v21);
		float d4343 = v43.Dot3(v43);
		float d2121 = v21.Dot3(v21);

		float denom = d2121 * d4343 - d4321 * d4321;
		if (Math.Abs(denom) < Math.Epsilon)
			return null;
		
		float numer = d1343 * d4321 - d1321 * d4343;

		float mua = numer / denom;
		float mub = (d1343 + d4321 * mua) / d4343;

		if (clamp)
		{
			mua = Math.Clamp(0.f, mua, 1.f);
			mub = Math.Clamp(0.f, mub, 1.f);
		}
		
		// clamp locks to end points of line segments
		// put intersect time in to w component
		Vector4 pa = v1.Add(v21.Multiply(mua));
		pa.SetW(mua);
		
		Vector4 pb = v3.Add(v43.Multiply(mub));
		pb.SetW(mub);

		Line result = new Line(pa, pb, Line.Type.Segment);		
		return result;
	}
	*/
	public Vector4[] 	v 		= new Vector4[2];
	public Type			type;
	
}

