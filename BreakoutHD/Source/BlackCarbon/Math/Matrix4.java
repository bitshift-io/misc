package BlackCarbon.Math;

import BlackCarbon.API.Memory;
import BlackCarbon.Math.Matrix3;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;




/*
 * 	0 4 8  12
	1 5 9  13
	2 6 10 14
	3 7 11 15
 */
public class Matrix4 
{
	public enum Init
	{
		Identity,
		Zero,
		Mirror,
	}
	
	public float[][] m = new float[4][4];
	
	public Matrix4() 
	{
	}

	public Matrix4(Init type)
	{
		if (type == Init.Identity)
			SetIdentity();
		else if (type == Init.Zero)
			SetZero();
		else
			SetMirror();
	}
	
	public Matrix4(String stringValue)
	{
		System.out.println("Matrix4(String stringValue) - NYI");
		/*
		StringTokenizer st = new StringTokenizer(stringValue, " ,");
		
		int index = 0;
		while (st.hasMoreTokens())
		{
			float value = Float.valueOf(st.nextToken().trim()).floatValue();
	 		v[index] = value;
	 		
			++index;
		}*/
	}
	
	public Matrix4(Matrix4 other) 
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				m[i][j] = other.m[i][j];
			}
		}		
	}
	
	public void Copy(Matrix4 other)
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				m[i][j] = other.m[i][j];
			}
		}
	}
	
	public Vector4 Multiply4(Vector4 vector)
	{
		Memory.Stack.Push();
		
		Vector4 result = Memory.Stack.New(Vector4.class);
		
		result.v[0] = (m[0][0] * vector.v[0]) + (m[1][0] * vector.v[1]) + (m[2][0] * vector.v[2]) + (m[3][0] * vector.v[3]);
		result.v[1] = (m[0][1] * vector.v[0]) + (m[1][1] * vector.v[1]) + (m[2][1] * vector.v[2]) + (m[3][1] * vector.v[3]);
		result.v[2] = (m[0][2] * vector.v[0]) + (m[1][2] * vector.v[1]) + (m[2][2] * vector.v[2]) + (m[3][2] * vector.v[3]);
		result.v[3] = (m[0][3] * vector.v[0]) + (m[1][3] * vector.v[1]) + (m[2][3] * vector.v[2]) + (m[3][3] * vector.v[3]);
		
		return Memory.Stack.PopAndReturn(result);
	}
	
	public Vector4 Multiply3(Vector4 vector)
	{
		Memory.Stack.Push();
		
		Vector4 result = Memory.Stack.New(Vector4.class);
		
		result.v[0] = (m[0][0] * vector.v[0]) + (m[1][0] * vector.v[1]) + (m[2][0] * vector.v[2]) + (m[3][0]);
		result.v[1] = (m[0][1] * vector.v[0]) + (m[1][1] * vector.v[1]) + (m[2][1] * vector.v[2]) + (m[3][1]);
		result.v[2] = (m[0][2] * vector.v[0]) + (m[1][2] * vector.v[1]) + (m[2][2] * vector.v[2]) + (m[3][2]);
		result.v[3] = vector.v[3];
		
		return Memory.Stack.PopAndReturn(result);
	}
	
	public Vector4 MultiplyRotation3(Vector4 vector)
	{
		Memory.Stack.Push();
		
		Vector4 result = Memory.Stack.New(Vector4.class);
		
		result.v[0] = (m[0][0] * vector.v[0]) + (m[1][0] * vector.v[1]) + (m[2][0] * vector.v[2]);
		result.v[1] = (m[0][1] * vector.v[0]) + (m[1][1] * vector.v[1]) + (m[2][1] * vector.v[2]);
		result.v[2] = (m[0][2] * vector.v[0]) + (m[1][2] * vector.v[1]) + (m[2][2] * vector.v[2]);
		result.v[3] = vector.v[3];
		
		return Memory.Stack.PopAndReturn(result);
	}
	
	public Matrix4 Multiply(Matrix4 other)
	{
		Memory.Stack.Push();
		
		Matrix4 result = Memory.Stack.New(Matrix4.class);
		float[][] m1 = m;
		float[][] m2 = other.m;
		
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				result.m[i][j] = m1[i][0]*m2[0][j] + m1[i][1]*m2[1][j] + m1[i][2]*m2[2][j] + m1[i][3]*m2[3][j];
			}
		}
		
		return Memory.Stack.PopAndReturn(result);
	}
	
	public Matrix4 MultiplyRotation(Matrix4 other)
	{
		Memory.Stack.Push();
		
		Matrix4 result = Memory.Stack.New(Matrix4.class);
		result.Copy(this);
		
		float[][] m1 = m;
		float[][] m2 = other.m;
		
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				result.m[i][j] = m1[i][0]*m2[0][j] + m1[i][1]*m2[1][j] + m1[i][2]*m2[2][j]; // + m1[i][3]*m2[3][j];
			}
		}
		
		return Memory.Stack.PopAndReturn(result);
	}
	
	public void SetIdentity()
	{		
		m[0][0] = m[1][1]  = m[2][2] = m[3][3] = 1.0f;
		
		m[0][1] = m[0][2] = m[0][3] =
			m[1][0] = m[1][2] = m[1][3] =
			m[2][0] = m[2][1] = m[2][3] =
			m[3][0] = m[3][1] = m[3][2] = 0.f;
	}
	
	public void SetZero()
	{
		m[0][0] = m[1][1]  = m[2][2] = m[3][3] = 0.0f;
		
		m[0][1] = m[0][2] = m[0][3] =
			m[1][0] = m[1][2] = m[1][3] =
			m[2][0] = m[2][1] = m[2][3] =
			m[3][0] = m[3][1] = m[3][2] = 0.f;
	}

	public void SetMirror()
	{
		m[1][1] = -1.f;
		m[0][0] =  m[2][2] = m[3][3] = 1.0f;
		
		m[0][1] = m[0][2] = m[0][3] =
			m[1][0] = m[1][2] = m[1][3] =
			m[2][0] = m[2][1] = m[2][3] =
			m[3][0] = m[3][1] = m[3][2] = 0.f;
	}
	
	public void SetRotateY(float angle)
	{
		float s = (float)Math.Sin(angle);
		float c = (float)Math.Cos(angle);

		m[0][0] = c;
		m[0][2] = s;
		m[2][0] = -s;
		m[2][2] = c;
	}

	public void SetRotateX(float angle)
	{		
		float s = (float)Math.Sin(angle);
		float c = (float)Math.Cos(angle);

		m[1][1] = c;
		m[1][2] = -s;
		m[2][1] = s;
		m[2][2] = c;
	}

	public void SetRotateZ(float angle)
	{
		float s = (float)Math.Sin(angle);
		float c = (float)Math.Cos(angle);

		m[0][0] = c;
		m[0][1] = -s;
		m[1][0] = s;
		m[1][1] = c;
	}
	
	public void SetTranslation(Vector4 translation)
	{
		m[3][0] = translation.v[0];
		m[3][1] = translation.v[1];
		m[3][2] = translation.v[2];
	}
	
	public void SetScale(Vector4 scale)
	{
		m[0][0] = scale.v[0];
		m[1][1] = scale.v[1];
		m[2][2] = scale.v[2];
	}

	public void SetOrthographic(float width, float height, float znear, float zfar)
	{
		float halfWidth = width * 0.5f;
		float halfHeight = height * 0.5f;
		SetOrthographic(-halfWidth, halfWidth, halfHeight, -halfHeight, znear, zfar);
	}

	public void SetOrthographic(float left, float right, float top, float bottom, float znear, float zfar)
	{
		SetIdentity();

		m[0][0] = 2.f / (right - left);
		m[1][1] = 2.f / (top - bottom);
		m[2][2] = -1.f / (zfar - znear); // m[10] = -2.f / (zfar - znear);

		//m[12] = -(right + left) / (right - left);
		//m[13] = -(top + bottom) / (top - bottom);
		//m[14] = (znear + zfar) / (znear - zfar);

		m[3][2] = znear / (znear - zfar);

	/*
		2/w  0    0           0
		0    2/h  0           0
		0    0    1/(zf-zn)   zn/(zn-zf)
		0    0    0			  1
		*/
	}

	public void SetPerspective(float fovy, float aspect, float znear, float zfar)
	{
		//SetZero();
		SetIdentity();

		float yScale = (float) (1.0f / Math.Tan(Math.DegToRad(fovy / 2.0f)));
		float xScale = yScale / aspect;

		m[0][0]  = xScale;
		m[1][1]  = yScale;
		m[2][2] = zfar / (znear - zfar);
		//m[10] = (zfar + znear) / (znear - zfar);

		m[2][3] = -1.0f;
		m[3][2] = (zfar * znear) / (znear - zfar);
		//m[14] = (2.0f * zfar * znear) / (znear - zfar);
	}
	
	public Vector4 GetXAxis()
	{
		return GetColumn(0);
	}
	
	public Vector4 GetYAxis()
	{
		return GetColumn(1);
	}
	
	public Vector4 GetZAxis()
	{
		return GetColumn(2);
	}

	public Vector4 GetTranslation()
	{
		return GetColumn(3);
	}
	
	public Vector4 GetColumn(int index)
	{
		Memory.Stack.Push();
		Vector4 result = Memory.Stack.New(Vector4.class);
		result.Set(m[index][0], m[index][1], m[index][2], m[index][3]);
		return Memory.Stack.PopAndReturn(result);
	}
	
	public void SetColumn(int index, Vector4 column)
	{
		m[index][0] = column.GetX();
		m[index][1] = column.GetY();
		m[index][2] = column.GetZ();
		m[index][3] = column.GetW();
	}
	
	public void SetRow(int index, Vector4 row)
	{
		m[0][index] = row.GetX();
		m[1][index] = row.GetY();
		m[2][index] = row.GetZ();
		m[3][index] = row.GetW();
	}
	
	public void CreateFromForward(Vector4 view, Vector4 up)
	{
		Memory.Stack.Push();
		
		Vector4 viewNew = Memory.Stack.New(Vector4.class);
		viewNew.Set(view.GetX(), view.GetY(), view.GetZ(), 0.f);
		Vector4 right = viewNew.Cross3(up);
		Vector4 upNew = right.Cross3(viewNew);
		
		SetIdentity();
		SetColumn(0, right);
		SetColumn(1, upNew);
		SetColumn(2, viewNew);
		
		Memory.Stack.Pop();
	}
	
	public void CreateView(Matrix4 world)
	{
		Memory.Stack.Push();
		
		Vector4 right = world.GetXAxis();
		Vector4 up = world.GetYAxis();
		Vector4 view = world.GetZAxis();
		Vector4 position = world.GetTranslation();
		
		view = view.Multiply(-1.f);

		SetIdentity();

		Vector4 translation = Memory.Stack.New(Vector4.class);
		translation.Set(-position.Dot3(right), -position.Dot3(up), -position.Dot3(view), 1.f);
		
		// set the camera view matrix
		SetRow(0, right);
		SetRow(1, up);
		SetRow(2, view);
		SetTranslation(translation);
		/*
		m[ 0] = right.GetX();
		m[ 4] = right.GetY();
		m[ 8] = right.GetZ();

		m[ 1] = up.GetX();
		m[ 5] = up.GetY();
		m[ 9] = up.GetZ();

		m[ 2] = -view.GetX();
		m[ 6] = -view.GetY();
		m[10] = -view.GetZ();

		m[12] = -position.Dot(right);
		m[13] = -position.Dot(up);
		m[14] = -position.Dot(view);
*/
		Memory.Stack.Pop();
	}
	
	Matrix4 GetAdjoint()
	{
		// http://www.cs.virginia.edu/~cs551dl/assignment3/matinv.c
		Memory.Stack.Push();
		
		Matrix4 ret = Memory.Stack.New(Matrix4.class);
		Matrix3 mat3 = null;

		// we are effectively doing a transpose here as well

		/*
		 * 	0 4 8  12
			1 5 9  13
			2 6 10 14
			3 7 11 15
		 */
		
		// column 0
		mat3 = GetMatrix3(0, 0);
		ret.m[0][0] = mat3.GetDeterminant();

		mat3 = GetMatrix3(1, 0);
		ret.m[1][0] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(2, 0);
		ret.m[2][0] = mat3.GetDeterminant();

		mat3 = GetMatrix3(3, 0);
		ret.m[3][0] = -mat3.GetDeterminant();

		// column 1
		mat3 = GetMatrix3(0, 1);
		ret.m[0][1] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(1, 1);
		ret.m[1][1] = mat3.GetDeterminant();

		mat3 = GetMatrix3(2, 1);
		ret.m[2][1] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(3, 1);
		ret.m[3][1] = mat3.GetDeterminant();

		// column 2
		mat3 = GetMatrix3(0, 2);
		ret.m[0][2] = mat3.GetDeterminant();

		mat3 = GetMatrix3(1, 2);
		ret.m[1][2] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(2, 2);
		ret.m[2][2] = mat3.GetDeterminant();

		mat3 = GetMatrix3(3, 2);
		ret.m[3][2] = -mat3.GetDeterminant();

		// column 3
		mat3 = GetMatrix3(0, 3);
		ret.m[0][3] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(1, 3);
		ret.m[1][3] = mat3.GetDeterminant();

		mat3 = GetMatrix3(2, 3);
		ret.m[2][3] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(3, 3);
		ret.m[3][3] = mat3.GetDeterminant();

		return Memory.Stack.PopAndReturn(ret);
	}
	
	// gets a matrix and skips the row/col passed in
	Matrix3 GetMatrix3(int row, int col)
	{
		Memory.Stack.Push();
		
		Matrix3 mat = Memory.Stack.New(Matrix3.class);
		
		for (int c = 0, i = 0; c < 4; ++c, ++i)
		{
			if (c == col)
			{
				++c;

				if (c >= 4)
					break;
			}

			for (int r = 0, j = 0; r < 4; ++r, ++j)
			{
				if (r == row)
				{
					++r;

					if (r >= 4)
						break;
				}

				mat.m[i][j] = m[c][r];
			}
		}

		return Memory.Stack.PopAndReturn(mat);
	}
	
	float GetDeterminant()
	{
		return (m[0][0] * m[1][1] * m[2][2] * m[3][3]) + (m[0][0] * m[1][2] * m[2][3] * m[3][1]) + (m[0][0] * m[1][3] * m[2][1] * m[3][2])
			 + (m[0][1] * m[1][0] * m[2][3] * m[3][2]) + (m[0][1] * m[1][2] * m[2][0] * m[3][3]) + (m[0][1] * m[1][3] * m[2][2] * m[3][0])
			 + (m[0][2] * m[1][0] * m[2][1] * m[3][3]) + (m[0][2] * m[1][1] * m[2][3] * m[3][0]) + (m[0][2] * m[1][3] * m[2][0] * m[3][1])
			 + (m[0][3] * m[1][0] * m[2][2] * m[3][1]) + (m[0][3] * m[1][1] * m[2][0] * m[3][2]) + (m[0][3] * m[1][2] * m[2][1] * m[3][0])
			 - (m[0][0] * m[1][1] * m[2][3] * m[3][2]) - (m[0][0] * m[1][2] * m[2][1] * m[3][3]) - (m[0][0] * m[1][3] * m[2][2] * m[3][1])
			 - (m[0][1] * m[1][0] * m[2][2] * m[3][3]) - (m[0][1] * m[1][2] * m[2][3] * m[3][0]) - (m[0][1] * m[1][3] * m[2][0] * m[3][2])
			 - (m[0][2] * m[1][0] * m[2][3] * m[3][1]) - (m[0][2] * m[1][1] * m[2][0] * m[3][3]) - (m[0][2] * m[1][3] * m[2][1] * m[3][0])
			 - (m[0][3] * m[1][0] * m[2][1] * m[3][2]) - (m[0][3] * m[1][1] * m[2][2] * m[3][0]) - (m[0][3] * m[1][2] * m[2][0] * m[3][1]);
	}
	
	public Matrix4 GetInverse()
	{
		Memory.Stack.Push();
		
		Matrix4 ret = Memory.Stack.New(Matrix4.class);
		Matrix4 adjoint = GetAdjoint();
		float invDet = 1.0f / GetDeterminant();

		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				ret.m[i][j] = invDet * adjoint.m[i][j];
			}
		}

		return Memory.Stack.PopAndReturn(ret);
	}
	
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder("[ ");
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				builder.append(m[j][i]);
				builder.append(" ");
			}
			
			if (i < 3)
				builder.append("\n  ");
		}
		builder.append(" ]");
		return builder.toString();
	}
}
