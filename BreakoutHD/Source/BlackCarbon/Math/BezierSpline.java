package BlackCarbon.Math;

import java.util.Collection;
import java.util.Vector;

// http://www.ibiblio.org/e-notes/Splines/Bezier.htm
public class BezierSpline 
{
	public enum Type
	{
		Linear,
		Quadratic,
		Cubic,
	}
	/*
	public Vector4 EvaluateLinear(float percent, ControlPoint c0, ControlPoint c1)
	{
		Vector4 pt = c0.position.Multiply(1 - percent).Add(c1.position.Multiply(percent));
		return pt;
	}
	
	public Vector4 EvaluateQuadratic(float percent, ControlPoint c0, ControlPoint c1)
	{
		Vector4 p0 = c0.position;
		Vector4 p1 = c0.position.Add(c1.tangent[0]);
		Vector4 p2 = c1.position;
		
		Vector4 p01 = p0.Multiply(1 - percent).Add(p1.Multiply(percent));
		Vector4 p12 = p1.Multiply(1 - percent).Add(p2.Multiply(percent));
		Vector4 pt = p01.Multiply(1 - percent).Add(p12.Multiply(percent));
		return pt;
	}
	
	public Vector4 Evaluate(float percent)
	{
		try
		{
			percent = Math.Clamp(0.f, percent, 1.f - Math.Epsilon);
			
			Object[] array = controlPoints.toArray();
			
			int numControlPoints = controlPoints.size();
			
			if (numControlPoints <= 0)
				return null;
				
			if (numControlPoints == 1)
				return ((ControlPoint)array[0]).position;
				
			float percentPerControl = 1.f / (numControlPoints - 1.f);
			float controlPointStartAndPercent = percent / percentPerControl;
			float controlPercent = Math.Decimal(controlPointStartAndPercent);
			int controlPointStart = (int)Math.Floor(controlPointStartAndPercent);
			
			if (type == Type.Linear)
			{
				return EvaluateLinear(controlPercent, (ControlPoint)array[controlPointStart], (ControlPoint)array[controlPointStart + 1]);
			}
			else if (type == Type.Quadratic)
			{
				return EvaluateQuadratic(controlPercent, (ControlPoint)array[controlPointStart], (ControlPoint)array[controlPointStart + 1]);
			}/*
			else if (type == Type.Cubic)
			{
				return EvaluateCubic(controlPercent, (ControlPoint)array[controlPointStart], (ControlPoint)array[controlPointStart + 1]);
			}* /
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void DrawDebug(Vector4 colour)
	{
		Device device = Factory.Get().GetDevice();
		
		float increment = 0.01f;
		for (float p = 0.f; p < 1.f; p += increment)
		{
			device.DrawLine(Evaluate(p), Evaluate(p + increment), colour);
		}
	}
	
	public void Add(ControlPoint value)
	{
		controlPoints.add(value);
	}
	
	public void Clear()
	{
		controlPoints.clear();
	}
	
	public int GetSize()
	{
		return controlPoints.size();
	}
	*/
	public class ControlPoint
	{
		public Vector4 		position;
		public Vector4[]	tangent		= new Vector4[2];
	};
	
	public Collection<ControlPoint> 	controlPoints 	= new Vector<ControlPoint>();
	public Type							type			= Type.Quadratic;
}
