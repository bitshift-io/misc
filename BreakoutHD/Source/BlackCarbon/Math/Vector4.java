package BlackCarbon.Math;

import java.io.Serializable;
import java.util.StringTokenizer;

import BlackCarbon.API.Memory;

public class Vector4 implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6327176359154383345L;
	public float[] v = new float[4];
	
	public Vector4()
	{
	}
	
	public Vector4(String stringValue)
	{
		StringTokenizer st = new StringTokenizer(stringValue, " ,");
		int index = 0;
		while (st.hasMoreTokens())
		{
			float value = Float.valueOf(st.nextToken().trim()).floatValue();
	 		v[index] = value;
	 		
			++index;
		}
	}
	
	public Vector4(Vector4 other)
	{
		Copy(other);
	}

	public Vector4(float x, float y)
	{
		Set(x, y, 0.f, 1.f);
	}
	
	public Vector4(float x, float y, float z)
	{
		Set(x, y, z, 1.f);
	}
	
	public Vector4(float x, float y, float z, float w)
	{
		Set(x, y, z, w);
	}
	
	public Vector4(Quaternion quaternion)
	{
		v[0] = quaternion.q[0];
		v[1] = quaternion.q[1];
		v[2] = quaternion.q[2];
		v[3] = quaternion.q[3];
	}
	
	public void Set(float x, float y, float z, float w)
	{
		v[0] = x;
		v[1] = y;
		v[2] = z;
		v[3] = w;
	}
	
	public void Copy(Vector4 other)
	{
		v[0] = other.GetX();
		v[1] = other.GetY();
		v[2] = other.GetZ();
		v[3] = other.GetW();
	}
	
	public float Dot3(Vector4 other)
	{
		return (GetX() * other.GetX()) + (GetY() * other.GetY()) + (GetZ() * other.GetZ());
	}
	
	public float Dot4(Vector4 other)
	{
		return (GetX() * other.GetX()) + (GetY() * other.GetY()) + (GetZ() * other.GetZ()) + (GetW() * other.GetW());
	}
	
	public Vector4 Add(Vector4 other)
	{
		Memory.Stack.Push();
		Vector4 r = Memory.Stack.New(Vector4.class);
		r.Set(v[0] + other.v[0], v[1] + other.v[1], v[2] + other.v[2], v[3] + other.v[3]);
		return Memory.Stack.PopAndReturn(r);
	}
	
	public Vector4 Subtract(Vector4 other)
	{
		Memory.Stack.Push();
		Vector4 r = Memory.Stack.New(Vector4.class);
		r.Set(v[0] - other.v[0], v[1] - other.v[1], v[2] - other.v[2], v[3] - other.v[3]);
		return Memory.Stack.PopAndReturn(r);
	}
	
	public Vector4 Multiply(Vector4 other)
	{
		Memory.Stack.Push();
		Vector4 r = Memory.Stack.New(Vector4.class);
		r.Set(v[0] * other.v[0], v[1] * other.v[1], v[2] * other.v[2], v[3] * other.v[3]);
		return Memory.Stack.PopAndReturn(r);
	}
	
	public Vector4 Multiply(float other)
	{
		Memory.Stack.Push();
		Vector4 r = Memory.Stack.New(Vector4.class);
		r.Set(v[0] * other, v[1] * other, v[2] * other, v[3] * other);
		return Memory.Stack.PopAndReturn(r);
	}
	
	public Vector4 Divide(float other)
	{
		Memory.Stack.Push();
		float inverseOther = 1.f / other;
		Vector4 r = Multiply(inverseOther);
		return Memory.Stack.PopAndReturn(r);
	}
	
	public float MagnitudeSqrd4()
	{
		return (v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]) + (v[3] * v[3]);
	}

	public float MagnitudeSqrd3()
	{
		return (v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);
	}
	
	public float Magnitude3()
	{
		return Math.Sqrt(MagnitudeSqrd3());
	}
	
	public float Magnitude4()
	{
		return Math.Sqrt(MagnitudeSqrd4());
	}
	
	public float Normalize4()
	{
		float mag = Magnitude4();
		float invMagnitude = 1.f / mag;
		v[0] *= invMagnitude;
		v[1] *= invMagnitude;
		v[2] *= invMagnitude;
		v[3] *= invMagnitude;

		return mag;
	}
	
	public float Normalize3()
	{
		float mag = Magnitude3();
		float invMagnitude = 1.f / mag;
		v[0] *= invMagnitude;
		v[1] *= invMagnitude;
		v[2] *= invMagnitude;

		return mag;
	}
	
	public float GetX()
	{
		return v[0];
	}
	
	public float GetY()
	{
		return v[1];
	}
	
	public float GetZ()
	{
		return v[2];
	}
	
	public float GetW()
	{
		return v[3];
	}
	
	public void SetX(float x)
	{
		v[0] = x;
	}
	
	public void SetY(float y)
	{
		v[1] = y;
	}
	
	public void SetZ(float z)
	{
		v[2] = z;
	}
	
	public void SetW(float w)
	{
		v[3] = w;
	}
	
	public Vector4 Cross3(Vector4 other)
	{
		Memory.Stack.Push();
		Vector4 r = Memory.Stack.New(Vector4.class);
		r.Set(other.v[1] * v[2] - other.v[2] * v[1], 
				other.v[2] * v[0] - other.v[0] * v[2],
				other.v[0] * v[1] - other.v[1] * v[0],
				0.f);
		return Memory.Stack.PopAndReturn(r);
	}
	
	public Vector4 Reflect3(Vector4 normal)
	{
		Memory.Stack.Push();
		// R = V - ( 2 * V [dot] N ) N 
		// (V = vector A, N = vector B, [dot] stands for dot product)
		
		float dot2 = Dot3(normal) * 2.f;
		
		Vector4 r = normal.Multiply(dot2);
		r = Subtract(r);
		return Memory.Stack.PopAndReturn(r);
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder("[ ");
		for (int i = 0; i < 4; i++)
		{
			builder.append(v[i]);
			builder.append(" ");
		}
		builder.append(" ]");
		return builder.toString();
	}
}

