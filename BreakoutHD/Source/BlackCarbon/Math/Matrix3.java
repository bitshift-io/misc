package BlackCarbon.Math;

/*
 * 	0 3 6
	1 4 7
	2 5 8
 */
public class Matrix3
{
	public float[][] m = new float[3][3];
	
	public Matrix3()
	{
		
	}
	
	public float GetDeterminant()
	{
		return	(m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1])) -
				(m[1][0] * (m[0][1] * m[2][2] - m[0][2] * m[2][1])) +
				(m[2][0] * (m[0][1] * m[1][2] - m[1][1] * m[0][2]));
	}
}