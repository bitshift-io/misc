package BlackCarbon.API;

import java.io.InputStream;
import java.util.Vector;

//
// Create an instance of this,
// holds pointers to all framework resources
//
// create implies creating a new one regardless of name
// acquire implies looking for an existing resource with said name and returning that, else creating a new one
//
public class Factory
{
	static public class InitParam
	{		
		// device param
		public String 	windowName		= "BlackCarbon";
		public int 		width			= 320;
		public int 		height			= 480;
		public boolean	windowVisible	= true;	// window initially visible
		
		public String	resourceDir		= "";
		
		public Factory 	factory; // internal only
	}
	
	public enum Platform
	{
		Unknown,
		Desktop,
		Android,
	}
	
	public Factory()
	{
		mFactory = this;
	}

	public RenderBuffer CreateRenderBuffer()
	{
		return null;
	}
	
	public RenderBuffer AcquireRenderBuffer(String name)
	{
		return null;
	}
	
	public void ReleaseRenderBuffer(RenderBuffer resource)
	{
		
	}
	
	public Texture AcquireTexture(String name)
	{
		return null;
	}
	
	public void ReleaseTexture(Texture resource)
	{

	}
	
	public Font AcquireFont(String name)
	{
		Font font = new Font();
		font.mName = name;
		font.mFactory = this;
		font.Load();
		return font;
	}
	
	public void ReleaseFont(Font resource)
	{

	}
	
	public Mesh AcquireMesh(String name)
	{
		Mesh mesh = new Mesh();
		mesh.mName = name;
		mesh.mFactory = this;
		mesh.Load();
		return mesh;
	}
	
	public void ReleaseMesh(Mesh resource)
	{

	}
	
	public Sound AquireSound(String name)
	{
		return null;
	}
	
	public void ReleaseSound()
	{
		
	}
	
	public Device GetDevice()
	{
		return mDevice;
	}
	
	public Network GetNetwork()
	{
		return mNetwork;
	}
	
	public Input GetInput()
	{
		return mInput;
	}
	
	public Audio GetAudio()
	{
		return mAudio;
	}
	
	public InitParam GetInitParam()
	{
		return null;
	}
	
	// returns false if the application should close
	public boolean Update()
	{
		if (mInput != null)
			mInput.Update();
		
		if (mNetwork != null)
			mNetwork.Update();
		
		if (mDevice != null)
			return mDevice.Update();
		
		return true;
	}
	
	static public Factory Get()
	{
		return mFactory;
	}
	
	public InputStream GetFileResource(String name, String extension)
	{
		return null;
	}
	
	public Platform GetPlatform()
	{
		return mPlatform;
	}
	
	protected Device			mDevice;
	protected Input				mInput;
	protected Audio				mAudio;
	protected Network			mNetwork;
	protected Platform			mPlatform				= Platform.Unknown;
	
	protected static Factory	mFactory;
	
	protected Vector<Texture>			mTexture				= new Vector<Texture>();
	protected Vector<Mesh>				mMesh					= new Vector<Mesh>(); 
	protected Vector<RenderBuffer>		mRenderBuffer			= new Vector<RenderBuffer>();
}


class Resource
{
	public String	mName;
	public Factory	mFactory;
}