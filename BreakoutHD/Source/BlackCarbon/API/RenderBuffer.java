package BlackCarbon.API;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import BlackCarbon.Math.Vector4;

public class RenderBuffer extends Resource
{
	// vertex format
	public static byte Position 		= 1 << 0;
	public static byte UV				= 1 << 1;
	public static byte Colour			= 1 << 2;
	
	// usage
	public static byte Static			= 1;
	public static byte Dynamic			= 2;
	
	public void Bind()
	{

	}
	
	public void Unbind()
	{
		
	}
	
	public void Render()
	{
		
	}
	
	public void CreateQuad(Vector4 normal)
	{
		Vector4 positions[] = {
	           	new Vector4(-0.5f, -0.5f, 0),
	           	new Vector4( 0.5f, -0.5f, 0),
			   	new Vector4(-0.5f,  0.5f, 0),
			   	new Vector4( 0.5f,  0.5f, 0),
	        };
		
		Vector4 uvs[] = {
				new Vector4(0,   1.f),
				new Vector4(1.f, 1.f),
				new Vector4(  0, 0.f),
				new Vector4(1.f, 0.f),
	        };

		byte indices[] = {
		        0, 1, 3,
		        0, 3, 2,
	        };
	        
		SetIndicies(indices);
		SetPositions(positions);
		SetUVs(uvs);
	}
	
	public void CreateUnitCube()
	{
		float half = 0.5f;

        Vector4 positions[] = {
               new Vector4(-half, -half, -half),
               new Vector4( half, -half, -half),
    		   new Vector4( half,  half, -half),
			   new Vector4(-half,  half, -half),
			   new Vector4(-half, -half,  half),
			   new Vector4( half, -half,  half),
			   new Vector4( half,  half,  half),
			   new Vector4(-half,  half,  half),
            };
        
        byte indices[] = {
                0, 4, 5,
                0, 5, 1,
                1, 5, 6,
                1, 6, 2,
                2, 6, 7,
                2, 7, 3,
                3, 7, 4,
                3, 4, 0,
                4, 7, 6,
                4, 6, 5,
                3, 0, 1,
                3, 1, 2
        };
        
        SetIndicies(indices);
        SetPositions(positions);
        //SetColours(colours);
	}


	// Buffers to be passed to gl*Pointer() functions
    // must be direct, i.e., they must be placed on the
    // native heap where the garbage collector cannot
    // move them.
    //
    // Buffers with multi-byte datatypes (e.g., short, int, float)
    // must have their byte order set to native order
	
	public void SetUVs(Vector4 uvs[])
	{
		ByteBuffer uvbb = ByteBuffer.allocateDirect(uvs.length * 2 * 4);
        uvbb.order(ByteOrder.nativeOrder());
        mUV = uvbb.asFloatBuffer();
	    for (int i = 0; i < uvs.length; ++i)
	    {
	    	mUV.put(uvs[i].v[0]);
	    	mUV.put(uvs[i].v[1]);
	    }
	    mUV.position(0);
	}
	
	public void SetUVs(float uvs[])
	{
		ByteBuffer uvbb = ByteBuffer.allocateDirect(uvs.length * 4);
        uvbb.order(ByteOrder.nativeOrder());
        mUV = uvbb.asFloatBuffer();
	    
	    for (int i = 0; i < uvs.length; ++i)
	    {
	    	mUV.put(uvs[i]);
	    }
	    mUV.position(0);
	}
	
	public void SetPositions(Vector4 positions[])
	{
		ByteBuffer vbb = ByteBuffer.allocateDirect(positions.length * 3 * 4);
	    vbb.order(ByteOrder.nativeOrder());
	    mPosition = vbb.asFloatBuffer();
	    for (int i = 0; i < positions.length; ++i)
	    {
	    	mPosition.put(positions[i].v[0]);
	    	mPosition.put(positions[i].v[1]);
	    	mPosition.put(positions[i].v[2]);
	    }
	    mPosition.position(0);
	}
	
	public void SetPositions(float positions[])
	{
		ByteBuffer vbb = ByteBuffer.allocateDirect(positions.length * 4);
	    vbb.order(ByteOrder.nativeOrder());
	    mPosition = vbb.asFloatBuffer();
	    
	    for (int i = 0; i < positions.length; ++i)
	    {
	    	mPosition.put(positions[i]);
	    }
	    mPosition.position(0); 
	}
	
	public void SetColours(Vector4 colours[])
	{
		ByteBuffer cbb = ByteBuffer.allocateDirect(colours.length * 4 * 4);
	    cbb.order(ByteOrder.nativeOrder());
	    mColour = cbb.asFloatBuffer();
        for (int i = 0; i < colours.length; ++i)
        	mColour.put(colours[i].v);
        mColour.position(0);
	}
	
	public void SetColours(float colours[])
	{
		ByteBuffer cbb = ByteBuffer.allocateDirect(colours.length * 4);
	    cbb.order(ByteOrder.nativeOrder());
	    mColour = cbb.asFloatBuffer();
        
        for (int i = 0; i < colours.length; ++i)
	    {
        	mColour.put(colours[i]);
	    }
        mColour.position(0);
	}
	
	public void SetIndicies(byte indices[])
	{
		mIndex = ByteBuffer.allocateDirect(indices.length);
		mIndex.put(indices);
		mIndex.position(0);
	}

	public void MakeStatic()
	{

	}
	
	public byte GetUsage()
	{
		return mUsage;
	}
	
	public int GetIndexCount()
	{
		if (mIndex == null)
			return 0;
		
		return mIndex.capacity();
	}
	
	protected FloatBuffer   	mPosition	= null;
	protected FloatBuffer   	mUV			= null;
	protected FloatBuffer   	mColour		= null;
	protected ByteBuffer  		mIndex		= null;
    
	protected byte				mUsage		= Dynamic;
}
