package BlackCarbon.API;

import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;

public class Camera extends Resource
{	
	public Camera()
	{
		SetPerspective();
	}
	
	public void Bind()
	{
		Device device = Factory.Get().GetDevice();
		device.SetMatrix(GetProjection(), Device.MatrixMode.Projection);
		device.SetMatrix(GetView(), Device.MatrixMode.View);
	}
	
	public Matrix4 GetProjection()
	{ 
		return mProjection; 
	}
	
	public void SetWorld(Matrix4 world)
	{
		mWorld.Copy(world);
		mViewDirty = true;
	}
	
	public Matrix4 GetWorld()
	{ 
		return mWorld;
	}
	
	public void SetView(Matrix4 view)
	{
		mView.Copy(view);
		mViewDirty = false;
	}

	public Matrix4 GetView()
	{ 
		if (mViewDirty)
		{
			mView.CreateView(mWorld);
			mViewDirty = false;
		}

		return mView; 
	}
	
	public void SetOrthographic()
	{
		SetOrthographic(2.f, 2.f, 0.f, 1.f);
	}
	
	public void SetOrthographic(float width, float height, float znear, float zfar)
	{
		mFarPlane = zfar;
		mNearPlane = znear;
		mAspect = (float)width / (float)height;
		mWidth = width;
		mHeight =  height;

		mFrustumDirty = true;
		mPerpective = false;
		mProjection.SetOrthographic(-width * 0.5f, width * 0.5f, height * 0.5f, -height * 0.5f, znear, zfar);
	}
	
	public void SetPerspective()
	{
		SetPerspective(90.0f, 0.01f, 1000.0f, 1.33f);
	}
	
	public void SetPerspective(float fovy, float znear, float zfar, float aspect)
	{
		mFarPlane = zfar;
		mNearPlane = znear;
		mFOV = fovy;
		mAspect = aspect;

		mFrustumDirty = true;
		mPerpective = true;
		mProjection.SetPerspective(fovy, aspect, znear, zfar);
	}
	
	public void SetProjection(Matrix4 proj)
	{
		mProjection.Copy(proj);
	}
	
	public void SetPosition(Vector4 position)
	{
		mFrustumDirty = true;
		mViewDirty = true;
		mWorld.SetTranslation(position);
	}
	
	public Vector4 GetPosition()
	{
		Memory.Stack.Push();
		Vector4 position = mWorld.GetTranslation();
		return Memory.Stack.PopAndReturn(position);
	}
	
	//private Frustum		mFrustum;
	@SuppressWarnings("unused")
	private boolean		mFrustumDirty;
	
	private Matrix4 	mWorld				= new Matrix4(Matrix4.Init.Identity);
	private Matrix4 	mProjection			= new Matrix4();
	private Matrix4 	mView				= new Matrix4(Matrix4.Init.Identity);
	private boolean		mViewDirty			= false;
	
	@SuppressWarnings("unused")
	private float		mFarPlane;
	@SuppressWarnings("unused")
	private float		mNearPlane;
	@SuppressWarnings("unused")
	private float		mFOV;
	@SuppressWarnings("unused")
	private float		mAspect;
	@SuppressWarnings("unused")
	private float		mWidth;
	@SuppressWarnings("unused")
	private float		mHeight;
	@SuppressWarnings("unused")
	private boolean		mPerpective;
}
