package BlackCarbon.API;

import java.util.HashMap;
import java.util.Vector;

//experimental
public class Memory
{
	public static class Heap
	{
		public static <T> T New(Class<T> type)
		{
			if (mLocked)
			{
				System.out.println("The heap is locked, either allocate this object when the heap is not locked, or use Memory.Stack.New");
				return null;
			}
			
			try 
			{
				if (Memory.mDebugMode == true)
				{
					System.out.println("[Memory.Heap.New] Allocating new instance in heap: " + type.getName());
				}
				
				return type.newInstance();
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		public static void SetLocked(boolean locked)
		{
			mLocked = locked;
		}
		
		static boolean mLocked		= false;		// if locked, we assert on allocation
	}
	
	public static class Stack
	{
		synchronized public static StackLevel Push()
		{
			Thread currentThread = Thread.currentThread();
			ThreadStack threadStack = mThreadStack.get(currentThread);
			if (threadStack == null)
			{
				threadStack = new ThreadStack();
				mThreadStack.put(currentThread, threadStack);
			}
			
			++threadStack.mStackDepth;
			if (threadStack.mStackDepth >= threadStack.mStack.size())
			{
				threadStack.mStack.add(new StackLevel());
			}
			
			return threadStack.mStack.get(threadStack.mStackDepth);
		}
		
		synchronized public static <T> T PopAndReturn(StackLevel stackLevel, T returnObject)
		{
			if (Memory.mDebugMode == true)
			{
				Thread currentThread = Thread.currentThread();
				ThreadStack threadStack = mThreadStack.get(currentThread);
				StackLevel expectedStackLevel = threadStack.mStack.get(threadStack.mStackDepth);
				
				if (!stackLevel.equals(expectedStackLevel))
				{
					System.out.println("[Memory.Stack.PopAndReturn] StackLevel is incorrect, Some method has a  mismatch with Push and Pop");
				}
			}
			
			return PopAndReturn(returnObject);
		}
		
		// pushes returnObject up to parent stack level
		// to indicate its still in use
		synchronized public static <T> T PopAndReturn(T returnObject)
		{
			if (returnObject != null)
			{
				Thread currentThread = Thread.currentThread();
				ThreadStack threadStack = mThreadStack.get(currentThread);
				
				StackLevel stackLevel = threadStack.mStack.get(threadStack.mStackDepth);
				StackLevel parentStackLevel = threadStack.mStack.get(threadStack.mStackDepth - 1);
				
				stackLevel.mObjectList.remove(returnObject);
				parentStackLevel.mObjectList.add(returnObject);
			}
			
			Pop();
			return returnObject;
		}
		
		// as above, but returns all objects in this stack, for when you need to return multiple
		// objects eg. you need to return 
		// object which points to other objects you've also allocated on the stack for example
		synchronized public static <T> T PopAndReturnAll(T returnObject)
		{
			if (returnObject != null)
			{
				Thread currentThread = Thread.currentThread();
				ThreadStack threadStack = mThreadStack.get(currentThread);
				
				StackLevel stackLevel = threadStack.mStack.get(threadStack.mStackDepth);
				StackLevel parentStackLevel = threadStack.mStack.get(threadStack.mStackDepth - 1);
				
				for (int i = 0; i < stackLevel.mObjectList.size(); ++i)
				{
					parentStackLevel.mObjectList.add(stackLevel.mObjectList.get(i));
				}
				stackLevel.mObjectList.clear();
			}
			
			Pop();
			return returnObject;
		}
		
		// returns all temporaries back to pool
		synchronized public static void Pop()
		{
			// return objects in stack to pool
			Thread currentThread = Thread.currentThread();
			ThreadStack threadStack = mThreadStack.get(currentThread);
			
			StackLevel stackLevel = threadStack.mStack.get(threadStack.mStackDepth);
		
			int objectListSize = stackLevel.mObjectList.size();
			for (int i = 0; i < objectListSize; ++i)
			{
				Object object = stackLevel.mObjectList.get(i);
				Vector<Object> objectPool = mObjectPool.get(object.getClass());
				
				if (objectPool == null)
				{
					System.out.println("[Memory.Stack.Pop] Object not found in pool, are you using new on a temporary variable?");
				}
				
				objectPool.add(object);
			}
			
			stackLevel.mObjectList.clear();
			--threadStack.mStackDepth;
		}
		
		// to verify the stack is valid, slower, only use when memory is constantly being allocated
		synchronized public static void Pop(StackLevel stackLevel)
		{
			if (Memory.mDebugMode == true)
			{
				Thread currentThread = Thread.currentThread();
				ThreadStack threadStack = mThreadStack.get(currentThread);
				StackLevel expectedStackLevel = threadStack.mStack.get(threadStack.mStackDepth);
				
				if (!stackLevel.equals(expectedStackLevel))
				{
					System.out.println("[Memory.Stack.Pop] StackLevel is incorrect, Some method has a  mismatch with Push and Pop");
				}
			}
			
			Pop();
		}
		
		@SuppressWarnings("unchecked")
		synchronized public static <T> T New(Class<T> type)
		{	
			Thread currentThread = Thread.currentThread();
			ThreadStack threadStack = mThreadStack.get(currentThread);
			
			Vector<Object> objectList = mObjectPool.get(type);
			if (objectList == null)
			{
				objectList = new Vector<Object>();
				mObjectPool.put(type, objectList);
			}
			
			StackLevel stackLevel = threadStack.mStack.get(threadStack.mStackDepth);
			
			// see if there is any in the pool, if so, move it to the current stack level
			// else increase the pool size
			Object object = null;
			if (objectList.size() > 0)
			{
				// remove from end
				object = objectList.get(objectList.size() - 1);
				objectList.remove(objectList.size() - 1);
			}
			else
			{
				try 
				{
					if (Memory.mDebugMode == true)
					{
						System.out.println("[Memory.Stack.New] Allocating new instance in stack: " + type.getName());
					}
					
					object = type.newInstance();
				} 
				catch (Exception e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (object == null)
				return null;
			
			stackLevel.mObjectList.add(object);
			return (T) object;
		}
		
		public static class StackLevel
		{
			Vector<Object> 	mObjectList			= new Vector<Object>();
			boolean			mCurrent			= false;					// for debugging
		}
		
		// each thread gets its own stack
		public static class ThreadStack
		{
			Vector<StackLevel>	mStack			= new Vector<StackLevel>();
			int					mStackDepth 	= -1;
		}
	
		static HashMap<Class<?>, Vector<Object>> 		mObjectPool 		= new HashMap<Class<?>, Vector<Object>>();
		static HashMap<Thread, ThreadStack>				mThreadStack		= new HashMap<Thread, ThreadStack>();
	}
	
	static public void SetDebugMode(boolean debugMode)
	{
		mDebugMode = debugMode;
	}
	
	static boolean mDebugMode 		= true;
}
