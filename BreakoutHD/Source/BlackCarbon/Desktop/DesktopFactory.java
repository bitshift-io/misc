package BlackCarbon.Desktop;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import BlackCarbon.API.*;

public class DesktopFactory extends Factory
{
	static public class InitParam extends Factory.InitParam
	{		

	}
	
	public DesktopFactory(InitParam param)
	{	
		mPlatform = Platform.Desktop;
		
		if (param == null)
			param = new InitParam();
			
		param.factory = this;
		mInitParam = param;
		
		mNetwork = new Network();
		
		mDevice = new DesktopDevice(param);
		
		mInput = new DesktopInput();
		mInput.mFactory = this;
	}
	
	public InitParam GetInitParam()
	{
		return mInitParam;
	}
	
	public RenderBuffer CreateRenderBuffer()
	{
		RenderBuffer resource = new DesktopRenderBuffer();
		resource.mFactory = this;
		return resource;
	}
	
	public RenderBuffer AcquireRenderBuffer(String name)
	{
		for (int i = 0; i < mRenderBuffer.size(); ++i)
		{
			RenderBuffer resource = mRenderBuffer.get(i);
			if (resource.mName.equalsIgnoreCase(name))
				return resource;
		}
		
		RenderBuffer resource = new DesktopRenderBuffer();
		resource.mFactory = this;
		resource.mName = name;
		mRenderBuffer.add(resource);
		return resource;
	}
	
	public Texture AcquireTexture(String name)
	{
		for (int i = 0; i < mTexture.size(); ++i)
		{
			Texture resource = mTexture.get(i);
			if (resource.mName.equalsIgnoreCase(name))
				return resource;
		}
		
		Texture resource = new DesktopTexture();
		resource.mFactory = this;
		resource.mName = name;
		resource.Load();
		mTexture.add(resource);
		return resource;
	}
	
	public InputStream GetFileResource(String name, String extension)
	{
		String resourceDir = mFactory.GetInitParam().resourceDir;
		String fileName = name + "_" + extension + "." + extension;
		if (resourceDir != null && resourceDir.length() > 0)
			fileName = resourceDir + "/" + fileName;
		
		InputStream is = null;
		try 
		{
			is = new FileInputStream(fileName);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		return is;
	}
	
	protected InitParam	mInitParam;
}
