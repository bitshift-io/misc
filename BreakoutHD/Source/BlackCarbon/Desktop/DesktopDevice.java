package BlackCarbon.Desktop;

import java.nio.FloatBuffer;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;


import BlackCarbon.API.Device;
import BlackCarbon.API.Memory;
import BlackCarbon.API.Device.DeviceState;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;

public class DesktopDevice extends Device
{	
	
	
	public DesktopDevice(DesktopFactory.InitParam param)
	{
		
		try
		{
			Display.setDisplayMode(new DisplayMode(param.width, param.height));
			Display.setTitle(param.windowName);
			Display.create();
			
			
			
			SetWindowVisible(param.windowVisible);


			mFactory = param.factory;


			GL11.glShadeModel(GL11.GL_SMOOTH);              // Enable Smooth Shading
	        
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glLoadIdentity();
	 //       glu.gluOrtho2D(-1.0f, 1.0f, -1.0f, 1.0f); // drawing square
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			GL11.glLoadIdentity();
	        
			GL11.glClearColor(0.2f, 0.2f, 0.5f, 1.0f);
			GL11.glClearDepth(1.0);                    // Enables Clearing Of The Depth Buffer
			GL11.glEnable(GL11.GL_DEPTH_TEST);           // Enables Depth Testing
			GL11.glDepthFunc(GL11.GL_LEQUAL);            // The Type Of Depth Test To Do
			GL11.glDisable(GL11.GL_LIGHTING);
		}
		catch (Exception e)
		{
			System.out.println("[DesktopDevice.DesktopDevice] Exception: " + e.getMessage());
		}
	}
	
	public void SetWindowVisible(boolean visible)
	{
		//Display.
		//mFrame.setVisible(visible);
	}
	
	public boolean Update()
	{
		Begin(); // to bind context to thread
		GL11.glViewport(0, 0, GetWidth(), GetHeight()); // update the viewport;
		Display.update();
		boolean result = !Display.isCloseRequested();
		End();
		return result;
	}
	
	public void SetClearColour(Vector4 colour)
	{
		GL11.glClearColor(colour.GetX(), colour.GetY(), colour.GetZ(), colour.GetW());
	}
	
	public void Clear(int clearFlags)
	{
        int flags = 0;
        if ((clearFlags & CF_Colour) != 0)
        	flags |= GL11.GL_COLOR_BUFFER_BIT;
        
        if ((clearFlags & CF_Depth) != 0)
        	flags |= GL11.GL_DEPTH_BUFFER_BIT;
        
        
        GL11.glClear(flags);
	}
	
	public void Begin()
	{
		AcquireSemaphore();
		
		try 
		{
			//Display.releaseContext();
			Display.makeCurrent();
		} 
		catch (Exception e)
		{
			System.out.println("[DesktopDevice.Begin] Exception: " + e.getMessage());
		}
		/*
		// restore default rendering
		Matrix4 identity = new Matrix4(Matrix4.Init.Identity);
		SetMatrix(identity, MatrixMode.Model);
		SetMatrix(identity, MatrixMode.Texture);
		SetMatrix(identity, MatrixMode.View);
		SetMatrix(identity, MatrixMode.Projection);
		*/
	}
	
	public void End()
	{
		try
		{
			Display.releaseContext();
		} 
		catch (Exception e)
		{
			System.out.println("[DesktopDevice.End] Exception: " + e.getMessage());
		}
		
		ReleaseSemaphore();
	}
	
	public void SetMatrix(Matrix4 matrix, MatrixMode mode)
	{
		Memory.Stack.Push();
		
		switch (mode)
		{
		case Texture:
		{
			mTextureMatrix.Copy(matrix);
			GL11.glMatrixMode(GL11.GL_TEXTURE);
			LoadMatrix(mTextureMatrix);
			break;
		}
			
		case Model:
		{
			mWorld.Copy(matrix);
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			Matrix4 worldView = mWorld.Multiply(mView);
			LoadMatrix(worldView);
			break;
		}
		
		case View:
		{
			mView.Copy(matrix);
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			Matrix4 worldView = mWorld.Multiply(mView);
			LoadMatrix(worldView);
			break;
		}
		
		case Projection:
		{
			mProjection.Copy(matrix);
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			LoadMatrix(mProjection);
			break;
		}
		}
		
		Memory.Stack.Pop();
	}
	
	protected void LoadMatrix(Matrix4 matrix)
	{
		FloatBuffer fb = FloatBuffer.allocate(16);
		
		fb.put(matrix.m[0]);
		fb.put(matrix.m[1]);
		fb.put(matrix.m[2]);
		fb.put(matrix.m[3]);
		
		fb.position(0);
		
		GL11.glLoadMatrix(fb);
	}
	
	public Matrix4 GetMatrix(MatrixMode mode)
	{
		switch (mode)
		{
		case Texture:
			return mTextureMatrix;
			
		case Model:
			return mWorld;
		
		case View:
			return mView;
		
		case Projection:
			return mProjection;
		}
		
		return null;
	}
	
	
	/*
	public void display(GLAutoDrawable glDrawable) 
	{
		//if (!mInit)
		//	return;
		
		GL gl = GetGL();
		gl.glLoadIdentity();
		
		//mTexture.Bind();
		Game.Get().Render();
	}
	*/
	public int GetWidth()
	{
		return Display.getDisplayMode().getWidth();
	}
	
	public int GetHeight()
	{
		return Display.getDisplayMode().getHeight();
	}
	
	/*
	public void SetDeviceState(DeviceState state)
	{
		if (state.alphaBlendEnable)
			GL11.glEnable(GL11.GL_BLEND);
		else
			GL11.glDisable(GL11.GL_BLEND);
			
		int alphaBlendSource = GetBlendMode(state.alphaBlendSource);
		int alphaBlendDest = GetBlendMode(state.alphaBlendDest);
		GL11.glBlendFunc(alphaBlendSource, alphaBlendDest);
		
		if (state.alphaCompare == Device.Compare.Always)
			GL11.glDisable(GL11.GL_ALPHA_TEST);
		else
			GL11.glEnable(GL11.GL_ALPHA_TEST);
		
		int alphaCompare = GetCompare(state.alphaCompare);
		GL11.glAlphaFunc(alphaCompare, state.alphaCompareValue);
	}*/
	
	@Override
	public void Render()
	{	
		// depth functionality
		if ((mDeviceState.dirtyFlags & DeviceState.DF_DepthTestEnable) != 0)
		{
			if (mDeviceState.depthTestEnable)
				GL11.glEnable(GL11.GL_DEPTH_TEST);
			else
				GL11.glDisable(GL11.GL_DEPTH_TEST);
		}
		
		
		// alpha functionality
		if ((mDeviceState.dirtyFlags & DeviceState.DF_AlphaBlendEnable) != 0)
		{
			if (mDeviceState.alphaBlendEnable)
				GL11.glEnable(GL11.GL_BLEND);
			else
				GL11.glDisable(GL11.GL_BLEND);
		}
		
		if ((mDeviceState.dirtyFlags & (DeviceState.DF_AlphaBlendSource | DeviceState.DF_AlphaBlendDest)) != 0)
		{
			int alphaBlendSource = GetBlendMode(mDeviceState.alphaBlendSource);
			int alphaBlendDest = GetBlendMode(mDeviceState.alphaBlendDest);
			GL11.glBlendFunc(alphaBlendSource, alphaBlendDest);
		}
		
		if ((mDeviceState.dirtyFlags & (DeviceState.DF_AlphaCompare | DeviceState.DF_AlphaCompareValue)) != 0)
		{
			if (mDeviceState.alphaCompare == Device.Compare.Always)
				GL11.glDisable(GL11.GL_ALPHA_TEST);
			else
				GL11.glEnable(GL11.GL_ALPHA_TEST);
			
			int alphaCompare = GetCompare(mDeviceState.alphaCompare);
			GL11.glAlphaFunc(alphaCompare, mDeviceState.alphaCompareValue);
		}

		if ((mDeviceState.dirtyFlags & DeviceState.DF_Colour) != 0)
		{
			GL11.glColor4f(mDeviceState.colour.GetX(), mDeviceState.colour.GetY(), mDeviceState.colour.GetZ(), mDeviceState.colour.GetW());
		}
		
		if ((mDirtyFlags & DF_Texture) != 0)
		{
			if (mTexture != null)
			{
				mTexture.Bind();
			}
			else
			{
				GL11.glDisable(GL11.GL_TEXTURE_2D);
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, -1);
			}
		}
		
		if ((mDirtyFlags & DF_RenderBuffer) != 0)
		{
			// need to unbind incase going from VBO to vertex arrays
			mRenderBuffer.Unbind();
			mRenderBuffer.Bind();
		}
		
		mRenderBuffer.Render();
		
		mDeviceState.dirtyFlags = 0;
		mDirtyFlags = 0;
	}
	
	int GetCompare(Compare value)
	{/*
		public enum Compare
		{
			Less,
			LessEqual,
			Equal,
			Greater,
			GreaterEqual,
			Always, // TODO: fix device enums
			Never,
			NotEqual,
		}*/	
		
		int lookup[] = {
				GL11.GL_LESS, 
				GL11.GL_LEQUAL,
				GL11.GL_EQUAL, 
				GL11.GL_GREATER, 
				GL11.GL_GEQUAL,
				GL11.GL_ALWAYS,
				GL11.GL_NEVER,
				GL11.GL_NOTEQUAL, 
		};
		
		return lookup[value.ordinal()];
	}
	
	int GetBlendMode(BlendMode value)
	{/*
		public enum BlendMode
		{
			Zero,
			One,
			DestColour,
			SrcColour,
			OneMinusDestColour,
			OneMinusSrcColour,
			SrcAlpha,
			OneMinusSrcAlpha,
			DestAlpha,
			OneMinusDestAlpha,
			SrcAlphaSaturate,
		}
		*/
		int lookup[] = {
				GL11.GL_ZERO, 
				GL11.GL_ONE,
				GL11.GL_DST_COLOR, 
				GL11.GL_SRC_COLOR, 
				GL11.GL_ONE_MINUS_DST_COLOR,
				GL11.GL_ONE_MINUS_SRC_COLOR,
				GL11.GL_SRC_ALPHA,
				GL11.GL_ONE_MINUS_SRC_ALPHA, 
				GL11.GL_DST_ALPHA,
				GL11.GL_ONE_MINUS_DST_ALPHA, 
				GL11.GL_SRC_ALPHA_SATURATE
		};
		
		return lookup[value.ordinal()];
	}
	
	int GetTopology(GeometryTopology value)
	{
		/*
		public enum GeometryTopology
	{
		LineList,
		PointList,
		TriangleList,
		TriangleListAdjacent,
	}
	
		 */
		int lookup[] = {
				GL11.GL_LINES, 
				GL11.GL_POINT,
				GL11.GL_TRIANGLES, 
				//GL11.GL_TRIANGLES_ADJACENCY_EXT, 
				GL11.GL_TRIANGLE_STRIP,
				GL11.GL_QUADS,
		};
		
		return lookup[value.ordinal()];
	}
	
	public DeviceState GetDeviceState()
	{
		return mDeviceState;
	}
	
	public void DrawLine(Vector4 start, Vector4 end, Vector4 colour)
	{
		GL11.glBegin(GL11.GL_LINES);
		
		if (colour != null)
			GL11.glColor4f(colour.GetX(), colour.GetY(), colour.GetZ(), 1.0f);
		else
			GL11.glColor4f(1.f, 1.f, 1.f, 1.f);
		
		GL11.glVertex3f(start.GetX(), start.GetY(), start.GetZ());
		GL11.glVertex3f(end.GetX(), end.GetY(), end.GetZ());
		GL11.glEnd();
		GL11.glColor4f(1.f, 1.f, 1.f, 1.f);
	}
	
	public void Primitive_Begin(GeometryTopology topology)
	{
		GL11.glBegin(GetTopology(topology));
	}
	
	public void Primitive_End()
	{
		GL11.glEnd();
	}
	
	public void Primitive_Colour(Vector4 value)
	{
		GL11.glColor4f(value.GetX(), value.GetY(), value.GetZ(), value.GetW());
	}
	
	public void Primitive_Vertex(Vector4 value)
	{
		GL11.glVertex3f(value.GetX(), value.GetY(), value.GetZ());//, value.GetW());
	}
	
	public void Primitive_TexCoord(Vector4 value)
	{
		GL11.glTexCoord4f(value.GetX(), value.GetY(), value.GetZ(), value.GetW());
	}
}
