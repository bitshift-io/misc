package BlackCarbon.Desktop;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBVertexBufferObject;
import org.lwjgl.opengl.GL11;

import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.RenderBuffer;
import BlackCarbon.Math.Vector4;

// http://lwjgl.org/wiki/doku.php/lwjgl/tutorials/opengl/basicvbo
public class DesktopRenderBuffer extends RenderBuffer
{
	private void BindStatic()
	{
		if (mPositionID != -1)
		{
			GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
			ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, mPositionID);
			GL11.glVertexPointer(3, GL11.GL_FLOAT, 0, 0);
		}
		else
		{
			GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		}
		
		if (mColourID != -1)
		{
			GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
			ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, mColourID);
			GL11.glColorPointer(4, GL11.GL_FLOAT, 0, 0);
		}
		else
		{
			GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
		}
        
		if (mUVID != -1)
		{
			GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
			ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, mUVID);
			GL11.glTexCoordPointer(2, GL11.GL_FLOAT, 0, 0);
		}
		else
		{
			GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		}
		
		if (mIndexID != -1)
		{
			ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, mIndexID);
		}
	}
	
	private void BindDynamic()
	{
		if (mPosition != null)
		{
			GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
			
			try
			{
				GL11.glVertexPointer(3, 0, mPosition);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		}
		
		if (mColour != null)
		{
			GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
			GL11.glColorPointer(4, 0, mColour);
		}
		else
		{
			GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
		}
        
		if (mUV != null)
		{
			GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
			GL11.glTexCoordPointer(2, 0, mUV);
		}
		else
		{
			GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		}
	}
	
	public void Bind()
	{
		if (mUsage == Dynamic)
			BindDynamic();
		else
			BindStatic();
	}
	
	public void Unbind()
	{
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, 0);
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
		
		GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
		GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		GL11.glDisableClientState(GL11.GL_INDEX_ARRAY);
	}
	
	public void Render()
	{	
		//GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		//GL11.glDisable(GL11.GL_CULL_FACE); // double sided plz for a bit
		//GL11.glFrontFace(GL11.GL_CW);
		
		try
		{
			if (mUsage == Dynamic)
				GL11.glDrawElements(GL11.GL_TRIANGLES, mIndex);
			else
				GL11.glDrawElements(GL11.GL_TRIANGLES, mIndexCount, GL11.GL_UNSIGNED_BYTE, 0);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private int CreateVBOWithData(FloatBuffer data)
	{
		IntBuffer buffer = BufferUtils.createIntBuffer(1);
	    ARBVertexBufferObject.glGenBuffersARB(buffer);
	    int id = buffer.get(0);
	    
		ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, id);
	    ARBVertexBufferObject.glBufferDataARB(ARBVertexBufferObject.GL_ARRAY_BUFFER_ARB, data, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);

	    return id;
	}

	public void MakeStatic()
	{
		mUsage = Static;
		
		Device device = Factory.Get().GetDevice();
		device.Begin();
		
		// index buffer to VBO
		try
		{
			mIndexCount = mIndex.capacity();
			
			// dynamic
			IntBuffer buffer = BufferUtils.createIntBuffer(1);
			//mIndexID = buffer.get(0);
		    ARBVertexBufferObject.glGenBuffersARB(buffer);
		    mIndexID = buffer.get(0);
		    
		    ARBVertexBufferObject.glBindBufferARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, mIndexID);
		    ARBVertexBufferObject.glBufferDataARB(ARBVertexBufferObject.GL_ELEMENT_ARRAY_BUFFER_ARB, mIndex, ARBVertexBufferObject.GL_STATIC_DRAW_ARB);
		    mIndex = null;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	    
		if (mPosition != null)
		{
			mPositionID = CreateVBOWithData(mPosition);
			mPosition = null;
		}
		
		if (mUV != null)
		{
			mUVID = CreateVBOWithData(mUV);
			mUV = null;
		}
		
		if (mColour != null)
		{
			mColourID = CreateVBOWithData(mColour);
			mColour = null;
		}
		
	    Unbind();
	    
		device.End();
	}
	
	@Override
	public int GetIndexCount()
	{
		if (mUsage == Static)
			return mIndexCount;
		
		return super.GetIndexCount();
	}
	
	int 	mPositionID		= -1;
	int		mUVID			= -1;
	int		mColourID		= -1;
	int		mIndexID		= -1;
	
	int		mIndexCount		= 0;
}
