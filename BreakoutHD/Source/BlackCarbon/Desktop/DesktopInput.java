package BlackCarbon.Desktop;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import BlackCarbon.API.Input;

public class DesktopInput extends Input
{	
	DesktopInput()
	{
		super();
		
		try
		{
			Keyboard.create();
		}
		catch (Exception e)
		{
			System.out.println("[DesktopInput.DesktopInput] " + e.getMessage());
		}
	}
	
	public void Update()
	{
		super.Update();
		
		Keyboard.poll();
		//int count = Keyboard.getNumKeyboardEvents();
		while (Keyboard.next()) 
		{/*
			int character_code = ((int)Keyboard.getEventCharacter()) & 0xffff;

			System.out.println("Checking key:" + Keyboard.getKeyName(Keyboard.getEventKey()));
			System.out.println("Pressed:" + Keyboard.getEventKeyState());
			System.out.println("Key character code: 0x" + Integer.toHexString(character_code));
			System.out.println("Key character: " + Keyboard.getEventCharacter());
			System.out.println("Repeat event: " + Keyboard.isRepeatEvent());
*/
			Control control = ControlLookup(Keyboard.getEventKey());
			float curState = mState[control.ordinal()].state;
			float newState = Keyboard.getEventKeyState() ? 1.f : 0.f;
			mState[control.ordinal()].state = newState;
			if (curState != newState && newState <= 0.f)
				mState[control.ordinal()].pressed = true;
		}

		Mouse.poll();
		while (Mouse.next())
		{
			ControlState state = mState[Input.Control.MouseAxisX.ordinal()];
			state.state = Mouse.getEventX();
			
			state = mState[Input.Control.MouseAxisY.ordinal()];
			state.state = Mouse.getEventY();
			
			if (Mouse.getEventButton() >= 0)
			{
				//
				Control control = ControlLookup(Mouse.getEventButton());
				float curState = mState[control.ordinal()].state;
				float newState = Mouse.getEventButtonState() ? 1.f : 0.f; 
				mState[control.ordinal()].state = newState;
				if (curState != newState && newState <= 0.f)
					mState[control.ordinal()].pressed = true;
				
				//System.out.println("mouse: " + Mouse.getEventButton() + " event state: " + newState);
			}
		}
	}
	
	public Control ControlLookup(int VK)
	{
		for (int i = 0; i < mControlLookup.length; ++i)
		{
			if (mControlLookup[i].VK == VK)
				return mControlLookup[i].control;
		}
		
		return Control.Unknown;
	}
	
	class ControlLookup
	{
		public ControlLookup(int _VK, Control _control)
		{
			VK = _VK;
			control = _control;
		}
		
		public int 		VK;
		public Control 	control;
	}
	
	protected ControlLookup[] mControlLookup =
	{
			new ControlLookup(Keyboard.KEY_ESCAPE, Control.KeyEscape),
			new ControlLookup(Keyboard.KEY_RETURN, Control.KeyEnter),
			new ControlLookup(Keyboard.KEY_LEFT, Control.KeyLeft),
			new ControlLookup(Keyboard.KEY_RIGHT, Control.KeyRight),
			new ControlLookup(Keyboard.KEY_UP, Control.KeyUp),
			new ControlLookup(Keyboard.KEY_DOWN, Control.KeyDown),
			new ControlLookup(0, Control.MouseLeft),
			new ControlLookup(1, Control.MouseRight),
			new ControlLookup(2, Control.MouseMiddle),
	};
}
