fn fnGetTVertPos mObj mVert mChannel =
(
	local mPos = undefined
	
	-- get vert faces
	local mFaceList = (meshop.getfacesusingvert mObj mVert) as array
	
	local mFaceVerts = (meshop.getvertsusingface mObj mFaceList[1]) as array
	local mTFaceVerts = meshop.getMapFace mObj mChannel mFaceList[1]
	local mFind = finditem mFaceVerts mVert

	mPos = meshop.getMapVert mObj mChannel mTFaceVerts[mFind]
	
	return mPos
)

fn fnExport mSelection mOutputFile =
(
	local mSelection = selection as array
	local mOutputFile = "d:\\testmesh_msh.msh"
	
	-- assume for now 1 object as editmesh
	mObj = mSelection[1]
	
	-- open/create binary file
	local mBinStream = fopen mOutputFile "wb"
	
	-- write the header
	local mNumFaces = mObj.numfaces
	local mNumIndices = mNumFaces * 3.0
	local mNumVerts = mObj.numverts
	
	if mObj.material != undefined then
		local mMaterialName = mObj.material.name
	else
		local mMaterialName = ""
	
	local mVertexFormat = 0x1
	if (meshop.getMapSupport mObj 0) or (meshop.getMapSupport mObj -2) do
		mVertexFormat = bit.or mVertexFormat 0x4
	if (meshop.getMapSupport mObj 1) do
		mVertexFormat = bit.or mVertexFormat 0x2
	
	WriteByte mBinStream mNumIndices #unsigned
	WriteByte mBinStream mNumVerts #unsigned
	WriteByte mBinStream mVertexFormat #unsigned
	WriteByte mBinStream mMaterialName.count #unsigned 

	-- write the body
	
	-- material name
	if mMaterialName.count > 0 do
		WriteString mBinStream mMaterialName
	
	-- faces
	for f = 1 to mNumFaces do
	(
		local mFaceVerts = (meshop.getVertsUsingFace mObj f) as array
		format "%\n" mFaceVerts
		for v = 1 to mFaceVerts.count do
			WriteByte mBinStream (mFaceVerts[v] - 1) #unsigned -- make this 0 based
	)
	
	-- verts
	for v = 1 to mNumVerts do
	(
		local mVPos = meshop.getVert mObj v
		print mVPos
		-- swap y,z
		WriteFloat mBinStream mVPos[1]
		WriteFloat mBinStream mVPos[3]
		WriteFloat mBinStream mVPos[2]
	)
	
	-- Note: Assume for for now 1 text coord/color per vert, will cause issues later. Blame Fab....
	
	-- map channel 1
	if (meshop.getMapSupport mObj 1) do
		for v = 1 to mNumVerts do
		(
			local mTV = fnGetTVertPos mObj v 1
			print mTV
			WriteFloat mBinStream mTV[1]
			WriteFloat mBinStream mTV[2]
		)		
	
	-- color + alpha
	if (meshop.getMapSupport mObj 0) or (meshop.getMapSupport mObj -2) do
		for v = 1 to mNumVerts do
		(
			if (meshop.getMapSupport mObj 0) then
				local mVColor = (fnGetTVertPos mObj v 0)
			else
				local mVColor = [1,1,1]
			
			if (meshop.getMapSupport mObj -2) then
				local mVAlpha = (fnGetTVertPos mObj v -2) 
			else
				local mVAlpha = [1,1,1]
			
			WriteFloat mBinStream mVColor[1]
			WriteFloat mBinStream mVColor[2]
			WriteFloat mBinStream mVColor[3]
			WriteFloat mBinStream mVAlpha[1]
		)	
	
	
	FClose mBinStream
	
	
)
