REM  all we really need to do is copy data in to the appropriate folder:)
REM copy this and call it Build.batand make this PreBuild to copy the data and make the appropriate folders


set PROJECT=BreakoutHD
set SDKDIR=D:\BlackCarbon\SDK\android-sdk\platforms\android-1.5
set APKDIR=D:\BlackCarbon\SDK\android-sdk\tools
set PATH=%PATH%;%SDKDIR%/tools;%APKDIR%

cd ..
cd ..

md Project\Android\gen
md Project\Android\res
md Project\Android\res\raw
copy /y Binary\Data Project\Android\res\raw
xcopy /e /y Binary\Android\Data Project\Android\res


REM Generate R.java
REM     aapt p -m -J Project/Android/gen -M Project/Android/AndroidManifest.xml -S Project/Android/res -I %SDKDIR%/android.jar

REM Compile code
md Project\Android\bin
REM     javac -encoding ascii -target 1.5 -d Project/Android/bin -sourcepath Source -bootclasspath %SDKDIR%/android.jar Source/%PROJECT%/Android/*.java

REM Compile resources
REM     aapt p -f -M Project/Android/AndroidManifest.xml -S Project/Android/res -I %SDKDIR%/android.jar -F Project/Android/bin/resources.ap_


REM Convert to byte code
REM     call dx --dex --verbose --debug --output=%CD%/Project/Android/bin/classes.dex %CD%/Project/Android/bin

echo call apkbuilder %CD%/Project/Android/bin/%PROJECT%.apk -z %CD%/Project/Android/bin/resources.ap_ -f %CD%/Project/Android/bin/classes.dex -rf %CD%/Source
REM Combine resources with compiled code
REM     call apkbuilder %CD%/Project/Android/bin/%PROJECT%.apk -z %CD%/Project/Android/bin/resources.ap_ -f %CD%/Project/Android/bin/classes.dex -rf %CD%/Source

rem pause