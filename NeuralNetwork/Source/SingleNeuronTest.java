import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SingleNeuronTest extends JFrame implements ActionListener
{
	Neuron	mNeuron = new Neuron();
	
    JLabel label_1;
    JTextField input0;
    JTextField input1;
    JLabel label_2;
    JTextField weight0;
    JTextField weight1;
    JLabel label_3;
    JTextField summedWeight;
    JLabel label_4;
    JTextField threshold;
    JLabel label_5;
    JTextField output;

    public SingleNeuronTest() {
    	
    	mNeuron.SetInputSize(2);
    	
        SingleNeuronTestLayout customLayout = new SingleNeuronTestLayout();

        getContentPane().setFont(new Font("Helvetica", Font.PLAIN, 12));
        getContentPane().setLayout(customLayout);

        label_1 = new JLabel("Inputs");
        getContentPane().add(label_1);

        input0 = new JTextField("0");
        getContentPane().add(input0);

        input1 = new JTextField("0");
        getContentPane().add(input1);

        label_2 = new JLabel("Weights");
        getContentPane().add(label_2);

        weight0 = new JTextField("1");
        getContentPane().add(weight0);

        weight1 = new JTextField("1");
        getContentPane().add(weight1);

        label_3 = new JLabel("Summed Weight");
        getContentPane().add(label_3);

        summedWeight = new JTextField("0");
        getContentPane().add(summedWeight);

        label_4 = new JLabel("Threshold");
        getContentPane().add(label_4);

        threshold = new JTextField("1");
        getContentPane().add(threshold);

        label_5 = new JLabel("Output");
        getContentPane().add(label_5);

        output = new JTextField("0");
        getContentPane().add(output);

        setSize(getPreferredSize());

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        
        
        // set listeners for textfields
        input0.addActionListener(this);
        input1.addActionListener(this);
        weight0.addActionListener(this);
        weight1.addActionListener(this);
        threshold.addActionListener(this);
        
        // set up initial values
        Calculate();
    }
    
    public void Calculate()
    {
    	mNeuron.SetWeight(0, Float.valueOf(weight0.getText().trim()));
    	mNeuron.SetWeight(1, Float.valueOf(weight1.getText().trim()));
    	
    	//mNeuron.SetThreshold(Float.valueOf(threshold.getText().trim()));
    	
    	// set up values
        float[] inputs = new float[2];
        inputs[0] = Float.valueOf(input0.getText().trim());
        inputs[1] = Float.valueOf(input1.getText().trim());
        mNeuron.Calculate(inputs);
        
        // display results
        weight0.setText("" + mNeuron.GetWeight(0));
        weight1.setText("" + mNeuron.GetWeight(1));
        summedWeight.setText("" + mNeuron.GetWeightedInput());
        //threshold.setText("" + mNeuron.GetThreshold());
        output.setText("" + mNeuron.GetOutput());
    }
    
    @Override
    public void actionPerformed(ActionEvent event)
    {
    	Calculate();
    }

    public static void Show()
    {
        SingleNeuronTest window = new SingleNeuronTest();

        window.setTitle("SingleNeuronTest");
        window.pack();
        window.show();
    }
}

class SingleNeuronTestLayout implements LayoutManager {

    public SingleNeuronTestLayout() {
    }

    public void addLayoutComponent(String name, Component comp) {
    }

    public void removeLayoutComponent(Component comp) {
    }

    public Dimension preferredLayoutSize(Container parent) {
        Dimension dim = new Dimension(0, 0);

        Insets insets = parent.getInsets();
        dim.width = 690 + insets.left + insets.right;
        dim.height = 423 + insets.top + insets.bottom;

        return dim;
    }

    public Dimension minimumLayoutSize(Container parent) {
        Dimension dim = new Dimension(0, 0);
        return dim;
    }

    public void layoutContainer(Container parent) {
        Insets insets = parent.getInsets();

        Component c;
        c = parent.getComponent(0);
        if (c.isVisible()) {c.setBounds(insets.left+8,insets.top+8,72,24);}
        c = parent.getComponent(1);
        if (c.isVisible()) {c.setBounds(insets.left+8,insets.top+40,72,24);}
        c = parent.getComponent(2);
        if (c.isVisible()) {c.setBounds(insets.left+8,insets.top+72,72,24);}
        c = parent.getComponent(3);
        if (c.isVisible()) {c.setBounds(insets.left+96,insets.top+8,72,24);}
        c = parent.getComponent(4);
        if (c.isVisible()) {c.setBounds(insets.left+96,insets.top+40,72,24);}
        c = parent.getComponent(5);
        if (c.isVisible()) {c.setBounds(insets.left+96,insets.top+72,72,24);}
        c = parent.getComponent(6);
        if (c.isVisible()) {c.setBounds(insets.left+184,insets.top+8,72,24);}
        c = parent.getComponent(7);
        if (c.isVisible()) {c.setBounds(insets.left+184,insets.top+56,72,24);}
        c = parent.getComponent(8);
        if (c.isVisible()) {c.setBounds(insets.left+272,insets.top+8,72,24);}
        c = parent.getComponent(9);
        if (c.isVisible()) {c.setBounds(insets.left+272,insets.top+56,72,24);}
        c = parent.getComponent(10);
        if (c.isVisible()) {c.setBounds(insets.left+352,insets.top+8,72,24);}
        c = parent.getComponent(11);
        if (c.isVisible()) {c.setBounds(insets.left+352,insets.top+56,72,24);}
    }
}
