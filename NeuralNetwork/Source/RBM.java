import java.util.ArrayList;
import java.util.Random;

import java.awt.List;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;

public class RBM 
{
	public Random r = new Random(12345);
	public float sig = 0.2f;
	public float epsW = 0.5f;
	public float epsA = 0.5f;
	public int nvis;
	public int nhid;
	//public int Ndat = 10;
	public float moment = 0.9f;
	public float cost = 0.000001f;
	
	public float[] Svis0;
	public float[] Svis;
	public float[] Shid;
	
	// weights, VISIBLE = ROWS
	// HIDDEN = COLUMNS
	public float[][] W;
	public float[][] dW;
	
	public float[] Avis;
	public float[] Ahid;
	public float[] dA;
	//public float[][] dat;
	
	public float[] stat;
	public float[] stat2;
	public int ksteps = 1;
	
	public ArrayList<Point2D.Float> dat = new ArrayList<Point2D.Float>();
	
	public RBM(int numVis, int numHidden)
	{
		nvis = numVis;
		nhid = numHidden;
		
		Svis0 = array(nvis + 1, 0.f);
		Svis = array(nvis + 1, 0.f);
		Shid = array(nhid + 1, 0.f);
		
		Svis[nvis] = 1.0f;
		Svis0[nvis] = 1.0f;
		
		W = standardNormal(nvis + 1, nhid + 1, 0.1f);
		dW = standardNormal(nvis + 1, nhid + 1, 0.001f);
		
		Avis = array(nvis + 1, 0.1f);
		Ahid = array(nhid + 1, 1.0f);
		dA = array(nvis + 1, 0.0f);
		
		// training set data
		// two clusters, one near 0, 1
		// the other near 1, 0
		dat.add(new Point2D.Float(0.f, 1.0f));
		dat.add(new Point2D.Float(0.1f, 0.9f));
		dat.add(new Point2D.Float(0.1f, 0.8f));
		
		dat.add(new Point2D.Float(1.0f, 0.f));
		dat.add(new Point2D.Float(0.9f, 0.1f));
		dat.add(new Point2D.Float(0.8f, 0.1f));
	}
	
	float[] array(int count, float defaultValue)
	{
		float[] a = new float[count];
		for (int i = 0; i < count; ++i)
		{
			a[i] = defaultValue;
		}
		return a;
	}
	
	float standardNormal(int x)
	{
		return (float) r.nextGaussian();
	}

	float[][] standardNormal(int x, int y, float multiplier)
	{
		float[][] array = new float[x][y];
		for (int i = 0; i < x; ++i)
		{
			for (int k = 0; k < y; ++k)
			{
				array[i][k] = multiplier * (float) r.nextGaussian();
			}
		}
		return array;
	}
	
	// Sigmoidal function
	float sigFun(float X, float A)
	{
		float lo = 0.0f;
		float hi = 1.0f;
		return ((lo + (hi - lo)) / (1.0f + (float)Math.exp(-A * X)));
	}
	
	// I think this function does the actual calculations to move
	// data through the neural net
	// activate visible neurons = 0
	// activate hidden neurons = 1
	void activ(int who)
	{
		if (who == 0)
		{
			for (int v = 0; v < nvis; ++v)
			{
				Svis[v] = 0.0f;
				for (int h = 0; h < nhid; ++h)
				{
					Svis[v] += Shid[h] * W[v][h];
				}
				
				// whats this next line do?
				//Svis[v] += sig * standardNormal(nvis + 1);
				Svis[v] = sigFun(Svis[v], Avis[v]);
			}
			Svis[nvis] = 1.0f; // bias
		}
		
		if (who == 1)
		{
			for (int h = 0; h < nhid; ++h)
			{
				Shid[h] = 0.0f;
				for (int v = 0; v < nvis; ++v)
				{
					Shid[h] += Svis[v] * W[v][h];
				}
				
				// whats this next line do?
				//Shid[h] += sig * standardNormal(nhid + 1);
				Shid[h] = sigFun(Shid[h], Ahid[h]);
			}
			Shid[nvis] = 1.0f; // bias
		}
	}
	
	public void learn(int epochmax)
	{
		float[] Err = array(epochmax, 0.0f);
		float[] E = array(epochmax, 0.0f);
		stat = array(epochmax, 0.0f);
		stat2 = array(epochmax, 0.0f);
		
		int ksteps = 1;
		
		for (int epoch = 1; epoch < epochmax; ++epoch)
		{
			float[][] wpos = new float[nvis + 1][nhid + 1];
			float[][] wneg = new float[nvis + 1][nhid + 1];
			float[] apos = array(nhid + 1, 0.0f);
			float[] aneg = array(nhid + 1, 0.0f);
			
			if (epoch > 0)
			{
				ksteps = 50;
			}
			if (epoch > 1000)
			{
				ksteps = (epoch - epoch % 100) / 100 + 40;
			}
			this.ksteps = ksteps;
			
			for (int point = 0; point < dat.size(); ++point)
			{
				Svis0[0] = dat.get(point).x;
				Svis0[1] = dat.get(point).y;
				Svis = Svis0;
				
				// positive phase
				activ(1);
				wpos = add(wpos, outer(Svis, Shid));
				apos = add(apos, multiply(Shid, Shid));
				
				// negative phase
				activ(0);
				activ(1);
				
				for (int recstep = 0; recstep < ksteps; ++recstep)
				{
					activ(0);
					activ(1);
				}
				
				float[][] tmp = outer(Svis, Shid);
				wneg = add(wneg, tmp);
				aneg = add(aneg, multiply(Shid, Shid));
				
				// calculate error delta
				float error = 0.0f;
				for (int i = 0; i < nvis; ++i)
				{
					float delta = Svis0[i] - Svis[i];
					delta = delta * delta;
					error += delta;
				}
				
				Err[epoch] += error;
				//E[epoch] = E[epoch] - 
			}
			
			// adjust weights?
		}
	}
	
	public float[] multiply(float[] a, float[] b)
	{
		float[] r = new float[a.length];
		for (int x = 0; x < a.length; ++x)
		{
			r[x] = a[x] * b[x];
		}
		return r;
	}
	
	public float[] add(float[] a, float[] b)
	{
		float[] r = new float[a.length];
		for (int x = 0; x < a.length; ++x)
		{
			r[x] = a[x] + b[x];
		}
		return r;
	}
	
	public float[][] add(float[][] a, float[][] b)
	{
		// adds each element from a to each element of b
		// r[x][y] = a[x][y] + b[x][y]
		float[][] r = new float[a.length][a[0].length];
		
		for (int x = 0; x < a.length; ++x)
		{
			for (int y = 0; y < a[0].length; ++y)
			{
				r[x][y] = a[x][y] + b[x][y];
			}
		}
		
		return r;
	}
	
	public float[][] outer(float[] a, float[] b)
	{
		// returns a 2D array
		// r[0] = b * a[0]
		// r[1] = b * a[1]
		// r[2] = b * a[2] ...
		float[][] r = new float[a.length][b.length];
		for (int x = 0; x < a.length; ++x)
		{
			for (int y = 0; y < b.length; ++y)
			{
				r[x][y] = b[y] * a[x];
			}
		}
		return r;
	}
	
	public void reconstruct(int Npoint, int Nsteps)
	{
		
	}
	
	public static void test()
	{
		RBM rbm = new RBM(2,8);
		rbm.learn(2);
		rbm.reconstruct(4, 1);
	}
}
