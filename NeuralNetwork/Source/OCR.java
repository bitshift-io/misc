import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;



// Optical character recognition
public class OCR 
{
	static class Image
	{
		String 	name;
		int 	width = 0;
		int 	height = 0;
		float 	pixels[];
	}
	
	OCR()
	{
		// set up for a 5x7 image
		mNet = new NNet();
		NNet.NeuronLayer[] layer = new NNet.NeuronLayer[2];
		layer[0] = new NNet.NeuronLayer(70);
		layer[1] = new NNet.NeuronLayer(256);
		mNet.Create(35, layer);
	}
	
	public void Train()
	{
		// load the training data
		// the files are named of the form:
		// [character]_[index].png
		File dir = new File("Data_5x7");
		
		FilenameFilter filter = new FilenameFilter() {
			    public boolean accept(File dir, String name) {
			        return name.endsWith(".png");
			    }
			};
			
		Vector<Image> imageList = new Vector<Image>();	
		String[] children = dir.list();
		
		Pattern pattern = Pattern.compile(".*?/(.*?)_.*");
		for (int i = 0; i < children.length; ++i)
		{
			Image image = LoadImage("Data_5x7/" + children[i]);
			
			// extract the character this image represents and modify the image name
			Matcher matcher = pattern.matcher(image.name);
			
			if (matcher.matches())
			{
				String c = matcher.group(1);
				image.name = c;
				imageList.add(image);
			}
		}
		
		int trainingIterations = 100;
		for (int t = 0; t < trainingIterations; ++t)
		{
			System.out.println("Training Iteration: " + t);
			for (int i = 0; i < imageList.size(); ++i)
			{
				Image image = imageList.get(i);
				TrainImage(image, image.name.charAt(0));
			}
		}
		
		/*
		// load training data
		Vector<Vector<Image>> imageList = new Vector<Vector<Image>>();		
		for (int c = 0; c < 3; ++c)
		{
			Vector<Image> img = new Vector<Image>();
			imageList.add(img);
			
			for (int i = 0; i < 3; ++i)
			{
				img.add(LoadImage("Data_5x7/" + c + "_" + i + ".png"));
			}
		}
		
		int trainingIterations = 100;
		for (int t = 0; t < trainingIterations; ++t)
		{
			System.out.println("Training Iteration: " + t);
			
			// iterative learning
			// lets just teach this sample neural net 1 character, such that it has a single output
			for (int c = 0; c < imageList.size(); ++c)
			//int c = 0;
			{
				Vector<Image> img = imageList.get(c);
				for (int i = 0; i < img.size(); ++i)
				{
					TrainImage(img.get(i), c);
				}
			}
		}
		
		// test
		System.out.println("Image 0_0 classified as: " + ClassifyImage(imageList.get(0).get(0)));
		System.out.println("Image 1_0 classified as: " + ClassifyImage(imageList.get(1).get(0)));
		System.out.println("Image 2_0 classified as: " + ClassifyImage(imageList.get(2).get(0)));*/
	}
	
	void TrainImage(Image image, int expectedResult)
	{
		float[] inputs = image.pixels; //new float[image.pixels.length];
		/*
		for (int i = 0; i < image.pixels.length; ++i)
		{
			//int r = (image.pixels[i] >> 16) & 0x000000ff;
			//int g = (image.pixels[i] >> 8) & 0x000000ff;
			int b = image.pixels[i] & 0x000000ff;
			
			inputs[i] = b > 0.f ? 1.f : 0.f;
		}*/
		
		float expectedResults[] = new float[256];
		for (int i = 0; i < expectedResults.length; ++i)
		{
			expectedResults[i] = 0.f;
			if (i == expectedResult)
				expectedResults[i] = 1.f;
		}
		
		
		float[] output = mNet.Train(inputs, expectedResults);
		
		//System.out.println("\toutput: " + output + " expected: " + expectedResult + " error: " + (expectedResult - output));
	}
	
	float[] ClassifyImage(Image image)
	{
		return mNet.Run(image.pixels);
		/*
		float[] inputs = image.pixels;
			/*
			new float[image.pixels.length];
		for (int i = 0; i < image.pixels.length; ++i)
		{
			//int r = (image.pixels[i] >> 16) & 0x000000ff;
			//int g = (image.pixels[i] >> 8) & 0x000000ff;
			//int b = image.pixels[i] & 0x000000ff;
			
			inputs[i] = image.pixels[i] > 0.f ? 1.f : 0.f;
		}* /
		
		//run
		float results[] = mNet.Run(inputs);
		
		// find hit with greatest chance of being our image
		int resultIndex = 0;
		for (int i = 0; i < results.length; ++i)
		{
			if (results[i] > results[resultIndex])
			{
				resultIndex = i;
			}
		}
		
		return resultIndex;*/
	}
	
	Image LoadImage(String name)
	{
		try
		{
			System.out.println("loading: " + name);
			
			File f = new File(name); //resourceDir + "/" + mName + "_tex.png");
			if (!f.exists())
				return null;
			
			int width = 0;
			int height = 0;
			
			int srcPixels[];
			
			BufferedImage bi = ImageIO.read(f);
			width = bi.getWidth();
			height = bi.getHeight();
			
			
			srcPixels = bi.getRGB(0, 0, width, height, null, 0, width);  
			
			float[] dstPixels = new float[srcPixels.length];

			for (int i = 0; i < srcPixels.length; ++i)
			{	
				//pixels[i] = (pixels[i] == -1) ? 0 : 1;
				
				int r = (srcPixels[i] >> 16) & 0x000000ff;
				dstPixels[i] = (float)r;
				/*
				int b = pixels[i] & 0x000000ff;
				
				pixels[i] &= 0xff00ff00;
				pixels[i] |= r;
				pixels[i] |= (b << 16);*/
			}
			
			Image image = new Image();
			image.pixels = dstPixels;
			image.width = width;
			image.height = height;
			image.name = name;
			return image;
		}
		catch (Exception e)
		{
			
		}
		
		return null;
	}
	
	void SaveImage(Image image)
	{
		// find a free filename to save the image as
		int index = 0;
		File f = null;
		do
		{
			String fileName = "Data_5x7/" + image.name + "_" + Integer.toString(index) + ".png";
			f = new File(fileName);
			++index;
		} while (f.exists());
		
		// render the image to an actual saveable image surface
		try 
		{
			BufferedImage bufferedImage = new BufferedImage(image.width, image.height, BufferedImage.TYPE_INT_ARGB);
			
			// Create a graphics contents on the buffered image
		    Graphics2D g2d = bufferedImage.createGraphics();

		    // Draw graphics
		    Color transparent = new Color(0.0f, 0.0f, 0.0f, 0.0f);
		    g2d.setColor(transparent);
		    g2d.fillRect(0, 0, image.width, image.height);
		    
		    for (int y = 0; y < image.height; ++y)
		    {
		    	for (int x = 0; x < image.width; ++x)
			    {
		    		float pixel = image.pixels[x + (y * image.width)];
		    		if (pixel > 0.0f)
		    		{
		    			Color c = new Color(pixel, pixel, pixel, 1.f);
		    			g2d.setColor(c);
		    			g2d.fillRect(x, y, 1, 1);
		    		}
			    }
		    }

		    // Graphics context no longer needed so dispose it
		    g2d.dispose();
		    
			ImageIO.write(bufferedImage, "png", f);
		} 
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// normalise the image from 1 to 256
	//
	void Normalize(Image image)
	{
		// zero is reserved for nothing
		// we want to push the minimum value up from just above 0
		// to the range say 0.5 > 1.0
		float max = 0;
		float min = 1;
		for (int i = 0; i < image.pixels.length; ++i)
		{
			if (image.pixels[i] > max)
				max = image.pixels[i];
			
			if (image.pixels[i] < min && image.pixels[i] > 0.0f)
				min = image.pixels[i];
		}
		
		// Normalise from 0.5 > 1.0 range
		float normalization = (1.0f / max) * 0.5f;
		
		for (int i = 0; i < image.pixels.length; ++i)
		{
			if (image.pixels[i] > 0.0f)
			{
				image.pixels[i] = 0.5f + (image.pixels[i] * normalization);
			}
		}
	}
	
	int				mCharacterWidth = 5;
	int				mCharacterHeight = 7;
	
	NNet 			mNet;
}
