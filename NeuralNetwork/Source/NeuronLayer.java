import java.util.Vector;


public class NeuronLayer
{
	NeuronLayer(int size)
	{
		mNeurons.setSize(size);
		
		for (int i = 0; i < size; ++i)
			mNeurons.set(i, new Neuron());
	}
	
	Neuron GetNeuron(int index)
	{
		return mNeurons.get(index);
	}
	
	void SetInputSize(int size)
	{
		for (int i = 0; i < mNeurons.size(); ++i)
			GetNeuron(i).SetInputSize(size);
	}
	
	//
	// given inputs
	//
	public void Calculate(float[] inputs)
	{
		for (int i = 0; i < mNeurons.size(); ++i)
		{
			GetNeuron(i).Calculate(inputs);
		}
	}
	
	public float[] GetOutputs()
	{
		float[] outputs = new float[mNeurons.size()];
		for (int i = 0; i < mNeurons.size(); ++i)
		{
			outputs[i] = GetNeuron(i).GetOutput();
		}
		
		return outputs;
	}
	
	public void AdjustWeights(float delta)
	{
		for (int i = 0; i < mNeurons.size(); ++i)
		{
			Neuron neuron = GetNeuron(i);
			float weight = neuron.GetWeight(i);
			neuron.SetWeight(i, weight + delta);
		}
	}
	
	public int GetSize()
	{
		return mNeurons.size();
	}
	
	public void Iterate(NeuronIterator it)
	{
		for (int i = 0; i < mNeurons.size(); ++i)
		{
			Neuron neuron = GetNeuron(i);
			neuron.Iterate(it);
		}
	}
	
	Vector<Neuron> mNeurons = new Vector<Neuron>();
}
