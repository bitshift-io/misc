import java.util.Vector;


public class NeuralNetwork 
{
	public void Create(int intputCount, int outputCount, int hiddenLayerCount)
	{/*
		if (hiddenLayerCount == 0)
		{
			NeuronLayer layer = new NeuronLayer(outputCount);
			layer.SetInputSize(intputCount);
			AddLayer(layer);
		}
		else
		{
			//  TODO
			
			
		}
		*/
		NeuronLayer layer = null;
		
		for (int h = 0; h < hiddenLayerCount; ++h)
		{
			layer = new NeuronLayer(intputCount);
			layer.SetInputSize(intputCount);
			AddLayer(layer);
		}
		
		layer = new NeuronLayer(outputCount);
		layer.SetInputSize(intputCount);
		AddLayer(layer);
	}
	
	// call after layers have been added to set up weights
	public void CreateConnections()
	{
		int prevLayerOutputSize = mLayers.firstElement().GetSize();
		for (int i = 0; i < mLayers.size(); ++i)
		{
			GetLayer(i).SetInputSize(prevLayerOutputSize);
			prevLayerOutputSize = GetLayer(i).GetSize();
		}
	}
	
	// call after CreateConnections
	public void RandomiseWeights()
	{
		for (int l = 0; l < mLayers.size(); ++l)
		{
			for (int n = 0; n < GetLayer(l).GetSize(); ++n)
			{
				Neuron neuron = GetLayer(l).GetNeuron(n);
				neuron.InitRandom();
			}
		}
	}
	
	public void AdjustWeights(float delta)
	{
		for (int i = 0; i < mLayers.size(); ++i)
		{
			GetLayer(i).AdjustWeights(delta);
		}
	}
	
	// add the output layer first, followed by hidden layers
	// and the output layer last
	public void AddLayer(NeuronLayer layer)
	{
		mLayers.add(layer);
	}
	
	public NeuronLayer GetLayer(int index)
	{
		return mLayers.get(index);
	}
	
	//
	// given inputs
	//
	public void Calculate(float[] inputs)
	{
		float[] layerInputs = inputs;
		for (int i = 0; i < mLayers.size(); ++i)
		{
			GetLayer(i).Calculate(layerInputs);
			layerInputs = GetLayer(i).GetOutputs();
		}
	}
/*	
	//
	// Returns the root mean square error for a complete training set.
	// 
	// @param len
	//            The length of a complete training set.
	// @return The current error for the neural network.
	//
	public double CalculateRMS()
	{
		final double err = Math.sqrt(this.globalError / (this.setSize));
		return err;

	}*/
	
	public float[] GetOutputs()
	{
		return mLayers.lastElement().GetOutputs();
	}

	public void Iterate(NeuronIterator it)
	{
		for (int i = 0; i < mLayers.size(); ++i)
		{
			NeuronLayer layer = GetLayer(i);
			layer.Iterate(it);
		}
	}
	
	public void Train(float[] inputs, float[] expectedOutputs, float learnRate)
	{
		Calculate(inputs);
		//float outputs[] = GetOutputs();
		/*
		float error = 0.f;
		for (int i = 0; i < outputs.length; ++i)
		{
			error += expectedOutputs[i] - outputs[i];
		}
		
		System.out.println("\toutput: " + outputs[0] + " expected: " + expectedOutputs[0] + " error: " + error);
		*/
		
		//
		// calculate the error from the output to the desired result
		// spread this change back, if the previous neuron had 75% effect on this neuron, change it by 75% back
		// such that neruons that have a greater effect are effected by the error more
		//
		NeuronLayer prevLayer = null;
		for (int i = mLayers.size() - 1; i >= 0; --i)
		{
			NeuronLayer layer = GetLayer(i);
			for (int n = 0; n < layer.GetSize(); ++n)
			{
				// delta error takes in to account number of input weights as well
				Neuron neuron = layer.GetNeuron(n);
				if (prevLayer == null)
				{
					//if ((neuron.GetOutput() > 0.f && expectedOutputs[n] <= 0.f) 
					//		|| (neuron.GetOutput() > 0.f && expectedOutputs[n] <= 0.f))
					float deltaError = expectedOutputs[n] - neuron.GetOutput(); //GetWeightedInput(); //GetOutput();
					deltaError *= learnRate;
					neuron.SetError(deltaError);
					
					//neuron.AdjustWeights(deltaError / neuron.GetInputSize());
				}
				else
				{
					float deltaError = 0.f;
					for (int pn = 0; pn < prevLayer.GetSize(); ++pn)
					{
						Neuron prevLayerNeuron = prevLayer.GetNeuron(pn);
						deltaError += /*prevLayerNeuron.GetWeight(n) * */prevLayerNeuron.GetError();
					}
					
					neuron.SetError(deltaError / prevLayer.GetSize());
				}
			}
			
			prevLayer = layer;
		}
		
		// adjust the weights based on the inputs from the previous layer (or the inputs if there is no previous layer)
		// apply error to adjust weights
		for (int i = 0; i < mLayers.size(); ++i)
		{
			NeuronLayer layer = GetLayer(i);
			for (int n = 0; n < layer.GetSize(); ++n)
			{
				Neuron neuron = layer.GetNeuron(n);
				
				for (int w = 0; w < neuron.GetInputSize(); ++w)
				{
					float weight = neuron.GetWeight(w);
					
					// work out the input for this neuron, input affects
					// the weight!
					float input = 0.f;
					if (i == 0)
					{
						input = inputs[w];
					}
					else
					{
						prevLayer = GetLayer(i - 1);
						Neuron prevNeuron = prevLayer.GetNeuron(w);
						input = prevNeuron.GetOutput();
					}
					/*
					// error determines which way we should learn (+ or -)
					// only adjust weights if the input sign matches the error sign (ie. make -ves more negative, make +vs more positive)
					if ((neuron.GetError() > 0.f && input > 0.f)
							|| (neuron.GetError() < 0.f && input < 0.f))
					{
						weight += (neuron.GetError() * Math.abs(input)) / neuron.GetInputSize();
					}*/
					
					//if (input == 0.f)
					//{
					//	input = -1.f;
					//}
					
					weight += (neuron.GetError() * input) / (neuron.GetInputSize() + 1);
					neuron.SetWeight(w, weight);
				}
				
				// adjust threshold
				float input = 1.f;
				float weight = neuron.GetThreshold();
				weight += (neuron.GetError() * input) / (neuron.GetInputSize() + 1);
				neuron.SetThreshold(weight);
				
				
				//neuron.AdjustWeights(neuron.GetError() / neuron.GetInputSize());
			}
		}
	}
	
	Vector<NeuronLayer>	mLayers = new Vector<NeuronLayer>();
}
