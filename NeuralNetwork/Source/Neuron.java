
public class Neuron 
{
	protected float[]	mWeights;
	protected float		mThreshold			= 0.f;	// this is best thought of as a weight, where the input is always 1.0f
	protected float		mWeightedInput;
	protected float		mOutput;
	protected float		mError;
	
	public Neuron()
	{
		
	}
	
	public int GetInputSize()
	{
		return mWeights.length;
	}
	
	public void SetInputSize(int size)
	{
		// degree refers to number of inputs
		mWeights = new float[size];
		for (int i = 0; i < mWeights.length; ++i)
			mWeights[i] = 0.f;
		
		mThreshold = 0.f;
		//InitRandom();
	}
	
	public void InitRandom()
	{
		// this creates somewhat normalised values
		float invLength = 1.f / mWeights.length;
		for (int i = 0; i < mWeights.length; ++i)
			mWeights[i] = ((float) Math.random() - 1.f);// * invLength;
		
		mThreshold = ((float) Math.random() - 1.f);
		//mThreshold = (float) Math.random();
	}
	
	//
	// takes inputs and calculates outputs
	//
	public void Calculate(float[] inputs)
	{
		mWeightedInput = 0;
		for (int i = 0; i < mWeights.length; ++i)
		{
			mWeightedInput += mWeights[i] * inputs[i];
		}
		
		mWeightedInput += mThreshold;
		
		if (mWeightedInput > 1000.f || mWeightedInput < -1000.f)
		{
			int nothing = 0;
		}
		
		//mWeightedInput -= mThreshold;
		
		// switch/functor based on threshold forumlae (ie. NOT, OR, AND)
		// for now we will just keep it simple and do STEP
		//mOutput = (mWeightedInput - mThreshold) > 0.f ? 1.f : 0.f;
		
		mOutput = mWeightedInput;

		// boolean step function
		if (mOutput >= 0.f)
			mOutput = 1.f;
		else
			mOutput = 0.f;
	}
	
	public float GetWeightedInput()
	{
		return mWeightedInput;
	}
	
	public float GetOutput()
	{
		return mOutput;
	}
	
	public float GetWeight(int index)
	{
		return mWeights[index];
	}
	
	public void SetWeight(int index, float weight)
	{
		mWeights[index] = weight;
	}
	
	public float GetThreshold()
	{
		return mThreshold;
	}
	
	public void SetThreshold(float threshold)
	{
		mThreshold = threshold;
	}
	
	void AdjustWeights(float delta)
	{
		for (int i = 0; i < mWeights.length; ++i)
		{
			mWeights[i] += delta;
		}
	}
	
	void SetError(float error)
	{
		mError = error;
	}
	
	float GetError()
	{
		return mError;
	}
	
	public void Iterate(NeuronIterator it)
	{
		it.Iterate(this);
	}
}
