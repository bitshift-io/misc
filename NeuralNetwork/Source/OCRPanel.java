import java.awt.Button;
import java.awt.Color;
import java.awt.Event;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;



// http://www.rgagnon.com/javadetails/java-0305.html
public class OCRPanel extends Panel implements MouseListener, MouseMotionListener
{
	
	Button mClassify = new Button("Classify");
	Button mClear = new Button("Clear");
	
	TextField mSaveName = new TextField("");
	Button mSave = new Button("Save");
	OCR.Image mImage = new OCR.Image();
	
	OCR mOCR = new OCR();
	
	Frame mFrame;
	
	float[] mClassification = null;
	
	int mPixelIndex = 1;
	
	// offset of the OCR panel you can draw in
	int mOffsetX = 50;
	int mOffsetY = 50;
	
	void Run()
	{	
		mOCR.Train();
		
		mFrame = new Frame();
		mFrame.addWindowListener(new java.awt.event.WindowAdapter() {
		       public void windowClosing(java.awt.event.WindowEvent e) {
		       System.exit(0);
		       };
		     });

		  addMouseListener( this );
	      addMouseMotionListener( this );
	      
		  //UnderlineText ut = new UnderlineText(); UnderlineText = this
		  this.setSize(300,350); // same size as defined in the HTML APPLET
		  mFrame.add(this);
		  mFrame.pack();
		  this.init();
		  mFrame.setSize(300,350 + 20); // add 20, seems enough for the Frame title,
		  mFrame.show();
	}
	
	public void init()
	{
		mImage.width = 5;
		mImage.height = 7;
		mImage.pixels = new float[mImage.width * mImage.height];
		Clear();
		
		add(mClassify);
		add(mClear);
		
		add(mSaveName);
		add(mSave);
	}
	
	void Clear()
	{
		mClassification = null;
		mPixelIndex = 1;
		for (int i = 0; i < (5 * 7); ++i)
		{
			mImage.pixels[i] = 0;
		}
		
		repaint();
	}
	
	void Save()
	{
		if (mSaveName.getText().length() != 1)
		{
			MsgBox message = new MsgBox(mFrame, "Enter the single character you have entered", true);
			message.dispose();
			return;
		}
		
		mImage.name = mSaveName.getText();
		mOCR.Normalize(mImage);
		mOCR.SaveImage(mImage);
	}
	
	public boolean action(Event e, Object args)
	  { 
		if (e.target == mClassify)
		{
			mOCR.Normalize(mImage);
			mClassification = mOCR.ClassifyImage(mImage);
			//Reset();
			repaint();
		}
		
	    if (e.target == mClear)
	    {
	    	Clear();
	    }
	    
	    if (e.target == mSave)
	    {
	    	Save();
	    }

	    return true;
	   }

	public void paint(Graphics g) 
	{
		// draw 5x7 grid
		int rectWidth = 20;
		int rectHeight = 20;
		
		for (int x = 0; x < mImage.width; ++x)
		{
			for (int y = 0; y < mImage.height; ++y)
			{
				if (mImage.pixels[(y * mImage.width) + x] > 0)
					g.fillRect(x * rectWidth + mOffsetX, y * rectHeight + mOffsetY, rectWidth, rectHeight);
				else
					g.drawRect(x * rectWidth + mOffsetX, y * rectHeight + mOffsetY, rectWidth, rectHeight);
			}
		}
		
		if (mClassification != null)
		{
			// highlight the maximum value as red
			float max = 0;
			int maxIndex = 0;
			for (int i = '0'; i <= '9'; ++i)
			{
				if (mClassification[i] > max)
				{
					max = mClassification[i];
					maxIndex = i;
				}
			}
			
			int index = 0;
			for (int i = '0'; i <= '9'; ++i)
			{
				if (i == maxIndex)
				{
					g.setColor(Color.red);
				}
				else
				{
					g.setColor(Color.black);
				}
					
				
				String line = (char)i + ": " + mClassification[i];
				g.drawString(line, mOffsetX, 8 * rectHeight + mOffsetY + (index * 12));
				++index;
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) 
	{
		PaintPixel(e);
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{

		
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		
	}

	@Override
	public void mousePressed(MouseEvent e) 
	{
		PaintPixel(e);
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
	
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
		PaintPixel(e);
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{

	}
	
	void PaintPixel(MouseEvent e)
	{
		int x = e.getX() - mOffsetX;
		int y = e.getY() - mOffsetY;
		
		if (x < 0 || y < 0)
		{
			return;
		}
		
		x /= 20;
		y /= 20;
		
		if (x >= mImage.width || y >= mImage.height)
		{
			return;
		}
		
		int pixelIndex = (y * mImage.width) + x;
		
		if (pixelIndex < 0 || pixelIndex >= mImage.pixels.length)
			return;
		
		mImage.pixels[pixelIndex] = mPixelIndex;
		++mPixelIndex;
		repaint();
	}
}
