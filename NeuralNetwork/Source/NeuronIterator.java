
public interface NeuronIterator 
{
	void Iterate(Neuron neuron);
}
