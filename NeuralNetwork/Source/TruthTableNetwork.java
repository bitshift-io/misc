
public class TruthTableNetwork
{
	TruthTableNetwork()
	{
		// created with 1 additional input for balancing
		mNOTNetwork = new NeuralNetwork();
		mNOTNetwork.Create(1, 1, 0);
		mNOTNetwork.RandomiseWeights();
		
		mANDNetwork = new NeuralNetwork();
		mANDNetwork.Create(2, 1, 0);
		mANDNetwork.RandomiseWeights();
		
		mORNetwork = new NeuralNetwork();
		mORNetwork.Create(2, 1, 0);
		mORNetwork.RandomiseWeights();
		
		mXORNetwork = new NeuralNetwork();
		mXORNetwork.Create(2, 1, 3);
		mXORNetwork.RandomiseWeights();
		
		mNOTANDNetwork = new NeuralNetwork();
		mNOTANDNetwork.Create(2, 1, 0);
		mNOTANDNetwork.RandomiseWeights();
	}
	
	void Run()
	{
		System.out.println("===============================================");

		System.out.println("\nTraining NOT Network\n");
		
		for (int t = 0; t < 100; t++)
		{
			System.out.println("Training Iteration: " + t);
			
			float error = 0.f;
			error += Train(mNOTNetwork, 0, 1);
			error += Train(mNOTNetwork, 1, 0);
			
			if (error <= 0.f)
				break;
		}
		
		System.out.println("\nTraining OR Network\n");
		
		for (int t = 0; t < 100; t++)
		{
			System.out.println("Training Iteration: " + t);
			
			float error = 0.f;
			error += Train(mORNetwork, 0, 0, 0);
			error += Train(mORNetwork, 1, 1, 1);
			error += Train(mORNetwork, 0, 1, 1);
			error += Train(mORNetwork, 1, 0, 1);
			
			if (error <= 0.f)
				break;
		}
		
		System.out.println("\nTraining AND Network\n");
		
		for (int t = 0; t < 100; t++)
		{
			System.out.println("Training Iteration: " + t);
			
			float error = 0.f;
			error += Train(mANDNetwork, 0, 0, 0);
			error += Train(mANDNetwork, 1, 1, 1);
			error += Train(mANDNetwork, 0, 1, 0);
			error += Train(mANDNetwork, 1, 0, 0);
			
			if (error <= 0.f)
				break;
		}
		
		System.out.println("\nTraining NOT AND Network\n");
		
		for (int t = 0; t < 100; t++)
		{
			System.out.println("Training Iteration: " + t);
			
			float error = 0.f;
			error += Train(mNOTANDNetwork, 1, 1, 0);
			error += Train(mNOTANDNetwork, 1, 0, 1);
			error += Train(mNOTANDNetwork, 0, 1, 0);
			error += Train(mNOTANDNetwork, 0, 0, 0);
			
			if (error <= 0.f)
				break;
		}
		
		// this needs another balancing node on the hidden layer
		System.out.println("\nTraining XOR Network\n");
		
		mLearnRate = 0.001f;
		for (int t = 0; t < 10000; t++)
		{
			System.out.println("Training Iteration: " + t);
			
			float error = 0.f;
			error += Train(mXORNetwork, 1, 0, 1);
			error += Train(mXORNetwork, 0, 0, 0);
			error += Train(mXORNetwork, 0, 1, 1);
			error += Train(mXORNetwork, 1, 1, 0);
			
			if (error <= 0.f)
				break;
		}
		
		System.out.println("Training Complete");
		System.out.println("===============================================");
	}
	
	float Train(NeuralNetwork network, float input0, float input1, float output)
	{
		// the problem with false == 0 is that it causes NO effect on the neuron,
		// we want it to have an effect, so it becomes -1 to cause a subtraction effect
		float[] inputs = new float[2];
		inputs[0] = input0;// == 0.f ? -1.f : 1.f;
		inputs[1] = input1;// == 0.f ? -1.f : 1.f;
		
		float[] expectedOutputs = new float[1];
		expectedOutputs[0] = output;// == 0.f ? -1.f : 1.f;
		
		network.Train(inputs, expectedOutputs, 0.5f);
		
		// display output information to see if we are improving
		float outputs[] = network.GetOutputs();
		float error = expectedOutputs[0] - outputs[0];
		System.out.println("\toutput: " + outputs[0] + " expected: " + expectedOutputs[0] + " error: " + error);
		return Math.abs(error);
	}
	
	float Train(NeuralNetwork network, float input0, float output)
	{
		// the problem with false == 0 is that it causes NO effect on the neuron,
		// we want it to have an effect, so it becomes -1 to cause a subtraction effect
		float[] inputs = new float[1];
		inputs[0] = input0;// == 0.f ? -1.f : 1.f;
		
		float[] expectedOutputs = new float[1];
		expectedOutputs[0] = output;// == 0.f ? -1.f : 1.f;
		
		network.Train(inputs, expectedOutputs, mLearnRate);
		
		// display output information to see if we are improving
		float outputs[] = network.GetOutputs();
		float error = expectedOutputs[0] - outputs[0];
		System.out.println("\toutput: " + outputs[0] + " expected: " + expectedOutputs[0] + " error: " + error);
		return Math.abs(error);
	}
	
	float			mLearnRate	= 0.5f;
	NeuralNetwork 	mNOTNetwork;
	NeuralNetwork 	mANDNetwork;
	NeuralNetwork 	mORNetwork;
	NeuralNetwork 	mXORNetwork;
	NeuralNetwork 	mNOTANDNetwork;
}
