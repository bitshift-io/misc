
public class SimpleNetworkTest
{
	SimpleNetworkTest()
	{
		mNeuralNetwork = new NeuralNetwork();
		mNeuralNetwork.Create(3, 1, 1);
		
		//mNeuralNetwork.AddLayer(new NeuronLayer(3));
		//mNeuralNetwork.AddLayer(new NeuronLayer(1));
		//mNeuralNetwork.CreateConnections();
	}

	void Run()
	{
		for (int t = 0; t < 100; t++)
		{
			System.out.println("Training Iteration: " + t);
			
			Train(0, 0, 1, 0);
			mDelta.presentPattern(0, 0, 1, 0);
			
			Train(0, 1, 1, 0);
			mDelta.presentPattern(0, 1, 1, 0);
			
			Train(1, 0, 1, 0);
			mDelta.presentPattern(1, 0, 1, 0);
			
			Train(1, 1, 1, 1);
			mDelta.presentPattern(1, 1, 1, 1);
		}
		
		System.out.println("Training Complete");
	}
	
	void Train(float i1, float i2,
			float i3, float expected) 
	{
		float[] inputs = new float[3];
		inputs[0] = i1;
		inputs[1] = i2;
		inputs[2] = i3;
		
		float[] expectedOutputs = new float[1];
		expectedOutputs[0] = expected;
		
		mNeuralNetwork.Train(inputs, expectedOutputs, 0.5f);
		
		// display output information to see if we are improving
		float outputs[] = mNeuralNetwork.GetOutputs();
		float error = expectedOutputs[0] - outputs[0];
		System.out.println("\toutput: " + outputs[0] + " expected: " + expectedOutputs[0] + " error: " + error);
	}
	
	NeuralNetwork 	mNeuralNetwork;
	Delta			mDelta				 = new Delta();
}
