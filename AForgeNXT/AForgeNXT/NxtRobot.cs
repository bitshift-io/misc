﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AForge.Robotics.Lego;
using System.Threading;
using System.Windows.Forms;
using ABT;

namespace AForgeNXT
{
    class NxtRobot : Robot
    {
        enum Task
        {
            Measure,
            Move,
            Last,
        }

        const int Steer = 0;
        const int Drive = 1;

        protected Thread m_thread;
        protected int m_threadSleepInterval;

        protected ArduinoBT m_abt = new ArduinoBT();


        public NxtRobot(Main form) : base(form)
        {
           
        }

        ~NxtRobot()
        {
            Disconnect();
        }

        public bool Connect(String comPort)
        {
            try
            {
                int port = Convert.ToInt32(comPort);
                m_abt.Connect(port);
            }
            catch (Exception ex)
            {
                return false;
            }

            /*
            if (!m_nxt.Connect(comPort)) // eg. "COM4"
            {
                Console.WriteLine("Failed to connect");
                return false;
            }

            m_nxt.SetSensorMode(NXTBrick.Sensor.Fourth, NXTBrick.SensorType.Lowspeed9V, NXTBrick.SensorMode.Raw);
            m_nxt.SetSensorMode(NXTBrick.Sensor.Third, NXTBrick.SensorType.Lowspeed9V, NXTBrick.SensorMode.Raw);

            m_nxt.ResetMotorPosition(NXTBrick.Motor.A);
            m_nxt.ResetMotorPosition(NXTBrick.Motor.B);
            m_nxt.ResetMotorPosition(NXTBrick.Motor.C);

            Thread.Sleep(1000); // let sensors calibrate*/
            return true;
        }

        public void Disconnect()
        {
            m_abt.Disconnect();
            /*
            SetMotor(NXTBrick.Motor.A, 0, 1);
            SetMotor(NXTBrick.Motor.B, 0, 1);
            SetMotor(NXTBrick.Motor.C, 0, 1);
            m_nxt.Disconnect();*/
        }

        public void StartThread()
        {
            m_thread = new Thread(new ThreadStart(RunThread));
            m_thread.Start();
        }

        public void StopThread()
        {
            m_thread.Interrupt();
        }

        void RunThread()
        {
            try
            {
                while (Thread.CurrentThread.IsAlive && m_network != null)
                {
                    MethodInvoker mi = new MethodInvoker(Step);
                    m_form.BeginInvoke(mi);
                    //Thread.Sleep(m_threadSleepInterval);
                }
            }
            catch
            {
            }
        }

        public void SetThreadSleepInterval(int interval)
        {
            m_threadSleepInterval = interval;
        }

        private bool Measure()
        {
            try
            {
                const double MAX_DISTANCE = 300.0;

                //Thread.Sleep(100);

                int leftValue = m_abt.GetUltrasoundRange(0);

                bool validLeftValue = leftValue < MAX_DISTANCE; //m_nxt.GetUltrasonicSensorsValue(NXTBrick.Sensor.Third, out leftValue);
                double leftNormalised = ((double)leftValue) / MAX_DISTANCE;

                //Thread.Sleep(100);

                int rightValue = m_abt.GetUltrasoundRange(1);
                bool validRightValue = rightValue < MAX_DISTANCE; // bool validRightValue = m_nxt.GetUltrasonicSensorsValue(NXTBrick.Sensor.Fourth, out rightValue);
                double rightNormalised = ((double)rightValue) / MAX_DISTANCE;

                double lerpValue = 0.6;

                // when values are 1, they seem to be annomalies, so apply a lerp to smooth the values
                // make them not so erratic
                if (validLeftValue)
                    m_measure[0] = (leftNormalised * lerpValue) + (m_measure[0] * (1.0 - lerpValue));

                if (validRightValue)
                    m_measure[1] = (rightNormalised * lerpValue) + (m_measure[1] * (1.0 - lerpValue));

                m_form.leftDistanceTxt.Text = m_measure[0].ToString();
                m_form.rightDistanceTxt.Text = m_measure[1].ToString();

                //Console.WriteLine("left: " + m_measure[0] + " right: " + m_measure[1]);
                return true;
            }
            catch
            {
            }

            return false;
        }
        /*
        public void SetMotor(NXTBrick.Motor motor, int power, int tacho)
        {
            m_abt.SetMotorSpeed();

            NXTBrick.MotorState motorState = new NXTBrick.MotorState();

            //int value = Convert.ToInt32(m_form.textBox2.Text);
            motorState.Power = power;
            motorState.TurnRatio = 100;
            motorState.Mode = NXTBrick.MotorMode.On;
            motorState.Regulation = NXTBrick.MotorRegulationMode.Idle;
            motorState.RunState = NXTBrick.MotorRunState.Running;
            motorState.TachoLimit = tacho; // keep going
            m_nxt.SetMotorState(motor, motorState);
        }*/

        public void SetMotorSpeed(int index, float speed)
        {
            m_abt.SetMotorSpeed(index, speed);
        }

        public void Move()
        {
            
            double[] output = m_move;



            //Console.WriteLine("steer: " + m_move[0] + " engine: " + m_move[1]);

            // Steer = A
            // Drive = B

            double powerLeft = m_move[Drive];
            double powerRight = m_move[Drive];

            if (m_move[Steer] < 0)
            {
                powerLeft = powerLeft * (1.0 - Math.Abs(m_move[Steer]));
            }
            else
            {
                powerRight = powerRight * (1.0 - Math.Abs(m_move[Steer]));
            }

            //powerLeft = powerRight = 1.0;

            int tacho = 100;

            /*

            int power = (int)(-100.0 * m_move[Drive]);
            int tacho = 500;

            int powerB = power;
            int powerC = power;

            if (m_move[Steer] < 0)
            {
                powerB 
            }
            else
            {
            }*/


            if (m_measure[0] <= 0.08 || m_measure[1] <= 0.08)
            {
                // STOP!!!!
                Console.WriteLine("STOP");
                powerLeft = 0;
                powerRight = 0;
                tacho = 0;
            }


            m_form.leftEngineTxt.Text = powerLeft.ToString();
            m_form.rightEngineTxt.Text = powerRight.ToString();
            m_form.fitnessTxt.Text = m_network.Fitness.ToString();

            //int powerLeftInt = (int)(powerLeft * -100.0);
            //int powerRightInt = (int)(powerRight * -100.0);

            SetMotorSpeed(0, (float)powerLeft);
            SetMotorSpeed(1, (float)powerRight);

            //SetMotor(NXTBrick.Motor.B, powerLeftInt, tacho);
            //SetMotor(NXTBrick.Motor.C, powerRightInt, tacho);

            //Thread.Sleep(1000);

            //SetMotorSpeed(0, (float)0);
            //SetMotorSpeed(1, (float)0);
            //RotateTo(m_move[Steer]);
            
        }
        /*
        public void RotateTo(double value)
        {
            value *= 800.0;

            NXTBrick.MotorState motorState;
            if (!m_nxt.GetMotorState(NXTBrick.Motor.A, out motorState))
            {
                Console.WriteLine("failed to get engine values!");
            }

            m_form.engine_value.Text = motorState.RotationCount.ToString();

            //while (Math.Abs(value - motorState.RotationCount) > 50)
            {
                double lerpValue = 0.2; // smooth steering values to stop over shoot

                if (value > motorState.RotationCount)
                    motorState.Power = 100;
                else
                    motorState.Power = -100;
                //motorState.Power = (int)(100.0 * Math.Sign((double)value - motorState.RotationCount));
                motorState.TurnRatio = 100;
                motorState.Mode = NXTBrick.MotorMode.On | NXTBrick.MotorMode.Brake;
                motorState.Regulation = NXTBrick.MotorRegulationMode.Idle;
                motorState.RunState = NXTBrick.MotorRunState.Running;
                motorState.TachoLimit = (int)(Math.Abs(value - motorState.RotationCount) * lerpValue);
                m_nxt.SetMotorState(NXTBrick.Motor.A, motorState);
                /*
                Thread.Sleep(100);
                /*
                // brake
                motorState.Mode = NXTBrick.MotorMode.On | NXTBrick.MotorMode.Brake;
                m_nxt.SetMotorState(NXTBrick.Motor.A, motorState);
                motorState.Power = 0;
                motorState.Mode = NXTBrick.MotorMode.On | NXTBrick.MotorMode.Brake;
                motorState.Regulation = NXTBrick.MotorRegulationMode.Idle;
                motorState.RunState = NXTBrick.MotorRunState.Running;
                motorState.TachoLimit = 0;
                m_nxt.SetMotorState(NXTBrick.Motor.A, motorState);
                * /
                m_nxt.GetMotorState(NXTBrick.Motor.A, out motorState);
            }
            /*
            // brake
            motorState.Mode = NXTBrick.MotorMode.On | NXTBrick.MotorMode.Brake;
            m_nxt.SetMotorState(NXTBrick.Motor.A, motorState);
            motorState.Power = 0;
            motorState.Mode = NXTBrick.MotorMode.On | NXTBrick.MotorMode.Brake;
            motorState.Regulation = NXTBrick.MotorRegulationMode.Idle;
            motorState.RunState = NXTBrick.MotorRunState.Running;
            motorState.TachoLimit = 0;
            m_nxt.SetMotorState(NXTBrick.Motor.A, motorState);* /
        }


        public void Test()
        {
            try
            {

                int value = Convert.ToInt32(m_form.textBox1.Text);
                RotateTo(value);
            }
            catch
            {
            }
            /*
            NXTBrick.MotorState motorState;

            // get motor's state
            if (!m_nxt.GetMotorState(NXTBrick.Motor.A, out motorState))
            {
                return;
            }

            // run motor A
            motorState = new NXTBrick.MotorState();

            motorState.Power = 70;
            motorState.TurnRatio = 10;
            motorState.Mode = NXTBrick.MotorMode.On;
            motorState.Regulation = NXTBrick.MotorRegulationMode.Idle;
            motorState.RunState = NXTBrick.MotorRunState.Running;
            motorState.TachoLimit = 20;

            m_nxt.SetMotorState(NXTBrick.Motor.A, motorState);

            /*
            NXTBrick.MotorState motorState = new NXTBrick.MotorState();
            motorState.Mode = NXTBrick.MotorMode.Brake | NXTBrick.MotorMode.On;// | NXTBrick.MotorMode.Regulated;
            motorState.Regulation = NXTBrick.MotorRegulationMode.Sync;
            motorState.RunState = NXTBrick.MotorRunState.Running;
            motorState.TurnRatio = 1;
            motorState.TachoLimit = 100;
            motorState.Power = 100;

            if (!m_nxt.SetMotorState(NXTBrick.Motor.A, motorState))
            {
                Console.WriteLine("Failed to write engine values?");
            }* /

        }

        public void Test2()
        {
            try
            {
                NXTBrick.MotorState motorState;

                // get motor's state
                if (!m_nxt.GetMotorState(NXTBrick.Motor.B, out motorState))
                {
                    return;
                }

                // run motor A
                motorState = new NXTBrick.MotorState();

                int value = Convert.ToInt32(m_form.textBox2.Text);
                motorState.Power = 100 * Math.Sign(value);
                motorState.TurnRatio = 100;
                motorState.Mode = NXTBrick.MotorMode.On | NXTBrick.MotorMode.Brake;
                motorState.Regulation = NXTBrick.MotorRegulationMode.Idle;
                motorState.RunState = NXTBrick.MotorRunState.Running;
                motorState.TachoLimit = Math.Abs(value);

                m_nxt.SetMotorState(NXTBrick.Motor.B, motorState);

            }
            catch
            {
            }
            /*
            NXTBrick.MotorState motorState = new NXTBrick.MotorState();
            motorState.Mode = NXTBrick.MotorMode.Brake | NXTBrick.MotorMode.On;// | NXTBrick.MotorMode.Regulated;
            motorState.Regulation = NXTBrick.MotorRegulationMode.Sync;
            motorState.RunState = NXTBrick.MotorRunState.Running;
            motorState.TurnRatio = 1;
            motorState.TachoLimit = 100;
            motorState.Power = -100;

            if (!m_nxt.SetMotorState(NXTBrick.Motor.A, motorState))
            {
                Console.WriteLine("Failed to write engine values?");
            }
            * /
        }*/

        public override void Step()
        {
            if (!m_abt.IsConnected() || m_network == null) // apply brakes?
                return;

            Measure();
            m_move = CalculateOutputs(m_measure);
            Move();
            //Thread.Sleep(100);
            /*

            switch (m_task)
            {
                case Task.Measure:
                    {
                        Measure();
                        break;
                    }

                case Task.Move:
                    {
                       
                        break;
                    }
            }*/
        }

        Task m_task;

        double[] m_measure  = new double[2];
        double[] m_move     = new double[2];

        NXTBrick m_nxt = new NXTBrick();
    }
}
