﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace AForgeNXT
{
    // 2 robot outputs, 1 for each engine
    class VRDualDrive : VirtualRobot
    {
        public VRDualDrive(VREnvironment environment, Main form)
            : base(environment, form)
        {

        }

        public override void Move(double[] outputs)
        {
            double engineDelta = outputs[0] - outputs[1];
            double engineNetMovement = outputs[0] + outputs[1];

            m_direction += engineDelta * 10.0;
            double distance = 4.0 * engineNetMovement;
            m_position = MoveTo(m_position, m_direction, distance);
        }

        public override void  NormalizeFitness(TimeSpan timeSpan)
        {
            m_network.Fitness /= timeSpan.TotalMilliseconds;
        }

        public override void IncrementFitness(VirtualRobot.StepData step)
        {
            double closest = Math.Min(step.fitnessInputs[0], step.fitnessInputs[1]);

            // bad is too close!
            if (closest <= 0.02)
            {
                m_network.Fitness -= 10.0;

                TimeSpan timeSpan = DateTime.Now - m_startTime;
                NormalizeFitness(timeSpan);

                m_network = null; // means we get a new brain next step
            }
            else
            {
                double fitnessIncrement = m_environment.GetDistance(m_initialPosition, m_stepList[0].position);
                m_network.Fitness += fitnessIncrement;
            }
        }
    }
}
