﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AForgeNXT
{
    //[Serializable]
    public interface ActivationFunction // wont serialize if its an interface
    {
        //public virtual 
            double Function(double input);// { return 0;  }
        //public virtual 
            double Derivative(double output);// { return 0; }
    }

    [Serializable]
    public class Sigmoid : ActivationFunction
    {
        Sigmoid(double alpha = 2)
        {
            m_alpha = alpha;
        }

        //public override double Function(double input)
        double ActivationFunction.Function(double input)
        {
            return (1.0 / (1.0 + Math.Exp(-m_alpha * input)));
        }

        //public override double Derivative(double output)
        double ActivationFunction.Derivative(double output)
        {
            return m_alpha * output * (1 - output);
        }

        double m_alpha = 2;
    }

    [Serializable]
    public class BipolarSigmoid : ActivationFunction
    {
        public BipolarSigmoid(double alpha = 6)
        {
            m_alpha = alpha;
        }

        //public override double Function(double input)
        double ActivationFunction.Function(double input)
        {
            return (2.0 / (1.0 + Math.Exp(-m_alpha * input))) - 1;
        }

        //public override double Derivative(double output)
        double ActivationFunction.Derivative(double output)
        {
            return m_alpha * (1 - Math.Pow(output, 2)) / 2;
        }

        double m_alpha = 6;
    }

    /* bad
    [Serializable]
    public class Power : ActivationFunction
    {
        double ActivationFunction.Function(double input)
        {
            return Math.Max(Math.Min(Math.Pow(input, 2), 1), 0);
        }

        double ActivationFunction.Derivative(double output)
        {
            return output * 2;
        }
    }*/

    [Serializable]
    public class NeuronLayer
    {
        public NeuronLayer()
        {
        }

        public NeuronLayer(int neuronCount)
        {
            neuron = new Neuron[neuronCount];
        }

        public Neuron[] neuron;
    }

    [Serializable]
    public class Neuron
    {
        public double[] weight;
        public double output;
        public double learnRate;
        public double error;
        public bool enabled = true;
        public bool feedThrough = false; // if false, will feed the output from the last neruon straight through as its output, as it it doesnt exist
    }

    [Serializable]
    public class NNet
    {
        public void create(int inputCount, NeuronLayer[] layer)
        {
            create(inputCount, layer, -1, 1, -1, 1);
        }

        public void create(int inputCount, NeuronLayer[] layer, double minWeight, double maxWeight)
        {
            create(inputCount, layer, minWeight, maxWeight, -1, 1);
        }


        public void create(int inputCount, NeuronLayer[] layer, double minWeight, double maxWeight, double minLearnRate, double maxLearnRate)
        {
            mLayer = layer;
            //new NeuronLayer[2];
            //mLayer[0] = new NeuronLayer(3);
            //mLayer[1] = new NeuronLayer(1);

            for (int l = 0; l < mLayer.Length; ++l)
            {
                for (int n = 0; n < mLayer[l].neuron.Length; ++n)
                {
                    int previousLayerCount = (l == 0) ? inputCount : mLayer[l - 1].neuron.Length;
                    Neuron neuron = new Neuron();
                    mLayer[l].neuron[n] = neuron;
                    neuron.weight = new double[previousLayerCount + 1]; // +1 for threshold weight
                    neuron.learnRate = mRandom.NextDouble() * (maxLearnRate - minLearnRate) + minLearnRate;

                    for (int w = 0; w < neuron.weight.Length; ++w)
                    {
                        neuron.weight[w] = mRandom.NextDouble() * (maxWeight - minWeight) + minWeight;
                        neuron.weight[w] = round(neuron.weight[w]);
                    }
                }
            }
        }

        public double round(double value)
        {
            if (mRound < 0)
                return value;

            return Math.Round(value, mRound);
        }

        public double[] train(double[] input, double[] expectedOutput)
        {
            double[] output = Compute(input);

            // go through and calculate errors
            for (int l = (mLayer.Length - 1); l >= 0; --l)
            {
                for (int n = 0; n < mLayer[l].neuron.Length; ++n)
                {
                    Neuron neuron = mLayer[l].neuron[n];

                    double error = mFunction.Derivative(neuron.output);
                    if ((l + 1) == mLayer.Length)
                    {
                        error += (expectedOutput[n] - neuron.output);
                    }
                    else
                    {
                        for (int on = 0; on < mLayer[l + 1].neuron.Length; ++on)
                        {
                            Neuron outputNeuron = mLayer[l + 1].neuron[on];
                            error += outputNeuron.weight[n] * outputNeuron.error;
                        }
                    }

                    neuron.error = error;
                }
            }

            // now alter the weights
            for (int l = 0; l < mLayer.Length; ++l)
            {
                for (int n = 0; n < mLayer[l].neuron.Length; ++n)
                {
                    Neuron neuron = mLayer[l].neuron[n];

                    for (int w = 0; w < neuron.weight.Length; ++w)
                    {
                        if ((w + 1) == neuron.weight.Length)
                        {
                            neuron.weight[w] += mLearnRate * neuron.error;
                        }
                        else if (l == 0)
                        {
                            neuron.weight[w] += mLearnRate * neuron.error * input[w];
                        }
                        else
                        {
                            Neuron previousNeuron = mLayer[l - 1].neuron[w];
                            neuron.weight[w] += mLearnRate * neuron.error * previousNeuron.output;
                        }

                        neuron.weight[w] = round(neuron.weight[w]);
                    }
                }
            }

            return output;
        }

        public double[] GetOutputs(int layer)
        {
            // gather outputs and return them
            NeuronLayer outputLayer = mLayer[layer];
            double[] outputs = new double[outputLayer.neuron.Length];

            for (int n = 0; n < outputLayer.neuron.Length; ++n)
            {
                outputs[n] = outputLayer.neuron[n].output;
            }

            return outputs;
        }

        public double[] Compute(double[] input)
        {
            for (int l = 0; l < mLayer.Length; ++l)
            {
                for (int n = 0; n < mLayer[l].neuron.Length; ++n)
                {
                    Neuron neuron = mLayer[l].neuron[n];
                    neuron.output = 0;

                    if (!neuron.enabled)
                        continue;

                    if (neuron.feedThrough)
                    {
                        if (l == 0)
                            neuron.output = input[n];
                        else
                            neuron.output = mLayer[l - 1].neuron[n].output;

                        continue;
                    }

                    for (int w = 0; w < neuron.weight.Length; ++w)
                    {
                        if ((w + 1) == neuron.weight.Length)
                        {
                            neuron.output += neuron.weight[w];
                        }
                        else if (l == 0)
                        {
                            neuron.output += neuron.weight[w] * input[w];
                        }
                        else
                        {
                            Neuron previousNeuron = mLayer[l - 1].neuron[w];
                            neuron.output += neuron.weight[w] * previousNeuron.output;
                        }
                    }

                    double output = mFunction.Function(neuron.output);
                    neuron.output = round(output);
                }
            }

            // gather outputs and return them
            return GetOutputs(mLayer.Length - 1);
        }

        public void test()
	    {
		    NeuronLayer[] layer = new NeuronLayer[2];
		    layer[0] = new NeuronLayer(3);
		    layer[1] = new NeuronLayer(1);
		    create(2, layer);
		
		
		    float trainingCount = 2000;
		
		    // XOR
		    for (int i=0;i<trainingCount;i++)
		    {
                train(new double[] { 0, 0 }, new double[] { 0 });
                train(new double[] { 0, 1 }, new double[] { 1 });
                train(new double[] { 1, 0 }, new double[] { 1 });
                train(new double[] { 1, 1 }, new double[] { 0 });
		    }

            Console.WriteLine("--- XOR ---");
            Console.WriteLine("0,0 = " + Compute(new double[] { 0, 0 })[0]);
            Console.WriteLine("0,1 = " + Compute(new double[] { 0, 1 })[0]);
            Console.WriteLine("1,0 = " + Compute(new double[] { 1, 0 })[0]);
            Console.WriteLine("1,1 = " + Compute(new double[] { 1, 1 })[0]);
    
		
		    // OR
		    for (int i=0;i<trainingCount;i++)
		    {
                train(new double[] { 0, 0 }, new double[] { 0 });
                train(new double[] { 0, 1 }, new double[] { 1 });
                train(new double[] { 1, 0 }, new double[] { 1 });
                train(new double[] { 1, 1 }, new double[] { 1 });
		    }

            Console.WriteLine("--- OR ---");
            Console.WriteLine("0,0 = " + Compute(new double[] { 0, 0 })[0]);
            Console.WriteLine("0,1 = " + Compute(new double[] { 0, 1 })[0]);
            Console.WriteLine("1,0 = " + Compute(new double[] { 1, 0 })[0]);
            Console.WriteLine("1,1 = " + Compute(new double[] { 1, 1 })[0]);
    
		    
		    // AND
		    for (int i=0;i<trainingCount;i++)
		    {
                train(new double[] { 0, 0 }, new double[] { 0 });
                train(new double[] { 0, 1 }, new double[] { 0 });
                train(new double[] { 1, 0 }, new double[] { 0 });
                train(new double[] { 1, 1 }, new double[] { 1 });
		    }

            Console.WriteLine("--- AND ---");
            Console.WriteLine("0,0 = " + Compute(new double[] { 0, 0 })[0]);
            Console.WriteLine("0,1 = " + Compute(new double[] { 0, 1 })[0]);
            Console.WriteLine("1,0 = " + Compute(new double[] { 1, 0 })[0]);
            Console.WriteLine("1,1 = " + Compute(new double[] { 1, 1 })[0]);
	    }

        public int mRound = -1; // round to n decimal points useful for user manual edit mode
        public Random mRandom = new Random();
        public NeuronLayer[] mLayer;
        public double mLearnRate = 0.5;
        public ActivationFunction mFunction = new BipolarSigmoid();
    }
}
