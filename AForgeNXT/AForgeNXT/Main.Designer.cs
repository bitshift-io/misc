﻿namespace AForgeNXT
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.pbTerrain = new System.Windows.Forms.PictureBox();
            this.pbRobot1 = new System.Windows.Forms.PictureBox();
            this.cbTrajeto = new System.Windows.Forms.CheckBox();
            this.cbLasers = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtInterval = new System.Windows.Forms.TextBox();
            this.weight_1_2_0 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.output_0_0 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.input_0 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.weight_0_0_0 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.weight_0_1_1 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.weight_1_1_1 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.weight_2_1_1 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.weight_2_0_1 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.weight_1_0_1 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.weight_0_0_1 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.weight_0_1_0 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.weight_0_2_0 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.weight_3_0_1 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.weight_3_1_1 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.weight_2_2_0 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.output_2_0 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.output_1_1 = new System.Windows.Forms.TextBox();
            this.output_0_1 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.useNetwork = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.weight_2_0_0 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.weight_2_1_0 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.weight_1_1_0 = new System.Windows.Forms.TextBox();
            this.weight_1_0_0 = new System.Windows.Forms.TextBox();
            this.output_1_0 = new System.Windows.Forms.TextBox();
            this.input_1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.shapeContainer2 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape12 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape8 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape11 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape10 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape9 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape7 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pause = new System.Windows.Forms.CheckBox();
            this.step = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.engine_value = new System.Windows.Forms.TextBox();
            this.leftDistanceTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rightDistanceTxt = new System.Windows.Forms.TextBox();
            this.fitnessTxt = new System.Windows.Forms.TextBox();
            this.leftEngineTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rightEngineTxt = new System.Windows.Forms.TextBox();
            this.nxt_connected = new System.Windows.Forms.CheckBox();
            this.nxt_disconnect = new System.Windows.Forms.Button();
            this.nxt_com_port = new System.Windows.Forms.TextBox();
            this.nxt_connect = new System.Windows.Forms.Button();
            this.runOnlyRobot = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbTerrain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRobot1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lineShape4
            // 
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 231;
            this.lineShape4.X2 = 146;
            this.lineShape4.Y1 = 19;
            this.lineShape4.Y2 = 127;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 537;
            this.lineShape3.X2 = 440;
            this.lineShape3.Y1 = 24;
            this.lineShape3.Y2 = 119;
            // 
            // pbTerrain
            // 
            this.pbTerrain.BackColor = System.Drawing.Color.White;
            this.pbTerrain.ErrorImage = null;
            this.pbTerrain.InitialImage = null;
            this.pbTerrain.Location = new System.Drawing.Point(3, 3);
            this.pbTerrain.Name = "pbTerrain";
            this.pbTerrain.Size = new System.Drawing.Size(500, 500);
            this.pbTerrain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbTerrain.TabIndex = 14;
            this.pbTerrain.TabStop = false;
            this.pbTerrain.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbTerrain_MouseDown);
            this.pbTerrain.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbTerrain_MouseMove);
            this.pbTerrain.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbTerrain_MouseUp);
            // 
            // pbRobot1
            // 
            this.pbRobot1.BackColor = System.Drawing.Color.Transparent;
            this.pbRobot1.Image = ((System.Drawing.Image)(resources.GetObject("pbRobot1.Image")));
            this.pbRobot1.Location = new System.Drawing.Point(212, 236);
            this.pbRobot1.Name = "pbRobot1";
            this.pbRobot1.Size = new System.Drawing.Size(10, 10);
            this.pbRobot1.TabIndex = 15;
            this.pbRobot1.TabStop = false;
            // 
            // cbTrajeto
            // 
            this.cbTrajeto.Location = new System.Drawing.Point(26, 27);
            this.cbTrajeto.Name = "cbTrajeto";
            this.cbTrajeto.Size = new System.Drawing.Size(120, 24);
            this.cbTrajeto.TabIndex = 21;
            this.cbTrajeto.Text = "&Track Path";
            // 
            // cbLasers
            // 
            this.cbLasers.Location = new System.Drawing.Point(26, 51);
            this.cbLasers.Name = "cbLasers";
            this.cbLasers.Size = new System.Drawing.Size(120, 24);
            this.cbLasers.TabIndex = 20;
            this.cbLasers.Text = "&Debug";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(23, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Move Interval (ms):";
            // 
            // txtInterval
            // 
            this.txtInterval.Location = new System.Drawing.Point(23, 94);
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.Size = new System.Drawing.Size(72, 20);
            this.txtInterval.TabIndex = 22;
            this.txtInterval.Text = "100";
            this.txtInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInterval.TextChanged += new System.EventHandler(this.txtInterval_TextChanged);
            // 
            // weight_1_2_0
            // 
            this.weight_1_2_0.Location = new System.Drawing.Point(569, 45);
            this.weight_1_2_0.Name = "weight_1_2_0";
            this.weight_1_2_0.Size = new System.Drawing.Size(46, 20);
            this.weight_1_2_0.TabIndex = 29;
            this.weight_1_2_0.Text = "0.5";
            this.weight_1_2_0.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(68, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "O";
            // 
            // output_0_0
            // 
            this.output_0_0.Enabled = false;
            this.output_0_0.Location = new System.Drawing.Point(89, 134);
            this.output_0_0.Name = "output_0_0";
            this.output_0_0.ReadOnly = true;
            this.output_0_0.Size = new System.Drawing.Size(100, 20);
            this.output_0_0.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(158, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Input";
            // 
            // input_0
            // 
            this.input_0.Location = new System.Drawing.Point(196, 26);
            this.input_0.Name = "input_0";
            this.input_0.Size = new System.Drawing.Size(110, 20);
            this.input_0.TabIndex = 25;
            this.input_0.Text = "1.0";
            this.input_0.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(113, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(18, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "W";
            // 
            // weight_0_0_0
            // 
            this.weight_0_0_0.Location = new System.Drawing.Point(134, 65);
            this.weight_0_0_0.Name = "weight_0_0_0";
            this.weight_0_0_0.Size = new System.Drawing.Size(55, 20);
            this.weight_0_0_0.TabIndex = 32;
            this.weight_0_0_0.Text = "1.0";
            this.weight_0_0_0.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.weight_0_1_1);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.weight_1_1_1);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.weight_2_1_1);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.weight_2_0_1);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.weight_1_0_1);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.weight_0_0_1);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.weight_0_1_0);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.weight_0_2_0);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.weight_3_0_1);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.weight_3_1_1);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.weight_2_2_0);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.output_2_0);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.output_1_1);
            this.groupBox1.Controls.Add(this.output_0_1);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.useNetwork);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.weight_2_0_0);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.weight_2_1_0);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.weight_1_1_0);
            this.groupBox1.Controls.Add(this.weight_1_0_0);
            this.groupBox1.Controls.Add(this.output_1_0);
            this.groupBox1.Controls.Add(this.input_1);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.input_0);
            this.groupBox1.Controls.Add(this.weight_0_0_0);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.output_0_0);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.weight_1_2_0);
            this.groupBox1.Controls.Add(this.shapeContainer2);
            this.groupBox1.Location = new System.Drawing.Point(509, 174);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(728, 329);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Network";
            // 
            // weight_0_1_1
            // 
            this.weight_0_1_1.Location = new System.Drawing.Point(429, 228);
            this.weight_0_1_1.Name = "weight_0_1_1";
            this.weight_0_1_1.Size = new System.Drawing.Size(55, 20);
            this.weight_0_1_1.TabIndex = 74;
            this.weight_0_1_1.Text = "1.0";
            this.weight_0_1_1.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(408, 231);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(18, 13);
            this.label30.TabIndex = 73;
            this.label30.Text = "W";
            // 
            // weight_1_1_1
            // 
            this.weight_1_1_1.Location = new System.Drawing.Point(486, 208);
            this.weight_1_1_1.Name = "weight_1_1_1";
            this.weight_1_1_1.Size = new System.Drawing.Size(55, 20);
            this.weight_1_1_1.TabIndex = 72;
            this.weight_1_1_1.Text = "1.0";
            this.weight_1_1_1.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(465, 211);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(18, 13);
            this.label29.TabIndex = 71;
            this.label29.Text = "W";
            // 
            // weight_2_1_1
            // 
            this.weight_2_1_1.Location = new System.Drawing.Point(580, 231);
            this.weight_2_1_1.Name = "weight_2_1_1";
            this.weight_2_1_1.Size = new System.Drawing.Size(55, 20);
            this.weight_2_1_1.TabIndex = 70;
            this.weight_2_1_1.Text = "1.0";
            this.weight_2_1_1.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(559, 234);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(18, 13);
            this.label28.TabIndex = 69;
            this.label28.Text = "W";
            // 
            // weight_2_0_1
            // 
            this.weight_2_0_1.Location = new System.Drawing.Point(345, 239);
            this.weight_2_0_1.Name = "weight_2_0_1";
            this.weight_2_0_1.Size = new System.Drawing.Size(55, 20);
            this.weight_2_0_1.TabIndex = 68;
            this.weight_2_0_1.Text = "1.0";
            this.weight_2_0_1.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(324, 242);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(18, 13);
            this.label27.TabIndex = 67;
            this.label27.Text = "W";
            // 
            // weight_1_0_1
            // 
            this.weight_1_0_1.Location = new System.Drawing.Point(260, 224);
            this.weight_1_0_1.Name = "weight_1_0_1";
            this.weight_1_0_1.Size = new System.Drawing.Size(55, 20);
            this.weight_1_0_1.TabIndex = 66;
            this.weight_1_0_1.Text = "1.0";
            this.weight_1_0_1.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(239, 227);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(18, 13);
            this.label26.TabIndex = 65;
            this.label26.Text = "W";
            // 
            // weight_0_0_1
            // 
            this.weight_0_0_1.Location = new System.Drawing.Point(149, 228);
            this.weight_0_0_1.Name = "weight_0_0_1";
            this.weight_0_0_1.Size = new System.Drawing.Size(55, 20);
            this.weight_0_0_1.TabIndex = 64;
            this.weight_0_0_1.Text = "1.0";
            this.weight_0_0_1.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(128, 231);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(18, 13);
            this.label25.TabIndex = 63;
            this.label25.Text = "W";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(223, 54);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(18, 13);
            this.label24.TabIndex = 62;
            this.label24.Text = "W";
            // 
            // weight_0_1_0
            // 
            this.weight_0_1_0.Location = new System.Drawing.Point(242, 52);
            this.weight_0_1_0.Name = "weight_0_1_0";
            this.weight_0_1_0.Size = new System.Drawing.Size(46, 20);
            this.weight_0_1_0.TabIndex = 61;
            this.weight_0_1_0.Text = "0.5";
            this.weight_0_1_0.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(308, 45);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(18, 13);
            this.label23.TabIndex = 60;
            this.label23.Text = "W";
            // 
            // weight_0_2_0
            // 
            this.weight_0_2_0.Location = new System.Drawing.Point(327, 43);
            this.weight_0_2_0.Name = "weight_0_2_0";
            this.weight_0_2_0.Size = new System.Drawing.Size(46, 20);
            this.weight_0_2_0.TabIndex = 59;
            this.weight_0_2_0.Text = "0.5";
            this.weight_0_2_0.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(86, 268);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(18, 13);
            this.label22.TabIndex = 58;
            this.label22.Text = "W";
            // 
            // weight_3_0_1
            // 
            this.weight_3_0_1.Location = new System.Drawing.Point(110, 265);
            this.weight_3_0_1.Name = "weight_3_0_1";
            this.weight_3_0_1.Size = new System.Drawing.Size(46, 20);
            this.weight_3_0_1.TabIndex = 57;
            this.weight_3_0_1.Text = "0.5";
            this.weight_3_0_1.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(365, 265);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(18, 13);
            this.label21.TabIndex = 56;
            this.label21.Text = "W";
            // 
            // weight_3_1_1
            // 
            this.weight_3_1_1.Location = new System.Drawing.Point(389, 262);
            this.weight_3_1_1.Name = "weight_3_1_1";
            this.weight_3_1_1.Size = new System.Drawing.Size(46, 20);
            this.weight_3_1_1.TabIndex = 55;
            this.weight_3_1_1.Text = "0.5";
            this.weight_3_1_1.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(483, 137);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(18, 13);
            this.label20.TabIndex = 54;
            this.label20.Text = "W";
            // 
            // weight_2_2_0
            // 
            this.weight_2_2_0.Location = new System.Drawing.Point(507, 134);
            this.weight_2_2_0.Name = "weight_2_2_0";
            this.weight_2_2_0.Size = new System.Drawing.Size(46, 20);
            this.weight_2_2_0.TabIndex = 53;
            this.weight_2_2_0.Text = "0.5";
            this.weight_2_2_0.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(559, 141);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(15, 13);
            this.label19.TabIndex = 52;
            this.label19.Text = "O";
            // 
            // output_2_0
            // 
            this.output_2_0.Enabled = false;
            this.output_2_0.Location = new System.Drawing.Point(580, 134);
            this.output_2_0.Name = "output_2_0";
            this.output_2_0.ReadOnly = true;
            this.output_2_0.Size = new System.Drawing.Size(100, 20);
            this.output_2_0.TabIndex = 51;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(441, 265);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 13);
            this.label17.TabIndex = 50;
            this.label17.Text = "Output";
            // 
            // output_1_1
            // 
            this.output_1_1.Enabled = false;
            this.output_1_1.Location = new System.Drawing.Point(486, 262);
            this.output_1_1.Name = "output_1_1";
            this.output_1_1.ReadOnly = true;
            this.output_1_1.Size = new System.Drawing.Size(100, 20);
            this.output_1_1.TabIndex = 49;
            // 
            // output_0_1
            // 
            this.output_0_1.Enabled = false;
            this.output_0_1.Location = new System.Drawing.Point(208, 262);
            this.output_0_1.Name = "output_0_1";
            this.output_0_1.ReadOnly = true;
            this.output_0_1.Size = new System.Drawing.Size(100, 20);
            this.output_0_1.TabIndex = 47;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(163, 265);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 13);
            this.label18.TabIndex = 48;
            this.label18.Text = "Output";
            // 
            // useNetwork
            // 
            this.useNetwork.AutoSize = true;
            this.useNetwork.Location = new System.Drawing.Point(9, 19);
            this.useNetwork.Name = "useNetwork";
            this.useNetwork.Size = new System.Drawing.Size(86, 17);
            this.useNetwork.TabIndex = 46;
            this.useNetwork.Text = "Use network";
            this.useNetwork.UseVisualStyleBackColor = true;
            this.useNetwork.CheckedChanged += new System.EventHandler(this.useNetwork_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(0, 137);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(18, 13);
            this.label16.TabIndex = 45;
            this.label16.Text = "W";
            // 
            // weight_2_0_0
            // 
            this.weight_2_0_0.Location = new System.Drawing.Point(23, 134);
            this.weight_2_0_0.Name = "weight_2_0_0";
            this.weight_2_0_0.Size = new System.Drawing.Size(46, 20);
            this.weight_2_0_0.TabIndex = 44;
            this.weight_2_0_0.Text = "0.5";
            this.weight_2_0_0.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(253, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(18, 13);
            this.label9.TabIndex = 43;
            this.label9.Text = "W";
            // 
            // weight_2_1_0
            // 
            this.weight_2_1_0.Location = new System.Drawing.Point(277, 140);
            this.weight_2_1_0.Name = "weight_2_1_0";
            this.weight_2_1_0.Size = new System.Drawing.Size(46, 20);
            this.weight_2_1_0.TabIndex = 42;
            this.weight_2_1_0.Text = "0.5";
            this.weight_2_1_0.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(329, 140);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(15, 13);
            this.label15.TabIndex = 41;
            this.label15.Text = "O";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(382, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(18, 13);
            this.label14.TabIndex = 40;
            this.label14.Text = "W";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(466, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 13);
            this.label13.TabIndex = 39;
            this.label13.Text = "W";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(550, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(18, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "W";
            // 
            // weight_1_1_0
            // 
            this.weight_1_1_0.Location = new System.Drawing.Point(486, 49);
            this.weight_1_1_0.Name = "weight_1_1_0";
            this.weight_1_1_0.Size = new System.Drawing.Size(46, 20);
            this.weight_1_1_0.TabIndex = 37;
            this.weight_1_1_0.Text = "0.5";
            this.weight_1_1_0.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // weight_1_0_0
            // 
            this.weight_1_0_0.Location = new System.Drawing.Point(401, 49);
            this.weight_1_0_0.Name = "weight_1_0_0";
            this.weight_1_0_0.Size = new System.Drawing.Size(46, 20);
            this.weight_1_0_0.TabIndex = 36;
            this.weight_1_0_0.Text = "0.5";
            this.weight_1_0_0.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // output_1_0
            // 
            this.output_1_0.Enabled = false;
            this.output_1_0.Location = new System.Drawing.Point(350, 137);
            this.output_1_0.Name = "output_1_0";
            this.output_1_0.ReadOnly = true;
            this.output_1_0.Size = new System.Drawing.Size(100, 20);
            this.output_1_0.TabIndex = 35;
            // 
            // input_1
            // 
            this.input_1.Location = new System.Drawing.Point(476, 19);
            this.input_1.Name = "input_1";
            this.input_1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.input_1.Size = new System.Drawing.Size(110, 20);
            this.input_1.TabIndex = 33;
            this.input_1.Text = "1.0";
            this.input_1.TextChanged += new System.EventHandler(this.NetworkTextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(439, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 34;
            this.label11.Text = "Input";
            // 
            // shapeContainer2
            // 
            this.shapeContainer2.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer2.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer2.Name = "shapeContainer2";
            this.shapeContainer2.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape12,
            this.lineShape8,
            this.lineShape6,
            this.lineShape5,
            this.lineShape2,
            this.lineShape1,
            this.lineShape11,
            this.lineShape10,
            this.lineShape9,
            this.lineShape7,
            this.lineShape4,
            this.lineShape3});
            this.shapeContainer2.Size = new System.Drawing.Size(722, 310);
            this.shapeContainer2.TabIndex = 0;
            this.shapeContainer2.TabStop = false;
            // 
            // lineShape12
            // 
            this.lineShape12.Name = "lineShape12";
            this.lineShape12.X1 = 581;
            this.lineShape12.X2 = 304;
            this.lineShape12.Y1 = 138;
            this.lineShape12.Y2 = 258;
            // 
            // lineShape8
            // 
            this.lineShape8.Name = "lineShape8";
            this.lineShape8.X1 = 595;
            this.lineShape8.X2 = 542;
            this.lineShape8.Y1 = 129;
            this.lineShape8.Y2 = 257;
            // 
            // lineShape6
            // 
            this.lineShape6.Name = "lineShape6";
            this.lineShape6.X1 = 398;
            this.lineShape6.X2 = 517;
            this.lineShape6.Y1 = 130;
            this.lineShape6.Y2 = 242;
            // 
            // lineShape5
            // 
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.X1 = 372;
            this.lineShape5.X2 = 268;
            this.lineShape5.Y1 = 130;
            this.lineShape5.Y2 = 249;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 181;
            this.lineShape2.X2 = 526;
            this.lineShape2.Y1 = 131;
            this.lineShape2.Y2 = 249;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 296;
            this.lineShape1.X2 = 578;
            this.lineShape1.Y1 = 25;
            this.lineShape1.Y2 = 122;
            // 
            // lineShape11
            // 
            this.lineShape11.Name = "lineShape11";
            this.lineShape11.X1 = 573;
            this.lineShape11.X2 = 611;
            this.lineShape11.Y1 = 19;
            this.lineShape11.Y2 = 118;
            // 
            // lineShape10
            // 
            this.lineShape10.Name = "lineShape10";
            this.lineShape10.X1 = 132;
            this.lineShape10.X2 = 236;
            this.lineShape10.Y1 = 133;
            this.lineShape10.Y2 = 252;
            // 
            // lineShape9
            // 
            this.lineShape9.Name = "lineShape9";
            this.lineShape9.X1 = 183;
            this.lineShape9.X2 = 480;
            this.lineShape9.Y1 = 125;
            this.lineShape9.Y2 = 18;
            // 
            // lineShape7
            // 
            this.lineShape7.Name = "lineShape7";
            this.lineShape7.X1 = 241;
            this.lineShape7.X2 = 363;
            this.lineShape7.Y1 = 28;
            this.lineShape7.Y2 = 123;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pause);
            this.groupBox2.Controls.Add(this.step);
            this.groupBox2.Controls.Add(this.cbTrajeto);
            this.groupBox2.Controls.Add(this.cbLasers);
            this.groupBox2.Controls.Add(this.txtInterval);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(509, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(131, 170);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Virtual Robot Values";
            // 
            // pause
            // 
            this.pause.AutoSize = true;
            this.pause.Location = new System.Drawing.Point(23, 119);
            this.pause.Name = "pause";
            this.pause.Size = new System.Drawing.Size(55, 17);
            this.pause.TabIndex = 25;
            this.pause.Text = "pause";
            this.pause.UseVisualStyleBackColor = true;
            this.pause.CheckedChanged += new System.EventHandler(this.pause_CheckedChanged);
            // 
            // step
            // 
            this.step.Location = new System.Drawing.Point(23, 142);
            this.step.Name = "step";
            this.step.Size = new System.Drawing.Size(75, 23);
            this.step.TabIndex = 24;
            this.step.Text = "Step";
            this.step.UseVisualStyleBackColor = true;
            this.step.Click += new System.EventHandler(this.step_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.runOnlyRobot);
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.engine_value);
            this.groupBox3.Controls.Add(this.leftDistanceTxt);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.rightDistanceTxt);
            this.groupBox3.Controls.Add(this.fitnessTxt);
            this.groupBox3.Controls.Add(this.leftEngineTxt);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.rightEngineTxt);
            this.groupBox3.Controls.Add(this.nxt_connected);
            this.groupBox3.Controls.Add(this.nxt_disconnect);
            this.groupBox3.Controls.Add(this.nxt_com_port);
            this.groupBox3.Controls.Add(this.nxt_connect);
            this.groupBox3.Location = new System.Drawing.Point(646, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(674, 170);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "NXT";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(568, 125);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 49;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // engine_value
            // 
            this.engine_value.Enabled = false;
            this.engine_value.Location = new System.Drawing.Point(316, 127);
            this.engine_value.Name = "engine_value";
            this.engine_value.Size = new System.Drawing.Size(100, 20);
            this.engine_value.TabIndex = 48;
            // 
            // leftDistanceTxt
            // 
            this.leftDistanceTxt.Enabled = false;
            this.leftDistanceTxt.Location = new System.Drawing.Point(316, 31);
            this.leftDistanceTxt.Name = "leftDistanceTxt";
            this.leftDistanceTxt.Size = new System.Drawing.Size(100, 20);
            this.leftDistanceTxt.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(313, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 39;
            this.label1.Text = "Left Distance";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(441, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 40;
            this.label2.Text = "Right Distance";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(441, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 47;
            this.label5.Text = "Fitness";
            // 
            // rightDistanceTxt
            // 
            this.rightDistanceTxt.Enabled = false;
            this.rightDistanceTxt.Location = new System.Drawing.Point(444, 29);
            this.rightDistanceTxt.Name = "rightDistanceTxt";
            this.rightDistanceTxt.Size = new System.Drawing.Size(100, 20);
            this.rightDistanceTxt.TabIndex = 41;
            // 
            // fitnessTxt
            // 
            this.fitnessTxt.Enabled = false;
            this.fitnessTxt.Location = new System.Drawing.Point(444, 128);
            this.fitnessTxt.Name = "fitnessTxt";
            this.fitnessTxt.Size = new System.Drawing.Size(100, 20);
            this.fitnessTxt.TabIndex = 46;
            // 
            // leftEngineTxt
            // 
            this.leftEngineTxt.Enabled = false;
            this.leftEngineTxt.Location = new System.Drawing.Point(316, 87);
            this.leftEngineTxt.Name = "leftEngineTxt";
            this.leftEngineTxt.Size = new System.Drawing.Size(100, 20);
            this.leftEngineTxt.TabIndex = 42;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(441, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 45;
            this.label4.Text = "Right Engine";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(313, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 43;
            this.label3.Text = "Left Engine";
            // 
            // rightEngineTxt
            // 
            this.rightEngineTxt.Enabled = false;
            this.rightEngineTxt.Location = new System.Drawing.Point(444, 86);
            this.rightEngineTxt.Name = "rightEngineTxt";
            this.rightEngineTxt.Size = new System.Drawing.Size(100, 20);
            this.rightEngineTxt.TabIndex = 44;
            // 
            // nxt_connected
            // 
            this.nxt_connected.AutoSize = true;
            this.nxt_connected.Enabled = false;
            this.nxt_connected.Location = new System.Drawing.Point(159, 125);
            this.nxt_connected.Name = "nxt_connected";
            this.nxt_connected.Size = new System.Drawing.Size(78, 17);
            this.nxt_connected.TabIndex = 37;
            this.nxt_connected.Text = "Connected";
            this.nxt_connected.UseVisualStyleBackColor = true;
            // 
            // nxt_disconnect
            // 
            this.nxt_disconnect.Location = new System.Drawing.Point(10, 125);
            this.nxt_disconnect.Name = "nxt_disconnect";
            this.nxt_disconnect.Size = new System.Drawing.Size(142, 23);
            this.nxt_disconnect.TabIndex = 36;
            this.nxt_disconnect.Text = "Disconnect";
            this.nxt_disconnect.UseVisualStyleBackColor = true;
            this.nxt_disconnect.Click += new System.EventHandler(this.nxt_disconnect_Click);
            // 
            // nxt_com_port
            // 
            this.nxt_com_port.Location = new System.Drawing.Point(159, 98);
            this.nxt_com_port.Name = "nxt_com_port";
            this.nxt_com_port.Size = new System.Drawing.Size(71, 20);
            this.nxt_com_port.TabIndex = 35;
            this.nxt_com_port.Text = "13";
            // 
            // nxt_connect
            // 
            this.nxt_connect.Location = new System.Drawing.Point(11, 96);
            this.nxt_connect.Name = "nxt_connect";
            this.nxt_connect.Size = new System.Drawing.Size(142, 23);
            this.nxt_connect.TabIndex = 34;
            this.nxt_connect.Text = "Connect\'n\'Run";
            this.nxt_connect.UseVisualStyleBackColor = true;
            this.nxt_connect.Click += new System.EventHandler(this.nxt_connect_Click);
            // 
            // runOnlyRobot
            // 
            this.runOnlyRobot.AutoSize = true;
            this.runOnlyRobot.Location = new System.Drawing.Point(16, 15);
            this.runOnlyRobot.Name = "runOnlyRobot";
            this.runOnlyRobot.Size = new System.Drawing.Size(95, 17);
            this.runOnlyRobot.TabIndex = 50;
            this.runOnlyRobot.Text = "Run only robot";
            this.runOnlyRobot.UseVisualStyleBackColor = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1332, 512);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pbRobot1);
            this.Controls.Add(this.pbTerrain);
            this.Name = "Main";
            this.Text = "Genetic Network";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Main_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pbTerrain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRobot1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        public System.Windows.Forms.PictureBox pbTerrain;
        public System.Windows.Forms.PictureBox pbRobot1;
        public System.Windows.Forms.CheckBox cbTrajeto;
        public System.Windows.Forms.CheckBox cbLasers;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtInterval;
        private System.Windows.Forms.TextBox weight_1_2_0;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox output_0_0;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox input_0;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox weight_0_0_0;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox weight_2_0_0;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox weight_2_1_0;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox weight_1_1_0;
        private System.Windows.Forms.TextBox weight_1_0_0;
        public System.Windows.Forms.TextBox output_1_0;
        public System.Windows.Forms.TextBox input_1;
        private System.Windows.Forms.Label label11;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape11;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape10;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape9;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape7;
        private System.Windows.Forms.CheckBox useNetwork;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox pause;
        private System.Windows.Forms.Button step;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox output_1_1;
        public System.Windows.Forms.TextBox output_0_1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox weight_3_0_1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox weight_3_1_1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox weight_2_2_0;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox output_2_0;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape8;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.TextBox weight_0_1_1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox weight_1_1_1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox weight_2_1_1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox weight_2_0_1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox weight_1_0_1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox weight_0_0_1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox weight_0_1_0;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox weight_0_2_0;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape12;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button nxt_disconnect;
        public System.Windows.Forms.TextBox nxt_com_port;
        private System.Windows.Forms.Button nxt_connect;
        private System.Windows.Forms.CheckBox nxt_connected;
        public System.Windows.Forms.TextBox leftDistanceTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox rightDistanceTxt;
        public System.Windows.Forms.TextBox fitnessTxt;
        public System.Windows.Forms.TextBox leftEngineTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox rightEngineTxt;
        public System.Windows.Forms.TextBox engine_value;
        public System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.CheckBox runOnlyRobot;
    }
}

