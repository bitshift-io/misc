﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace AForgeNXT
{
    // virtual robot environment
    class VREnvironment
    {
        Main m_form;
        Bitmap m_map;
        public Random m_random = new Random();

        public VREnvironment(Main form)
        {
            m_form = form;
            m_map = m_form.pbTerrain.Image as Bitmap;
        }

        public int GetDistance(Point p1, Point p2)
        {
            return (Convert.ToInt32(Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2))));
        }

        public int GetMeasure(Point position, double direction)
        {
            float increment = 5;
            double rad = (direction * Math.PI) / 180;
            float xIncrement = (float)(increment * Math.Cos(rad));
            float yIncrement = (float)(increment * Math.Sin(rad));

            PointF p = new PointF(position.X, position.Y);
            Point pi = new Point();
            
            // to a course forward search
            while (true)
            {
                p.X = p.X + xIncrement;
                p.Y = p.Y + yIncrement;

                pi.X = Convert.ToInt32(p.X);
                pi.Y = Convert.ToInt32(p.Y);

                if (pi.X < 0 || pi.Y < 0 || pi.X >= m_map.Width || pi.Y >= m_map.Height)
                    break;

                Color pixel = m_map.GetPixel(pi.X, pi.Y);
                if (pixel.R == 0 && pixel.G == 0 && pixel.B == 0)
                    break;
            }

            pi.X = Convert.ToInt32(p.X);
            pi.Y = Convert.ToInt32(p.Y);
            
            // now search backwards with a fine search
            increment = -1;
            xIncrement = (float)(increment * Math.Cos(rad));
            yIncrement = (float)(increment * Math.Sin(rad));

            while (true)
            {
                p.X = p.X + xIncrement;
                p.Y = p.Y + yIncrement;

                pi.X = Convert.ToInt32(p.X);
                pi.Y = Convert.ToInt32(p.Y);

                if (pi.X < 0 || pi.Y < 0 || pi.X >= m_map.Width || pi.Y >= m_map.Height)
                    break;

                Color pixel = m_map.GetPixel(pi.X, pi.Y);
                if (pixel.R == 255 && pixel.G == 255 && pixel.B == 255)
                    break;
            }

            int distance = GetDistance(position, pi);
            return distance;
        }
    }
}
