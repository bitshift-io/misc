﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using AForge.Robotics.Lego;
using AForge.Neuro;
using AForge.Neuro.Learning;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml.Serialization;

namespace AForgeNXT
{
    public partial class Main : Form
    {
        VREnvironment m_environment;
        List<VirtualRobot> m_virtualRobotList = new List<VirtualRobot>();
        int m_threadSleepInterval = 100;
        int m_populationCount = 5;
        List<GeneticPopulation> m_populationList = new List<GeneticPopulation>();


        private int LastX;
        private int LastY;
        private Bitmap OriginalMap, InitialMap;

        private bool m_useNetwork = false;
        private GeneticNetwork m_nnet = new GeneticNetwork();

        private NxtRobot m_nxtRobot;

        private Thread m_thread;

        double[] m_neuronInput = new double[] { 1, 1 };

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            // fill white
            try
            {
                pbTerrain.Image = new Bitmap(500, 500);
                Graphics g = Graphics.FromImage(pbTerrain.Image);
                g.FillRectangle(new SolidBrush(Color.White), 0, 0, pbTerrain.Width, pbTerrain.Height);
                g.Dispose();
                pbTerrain.Refresh();
            }
            catch (Exception ex)
            {
            }

            OriginalMap = LoadObject<Bitmap>("map.ser");
            if (OriginalMap == null)
            {
                OriginalMap = new Bitmap(pbTerrain.Image);
                //InitialMap = new Bitmap(pbTerrain.Image);
            }

            m_populationList = LoadObject<List<GeneticPopulation>>("population_list.ser");
            if (m_populationList == null)
            {
                m_populationList = new List<GeneticPopulation>();
                for (int i = 0; i < m_populationCount; ++i)
                {
                    m_populationList.Add(new GeneticPopulation());
                }
            }

            m_environment = new VREnvironment(this);

            foreach (GeneticPopulation p in m_populationList)
            {
                for (int i = 0; i < p.m_networks.Count; ++i)
                {
                    m_virtualRobotList.Add(new VRDualDrive(m_environment, this));
                }
            }
            
            m_nnet = LoadObject<GeneticNetwork>("nnet.ser");
            if (m_nnet == null)
            {
                m_nnet = new GeneticNetwork();
            }
            DisplayNetwork();

            /*
            foreach (VirtualRobot r in m_virtualRobotList)
            {
                r.SetNetwork(m_populationList.GetNextNetwork());
            }*/


            // only need this for locating the spawn spot
            pbRobot1.Visible = false;

            StartThread();
            //StartRobots();

            m_nxtRobot = new NxtRobot(this);
        }

        public void StartThread()
        {
            m_thread = new Thread(new ThreadStart(RunThread));
            m_thread.IsBackground = true;
            m_thread.Priority = ThreadPriority.AboveNormal;
            m_thread.Start();
        }

        public void StopThread()
        {
            m_thread.Interrupt();
        }

        void RunThread()
        {
            try
            {
                while (Thread.CurrentThread.IsAlive)
                {
                    MethodInvoker mi = new MethodInvoker(Step);
                    BeginInvoke(mi);

                    m_threadSleepInterval = Convert.ToInt32(txtInterval.Text);
                    Thread.Sleep(m_threadSleepInterval);
                }
            }
            catch
            {
            }
        }

        void Step()
        {
            if (m_useNetwork)
            {
                m_nxtRobot.SetNetwork(m_nnet);
            }

            if (runOnlyRobot.Checked)
            {
                m_nxtRobot.Step();
                return;
            }

            int nullNetworkCount = 0;
            GeneticPopulation population = m_populationList[0];
            foreach (VirtualRobot r in m_virtualRobotList)
            {
                r.Step();
                if (r.GetNetwork() == null)
                    ++nullNetworkCount;

                /*
                if (r.GetNetwork() == null)
                {
                    double percent = (double)population.GetCurrentNetworkIndex() / (double)population.GetNetworkCount();
                    Color c = Color.FromArgb((int)((double)(Color.Red.R) * (1 - percent) + (double)(Color.Blue.R) * percent),
                                                (int)((double)(Color.Red.G) * (1 - percent) + (double)(Color.Blue.G) * percent),
                                                (int)((double)(Color.Red.B) * (1 - percent) + (double)(Color.Blue.B) * percent));
                    r.SetColor(c);
                    r.SetNetwork(population.GetNextNetwork());
                }

                if (r.GetNetwork() != null)
                {
                    //MethodInvoker mi = new MethodInvoker(r.Step());
                    //BeginInvoke(mi);
                    r.Step();
                }
                else
                {
                    ++nullNetworkCount;
                }

                if (r.GetNetwork() == null)
                {
                    double percent = (double)population.GetCurrentNetworkIndex() / (double)population.GetNetworkCount();
                    Color c = Color.FromArgb((int)((double)(Color.Red.R) * (1 - percent) + (double)(Color.Blue.R) * percent),
                                                (int)((double)(Color.Red.G) * (1 - percent) + (double)(Color.Blue.G) * percent),
                                                (int)((double)(Color.Red.B) * (1 - percent) + (double)(Color.Blue.B) * percent));
                    r.SetColor(c);
                    r.SetNetwork(population.GetNextNetwork());
                }*/
            }

            m_nxtRobot.Step();

            if (nullNetworkCount >= m_virtualRobotList.Count)
            {
                foreach (GeneticPopulation p in m_populationList)
                {
                    p.PerformIteration();
                }

                SaveObject(m_populationList, "population_list.ser");

                // find the absolute best brain and apply to the robot
                GeneticNetwork best = m_populationList[0].m_networks[0];
                foreach (GeneticPopulation p in m_populationList)
                {
                    if (p.m_networks[0].Fitness > best.Fitness)
                    {
                        best = p.m_networks[0];
                    }
                }

                if (!m_useNetwork)
                {
                    m_nxtRobot.SetNetwork(best);
                }


                int pi = 0;
                int ri = 0;
                foreach (GeneticPopulation p2 in m_populationList)
                {
                    Color topColor = Color.Red;
                    Color worstColor = Color.Blue;
                    switch (pi)
                    {
                        case 0:
                            topColor = Color.Red;
                            break;

                        case 1:
                            topColor = Color.Green;
                            break;

                        case 2:
                            topColor = Color.Blue;
                            break;

                        case 3:
                            topColor = Color.Brown;
                            break;

                        case 4:
                            topColor = Color.Violet;
                            break;
                    }

                    //GeneticNetwork n = null;
                    //while ((n = p2.GetNextNetwork()) != null)
                    foreach (GeneticNetwork n in p2.m_networks)
                    {
                        /*
                        double percent = (double)p2.GetCurrentNetworkIndex() / (double)p2.GetNetworkCount();
                        Color c = Color.FromArgb((int)((double)(topColor.R) * (1 - percent) + (double)(worstColor.R) * percent),
                                                    (int)((double)(topColor.G) * (1 - percent) + (double)(worstColor.G) * percent),
                                                    (int)((double)(topColor.B) * (1 - percent) + (double)(worstColor.B) * percent));
                        m_virtualRobotList[ri].SetColor(c);*/
                        m_virtualRobotList[ri].SetColor(topColor);

                        m_virtualRobotList[ri].SetNetwork(n);
                        ++ri;
                    }

                    ++pi;
                }
            }

            // draw
            {
                Graphics g = Graphics.FromImage(pbTerrain.Image);
                g.DrawImage(OriginalMap, 0, 0, pbTerrain.Width, pbTerrain.Height);

                foreach (VirtualRobot r in m_virtualRobotList)
                {
                    r.Draw(g, cbLasers.Checked);
                }

                g.Dispose();
                pbTerrain.Refresh();
            }
        }
        /*
        void StartRobots()
        {
            foreach (VirtualRobot r in m_virtualRobotList)
            {
                r.SetThreadSleepInterval(m_threadSleepInterval);
                r.StartThread();
            }
        }

        void StopRobots()
        {
            foreach (VirtualRobot r in m_virtualRobotList)
            {
                r.StopThread();
            }
        }
        
        void StepRobots()
        {
            foreach (VirtualRobot r in m_virtualRobotList)
            {
                r.Step();
            }
        }*/

        private void SaveObject<T>(T List, string FileName)
        {
            try
            {

                //create a backup
                string backupName = Path.ChangeExtension(FileName, ".old");
                if (File.Exists(FileName))
                {
                    if (File.Exists(backupName))
                        File.Delete(backupName);
                    File.Move(FileName, backupName);
                }

                using (FileStream fs = new FileStream(FileName, FileMode.Create))
                {
                    //XmlSerializer ser = new XmlSerializer(typeof(T));
                    BinaryFormatter ser = new BinaryFormatter();
                    ser.Serialize(fs, List);
                    fs.Flush();
                    fs.Close();
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private static T LoadObject<T>(string FileName)
        {
            if (!File.Exists(FileName))
            {
                return default(T);
            }

            T result = default(T);

            try
            {
                using (FileStream fs = new FileStream(FileName, FileMode.Open))
                {
                    //XmlSerializer ser = new XmlSerializer(typeof(T));
                    BinaryFormatter ser = new BinaryFormatter();
                    result = (T)ser.Deserialize(fs);
                }
            }
            catch
            {
            }
            return result;
        }
      
        /*
        private void ThreadRun()
        {
            try
            {
                while (Thread.CurrentThread.IsAlive)
                {
                    int sleepInterval = (Math.Max(0, Convert.ToInt32(txtInterval.Text)));

                    MethodInvoker mi = new MethodInvoker(RobotStep);
                    this.BeginInvoke(mi);
                    Thread.Sleep(sleepInterval);
                }
            }
            catch
            {
            }
        }

        private void RobotStep()
        {
            // the robot will keep using a network till it runs in to trouble,
            // then swap to the next network
            // when we get through all networks
            // we create a new generation

            foreach (VRSteerDrive robot in m_robot)
            {
                if (m_useNetwork)
                {
                    robot.SetNetwork(m_nnet);
                }

                if (robot.m_network == null)
                {
                    bool currentBrainInUse = false;
                    do
                    {
                        currentBrainInUse = false;
                        foreach (VRSteerDrive r in m_robot)
                        {
                            if (r.m_network == m_networks[m_currentNetwork])
                            {
                                currentBrainInUse = true;
                                break;
                            }
                        }
                    } while (currentBrainInUse);

                    robot.SetNetwork(m_networks[m_currentNetwork]);
                    ++m_currentNetwork;
                }

                robot.PerformStep();

                if (m_currentNetwork >= m_networks.Count)
                {
                    NetworkIteration();
                    m_currentNetwork = 0;
                }
            }
        }
        */
        /*
        void NetworkIteration()
        {
            ++m_iterations;

            // the older we get the less variation we want to see
            double variation = (1.0 / m_iterations);

            // sort by fitness
            // discard lowest
            // keep heighest
            // time to make babies!
            // throw in some new randoms to keep things sexy!

            m_networks.Sort(GeneticNetwork.SortByFitness);
            m_networks.RemoveRange(m_bestCount, m_networks.Count - m_bestCount);

            for (int i = 0; i < m_childCount; ++i)
            {
                // 50% of these will just be mutations of thier parent, while
                // 50% are actually breeded
                GeneticNetwork child = m_breeder.GenerateChild(m_networks[m_breeder.Random.Next(m_bestCount)], m_networks[m_breeder.Random.Next(m_bestCount)]);
                m_networks.Add(child);
            }

            for (int i = 0; i < m_mutationCount; ++i)
            {
                // 50% of these will just be mutations of thier parent, while
                // 50% are actually breeded
                GeneticNetwork child = m_breeder.GenerateMutation(m_networks[m_breeder.Random.Next(m_bestCount)], variation);
                m_networks.Add(child);
            }

            for (int i = 0; i < m_randomCount; ++i)
            {
                GeneticNetwork child = m_breeder.GenerateRandom();
                m_networks.Add(child);
            }
        }
        /*
        void SetText(TextBox txtBox, String text)
        {
            if (txtBox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { txtBox, text });
            }
            else
            {
                txtBox.Text = text;
            }
        }

        private void RobotStep()
        {
            foreach (GeneticNetwork network in m_networks)
            {
                // get a reading to the left
                // get a reading to the right
                // feed to network
                // use results as engine outputs
                // did we hit anything?
                //  yes - bad result - backup
                //  no - good result
                // update fitness

                // if we havent moved since the last ultrasound readings, just use them
                double[] input = (m_lastUltrasoundReadings != null) ? m_lastUltrasoundReadings : GetUltrasoundDistances();

                SetText(leftDistanceTxt, input[0].ToString());
                SetText(rightDistanceTxt, input[1].ToString());
                Invalidate();

                double[] output = network.Compute(input);
                output[0] = (output[0] - 0.5) * 2.0; // output is from 0 to 1, need to make it -1 to 1
                output[1] = (output[0] - 0.5) * 2.0;

                DriveEngines(output);
                
                SetText(leftEngineTxt, output[0].ToString());
                SetText(rightEngineTxt,  output[1].ToString());

                Invalidate();
                Thread.Sleep(3000);

                m_lastUltrasoundReadings = GetUltrasoundDistances();
                double closest = Math.Min(m_lastUltrasoundReadings[0], m_lastUltrasoundReadings[1]);

                // bad is too close!
                if (closest <= 0.08)
                {
                    m_lastUltrasoundReadings = null; // we need to take new ultra sound readings again

                    network.Fitness -= 1.0;

                    // reverse! time to swap to a new brain!
                    output[0] = -output[0];
                    output[1] = -output[1];
                    DriveEngines(output);
                    Thread.Sleep(3000);
                }
                else
                {
                    // the more forwards we go the fitter it is, we want the thing going forwards
                    // to make progress
                    network.Fitness += 5.0 + (output[0] + output[1]);
                }

                SetText(fitnessTxt, network.Fitness.ToString());
                Invalidate();

                if (m_exit)
                    break;
            }

            ++m_iterations;

            // the older we get the less variation we want to see
            double variation = (1.0 / m_iterations);

            // sort by fitness
            // discard lowest
            // keep heighest
            // time to make babies!
            // throw in some new randoms to keep things sexy!

            m_networks.Sort(GeneticNetwork.SortByFitness);
            int length = m_networks.Count;
            int thirdLength = length / 3;
            m_networks.RemoveRange(thirdLength, m_networks.Count - thirdLength);
                //(thirdLength, m_networks.Count - 1);

            for (int i = 0; i < thirdLength; ++i)
            {
                // 50% of these will just be mutations of thier parent, while
                // 50% are actually breeded
                GeneticNetwork child = null;
                if (m_breeder.Random.NextDouble() > 0.5)
                    child = m_breeder.GenerateChild(m_networks[m_breeder.Random.Next(thirdLength)], m_networks[m_breeder.Random.Next(thirdLength)]);
                else
                    child = m_breeder.GenerateMutation(m_networks[m_breeder.Random.Next(thirdLength)], variation);

                m_networks.Add(child);
            }

            for (int i = 0; i < thirdLength; ++i)
            {
                GeneticNetwork child = m_breeder.GenerateRandom();
                m_networks.Add(child);
            }
        }

        private void SetCurrentNetwork(GeneticNetwork network)
        {
        }
        */
        /*
        
        // AGV's terrain drawing
        private void pbTerrain_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            pbTerrain.Image = CopyImage(OriginalMap);
            LastX = e.X;
            LastY = e.Y;
        }

        // AGV's terrain drawing
        private void pbTerrain_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            Graphics g = Graphics.FromImage(pbTerrain.Image);

            Color c = Color.Yellow;

            if (e.Button == MouseButtons.Left)
                c = Color.White;
            else if (e.Button == MouseButtons.Right)
                c = Color.Black;

            if (c != Color.Yellow)
            {
                g.FillRectangle(new SolidBrush(c), e.X - 40, e.Y - 40, 80, 80);

                LastX = e.X;
                LastY = e.Y;

                g.DrawImage(pbTerrain.Image, 0, 0);
                OriginalMap = CopyImage(pbTerrain.Image as Bitmap);
                pbTerrain.Refresh();
                g.Dispose();
            }

        }

        private double[] m_lastUltrasoundReadings = null;

        

        private NXTBrick m_nxt = new NXTBrick();

        
        */

        private void NetworkTextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox txtBox = (TextBox)sender;
                String txt = txtBox.Text;
                String name = txtBox.Name;
                String[] nameList = name.Split('_');
                if (nameList.Length <= 0)
                    return;

                switch (nameList[0])
                {
                    case "weight":
                        {
                            int w = Convert.ToInt32(nameList[1]);
                            int n = Convert.ToInt32(nameList[2]);
                            int l = Convert.ToInt32(nameList[3]);
                            m_nnet.mLayer[l].neuron[n].weight[w] = Convert.ToDouble(txt);
                            break;
                        }

                    case "output":
                        {
                            //int n = Convert.ToInt32(nameList[1]);
                            //int l = Convert.ToInt32(nameList[2]);
                            break;
                        }

                    case "input":
                        {
                            int n = Convert.ToInt32(nameList[1]);
                            m_neuronInput[n] = Convert.ToDouble(txt);
                            break;
                        }

                    default:
                        break;
                }

                DisplayNetwork(false);
            }
            catch 
            {
            }
        }

        void DisplayNetwork(bool setWeights = true)
        {
            if (setWeights)
            {
                // layer 0
                weight_0_0_0.Text = m_nnet.mLayer[0].neuron[0].weight[0].ToString();
                weight_1_0_0.Text = m_nnet.mLayer[0].neuron[0].weight[1].ToString();
                weight_2_0_0.Text = m_nnet.mLayer[0].neuron[0].weight[2].ToString();

                weight_0_1_0.Text = m_nnet.mLayer[0].neuron[1].weight[0].ToString();
                weight_1_1_0.Text = m_nnet.mLayer[0].neuron[1].weight[1].ToString();
                weight_2_1_0.Text = m_nnet.mLayer[0].neuron[1].weight[2].ToString();

                weight_0_2_0.Text = m_nnet.mLayer[0].neuron[2].weight[0].ToString();
                weight_1_2_0.Text = m_nnet.mLayer[0].neuron[2].weight[1].ToString();
                weight_2_2_0.Text = m_nnet.mLayer[0].neuron[2].weight[2].ToString();

                // layer 1
                weight_0_0_1.Text = m_nnet.mLayer[1].neuron[0].weight[0].ToString();
                weight_1_0_1.Text = m_nnet.mLayer[1].neuron[0].weight[1].ToString();
                weight_2_0_1.Text = m_nnet.mLayer[1].neuron[0].weight[2].ToString();
                weight_3_0_1.Text = m_nnet.mLayer[1].neuron[0].weight[3].ToString();

                weight_0_1_1.Text = m_nnet.mLayer[1].neuron[1].weight[0].ToString();
                weight_1_1_1.Text = m_nnet.mLayer[1].neuron[1].weight[1].ToString();
                weight_2_1_1.Text = m_nnet.mLayer[1].neuron[1].weight[2].ToString();
                weight_3_1_1.Text = m_nnet.mLayer[1].neuron[1].weight[3].ToString();
            }

            double[] output = m_nnet.Compute(m_neuronInput);

            output_0_0.Text = m_nnet.mLayer[0].neuron[0].output.ToString();
            output_1_0.Text = m_nnet.mLayer[0].neuron[1].output.ToString();
            output_2_0.Text = m_nnet.mLayer[0].neuron[2].output.ToString();

            output_0_1.Text = output[0].ToString();
            output_1_1.Text = output[1].ToString();
        }

        private void useNetwork_CheckedChanged(object sender, EventArgs e)
        {
            m_useNetwork = (useNetwork.CheckState == CheckState.Checked);
        }

        private void step_Click(object sender, EventArgs e)
        {
            Step();
            //StepRobots();
        }

        private void pause_CheckedChanged(object sender, EventArgs e)
        {
            if (pause.CheckState == CheckState.Checked)
                StopThread(); //StopRobots();
            else
                StartThread(); // StartRobots();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopThread(); // StopRobots();
            SaveObject(m_populationList, "population_list.ser");
            SaveObject(m_nnet, "nnet.ser");
            SaveObject(OriginalMap, "map.ser");
        }

        private void txtInterval_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox txtBox = (TextBox)sender;
                m_threadSleepInterval = Convert.ToInt32(txtBox.Text);
                /*
                foreach (VirtualRobot r in m_virtualRobotList)
                {
                    r.SetThreadSleepInterval(m_threadSleepInterval);
                }*/
            }
            catch
            {
            }
        }

        private void Main_Paint(object sender, PaintEventArgs e)
        {/*
            Graphics g = CreateGraphics(); // Graphics.FromImage(m_form.pbTerrain.Image); //m_form.CreateGraphics();
            //if (m_form.cbLasers.Checked)
            {
                //g.DrawLine(new Pen(Color.Green, 1), pFrontObstacle, pPos);
                g.DrawLine(new Pen(Color.Green, 1), new Point(100, 100), new Point(0, 0));
                //g.DrawLine(new Pen(Color.Red, 1), pRightObstacle, pPos);
            }*/
        }

        // terrain drawing
        private void pbTerrain_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //pbTerrain.Image = new Bitmap(OriginalMap);
            LastX = e.X;
            LastY = e.Y;
        }

        // terrain drawing
        private void pbTerrain_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            Graphics g = Graphics.FromImage(pbTerrain.Image);

            Color c = Color.Yellow;

            int size = 10;
            if (e.Button == MouseButtons.Left)
            {
                c = Color.White;
                size = 15;
            }
            else if (e.Button == MouseButtons.Right)
            {
                c = Color.Black;
            }

            if (c != Color.Yellow)
            {
                int halfSize = size / 2;
                g.FillEllipse(new SolidBrush(c), e.X - halfSize, e.Y - halfSize, size, size);

                LastX = e.X;
                LastY = e.Y;

                g.DrawImage(pbTerrain.Image, 0, 0);
                OriginalMap = new Bitmap(pbTerrain.Image as Bitmap);
                pbTerrain.Refresh();
                g.Dispose();
            }

        }

        private void pbTerrain_MouseUp(object sender, MouseEventArgs e)
        {
            SaveObject(OriginalMap, "map.ser");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //m_nxtRobot.Test();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //m_nxtRobot.Test2();
        }

        private void nxt_connect_Click(object sender, EventArgs e)
        {
            if (m_nxtRobot.Connect(nxt_com_port.Text))
            {
                nxt_connected.Checked = true;
            }
            else
            {
                nxt_connected.Checked = false;
            }
        }

        private void nxt_disconnect_Click(object sender, EventArgs e)
        {
            m_nxtRobot.Disconnect();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double value = Convert.ToDouble(textBox3.Text);
                //m_nxtRobot.RotateTo(value);
            }
            catch
            {
            }
        }
    }
}
