﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace AForgeNXT
{
    // outputs from nn steer drive direction/power and the steering engine
    class VRSteerDrive : VirtualRobot
    {
        const int Steer = 0;
        const int Drive = 1;

        public VRSteerDrive(VREnvironment environment, Main form)
            : base(environment, form)
        {

        }

        public override void Move(double[] outputs)
        {
            m_direction += outputs[Steer] * 10.0;
            double distance = 4.0 * outputs[Drive];
            m_position = MoveTo(m_position, m_direction, distance);
        }

        public override void IncrementFitness(VirtualRobot.StepData step)
        {
            double closest = Math.Min(step.fitnessInputs[0], step.fitnessInputs[1]);

            // bad is too close!
            if (closest <= 0.02)
            {
                m_network.Fitness -= 10.0;
                m_network = null; // means we get a new brain next step
            }
            else
            {
                double fitnessIncrement = m_environment.GetDistance(m_initialPosition, m_stepList[0].position);
                m_network.Fitness += fitnessIncrement;
            }
        }
    }
}
