﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

namespace AForgeNXT
{
    class Robot
    {
        protected Main m_form;
        
        protected GeneticNetwork m_network;

        public Robot(Main form)
        {
            m_form = form;
        }

        public virtual void SetNetwork(GeneticNetwork network)
        {
            m_network = network;
            if (m_network != null)
            {
                m_network.Fitness = 0;
            }
        }

        public virtual double[] CalculateOutputs(double[] inputs)
        {
            if (inputs == null || m_network == null)
                return null;

            return m_network.Compute(inputs);
        }

        public virtual GeneticNetwork GetNetwork()
        {
            return m_network;
        }

        public virtual void Step()
        {
        }
    }

    class VirtualRobot : Robot
    {
        public class StepData
        {
            public double[] inputs = new double[2];
            public double[] outputs = new double[2];
            public double[] fitnessInputs = new double[2];

            public Point position;
            public double direction;
        }

        protected Color m_color = Color.Red;
        protected DateTime m_startTime;
        protected VREnvironment   m_environment;
        protected Point m_position;
        protected Point m_initialPosition;
        protected double m_initialDirection = -90;
        protected double m_direction = 0;
        protected List<StepData> m_stepList = new List<StepData>(); // history

        public VirtualRobot(VREnvironment environment, Main form)
            : base(form)
        {
            m_environment = environment;
            m_initialPosition = m_form.pbRobot1.Location;
            Reset();
        }

        public override void SetNetwork(GeneticNetwork network)
        {
            base.SetNetwork(network);
            Reset();
        }

        // default implementation simply ray casts out at 45 degrees from forward
        public virtual double[] GetNetworkInputs()
        {
            double[] result = new double[2];
            result[0] = (double)m_environment.GetMeasure(m_position, m_direction - 45) / 256.0;
            result[1] = (double)m_environment.GetMeasure(m_position, m_direction + 45) / 256.0;
            return result;
        }

        public virtual void IncrementFitness(double[] initialInputs, double[] outputs, double[] fitnessInputs)
        {
        }

        public virtual void Move(double[] outputs)
        {
        }

        public virtual Point MoveTo(Point fromPosition, double direction, double distance)
        {
            double rad = (direction * Math.PI) / 180;
            int xMovement = Convert.ToInt32(distance * Math.Cos(rad));
            int yMovement = Convert.ToInt32(distance * Math.Sin(rad));

            return new Point(fromPosition.X + xMovement, fromPosition.Y + yMovement);
        }

        public override void Step()
        {
            if (m_network == null)
                return;

            StepData step = new StepData();
            m_stepList.Insert(0, step);
            step.inputs = GetNetworkInputs();
            step.outputs = CalculateOutputs(step.inputs);
            Move(step.outputs);
            step.fitnessInputs = GetNetworkInputs();
            step.direction = m_direction;
            step.position = m_position;
            IncrementFitness(step);

            // stop brain after n seconds
            TimeSpan timeSpan = DateTime.Now - m_startTime;
            if (m_network != null && timeSpan.TotalSeconds > 10)
            {
                NormalizeFitness(timeSpan);
                m_network = null;
            }
        }

        StepData GetStepData()
        {
            return m_stepList[0];
        }

        virtual public void NormalizeFitness(TimeSpan timeSpan)
        {
        }

        virtual public void IncrementFitness(StepData step)
        {
        }

        virtual public void Reset()
        {
            m_stepList.Clear();
            m_startTime = DateTime.Now;
            m_position = m_initialPosition;
            m_direction = m_environment.m_random.Next(0, 360); 
                //m_initialDirection;
        }

        public void SetColor(Color color)
        {
            m_color = color;
        }

        virtual public void DrawLine(Graphics g, Pen pen, Point position, double direction, double distance)
        {
            double rad = (direction * Math.PI) / 180;
            int xMovement = Convert.ToInt32(distance * Math.Cos(rad));
            int yMovement = Convert.ToInt32(distance * Math.Sin(rad));

            Point destination = new Point(position.X + xMovement, position.Y + yMovement);
            g.DrawLine(pen, position, destination);
        }

        virtual public void Draw(Graphics g, bool debug)
        {
            if (m_network == null || m_stepList.Count <= 0)
                return;

            StepData step = m_stepList[0];
            
            if (debug)
            {
                DrawLine(g, new Pen(Color.Green, 1), step.position, step.direction - 45, step.fitnessInputs[0] * 256);
                DrawLine(g, new Pen(Color.Blue, 1), step.position, step.direction + 45, step.fitnessInputs[1] * 256);
            }

            int size = 6;
            int halfSize = size / 2;
            g.FillEllipse(new SolidBrush(m_color), step.position.X - halfSize, step.position.Y - halfSize, size, size);
            if (debug)
            {
                try
                {
                    g.DrawString(/*"I:" + m_network.m_id *+ */" F:" + Convert.ToInt32(m_network.Fitness).ToString(), new Font("Arial", 8), new SolidBrush(m_color), step.position);
                }
                catch { }
            }
        }
    }
}
