varying vec4 color;
varying vec4 diffuse;

void main()
{
   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
   gl_TexCoord[0] = gl_MultiTexCoord0;
   
   vec4 normEye = vec4( normalize( gl_NormalMatrix * gl_Normal ), 1.0 ); //this is eye space!
   vec4 eye = normalize( gl_ModelViewMatrix * gl_Vertex );
   vec4 light = normalize( gl_LightSource[0].position - eye );
   vec4 secondary = vec4( 0.3, 0.3, 0.3, 1.0);
   vec4 primary = vec4( 0.0, 0.0, 0.0, 1.0);
   
   float diff = clamp( dot( light, normEye ), 0.0, 1.0 );   
   float d = max( dot(-eye, normEye), 0.0 ) / 2.0; 
   float p = pow( 1.0 - d, 4.0 ); 
   
   diffuse = ((gl_LightSource[0].diffuse * diff) + gl_LightSource[0].ambient);   
   color = (primary * d + secondary * p);
}
