varying vec4 color;
uniform sampler2D tex;
varying vec4 diffuse;

void main()
{
   vec4 base = texture2D( tex, vec2(gl_TexCoord[0]) );
   gl_FragColor = color + diffuse * base;
}
