uniform sampler2D texture;
uniform sampler2D normal;
uniform sampler2D height;
varying vec3 n;

void main( void )
{
	//lets start with some per pixel lighting
	
	
	gl_FragColor = vec4( n, 1);
	/*
	gl_FragColor = texture2D( texture, vec2(gl_TexCoord[0]) ) + 
		texture2D( normal, vec2(gl_TexCoord[0]) ) +
		texture2D( height, vec2(gl_TexCoord[0]) );*/
}
