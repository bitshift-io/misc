uniform sampler2D texture;

varying vec3 eye;
varying vec3 light;
varying vec3 normal;

void main()
{
	vec3 nEye = normalize( eye );
	vec3 nLight = normalize( light );
	vec3 nNormal = normalize( normal );	
	vec3 reflectEye = reflect( nEye, nNormal );
	
	float diff = clamp( dot( nLight, nNormal ), 0.0, 1.0 );
	
	float spec = 0.0;		
	if( diff > 0.0 )
		spec = clamp( pow( clamp( dot( reflectEye, nLight ), 0.0, 1.0 ), gl_FrontMaterial.shininess ), 0.0, 1.0);
	
	vec4 difCol = texture2D( texture, vec2(gl_TexCoord[0]) ) * 
		((gl_LightSource[0].diffuse * diff) + gl_LightSource[0].ambient );
		
	vec4 specCol = gl_LightSource[0].specular * spec * gl_FrontMaterial.specular;
	
	gl_FragColor = difCol + specCol;
}