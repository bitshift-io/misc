varying vec3 vertEye;
varying vec3 normalDir;

void main (void)
{	
    	vec3 lightDir = normalize( gl_LightSource[0].position.xyz - vertEye );
    	vec3 eyeDir = normalize( -vertEye );
    	vec3 reflectDir = normalize( -reflect( lightDir, normalDir ) );
    	
    	float diffuseAngle = max( dot( normalDir, lightDir ), 0.0 );
    	
    	//float specAngle = pow( max( 1.0 - dot( lightDir, -reflectDir ), 0.0 ), 2.0);

	vec4 base = vec4(1,1,1,1);//texture2D( tex, vec2(gl_TexCoord[0]) );

	gl_FragColor = base * diffuseAngle;// + base * specAngle;
}
