varying vec3 eye;
varying vec3 light;
varying vec3 normal;

void main()
{
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_TexCoord[0] = gl_MultiTexCoord0;
	
	eye = vec3( gl_ModelViewMatrix * gl_Vertex );
	light = normalize( gl_LightSource[0].position.xyz - eye );
	normal = normalize( gl_NormalMatrix * gl_Normal );
}