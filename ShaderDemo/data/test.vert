varying vec3 vertEye;
varying vec3 normalDir;

void main(void) 
{
    	vertEye = vec3( gl_ModelViewMatrix * gl_Vertex );
    	normalDir = normalize( gl_NormalMatrix * gl_Normal );
    	
    	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
    	gl_TexCoord[0] = gl_MultiTexCoord0;
}