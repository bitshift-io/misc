uniform sampler2D texture;

varying vec3 eye;
varying vec3 light;
varying vec3 normal;

void main()
{
	vec3 nEye = normalize( eye );
	vec3 nLight = normalize( light );
	vec3 nNormal = normalize( normal );	
	vec3 reflectEye = normalize( reflect( nEye, nNormal ) );
	
	float diff = clamp( pow( dot( nLight, nNormal ), 0.0, 1.0 ), 32);
	
	float spec = 0.0;
	if( diff > 0.0 )
		spec = clamp( pow( dot( reflectEye, nLight ), 32.0), 0.0, 1.0 );
	
	vec4 difCol = texture2D( texture, vec2(gl_TexCoord[0]) ) * gl_LightSource[0].diffuse * diff;
	vec4 ambCol = gl_LightSource[0].ambient;
	
	vec4 specCol = gl_LightSource[0].specular * spec;
	
	gl_FragColor = difCol + ambCol; + specCol;
}