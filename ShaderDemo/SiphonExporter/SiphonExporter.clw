; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=ExportDialog
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "siphonexporter.h"
LastPage=0

ClassCount=1

ResourceCount=3
Class1=ExportDialog
Resource1=IDD_PANEL (English (U.S.))
Resource2=DLG_SMAP
Resource3=DLG_SMODEL

[DLG:IDD_PANEL (English (U.S.))]
Type=1
Class=ExportDialog
ControlCount=2
Control1=IDC_EXPORT,button,1342242816
Control2=IDC_CANCEL,button,1342242816

[CLS:ExportDialog]
Type=0
HeaderFile=ExportDialog.h
ImplementationFile=ExportDialog.cpp
BaseClass=CDialog
Filter=D
LastObject=ExportDialog
VirtualFilter=dWC

[DLG:DLG_SMAP]
Type=1
Class=?
ControlCount=10
Control1=IDC_STATIC,static,1342308352
Control2=IDC_RENDER_DIVISIONS,edit,1350639744
Control3=IDC_COLLISION_DIVISIONS,edit,1350639744
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,button,1342177287
Control6=IDC_STATIC,button,1342177287
Control7=IDC_CHECK1,button,1342242819
Control8=IDC_CHECK2,button,1342242819
Control9=IDC_CHECK3,button,1342242819
Control10=IDC_STATIC,static,1342177294

[DLG:DLG_SMODEL]
Type=1
Class=?
ControlCount=5
Control1=IDC_STATIC,button,1342177287
Control2=IDC_CHECK1,button,1342242819
Control3=IDC_CHECK2,button,1342242819
Control4=IDC_CHECK3,button,1342242819
Control5=IDC_STATIC,static,1342177294

