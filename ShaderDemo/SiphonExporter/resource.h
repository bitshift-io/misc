//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SiphonExporter.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDS_TAB1                        6
#define IDS_TAB2                        7
#define IDD_PANEL                       101
#define DLG_SMAP                        105
#define DLG_SMODEL                      106
#define IDB_IMAGE                       110
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_EXPORT                      1002
#define IDC_CANCEL                      1013
#define IDC_RENDER_DIVISIONS            1020
#define IDC_COLLISION_DIVISIONS         1021
#define IDC_CHECK1                      1023
#define IDC_CHECK2                      1024
#define IDC_CHECK3                      1025
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_SPIN                        1496

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        111
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
