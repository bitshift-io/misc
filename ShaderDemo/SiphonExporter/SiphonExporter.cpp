/**********************************************************************
 *<
	FILE: SiphonExporter.cpp

	DESCRIPTION:	Appwizard generated plugin

	CREATED BY: 

	HISTORY: 

 *>	Copyright (c) 2000, All Rights Reserved.
 **********************************************************************/

#include "SiphonExporter.h"

#define SIPHONEXPORTER_CLASS_ID	Class_ID(0x19c02651, 0x274632a6)

class SiphonExporter : public SceneExport 
{
public:
		bool exportSelected;
		bool bExportAsModel;
		bool bExportAsMap;
		int renderDivisions;
		int collisionDivisions;
		int noSectors;
		LPTSTR temp;


		IGameScene* scene;


		static HWND hParams;


		int				ExtCount();					// Number of extensions supported
		const TCHAR *	Ext(int n);					// Extension #n (i.e. "3DS")
		const TCHAR *	LongDesc();					// Long ASCII description (i.e. "Autodesk 3D Studio File")
		const TCHAR *	ShortDesc();				// Short ASCII description (i.e. "3D Studio")
		const TCHAR *	AuthorName();				// ASCII Author name
		const TCHAR *	CopyrightMessage();			// ASCII Copyright message
		const TCHAR *	OtherMessage1();			// Other message #1
		const TCHAR *	OtherMessage2();			// Other message #2
		unsigned int	Version();					// Version number * 100 (i.e. v3.01 = 301)
		void			ShowAbout(HWND hWnd);		// Show DLL's "About..." box

		BOOL SupportsOptions(int ext, DWORD options);
		int				DoExport(const TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts=FALSE, DWORD options=0);


		bool ReadConfig();
		bool WriteConfig();
		TSTR GetCfgFilename();

		//Constructor/Destructor

		SiphonExporter();
		~SiphonExporter();		

};

class SiphonExporterClassDesc:public ClassDesc2 
{
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new SiphonExporter(); }
	const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); }
	SClass_ID		SuperClassID() { return SCENE_EXPORT_CLASS_ID; }
	Class_ID		ClassID() { return SIPHONEXPORTER_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("SiphonExporter"); }	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance() { return hInstance; }				// returns owning module handle
	virtual char* GetRsrcString( long t ){ return NULL; }
};

static SiphonExporter *sImp = NULL;
static SiphonExporterClassDesc SiphonExporterDesc;
ClassDesc2* GetSiphonExporterDesc() { return &SiphonExporterDesc; }

BOOL CALLBACK SiphonExporterOptionsDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam);

//TAB CONTROL
//http://msdn.microsoft.com/library/default.asp?url=/library/en-us/shellcc/platform/commctls/tab/tab.asp

HWND WINAPI DoCreateTabControl(HWND hwndParent) 
{ 
    RECT rcClient; 
    HWND hwndTab; 
    TCITEM tie; 
    int i; 

	char g_achTemp[256]; //temp char buffer
	HINSTANCE g_hinst = (HINSTANCE)GetWindowLong(hwndParent,GWL_HINSTANCE);
 
	int margin = 5;
    // Get the dimensions of the parent window's client area, and 
    // create a tab control child window of that size. 
    GetClientRect(hwndParent, &rcClient); 
    InitCommonControls(); 
    hwndTab = CreateWindow( 
        WC_TABCONTROL, "", 
        WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE,// | WS_BORDER, 
        margin, margin, rcClient.right - margin * 2, rcClient.bottom - 50, 
        hwndParent, NULL, g_hinst, NULL 
        ); 
    if (hwndTab == NULL) 
        return NULL; 
 
    // Add tabs for each day of the week. 
    tie.mask = TCIF_TEXT | TCIF_IMAGE; 
    tie.iImage = -1; 
    tie.pszText = g_achTemp; 
 
    for (i = 0; i < 2; i++) 
	{ 
        LoadString(g_hinst, IDS_TAB1 + i, 
                g_achTemp, sizeof(g_achTemp)/sizeof(g_achTemp[0])); 

        if( TabCtrl_InsertItem(hwndTab, i, &tie) == -1 ) 
		{ 
            DestroyWindow(hwndTab); 
            return NULL; 
        } 
    } 
    return hwndTab; 
}

BOOL CALLBACK TabDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam) 
{
	static SiphonExporter *imp = NULL;

	switch(message) 
	{
		case WM_INITDIALOG:
			imp = (SiphonExporter *)lParam;
			CenterWindow(hWnd,GetParent(hWnd));
			break;
	}

	return FALSE;
}

HWND WINAPI DoCreateDisplayWindow(HWND hwndParent) 
{ 
	HINSTANCE g_hinst = (HINSTANCE)GetWindowLong(hwndParent,GWL_HINSTANCE);

	RECT rcClient;
	GetClientRect(hwndParent, &rcClient);

    HWND hwndStatic = CreateWindow("STATIC", "", 
        WS_CHILD | WS_VISIBLE, // | WS_BORDER, 
        0, 20, rcClient.right, rcClient.bottom - 20, 
        hwndParent, NULL, g_hinst, NULL); 
 
    return hwndStatic; 
} 

DLGTEMPLATE* WINAPI DoLockDlgRes(LPCSTR lpszResName, HINSTANCE g_hinst ) 
{ 
    HRSRC hrsrc = FindResource(NULL, lpszResName, RT_DIALOG); 
    HGLOBAL hglb = LoadResource(g_hinst, hrsrc); 
    return (DLGTEMPLATE *) LockResource(hglb); 
} 

void SetTab( int page, HWND g_hwndDisplay )
{
	static HWND g_tabDisplay = NULL;
	DLGTEMPLATE* dlgPage = NULL;
	static HINSTANCE g_hinst = (HINSTANCE)GetWindowLong(g_hwndDisplay,GWL_HINSTANCE);

	RECT rcClient;
	GetClientRect(g_hwndDisplay, &rcClient);

	struct DlgTemplate 
	{
		DLGTEMPLATE dt;
		WORD wMenu;
		WORD wClass;
		WORD wTitle;
	};

/*
	HRSRC hrsrc = FindResource(NULL, MAKEINTRESOURCE(DLG_FIRST), RT_DIALOG); 
    HGLOBAL hglb = LoadResource(g_hinst, hrsrc); 
    dlgPage = (DLGTEMPLATE*)LockResource(hglb);
*/
	return;
/*
	if( dlgPage == NULL )
	{
		exit(0);
	}


	switch( page )
	{
	case 0:
		dlgPage = DoLockDlgRes(MAKEINTRESOURCE(DLG_FIRST), g_hinst);
		break;
	case 1:
		dlgPage = DoLockDlgRes(MAKEINTRESOURCE(DLG_SECOND), g_hinst);
		break;
	case 2:
		dlgPage = DoLockDlgRes(MAKEINTRESOURCE(DLG_THIRD), g_hinst);
		break;
	}
*/
	dlgPage->style = WS_CHILD | WS_VISIBLE | WS_BORDER;
	dlgPage->cx = rcClient.right;
	dlgPage->cy = rcClient.bottom;


	// Destroy the current child dialog box, if any.
	//if( g_tabDisplay != NULL) 
	//	DestroyWindow(g_hwndDisplay); 
 
	// Create the new child dialog box. 
//	g_tabDisplay = CreateDialogIndirect(g_hinst, 
//		dlgPage, g_hwndDisplay, TabDlgProc);
}

LRESULT WINAPI WinProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
    {
        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;
        break;
        case WM_KEYDOWN: 
            switch (wParam)
            { 
                case VK_ESCAPE:
                    //User has pressed the escape key, so quit
                    DestroyWindow(hWnd);
                    return 0;
                break;
            } 
        break;

    }

    return DefWindowProc(hWnd, msg, wParam, lParam);
}

BOOL CALLBACK PanelDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam) 
{
	//MessageBox( 0, "OY!!!", "HRMM", MB_OK );

	//if( sImp != NULL )
	//	return false;

	switch(message) 
	{/*
		case WM_INITDIALOG:
			{
				SetWindowText( GetDlgItem(hWnd, IDC_RENDER_DIVISIONS), "2" );
				SetWindowText( GetDlgItem(hWnd, IDC_COLLISION_DIVISIONS), "5" );

			}*/
		case WM_NOTIFY:
			switch (((NMHDR FAR *) lParam)->code) 
			{/*
			case WM_INITDIALOG:
				{
					exit(0);
				}	
				break;
			case PSN_SETACTIVE:
				{
					SetWindowText( GetDlgItem(hWnd, IDC_RENDER_DIVISIONS), "2" );
					SetWindowText( GetDlgItem(hWnd, IDC_COLLISION_DIVISIONS), "5" );
					exit(0);
				}
				break;*/
			case PSN_APPLY:
				{/*
					LPTSTR txt;
					GetWindowText( GetDlgItem(hWnd, IDC_RENDER_DIVISIONS), txt ,NULL );
					imp->renderDivisions = atoi( txt );

					GetWindowText( GetDlgItem(hWnd, IDC_COLLISION_DIVISIONS), txt ,NULL );
					imp->collisionDivisions = atoi( txt );
					EndDialog(hWnd,0);*/
					//MessageBox( 0, "hello?", "HRMM", MB_OK );
					if( sImp != NULL )
					{
						
						LPTSTR txt = new char[256];
						GetWindowText( GetDlgItem(hWnd, IDC_RENDER_DIVISIONS), txt, 256 );
						sImp->renderDivisions = atoi( txt );
						//sImp->temp = txt;

						//FILE* temp =  fopen("test.txt", "w");
						//fprintf(temp, "%s\n", txt );
						//MessageBox( 0, txt, "HRMM", MB_OK );


						GetWindowText( GetDlgItem(hWnd, IDC_COLLISION_DIVISIONS), txt, 256 );
						sImp->collisionDivisions = atoi( txt );

						//MessageBox( 0, txt, "HRMM", MB_OK );

						//fprintf(temp, "%s\n", txt );

						//fclose( temp );

						delete txt;
						txt = 0;
					}
				}
				break;
			}
			break;
	}
	return FALSE;
}


VOID DoPropertySheet(HWND hwndOwner)
{
	HINSTANCE g_hinst = (HINSTANCE)GetWindowLong(hwndOwner,GWL_HINSTANCE);
/*
	HWND hWnd;
	HINSTANCE hInstance;
	
	//Register the window class
	WNDCLASSEX wc = {sizeof(WNDCLASSEX), CS_CLASSDC, WinProc, 0L, 0L, 
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     "SIPHON", NULL};

   	//wc.hIcon	   = LoadIcon(hInstance, (LPCTSTR)IDI_ICON1);
    //wc.hIconSm	   = LoadIcon(hInstance, (LPCTSTR)IDI_ICON1);
	
	//Set the mouse pointer to an arrow
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);	
	RegisterClassEx(&wc);

    //Create the application's window
	hWnd = CreateWindow("SIPHON", "SIPHON", 
                              WS_OVERLAPPEDWINDOW|WS_EX_TOPMOST, 20, 20, 100, 100,
                              NULL, NULL, hInstance, NULL);


    SetFocus( hWnd );
    SetForegroundWindow( hWnd );
	ShowWindow(hWnd, SW_SHOWNORMAL);
    UpdateWindow(hWnd);
	*/

    PROPSHEETPAGE psp[2];
    PROPSHEETHEADER psh;
    psp[0].dwSize = sizeof(PROPSHEETPAGE);
    psp[0].dwFlags =  PSP_USETITLE; //PSP_USEICONID |
    psp[0].hInstance = g_hinst;
    psp[0].pszTemplate = MAKEINTRESOURCE(DLG_SMAP);
    //psp[0].pszIcon = MAKEINTRESOURCE(IDI_FONT);
    psp[0].pfnDlgProc = PanelDlgProc; //SiphonExporterOptionsDlgProc;
    psp[0].pszTitle = "Map";//MAKEINTRESOURCE(IDS_FONT)
    psp[0].lParam = 0;
    psp[0].pfnCallback = NULL;
	
    psp[1].dwSize = sizeof(PROPSHEETPAGE);
    psp[1].dwFlags = PSP_USEICONID | PSP_USETITLE;
    psp[1].hInstance = g_hinst;
    psp[1].pszTemplate =
       MAKEINTRESOURCE(DLG_SMODEL);
    //psp[1].pszIcon = MAKEINTRESOURCE(IDI_BORDER);
    psp[1].pfnDlgProc = PanelDlgProc;
    psp[1].pszTitle = "Model";//MAKEINTRESOURCE(IDS_BORDER);
    psp[1].lParam = 0;
    psp[1].pfnCallback = NULL;
    psh.dwSize = sizeof(PROPSHEETHEADER);
    psh.dwFlags = PSH_PROPSHEETPAGE; //PSH_USEICONID |
    psh.hwndParent = hwndOwner;
    psh.hInstance = g_hinst;

	psh.dwFlags |= PSH_NOAPPLYNOW;

    //psh.pszIcon =
    //   MAKEINTRESOURCE(IDI_CELL_PROPERTIES);
    psh.pszCaption = (LPSTR) "Siphon Exporter";
    psh.nPages = sizeof(psp) /
       sizeof(PROPSHEETPAGE);
    psh.nStartPage = 0;
    psh.ppsp = (LPCPROPSHEETPAGE) &psp;
    psh.pfnCallback = NULL;
    PropertySheet(&psh);
    return;
}

BOOL CALLBACK SiphonExporterOptionsDlgProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam) 
{
	static SiphonExporter *imp = NULL;
	static HWND g_hwndTab;
	static HWND g_hwndDisplay;
	char g_achTemp[256]; //temp char buffer
	static HINSTANCE g_hinst = (HINSTANCE)GetWindowLong(hWnd,GWL_HINSTANCE);
	static bool bInit = false;

	switch(message) 
	{
		case WM_INITDIALOG:
			{
				if( bInit )
				{
					//DoPropertySheet(hWnd);
					//return TRUE;
				}
				else
				{
					bInit = true;

					sImp = (SiphonExporter *)lParam;
					imp = (SiphonExporter *)lParam;
					CenterWindow(hWnd,GetParent(hWnd));
				}

				DoPropertySheet(hWnd);

				//OnTabbedDialogInit(hWnd);
				//g_hwndTab = DoCreateTabControl(hWnd);
				//g_hwndDisplay = DoCreateDisplayWindow(g_hwndTab);
				//SetTab( 0, g_hwndDisplay );
				EndDialog(hWnd,0);

				return TRUE;
			}

		//case WM_NOTIFY: 
           


		case WM_COMMAND:
			switch( LOWORD(wParam) ) 
			{
				case IDC_CANCEL:
					EndDialog(hWnd,1);
					return TRUE;
					break;

				case IDC_EXPORT:
					// Retrieve preferences
					//imp->bExportAsMap = GetCheckBox(hWnd,IDC_MAP)?1:0;
					//imp->bExportAsModel = GetCheckBox(hWnd,IDC_MODEL)?1:0;

					//imp->noSectors = GetText(hWnd,IDC_SECTORS); //?0:1;
					

					EndDialog(hWnd,0);
				break;
			}
			break;

		case WM_CLOSE:
			/*
			LPTSTR txt;
			GetWindowText( GetDlgItem(hWnd, IDC_RENDER_DIVISIONS), txt ,NULL );
			imp->renderDivisions = atoi( txt );

			GetWindowText( GetDlgItem(hWnd, IDC_COLLISION_DIVISIONS), txt ,NULL );
			imp->collisionDivisions = atoi( txt );*/
			EndDialog(hWnd, 1);
			return TRUE;
	}
	return FALSE;
}

//--- SiphonExporter -------------------------------------------------------
SiphonExporter::SiphonExporter()
{

}

SiphonExporter::~SiphonExporter() 
{

}

int SiphonExporter::ExtCount()
{
	//TODO: Returns the number of file name extensions supported by the plug-in.
	return 3;
}

const TCHAR *SiphonExporter::Ext(int n)
{		
	//TODO: Return the 'i-th' file name extension (i.e. "3DS").
	switch( n )
	{
	case 0:
		return _T("SML");
		break;
	case 1:
		return _T("SST");
		break;
	case 2:
		return _T("SMP");
		break;
	}
	return _T("");
}

const TCHAR *SiphonExporter::LongDesc()
{
	//TODO: Return long ASCII description (i.e. "Targa 2.0 Image File")
	return _T("Siphon Exporter");
}
	
const TCHAR *SiphonExporter::ShortDesc() 
{			
	//TODO: Return short ASCII description (i.e. "Targa")
	return _T("Siphon");
}

const TCHAR *SiphonExporter::AuthorName()
{			
	//TODO: Return ASCII Author name
	return _T("Fabian Mathews");
}

const TCHAR *SiphonExporter::CopyrightMessage() 
{	
	// Return ASCII Copyright message
	return _T("");
}

const TCHAR *SiphonExporter::OtherMessage1() 
{		
	//TODO: Return Other message #1 if any
	return _T("");
}

const TCHAR *SiphonExporter::OtherMessage2() 
{		
	//TODO: Return other message #2 in any
	return _T("");
}

unsigned int SiphonExporter::Version()
{				
	//TODO: Return Version number * 100 (i.e. v3.01 = 301)
	return 100;
}

void SiphonExporter::ShowAbout(HWND hWnd)
{			
	// Optional
}

BOOL SiphonExporter::SupportsOptions(int ext, DWORD options)
{
	// TODO Decide which options to support.  Simply return
	// true for each option supported by each Extension 
	// the exporter supports.

	return TRUE;
}

TSTR SiphonExporter::GetCfgFilename()
{
	TSTR filename;
	
	filename += GetCOREInterface()->GetDir(APP_PLUGCFG_DIR);
	filename += "\\";
	filename += "IgameExport.cfg";

	return filename;
}

bool SiphonExporter::ReadConfig()
{
	FILE* pFile = fopen( GetCfgFilename(), "rb" );

	fclose( pFile );
	return true;
}

bool SiphonExporter::WriteConfig()
{
	FILE* pFile = fopen( GetCfgFilename(), "wb" );

	fclose( pFile );
	return true;
}

// Dummy function for progress bar
DWORD WINAPI fn(LPVOID arg)
{
	return(0);
}

int	SiphonExporter::DoExport(const TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts, DWORD options)
{
	//TODO: Implement the actual file Export here and 
	//		return TRUE If the file is exported properly



	HRESULT hr;

	Interface* ip = GetCOREInterface();

	//MyErrorProc pErrorProc;

	UserCoord Whacky = {
		1,	//Right Handed
		1,	//X axis goes right
		4,	//Y Axis goes in
		3,	//Z Axis goes down.
		0,	//U Tex axis is left
		1,  //V Tex axis is Down
	};	

	//SetErrorCallBack(&pErrorProc);

	//ReadConfig();

	// Set a global prompt display switch
	//showPrompts = suppressPrompts ? FALSE : TRUE;

	exportSelected = (options & SCENE_EXPORT_SELECTED) ? true : false;


	if(!suppressPrompts)
	{
		//SiphonExporterOptionsDlgProc( GetActiveWindow(),WM_INITDIALOG, 0, 0);
		//DoPropertySheet( GetActiveWindow() );

		//CenterWindow( GetActiveWindow(),GetParent(GetActiveWindow()));
	//	DoPropertySheet(GetActiveWindow());
		
		//SetWindowLong( GetActiveWindow(), DWL_MSGRESULT, );

		if( DialogBoxParam(hInstance, 
				MAKEINTRESOURCE(IDD_PANEL), 
				GetActiveWindow(), 
				SiphonExporterOptionsDlgProc, (LPARAM)this) )
				return TRUE;
	}


	/*
	if(showPrompts) 
	{
		// Prompt the user with our dialogbox, and get all the options.
		if (!DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_PANEL),
			i->GetMAXHWnd(), IGameExporterOptionsDlgProc, (LPARAM)this)) {
			return 1;
		}
	}*/

	//curNode = 0;
	//ip->ProgressStart(_T("Exporting..."), TRUE, fn, NULL);

	ip->ProgressStart(_T("Processing Nodes"), TRUE, fn, NULL);
	
	scene = GetIGameInterface();
	
	IGameConversionManager * cm = GetConversionManager();
	cm->SetUserCoordSystem(Whacky);
	cm->SetCoordSystem( IGameConversionManager::IGAME_OGL );
//	pIgame->SetPropertyFile(_T("hello world"));
	scene->InitialiseIGame( exportSelected );
//	scene->SetStaticFrame(staticFrame);
/*	
	ExportSceneInfo();
	ExportMaterials();

*/
	//Export* pExport = new ExportStatic( scene, (TCHAR*)name, ip ); 
	bool bModel = true;
	ExportModel* pExport = new ExportModel( scene, (TCHAR*)name, ip );
	
	//if( !bModel )
	//	pExport->SetMapDivisions( renderDivisions, collisionDivisions );

	int nodePct = 100 / scene->GetTopLevelNodeCount();
	int percent = 0;

	for( int loop = 0; loop < scene->GetTopLevelNodeCount();loop++ )
	{
		ip->ProgressUpdate(percent);
		percent += nodePct;

		IGameNode* pNode = scene->GetTopLevelNode(loop);

		//check for selected state - we deal with targets in the light/camera section
		//if(pNode->IsTarget())
		//	continue;

		pExport->ExportNode( pNode );
		//ExportNodeInfo(pGameNode);
	}
	ip->ProgressEnd();

	ip->ProgressStart(_T("Final Processing"), TRUE, fn, NULL);
	pExport->Done();
	ip->ProgressEnd();

	delete pExport;

	scene->ReleaseIGame();
		
	//WriteConfig();

	return TRUE;
}


