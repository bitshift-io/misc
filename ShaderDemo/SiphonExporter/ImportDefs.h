#ifndef _SIPHON_IMPORTDEFS_
#define _SIPHON_IMPORTDEFS_

#define VERTICES		0
#define UVCOORDS		1
#define FACES			2
#define NORMALS			3
#define MATERIALTABLE	4
#define COLORS			5
#define WEIGHTS			6
#define ENDOFFILE		7
#define MESH_HEAD		8
#define MESH_END		9
#define TEXTUREREF		10
#define MATERIAL		11
#define VOLUME			12
#define COLLISION_HEAD	13
#define COLLISION_END	14
#define ODE_RECTANGLE	15
#define ODE_CYLINDER	16
#define ODE_SPHERE		17
#define SECTOR_RENDER_INFO		18
#define NEW_SECTOR_HEADER		19	
#define SECTOR_COLLISION_INFO	20
#define LIGHT_MAP				21
#define CUSTOM_HEAD				22
#define CUSTOM_END				23
#define CUSTOM_STRING			24
#define SPAWN					25
#define SOUND					26

#pragma pack(push,1)
struct Head
{
	unsigned int type; //the type of the body
	long size; //the size of the body
	unsigned int noElements;   //the number of elements eg. 400 vertices
};

struct sectorInfo
{
	float startValue;
	float cellGap;
	int cols;
};

struct odeSphere
{
	float radius;
	float position[3];
};

struct odeCylinder
{
	float radis;
	float length;
	float position[3];
	float rotation[3];
};

struct odeBox
{
	float width;
	float length;
	float height;
	float matrix[16];
	//float position[3];
	//float rotation[9]; //3x3 rotational matrix
};

struct sFace
{
	unsigned int idx[3];
};

struct sVertex
{
	float vert[3];
};

struct sUV
{
	float uv[2];
};

struct sMaterial
{
	sVertex specular;
	sVertex diffuse;
	float shine;
};

struct sSound
{
	char sound[256];
	float position[3];
	float radius;
};

struct sSpawn
{
	float matrix[16];
	//float position[3];
	//float rotation[3];
	int spawnId;
};

struct sCustData
{
	char* custStr; //a string representing some info, and telling us what the cust structs are
	void* custStruct; //an array of pointers to structs....
	int custStructSize; //no elements in array
};

struct sMaterialTable
{
	unsigned int startIndex;
	unsigned int numIndices;
	unsigned int numPrimitives;
	char material[256];
};
#pragma pop

#endif