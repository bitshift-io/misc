#ifndef _SIPHON_STATICEXPORT_
#define _SIPHON_STATICEXPORT_

#include <vector>
#include <list>
#include <set>

#include "ImportDefs.h"

struct snMaterialTable
{
	sMaterialTable matTable;
	IGameMaterial* material;
};

struct snMesh
{
	IGameMaterial* material;

	std::vector<snMaterialTable> materialTable;
	std::list<sFace> faces;
	std::vector<unsigned int> indices;
	std::vector<Point3> positions;
	std::vector<Point2> uvCoords;
	std::vector<Point2> uvCoords2; //light maps or what ever
	std::vector<Point3> normals;
	std::vector<Point3> colors;
};

class ExportWorld
{
public:
	ExportWorld( IGameScene* scene, TCHAR *name, Interface* ip );
	~ExportWorld();

	virtual void SetMapDivisions( int renderDivisions, int collisionDivisions );

	virtual bool Done();

	virtual bool ExportNode( IGameNode* pNode );
	virtual bool ExportMesh( IGameMesh* mesh, IGameNode* node );
	virtual bool AppendMesh( snMesh* sMesh, Tab<FaceEx*>& exFaces, IGameMesh* mesh );

	virtual bool DivideMesh( snMesh* mesh );
	virtual bool DivideCollisionMesh( snMesh* mesh );


	virtual bool ExportSkyBox( IGameMesh* mesh, IGameNode* node );
	virtual bool ExportRawMesh( snMesh* mesh );

	virtual bool ExportVolume( IGameNode* pNode );

	/**
	 * Export spawn point...
	 * everything needs a spawn point, even if just for camera
	 */
	virtual bool ExportSpawn( IGameNode* pNode );

	/**
	 * Export sound!
	 * this must be a sphere to get radius
	 */
	virtual bool ExportSound( IGameNode* pNode );

	/**
	 * Ques the ODE shapes into appropriate collision
	 * sectors
	 */
	virtual bool QueODEShapes( IGameNode* pNode );

	/**
	 * actually exports the ODE shapes to file
	 */
	virtual bool ExportODEShape( IGameNode* pNode );
	virtual bool ExportCollision( IGameMesh* mesh, IGameNode* node );

	virtual bool ExportBasicShape( INode* maxNode );

	virtual bool ExportMaterial( IGameMaterial* material ); 

	virtual char* FixPath( const char* const bmpName, char ext[3]  );
	//virtual bool ExportMaterial();

	/*
	 * This exports the given data is size size, and also adds in a header ot type
	 * type. Sub header info is info that goes after the header and before the data,
	 * this is used for example, to tell how many vertices there are.
	 */
	virtual bool ExportData( FILE* pFile, void* data, long size, unsigned int type, unsigned int noElements = 0 );

protected:
	IGameScene* scene;
	FILE* pFile;
	Interface* ip;

	struct RenderSector
	{
		std::list<snMesh*> meshes;
	};

	struct CollisionSector
	{
		snMesh collisionMesh;
		//a list of ODE shapes also?
		//std::list<IGameNode*> collisionShapes;
	};

	std::list<IGameNode*> collisionShapes;

	int renderSectorDivision;
	sectorInfo renderSectorInfo;
	std::vector< RenderSector > renderSectors;

	sectorInfo collisionSectorInfo;
	std::vector< CollisionSector > collisionSectors;

	std::list<snMesh*> meshes;
	snMesh collisionMesh;
};
/*
class ExportStatic : public Export
{
public:
	ExportStatic( IGameScene* scene, TCHAR *name, Interface* ip ) : Export( scene, name, ip ){}
};
*/
#endif