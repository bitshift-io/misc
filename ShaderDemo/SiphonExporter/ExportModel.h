#ifndef _SIPHON_EXPORTMODEL_
#define _SIPHON_EXPORTMODEL_

#include <vector>
#include <list>
#include <set>

#include "ImportDefs.h"

class ExportModel
{
public:
	ExportModel( IGameScene* scene, TCHAR *name, Interface* ip );
	~ExportModel();

	virtual bool Done();

	virtual bool ExportNode( IGameNode* pNode );
	virtual bool ExportMesh( IGameMesh* mesh, IGameNode* node );
	virtual bool AppendMesh( snMesh* sMesh, Tab<FaceEx*>& exFaces, IGameMesh* mesh, IGameMaterial* material );

	virtual bool ExportRawMesh( snMesh* mesh );

	virtual bool ExportMaterial( IGameMaterial* material, sMaterialTable& matTable ); 
	virtual char* FixPath( const char* const bmpName, char ext[3]  );

	/*
	 * This exports the given data is size size, and also adds in a header ot type
	 * type. Sub header info is info that goes after the header and before the data,
	 * this is used for example, to tell how many vertices there are.
	 */
	virtual bool ExportData( FILE* pFile, void* data, long size, unsigned int type, unsigned int noElements = 0 );

protected:
	IGameScene* scene;
	FILE* pFile;
	Interface* ip;

	//std::list<snMesh*> meshes;
	snMesh mesh;
};

#endif