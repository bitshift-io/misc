#ifndef _SIPHON_BATCH_
#define _SIPHON_BATCH_

/**
 * This class gets each node
 * goes through each mat id
 * and creates a new list of nodes
 * sorted by material,
 * batched together
 */
class Batch
{
public:
	virtual bool BatchNode( IGameNode* pNode );
protected:

};

#endif
