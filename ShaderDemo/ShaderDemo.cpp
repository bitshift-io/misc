#include "ShaderDemo.h"

//#include <SDL/sdl.h>

Engine eng;

enum
{
	MESH,
	BOX,
};

void drawBox()
{
	glBegin(GL_QUADS);
			// Front Face
			glNormal3f( 0.0f, 0.0f, 1.0f);					// Normal Pointing Towards Viewer
			glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);	// Point 1 (Front)
			glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);	// Point 2 (Front)
			glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  1.0f);	// Point 3 (Front)
			glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  1.0f);	// Point 4 (Front)
			// Back Face
			glNormal3f( 0.0f, 0.0f,-1.0f);					// Normal Pointing Away From Viewer
			glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f, -1.0f);	// Point 1 (Back)
			glNormal3f( 0.0f, 0.0f,-1.0f);
			glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);	// Point 2 (Back)
			glNormal3f( 0.0f, 0.0f,-1.0f);
			glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);	// Point 3 (Back)
			glNormal3f( 0.0f, 0.0f,-1.0f);
			glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f, -1.0f);	// Point 4 (Back)
			// Top Face
			glNormal3f( 0.0f, 1.0f, 0.0f);					// Normal Pointing Up
			glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);	// Point 1 (Top)
			glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f,  1.0f,  1.0f);	// Point 2 (Top)
			glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  1.0f,  1.0f);	// Point 3 (Top)
			glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);	// Point 4 (Top)
			// Bottom Face
			glNormal3f( 0.0f,-1.0f, 0.0f);					// Normal Pointing Down
			glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f, -1.0f, -1.0f);	// Point 1 (Bottom)
			glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f, -1.0f, -1.0f);	// Point 2 (Bottom)
			glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);	// Point 3 (Bottom)
			glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);	// Point 4 (Bottom)
			// Right face
			glNormal3f( 1.0f, 0.0f, 0.0f);					// Normal Pointing Right
			glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f, -1.0f, -1.0f);	// Point 1 (Right)
			glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);	// Point 2 (Right)
			glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  1.0f);	// Point 3 (Right)
			glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);	// Point 4 (Right)
			// Left Face
			glNormal3f(-1.0f, 0.0f, 0.0f);					// Normal Pointing Left
			glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f, -1.0f);	// Point 1 (Left)
			glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);	// Point 2 (Left)
			glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  1.0f);	// Point 3 (Left)
			glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);	// Point 4 (Left)
		glEnd();
}

int main( int argc, char** argv ) 
{
	int shape = MESH;
	char fps[256] = "\0";

	float yaw = 20.0, pitch = 0, roll = 0;
	float depth = 55;

	float light_rotation = 0;

	if( !eng.Create() )
		return 1;

	SFont font;
	font.CreateFont("font.dds");

	Matrix4 matLight;
	Mesh* m = eng.GetFileMgr()->LoadMesh("map.SML");

	RenderTree render;
	//m->Insert( render );

//	for( int i = 0; i < 20; ++i )
		m->Insert( render ); //test
	//render.Insert(

//	Material* mat = eng.GetFileMgr()->LoadMaterial("box.SMT");
//	mat->Bind();

	GLfloat LightPosition[]= { depth, 0.0f, 0.0f, 1.0f };
	GLfloat LightAmbient[]= { 0.5f, 0.5f, 0.5f, 1.0f };
	GLfloat LightDiffuse[]= { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat LightSpecular[]= { 1.0f, 1.0f, 1.0f, 1.0f };

	glLightfv(GL_LIGHT0, GL_SPECULAR, LightSpecular);
	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION,LightPosition);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	
//	glEnable(GL_TEXTURE_2D);							// Enable Texture Mapping
//	glShadeModel(GL_SMOOTH);

	bool bCont = true;
	while( !eng.ShouldQuit() )
	{
		eng.CheckEvent();

		if( eng.GetInput()->KeyDown( SDLK_ESCAPE ) )
			eng.Quit();

		if( eng.GetInput()->KeyDown( SDLK_a ) )
			yaw += 0.1f;
		if( eng.GetInput()->KeyDown( SDLK_z ) )
			yaw -= 0.1f;

		if( eng.GetInput()->KeyDown( SDLK_s ) )
			pitch += 0.1f;
		if( eng.GetInput()->KeyDown( SDLK_x ) )
			pitch -= 0.1f;
		else
			pitch += 0.05f;

		if( eng.GetInput()->KeyDown( SDLK_n ) )
			depth += 0.04f;
		if( eng.GetInput()->KeyDown( SDLK_m ) )
			depth -= 0.04f;

		light_rotation += 0.001f;
		if(  eng.GetInput()->KeyDown( SDLK_q ) )
			light_rotation += 0.01f;
		if(  eng.GetInput()->KeyDown( SDLK_w ) )
			light_rotation -= 0.01f;

		if( eng.GetInput()->KeyPressed( SDLK_p ) )
		{
			shape++;
			if( shape >= 2 )
				shape = 0;
		}

		glLoadIdentity();
		matLight.LoadIdentity();
		matLight.Translate( depth, 0, 0 );
		matLight.RotateY( light_rotation );
		Vector3 rot = matLight * Vector3(LightPosition[0], LightPosition[1], LightPosition[2]);
		float pos[4] = { rot[0], rot[1], rot[2], 1.0 };

		glLightfv(GL_LIGHT0, GL_POSITION,pos);

		eng.GetDevice()->BeginRender();
		{
			glLoadIdentity();
			gluLookAt(0, 0, depth,     0, 0, 0,     0, 1, 0); //look at 0,0,0

			//glTranslatef( 0, 0, depth);
			glRotatef( yaw, 1, 0, 0 );
			glRotatef( pitch, 0, 1, 0 );
			glRotatef( roll, 0, 0, 1 );

			switch(shape)
			{
			case BOX:
				drawBox();
				break;
			case MESH:
			default:
				//for( int i = 0; i < 100; ++i )
				//m->Render();
				//printf( "%i\n", render.Render());
				font.Print( 10, 10, "Polys: %i\nFPS: %i", render.Render(), eng.GetDevice()->GetFPS() ); 
				break;
			};
			//glPopMatrix();

			font.Render();
			font.Update();
		}
		eng.GetDevice()->EndRender();
	}

	return 0;
}