//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "SiphonGL.h"

//--------------------------------------------------------------------------------------

Light::Light() : position(0, 0, 0)
{
	//some of this should be part of the light manager
	glEnable(GL_LIGHTING);
	//glEnable(GL_RESCALE_NORMAL); //GL_NORMALIZE);
/*
	float mAmbient[] = { 0.0, 0.0, 0.0, 1 };
	glLightModelfv( GL_LIGHT_MODEL_AMBIENT, mAmbient );
	glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, 1 );

	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_DIFFUSE);
*/
	glEnable(GL_LIGHT1);

	//float position[] = { 100, 100, 100 };
	float ambient[] = { 0, 0, 0, 1 };
	float specular[] = { 0, 0, 0, 1 };
	float diffuse[] = { 0.5, 0.5, 0.5, 1 };

	//glLightfv(GL_LIGHT0, GL_POSITION, position );
	glLightfv(GL_LIGHT1, GL_AMBIENT, ambient );
	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse );
	glLightfv(GL_LIGHT1, GL_SPECULAR, specular );
/*
	glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.0f );
	glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.0 );
	glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.0 );
	*/
	float matSpecular[] = { 0, 0, 0, 1 };
	float matEmissive[] = { 0, 0, 0, 1 };
	float matDiffuse[] = { 0.5, 0.5, 0.5, 1 };

	glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
	glMaterialfv(GL_FRONT, GL_EMISSION, matEmissive);
	glMaterialf(GL_FRONT, GL_SHININESS, 10.0f );
//	glColor4f( 0.2, 0.2, 0.2, 1 );



//	Bind();
}

//--------------------------------------------------------------------------------------

void Light::Bind()
{
	 //the light number will ge given by light manager
	float position[] = { 250, 50, 0, 1 };
	glLightfv(GL_LIGHT1, GL_POSITION, position ); //.GetPointer() );
}

//--------------------------------------------------------------------------------------

void Light::SetPosition( Vector3& position )
{
	float pos[4] = { position[0], position[1], position[2], 1 };
	glLightfv(GL_LIGHT1, GL_POSITION, pos );
}