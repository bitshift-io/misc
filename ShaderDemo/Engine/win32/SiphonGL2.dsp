# Microsoft Developer Studio Project File - Name="SiphonGL2" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=SiphonGL2 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "SiphonGL2.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "SiphonGL2.mak" CFG="SiphonGL2 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "SiphonGL2 - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "SiphonGL2 - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "SiphonGL2 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /Ob2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0xc09 /d "NDEBUG"
# ADD RSC /l 0xc09 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\..\SiphonGL.lib"

!ELSEIF  "$(CFG)" == "SiphonGL2 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MD /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD BASE RSC /l 0xc09 /d "_DEBUG"
# ADD RSC /l 0xc09 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\..\SiphonGL.lib"

!ENDIF 

# Begin Target

# Name "SiphonGL2 - Win32 Release"
# Name "SiphonGL2 - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\Camera.cpp
# End Source File
# Begin Source File

SOURCE=..\DDS.cpp
# End Source File
# Begin Source File

SOURCE=..\Device.cpp
# End Source File
# Begin Source File

SOURCE=..\Engine.cpp
# End Source File
# Begin Source File

SOURCE=..\FileMgr.cpp
# End Source File
# Begin Source File

SOURCE=..\Font.cpp
# End Source File
# Begin Source File

SOURCE=..\Input.cpp
# End Source File
# Begin Source File

SOURCE=..\Light.cpp
# End Source File
# Begin Source File

SOURCE=..\Material.cpp
# End Source File
# Begin Source File

SOURCE=..\MaterialDefs.cpp
# End Source File
# Begin Source File

SOURCE=..\Math3D.cpp
# End Source File
# Begin Source File

SOURCE=..\Mesh.cpp
# End Source File
# Begin Source File

SOURCE=..\mmgr\mmgr.cpp
# End Source File
# Begin Source File

SOURCE=..\RenderTree.cpp
# End Source File
# Begin Source File

SOURCE=..\Shader.cpp
# End Source File
# Begin Source File

SOURCE=..\Texture.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\Camera.h
# End Source File
# Begin Source File

SOURCE=..\Config.h
# End Source File
# Begin Source File

SOURCE=..\DDS.h
# End Source File
# Begin Source File

SOURCE=..\Device.h
# End Source File
# Begin Source File

SOURCE=..\Engine.h
# End Source File
# Begin Source File

SOURCE=..\FileMgr.h
# End Source File
# Begin Source File

SOURCE=..\Font.h
# End Source File
# Begin Source File

SOURCE=..\glext.h
# End Source File
# Begin Source File

SOURCE=..\ImportDefs.h
# End Source File
# Begin Source File

SOURCE=..\Input.h
# End Source File
# Begin Source File

SOURCE=..\Light.h
# End Source File
# Begin Source File

SOURCE=..\Material.h
# End Source File
# Begin Source File

SOURCE=..\MaterialDefs.h
# End Source File
# Begin Source File

SOURCE=..\Math3D.h
# End Source File
# Begin Source File

SOURCE=..\Mesh.h
# End Source File
# Begin Source File

SOURCE=..\mmgr\mmgr.h
# End Source File
# Begin Source File

SOURCE=..\RenderTree.h
# End Source File
# Begin Source File

SOURCE=..\Shader.h
# End Source File
# Begin Source File

SOURCE=..\SiphonGL.h
# End Source File
# Begin Source File

SOURCE=..\Texture.h
# End Source File
# End Group
# End Target
# End Project
