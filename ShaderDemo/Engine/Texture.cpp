//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "SiphonGL.h"

//--------------------------------------------------------------------------------------

Texture::~Texture()
{
//	Release();
}

//--------------------------------------------------------------------------------------

bool Texture::Load( const char* file, bool bMipMap )
{
	this->file = std::string(file);
	std::string path = Engine::GetInstance()->GetDataPath() + std::string( file );

	DDS dds;
	if( dds.Load( path.c_str(), bMipMap ) )
	{
		texture = dds.GetTexture();
		unsigned int bpp;
		dds.GetInfo( width, height, bpp, blendType );
		return true;
	}
	else
	{
		printf("ERROR: Failed to load DDS texture: %s\n", file );
	}

	return false;
}

//--------------------------------------------------------------------------------------

void Texture::ShaderBind( GLint handle )
{
	glUniform1iARB( handle, texture );
}

//--------------------------------------------------------------------------------------

void Texture::Bind( int texUnit )
{
	if( blendType == BLEND )
	{
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glAlphaFunc(GL_GREATER, 0.0);
		
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);
	}
	else if( blendType == MASK )
	{
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GREATER, 0.5);
		glDisable(GL_CULL_FACE);
	}
	else
	{
		glDisable(GL_BLEND);
		glEnable(GL_CULL_FACE);
		glDisable(GL_ALPHA_TEST);
	}

	glActiveTextureARB( GL_TEXTURE0_ARB + texUnit );
	glBindTexture( GL_TEXTURE_2D, texture );
}

//--------------------------------------------------------------------------------------

const char* Texture::GetName()
{
	return file.c_str();
}