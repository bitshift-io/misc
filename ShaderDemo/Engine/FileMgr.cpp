//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "SiphonGL.h"

//--------------------------------------------------------------------------------------

FileMgr::~FileMgr()
{
	//free meshes
	std::vector<Mesh*>::iterator meshIt; 
	for( meshIt = meshes.begin(); meshIt != meshes.end(); ++meshIt )
	{
		SAFE_DELETE(*meshIt);
	}

	//free textures
	std::vector<Texture*>::iterator texIt;
	for( texIt = textures.begin(); texIt != textures.end(); ++texIt )
	{
		SAFE_DELETE(*texIt);
	}

	//free materials
	std::vector<Material*>::iterator matIt; 
	for( matIt = materials.begin(); matIt != materials.end(); ++matIt )
	{
		SAFE_DELETE(*matIt);
	}

	//free shaders
	std::vector<Shader*>::iterator shadIt; 
	for( shadIt = shaders.begin(); shadIt != shaders.end(); ++shadIt )
	{
		SAFE_DELETE(*shadIt);
	}
}

//--------------------------------------------------------------------------------------

Texture* FileMgr::LoadTexture( const char* file, bool bMipMap )
{
	std::vector<Texture*>::iterator texIt; 
	for( texIt = textures.begin(); texIt != textures.end(); ++texIt )
	{
		if( strcmp( (*texIt)->GetName(),  file ) == 0 )
		{
			return (*texIt);
		}
	}

	Texture* tex = new Texture();
	if( !tex->Load( file, bMipMap ) )
	{
		SAFE_DELETE( tex );
		return 0;
	}

	textures.push_back(tex);
	return tex;
}

//--------------------------------------------------------------------------------------

Shader* FileMgr::LoadShader( const char* vertex, const char* fragment )
{
	std::vector<Shader*>::iterator shadIt; 
	for( shadIt = shaders.begin(); shadIt != shaders.end(); ++shadIt )
	{
		if( strcmp( (*shadIt)->GetVertexName(),  vertex ) == 0 &&
			strcmp( (*shadIt)->GetFragmentName(),  fragment ) == 0 )
		{
			return (*shadIt);
		}
	}

	Shader* shad = new Shader();
	if( !shad->Load( vertex, fragment ) )
	{
		SAFE_DELETE( shad );
		return 0;
	}

	shaders.push_back(shad);
	return shad;
}

//--------------------------------------------------------------------------------------

Material* FileMgr::LoadMaterial( const char* file )
{
	std::vector<Material*>::iterator matIt; 
	for( matIt = materials.begin(); matIt != materials.end(); ++matIt )
	{
		if( strcmp( (*matIt)->GetName(),  file ) == 0 )
		{
			return (*matIt);
		}
	}

	Material* mat = new Material();
	if( !mat->Load( file ) )
	{
		SAFE_DELETE( mat );
		return 0;
	}
	
	materials.push_back(mat);

	return mat;
}

//--------------------------------------------------------------------------------------

Mesh* FileMgr::LoadMesh( const char* file )
{
	std::vector<Mesh*>::iterator meshIt; 
	for( meshIt = meshes.begin(); meshIt != meshes.end(); ++meshIt )
	{
		if( strcmp( (*meshIt)->GetName(),  file ) == 0 )
		{
			return (*meshIt);
		}
	}

	Mesh* mesh = new Mesh();
	if( !mesh->Load( file ) )
	{
		SAFE_DELETE( mesh );
		return 0;
	}

	meshes.push_back(mesh);
	return mesh;
}

