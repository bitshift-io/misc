//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_MESH_
#define _SIPHON_MESH_

#include "SiphonGL.h"
#include "MaterialDefs.h"
#include "ImportDefs.h"

/**
 * a group of polygons really
 * polygons can have normals
 * uvw's etc..
 */
class Mesh
{
public:

	enum
	{
		DYNAMIC = 1,
		STATIC = 2,
	};

	Mesh();
	~Mesh();

	/**
	 * Compile this mesh's VBO's
	 */
	bool Build();

	void SetType( unsigned int flags );

	/**
	 * renders a part of this mesh,
	 * the -1 indicates it hsould just render all
	 */
	int Render( int matTableNum = -1 );
	int GetNumMaterialTables();

	/**
	 * insert all materials into the render tree
	 */
	void Insert( RenderTree& tree );

	/**
	 * load a mesh file
	 */
	bool Load( const char* file );
	const char* GetName();

protected:
	
	unsigned int noVertices;
	unsigned int noIndices;
	unsigned int noPolygons;

	Color* colors;
	Vector3* positions;
	Vector3* normals;
	Vector3* tangents;
	UVCoord* uvCoords;
	unsigned int* indices;

	//the following are VBO's, which quadrupel my frames :)
	GLuint vboPositions;
	GLuint vboTexCoords;
	GLuint vboIndices;
	GLuint vboNormals;
	GLuint vboColors;

	unsigned int flags;

	//our table of material to index count
	struct MaterialTable
	{
		unsigned int startIndex;
		unsigned int numIndices;
		unsigned int numPrimitives;
		Material* pMaterial;
	};
	std::vector< MaterialTable > attributeTable;

	/**
	 * The following are functions for reading in the file
	 */
	bool ReadChunk( FILE* pFile );
	bool ReadVertices( FILE* pFile, Head& header );
	bool ReadFaces( FILE* pFile, Head& header );
	bool ReadUVCoords( FILE* pFile, Head& header );
	bool ReadNormals( FILE* pFile, Head& header );
	bool ReadColors( FILE* pFile, Head& header );
	bool ReadMaterialTable( FILE* pFile, Head& header );
	bool ReadHead( FILE* pFile, Head& header );

	std::string file;
};

#endif