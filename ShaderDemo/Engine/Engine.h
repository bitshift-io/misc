//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_ENGINE_
#define _SIPHON_ENGINE_

#include "SiphonGL.h"

/**
 * Whats an engine? an instance of my engine
 * It is the uber interface to all other managers
 */
class Engine
{
public:

	/**
	 * Init and clean up
	 */
	Engine();
	~Engine();

	/**
	 * Create/initilise an engine with default devices/managers
	 * and the like
	 */
	virtual bool Create();

	/**
	 * check for special events
	 */
	void CheckEvent();

	/**
	 * pack up and go home!
	 * this will tell us when
	 */
	bool ShouldQuit();

	/**
	 * quit!
	 */
	void Quit();

	/**
	 * Set'ots and get'ors
	 * for managers
	 * getors should be inline!
	 */
	void SetDevice( Device* pDevice );
	Device* GetDevice();

	void SetInput( Input* pInput );
	Input* GetInput();

	void SetFileMgr( FileMgr* pFileMgr );
	FileMgr* GetFileMgr();

	static Engine* GetInstance();

	const std::string& GetDataPath();

protected:
	Device* pDevice;
	Input* pInput;
	FileMgr* pFileMgr;

	static Engine* pEngine;

	std::string dataPath;

	bool bExit;
};

#endif 