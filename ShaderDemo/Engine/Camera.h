#ifndef _SIPHON_CAMERA_
#define _SIPHON_CAMERA_

#include "Math3D.h"

struct BOUNDS
{
	Vector3 points[8]; //8 points to our camera's bounds, used to determine what chunks of map to draw
	Plane planes[6];
};

class Camera
{
public:
	Camera();

	void Update();

	void FreeCam();

	void Rotate( float angle, float x, float y, float z );
	void LookAt();

	Vector3& GetPosition();
	Vector3& GetRotation();

	void SetPosition( float x, float y, float z );

	void SetFreeCam( bool bFreeCam );

	void SetLookAt( Vector3& view, Vector3& up, Vector3& right );

	void Transform();

	void SetupFrustum();
	//Vector3 Plane3Intersect( Plane& p1, Plane& p2, Plane& p3 );

	/**
	 * get the transformed version of the cameras
	 * frustrum
	 */
	BOUNDS GetTransformedBounds();

protected:
	Vector3 rotation;
	Vector3 position;
	Vector3 view;
	Vector3 up;
	Vector3 right;

	BOUNDS frustum;

	Matrix4 projection;
	Matrix4 world;
	Matrix4 transform;

	bool bFreeCam;
};

#endif

