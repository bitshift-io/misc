//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_DEVICE_
#define _SIPHON_DEVICE_

#include "SiphonGL.h"
#include "glext.h"

#define BUFFER_OFFSET(i) ((char *)0 + (i))

//MULTI TEXTURE
#define GL_ACTIVE_TEXTURE_ARB               0x84E0
#define GL_CLIENT_ACTIVE_TEXTURE_ARB        0x84E1
#define GL_MAX_TEXTURE_UNITS_ARB            0x84E2
#define GL_TEXTURE0_ARB                     0x84C0
#define GL_TEXTURE1_ARB                     0x84C1
#define GL_TEXTURE2_ARB                     0x84C2
#define GL_TEXTURE3_ARB                     0x84C3
#define GL_TEXTURE4_ARB                     0x84C4
#define GL_TEXTURE5_ARB                     0x84C5
#define GL_TEXTURE6_ARB                     0x84C6
#define GL_TEXTURE7_ARB                     0x84C7
#define GL_TEXTURE8_ARB                     0x84C8
#define GL_TEXTURE9_ARB                     0x84C9
#define GL_TEXTURE10_ARB                    0x84CA
#define GL_TEXTURE11_ARB                    0x84CB
#define GL_TEXTURE12_ARB                    0x84CC
#define GL_TEXTURE13_ARB                    0x84CD
#define GL_TEXTURE14_ARB                    0x84CE
#define GL_TEXTURE15_ARB                    0x84CF
#define GL_TEXTURE16_ARB                    0x84D0
#define GL_TEXTURE17_ARB                    0x84D1
#define GL_TEXTURE18_ARB                    0x84D2
#define GL_TEXTURE19_ARB                    0x84D3
#define GL_TEXTURE20_ARB                    0x84D4
#define GL_TEXTURE21_ARB                    0x84D5
#define GL_TEXTURE22_ARB                    0x84D6
#define GL_TEXTURE23_ARB                    0x84D7
#define GL_TEXTURE24_ARB                    0x84D8
#define GL_TEXTURE25_ARB                    0x84D9
#define GL_TEXTURE26_ARB                    0x84DA
#define GL_TEXTURE27_ARB                    0x84DB
#define GL_TEXTURE28_ARB                    0x84DC
#define GL_TEXTURE29_ARB                    0x84DD
#define GL_TEXTURE30_ARB                    0x84DE
#define GL_TEXTURE31_ARB                    0x84DF

typedef void (APIENTRY * PFNGLACTIVETEXTUREARBPROC) (GLenum target);
typedef void (APIENTRY * PFNGLCLIENTACTIVETEXTUREARBPROC) (GLenum target);

extern PFNGLACTIVETEXTUREARBPROC glActiveTextureARB;
extern PFNGLCLIENTACTIVETEXTUREARBPROC glClientActiveTexture;

//DRAW RANGE ELEMENTS
typedef void (APIENTRY * PFNGLDRAWRANGEELEMENTSPROC) ( GLenum mode, GLuint start,
 	GLuint end, GLsizei count, GLenum type, const GLvoid *indices );
extern PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElements;

//VERTEX BUFFERS
#define GL_ARRAY_BUFFER_ARB 0x8892
#define GL_STATIC_DRAW_ARB 0x88E4
#define GL_ELEMENT_ARRAY_BUFFER_ARB 0x8893

typedef void (APIENTRY * PFNGLBINDBUFFERARBPROC) (GLenum target, GLuint buffer);
typedef void (APIENTRY * PFNGLDELETEBUFFERSARBPROC) (GLsizei n, const GLuint *buffers);
typedef void (APIENTRY * PFNGLGENBUFFERSARBPROC) (GLsizei n, GLuint *buffers);
typedef void (APIENTRY * PFNGLBUFFERDATAARBPROC) (GLenum target, int size, const GLvoid *data, GLenum usage);
typedef void (APIENTRY * PFNGLDELETEBUFFERSARBPROC) (GLsizei n, const GLuint *buffers);

extern PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB;
extern PFNGLBINDBUFFERARBPROC glBindBufferARB;
extern PFNGLGENBUFFERSARBPROC glGenBuffersARB;
extern PFNGLBUFFERDATAARBPROC glBufferDataARB;

//SHADERS

typedef char GLcharARB;
typedef unsigned int GLhandleARB;

#define FRAGMENT_SHADER_ARB 0x8B30
#define MAX_FRAGMENT_UNIFORM_COMPONENTS_ARB 0x8B49
#define MAX_TEXTURE_COORDS_ARB 0x8871
#define MAX_TEXTURE_IMAGE_UNITS_ARB 0x8872
#define OBJECT_TYPE_ARB	0x8B4E
#define OBJECT_SUBTYPE_ARB 0x8B4F
#define SHADER_OBJECT_ARB 0x8B48

#define GL_PROGRAM_OBJECT_ARB             0x8B40
#define GL_SHADER_OBJECT_ARB              0x8B48
#define GL_OBJECT_TYPE_ARB                0x8B4E
#define GL_OBJECT_SUBTYPE_ARB             0x8B4F
#define GL_FLOAT_VEC2_ARB                 0x8B50
#define GL_FLOAT_VEC3_ARB                 0x8B51
#define GL_FLOAT_VEC4_ARB                 0x8B52
#define GL_INT_VEC2_ARB                   0x8B53
#define GL_INT_VEC3_ARB                   0x8B54
#define GL_INT_VEC4_ARB                   0x8B55
#define GL_BOOL_ARB                       0x8B56
#define GL_BOOL_VEC2_ARB                  0x8B57
#define GL_BOOL_VEC3_ARB                  0x8B58
#define GL_BOOL_VEC4_ARB                  0x8B59
#define GL_FLOAT_MAT2_ARB                 0x8B5A
#define GL_FLOAT_MAT3_ARB                 0x8B5B
#define GL_FLOAT_MAT4_ARB                 0x8B5C
#define GL_OBJECT_DELETE_STATUS_ARB       0x8B80
#define GL_OBJECT_COMPILE_STATUS_ARB      0x8B81
#define GL_OBJECT_LINK_STATUS_ARB         0x8B82
#define GL_OBJECT_VALIDATE_STATUS_ARB     0x8B83
#define GL_OBJECT_INFO_LOG_LENGTH_ARB     0x8B84
#define GL_OBJECT_ATTACHED_OBJECTS_ARB    0x8B85
#define GL_OBJECT_ACTIVE_UNIFORMS_ARB     0x8B86
#define GL_OBJECT_ACTIVE_UNIFORM_MAX_LENGTH_ARB 0x8B87
#define GL_OBJECT_SHADER_SOURCE_LENGTH_ARB 0x8B88

#define GL_VERTEX_SHADER_ARB              0x8B31
#define GL_FRAGMENT_SHADER_ARB            0x8B30

typedef GLhandleARB (APIENTRY * PFNGLCREATESHADEROBJECTARBPROC) (GLenum shaderType);
extern PFNGLCREATESHADEROBJECTARBPROC glCreateShaderObjectARB;

typedef void (APIENTRY * PFNGLUSEPROGRAMOBJECTARBPROC) (GLhandleARB programObj);
extern PFNGLCREATESHADEROBJECTARBPROC glUseProgramObjectARB;

typedef void (APIENTRY * PFNGLSHADERSOURCEARBPROC) (GLhandleARB, GLsizei, const GLcharARB* *, const GLint *);
extern PFNGLSHADERSOURCEARBPROC glShaderSourceARB;

typedef void (APIENTRY * PFNGLCOMPILESHADERARBPROC) (GLhandleARB);
extern PFNGLCOMPILESHADERARBPROC glCompileShaderARB;

typedef void (APIENTRY * PFNGLGETOBJECTPARAMETERIVARBPROC) (GLhandleARB, GLenum, GLint *);
extern PFNGLGETOBJECTPARAMETERIVARBPROC glGetObjectParameterivARB;

typedef void (APIENTRY * PFNGLATTACHOBJECTARBPROC) (GLhandleARB, GLhandleARB);
extern PFNGLATTACHOBJECTARBPROC glAttachObjectARB;

typedef GLhandleARB (APIENTRY * PFNGLCREATEPROGRAMOBJECTARBPROC) (void);
extern PFNGLCREATEPROGRAMOBJECTARBPROC glCreateProgramObjectARB;

typedef void (APIENTRY * PFNGLGETINFOLOGARBPROC) (GLhandleARB, GLsizei, GLsizei *, GLcharARB *);
extern PFNGLGETINFOLOGARBPROC glGetInfoLogARB;

typedef void (APIENTRY * PFNGLLINKPROGRAMARBPROC) (GLhandleARB);
extern PFNGLLINKPROGRAMARBPROC glLinkProgramARB;

typedef void (APIENTRY * PFNGLDELETEOBJECTARBPROC) (GLhandleARB);
extern PFNGLDELETEOBJECTARBPROC glDeleteObjectARB;

typedef GLint (APIENTRY * PFNGLGETUNIFORMLOCATIONARBPROC) (GLhandleARB programObj, const GLcharARB *name);
extern PFNGLGETUNIFORMLOCATIONARBPROC glGetUniformLocationARB;

typedef void (APIENTRY * PFNGLUNIFORM1IARBPROC) (GLint location, GLint v0);
extern PFNGLUNIFORM1IARBPROC glUniform1iARB;


/*
void VertexAttrib1fARB(uint index, float v0)
void VertexAttrib1sARB(uint index, short v0)
void VertexAttrib1dARB(uint index, double v0)
void VertexAttrib2fARB(uint index, float v0, float v1)
void VertexAttrib2sARB(uint index, short v0, short v1)void        VertexAttrib2dARB(uint index, double v0, double v1)
void VertexAttrib3fARB(uint index, float v0, float v1, float v2)void        VertexAttrib3sARB(uint index, short v0, short v1, short v2)
void VertexAttrib3dARB(uint index, double v0, double v1, double v2)
void VertexAttrib4fARB(uint index, float v0, float v1, float v2, float v3)
void VertexAttrib4sARB(uint index, short v0, short v1, short v2, short v3)
void VertexAttrib4dARB(uint index, double v0, double v1, double v2, double v3)
void VertexAttrib4NubARB(uint index, ubyte x, ubyte y, ubyte z, ubyte w)
void VertexAttrib1fvARB(uint index, const float *v)
void VertexAttrib1svARB(uint index, const short *v)
void VertexAttrib1dvARB(uint index, const double *v)
void VertexAttrib2fvARB(uint index, const float *v)
void VertexAttrib2svARB(uint index, const short *v)
void VertexAttrib2dvARB(uint index, const double *v)
void VertexAttrib3fvARB(uint index, const float *v)
void VertexAttrib3svARB(uint index, const short *v)
void VertexAttrib3dvARB(uint index, const double *v)
void VertexAttrib4fvARB(uint index, const float *v)
void VertexAttrib4svARB(uint index, const short *v)
void VertexAttrib4dvARB(uint index, const double *v)
void VertexAttrib4ivARB(uint index, const int *v)
void VertexAttrib4bvARB(uint index, const byte *v)
void VertexAttrib4ubvARB(uint index, const ubyte *v)
void VertexAttrib4usvARB(uint index, const ushort *v)
void VertexAttrib4uivARB(uint index, const uint *v)
void VertexAttrib4NbvARB(uint index, const byte *v)
void VertexAttrib4NsvARB(uint index, const short *v)
void VertexAttrib4NivARB(uint index, const int *v)
void VertexAttrib4NubvARB(uint index, const ubyte *v)
void VertexAttrib4NusvARB(uint index, const ushort *v)
void VertexAttrib4NuivARB(uint index, const uint *v)
void VertexAttribPointerARB(uint index, int size, enum type, boolean normalized,
                                   sizei stride, const void *pointer)void        EnableVertexAttribArrayARB(uint index)void        DisableVertexAttribArrayARB(uint index)void        BindAttribLocationARB(handleARB programObj, uint index, const charARB *name)void        GetActiveAttribARB(handleARB programObj, uint index, sizei maxLength,                               sizei *length, int *size, enum *type, charARB *name)GLint       GetAttribLocationARB(handle programObj, const charARB *name)void        GetVertexAttribdvARB(uint index, enum pname, double *params)
void GetVertexAttribfvARB(uint index, enum pname, float *params)
void GetVertexAttribivARB(uint index, enum pname, int *params)
void GetVertexAttribPointervARB(uint index, enum pname, void **pointer)
*/

/**
 * This is the OpenGL device,
 * this class is responsible for
 * device related specifics,
 * it is also responsible for
 * windows/cursor management 
 * as these things are closely
 * attached/related
 */
class Device
{
public:

	Device();
	~Device();
	/**
	 * Creates the OpenGL device, and window
	 * and intilized them
	 */

#ifdef DEBUG
	bool Create( int width = 640, int height = 480, bool fullscreen = false, char* title = "SiphonGL" );
#else
	bool Create( int width = 1024, int height = 768, bool fullscreen = true, char* title = "SiphonGL" );
#endif

	void Destroy();

	bool CreateDevice();

	/**
	 * OS/Window specific
	 */
	void SetWindowTitle( const char* title );
	void ToggleFullScreen();
	void SetCursor( bool visible );

	void ResizeDevice( int width, int height );
	
	/**
	 * Device/GL specific
	 */
	int ExtensionSupported( const char* extension );
	void GetDeviceSize( int& width, int& height );

	void BeginRender();
	void EndRender();

	bool VBOSupported();
	bool ShaderSupported();

	unsigned int GetFPS();


protected:
	SDL_Surface* windowHandle;

	unsigned int framesThisSecond;
	unsigned int fps;
	unsigned int ticks;

	float farPlane;
	int width;
	int height;

	bool bVBOSupported;
	bool bShadersSupported;
};

#endif