//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_MATERIALDEFS_
#define _SIPHON_MATERIALDEFS_

/**
 * Whats a resource? a file
 * that needs to be loaded
 * this templated version,
 * allows us to have only 1 bound
 * at a time
 */
template< class T > class Resource
{
public:
	virtual void Bind() = 0;
	virtual bool Load( const char* file ) = 0;

protected:
	static T* boundResource;
	const char* file;
};

/**
 * Whats a resource? a file,
 * so a templated resource,
 * allows for a manager!
 * a manged resource means when you go to 
 * load it, it returns a pointer
 * to the loaded version
 * when destroy and references = 0, should clear self from resourcemap
 */
template< class T > class ResourceManaged : public Resource<T>
{
public:

protected:
	virtual Release();
	virtual Acquire( const char* file );

	unsigned int references;
	static std::map< const char*, T* > resourceMap; //loaded file list
};



class Color
{
public:
	enum COLOR
	{
		R = 0,
		G = 1,
		B = 2,
		A = 3
	};

	Color()
	{

	}

	Color( float r, float g, float b, float a = 1.0f )
	{
		color[0] = r;
		color[1] = g;
		color[2] = b;
		color[3] = a;
	}

	float& operator[]( const int index )
	{
		return color[index];
	}

	float* GetPointer()
	{
		return color;
	}

protected:
	float color[4];
};

class UVCoord
{
public:
	enum UVCOORDINATE
	{
		U = 0,
		V = 1
	};

	float& operator[]( const int index )
	{
		return uvCoord[index];
	}

protected:
	float uvCoord[2];
};

#endif
