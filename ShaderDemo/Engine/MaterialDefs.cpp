//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "SiphonGL.h"

template<class T> T* Resource<T>::boundResource = 0;

template<class T> ResourceManaged<T>::Acquire( const char* file )
{
	++references;
}

template<class T> ResourceManaged<T>::Release()
{
	--references;

	if( references <= 0 )
	{
		resourceMap.erase( file );

		if( resourceMap.size() <= 0 )
			printf("Resources Released\t\t\t[OK]\n");
	}
}
