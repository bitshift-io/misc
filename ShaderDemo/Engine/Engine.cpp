//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "SiphonGL.h"
#include "Engine.h"

Engine* Engine::pEngine = 0;

//--------------------------------------------------------------------------------------

Engine::Engine() : pDevice(0), pInput(0), pFileMgr(0), bExit(false), dataPath("data/")
{
	printf("---------------------------------------------\n");
	printf("\t\tSiphonGL 2.0\n");
	printf("\t(C) 2004 Fabian Mathews\n");
	printf("---------------------------------------------\n\n");

	if( pEngine )
	{
		SAFE_DELETE( pEngine );
	}

	pEngine = this;
}

//--------------------------------------------------------------------------------------

Engine::~Engine()
{
	SAFE_DELETE( pDevice );
	SAFE_DELETE( pInput );
	SAFE_DELETE( pFileMgr );
}

//--------------------------------------------------------------------------------------

bool Engine::Create()
{
	if( !pDevice )
	{
		pDevice = new Device();

		if( !pDevice )
			return false;

		if( !pDevice->Create() )
			return false;
	}

	if( !pInput )
	{
		pInput = new Input();

		if( !pInput )
			return false;
	}

	if( !pFileMgr )
	{
		pFileMgr = new FileMgr();

		if( !pFileMgr )
			return false;
	}

	return true;
}

//--------------------------------------------------------------------------------------

void Engine::SetDevice( Device* pDevice )
{
	this->pDevice = pDevice;
}

//--------------------------------------------------------------------------------------

Device* Engine::GetDevice()
{
	return pDevice;
}

//--------------------------------------------------------------------------------------

void Engine::SetInput( Input* pInput )
{
	this->pInput = pInput;
}

//--------------------------------------------------------------------------------------

Input* Engine::GetInput()
{
	return pInput;
}

//--------------------------------------------------------------------------------------

Engine* Engine::GetInstance()
{
	return pEngine;
}

void Engine::CheckEvent()
{
	SDL_ActiveEvent actEvent;
    SDL_Event event;
    
	while( SDL_PollEvent(&event) )
	{
		//	SDL_PollEvent(&actEvent);
      	switch (event.type)
		{
        // If a quit event was recieved
        case SDL_QUIT:
         	bExit = true;				
        break;
		case SDL_ACTIVEEVENT:
			if( event.active.state != SDL_APPMOUSEFOCUS )
			{
				GetInput()->SetFocus( event.active.gain );
			}
		break;
      	}
	}

	GetInput()->Update();
}

//--------------------------------------------------------------------------------------

bool Engine::ShouldQuit()
{
	return bExit;
}

//--------------------------------------------------------------------------------------

void Engine::Quit()
{
	bExit = true;
}

//--------------------------------------------------------------------------------------

const std::string& Engine::GetDataPath()
{
	return dataPath;
}

//--------------------------------------------------------------------------------------

void Engine::SetFileMgr( FileMgr* pFileMgr )
{
	this->pFileMgr = pFileMgr;
}

//--------------------------------------------------------------------------------------

FileMgr* Engine::GetFileMgr()
{
	return pFileMgr;
}

