//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "SiphonGL.h"

//--------------------------------------------------------------------------------------

Material::Material() : shaders(0), textures(0), parameters(0), shine(10), specular(0,0,0), diffuse(0.5, 0.5, 0.5), ambient(1,1,1)
{

}

//--------------------------------------------------------------------------------------

Material::~Material()
{

}

//--------------------------------------------------------------------------------------

void Material::BindFixedFunction()
{
	//bind fixed function material properties
	glMaterialf( GL_FRONT, GL_SHININESS, shine );
	glMaterialfv( GL_FRONT, GL_SPECULAR, specular.GetPointer() );
	glMaterialfv( GL_FRONT, GL_DIFFUSE, diffuse.GetPointer() );
	glMaterialfv( GL_FRONT, GL_AMBIENT, ambient.GetPointer() );
}

//--------------------------------------------------------------------------------------

void Material::Bind()
{
	BindFixedFunction();

	for( int i = 0; i < textures.size(); ++i ) //textures
	{
		textures[i]->Bind(i);
	}

	if( shaders.size() ) //vert shader
	{
		shaders[0]->Bind();
	}
	else if( Engine::GetInstance()->GetDevice()->ShaderSupported() )//unbind previous shaders
	{
		//shaders[0]->Disable();
		glUseProgramObjectARB( 0 );
	}

	if( shaders.size() >= 2 ) //pixel shader
	{
		shaders[1]->Bind();
		shaders[1]->SetTextures( textures.size() );
	}
}

//--------------------------------------------------------------------------------------

std::vector<Shader*>& Material::GetShaders()
{
	return shaders;
}

//--------------------------------------------------------------------------------------

std::vector<Texture*>& Material::GetTextures()
{
	return textures;
}

//--------------------------------------------------------------------------------------
// Material loading functions below
//--------------------------------------------------------------------------------------

bool Material::Load( const char* file )
{
	this->file = std::string(file);
	std::string path = Engine::GetInstance()->GetDataPath() + std::string( file );

	FILE* pFile = 0;
	pFile = fopen( path.c_str(), "r" );

	if( !pFile )
	{
		printf("ERROR: Failed to load material %s\n", file );
		return false;
	}

	printf("Loading material: %s\n", file );

	char buffer[256];
	do
	{
		fscanf( pFile, "%s", &buffer );
		//printf("BUF: %s\n", buffer);
		
		if( strcmp( buffer, TEXTURE ) == 0 )
		{
			ReadTexture( pFile );
		}
		else if( Engine::GetInstance()->GetDevice()->ShaderSupported() &&
			strcmp( buffer, SHADERS ) == 0 )
		{
			ReadShader( pFile );
		}
		else if( strcmp( buffer, SHINE ) == 0 )
		{
			ReadShine( pFile );
		}
		else if( strcmp( buffer, DIFFUSE ) == 0 )
		{
			ReadDiffuse( pFile );
		}
		else if( strcmp( buffer, SPECULAR ) == 0 )
		{
			ReadSpecular( pFile );
		}

		buffer[0] = '\0';
	}while( !feof(pFile) );

	fclose( pFile );
	return true;
}

//--------------------------------------------------------------------------------------

bool Material::ReadShine( FILE* pFile )
{
	char buffer[256] = "\0";
	fscanf( pFile, "%f", &shine );
	return true;
}

//--------------------------------------------------------------------------------------

bool Material::ReadSpecular( FILE* pFile )
{
	char buffer[256] = "\0";
	fscanf( pFile, "%f %f %f", &specular[0], &specular[1], &specular[2] );
	return true;
}

//--------------------------------------------------------------------------------------

bool Material::ReadDiffuse( FILE* pFile )
{
	char buffer[256] = "\0";
	fscanf( pFile, "%f %f %f", &diffuse[0], &diffuse[1], &diffuse[2] );
	return true;
}

//--------------------------------------------------------------------------------------

bool Material::ReadTexture( FILE* pFile )
{
	char buffer[256] = "\0";
	fscanf( pFile, "%s", &buffer );

	Texture* tex = Engine::GetInstance()->GetFileMgr()->LoadTexture( buffer );
	if( !tex )
		return false;

	textures.push_back( tex );
	return true;
}

//--------------------------------------------------------------------------------------

bool Material::ReadShader( FILE* pFile )
{
	char first[256] = "\0";
	char second[256] = "\0";
	fscanf( pFile, "%s %s", &first, &second );

	Shader* shad = Engine::GetInstance()->GetFileMgr()->LoadShader( first, second );
	if( !shad )
		return false;

	shaders.push_back( shad );
	return true;
}

//--------------------------------------------------------------------------------------

bool Material::ReadParameter( FILE* pFile )
{
	return true;
}

//--------------------------------------------------------------------------------------

const char* Material::GetName()
{
	return file.c_str();
}