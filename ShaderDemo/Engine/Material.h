//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_MATERIAL_
#define _SIPHON_MATERIAL_

#include "SiphonGL.h"

/**
 * Whats a material?
 * a combination of textures and shaders
 * and is applied to a model/mesh
 */
class Material
{
public:
	Material();
	~Material();

	bool Load( const char* file );

	void Bind(); //what about passes?

	/**
	 * bind fixed function params
	 */
	void BindFixedFunction();

	const char* GetName();

	std::vector<Shader*>& GetShaders();
	std::vector<Texture*>& GetTextures();

protected:
	bool ReadTexture( FILE* pFile );
	bool ReadShader( FILE* pFile );
	bool ReadParameter( FILE* pFile );

	bool ReadShine( FILE* pFile );
	bool ReadSpecular( FILE* pFile );
	bool ReadDiffuse( FILE* pFile );

	std::vector<Shader*> shaders;
	std::vector<Texture*> textures;
	std::vector<Parameter*> parameters; //parameters for shader

	std::string file;

	//fixed function material properties
	Vector3 specular;
	Vector3 diffuse;
	Vector3 ambient;
	float shine;
};

#endif