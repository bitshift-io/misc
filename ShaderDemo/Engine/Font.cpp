//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "SiphonGL.h"

//--------------------------------------------------------------------------------------

bool SFont::CreateFont( char* file )
{
	font = Engine::GetInstance()->GetFileMgr()->LoadTexture( file, false );
	if( font == 0 )
	{
		printf("Failed to load font: %s\n", file );
		return false;
	}

	font->Bind();

	//chars
	chars = glGenLists(256);

	glColor3f(1.0f,1.0f,1.0f);

	int sizeX = 14, sizeY = 16;
	float width = (1.0f / 16.0f);

	for( int i = 0; i < 16; ++i )
  	{
		for( int j = 0; j < 16; ++j)
		{
    		glNewList( chars+((j*16)+i), GL_COMPILE );

			font->Bind();

			glColor3f(1.0f,1.0f,1.0f);

    		glBegin( GL_QUADS ); 

			glTexCoord2f( width*i, 1 - (width*j) );
      		glVertex2i( 0,0 );                           // Vertex Coord (Top Left)

			glTexCoord2f( width*i, 1 - (width*j + width) );
      		glVertex2d( 0,sizeY );                          // Vertex Coord (Bottom Left)

			glTexCoord2f( width*i + width, 1 - (width*j + width) );
      		glVertex2i( sizeX,sizeY );                         // Vertex Coord (Bottom Right)
			
			glTexCoord2f( width*i + width, 1 - (width*j) );
      		glVertex2i( sizeX,0 );                          // Vertex Coord (Top Right)

			glEnd(); 
    		glTranslated(sizeX,0,0);                      
    		glEndList(); 
		}
  	}

	return true;
}

//--------------------------------------------------------------------------------------

bool SFont::Print( float x, float y, char* text, ... )
{
	va_list ap;
	char newText[1024];

 	if( text == 0 )
    	return false;

  	va_start(ap, text);
  	vsprintf(newText, text, ap);
  	va_end(ap);

	TextInfo newTextInf;
	newTextInf.str = std::string( newText );
	newTextInf.x = x;
	newTextInf.y = y;
	this->text.push_front( newTextInf );

	return true;
}

//--------------------------------------------------------------------------------------

bool SFont::DrawText( float x, float y, char* text )
{
	GLint blendSrc,blendDst, blendOn;
	blendOn = glIsEnabled(GL_BLEND);
  	glGetIntegerv(GL_BLEND_SRC, &blendSrc);
	glGetIntegerv(GL_BLEND_DST, &blendDst);

	glEnable(GL_TEXTURE_2D);
	glEnable( GL_BLEND );
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	glMatrixMode(GL_PROJECTION); // and initalize Ortho mode
  	glPushMatrix();
  	glLoadIdentity();
  	glOrtho(0.0f, 800,600, 0.0f,-1.0f,1.0f);

  	glMatrixMode(GL_MODELVIEW);
  	glPushMatrix();

	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(GL_FALSE);

	glUseProgramObjectARB( 0 );

	glLoadIdentity();
  	glTranslated( x, y, 0 );
	//glScalef( scalex,sizey,1.0f);

  	glListBase( chars-32 );

	font->Bind();

	for( int i  = 0 ; i < strlen(text); i++ )
	{
		if( text[i] == '\n' )
		{
			y += 16;
			glLoadIdentity();
			glTranslated( x, y, 0 );
		}
		else
		{
			glCallList( (chars + (text[i] - 32))  );
		}
	}

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);

	glMatrixMode(GL_PROJECTION);
  	glPopMatrix();
  	glMatrixMode(GL_MODELVIEW);
  	glPopMatrix();

	glBlendFunc(blendSrc, blendDst);

	return true;
}

//--------------------------------------------------------------------------------------

void SFont::Render()
{
	glDisable(GL_LIGHTING);

	std::list<TextInfo>::iterator txtIt;
	for( txtIt = text.begin(); txtIt != text.end(); ++txtIt )
	{
		DrawText( txtIt->x, txtIt->y, (char*)txtIt->str.c_str() );	
	}

	glEnable(GL_LIGHTING);
}

//--------------------------------------------------------------------------------------

void SFont::Update()
{
	text.clear();
}
