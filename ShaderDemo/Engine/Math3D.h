//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_MATH3D_
#define _SIPHON_MATH3D_

inline float DtoR( float degrees )
{
	return (float)degrees * 0.01745f; //(PI / 180 )
}

inline float RtoD( float radians )
{
	return (float)radians * 57.29577f;//( 180 / PI )
}
/*
extern float DtoR( float degrees );
extern float RtoD( float radians );
*/

/**
 * a vector in 3d space
 */
class Vector3
{
public:
	enum POSTION
	{
		X = 0,
		Y = 1,
		Z = 2
	};

	enum ROTATION
	{
		YAW = 0,
		PITCH = 1,
		ROLL = 2
	};

	Vector3( float x = 0, float y = 0, float z = 0 )
	{
		v[X] = x;
		v[Y] = y;
		v[Z] = z;
	}

	float& operator[]( const int index )
	{
		return v[ index ];
	}

	void operator=( const Vector3& rhs )
	{
		for( int i = 0; i < 3; ++i )
			v[i] = rhs.v[i];
	}

	bool operator==( const Vector3& rhs )
	{
		if( rhs.v[0] == v[0] && 
			rhs.v[1] == v[1] &&
			rhs.v[2] == v[2] )
			return true;

		return false;
	}

	Vector3 operator-( const Vector3& rhs )
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] - rhs.v[i];

		return ret;
	}

	Vector3 operator+( const Vector3& rhs )
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] + rhs.v[i];

		return ret;
	}

	Vector3 operator*( const float rhs )
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] * rhs;

		return ret;
	}

	Vector3 operator/( const float rhs )
	{
		Vector3 ret;

		for( int i = 0; i < 3; ++i )
			ret[i] = v[i] / rhs;

		return ret;
	}

	float* GetPointer()
	{
		return v;
	}

	Vector3 CrossProduct( Vector3& other )
	{
		Vector3 vNormal;

		// Calculate the cross product with the non communitive equation
		vNormal[X] = ((other[Y] * v[Z]) - (other[Z] * v[Y]));
		vNormal[Y] = ((other[Z] * v[X]) - (other[X] * v[Z]));
		vNormal[Z] = ((other[X] * v[Y]) - (other[Y] * v[X]));

		return vNormal;
	}

	float Dot( Vector3& other )
	{
		float dot = 0;
		
		for( int i = 0; i < 3; ++i )
			dot += v[i] * other[i];

		return dot;
	}

	float Magnitude()
	{
		// Here is the equation:  magnitude = sqrt(V.x^2 + V.y^2 + V.z^2) : Where V is the vector
		return (float)sqrt( (v[X] * v[X]) +
				(v[Y] * v[Y]) +	(v[Z] * v[Z]) );
	}

	void Normalize()
	{
		// Get the magnitude of our normal
		float magnitude = Magnitude();

		// Now that we have the magnitude, we can divide our vector by that magnitude.
		// That will make our vector a total length of 1.
		for( int i = 0; i < 3; ++i )
			v[i] /= magnitude;
	}

	void Rotate( float angle, float x, float y, float z )
	{
		// Calculate the sine and cosine of the angle once
		float cosTheta = (float)cos(angle);
		float sinTheta = (float)sin(angle);

		Vector3 old = *this;
		// Find the new x position for the new rotated point
		v[X]  = (cosTheta + (1 - cosTheta) * x * x) * old[X];
		v[X] += ((1 - cosTheta) * x * y - z * sinTheta)	* old[Y];
		v[X] += ((1 - cosTheta) * x * z + y * sinTheta)	* old[Z];

		// Find the new y position for the new rotated point
		v[Y]  = ((1 - cosTheta) * x * y + z * sinTheta)	* old[X];
		v[Y] += (cosTheta + (1 - cosTheta) * y * y) * old[Y];
		v[Y] += ((1 - cosTheta) * y * z - x * sinTheta)	* old[Z];

		// Find the new z position for the new rotated point
		v[Z]  = ((1 - cosTheta) * x * z - y * sinTheta)	* old[X];
		v[Z] += ((1 - cosTheta) * y * z + x * sinTheta)	* old[Y];
		v[Z] += (cosTheta + (1 - cosTheta) * z * z) * old[Z];
	}

protected:
	float v[3];
};

/**
 * a plane in 3d space
 */
class Plane
{
public:
	enum PLANE
	{
		ON = 0,
		BACK = -1,
		FRONT = 1
	};

	float& operator[]( const int index )
	{
		return p[ index ];
	}


	int SideOfPlane( Vector3& pnt )
	{
		//plug point into plane equation
		//ax + by + cz = d
		float value = p[0] * pnt[Vector3::X] + p[1] * pnt[Vector3::Y] + p[2] * pnt[Vector3::Z] - p[3];

		if( value > 0 )
			return FRONT;
		if( value == 0 )
			return ON;
		if( value < 0 )
			return BACK;

	}

	Vector3 Intersect3( Plane& p1, Plane& p2 )
	{
		Vector3 n1 = Vector3( p[0], p[1], p[2] );
		Vector3 n2 = Vector3( p1[0], p1[1], p1[2] );
		Vector3 n3 = Vector3( p2[0], p2[1], p2[2] );
		float d1 = p[3];
		float d2 = p1[3];
		float d3 = p2[3];

		//http://astronomy.swin.edu.au/~pbourke/geometry/3planes/
		Vector3 point = ((n2.CrossProduct( n3 ) * d1) + (n1.CrossProduct( n3 ) * d2) + (n1.CrossProduct( n2 ) * d3))
			/ n1.Dot( n2.CrossProduct( n3 ) );

		return point;
	}

	void CreateFromFace( Vector3& p1, Vector3& p2, Vector3& p3 )
	{
		//http://easyweb.easynet.co.uk/~mrmeanie/plane/planes.htm

		Vector3 vec1 = p1 - p2;
		Vector3 vec2 = p1 - p3;
		Vector3 normal = vec1.CrossProduct( vec2 );

		p[0] = normal[0];
		p[1] = normal[1];
		p[2] = normal[2];
		p[3] = p1.Dot( normal ); //p[0] * p1[X] + p[1] * p1[Y] + p[2] * p1[Z];
	}

protected:
	float p[4];
};

/**
 * 4x4 matrix 
 *
 * m[0]  m[4]  m[ 8]  m[12]
 * m[1]  m[5]  m[ 9]  m[13]
 * m[2]  m[6]  m[10]  m[14]
 * m[3]  m[7]  m[11]  m[15]
 *
 *   x axis - column 0
 *  y axis - column 1
 *   z axis - column 3
 */
class Matrix4
{
public:
	Matrix4()
	{
		LoadIdentity();
	}

	Matrix4( float mat[16] )
	{
		for( int i = 0; i < 16; ++i )
		{
			m[i] = mat[i];
		}
	}

	void LoadIdentity()
	{
		m[0] = m[5]  = m[10] = m[15] = 1.0;
		m[1] = m[2]  = m[3]  = m[4]  = 0.0;
		m[6] = m[7]  = m[8]  = m[9]  = 0.0;
		m[11]= m[12] = m[13] = m[14] = 0.0;
	}

	void LoadZero()
	{
		m[0] = m[5]  = m[10] = m[15] = 0.0;
		m[1] = m[2]  = m[3]  = m[4]  = 0.0;
		m[6] = m[7]  = m[8]  = m[9]  = 0.0;
		m[11]= m[12] = m[13] = m[14] = 0.0;
	}

	float& operator[]( const int index )
	{
		return m[ index ];
	}

	void Translate( float x, float y, float z )
	{
		m[12] = x;
		m[13] = y;
		m[14] = z;
	}

	void Print()
	{
		for( int i = 0; i < 16; ++i )
		{
			printf("m[%i]  =  %f\n", i, m[i] );
		}
	}


	void RotateY( float angle )
	{
		Matrix4 n;

		float c = (float)cos( angle );
		float s = (float)sin( angle );

		n.m[0] = c;
		n.m[2] = s;
		n.m[8] = -s;
		n.m[10] = c;

		(*this) = (*this) * n;
	}

	void RotateX( float angle )
	{
		Matrix4 n;

		float c = (float)cos( angle );
		float s = (float)sin( angle );

		n.m[5] = c;
		n.m[6] = -s;
		n.m[9] = s;
		n.m[10] = c;

		(*this) = (*this) * n;
	}

	void RotateZ( float angle ) //doesnt work :-/ hrmm
	{
		Matrix4 n;

		float c = (float)cos( angle );
		float s = (float)sin( angle );

		n.m[0] = c;
		n.m[1] = -s;
		n.m[5] = s;
		n.m[5] = c;

		(*this) = (*this) * n;
	}

	Vector3 operator*( Vector3& vector )
	{
		float vec[4], vecCur[4];
		int i;

		for( i = 0; i < 4; ++i )
		{
			vec[i] = 0;

			if( i < 3 )
				vecCur[i] = vector[i];
			else
				vecCur[i] = 1;
		}

		//multiply matrix by vector.. arg
		for( i = 0; i < 4; ++i )
		{
			for( int j = 0; j < 4; ++j )
				vec[i] += (m[ i + (4 * j) ] * vecCur[j]);
		}
		/*
		if( vec[3] != 1 )
			printf("ERROR DURING CALCULATION: %i\n", vec[3]);
		else
			printf("CORRECT CALCULATION: %i\n", vec[3]);
		*/

		return Vector3( vec[0], vec[1], vec[2] );
	}

	Matrix4 operator*( const Matrix4 rhs ) 
	{
		Matrix4 ret;
		ret.m[0] = ret.m[5] = ret.m[10] = ret.m[15] = 0.0;

		for( int k = 0; k < 4; ++k )
		{
			for( int i = 0; i < 4; ++i )
			{
				for( int j = 0; j < 4; ++j )
				{
					ret.m[ 4 * k + i] += m[ 4 * k + j] * rhs.m[ 4 * j + i];
					//ret.m[ 4 * k + i] += m[ 4 * j + i] * rhs.m[ 4 * k + j];
				}
			}
		}

		return ret;
	}

	void operator=( const Matrix4 rhs )
	{
		for( int i = 0; i < 16; ++i )
			m[i] = rhs.m[i];
	}

	/**
	 * Loads current modelview matrix
	 */
	void LoadCurrent( GLint type = GL_MODELVIEW_MATRIX )
	{
		glGetFloatv( type, m );
	}

	/**
	 * set this as current matrix
	 */
	void Set()
	{
		glLoadMatrixf(m);
	}

	/**
	 * set this as current matrix
	 * but multiplies with the current gl matrix
	 */
	void SetM()
	{
		glMultMatrixf(m);
	}

	/**
	 * load a 4x3 rotation matrix OVER this rotation matrix
	 */
	void LoadRotation4x3( float* rotation )
	{
		for( int i = 0; i < 12; ++i )
		{
			m[i] = rotation[i];
		}
	}

	/**
	 * loads an ODE rotation,
	 * ODE uses different coord system,
	 * so needs to be transposed in
	 */
	void ODELoadRotation4x3( float* rotation )
	{
		m[0] = rotation[0];
		m[1] = rotation[4];
		m[2] = rotation[8];
		m[4] = rotation[1];
		m[5] = rotation[5];
		m[6] = rotation[9];
		m[8] = rotation[2];
		m[9] = rotation[6];
		m[10] = rotation[10];
		/*
		m[0] = rotation[0];
		m[1] = rotation[1];
		m[2] = rotation[2];
		m[4] = rotation[4];
		m[5] = rotation[5];
		m[6] = rotation[6];
		m[8] = rotation[8];
		m[9] = rotation[9];
		m[10] = rotation[10];*/
	}

	void Load( float* fm )
	{
		for( int i = 0; i < 16; ++i )
		{
			m[i] = fm[i];
		}
	}

	void Transpose()
	{
		Matrix4 temp = (*this);
		m[1] = temp[4];
		m[4] = temp[1];

		m[2] = temp[8];
		m[8] = temp[2];

		m[6] = temp[9];
		m[9] = temp[6];


		m[3] = temp[12];
		m[12] = temp[3];

		m[7] = temp[13];
		m[13] = temp[7];

		m[11] = temp[14];
		m[14] = temp[11];
	}

	void Inverse()
	{
		Vector3 position = Vector3( m[12], m[13], m[14] );
		Transpose();
		
		m[12] = -( (position[Vector3::X] * m[0]) + (position[Vector3::Y] * m[4]) + (position[Vector3::Z] * m[8]) );
		m[13] = -( (position[Vector3::X] * m[1]) + (position[Vector3::Y] * m[5]) + (position[Vector3::Z] * m[9]) );
		m[14] = -( (position[Vector3::X] * m[2]) + (position[Vector3::Y] * m[6]) + (position[Vector3::Z] * m[10]) );
	}

	/*
	 * Returns a row of the matrix as a vector
	 */
	Vector3 GetRow( int row )
	{
		return Vector3( m[row], m[row + 4], m[row + 8] );
	}

protected:
	float m[16];
};

#endif

