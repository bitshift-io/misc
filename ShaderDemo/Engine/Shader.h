//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_SHADER_
#define _SIPHON_SHADER_

#include "SiphonGL.h"

/**
 * a shader parameter
 */
class Parameter
{
	
};

/**
 * This represents a vertex/pixel shader
 */
class Shader
{
public:

	struct Param
	{
		enum
		{
			SAMPLER2D,
			VEC4,
			VEC3,
			MATRIX4X4
		};
		GLint handle;
		int type;
	};

	enum //what values should be set by this shader?
	{
		TIME = 0x00000001,
		CAMERA = 0x00000010,
		INV_CAMERA = 0x00000100,
		LIGHT,
		INV_LIGHT,
		LIGHT_COLOR,
		FOG_COLOR,
		PROJECTION,
		INV_PROJECTION,
		MODEL_VIEW,
		INV_MODEL_VIEW,
		PARAMETER,	//something like a vector, or matieral properties for example?
	};

	~Shader();

	/**
	 * Loads a shader
	 */
	bool Load( const char* vertex, const char* fragment );

	/**
	 * bind to video 
	 */
	void Bind();

	/**
	 * disable shaders
	 */
	void Disable();

	/**
	 * pass this shader the required texture
	 * Bind must be called before this!
	 */
	void SetParameter( Texture* texture );

	/**
	 * okay, this function tells
	 * how many textures are bound,
	 * then sets from 0 to noBoundTex - 1
	 * as shader samplers
	 */
	void SetTextures( int noBoundTex );

	/**
	 * assume all textures set correctly
	 */
	void SetTextures();
/*
	void Begin();
	void End();*/

	const char* GetFragmentName();
	const char* GetVertexName();
protected:

	/**
	 * load a shader from a text file
	 * buffer is returned with the file in it
	 * please delete it when done!
	 */
	const char* LoadText( const char* file );

	/**
	 * determine sahder parameters/requirements
	 */
	bool Parse( const char* shader );

	/**
	 * compile the shader
	 */
	bool Compile( const char* vertex, const char* fragment );

	unsigned int type; //fragment or vertex?
	unsigned int flags;

	GLhandleARB vertShader;
	GLhandleARB fragShader;

	GLhandleARB program;

	std::vector<Param> parameters;

	std::string fragmentFile;
	std::string vertexFile;

	int curTextureBind;
};

#endif