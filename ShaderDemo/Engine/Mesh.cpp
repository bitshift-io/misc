//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "SiphonGL.h"

//--------------------------------------------------------------------------------------

Mesh::Mesh() : noVertices(0), noIndices(0), noPolygons(0), 
		colors(0), positions(0), normals(0), uvCoords(0), indices(0), flags(0)
{

}

//--------------------------------------------------------------------------------------

Mesh::~Mesh()
{
	SAFE_DELETE_ARRAY( colors );
	SAFE_DELETE_ARRAY( positions );
	SAFE_DELETE_ARRAY( normals );
	SAFE_DELETE_ARRAY( uvCoords );
	SAFE_DELETE_ARRAY( indices );

	//release vbos?
	if( flags == STATIC )
	{
		glDeleteBuffersARB( 1, &vboPositions );
		glDeleteBuffersARB( 1, &vboTexCoords );
		glDeleteBuffersARB( 1, &vboIndices );
		glDeleteBuffersARB( 1, &vboNormals );
		glDeleteBuffersARB( 1, &vboColors );
	}
}

//--------------------------------------------------------------------------------------

void Mesh::Insert( RenderTree& tree )
{
	std::vector< MaterialTable >::iterator atIt; 
	int i =0;
	for( atIt = attributeTable.begin(); atIt != attributeTable.end(); ++atIt, ++i )
	{
		tree.Insert( atIt->pMaterial, this, i );
	}
}

//--------------------------------------------------------------------------------------

int Mesh::Render( int matTableNum )
{
	int polys = 0;

	//render all!
//	if( matTableNum == -1)
//	{
		if( flags == STATIC )
		{
			glEnableClientState( GL_VERTEX_ARRAY );
			glEnableClientState( GL_INDEX_ARRAY );

			glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboPositions );
			glVertexPointer( 3, GL_FLOAT, sizeof(Vector3), 0 );	
			
			if( normals != 0 )
			{
				glEnableClientState( GL_NORMAL_ARRAY );
				glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboNormals );
				glNormalPointer( GL_FLOAT, sizeof(Vector3), 0 );
			}

			if( uvCoords != 0 )
			{
				glClientActiveTexture( GL_TEXTURE0_ARB );			
				glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboTexCoords );
				glTexCoordPointer( 2, GL_FLOAT, sizeof(UVCoord), 0 );
				glEnableClientState( GL_TEXTURE_COORD_ARRAY );

				glActiveTextureARB( GL_TEXTURE0_ARB );
				glEnable(GL_TEXTURE_2D);
			}
		/*
			if( colors != 0 )
			{
				glEnable(GL_COLOR_ARRAY);
				glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboColors );
				glColorPointer( 4, GL_FLOAT, sizeof(Color), 0 );
			}
			else
			{
				glDisable(GL_COLOR_ARRAY);
			}*/

	
			glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, vboIndices );

	//		glDrawElements(GL_TRIANGLES, noIndices, GL_UNSIGNED_INT, 0);
							
/*
			glDrawRangeElements( GL_TRIANGLES, 1476, //1476,
					2952,
					0, //1476,
					GL_UNSIGNED_INT, 0 );*/

			
			if( matTableNum == -1 )
			{
				std::vector< MaterialTable >::iterator matIt; // attributeTable;
				for( matIt = attributeTable.begin(); matIt != attributeTable.end(); ++matIt )
				{
					if( matIt->pMaterial != 0 )
						matIt->pMaterial->Bind();

					glDrawElements(GL_TRIANGLES,matIt->numIndices,GL_UNSIGNED_INT,BUFFER_OFFSET(matIt->startIndex*sizeof(unsigned int)));
				/*	glDrawRangeElements( GL_TRIANGLES, matIt->startIndex,
						(matIt->startIndex + matIt->numIndices),
						matIt->numPrimitives * 3,
						GL_UNSIGNED_INT, 0 );*/

					polys += matIt->numPrimitives;
				}
			}
			else //dont bind our material! this means we are being done via rendertree
			{
				glDrawElements(GL_TRIANGLES, attributeTable[matTableNum].numIndices,GL_UNSIGNED_INT,BUFFER_OFFSET(attributeTable[matTableNum].startIndex*sizeof(unsigned int)));
				polys += attributeTable[matTableNum].numPrimitives;
			}

			glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
			glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0 );

			glDisableClientState( GL_NORMAL_ARRAY );

			glClientActiveTexture( GL_TEXTURE0_ARB );
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
			glDisableClientState( GL_TEXTURE_COORD_ARRAY );
			
			glClientActiveTexture( GL_TEXTURE1_ARB );
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
			glDisableClientState( GL_TEXTURE_COORD_ARRAY );
			
			glActiveTextureARB( GL_TEXTURE1_ARB );
			glDisable(GL_TEXTURE_2D);
			glActiveTextureARB( GL_TEXTURE0_ARB );
			glDisable(GL_COLOR_ARRAY);
		}
		else if( flags == DYNAMIC )
		{
			glEnableClientState(GL_VERTEX_ARRAY);

			if( normals != 0 )
			{
				glEnableClientState(GL_NORMAL_ARRAY);
				glNormalPointer( GL_FLOAT, sizeof(Vector3), normals );
			}

			if( uvCoords != 0 )
			{
				glClientActiveTexture( GL_TEXTURE0_ARB );			

				glEnableClientState( GL_TEXTURE_COORD_ARRAY );
				glTexCoordPointer( 2, GL_FLOAT, sizeof(UVCoord), uvCoords );
				
				glActiveTextureARB( GL_TEXTURE0_ARB );
				glEnable(GL_TEXTURE_2D);
			}

			glVertexPointer( 3, GL_FLOAT, sizeof(Vector3), positions );
			glDrawElements( GL_TRIANGLES, noPolygons*3, GL_UNSIGNED_INT, indices);
		}
//	}
//	else
//	{

//	}
/*
	glBegin(GL_TRIANGLES);
		glColor3f( 0.5, 0.5, 0.5 );

		for( int v = 0; v < noVertices; ++v )
		{
			int i = indices[v];
			glNormal3f( normals[i][0], normals[i][1], normals[i][2]);
			glTexCoord2f(uvCoords[i][0], uvCoords[i][1]);
			glVertex3f( positions[i][0], positions[i][1], positions[i][2]);
		}
	glEnd();*/

	return polys;
}

//--------------------------------------------------------------------------------------

int Mesh::GetNumMaterialTables()
{
	return attributeTable.size();
}

//--------------------------------------------------------------------------------------

bool Mesh::Build()
{
	if( flags == DYNAMIC )
		return true;

	if( Engine::GetInstance()->GetDevice()->VBOSupported() )
	{	
		SetType( STATIC );

		glGenBuffersARB( 1, &vboPositions );
		glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboPositions );
		glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Vector3) * noVertices, positions, GL_STATIC_DRAW_ARB );

		if( normals != 0 )
		{
			glGenBuffersARB( 1, &vboNormals );
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboNormals );
			glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Vector3) * noVertices, normals, GL_STATIC_DRAW_ARB );
		}

		if( uvCoords != 0 )
		{
			glGenBuffersARB( 1, &vboTexCoords );
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboTexCoords );
			glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(UVCoord) * noVertices, uvCoords, GL_STATIC_DRAW_ARB );
		}

		if( colors != 0 )
		{
			glGenBuffersARB( 1, &vboColors );
			glBindBufferARB( GL_ARRAY_BUFFER_ARB, vboColors );
			glBufferDataARB( GL_ARRAY_BUFFER_ARB, sizeof(Color) * noVertices, colors, GL_STATIC_DRAW_ARB );
		}

		glGenBuffersARB( 1, &vboIndices );
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, vboIndices );
		glBufferDataARB( GL_ELEMENT_ARRAY_BUFFER_ARB, sizeof(unsigned int) * noIndices, indices, GL_STATIC_DRAW_ARB );

		glBindBufferARB( GL_ARRAY_BUFFER_ARB, 0 );
		glBindBufferARB( GL_ELEMENT_ARRAY_BUFFER_ARB, 0 );
	}
	else
	{
		SetType( DYNAMIC );
	}

	return true;
}

//--------------------------------------------------------------------------------------

void Mesh::SetType( unsigned int flags )
{
	this->flags = flags;
}

//--------------------------------------------------------------------------------------

const char* Mesh::GetName()
{
	return file.c_str();
}

//--------------------------------------------------------------------------------------
// Mesh loading functions below
//--------------------------------------------------------------------------------------

bool Mesh::Load( const char* file )
{
	this->file = std::string(file);
	std::string path = Engine::GetInstance()->GetDataPath() + std::string( file );

	FILE* pFile = 0;
	pFile = fopen( path.c_str(), "rb" );

	if( pFile == 0 )
	{
		printf("ERROR:failed to open file\n");
		return false;
	}

	printf("Loading model: %s\n", file );

	if( !ReadChunk( pFile ) )
	{
		printf("ERROR:Read chunk failed\n");
		fclose( pFile );	
		return false;
	}

	fclose( pFile );

	Build();

	return true;
}

//--------------------------------------------------------------------------------------

bool Mesh::ReadChunk( FILE* pFile )
{
	Head header;
	if( !ReadHead( pFile, header ) )
	{
		printf("ERROR:Read head failed\n");
		return false;
	}

	switch( header.type )
	{
	case FACES:
		ReadFaces( pFile, header );	
		break;
	case VERTICES:
		ReadVertices( pFile, header );
		break;
	case NORMALS:
		ReadNormals( pFile, header );
		break;
	case UVCOORDS:
		ReadUVCoords( pFile, header );
		break;
	/*case COLORS:
		ReadColors( header );
		break;*/
	case MATERIALTABLE:
		ReadMaterialTable( pFile, header );
		break;
	case ENDOFFILE:
	case MESH_END:
		return true;
		break;
	case MESH_HEAD:
	default:
		fseek( pFile, header.size, SEEK_CUR );
		break;
	};

	if( !feof( pFile ) )
		ReadChunk( pFile );

	return true;
}

//--------------------------------------------------------------------------------------

bool Mesh::ReadHead( FILE* pFile, Head& header )
{
    fread( &header, sizeof(Head), 1, pFile );
	return true;
}

//--------------------------------------------------------------------------------------

bool Mesh::ReadFaces( FILE* pFile, Head& header )
{
	sFace* faces = new sFace[ header.noElements ];
	bool ret = fread( faces, header.size, 1, pFile );

	noIndices = header.noElements * 3;
	noPolygons = header.noElements;

	indices = new unsigned int[ header.noElements * 3 ];

	int k = 0;
	for( int i = 0; i < header.noElements; ++i, k += 3 )
	{
		indices[k + 0] = faces[i].idx[0];
		indices[k + 1] = faces[i].idx[1];
		indices[k + 2] = faces[i].idx[2];
	}

	delete[] faces;

	return ret;
}

//--------------------------------------------------------------------------------------

bool Mesh::ReadVertices( FILE* pFile, Head& header )
{
	sVertex* vertices = new sVertex[ header.noElements ];
	bool ret = fread( vertices, header.size, 1, pFile );

	noVertices = header.noElements;
	positions = new Vector3[ header.noElements ];
	for( int i = 0; i < header.noElements; ++i )
	{
		for( int j = 0; j < 3; ++j )
		{
			positions[i][j] = vertices[i].vert[j];
		}
	}

	delete[] vertices;

	return ret;
}

//--------------------------------------------------------------------------------------

bool Mesh::ReadUVCoords( FILE* pFile, Head& header )
{
	sUV* uvs = new sUV[ header.noElements ];
	bool ret = fread( uvs, header.size, 1, pFile );

	if( uvCoords == 0 )
	{
		uvCoords = new UVCoord[ header.noElements ];
		for( int i = 0; i < header.noElements; ++i )
		{
			for( int j = 0; j < 2; ++j )
			{
				uvCoords[i][j] = uvs[i].uv[j];
				//printf("%f ", m.uvCoords[i][j] );
			}
		}		
	}

	delete[] uvs;
	return ret;
}

//--------------------------------------------------------------------------------------

bool Mesh::ReadNormals( FILE* pFile, Head& header )
{
	sVertex* norms = new sVertex[ header.noElements ];
	bool ret = fread( norms, header.size, 1, pFile );

	normals = new Vector3[ header.noElements ];
	for( int i = 0; i < header.noElements; ++i )
	{
		for( int j = 0; j < 3; ++j )
		{
			normals[i][j] = norms[i].vert[j];
		}
	}

	delete[] norms;
	return ret;
}

//--------------------------------------------------------------------------------------

bool Mesh::ReadMaterialTable( FILE* pFile, Head& header )
{
	sMaterialTable material;
	fread( &material, header.size, 1, pFile );

	MaterialTable matTbl;
	Material* mat = Engine::GetInstance()->GetFileMgr()->LoadMaterial( material.material );

	if( mat == 0 ) 
		printf("failed to load material: %s\n", material.material );

	matTbl.pMaterial = mat;

	matTbl.numIndices = material.numIndices;
	matTbl.startIndex = material.startIndex;
	matTbl.numPrimitives = material.numPrimitives;

	attributeTable.push_back( matTbl );
	return true;
}

//--------------------------------------------------------------------------------------

bool Mesh::ReadColors( FILE* pFile, Head& header )
{
	sVertex* cols = new sVertex[ header.noElements ];
	bool ret = fread( cols, header.size, 1, pFile );

	colors = new Color[ header.noElements ];
	for( int i = 0; i < header.noElements; ++i )
	{
		for( int j = 0; j < 3; ++j )
		{
			colors[i][j] = cols[i].vert[j];
			if( colors[i][j] < 0 )
				printf("bad color!!!!\n");
		}
		colors[i][3] = 1.0;
	}

	delete[] cols;
	return ret;
}

