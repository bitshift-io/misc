//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#include "SiphonGL.h"

//--------------------------------------------------------------------------------------

Shader::~Shader()
{
	glDeleteObjectARB( vertShader );
	glDeleteObjectARB( fragShader );

    glDeleteObjectARB( program );
}

//--------------------------------------------------------------------------------------

bool Shader::Load( const char* vertex, const char* fragment )
{
	this->fragmentFile = std::string(fragment);
	this->vertexFile = std::string(vertex);

	char* vertFile = (char*)LoadText( vertex );
	char* fragFile = (char*)LoadText( fragment );

	bool bRet = Compile( vertFile, fragFile );

	if( bRet )
		Parse( fragFile );

	SAFE_DELETE_ARRAY( vertFile );
	SAFE_DELETE_ARRAY( fragFile );

	return bRet;
}

//--------------------------------------------------------------------------------------

const char* Shader::LoadText( const char* file )
{
	std::string path = Engine::GetInstance()->GetDataPath() + std::string( file );
	FILE* pFile = 0;
	pFile = fopen( path.c_str(), "r" );

	if( !pFile )
	{
		printf("ERROR: Failed to load shader %s\n", file );
		return false;
	}

	printf("Loading shader: %s\n", file );

	// obtain file size.
	fseek( pFile , 0 , SEEK_END );
	long lSize = ftell (pFile);
	rewind(pFile);

	char* buffer = new char[ lSize + 1 ];
	unsigned int size = fread( buffer, 1, lSize, pFile );
	buffer[ size ] = '\0';
	fclose( pFile );

	return buffer;
}

//--------------------------------------------------------------------------------------

bool Shader::Compile( const char* vertex, const char* fragment )
{
	program = glCreateProgramObjectARB();

	//VERTEX SHADER COMPILE
	const char* vertStrings[1];
	vertStrings[0] = vertex;
	//printf("vertex:\n[%s]\n", vertStrings[0]);

	vertShader = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);

	glShaderSourceARB( vertShader, 1, vertStrings, NULL );
    glCompileShaderARB( vertShader );
	
	int nResult;
    glGetObjectParameterivARB( vertShader, GL_OBJECT_COMPILE_STATUS_ARB, &nResult );
	if( !nResult )
	{
		char strErr[1024];
		glGetInfoLogARB( vertShader, sizeof(strErr), NULL, strErr );
		printf("VERTEX ERROR: %s\n", strErr);
		return false;
	}

	glAttachObjectARB( program, vertShader );

	//FRAGMENT SHADER COMPILE
	const char* fragStrings[1];
	fragStrings[0] = fragment;
	//printf("fragment:\n[%s]\n", fragStrings[0]);

	fragShader = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);

	glShaderSourceARB( fragShader, 1, fragStrings, NULL );
    glCompileShaderARB( fragShader );
	
    glGetObjectParameterivARB( fragShader, GL_OBJECT_COMPILE_STATUS_ARB, &nResult );
	if( !nResult )
	{
		char strErr[1024];
		glGetInfoLogARB( fragShader, sizeof(strErr), NULL, strErr );
		printf("FRAGMENT ERROR: %s\n", strErr);
		return false;
	}

	glAttachObjectARB( program, fragShader );

	//LINK
	glLinkProgramARB( program );
    glGetObjectParameterivARB( program, GL_OBJECT_LINK_STATUS_ARB, &nResult );
    if( !nResult )
	{
		char strErr[1024];
		glGetInfoLogARB( program, sizeof(strErr), NULL, strErr );
		printf("ERROR: %s\n", strErr);
		return false;
	}

	return true;	
}

//--------------------------------------------------------------------------------------

void Shader::Bind()
{
	curTextureBind = 0;
	glUseProgramObjectARB( program );

	//printf("binding: %s %s\n", fragmentFile.c_str(), vertexFile.c_str() );
}

//--------------------------------------------------------------------------------------

void Shader::Disable()
{
	glUseProgramObjectARB( 0 );
}

//--------------------------------------------------------------------------------------

void Shader::SetParameter( Texture* texture )
{
	//printf("bind texture %s to handle %i\n", texture->GetName(), parameters[curTextureBind].handle );
	texture->ShaderBind( parameters[curTextureBind].handle );
	++curTextureBind;
}

//--------------------------------------------------------------------------------------

void Shader::SetTextures( int noBoundTex )
{
	//make sure we dont set to many params, or to many texture
	//units
	int size = 0;
	if( parameters.size() <= noBoundTex )
		size = noBoundTex;
	else
		size = parameters.size();
	
	for( int i = 0; i < size; ++i )
	{	
		glUniform1iARB( parameters[i].handle, i );
	}
}

//--------------------------------------------------------------------------------------

void Shader::SetTextures()
{
	int size = size = parameters.size();
	for( int i = 0; i < size; ++i )
	{	
		glUniform1iARB( parameters[i].handle, i );
	}
}

//--------------------------------------------------------------------------------------

bool Shader::Parse( const char* shader )
{
	int pos = 0;	
	char buffer[256];

	char first[256];
	char second[256];
	char third[256];

	int read = 0;
	do
	{
		buffer[0] = '\0';
		first[0] = '\0';
		second[0] = '\0';

		read = sscanf( (shader + pos), "%[^;^\n]s", &buffer );

		if( strlen(buffer) <= 0 )
			pos += 1;
		else
			pos += strlen(buffer);

		//printf("[%s] %i\n", buffer, strlen(buffer));
		//printf("rest of shader: %s\n", (shader + pos) );

		if( strcmp( buffer, "void main()" ) == 0 || strcmp( buffer, "void main(void)" ) == 0 )
			break; //no point reading past main for parameters

		sscanf( buffer, "%s %s %s", &first, &second, &third );

	//printf("|%s|%s|%s|\n", first, second, third);
		if( strcmp( first, "uniform") == 0 )
		{
			if( std::string("buffer").find("sampler") ) //strcmp( buffer, "sampler" ) == 0 )
			{
				Param p;

				p.type = Param::SAMPLER2D;
				p.handle = glGetUniformLocationARB( program, third );
				//printf("texture %s found with handle: %i\n", third, p.handle );

				//printf("texture/shader param pushed back!\n");
				parameters.push_back( p );
			}			
		}

	}while( pos <= strlen(shader) );

	return true;
}

//--------------------------------------------------------------------------------------

const char* Shader::GetFragmentName()
{
	return fragmentFile.c_str();
}

//--------------------------------------------------------------------------------------

const char* Shader::GetVertexName()
{
	return vertexFile.c_str();
}