//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_TEXTURE_
#define _SIPHON_TEXTURE_

#include "SiphonGL.h"

/**
 * This hold the opengl texture,
 * plus extra parameters on the texture,
 * blend operations, cube map etc...
 */
class Texture //: ResourceManaged<Texture>
{
public:
	~Texture();

	enum BLENDTYPE
	{
		MASK,
		BLEND,
		SOLID
	};

	enum TEXTURE_TYPE
	{
		TEX_DIFFUSE, //normal texture
		TEX_CUBE,
		TEX_BUMP,
		TEX_NORMAL,
		TEX_DETAIL,
		TEX_PARRALLAX,
		TEX_SPECULAR,
	};

	enum FLAGS
	{
		CLAMP,
		CLAMP_TO_EDGE,
		NEAREST,
		LINEAR,
		NEAREST_MIPMAP_NEAREST,
		LINEAR_MIPMAP_NEAREST,
		LINEAR_MIPMAP_LINEAR,
	};

	/**
	 * bind a texture to a texture unit,
	 * must supply which unit!
	 */
	void Bind( int texUnit = 0 );

	/**
	 * Bind this shader to this shader handle
	 */
	void ShaderBind( GLint handle );


	bool Load( const char* file, bool bMipMap = true );
	const char* GetName();
	/*
	void RenderQuad( int lx, int ty, int rx, int by );
	void Begin();
	void End();*/
protected:

	GLuint texture;

	unsigned int flags;
	unsigned int blendType;
	unsigned int textureType;
	unsigned int width, height;

	std::string file;
};

#endif