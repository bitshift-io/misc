//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_FILEMGR_
#define _SIPHON_FILEMGR_

//#include "SiphonGL.h"
#include <map>
class Texture;
class Shader;
class Material;
class Mesh;

class FileMgr
{
public:
	~FileMgr();

	Texture* LoadTexture( const char* file, bool bMipMap = true );
	Shader* LoadShader( const char* vertex, const char* fragment );
	Material* LoadMaterial( const char* file );
	Mesh* LoadMesh( const char* file );
protected:
	std::vector<Texture*> textures;
	std::vector<Shader*> shaders;
	std::vector<Material*> materials;
	std::vector<Mesh*> meshes;
};

#endif