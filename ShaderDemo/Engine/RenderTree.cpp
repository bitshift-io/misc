//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------

#include "SiphonGL.h"

//--------------------------------------------------------------------------------------
//		State
//--------------------------------------------------------------------------------------

State::State() : parent(0)
{

}

//--------------------------------------------------------------------------------------

State::~State()
{
	std::list< State* >::iterator childIt;
	for( childIt = children.begin(); childIt != children.end(); ++childIt )
	{
		SAFE_DELETE( (*childIt) );
	}
}

//--------------------------------------------------------------------------------------

inline unsigned int State::Render()
{
	int polys = 0;
	std::list< State* >::iterator childIt;
	for( childIt = children.begin(); childIt != children.end(); ++childIt )
	{
		polys += (*childIt)->Render();
	}

	return polys;
}

//--------------------------------------------------------------------------------------

State* State::FindInChildren( Material* pMat, Mesh* pMesh, unsigned int index )
{
	std::list<State*>::iterator childIt;
	int idx = 0;
	for( childIt = children.begin(); childIt != children.end(); ++childIt, ++idx )
	{
		if( (*childIt)->Find( pMat, pMesh, index ) )
			return (*childIt);
	}

	return 0;
}

//--------------------------------------------------------------------------------------
//		ShaderState
//--------------------------------------------------------------------------------------

ShaderState::ShaderState() : pShader(0), State()
{

}

//--------------------------------------------------------------------------------------

void ShaderState::Insert( Material* pMat, Mesh* pMesh, unsigned int index )
{
	//does one of our children match?
	//if so, pass it and return
	State* child;
	if( (child = FindInChildren( pMat, pMesh, index )) != 0 )
	{
		child->Insert( pMat, pMesh, index );
		return;
	}

	//does this material have a shader or not?
	std::vector<Shader*>& shaders = pMat->GetShaders();

	if( shaders.size() )
		pShader = shaders[0];

	//create a texture, and pass it off
	TextureState* t = new TextureState();
	t->Insert( pMat, pMesh, index );

	children.push_front( t ); //we should sort here!!!!
}

//--------------------------------------------------------------------------------------

inline unsigned int ShaderState::Render()
{
	if( pShader )
	{
		pShader->Bind();
		pShader->SetTextures();
	}
	else
	{
		glUseProgramObjectARB( 0 );
	}

	return State::Render();
}

//--------------------------------------------------------------------------------------

bool ShaderState::Find( Material* pMat, Mesh* pMesh, unsigned int index )
{
	std::vector<Shader*>& shaders = pMat->GetShaders();
	std::vector<Shader*>::iterator shadIt;

	for( shadIt = shaders.begin(); shadIt != shaders.end(); ++shadIt )
	{
		if( this->pShader == (*shadIt) )
			return true;
	}

	return false;
}

//--------------------------------------------------------------------------------------

unsigned int ShaderState::GetType()
{
	return STATE_SHADER;
}

//--------------------------------------------------------------------------------------
//		TextureState
//--------------------------------------------------------------------------------------

TextureState::TextureState() : pTexture(0), textureUnit(0), State()
{

}

//--------------------------------------------------------------------------------------

void TextureState::Insert( Material* pMat, Mesh* pMesh, unsigned int index )
{
	//does one of our children match?
	//if so, pass it and return
	State* child;
	if( (child = FindInChildren( pMat, pMesh, index )) != 0 )
	{
		child->Insert( pMat, pMesh, index );
		printf("texture found in tree\n");
		return;
	}

	//are there any textures?
	std::vector<Texture*>& textures = pMat->GetTextures();

	if( textures.size() > textureUnit )
	{
		//set this texture,
		//then create another and pass it off!
		pTexture = textures[ textureUnit ];
		TextureState* t = new TextureState();
		t->Insert( pMat, pMesh, index, textureUnit + 1 );

		children.push_front( t );//we should sort here!!!!
	}
	else
	{
		//create a mesh, and pass it off
		MeshState* m = new MeshState();
		m->Insert( pMat, pMesh, index );

		children.push_front( m );//we should sort here!!!!
	}
}

//--------------------------------------------------------------------------------------

void TextureState::Insert( Material* pMat, Mesh* pMesh, unsigned int index, unsigned int textureUnit )
{
	this->textureUnit = textureUnit;

	//are there any textures?
	std::vector<Texture*>& textures = pMat->GetTextures();

	if( textures.size() > textureUnit )
	{
		//set this texture,
		//then create another and pass it off!
		pTexture = textures[ textureUnit ];
		TextureState* t = new TextureState();
		t->Insert( pMat, pMesh, index, textureUnit + 1 );

		children.push_front( t );//we should sort here!!!!
	}
	else
	{
		//create a mesh, and pass it off
		MeshState* m = new MeshState();
		m->Insert( pMat, pMesh, index );

		children.push_front( m );//we should sort here!!!!
	}
}

//--------------------------------------------------------------------------------------

inline unsigned int TextureState::Render()
{
	if( pTexture )
		pTexture->Bind( textureUnit );

	return State::Render();
}

//--------------------------------------------------------------------------------------

bool TextureState::Find( Material* pMat, Mesh* pMesh, unsigned int index )
{
	std::vector<Texture*>& textures = pMat->GetTextures();
	std::vector<Texture*>::iterator texIt;

	for( texIt = textures.begin(); texIt != textures.end(); ++texIt )
	{
		if( this->pTexture == (*texIt) )
			return true;
	}

	return false;
}

//--------------------------------------------------------------------------------------

unsigned int TextureState::GetType()
{
	return STATE_TEXTURE;
}

//--------------------------------------------------------------------------------------
//		MeshState
//--------------------------------------------------------------------------------------

MeshState::MeshState() : pMesh(0), State()
{

}

//--------------------------------------------------------------------------------------

void MeshState::Insert( Material* pMat, Mesh* pMesh, unsigned int index )
{
	//nothing to pass off here!
	this->pMesh = pMesh;
	this->index = index;
	this->pMat = pMat;
}

//--------------------------------------------------------------------------------------

inline unsigned int MeshState::Render()
{
	pMat->BindFixedFunction();
	return pMesh->Render( index );
}

//--------------------------------------------------------------------------------------

bool MeshState::Find( Material* pMat, Mesh* pMesh, unsigned int index )
{
	if( this->pMesh == pMesh && this->index == index )
		return true;

	return false;
}

//--------------------------------------------------------------------------------------

unsigned int MeshState::GetType()
{
	return STATE_MESH;
}

//--------------------------------------------------------------------------------------
//		RenderTree
//--------------------------------------------------------------------------------------

RenderTree::RenderTree() : State()
{

}

//--------------------------------------------------------------------------------------

void RenderTree::Insert( Material* pMat, Mesh* pMesh, unsigned int index )
{
	if( pMat == 0 )
	{
		//printf("not a material!\n");
		return;
	}

	//does one of our children match?
	//if so, pass it and return
	State* child;
	if( (child = FindInChildren( pMat, pMesh, index )) != 0 )
	{
		child->Insert( pMat, pMesh, index );
		return;
	}

	//create a shader, and pass it off:
	ShaderState* s = new ShaderState();
	s->Insert( pMat, pMesh, index );

	children.push_front( s ); //we should sort here!!!!
}

//--------------------------------------------------------------------------------------

bool RenderTree::Find( Material* pMat, Mesh* pMesh, unsigned int index )
{
	//the best code is no code!
	return true;
}

//--------------------------------------------------------------------------------------

unsigned int RenderTree::GetType()
{
	return STATE_ROOT;
}
