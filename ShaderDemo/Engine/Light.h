//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_LIGHT_
#define _SIPHON_LIGHT_

//--------------------------------------------------------------------------------------

class Light
{
public:
	Light();
/*
	void SetPosition();
	void SetAmbient();
	void SetSpecular();*/

	void Bind();
	void SetPosition( Vector3& position );

protected:

	Vector3 position;
};

#endif