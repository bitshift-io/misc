#include "SiphonGL.h"

Camera::Camera() : position(0,0,0), view(0,0,1), right(1,0,0), up(0,1,0), rotation(0,180,0), bFreeCam(true)
{
	LookAt();
	SetupFrustum();
}

void Camera::SetupFrustum()
{/*
	int width, height;
	GLDevice::GetInstance().GetDeviceSize( width, height );
	//glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	// Calculate The Aspect Ratio Of The Window
	gluPerspective( 90.0f, (GLfloat)width/(GLfloat)height, 0.1f, 1000.0f );
	
*/
	//the proper way:
	Matrix4 proj;
	proj.LoadCurrent( GL_PROJECTION_MATRIX );
	projection.LoadCurrent( GL_PROJECTION_MATRIX );
	proj.Transpose();

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	//http://www2.ravensoft.com/users/ggribb/plane%20extraction.pdf

	//left plane
	frustum.planes[0][0] = proj[12] + proj[0];
	frustum.planes[0][1] = proj[13] + proj[1];
	frustum.planes[0][2] = proj[14] + proj[2];
	frustum.planes[0][3] = proj[15] + proj[3];

	//right plane
	frustum.planes[1][0] = proj[12] - proj[0];
	frustum.planes[1][1] = proj[13] - proj[1];
	frustum.planes[1][2] = proj[14] - proj[2];
	frustum.planes[1][3] = proj[15] - proj[3];

	//top plane
	frustum.planes[2][0] = proj[12] - proj[4];
	frustum.planes[2][1] = proj[13] - proj[5];
	frustum.planes[2][2] = proj[14] - proj[6];
	frustum.planes[2][3] = proj[15] - proj[7];

	//bottom plane
	frustum.planes[3][0] = proj[12] + proj[4];
	frustum.planes[3][1] = proj[13] + proj[5];
	frustum.planes[3][2] = proj[14] + proj[6];
	frustum.planes[3][3] = proj[15] + proj[7];

	//near plane
	frustum.planes[4][0] = proj[12] + proj[8];
	frustum.planes[4][1] = proj[13] + proj[9];
	frustum.planes[4][2] = proj[14] + proj[10];
	frustum.planes[4][3] = proj[15] + proj[11];

	//far plane
	frustum.planes[5][0] = proj[12] - proj[8];
	frustum.planes[5][1] = proj[13] - proj[9];
	frustum.planes[5][2] = proj[14] - proj[10];
	frustum.planes[5][3] = proj[15] - proj[11];

	//find intersection of planes
	//left top near
	frustum.points[0] = frustum.planes[0].Intersect3( frustum.planes[2], frustum.planes[4] ); 

	//right top near
	frustum.points[1] = frustum.planes[1].Intersect3( frustum.planes[2], frustum.planes[4] );

	//left bottom near
	frustum.points[2] = frustum.planes[0].Intersect3( frustum.planes[3], frustum.planes[4] );

	//right bottom near
	frustum.points[3] = frustum.planes[1].Intersect3( frustum.planes[3], frustum.planes[4] );

	//left top far
	frustum.points[4] = frustum.planes[0].Intersect3( frustum.planes[2], frustum.planes[5] );

	//right top far
	frustum.points[5] = frustum.planes[1].Intersect3( frustum.planes[2], frustum.planes[5] );

	//left bottom far
	frustum.points[6] = frustum.planes[0].Intersect3( frustum.planes[3], frustum.planes[5] );

	//right bottom far
	frustum.points[7] = frustum.planes[1].Intersect3( frustum.planes[3], frustum.planes[5] );
}

void Camera::Rotate( float angle, float x, float y, float z )
{
	view.Rotate(angle, x, y, z);
	view.Normalize();
}

void Camera::Update()
{
	if( bFreeCam )
		FreeCam();

	Transform();
}

void Camera::SetFreeCam( bool bFreeCam )
{
	this->bFreeCam = bFreeCam;
}

void Camera::SetPosition( float x, float y, float z )
{
	position[Vector3::X] = x;
	position[Vector3::Y] = y;
	position[Vector3::Z] = z;
}

void Camera::FreeCam()
{
	int new_x = 0, new_y = 0;
	Engine::GetInstance()->GetInput()->GetMouseState( new_x, new_y );

	rotation[Vector3::YAW] -= (float)new_x;

	//if( rotation[PITCH] + new_y > 90 && rotation[PITCH] + new_y < 270 )
		rotation[Vector3::PITCH] += (float)new_y;

	Matrix4 m;
//	m.RotateY( rotation[Vector3::YAW] / 100.0f );
//	m.RotateX( rotation[Vector3::PITCH] / 100.0f );
/*
	up = Vector3( m[0][1], m[1][1], m[2][1] );
	view = Vector3( m[0][2], m[1][2], m[2][2] );
	right = Vector3( m[0][0], m[1][0], m[2][0] );*/

	/**
 * 4x4 matrix 
 *
 * m[0]  m[4]  m[ 8]  m[12]
 * m[1]  m[5]  m[ 9]  m[13]
 * m[2]  m[6]  m[10]  m[14]
 * m[3]  m[7]  m[11]  m[15]
 *
 *   x axis - column 0
 *  y axis - column 1
 *   z axis - column 3
 */

	up = Vector3( m[4], m[5], m[6] );
	view = Vector3( m[8], m[9], m[10] );
	right = Vector3( m[0], m[4], m[8] );

	if( Engine::GetInstance()->GetInput()->KeyDown(SDLK_s) )
	{
		position =  (view * 1.0) + position;
	}

	if( Engine::GetInstance()->GetInput()->KeyDown(SDLK_w) )
	{
		position =  position - (view * 1.0);
	}

	if( Engine::GetInstance()->GetInput()->KeyDown(SDLK_a)  )
	{
		//Vector3 right( m[0][0], m[1][0], m[2][0] );
		position = position - (right * 1.0);
	}

	if( Engine::GetInstance()->GetInput()->KeyDown(SDLK_d) )
	{
		//Vector3 right( m[0][0], m[1][0], m[2][0] );
		position = position + (right * 1.0);
	}

	if( Engine::GetInstance()->GetInput()->KeyDown(SDLK_SPACE) )
	{
		position[Vector3::Y] += 1.0;
	}

	if( Engine::GetInstance()->GetInput()->KeyDown(SDLK_LCTRL) )
	{
		position[Vector3::Y] -= 1.0;
	}

	return;
}

/**
 * 4x4 matrix 
 *
 * m[0]  m[4]  m[ 8]  m[12]
 * m[1]  m[5]  m[ 9]  m[13]
 * m[2]  m[6]  m[10]  m[14]
 * m[3]  m[7]  m[11]  m[15]
 *
 *   x axis - column 0
 *  y axis - column 1
 *   z axis - column 3
 */
void Camera::Transform()
{
	transform[ 0] = right[Vector3::X];
	transform[ 4] = right[Vector3::Y];
	transform[ 8] = right[Vector3::Z];

	transform[ 1] = up[Vector3::X];
	transform[ 5] = up[Vector3::Y];
	transform[ 9] = up[Vector3::Z];

	transform[ 2] = view[Vector3::X];
	transform[ 6] = view[Vector3::Y];
	transform[10] = view[Vector3::Z];

	transform[12] = -( (position[Vector3::X] * right[Vector3::X]) + (position[Vector3::Y] * right[Vector3::Y]) + (position[Vector3::Z] * right[Vector3::Z]) );
	transform[13] = -( (position[Vector3::X] * up[Vector3::X]) + (position[Vector3::Y] * up[Vector3::Y]) + (position[Vector3::Z] * up[Vector3::Z]) );
	transform[14] = -( (position[Vector3::X] * view[Vector3::X]) + (position[Vector3::Y] * view[Vector3::Y]) + (position[Vector3::Z] * view[Vector3::Z]) );

	//set world matrix also
	world[ 0] = -right[Vector3::X];
	world[ 1] = -right[Vector3::Y];
	world[ 2] = -right[Vector3::Z];

	world[ 4] = -up[Vector3::X];
	world[ 5] = -up[Vector3::Y];
	world[ 6] = -up[Vector3::Z];

	world[ 8] = -view[Vector3::X];
	world[ 9] = -view[Vector3::Y];
	world[10] = -view[Vector3::Z];
	world.Translate( position[Vector3::X], position[Vector3::Y], position[Vector3::Z] );
}

BOUNDS Camera::GetTransformedBounds()
{
	BOUNDS worldBounds;

	for( int i = 0 ; i < 8; ++i )
	{
		worldBounds.points[i] = world * frustum.points[i];
	}

	return worldBounds;
}

void Camera::LookAt()
{
	transform.Set();
}

Vector3& Camera::GetPosition()
{
	return position;
}

Vector3& Camera::GetRotation()
{
	return view;
}

