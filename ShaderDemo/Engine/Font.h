//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_FONT_
#define _SIPHON_FONT_

#include "DDS.h"
#include <list>

//--------------------------------------------------------------------------------------

/**
 * when draw string is called, the info is qued
 * and it is rendered last
 */
struct TextInfo
{
	std::string str;
	float x, y;
};

//--------------------------------------------------------------------------------------

/**
 * font class to draw text on the screen
 */
class SFont
{
public:
	bool CreateFont( char* file );
	bool DrawText( float x, float y, char* text );
	bool Print( float x, float y, char* text, ... );

	void Render();

	void Update();

protected:
	Texture* font;
	GLuint chars;

	std::list<TextInfo> text;
};

#endif

