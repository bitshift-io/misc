//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
//#include "Device.h"
#include "SiphonGL.h"
#include "glext.h"

PFNGLACTIVETEXTUREARBPROC glActiveTextureARB = 0;
PFNGLCLIENTACTIVETEXTUREARBPROC glClientActiveTexture = 0;

PFNGLBINDBUFFERARBPROC glBindBufferARB = 0;
PFNGLGENBUFFERSARBPROC glGenBuffersARB = 0;
PFNGLBUFFERDATAARBPROC glBufferDataARB = 0;
PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB = 0;

PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElements = 0;

PFNGLCREATESHADEROBJECTARBPROC glCreateShaderObjectARB = 0;
PFNGLCREATESHADEROBJECTARBPROC glUseProgramObjectARB = 0;

PFNGLSHADERSOURCEARBPROC glShaderSourceARB = 0;
PFNGLCOMPILESHADERARBPROC glCompileShaderARB = 0;
PFNGLGETOBJECTPARAMETERIVARBPROC glGetObjectParameterivARB = 0;
PFNGLATTACHOBJECTARBPROC glAttachObjectARB = 0;
PFNGLCREATEPROGRAMOBJECTARBPROC glCreateProgramObjectARB = 0;
PFNGLGETINFOLOGARBPROC glGetInfoLogARB = 0;
PFNGLLINKPROGRAMARBPROC glLinkProgramARB = 0;
PFNGLDELETEOBJECTARBPROC glDeleteObjectARB = 0;
PFNGLGETUNIFORMLOCATIONARBPROC glGetUniformLocationARB = 0;
PFNGLUNIFORM1IARBPROC glUniform1iARB = 0;

//--------------------------------------------------------------------------------------

Device::Device() : framesThisSecond(0), fps(0), ticks(0)
{

}

//--------------------------------------------------------------------------------------

Device::~Device()
{
	Destroy();
}

//--------------------------------------------------------------------------------------

void Device::ResizeDevice( int width, int height )
{
	if( height <= 0 )
		height = 1;

	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	// Calculate The Aspect Ratio Of The Window
	gluPerspective( 90.0f, (GLfloat)width/(GLfloat)height, 0.1f, farPlane );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	this->width = width;
	this->height = height;
}

//--------------------------------------------------------------------------------------

bool Device::Create( int width, int height, bool fullscreen, char* title )
{
	int flags;
	int size;

	// Initialize SDL
	if( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		fprintf(stderr, "Couldn't init SDL: %s\n", SDL_GetError());
		return false;
	}

	flags = SDL_OPENGL;
	if ( fullscreen )
	{
		flags |= SDL_FULLSCREEN;
	}

	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 1 );
	if( (windowHandle = SDL_SetVideoMode(width, height, 0, flags)) == NULL )
	{
		return false;
	}
	SDL_GL_GetAttribute( SDL_GL_STENCIL_SIZE, &size);

	ResizeDevice(width, height);

	if( !CreateDevice() )
	{
		printf("ERROR: Cannot create device!!!\n");
		Destroy();
		return false;
	}

	SetWindowTitle( title );
	SetCursor( false );

	this->width = width;
	this->height = height;

	return true;
}

//--------------------------------------------------------------------------------------

void Device::Destroy()
{
	SDL_Quit();
}

//--------------------------------------------------------------------------------------

bool Device::CreateDevice()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear( GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); //GL_COLOR_BUFFER_BIT
	EndRender();

	glShadeModel(GL_SMOOTH);
	glEnable(GL_TEXTURE_2D);	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); //GL_FASTEST
	glColor3f(1.0f,1.0f,1.0f);
/* //is the lighting issue here?
	glFrontFace(GL_CW);
	glCullFace(GL_BACK);*/
	glEnable(GL_CULL_FACE);

	printf("----------------------------------------\n");
	printf("\tOpenGL Extensions\n");
	printf("----------------------------------------\n");
	if( ExtensionSupported("GL_ARB_multitexture") )
	{
		glActiveTextureARB = (PFNGLACTIVETEXTUREARBPROC) 
		SDL_GL_GetProcAddress("glActiveTextureARB");
		
		glActiveTextureARB( GL_TEXTURE1_ARB );
		glEnable(GL_TEXTURE_2D);

		glActiveTextureARB( GL_TEXTURE0_ARB );
		glEnable(GL_TEXTURE_2D);

		printf("Multitexture\t\t\t[OK]\n");
	}
	else
	{
		//we NEED multitexture
		printf("Multitexture\t\t\t[FAIL]\n");
		return false;
	}

	if( ExtensionSupported("GL_EXT_draw_range_elements") )
	{
		printf("Draw Range Element\t\t[OK]\n");
		glDrawRangeElements = (PFNGLDRAWRANGEELEMENTSPROC)
		SDL_GL_GetProcAddress("glDrawRangeElements");		
	}
	else
	{
		//required extension
		printf("Draw Range Element\t\t[FAIL]\n");
		return false;
	}

	if( ExtensionSupported("GL_ARB_vertex_buffer_object") )
	{
		bVBOSupported = true;

		glClientActiveTexture = (PFNGLCLIENTACTIVETEXTUREARBPROC)
		SDL_GL_GetProcAddress("glClientActiveTexture");

		glBindBufferARB = (PFNGLBINDBUFFERARBPROC) 
		SDL_GL_GetProcAddress("glBindBufferARB");

		glGenBuffersARB = (PFNGLGENBUFFERSARBPROC) 
		SDL_GL_GetProcAddress("glGenBuffersARB");

		glBufferDataARB = (PFNGLBUFFERDATAARBPROC) 
		SDL_GL_GetProcAddress("glBufferDataARB");

		glCreateShaderObjectARB = (PFNGLCREATESHADEROBJECTARBPROC)
		SDL_GL_GetProcAddress("glCreateShaderObjectARB");

		glDeleteBuffersARB = (PFNGLDELETEBUFFERSARBPROC)
		SDL_GL_GetProcAddress("glDeleteBuffersARB");

		printf("Vertex Buffer Objects\t\t[OK]\n");
	}
	else
	{
		bVBOSupported = false;
	}

	if( ExtensionSupported("GL_ARB_fragment_shader") &&
		ExtensionSupported("GL_ARB_vertex_shader") &&
		ExtensionSupported("GL_ARB_shader_objects") )
	{
		bShadersSupported = true;

		glBindBufferARB = (PFNGLBINDBUFFERARBPROC)
		SDL_GL_GetProcAddress("glBindBufferARB");

		glGenBuffersARB = (PFNGLGENBUFFERSARBPROC)
		SDL_GL_GetProcAddress("glGenBuffersARB");

		glBufferDataARB = (PFNGLBUFFERDATAARBPROC)
		SDL_GL_GetProcAddress("glBufferDataARB");

		glUseProgramObjectARB = (PFNGLCREATESHADEROBJECTARBPROC)
		SDL_GL_GetProcAddress("glUseProgramObjectARB");

		glShaderSourceARB = (PFNGLSHADERSOURCEARBPROC)
		SDL_GL_GetProcAddress("glShaderSourceARB");

		glCompileShaderARB = (PFNGLCOMPILESHADERARBPROC)		
		SDL_GL_GetProcAddress("glCompileShaderARB");
		
		glGetObjectParameterivARB = (PFNGLGETOBJECTPARAMETERIVARBPROC)
		SDL_GL_GetProcAddress("glGetObjectParameterivARB");

		glAttachObjectARB = (PFNGLATTACHOBJECTARBPROC)
		SDL_GL_GetProcAddress("glAttachObjectARB");

		glCreateProgramObjectARB = (PFNGLCREATEPROGRAMOBJECTARBPROC)
		SDL_GL_GetProcAddress("glCreateProgramObjectARB");

		glGetInfoLogARB = (PFNGLGETINFOLOGARBPROC)
		SDL_GL_GetProcAddress("glGetInfoLogARB");

		glLinkProgramARB = (PFNGLLINKPROGRAMARBPROC)
		SDL_GL_GetProcAddress("glLinkProgramARB");

		glDeleteObjectARB = (PFNGLDELETEOBJECTARBPROC)
		SDL_GL_GetProcAddress("glDeleteObjectARB");

		glGetUniformLocationARB = (PFNGLGETUNIFORMLOCATIONARBPROC)
		SDL_GL_GetProcAddress("glGetUniformLocationARB");

		glUniform1iARB = (PFNGLUNIFORM1IARBPROC)
		SDL_GL_GetProcAddress("glUniform1iARB");

		printf("Vertex/Pixel Shaders\t\t[OK]\n");
	}
	else
	{
		bShadersSupported = false;
		printf("Vertex/Pixel Shaders\t\t[FAIL]\n");
	}

	printf("----------------------------------------\n\n");

	//printf("\n");
	return true;
}

//--------------------------------------------------------------------------------------

int Device::ExtensionSupported( const char* extension )
{
	const GLubyte *extensions = NULL;
	const GLubyte *start;
	GLubyte *where, *terminator;

	// Extension names should not have spaces. 
	where = (GLubyte *) strchr(extension, ' ');
	if( where || *extension == '\0' )
	{
		return 0;
	}

	extensions = glGetString(GL_EXTENSIONS);
	/* It takes a bit of care to be fool-proof about parsing the
	OpenGL extensions string. Don't be fooled by sub-strings,
	etc. */
	start = extensions;
	while(1)
	{
		where = (GLubyte *) strstr((const char *) start, extension);
		if( !where )
		{
			break;
		}

		terminator = where + strlen(extension);
		if( where == start || *(where - 1) == ' ' )
		{
			if( *terminator == ' ' || *terminator == '\0' )
			{
				return 1;
			}
		}

		start = terminator;
	}

	return 0;
}

//--------------------------------------------------------------------------------------

void Device::ToggleFullScreen()
{
	if( SDL_WM_ToggleFullScreen( windowHandle ) == 0 )
	{
		printf("Failed to Toggle Fullscreen mode: %s\n", SDL_GetError() );
	}
}

//--------------------------------------------------------------------------------------

void Device::SetCursor( bool visible )
{
	SDL_ShowCursor( SDL_DISABLE );
}

//--------------------------------------------------------------------------------------

void Device::SetWindowTitle( const char* title )
{
	SDL_WM_SetCaption( title, title );
}

//--------------------------------------------------------------------------------------

void Device::BeginRender()
{
	if( SDL_GetTicks() >= ticks )
	{
		fps = framesThisSecond;
		framesThisSecond = 0;
		ticks += 1000;
	}
	else
	{
		++framesThisSecond;
	}

	glStencilFunc(GL_ALWAYS, 0, ~0 );
	glClear( GL_STENCIL_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );
}

//--------------------------------------------------------------------------------------

void Device::EndRender()
{
	SDL_GL_SwapBuffers();
}

//--------------------------------------------------------------------------------------

void Device::GetDeviceSize( int& width, int& height )
{
	width = this->width;
	height = this->height;
}

//--------------------------------------------------------------------------------------

bool Device::VBOSupported()
{
	return bVBOSupported;
}

//--------------------------------------------------------------------------------------

bool Device::ShaderSupported()
{
	return bShadersSupported;
}

//--------------------------------------------------------------------------------------

unsigned int Device::GetFPS()
{
	return fps;
}