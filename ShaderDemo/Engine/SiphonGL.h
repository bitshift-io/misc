//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_SIPHONGL_
#define _SIPHON_SIPHONGL_

/**
 * This is my StdAfx file
 */
#define SAFE_DELETE(p){ if(p){delete p; p = 0;} }
#define SAFE_DELETE_ARRAY(p){ if(p){delete[] p; p = 0;} }

//--------------------------------------------------------------------------------------

#ifdef WIN32

	#pragma comment(lib, "OpenGL32.lib")
	#pragma comment(lib, "GLu32.lib")
	#pragma comment(lib, "GLaux.lib")
	#pragma comment(lib, "SDL.lib")
	#pragma comment(lib, "SDLmain.lib")

	#include <windows.h>

#endif

//--------------------------------------------------------------------------------------

#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <map>
#include <vector>
#include <list>

#include "Config.h"
#ifdef DEBUG

	#include "mmgr/mmgr.h"

#endif

#define GL_GLEXT_PROTOTYPES
#include "glext.h"

#include "Input.h"
#include "Device.h"
#include "FileMgr.h"
#include "Engine.h"
#include "Math3D.h"
#include "Light.h"
#include "Camera.h"

#include "Texture.h"
#include "Font.h"
#include "Shader.h"
#include "Material.h"

#include "RenderTree.h"
#include "Mesh.h"
#include "ImportDefs.h"
#include "MaterialDefs.h"
#include "DDS.h"



#endif
