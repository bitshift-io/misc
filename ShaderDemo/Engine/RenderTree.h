//--------------------------------------------------------------------------------------
//
//	(C) 2004 - Fabian Mathews 
//
//	Siphon Studios
//	SiphonGL engine 2.0
//
//--------------------------------------------------------------------------------------
#ifndef _SIPHON_RENDERTREE_
#define _SIPHON_RENDERTREE_

#include "SiphonGL.h"

/**
 * a state is a node in the render tree
 * it represents a opengl state
 */
class State
{
public:
	enum
	{
		STATE_ROOT,
		STATE_TEXTURE,
		STATE_SHADER,
		STATE_MESH
	};

	State();
	~State();

	/**
	 * insert mesh/material into tree,
	 * index is the number we call on the mesh render:
	 * eg. pMesh->Render( index )
	 */
	virtual void Insert( Material* pMat, Mesh* pMesh, unsigned int index ) = 0;

	/**
	 * this should do what it must do! set texture/shader/render what eva
	 */
	virtual unsigned int Render();

	/**
	 * search our children, and return the child,
	 * otherwise return 0
	 */
	State* FindInChildren( Material* pMat, Mesh* pMesh, unsigned int index );

	/**
	 * call this on the children
	 * to see if a child already exists
	 * with this texture/shader for example
	 */
	virtual bool Find( Material* pMat, Mesh* pMesh, unsigned int index ) = 0;

	virtual unsigned int GetType() = 0;

protected:
	State* parent;
	std::list<State*> children;
};

class ShaderState : public State
{
public:
	ShaderState();

	virtual void Insert( Material* pMat, Mesh* pMesh, unsigned int index );
	virtual unsigned int Render();
	virtual bool Find( Material* pMat, Mesh* pMesh, unsigned int index );
	virtual unsigned int GetType();
protected:
	Shader* pShader;
};

class TextureState : public State
{
public:
	TextureState();

	/**
	 * our texture unit is = 0
	 */
	virtual void Insert( Material* pMat, Mesh* pMesh, unsigned int index );

	/**
	 * because multiple texture can be linked, we need to know our depth
	 * so we know which unit to bind our texture to.
	 */
	virtual void Insert( Material* pMat, Mesh* pMesh, unsigned int index, unsigned int textureUnit );
	virtual unsigned int Render();
	virtual bool Find( Material* pMat, Mesh* pMesh, unsigned int index );
	virtual unsigned int GetType();
protected:
	Texture* pTexture;
	unsigned int textureUnit;
};

class MeshState : public State
{
public:
	MeshState();

	virtual void Insert( Material* pMat, Mesh* pMesh, unsigned int index );
	virtual unsigned int Render();
	virtual bool Find( Material* pMat, Mesh* pMesh, unsigned int index );
	virtual unsigned int GetType();
protected:
	Mesh* pMesh;
	Material* pMat;
	unsigned int index;
};

/**
 * what does this do?
 * you pass it a material, and a mesh,
 * and it puts it into a tree strucutre
 * which minimizes state changes for rendering
 * it is the root of the tree
 */
class RenderTree : public State
{
public:
	RenderTree();
	virtual void Insert( Material* pMat, Mesh* pMesh, unsigned int index );

protected:
	/**
	 * no need to find in the root, so hide it!
	 */
	bool Find( Material* pMat, Mesh* pMesh, unsigned int index );
	unsigned int GetType();
};

#endif