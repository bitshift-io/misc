// LINEWARS
// by Fabian Mathews
// (c) 2000


// INCLUDES
#include "windows.h"
#include "ddraw.h"
#include "ddutil.h"
#include "math.h"

// DEFINES
//#define playerwidth 6
int playerwidth=6;

// PROTOTYPES
LRESULT CALLBACK WndFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
BOOL InitWindow(HINSTANCE hThisInstance, int nCmdShow );
BOOL InitDirectDraw();
BOOL InitPlayers();
BOOL GameLoop();
BOOL AI(int ai);
BOOL Menu();
void OnDestroy();
void UpdateScreen();
void checkkeyboard();
void Display_Number(int player);

//used to determine when all but 1 player is dead to restart the level
int aliveplayers=6;
int deadplayers=0;

//to display numbers
LPDIRECTDRAWSURFACE numbers_offscreen;
int pow10[6]={1,10,100,1000,10000,100000}; 
int digit_xoffsets[10]={0,24,48,72,96,120,144,168,192,216};

// GLOBALS
HDC xdc; // the working dc

HWND hwnd;
const modewidth = 1024;
const modeheight = 768;

const modedepth = 16;
LPDIRECTDRAW ddraw;
LPDIRECTDRAWSURFACE primsurf;
LPDIRECTDRAWSURFACE backsurf;

//MENU stuff
int menuloop=1;

LPDIRECTDRAWSURFACE playerstate;
int state_xoffsets[10]={0,80,160,240};

LPDIRECTDRAWSURFACE menusurf;
RECT menurect;

//about(in menu)
LPDIRECTDRAWSURFACE aboutsurf;
RECT aboutrect;
int about=0;

//cursor stuff
LPDIRECTDRAWSURFACE cursorsurf;
RECT cursorrect;
int cursor;

//AI
int pixeltestchoice;

//ALL PLAYERS
int player=0; //used in my loops to tell which player it is
int delay=10;
double turningangle=0.087;

//Used for gaps:
LPDIRECTDRAWSURFACE blank;
RECT blankrect;
int nogap=50;
int gap=20;
int gaptotal=nogap+gap;
int gaptimer=0;
int gapchanger=0;

//player array settings
const noplayers=6;
double array[noplayers][20];

// the following may be accessed by AI or HUMAN
const x=0;
const y=1;
const angle=2;

// set by the game
const state=3; //1=alive, 0=dead, 2=AI
const setstate=12; //this is the state set by the user
const score=4;


//acessed ONLY by AI
const direction=5;
const timer=6;
const endtimer=7;

//used for gaps
const xold=8;
const yold=9;

const xold2=10;
const yold2=11;


// settings that cant go into the array
LPDIRECTDRAWSURFACE ply1;
const p1=0;
RECT p1rect;

LPDIRECTDRAWSURFACE ply2;
const p2=1;
RECT p2rect;

LPDIRECTDRAWSURFACE ply3;
const p3=2;
RECT p3rect;

LPDIRECTDRAWSURFACE ply4;
const p4=3;
RECT p4rect;

LPDIRECTDRAWSURFACE ply5;
const p5=4;
RECT p5rect;

LPDIRECTDRAWSURFACE ply6;
const p6=5;
RECT p6rect;



// WINMAIN
int WINAPI WinMain (HINSTANCE hThisInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{


    MSG msg;
    lpszCmdParam = lpszCmdParam;
    hPrevInstance = hPrevInstance;

    if(!InitWindow(hThisInstance, nCmdShow)) return FALSE;
	if(!InitDirectDraw()) return FALSE;

	if(!InitPlayers()) return FALSE;
	if(!Menu()) return FALSE;
	

    while(1)
    {
		if(PeekMessage(&msg, NULL,0,0,PM_NOREMOVE))
		{
			if(!GetMessage (&msg, NULL, 0, 0)) return msg.wParam;
			TranslateMessage (&msg);
			DispatchMessage (&msg);        
		}
		else 
		{ 

			GameLoop();

		}
	}

    return msg.wParam;
}


// WNDFUNC 
LRESULT CALLBACK WndFunc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
     HDC         hdc;
     PAINTSTRUCT ps;

	switch (message)
    {
		case WM_PAINT:
			hdc = BeginPaint (hwnd, &ps);
			EndPaint (hwnd, &ps);
			break;

        case WM_KEYDOWN:
			switch(wParam)
            {
			//	case VK_ESCAPE:
			//	PostQuitMessage(0);
			//	break;

            }
            break;

        case WM_DESTROY:
			OnDestroy();
			PostQuitMessage (0);
			break;
    }

    return DefWindowProc (hwnd, message, wParam, lParam);
}

// ONDESTROY
void OnDestroy()
{
	ddraw->Release();ddraw=0;
	primsurf->Release();primsurf=0;
	backsurf->Release();backsurf=0;
}

// INITWINDOW
BOOL InitWindow( HINSTANCE hThisInstance, int nCmdShow )
{
     WNDCLASS    wndclass;
     char szAppName[] = "Line Wars    by Fabian Mathews";

     wndclass.style         = CS_DBLCLKS;
     wndclass.lpfnWndProc   = WndFunc;		 // window function
     wndclass.cbClsExtra    = 0;			 // no extra count of bytes
     wndclass.cbWndExtra    = 0;			 // no extra count of bytes
     wndclass.hInstance     = hThisInstance; // this instance
     wndclass.hIcon         = LoadIcon (NULL, IDI_APPLICATION);
     wndclass.hCursor       = LoadCursor (NULL, IDC_ARROW);
     wndclass.hbrBackground = (HBRUSH) GetStockObject (BLACK_BRUSH);
     wndclass.lpszMenuName  = NULL;
     wndclass.lpszClassName = szAppName;

     RegisterClass (&wndclass);

     hwnd = CreateWindowEx (
                    WS_EX_TOPMOST,         
                    szAppName,             
                    "Line Wars    by Fabian Mathews",
                    WS_VISIBLE | WS_POPUP, 
                    CW_USEDEFAULT,         
                    CW_USEDEFAULT,         
                    modewidth,                   
                    modeheight,                 
                    NULL,                  
                    NULL,                  
                    hThisInstance,        
                    NULL);              

     if (!hwnd)
		return FALSE;

     ShowWindow (hwnd, nCmdShow);
     UpdateWindow (hwnd);
     SetFocus(hwnd);
     ShowCursor(FALSE);
	
     return TRUE;
}

//MENU
BOOL Menu()
{	
	InitPlayers();//this resets the scores and starting positions

	//load ABOUT page into memory
	aboutsurf= DDLoadBitmap(ddraw,"about.bmp",824,590);
	aboutrect.top=0;
	aboutrect.left=0;
	aboutrect.right=824;
	aboutrect.bottom=590;
	int aboutx=195;
	int abouty=178;
	about=0;

	//load the cursor
	int cursor=0;
	int cursorx=332;
	int cursory=195;

	cursorsurf= DDLoadBitmap(ddraw,"cursor.bmp",299,41);
	DDSetColorKey(cursorsurf,RGB(0,0,0));
	cursorrect.top=0;
	cursorrect.left=0;
	cursorrect.right=299;
	cursorrect.bottom=41;
	
	int gloop=0;
	while(gloop==0)
	{

		if(GetAsyncKeyState(VK_ESCAPE))
		{
			if(about==1)
			{
				about=0;
			}
			else
			{
				PostQuitMessage(0);
				gloop=1;
			}
		}
		if(GetAsyncKeyState(VK_DOWN))
		{
			cursory=cursory+40;
			if(cursory>715)
			{
				cursory=195;
			}
		}
		if(GetAsyncKeyState(VK_UP))
		{
			cursory=cursory-40;
			if(cursory<195)
			{
				cursory=715;
			}
		}

		if(GetAsyncKeyState(VK_RIGHT))
		{
			//this loop cheaks where the cursur is and if it compares to the player, change
			// thier STATE
			for(player=0;player<noplayers;player++)
			{
				if( ((cursory-195)/40)-2==player )
				{
					array[player][state]++;
					if(array[player][state]>2)
					{
						array[player][state]=0;
					}
				}
			}
			if( (cursory-195)/40==12 )//GAME SPEED
			{
				delay++;
			}
			if( (cursory-195)/40==8)//GAP DISTANCE
			{
				gap++;	
			}
			if( (cursory-195)/40==9)//LINE DISTANCE
			{
				nogap++;	
			}
			if( (cursory-195)/40==11)//PLAYER WIDTH(LINE THICKNESS)
			{
				playerwidth++;
				if(playerwidth>=20)
				{
					playerwidth=20;
				}
			}
			if( (cursory-195)/40==10)//TURNING ANGLE
			{
				turningangle=turningangle+0.0175;
			}

		}

		if(GetAsyncKeyState(VK_LEFT))
		{
			//this loop cheaks where the cursur is and if it compares to the player, change
			// thier STATE
			for(player=0;player<noplayers;player++)
			{
				if( ((cursory-195)/40)-2==player )
				{
					array[player][state]--;
					if(array[player][state]<0)
					{
						array[player][state]=2;
					}
				}
			}
			if( (cursory-195)/40==12 )//GAME SPEED
			{
				delay--;	
			}
			if( (cursory-195)/40==8)//GAP DISTANCE
			{
				gap--;	
			}
			if( (cursory-195)/40==9)//LINE DISTANCE
			{
				nogap--;	
			}
			if( (cursory-195)/40==11)//PLAYER WIDTH(LINE THICKNESS)
			{
				playerwidth--;
				if(playerwidth<=3)
				{
					playerwidth=3;
				}
			}
			if( (cursory-195)/40==10)//TURNING ANGLE
			{
				turningangle=turningangle-0.0175;				
			}


		}

		if(GetAsyncKeyState(VK_RETURN))
		{
			if( (cursory-195)/40==0 )//PLAY
			{					
				for(player=0;player<noplayers;player++)
				{
					if(array[player][state]==0)
					{
						array[player][state]=-1;
					}
				}
				gloop=1;
			}
			if( (cursory-195)/40==1 )//EXIT
			{
					PostQuitMessage(0);
					gloop=1;
			}
			if( (cursory-195)/40==13 )//ABOUT
			{	
				if(about==1)
				{
					about=0; //1 means being displayed, 0 is not
				}
				else
				{
					about=1;
				}
			}
		}
	

		DDReLoadBitmap(backsurf, "menu.bmp");
		backsurf->BltFast(cursorx,cursory,cursorsurf,&cursorrect,DDBLTFAST_SRCCOLORKEY);
		
		
		//update player STATE on screen
			//load numbers
		playerstate = DDLoadBitmap(ddraw, "playerstate.bmp", 246,28);
		DDSetColorKey(playerstate,RGB(0,0,0));
		for(player=0;player<noplayers;player++)
		{
			int digitx=565;
			int digity=( (player+2)*40)+200;
			int number=array[player][state];

			int i;
			int digit;
			int powerof10;
			RECT rect;
			
			rect.top=0; 
			rect.bottom=28;
			for(i=0;i>=0;i--) 
			{
				powerof10 = pow10[i];
				digit = number/powerof10;
				number -= powerof10*digit;
				rect.left = state_xoffsets[digit]+2;
				rect.right = rect.left + 80;
				backsurf->BltFast(digitx+(5-i)*24,digity,playerstate,&rect,DDBLTFAST_NOCOLORKEY);
			}
		}


		//update gamespeed
		//delay=20;
			int digitx=610;
			int digity=(12*40)+200;
			int number=delay;

			int i;
			int digit;
			int powerof10;
			RECT rect;
			
			rect.top=0; 
			rect.bottom=28;
			for(i=1;i>=0;i--) 
			{
				powerof10 = pow10[i];
				digit = number/powerof10;
				number -= powerof10*digit;
				rect.left = digit_xoffsets[digit]+2;
				rect.right = rect.left + 23;
				backsurf->BltFast(digitx+(5-i)*24,digity,numbers_offscreen,&rect,DDBLTFAST_NOCOLORKEY);
			}
			
		//update dap Distance
			digitx=610;
			digity=(8*40)+200;
			number=gap;

			rect.top=0; 
			rect.bottom=28;
			for(i=1;i>=0;i--) 
			{
				powerof10 = pow10[i];
				digit = number/powerof10;
				number -= powerof10*digit;
				rect.left = digit_xoffsets[digit]+2;
				rect.right = rect.left + 23;
				backsurf->BltFast(digitx+(5-i)*24,digity,numbers_offscreen,&rect,DDBLTFAST_NOCOLORKEY);
			}

		//update none dap Distance
			digitx=610;
			digity=(9*40)+200;
			number=nogap;

			rect.top=0; 
			rect.bottom=28;
			for(i=1;i>=0;i--) 
			{
				powerof10 = pow10[i];
				digit = number/powerof10;
				number -= powerof10*digit;
				rect.left = digit_xoffsets[digit]+2;
				rect.right = rect.left + 23;
				backsurf->BltFast(digitx+(5-i)*24,digity,numbers_offscreen,&rect,DDBLTFAST_NOCOLORKEY);
			}

		//update PLAYERWIDTH(LINE THICKNESS)
		//delay=20;
			digitx=610;
			digity=(11*40)+200;
			number=playerwidth;
			
			rect.top=0; 
			rect.bottom=28;
			for(i=1;i>=0;i--) 
			{
				powerof10 = pow10[i];
				digit = number/powerof10;
				number -= powerof10*digit;
				rect.left = digit_xoffsets[digit]+2;
				rect.right = rect.left + 23;
				backsurf->BltFast(digitx+(5-i)*24,digity,numbers_offscreen,&rect,DDBLTFAST_NOCOLORKEY);
			}

		//update TURNING ANGLE
		//delay=20;
			digitx=610;
			digity=(10*40)+200;
			number=(turningangle*180)/3.1416;
			
			rect.top=0; 
			rect.bottom=28;
			for(i=1;i>=0;i--) 
			{
				powerof10 = pow10[i];
				digit = number/powerof10;
				number -= powerof10*digit;
				rect.left = digit_xoffsets[digit]+2;
				rect.right = rect.left + 23;
				backsurf->BltFast(digitx+(5-i)*24,digity,numbers_offscreen,&rect,DDBLTFAST_NOCOLORKEY);
			}

			//if ABOUT is "on" then overwrite everything else	
			if(about==1)
			{
				backsurf->BltFast(aboutx,abouty,aboutsurf,&aboutrect,DDBLTFAST_NOCOLORKEY);
			}

		//Draws screen
		RECT menurect;
		menurect.top = 0;
		menurect.left = 0;
		menurect.right = modewidth;
		menurect.bottom = modeheight;

		primsurf->BltFast(0,0,backsurf,&menurect,DDBLTFAST_NOCOLORKEY);	

	}//end while loop


	//fixes gap stuff to be used
	gaptotal=nogap+gap;

	//this loop sets it so we can re-use the player states later
	for(player=0;player<noplayers;player++)
	{
		array[player][setstate]=array[player][state];
	}

	//this loop tells me how many players alive there are
	// this is used to restart the level when all but 1 player is dead
	aliveplayers=6;
	for(player=0;player<noplayers;player++)
	{
		if(array[player][state]==-1)
		{	
			aliveplayers--;
		}
	}

	//end of menu- so load game stuffs				
	DDReLoadBitmap(backsurf, "background.bmp");

	// this draws the screen so the lines do not do a collision detection with the menu
	RECT menurect;
	menurect.top = 0;
	menurect.left = 0;
	menurect.right = modewidth;
	menurect.bottom = modeheight;

	primsurf->BltFast(0,0,backsurf,&menurect,DDBLTFAST_NOCOLORKEY);	


	return TRUE;
}

// INITDIRECTDRAW
BOOL InitDirectDraw()
{
	DirectDrawCreate(0,&ddraw,0);
	ddraw->SetCooperativeLevel( hwnd, DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN | DDSCL_ALLOWMODEX | DDSCL_ALLOWREBOOT);
	ddraw->SetDisplayMode( modewidth, modeheight, modedepth );

	//primary surface
	DDSURFACEDESC desc;
	desc.dwSize = sizeof(desc);
	desc.dwFlags = DDSD_BACKBUFFERCOUNT | DDSD_CAPS;
	desc.dwBackBufferCount = 1;
	desc.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE | DDSCAPS_FLIP | DDSCAPS_COMPLEX;
	ddraw->CreateSurface(&desc,&primsurf,0);
	//backbuffer surface
	DDSCAPS ddscaps;
	ddscaps.dwCaps = DDSCAPS_BACKBUFFER;
	primsurf->GetAttachedSurface(&ddscaps,&backsurf);

	//load numbers
	numbers_offscreen = DDLoadBitmap(ddraw, "numberpic.bmp", 246,28);
	DDSetColorKey(numbers_offscreen,RGB(0,0,0));

	return TRUE;
}

BOOL InitPlayers()
{

	//loadimage bitmap-background
	DDReLoadBitmap(backsurf, "background.bmp"); 

	for(player=0;player<noplayers;player++)
	{
		array[player][x]=100+rand()%650;
		array[player][y]=100+rand()%450;
		array[player][state]=1;//human defult
		array[player][angle]=rand()%6;
		array[player][timer]=0;
		array[player][score]=0;
		array[player][endtimer]=rand()%10;	
	}
	
	p1rect.top = 0;
	p1rect.left = 0;
	p1rect.bottom = playerwidth;
	p1rect.right = playerwidth;

	ply1= DDLoadBitmap(ddraw,"ply1.bmp",playerwidth,playerwidth);
	DDSetColorKey(ply1,RGB(0,0,0));

	
	p2rect.top = 0;
	p2rect.left = 0;
	p2rect.bottom = playerwidth;
	p2rect.right = playerwidth;


	ply2= DDLoadBitmap(ddraw,"ply2.bmp",playerwidth,playerwidth);
	DDSetColorKey(ply2,RGB(0,0,0));

	
	p3rect.top = 0;
	p3rect.left = 0;
	p3rect.bottom = playerwidth;
	p3rect.right = playerwidth;
	array[p3][state]=2;//AI

	ply3= DDLoadBitmap(ddraw,"ply3.bmp",playerwidth,playerwidth);
	DDSetColorKey(ply3,RGB(0,0,0));

	
	p4rect.top = 0;
	p4rect.left = 0;
	p4rect.bottom = playerwidth;
	p4rect.right = playerwidth;
	array[p4][state]=2;//AI

	ply4= DDLoadBitmap(ddraw,"ply4.bmp",playerwidth,playerwidth);
	DDSetColorKey(ply4,RGB(0,0,0));

	
	p5rect.top = 0;
	p5rect.left = 0;
	p5rect.bottom = playerwidth;
	p5rect.right = playerwidth;
	array[p5][state]=2;//AI

	ply5= DDLoadBitmap(ddraw,"ply5.bmp",playerwidth,playerwidth);
	DDSetColorKey(ply5,RGB(0,0,0));

	
	p6rect.top = 0;
	p6rect.left = 0;
	p6rect.bottom = playerwidth;
	p6rect.right = playerwidth;
	array[p6][state]=2;//AI

	ply6= DDLoadBitmap(ddraw,"ply6.bmp",playerwidth,playerwidth);
	DDSetColorKey(ply6,RGB(0,0,0));

	
	blankrect.top = 0;
	blankrect.left = 0;
	blankrect.bottom = playerwidth;
	blankrect.right = playerwidth;

	blank= DDLoadBitmap(ddraw,"blank.bmp",playerwidth,playerwidth);
	DDSetColorKey(blank,RGB(255,255,255));
	

	//display scores	
	for(player=0;player<noplayers;player++)
	{
		Display_Number(player);
	}


	return TRUE;
}

void Display_Number(int player)
{

	//scorex and y determine the postion on the screen the score goes
	int scorex=830;
	//int scorex=620;
	// this line evenly spaces out the numbers
	// and should fit nicely into the background
	int scorey=(player*110)+100;

	int number=array[player][score];	

	int i;
	int digit;
	int powerof10;
	RECT rect;
	
	rect.top=0; 
	rect.bottom=28;
	for(i=2;i>=0;i--) 
	{
		powerof10 = pow10[i];
		digit = number/powerof10;
		number -= powerof10*digit;
		rect.left = digit_xoffsets[digit]+2;
		rect.right = rect.left + 23;
		backsurf->BltFast(scorex+(5-i)*24,scorey,numbers_offscreen,&rect,DDBLTFAST_NOCOLORKEY);
	}
}

void UpdateScreen()
{
	RECT rect;
	rect.top = 0;
	rect.left = 0;
	rect.right = modewidth;
	rect.bottom = modeheight;


	primsurf->BltFast(0,0,backsurf,&rect,DDBLTFAST_NOCOLORKEY);

	//if gap=1(gap) blank behind the players
	if(gapchanger==1)
	{
		for(player=0;player<noplayers;player++)
		{
			backsurf->BltFast(array[player][xold2],array[player][yold2],blank,&blankrect,DDBLTFAST_SRCCOLORKEY);
		}
	}

	//Test player state to see if player is alive, 1=alive 0=dead, 2=AI
	// did >0 to help with the scorring system
	if(array[p1][state]>=0)
	{
		backsurf->BltFast(array[p1][x],array[p1][y],ply1,&p1rect,DDBLTFAST_SRCCOLORKEY);
	}
	if(array[p2][state]>=0)
	{
		backsurf->BltFast(array[p2][x],array[p2][y],ply2,&p2rect,DDBLTFAST_SRCCOLORKEY);
	}
	if(array[p3][state]>=0)
	{
		backsurf->BltFast(array[p3][x],array[p3][y],ply3,&p3rect,DDBLTFAST_SRCCOLORKEY);
	}
	if(array[p4][state]>=0)
	{
		backsurf->BltFast(array[p4][x],array[p4][y],ply4,&p4rect,DDBLTFAST_SRCCOLORKEY);
	}
	if(array[p5][state]>=0)
	{
		backsurf->BltFast(array[p5][x],array[p5][y],ply5,&p5rect,DDBLTFAST_SRCCOLORKEY);
	}
	if(array[p6][state]>=0)
	{
		backsurf->BltFast(array[p6][x],array[p6][y],ply6,&p6rect,DDBLTFAST_SRCCOLORKEY);
	}



}


void checkkeyboard()
{
	//moving players
	if(GetAsyncKeyState(VK_LEFT))
	{		
		array[p1][angle]=array[p1][angle]-turningangle;
	}
	if(GetAsyncKeyState(VK_RIGHT))
	{
		array[p1][angle]=array[p1][angle]+turningangle;
	}

	if(GetAsyncKeyState(VK_TAB))
	{		
		array[p2][angle]=array[p2][angle]-turningangle;
	}
	if(GetAsyncKeyState(VK_SPACE))
	{
		array[p2][angle]=array[p2][angle]+turningangle;
	}
	if(GetAsyncKeyState(VK_ESCAPE))
	{
		Menu();

	}




}

BOOL GameLoop()
{

	int timedelay=GetTickCount();//timmer



	//checks player STATE if equal to 2=AI, call AI which moves it
	for(player=0;player<noplayers;player++)
	{
		if(array[player][state]==2)
		{
			AI(player);
		}
	}



	checkkeyboard();

	//update x and y coords according to angle	
	for(player=0;player<noplayers;player++)
	{
		if(array[player][state]>0)
		{
			//xold2 and xold are used for gaps, they stop players
			//flashing when drawing gaps
			array[player][xold2]=array[player][xold];
			array[player][yold2]=array[player][yold];

			array[player][xold]=array[player][x];
			array[player][yold]=array[player][y];

			array[player][x]=array[player][x]+cos(array[player][angle]);
			array[player][y]=array[player][y]+sin(array[player][angle]);
		}
	}
		


			

	// Collision detection
	// GetDC return control to windows and ReleaseDC return to DX
	// get the dc from surface
	if (primsurf->GetDC(&xdc)!=DD_OK)
	return(0);

	for(player=0;player<noplayers;player++)
	{

		//need to stop it cheaking if the player is dead or else the score keeps going up 
		if(array[player][state]>0)
		{
			if(GetPixel(xdc,array[player][x]+(playerwidth-1)*cos(array[player][angle]),
					array[player][y]+(playerwidth-1)*sin(array[player][angle]))!=RGB(0,0,0))
			{
				//kill player
				array[player][state]=0;
			}
		}

	}

	// release the dc
	primsurf->ReleaseDC(xdc);


	//the following makes gaps by setting
	//gapchanger
	gaptimer++;
	if(gaptimer==nogap)
	{		
		gapchanger=1;
	}
	if(gaptimer==gaptotal)
	{
		gapchanger=0;
		gaptimer=0;
	}

	UpdateScreen();//draws the screen

	//scoreing system
	for(player=0;player<noplayers;player++)
	{
		//see if player just died
		if(array[player][state]==0)
		{
			array[player][state]=-1;
			deadplayers++;
			
			//increse all scores exept the dead people
			for(player=0;player<noplayers;player++)
			{				
				if(array[player][state]>0)
				{		
					array[player][score]++;		
				}
			}

		}
		Display_Number(player);
	}


	//tests the number of players that are alive against
	// those that are dead to see if it should go back and 
	//restart the game
	if(deadplayers==(aliveplayers-1) )
	{
		deadplayers=0;

		//time delay
		int timedelay=GetTickCount();
		while(GetTickCount()-timedelay < 2000);

		DDReLoadBitmap(backsurf, "background.bmp"); 
		for(player=0;player<noplayers;player++)
		{
			array[player][state]=array[player][setstate];
			array[player][x]=100+rand()%650;
			array[player][y]=100+rand()%450;
			array[player][angle]=rand()%6;
			array[player][timer]=0;
			array[player][endtimer]=rand()%10;
		}

		//display scores	
		for(player=0;player<noplayers;player++)
		{
			Display_Number(player);
		}
	//	Menu();
	}






	while(GetTickCount()-timedelay < delay);//timmer

	return TRUE;
}



BOOL AI(int ai)
{

	//The following code, helps make the AI more human like and not all 'wobbly'
	array[ai][timer]++;

	if(array[ai][timer]>=array[ai][endtimer])
	{
		array[ai][endtimer]=rand()%15;
		array[ai][timer]=0;
		array[ai][direction]=rand()%10;
	}


			// Collision detection for AI, this is a prong shaped collsion detection
			// GetDC return control to windows and ReleaseDC return to DX
			// get the dc from surface
			if (primsurf->GetDC(&xdc)!=DD_OK)
			return(0);
			
			pixeltestchoice=rand()%2;

			double aiangle=0.785;

			
			//Left collision test
			if(GetPixel(xdc,array[ai][x]+((playerwidth+25)*cos(array[ai][angle]+aiangle)),
					array[ai][y]+((playerwidth+25)*sin(array[ai][angle]+aiangle)))!=RGB(0,0,0))
			{
				array[ai][timer]=0;
				array[ai][endtimer]=rand()%10+15;
				array[ai][direction]=1;
			}
	
			//Right collision test
			if(GetPixel(xdc,array[ai][x]+((playerwidth+25)*cos(array[ai][angle]-aiangle)),
					array[ai][y]+((playerwidth+25)*sin(array[ai][angle]-aiangle)))!=RGB(0,0,0))
			{
				array[ai][timer]=0;
				array[ai][endtimer]=rand()%10+15;
				array[ai][direction]=0;
			}

			//Striaght/Close collision test
			if(GetPixel(xdc,array[ai][x]+((playerwidth+15)*cos(array[ai][angle])),
					array[ai][y]+((playerwidth+15)*sin(array[ai][angle])))!=RGB(0,0,0))
			{
				if(pixeltestchoice>=1)
				{
					array[ai][endtimer]=rand()%10+20;
					array[ai][direction]=0;
				}
				if(pixeltestchoice<1)
				{
					array[ai][endtimer]=rand()%10+20;
					array[ai][direction]=1;
				}
				array[ai][timer]=0;
				//direction=0;
			}
			
			

			// release the dc
			primsurf->ReleaseDC(xdc);
		


	// Adjust direction acording to settings
	if(array[ai][direction]<1)
	{
			array[ai][angle]=array[ai][angle]+turningangle;
	}
	if(array[ai][direction]>=1 && array[ai][direction]<2)
	{
			array[ai][angle]=array[ai][angle]-turningangle;
	}
	if(array[ai][direction]>=2)
	{
			array[ai][angle]=array[ai][angle];
	}


	return TRUE;
}