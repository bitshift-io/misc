﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections;

namespace ObjectTrackingAndRecognition
{
    // uses a neural net to guess the trajectory of an object

    // things to try, feed noramlised values for x, y, w, h - 4 inputs instead of as bit array
    // try making history relative to each other or give it velocity history instead of absolute history
    public class Tracker
    {
        NNet m_net = new NNet();
        List<Rectangle> m_history = new List<Rectangle>();
        int m_historyLength = 2;
        int m_inputsPerRect;

        Rectangle m_output;

        public Tracker()
        {
            NNet test = new NNet();
            test.test();

            Rectangle temp = new Rectangle();
            List<double> tempBytes = RectangleToInputs(temp);
            m_inputsPerRect = tempBytes.Count;
            
            NeuronLayer[] layerList = new NeuronLayer[1];
            layerList[0] = new NeuronLayer(m_inputsPerRect);
            m_net.create(m_inputsPerRect * m_historyLength, layerList);

            // populate history with empty rects
            for (int i = 0; i < m_historyLength; ++i)
            {
                m_history.Add(new Rectangle(0, 0, 0, 0));
            }
        }

        public Rectangle Output()
        {
            return m_output;
        }

        public List<double> RectangleToInputs(Rectangle rect)
        {
            int[] values = new int[4];
            values[0] = rect.X;
            values[1] = rect.Y;
            values[2] = rect.Width;
            values[3] = rect.Height;

            BitArray bitArray = new BitArray(values);

            List<double> result = new List<double>(bitArray.Count);
            for (int i = 0; i < bitArray.Count; ++i)
            {
                result.Add(bitArray.Get(i) ? 1.0 : 0.0);
            }

            return result;
        }
        
        public Rectangle RectangleFromOutputs(List<double> outputList)
        {
            BitArray bitArray = new BitArray(outputList.Count);
            for (int i = 0; i < outputList.Count; ++i)
            {
                bitArray[i] = (outputList[i] > 0.5) ? true : false;
            }

            int[] values = new int[4];
            bitArray.CopyTo(values, 0);

            Rectangle rect = new Rectangle();
            rect.X = values[0];
            rect.Y = values[1];
            rect.Width = values[2];
            rect.Height = values[3];
            return rect;
        }
        
        public void Track(Rectangle rect)
        {
            List<double> inputList = new List<double>(m_inputsPerRect * m_historyLength);
            foreach (Rectangle r in m_history)
            {
                inputList.AddRange(RectangleToInputs(r));
            }
            
            List<double> expectedOutputList = new List<double>(m_inputsPerRect);
            expectedOutputList.AddRange(RectangleToInputs(rect));
            double[] actualOutputArray = m_net.Train(inputList.ToArray(), expectedOutputList.ToArray());
            List<double> actualOutputList = new List<double>();
            actualOutputList.AddRange(actualOutputArray);
            m_output = RectangleFromOutputs(actualOutputList);

            Console.WriteLine("Input/Expected rect: X: " + rect.X + " Y: " + rect.Y + " W: " + rect.Width + " H: " + rect.Height);
            Console.WriteLine("Output rect:         X: " + m_output.X + " Y: " + m_output.Y + " W: " + m_output.Width + " H: " + m_output.Height);

            m_history.Add(rect);
            if (m_history.Count > m_historyLength)
            {
                m_history.RemoveAt(0);
            }
        }
    }
}
