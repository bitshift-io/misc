﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Timers;

namespace ObjectTrackingAndRecognition
{
    public partial class Form1 : Form
    {

        Tracker m_tracker = new Tracker();
        Rectangle m_rect;
        System.Timers.Timer m_timer;

        public Form1()
        {
            InitializeComponent();

            m_timer = new System.Timers.Timer();
            m_timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            m_timer.Interval = 1000;
            //m_timer.Start();
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            MethodInvoker mi = new MethodInvoker(UpdateTacking);
            BeginInvoke(mi);
        }

        private void UpdateTacking()
        {
            m_timer.Stop();

            if (m_rect != null)
            {
                m_tracker.Track(m_rect);
            }

            m_timer.Start();
            this.Invalidate();
        }
        
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {/*
            Graphics g = Graphics.FromImage(pictureBox1.Image);
            g.Clear(System.Drawing.Color.White);

            g.Dispose();
            pictureBox1.Refresh();*/
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Bitmap bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            Graphics g = Graphics.FromImage(bmp);
            g.Clear(System.Drawing.Color.Black);

            Point pos = System.Windows.Forms.Control.MousePosition;
            Point localPos = pictureBox1.PointToClient(pos);

            m_rect = new Rectangle(localPos, new Size(10, 10));
            g.DrawRectangle(new Pen(Color.FromArgb(255, 0, 0)), m_rect);
            //g.DrawImage(pictureBox2.Image, localPos.X, localPos.Y, pictureBox2.Width, pictureBox2.Height);

            
            if (m_tracker.Output() != null)
            {
                g.DrawRectangle(new Pen(Color.FromArgb(0, 255, 0)), m_tracker.Output());
            }

            g.Dispose();
            pictureBox1.Image = bmp;
            pictureBox1.Refresh();
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_rect != null)
            {
                m_tracker.Track(m_rect);
            }

            this.Invalidate();
        }
    }
}
