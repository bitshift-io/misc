#ifndef _SOUNDSOURCE_H_
#define _SOUNDSOURCE_H_

#include "Math/Vector.h"

#ifdef MULTITHREAD
	#include "Multithread/Semaphore.h"
#endif

class SoundBuffer;

//
// A sound which is playing
//
class SoundSource
{
	friend class SoundFactory;
	friend class SoundEffect;
	friend class SoundBuffer;

public:

	bool Valid()								{ return mAlSource != 0; }
	void SetBuffer(SoundBuffer* buffer, bool semaAcquire = true);

	void SetRadius(float radius);
	void SetPosition(const Vector4& position);
	void SetVelocity(const Vector4& velocity);
	void SetSourceRelative(bool relative);
	void Play(bool semaAcquire = true);
	void Stop(bool semaAcquire = true);
	bool IsPlaying(bool semaAcquire = true);
	void SetLooping(bool state);
	void SetPitch(float pitch);
	void SetGain(float gain);
	bool IsStreaming();
	bool IsLooping();

	float	GetGain();

	inline bool InUse()					{ return mInUse; }
	inline void SetInUse(bool inUse)	{ mInUse = inUse; }

	SoundBuffer*	GetBuffer()			{ return mBuffer; }

	bool			Update();

protected:

	SoundSource(SoundFactory* factory);
	~SoundSource();

	SoundBuffer*	mBuffer;
	SoundFactory*	mFactory;
	unsigned int	mAlSource;
	bool			mInUse;
	bool			mLooping;
	bool			mPlaying;

#ifdef MULTITHREAD
	Semaphore		mSemaphore;
#endif
};

#endif