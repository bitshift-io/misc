#include "SoundSource.h"
#include "SoundBuffer.h"
#include "SoundFactory.h"
#include "al.h"
#include "File/Log.h"

SoundSource::SoundSource(SoundFactory* factory) :
	mInUse(false),
	mAlSource(0),
	mBuffer(0),
	mFactory(factory),
	mLooping(false),
	mPlaying(false)
{
#ifdef MULTITHREAD
	mSemaphore.Init();
#endif

	alGenSources(1, &mAlSource);
	while (alGetError())
	{
	}
	//if (error != AL_NO_ERROR)
	//	Log::Print("OpenAL Error: %s\n", alGetString(error));
}

SoundSource::~SoundSource()
{
	alDeleteSources(1, &mAlSource);
	while (alGetError())
	{
	}

#ifdef MULTITHREAD
	mSemaphore.Deinit();
#endif
}

void SoundSource::SetRadius(float radius)
{
	alSourcef(mAlSource, AL_REFERENCE_DISTANCE, radius);
	alSourcef(mAlSource, AL_ROLLOFF_FACTOR, radius / 4);
}

void SoundSource::SetPitch(float pitch)
{
	alSourcef(mAlSource, AL_PITCH, pitch);
}

float SoundSource::GetGain()
{
	ALfloat gain;
	alGetSourcef(mAlSource, AL_GAIN, &gain);
	return gain;
}

void SoundSource::SetGain(float gain)
{
	if (gain > 0.f)
	{
		int nothing = 0;
	}

	alSourcef(mAlSource, AL_GAIN, gain);
}

void SoundSource::SetLooping(bool state)
{
	if (mBuffer->IsStreaming())
		mLooping = state;
	else
		alSourcei(mAlSource, AL_LOOPING, state);
}

bool SoundSource::IsLooping()
{
	if (mBuffer->IsStreaming())
		return mLooping;

	ALint looping;
	alGetSourcei(mAlSource, AL_LOOPING, &looping);
	return looping;
}

void SoundSource::SetPosition(const Vector4& position)
{
	alSource3f(mAlSource, AL_POSITION, position.x, position.y, position.z);
}

void SoundSource::SetVelocity(const Vector4& velocity)
{
	alSource3f(mAlSource, AL_VELOCITY, velocity.x, velocity.y, velocity.z);
}

void SoundSource::SetBuffer(SoundBuffer* buffer, bool semaAcquire)
{
#ifdef MULTITHREAD
	if (semaAcquire)
		mSemaphore.Acquire();
#endif

	if (mBuffer && IsStreaming())
	{
		// hack around for a bug with streaming sounds
		int processed = 0;
		alGetSourcei(mAlSource, AL_BUFFERS_PROCESSED, &processed);

		// detach buffers
		//int queued = 0;
		//alGetSourcei(mAlSource, AL_BUFFERS_QUEUED, &queued);
	    
		while (processed--) //queued--)
		{
			ALuint buffer;
			alSourceUnqueueBuffers(mAlSource, 1, &buffer);

			ALenum error = alGetError();
			if (error != AL_NO_ERROR)
				WARN("OpenAL Error: %s\n", alGetString(error));
		}
	}

	mBuffer = buffer;
	if (!mBuffer)
	{
#ifdef MULTITHREAD
		if (semaAcquire)
			mSemaphore.Release();
#endif

		return;
	}

	if (buffer->IsStreaming())
	{
		alSourceQueueBuffers(mAlSource, 2, buffer->mAlSampleSet);
	}
	else
	{
		alSourcei(mAlSource, AL_BUFFER, buffer->mAlSampleSet[0]);
	}

	ALenum error = alGetError();
	if (error != AL_NO_ERROR)
		WARN("OpenAL Error: %s\n", alGetString(error));

#ifdef MULTITHREAD
	if (semaAcquire)
		mSemaphore.Release();
#endif
}

void SoundSource::SetSourceRelative(bool relative)
{
	alSourcei(mAlSource, AL_SOURCE_RELATIVE, relative ? AL_TRUE : AL_FALSE);
}

void SoundSource::Play(bool semaAcquire)
{
	if (IsPlaying(semaAcquire))
		return;

#ifdef MULTITHREAD
	if (semaAcquire)
		mSemaphore.Acquire();
#endif

	mPlaying = true;

	int failCount = 0;
	ALint state = 0;
	while (state != AL_PLAYING && failCount < 10)
	{
		alSourcePlay(mAlSource);

		ALenum error = alGetError();
		if (error != AL_NO_ERROR)
		{
			WARN("OpenAL Error: %s\n", alGetString(error));
			break;
		}

		alGetSourcei(mAlSource, AL_SOURCE_STATE, &state);
		++failCount;
	}

#ifdef MULTITHREAD
	if (semaAcquire)
		mSemaphore.Release();
#endif

	if (state != AL_PLAYING)
		Stop();
}

bool SoundSource::IsPlaying(bool semaAcquire)
{
#ifdef MULTITHREAD
	if (semaAcquire)
		mSemaphore.Acquire();
#endif

	if (mBuffer && mBuffer->IsStreaming())
	{
		bool bPlaying = mPlaying;
#ifdef MULTITHREAD
		if (semaAcquire)
			mSemaphore.Release();
#endif

		return bPlaying;
	}

	ALint state;
	alGetSourcei(mAlSource, AL_SOURCE_STATE, &state);

#ifdef MULTITHREAD
	if (semaAcquire)
		mSemaphore.Release();
#endif
	return state == AL_PLAYING;// || state == AL_INITIAL;
}

void SoundSource::Stop(bool semaAcquire)
{
#ifdef MULTITHREAD
	if (semaAcquire)
		mSemaphore.Acquire();
#endif

	mPlaying = false;

	int failCount = 0;
	ALint state = 0;
	while (state != AL_STOPPED && failCount < 10)
	{
		alSourceStop(mAlSource);

		ALenum error = alGetError();
		if (error != AL_NO_ERROR)
		{
			WARN("OpenAL Error: %s\n", alGetString(error));
			break;
		}

		alGetSourcei(mAlSource, AL_SOURCE_STATE, &state);
		++failCount;
	}

#ifdef MULTITHREAD
	if (semaAcquire)
		mSemaphore.Release();
#endif
}

bool SoundSource::IsStreaming()
{
	return mBuffer && mBuffer->IsStreaming();
}

bool SoundSource::Update()
{
#ifdef MULTITHREAD
	mSemaphore.Acquire();
#endif
	bool result = mBuffer->Update(this);
#ifdef MULTITHREAD
	mSemaphore.Release();
#endif
	return result;
}