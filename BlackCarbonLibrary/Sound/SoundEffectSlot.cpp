#include "SoundEffectSlot.h"
#include "SoundFactory.h"

SoundEffectSlot::SoundEffectSlot(SoundFactory* factory) :
	mAlEffectSlot(0),
	mInUse(false)
{
	alGenAuxiliaryEffectSlots(1, &mAlEffectSlot); 
	while (alGetError())
	{
	}
}

SoundEffectSlot::~SoundEffectSlot()
{
	alDeleteAuxiliaryEffectSlots(1, &mAlEffectSlot);
	while (alGetError())
	{
	}
}