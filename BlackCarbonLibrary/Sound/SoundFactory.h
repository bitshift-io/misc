#ifndef _SOUNDFACTORY_H_
#define _SOUNDFACTORY_H_

#include <string>
#include <vector>
#include <windows.h>

#include "Math/Vector.h"

#include "Include/al.h"
#include "Include/alc.h"
#include "Include/efx.h"
#include "Include/Vorbis/vorbisfile.h"

using namespace std;

class Sound;
class SoundBuffer;
class SoundSource;
class SoundEffect;
class SoundEffectSlot;

//struct ALCcontext;
//struct ALCdevice;

extern LPALGENEFFECTS alGenEffects; 
extern LPALDELETEEFFECTS alDeleteEffects; 
extern LPALISEFFECT alIsEffect;
extern LPALGENAUXILIARYEFFECTSLOTS alGenAuxiliaryEffectSlots;
extern LPALDELETEAUXILIARYEFFECTSLOTS alDeleteAuxiliaryEffectSlots;
extern LPALEFFECTI alEffecti;
extern LPALEFFECTF alEffectf;
extern LPALGENFILTERS alGenFilters;
extern LPALISFILTER alIsFilter;
extern LPALFILTERI alFilteri;
extern LPALFILTERF alFilterf;
extern LPALAUXILIARYEFFECTSLOTI alAuxiliaryEffectSloti;

typedef int (*LPOVCLEAR)(OggVorbis_File *vf);
typedef long (*LPOVREAD)(OggVorbis_File *vf,char *buffer,int length,int bigendianp,int word,int sgned,int *bitstream);
typedef ogg_int64_t (*LPOVPCMTOTAL)(OggVorbis_File *vf,int i);
typedef vorbis_info * (*LPOVINFO)(OggVorbis_File *vf,int link);
typedef vorbis_comment * (*LPOVCOMMENT)(OggVorbis_File *vf,int link);
typedef int (*LPOVOPENCALLBACKS)(void *datasource, OggVorbis_File *vf,char *initial, long ibytes, ov_callbacks callbacks);

extern LPOVCLEAR fn_ov_clear;
extern LPOVREAD fn_ov_read;
extern LPOVPCMTOTAL fn_ov_pcm_total;
extern LPOVINFO fn_ov_info;
extern LPOVCOMMENT fn_ov_comment;
extern LPOVOPENCALLBACKS fn_ov_open_callbacks;

class SoundFactory
{
	friend class Sound;
	friend class SoundEffect;

public:

	typedef vector<SoundSource*>::iterator SoundSourceIterator;

	bool	Init(bool disableSound = false);
	void	Deinit();

	float	GetListenerGain();
	void	SetListenerGain(float gain = 1.f); // volume
	void	SetListenerPosition(const Vector4& position);
	void	SetListenerOrientation(const Vector4& direction, const Vector4& velocity);

	Sound*			CreateSound(const string& name, bool stream = false);
	void			ReleaseSound(Sound** sound);

	SoundEffect*	CreateEffect();
	void			ReleaseEffect(SoundEffect** effect);

	SoundSourceIterator SoundSourceBegin()		{ return mSource.begin(); }
	SoundSourceIterator SoundSourceEnd()		{ return mSource.end(); }

	// update streaming sounds
	// this could be done in another thread
	void	Update();

protected:

	bool				InitVorbisFile();
	void				DeinitVorbisFile();

	SoundEffectSlot*	GetEffectSlot();
	void				ReleaseEffectSlot(SoundEffectSlot** effectSlot);

	SoundSource*	GetSource();
	void			ReleaseSource(SoundSource** source);

	SoundBuffer*	GetBuffer(const char* name, bool stream = false);

	ALCcontext*		mContext;
	ALCdevice*		mDevice;
	string			mBasePath;

	vector<SoundSource*>		mSource;
	vector<SoundBuffer*>		mBuffer;
	vector<SoundEffectSlot*>	mEffectSlot;

	float						mListenerGain;
	bool						mDisableSound;

	// Ogg Voribis DLL Handle
	HINSTANCE					mVorbisFileDLL;
};

#endif

