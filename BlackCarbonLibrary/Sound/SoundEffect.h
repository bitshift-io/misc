#ifndef _SOUNDEFFECT_H_
#define _SOUNDEFFECT_H_

#include <memory.h>
#include <vector>

using namespace std;

class Sound;
class SoundEffectSlot;

enum SoundEffectType
{
	SET_None,
	SET_Reverb,
	SET_Chorus,
	SET_Distortion,
	SET_Echo,
	SET_Flanger,
	SET_FrequencyShift,
	SET_VocalMorph,
	SET_PitchShift,
	SET_RingModulator,
	SET_Compressor,
	SET_Equalizer,
	SET_AutoWah,
};

struct Reverb
{
	Reverb()
	{
		memset(this, 0, sizeof(Reverb));
	}

	float	density;
	float	diffusion;
	float	gain;
	float	gainHF;
	float	decayTime;
	float	decayHFRatio;
	float	reflectionsGain;
	float	reflectionsDelay;
	float	lateReverbGain;
	float	lateReverbDelay;
	float	airAbsorptionGainHF;
	float	roomRolloffFactor;
	float	decayHFLimit;
};

struct Chorus
{
	Chorus()
	{
		memset(this, 0, sizeof(Chorus));
	}

	float	waveform;
	float	phase;
	float	rate;
	float	depth;
	float	feedback;
	float	delay;
};

struct Distortion
{
	Distortion()
	{
		memset(this, 0, sizeof(Distortion));
	}

	float	edge;
	float	gain;
	float	lowPassCutoff;
	float	EQCenter;
	float	EQBandwidth;
};

struct Echo
{
	Echo()
	{
		memset(this, 0, sizeof(Echo));
	}

	float	delay;
	float	LRDelay;
	float	damping;
	float	feedback;
	float	spread;
};

struct Flanger
{
	Flanger()
	{
		memset(this, 0, sizeof(Flanger));
	}

	float	waveform;
	float	phase;
	float	rate;
	float	depth;
	float	feedback;
	float	delay;
};

struct FrequencyShift
{
	FrequencyShift()
	{
		memset(this, 0, sizeof(FrequencyShift));
	}

	float	frequency;
	float	leftDirection;
	float	rightDirection;
};

struct VocalMorph
{
	VocalMorph()
	{
		memset(this, 0, sizeof(VocalMorph));
	}

	float	phonemeA;
	float	phonemeACoarseTuning;
	float	phonemeB;
	float	phonemeBCoarseTuning;
	float	waveform;
	float	rate;
};

struct PitchShift
{
	PitchShift()
	{
		memset(this, 0, sizeof(PitchShift));
	}

	float coarseTune;
	float	fineTune;
};

struct RingModulator
{
	RingModulator()
	{
		memset(this, 0, sizeof(RingModulator));
	}

	float	frequency;
	float	highPassCutoff;
	float	waveform;
};

struct AutoWah
{
	AutoWah()
	{
		memset(this, 0, sizeof(AutoWah));
	}

	float	attackTime;
	float	releasetime;
	float	resonance;
	float	peakGain;
};

struct Compressor
{
	Compressor()
	{
		memset(this, 0, sizeof(Compressor));
	}

	bool	enabled;
};

struct Equalizer
{
	Equalizer()
	{
		memset(this, 0, sizeof(Equalizer));
	}

	float	lowGain;
	float	lowCutoff;
	float	mid1Gain;
	float	mid1Center;
	float	mid1Width;
	float	mid2Gain;
	float	mid2Center;
	float	mid2Width;
	float	highGain;
	float	highCutoff;
};

//
// a specific effect, multiple effects can be combined in to a sound group and applied to a number of sounds
//
class SoundEffect
{
	friend class SoundFactory;

public:

	typedef vector<Sound*>::iterator SoundIterator;


	bool			Valid()									{ return mAlEffect != 0; }

	SoundEffectType GetEffectType()							{ return mEffectType; }

	bool			SetReverb(const Reverb& reverb);
	const Reverb&	GetReverb();

	void			InsertSound(Sound* sound);

	bool			IsEnabled();
	bool			SetEnabled(bool enable);

	// call this after modifying any effect parameters
	void			Apply();

protected:
	
	SoundEffect(SoundFactory* factory);
	~SoundEffect();

	SoundEffectSlot*	mEffectSlot;
	SoundFactory*		mSoundFactory;

	SoundEffectType		mEffectType;
	Reverb				mReverb;
	Equalizer			mEqualizer;
	Compressor			mCompressor;
	AutoWah				mAutoWah;
	RingModulator		mRingModulator;
	PitchShift			mPitchShift;
	VocalMorph			mVocalMorph;
	FrequencyShift		mFrequencyShift;
	Flanger				mFlanger;
	Echo				mEcho;
	Distortion			mDistortion;
	Chorus				mChorus;

	unsigned int		mAlEffect;
	vector<Sound*>		mSound;
};

#endif