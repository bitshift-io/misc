#include "SoundFactory.h"
#include "SoundSource.h"
#include "Sound.h"
#include "SoundBuffer.h"
#include "SoundEffectSlot.h"
#include "SoundEffect.h"
#include "File/Log.h"

#pragma comment(lib, "openal32.lib")

LPALGENEFFECTS alGenEffects = 0; 
LPALDELETEEFFECTS alDeleteEffects = 0; 
LPALISEFFECT alIsEffect = 0;
LPALGENAUXILIARYEFFECTSLOTS alGenAuxiliaryEffectSlots = 0;
LPALDELETEAUXILIARYEFFECTSLOTS alDeleteAuxiliaryEffectSlots = 0;
LPALEFFECTI alEffecti = 0;
LPALEFFECTF alEffectf = 0;
LPALGENFILTERS alGenFilters = 0;
LPALISFILTER alIsFilter = 0;
LPALFILTERI alFilteri = 0;
LPALFILTERF alFilterf = 0; 
LPALAUXILIARYEFFECTSLOTI alAuxiliaryEffectSloti = 0;

LPOVCLEAR fn_ov_clear = 0;
LPOVREAD fn_ov_read = 0;
LPOVPCMTOTAL fn_ov_pcm_total = 0;
LPOVINFO fn_ov_info = 0;
LPOVCOMMENT fn_ov_comment = 0;
LPOVOPENCALLBACKS fn_ov_open_callbacks = 0;

bool SoundFactory::Init(bool disableSound)
{
	// fixes audio pop on init of game
	SetListenerGain(0.f);

	mDisableSound = disableSound;

	if (mDisableSound)
		return true;

	if (!InitVorbisFile())
	{
		Log::Error("Failed to init Vorbis File\n");
		mDisableSound = true;
		return false;
	}

	//Open device
	mDevice = alcOpenDevice(NULL);
	//mDevice = NULL; // testing 
	if (!mDevice)
	{
		//we failed to initialize the device
		Log::Error("Failed to get OpenAL device. Check The OpenAL redistributable is installed.\n");
		mDisableSound = true;
		return false;
	}

	//Create context(s)
	mContext = alcCreateContext(mDevice, NULL);

	//Set active context
	alcMakeContextCurrent(mContext);

//	alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);

	// Clear Error Code
	ALenum error;
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		WARN("OpenAL Error: %s\n", alGetString(error));
		return false;
	}

	// Get the device specifier.
    const char* deviceSpecifier = alcGetString(mDevice, ALC_DEVICE_SPECIFIER);
	Log::Print("OpenAL Initilised using: %s\n", deviceSpecifier);

	// generate sources until we cant get more
	//mSource.setcapacity(16);
	SoundSource* src = 0;
	do
	{
		src = new SoundSource(this);

		if (!src->Valid())
		{
			delete src;
			src = 0;
		}
		else
		{
			mSource.push_back(src);
		}
	} while (src);

	
	SetListenerGain(1.f);
	Log::Print("Using %i sources\n", mSource.size());

	// init extensions
	if (alcIsExtensionPresent(mDevice, "ALC_EXT_EFX") == AL_FALSE) 
	{
		Log::Print("Failed to get OpenAL effects extension\n");
		return true; 
	}

	// Get the Effect Extension function pointers 
	alGenEffects = (LPALGENEFFECTS)alGetProcAddress("alGenEffects"); 
	alDeleteEffects = (LPALDELETEEFFECTS)alGetProcAddress("alDeleteEffects"); 
	alIsEffect = (LPALISEFFECT)alGetProcAddress("alIsEffect");
	alGenAuxiliaryEffectSlots = (LPALGENAUXILIARYEFFECTSLOTS)alGetProcAddress("alGenAuxiliaryEffectSlots");
	alDeleteAuxiliaryEffectSlots = (LPALGENAUXILIARYEFFECTSLOTS)alGetProcAddress("alDeleteAuxiliaryEffectSlots");
	alEffecti = (LPALEFFECTI)alGetProcAddress("alEffecti");
	alEffectf = (LPALEFFECTF)alGetProcAddress("alEffectf");
	alGenFilters = (LPALGENFILTERS)alGetProcAddress("alGenFilters");
	alIsFilter = (LPALISFILTER)alGetProcAddress("alIsFilter");
	alFilteri = (LPALFILTERI)alGetProcAddress("alFilteri");
	alFilterf = (LPALFILTERF)alGetProcAddress("alFilterf"); 
	alAuxiliaryEffectSloti = (LPALAUXILIARYEFFECTSLOTI)alGetProcAddress("alAuxiliaryEffectSloti"); 

	// Check function pointers are valid
	if (!(alGenEffects && alDeleteEffects && alIsEffect && alGenAuxiliaryEffectSlots)) 
		return true;


	// generate effect slots until we cant get more
	SoundEffectSlot* slot = 0;
	do
	{
		slot = new SoundEffectSlot(this);

		if (!slot->Valid())
		{
			delete slot;
			slot = 0;
		}
		else
		{
			mEffectSlot.push_back(slot);
		}
	} while (slot);

/*
	// generate effects until we cant get more
	SoundEffect* effect = 0;
	do
	{
		effect = new SoundEffect(this);

		if (!effect->Valid())
		{
			delete effect;
			effect = 0;
		}
		else
		{
			mEffect.push_back(effect);
		}
	} while (effect);

	int nothing = 0;*/
/*
	ALuint uiEffectSlot[4] = { 0 }; 
	ALuint uiEffect[2] = { 0 }; 
	ALuint uiFilter[1] = { 0 }; 
	ALuint uiLoop = 0; 
	 
	// Try to create 4 Auxiliary Effect Slots
	alGetError(); 
	for (uiLoop = 0; uiLoop < 4; uiLoop++) 
	{ 
		alGenAuxiliaryEffectSlots(1, &uiEffectSlot[uiLoop]); 
		if (alGetError() != AL_NO_ERROR) 
			break; 
	} 

	Log::Print("Using %i Aux Effect Slots\n", uiLoop);

	// Try to create 2 Effects
	for (uiLoop = 0; uiLoop < 2; uiLoop++) 
	{ 
		alGenEffects(1, &uiEffect[uiLoop]); 
		if (alGetError() != AL_NO_ERROR) 
			break; 
	} 
	 
	Log::Print("Using %i Effects\n", uiLoop);


	// Set first Effect Type to Reverb and change Decay Time
	alGetError(); 
	if (alIsEffect(uiEffect[0])) 
	{ 
	  alEffecti(uiEffect[0], AL_EFFECT_TYPE, AL_EFFECT_REVERB); 
	  if (alGetError() != AL_NO_ERROR) 
		printf("Reverb Effect not supported\n"); 
	 else 
	  alEffectf(uiEffect[0], AL_REVERB_DECAY_TIME, 5.0f); 
	} 
	 /*
	// Set second Effect Type to Flanger and change Phase
	alGetError(); 
	if (alIsEffect(uiEffect[1])) 
	{ 
	 alEffecti(uiEffect[1],AL_EFFECT_TYPE,AL_EFFECT_FLANGER); 
	  if (alGetError() != AL_NO_ERROR) 
		printf("Flanger effect not support\n"); 
	 else 
	  alEffecti(uiEffect[1], AL_FLANGER_PHASE, 180); 
	} 
	 * /
	// Try to create a Filter 
	alGetError(); 
	alGenFilters(1, &uiFilter[0]); 
	if (alGetError() == AL_NO_ERROR) 
	  printf("Generated a Filter\n"); 
	 
	if (alIsFilter(uiFilter[0])) 
	{ 
	  // Set Filter type to Low-Pass and set parameters 
	 alFilteri(uiFilter[0],AL_FILTER_TYPE,AL_FILTER_LOWPASS); 
	 
	  if (alGetError() != AL_NO_ERROR) 
		printf("Low Pass Filter not supported\n"); 
	 else 
	 { 
	  alFilterf(uiFilter[0], AL_LOWPASS_GAIN, 0.5f); 
	  alFilterf(uiFilter[0], AL_LOWPASS_GAINHF, 0.5f); 
	 } 
	} 


	// Attach Effect to Auxiliary Effect Slot 
 
	// uiEffectSlot[0] is the ID of an Aux Effect Slot 
	// uiEffect[0] is the ID of an Effect 
	 
	alAuxiliaryEffectSloti(uiEffectSlot[0], 
	 AL_EFFECTSLOT_EFFECT, uiEffect[0]); 
	 
	if (alGetError() == AL_NO_ERROR) 
	  printf("Successfully loaded effect into effect slot\n");


	// Configure Source Auxiliary Effect Slot Sends 
	 
	// uiEffectSlot[0] and uiEffectSlot[1] are Auxiliary 
	// Effect Slot IDs 
	// uiEffect[0] is an Effect ID 
	// uiFilter[0] is a Filter ID 
	// uiSource is a Source ID  
	SoundSource* pSource = mSource[0];
	ALuint& uiSource = pSource->mAlSource;
	 
	// Set Source Send 0 to feed uiEffectSlot[0] without 
	// filtering 
	alSource3i(uiSource,AL_AUXILIARY_SEND_FILTER, uiEffectSlot[0],
	0, NULL); 
	if (alGetError() != AL_NO_ERROR) 
	  printf("Failed to configure Source Send 0\n"); 
	 /*
	// Set Source Send 1 to feed uiEffectSlot[1] with
	// filter uiFilter[0]
	alSource3i(uiSource,AL_AUXILIARY_SEND_FILTER, uiEffectSlot[1],
	1, uiFilter[0]); 
	if (alGetError() != AL_NO_ERROR) 
	  printf("Failed to configure Source Send 1\n"); 
* /
	// Disable Send 0 
	alSource3i(uiSource,AL_AUXILIARY_SEND_FILTER,
	  AL_EFFECTSLOT_NULL, 0, NULL); 
	if (alGetError() != AL_NO_ERROR) 
	  printf("Failed to disable Source Send 0\n"); 
	 /*
	// Disable Send 1 
	alSource3i(uiSource,AL_AUXILIARY_SEND_FILTER,
	  AL_EFFECTSLOT_NULL, 1, NULL); 
	if (alGetError() != AL_NO_ERROR) 
	  printf("Failed to disable Source Send 1\n"); 
*/

	// TUT 5
/*
	// Filter 'uiSource', a generated Source 
 
	alSourcei(uiSource, AL_DIRECT_FILTER, uiFilter[0]); 
	if (alGetError() == AL_NO_ERROR) 
	{ 
	  printf("Successfully applied a direct path filter\n"); 
	  // Remove filter from 'uiSource' 
	  alSourcei(uiSource, AL_DIRECT_FILTER, AL_FILTER_NULL); 
	  if (alGetError() == AL_NO_ERROR) 
		printf("Successfully removed direct filter\n"); 
	} 
	 * /
	 
	// Filter the Source send 0 from 'uiSource' to 
	// Auxiliary Effect Slot uiEffectSlot[0] 
	// using Filter uiFilter[0] 
	 
	alSource3i(uiSource,AL_AUXILIARY_SEND_FILTER,    
	  uiEffectSlot[0], 0, uiFilter[0]); 
	if (alGetError() == AL_NO_ERROR) 
	{ 
	  printf("Successfully applied aux send filter\n"); 
	 
	  // Remove Filter from Source Auxiliary Send 
	 alSource3i(uiSource,AL_AUXILIARY_SEND_FILTER,   
	  uiEffectSlot[0], 0, AL_FILTER_NULL); 
	  if (alGetError() == AL_NO_ERROR) 
	  printf("Successfully removed filter\n"); 
	} 
/*
	// TUT 6

	// Set Source Cone Outer Gain HF value 
	alSourcef(uiSource, AL_CONE_OUTER_GAINHF, 0.5f); 
	if (alGetError() == AL_NO_ERROR) 
	  printf("Successfully set cone outside gain filter\n");
*/
/*
	alListeneri(AL_DISTANCE_MODEL, AL_NONE);

	SetListenerPosition(Vector4(0.f, 0.f, 0.f));
	SetListenerOrientation(Vector4(0.f, 0.f, 1.f), Vector4(0.f, 0.f, 0.f));
*/
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		WARN("Unknown openal error: %s\n", alGetString(error));
	}

	return true;
}

void SoundFactory::Deinit()
{
	// fixes audio pop on exiting of game
	SetListenerGain(0.f);

	vector<SoundSource*>::iterator sourceIt = mSource.begin();
	while (mSource.size())
	{
		delete mSource[0];
		sourceIt = mSource.erase(sourceIt);
	}

	vector<SoundBuffer*>::iterator bufferIt = mBuffer.begin();
	while (mBuffer.size())
	{
		delete mBuffer[0];
		bufferIt = mBuffer.erase(bufferIt);
	}

	if (mDisableSound)
		return;

	//Get active context
	mContext = alcGetCurrentContext();

	//Get device for active context
	mDevice = alcGetContextsDevice(mContext);

	//Disable context
	alcMakeContextCurrent(NULL);

	//Release context(s)
	alcDestroyContext(mContext);

	//Close device
	alcCloseDevice(mDevice);

	DeinitVorbisFile();
}

Sound* SoundFactory::CreateSound(const string& name, bool stream)
{
	std::string path = name;
	Sound* snd = new Sound(this);
	if (!snd->Load(path.c_str(), stream))
	{
		delete snd;
		return 0;
	}

	return snd;
}

void SoundFactory::ReleaseSound(Sound** resource)
{
	if (!*resource)
		return;

	(*resource)->Stop();
	delete *resource;
	resource = 0;
}

SoundEffect* SoundFactory::CreateEffect()
{
	SoundEffect* effect = new SoundEffect(this);
	if (!effect->Valid())
	{
		delete effect;
		return 0;
	}

	return effect;
}

void SoundFactory::ReleaseEffect(SoundEffect** effect)
{
	if (!*effect)
		return;

	delete *effect;
	*effect = 0;
}

/*
void SoundFactory::Update()
{
	for (int i = 0; i < sources.GetSize(); ++i)
	{
		SoundSource* src = sources[i];
		if (!src->IsPlaying())
			src->SetInUse(false);
	}
}
*/

float SoundFactory::GetListenerGain()
{
	ALfloat gain;
	alGetListenerf(AL_GAIN, &gain);
	return gain;
}

void SoundFactory::SetListenerGain(float gain)
{
	alListenerf(AL_GAIN, gain);
}

void SoundFactory::SetListenerPosition(const Vector4& position)
{
	//set the position using 3 seperate floats
	alListener3f(AL_POSITION, position.x, position.y, position.z);
}

void SoundFactory::SetListenerOrientation(const Vector4& direction, const Vector4& velocity)
{
	//set the orientation using an array of floats
	float vec[6];
	vec[0] = -direction.x;
	vec[1] = -direction.y;
	vec[2] = -direction.z;
	vec[3] = velocity.x;
	vec[4] = velocity.y;
	vec[5] = velocity.z;
	alListenerfv(AL_ORIENTATION, vec);
}

SoundEffectSlot* SoundFactory::GetEffectSlot()
{
	for (unsigned int i = 0; i < mEffectSlot.size(); ++i)
	{
		SoundEffectSlot* src = mEffectSlot[i];
		if (!src->InUse())
		{
			src->SetInUse(true);
			return src;
		}
	}

	return 0;
}

void SoundFactory::ReleaseEffectSlot(SoundEffectSlot** effectSlot)
{
	if (!*effectSlot)
		return;

	(*effectSlot)->SetInUse(false);
	*effectSlot = 0;
}

SoundSource* SoundFactory::GetSource()
{
	for (unsigned int i = 0; i < mSource.size(); ++i)
	{
		SoundSource* src = mSource[i];
		if (!src->InUse())
		{
			src->SetInUse(true);
			return src;
		}
	}

	return 0;
}

void SoundFactory::ReleaseSource(SoundSource** source)
{
	if (!*source)
		return;

	(*source)->Stop();
	(*source)->SetInUse(false);
	*source = 0;
}

SoundBuffer* SoundFactory::GetBuffer(const char* name, bool stream)
{
	if (mDisableSound)
		return 0;

	// dont return streaming buffers
	for (unsigned int i = 0; i < mBuffer.size(); ++i)
	{
		SoundBuffer* buffer = mBuffer[i];
		if (!buffer->IsStreaming() && strcmp(buffer->GetName(), name) == 0)
			return buffer;
	}

	SoundBuffer* buffer = new SoundBuffer();
	if (!buffer->Load(name, stream))
	{
		delete buffer;
		return 0;
	}

	mBuffer.push_back(buffer);
	return mBuffer.back();
}

bool SoundFactory::InitVorbisFile()
{
	// Try and load Vorbis DLLs (VorbisFile.dll will load ogg.dll and vorbis.dll)
	mVorbisFileDLL = LoadLibrary("vorbisfile.dll");
	if (mVorbisFileDLL)
	{
		fn_ov_clear = (LPOVCLEAR)GetProcAddress(mVorbisFileDLL, "ov_clear");
		fn_ov_read = (LPOVREAD)GetProcAddress(mVorbisFileDLL, "ov_read");
		fn_ov_pcm_total = (LPOVPCMTOTAL)GetProcAddress(mVorbisFileDLL, "ov_pcm_total");
		fn_ov_info = (LPOVINFO)GetProcAddress(mVorbisFileDLL, "ov_info");
		fn_ov_comment = (LPOVCOMMENT)GetProcAddress(mVorbisFileDLL, "ov_comment");
		fn_ov_open_callbacks = (LPOVOPENCALLBACKS)GetProcAddress(mVorbisFileDLL, "ov_open_callbacks");

		if (fn_ov_clear && fn_ov_read && fn_ov_pcm_total && fn_ov_info &&
			fn_ov_comment && fn_ov_open_callbacks)
		{
			return true;
		}
	}

	return false;
}

void SoundFactory::DeinitVorbisFile()
{
	if (mVorbisFileDLL)
	{
		FreeLibrary(mVorbisFileDLL);
		mVorbisFileDLL = NULL;
	}
}

void SoundFactory::Update()
{
	if (mDisableSound)
		return;

	// update sources as there could be a significant amount of buffers
	for (unsigned int i = 0; i < mSource.size(); ++i)
	{
		SoundSource* source = mSource[i];
		if (source->InUse() && source->IsPlaying() && source->IsStreaming())
		{
			source->Update();
		}
	}
}