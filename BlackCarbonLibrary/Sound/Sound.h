#ifndef _SOUND_H_
#define _SOUND_H_

#include "Math/Vector.h"
#include <vector>

using namespace std;

class SoundFactory;
class SoundSource;
class SoundBuffer;

//
// a sound, which has a sound source
// there are a limited amount of sound sources
// so this hides that fact, when you play this sound
// it will attempt to get a sound source from the pool
// if it fails, the sound will not play
//
class Sound
{
	friend class SoundFactory;
	friend class SoundEffect;

public:

	typedef vector<SoundEffect*>::iterator EffectIterator;

	virtual bool Load(const char* name, bool stream = false);

	void Play();
	void Stop();
	bool IsPlaying();

	void Destroy();

	void SetRadius(float radius);
	void SetPosition(const Vector4& position);
	void SetVelocity(const Vector4& velocity);

	//this function makes a sound source relative so all direction and velocity
	//parameters become relative to the source rather than the listener
	//useful for background music that you want to stay constant relative to the listener
	//no matter where they go
	void SetSourceRelative(bool relative);

	void SetLooping(bool looping);
	void SetPitch(float pitch);
	void SetGain(float gain);

	float GetPitch();

protected:

	void	InsertEffect(SoundEffect* effect);
	void	RemoveEffect(SoundEffect* effect);

	Sound(SoundFactory* factory);
	virtual ~Sound();

	vector<SoundEffect*>		mEffect;

	SoundSource*	mSource;
	SoundBuffer*	mBuffer; // a pointer to the buffer, when we obtain a sound, we set it to use this buffer
	float			mRadius;
	Vector4			mPosition;
	Vector4			mVelocity;
	bool			mLooping;
	float			mPitch;
	float			mGain;
	bool			mRelative;
	SoundFactory*	mFactory;
};

#endif
