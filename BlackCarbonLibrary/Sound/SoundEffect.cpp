#include "SoundEffect.h"
#include "SoundFactory.h"
#include "Sound.h"
#include "SoundEffectSlot.h"
#include "SoundSource.h"

SoundEffect::SoundEffect(SoundFactory* factory) :
	mEffectType(SET_None),
	mEffectSlot(0),
	mSoundFactory(factory)
{
	alGenEffects(1, &mAlEffect); 
}

SoundEffect::~SoundEffect()
{
	alDeleteEffects(1, &mAlEffect);
}

bool SoundEffect::SetReverb(const Reverb& reverb)
{
	mEffectType = SET_Reverb;
	mReverb = reverb;

	alEffecti(mAlEffect, AL_EFFECT_TYPE, AL_EFFECT_REVERB); 
	if (alGetError() != AL_NO_ERROR) 
		return false;

	alEffectf(mAlEffect, AL_REVERB_DECAY_TIME, mReverb.decayTime);

	Apply();
	return true;
}

const Reverb& SoundEffect::GetReverb()
{
	return mReverb;
}


void SoundEffect::InsertSound(Sound* sound)
{
	mSound.push_back(sound);
	sound->InsertEffect(this);
}

bool SoundEffect::IsEnabled()
{
	return mEffectSlot != 0;
}

bool SoundEffect::SetEnabled(bool enable)
{
	if (enable)
	{
		if (mEffectSlot)
			return true;

		mEffectSlot = mSoundFactory->GetEffectSlot();
		return IsEnabled();
	}
	
	mSoundFactory->ReleaseEffectSlot(&mEffectSlot);
	Apply();
	return true;
}

void SoundEffect::Apply()
{	
	if (!mEffectSlot)
	{
		SoundIterator soundIt;
		for (soundIt = mSound.begin(); soundIt != mSound.end(); ++soundIt)
		{
			SoundSource* source = (*soundIt)->mSource;
			if (!source)
				continue;

			ALuint& uiSource = source->mAlSource;

			// Enable (non-filtered) Send from Source to Auxiliary Effect Slot
			alSource3i(uiSource, AL_AUXILIARY_SEND_FILTER, AL_EFFECTSLOT_NULL, 0, AL_FILTER_NULL);
		}

		// unbind effect from effect slot
		//alAuxiliaryEffectSloti(AL_EFFECTSLOT_NULL, AL_EFFECTSLOT_EFFECT, uiEffect);

		return;
	}
/*
	// Attach Effects to Auxiliary Effect Slot 
	EffectIterator effectIt;
	for (effectIt = mEffect.begin(); effectIt != mEffect.end(); ++effectIt)
	{
		alAuxiliaryEffectSloti(mEffectSlot->mAlEffectSlot, AL_EFFECTSLOT_EFFECT, (*effectIt)->mAlEffect); 
		 
		if (alGetError() != AL_NO_ERROR) 
			Log::Warning("[SoundGroup::Apply] Successfully loaded effect into effect slot\n");
	}
*/

	/*
	// pipe sounds in to the effect slot to feed the effects
	SoundIterator soundIt;
	for (soundIt = mSound.begin(); soundIt != mSound.end(); ++soundIt)
	{
		// Set Source Send 0 to feed uiEffectSlot[0] without * filtering
		if (!(*soundIt)->mSource)
			continue;

		// Try to create a Filter 
		ALuint uiFilter[1] = { 0 }; 

		alGetError(); 
		alGenFilters(1, &uiFilter[0]); 
		if (alGetError() == AL_NO_ERROR) 
		  printf("Generated a Filter\n"); 
		 
		if (alIsFilter(uiFilter[0])) 
		{ 
		  // Set Filter type to Low-Pass and set parameters 
		 alFilteri(uiFilter[0],AL_FILTER_TYPE,AL_FILTER_LOWPASS); 
		 
		  if (alGetError() != AL_NO_ERROR) 
			printf("Low Pass Filter not supported\n"); 
		 else 
		 { 
		  alFilterf(uiFilter[0], AL_LOWPASS_GAIN, 0.5f); 
		  alFilterf(uiFilter[0], AL_LOWPASS_GAINHF, 0.5f); 
		 } 
		} 

		
		// Attach Effect to Auxiliary Effect Slot 
 
		// uiEffectSlot[0] is the ID of an Aux Effect Slot 
		// uiEffect[0] is the ID of an Effect 
		 
		alAuxiliaryEffectSloti(mEffectSlot->mAlEffectSlot, 
		 AL_EFFECTSLOT_EFFECT, (*mEffect.begin())->mAlEffect); 
		 
		if (alGetError() == AL_NO_ERROR) 
		  printf("Successfully loaded effect into effect slot\n");


		alSource3i((*soundIt)->mSource->mAlSource, AL_AUXILIARY_SEND_FILTER, mEffectSlot->mAlEffectSlot, 0, NULL); 
		if (alGetError() != AL_NO_ERROR) 
		  Log::Warning("[SoundGroup::Apply] Failed to configure Source Send 0\n"); 

		// Disable Send 0 
	alSource3i((*soundIt)->mSource->mAlSource,AL_AUXILIARY_SEND_FILTER,
	  AL_EFFECTSLOT_NULL, 0, NULL); 
	if (alGetError() != AL_NO_ERROR) 
	  printf("Failed to disable Source Send 0\n"); 


		// Filter the Source send 0 from 'uiSource' to 
		// Auxiliary Effect Slot uiEffectSlot[0] 
		// using Filter uiFilter[0] 
		 
		alSource3i((*soundIt)->mSource->mAlSource, AL_AUXILIARY_SEND_FILTER,    
		  mEffectSlot->mAlEffectSlot, 0, uiFilter[0]); 
		if (alGetError() == AL_NO_ERROR) 
		{ 
		  printf("Successfully applied aux send filter\n"); 
		 
		  // Remove Filter from Source Auxiliary Send 
		 alSource3i((*soundIt)->mSource->mAlSource, AL_AUXILIARY_SEND_FILTER,   
		  mEffectSlot->mAlEffectSlot, 0, AL_FILTER_NULL); 
		  if (alGetError() == AL_NO_ERROR) 
			printf("Successfully removed filter\n"); 
		}
	}
*/

	
/*
	ALuint uiEffectSlot[4] = { 0 }; 
	ALuint uiEffect[2] = { 0 }; 
	ALuint uiFilter[1] = { 0 }; 
	ALuint uiLoop = 0; 
	 

	uiEffectSlot[0] = mEffectSlot->mAlEffectSlot;
	uiEffect[0] = mAlEffect;


	// Set first Effect Type to Reverb and change Decay Time
	alGetError(); 
	if (alIsEffect(uiEffect[0])) 
	{ 
	  alEffecti(uiEffect[0], AL_EFFECT_TYPE, AL_EFFECT_REVERB); 
	  if (alGetError() != AL_NO_ERROR) 
		printf("Reverb Effect not supported\n"); 
	 else 
	  alEffectf(uiEffect[0], AL_REVERB_DECAY_TIME, 5.0f); 
	} 

	// Try to create a Filter 
	alGetError(); 
	alGenFilters(1, &uiFilter[0]); 
	if (alGetError() == AL_NO_ERROR) 
	  printf("Generated a Filter\n"); 
	 
	if (alIsFilter(uiFilter[0])) 
	{ 
	  // Set Filter type to Low-Pass and set parameters 
	 alFilteri(uiFilter[0],AL_FILTER_TYPE,AL_FILTER_LOWPASS); 
	 
	  if (alGetError() != AL_NO_ERROR) 
		printf("Low Pass Filter not supported\n"); 
	 else 
	 { 
	  alFilterf(uiFilter[0], AL_LOWPASS_GAIN, 0.5f); 
	  alFilterf(uiFilter[0], AL_LOWPASS_GAINHF, 0.5f); 
	 } 
	} 


	// Attach Effect to Auxiliary Effect Slot 
 
	// uiEffectSlot[0] is the ID of an Aux Effect Slot 
	// uiEffect[0] is the ID of an Effect 
	 
	alAuxiliaryEffectSloti(uiEffectSlot[0], 
	 AL_EFFECTSLOT_EFFECT, uiEffect[0]); 
	 
	if (alGetError() == AL_NO_ERROR) 
	  printf("Successfully loaded effect into effect slot\n");


	// Configure Source Auxiliary Effect Slot Sends 
	 
	// uiEffectSlot[0] and uiEffectSlot[1] are Auxiliary 
	// Effect Slot IDs 
	// uiEffect[0] is an Effect ID 
	// uiFilter[0] is a Filter ID 
	// uiSource is a Source ID  
	//mSound[0]->mSource = mSoundFactory->GetSource(); 

	SoundSource* pSource = mSound[0]->mSource;
	if (!pSource)
		return; // Cleanup

	ALuint& uiSource = pSource->mAlSource;
	 
	// Set Source Send 0 to feed uiEffectSlot[0] without 
	// filtering 
	alSource3i(uiSource,AL_AUXILIARY_SEND_FILTER, uiEffectSlot[0],
	0, NULL); 
	if (alGetError() != AL_NO_ERROR) 
	  printf("Failed to configure Source Send 0\n"); 

	// Disable Send 0 
	alSource3i(uiSource,AL_AUXILIARY_SEND_FILTER,
	  AL_EFFECTSLOT_NULL, 0, NULL); 
	if (alGetError() != AL_NO_ERROR) 
	  printf("Failed to disable Source Send 0\n"); 



	 
	// Filter the Source send 0 from 'uiSource' to 
	// Auxiliary Effect Slot uiEffectSlot[0] 
	// using Filter uiFilter[0] 
	 
	alSource3i(uiSource,AL_AUXILIARY_SEND_FILTER,    
	  uiEffectSlot[0], 0, uiFilter[0]); 
	if (alGetError() == AL_NO_ERROR) 
	{ 
	  printf("Successfully applied aux send filter\n"); 
	 
	  // Remove Filter from Source Auxiliary Send 
	 alSource3i(uiSource,AL_AUXILIARY_SEND_FILTER,   
	  uiEffectSlot[0], 0, AL_FILTER_NULL); 
	  if (alGetError() == AL_NO_ERROR) 
	  printf("Successfully removed filter\n"); 
	} 
*/



	ALuint uiEffectSlot = mEffectSlot->mAlEffectSlot;
	ALuint uiEffect = mAlEffect;

	// Load Effect into Auxiliary Effect Slot
	alAuxiliaryEffectSloti(uiEffectSlot, AL_EFFECTSLOT_EFFECT, uiEffect);

	SoundIterator soundIt;
	for (soundIt = mSound.begin(); soundIt != mSound.end(); ++soundIt)
	{
		SoundSource* source = (*soundIt)->mSource;
		if (!source)
			continue;

		ALuint& uiSource = source->mAlSource;

		// Enable (non-filtered) Send from Source to Auxiliary Effect Slot
		alSource3i(uiSource, AL_AUXILIARY_SEND_FILTER, uiEffectSlot, 0, AL_FILTER_NULL);
	}
}