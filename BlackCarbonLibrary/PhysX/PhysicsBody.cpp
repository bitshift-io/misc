#include "PhysicsBody.h"
#include "PhysicsScene.h"
#include "PhysicsActorController.h"
#include "NxuStream2/NXU_Streaming.h"
#include "NxCooking.h"
#include "NxCapsuleController.h"
#include "File/File.h"

using namespace NXU;

// temporary
enum GameGroup
{
	GROUP_NON_COLLIDABLE,
	GROUP_COLLIDABLE_NON_PUSHABLE,
	GROUP_COLLIDABLE_PUSHABLE,
};


void TriMeshConstructor::Init(unsigned int numIndex, unsigned int numPosition)
{
	mPosition = (Vector4*)_aligned_malloc(numPosition * sizeof(Vector4), 16);
	mIndex = new unsigned int[numIndex];
}

void TriMeshConstructor::Deinit()
{
	delete[] mIndex;
	_aligned_free(mPosition);
}


PhysicsBodyConstructor::PhysicsBodyConstructor()
{

}

PhysicsBodyConstructor::~PhysicsBodyConstructor()
{
	list<ShapeInfo>::iterator it;
	it = mShapeInfo.begin();
	while (mShapeInfo.size())
	{
		NxShapeDesc* shape = it->shapeDesc;
		delete shape;
		it = mShapeInfo.erase(it);
	}
}

bool PhysicsBodyConstructor::Load(const PhysicsFactory* factory, const string& name)
{
	// this hsould load from a scene?
	return true;
}

void PhysicsBodyConstructor::AddConvexMesh(const PhysicsFactory* factory, const TriMeshConstructor& meshData, int collisionGroup, const Matrix4* local)
{
	NxConvexMeshDesc convexDesc;
	convexDesc.numVertices				= meshData.mPositionCount;
	convexDesc.numTriangles				= meshData.mIndexCount / 3;
	convexDesc.pointStrideBytes			= sizeof(Vector4);
	convexDesc.triangleStrideBytes		= 3 * sizeof(unsigned int);
	convexDesc.points					= meshData.mPosition;
	convexDesc.triangles				= meshData.mIndex;
	convexDesc.flags					= 0;

	NxInitCooking();
	MemoryWriteBuffer buf;
	
	if (NxCookConvexMesh(convexDesc, buf))
	{
		NxConvexShapeDesc* convexShapeDesc = new NxConvexShapeDesc;
		SetCollisionGroup(convexShapeDesc, collisionGroup);

		if (local)
		{
			NxMat34 nxTransform;
			nxTransform.setColumnMajor44(local->m);
			convexShapeDesc->localPose = nxTransform;
		}
		
		convexShapeDesc->meshData = factory->GetHandle()->createConvexMesh(MemoryReadBuffer(buf.data));
		mActorDesc.shapes.pushBack(convexShapeDesc);

		ShapeInfo info;
		info.shapeDesc = convexShapeDesc;
		mShapeInfo.push_front(info);
	}
}

void PhysicsBodyConstructor::AddTriMesh(const PhysicsFactory* factory, const TriMeshConstructor& meshData, int collisionGroup, const Matrix4* local)
{
	NxTriangleMeshDesc meshDesc;
	meshDesc.numVertices				= meshData.mPositionCount;
	meshDesc.numTriangles				= meshData.mIndexCount / 3;
	meshDesc.pointStrideBytes			= sizeof(Vector4);
	meshDesc.triangleStrideBytes		= 3 * sizeof(unsigned int);
	meshDesc.points						= meshData.mPosition;
	meshDesc.triangles					= meshData.mIndex;
	meshDesc.flags						= 0;

	NxInitCooking();

	MemoryWriteBuffer buf;
	bool status = NxCookTriangleMesh(meshDesc, buf);
	MemoryReadBuffer readBuffer(buf.data);
	NxTriangleMesh* triMesh = factory->GetHandle()->createTriangleMesh(readBuffer);

	NxTriangleMeshShapeDesc* meshShapeDesc = new NxTriangleMeshShapeDesc;
	SetCollisionGroup(meshShapeDesc, collisionGroup);
	meshShapeDesc->meshData = triMesh;
	meshShapeDesc->meshFlags = 0;

	if (local)
	{
		NxMat34 nxTransform;
		nxTransform.setColumnMajor44(local->m);
		meshShapeDesc->localPose = nxTransform;
	}

	bool valid = meshShapeDesc->isValid();
	mActorDesc.shapes.pushBack(meshShapeDesc);

	ShapeInfo info;
	info.shapeDesc = meshShapeDesc;
	mShapeInfo.push_front(info);
}

void PhysicsBodyConstructor::AddBox(const Vector4& dimensions, int collisionGroup)
{
    NxBoxShapeDesc* boxDesc = new NxBoxShapeDesc;
    boxDesc->dimensions.set(dimensions.x, dimensions.y, dimensions.z);
	SetCollisionGroup(boxDesc, collisionGroup);
    mActorDesc.shapes.pushBack(boxDesc);

	ShapeInfo info;
	info.shapeDesc = boxDesc;
	mShapeInfo.push_front(info);
}

void PhysicsBodyConstructor::AddPlane(int collisionGroup)
{
    NxPlaneShapeDesc* planeDesc = new NxPlaneShapeDesc;
	SetCollisionGroup(planeDesc, collisionGroup);
    mActorDesc.shapes.pushBack(planeDesc);

	ShapeInfo info;
	info.shapeDesc = planeDesc;
	mShapeInfo.push_front(info);
}

void PhysicsBodyConstructor::AddCapsule(float radius, float height, int collisionGroup)
{
	NxCapsuleShapeDesc* capDesc = new NxCapsuleShapeDesc;
	capDesc->height = height;
	capDesc->radius = radius;
	SetCollisionGroup(capDesc, collisionGroup);
    mActorDesc.shapes.pushBack(capDesc);

	ShapeInfo info;
	info.shapeDesc = capDesc;
	mShapeInfo.push_front(info);
}

void PhysicsBodyConstructor::AddSphere(float radius, int collisionGroup)
{
	NxSphereShapeDesc* sphereDesc = new NxSphereShapeDesc;
	SetCollisionGroup(sphereDesc, collisionGroup);
	sphereDesc->radius = radius;
	//sphereDesc->mass = 1000.f;
	//sphereDesc->density = 10000.f;
	mActorDesc.shapes.push_back(sphereDesc);

	ShapeInfo info;
	info.shapeDesc = sphereDesc;
	mShapeInfo.push_front(info);
}

void PhysicsBodyConstructor::SetDynamicBody(float mass, float linearDamping, float angularDamping)
{
	//mBodyDesc.angularDamping = 0.5;
	
	mBodyDesc.angularDamping = angularDamping;
	mBodyDesc.linearDamping = linearDamping;
	mBodyDesc.mass = mass;

	bool valid = mBodyDesc.isValid();

	mActorDesc.density = 0.f; //mass; //0.f;
	mActorDesc.body = &mBodyDesc;
}

void PhysicsBodyConstructor::SetTrigger()
{
	list<ShapeInfo>::iterator it;
	for (it = mShapeInfo.begin(); it != mShapeInfo.end(); ++it)
	{
		it->shapeDesc->shapeFlags |= NX_TRIGGER_ENABLE;
	}
}

void PhysicsBodyConstructor::SetCollisionGroup(NxShapeDesc* shape, int collisionGroup)
{
	shape->group = collisionGroup;
}


// =====================================================


void PhysicsBody::Init(PhysicsScene* scene, const PhysicsBodyConstructor& constructor)
{
	NxScene* nxScene = scene->GetHandle();

	bool valid = constructor.mActorDesc.isValid();
	mActor = nxScene->createActor(constructor.mActorDesc);
	mActor->userData = this;

	NxShape* const* shapes = mActor->getShapes();
	NxU32 nShapes = mActor->getNbShapes();
    while (nShapes--)
    {
        shapes[nShapes]->userData = this;
    }
}
/*
void PhysicsBody::Init(PhysicsScene* scene, PhysicsActorController* ctrl)
{
	SetActorController(ctrl);
	mActor = mController->mController->getActor();
}
*/
void PhysicsBody::ApplyLinearForce(const Vector4& force, Space space)
{
	switch (space)
	{
	case World: 
		mActor->addForce(NxVec3(force.x, force.y, force.z));
		break;

	case Local:
		mActor->addLocalForce(NxVec3(force.x, force.y, force.z));
		break;
	}
}

void PhysicsBody::ApplyLinearForce(const Vector4& force, const Vector4& point, Space forceSpace, Space pointSpace)
{
	switch (forceSpace)
	{
	case World:
		{
			switch (pointSpace)
			{
			case World:
				mActor->addForceAtPos(NxVec3(force.x, force.y, force.z), NxVec3(point.x, point.y, point.z));
				break;

			case Local:
				mActor->addForceAtLocalPos(NxVec3(force.x, force.y, force.z), NxVec3(point.x, point.y, point.z));
				break;
			}
		}
		break;

	case Local:
		{
			switch (pointSpace)
			{
			case World:
				mActor->addLocalForceAtPos(NxVec3(force.x, force.y, force.z), NxVec3(point.x, point.y, point.z));
				break;

			case Local:
				mActor->addLocalForceAtLocalPos(NxVec3(force.x, force.y, force.z), NxVec3(point.x, point.y, point.z));
				break;
			}
		}
		break;
	}
}

void PhysicsBody::ApplyAngularForce(const Vector4& force, const Vector4& point, Space forceSpace, Space pointSpace)
{

}

void PhysicsBody::ApplyAngularForce(const Vector4& force, Space space)
{
	switch (space)
	{
	case World:
		mActor->addTorque(NxVec3(&force.x));
		break;

	case Local:
		mActor->addLocalTorque(NxVec3(&force.x));
		break;
	}
}

Vector4 PhysicsBody::GetPointVelocity(const Vector4& point)
{
	return Vector4(mActor->getPointVelocity(NxVec3(point.x, point.y, point.z)).get());	
}

Vector4 PhysicsBody::GetLinearVelocity()
{
	return Vector4(mActor->getLinearVelocity().get());
}

void PhysicsBody::SetLinearVelocity(const Vector4& velocity)
{
	mActor->setLinearVelocity(NxVec3(velocity.x, velocity.y, velocity.z));
}

Vector4 PhysicsBody::GetAngularVelocity()
{
	return Vector4(mActor->getAngularVelocity().get());
}


void PhysicsBody::SetAngularVelocity(const Vector4& velocity)
{
	mActor->setAngularVelocity(NxVec3(velocity.x, velocity.y, velocity.z));
}

void PhysicsBody::SetPosition(const Vector4& position)
{
	mActor->setGlobalPosition(NxVec3(position.x, position.y, position.z));
}

Vector4	PhysicsBody::GetPosition()
{
	NxVec3 pos = mActor->getGlobalPosition();
	return Vector4(pos.x, pos.y, pos.z);
}

void PhysicsBody::SetCollisionGroup(int collisionGroup)
{
	NxShape* const* shapes = mActor->getShapes();
    NxU32 nShapes = mActor->getNbShapes();
    while (nShapes--)
    {
        shapes[nShapes]->setGroup(collisionGroup);
    }
}

void PhysicsBody::SetContactReport(ContactReport* report)		
{ 
	mContactReport = report;
/*
	NxShape* const* shapes = mActor->getShapes();
    NxU32 nShapes = mActor->getNbShapes();
    while (nShapes--)
    {
		shapes[nShapes]->setGroup(report == 0 ? 0 : 1);
    }*/
}

Matrix4	PhysicsBody::GetTransform()
{
	NxMat34 nxTransform = mActor->getGlobalPose();
	Matrix4 transform;
	nxTransform.getColumnMajor44(transform.m);
	return transform;
}

void PhysicsBody::SetTransform(const Matrix4& transform)
{
	NxMat34 nxTransform;
	nxTransform.setColumnMajor44(transform.m);

	if (IsKinematic())
		mActor->moveGlobalPose(nxTransform);
	else
		mActor->setGlobalPose(nxTransform);
}

void PhysicsBody::SetRotation(const Matrix4& transform)
{
	NxMat33 nxOrientation(NxVec3(transform.GetXAxis().x, transform.GetXAxis().y, transform.GetXAxis().z), 
		NxVec3(transform.GetYAxis().x, transform.GetYAxis().y, transform.GetYAxis().z), 
		NxVec3(transform.GetZAxis().x, transform.GetZAxis().y, transform.GetZAxis().z));

	if (IsKinematic())
		mActor->moveGlobalOrientation(nxOrientation);
	else
		mActor->setGlobalOrientation(nxOrientation);
}

void PhysicsBody::EnableGravity(bool enable)
{
	if (enable)
		mActor->clearBodyFlag(NX_BF_DISABLE_GRAVITY);
	else
		mActor->raiseBodyFlag(NX_BF_DISABLE_GRAVITY);
}

void PhysicsBody::Enable()
{
	mActor->clearActorFlag(NxActorFlag(NX_AF_DISABLE_COLLISION |  NX_AF_DISABLE_RESPONSE));
	mActor->wakeUp();
}

void PhysicsBody::Disable()
{
	mActor->raiseActorFlag(NxActorFlag(NX_AF_DISABLE_COLLISION |  NX_AF_DISABLE_RESPONSE));
	mActor->putToSleep();
}

bool PhysicsBody::IsDynamic()
{
	return mActor->isDynamic();
}

bool PhysicsBody::IsKinematic()
{
	return mActor->readBodyFlag(NX_BF_KINEMATIC);
}

void PhysicsBody::SetKinematic(bool kinematic)
{
	if (kinematic)
		mActor->raiseBodyFlag(NX_BF_KINEMATIC);
	else
		mActor->clearBodyFlag(NX_BF_KINEMATIC);
}