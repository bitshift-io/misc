#ifndef _PHYSICSACTORCONTROLLER_H_
#define _PHYSICSACTORCONTROLLER_H_

#include "PhysicsFactory.h"
#include "PhysicsBody.h"
#include "Math/Vector.h"
#include <list>

using namespace std;

class NxShapeDesc;

class PhysicsActorControllerConstructor : public PhysicsBodyConstructor
{
	friend class PhysicsActorController;

public:

	PhysicsActorControllerConstructor() :
	  mRadius(0.3f),
	  mHeight(1.2f)
	{
	}
/*
	~PhysicsActorControllerConstructor();

	void		AddBox();
	void		AddPlane();
	void		AddSphere(const Vector4& pos, float radius, const Vector4* initialVelocity = 0, bool staticShape = false);
	//void		AddTriMesh();
	//void		Add

	void		SetDynamicBody();

protected:

	NxActorDesc			mActorDesc;
	NxBodyDesc			mBodyDesc;
	list<NxShapeDesc*>	mShapeDesc;*/

	float	mRadius;
	float	mHeight;
};

//
// A group of smaller primitives to create a body
//
class PhysicsActorController : public PhysicsBody
{
public:

	enum Space
	{
		World,
		Local,
	};

	void			Init(PhysicsFactory* factory, PhysicsScene* scene, const PhysicsActorControllerConstructor& constructor);

	void			Move(const Vector4& distance);
	void			SetPosition(const Vector4& position);

	Vector4			GetPosition();

	PhysicsScene*	GetScene();

	void			SetCollisionGroup(int collisionGroup);

	PhysicsBody*	GetFloor()															{ return mFloor; }

/*
	void			SetUserData(void* data);
	void*			GetUserData();
*/
	virtual PhysicsActorController*	GetActorController()		{ return this; }

/*
	void	ApplyLinearForce(const Vector4& force, Space space = World);
	void	ApplyAngularForce(const Vector4& force, Space space = World);

protected:
*/
	NxController*	mController;
	int				mCollisionGroup;
	float			mGravity;
	bool			mOnGround;
	float			mTimeLastFrame;
	PhysicsBody*	mFloor;
};

#endif
