#include "PhysicsScene.h"
#include "PhysicsReport.h"
#include "PhysicsFactory.h"
#include "PhysicsBody.h"
#include "NxPhysics.h"
#include "Render/Device.h"

void InternalContactReport::onContactNotify(NxContactPair& pair, NxU32 events)
{
	unsigned int contactEvent = 0;
	if (events & NX_NOTIFY_ON_START_TOUCH)
		contactEvent |= CF_NotifyOnStartTouch;

	if (events & NX_NOTIFY_ON_END_TOUCH)
		contactEvent |= CF_NotifyOnEndTouch;

	if (events & NX_NOTIFY_ON_TOUCH)
		contactEvent |= CF_NotifyOnTouch;

	if (events & NX_NOTIFY_ON_IMPACT)
		contactEvent |= CF_NotifyOnImpact;

	if (events & NX_NOTIFY_ON_ROLL)
		contactEvent |= CF_NotifyOnRoll;

	if (events & NX_NOTIFY_ON_SLIDE)
		contactEvent |= CF_NofityOnSlide;

	ContactInfoIterator contactIt(pair.stream);
	
	if (pair.actors[0]->userData)
	{
		ContactReport* report = static_cast<PhysicsBody*>(pair.actors[0]->userData)->GetContactReport();
		if (report)
		{
			report->OnContactNotify(static_cast<PhysicsBody*>(pair.actors[0]->userData), static_cast<PhysicsBody*>(pair.actors[1]->userData), contactIt, ContactFlags(contactEvent));
		}
	}

	if (pair.actors[1]->userData)
	{
		ContactReport* report = static_cast<PhysicsBody*>(pair.actors[1]->userData)->GetContactReport();
		if (report)
		{
			report->OnContactNotify(static_cast<PhysicsBody*>(pair.actors[0]->userData), static_cast<PhysicsBody*>(pair.actors[1]->userData), contactIt, ContactFlags(contactEvent));
		}		
	}
}



Vector4 GetColor(const NxU32 color)
{
	NxF32 blue	= NxF32((color)&0xff)/255.0f;
	NxF32 green	= NxF32((color>>8)&0xff)/255.0f;
	NxF32 red	= NxF32((color>>16)&0xff)/255.0f;
	return Vector4(blue, green, red);
}


PhysicsScene::PhysicsScene(PhysicsFactory* factory) : 
	mPhysicsFactory(factory)
{
}

bool PhysicsScene::Init(const Vector4& gravity)
{
	mScene = 0;
	if (!mPhysicsFactory->GetHandle())
		return false;

	// Create a scene
	NxSceneDesc sceneDesc;
	sceneDesc.gravity				= NxVec3(&gravity.x);
	mScene = mPhysicsFactory->GetHandle()->createScene(sceneDesc);
	mScene->userData = this;

	NxMaterial* defaultMaterial = mScene->getMaterialFromIndex(0); 
	defaultMaterial->setRestitution(0.0f);
	defaultMaterial->setStaticFriction(0.5f);
	defaultMaterial->setDynamicFriction(0.5f);

	for (int i = 0; i < 8; ++i)
		mScene->setActorGroupPairFlags(i, i, NX_NOTIFY_ON_START_TOUCH | NX_NOTIFY_ON_END_TOUCH | NX_NOTIFY_ON_TOUCH | NX_NOTIFY_ON_IMPACT | NX_NOTIFY_ON_ROLL | NX_NOTIFY_ON_SLIDE);

	mScene->setUserContactReport(&mContactReport);

	mTimeLastFrame = -1.f; 

	for (int i = 0; i < 32; ++i)
		mCollisionGroupFlags[i] = 0;

	return true;
}

void PhysicsScene::Deinit()
{
	if (mPhysicsFactory->GetHandle())
		mPhysicsFactory->GetHandle()->releaseScene(*mScene);

	mScene = 0;
}

void PhysicsScene::EnableCollisionBetweenGroup(int group1, int group2, bool enable)	
{
	mScene->setGroupCollisionFlag(group1, group2, enable); 
}


void PhysicsScene::Update(float deltaTime)
{/*
	// calualate delta time
	float curTime = gTime.GetSeconds();

	if (mTimeLastFrame == -1.f)
		mTimeLastFrame = curTime;

	float deltaTime = curTime - mTimeLastFrame;
	mTimeLastFrame = curTime;
*/
	if (!mScene)
		return;

	mScene->simulate(deltaTime);
	mScene->flushStream();
	while (!mScene->fetchResults(NX_RIGID_BODY_FINISHED, false));
}

void PhysicsScene::Draw(Device* device) const
{
	device->SetMatrix(Matrix4(MI_Identity), MM_Model);
	const NxDebugRenderable& data = *mScene->getDebugRenderable();

	// Render points
	NxU32 nbPoints = data.getNbPoints();
	if (nbPoints)
	{
		const NxDebugPoint* points = data.getPoints();

		while(nbPoints--)
		{
			device->DrawPoint(Vector4(points->p.x, points->p.y, points->p.z), GetColor(points->color));
			points++;
		}
	}

	// Render lines
	NxU32 nbLines = data.getNbLines();
	if (nbLines)
	{
		const NxDebugLine* lines = data.getLines();

		while(nbLines--)
		{
			device->DrawLine(Vector4(lines->p0.x, lines->p0.y, lines->p0.z), Vector4(lines->p1.x, lines->p1.y, lines->p1.z), GetColor(lines->color));
			lines++;
		}
	}

	// Render triangles
	NxU32 nbTris = data.getNbTriangles();
	if (nbTris)
	{
		const NxDebugTriangle* triangles = data.getTriangles();
/*
		glBegin(GL_TRIANGLES);
		while(nbTris--)
		{
			SetYAxisColor(Triangles->color);
			glVertex3fv(&Triangles->p0.x);
			glVertex3fv(&Triangles->p1.x);
			glVertex3fv(&Triangles->p2.x);
			Triangles++;
		}
		glEnd();*/
	}

	// draw actors
	int nbActors = mScene->getNbActors();
    NxActor** actors = mScene->getActors();
    while (nbActors--)
    {
		NxActor* actor = *actors++;
		DrawActor(device, actor);
	}
}








void DrawCircle(NxU32 nbSegments, const NxMat34& matrix, const NxVec3& color, const NxF32 radius, const bool semicircle)
{/*
	NxF32 step = NxTwoPiF32/NxF32(nbSegments);
	NxU32 segs = nbSegments;
	if (semicircle)	
	{
		segs /= 2;
	}

	for(NxU32 i=0;i<segs;i++)
	{
		NxU32 j=i+1;
		if(j==nbSegments)	j=0;

		NxF32 angle0 = NxF32(i)*step;
		NxF32 angle1 = NxF32(j)*step;

		NxVec3 p0,p1;
		matrix.multiply(NxVec3(radius * sinf(angle0), radius * cosf(angle0), 0.0f), p0);
		matrix.multiply(NxVec3(radius * sinf(angle1), radius * cosf(angle1), 0.0f), p1);

		DrawLine(p0, p1, color);
	}
	glColor3f(1,1,1);*/
}

void DrawArrow(const NxVec3& posA, const NxVec3& posB, const NxVec3& color)
{/*
	NxVec3 vec = posB - posA;
	NxVec3 t0, t1, t2;
	NxNormalToTangents(vec, t1, t2);

	t0 = posB - posA;
	t0.normalize();

	NxVec3 lobe1  = posB - t0*0.15 + t1 * 0.15;
	NxVec3 lobe2  = posB - t0*0.15 - t1 * 0.15;
	NxVec3 lobe3  = posB - t0*0.15 + t2 * 0.15;
	NxVec3 lobe4  = posB - t0*0.15 - t2 * 0.15;

	glPushMatrix();
	glDisable(GL_LIGHTING);
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(color.x,color.y,color.z);
	glVertex3fv(&posA.x);
	glVertex3fv(&posB.x);
	glVertex3fv(&posB.x);
	glVertex3fv(&lobe1.x);
	glVertex3fv(&posB.x);
	glVertex3fv(&lobe2.x);
	glVertex3fv(&posB.x);
	glVertex3fv(&lobe3.x);
	glVertex3fv(&posB.x);
	glVertex3fv(&lobe4.x);
	glEnd();
	glEnable(GL_LIGHTING);
	glPopMatrix();
	glColor3f(1,1,1);*/
}

void DrawContactPoint(const NxVec3& pos, const NxReal radius, const NxVec3& color)
{/*
	NxMat34 pose;
	pose.t = pos;

	NxVec3 c0;	pose.M.getColumn(0, c0);
	NxVec3 c1;	pose.M.getColumn(1, c1);
	NxVec3 c2;	pose.M.getColumn(2, c2);
	DrawCircle(20, pose, color, radius);

	pose.M.setColumn(0, c1);
	pose.M.setColumn(1, c2);
	pose.M.setColumn(2, c0);
	DrawCircle(20, pose, color, radius);

	pose.M.setColumn(0, c2);
	pose.M.setColumn(1, c0);
	pose.M.setColumn(2, c1);
	DrawCircle(20, pose, color, radius);*/
}



void SetYAxisGLMatrix(Device* device, const NxVec3& pos, const NxMat33& orient)
{
	float glmat[16];	//4x4 column major matrix for OpenGL.
	orient.getColumnMajorStride4(&(glmat[0]));
	pos.get(&(glmat[12]));

	//clear the elements we don't need:
	glmat[3] = glmat[7] = glmat[11] = 0.0f;
	glmat[15] = 1.0f;

	device->SetMatrix(Matrix4(glmat), MM_Model);
}

void DrawWirePlane(Device* device, NxShape* plane, const NxVec3& color)
{
	NxMat34 pose = plane->getGlobalPose();

	SetYAxisGLMatrix(device, pose.t, pose.M);

	device->DrawLine(Vector4(-1,0,-1), Vector4(-1,0,1));
	device->DrawLine(Vector4(-1,0,1), Vector4(1,0,1));
	device->DrawLine(Vector4(1,0,1), Vector4(1,0,-1));
	device->DrawLine(Vector4(1,0,-1), Vector4(-1,0,-1));
	
	device->SetMatrix(Matrix4(MI_Identity), MM_Model);
}


void DrawPlane(NxShape* plane)
{/*
	NxMat34 pose = plane->getGlobalPose();

	glPushMatrix();
	glDisable(GL_LIGHTING);
	glColor3f(0.1f, 0.2f, 0.3f);
	pose.t.y -= 0.1f;
	SetYAxisGLMatrix(pose.t, pose.M);
	glScaled(1024,0,1024);
	glCallList(dispListPlane);
	glColor3f(1.0f, 1.0f, 1.0f);
	glEnable(GL_LIGHTING);
	glPopMatrix();*/
}

// Draw Test AABB and OBB
void DrawWireBox(NxBox* obb, const NxVec3& color)
{/*
	// Compute obb vertices
	NxVec3 pp[8];
	NxComputeBoxPoints(*obb, pp);
		
	// Draw all lines
	const NxU32* Indices = NxGetBoxEdges();
	for(NxU32 i=0;i<12;i++)
	{
		NxU32 VRef0 = *Indices++;
		NxU32 VRef1 = *Indices++;
		DrawLine(pp[VRef0], pp[VRef1], color);
	}*/
}

void DrawWireBox(Device* device, NxShape* box, const NxVec3& color)
{
	NxBox obb;
	box->isBox()->getWorldOBB(obb);

	// Compute obb vertices
	NxVec3 pp[8];
	NxComputeBoxPoints(obb, pp);

	// Draw all lines
	const NxU32* Indices = NxGetBoxEdges();
	for(NxU32 i=0;i<12;i++)
	{
		NxU32 VRef0 = *Indices++;
		NxU32 VRef1 = *Indices++;
		device->DrawLine(Vector4(&pp[VRef0].x), Vector4(&pp[VRef1].x), Vector4(&color.x));
	}
}

void DrawBox(NxShape *box)
{/*
	NxMat34 pose = box->getGlobalPose();

	glPushMatrix();
	SetYAxisGLMatrix(pose.t, pose.M);
	NxVec3 boxDim = box->isBox()->getDimensions();
	glScaled(boxDim.x, boxDim.y, boxDim.z);
	glCallList(dispListBox);
	glPopMatrix();*/
}

void DrawWireSphere(NxSphere* sphere, const NxVec3& color)
{/*
	NxMat34 pose;

	pose.t = sphere->center;

	glPushMatrix();
	NxReal r = sphere->radius;

	NxVec3 c0;	pose.M.getColumn(0, c0);
	NxVec3 c1;	pose.M.getColumn(1, c1);
	NxVec3 c2;	pose.M.getColumn(2, c2);
	DrawCircle(20, pose, color, r);

	pose.M.setColumn(0, c1);
	pose.M.setColumn(1, c2);
	pose.M.setColumn(2, c0);
	DrawCircle(20, pose, color, r);

	pose.M.setColumn(0, c2);
	pose.M.setColumn(1, c0);
	pose.M.setColumn(2, c1);
	DrawCircle(20, pose, color, r);

	glPopMatrix();*/
}

void DrawWireSphere(Device* device, NxShape* sphere, const NxVec3& color)
{/*
	NxMat34 pose = sphere->getGlobalPose();

	glPushMatrix();
	NxReal r = sphere->isSphere()->getRadius();

	NxVec3 c0;	pose.M.getColumn(0, c0);
	NxVec3 c1;	pose.M.getColumn(1, c1);
	NxVec3 c2;	pose.M.getColumn(2, c2);
	DrawCircle(20, pose, color, r);

	pose.M.setColumn(0, c1);
	pose.M.setColumn(1, c2);
	pose.M.setColumn(2, c0);
	DrawCircle(20, pose, color, r);

	pose.M.setColumn(0, c2);
	pose.M.setColumn(1, c0);
	pose.M.setColumn(2, c1);
	DrawCircle(20, pose, color, r);

	glPopMatrix();*/
}

void DrawSphere(NxShape *sphere)
{/*
	NxMat34 pose = sphere->getGlobalPose();

	glPushMatrix();
	SetYAxisGLMatrix(pose.t, pose.M);
	NxReal r = sphere->isSphere()->getRadius();
	glScaled(r,r,r);
	glCallList(dispListSphere);
	glPopMatrix();*/
}

// Draw Test Capsule
void DrawWireCapsule(Device* device, NxCapsule* capsule, const NxVec3& color)
{/*
	NxMat34 pose;
	pose.id();

	NxVec3 axis, binormal, normal;

	axis = capsule->computeDirection();
	axis.normalize();
	NxNormalToTangents(axis, binormal, normal);
	NxMat33 orient(normal, axis, binormal);

	pose.M = orient;
	capsule->computePoint(pose.t, 0.5);

	glPushMatrix();
	NxReal r = capsule->radius;
	NxReal h = capsule->computeLength();

	NxSegment SG;
	pose.M.getColumn(1, SG.p1);
	SG.p1 *= 0.5*h;
	SG.p0 = -SG.p1;
	SG.p0 += pose.t;
	SG.p1 += pose.t;

	NxVec3 c0;	pose.M.getColumn(0, c0);
	NxVec3 c1;	pose.M.getColumn(1, c1);
	NxVec3 c2;	pose.M.getColumn(2, c2);
	DrawLine(SG.p0 + c0*r, SG.p1 + c0*r, color);
	DrawLine(SG.p0 - c0*r, SG.p1 - c0*r, color);
	DrawLine(SG.p0 + c2*r, SG.p1 + c2*r, color);
	DrawLine(SG.p0 - c2*r, SG.p1 - c2*r, color);

	pose.M.setColumn(0, -c1);
	pose.M.setColumn(1, -c0);
	pose.M.setColumn(2, c2);
	pose.t = SG.p0;
	DrawCircle(20, pose, color, r, true);  // halfcircle -- flipped

	pose.M.setColumn(0, c1);
	pose.M.setColumn(1, -c0);
	pose.M.setColumn(2, c2);

	pose.t = SG.p1;
	DrawCircle(20, pose, color, r, true);

	pose.M.setColumn(0, -c1);
	pose.M.setColumn(1, c2);
	pose.M.setColumn(2, c0);
	pose.t = SG.p0;
	DrawCircle(20, pose, color, r, true);  // halfcircle -- good

	pose.M.setColumn(0, c1);
	pose.M.setColumn(1, c2);
	pose.M.setColumn(2, c0);
	pose.t = SG.p1;
	DrawCircle(20, pose, color, r, true);

	pose.M.setColumn(0, c2);
	pose.M.setColumn(1, c0);
	pose.M.setColumn(2, c1);
	pose.t = SG.p0;
	DrawCircle(20, pose, color, r);  // full circle
	pose.t = SG.p1;
	DrawCircle(20, pose, color, r);*/
}

void DrawWireCapsule(Device* device, NxShape* capsule, const NxVec3& color)
{/*
	NxMat34 pose = capsule->getGlobalPose();

	const NxReal & r = capsule->isCapsule()->getRadius();
	const NxReal & h = capsule->isCapsule()->getHeight();

	NxSegment SG;
	pose.M.getColumn(1, SG.p1);
	SG.p1 *= 0.5*h;
	SG.p0 = -SG.p1;
	SG.p0 += pose.t;
	SG.p1 += pose.t;

	NxVec3 c0;	pose.M.getColumn(0, c0);
	NxVec3 c1;	pose.M.getColumn(1, c1);
	NxVec3 c2;	pose.M.getColumn(2, c2);
	DrawLine(SG.p0 + c0*r, SG.p1 + c0*r, color);
	DrawLine(SG.p0 - c0*r, SG.p1 - c0*r, color);
	DrawLine(SG.p0 + c2*r, SG.p1 + c2*r, color);
	DrawLine(SG.p0 - c2*r, SG.p1 - c2*r, color);

	pose.M.setColumn(0, -c1);
	pose.M.setColumn(1, -c0);
	pose.M.setColumn(2, c2);
	pose.t = SG.p0;
	DrawCircle(20, pose, color, r, true);	//halfcircle -- flipped

	pose.M.setColumn(0, c1);
	pose.M.setColumn(1, -c0);
	pose.M.setColumn(2, c2);
	pose.t = SG.p1;
	DrawCircle(20, pose, color, r, true);

	pose.M.setColumn(0, -c1);
	pose.M.setColumn(1, c2);
	pose.M.setColumn(2, c0);
	pose.t = SG.p0;
	DrawCircle(20, pose, color, r, true);//halfcircle -- good

	pose.M.setColumn(0, c1);
	pose.M.setColumn(1, c2);
	pose.M.setColumn(2, c0);
	pose.t = SG.p1;
	DrawCircle(20, pose, color, r, true);

	pose.M.setColumn(0, c2);
	pose.M.setColumn(1, c0);
	pose.M.setColumn(2, c1);
	pose.t = SG.p0;
	DrawCircle(20, pose, color, r);	//full circle
	pose.t = SG.p1;
	DrawCircle(20, pose, color, r);*/
}

void DrawCapsule(NxShape *capsule)
{/*
	NxMat34 pose = capsule->getGlobalPose();

	const NxReal & r = capsule->isCapsule()->getRadius();
	const NxReal & h = capsule->isCapsule()->getHeight();

	glPushMatrix();
	SetYAxisGLMatrix(pose.t, pose.M);

	glPushMatrix();
	glTranslated(0.0f, h*0.5f, 0.0f);
	glScaled(r,r,r);
	glCallList(dispListSphere);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.0f,-h*0.5f, 0.0f);
	glScaled(r,r,r);
	glCallList(dispListSphere);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.0f,h*0.5f, 0.0f);
	glScaled(r,h,r);
	glRotated(90.0f,1.0f,0.0f,0.0f);
	glCallList(dispListCylinder);
	glPopMatrix();

	glPopMatrix();*/
}

typedef NxVec3 Point;
typedef struct _Triangle { NxU32 p0; NxU32 p1; NxU32 p2; } Triangle;

void DrawWireConvex(Device* device, NxShape* mesh, const NxVec3& color)
{/*
    if (!mesh->userData)  return;

	NxMat34 pose = mesh->getGlobalPose();

	NxConvexMeshDesc meshDesc = *((NxConvexMeshDesc*)(mesh->userData));
//	mesh->isConvexMesh()->getConvexMesh().saveToDesc(meshDesc);

	NxU32 nbVerts = meshDesc.numVertices;
	NxU32 nbTriangles = meshDesc.numTriangles;

	Point* points = (Point *)meshDesc.points;
	Triangle* triangles = (Triangle *)meshDesc.triangles;

	glPushMatrix();

	float glmat[16];	// 4x4 column major matrix for OpenGL
	pose.M.getColumnMajorStride4(&(glmat[0]));
	pose.t.get(&(glmat[12]));

	// Clear the elements we don't need
	glmat[3] = glmat[7] = glmat[11] = 0.0f;
	glmat[15] = 1.0f;

	glMultMatrixf(&(glmat[0]));

	glPushMatrix();
	glBegin(GL_TRIANGLES);
	while(nbTriangles--)
	{
		DrawLine(points[triangles->p0], points[triangles->p1], color);
		DrawLine(points[triangles->p1], points[triangles->p2], color);
		DrawLine(points[triangles->p2], points[triangles->p0], color);
		triangles++;
	}
	glEnd();
	glPopMatrix();

	glPopMatrix();*/
}

void DrawConvex(Device* device, NxShape* mesh)
{/*
    if (!mesh->userData)  return;

	NxMat34 pose = mesh->getGlobalPose();

	NxConvexMeshDesc meshDesc = *((NxConvexMeshDesc*)(mesh->userData));
//	mesh->isConvexMesh()->getConvexMesh().saveToDesc(meshDesc);

	NxU32 nbVerts = meshDesc.numVertices;
	NxU32 nbTriangles = meshDesc.numTriangles;

	Point* points = (Point *)meshDesc.points;
	Triangle* triangles = (Triangle *)meshDesc.triangles;

	glPushMatrix();

	float glmat[16];	// 4x4 column major matrix for OpenGL
	pose.M.getColumnMajorStride4(&(glmat[0]));
	pose.t.get(&(glmat[12]));

	// Clear the elements we don't need
	glmat[3] = glmat[7] = glmat[11] = 0.0f;
	glmat[15] = 1.0f;

	glMultMatrixf(&(glmat[0]));

	glPushMatrix();
	glBegin(GL_TRIANGLES);
	while(nbTriangles--)
	{
		glVertex3fv(&points[triangles->p0].x);
		glVertex3fv(&points[triangles->p1].x);
		glVertex3fv(&points[triangles->p2].x);
		triangles++;
	}
	glEnd();
	glPopMatrix();

	glPopMatrix();*/
}

void DrawWireMesh(Device* device, NxShape* mesh, const NxVec3& color)
{/*
    if (!mesh->userData)  
		return;

	Matrix4 transform;
	NxMat34 pose = mesh->getGlobalPose();
	pose.getColumnMajor44(transform.m);
	device->SetMatrix(transform, Device::Model);

	NxTriangleMeshDesc meshDesc = *((NxTriangleMeshDesc*)(mesh->userData));

	NxU32 nbVerts = meshDesc.numVertices;
	NxU32 nbTriangles = meshDesc.numTriangles;

	Point* points = (Point *)meshDesc.points;
	Triangle* triangles = (Triangle *)meshDesc.triangles;

	while (nbTriangles--)
	{
		float* p0 = (float*)&points[triangles->p0];
		float* p1 = (float*)&points[triangles->p1];
		float* p2 = (float*)&points[triangles->p2];
		float* colour = (float*)&color;

		device->DrawLine(Vector4(p0), Vector4(p1), Vector4(colour));
		device->DrawLine(Vector4(p1), Vector4(p2), Vector4(colour));
		device->DrawLine(Vector4(p2), Vector4(p0), Vector4(colour));
		triangles++;
	}*/
}

void DrawMesh(Device* device, NxShape* mesh)
{/*
    if (!mesh->userData)  return;

	NxMat34 pose = mesh->getGlobalPose();

	NxTriangleMeshDesc meshDesc = *((NxTriangleMeshDesc*)(mesh->userData));
//	mesh->isTriangleMesh()->getTriangleMesh().saveToDesc(meshDesc);

	NxU32 nbVerts = meshDesc.numVertices;
	NxU32 nbTriangles = meshDesc.numTriangles;

	Point* points = (Point *)meshDesc.points;
	Triangle* triangles = (Triangle *)meshDesc.triangles;

	glPushMatrix();

	float glmat[16];	//4x4 column major matrix for OpenGL.
	pose.M.getColumnMajorStride4(&(glmat[0]));
	pose.t.get(&(glmat[12]));

	//clear the elements we don't need:
	glmat[3] = glmat[7] = glmat[11] = 0.0f;
	glmat[15] = 1.0f;

	glMultMatrixf(&(glmat[0]));

	glPushMatrix();
	glBegin(GL_TRIANGLES);
	while(nbTriangles--)
	{
		glVertex3fv(&points[triangles->p0].x);
		glVertex3fv(&points[triangles->p1].x);
		glVertex3fv(&points[triangles->p2].x);
		triangles++;
	}
	glEnd();
	glPopMatrix();

	glPopMatrix();*/
}

void DrawWireShape(Device* device, NxShape *shape, const NxVec3& color)
{
    switch(shape->getType())
    {
		case NX_SHAPE_PLANE:
			DrawWirePlane(device, shape, color);
			break;
		case NX_SHAPE_BOX:
			DrawWireBox(device, shape, color);
			break;
		case NX_SHAPE_SPHERE:
			DrawWireSphere(device, shape, color);
			break;
		case NX_SHAPE_CAPSULE:
			DrawWireCapsule(device, shape, color);
			break;
		case NX_SHAPE_CONVEX:
			DrawWireConvex(device, shape, color);
			break;		
		case NX_SHAPE_MESH:
			DrawWireMesh(device, shape, color);
			break;
	}
}

void PhysicsScene::DrawActor(Device* device, NxActor* actor) const
{
	NxShape*const* shapes = actor->getShapes();
	NxU32 nShapes = actor->getNbShapes();

	nShapes = actor->getNbShapes();
	while (nShapes--)
	{
		if (shapes[nShapes]->getFlag(NX_TRIGGER_ENABLE))
			DrawWireShape(device, shapes[nShapes], NxVec3(0,0,1));
		else
			DrawWireShape(device, shapes[nShapes], NxVec3(1,1,1));
	}
}
/*
class RaycastReport : public NxUserRaycastReport
{
public:

	virtual bool onHit(const NxRaycastHit& hits)
	{
		if (mDistance >= 0.f && mDistance > hits.distance)
			return false;

		if (!mResult)
			return true;

		RaycastResult result;
		mResult->push_back(result);
		return true;
	}

	list<RaycastResult>*	mResult;
	float					mDistance;
};

int PhysicsScene::Raycast(const Vector4& point, const Vector4& direction, float distance, list<RaycastResult>* result, unsigned int group)
{
	RaycastReport report;
	report.mResult = result;
	report.mDistance = distance;
	NxU32 count = GetHandle()->raycastAllShapes(NxRay(point.vec, direction.vec), report, NX_ALL_SHAPES, group);
	return count;
}*/
