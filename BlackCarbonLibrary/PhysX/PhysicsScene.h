#ifndef _PHYSICSSCENE_H_
#define _PHYSICSSCENE_H_

#include "NxUserContactReport.h"
#include "Math/Vector.h"
#include <list>

using namespace std;

class Device;
class PhysicsFactory;
class NxScene;
class NxActor;
class PhysicsBody;

enum CollisionGroupFlags
{
	CGF_None		= 0,
	CGF_Pushable	= 1 << 0,
};

struct RaycastResult
{
	PhysicsBody*	body;
	Vector4			impactPoint;
	Vector4			impactNormal;
	float			distance;
};

class InternalContactReport : public NxUserContactReport
{
public:
	virtual void onContactNotify(NxContactPair& pair, NxU32 events);
};

// should this store pointers to the bodies so we can iterate through them?
class PhysicsScene
{
	friend class PhysicsFactory;

public:

	bool			Init(const Vector4& gravity = Vector4(0.f, -9.8f, 0.f));
	void			Deinit();

	void			Update(float deltaTime);


	NxScene*		GetHandle()												{ return mScene; }
	PhysicsFactory*	GetFactory()											{ return mPhysicsFactory; }

	// for debuggging
	void			Draw(Device* device) const;

	void			SetCollisionGroupFlags(int collisionGroup, int flags)	{ mCollisionGroupFlags[collisionGroup] = flags; }
	int				GetCollisionGroupFlags(int collisionGroup)				{ return mCollisionGroupFlags[collisionGroup]; }
	void			EnableCollisionBetweenGroup(int group1, int group2, bool enable);

	//int				Raycast(const Vector4& point, const Vector4& direction, float distance = -1, list<RaycastResult>* result = 0, unsigned int group = -1);

protected:

	PhysicsScene(PhysicsFactory* factory);

	void			DrawActor(Device* device, NxActor* actor) const;

	NxScene*		mScene;
	PhysicsFactory*	mPhysicsFactory;
	float			mTimeLastFrame;

	int				mCollisionGroupFlags[32];

	InternalContactReport	mContactReport;
};

#endif
