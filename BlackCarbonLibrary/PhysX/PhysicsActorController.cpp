#include "PhysicsActorController.h"
#include "NxBoxController.h"
#include "NxCapsuleController.h"
#include "PhysicsScene.h"
#include "PhysicsBody.h"

#define SKINWIDTH	0.1f
#define HEIGHT 1.2f
#define HEIGHT_OFFSET 1.8f * 0.5f

class HitReport : public NxUserControllerHitReport
{
public:
	virtual NxControllerAction  onShapeHit(const NxControllerShapeHit& hit)
	{
		//PhysicsBody* controllerBody = (PhysicsBody*)hit.controller->getActor()->userData;
		PhysicsActorController* controller = (PhysicsActorController*)hit.controller->getActor()->userData;
		ContactReport* report = controller->GetContactReport();
		if (report)
		{
			ContactInfoIterator contactIt;
			contactIt.mInfo.normal = Vector4(hit.worldNormal.x, hit.worldNormal.y, hit.worldNormal.z);
			contactIt.mInfo.position = Vector4(hit.worldPos.x, hit.worldPos.y, hit.worldPos.z);
			contactIt.mEnd = 1;

			report->OnActorContact(static_cast<PhysicsBody*>(hit.shape->getActor().userData), controller, contactIt);
		}
		else
		{
			if (!hit.shape)
				return NX_ACTION_NONE;

			PhysicsBody* body = (PhysicsBody*)hit.shape->userData;
			if (body && hit.dir.y < 0.f)
				controller->mFloor = body;

			NxCollisionGroup group = hit.shape->getGroup();
			int groupFlags = controller->GetScene()->GetCollisionGroupFlags(group);

			if (groupFlags & CGF_Pushable)
			{
				NxActor& actor = hit.shape->getActor();
				if (actor.isDynamic())
				{
					// We only allow horizontal pushes. Vertical pushes when we stand on dynamic objects creates
					// useless stress on the solver. It would be possible to enable/disable vertical pushes on
					// particular objects, if the gameplay requires it.
					if (hit.dir.y == 0.0f)
					{
						NxF32 coeff = actor.getMass() * hit.length * 10.0f;
						actor.addForceAtLocalPos(hit.dir*coeff, NxVec3(0,0,0), NX_IMPULSE);
	//						actor.addForceAtPos(hit.dir*coeff, hit.controller->getPosition(), NX_IMPULSE);
	//						actor.addForceAtPos(hit.dir*coeff, hit.worldPos, NX_IMPULSE);
					}
				}
			}
		}

		return NX_ACTION_NONE;
	}

	virtual NxControllerAction  onControllerHit(const NxControllersHit& hit)
	{
		return NX_ACTION_NONE;
	}

}gMyReport;

void PhysicsActorController::Init(PhysicsFactory* factory, PhysicsScene* scene, const PhysicsActorControllerConstructor& constructor)
{
//#define BOX_CONTROLLER

#ifndef BOX_CONTROLLER
	NxCapsuleControllerDesc desc;
#else
	NxBoxControllerDesc desc;
#endif
	desc.position.x		= 0;
	desc.position.y		= 0;
	desc.position.z		= 0;

#ifdef BOX_CONTROLLER
	desc.extents.x = 0.3f;
	desc.extents.y = HEIGHT * 0.5f;
	desc.extents.z = 0.3f;
	desc.stepOffset		= desc.extents.x * 0.5f;
#else
	desc.radius			= constructor.mRadius; //0.3f;
	desc.height			= constructor.mHeight; //HEIGHT;
	desc.stepOffset		= desc.radius * 0.5f;
#endif

	desc.upDirection	= NX_Y;
	desc.slopeLimit		= 0.f; //NxMath::degToRad(60.0f);
	desc.skinWidth		= 0.01f;
	desc.callback		= &gMyReport;

	mController = factory->GetActorController().createController(scene->GetHandle(), desc);
	mController->getActor()->userData = this;
	//mPhysicsBody.SetActorController(this);
	mActor = mController->getActor();

	SetCollisionGroup(1);

	mGravity = 0.f;
	mTimeLastFrame = -1.f;
	mFloor = 0;

	//PhysicsBody::Init(scene, constructor);
}

void PhysicsActorController::Move(const Vector4& distance)
{
	mFloor = 0;
	/*
	// apply gravity
	float curTime = gTime.GetSeconds();

	if (mTimeLastFrame == -1.f)
		mTimeLastFrame = curTime;

	float deltaTime = curTime - mTimeLastFrame;
	mTimeLastFrame = curTime;

	mGravity -= 9.8f * deltaTime;
*/
	NxU32 activeGroups = 0;
	for (int i = 1; i < 32; ++i)
	{
		bool enable = GetScene()->GetHandle()->getGroupCollisionFlag(mCollisionGroup, i);
		if (enable)
			activeGroups |= 1 << i;
	}

		//getGroupCollisionFlag
	//NxU32 activeGroups = -1; //GetScene()->GetHandle()->getGroupCollisionFlag(mCont; //-1;
	NxU32 collisionFlags = NX_ACTION_PUSH ; //COLLIDABLE_MASK;
	mController->move(NxVec3(distance.x, distance.y + mGravity, distance.z), activeGroups, 0.f, collisionFlags);
}

void PhysicsActorController::SetPosition(const Vector4& position)
{
	mController->setPosition(NxExtendedVec3(position.x, position.y + HEIGHT_OFFSET, position.z));
}

Vector4	PhysicsActorController::GetPosition()
{
	const NxExtendedVec3& pos = mController->getPosition();
	return Vector4((float)pos.x, (float)pos.y - HEIGHT_OFFSET, (float)pos.z);
}

PhysicsScene* PhysicsActorController::GetScene()
{
	return (PhysicsScene*)mController->getActor()->getScene().userData;
}

void PhysicsActorController::SetCollisionGroup(int collisionGroup)
{
	mCollisionGroup = collisionGroup;

	NxShape* const* shapes = mController->getActor()->getShapes();
    NxU32 nShapes = mController->getActor()->getNbShapes();
    while (nShapes--)
    {
        shapes[nShapes]->setGroup(collisionGroup);
    }

	//mController->getActor()->setGroup(collisionGroup);
}
/*
void PhysicsActorController::SetUserData(void* data)
{
	mPhysicsBody.SetUserData(data);
}

void* PhysicsActorController::GetUserData()
{
	return mPhysicsBody.GetUserData();
}*/
