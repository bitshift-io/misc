#ifndef _NOMEMORY_H_
#define _NOMEMORY_H_

#undef	new
#undef	delete
#undef	malloc
#undef	calloc
#undef	realloc
#undef	free
#undef	_aligned_malloc
#undef	_aligned_free

#endif
