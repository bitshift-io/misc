#include "Memory.h"

using namespace std;

unsigned int MemTracker::mAllocationCount = 0;
unsigned int MemTracker::mAllocationSize = 0;

unsigned int MemTracker::mPrefixPattern          = 0xbaadf00d; // Fill pattern for bytes preceeding allocated blocks
unsigned int MemTracker::mPostfixPattern         = 0xdeadc0de; // Fill pattern for bytes following allocated blocks
unsigned int MemTracker::mUnusedPattern          = 0xfeedface; // Fill pattern for freshly allocated blocks
unsigned int MemTracker::mReleasedPattern        = 0xdeadbeef; // Fill pattern for deallocated blocks

const char*		MemTracker::mSourceFile = "";
unsigned int	MemTracker::mSourceLine = 0;
const char*		MemTracker::mSourceFunc = "";

unsigned int	MemTracker::mAllocationNumber = 0;
unsigned int	MemTracker::mBreakOnAllocNumber = -1;

bool			MemTracker::mEnableLog = true;

MemTracker::Allocation*		MemTracker::mAllocations = 0;
MemTracker::MemoryGroup*	MemTracker::mMemoryGroups = 0;
MemTracker::MemoryGroup*	MemTracker::mOldMemoryGroups = 0;

#ifdef MULTITHREAD
	Mutex						MemTracker::mMutex;
#endif

const char* MemTracker::mAllocationTypes[] =
{
	"Unknown",
	"new",
	"new[]",
	"malloc",
	"calloc",
	"realloc",
	"delete",
	"delete[]",
	"free",
	"_aligned_malloc",
	"_aligned_free",
};

//
// Turn off my macros
//
#include "NoMemory.h"

void MemTracker::PushMemoryGroup(const char* name)
{
	// create new memory group and push on to stack
	MemoryGroup* group = new MemoryGroup;
	group->mName = strdup(name);

	group->mNext = mMemoryGroups;
	group->mPrevious = 0;

	if (mMemoryGroups)
		mMemoryGroups->mPrevious = group;

	mMemoryGroups = group;
}

void MemTracker::PopMemoryGroup()
{
	// pop from the current stack and put in to a keeper list till app exists
	MemoryGroup* group = mMemoryGroups;

	mMemoryGroups = mMemoryGroups->mNext;

	if (mMemoryGroups)
		mMemoryGroups->mPrevious = 0;

	group->mNext = mOldMemoryGroups;
	group->mPrevious = 0;

	if (mOldMemoryGroups)
		mOldMemoryGroups->mPrevious = group;

	mOldMemoryGroups = group;
}

void MemTracker::FreeMemory()
{
	//
	// free memory groups
	//
	MemoryGroup* group = mMemoryGroups;
	while (group)
	{
		MemoryGroup* nextGroup = group->mNext;

		free((void*)group->mName);
		delete group;

		group = nextGroup;
	}
	mMemoryGroups = 0;

	group = mOldMemoryGroups;
	while (group)
	{
		MemoryGroup* nextGroup = group->mNext;

		free((void*)group->mName);
		delete group;

		group = nextGroup;
	}
	mOldMemoryGroups = 0;
/*
	//
	// free all allocations (leaks)
	//
	Allocation* allocation = mAllocations;
	while (allocation)
	{
		Allocation* nextAllocation = allocation->mNext;
		free(allocation);
		allocation = nextAllocation;
	}
	mAllocations = 0;*/
}

void MemTracker::SetOwner(const char* file, unsigned int line, const char* func, bool mutexLock)
{
#ifdef MULTITHREAD
	if (mutexLock)
	{/*
		ofstream file("temp.txt", ios_base::app);
		file << "LOCK - SetOwner" << endl;
		file.close();
*/
		mMutex.Init(1, 1);
		mMutex.Acquire();
	}
#endif

	//if (mBreakOnAllocNumber == mAllocationNumber)
	//{
	//	Assert(false); // check where the mem leak is if you break here
	//}

	mSourceFile = file;
	mSourceLine = line;
	mSourceFunc = func;
}

void* MemTracker::Allocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, unsigned int reportedSize, unsigned int alignment, bool mutexLock)
{
	if (reportedSize == 0)
	{
#ifdef MULTITHREAD
		/*
		ofstream file2("temp.txt", ios_base::app);
		file2 << "RELEASE - Allocator" << endl;
		file2.close();
*/
		mMutex.Release();
#endif
		return 0;
	}

#ifdef MULTITHREAD
	if (mutexLock)
	{/*
		ofstream file("temp.txt", ios_base::app);
		file << "LOCK - Allocator" << endl;
		file.close();
*/
		mMutex.Init(1, 1);
		mMutex.Acquire();
	}
#endif

	if (mBreakOnAllocNumber == mAllocationNumber)
	{
		Assert(false); // check where the mem leak is if you break here
	}

	++mAllocationCount;
	mAllocationSize += reportedSize;

	// allocate enough space for the Allocation, what the app wants, as well as buffer over/under run checks
	Allocation* allocation = 0;
		
	if (allocationType == eAlignedMalloc)
	{
		// we need to calculate enough space to fit the header right up against the real address, and keep alignment
		void* memory = malloc(reportedSize + sizeof(Allocation) + sizeof(unsigned int) + alignment);
		Assert(memory);

		// calculate allocation address
		// we have to put the Allocation structure right up against the returned address,
		// so any padding actually goes before the allocation structure, this makes it quick and easy to 
		// calulate the Allocation address when dealocating memory
		Assert(sizeof(void*) <= sizeof(unsigned long));
		unsigned long alignedMemory = reinterpret_cast<unsigned long>(memory) + sizeof(Allocation);
		unsigned long remainder = alignedMemory % alignment;
		alignedMemory = alignedMemory + (alignment - remainder);

		allocation = reinterpret_cast<Allocation*>(alignedMemory - sizeof(Allocation));
		allocation->mReportedAddress = reinterpret_cast<void*>(alignedMemory); //static_cast<char*>(memory) + sizeof(Allocation);
		allocation->mRealAddress = static_cast<void*>(memory);

		// hitting this assert means there is something wrong with the alignment equation!
		if (alignment)
			Assert(((unsigned long)allocation->mReportedAddress % alignment) == 0);
	}
	else
	{
		void* memory = malloc(reportedSize + sizeof(Allocation) + sizeof(unsigned int));
		Assert(memory);
		allocation = static_cast<Allocation*>(memory);
		allocation->mReportedAddress = static_cast<char*>(memory) + sizeof(Allocation);
		allocation->mRealAddress = static_cast<char*>(memory);
	}

	// set up the buffer over checks

	// set up the linked list
	allocation->mPrevious = 0;
	allocation->mNext = mAllocations;

	if (mAllocations)
		mAllocations->mPrevious = allocation;

	mAllocations = allocation;


	// set up the debug vars

	// strip to back slash
	int len = strlen(file);
	allocation->mFile = file;
	for (int i = len; i > 0; --i)
	{
		if (file[i - 1] == '\\' || file[i - 1] == '/')
		{
			const char* str = &file[i];
			allocation->mFile = str;
			break;
		}
	}
	allocation->mLine = line;
	allocation->mFunc = func;
	allocation->mAllocationType = allocationType;
	allocation->mReportedSize = reportedSize;
	allocation->mType = (MemType)allocationType;
	allocation->mAllocNumber = mAllocationNumber;
	allocation->mMemoryGroup = mMemoryGroups;
	allocation->mPrefixPattern = mPrefixPattern;

	SetOwner("", 0, "", false);
	++mAllocationNumber;

#ifdef MULTITHREAD
	/*
	ofstream file2("temp.txt", ios_base::app);
	file2 << "RELEASE - Allocator" << endl;
	file2.close();
*/
	mMutex.Release();
#endif
	return allocation->mReportedAddress;
}

void* MemTracker::Reallocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, unsigned int reportedSize, void* reportedAddress)
{
	return realloc(reportedAddress, reportedSize);
}

void MemTracker::Deallocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, void* reportedAddress, bool mutexLock)
{
	if (reportedAddress == 0)
	{
#ifdef MULTITHREAD
		/*
		ofstream file2("temp.txt", ios_base::app);
		file2 << "RELEASE - Allocator" << endl;
		file2.close();
*/
		mMutex.Release();
#endif
		return;
	}

#ifdef MULTITHREAD
	if (mutexLock)
	{/*
		ofstream file2("temp.txt", ios_base::app);
		file2 << "LOCK - Deallocator" << endl;
		file2.close();
*/
		mMutex.Init(1, 1);
		mMutex.Acquire();
	}
#endif

	Allocation* allocation = static_cast<Allocation*>(CalulateAllocationAddress(reportedAddress));

	// if we hit this assert we have a memory underrun, or something corrupting this memory
	Assert(allocation->mPrefixPattern == mPrefixPattern);

	--mAllocationCount;
	mAllocationSize -= allocation->mReportedSize;

	// remove from the doubly linked list
	if (allocation->mPrevious == 0)
	{
		mAllocations = allocation->mNext;
		if (mAllocations)
			mAllocations->mPrevious = 0;
	}
	else
	{
		Allocation* prev = allocation->mPrevious;
		Allocation* next = allocation->mNext;

		prev->mNext = next;
		if (next)
			next->mPrevious = prev;
	}

	SetOwner("", 0, "", false);

	//if (allocationType == eAlignedFree)
	//	_aligned_free(allocation->mRealAddress);
	//else
		free(allocation->mRealAddress);

#ifdef MULTITHREAD
	/*
	ofstream file22("temp.txt", ios_base::app);
	file22 << "RELEASE - Deallocator" << endl;
	file22.close();
*/
	mMutex.Release();
#endif
}

void* MemTracker::CalulateAllocationAddress(void* reportedAddress)
{
	return static_cast<char*>(reportedAddress) - sizeof(Allocation);
}

unsigned int MemTracker::GetAllocationSize()
{
	return mAllocationSize;
}

unsigned int MemTracker::GetAllocationCount()
{
	return mAllocationCount;
}

void MemTracker::Log()
{
	if (!mEnableLog)
		return;

	ofstream file("memory.html");

	file << "<html><body>" << endl;

	file << "<b>";
	file << "Number of leaks   " << mAllocationCount << "<br>";
	file << "Memory leaked     " << mAllocationSize << " bytes (" << float(mAllocationSize) / (1024.f * 1024.f) << " MB)" << "<br>" << "<br>";
	file << "</b>";

	file << "<table border=1 bordercolor=gray cellpadding=5 cellspacing=0>";

	file << "<tr><td><b>Alloc No</b></td><td><b>Size (bytes)</b></td><td><b>Type</b></td><td><b>Line</b></td><td><b>File</b></td><td><b>Function</b></td><td><b>Group</b></td></tr>";

	Allocation* al = mAllocations;
	Allocation* alSorted = 0;
	Allocation* alSortedIt = 0;
	Allocation* alSortedPrevIt = 0;

	// sort by size
	while (al)
	{
		Allocation* alInsert = al;

		al = al->mNext;

		alInsert->mNext = 0;
		alInsert->mPrevious = 0;

		if (!alSorted)
		{
			alSorted = alInsert;
		}
		else
		{
			alSortedIt = alSorted;
			while (alSortedIt)
			{
				Allocation* alPrev = alSortedIt->mPrevious;

				// insert before current node
				if (alInsert->mReportedSize > alSortedIt->mReportedSize)
				{
					Allocation* alNode = al;

					// insert in to sorted list
					alInsert->mNext = alSortedIt;
					alInsert->mPrevious = alSortedIt->mPrevious;

					alSortedIt->mPrevious = alInsert;

					if (alPrev)
						alPrev->mNext = alInsert;

					// new head of the list?
					if (alSorted == alSortedIt)
					{
						alSorted = alInsert;
					}

					break;
				}

				if (alSortedIt->mNext == 0)
				{
					alSortedIt->mNext = alInsert;
					alInsert->mPrevious =  alSortedIt;
					break;
				}

				alSortedIt = alSortedIt->mNext;
			}
		}
	}

	al = alSorted;

	while (al)
	{
		file << "<tr><td>" << al->mAllocNumber << "</td><td>" << al->mReportedSize << "</td><td>" << mAllocationTypes[al->mType] <<
			 "</td><td>" << al->mLine << "</td><td>" << (strlen(al->mFile) <= 0 ? "&nbsp;" : al->mFile) << "</td><td>" << (strlen(al->mFunc) <= 0 ? "&nbsp;" : al->mFunc) << "</td><td>" <<
			 (al->mMemoryGroup ? al->mMemoryGroup->mName : "&nbsp;") << "</td></tr>";

		al = al->mNext;
	}

	file << "</table>";
	file << "</body></html>" << endl;

	file.close();
}


void* operator new(size_t reportedSize)
{
	return MemTracker::Allocator(MemTracker::mSourceFile, MemTracker::mSourceLine, MemTracker::mSourceFunc, MemTracker::eNew, reportedSize);
}

void* operator new[](size_t reportedSize)
{
	return MemTracker::Allocator(MemTracker::mSourceFile, MemTracker::mSourceLine, MemTracker::mSourceFunc, MemTracker::eNewArray, reportedSize);
}

void operator delete(void *reportedAddress)
{
#if !defined(WIN32) && !defined(UNIX)
    try
    {
#endif
        MemTracker::Deallocator(MemTracker::mSourceFile, MemTracker::mSourceLine, MemTracker::mSourceFunc, MemTracker::eDelete, reportedAddress);
#if !defined(WIN32) && !defined(UNIX)
    }
    catch (...)
    {

    }
#endif
}

void operator delete[](void *reportedAddress)
{
	MemTracker::Deallocator(MemTracker::mSourceFile, MemTracker::mSourceLine, MemTracker::mSourceFunc, MemTracker::eDeleteArray, reportedAddress);
}

#ifdef _WIN32

void* operator new(size_t reportedSize, const char *sourceFile, int sourceLine)
{
	return MemTracker::Allocator(MemTracker::mSourceFile, MemTracker::mSourceLine, "???", MemTracker::eNew, reportedSize);
}

void* operator new[](size_t reportedSize, const char *sourceFile, int sourceLine)
{
	return MemTracker::Allocator(MemTracker::mSourceFile, MemTracker::mSourceLine, "???", MemTracker::eNewArray, reportedSize);
}

#endif

class StaticLog
{
public:
	StaticLog() { }
	~StaticLog()
	{
		MemTracker::Log();
		//MemTracker::FreeMemory();
	}
};
static StaticLog sStaticLog;
