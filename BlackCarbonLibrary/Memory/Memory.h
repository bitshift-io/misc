#ifndef _MEMORY_H_
#define _MEMORY_H_

//#include <stdio.h>
//#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <math.h>

#ifdef MULTITHREAD
	#include "Thread/Semaphore.h"
#endif

#ifdef	WIN32
	#ifdef	_DEBUG
	#define	Assert(x) if ((x) == false) __asm { int 3 }
	#else
	#define	Assert(x) {}
	#endif
#else	// Linux uses assert, which we can use safely, since it doesn't bring up a dialog within the program.
	#include <assert.h>
	#define	Assert assert
#endif

//
// NOTE: If we make this a singleton, we should be able to override it with our memory pools
//	reportedAddress - the actual address of the data,
//	realAddress - includes padding to check for buffer over/under runs
//
class MemTracker
{
public:

	enum MemType
	{
		eUnknown,
		eNew,
		eNewArray,
		eMalloc,
		eCalloc,
		eRealloc,
		eDelete,
		eDeleteArray,
		eFree,
		eAlignedMalloc,
		eAlignedFree,
	};

	//
	// you can put memory in to groups, to help track leaks
	//
	struct MemoryGroup
	{
		const char*			mName;
		MemoryGroup*		mNext;
		MemoryGroup*		mPrevious;
	};

	//
	// track all memory
	//
	struct Allocation
	{
		const char*		mFile;
		unsigned int	mLine;
		const char*		mFunc;
		unsigned int	mReportedSize;
		unsigned int	mAllocationType;
		void*			mReportedAddress;
		void*			mRealAddress;
		MemType			mType;
		Allocation*		mNext;
		Allocation*		mPrevious;
		unsigned int	mAllocNumber;
		MemoryGroup*	mMemoryGroup;
		unsigned int	mPrefixPattern;
	};


	static void PushMemoryGroup(const char* name);
	static void PopMemoryGroup();

	static void SetOwner(const char* file, unsigned int line, const char* func, bool mutexLock = true);
	static void* Allocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, unsigned int reportedSize, unsigned int alignment = 0, bool mutexLock = false);
	static void* Reallocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, unsigned int reportedSize, void* reportedAddress);
	static void Deallocator(const char* file, unsigned int line, const char* func, unsigned int allocationType, void* reportedAddress, bool mutexLock = false);
	static void* CalulateAllocationAddress(void* reportedAddress);
	static unsigned int GetAllocationSize();
	static unsigned int GetAllocationCount();
	static void Log();
	static void FreeMemory();

	static void SetBreakOnAllocNumber(unsigned int allocNumber)			{ mBreakOnAllocNumber = allocNumber; }

//protected:
	static unsigned int mAllocationCount;
	static unsigned int mAllocationSize;
	static const char*	mAllocationTypes[];

	static unsigned int	mPrefixPattern;
	static unsigned int	mPostfixPattern;
	static unsigned int	mUnusedPattern;
	static unsigned int	mReleasedPattern;

	static const char*	mSourceFile;
	static unsigned int mSourceLine;
	static const char*	mSourceFunc;

	static MemoryGroup* mMemoryGroups;	// head of duble linked stack
	static MemoryGroup* mOldMemoryGroups; // memory groups need to be kept around till app exists

	static Allocation*	mAllocations; // head of the doubly linked list
	static unsigned int	mAllocationNumber; // use this to break!
	static unsigned int	mBreakOnAllocNumber;// use this to break!

	static bool			mEnableLog;

#ifdef MULTITHREAD
	static Mutex		mMutex;
#endif
};

void* operator new(size_t reportedSize);
void* operator new[](size_t reportedSize);
void operator delete(void *reportedAddress);
void operator delete[](void *reportedAddress);

#ifdef _WIN32

void* operator new(size_t reportedSize, const char *sourceFile, int sourceLine);
void* operator new[](size_t reportedSize, const char *sourceFile, int sourceLine);

#endif

// TODO: why does this not work on mac?
#if defined(WIN32) || defined(UNIX)
#define	new									(MemTracker::SetOwner(__FILE__,__LINE__,__FUNCTION__, true), false) ? 0 : new
#define	delete								(MemTracker::SetOwner(__FILE__,__LINE__,__FUNCTION__, true), false) ? 0 : delete
#endif

#define	malloc(sz)							MemTracker::Allocator(__FILE__,__LINE__,__FUNCTION__, MemTracker::eMalloc, sz, 0, true)
#define	calloc(sz)							MemTracker::Allocator(__FILE__,__LINE__,__FUNCTION__, MemTracker::eCalloc, sz, 0, true)
#define	realloc(ptr,sz)						MemTracker::Reallocator(__FILE__,__LINE__,__FUNCTION__, MemTracker::eRealloc, sz, ptr)
#define	free(ptr)							MemTracker::Deallocator(__FILE__,__LINE__,__FUNCTION__, MemTracker::eFree, ptr)
#define	_aligned_malloc(sz, alignment)		MemTracker::Allocator(__FILE__,__LINE__,__FUNCTION__, MemTracker::eAlignedMalloc, sz, alignment, true)
#define	_aligned_free(ptr)					MemTracker::Deallocator(__FILE__,__LINE__,__FUNCTION__, MemTracker::eAlignedFree, ptr)

#endif
