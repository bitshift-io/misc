#ifndef _REFLECTSCRIPT_H_
#define _REFLECTSCRIPT_H_

#include "File/ScriptFile.h"

//
// might be useful one day:
// c++ typeid
// http://publib.boulder.ibm.com/infocenter/iadthelp/v6r0/index.jsp?topic=/com.ibm.etools.iseries.pgmgd.doc/cpprog612.htm
//

class ScriptClass;
class ClassDesc;
class VariableDesc;

class ReflectScript : public ScriptFile
{
public:	

	typedef ScriptFile Parent;
	typedef void(*CreateCallback)(void* instance, const ScriptClass* scriptClass, const ClassDesc* classDesc);

	bool				Open(const char* ph, list<void*>* entityList, CreateCallback callback = 0);
	void*				CreateInstance(const ScriptClass* scriptClass, const ClassDesc** ppClassDesc = 0);
	bool				PopulateInstance(void* instance, const ScriptClass* scriptClass, const ClassDesc* classDesc);

protected:

	bool				PopulateValue(void* var, VariableDesc* varDesc, const ScriptVariable::VariableValue* value, int index);

	bool				HasProperty(const string& property, const ScriptClass& scriptClass);

	CreateCallback		mCreateCallback;
};

#endif