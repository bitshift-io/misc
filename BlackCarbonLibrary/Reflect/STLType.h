#ifndef _STL_H_
#define _STL_H_

#include "Type.h"
#include <vector>
#include <list>

using namespace std;


#define VAR_STL(x) \
	*(new VariableDesc(#x, (char*)&x-(char*)this, sizeof(x), 0, TypeOf(x)))

// variable is not a pointer, but the template arg of the STL is a pointer eg. vector<SomePtr*>
#define VAR_PTR_STL(x) \
    *(new VariableDesc(#x, (char*)&x-(char*)this, sizeof(x), 0, TypeOfPtr(x)))




enum STLType
{
	STLT_Vector,
	STLT_List,
};

class STLTypeDesc : public TypeDesc
{
public:

	STLTypeDesc(STLType _stlType, TypeDesc* _templateType) :
	  templateType(_templateType),
	  stlType(_stlType)
	{
	}

	void					GetTypeName(char* buf);

	virtual Type			GetType() const																{ return T_STL; }

	STLType					stlType;
	TypeDesc*				templateType;
};


template <class T>
class STLTypeDescTemplate : public STLTypeDesc
{
public:

	STLTypeDescTemplate(STLType _stlType, TypeDesc* _templateType) :
		STLTypeDesc(_stlType, _templateType)
	{
	}

	virtual bool LoadString(void* variable, const char* strValue, int index = 0) const;
	virtual bool LoadBinary(void* variable, void* data, int index = 0) const;
};


template <class T>
bool STLTypeDescTemplate<T>::LoadString(void* variable, const char* strValue, int index) const
{
	switch (stlType)
	{
	case STLT_Vector:
		{
			if (templateType->GetType() == T_Pointer)
			{
				PointerTypeDesc* ptrTypeDesc = static_cast<PointerTypeDesc*>(templateType);
				void* ptr = ptrTypeDesc->ptrType->Create();
				ptrTypeDesc->ptrType->LoadString(ptr, strValue, index);

				vector<T>* stl = static_cast<vector<T>* >(variable);
				stl->push_back((T)ptr);
			}
			else
			{/*
				void* ptr = templateType->Create();
				templateType->LoadString(ptr, strValue, index);

				vector<T>* stl = static_cast<vector<T>* >(variable);
				stl->push_back((T)*ptr);*/
				Log::Error("Note yet implemented\n");
			}
		}
		break;

	case STLT_List:
		{
			if (templateType->GetType() == T_Pointer)
			{
				PointerTypeDesc* ptrTypeDesc = static_cast<PointerTypeDesc*>(templateType);
				void* ptr = ptrTypeDesc->ptrType->Create();
				ptrTypeDesc->ptrType->LoadString(ptr, strValue, index);

				list<T>* stl = static_cast<list<T>* >(variable);
				stl->push_back((T)ptr);
			}
			else
			{/*
				void* ptr = templateType->Create();
				templateType->LoadString(ptr, strValue, index);

				list<T>* stl = static_cast<list<T>* >(variable);
				stl->push_back((T)*ptr);*/
				Log::Error("Note yet implemented\n");
			}
		}
		break;

	default:
		Log::Error("[STLTypeDesc<T>::LoadString] Type not supported");
		return false;
	}

	return true;
}

template <class T>
bool STLTypeDescTemplate<T>::LoadBinary(void* variable, void* data, int index = 0) const
{
	switch (stlType)
	{
	case STLT_Vector:
		{
			vector<T>* stl = static_cast<vector<T>* >(variable);
			stl->push_back((T)data);			
		}
		break;

	case STLT_List:
		{
			list<T>* stl = static_cast<list<T>* >(variable);
			stl->push_back((T)data);
		}
		break;

	default:
		Log::Error("[STLTypeDesc<T>::LoadString] Type not supported");
		return false;
	}

	return true;
}




template<class __T>
inline TypeDesc* TypeOf(vector<__T>&) 
{
	// bit dodgey, declare a pointer so we dont have to create an instance
	__T* t = 0;
	TypeDesc* type = TypeOf(*t);

	static STLTypeDescTemplate<__T> stlType(STLT_Vector, type);
	return &stlType;
}

template<class __T>
inline TypeDesc* TypeOfPtr(vector<__T>&) 
{
	__T t = 0;
	TypeDesc* type = TypeOfPtr(&t);

	static STLTypeDescTemplate<__T> stlType(STLT_Vector, type);
	return &stlType;
}



#endif