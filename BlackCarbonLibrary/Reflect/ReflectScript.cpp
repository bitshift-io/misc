#include "ReflectScript.h"
#include "ReflectSystem.h"
#include "Class.h"
#include "Type.h"
#include "STLType.h"
#include "File/Log.h"

bool ReflectScript::Open(const char* ph, list<void*>* entityList, CreateCallback callback)
{
	mCreateCallback = callback;

	Parent::Open(ph);
	if (!Valid())
		return false;

	if (!entityList)
		return true;

	// create instances of the classes
	ScriptFile::ClassIterator classIt;
	for (classIt = ClassBegin(); classIt != ClassEnd(); ++classIt)
	{
		if (HasProperty("abstract", *classIt))
			continue;

		void* classInstance = CreateInstance(&(*classIt));
		if (classInstance)
			entityList->push_back(classInstance);
	}

	return true;
}

bool ReflectScript::HasProperty(const string& property, const ScriptClass& scriptClass)
{
	ScriptClass::PropertyIterator propIt;
	for (propIt = scriptClass.PropertyBegin(); propIt != scriptClass.PropertyEnd(); ++propIt)
	{
		if (strcmpi(propIt->c_str(), "abstract") == 0)
		{
			return true;
		}
	}

	return false;
}

void* ReflectScript::CreateInstance(const ScriptClass* scriptClass, const ClassDesc** ppClassDesc)
{
	const ClassDesc* classDesc = 0;
	const ScriptClass* scriptParent = GetScriptClass(scriptClass->GetExtendsName());
	if (scriptParent)
	{
		void* instance = CreateInstance(scriptParent, ppClassDesc ? ppClassDesc : &classDesc);
		PopulateInstance(instance, scriptClass, ppClassDesc ? *ppClassDesc : classDesc);
		return instance;
	}

	classDesc = gReflectSystem.GetClassDescByName(scriptClass->GetExtendsName().c_str());
	if (!classDesc)
	{
		Log::Warning("Failed to find reflectance information for '%s'. Cant create instance\n", scriptClass->GetExtendsName().length() ? scriptClass->GetExtendsName().c_str() : scriptClass->GetName().c_str());
		return 0;
	}

	if (ppClassDesc)
		*ppClassDesc = classDesc;

	void* instance = classDesc->Create();
	if (mCreateCallback)
		mCreateCallback(instance, scriptClass, classDesc);

	PopulateInstance(instance, scriptClass, classDesc);
	return instance;
}

bool ReflectScript::PopulateInstance(void* instance, const ScriptClass* scriptClass, const ClassDesc* classDesc)
{
	// iterate through all variables of this class
	ScriptClass::VariableIterator varIt;
	for (varIt = scriptClass->VariableBegin(); varIt != scriptClass->VariableEnd(); ++varIt)
	{
		VariableDesc* varDesc = classDesc->GetVariableDesc(varIt->GetName().c_str());		
		if (!varDesc)
		{
			Log::Print("Couldn't find variable relating to '%s' for class [%s : %s]\n", varIt->GetName().c_str(), scriptClass->GetName().c_str(), scriptClass->GetExtendsName().c_str());
			continue;
		}

		TypeDesc* typeDesc = varDesc->type;

		PointerTypeDesc* ptrTypeDesc = (typeDesc->GetType() == T_Pointer) ? static_cast<PointerTypeDesc*>(typeDesc) : 0;
		if (typeDesc->GetType() == T_STL)
		{
			STLTypeDesc* stlTypeDesc = static_cast<STLTypeDesc*>(typeDesc);
			ptrTypeDesc = (stlTypeDesc->templateType->GetType() == T_Pointer) ? static_cast<PointerTypeDesc*>(stlTypeDesc->templateType) : 0;
		}

		// iterate through all values
		if (!ptrTypeDesc && varIt->ValueSize() > 1)
		{
			Log::Print("Data wants to load an array of data, while only one can be loaded: variable %s, class %s\n", varDesc->name, scriptClass->GetName().c_str());
			continue;
		}

		// iterate through all values assigned to this variable
		int i = 0; 
		ScriptVariable::ValueConstIterator valueIt;		
		for (valueIt = varIt->ValueConstBegin(); valueIt != varIt->ValueConstEnd(); ++valueIt, ++i)
		{
			PopulateValue((char*)instance + varDesc->offset, varDesc, &(*valueIt), i);
			int nothing = 0;
		}
	}

	return true;
}

bool ReflectScript::PopulateValue(void* var, VariableDesc* varDesc, const ScriptVariable::VariableValue* value, int index)
{		
	//
	// FMNOTE: need some sort of TypeDesc->Load to load from a string/file
	//
	TypeDesc* typeDesc = varDesc->type;

	PointerTypeDesc* ptrTypeDesc = (typeDesc->GetType() == T_Pointer) ? static_cast<PointerTypeDesc*>(typeDesc) : 0;
	if (typeDesc->GetType() == T_STL)
	{
		STLTypeDesc* stlTypeDesc = static_cast<STLTypeDesc*>(typeDesc);
		ptrTypeDesc = (stlTypeDesc->templateType->GetType() == T_Pointer) ? static_cast<PointerTypeDesc*>(stlTypeDesc->templateType) : 0;
	}

	switch (value->mType)
	{
	case ScriptVariable::VVT_String:
		{
			const char* stringValue = value->mString.c_str();
			if (ptrTypeDesc && ptrTypeDesc->ptrType->GetType() == T_BasicType)
			{
				BasicTypeDesc* basicTypeDesc = static_cast<BasicTypeDesc*>(ptrTypeDesc->ptrType);
				switch (basicTypeDesc->basicType)
				{
				case BasicTypeDesc::BT_Char:
					{
						char** charVar = ((char**)var);
						*charVar = strdup(stringValue);
					}
					break;
				}
			}
			else if (!typeDesc->LoadString(var, stringValue, index))
			{
				Log::Error("[ReflectScript::PopulateValue] Error trying to load '%s' in to variable '%s'", stringValue, varDesc->name);
			}
/*
			const char* format = 0;
			switch (typeDesc->Get)
			{
			case T_UChar:
			case T_UShort:
			case T_UInt:
				format = "%u";
				break;

			case T_Short:
			case T_Char:
			case T_SChar:
			case T_Int:
				format = "%d";
				break;

			case T_Long:
			case T_ULong:
				format = "%l";
				break;

			case T_Float:
				format = "%f";
				break;

			case T_Double:
				Log::Error("Loading T_Double not yet supported");
				break;

			case T_Bool:
				{
					bool bVal = false;
					if (strcmpi(stringValue, "true") == 0 || strcmpi(stringValue, "1") == 0)
						bVal = true;

					*((char*)var) = bVal;
					return true;
				}
				break;
			}
			
			if (format)
				sscanf(stringValue, format, var);

			switch (typeDesc->type)
			{
			case T_Ptr:
				{
					switch (ptrTypeDesc->ptrType->type)
					{
					case T_Char:
						{
							char* charVar = ((char*)var);
							charVar = strdup(stringValue);
						}
						break;
					}
				}
				break;
			}*/
		}
		break;

	case ScriptVariable::VVT_Instance:
	case ScriptVariable::VVT_Pointer:
		{
			const ScriptClass* valueClass = (value->mType == ScriptVariable::VVT_Instance) ? &value->mScriptClass : value->mScriptClassPtr;
			if (!valueClass)
				break;

			if (typeDesc->GetType() == T_STL)
			{
				if (ptrTypeDesc)
				{
					if (ptrTypeDesc->ptrType->GetType() == T_Class)
					{
						//ClassDesc* valueClassDesc = static_cast<ClassDesc*>(ptrTypeDesc->ptrType);
						void* instance = CreateInstance(valueClass);

						STLTypeDesc* stlTypeDesc = static_cast<STLTypeDesc*>(typeDesc);
						stlTypeDesc->LoadBinary(var, instance);
					}
					else
					{
						Log::Error(false, "Error in [ReflectScript::PopulateInstance]");
					}
				}
			}
			else if (ptrTypeDesc)
			{
				if (ptrTypeDesc->ptrType->GetType() == T_Class)
				{
					//ClassDesc* valueClassDesc = static_cast<ClassDesc*>(ptrTypeDesc->ptrType);
					void* instance = CreateInstance(valueClass);

					// THIS IS 64BIT SAFE
					int** ptr = (int**)((char*)var);
					*ptr = (int*)instance;
				}
				else
				{
					Log::Error(false, "Error in [ReflectScript::PopulateInstance]");
				}
			}
			else if (typeDesc->GetType() == T_Class)//if (ptrTypeDesc->type == T_Struct)
			{
				PopulateInstance(var, valueClass, static_cast<ClassDesc*>(typeDesc));
			}
			else
			{
				Log::Error(false, "Error in [ReflectScript::PopulateInstance]");
			}
		}
		break;
	}

	return true;
/*
	if (valueClass)
	{
		if (isPointer)
		{
			bool isNextPointer = false;
			const ClassMetadata* metaInfo = 0;// = gMetadataMgr.GetClassMetadata(valueClass->GetName().c_str(), &isNextPointer);

			/*
			if (!metaInfo)
				metaInfo = gMetadataMgr.GetClassMetadata(valueClass->GetExtendsName().c_str(), &isNextPointer);

			if (!metaInfo)
			{
				Log::Print("No metadata for: variable %s, class %s\n", variableInfo->name, scriptClass->GetName().c_str());
				continue;
			}* /

			
	//if (strcmpi(variableInfo->GetName().c_str(), "mywnd") == 0)
	//			int ntihng = 0;

			void* nextInstance = CreateInstance(valueClass, &metaInfo);
			if (metaParser)
			{
				metaParser->ReadInstance(*variableInfo, (char*)instance + variableInfo->offset, nextInstance, this);
			}
			else
			{
				// THIS IS 64BIT SAFE
				int** ptr = (int**)((char*)instance + variableInfo->offset);
				*ptr = (int*)nextInstance;
			}
		}
		else
		{
			PopulateInstance(valueClass, (char*)instance + variableInfo->offset, metaInfo);
		}
	}*/

}