#include "Variable.h"

VariableDesc::VariableDesc(char const* _name, long int _offset, unsigned int _size, unsigned int _flags, TypeDesc* _type) :
	name(_name),
	offset(_offset),
	size(_size),
	flags(_flags),
	type(_type),
	next(0),
	chain(&next)
{ 
}

VariableDesc& VariableDesc::operator,(VariableDesc& field) 
{
	*chain = &field;
	chain = &field.next;
	return *this;
}