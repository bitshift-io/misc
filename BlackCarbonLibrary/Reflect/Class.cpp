#include "Class.h"
#include "ReflectSystem.h"

static int sCurrentClassTypeId = 0;

ClassDesc::ClassDesc(char const* _name, ClassDesc* _parent, unsigned int _size, VariableDescFn varDescFn, MethodDescFn methodDescFn, CreateFn createFn, DestroyFn destroyFn, int _flags) :
	name(_name),
	flags(_flags),
	size(_size),
	create(createFn),
	destroy(destroyFn),
	parent(_parent)
{
    gReflectSystem.Register(this);

	uniqueId = sCurrentClassTypeId;
	sCurrentClassTypeId++;

    variable = (*varDescFn)();
    method = (*methodDescFn)();

}

VariableDesc* ClassDesc::GetVariableDesc(const char* name) const
{
	VariableDesc* v = variable;
	while (v)
	{
		if (strcmpi(name, v->name) == 0)
			return v;

		v = v->next;
	}

	if (parent)
		return parent->GetVariableDesc(name);

	return 0;
}

MethodDesc*	ClassDesc::GetMethodDesc(const char* name) const
{
	MethodDesc* m = method;
	while (m)
	{
		if (strcmpi(name, m->name) == 0)
			return m;

		m = m->next;
	}

	if (parent)
		return parent->GetMethodDesc(name);

	return 0;
}

ClassDesc* ClassDesc::InheritsFrom(ClassDesc* potentialParent) const
{
	if (this == potentialParent)
		return potentialParent;

	if (parent)
		return parent->InheritsFrom(potentialParent);

	return 0;
}

void* ClassDesc::Create() const
{
	if (create)
		return create();

	return 0;
}

void ClassDesc::Destroy(void** inst) const
{
	destroy(inst);
}

void ClassDesc::GetTypeName(char* buf)
{
    strcpy(buf, name);
}