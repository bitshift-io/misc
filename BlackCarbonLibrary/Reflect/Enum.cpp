#include "Enum.h"
#include "Template/String.h"

EnumTypeDesc::EnumTypeDesc(char const* _name, EnumEntry* _enumEntry)
{
	name = _name;
	enumEntry = _enumEntry;
}

void EnumTypeDesc::GetTypeName(char* buf)
{
	strcpy(buf, name);
}

bool EnumTypeDesc::LoadString(void* variable, const char* strValue, int index) const
{
	vector<string>::iterator it;
	vector<string> tokens = String::Tokenize(strValue, "|", true);

	bool bFound = false;
	int result = 0;
	for (it = tokens.begin(); it != tokens.end(); ++it)
	{
		int i = 0;
		while (enumEntry[i].name)
		{
			if (stricmp(enumEntry[i].name, it->c_str()) == 0)
			{
				result |= enumEntry[i].value;
				bFound = true;
				break;
			}

			++i;
		}
	}

	*((int*)variable) = result;
	return bFound;
}

bool EnumTypeDesc::LoadBinary(void* variable, void* data, int index) const
{
	*((int*)variable) = *((int*)data);
	return true;
}