#include "Type.h"
#include "Class.h"
#include <stdio.h>
#include <string.h>

#ifdef LoadString
#undef LoadString
#endif

void TypeDesc::GetTypeName(char* buf)										
{ 
	strcpy(buf, "???"); 
}

void BasicTypeDesc::GetTypeName(char* buf)
{
    char* p = "???";

    switch (basicType) 
	{
    case BT_Void:
		p = "void";
		break;
    case BT_Char:
		p = "char";
		break;
    case BT_UChar:
		p = "unsigned char";
		break;
    case BT_SChar:
		p = "signed char";
		break;
    case BT_Short:
		p = "short";
		break;
    case BT_UShort:
		p = "unsigned short";
		break;
    case BT_Int:
		p = "int";
		break;
    case BT_UInt:
		p = "unsigned int";
		break;
    case BT_Long:
		p = "long";
		break;
    case BT_ULong:
		p = "unsigned long";
		break;
    case BT_Float:
		p = "float";
		break;
    case BT_Double:
		p = "double";
		break;
    case BT_Bool:
		p = "bool";
		break;
    }

    strcpy(buf, p);
}



void PointerTypeDesc::GetTypeName(char* buf)
{
    ptrType->GetTypeName(buf);
    buf += strlen(buf);
    *buf++ = '*';
    *buf = '\0';
}

bool PointerTypeDesc::LoadString(void* variable, const char* strValue, int index) const
{
	/*
	void* instance = ptrType->Create();
	ptrType->LoadString(instance, strValue, index);

	int** ptr = (int**)((char*)variable);
	*ptr = (int*)instance;
	return true;*/
	return false;
}

bool PointerTypeDesc::LoadBinary(void* variable, void* data, int index) const
{
	// this isnt really safe!
	if (ptrType->GetType() == T_BasicType)
	{
		char** p = ((char**)variable);
		char** d = ((char**)data);
		*p = strdup(((char*)*d));
	}
	/*
	void* instance = ptrType->Create();
	ptrType->LoadBinary(instance, data, index);

	int** ptr = (int**)((char*)variable);
	*ptr = (int*)instance;
	return true;*/
	return false;
}



void ArrayTypeDesc::GetTypeName(char* buf)
{
    elemType->GetTypeName(buf);
    buf += strlen(buf);
    *buf++ = '[';
    if (numElem != 0) 
	{ 
		buf += sprintf(buf, "%d", numElem);
    }
    *buf++ = ']';
    *buf = '\0';
}

/*
void DerivedTypeDesc::GetTypeName(char* buf) 
{ 
    baseClass->GetTypeName(buf);
}
*/
void MethodTypeDesc::GetTypeName(char* buf)
{
    returnType->GetTypeName(buf);
    buf += strlen(buf);
    *buf++  = '(';
    methodClass->GetTypeName(buf);
    buf += strlen(buf);
    *buf++  = ':';
    *buf++  = ':';
    *buf++  = '*';
    *buf++  = ')';
    *buf++  = '(';
    for (int i = 0; i < numParam; i++) 
	{ 
		if (i != 0) 
		{ 
			*buf++ = ',';
		}
		paramTypes[i]->GetTypeName(buf);
    }
    *buf++ = ')';
    *buf = '\0';
}


void MethodTypeDesc::GetMethodDeclaration(char* buf, char const* name)
{
    returnType->GetTypeName(buf);
    buf += strlen(buf);
    *buf++ = ' ';
    *buf++ = '\t';
    methodClass->GetTypeName(buf); 
    buf += strlen(buf);
    buf += sprintf(buf, "::%s(", name);
    for (int i = 0; i < numParam; i++) 
	{ 
		if (i != 0) 
		{ 
			*buf++ = ',';
		}
		paramTypes[i]->GetTypeName(buf);
		buf += strlen(buf);
    }
    *buf++ = ')';
    *buf++ = ';';
    *buf = '\0';
}
    

