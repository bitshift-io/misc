#ifndef _WIN32INPUT_H_
#define _WIN32INPUT_H_

#include "Input.h"
#include <string>
#include <vector>
#include <dinput.h>
#include <XInput.h>

using namespace std;

class Window;
class Win32Window;

class Win32JoystickEffect : public JoystickEffect
{
public:

	virtual void	CreateEffect(LPDIRECTINPUTDEVICE8 pDevice);
	void			Update();

	DIPERIODIC diPeriodic;      // type-specific parameters
	DIENVELOPE diEnvelope;      // envelope
	DIEFFECT   diEffect;        // general parameters
};

class Win32Joystick : public Joystick
{
public:

	Win32Joystick() :
		deadZone(0.15f),
		sensitivity(0.1f),
		xboxIdx(-1)
	{
	}

	virtual void	ApplyForce(const Vector4& dir, float force);
	virtual void	InsertEffect(JoystickEffect* _effect);
	virtual int		GetFlags();
	void			Update();

	int						xboxIdx;
	LPDIRECTINPUTDEVICE8	pDevice;
	DIDEVCAPS				caps;
	DIJOYSTATE2				state;
	bool					buttonLock[128];
	float					deadZone;
	float					sensitivity;

	vector<Win32JoystickEffect*>	effect;
};

class Win32Input
{
	friend class Window;

public:

	Win32Input();

	bool			Init(Window* window, bool background = false);
	void			Deinit();

	void			Update();

	//
	// 0 is up, 1 is down
	// 
	//
	bool			GetStateBool(InputControl control);
	float			GetState(InputControl control);
	bool			WasPressed(InputControl control);
/*
	//
	// Determines if they key is currently down
	//
	bool			KeyDown(int key);

	//
	// determines if the key was pressed
	//
	bool			KeyPressed(int key);

	//
	// give this a string like "a" or "shift"
	// and this function will return the
	// dx key code eg. DIK_A or DIK_SHIFT
	//
	int				GetKeyCode(string str);

	//
	// Return the state of the mouse
	//
	void			GetMouseState( int& x, int& y );

	//
	// Determines if they mouse button is currently down
	//
	bool			MouseDown(int button);

	//
	// determines if the mouse button was pressed
	//
	bool			MousePressed(int button);
*/
	// use these for alt-tab
	void			AcquireMouse();
	void			UnaquireMouse();

	Joystick*		GetJoystick(int  idx);
	int				GetJoystickCount()											{ return (int)mJoystick.size(); }

	void			SetMouseSensitivity(float sensitivity)						{ mMouseSensitivity = sensitivity; }
	/*
	bool			JoystickButtonDown(int joystick, int button);
	bool			JoystickButtonPressed(int joystick, int button);
	void			JoystickAxisState(int joystick, int axisIndex, float* xAxis, float* yAxis, float* zAxis = 0);
	*/
protected:

	void					NotifyActive(bool active);

	static BOOL CALLBACK	EnumJoystickCallback(const DIDEVICEINSTANCE* pdidInstance, VOID* pContext);
	void					EnumXBoxController(int index);

	LPDIRECTINPUTDEVICE8	mMouseDevice;
	LPDIRECTINPUTDEVICE8	mKeyboardDevice;
	LPDIRECTINPUT8			mDI;

	vector<Win32Joystick>	mJoystick;

	float					mMouseSensitivity;
/*
	// keyboard
	char					mKey[256];
	bool					mKeyLock[256];

	// mouse
	DIMOUSESTATE2			mMouse;
	bool					mMouseButtonLock[8];
*/

	struct InputControlState
	{
		float	state;
		bool	pressed;
	}						mState[InputControl_Max];

	bool					mBackground;
	bool					mMouseWarp;
	bool					mMouseVisibility;
	bool					mAltTabStyleMouse;

	Win32Window*			mWindow; // what window is giving us input
};

#endif
