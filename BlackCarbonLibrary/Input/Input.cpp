#include "Input.h"

#ifdef WIN32
	#include "Win32Input.h"
#else
	#include "LinuxInput.h"
#endif

Input::Input() :
	mInput(0)
{

}

bool Input::Init(Window* window, bool background)
{
#ifdef WIN32
	mInput = new Win32Input;
#else
	mInput = new LinuxInput;
#endif

	return mInput->Init(window, background);
}

void Input::Deinit()
{
	mInput->Deinit();
	delete mInput;
}

void Input::Update()
{ 
	mInput->Update();
}

bool Input::GetStateBool(InputControl control)
{
	return mInput->GetStateBool(control);
}

float Input::GetState(InputControl control)
{
	return mInput->GetState(control);
}

bool Input::WasPressed(InputControl control)
{
	return mInput->WasPressed(control);
}

int Input::GetJoystickCount()
{ 
	return mInput->GetJoystickCount(); 
}

int Input::GetJoystickIndex(InputControl control)
{
	if (control > InputControl_JoyStick0Start && control < InputControl_JoyStick0End)
		return 0;

	if (control > InputControl_JoyStick1Start && control < InputControl_JoyStick1End)
		return 1;

	if (control > InputControl_JoyStick2Start && control < InputControl_JoyStick2End)
		return 2;

	if (control > InputControl_JoyStick3Start && control < InputControl_JoyStick3End)
		return 3;

	return -1;
}

Joystick* Input::GetJoystick(int idx)
{
	return mInput->GetJoystick(idx);
}

void Input::SetMouseSensitivity(float sensitivity)	
{
	 mInput->SetMouseSensitivity(sensitivity); 
}

