#ifndef _TASKMGR_H_
#define _TASKMGR_H_

#include "Template/Singleton.h"
#include "Thread.h"
#include "Semaphore.h"
#include <vector>
#include <queue>

using namespace std;

#define gThreadPool ThreadPool::GetInstance()

// use this task if you want to sync all threads after a certain task
//unsigned long WaitTask(void* ptr, Thread* thread);

class ThreadPool : public Singleton<ThreadPool>
{
public:

	typedef vector<Thread>::iterator ThreadIterator;

	void			Init(int numThreads = -1);
	void			Deinit();

	Task*			CreateTask(unsigned int taskFlags = 0);

	void			AddTask(Task* task);
	Task*			PopLastTask();

	// returns the number of logical cpu cores
	unsigned int	GetNumCPULogicalCores();

	// wait for all tasks to be completed
	void			Wait();

	// tasks marked with TF_AddWhenDone will be readded to the queue once this method is called
	// this is done so wait wont get stuck in an infinite loop
	void			AddTasksToAdd();

	ThreadIterator	ThreadBegin()			{ return mThreads.begin(); }
	ThreadIterator	ThreadEnd()				{ return mThreads.end(); }
	unsigned int	GetThreadCount()		{ return mThreads.size(); }

protected:

	unsigned long TaskFn(void* ptr, Thread* thread);
	static unsigned long StaticTaskFn(void* ptr, Thread* thread);

	unsigned int	mNumLogicalCores;
	queue<Task*>	mTasks;
	queue<Task*>	mTasksToAdd;	// tasks that need reading
	vector<Thread>	mThreads;
	Task			mDefaultThreadTask;
	Semaphore		mSemaphore;
	Mutex			mActiveThreadMutex;
	unsigned int	mActiveThread;
	queue<Task*>	mTaskPool; // unused tasks
};

#endif
