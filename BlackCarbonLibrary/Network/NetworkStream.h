#ifndef _NETWORKSTREAM_H_
#define _NETWORKSTREAM_H_

#ifdef WIN32
	#define WIN32_LEAN_AND_MEAN
    #define NOMINMAX
    #include <windows.h>

    #include <winsock.h>
#else
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <netdb.h>
    #include <unistd.h>

    typedef int SOCKET;
#endif

//#undef GetMessage


#ifndef WIN32
    #define SOCKET_ERROR -1
    #define INVALID_SOCKET -1
    #define SOCKADDR sockaddr

    #define closesocket close
#endif

#define SD_BOTH 2

enum StreamState
{
	eNone	= 0x0,
	eRead	= 0x1 << 0,
	eWrite	= 0x1 << 1,
	eError	= 0x1 << 2
};

class NetworkStream
{
	friend class NetworkFactory;

public:

	NetworkStream() : mSocket(0) { }
	~NetworkStream() {}

	bool IsValid() { return mSocket == 0 ? false : true; }

	//virtual void Send(Packet* packet) = 0;
	//virtual void Receive(Packet* packet) = 0;

	virtual StreamState GetState();

	void Close()
	{
	    shutdown(mSocket, SD_BOTH);
        closesocket(mSocket);
		mSocket = 0;
	}

protected:

	SOCKET		mSocket;
};

#endif
