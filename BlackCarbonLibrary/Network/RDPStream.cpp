#include "RDPStream.h"
#include "Packet.h"
#include "Memory/Memory.h"


RDPStream::RDPStream(NetworkFactory* factory) : 
	mUDPStream(0),
	mFactory(factory)
{
	mUDPStream = mFactory->CreateUDPStream();
}

RDPStream::~RDPStream()
{
	mFactory->Release(&mUDPStream);
}

bool RDPStream::Bind(const SocketAddr& onAddr)
{
	return mUDPStream->Bind(onAddr);
}

RDPPeer* RDPStream::Connect(const SocketAddr& toAddr, bool bHost)
{
	list<RDPPeer*>::iterator it;
	for (it = mPeer.begin(); it != mPeer.end(); ++it)
	{
		if (mFactory->Equals((*it)->GetConnectAddr(), toAddr))	
			return *it;
	}

	RDPPeer* connecting = new RDPPeer(this);
	connecting->Connect(toAddr, bHost);
	mPeer.push_back(connecting);
	return connecting;
}

void RDPStream::UpdateReceive()
{
	// update UDP stream
	while (mUDPStream->GetState() & eRead)
	{
		Packet* p = new Packet();
		mUDPStream->Receive(p);

		// find which peer the packet came from
		bool peerFound = false;
		list<RDPPeer*>::iterator it;
		for (it = mPeer.begin(); it != mPeer.end(); ++it)
		{
			if (mFactory->Equals(p->GetSockAddress(), (*it)->GetConnectAddr()))
			{
				(*it)->ReceiveInternal(p);
				peerFound = true;
				break;
			}
		}

		// oh my! we found a new client trying to connect
		if (!peerFound)
		{
			RDPPeer* peer = new RDPPeer(this);
			peer->Accept(p->GetSockAddress());
			mPeer.push_back(peer);
			peer->ReceiveInternal(p);
		}
	}
}

void RDPStream::UpdateSend()
{
	// update peers
	list<RDPPeer*>::iterator it;
	for (it = mPeer.begin(); it != mPeer.end(); ++it)
	{
		(*it)->Update();
	}
}

int	RDPStream::Send(Packet* packet)
{
	list<RDPPeer*>::iterator it;
	for (it = mPeer.begin(); it != mPeer.end(); ++it)
	{
		(*it)->Send(packet);
	}

	return packet->GetSize();
}
