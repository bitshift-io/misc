#include "NetworkStream.h"

StreamState NetworkStream::GetState()
{
	if (!IsValid())
		return eNone;

	int state = 0;

	fd_set readFDs, writeFDs, exceptFDs;
	FD_ZERO(&readFDs);
	FD_ZERO(&writeFDs);
	FD_ZERO(&exceptFDs);

	FD_SET(mSocket, &readFDs);
	FD_SET(mSocket, &exceptFDs);
	FD_SET(mSocket, &writeFDs);

	timeval val;
	val.tv_sec = 0;
	val.tv_usec = 0;

	int value = select(0, &readFDs, &writeFDs, &exceptFDs, &val);
	if (value == SOCKET_ERROR)
		return eNone;

	if (value > 0)
	{
		if (FD_ISSET(mSocket, &readFDs))
			state |= eRead;

		if (FD_ISSET(mSocket, &exceptFDs))
			state |= eError;

		if (FD_ISSET(mSocket, &writeFDs))
			state |= eWrite;
	}

	return StreamState(state);
}
