#include "HTTP.h"
#include "NetworkFactory.h"
#include "TCPStream.h"
#include "Packet.h"

HTTP::HTTP(NetworkFactory* factory) :
	mFactory(factory)
{

}

bool HTTP::Connect(const char* host, int port)
{
	mPort = port;
	mHost = host;

	mStream = mFactory->CreateTCPStream();
	if (!mStream->Connect(mFactory->GetAddress(host, port)))
		return false;

	return true;
}

void HTTP::Close()
{
	mStream->Close();
	mFactory->Release(&mStream);
}

bool HTTP::Post(const char* URI, const char* contentType, Packet* send, Packet* receive)
{
	Packet p;

	string line = "POST " + string(URI) + " HTTP/1.1\r\nCache-Control: no-cache\r\n";	
	p.Append(line.c_str(), line.length());

	line = "Host: " + mHost + "\r\n";
	p.Append(line.c_str(), line.length());

	line = "Content-Type: " + string(contentType) + "\r\n";
	p.Append(line.c_str(), line.length());

	char buffer[256];
	sprintf(buffer, "%i", send->GetSize());
	line = "Content-Length: " + string(buffer) + "\r\n";
	p.Append(line.c_str(), line.length());

	AppendHTTPArg(&p);

	line = "\r\n";
	p.Append(line.c_str(), line.length());

	p.Append(send->GetBuffer(), send->GetSize());

	mStream->Send(&p);
	mStream->Receive(receive);
	return CheckAndStripHeader(receive);
}

bool HTTP::Get(const char* URI, Packet* receive)
{
	Packet p;

	string line = "GET " + string(URI) + " HTTP/1.1\r\n";	
	p.Append(line.c_str(), line.length());

	line = "Host: " + mHost + "\r\n";
	p.Append(line.c_str(), line.length());

	AppendHTTPArg(&p);

	line = "\r\n";
	p.Append(line.c_str(), line.length());

	mStream->Send(&p);
	mStream->Receive(receive);
	return CheckAndStripHeader(receive);
}

bool HTTP::CheckAndStripHeader(Packet* received)
{
	char line[512];
	received->ReadLine(line, 512);

	bool ret = false;
	if (strcmpi("HTTP/1.1 200 OK", line) == 0)
		ret = true;

	// strip rest of http header
	while (strcmpi("", line) != 0)
	{
		received->ReadLine(line, 512);
	}

	return ret;
}

void HTTP::InsertHTTPArg(const char* arg)
{
	mHTTPArg.push_back(string(arg));
}

void HTTP::AppendHTTPArg(Packet* send)
{
	vector<string>::iterator it;
	for (it = mHTTPArg.begin(); it != mHTTPArg.end(); ++it)
	{
		string line = *it + string("\r\n");
		send->Append(line.c_str(), line.length());
	}
}