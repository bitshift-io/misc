#pragma comment(lib, "Ws2_32.lib")

#include "NetworkFactory.h"
#include "RDPStream.h"
#include "UDPStream.h"
#include "TCPStream.h"
#include "HTTP.h"
#include "File/Log.h"
#include "Memory/NoMemory.h"
#include "Regex/Regex.h"
#include "Memory/Memory.h"

using namespace boost;

ostream& operator <<(ostream& os, const SocketAddr& addr)
{
	unsigned char* ip = (unsigned char*)&(addr.sin_addr.s_addr);
	os << (int)ip[0] << "." << (int)ip[1] << "." << (int)ip[2] << "." << (int)ip[3] << ":" << (int)ntohs(addr.sin_port);
	return os;
}

NetworkFactory::NetworkFactory()
{
#ifdef WIN32
	// start winsock
	WSAData wsaData;
	int nCode;
	if ((nCode = WSAStartup(MAKEWORD(2, 2), &wsaData)) != 0)
	{
		Log::Error("WSAStartup returned error code %i\n", nCode);
	}
#endif
}

NetworkFactory::~NetworkFactory()
{
#ifdef WIN32
	// shutdown winsock
	WSACleanup();
#endif
}

unsigned long NetworkFactory::ResolveAddress(const char* addr)
{
	unsigned long remoteAddr = inet_addr(addr);
	if (remoteAddr == INADDR_NONE)
	{
		// pcHost isn't a dotted IP, so resolve it through DNS
		hostent* he = gethostbyname(addr);
		if (he == 0)
			return INADDR_NONE;

		remoteAddr = *((unsigned long*)he->h_addr_list[0]);
	}

	return remoteAddr;
}

SocketAddr NetworkFactory::GetAddress(const char* addr, unsigned int port)
{
	sockaddr_in adr;
	memset(&adr, 0, sizeof(sockaddr_in));
	adr.sin_family = AF_INET;
	adr.sin_port = htons(port);
	adr.sin_addr.s_addr = ResolveAddress(addr);

	return adr;
}

SocketAddr NetworkFactory::GetBroadcastAddress(unsigned int port)
{
	sockaddr_in adr;
	memset(&adr, 0, sizeof(sockaddr_in));
	adr.sin_family = AF_INET;

	adr.sin_port = htons(port);
	adr.sin_addr.s_addr = htonl(INADDR_ANY);
	return adr;
}

SocketAddr NetworkFactory::GetAnyAddress(unsigned int port)
{
	sockaddr_in adr;
	memset(&adr, 0, sizeof(sockaddr_in));
	adr.sin_family = AF_INET;

	adr.sin_port = htons(port);
	adr.sin_addr.s_addr = htonl(INADDR_ANY);
	return adr;
}

SocketAddr NetworkFactory::GetInvalidAddress()
{
	sockaddr_in adr;
	memset(&adr, 0, sizeof(sockaddr_in));
	return adr;
}

bool NetworkFactory::Equals(const SocketAddr& a, const SocketAddr& b)
{
	if (a.sin_addr.s_addr == b.sin_addr.s_addr
		&& a.sin_port == b.sin_port)
		return true;

	return false;
}

int NetworkFactory::GetLastError()
{
#ifdef WIN32
	return WSAGetLastError();
#else
    return 0;
#endif
}

RDPStream* NetworkFactory::CreateRDPStream()
{
	return new RDPStream(this);
}

void NetworkFactory::Release(RDPStream** stream)
{
	if (!*stream)
		return;

	delete *stream;
	*stream = 0;
}

UDPStream* NetworkFactory::CreateUDPStream()
{
	return new UDPStream();
}

void NetworkFactory::Release(UDPStream** stream)
{
	if (!*stream)
		return;

	delete *stream;
	*stream = 0;
}

TCPStream* NetworkFactory::CreateTCPStream()
{
	return new TCPStream();
}

void NetworkFactory::Release(TCPStream** stream)
{
	if (!*stream)
		return;

	delete *stream;
	*stream = 0;
}

HTTP* NetworkFactory::CreateHTTP()
{
	return new HTTP(this);
}

void NetworkFactory::Release(HTTP** http)
{
	if (!*http)
		return;

	delete *http;
	*http = 0;
}

bool NetworkFactory::ConvertToString(const SocketAddr& addr, string& ip, unsigned int& port)
{
	port = ntohs(addr.sin_port);

	char buffer[256];

	#ifdef WIN32
        int a = addr.sin_addr.s_net;
        int b = addr.sin_addr.s_host;
        int c = addr.sin_addr.s_lh;
        int d = addr.sin_addr.s_impno;
	#else // TODO: TEST ME
        int a = (addr.sin_addr.s_addr >> 24) & 0xff;
        int b = (addr.sin_addr.s_addr >> 16) & 0xff;
        int c = (addr.sin_addr.s_addr >> 8) & 0xff;
        int d = addr.sin_addr.s_addr & 0xff;
	#endif

	sprintf(buffer, "%i.%i.%i.%i", a, b, c, d);
	ip = buffer;
	return true;
}

bool NetworkFactory::ParseURL(const char* URL, string* protocol, string* host, int* port, string* path, string* args)
{
	cmatch match;
	regex expression("^(.*)://([^:/]*)([^/]*)([^?#&]*)(.*)$");
	if (!regex_search(URL, match, expression))
		return false;

	if (protocol)
	{
		char buffer[256] = "";
		strcpy(buffer, match[1].first);
		buffer[match[1].second - match[1].first] = '\0';
		strcpy(buffer, StripWhitespace(buffer).c_str());
		*protocol = buffer;
	}

	if (host)
	{
		char buffer[256] = "";
		strcpy(buffer, match[2].first);
		buffer[match[2].second - match[2].first] = '\0';
		strcpy(buffer, StripWhitespace(buffer).c_str());
		*host = buffer;
	}

	
	if (port)
	{
		*port = 80;

		char buffer[256] = "";
		strcpy(buffer, match[3].first);
		buffer[match[3].second - match[3].first] = '\0';
		strcpy(buffer, StripWhitespace(buffer).c_str());
		if (strlen(buffer) > 1)
			*port = atoi(buffer + 1);
	}

	if (path)
	{
		char buffer[256] = "";
		strcpy(buffer, match[4].first);
		buffer[match[4].second - match[4].first] = '\0';
		strcpy(buffer, StripWhitespace(buffer).c_str());
		*path = buffer;
	}

	if (args)
	{
		char buffer[256] = "";
		strcpy(buffer, match[5].first);
		buffer[match[5].second - match[5].first] = '\0';
		strcpy(buffer, StripWhitespace(buffer).c_str());
		*args = buffer;
	}

	return true;	
}

string NetworkFactory::ConvertStringToURL(const char* str)
{
	// http://www.w3schools.com/HTML/html_urlencode.asp
	struct URLConvertTable
	{
		char		ascii;
		const char* html;
	};
	URLConvertTable convertTable[] = 
	{
		'[', "%5B",
		']', "%5D",
		'_', "%5F",
		' ', "%20",
		'.', "%2E",
		':', "%3A",
		';', "%3B",
		'{', "%7B",
		'}', "%7D",
		'}', "%7D",
		'\"', "%22",
		0, 0,
	};

	string retStr;
	int strLen = strlen(str);
	for (int i = 0; i < strLen; ++i)
	{
		int l = 0;
		while (convertTable[l].html)
		{
			if (convertTable[l].ascii == str[i])
			{
				retStr += convertTable[l].html;
				break;
			}

			++l;
		}

		if (convertTable[l].html == 0)
			retStr.push_back(str[i]);
	}
	
	return retStr;
}

string NetworkFactory::StripWhitespace(const string& str)
{
	string temp = str;
	int len = temp.length();
	int i = 0;
	for (; i < len; ++i)
	{
		if (temp[i] != ' ' && temp[i] != '\n' && temp[i] != '\t' && temp[i] != '\r' && temp[i] != '\f')
		{
			if (i != 0)
				temp = temp.substr(i, len);

			break;
		}
	}

	if (i >= len)
		return "";

	len = temp.length();
	for (i = len - 1; i >= 0; --i)
	{
		if (temp[i] != ' ' && temp[i] != '\n' && temp[i] != '\t' && temp[i] != '\r' && temp[i] != '\f')
		{
			if (i != len)
				temp = temp.substr(0, i + 1);

			break;
		}
	}

	return temp;
}

char* NetworkFactory::Base64Encode(char *dst, const char *src, unsigned int len)
{
	#define MAXLINE 76 

	char base64_table[] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ""abcdefghijklmnopqrstuvwxyz""0123456789+/"};
	int x, y = 0, i, flag=0, m=0;
	int n = 3;
	char triple[3], quad[4], c;
	static char encode[1024]={0};
 
	for(x = 0; x < len; x = x + 3)
	{
		if((len - x) / 3 == 0) n = (len - x) % 3;

		for(i=0; i < 3; i++) triple[i] = '0';
		for(i=0; i < n; i++) triple[i] = src[x + i];
		quad[0] = base64_table[(triple[0] & 0xFC) >> 2]; // FC = 11111100
		quad[1] = base64_table[((triple[0] & 0x03) << 4) | ((triple[1] & 0xF0) >> 4)]; // 03 = 11
		quad[2] = base64_table[((triple[1] & 0x0F) << 2) | ((triple[2] & 0xC0) >> 6)]; // 0F = 1111, C0=11110
		quad[3] = base64_table[triple[2] & 0x3F]; // 3F = 111111
		if(n < 3) quad[3] = '=';
		if(n < 2) quad[2] = '=';
		for(i=0; i < 4; i++) dst[y + i] = quad[i];
		y = y + 4;
		m = m + 4;
		if((m != 0)&&((m % MAXLINE) == 0))
		{
			dst[y] = '\r';
			dst[y+1] = '\n';
			flag++;
			y += 2;
			m = 0;
		}
	}

	dst[y] = '\0';
	sprintf(encode,dst);
	return encode;
} 