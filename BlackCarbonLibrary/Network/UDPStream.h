#ifndef _UDPSTREAM_H_
#define _UDPSTREAM_H_

#include "NetworkFactory.h"
#include "NetworkStream.h"
#include <list>

#define	UDP_BUFFER_SIZE		2048

using namespace std;

class Packet;

class UDPStream : public NetworkStream
{
public:

	UDPStream(); // what port to send udp from

	bool				Bind(const SocketAddr& onAddr);
	int					Send(const SocketAddr& toAddr, Packet* packet);
	virtual int			Receive(Packet* packet);
	const SocketAddr&	GetBindAddr()									{ return mBindAddr; }

	virtual StreamState GetState();

protected:

	SocketAddr		mBindAddr;
};

#endif
