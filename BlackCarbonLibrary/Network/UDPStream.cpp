#include "UDPStream.h"
#include "NetworkFactory.h"
#include "Packet.h"
#include "Template/Time.h"
#include "File/Log.h"

// enable to fake packet loss
//#define FAKE_LAG
//#define FAKE_PACKETLOSS

#if defined FAKE_PACKETLOSS || defined FAKE_LAG

#include <list>

using namespace std;

const int cPacketLoss = 10;
const unsigned int cSendDelay = 500;

struct PacketInfo
{
	Packet*			packet;
	unsigned int	time;
};
list<PacketInfo>	sDelayPacket;

#endif


UDPStream::UDPStream()
{
	mSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (mSocket == INVALID_SOCKET)
	{
		mSocket = 0;
		return;
	}
}

StreamState UDPStream::GetState()
{
#ifdef FAKE_LAG

	// send any delayed packets
	unsigned int time = gTime.GetMilliseconds();
	list<PacketInfo>::iterator it;
	for (it = sDelayPacket.begin(); it != sDelayPacket.end();)
	{
		if ((it->time + cSendDelay) < time)
		{
			Send(it->packet->GetSockAddress(), it->packet);
			delete it->packet;
			it = sDelayPacket.erase(it);
		}
		else
		{
			++it;
		}
	}
#endif

	return NetworkStream::GetState();
}

int UDPStream::Send(const SocketAddr& toAddr, Packet* packet)
{
#ifdef FAKE_PACKETLOSS

	int r = rand() % 100;
	if (r < cPacketLoss)
		return packet->GetSize();

#endif

#ifdef FAKE_LAG

	// see if packet is already in the queue, if it is, send it
	bool found = false;
	list<PacketInfo>::iterator it;
	for (it = sDelayPacket.begin(); it != sDelayPacket.end(); ++it)
	{
		if (it->packet == packet)
		{
			found = true;
			break;
		}
	}

	if (!found)
	{
		Packet* p = new Packet();
		p->Append(packet->GetBuffer(), packet->GetSize());
		p->SetSockAddress(toAddr);

		PacketInfo info;
		info.packet = p;
		info.time = gTime.GetMilliseconds();
		sDelayPacket.push_back(info);
		return packet->GetSize();
	}

	// send anything in the queue older than cSendDelay
#endif

	int bytes = sendto(mSocket, packet->GetBuffer(), packet->GetSize(), 0, (SOCKADDR*)&toAddr, sizeof(SocketAddr));

/*
	static int total = 0;
	total += bytes;
	Log::Print("total sent: %i\n", total);
*/
	return bytes;
}

bool UDPStream::Bind(const SocketAddr& onAddr)
{
	if (bind(mSocket, (SOCKADDR*)&onAddr, sizeof(SocketAddr)) == SOCKET_ERROR)
	{
		return false;
	}

	mBindAddr = onAddr;
	return true;
}

int UDPStream::Receive(Packet* packet)
{
	sockaddr_in rcvAddr;

	#ifdef WIN32
		int i = sizeof(SOCKADDR);
	#else
		unsigned int i = sizeof(SOCKADDR);
	#endif

	char buffer[UDP_BUFFER_SIZE];

	int bytesReceived = recvfrom(mSocket, buffer, UDP_BUFFER_SIZE, 0, (SOCKADDR*)&rcvAddr, &i);

	if (bytesReceived <= 0)
	{
		return bytesReceived;
	}

	packet->Append(buffer, bytesReceived);
	packet->SetSockAddress(rcvAddr);
/*
	static int total = 0;
	total += bytesReceived;
	Log::Print("total received: %i\n", total);
*/
	return bytesReceived;
}
