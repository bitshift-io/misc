#include <string>

// http://www.adp-gmbh.ch/cpp/common/base64.html
std::string base64_encode(const char* , unsigned int len);
std::string base64_decode(std::string const& s);
