#ifndef _RDPPEER_H_
#define _RDPPEER_H_

#include "NetworkFactory.h"
#include "NetworkStream.h"

class RDPState;
class Packet;

enum RDPConnectionState
{
	CS_Closed,
	CS_Open,
	CS_Connecting,
	CS_Connected,	// only in this state for a single frame
};

class RDPPeer
{
	friend class RDPStream;

public:

	const SocketAddr&	GetConnectAddr()									{ return mConnectAddr; }

	RDPConnectionState	GetConnectionState();

	StreamState			GetState();

	//
	// increasing this means data tranfers faster, but uses more bandwidth so could chock up your connection
	//
	void				SetSendBandwidth(unsigned int bytesPerUpdate)		{ mSendBytesPerUpdate = bytesPerUpdate; }
	int					GetSentQueueSize()									{ return mSendList.size(); }
	int					GetPacketsSent()									{ return mPacketsSentThisUpdate; }

	int					Send(Packet* packet);
	int					Receive(Packet* packet);

	//
	// Note: ping is sent using the reliable protocol so it as if we were sending ping as a reliable message
	// this ensures maximum accuracey and helps me to be able to optmise my network code
	//
	unsigned int		GetPing()											{ return mPing; }

protected:


	enum RDPPeerState
	{
		PS_Closed,
		PS_SyncReceived,
		PS_SyncSent,
		PS_Open,
		PS_WaitClose,
	};

	RDPPeer(RDPStream* stream);

	void				Accept(const SocketAddr& fromAddr);
	void				Connect(const SocketAddr& toAddr, bool bHost);
	void				ReceiveInternal(Packet* packet);
	void				ReceiveProtocol(Packet* packet);
	void				ReceiveReliableProtocol(Packet* packet);
	int					SendInternal(Packet* packet);

	void				SendPingPacket();

	void				Update();

	SocketAddr			mConnectAddr;
	RDPStream*			mStream;

	RDPPeerState		mState;
	RDPConnectionState	mConnectionState;

	list<Packet*>		mSendList;
	list<Packet*>		mReceiveList;

	unsigned int		mSendPacketId;		// used to label outbound packets
	unsigned int		mReceivePacketId;	// used to indicate what packets we have received in order

	unsigned long		mPing;				// ping in ms
	unsigned int		mSendBytesPerUpdate;
	unsigned long		mPingSendTime;

	unsigned int		mPacketsSentThisUpdate;
};

#endif
