#include "PostEffect.h"
#include "Device.h"
#include "RenderFactory.h"
#include "Camera.h"
#include "Material.h"
#include "Geometry.h"

void PostEffect::Init(RenderFactory* factory, Device* device, Material* material)
{
	mTime = 0.f;
	mMaterial = material;
	mGeometry = InitQuad(factory, device, 2.0f, 2.0f);
}

void PostEffect::Deinit(RenderFactory* factory, Device* device)
{
	DeinitQuad(factory, mGeometry);
}

void PostEffect::Render(Device* device)
{
	RenderScreenQuad(device, mGeometry, mMaterial, mTime);
}

Geometry* PostEffect::InitQuad(RenderFactory* factory, Device* device, float width, float height)
{
	Geometry* geometry = static_cast<Geometry*>(factory->Create(Geometry::Type));

	geometry->Init(device, VF_Position | VF_TexCoord0, 4, 6, RBU_Static);
	//geometry->Lock(VF_Index | VF_Position | VF_TexCoord0);

	Vector4	topLeft(-width * 0.5f, height * 0.5f);
	Vector4 bottomRight(width * 0.5f, -height * 0.5f);
	
	RenderBuffer* idxBuffer = geometry->GetIndexBuffer();
	unsigned int idx = 0; //idxBuffer->GetSize();

	VertexFrame& buffer = geometry->GetVertexFrame();
	unsigned int vertIdx = 0; //buffer.mPosition->GetSize();

	RenderBufferLock lock;
	idxBuffer->Lock(lock);
	lock.uintData[idx + 0] = vertIdx + 0;
	lock.uintData[idx + 1] = vertIdx + 2;
	lock.uintData[idx + 2] = vertIdx + 1;

	lock.uintData[idx + 3] = vertIdx + 0;
	lock.uintData[idx + 4] = vertIdx + 3;
	lock.uintData[idx + 5] = vertIdx + 2;
	idxBuffer->Unlock(lock);

	buffer.mTexCoord[0]->Lock(lock);
	lock.vector4Data[vertIdx + 0] = Vector4(0.f, 0.f);
	lock.vector4Data[vertIdx + 1] = Vector4(0.f, 1.f);
	lock.vector4Data[vertIdx + 2] = Vector4(1.f, 1.f);
	lock.vector4Data[vertIdx + 3] = Vector4(1.f, 0.f);
	buffer.mTexCoord[0]->Unlock(lock);

	buffer.mPosition->Lock(lock);
	lock.vector4Data[vertIdx + 0] = Vector4(topLeft);
	lock.vector4Data[vertIdx + 1] = Vector4(topLeft.x, bottomRight.y);
	lock.vector4Data[vertIdx + 2] = Vector4(bottomRight);
	lock.vector4Data[vertIdx + 3] = Vector4(bottomRight.x, topLeft.y);
	buffer.mPosition->Unlock(lock);
/*
	idxBuffer->SetUnsignedInt(idx + 0, vertIdx + 0);
	idxBuffer->SetUnsignedInt(idx + 1, vertIdx + 2);
	idxBuffer->SetUnsignedInt(idx + 2, vertIdx + 1);

	idxBuffer->SetUnsignedInt(idx + 3, vertIdx + 0);
	idxBuffer->SetUnsignedInt(idx + 4, vertIdx + 3);
	idxBuffer->SetUnsignedInt(idx + 5, vertIdx + 2);
* /
	buffer.mTexCoord[0]->SetVector4(vertIdx + 0, Vector4(0.f, 0.f));
	buffer.mTexCoord[0]->SetVector4(vertIdx + 1, Vector4(0.f, 1.f));
	buffer.mTexCoord[0]->SetVector4(vertIdx + 2, Vector4(1.f, 1.f));
	buffer.mTexCoord[0]->SetVector4(vertIdx + 3, Vector4(1.f, 0.f));

	buffer.mPosition->SetVector4(vertIdx + 0, Vector4(topLeft));
	buffer.mPosition->SetVector4(vertIdx + 1, Vector4(topLeft.x, bottomRight.y));
	buffer.mPosition->SetVector4(vertIdx + 2, Vector4(bottomRight));
	buffer.mPosition->SetVector4(vertIdx + 3, Vector4(bottomRight.x, topLeft.y));

	geometry->Unlock(VF_Index | VF_Position | VF_TexCoord0);
*/
	return geometry;
}

void PostEffect::DeinitQuad(RenderFactory* factory, Geometry* geometry)
{
	factory->Release((SceneObject**)&geometry);
}

void PostEffect::RenderScreenQuad(Device* device, Geometry* geometry, Material* material, float time)
{
	Camera camera;
	camera.SetOrthographic();

	Matrix4 transform(MI_Identity);

	int count = material->Begin();
	for (int i = 0; i < count; ++i)
	{
		material->BeginPass(i);
		material->GetEffect()->SetMatrixParameters(camera.GetProjection(), camera.GetView(), camera.GetWorld(), transform);
		//material->GetEffect()->SetTimeParameters(time);
		material->SetPassParameter();
		//device->Draw(DT_NotInstanced, material, geometry, material->GetVertexFormat(i));
		geometry->Draw();
		material->EndPass();
		material->End();
	}
}