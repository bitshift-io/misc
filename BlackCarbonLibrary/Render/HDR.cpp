#include "HDR.h"
#include "File/Log.h"

//#include <malloc.h>
#include <math.h>
#include <string.h>
#include <ctype.h>


/* offsets to red, green, and blue components in a data (float) pixel */
#define RGBE_DATA_RED    0
#define RGBE_DATA_GREEN  1
#define RGBE_DATA_BLUE   2

/* number of floats per pixel */
#define RGBE_DATA_SIZE   3

enum RgbeError_codes {
  rgbe_read_error,
  rgbe_write_error,
  rgbe_format_error,
  rgbe_memory_error,
};
/*
HDR::HDR() : image(0)
{

}

HDR::~HDR()
{
	if (image)
		delete[] image;
}*/

bool HDR::Load(const char* file)
{/*
	RgbaInputFile hdrFile(file);
	Imath::Box2i dw = hdrFile.dataWindow();

	width = dw.max.x - dw.min.x + 1;
	height = dw.max.y - dw.min.y + 1;

	image = (unsigned char*)malloc(sizeof(Rgba) * width * height);

	hdrFile.setFrameBuffer((Rgba*)image, 1, width); // - dw.min.x - dw.min.y * width
	hdrFile.readPixels(dw.min.y, dw.max.y);
*/

	FILE *fp = fopen(file, "rb");
	if (!fp)
	{
		fprintf(stderr, "Error opening file '%s'\n", file);
		return false;
	}

	int width;
	int height;
	rgbe_header_info header;
	if (!ReadHeader(fp, &width, &height, &header))
		return false;

	unsigned char* image = new unsigned char[width*height*4];

	if (!ReadPixels_Raw_RLE(fp, image, width, height))
		return false;

	fclose(fp);

	ConvertToFloat32(CreateResourceInfo(TF_HDR_RGB, width, height), image);
	delete[] image;

	return true;
}
/*
unsigned char* HDR::GetPixels()
{
	return image;
}

int HDR::GetWidth()
{
	return width;
}

int HDR::GetHeight()
{
	return height;
}

int HDR::GetBPP()
{
	return 0;//(sizeof(Rgba) / 4) * 8;
}
*/
void HDR::ConvertToFloat32(TextureSubresource* resource, unsigned char* image)
{
	unsigned char* rgbeImage = image;
	unsigned char* floatImage = (unsigned char*)resource->pixel; //new unsigned char[sizeof(float) * 3 * width * height];

	float* fTex = (float*)floatImage;
	unsigned char* cTex = rgbeImage;
	for (int i = 0; i < resource->width * resource->height; ++i)
	{
		Rgbe2float(&fTex[0], &fTex[1], &fTex[2], cTex);
		fTex += 3;
		cTex += 4;
	}

	//image = floatImage;
	//delete[] rgbeImage;
}

/* default error routine.  change this to change error handling */
bool HDR::RgbeError(int RgbeError_code, char *msg)
{
	switch (RgbeError_code)
	{
	case rgbe_read_error:
		perror("RGBE read error");
		break;
	case rgbe_write_error:
		perror("RGBE write error");
		break;
	case rgbe_format_error:
		fprintf(stderr,"RGBE bad file format: %s\n",msg);
		break;
	default:
	case rgbe_memory_error:
		fprintf(stderr,"RGBE error: %s\n",msg);
	}
	return false;
}

void HDR::Float2rgbe(unsigned char rgbe[4], float red, float green, float blue)
{
	float v;
	int e;

	v = red;
	if (green > v)
		v = green;

	if (blue > v)
		v = blue;

	if (v < 1e-32)
	{
		rgbe[0] = rgbe[1] = rgbe[2] = rgbe[3] = 0;
	}
	else
	{
		v = frexp(v,&e) * 256.0/v;
		rgbe[0] = (unsigned char) (red * v);
		rgbe[1] = (unsigned char) (green * v);
		rgbe[2] = (unsigned char) (blue * v);
		rgbe[3] = (unsigned char) (e + 128);
	}
}

void HDR::Rgbe2float(float *red, float *green, float *blue, unsigned char rgbe[4])
{
	float f;

	if (rgbe[3])
	{
		f = ldexp(1.0,rgbe[3]-(int)(128+8));
		*red = rgbe[0] * f;
		*green = rgbe[1] * f;
		*blue = rgbe[2] * f;
	}
	else
	{
		*red = *green = *blue = 0.0;
	}
}

bool HDR::WriteHeader(FILE *fp, int width, int height, rgbe_header_info *info)
{
	char *programtype = "RGBE";

	if (info && (info->valid & RGBE_VALID_PROGRAMTYPE))
		programtype = info->programtype;

	if (fprintf(fp,"#?%s\n",programtype) < 0)
		return RgbeError(rgbe_write_error,NULL);

	/* The #? is to identify file type, the programtype is optional. */
	if (info && (info->valid & RGBE_VALID_GAMMA))
	{
		if (fprintf(fp,"GAMMA=%g\n",info->gamma) < 0)
		return RgbeError(rgbe_write_error,NULL);
	}

	if (info && (info->valid & RGBE_VALID_EXPOSURE))
	{
		if (fprintf(fp,"EXPOSURE=%g\n",info->exposure) < 0)
		return RgbeError(rgbe_write_error,NULL);
	}

	if (fprintf(fp,"FORMAT=32-bit_rle_rgbe\n\n") < 0)
		return RgbeError(rgbe_write_error,NULL);

	if (fprintf(fp, "-Y %d +X %d\n", height, width) < 0)
		return RgbeError(rgbe_write_error,NULL);

	return true;
}

int HDR::ReadHeader(FILE *fp, int *width, int *height, rgbe_header_info *info)
{
	char buf[128];
	int found_format;
	float tempf;
	int i;

	found_format = 0;
	if (info)
	{
		info->valid = 0;
		info->programtype[0] = 0;
		info->gamma = info->exposure = 1.0;
	}

	if (fgets(buf,sizeof(buf)/sizeof(buf[0]),fp) == NULL)
		return RgbeError(rgbe_read_error,NULL);

	if ((buf[0] != '#')||(buf[1] != '?'))
	{
		/* if you want to require the magic token then uncomment the next line */
		/*return RgbeError(rgbe_format_error,"bad initial token"); */
	}
	else if (info)
	{
		info->valid |= RGBE_VALID_PROGRAMTYPE;
		for(i=0;i<sizeof(info->programtype)-1;i++)
		{
			if ((buf[i+2] == 0) || isspace(buf[i+2]))
				break;

			info->programtype[i] = buf[i+2];
		}

		info->programtype[i] = 0;
		if (fgets(buf,sizeof(buf)/sizeof(buf[0]),fp) == 0)
			return RgbeError(rgbe_read_error,NULL);
	}

	for(;;)
	{
		if ((buf[0] == 0)||(buf[0] == '\n'))
		{
			return RgbeError(rgbe_format_error,"no FORMAT specifier found");
		}
		else if (strcmp(buf,"FORMAT=32-bit_rle_rgbe\n") == 0)
		{
			break;       /* format found so break out of loop */
		}
		else if (info && (sscanf(buf,"GAMMA=%g",&tempf) == 1))
		{
			info->gamma = tempf;
			info->valid |= RGBE_VALID_GAMMA;
		}
		else if (info && (sscanf(buf,"EXPOSURE=%g",&tempf) == 1))
		{
			info->exposure = tempf;
			info->valid |= RGBE_VALID_EXPOSURE;
		}
		if (fgets(buf,sizeof(buf)/sizeof(buf[0]),fp) == 0)
		{
			return RgbeError(rgbe_read_error,NULL);
		}
	}

#if 0
	if (fgets(buf,sizeof(buf)/sizeof(buf[0]),fp) == 0)
		return RgbeError(rgbe_read_error,NULL);

	if (strcmp(buf,"\n") != 0)
		return RgbeError(rgbe_format_error, "missing blank line after FORMAT specifier");
#endif

	for(;;)
	{
		if (fgets(buf,sizeof(buf)/sizeof(buf[0]),fp) == 0)
			return RgbeError(rgbe_read_error,NULL);

		if (sscanf(buf,"-Y %d +X %d",height,width) == 2)
			break;
	}

	return true;
}

int HDR::WritePixels(FILE *fp, float *data, int numpixels)
{
	unsigned char rgbe[4];

	while (numpixels-- > 0)
	{
		Float2rgbe(rgbe,data[RGBE_DATA_RED], data[RGBE_DATA_GREEN],data[RGBE_DATA_BLUE]);
		data += RGBE_DATA_SIZE;

		if (fwrite(rgbe, sizeof(rgbe), 1, fp) < 1)
			return RgbeError(rgbe_write_error,NULL);
	}

	return true;
}

int HDR::ReadPixels(FILE *fp, float *data, int numpixels)
{
  unsigned char rgbe[4];

  while(numpixels-- > 0)
  {
    if (fread(rgbe, sizeof(rgbe), 1, fp) < 1)
      return RgbeError(rgbe_read_error,NULL);

    Rgbe2float(&data[RGBE_DATA_RED],&data[RGBE_DATA_GREEN],
	       &data[RGBE_DATA_BLUE],rgbe);

    data += RGBE_DATA_SIZE;
  }

  return true;
}

int HDR::ReadPixels_Raw(FILE *fp, unsigned char *data, int numpixels)
{
	if (fread(data, 4, numpixels, fp) < numpixels)
		return RgbeError(rgbe_read_error,NULL);

	return true;
}

int HDR::WriteBytes_RLE(FILE *fp, unsigned char *data, int numbytes)
{
#define MINRUNLENGTH 4
	int cur, beg_run, run_count, old_run_count, nonrun_count;
	unsigned char buf[2];

	cur = 0;
	while(cur < numbytes)
	{
		beg_run = cur;
		/* find next run of length at least 4 if one exists */
		run_count = old_run_count = 0;

		while((run_count < MINRUNLENGTH) && (beg_run < numbytes))
		{
			beg_run += run_count;
			old_run_count = run_count;
			run_count = 1;

			while( (beg_run + run_count < numbytes) && (run_count < 127)
					&& (data[beg_run] == data[beg_run + run_count]))
				run_count++;
		}

		/* if data before next big run is a short run then write it as such */
		if ((old_run_count > 1)&&(old_run_count == beg_run - cur))
		{
			buf[0] = 128 + old_run_count;   /*write short run*/
			buf[1] = data[cur];

			if (fwrite(buf,sizeof(buf[0])*2,1,fp) < 1)
				return RgbeError(rgbe_write_error,NULL);

			cur = beg_run;
		}

		/* write out bytes until we reach the start of the next run */
		while(cur < beg_run)
		{
			nonrun_count = beg_run - cur;

			if (nonrun_count > 128)
				nonrun_count = 128;

			buf[0] = nonrun_count;

			if (fwrite(buf,sizeof(buf[0]),1,fp) < 1)
				return RgbeError(rgbe_write_error,NULL);

			if (fwrite(&data[cur],sizeof(data[0])*nonrun_count,1,fp) < 1)
				return RgbeError(rgbe_write_error,NULL);

			cur += nonrun_count;
		}

		/* write out next run if one was found */
		if (run_count >= MINRUNLENGTH)
		{
			buf[0] = 128 + run_count;
			buf[1] = data[beg_run];

			if (fwrite(buf,sizeof(buf[0])*2,1,fp) < 1)
				return RgbeError(rgbe_write_error,NULL);

			cur += run_count;
		}
	}

	return true;
#undef MINRUNLENGTH
}

int HDR::WritePixels_RLE(FILE *fp, float *data, int scanline_width,
			 int num_scanlines)
{
	unsigned char rgbe[4];
	unsigned char *buffer;
	int i, err;

	/* run length encoding is not allowed so write flat*/
	if ((scanline_width < 8)||(scanline_width > 0x7fff))
		return WritePixels(fp,data,scanline_width*num_scanlines);

	buffer = (unsigned char *)malloc(sizeof(unsigned char)*4*scanline_width);

	/* no buffer space so write flat */
	if (buffer == NULL)
		return WritePixels(fp,data,scanline_width*num_scanlines);

	while(num_scanlines-- > 0)
	{
		rgbe[0] = 2;
		rgbe[1] = 2;
		rgbe[2] = scanline_width >> 8;
		rgbe[3] = scanline_width & 0xFF;
		if (fwrite(rgbe, sizeof(rgbe), 1, fp) < 1)
		{
			free(buffer);
			return RgbeError(rgbe_write_error,NULL);
		}

		for(i=0;i<scanline_width;i++)
		{
			Float2rgbe(rgbe,data[RGBE_DATA_RED],
				data[RGBE_DATA_GREEN],data[RGBE_DATA_BLUE]);
			buffer[i] = rgbe[0];
			buffer[i+scanline_width] = rgbe[1];
			buffer[i+2*scanline_width] = rgbe[2];
			buffer[i+3*scanline_width] = rgbe[3];
			data += RGBE_DATA_SIZE;
		}
		/* write out each of the four channels separately run length encoded */
		/* first red, then green, then blue, then exponent */
		for(i=0;i<4;i++)
		{
			if ((err = WriteBytes_RLE(fp,&buffer[i*scanline_width],
							scanline_width)) != true)
			{
				free(buffer);
				return err;
			}
		}
	}

	free(buffer);
	return true;
}

int HDR::ReadPixels_RLE(FILE *fp, float *data, int scanline_width,
			int num_scanlines)
{
	unsigned char rgbe[4], *scanline_buffer, *ptr, *ptr_end;
	int i, count;
	unsigned char buf[2];

	/* run length encoding is not allowed so read flat*/
	if ((scanline_width < 8)||(scanline_width > 0x7fff))
		return ReadPixels(fp,data,scanline_width*num_scanlines);

	scanline_buffer = NULL;

	/* read in each successive scanline */
	while(num_scanlines > 0)
	{
		if (fread(rgbe,sizeof(rgbe),1,fp) < 1)
		{
			free(scanline_buffer);
			return RgbeError(rgbe_read_error,NULL);
		}

		if ((rgbe[0] != 2)||(rgbe[1] != 2)||(rgbe[2] & 0x80))
		{
			/* this file is not run length encoded */
			Rgbe2float(&data[0],&data[1],&data[2],rgbe);
			data += RGBE_DATA_SIZE;
			free(scanline_buffer);
			return ReadPixels(fp,data,scanline_width*num_scanlines-1);
		}

		if ((((int)rgbe[2])<<8 | rgbe[3]) != scanline_width)
		{
			free(scanline_buffer);
			return RgbeError(rgbe_format_error,"wrong scanline width");
		}

		if (scanline_buffer == NULL)
			scanline_buffer = (unsigned char *)	malloc(sizeof(unsigned char)*4*scanline_width);

		if (scanline_buffer == NULL)
			return RgbeError(rgbe_memory_error,"unable to allocate buffer space");

		ptr = &scanline_buffer[0];

		/* read each of the four channels for the scanline into the buffer */
		for(i=0;i<4;i++)
		{
			ptr_end = &scanline_buffer[(i+1)*scanline_width];
			while(ptr < ptr_end)
			{
				if (fread(buf,sizeof(buf[0])*2,1,fp) < 1)
				{
					free(scanline_buffer);
					return RgbeError(rgbe_read_error,NULL);
				}
				if (buf[0] > 128)
				{
					/* a run of the same value */
					count = buf[0]-128;
					if ((count == 0)||(count > ptr_end - ptr))
					{
						free(scanline_buffer);
						return RgbeError(rgbe_format_error,"bad scanline data");
					}
					while(count-- > 0)
						*ptr++ = buf[1];
				}
				else
				{
					/* a non-run */
					count = buf[0];
					if ((count == 0)||(count > ptr_end - ptr))
					{
						free(scanline_buffer);
						return RgbeError(rgbe_format_error,"bad scanline data");
					}

					*ptr++ = buf[1];
					if (--count > 0)
					{
						if (fread(ptr,sizeof(*ptr)*count,1,fp) < 1)
						{
							free(scanline_buffer);
							return RgbeError(rgbe_read_error,NULL);
						}
						ptr += count;
					}
				}
			}
		}

		/* now convert data from buffer into floats */
		for(i=0;i<scanline_width;i++)
		{
			rgbe[0] = scanline_buffer[i];
			rgbe[1] = scanline_buffer[i+scanline_width];
			rgbe[2] = scanline_buffer[i+2*scanline_width];
			rgbe[3] = scanline_buffer[i+3*scanline_width];
			Rgbe2float(&data[RGBE_DATA_RED],&data[RGBE_DATA_GREEN],
			&data[RGBE_DATA_BLUE],rgbe);
			data += RGBE_DATA_SIZE;
		}
		num_scanlines--;
	}

	free(scanline_buffer);
	return true;
}


int HDR::ReadPixels_Raw_RLE(FILE *fp, unsigned char *data, int scanline_width,
			int num_scanlines)
{
	unsigned char rgbe[4], *scanline_buffer, *ptr, *ptr_end;
	int i, count;
	unsigned char buf[2];

	/* run length encoding is not allowed so read flat*/
	if ((scanline_width < 8)||(scanline_width > 0x7fff))
		return ReadPixels_Raw(fp,data,scanline_width*num_scanlines);

	scanline_buffer = NULL;

	/* read in each successive scanline */
	while(num_scanlines > 0)
	{
		if (fread(rgbe,sizeof(rgbe),1,fp) < 1)
		{
			free(scanline_buffer);
			return RgbeError(rgbe_read_error,NULL);
		}

		if ((rgbe[0] != 2)||(rgbe[1] != 2)||(rgbe[2] & 0x80))
		{
			/* this file is not run length encoded */
			data[0] = rgbe[0];
			data[1] = rgbe[1];
			data[2] = rgbe[2];
			data[3] = rgbe[3];
			data += RGBE_DATA_SIZE;
			free(scanline_buffer);
			return ReadPixels_Raw(fp,data,scanline_width*num_scanlines-1);
		}

		if ((((int)rgbe[2])<<8 | rgbe[3]) != scanline_width)
		{
			free(scanline_buffer);
			return RgbeError(rgbe_format_error,"wrong scanline width");
		}

		if (scanline_buffer == NULL)
			scanline_buffer = (unsigned char *) malloc(sizeof(unsigned char)*4*scanline_width);

		if (scanline_buffer == NULL)
			return RgbeError(rgbe_memory_error,"unable to allocate buffer space");

		ptr = &scanline_buffer[0];
		/* read each of the four channels for the scanline into the buffer */
		for(i=0;i<4;i++)
		{
			ptr_end = &scanline_buffer[(i+1)*scanline_width];
			while(ptr < ptr_end)
			{
				if (fread(buf,sizeof(buf[0])*2,1,fp) < 1)
				{
					free(scanline_buffer);
					return RgbeError(rgbe_read_error,NULL);
				}

				if (buf[0] > 128)
				{
					/* a run of the same value */
					count = buf[0]-128;
					if ((count == 0)||(count > ptr_end - ptr))
					{
						free(scanline_buffer);
						return RgbeError(rgbe_format_error,"bad scanline data");
					}

					while(count-- > 0)
						*ptr++ = buf[1];
				}
				else
				{
					/* a non-run */
					count = buf[0];
					if ((count == 0)||(count > ptr_end - ptr))
					{
						free(scanline_buffer);
						return RgbeError(rgbe_format_error,"bad scanline data");
					}

					*ptr++ = buf[1];
					if (--count > 0)
					{
						if (fread(ptr,sizeof(*ptr)*count,1,fp) < 1)
						{
							free(scanline_buffer);
							return RgbeError(rgbe_read_error,NULL);
						}
						ptr += count;
					}
				}
			}
		}

		/* copy byte data to output */
		for(i=0;i<scanline_width;i++)
		{
			data[0] = scanline_buffer[i];
			data[1] = scanline_buffer[i+scanline_width];
			data[2] = scanline_buffer[i+2*scanline_width];
			data[3] = scanline_buffer[i+3*scanline_width];
			data += 4;
		}
		num_scanlines--;
	}
	free(scanline_buffer);
	return true;
}
