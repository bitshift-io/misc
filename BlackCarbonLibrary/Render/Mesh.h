#ifndef __MESH_H_
#define __MESH_H_

#include "SceneObject.h"
#include "Math/Matrix.h"

class Material;
class Geometry;
class RenderBuffer;

enum MeshFlags
{
	MF_None				= 0,
	MF_NonRenderable	= 1 << 0,
};

struct MeshMat
{
	MeshMat() : material(0), geometry(0)				{}
	MeshMat(Material* _material, Geometry* _geometry)	{ material = _material; geometry = _geometry; }

	Material*	material;
	Geometry*	geometry;
};

struct ReferencePoint
{
	string		mName;
	Matrix4		mTransform;
};

//
// The unification of geometry, transform, material, animation
//
class Mesh : public SceneObject
{
	friend class RenderFactory;

public:

	TYPE(7)

	typedef	vector<MeshMat>::iterator	MeshMatIterator;

	Mesh();

	virtual bool		Save(const File& out, SceneNode* node);
	virtual bool		Load(const ObjectLoadParams* params, SceneNode* node);

	virtual SceneObject*	Clone(SceneNode* owner = 0);

	unsigned int		GetNumMeshMat()						{ return (unsigned int)mMeshMat.size(); }
	MeshMat&			GetMeshMat(unsigned int idx)		{ return mMeshMat[idx]; }

	MeshMatIterator		Begin()								{ return mMeshMat.begin(); }
	MeshMatIterator		End()								{ return mMeshMat.end(); }

	virtual bool		Render(Device* device, unsigned int meshMatIdx, int vertexFormat);

	void				AddMeshMat(const MeshMat& meshMat)	{ mMeshMat.push_back(meshMat); }

	unsigned int		GetFlags()							{ return mFlags; }
	void				SetFlags(unsigned int flags)		{ mFlags |= flags; }
	void				ClearFlags(unsigned int flags)		{ mFlags &= ~flags; }

	void				Debug(Device* device, const Matrix4& world);

	virtual bool		GetLocalBoundBox(Box& box);
	void				SetLocalBoundBox(Box& box);

	virtual void		AddReference();
	virtual bool		ReleaseReference();

protected:

	virtual void		Destroy(RenderFactory* factory);

	vector<MeshMat>		mMeshMat;
	unsigned int		mFlags;
	string				mName;
	Box					mBoundBox;
	RenderFactory*		mFactory;
	Device*				mDevice;
};

#endif
