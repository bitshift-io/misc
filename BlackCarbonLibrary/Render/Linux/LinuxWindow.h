#ifndef _LINUXWINDOW_H_
#define _LINUXWINDOW_H_

#ifdef UNIX

//#define GLX_GLXEXT_LEGACY
/*
#include <stdio.h>
#include <GL/glx.h>
#include <GL/gl.h>
#include <GL/glu.h>
//#include <X11/extensions/xf86vmode.h>
//#include <X11/keysym.h>
*/
#include <stdio.h>
#include <stdlib.h>

#include <GL/gl.h>

namespace Linux
{
    #include <GL/glx.h>    /* this includes the necessary X headers */


    #include <X11/X.h>    /* X11 constant (e.g. TrueColor) */
    #include <X11/keysym.h>
};

#include "../Window.h"

class LinuxWindow : public Window
{
	friend class OGLDevice;

public:

	virtual bool	Init(const char* title, int width, int height, bool fullscreen = false, int bits = 32);
	virtual void	Deinit();
	virtual bool	Show();

	// returns true if quit message recived
	bool			HandleMessages();

	virtual bool	IsFullScreen()					{ return mFullScreen; }

	virtual int		GetWidth() const				{ return mWidth; }
	virtual int		GetHeight()	const				{ return mHeight; }

	virtual bool	GetCursorPosition(int& x, int& y);
	virtual int		SetCursorVisibility(bool visible);
	virtual bool	IsCursorVisible();
	virtual void	CenterCursor();

	virtual bool	IsResolutionSupported(int width, int height, int bits);
	virtual bool	HasFocus();

	virtual const char*		GetTitle();

//protected:

	bool			mExit;
	unsigned int	mWidth;
	unsigned int	mHeight;
	bool			mFocus;
	bool			mFullScreen;
	string			mTitle;

	Linux::Display*   mDisplay;
    Linux::Window     mWindow;
};

#endif

#endif
