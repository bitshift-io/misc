#ifndef _GEOMETRY_H_
#define _GEOMETRY_H_

#include "SceneObject.h"
#include "RenderBuffer.h"
#include "Device.h"

//
// vertex frame
// Geometry can be animated, so we need vertex frames
//
class VertexFrame
{
public:

	VertexFrame();

	virtual void	Init(Device* device, int vertexFormat, int numVerts = -1, RenderBufferUseage useage = RBU_Static);
	virtual void	Deinit(Device* device);

	virtual bool	Save(const File& out);
	virtual bool	Load(Device* device, const File& in);

	int				GetVertexFormat() const;

	RenderBuffer*			mPosition;
	RenderBuffer*			mNormal;
	RenderBuffer*			mTangent;
	RenderBuffer*			mBinormal;
	RenderBuffer*			mTexCoord[MAX_TEXCOORD];
	RenderBuffer*			mColour;
	RenderBuffer*			mBoneWeight;
	RenderBuffer*			mBoneIndex;
};

//
// Geometry buffer
//
class Geometry : public SceneObject
{
public:

	TYPE(0)

	virtual bool	Save(const File& out, SceneNode* node);
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node);

	//
	// Init this geometry customly
	//
	void			Init(Device* device, unsigned int vertexFormat, unsigned int numVerts, unsigned int numIndices, RenderBufferUseage useage, unsigned int numVertexFrame = 1);

	void			Init(Device* device, unsigned int vertexFormat, unsigned int numVertexFrame = 1);

	unsigned int	GetVertexFrameCount() const;
	VertexFrame&	GetVertexFrame(unsigned int frameIdx = 0);

	RenderBuffer*	GetIndexBuffer()								{ return mIndex; }

	void				SetTopology(GeometryTopology topology)	{ mTopology = topology; }
	GeometryTopology	GetTopology()							{ return mTopology; }

	void			Draw(unsigned int startIndex = 0, unsigned int numIndices = -1, int instanceCount = DT_NotInstanced);

protected:

	virtual void	Destroy(RenderFactory* factory);

	GeometryTopology		mTopology;

	RenderBuffer*			mIndex;
	vector<VertexFrame>		mVertexFrame;
	Device*					mDevice;
};

#endif
