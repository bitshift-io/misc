#include "Camera.h"
#include "Device.h"

Camera::Camera() :
	mView(MI_Identity),
	mWorld(MI_Identity),
	mViewDirty(false),
	mFrustumDirty(false),
	mWidth(0.f),
	mHeight(0.f)
{
	SetPerspective();
}

bool Camera::Save(const File& out, SceneNode* node)												
{ 
	// write out what we need
	out.Write(&mFOV, sizeof(float));
	out.Write(&mAspect, sizeof(float));
	out.Write(&mNearPlane, sizeof(float));
	out.Write(&mFarPlane, sizeof(float));
	out.Write(&mWorld, sizeof(Matrix4));
	return true; 
}

bool Camera::Load(const ObjectLoadParams* params, SceneNode* node)	
{ 
	File* in = params->mFile;

	// load in what we need
	in->Read(&mFOV, sizeof(float));
	in->Read(&mAspect, sizeof(float));
	in->Read(&mNearPlane, sizeof(float));
	in->Read(&mFarPlane, sizeof(float));
	in->Read(&mWorld, sizeof(Matrix4));

	SetPerspective(mFOV, mNearPlane, mFarPlane, mAspect);
	mViewDirty = true;
	GetView();
	return true; 
}

void Camera::Bind(Device* device)
{
	device->SetMatrix(GetProjection(), MM_Projection);
	device->SetMatrix(GetView(), MM_View);
}

const Matrix4& Camera::GetView()
{ 
	if (mViewDirty)
	{
		mView.CreateView(mWorld);
		mViewDirty = false;
	}

	return mView; 
}

const Frustum& Camera::GetFrustum()
{
	if (mFrustumDirty)
	{
		mFrustum.Init(GetWorld(), mFOV, mAspect, mNearPlane, mFarPlane);
		mFrustumDirty = false;
	}

	return mFrustum;
}

void Camera::SetOrthographic(float width, float height, float znear, float zfar)
{
	mFarPlane = zfar;
	mNearPlane = znear;
	mAspect = (float)width / (float)height;
	mWidth = width;
	mHeight =  height;

	mFrustumDirty = true;
	mPerpective = false;
	mProjection.SetOrthographic(-width * 0.5f, width * 0.5f, height * 0.5f, -height * 0.5f, znear, zfar);
}

void Camera::SetPerspective(float fovy, float znear, float zfar, float aspect)
{
	mFarPlane = zfar;
	mNearPlane = znear;
	mFOV = fovy;
	mAspect = aspect;

	mFrustumDirty = true;
	mPerpective = true;
	mProjection.SetPerspective(fovy, aspect, znear, zfar);
}

void Camera::SetPosition(const Vector4& position)
{
	mFrustumDirty = true;
	mViewDirty = true;
	mWorld.SetTranslation(position);
}

const Vector4& Camera::GetPosition() const
{
	return *(Vector4*)&mWorld.Column(3);
}

void Camera::LookAt(const Vector4& view, const Vector4& up, const Vector4& right)
{
	mFrustumDirty = true;
	mViewDirty = true;
	mWorld.Column(0) = right;
	mWorld.Column(1) = up;
	mWorld.Column(2) = view;
}

void Camera::LookAt(const Matrix4& view)
{
	mFrustumDirty = true;
	mViewDirty = true;
	mWorld = view;
}

void Camera::LookAt(const Vector4& pos, const Vector4& target)
{
	mFrustumDirty = true;
	mViewDirty = true;

	mWorld.CreateLookAt(pos, target);
	/*
	Vector4 up = Vector4(0.f, 1.f, 0.f);
	Vector4 view = target - pos;
	view.Normalize();

	Vector4 right = view.Cross(up);
	right.Normalize();

	up = right.Cross(view);
	up.Normalize();

	mWorld.Column(0) = right;
	mWorld.Column(1) = up;
	mWorld.Column(2) = view;
	mWorld.SetTranslation(pos);*/
}

void Camera::Draw(Device* device)
{
	device->DrawMatrix(mWorld); //Matrix4(MI_Identity));
/*
	const Vector4& position = GetPosition();
	device->DrawLine(position, position + mView.GetZAxis());
	device->DrawLine(position, position + Vector4(0.f, 0.5f, 0.f));*/
}

void Camera::SetAspectRatio(float aspect)
{
	if (aspect == mAspect)
		return;

	mAspect = aspect;

	if (mPerpective)
		SetPerspective(mFOV, mNearPlane, mFarPlane, aspect);
	else
		mProjection.SetOrthographic(-mWidth * 0.5f, mWidth * 0.5f, mHeight * 0.5f, -mHeight * 0.5f, mNearPlane, mFarPlane);
}