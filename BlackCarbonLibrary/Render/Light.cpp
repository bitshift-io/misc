#include "Light.h"
#include "Device.h"

bool Light::Save(const File& out, SceneNode* node)
{
	out.Write(&mDecayType, sizeof(ELightDecayType));
	out.Write(&mDiffuse, sizeof(Vector4));
	out.Write(&mType, sizeof(ELightType));
	out.Write(&mBrightness, sizeof(float));
	out.Write(&mCastShadow, sizeof(bool));
	out.Write(&mEnabled, sizeof(bool));
	out.Write(&mPosition, sizeof(Vector4));
	out.Write(&mDirection, sizeof(Vector4));
	return true;
}

bool Light::Load(const ObjectLoadParams* params, SceneNode* node)
{
	File* in = params->mFile;

	in->Read(&mDecayType, sizeof(ELightDecayType));
	in->Read(&mDiffuse, sizeof(Vector4));
	in->Read(&mType, sizeof(ELightType));
	in->Read(&mBrightness, sizeof(float));
	in->Read(&mCastShadow, sizeof(bool));
	in->Read(&mEnabled, sizeof(bool));
	in->Read(&mPosition, sizeof(Vector4));
	in->Read(&mDirection, sizeof(Vector4));
	return true;
}

bool Light::InRange(const Vector4& position)
{
	return true;
}

void Light::Draw(Device* device)
{
	device->DrawLine(mPosition, mPosition + mDirection);
	device->DrawLine(mPosition, mPosition + Vector4(0.f, 0.5f, 0.f));
}