#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "SceneObject.h"
#include "Math/Matrix.h"
#include "Math/Frustum.h"

//
// Camera - is this really savable?
//
class Camera : public SceneObject
{
public:

	TYPE(1)

	Camera();

	virtual bool		Save(const File& out, SceneNode* node);
	virtual bool		Load(const ObjectLoadParams* params, SceneNode* node);

	void				Bind(Device* device);

	void				SetOrthographic(float width = 2.f, float height = 2.f, float znear = 0.f, float zfar = 1.f);
	void				SetPerspective(float fovy = 90.0f, float znear = 0.01f, float zfar = 1000.0f, float aspect = 1.33f);

	void				SetPosition(const Vector4& position);
	const Vector4&		GetPosition() const;

	void				LookAt(const Vector4& view, const Vector4& up, const Vector4& right);
	void				LookAt(const Vector4& pos, const Vector4& target);
	void				LookAt(const Matrix4& view);

	const Matrix4&		GetProjection() const												{ return mProjection; }
	const Matrix4&		GetView();
	const Matrix4&		GetWorld() const													{ return mWorld; }

	const Frustum&		GetFrustum();

	// only for debugging please
	void				Draw(Device* device);

	void				SetAspectRatio(float aspect);
	virtual void		ReloadResource(RenderFactory* factory, Device* device)				{ mViewDirty = true; mAspect = 0.f; }

	float				GetFOV()															{ return mFOV; }
	float				GetFarPlane()														{ return mFarPlane; }
	float				GetNearPlane()														{ return mNearPlane; }
	float				GetAspectRatio()													{ return mAspect; }

protected:

	Frustum		mFrustum;
	bool		mFrustumDirty;

	Matrix4		mProjection;
	Matrix4		mWorld;
	Matrix4		mView;
	bool		mViewDirty;

	float		mFarPlane;
	float		mNearPlane;
	float		mFOV;
	float		mAspect;
	float		mWidth;
	float		mHeight;
	bool		mPerpective;
};

#endif
