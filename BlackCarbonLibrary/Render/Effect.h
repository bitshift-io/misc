#ifndef __EFFECT_H_
#define __EFFECT_H_

#include "SceneObject.h"
#include "Math/Vector.h"
#include "Math/Matrix.h"

class Device;
class Texture;
class Light;
class Projector;
class Effect;
class EffectParameter;
class Camera;

typedef void* EffectParameterHandle;
typedef void* EffectTechniqueHandle;

//
// A list of engine known parameter semantic's to save doing string compares
//
enum EffectParameterID
{
	// Matrix
	EPID_Projection,
	EPID_ProjectionInverse,
	EPID_ViewProjectionInverse,
	EPID_World,
	EPID_WorldInverse,
	EPID_WorldInverseTranspose,
	EPID_Camera, // camera world
	EPID_View,
	EPID_ViewInverseTranspose,
	EPID_ViewInverse,
	EPID_ViewTranspose,
	EPID_ViewProjection,
	EPID_WorldView,
	EPID_WorldViewInverseTranspose,
	EPID_WorldViewInverse,
	EPID_WorldViewProjection,
	EPID_WorldViewProjectionInverseTranspose,
	EPID_WorldViewProjectionInverse,

	// Skin
	EPID_SkinPalette,

	// Light
	EPID_LightPosition,
	EPID_LightDirection,
	EPID_LightType,				// 0 - omni, 1 - spot, 2 - directional
	EPID_LightDiffuseColour,
	EPID_LightDecayType,		// 0 - none, 1- inverse, 2- inverse square
	EPID_LightFalloff,

	// Misc
	EPID_Time,

	// Structures
	EPID_Light,
	EPID_LightCount,

	EPID_ProjectorTexture,
	EPID_Projector,
	EPID_ProjectorCount,

	EPID_InstanceData,

	// Unknown
	EPID_Unknown,

	EPID_Max,
};

//
// the data type
//
enum EffectParameterType
{
	EPT_Texture,
	EPT_Texture2D,
	EPT_TextureCube,
	EPT_Texture3D,
	EPT_Float,
	EPT_Vector4,
	EPT_Int,
	EPT_Matrix4,
	EPT_Struct,
	EPT_Unknown,
};

class EffectParameterValue
{
public:

	EffectParameterValue(EffectParameter* param);

	bool				GetValue(Matrix4* value, unsigned int count = 1);
	bool				GetValue(float* value, unsigned int count = 1);
	bool				GetValue(Texture** value, unsigned int count = 1);
	bool				GetValue(Vector4* value, unsigned int count = 1);
	bool				GetValue(int* value, unsigned int count = 1);
	bool				GetValue(void* value, unsigned int size);

	bool				SetValue(const Matrix4* value, unsigned int count = 1);
	bool				SetValue(const float* value, unsigned int count = 1);
	bool				SetValue(Texture** value, unsigned int count = 1);
	bool				SetValue(const Vector4* value, unsigned int count = 1);
	bool				SetValue(const int* value, unsigned int count = 1);
	bool				SetValue(const void* value, unsigned int size);

	bool				SetValue(EffectParameterValue* value);

	EffectParameter*	GetEffectParameter() const				{ return mEffectParameter; }

	// need a better way to resolve render targets :-/
	const string&		GetUserString()							{ return mUserString; }
	void				SetUserString(const char* str)			{ mUserString = str; }

	bool				Equals(EffectParameterValue& rhs);

	bool				IsDirty()								{ return mDirty; }

	void				Apply();

	unsigned int		GetTypeSize(EffectParameterType type);
	unsigned int		GetSize()								{ return mSize; }

protected:

	void				SetSize(unsigned int size);

	string					mUserString;			// for identifying render targets in the case of materials
	void*					mData;
	unsigned int			mSize;
	EffectParameterType		mType;

	EffectParameter*		mEffectParameter;
	bool					mDirty;
};

class EffectParameter
{
public:

	EffectParameter() : mData(this)
	{		
	}

	bool				GetValue(Matrix4* value, unsigned int count = 1)				{ return mData.GetValue(value, count); }
	bool				GetValue(float* value, unsigned int count = 1)					{ return mData.GetValue(value, count); }
	bool				GetValue(Texture** value, unsigned int count = 1)				{ return mData.GetValue(value, count); }
	bool				GetValue(Vector4* value, unsigned int count = 1) 				{ return mData.GetValue(value, count); }
	bool				GetValue(int* value, unsigned int count = 1) 					{ return mData.GetValue(value, count); }
	bool				GetValue(void* value, unsigned int size)						{ return mData.GetValue(value, size); }

	bool				SetValue(const Matrix4* value, unsigned int count = 1) 			{ return mData.SetValue(value, count); }
	bool				SetValue(const float* value, unsigned int count = 1) 			{ return mData.SetValue(value, count); }
	bool				SetValue(Texture** value, unsigned int count = 1) 				{ return mData.SetValue(value, count); }
	bool				SetValue(const Vector4* value, unsigned int count = 1) 			{ return mData.SetValue(value, count); }
	bool				SetValue(const int* value, unsigned int count = 1) 				{ return mData.SetValue(value, count); }
	bool				SetValue(const void* value, unsigned int size)					{ return mData.SetValue(value, size); }

	bool				SetValue(EffectParameterValue* value)							{ return mData.SetValue(value); }

	void				Apply();

	unsigned int		GetDataSize()													{ return mData.GetSize(); }

//protected:

	EffectParameterHandle		mHandle;
	EffectParameterType			mType;
	EffectParameterID			mID;
	string						mName;
	string						mSemantic;
	unsigned int				mBufferOffset;
	unsigned int				mSize;			// size
	unsigned int				mStructOffset;	// offset from start of parent struct
	unsigned int				mArraySize;
	Effect*						mEffect;
	EffectParameterValue		mData;
	vector<EffectParameter*>	mChild; // for structures
};

class EffectTechnique
{
public:

	int						GetVertexFormat()	{ return mVertexFormat; }

	EffectTechniqueHandle	mHandle;
	string					mName;
	int						mVertexFormat;
	Effect*					mEffect;
	bool					mDirty;				// indicates if the current technique has changed
};


class Effect : public SceneObject
{
	friend class EffectParameter;

public:

	TYPE(9)

	Effect() :
		mCurrentTechnique(0)
	{
	}

	typedef vector<EffectParameter*>::iterator EffectParameterIterator;
	typedef vector<EffectTechnique*>::iterator EffectTechniqueIterator;

	virtual bool	Save(const File& out, SceneNode* node = 0)										{ return true; }
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node = 0)						= 0;

	virtual int		Begin() = 0;
	virtual void	BeginPass(int idx) = 0;
	virtual void	SetPassParameter();
	virtual void	EndPass() = 0;
	virtual void	End() = 0;


	//
	// Calulate a matrix using the EffectParameterID, returns identity if it fails
	//
	virtual Matrix4	CalculateMatrix(EffectParameterID id, const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world);
	virtual void	SetMatrixParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world);
	virtual bool	SetProjectorParameters(Camera** camera, unsigned int numCamera, const Matrix4& world);
/*
	virtual void	SetProjectorParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world) = 0;
	virtual void	SetMatrixParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world) = 0;
	virtual void	SetLightParameters(Light* lights[], unsigned int numLights) = 0;
	virtual void	SetTimeParameters(float time) = 0;
*/
	virtual EffectParameter*		GetParameterBySemantic(const char* semantic);
	virtual EffectParameter*		GetParameterByName(const char* name);
	virtual EffectParameter*		GetParameterByID(EffectParameterID id, bool searchChildren);
	virtual EffectParameter*		GetParameterByID(EffectParameterID id, EffectParameter* effectParam, bool searchChildren);

	EffectParameterIterator			BeginParameter()		{ return mParameter.begin(); }
	EffectParameterIterator			EndParameter()			{ return mParameter.end(); }

	EffectTechniqueIterator			BeginTechnique()		{ return mTechnique.begin(); }
	EffectTechniqueIterator			EndTechnique()			{ return mTechnique.end(); }


	virtual EffectTechnique*		GetTechniqueByName(const char* name);
	virtual void					SetCurrentTechnique(EffectTechnique* technique);
	virtual EffectTechnique*		GetCurrentTechnique();

/*
	void			SetMatrixParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& model, const Vector4& eyePosition);
	void			SetLightParameters(Light* lights[], unsigned int numLights);
	void			SetProjectorParameters(const Matrix4& model, Projector* projectors[], unsigned int numProjectors);
	void			SetSkinParameters(const Matrix4 bones[], unsigned int numBones);
	void			SetTimeParameter(float time);* /
*/

	virtual EffectParameterValue*	CreateEffectParameterValue(EffectParameter* param);
	virtual void					ReleaseEffectParameterValue(EffectParameterValue** data);

	//
	// might be good to have this list expandable by the user!
	//
	int								GetParameterIDByName(const char* name);

	const char*						GetName()									{ return mName.c_str(); }
	virtual bool					IsInstance(const ObjectLoadParams* params, SceneObject* loaded);

protected:

	virtual void			SetValue(EffectParameter* param);
	virtual void			SetValue(EffectParameter* param, const Matrix4* value, unsigned int count = 1) = 0;
	virtual void			SetValue(EffectParameter* param, const float* value, unsigned int count = 1) = 0;
	virtual void			SetValue(EffectParameter* param, Texture** value, unsigned int count = 1) = 0;
	virtual void			SetValue(EffectParameter* param, const Vector4* value, unsigned int count = 1) = 0;
	virtual void			SetValue(EffectParameter* param, const int* value, unsigned int count = 1) = 0;
	virtual void			SetValue(EffectParameter* param, void* buffer, unsigned int size) = 0;


	vector<EffectTechnique*>	mTechnique;
	vector<EffectParameter*>	mParameter;

	EffectTechnique*			mCurrentTechnique;
	string						mName;

	RenderFactory*				mFactory;
	Device*						mDevice;
};

#endif
