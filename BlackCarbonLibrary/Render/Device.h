#ifndef _DEVICE_H_
#define _DEVICE_H_

#include "Math/Matrix.h"
#include "Math/Sphere.h"
#include "Math/Box.h"
#include "Math/Frustum.h"
#include "Math/Vector.h"

#include <string>
#include <vector>

using namespace std;

class Window;
class RenderBuffer;
class Geometry;
class Material;
class RenderTexture;
class RenderFactory;

enum GeometryTopology
{
	GT_PointList,
	GT_TriangleList,
	GT_TriangleListAdjacent,
};

enum MatrixMode
{
	MM_Model,
	MM_View,
	MM_Projection,
};

enum BlendMode
{
	BM_Zero,
	BM_One,
	BM_DestColour,
	BM_SrcColour,
	BM_OneMinusDestColour,
	BM_OneMinusSrcColour,
	BM_SrcAlpha,
	BM_OneMinusSrcAlpha,
	BM_DestAlpha,
	BM_OneMinusDestAlpha,
	BM_SrcAlphaSaturate,
};

enum Compare
{
	C_Less,
	C_LessEqual,
	C_Equal,
	C_Greater,
	C_GreaterEqual,
	C_Always, // TODO: fix device enums
	C_Never,
	C_NotEqual,
};

enum Operation
{
	O_Zero, // biatch!
	O_Keep,
	O_Replace,
	O_Increment,
	O_Decrement,
	O_Invert,
};

enum CullFace
{
	CF_Front,
	CF_Back,
	CF_None,
};

enum DrawType
{
	DT_NotInstanced = -1,
};

struct Viewport
{
	Viewport() :
		nearFarDepth(0.f, 1.f)
	{
	}

	Vector4 topLeft;
	Vector4 bottomRight;
	Vector4 nearFarDepth;
};

// TODO: have a clear it takes a ptr to a clear state
// and flags on what to clear
enum CleaFlags
{
	CF_Stencil,
	CF_Colour,
};

struct ClearState
{
	Vector4			clearColour;
	unsigned int	stencilClear;
	float			clearDepth;
};

struct DeviceState
{
	struct StencilFace
	{
		StencilFace() :
			compare(C_LessEqual),
			depthFail(O_Zero),
			fail(O_Zero),
			pass(O_Zero)
		{
		}

		Compare		compare;
		Operation	pass;
		Operation	fail;
		Operation	depthFail;			
	};

	DeviceState() :
		cullFace(CF_Back),
		depthTestEnable(true),
		depthWriteEnable(true),
		alphaBlendEnable(false),
		alphaBlendSource(BM_SrcAlpha),
		alphaBlendDest(BM_OneMinusSrcAlpha),
		depthCompare(C_LessEqual),
		alphaToCoverageEnable(false),
		stencilEnable(false),
		stencilReadMask(-1),
		stencilWriteMask(-1),
		topology(GT_TriangleList),
		vertexFormat(-1)
	{
	}

	// geometry
	int					vertexFormat;
	GeometryTopology	topology;
	CullFace			cullFace;

	// alpha
	bool				alphaToCoverageEnable;
	bool				alphaBlendEnable;
	BlendMode			alphaBlendSource;
	BlendMode			alphaBlendDest;

	// depth
	bool				depthTestEnable;
	bool				depthWriteEnable;
	Compare				depthCompare;

	// stencil
	bool				stencilEnable;
	unsigned int		stencilReadMask;
	unsigned int		stencilWriteMask;

	StencilFace			stencilFace[2];
};

struct DeviceDebugStats
{
	unsigned int			framesThisSecond;
	unsigned int			frameRate;
	unsigned int			frameTime;
	unsigned int			drawCallCount;
	unsigned int			polyCount;
};

// TODO: replace with ClearState, DeviceState and Viewport struct's
class Stencil
{
public:

	Stencil() :
		readMask(-1),
		writeMask(-1)
	{
	}

	unsigned int readMask;
	unsigned int writeMask;

	struct StencilFace
	{
		Compare		compare;
		Operation	pass;
		Operation	fail;
		Operation	depthFail;			
	};

	StencilFace face[2];
};

// so it compiles out of release :)
#ifdef DEBUG
	#define BEGINDEBUGEVENT(d, t, c)	d->BeginDebugEvent(t, c)
	#define ENDDEBUGEVENT(d)			d->EndDebugEvent()	
#else
	#define BEGINDEBUGEVENT(d, t, c)	{}
	#define ENDDEBUGEVENT(d)			{}
#endif

class Device
{
public:

	Device() :
		mFramesThisSecond(0),
		mFPS(0),
		mFrameTime(0),
		mDrawCallCount(0),
		mPolyCount(0)
	{
	}

	virtual bool			Init(RenderFactory* factory, Window* window, int multisample = 0)			= 0;
	virtual void			Deinit(RenderFactory* factory, Window* window)								= 0;

	virtual int				GetMultisample()									= 0;

	virtual void			Begin();
	virtual void			End()												= 0;

	virtual RenderBuffer*	CreateRenderBuffer()								= 0;
	virtual void			DestroyRenderBuffer(RenderBuffer** buffer)			= 0;

	//virtual void			Draw(int instanceCount, Material* material, Geometry* geometry, int vertexFormat = 0, unsigned int startIndex = 0, unsigned int numIndices = -1/*, vector<RenderBuffer*>* instanceBuffer = 0*/);

	virtual bool			Draw(Geometry* geometry, unsigned int startIndex = 0, unsigned int numIndices = -1, int instanceCount = DT_NotInstanced);

	virtual void			SetMatrix(const Matrix4& matrix, MatrixMode mode)	= 0;

	virtual void			DrawPoint(const Vector4& p, const Vector4& colour = Vector4(VI_One))							= 0;
	virtual void			DrawLine(const Vector4& p1, const Vector4& p2, const Vector4& colour = Vector4(VI_One))			= 0;
	virtual void			DrawMatrix(const Matrix4& matrix, float scale = 1.0f);
	virtual void			DrawSphere(const Sphere& sphere);
	virtual void			DrawBox(const Box& box);
	virtual void			DrawFrustum(const Frustum& frustum);

	virtual void			SetGeometryTopology(GeometryTopology topology)		= 0;

	virtual bool			SetDeviceState(const DeviceState& state, bool force = false)			= 0;
	virtual bool			GetDeviceState(DeviceState& state)					= 0;

	// for debugging the stencil buffer
	virtual void			DrawStencil()										= 0;
	virtual void			SaveStencil()										= 0;

	virtual Window*			GetWindow()											= 0;
	virtual void			ReloadResource()									= 0;

	unsigned int			GetFPS()											{ return mFPS; }
	unsigned int			GetDrawCallCount()									{ return mDrawCallCount; }
	unsigned int			GetPolyCount()										{ return mPolyCount; }

	// for debugging, pix in windows, some gl extension in gl
	virtual void			BeginDebugEvent(const string& name, const Vector4& colour)	{}
	virtual void			EndDebugEvent()												{}

	virtual Vector4			GetClearColour()									= 0;
	virtual void			SetClearColour(const Vector4& colour)				= 0;
	//virtual void			SetClearDepth(float depth)							= 0;
	//virtual void			SetClearStencil(unsigned int stencil)				= 0;

protected:

	// stats
	unsigned int			mFramesThisSecond;
	unsigned int			mFPS;
	unsigned int			mFrameTime;
	unsigned int			mDrawCallCount;
	unsigned int			mPolyCount;
};

#endif
