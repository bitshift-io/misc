#include "ParticleAction.h"
#include "ParticleSystem.h"
#include "Math/Quaternion.h"
#include "Math/Plane.h"
#include "Scene.h"
#include "File/Log.h"

ParticleBirth::ParticleBirth()
{
	Reset();
}

void ParticleBirth::Reset()
{
	mEmitCount = 0.f;
}

bool ParticleBirth::Save(const File& out, SceneNode* node)
{
	// write the type
	out.Write(&mType, sizeof(BirthType));

	// write times
	out.Write(&mStartTime, sizeof(float));
	out.Write(&mEndTime, sizeof(float));

	// write the
	switch (mType)
	{
	case BT_Rate:
		out.Write(&mRate, sizeof(float));
		break;

	case BT_Amount:
		out.Write(&mAmount, sizeof(float));
		break;
	}

	return true;
}

bool ParticleBirth::Load(const ObjectLoadParams* params, SceneNode* node)
{
	// read the type
	params->mFile->Read(&mType, sizeof(BirthType));

	// read times
	params->mFile->Read(&mStartTime, sizeof(float));
	params->mFile->Read(&mEndTime, sizeof(float));

	// write the
	switch (mType)
	{
	case BT_Rate:
		params->mFile->Read(&mRate, sizeof(float));
		break;

	case BT_Amount:
		params->mFile->Read(&mAmount, sizeof(float));
		break;
	}

	return true;
}

void ParticleBirth::Update(ParticleSystem* system)
{
	switch (mType)
	{
	case BT_Rate:
		{
			// if rate is set, we will continue to spawn particles for ever
			// unlike in max

			float delta = system->GetDeltaTime();
			mEmitCount += delta * mRate;
			while (mEmitCount >= 1.f)
			{
				system->AddParticle(mActionGroup);
				mEmitCount -= 1.f;
			}
		}
		break;

	case BT_Amount:
		{
			if (system->GetTime() < mStartTime || system->GetTime() > mEndTime)
				return;

			float delta = system->GetDeltaTime();
			float rate = mAmount / (mEndTime - mStartTime);
			int amount = (int)(delta * rate);
			amount = Math::Max(1, amount); // TODO: look in to a better way to do this

			// add new particles - we need to save ur state some where, in the container :-/
			for (int i = 0; i < amount; ++i)
				system->AddParticle(mActionGroup);

			//system->AddToIgnoreList(this);
		}
		break;
	}
}

ParticleAction* ParticleBirth::Clone(ParticleActionGroup* ownerGroup, SceneNode* owner)
{
	ParticleBirth* action = new ParticleBirth;
	memcpy(action, this, sizeof(ParticleBirth));
	action->mActionGroup = ownerGroup;
	action->Reset();
	return action;
}

// =====================================================

bool ParticleDeath::Save(const File& out, SceneNode* node)
{
	out.Write(&mType, sizeof(DeathType));
	out.Write(&mLife, sizeof(float));
	out.Write(&mVariation, sizeof(float));
	return true;
}

bool ParticleDeath::Load(const ObjectLoadParams* params, SceneNode* node)
{
	params->mFile->Read(&mType, sizeof(DeathType));
	params->mFile->Read(&mLife, sizeof(float));
	params->mFile->Read(&mVariation, sizeof(float));
	return true;
}

bool ParticleDeath::Update(ParticleSystem* system, Particle* particle)
{
	switch (mType)
	{
	case DT_All:
		particle->mLife = -1.f;
		return false;

	case DT_Life:
		{
			if (particle->mLife > mLife)
			{
				particle->mLife = -1.f;
				return false;
			}
/*
			ParticleSystem::Iterator it;
			for (it = system->ExistingBegin(); it != system->ExistingEnd(); )
			{
				if ((*it)->mLife > mLife)
					it = system->Erase(it);
				else
					++it;
			}*/
		}
		break;
	}

	/*
	// iterate over old particles and kill them off if type != all
	switch (mType)
	{
	case DT_All:
		system->Clear();
		break;

	case DT_Life:
		{
			ParticleSystem::Iterator it;
			for (it = system->ExistingBegin(); it != system->ExistingEnd(); )
			{
				if ((*it)->mLife > mLife)
					it = system->Erase(it);
				else
					++it;
			}
		}
		break;
	}*/

	return true;
}

ParticleAction* ParticleDeath::Clone(ParticleActionGroup* ownerGroup, SceneNode* owner)
{
	ParticleDeath* action = new ParticleDeath;
	memcpy(action, this, sizeof(ParticleDeath));
	action->mActionGroup = ownerGroup;
	action->Reset();
	return action;
}

// =====================================================

bool ParticleSpeed::Save(const File& out, SceneNode* node)
{
	out.Write(&mDirection, sizeof(SpeedDirection));
	out.Write(&mSpeed, sizeof(float));
	out.Write(&mVariation, sizeof(float));
	out.Write(&mDivergence, sizeof(float));
	return true;
}

bool ParticleSpeed::Load(const ObjectLoadParams* params, SceneNode* node)
{
	params->mFile->Read(&mDirection, sizeof(SpeedDirection));
	params->mFile->Read(&mSpeed, sizeof(float));
	params->mFile->Read(&mVariation, sizeof(float));
	params->mFile->Read(&mDivergence, sizeof(float));
	return true;
}

void ParticleSpeed::Update(ParticleSystem* system)
{
	ParticleSystem::Iterator it;
	for (it = system->NewBegin(); it != system->NewEnd(); ++it)
	{
		const Matrix4& transform = mActionGroup->GetSceneNode()->GetWorldTransform();

		Vector4 direction;
		Vector4 right;
		Vector4 up;
		switch (mDirection)
		{
		case SD_Random:
			{
				Quaternion qRot(Vector4(VI_Up), Math::Rand(-Math::DtoR(180), Math::DtoR(180)));
				Quaternion qRot2(Vector4(1.f, 0.f, 0.f), Math::Rand(-Math::DtoR(180), Math::DtoR(180)));
				Quaternion qFinal = qRot2 * qRot;

				direction = qFinal * Vector4(0.f, 0.f, 1.f);
			}
			break;

		case SD_Horizontal:
			{
				Quaternion qRot(up, Math::Rand(-Math::DtoR(180), Math::DtoR(180)));
				direction = qRot * Vector4(0.f, 0.f, 1.f);
			}
			break;

		case SD_YAxis:
			{
				direction = transform.GetYAxis();
				//right = transform.GetXAxis();
				up = -transform.GetZAxis();
			}
			break;

		case SD_XAxis:
			{
				direction = transform.GetXAxis();
				//right = -transform.GetZAxis();
				up = transform.GetYAxis();
			}
			break;

		case SD_ZAxis:
			{
				direction = transform.GetZAxis();
				//right = transform.GetXAxis();
				up = transform.GetYAxis();
			}
			break;
		}

		// offset by variation and divergence
		Quaternion qRot(up, Math::Rand(-mDivergence, mDivergence)); // TODO: FIX ME HACK
		Quaternion qRot2(direction, Math::Rand(-Math::DtoR(180), Math::DtoR(180)));
		Quaternion qFinal = qRot2 * qRot;
		direction = qFinal * direction;
		(*it)->mVelocity += direction * (mSpeed + Math::Rand(-mVariation, mVariation));
		int nothing = 0;
	}
}

ParticleAction* ParticleSpeed::Clone(ParticleActionGroup* ownerGroup, SceneNode* owner)
{
	ParticleSpeed* action = new ParticleSpeed;
	memcpy(action, this, sizeof(ParticleSpeed));
	action->mActionGroup = ownerGroup;
	action->Reset();
	return action;
}

// =====================================================

ParticlePosition::ParticlePosition()
{
	Reset();
}

void ParticlePosition::Reset()
{
	mPreviousTransform[15] = -1.f;
}

bool ParticlePosition::Save(const File& out, SceneNode* node)
{
	out.Write(&mPositionType, sizeof(PositionType));
	out.Write(&mInheritSpeedMovementPercent, sizeof(float));
	return true;
}

bool ParticlePosition::Load(const ObjectLoadParams* params, SceneNode* node)
{
	params->mFile->Read(&mPositionType, sizeof(PositionType));
	params->mFile->Read(&mInheritSpeedMovementPercent, sizeof(float));
	mPreviousTransform = Matrix4(MI_Identity);
	return true;
}

void ParticlePosition::Update(ParticleSystem* system)
{
	const Matrix4& transform = mActionGroup->GetSceneNode()->GetWorldTransform();

	if (mPreviousTransform[15] < 0.f)
		mPreviousTransform = transform;

	ParticleSystem::Iterator it;
	for (it = system->NewBegin(); it != system->NewEnd(); ++it)
	{
		switch (mPositionType)
		{
		case PT_Centre:
			(*it)->mTransform = transform;
			break;

		default:
			Log::Warning("ParticlePosition mPositionType not yet supported\n");
			break;
		}

		if (mInheritSpeedMovementPercent != 0.f)
		{
			Vector4 deltaPos = transform.GetTranslation() - mPreviousTransform.GetTranslation();
			deltaPos /= system->GetDeltaTime();
			deltaPos *= mInheritSpeedMovementPercent;
			(*it)->mVelocity += deltaPos;
		}

		
	}

	mPreviousTransform = transform;
}

ParticleAction* ParticlePosition::Clone(ParticleActionGroup* ownerGroup, SceneNode* owner)
{
	ParticlePosition* action = new ParticlePosition;
	memcpy(action, this, sizeof(ParticlePosition));
	action->mActionGroup = ownerGroup;
	action->Reset();
	return action;
}

// =====================================================

bool ParticleForce::Save(const File& out, SceneNode* node)
{
	unsigned int count = mNode->GetName().size();
	out.Write(&count, sizeof(unsigned int));
	out.Write(mNode->GetName().c_str(), sizeof(char) * count);

	out.Write(&mType, sizeof(ForceType));
	out.Write(&mInfluence, sizeof(float));
	out.Write(&mStrength, sizeof(float));
	out.Write(&mDecay, sizeof(float));
	return true;
}

bool ParticleForce::Load(const ObjectLoadParams* params, SceneNode* node)
{
	// resolve node name to node
	char buffer[256];
	unsigned int count = 0;
	params->mFile->Read(&count, sizeof(unsigned int));
	params->mFile->Read(buffer, sizeof(char) * count);
	buffer[count] = '\0';
	mNode = node->GetOwner()->GetNodeByName(buffer);

	params->mFile->Read(&mType, sizeof(ForceType));
	params->mFile->Read(&mInfluence, sizeof(float));
	params->mFile->Read(&mStrength, sizeof(float));
	params->mFile->Read(&mDecay, sizeof(float));
	return true;
}

bool ParticleForce::Update(ParticleSystem* system, Particle* particle)
{
	Matrix4 worldMat = mNode->GetWorldTransform();

	switch (mType)
	{
	case FT_Directional:
		{
			float strength = mStrength;
			if (mDecay != 0.f)
			{
				Plane p(worldMat.GetTranslation(), worldMat.GetYAxis());
				float dist = fabsf(p.DistanceToPoint(particle->mTransform.GetTranslation()));

				float decayValue = (float)exp(-mDecay * dist);
				strength *= decayValue;
			}
			
			particle->mVelocity += worldMat.GetYAxis() * system->GetDeltaTime() * mInfluence * strength;
		}
		break;

	case FT_Point:
		{
			Vector4 vForceDir = worldMat.GetTranslation() - particle->mTransform.GetTranslation();
			float dist = vForceDir.Normalize();

			float strength = mStrength;			
			if (mDecay != 0.f)
			{
				float decayValue = (float)exp(-mDecay * dist);
				strength *= decayValue;
			}
			
			particle->mVelocity += vForceDir * system->GetDeltaTime() * mInfluence * strength;
		}
		break;
	}

	return true;
}

ParticleAction* ParticleForce::Clone(ParticleActionGroup* ownerGroup, SceneNode* owner)
{
	ParticleForce* action = new ParticleForce;
	memcpy(action, this, sizeof(ParticleForce));
	action->mActionGroup = ownerGroup;
	action->Reset();
	return action;
}

// =====================================================

bool ParticleWind::Save(const File& out, SceneNode* node)
{
	ParticleForce::Save(out, node);

	out.Write(&mTurbulence, sizeof(float));
	out.Write(&mFrequency, sizeof(float));
	out.Write(&mScale, sizeof(float));
	return true;
}

bool ParticleWind::Load(const ObjectLoadParams* params, SceneNode* node)
{
	ParticleForce::Load(params, node);

	params->mFile->Read(&mTurbulence, sizeof(float));
	params->mFile->Read(&mFrequency, sizeof(float));
	params->mFile->Read(&mScale, sizeof(float));
	return true;
}

bool ParticleWind::Update(ParticleSystem* system, Particle* particle)
{
	Matrix4 worldMat = mNode->GetWorldTransform();

	switch (mType)
	{
	case FT_Directional:
		{
			float strength = mStrength;
			if (mDecay != 0.f)
			{
				Plane p(worldMat.GetTranslation(), worldMat.GetYAxis());
				float dist = fabsf(p.DistanceToPoint(particle->mTransform.GetTranslation()));

				float decayValue = (float)exp(-mDecay * dist);
				strength *= decayValue;
			}
			
			Vector4 turbulence(VI_Zero);
			if (mTurbulence != 0.0f) 
			{
				#define METERS_TO_MAX	39.37f
				#define MAX_TO_METERS	1.0f / 39.37f

				float time = system->GetTime() * 4800;
				Vector4 vForceDir = (worldMat.GetTranslation() - particle->mTransform.GetTranslation()) * METERS_TO_MAX;
				Vector4 p;
				p    = vForceDir;
				p.x  = mFrequency * time;
				turbulence.x = Math::Noise3(p * mScale);
				p    = vForceDir;
				p.y  = mFrequency * time;
				turbulence.z = Math::Noise3(p * mScale); // swap z and y cuz max blows
				p    = vForceDir;
				p.z  = mFrequency * time;
				turbulence.y = Math::Noise3(p * mScale);

				turbulence *= mTurbulence;
			} 
			
			particle->mVelocity += ((worldMat.GetYAxis() * strength) + turbulence) * system->GetDeltaTime() * mInfluence;
		}
		break;

	case FT_Point:
		{
			Vector4 vForceDir = worldMat.GetTranslation() - particle->mTransform.GetTranslation();
			float dist = vForceDir.Normalize();

			float strength = mStrength;			
			if (mDecay != 0.f)
			{
				float decayValue = (float)exp(-mDecay * dist);
				strength *= decayValue;
			}
			
			Vector4 turbulence(VI_Zero);
			if (mTurbulence != 0.0f) 
			{
				#define METERS_TO_MAX	39.37f
				#define MAX_TO_METERS	1.0f / 39.37f

				float time = system->GetTime() * 4800;
				vForceDir *= METERS_TO_MAX;
				Vector4 p;
				p    = vForceDir;
				p.x  = mFrequency * time;
				turbulence.x = Math::Noise3(p * mScale);
				p    = vForceDir;
				p.y  = mFrequency * time;
				turbulence.z = Math::Noise3(p * mScale); // swap z and y cuz max blows
				p    = vForceDir;
				p.z  = mFrequency * time;
				turbulence.y = Math::Noise3(p * mScale);

				turbulence *= mTurbulence;
			} 
			
			particle->mVelocity += ((worldMat.GetYAxis() * strength) + turbulence) * system->GetDeltaTime() * mInfluence;
		}
		break;
	}

	return true;
}

ParticleAction* ParticleWind::Clone(ParticleActionGroup* ownerGroup, SceneNode* owner)
{
	ParticleWind* action = new ParticleWind;
	memcpy(action, this, sizeof(ParticleWind));
	action->mActionGroup = ownerGroup;
	action->Reset();
	return action;
}