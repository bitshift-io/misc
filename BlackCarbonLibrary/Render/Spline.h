#ifndef _SPLINE_H_
#define _SPLINE_H_

#include "SceneObject.h"
#include "Math/Spline.h"
#include <vector>

using namespace std;

class Spline : public SceneObject
{
	friend class RenderFactory;

public:

	TYPE(2)

	virtual bool	Save(const File& out, SceneNode* node);
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node);

	unsigned int	GetNumSpline()							{ return (unsigned int)mSpline.size(); }
	Spline3&		GetSpline(unsigned int idx)				{ return mSpline[idx]; }

	// only for debugging please
	void			Draw(Device* device);

	vector<Spline3>	mSpline;

protected:

	virtual void	Destroy(const RenderFactory* factory);
};

#endif
