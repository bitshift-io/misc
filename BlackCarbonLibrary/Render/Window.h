#ifndef _WINDOW_H_
#define _WINDOW_H_

#include <string>
#include <vector>

using namespace std;

enum MessageBoxFlags
{
	MBF_Ok,
	MBF_Cancel,
	MBF_Yes,
	MBF_No,
};

struct WindowResolution
{
	int width;
	int height;
	int bits;
};

class Window
{
public:

	typedef vector<WindowResolution>::iterator WindowResolutionIterator;

	virtual bool	Init(const char* title, const WindowResolution* resolution = 0, bool fullscreen = false) 	= 0;
	virtual void	Deinit()								= 0;
	virtual bool	Show()									= 0;

	// window resolution setting
	virtual WindowResolutionIterator	WindowResolutionBegin()		{ return mResolution.begin(); }
	virtual WindowResolutionIterator	WindowResolutionEnd()		{ return mResolution.end(); }

	virtual bool						SetWindowResolution(const WindowResolution* resolution) = 0;
	virtual bool						GetWindowResolution(WindowResolution& resolution) const	= 0;
	virtual bool						GetDesktopResolution(WindowResolution& resolution) const = 0;

	// returns true if quit message recived
	virtual bool	HandleMessages()						= 0;
	virtual bool	IsFullScreen()							= 0;

	float			GetAspectRatio() const
	{
		WindowResolution res;
		GetWindowResolution(res);
		return float(res.width) / float(res.height);
	}

	// returns cursor position relative to client area
	// returns true if in the client area
	virtual bool	SetCursorPosition(int x, int y)			= 0;
	virtual bool	GetCursorPosition(int& x, int& y)		= 0;
	virtual int		SetCursorVisibility(bool visible)		= 0;
	virtual bool	IsCursorVisible()						= 0;
	virtual void	CenterCursor()							= 0;

	virtual bool	IsResolutionSupported(int width, int height, int bits) = 0;
	virtual bool	HasFocus()								= 0;

	virtual const char*	GetTitle()						= 0;

	virtual int		MessageBox(const char* text, const char* title, int flags) = 0;

protected:

	vector<WindowResolution>	mResolution;
};

#endif
