#ifndef _SOFTWAREDEVICE_H_
#define _SOFTWAREDEVICE_H_

#ifdef WIN32
    #define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
    #define NOMINMAX
    #include <windows.h>
    #undef SendMessage
#endif

#include "Render/Device.h"
#include <list>
#include <GL/gl.h>
#include <GL/glu.h>

using namespace std;

struct StructInputLayout;
struct PixelInput;
class SoftwarePixelShader;
class RenderBuffer;
class ParameterData;
class SoftwareEffect;

#ifdef WIN32
	class Win32Window;
#else
	class LinuxWindow;
#endif

class SoftwareDevice : public Device
{
	friend class DrawTask;

public:

	virtual bool			Init(RenderFactory* factory, Window* window, int multisample = 0);
	virtual void			Deinit(RenderFactory* factory, Window* window);

	virtual int				GetMultisample()									{ return 0; }

	virtual void			Begin();
	virtual void			End();

	virtual RenderBuffer*	CreateRenderBuffer();
	virtual void			DestroyRenderBuffer(RenderBuffer** buffer);

	virtual bool			Draw(Geometry* geometry, unsigned int startIndex = 0, unsigned int numIndices = -1, int instanceCount = DT_NotInstanced);

//	virtual void			Draw(int instanceCount, Material* material, Geometry* geometry, int vertexFormat = 0, unsigned int startIndex = 0, unsigned int numIndices = -1);
	virtual void			SetMatrix(const Matrix4& matrix, MatrixMode mode)	{}

	virtual void			DrawPoint(const Vector4& p, const Vector4& colour = Vector4(VI_One))							{}
	virtual void			DrawLine(const Vector4& p1, const Vector4& p2, const Vector4& colour = Vector4(VI_One))			{}

	virtual void			SetGeometryTopology(GeometryTopology topology)		{}

	virtual bool			SetDeviceState(const DeviceState& state, bool force = false);
	virtual bool			GetDeviceState(DeviceState& state);

	void					SetInterlaced(bool interlaced)						{ mInterlace = interlaced; }
	bool					IsInterlaced()										{ return mInterlace; }

	// for debugging the stencil buffer
	virtual void			DrawStencil()										{}
	virtual void			SaveStencil()										{}

	virtual Window*			GetWindow();
	virtual void			ReloadResource()									{}

	// for debugging, pix in windows, some gl extension in gl
	virtual void			BeginDebugEvent(const string& name, const Vector4& colour)	{}
	virtual void			EndDebugEvent()												{}

	virtual Vector4			GetClearColour();
	virtual void			SetClearColour(const Vector4& colour);

	template <class T>
	void					SetPixelShader()
	{
		mRasterizeFunction = &SoftwareDevice::Rasterize<T>;
	}

	template <class T>
	void					Rasterize(PixelInput* pixelInput[3], PixelInput* interpolators[6], unsigned int pixelInputSize, SoftwarePixelShader* pixelShader, ParameterData* parameterData); // fisrt parameter are the clipped verts

	void					SetEffect(SoftwareEffect* effect)						{ mCurrentEffect = effect; }

//protected:

	void					IncreaseMemoryPoolSize(unsigned int newSize);

	
	void					DrawInternal(SoftwareEffect* effect, Geometry* geometry, unsigned int startIndex, unsigned int numIndices, int instanceCount, unsigned char* memory = 0);
	//virtual void			DrawInternal(int instanceCount, Material* material, Geometry* geometry, int vertexFormat = 0, unsigned int startIndex = 0, unsigned int numIndices = -1, unsigned char* memory = 0);
	void					Clip(unsigned int pixelInputSize, const Plane& clipPlane, PixelInput** verts, unsigned int numVerts, PixelInput** clippedVerts, unsigned int* clippedNumVerts);

	void					InterpInterpolant(unsigned int pixelInputSize, PixelInput* from, PixelInput* to, PixelInput* result, float percent);
	void					CalculateInterpolantIncrement(unsigned int pixelInputSize, PixelInput* from, PixelInput* to, PixelInput* increment, int numSteps);
	void					AddInterpolantIncrement(unsigned int pixelInputSize, PixelInput* to, PixelInput* increment);
	void					CopyInterpolant(unsigned int pixelInputSize, PixelInput* from, PixelInput* to);

	void					ResetDevice(int width, int height);
	void					CheckError();

#ifdef WIN32
	Win32Window*			mWindow;

	HDC						mHDC;		// Private GDI Device Context
	HGLRC					mHRC;		// Permanent Rendering Context
#else
	LinuxWindow*			mWindow;
#endif

	list<RenderBuffer*>		mRenderBuffer;

	unsigned char*			mColourBuffer;
	float*					mDepthBuffer;
	unsigned char*			mStencilBuffer;

	int						mWidth;
	int						mHeight;

	unsigned char			mClearColour[4];

	CullFace				mCullFace;

	void (SoftwareDevice::*mRasterizeFunction)(PixelInput* pixelInput[3], PixelInput* interpolators[6], unsigned int pixelInputSize, SoftwarePixelShader* pixelShader, ParameterData* parameterData);

	unsigned char*			mMemoryPool;
	unsigned int			mMemoryPoolSize;

	bool					mInterlace; //skip line? whats this actually called?

	DeviceState				mDeviceState;
	SoftwareEffect*			mCurrentEffect;
};

#include "SoftwareDevice.inl"

#endif