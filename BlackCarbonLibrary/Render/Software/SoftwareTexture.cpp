#include "SoftwareTexture.h"
#include "Render/TGA.h"
#include "Render/RenderFactory.h"
#include "File/Log.h"

bool SoftwareTexture::IsInstance(const ObjectLoadParams* params, SceneObject* loaded)
{
	if (strcmpi(params->mName.c_str(), mName.c_str()) == 0)
		return true;

	return false;
}

bool SoftwareTexture::Init(Device* device, unsigned char* image, int channels, int width, int height, int depth, int flags) 
{ 
	mWidth = width;
	mHeight = height;
	mChannels = channels;

	mTexture = new unsigned char[mWidth * mHeight * mChannels];
	memcpy(mTexture, image, mWidth * mHeight * mChannels);
	return true;
}

bool SoftwareTexture::Load(const ObjectLoadParams* params, SceneNode* node)
{
	mName = params->mName;

	string path;
	if (params->mFactory->GetParameter().GetExistingFile(params->mName, "hdr", path))
	{
		Log::Error("[SoftwareTexture::Load] HDR textures not supported yet");
	}
	else
	{
		TGA tga;
		params->mFactory->GetParameter().GetExistingFile(params->mName, "tga", path);
		
		if (!tga.Load(path.c_str()))
			return false;

		tga.ConvertToFormat(TF_RGBA);

		if (tga.GetResourceInfo(0)->width == (tga.GetResourceInfo(0)->height * 6))
		{
			tga.FlipVertical(tga.GetResourceInfo(0));
			bool result = GenerateCubeTexture(&tga, params->mFlags);
			tga.Deinit();
			return result;
		}

		bool result = GenerateTexture(&tga, params->mFlags);
		tga.Deinit();
		return result;
	}

	return false;
}

bool SoftwareTexture::GenerateTexture(TextureResource* texRes, int flags)
{
	TextureSubresource* resource = texRes->GetResourceInfo(0);
	mWidth = resource->width;
	mHeight = resource->height;
	mChannels = 4;

	mTexture = new unsigned char[mWidth * mHeight * mChannels];
	memcpy(mTexture, resource->pixel, mWidth * mHeight * mChannels);
	return true;
}

bool SoftwareTexture::GenerateCubeTexture(TextureResource* texRes, int flags)
{
	return false;
}

void SoftwareTexture::Destroy(RenderFactory* factory)												
{
}
