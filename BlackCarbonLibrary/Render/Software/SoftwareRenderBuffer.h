#ifndef _SOFTWARE_RENDERBUFFER_H_
#define _SOFTWARE_RENDERBUFFER_H_

#include "Render/RenderBuffer.h"
#include "SoftwareDevice.h"

//
// Render buffer
//
class SoftwareRenderBuffer : public RenderBuffer
{
public:

	SoftwareRenderBuffer();
	~SoftwareRenderBuffer();

	virtual void	Init(RenderBufferType type, unsigned int size, RenderBufferUseage useage);
	virtual void	Deinit();

	virtual bool	Save(const File& out);
	virtual bool	Load(Device* device, const File& in);

	virtual void	Bind(VertexFormat channel, int index = 0);
	virtual void	Unbind();

	virtual bool	Lock(RenderBufferLock& lock, int lockFlags = 0);
	virtual void	Unlock(RenderBufferLock& lock);

	virtual unsigned int	GetSize()				{ return mSize; }

	virtual void		ReloadResource(Device* device);

	virtual RenderBufferUseage	GetUseage()								{ return mUseage; }
	virtual void				Draw(unsigned int startIndex = 0, unsigned int numIndices = -1);

protected:

	VertexFormat			mChannel;
	int						mIndex;
	int						mSize;
	RenderBufferUseage		mUseage;
	void*					mData;
};

#endif
