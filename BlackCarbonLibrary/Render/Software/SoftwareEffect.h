#ifndef _SOFTWAREEFFECT_H_
#define _SOFTWAREEFFECT_H_

#include "Render/Effect.h"
#include "Render/RenderBuffer.h"
#include "Math/IntVector.h"
#include "SoftwareDevice.h"

//
// use this macro to set up the variable offset in the ParameterData structure
//
#define VAR_OFFSET(_x_) (unsigned long)&(_x_) - (unsigned long)this;

//
// this is used to describe the layout of the vertex input so the software
// renderer can put appropriate data in to the appropriate offset (VertexInput is unknown block of memory)
//
struct VertexInputLayout
{
	VertexFormat	dataType;
	unsigned int	offset;
	unsigned int	size;
};

struct VertexInput
{
};

//
// all values must be float point values in this struct to be handled by interpolation correctly
//
struct PixelInput
{
	Vector4		position;
};

struct PixelOutput
{
	Vector4 colour;
	float	depth;
};

//
// this is used to describe the memory layout of ParameterData as ParameterData is an unknown block of memory
//
struct ParameterDataLayout
{
	EffectParameterType		type;
	EffectParameterID		ID;
	unsigned int			offset;
	unsigned int			size;
	string					name;
};

//
// Any parameters needed by the vertex/pixel shader that are uploaded by the effect/material
//
class ParameterData
{
public:

	virtual ParameterData*			Clone() = 0;
	virtual ParameterDataLayout*	GetParameterDataLayout() = 0;
};

//
// interface for software vertex shader
//
class SoftwareVertexShader
{
public:

	virtual void				Apply(const ParameterData* parameter, const VertexInput* input, PixelInput* output) = 0;
	virtual VertexInputLayout*	GetVertexInputLayout() = 0;
	virtual VertexInput*		CreateVertexInput() = 0;
	virtual unsigned int		GetVertexInputSize() = 0;
	virtual void				ReleaseVertexInput(VertexInput** vertexInput) = 0;
};

//
// template extension of the above, extend from this
//
template <class T>
class SoftwareVertexShaderTemplate : SoftwareVertexShader
{
public:

    virtual ~SoftwareVertexShaderTemplate() {}

	virtual VertexInput*		CreateVertexInput()
	{
		return new T;
	}

	virtual void				ReleaseVertexInput(VertexInput** vertexInput)
	{
		delete *vertexInput;
	}

	virtual unsigned int		GetVertexInputSize()
	{
		unsigned int size = sizeof(T);
		float remainder = float(size) / 16.f;
		return remainder == 0.f ? size : (size + 16) & 0xfffffff0;
	}
};

//
// interface for software pixel shader
//
class SoftwarePixelShader
{
public:

	//virtual void				Apply(const ParameterData* parameter, const PixelInput* input, PixelOutput* output) = 0;
	virtual unsigned int		GetPixelInputSize() = 0;
	virtual void				SetPixelShader(Device* device) = 0;
};

//
// template extension of the above, extend from this
//
template <class T, class R>
class SoftwarePixelShaderTemplate : public SoftwarePixelShader
{
public:

	SoftwarePixelShaderTemplate()
	{
	//	rasterizeFunc = &SoftwareDevice::Rasterize<T>;
	}

	void SetPixelShader(Device* device)
	{
		((SoftwareDevice*)device)->SetPixelShader<T>();
	}

	virtual unsigned int		GetPixelInputSize()
	{
		unsigned int size = sizeof(R);
		float remainder = float(size) / 16.f;
		return remainder == 0.f ? size : (size + 16) & 0xfffffff0;
	}
};

//
// a pairing of vertex, pixel shaders and the parameter block used by them
// NOTE: both shaders must accept the same ParameterData
// NOTE: PixelInput must be common between pixel and vertex shader
//
class SoftwareEffectTechnique : public EffectTechnique
{
public:

	SoftwareEffectTechnique() :
		mVertexShader(0),
		mPixelShader(0)
	{
	}

	SoftwareEffectTechnique(const string& name, SoftwareVertexShader* vertexShader, SoftwarePixelShader* pixelShader, ParameterData* parameterData) :
		mVertexShader(vertexShader),
		mPixelShader(pixelShader),
		mParameterData(parameterData)
	{
		mName = name;
	}

	SoftwareVertexShader*	mVertexShader;
	SoftwarePixelShader*	mPixelShader;
	ParameterData*			mParameterData;
};

class SoftwareEffect : public Effect
{
public:

    virtual					~SoftwareEffect()           {}

	virtual int				Begin();
	virtual void			BeginPass(int idx)			{}
	virtual void			SetPassParameter();
	virtual void			EndPass()					{}
	virtual void			End();

	virtual bool			Load(const ObjectLoadParams* params, SceneNode* node = 0);

	SoftwareVertexShader*	GetVertexShader();
	SoftwarePixelShader*	GetPixelShader();
	ParameterData*			GetParameterData();

	void					SetName(const char* name);
	void					AddTechnique(SoftwareEffectTechnique* technique);

protected:

	virtual void			SetValue(EffectParameter* param, const Matrix4* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, const float* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, Texture** value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, const Vector4* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, const int* value, unsigned int count = 1);
	virtual void			SetValue(EffectParameter* param, void* buffer, unsigned int size);

	virtual void			Destroy(RenderFactory* factory);
};

#endif
