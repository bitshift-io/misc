#include "SoftwareEffect.h"

inline void	SoftwareDevice::InterpInterpolant(unsigned int pixelInputSize, PixelInput* from, PixelInput* to, PixelInput* result, float percent)
{
	// calculates for each float value of PixelInput:
	// result = from + (to - from) * percent
	// this doesnt need to be hand optimised yet, as its only used by the clipper

	int loop = pixelInputSize >> 4; // divide by 4

	__m128* r = (__m128*)result;
	__m128* t = (__m128*)to;
	__m128* f = (__m128*)from;

	__m128 mPercent = _mm_set_ps1(percent);

	for (int x = 0; x < loop; ++x)
	{
		*r = _mm_sub_ps(*t, *f);
		*r = _mm_mul_ps(*r, mPercent);
		*r = _mm_add_ps(*r, *f);
		++t;
		++f;
		++r;
	}
}

inline void SoftwareDevice::CopyInterpolant(unsigned int pixelInputSize, PixelInput* from, PixelInput* to)
{
	// simply copies PixelInput from -> to
#ifndef WIN32
	int loop = pixelInputSize >> 4; // divide by 4

	__m128* f = (__m128*)from;
	__m128* t = (__m128*)to;

	for (int x = 0; x < loop; ++x)
	{
		*t = *f;
		++t;
		++f;
	}
#else
	__asm
	{
		// move 'from' and 'to' in to some registers
		mov         esi, from
		mov         edi, to

		// eax register will contain 'pixelInputSize >> 4'
		// so we need ot use this for the loop counter
		mov			eax, pixelInputSize
		shr			eax, 4

		// edx register is our counter
		mov			edx, 0

	loop_start:
		// increment eax
		add			edx, 1

		// copy edx counter to ecx so we can do a compare
		mov			ecx, edx
		cmp			ecx, eax

		// jump to end if finished looping
		jge			loop_end

		// load 'from' into xmm1.
		movups   xmm1, [esi]

		// load 'to' into xmm2.
		movups   xmm2, [edi]

		// copy 'from' to 'to'
		movaps	xmm2, xmm1

		// write the results to vout
		movups   [edi], xmm2

		// increment 'from' and 'to' pointers by 16 bytes
		add         esi, 10h
		add         edi, 10h

		jmp			loop_start

	loop_end:
	}
#endif
}

inline void SoftwareDevice::CalculateInterpolantIncrement(unsigned int pixelInputSize, PixelInput* from, PixelInput* to, PixelInput* increment, int numSteps)
{
	// calculates for each float value of PixelInput:
	// increment = (to - from) * invNumSteps
//#ifndef WIN32
	register int loop = pixelInputSize >> 4; // divide by 4

	float invNumSteps = 1.f / (float)numSteps;
	__m128 mInvNumSteps = _mm_set_ps1(invNumSteps);

	register __m128* i = (__m128*)increment;
	register __m128* t = (__m128*)to;
	register __m128* f = (__m128*)from;

	for (int x = 0; x < loop; ++x)
	{
		*i = _mm_sub_ps(*t, *f);
		*i = _mm_mul_ps(*i, mInvNumSteps);
		++t;
		++f;
		++i;
	}
	/*
#else
	// NOT ENOUGH REGISTERS?!?!?!?!
	__asm
	{
		// move 'increment', 'from' and 'to' in to some registers
		mov         esi, from
		mov         edi, to
		mov			ebx, increment

		// eax register will contain 'pixelInputSize >> 4'
		// so we need ot use this for the loop counter
		mov			eax, pixelInputSize
		shr			eax, 4

		// edx register is our counter
		mov			edx, 0

	loop_start:
		// increment eax
		add			edx, 1

		// copy edx counter to ecx so we can do a compare
		mov			ecx, edx
		cmp			ecx, eax

		// jump to end if finished looping
		jge			loop_end

		// load 'from' into xmm1.
		movups   xmm1, [esi]

		// load 'to' into xmm2.
		movups   xmm2, [edi]

		// add 'from' to 'to' and put the result in xmm2
		addps      xmm2, xmm1


		// write the results to vout
		movups   [ebx], xmm2

		// increment 'increment', 'from' and 'to' pointers by 16 bytes
		add         esi, 10h
		add         edi, 10h
		add         ebx, 10h

		jmp			loop_start

	loop_end:
	}
#endif*/
}

inline void SoftwareDevice::AddInterpolantIncrement(unsigned int pixelInputSize, PixelInput* to, PixelInput* increment)
{
	// calculates for each float value of PixelInput:
	// to += increment

#ifndef WIN32
	register int loop = pixelInputSize >> 4; // divide by 4

	__m128* i = (__m128*)increment;
	__m128* t = (__m128*)to;

	for (int x = 0; x < loop; ++x)
	{
		*t = _mm_add_ps(*t, *i);
		++t;
		++i;
	}
#else
	__asm 
	{
		// move 'increment' and 'to' in to some registers
		mov         esi, increment
		mov         edi, to

		// eax register will contain 'pixelInputSize >> 4'
		// so we need ot use this for the loop counter
		mov			eax, pixelInputSize
		shr			eax, 4

		// edx register is our counter
		mov			edx, 0

	loop_start:
		// increment eax
		add			edx, 1

		// copy edx counter to ecx so we can do a compare
		mov			ecx, edx
		cmp			ecx, eax

		// jump to end if finished looping
		jge			loop_end

		// load 'increment' into xmm1.
		movups   xmm1, [esi]

		// load 'to' into xmm2.
		movups   xmm2, [edi]

		// add 'increment' to 'to' and put the result in xmm2
		addps      xmm2, xmm1

		// write the results to vout
		movups   [edi], xmm2

		// increment 'increment' and 'to' pointers by 16 bytes
		add         esi, 10h
		add         edi, 10h

		jmp			loop_start

	loop_end:
	}
#endif
}

template <class T>
void SoftwareDevice::Rasterize(PixelInput* pixelInput[3], PixelInput* interpolators[6], unsigned int pixelInputSize, SoftwarePixelShader* pixelShader, ParameterData* parameterData)
{
	//
	// give interpolators meaning full names
	//
	PixelInput* piLeftCurrent  = interpolators[0];
	PixelInput* piLeftIncrement  = interpolators[1];

	PixelInput* piRightCurrent  = interpolators[2];
	PixelInput* piRightIncrement  = interpolators[3];

	PixelInput* piCenterCurrent  = interpolators[4];
	PixelInput* piCenterIncrement  = interpolators[5];

	//
	//	/\
	//	|
	//  +Y
	//      +X -->
	//
	//		 0
	//		| \
	//		|  \
	//		|	\
	//		|	 \
	// divide ---- 1
	//		|	 /
	//		|	/
	//		|  /
	//		| /
	//		 2

	// sort indices by screen height, pixelInput is sorted by winding
	// ySortIdx maps between the ysorted verts to the winding sorted verts
	// winding is counter clockwise
	PixelInput* ySortPixelInput[3] = { pixelInput[0], pixelInput[1], pixelInput[2] };
	int ySortIdx[3] = {0, 1, 2};

	if (ySortPixelInput[1]->position.y > ySortPixelInput[0]->position.y)
	{
		Math::Swap(ySortIdx[0], ySortIdx[1]);
		Math::Swap(ySortPixelInput[0], ySortPixelInput[1]);
	}

	if (ySortPixelInput[2]->position.y > ySortPixelInput[1]->position.y)
	{
		Math::Swap(ySortIdx[1], ySortIdx[2]);
		Math::Swap(ySortPixelInput[1], ySortPixelInput[2]);
	}

	if (ySortPixelInput[1]->position.y > ySortPixelInput[0]->position.y)
	{
		Math::Swap(ySortIdx[0], ySortIdx[1]);
		Math::Swap(ySortPixelInput[0], ySortPixelInput[1]);
	}


	float invDeltaY[3];
	invDeltaY[0] = ySortPixelInput[0]->position.y - ySortPixelInput[2]->position.y;
	invDeltaY[1] = ySortPixelInput[0]->position.y - ySortPixelInput[1]->position.y;
	invDeltaY[2] = ySortPixelInput[1]->position.y - ySortPixelInput[2]->position.y;

	if (invDeltaY[0] != 0.f)
		invDeltaY[0] = 1.0f / invDeltaY[0];
	else
		int nothing = 0;

	if (invDeltaY[1] != 0.f)
		invDeltaY[1] = 1.0f / invDeltaY[1];
	else
		int nothing = 0;

	if (invDeltaY[2] != 0.f)
		invDeltaY[2] = 1.0f / invDeltaY[2];
	else
		int nothing = 0;


	// http://www.devmaster.net/articles/software-rendering/part3.php

	// find if the major edge is left or right aligned
	// not sure how this bit works o.0
	// its checking (deltaX_20 * deltaY_01) - (deltaY_20 * deltaX_01) > 0
	bool midVertLeft = true;

	float	v1[2], v2[2];
	v1[0] = ySortPixelInput[0]->position.x - ySortPixelInput[2]->position.x;
	v1[1] = ySortPixelInput[0]->position.y - ySortPixelInput[2]->position.y;
	v2[0] = ySortPixelInput[1]->position.x - ySortPixelInput[0]->position.x;
	v2[1] = ySortPixelInput[1]->position.y - ySortPixelInput[0]->position.y;

	if (v1[0] * v2[1] - v1[1] * v2[0] > 0)
		midVertLeft = true;
	else
		midVertLeft = false;

	// render top half of triangle

	// yTop should be smaller than yBottom as 0,0 is at the top left of the screen
	int yTop = Math::IntFloor(ySortPixelInput[0]->position.y) - 1;
	int yMid1 = Math::IntFloor(ySortPixelInput[1]->position.y);// Math::Min((int)floor(ySortPixelInput[1]->position.y) - 1, yTop); // make sure mid1 at least equals yTop
	int yMid2 = Math::IntFloor(ySortPixelInput[1]->position.y) - 1;
	int yBottom = Math::IntFloor(ySortPixelInput[2]->position.y);

	float ySub = ySortPixelInput[0]->position.y - Math::Floor(yTop);

	int leftVertIdx = (ySortIdx[0] + 2) % 3;
	float yStepLeft = ((Math::Floor(ySortPixelInput[0]->position.y) - 1) - Math::Floor(pixelInput[leftVertIdx]->position.y)) + 1;
	float xStepLeft = (pixelInput[leftVertIdx]->position.x - ySortPixelInput[0]->position.x) / (yStepLeft == 0 ? 1.f : yStepLeft);

	int rightVertIdx = (ySortIdx[0] + 1) % 3;
	float yStepRight = ((Math::Floor(ySortPixelInput[0]->position.y) - 1) - Math::Floor(pixelInput[rightVertIdx]->position.y)) + 1;
	float xStepRight = (pixelInput[rightVertIdx]->position.x - ySortPixelInput[0]->position.x) / (yStepRight == 0 ? 1.f : yStepRight);

	float xLeft = ySortPixelInput[0]->position.x;
	float xRight = ySortPixelInput[0]->position.x;

	CopyInterpolant(pixelInputSize, ySortPixelInput[0], piLeftCurrent);
	CopyInterpolant(pixelInputSize, ySortPixelInput[0], piRightCurrent);

	CalculateInterpolantIncrement(pixelInputSize, ySortPixelInput[0], pixelInput[leftVertIdx], piLeftIncrement, yStepLeft);
	CalculateInterpolantIncrement(pixelInputSize, ySortPixelInput[0], pixelInput[rightVertIdx], piRightIncrement, yStepRight);

	ALIGN16 PixelOutput pixelOutput;

	if (invDeltaY[1])
	{
		for (int y = yTop; y >= yMid1; --y)
		{
			// interlaced
			if (!mInterlace || (mInterlace && (y & 0x1) == 0))
			{
				int xStart = Math::IntCeil(xLeft);
				int xEnd = (Math::IntCeil(xRight) - 1);

				// make sure our clipper is working correctly
	#ifdef _DEBUG
				if (y < 0 || y >= mHeight || xStart < 0 || xEnd >= mWidth)
				{
					Assert(false);
					break;
				}
	#endif
				CopyInterpolant(pixelInputSize, piLeftCurrent, piCenterCurrent);
				CalculateInterpolantIncrement(pixelInputSize, piLeftCurrent, piRightCurrent, piCenterIncrement, (xEnd - xStart) + 1);

				unsigned int* pixelColour = (unsigned int*)&mColourBuffer[(xStart + y * mWidth) * 4];
				float* pixelDepth = &mDepthBuffer[(xStart + y * mWidth)];
				for (int x = xStart; x <= xEnd; ++x)
				{
					// do depth compare and discard, can we do this before shader is applied in some cases?
					// maybe pass in 2 depths, current pixel, current colour and let the shader return when needed
					// it could also apply the blending required o.0, pimp!
					if (piCenterCurrent->position.z <= *pixelDepth)
					{
						//pixelOutput.depth = piCenterCurrent->position.z;
						T::Apply(parameterData, piCenterCurrent, &pixelOutput);
						//pixelShader->Apply(parameterData, piCenterCurrent, &pixelOutput);

						//*pixelDepth = pixelOutput.depth;
						*pixelDepth = piCenterCurrent->position.z;

						// multiply float values by 255
						__m128 convertFloatToChar = _mm_set_ps1(255.f);
						__m128* colour = (__m128*)&pixelOutput.colour;
						*colour = _mm_mul_ps(*colour, convertFloatToChar);

						unsigned char* charPixelColour = (unsigned char*)pixelColour;
						charPixelColour[0] = Math::IntRound(pixelOutput.colour.x);
						charPixelColour[1] = Math::IntRound(pixelOutput.colour.y);
						charPixelColour[2] = Math::IntRound(pixelOutput.colour.z);
						charPixelColour[3] = Math::IntRound(pixelOutput.colour.w); // do alpha blending here
					}

					AddInterpolantIncrement(pixelInputSize, piCenterCurrent, piCenterIncrement);

					++pixelColour;
					++pixelDepth;
				}
			}

			AddInterpolantIncrement(pixelInputSize, piLeftCurrent, piLeftIncrement);
			AddInterpolantIncrement(pixelInputSize, piRightCurrent, piRightIncrement);

			xLeft += xStepLeft;
			xRight += xStepRight;
		}
	}

	// render bottom half of triangle
	if (invDeltaY[2])
	{
		if (midVertLeft)
		{
			xLeft = ySortPixelInput[1]->position.x;

			// mid point is on the left of the triangle
			yStepLeft = ((Math::IntFloor(ySortPixelInput[1]->position.y) - 1) - Math::IntFloor(ySortPixelInput[2]->position.y)) + 1;
			xStepLeft = (ySortPixelInput[2]->position.x - ySortPixelInput[1]->position.x) / (yStepLeft == 0 ? 1.f : yStepLeft);

			CopyInterpolant(pixelInputSize, ySortPixelInput[1], piLeftCurrent);
			CalculateInterpolantIncrement(pixelInputSize, ySortPixelInput[1], ySortPixelInput[2], piLeftIncrement, yStepLeft);
		}
		else
		{
			xRight = ySortPixelInput[1]->position.x;

			// mid point is on the right of the triangle
			yStepRight = ((Math::IntFloor(ySortPixelInput[1]->position.y) - 1) - Math::IntFloor(ySortPixelInput[2]->position.y)) + 1;
			xStepRight = (ySortPixelInput[2]->position.x - ySortPixelInput[1]->position.x) / (yStepRight == 0 ? 1.f : yStepRight);

			CopyInterpolant(pixelInputSize, ySortPixelInput[1], piRightCurrent);
			CalculateInterpolantIncrement(pixelInputSize, ySortPixelInput[1], ySortPixelInput[2], piRightIncrement, yStepRight);
		}

		for (int y = yMid2; y >= yBottom; --y)
		{
			// interlaced
			if (!mInterlace || (mInterlace && (y & 0x1) == 0))
			{
				int xStart = Math::IntCeil(xLeft);
				int xEnd = (Math::IntCeil(xRight) - 1);

				// make sure our clipper is working correctly
	#ifdef _DEBUG
				if (y < 0 || y >= mHeight || xStart < 0 || xEnd >= mWidth)
				{
					Assert(false);
					break;
				}
	#endif

				CopyInterpolant(pixelInputSize, piLeftCurrent, piCenterCurrent);
				CalculateInterpolantIncrement(pixelInputSize, piLeftCurrent, piRightCurrent, piCenterIncrement, (xEnd - xStart) + 1);

				unsigned int* pixelColour = (unsigned int*)&mColourBuffer[(xStart + y * mWidth) * 4];
				float* pixelDepth = &mDepthBuffer[(xStart + y * mWidth)];
				for (int x = xStart; x <= xEnd; ++x)
				{
					// do depth compare and discard, can we do this before shader is applied in some cases?
					// maybe pass in 2 depths, current pixel, current colour and let the shader return when needed
					// it could also apply the blending required o.0, pimp!
					if (piCenterCurrent->position.z <= *pixelDepth)
					{
						//pixelOutput.depth = piCenterCurrent->position.z;
						//pixelShader->Apply(parameterData, piCenterCurrent, &pixelOutput);
						T::Apply(parameterData, piCenterCurrent, &pixelOutput);
						//*pixelDepth = pixelOutput.depth;
						*pixelDepth = piCenterCurrent->position.z;

						// multiply float values by 255
						__m128 convertFloatToChar = _mm_set_ps1(255.f);
						__m128* colour = (__m128*)&pixelOutput.colour;
						*colour = _mm_mul_ps(*colour, convertFloatToChar);

						unsigned char* charPixelColour = (unsigned char*)pixelColour;
						charPixelColour[0] = Math::IntRound(pixelOutput.colour.x);
						charPixelColour[1] = Math::IntRound(pixelOutput.colour.y);
						charPixelColour[2] = Math::IntRound(pixelOutput.colour.z);
						charPixelColour[3] = Math::IntRound(pixelOutput.colour.w); // do alpha blending here
					}

					AddInterpolantIncrement(pixelInputSize, piCenterCurrent, piCenterIncrement);

					++pixelColour;
					++pixelDepth;
				}
			}

			AddInterpolantIncrement(pixelInputSize, piLeftCurrent, piLeftIncrement);
			AddInterpolantIncrement(pixelInputSize, piRightCurrent, piRightIncrement);

			xLeft += xStepLeft;
			xRight += xStepRight;
		}
	}
}