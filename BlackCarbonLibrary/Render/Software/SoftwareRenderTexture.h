#ifndef _SOFTWARERENDERTEXTURE_H_
#define _SOFTWARERENDERTEXTURE_H_

#include "Render/RenderTexture.h"

class SoftwareRenderTexture : public RenderTexture
{
public:

	virtual bool		Init(Device* device, RenderFactory* factory, int width, int height, unsigned int flags, int targetCount = 1)	{ return false; }
	virtual void		Deinit()												{}

	virtual int			Begin()													{ return 0; }
	virtual void		End()													{}

	virtual void		SetViewport(const Vector4& topLeft, const Vector4& bottomRight, const Vector4& nearFarDepth = Vector4(0.f, 1.f))	{}
	virtual void		BindTarget(int index, bool clear = true, int flags = RTF_Colour | RTF_Depth | RTF_Stencil)				{}
	virtual void		SetClearColour(const Vector4& colour)					{}
	virtual void		SetClearDepth(float depth)								{}
	virtual void		SetClearStencil(unsigned int stencil)					{}

	virtual Texture*	GetTexture(RenderTextureFlags type, int lockFlags = 0)	{ return 0; }

	virtual int			GetWidth()												{ return 0; }
	virtual int			GetHeight()												{ return 0; }

protected:

};

#endif