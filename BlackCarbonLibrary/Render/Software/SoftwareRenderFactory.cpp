#include "SoftwareRenderFactory.h"
#include "SoftwareDevice.h"
#include "SoftwareEffect.h"
#include "SoftwareTexture.h"
#include "SoftwareRenderTexture.h"
#include "Memory/Memory.h"

#ifdef WIN32
	#include "Render/Win32/Win32Window.h"
	#undef CreateWindow
#else
	#include "Render/Linux/LinuxWindow.h"
#endif

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

SoftwareRenderFactory::SoftwareRenderFactory(const RenderFactoryParameter& parameter) :
	RenderFactory(parameter)
{
	Register(ObjectCreator<SoftwareEffect>,			Effect::Type);
	Register(ObjectCreator<SoftwareTexture>,		Texture::Type);
	Register(ObjectCreator<SoftwareRenderTexture>,	RenderTexture::Type);
}

void SoftwareRenderFactory::Init()
{

}

void SoftwareRenderFactory::Deinit()
{

}

Window* SoftwareRenderFactory::CreateWindow()
{
#ifdef WIN32
	return new Win32Window;
#else
	return new LinuxWindow;
#endif
}

Device*	SoftwareRenderFactory::CreateDevice()
{
	return new SoftwareDevice;
}

void SoftwareRenderFactory::ReleaseWindow(Window** window)
{
#ifdef WIN32
	Win32Window* win = static_cast<Win32Window*>(*window);
	delete win;
	*window = 0;
#else
	LinuxWindow* win = static_cast<LinuxWindow*>(*window);
	delete win;
	*window = 0;
#endif
}

void SoftwareRenderFactory::ReleaseDevice(Device** device)
{
	SoftwareDevice* dev = static_cast<SoftwareDevice*>(*device);
	delete dev;
	*device = 0;
}
/*
void SoftwareRenderFactory::RegisterEffectTechnique(SoftwareEffectTechnique* technique)
{
	mEffectTechnique.push_back(technique);
}

SoftwareEffectTechnique* SoftwareRenderFactory::GetEffectTechnique(const string& name)
{
	vector<SoftwareEffectTechnique*>::iterator it;
	for (it = mEffectTechnique.begin(); it != mEffectTechnique.end(); ++it)
	{
		if (strcmpi((*it)->mName.c_str(), name.c_str()) == 0)
			return *it;
	}

	return 0;
}
*/
SoftwareEffect* SoftwareRenderFactory::GetEffectByName(const char* name)
{
	map<int, list<SceneObject*> >::iterator objList = mSceneObjectList.find(Effect::Type);
	if (objList == mSceneObjectList.end())
		return 0;

	list<SceneObject*>::iterator listIt;
	for (listIt = objList->second.begin(); listIt != objList->second.end(); ++listIt)
	{
		SoftwareEffect* effect = (SoftwareEffect*)(*listIt);
		if (strcmpi(effect->GetName(), name) == 0)
		{
			return effect;
		}
	}

	// not found, so look for null
	for (listIt = objList->second.begin(); listIt != objList->second.end(); ++listIt)
	{
		SoftwareEffect* effect = (SoftwareEffect*)(*listIt);
		if (strcmpi(effect->GetName(), "null") == 0)
		{
			return effect;
		}
	}

	return 0;
}
