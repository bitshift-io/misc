#include "Device.h"
#include "Template/Time.h"
#include "RenderBuffer.h"

void Device::Begin()
{
	mPolyCount = 0;
	mDrawCallCount = 0;
	if (Time::GetMilliseconds() >= mFrameTime)
	{
		mFPS = mFramesThisSecond;
		mFramesThisSecond = 0;
		mFrameTime = Time::GetMilliseconds() + 1000;
	}
	else
	{
		++mFramesThisSecond;
	}
}

bool Device::Draw(Geometry* geometry, unsigned int startIndex, unsigned int numIndices, int instanceCount)
{
	++mDrawCallCount;
	unsigned int polyCount = (numIndices / 3) * ((instanceCount == DT_NotInstanced) ? 1 : instanceCount);
	mPolyCount += polyCount;
	return true;
}

void Device::DrawMatrix(const Matrix4& matrix, float scale)
{
	const Vector4& position = matrix.GetTranslation();
	DrawLine(position, position + (matrix.GetZAxis() * scale), Vector4(0.f, 0.f, 1.f));
	DrawLine(position, position + (matrix.GetXAxis() * scale), Vector4(1.f, 0.f, 0.f));
	DrawLine(position, position + (matrix.GetYAxis() * scale),  Vector4(0.f, 1.f, 0.f));
}

void Device::DrawSphere(const Sphere& sphere)
{
	Vector4 cur;
	Vector4 last(sinf(0), cosf(0), 0);
	last = sphere.GetPosition() + (last * sphere.GetRadius());

	for (float a = 0.1f; a <= (Math::PI * 2) + 0.1f; a += 0.1f)
	{
		cur = sphere.GetPosition() + Vector4(sinf(a), cosf(a), 0) * sphere.GetRadius();
		DrawLine(last, cur);
		last = cur;
	}

	last = sphere.GetPosition() + (Vector4(sinf(0), 0, cosf(0)) * sphere.GetRadius());
	for (float a = 0.1f; a <= (Math::PI * 2) + 0.1f; a += 0.1f)
	{
		cur = sphere.GetPosition() + Vector4(sinf(a), 0, cosf(a)) * sphere.GetRadius();
		DrawLine(last, cur);
		last = cur;
	}

	last = sphere.GetPosition() + (Vector4(0, sinf(0), cosf(0)) * sphere.GetRadius());
	for (float a = 0.1f; a <= (Math::PI * 2) + 0.1f; a += 0.1f)
	{
		cur = sphere.GetPosition() + Vector4(0, sinf(a), cosf(a)) * sphere.GetRadius();
		DrawLine(last, cur);
		last = cur;
	}
}

void Device::DrawBox(const Box& box)
{
	Vector4 min = box.min;
	Vector4 max = box.max;
	Vector4 transBox[8] = {	Vector4(min.x, min.y, min.z),
							Vector4(min.x, min.y, max.z),
							Vector4(min.x, max.y, max.z),
							Vector4(max.x, max.y, max.z),
							Vector4(max.x, max.y, min.z),
							Vector4(max.x, min.y, min.z),
							Vector4(min.x, max.y, min.z),
							Vector4(max.x, min.y, max.z)};

	for (int i = 0; i < 8; ++i)
		transBox[i] = box.transform * transBox[i];

	DrawLine(transBox[0], transBox[1]);
	DrawLine(transBox[1], transBox[2]);
	DrawLine(transBox[2], transBox[3]);
	DrawLine(transBox[3], transBox[4]);
	DrawLine(transBox[4], transBox[5]);
	DrawLine(transBox[5], transBox[0]);
	DrawLine(transBox[6], transBox[0]);
	DrawLine(transBox[6], transBox[2]);
	DrawLine(transBox[6], transBox[4]);
	DrawLine(transBox[7], transBox[3]);
	DrawLine(transBox[7], transBox[5]);
	DrawLine(transBox[7], transBox[1]);
}

void Device::DrawFrustum(const Frustum& frustum)
{
	DrawLine(frustum.points[Frustum::TopLeftNear], frustum.points[Frustum::TopLeftFar]);
	DrawLine(frustum.points[Frustum::TopRightNear], frustum.points[Frustum::TopRightFar]);

	DrawLine(frustum.points[Frustum::BottomLeftNear], frustum.points[Frustum::BottomLeftFar]);
	DrawLine(frustum.points[Frustum::BottomRightNear], frustum.points[Frustum::BottomRightFar]);

	DrawLine(frustum.points[Frustum::TopLeftNear], frustum.points[Frustum::BottomLeftNear]);
	DrawLine(frustum.points[Frustum::TopRightNear], frustum.points[Frustum::BottomRightNear]);

	DrawLine(frustum.points[Frustum::BottomLeftFar], frustum.points[Frustum::TopLeftFar]);
	DrawLine(frustum.points[Frustum::BottomRightFar], frustum.points[Frustum::TopRightFar]);

	DrawLine(frustum.points[Frustum::TopLeftNear], frustum.points[Frustum::TopRightNear]);
	DrawLine(frustum.points[Frustum::TopRightFar], frustum.points[Frustum::TopLeftFar]);

	DrawLine(frustum.points[Frustum::BottomLeftFar], frustum.points[Frustum::BottomRightFar]);
	DrawLine(frustum.points[Frustum::BottomRightNear], frustum.points[Frustum::BottomLeftNear]);
}