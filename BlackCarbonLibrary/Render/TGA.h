#ifndef _TGA_H_
#define _TGA_H_

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "File/File.h"
#include "TextureResource.h"

class TGA : public TextureResource
{
public:

	virtual bool			Load(const char* file);
	virtual bool			Save(const char* file);

	bool Save(const char* file, const void* data, int width, int height, int bpp);

/*
	TGA();
	~TGA();
	bool Load(const char* file);

	virtual unsigned char* GetPixels();
	virtual int GetWidth();
	virtual int GetHeight();

	//bytes per pixel
	virtual int GetBPP();

	bool ConvertToGreyScale();
	bool ConvertToRGBA();

	bool Save(const char* file, const void* data, int width, int height, int bpp);

	void FlipV();

	// for cube maps
	bool CreateCubeFace(TGA& tga, int idx);

	void GetMipmap(TGA& out, int level);
	

	void GetBilinearPixel(int x, int y, unsigned char* data);
	void GetPixel(int x, int y, unsigned char* data);

protected:
*/
	enum ImageType
	{
		IT_Compressed = 10,
		IT_Uncompressed = 2,
	};

	#pragma pack(push,1)
	struct TGAHead
    {
		char identsize;
		char colorType;
		char imageType; //compressed if 10, uncom if 2
		short mapstart;
		short length;
		char mapBits;
		short xstart;
		short ystart;
		short width;
		short height;
		char bpp;

		  /* Image descriptor.
			 3-0: attribute bpp
			 4:   left-to-right ordering
			 5:   top-to-bottom ordering
			 7-6: zero
			*/
		char descritor;
    };
	#pragma pack(pop)
/*
	TGAHead header;
	char*	image;
	int		numMipmap;
*/
	bool LoadUncompresed(File& in, TGAHead& header, TextureSubresource* resource);
	bool LoadCompresed(File& in, TGAHead& header, TextureSubresource* resource);
};

#endif
