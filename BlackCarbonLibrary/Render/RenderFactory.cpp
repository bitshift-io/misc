#include "RenderFactory.h"
#include "Geometry.h"
#include "Camera.h"
#include "Spline.h"
#include "Mesh.h"
#include "Light.h"
#include "Material.h"
#include "Renderer.h"
#include "Occluder.h"
#include "Skin.h"
#include "Font.h"
#include "Shape.h"
#include "Device.h"
#include "ParticleSystem.h"
#include "AnimationController.h"
#include "Memory/Memory.h"
#include "File/Log.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

bool RenderFactoryParameter::GetExistingFile(const string& filename, const string& extension, string& outPath) const
{
	File f;

	outPath = filename + string(".") + extension;
	if (f.Open(outPath.c_str(), FAT_Read | FAT_Binary))
	{
		outPath = f.GetName();
		return true;
	}

	// last resort
	outPath = string("null") + string(".") + extension;
	if (f.Open(outPath.c_str(), FAT_Read | FAT_Binary))
	{
		outPath = f.GetName();
		return false;
	}

	return false;
}

RenderFactory::RenderFactory(const RenderFactoryParameter& parameter)
{
	mParameter = parameter;

	Register(ObjectCreator<Geometry>,		Geometry::Type);
	Register(ObjectCreator<Camera>,			Camera::Type);
	Register(ObjectCreator<Spline>,			Spline::Type);
	Register(ObjectCreator<Mesh>,			Mesh::Type);
	Register(ObjectCreator<Light>,			Light::Type);
	Register(ObjectCreator<MaterialBase>,	MaterialBase::Type);
	Register(ObjectCreator<Material>,		Material::Type);
	Register(ObjectCreator<SkinMesh>,		SkinMesh::Type);
	Register(ObjectCreator<Font>,			Font::Type);
	Register(ObjectCreator<Shape>,			Shape::Type);

	Register(ObjectCreator<ParticleActionGroup>,	ParticleActionGroup::Type);
	Register(ObjectCreator<ParticleSystem>,			ParticleSystem::Type);

	Register(ObjectCreator<Animation>,		Animation::Type);
}

void RenderFactory::Register(FnObjectCreate createFn, int type)
{
#ifdef DEBUG
	vector<CreatorInfo>::iterator it;
	for (it = mCreator.begin(); it != mCreator.end(); ++it)
	{
		if (it->type == type)
		{
			Log::Print("TYPE HAS ALREADY BEEN REGISTERED!");
		}
	}
#endif

	CreatorInfo info;
	info.createFn = createFn;
	info.type = type;
	mCreator.push_back(info);
}

SceneObject* RenderFactory::Clone(SceneObject* obj, SceneNode* owner)
{
	int type = obj->GetType();
	SceneObject* sceneObj = obj->Clone(owner);
	
	// add to list
	map<int, list<SceneObject*> >::iterator objList  = mSceneObjectList.find(type);

	if (objList == mSceneObjectList.end())
	{
		mSceneObjectList[type] = list<SceneObject*>();
		objList = mSceneObjectList.find(type);
	}

	if (sceneObj)
		objList->second.push_back(sceneObj);

	sceneObj->AddReference();
	return sceneObj;
}

SceneObject* RenderFactory::Load(int type, const ObjectLoadParams* params)
{
	ObjectLoadParams constParams = *params;
	constParams.mFactory = this;

	long cursorPos = 0;
	SceneObject* tempSceneObj = 0;
	if (params->mFile)
	{
		cursorPos = constParams.mFile->GetPosition();
		constParams.mFlags |= OLF_LoadForInstance;

		// load a temporary/light version from disk
		tempSceneObj = Create(type, false);
		if (!tempSceneObj->Load(&constParams, 0))
		{
			Release(&tempSceneObj);
			return 0;
		}
	}

	// now check if its already loaded
	bool alreadyInList = false;
	SceneObject* sceneObj = 0;
	map<int, list<SceneObject*> >::iterator objList = mSceneObjectList.find(type);

	if (objList == mSceneObjectList.end())
	{
		mSceneObjectList[type] = list<SceneObject*>();
		objList = mSceneObjectList.find(type);
	}

	list<SceneObject*>::iterator listIt;
	for (listIt = objList->second.begin(); listIt != objList->second.end(); ++listIt)
	{
		if ((*listIt)->IsInstance(&constParams, tempSceneObj))
		{
			sceneObj = (*listIt)->Clone(&constParams, tempSceneObj);
			alreadyInList = (sceneObj == (*listIt));
			break;
		}
	}

	if (!sceneObj)
	{
		// load properly, remove load for instance flag
		constParams.mFlags = params->mFlags & ~OLF_LoadForInstance;
		if (constParams.mFile)
			constParams.mFile->Seek(cursorPos, FST_Begin);

		if (!tempSceneObj)
			tempSceneObj = Create(type, false);

		if (!tempSceneObj->Load(&constParams, 0))
		{
			Release(&tempSceneObj);
			return 0;
		}

		sceneObj = tempSceneObj;
	}
	else
	{
		Release(&tempSceneObj);
	}

	//if (params->mFile)
	//	Release(&tempSceneObj);

	if (!alreadyInList)
	{
		objList->second.push_back(sceneObj);
		//sceneObj->AddReference();
	}

	return sceneObj;
}

SceneObject* RenderFactory::Create(int type, bool addToList)
{
	vector<CreatorInfo>::const_iterator it;
	for (it = mCreator.begin(); it != mCreator.end(); ++it)
	{
		if (it->type == type)
		{
			SceneObject* sceneObj = it->createFn();
			sceneObj->Create(this);

			if (addToList)
			{
				// now check if its already loaded
				map<int, list<SceneObject*> >::iterator objList  = mSceneObjectList.find(type);

				if (objList == mSceneObjectList.end())
				{
					mSceneObjectList[type] = list<SceneObject*>();
					objList = mSceneObjectList.find(type);
				}

				if (sceneObj)
					objList->second.push_back(sceneObj);
			}

			sceneObj->AddReference();
			return sceneObj;
		}
	}

	return 0;
}

SceneObject* RenderFactory::Release(SceneObject** obj)
{
	if (!*obj)
		return 0;

	if ((*obj)->ReleaseReference())
	{
		map<int, list<SceneObject*> >::iterator objList = mSceneObjectList.find((*obj)->GetType());
		if (objList != mSceneObjectList.end())
			objList->second.remove(*obj);

		(*obj)->Destroy(this);
		delete *obj;
		return 0;
	}

	SceneObject* retObj = *obj;
	*obj = 0;
	return retObj;
}

Scene* RenderFactory::LoadScene(const ObjectLoadParams* params)
{
    ObjectLoadParams constParams = *params;
	constParams.mFactory = this;

	list<Scene*>::iterator it;
	for (it = mSceneList.begin(); it != mSceneList.end(); ++it)
	{
		if (strcmpi((*it)->GetName().c_str(), params->mName.c_str()) == 0)
		{
			Scene* scene = CreateScene();
			if (!scene->Clone(&constParams, *it))
				ReleaseScene(&scene);

			if (scene)
				mSceneList.push_back(scene);

			return scene;
		}
	}

	Scene* scene = CreateScene();
	if (!scene->Load(&constParams))
		ReleaseScene(&scene);

	if (scene)
		mSceneList.push_back(scene);

	return scene;
}

Scene* RenderFactory::CreateScene()
{
	Scene* pScene = new Scene;
	pScene->Create(this);
	return pScene;
}

void RenderFactory::ReleaseScene(Scene** scene)
{
	if (!*scene)
		return;

	mSceneList.remove(*scene);
	(*scene)->Destroy(this);
	delete *scene;
	*scene = 0;
}

Renderer* RenderFactory::CreateRenderer()
{
	return new Renderer();
}

void RenderFactory::ReleaseRenderer(Renderer** renderer)
{
	delete *renderer;
	*renderer = 0;
}

Occluder* RenderFactory::CreateOccluder()
{
	return new Occluder();
}

void RenderFactory::ReleaseOccluder(Occluder** occluder)
{
	delete *occluder;
	*occluder = 0;
}

void RenderFactory::ReloadResource(Device* device)
{
	device->ReloadResource();

	map<int, list<SceneObject*> >::iterator listIt;
	for (listIt = mSceneObjectList.begin(); listIt != mSceneObjectList.end(); ++listIt)
	{
		list<SceneObject*>::iterator it;
		for (it = listIt->second.begin(); it != listIt->second.end(); ++it)
		{
			(*it)->ReloadResource();
		}
	}
}
