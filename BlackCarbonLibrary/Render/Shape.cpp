#include "Shape.h"
#include "File/File.h"

bool Shape::Save(const File& out, SceneNode* node)
{
	out.Write(&mType, sizeof(ShapeType));
	out.Write(&mPlane, sizeof(PlaneShape));
	out.Write(&mBox, sizeof(BoxShape));
	out.Write(&mSphere, sizeof(SphereShape));
	out.Write(&mCylinder, sizeof(CylinderShape));

	return true;
}

bool Shape::Load(const ObjectLoadParams* params, SceneNode* node)
{
	File* in = params->mFile;
	in->Read(&mType, sizeof(ShapeType));
	in->Read(&mPlane, sizeof(PlaneShape));
	in->Read(&mBox, sizeof(BoxShape));
	in->Read(&mSphere, sizeof(SphereShape));
	in->Read(&mCylinder, sizeof(CylinderShape));
	return true;
}

Sphere Shape::GetSphere(SceneNode* node)
{
	Sphere sphere;
	sphere.radius = mSphere.radius;
	sphere.position = node->GetWorldTransform().GetTranslation();
	return sphere;
}

bool Shape::GetLocalBoundBox(Box& box)
{
	switch (mType)
	{
	case ST_Box:
		{
			box.SetMin(-Vector4(mBox.width * 0.5f, mBox.height * 0.5f, mBox.length * 0.5f));
			box.SetMax(Vector4(mBox.width * 0.5f, mBox.height * 0.5f, mBox.length * 0.5f));
		}
		break;

	case ST_Sphere:
		{
			box.SetMin(-Vector4(mSphere.radius, mSphere.radius, mSphere.radius));
			box.SetMax(Vector4(mSphere.radius, mSphere.radius, mSphere.radius));
		}
		break;

	case ST_Cylinder:
		{
			return false;
		}
		break;

	case ST_Plane:
		{
			box.SetMin(-Vector4(mPlane.width * 0.5f, 0.f, mPlane.length * 0.5f));
			box.SetMax(Vector4(mPlane.width * 0.5f, 0.f, mPlane.length * 0.5f));
		}
		break;
	}

	return true;
}
