#include "OGLEffect.h"
#include <iostream>
#include "Render/RenderFactory.h"
#include "Render/Texture.h"
#include "Render/Light.h"
#include "File/Log.h"
#include "Memory/Memory.h"

int			OGLEffect::sPassIdx = 0;
CGpass		OGLEffect::sPass;
map<string, CGeffect> OGLEffect::sEffect;


/*
void OGLEffect::ReloadResource()
{
	// clean up effects
	static map<string, CGeffect>::iterator it;
	for (it = sEffect.begin(); it != sEffect.end(); ++it)
	{
		cgDestroyEffect(it->second);
		it->second = 0;
	}

	// destory context
	cgDestroyContext(sContext);
	sContext = 0;
/*
	// create new context
	sContext = cgCreateContext();

	cgSetErrorCallback(ErrorCallback);
	cgGLRegisterStates(sContext);
	
	cgGLSetOptimalOptions(CG_PROFILE_ARBVP1);
	cgGLSetOptimalOptions(CG_PROFILE_ARBFP1);* /
}

void OGLEffect::Deinit()
{
	mParameter.clear();
	//cgDestroyEffect(mEffect);
	mEffect = 0;
	//sEffect[mName] = 0;
}
*/
bool OGLEffect::Load(const ObjectLoadParams* params, SceneNode* node)
{
	Effect::Load(params, node);

	mName = params->mName;

	CGcontext sContext = ((OGLDevice*)params->mDevice)->GetContext();	

	string path;
	params->mFactory->GetParameter().GetExistingFile(mName, "cg", path);
	mEffect = sEffect[mName];
	mOrigonal = false;
	if (!mEffect)
	{
		mOrigonal = true;
		const char* args[] = {"-DOGL=1", 0};
		//mEffect = cgCreateEffectFromFile(sContext, path.c_str(), args);

		File shaderFile(path.c_str(), FAT_Read | FAT_Binary);
		if (!shaderFile.Valid())
		{
			Log::Error("[OGLEffect::Load] '%s' does not exist", path.c_str());
			return false;
		}

		unsigned int size = shaderFile.Size() + 2;
		char* code = new char[size];
		memset(code, 0, size);
		shaderFile.Read(code, size - 2);
		mEffect = cgCreateEffect(sContext, code, args);
		delete[] code;
		if (!mEffect)
		{
			const char* listing = cgGetLastListing(sContext);
			Log::Error("Call to cgCreateEffectFromFile failed:\n%s\n\n", listing ? listing :  "");
			return false;
		}
		sEffect[mName] = mEffect;
	}

	ProcessTechniques(params->mDevice);
    mCurrentTechnique = *BeginTechnique(); //GetTechniqueByName("Render");

	ProcessParameters();

/*
	// find a valid technique
	mTechnique = cgGetFirstTechnique(mEffect);
    while (mTechnique)
	{
        if (cgValidateTechnique(mTechnique) == CG_FALSE)
			Log::Print("Technique %s did not validate\n", cgGetTechniqueName(mTechnique));

        mTechnique = cgGetNextTechnique(mTechnique);
    }

	mTechnique = cgGetFirstTechnique(mEffect);
    while (mTechnique && !cgIsTechniqueValidated(mTechnique))
		mTechnique = cgGetNextTechnique(mTechnique);

    if (!mTechnique)
	{
		Log::Print("No valid techniques found\n");
        return false;
    }

	// calulate pas count
	mNumPasses = 0;
	CGpass pass = cgGetFirstPass(mTechnique);
    while (pass) 
	{
        cgSetPassState(pass);
        pass = cgGetNextPass(pass);
		++mNumPasses;
    }

	ProcessParameters();
	ProcessStructs();
	*/
	return true;
}

bool OGLEffect::ProcessTechniques(Device* device)
{
	CGtechnique cgTechnique = cgGetFirstTechnique(mEffect);
    while (cgTechnique)
	{
        if (cgValidateTechnique(cgTechnique) == CG_FALSE)
			Log::Print("Technique %s did not validate\n", cgGetTechniqueName(cgTechnique));

		EffectTechnique* technique = new EffectTechnique();
		technique->mVertexFormat = 0;
		technique->mHandle = cgTechnique;
		technique->mName = cgGetTechniqueName(cgTechnique);	
		mTechnique.push_back(technique);

        cgTechnique = cgGetNextTechnique(cgTechnique);
    }

	return true;
}

void OGLEffect::ProcessParameters(EffectParameter* parent)
{
	CGparameter cgParam = cgGetFirstEffectParameter(mEffect);
    while (cgParam)
	{
		const char* semantic = cgGetParameterSemantic(cgParam);
		int parameterID = GetParameterIDByName(semantic);

		EffectParameter* effectParam = new EffectParameter();
		effectParam->mEffect = this;
		effectParam->mHandle = (EffectParameterHandle)cgParam;
		effectParam->mID = (EffectParameterID)parameterID;
		effectParam->mSemantic = semantic ? semantic : "";
		effectParam->mType = EPT_Unknown;
		effectParam->mBufferOffset = 0;
		effectParam->mName = cgGetParameterName(cgParam);

		CGtype cgType = cgGetParameterNamedType(cgParam);
		if (cgType == CG_ARRAY)
		{
			effectParam->mArraySize = cgGetArraySize(cgParam, 0);
			cgType = cgGetArrayType(cgParam);
		}

		switch (cgType)
		{
		case CG_STRUCT:
			break;

		case CG_FLOAT4x4:
			effectParam->mType = EPT_Matrix4;
			break;

		case CG_FLOAT:
			effectParam->mType = EPT_Float;
			break;

		case CG_FLOAT2:
			effectParam->mType = EPT_Vector4;
			break;

		case CG_FLOAT3:
			effectParam->mType = EPT_Vector4;
			break;

		case CG_FLOAT4:
			effectParam->mType = EPT_Vector4;
			break;

		case CG_INT:
			break;

		case CG_SAMPLER2D:
			effectParam->mType = EPT_Texture2D;
			break;

		case CG_SAMPLER3D:
			effectParam->mType = EPT_Texture3D;
			break;

		case CG_SAMPLERCUBE:
			effectParam->mType = EPT_TextureCube;
			break;

		default:
			effectParam->mType = EPT_Unknown;
			break;
		}


		mParameter.push_back(effectParam);

		cgParam = cgGetNextParameter(cgParam);		
	}
}






/*
void OGLEffect::ProcessStructs()
{
	/*
	EffectParamHandle param = (EffectParamHandle)cgGetFirstStructParameter(mEffect);
CGDLL_API CGparameter cgGetFirstStructParameter(CGparameter param);
CGDLL_API CGparameter cgGetNamedStructParameter(CGparameter param, 
                                                const char *name);* /
}

void OGLEffect::ProcessParameters()
{
	mLight = false;
	mProjector = false;
	mTime = false;
	mSkin = false;

	EffectParamHandle param = (EffectParamHandle)cgGetFirstEffectParameter(mEffect);
    while (param)
	{
		const char* semantic = cgGetParameterSemantic((CGparameter)param);
		const char* paramName = cgGetParameterName((CGparameter)param);

		// process the parameter
        if (semantic == NULL)
		{
			param = (EffectParamHandle)cgGetNextParameter((CGparameter)param);
			continue;
		}

		int i = 0;
		while (sEffectParamIDLookup[i].name != 0)
		{
			if (stricmp(semantic, sEffectParamIDLookup[i].name) == 0)
			{
				// bones
				// HACK till cg is fixed
				if (stricmp(semantic, "Bones") == 0 || stricmp(paramName, "bones") == 0)
				{
					mParameter.push_back(pair<EffectParamID, EffectParamHandle>(Bones, param));
					mSkin = true;
				}
				else
				{
					mParameter.push_back(pair<EffectParamID, EffectParamHandle>((EffectParamID)sEffectParamIDLookup[i].id, param));
				}

				switch (sEffectParamIDLookup[i].id)
				{
				case LightPosition:
				case LightDirection:
				case LightType:
				case LightDiffuseColour:
				case LightBrightness:
				case LightDecayType:
					mLight = true;
					break;

				case ProjectorProjection:
				case ProjectorView:
				case ProjectorViewProjection:
				case ProjectorViewProjectionInverseTranspose:
				case ProjectorViewInverseTranspose:
				case ProjectorViewInverse:
				case ProjectorWorldViewProjection:
				case ProjectorProjectionTexture:
				case ProjectorViewTexture:
				case ProjectorViewProjectionTexture:
				case ProjectorViewProjectionInverseTransposeTexture:
				case ProjectorViewInverseTransposeTexture:
				case ProjectorViewInverseTexture:
				case ProjectorWorldViewProjectionTexture:
					mProjector = true;
					break;

				case Time:
					mTime = true;
					break;

				case Bones:
					mSkin = true;
					break;
				}

				break;
			}

			++i;
		}

		param = (EffectParamHandle)cgGetNextParameter((CGparameter)param);
	}
}

EffectParam* OGLEffect::ReadParameter(Material* material, RenderFactory* factory, Device* device, const char* name, File* in)
{
	char buffer[256];

	// what about texture matrices?
	CGparameter cgParam = (CGparameter)GetParameterByName(name);
	if (!cgParam)
	{
		Log::Warning("Unexpected parameter %s in material %s\n", name, material->GetName().c_str());
		return 0;
	}

	CGtype type = GetParameterType(cgParam);

	EffectParam* param = 0;
	switch (type)
	{
	case CG_SAMPLER2D:
	case CG_SAMPLERRECT:
	case CG_SAMPLERCUBE:
		{
			CGannotation resType = GetAnnotationByName(cgParam, "ResourceType");

			const char* string = resType ? cgGetStringAnnotationValue(resType) : 0;

			param = new TextureParam((EffectParamHandle)cgParam);
			TextureParam* texParam = static_cast<TextureParam*>(param);

			Material::ReadLine(in, buffer);

			// check for render textures
			if (buffer[0] == '(' && buffer[strlen(buffer) - 1] == ')')
			{
				texParam->mName = buffer;
				texParam->mName = texParam->mName.substr(1, texParam->mName.length() - 2);
				break;
			}


			material->InterpretString(buffer);

			texParam->mName = buffer;

			if (string && stricmp("Cube", string) == 0) // cube map
			{
				int nothing = 0;
			}

				/*
				char* cBuffer[6];
				char name[1024] = "\0";

				for (int i = 0; i < 6; ++i)
				{
					cBuffer[i] = new char[256];

					if (i == 0)
						strcpy(cBuffer[i], buffer);
					else
						Material::ReadString(in, cBuffer[i]);

					strcat(name, cBuffer[i]);
				}

				texParam->texture = TextureMgr::LoadTexCube(cBuffer);

				for (int i = 0; i < 6; ++i)
					delete[] cBuffer[i];* /
			}
			else // normal texture* /
			{
				 
				//texParam->mTexture = (Texture*)factory->Create(Texture::Type);
				ObjectLoadParams p;//(factory, device, string(buffer));
				p.mFactory = factory;
				p.mDevice = device;
				p.mName = buffer;
				
				//texParam->mTexture->Load(&p);

				texParam->mTexture = (Texture*)factory->Load(Texture::Type, &p);
			}

			if (!texParam->mTexture)
			{
				delete param;
				return 0;
			}
		}
		break;

	case CG_FLOAT:
		{
			param = new FloatParam((EffectParamHandle)cgParam);
			FloatParam* floatParam = static_cast<FloatParam*>(param);
			Material::ReadFloat(in, floatParam->mData);
		}
		break;

	case CG_FLOAT2:
		{
			param = new Vector4Param((EffectParamHandle)cgParam);
			Vector4Param* float2Param = static_cast<Vector4Param*>(param);
			Material::ReadVector4(in, float2Param->mData);
		}
		break;

	case CG_FLOAT3:
		{
			param = new Vector4Param((EffectParamHandle)cgParam);
			Vector4Param* vec3Param = static_cast<Vector4Param*>(param);
			Material::ReadVector4(in, vec3Param->mData);
		}
		break;

	case CG_FLOAT4:
		{
			param = new Vector4Param((EffectParamHandle)cgParam);
			Vector4Param* vec4Param = static_cast<Vector4Param*>(param);
			Material::ReadVector4(in, vec4Param->mData);
		}
		break;
	}

	return param;
}

void OGLEffect::DestroyParameter(RenderFactory* factory, EffectParam* param)
{
	if (!param)
		return;

	switch (param->mType)
	{
	case ParamTexture:
		factory->id_RELEASE((SceneObject**)&(static_cast<TextureParam*>(param)->mTexture));
		break;
	}

	delete param;
}
*/

int	OGLEffect::Begin()
{
	int numPasses = 0;
	CGpass cgPass = cgGetFirstPass((CGtechnique)mCurrentTechnique->mHandle);
    while (cgPass) 
	{
        cgPass = cgGetNextPass(cgPass);
		++numPasses;
    }

	sPass = 0;
	return numPasses;
}

void OGLEffect::BeginPass(int idx)
{
	// need to set textures before cgSetPassState :|
	// cgSetPassState cant be done in SetPassParameter
	// as it overrides the device stats i set in the material
	Effect::SetPassParameter();

	sPassIdx = idx;
/*
	CGpass newPass = cgGetFirstPass((CGtechnique)mCurrentTechnique->mHandle); 
	for (int i = 0; i < sPassIdx; ++i)
	{
        newPass = cgGetNextPass(newPass);
		if (!newPass)
			break;
    }

	// this optimisation may be bad! if a texture is changed, this may need to be called
	if (sPass != newPass)
	{
		sPass = newPass;
		cgSetPassState(sPass);
	}*/
}

void OGLEffect::EndPass()
{
	//cgResetPassState(sPass);
	//sPass = cgGetNextPass(sPass);
}

void OGLEffect::End()
{
}

void OGLEffect::SetPassParameter()
{
	DeviceState state;
	mDevice->GetDeviceState(state);

	Effect::SetPassParameter();

	CGpass newPass = cgGetFirstPass((CGtechnique)mCurrentTechnique->mHandle); 
	for (int i = 0; i < sPassIdx; ++i)
	{
        newPass = cgGetNextPass(newPass);
		if (!newPass)
			break;
    }

	// this optimisation may be bad! if a texture is changed, this may need to be called
	//if (sPass != newPass)
	{
		sPass = newPass;
		cgSetPassState(sPass);
	}

	//if (idx == 0)
	//	sPass = cgGetFirstPass((CGtechnique)mCurrentTechnique->mHandle);

	mDevice->SetDeviceState(state);
}

void OGLEffect::ReloadResource()
{
	CGcontext sContext = ((OGLDevice*)mDevice)->GetContext();	

	string path;
	mFactory->GetParameter().GetExistingFile(mName, "cg", path);
	mEffect = sEffect[mName];
	mOrigonal = false;
	if (!mEffect)
	{
		mOrigonal = true;
		const char* args[] = {"-DOGL=1", 0};
		//mEffect = cgCreateEffectFromFile(sContext, path.c_str(), args);

		File shaderFile(path.c_str(), FAT_Read | FAT_Binary);
		if (!shaderFile.Valid())
		{
			Log::Error("[OGLEffect::Load] '%s' does not exist", path.c_str());
			return;
		}

		unsigned int size = shaderFile.Size() + 2;
		char* code = new char[size];
		memset(code, 0, size);
		shaderFile.Read(code, size - 2);
		mEffect = cgCreateEffect(sContext, code, args);
		delete[] code;
		if (!mEffect)
		{
			const char* listing = cgGetLastListing(sContext);
			Log::Error("Call to cgCreateEffectFromFile failed:\n%s\n\n", listing ? listing :  "");
			return;
		}
		sEffect[mName] = mEffect;
	}

	// reprocess techniques
	// NOTE: This assumes old techniauqes and the new techniques are all the same
	int i = 0;
	CGtechnique cgTechnique = cgGetFirstTechnique(mEffect);
    while (cgTechnique)
	{
        if (cgValidateTechnique(cgTechnique) == CG_FALSE)
			Log::Print("Technique %s did not validate\n", cgGetTechniqueName(cgTechnique));

		Assert(mTechnique[i]->mName == cgGetTechniqueName(cgTechnique));
		//EffectTechnique* technique = new EffectTechnique();
		//technique->mHandle = cgTechnique;
		mTechnique[i]->mHandle = cgTechnique;

        cgTechnique = cgGetNextTechnique(cgTechnique);
		++i;
    }

	// reprocess parameters
	i = 0;
	CGparameter cgParam = cgGetFirstEffectParameter(mEffect);
    while (cgParam)
	{
		const char* semantic = cgGetParameterSemantic(cgParam);
		Assert(mParameter[i]->mSemantic == semantic);

		mParameter[i]->mHandle = (EffectParameterHandle)cgParam;
		cgParam = cgGetNextParameter(cgParam);
		++i;
	}
}

void OGLEffect::StaticReloadResource()
{
	sEffect.clear();
}

/*
void OGLEffect::SetProjectorParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world)
{

}

void OGLEffect::SetMatrixParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world)
{
	// http://www.gignews.com/realtime020100.htm
	for (unsigned int p = 0; p < mParameter.size(); ++p)
	{
		pair<EffectParamID, EffectParamHandle>& pair = mParameter[p];

		switch (pair.first)
		{
		case EyePosition:
			{
				// can we extract this from the view matrix cheaply?
				SetVector4(pair.second, const_cast<Vector4&>(camWorld.GetTranslation()));
			}
			break;
		case EyeDirection:
			{
				Vector4 eyeDir = -view.GetZAxis();
				SetVector4(pair.second, eyeDir);
			}
			break;
		case View:
			{
				SetMatrix4(pair.second, view);
			}
			break;
		case ViewInverseTranspose:
			{
				Matrix4 viewIT = view;
				viewIT.Inverse();
				viewIT.Transpose();
				SetMatrix4(pair.second, viewIT);
			}
			break;
		case ViewInverse:
			{
				Matrix4 viewInv = view;
				viewInv.Inverse();
				SetMatrix4(pair.second, viewInv);
			}
			break;
		case World:
			{
				SetMatrix4(pair.second, world);
			}
			break;
		case WorldInverseTranspose:
			{
				Matrix4 worldIT = world;
				worldIT.Inverse();
				worldIT.Transpose();
				SetMatrix4(pair.second, worldIT);
			}
			break;
		case WorldView:
			{
				Matrix4 worldView = world * view;
				SetMatrix4(pair.second, worldView);
			}
			break;
		case WorldInverse:
			{
				Matrix4 worldInv = world;
				worldInv.Inverse();
				SetMatrix4(pair.second, worldInv);
			}
			break;
		case WorldViewInverseTranspose:
			{
				Matrix4 worldView = world * view;
				worldView.Inverse();
				worldView.Transpose();
				SetMatrix4(pair.second, worldView);
			}
			break;
		case WorldViewInverse:
			{
				Matrix4 worldViewInv = world * view;
				worldViewInv.Inverse();
				SetMatrix4(pair.second, worldViewInv);
			}
			break;
		case Projection:
			{
				SetMatrix4(pair.second, projection);
			}
			break;
		case WorldViewProjection:
			{
				Matrix4 worldViewProj = world * view * projection;
				SetMatrix4(pair.second, worldViewProj);
			}
			break;
		case WorldViewProjectionInverseTranspose:
			{
				Matrix4 worldViewProj = world * view * projection;
				worldViewProj.Inverse();
				worldViewProj.Transpose();
				SetMatrix4(pair.second, worldViewProj);
			}
			break;
		default:
			break;
		}
	}
}

void OGLEffect::SetLightParameters(Light* lights[], unsigned int numLights)
{
	if (!mLight)
		return;

	unsigned int posCount = 0;
	for (unsigned int p = 0; p < mParameter.size(); ++p)
	{
		pair<EffectParamID, EffectParamHandle>& pair = mParameter[p];

		switch (pair.first)
		{
		case LightPosition: // this should be in material->SetLights(Light* lights, numlights);?
			{
				if (posCount >= numLights)
					break;

				SetVector4(pair.second, lights[posCount]->GetPosition());
				++posCount;
			}
			break;

		default:
			break;
		}
	}
}

void OGLEffect::ReloadResource(RenderFactory* factory, Device* device)
{
	//if (!mOrigonal)
	//	return;

	Deinit();
	Load(&ObjectLoadParams(factory, device, mName));
}*/

void OGLEffect::SetValue(EffectParameter* param, const Matrix4* value, unsigned int count)
{
	CGparameter handle = static_cast<CGparameter>(param->mHandle);
	if (count <= 1)
		cgSetMatrixParameterfc(handle, (const float*)value[0].m);
	else
		cgGLSetMatrixParameterArrayfc(handle, 0, count, (const float*)value);
}

void OGLEffect::SetValue(EffectParameter* param, const float* value, unsigned int count)
{
	CGparameter handle = static_cast<CGparameter>(param->mHandle);
	if (count <= 1)
		cgSetParameter1f(handle, value[0]);
	else
		Assert(false);
}

void OGLEffect::SetValue(EffectParameter* param, Texture** value, unsigned int count)
{
	CGparameter handle = static_cast<CGparameter>(param->mHandle);
	if (count <= 1)
	{
		if (value && value[0])
			cgGLSetTextureParameter(handle, static_cast<OGLTexture*>(value[0])->GetHandle());
		else
			cgGLSetTextureParameter(handle, 0);
	}
	else
	{
		Assert(false);
	}
}

void OGLEffect::SetValue(EffectParameter* param, const Vector4* value, unsigned int count)
{
	CGparameter handle = static_cast<CGparameter>(param->mHandle);
	if (count <= 1)
		cgSetParameter4fv(handle, (const float*)&value[0]);
	else
		Assert(false);
}

void OGLEffect::SetValue(EffectParameter* param, const int* value, unsigned int count)
{
	CGparameter handle = static_cast<CGparameter>(param->mHandle);
	if (count <= 1)
		cgSetParameter1i(handle, value[0]);
	else
		Assert(false);
}

void OGLEffect::SetValue(EffectParameter* param, void* buffer, unsigned int size)
{
	Assert(false);
}
