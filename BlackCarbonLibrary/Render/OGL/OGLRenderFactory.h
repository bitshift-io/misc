#ifndef _OGL_RENDERFACTORY_H_
#define _OGL_RENDERFACTORY_H_

#include "../RenderFactory.h"

#undef CreateWindow

class Window;

class OGLRenderFactory : public RenderFactory
{
public:

	OGLRenderFactory(const RenderFactoryParameter& parameter);

	virtual void			Init();
	virtual void			Deinit();

	virtual Window*			CreateWindow();
	virtual void			ReleaseWindow(Window** window);

	virtual Device*			CreateDevice();
	virtual void			ReleaseDevice(Device** device);

	virtual void			ReloadResource(Device* device);

	virtual RenderFactoryType	GetRenderFactoryType()			{ return RFT_OpenGL; }

protected:

};

#endif
