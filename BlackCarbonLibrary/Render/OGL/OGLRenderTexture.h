#ifndef _OGL_RENDERTEXTURE_H_
#define _OGL_RENDERTEXTURE_H_

#include "Render/RenderTexture.h"
#include "OGLDevice.h"

class OGLTexture;

class OGLRenderTexture : public RenderTexture
{
public:

	virtual bool		Init(Device* device, RenderFactory* factory, int width, int height, unsigned int flags, int targetCount = 1);
	virtual void		Deinit();

	virtual int			Begin();
	virtual void		End();

	virtual void		SetViewport(const Vector4& topLeft, const Vector4& bottomRight, const Vector4& nearFarDepth = Vector4(0.f, 1.f))	{}
	virtual void		BindTarget(int index, bool clear = true, int flags = RTF_Colour | RTF_Depth | RTF_Stencil);
	virtual void		SetClearColour(const Vector4& colour);
	virtual void		SetClearDepth(float depth);
	virtual void		SetClearStencil(unsigned int stencil);

	virtual Texture*	GetTexture(RenderTextureFlags type, int lockFlags = 0);

	void				CheckFramebufferStatus();

	virtual int			GetWidth()												{ return mWidth; }
	virtual int			GetHeight()												{ return mHeight; }

protected:

	int			mTargetCount;
	OGLTexture*	mColour;
	OGLTexture*	mDepth;
	OGLTexture*	mStencil;

	GLuint		mFrameBuffer;  // color render target
	//GLuint	depth; // depth render target
	//GLuint	stencil; // depth render target

	int			mWidth;
	int			mHeight;

	Vector4		mClearColour;
	float		mClearDepth;
	unsigned int mClearStencil;

	GLenum		mTarget;
};

#endif
