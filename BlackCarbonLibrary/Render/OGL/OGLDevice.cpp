#include "Render/Geometry.h"

#include "OGLDevice.h"
#include "OGLRenderBuffer.h"
#include "File/Log.h"
#include "Render/Material.h"
#include "Render/TGA.h"

#ifdef WIN32
	#include "Render/Win32/Win32Window.h"
#elif __APPLE__
    #include "Render/Mac/MacWindow.h"
#else
	#include "Render/Linux/LinuxWindow.h"
#endif

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLu32.lib")
//#pragma comment(lib, "GLaux.lib")

#pragma comment(lib, "cg.lib")
#pragma comment(lib, "cgGL.lib")

#ifdef __APPLE__
    #define LoadGLExtension(device, f)
#else
    #define LoadGLExtension(device, f)							\
    {				                                            \
        void **p = reinterpret_cast<void**>(&f);				\
        *p = device->GetExtensionAddress(#f);					\
    }
#endif

void ErrorCallback()
{
	const char* error = cgGetErrorString(cgGetError());
	Log::Warning("%s\n", error);
}

bool OGLDevice::Init(RenderFactory* factory, Window* window, int multisample)
{
	mMultisample = multisample;

	bool result = InitInternal(factory, window);
	if (!result)
		return false;

#ifdef WIN32
	// recreate window and ogl device but multisampled :)
	if (multisample != 0)
	{
		int pixelFormat = GetMultisamplePixelFormat(multisample);
		mWindow->Deinit();
		WindowResolution res;
		mWindow->GetWindowResolution(res);
		mWindow->Init(mWindow->GetTitle(), &res, mWindow->IsFullScreen());
		return InitInternal(factory, window, pixelFormat);
	}
#endif

	return true;
}

bool OGLDevice::InitInternal(RenderFactory* factory, Window* window, int pixelFormat)
{
	Log::Print("[OGLDevice::InitInternal] Attempting to Init OGL device\n");

	mWorld.SetIdentity();
	mView.SetIdentity();
	mProjection.SetIdentity();

#ifdef WIN32
	mWindow = (Win32Window*)window;

	static PIXELFORMATDESCRIPTOR pfd =				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		32,											// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		16,											// 16Bit Z-Buffer (Depth Buffer)
		1,											// Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};

	if (!(mHDC = GetDC(mWindow->GetHandle())))							// Did We Get A Device Context?
	{
		MessageBoxW(NULL, (LPCWSTR)"Can't Create A GL Device Context.", (LPCWSTR)"ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;
	}

	if (pixelFormat == -1)
	{
		if (!(pixelFormat = ChoosePixelFormat(mHDC, &pfd)))	// Did Windows Find A Matching Pixel Format?
		{
			MessageBoxW(NULL, (LPCWSTR)"Can't Find A Suitable PixelFormat.", (LPCWSTR)"ERROR", MB_OK|MB_ICONEXCLAMATION);
			return FALSE;								// Return FALSE
		}
	}

	if (!SetPixelFormat(mHDC, pixelFormat, &pfd))		// Are We Able To Set The Pixel Format?
	{
		MessageBoxW(NULL, (LPCWSTR)"Can't Set The PixelFormat.", (LPCWSTR)"ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(mHRC = wglCreateContext(mHDC)))				// Are We Able To Get A Rendering Context?
	{
		MessageBoxW(NULL, (LPCWSTR)"Can't Create A GL Rendering Context.", (LPCWSTR)"ERROR", MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!wglMakeCurrent(mHDC, mHRC))					// Try To Activate The Rendering Context
	{
		MessageBoxW(NULL, (LPCWSTR)"Can't Activate The GL Rendering Context.", (LPCWSTR)"ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

#elif __APPLE__

    mWindow = (MacWindow*)window;
    mOriginalMode = CGDisplayCurrentMode(kCGDirectMainDisplay);

    GLint WindowedAttributes[]= {
				AGL_RGBA,
				AGL_DOUBLEBUFFER,
				AGL_ACCELERATED,
				AGL_DEPTH_SIZE, 24,
				AGL_STENCIL_SIZE, 8,
				AGL_NO_RECOVERY,
				AGL_SINGLE_RENDERER,
				AGL_NONE
				};

	CGDirectDisplayID mainID = CGMainDisplayID();
	GDHandle mainDevice;
	OSErr err = DMGetGDeviceByDisplayID ((DisplayIDType)mainID, &mainDevice, true);

	AGLPixelFormat fmt = aglChoosePixelFormat(&mainDevice, 1, WindowedAttributes);

	if ((fmt == nil) || (aglGetError() != AGL_NO_ERROR))
		Log::Error("Cannot make a draw context!");

	mOGLContext = aglCreateContext(fmt, nil);
	aglDestroyPixelFormat(fmt);

	if ((mWindow->GetWindowRef() == nil) || (aglGetError() != AGL_NO_ERROR))
		Log::Error("aglCreateContext failed!");

	mDrawable = (AGLDrawable)GetWindowPort(mWindow->GetWindowRef());
/*
	if (_firstFrame)
	{
		GSEventOpenGLContextCreated();
		GSOpenGLContextCreatedEventSent = true;
		_firstFrame = false;
		_lastFrameTime = CFAbsoluteTimeGetCurrent();
	}

	Rect bounds;
	// kWindowStructureRgn or kWindowContentRgn.
	WindowRegionCode regionCode = kWindowContentRgn;
	GetWindowBounds( _windowRef, regionCode, &bounds);
	CGSize contextSize = CGSizeMake(bounds.right - bounds.left, bounds.bottom - bounds.top);
	GSEventOpenGLContextResized((int)contextSize.width, (int)contextSize.height);
	GSEventOpenGLContextStateInvalidated();
*/


    aglSetDrawable(mOGLContext, mDrawable);
    aglSetCurrentContext(mOGLContext);

    GLint swap = 1;
    aglSetInteger(mOGLContext, AGL_SWAP_INTERVAL, &swap);

#else
    mWindow = (LinuxWindow*)window;
#endif

	CheckError();

	SetClearColour(Vector4(0.f, 0.f, 0.f, 0.f));

	// set up default device state
	SetDeviceState(DeviceState(), true);

    bool result = InitExtension();
	Log::Print("[OGLDevice::InitInternal] Init OGL device complete\n");
	return result;
}

void* OGLDevice::GetExtensionAddress(const char* extension)
{
#ifdef WIN32
	return (void*)wglGetProcAddress(extension);
#elif UNIX
    using namespace Linux;
	return (void*)glXGetProcAddress((const GLubyte*)extension);
	//return 0;
#endif
}

bool OGLDevice::InitExtension()
{
	Log::Print("[OGLDevice::InitExtension] Attempting to Init OGL Extensions\n");

#ifdef DEBUG
	//if (ExtensionSupported("GL_GREMEDY_string_marker"))
	{
	    LoadGLExtension(this, glStringMarkerGREMEDY);
        Log::Print("GREMEDY String Marker\t\t\t[OK]\n");
	}
#endif


	if (ExtensionSupported("GL_ARB_vertex_buffer_object"))
	{
#ifdef WIN32
		LoadGLExtension(this, glClientActiveTexture);
#endif
		LoadGLExtension(this, glBindBufferARB);
		LoadGLExtension(this, glGenBuffersARB);
		LoadGLExtension(this, glBufferDataARB);
		LoadGLExtension(this, glDeleteBuffersARB);
		LoadGLExtension(this, glMapBufferARB);
		LoadGLExtension(this, glUnmapBufferARB);
	}

	if (ExtensionSupported("GL_EXT_framebuffer_object"))
	{
		LoadGLExtension(this, glBindFramebufferEXT);
		LoadGLExtension(this, glBindRenderbufferEXT);
		LoadGLExtension(this, glCheckFramebufferStatusEXT);

		LoadGLExtension(this, glDeleteFramebuffersEXT);
		LoadGLExtension(this, glDeleteRenderbuffersEXT);
		LoadGLExtension(this, glFramebufferRenderbufferEXT);

		LoadGLExtension(this, glFramebufferTexture1DEXT);
		LoadGLExtension(this, glFramebufferTexture2DEXT);
		LoadGLExtension(this, glFramebufferTexture3DEXT);

		LoadGLExtension(this, glGenFramebuffersEXT);
		LoadGLExtension(this, glGenRenderbuffersEXT);
		LoadGLExtension(this, glGenerateMipmapEXT);

		LoadGLExtension(this, glGetFramebufferAttachmentParameterivEXT);
		LoadGLExtension(this, glGetRenderbufferParameterivEXT);
		LoadGLExtension(this, glIsFramebufferEXT);

		LoadGLExtension(this, glIsRenderbufferEXT);
		LoadGLExtension(this, glRenderbufferStorageEXT);
	}

	Log::Print("[OGLDevice::InitExtension] Attempting to Init CG\n");

	// attempt to init cg library
	mContext = cgCreateContext();

	cgSetErrorCallback(ErrorCallback);
	cgGLRegisterStates(mContext);

	CGprofile cgVertexProfile = cgGLGetLatestProfile(CG_GL_VERTEX);				// Get The Latest GL Vertex Profile

	// Validate Our Profile Determination Was Successful
	if (cgVertexProfile == CG_PROFILE_UNKNOWN)
	{
		Log::Error("[OGLDevice::InitExtension] CG not supported\n");
		return FALSE;													// We Cannot Continue
	}

/*
	if (!cgGLIsProfileSupported(CG_PROFILE_ARBVP1) || !cgGLIsProfileSupported(CG_PROFILE_ARBFP1))
	{
		Log::Error("[OGLDevice::InitExtension] CG not supported\n");
		cgDestroyContext(mContext);
		mContext = 0;
		return false;
	}

	cgGLSetOptimalOptions(CG_PROFILE_ARBVP1);
	cgGLSetOptimalOptions(CG_PROFILE_ARBFP1);*/
	return true;
}

void OGLDevice::Deinit(RenderFactory* factory, Window* window)
{
#ifdef WIN32
	if (mHRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBoxW(NULL,(LPCWSTR)"Release Of DC And RC Failed.",(LPCWSTR)"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(mHRC))						// Are We Able To Delete The RC?
		{
			MessageBoxW(NULL,(LPCWSTR)"Release Rendering Context Failed.",(LPCWSTR)"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		mHRC = NULL;										// Set RC To NULL
	}

	if (mHDC && !ReleaseDC(mWindow->GetHandle(), mHDC))					// Are We Able To Release The DC
	{
		MessageBoxW(NULL,(LPCWSTR)"Release Device Context Failed.",(LPCWSTR)"SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		mHDC = NULL;										// Set DC To NULL
	}
#else

#endif

	cgDestroyContext(mContext);
}

void OGLDevice::Begin()
{
	Device::Begin();

	WindowResolution res;
	mWindow->GetWindowResolution(res);
	ResetDevice(res.width, res.height);

	CheckError();

	glClearStencil(0);
	glClear(GL_STENCIL_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	CheckError();
}

void OGLDevice::End()
{
	CheckError();

	glDepthMask(GL_TRUE); // hack, needs to be enabled for the clear to work 0.o driver bug?

#ifdef WIN32
	SwapBuffers(mHDC);
#elif __APPLE__
    aglSwapBuffers(mOGLContext);
#elif UNIX
    glXSwapBuffers(mWindow->mDisplay, mWindow->mWindow);
#endif
	CheckError();
}

RenderBuffer* OGLDevice::CreateRenderBuffer()
{
	RenderBuffer* buff = new OGLRenderBuffer;
	mRenderBuffer.push_back(buff);
	return buff;
}

void OGLDevice::DestroyRenderBuffer(RenderBuffer** buffer)
{
	if (*buffer == 0)
		return;

	(*buffer)->Deinit();
	mRenderBuffer.remove(*buffer);

	delete *buffer;
	*buffer = 0;
}

int OGLDevice::ExtensionSupported(const char* extension)
{
	const GLubyte *extensions = glGetString(GL_EXTENSIONS);
	if (!extensions)
		return 0;

	return FindExtension(extension, (const char*)extensions);
}

int OGLDevice::FindExtension(const char* extension, const char* extensions)
{
	char *where, *terminator;

	// Extension names should not have spaces.
	where = (char*)strchr(extension, ' ');
	if (where || *extension == '\0')
	{
		return 0;
	}

	// It takes a bit of care to be fool-proof about parsing the
	// OpenGL extensions string. Don't be fooled by sub-strings
	const char *start = extensions;
	while(1)
	{
		where = (char*) strstr((const char *) start, extension);
		if (!where)
		{
			break;
		}

		terminator = where + strlen(extension);
		if (where == start || *(where - 1) == ' ')
		{
			if (*terminator == ' ' || *terminator == '\0')
			{
				return 1;
			}
		}

		start = terminator;
	}

	return 0;
}

bool OGLDevice::Draw(Geometry* geometry, unsigned int startIndex, unsigned int numIndices, int instanceCount)
{
	SetDeviceStateInternal();

	VertexFrame& frame = geometry->GetVertexFrame(0);
	RenderBuffer* idxBuffer = geometry->GetIndexBuffer();

	if (idxBuffer->GetSize() == 0)
		return false;

	CheckError();
	BEGINDEBUGEVENT(this, "OGLDevice::Draw", Vector4(1.f, 1.f, 1.f, 1.f));

	idxBuffer->Bind(VF_Index);
	int index = 0;
	int vertexFormat = mCurrentDeviceState.vertexFormat;

	if (frame.mPosition && (vertexFormat & VF_Position))
		frame.mPosition->Bind(VF_Position);

	if (frame.mNormal && (vertexFormat & VF_Normal))
		frame.mNormal->Bind(VF_Normal);

	if (frame.mColour && (vertexFormat & VF_Colour))
		frame.mColour->Bind(VF_Colour);

	if (frame.mTangent && (vertexFormat & VF_Tangent))
		frame.mTangent->Bind(VF_Tangent, index++);

	if (frame.mBinormal && (vertexFormat & VF_Binormal))
		frame.mBinormal->Bind(VF_Binormal, index++);

	if (frame.mBoneIndex && frame.mBoneWeight && (vertexFormat & VF_BoneData))
	{
		frame.mBoneIndex->Bind(VF_BoneData, index++);
		frame.mBoneWeight->Bind(VF_BoneData, index++);
	}

	if ((vertexFormat & VF_TexCoord0) && frame.mTexCoord[0])
		frame.mTexCoord[0]->Bind(VF_TexCoord0, index++);

	CheckError();
	idxBuffer->Draw(startIndex, numIndices);
	CheckError();

	if (frame.mPosition && (vertexFormat & VF_Position))
		frame.mPosition->Unbind();

	CheckError();

	if (frame.mNormal && (vertexFormat & VF_Normal))
		frame.mNormal->Unbind();

	if (frame.mColour && (vertexFormat & VF_Colour))
		frame.mColour->Unbind();

	if (frame.mTangent && (vertexFormat & VF_Tangent))
		frame.mTangent->Unbind();

	if (frame.mBinormal && (vertexFormat & VF_Binormal))
		frame.mBinormal->Unbind();

	if (frame.mBoneIndex && frame.mBoneWeight && (vertexFormat & VF_BoneData))
	{
		frame.mBoneIndex->Unbind();
		frame.mBoneWeight->Unbind();
	}

	if ((vertexFormat & VF_TexCoord0) && frame.mTexCoord[0])
		frame.mTexCoord[0]->Unbind();

	CheckError();
	ENDDEBUGEVENT(this);

	return Device::Draw(geometry, startIndex, numIndices == -1 ? idxBuffer->GetSize() : numIndices, instanceCount);
}

void OGLDevice::SetMatrix(const Matrix4& matrix, MatrixMode mode)
{
	switch (mode)
	{
	case MM_View:
		{
			mView = matrix;

			glMatrixMode(GL_MODELVIEW);
			glLoadMatrixf((mView * mWorld).m);
		}
		break;

	case MM_Projection:
		{
			mProjection = matrix;

			glMatrixMode(GL_PROJECTION);
			glLoadMatrixf(mProjection.m);
		}
		break;

	case MM_Model:
		{
			mWorld = matrix;

			glMatrixMode(GL_MODELVIEW);
			glLoadMatrixf((mView * mWorld).m);
		}
		break;
	}
}

void OGLDevice::DrawPoint(const Vector4& p, const Vector4& colour)
{
	glColor4f(colour.x, colour.y, colour.z, 1.0f);
	glBegin(GL_POINTS);
	glVertex3f(p.x, p.y, p.z);
	glEnd();
	glColor4f(1.f, 1.f, 1.f, 1.f);
}

void OGLDevice::DrawLine(const Vector4& p1, const Vector4& p2, const Vector4& colour)
{
	glColor4f(colour.x, colour.y, colour.z, 1.0f);
	glBegin(GL_LINES);
	glVertex3f(p1.x, p1.y, p1.z);
	glVertex3f(p2.x, p2.y, p2.z);
	glEnd();
	glColor4f(1.f, 1.f, 1.f, 1.f);
}

void OGLDevice::ResetDevice(int width, int height)
{
    // should this go in the camera code also?
	glViewport(0, 0, width, height);
}

Window* OGLDevice::GetWindow()
{
	return mWindow;
}

void OGLDevice::ReloadResource()
{
	list<RenderBuffer*>::iterator it;
	for (it = mRenderBuffer.begin(); it != mRenderBuffer.end(); ++it)
	{
		(*it)->ReloadResource(this);
	}
}

void OGLDevice::DrawStencil()
{
	WindowResolution res;
	mWindow->GetWindowResolution(res);
	int width = res.width;
	int height = res.height;
	unsigned char* stencil = new unsigned char[width * height];
	unsigned char* colour = new unsigned char[width * height * 3];

	memset(stencil, 0, sizeof(char) * width * height);

	glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, colour);
	glReadPixels(0, 0, width, height, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, stencil);


	// blend in the stencil with the colour
	for (int h = 0; h < height; ++h)
	{
		for (int w = 0; w < width; ++w)
		{
			colour[(w + h * width) * 3 + 0] = colour[(w + h * width) * 3 + 0] / 2 + stencil[w + h * width] * 50;
			colour[(w + h * width) * 3 + 1] = colour[(w + h * width) * 3 + 1] / 2 + stencil[w + h * width] * 50;
			colour[(w + h * width) * 3 + 2] = colour[(w + h * width) * 3 + 2] / 2 + stencil[w + h * width] * 50;
		}
	}

	memset(colour, 0xff, sizeof(char) * width * height * 3);

	glDrawPixels(width, height, GL_RGB, GL_UNSIGNED_BYTE, colour);

	delete[] stencil;
	delete[] colour;
}

void OGLDevice::SaveStencil()
{/*
	int width = mWindow->GetWidth();
	int height = mWindow->GetHeight();
	unsigned char* stencil = new unsigned char[width * height];
	unsigned char* colour = new unsigned char[width * height * 3];
	unsigned char* combine = new unsigned char[width * height * 4];

	glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, colour);
	glReadPixels(0, 0, width, height, GL_STENCIL_INDEX, GL_UNSIGNED_BYTE, stencil);

	memset(combine, 0, sizeof(char) * width * height * 4);

	// blend in the stencil with the colour
	for (int h = 0; h < height; ++h)
	{
		for (int w = 0; w < width; ++w)
		{
			combine[(w + h * width) * 4 + 0] = colour[(w + h * width) * 3 + 2];
			combine[(w + h * width) * 4 + 1] = colour[(w + h * width) * 3 + 1];
			combine[(w + h * width) * 4 + 2] = colour[(w + h * width) * 3 + 0];
			combine[(w + h * width) * 4 + 3] = stencil[w + h * width];
		}
	}

	TGA tga;
	tga.Save("stencil.tga", combine, width, height, 32);

	delete[] stencil;
	delete[] colour;
	delete[] combine;*/
}

void OGLDevice::CheckError()
{
#ifndef __APPLE__
	GLenum err = glGetError();
    while (err != GL_NO_ERROR)
	{
			Log::Warning("[OGLDevice::CheckError] %s\n", (char *)gluErrorString(err));
            err = glGetError();
    }
#endif
}

Vector4	OGLDevice::GetClearColour()
{
	Vector4 clearColour;
	glGetFloatv(GL_COLOR_CLEAR_VALUE, (GLfloat*)&clearColour);
	return clearColour;
}

void OGLDevice::SetClearColour(const Vector4& colour)
{
    glClearColor(colour.vec[0], colour.vec[1], colour.vec[2], colour.vec[3]);
}

void OGLDevice::BeginDebugEvent(const string& name, const Vector4& colour)
{
	mDebugString.push_back(name);
	char buffer[1024];
	sprintf(buffer, "[START] %s\n", name.c_str());

	if (glStringMarkerGREMEDY)
		glStringMarkerGREMEDY(strlen(buffer), buffer);
}

void OGLDevice::EndDebugEvent()
{
	string& name = mDebugString.back();
	char buffer[1024];
	sprintf(buffer, "[END] %s\n", name.c_str());

	if (glStringMarkerGREMEDY)
		glStringMarkerGREMEDY(strlen(buffer), buffer);

	mDebugString.pop_back();
}

#ifdef WIN32
int OGLDevice::GetMultisamplePixelFormat(int multisample)
{
	int		pixelFormat;
	int		valid;
	UINT	numFormats;
	float	fAttributes[] = {0,0};
	const char* extensions = NULL;

	// Try To Use wglGetExtensionStringARB On Current DC, If Possible
	//PROC wglGetExtString = wglGetProcAddress("wglGetExtensionsStringARB");
	LoadGLExtension(this, wglGetExtensionsStringARB);
	if (!wglGetExtensionsStringARB)
	{
		Log::Warning("Multisampling not supported");
		return -1;
	}

	extensions = wglGetExtensionsStringARB(mHDC);
	//	((char*(__stdcall*)(HDC))wglGetExtString)(wglGetCurrentDC());

	// See If The String Exists In WGL!
	if (!FindExtension("WGL_ARB_multisample", extensions))
		return -1;

	LoadGLExtension(this, wglChoosePixelFormatARB);

	// These Attributes Are The Bits We Want To Test For In Our Sample
	// Everything Is Pretty Standard, The Only One We Want To
	// Really Focus On Is The SAMPLE BUFFERS ARB And WGL SAMPLES
	// These Two Are Going To Do The Main Testing For Whether Or Not
	// We Support Multisampling On This Hardware.
	int iAttributes[] =
	{
		WGL_DRAW_TO_WINDOW_ARB,GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB,GL_TRUE,
		WGL_ACCELERATION_ARB,WGL_FULL_ACCELERATION_ARB,
		WGL_COLOR_BITS_ARB,32,
		WGL_ALPHA_BITS_ARB,8,
		WGL_DEPTH_BITS_ARB,16,
		WGL_STENCIL_BITS_ARB,1,
		WGL_DOUBLE_BUFFER_ARB,GL_TRUE,
		WGL_SAMPLE_BUFFERS_ARB,GL_TRUE,
		WGL_SAMPLES_ARB, multisample,
		0,0
	};

	// if we cant get a valid  size, check the next lowest
	do
	{
		iAttributes[19] = multisample;
		valid = wglChoosePixelFormatARB(mHDC, iAttributes, fAttributes, 1, &pixelFormat, &numFormats);

		// If We Returned True, And Our Format Count Is Greater Than 1
		if (valid && numFormats >= 1)
			return pixelFormat;

		multisample /= 2;
	} while (multisample >= 2);

	iAttributes[19] = 0;
	valid = wglChoosePixelFormatARB(mHDC, iAttributes, fAttributes, 1, &pixelFormat, &numFormats);

	// If We Returned True, And Our Format Count Is Greater Than 1
	if (valid && numFormats >= 1)
		return pixelFormat;

	return -1;
}
#endif

bool OGLDevice::SetDeviceState(const DeviceState& state, bool force)
{
	mDesiredDeviceState = state;

	if (force)
		SetDeviceStateInternal(force);

	return true;
}

bool OGLDevice::SetDeviceStateInternal(bool force)
{
	// matches enum Compare
	int compareLookup[] =
	{
		GL_LESS,
		GL_LEQUAL,
		GL_EQUAL,
		GL_GREATER,
		GL_GEQUAL,
		GL_ALWAYS,
		GL_NEVER,
		GL_NOTEQUAL,
	};

	// matches Operation enum
	int operationLookup[] =
	{
		GL_ZERO,
		GL_KEEP,
		GL_REPLACE,
		GL_INCR,
		GL_DECR,
		GL_INVERT,
	};

	// matches blendmode enum
	int blendModeLookup[] =
	{
		GL_ZERO,
		GL_ONE,
		GL_DST_COLOR,
		GL_SRC_COLOR,
		GL_ONE_MINUS_DST_COLOR,
		GL_ONE_MINUS_SRC_COLOR,
		GL_SRC_ALPHA,
		GL_ONE_MINUS_SRC_ALPHA,
		GL_DST_ALPHA,
		GL_ONE_MINUS_DST_ALPHA,
		GL_SRC_ALPHA_SATURATE,
	};

	DeviceState& state = mDesiredDeviceState;
	DeviceState& prevState = mCurrentDeviceState;

	if (force || state.cullFace != prevState.cullFace)
	{
		switch (state.cullFace)
		{
		case CF_None:
			glDisable(GL_CULL_FACE);
			break;

		case CF_Back:
			glEnable(GL_CULL_FACE);
			glCullFace(GL_FRONT);
			break;

		case CF_Front:
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			break;
		}
	}

	// alpha
	// OGL doesnt like to remember this state!
	//if (force || state.alphaBlendEnable != prevState.alphaBlendEnable)
	{
		if (state.alphaBlendEnable)
			glEnable(GL_BLEND);
		else
			glDisable(GL_BLEND);
	}

	if (force || state.alphaBlendEnable)
	{
		if (force || state.alphaBlendSource != prevState.alphaBlendSource || state.alphaBlendDest != prevState.alphaBlendDest)
		{
			//glBlendFunc(LookupBlendMode(state.alphaBlendSource), LookupBlendMode(state.alphaBlendDest));
			glBlendFunc(blendModeLookup[state.alphaBlendSource], blendModeLookup[state.alphaBlendDest]);
		}
	}

	// depth
	if (force || state.depthCompare != prevState.depthCompare)
	{
		glDepthFunc(compareLookup[state.depthCompare]);
	}

	if (force || state.depthTestEnable != prevState.depthTestEnable)
	{
		if (state.depthTestEnable)
			glEnable(GL_DEPTH_TEST);
		else
			glDisable(GL_DEPTH_TEST);
	}

	if (force || state.depthWriteEnable != prevState.depthWriteEnable)
	{
		if (state.depthWriteEnable)
			glDepthMask(GL_TRUE);
		else
			glDepthMask(GL_FALSE);
	}

	// stencil
	if (force || prevState.stencilEnable != state.stencilEnable)
	{
		if (state.stencilEnable)
			glEnable(GL_STENCIL_TEST);
		else
			glDisable(GL_STENCIL_TEST);
	}

	if (force || state.stencilEnable)
	{
		if (force || state.stencilFace[CF_Front].compare != prevState.stencilFace[CF_Front].compare
			|| state.stencilReadMask != prevState.stencilReadMask
			|| state.stencilWriteMask != prevState.stencilWriteMask)
		{
			glStencilFunc(compareLookup[state.stencilFace[CF_Front].compare], state.stencilReadMask, state.stencilWriteMask);
		}

		if (force || state.stencilFace[CF_Front].fail != prevState.stencilFace[CF_Front].fail
			|| state.stencilFace[CF_Front].depthFail != prevState.stencilFace[CF_Front].depthFail
			|| state.stencilFace[CF_Front].pass != prevState.stencilFace[CF_Front].pass)
		{
			glStencilOp(operationLookup[state.stencilFace[CF_Front].fail], operationLookup[state.stencilFace[CF_Front].depthFail],
				operationLookup[state.stencilFace[CF_Front].pass]);
		}
	}

	mCurrentDeviceState = mDesiredDeviceState;
	return true;
}

bool OGLDevice::GetDeviceState(DeviceState& state)
{
	state = mDesiredDeviceState;
	return true;
}

