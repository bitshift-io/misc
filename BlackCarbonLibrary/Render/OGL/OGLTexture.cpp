#include "OGLTexture.h"
#include "Render/TGA.h"
#include "Render/RenderFactory.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

bool OGLTexture::IsInstance(const ObjectLoadParams* params, SceneObject* loaded)
{
	if (strcmpi(params->mName.c_str(), mName.c_str()) == 0)
		return true;

	return false;
}

void OGLTexture::Create(RenderFactory* factory)
{
	mFactory = factory;
}

bool OGLTexture::Load(const ObjectLoadParams* params, SceneNode* node)
{
	mDevice = params->mDevice;
	mFactory = params->mFactory;

	mName = params->mName;

	string path;
	if (params->mFactory->GetParameter().GetExistingFile(params->mName, "hdr", path))
	{
		/*
		HDR hdr;
		if (!hdr.Load(path.c_str()))
			return false;

		if (hdr.GetResourceInfo(0)->width == (hdr.GetResourceInfo(0)->height * 6))
		{
			hdr.FlipVertical(hdr.GetResourceInfo(0));
			bool result = GenerateCubeTexture(d3dDevice, &hdr, params->mFlags);
			hdr.Deinit();
			return result;
		}

		bool result = GenerateTexture(d3dDevice, &hdr, params->mFlags);
		hdr.Deinit();
		return result;*/
		return false;
	}
	else
	{
		TGA tga;
		params->mFactory->GetParameter().GetExistingFile(params->mName, "tga", path);

		if (!tga.Load(path.c_str()))
			return false;

		tga.ConvertToFormat(TF_RGBA);

		if (tga.GetResourceInfo(0)->width == (tga.GetResourceInfo(0)->height * 6))
		{
			tga.FlipVertical(tga.GetResourceInfo(0));
			bool result = GenerateCubeTexture(static_cast<OGLDevice*>(params->mDevice), &tga, params->mFlags);
			tga.Deinit();
			return result;
		}

		bool result = GenerateTexture(static_cast<OGLDevice*>(params->mDevice), &tga, params->mFlags);
		tga.Deinit();
		return result;
	}

	return false;
}

void OGLTexture::ReloadResource()
{
	Deinit();
	Load(&ObjectLoadParams(mFactory, mDevice, mName));
}

bool OGLTexture::Init(Device* device, unsigned char* image, int channels, int width, int height, int depth, int flags)
{
	mDevice = device;

	mTarget = GL_TEXTURE_2D;

	glGenTextures(1, &mTexture);
	glBindTexture(mTarget, mTexture);

	glTexParameteri(mTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(mTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(mTarget, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);

	glTexImage2D(mTarget, 0, channels == 3 ? GL_RGB : GL_RGBA, width, height, 0, channels == 3 ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, image);
	glBindTexture(mTarget, 0);
	return true;
}

bool OGLTexture::Update(unsigned char* data)
{
	return false;
}

bool OGLTexture::GenerateTexture(OGLDevice* device, TextureResource* texRes, int flags)
{
	TextureSubresource* resource = texRes->GetResourceInfo(0);

	mTarget = GL_TEXTURE_2D;

	glGenTextures(1, &mTexture);
	glBindTexture(mTarget, mTexture);
/*
	if (!(flags & TF_LowDynamicRange) || flags & TF_VertexTexture)
	{
		glTexParameteri(mTarget, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(mTarget, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
	else*/
	{
		glTexParameteri(mTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(mTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

	//if (!(flags & TF_NoMipMaps))
		glTexParameteri(mTarget, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);

	glTexImage2D(mTarget, 0, GL_RGBA, resource->width, resource->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, resource->pixel);
	glBindTexture(mTarget, 0);
	return true;
}

bool OGLTexture::GenerateCubeTexture(OGLDevice* device, TextureResource* texRes, int flags)
{
	texRes->GenerateCubemap();
	mTarget = GL_TEXTURE_CUBE_MAP;

	glGenTextures(1, &mTexture);
	glBindTexture(mTarget, mTexture);

	glTexParameteri(mTarget, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(mTarget, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(mTarget, GL_TEXTURE_WRAP_R, GL_REPEAT);

	glTexParameteri(mTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(mTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//if (!(flags & TF_NoMipMaps))
		glTexParameteri(mTarget, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);

	//FlipV(image, resource->width, height, bpp);

	for (int i = 0; i < 6; ++i)
	{
		TextureSubresource* resource = texRes->GetResourceInfo(i);

		GLenum target;
		switch (i)
		{
		case 0:
			target = GL_TEXTURE_CUBE_MAP_POSITIVE_X;
			break;
		case 1:
			target = GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
			break;
		case 2:
			target = GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
			break;
		case 3:
			target = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
			break;
		case 4:
			target = GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
			break;
		case 5:
			target = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
			break;
		}

		glTexImage2D(target, 0, GL_RGBA, resource->width, resource->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, resource->pixel);
	}

	glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP);
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP);
	glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP);

	return true;
}

void OGLTexture::Deinit()
{
	glDeleteTextures(1, &mTexture);
}

void OGLTexture::DebugDraw(float x, float y, float size)
{
	glMatrixMode(GL_PROJECTION); // and initalize Ortho mode
  	glPushMatrix();
  	glLoadIdentity();
  	glOrtho(0.0f, 800,600, 0.0f,-1.0f,1.0f);

  	glMatrixMode(GL_MODELVIEW);
  	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS);
	glDepthMask(GL_FALSE);

	//glActiveTextureARB(GL_TEXTURE0_ARB + 0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, mTexture);

	glColor3f(1, 1, 1);

	glBegin(GL_QUADS);
	{
		glTexCoord2f(0, 1);
		glVertex2i(0 + x, 0 + y);

		glTexCoord2f(0, 0);
		glVertex2i(0 + x, size + y);

		glTexCoord2f(1, 0);
		glVertex2i(int(size * (800.0f/600.0f) + x), size + y);

		glTexCoord2f(1, 1);
		glVertex2i(int(size * (800.0f/600.0f) + x), 0 + y);
	}
	glEnd();

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);

	glMatrixMode(GL_PROJECTION);
  	glPopMatrix();
  	glMatrixMode(GL_MODELVIEW);
  	glPopMatrix();
}
