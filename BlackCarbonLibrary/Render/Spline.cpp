#include "Spline.h"
#include "Device.h"
#include "Memory/Memory.h"

void Spline::Destroy(const RenderFactory* factory)
{
	mSpline.resize(0);
}

bool Spline::Save(const File& out, SceneNode* node)
{
	// write number of splines
	unsigned int size = mSpline.size();
	out.Write(&size, sizeof(unsigned int));

	for (unsigned int i = 0; i < mSpline.size(); ++i)
	{
		Spline3& spline = mSpline[i];

		// write the number of knots
		size = spline.mCount;
		out.Write(&size, sizeof(unsigned int));

		// write the knots
		if (out.Write(spline.mKnot, sizeof(Knot3) * size) != (sizeof(Knot3) * size))
			return false;
	}

	return true;
}

bool Spline::Load(const ObjectLoadParams* params, SceneNode* node)
{
	File* in = params->mFile;

	// read number of splines
	unsigned int size = -1;
	if (in->Read(&size, sizeof(unsigned int)) != sizeof(unsigned int))
		return false;

	mSpline.resize(size);
	for (unsigned int i = 0; i < mSpline.size(); ++i)
	{
		Spline3& spline = mSpline[i];

		// read the number of knots
		unsigned int size = -1;
		if (in->Read(&size, sizeof(unsigned int)) != sizeof(unsigned int))
			return false;

		// read the knots
		spline.Init(size);
		Assert(spline.mKnot != 0);
		if (in->Read(spline.mKnot, sizeof(Knot3) * size) != (sizeof(Knot3) * size))
			return false;
	}

	return true;
}

void Spline::Draw(Device* device)
{/*
	Vector4 last;
	for (unsigned int i = 0; i < mSpline.size(); ++i)
	{
		Spline3& spline = mSpline[i];

		for (unsigned int j = 0; j < spline.mKnot.size(); ++j)
		{
			Knot3 knot = spline.mKnot[j];
			if (!(i == 0 && j == 0))
				device->DrawLine(last, knot.mPosition);

			last = knot.mPosition;
		}
	}*/
}