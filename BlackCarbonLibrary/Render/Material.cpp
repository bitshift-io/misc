#include "Memory/NoMemory.h"
#include "Regex/Regex.h"
#include "Memory/Memory.h"
#include "Material.h"
#include "Texture.h"
#include "RenderFactory.h"
#include "RenderBuffer.h"
#include "File/ScriptFile.h"
#include "File/Log.h"
#include <iostream>
#include <string>

using namespace std;
using namespace boost;

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

#define START_BRACE		"{"
#define END_BRACE		"}"
#define STR_PASS		"pass"
#define TECHNIQUE		"technique"
#define RENDERORDER		"renderorder"
#define TEXTURE			"texture"
#define EFFECT			"effect"
#define EFFECTTECHNIQUE	"effecttechnique"
#define VERTEXFORMAT	"vertexformat"
#define ALPHATOCOVERAGE "alphatocoverage"
#define CULLFACE		"cullface"
#define DEPTHCOMPARE	"depthcompare"
#define BLENDENABLE		"blendenable"
#define DEPTHTEST		"depthtest" // TODO change this to DEPTHHWRITE
#define DEPTHWRITE		"depthwrite"
#define SOURCEBLEND		"sourceblend"
#define DESTBLEND		"destblend"


bool MaterialPass::Equals(MaterialPass& rhs)
{
	if (memcmp(&mDeviceState, &rhs.mDeviceState, sizeof(DeviceState)) != 0 || mParameterData.size() != rhs.mParameterData.size())
		return false;

	EffectParameterValueIterator rhsDataIt = rhs.EffectParameterValueBegin();
	EffectParameterValueIterator dataIt = EffectParameterValueBegin();
	for ( ; dataIt != EffectParameterValueEnd(); ++dataIt, ++rhsDataIt)
	{
		if (!(*dataIt)->Equals(**rhsDataIt))
			return false;
	}


	return true;
}


MaterialTechnique::MaterialTechnique() : 
	mID(0),
	mRenderOrder(0),
	mEffect(0),
	mTechnique(0)
{
}

bool MaterialTechnique::Equals(MaterialTechnique& rhs)
{
	if (mEffect != rhs.mEffect || mRenderOrder != rhs.mRenderOrder || mTechnique != rhs.mTechnique || mPass.size() != rhs.mPass.size())
		return false;

	PassIterator passIt = PassBegin();
	PassIterator rhsPassIt = rhs.PassBegin();
	for ( ; passIt != PassEnd(); ++passIt, ++rhsPassIt)
	{
		if (!passIt->Equals(*rhsPassIt))
			return false;
	}

	return true;
}

int MaterialTechnique::Begin()
{
	if (!mEffect || !mTechnique)
		return 0;

	mCurPass = -1;

	// set technique
	if (mTechnique != GetEffect()->GetCurrentTechnique())
	{
		GetEffect()->SetCurrentTechnique(mTechnique);
	}

	return mEffect->Begin();
}

void MaterialTechnique::BeginPass(int idx)
{
	if (!mEffect)
		return;

	BEGINDEBUGEVENT(mMaterial->mDevice, "Material::BeginPass", Vector4(1.f, 1.f, 1.f, 1.f));

	mCurPass = idx;
	MaterialPass& pass = mPass[idx];

	// bind textures
	for (unsigned int p = 0; p < pass.mParameterData.size(); ++p)
	{
		EffectParameterValue* param = pass.mParameterData[p];
		if (!param)
			continue;

		param->Apply();
/*
		//param->GetEffectParameter()->SetValue(param);

		// we hsould replace this with a param->Bind(mEffect);
		switch (param->GetEffectParameter()->mType)
		{
		case EPT_Texture:
		case EPT_Texture2D:
		case EPT_TextureCube:
		case EPT_Texture3D:
			{
				Texture* texture;
				param->GetValue(texture);

				if (!texture)
				{
					//Log::Error("[Material::Pass] Trying to bind a texture that is null in material '%s', parameter '%s'\n",
					//	mName.c_str(), param->GetEffectParameter()->mName.c_str());
				}

				mEffect->SetTexture(param->GetEffectParameter()->mHandle, texture);
			}
			break;

		case EPT_Matrix4:
			{
				Matrix4 value;
				param->GetValue(value);
				mEffect->SetMatrix4(param->GetEffectParameter()->mHandle, value);
			}
			break;

		case EPT_Vector4:
			{
				Vector4 value;
				param->GetValue(value);
				mEffect->SetVector4(param->GetEffectParameter()->mHandle, value);
			}
			break;

		case EPT_Float:
			{
				float value;
				param->GetValue(value);
				mEffect->SetFloat(param->GetEffectParameter()->mHandle, value);
			}
			break;
		}*/
	}

	// does dx10 need to be ander the following code?
	// ogl needs this before this shit
	mEffect->BeginPass(idx);

	// set material overrides
	mMaterial->mDevice->SetDeviceState(pass.mDeviceState);

	ENDDEBUGEVENT(mMaterial->mDevice);
}

void MaterialTechnique::SetPassParameter()
{
	BEGINDEBUGEVENT(mMaterial->mDevice, "Material::SetPassParameter", Vector4(1.f, 1.f, 1.f, 1.f));
	mEffect->SetPassParameter();
	ENDDEBUGEVENT(mMaterial->mDevice);
}

void MaterialTechnique::EndPass()
{
	// BeginPass never called
	if (mCurPass <= -1)
		return;

	BEGINDEBUGEVENT(mMaterial->mDevice, "Material::EndPass", Vector4(1.f, 1.f, 1.f, 1.f));

	MaterialPass& pass = mPass[mCurPass];
	for (unsigned int p = 0; p < pass.mParameterData.size(); ++p)
	{
		EffectParameterValue* param = pass.mParameterData[p];

		if (!param)
			continue;

		switch (param->GetEffectParameter()->mType)
		{
		case EPT_Texture:
		case EPT_Texture2D:
		case EPT_TextureCube:
		case EPT_Texture3D:
			{
				Texture* ptr = 0;
				//mEffect->SetValue(param->GetEffectParameter(), &ptr);
				param->GetEffectParameter()->SetValue(&ptr);
				param->Apply();
			}
			break;
		}
	}

	mEffect->EndPass();

	ENDDEBUGEVENT(mMaterial->mDevice);
}

void MaterialTechnique::End()
{
	if (!mEffect)
		return;

	mEffect->End();
}

EffectParameterValue* MaterialTechnique::GetParameterByUserString(const char* userString)
{
	PassIterator passIt;
	for (passIt = PassBegin(); passIt != PassEnd(); ++passIt)
	{
		MaterialPass::EffectParameterValueIterator effectIt;
		for (effectIt = passIt->EffectParameterValueBegin(); effectIt != passIt->EffectParameterValueEnd(); ++effectIt)
		{
			EffectParameterValue* effectParam = (*effectIt);
			string userStr = effectParam->GetUserString();
			if (strcmpi(userStr.c_str(), userString) == 0)
				return effectParam;
		}
	}

	return 0;
}

EffectParameterValue* MaterialTechnique::GetParameterByName(const char* name)
{
	PassIterator passIt;
	for (passIt = PassBegin(); passIt != PassEnd(); ++passIt)
	{
		MaterialPass::EffectParameterValueIterator effectIt;
		for (effectIt = passIt->EffectParameterValueBegin(); effectIt != passIt->EffectParameterValueEnd(); ++effectIt)
		{
			EffectParameterValue* effectParam = (*effectIt);
			if (strcmpi(effectParam->GetEffectParameter()->mName.c_str(), name) == 0)
				return effectParam;
		}
	}

	return 0;
}

EffectParameterValue* MaterialTechnique::AddParameterByName(const char* name, int pass)
{
	EffectParameterValue* effectParamData = GetParameterByName(name);
	if (effectParamData)
		return effectParamData;

	EffectParameter* effectParam = mEffect->GetParameterByName(name);
	if (!effectParam)
	{
		Log::Warning("[MaterialTechnique::AddParameterByName] Invalid EffectParameter '%s' in material '%s'\n", name, mMaterial->GetName().c_str());
		return 0;
	}

	effectParamData = mEffect->CreateEffectParameterValue(effectParam);
	mPass[pass].mParameterData.push_back(effectParamData);
	return effectParamData;
/*
	effectParam = mEffect->ReadParameter(this, mFactory, mDevice, name, 0);
	if (!effectParam)
	{
		Log::Print("Invalid EffectParam %s\n", name);
		return false;
	}
	else
	{
		mPass[pass].mParameter.push_back(effectParam);
	}

	return effectParam;
	return 0;*/
}

bool MaterialTechnique::AddParameter(EffectParameterValue* effectParamData, int pass)
{
	if (!effectParamData || effectParamData->GetEffectParameter()->mEffect != mEffect)
		return false;

	mPass[pass].mParameterData.push_back(effectParamData);
	return true;
}



// ================================



MaterialBase::MaterialBase() :
	mTechniqueIdx(0)
{
}

void MaterialBase::Destroy(RenderFactory* factory)
{
	mTechnique.clear();
	std::vector<MaterialTechnique>(mTechnique).swap(mTechnique);
}

void MaterialBase::AddReference()
{
	TechniqueIterator techniqueIt;
	for (techniqueIt = TechniqueBegin(); techniqueIt != TechniqueEnd(); ++techniqueIt)
	{
		if (techniqueIt->mEffect)
		{
			//techniqueIt->mEffect = (Effect*)mFactory->Load(Effect::Type, &ObjectLoadParams(mDevice, techniqueIt->mEffect->GetName()));
			techniqueIt->mEffect->AddReference();
		}
	}

	SceneObject::AddReference();
}

bool MaterialBase::ReleaseReference()
{
	bool willRelease = SceneObject::ReleaseReference();

	// clean up
	if (willRelease)
	{
		TechniqueIterator techniqueIt;
		for (techniqueIt = TechniqueBegin(); techniqueIt != TechniqueEnd(); ++techniqueIt)
		{
			vector<MaterialPass>::iterator it;
			for (it = techniqueIt->mPass.begin(); it != techniqueIt->mPass.end(); ++it)
			{
				vector<EffectParameterValue*>::iterator paramIt;
				for (paramIt = it->mParameterData.begin(); paramIt != it->mParameterData.end(); ++paramIt)
				{
					techniqueIt->mEffect->ReleaseEffectParameterValue(&(*paramIt));
					//mEffect->DestroyParameter(factory, *paramIt);
				}
				it->mParameterData.clear();
			}
			techniqueIt->mPass.clear();
		}
	}

	TechniqueIterator techniqueIt;
	for (techniqueIt = TechniqueBegin(); techniqueIt != TechniqueEnd(); ++techniqueIt)
	{
		if (techniqueIt->mEffect)
		{
			techniqueIt->mEffect = mFactory->Release<Effect>(&techniqueIt->mEffect);
			//techniqueIt->mEffect->ReleaseReference();
		}
	}

	return willRelease;
}

bool MaterialBase::Save(const File& out, SceneNode* node)
{
	// write out the name
	unsigned int size = mName.length();
	out.Write(&size, sizeof(unsigned int));
	out.Write(mName.c_str(), size);

	// save the material to disk

	return true;
}

bool MaterialBase::Load(const ObjectLoadParams* params, SceneNode* node)
{
	File* in = params->mFile;

	mDevice = params->mDevice;
	mFactory = params->mFactory;

	if (params->mFlags & OLF_LoadForInstance)
	{
		// read the name
		int size = 0;
		in->Read(&size, sizeof(unsigned int));

		char buffer[256];
		in->Read(buffer, size);
		buffer[size] = '\0';
		SetName(buffer);

		mName = buffer;
		return true;
	}

	if (in)
	{
		Destroy(params->mFactory);

		// read the name
		int size = 0;
		in->Read(&size, sizeof(unsigned int));

		char buffer[256];
		in->Read(buffer, size);
		buffer[size] = '\0';
		SetName(buffer);

		// now load the material from disk
		Load(&ObjectLoadParams(params->mFactory, params->mDevice, mName), 0);
	}
	else
	{
		SetName(params->mName);

		string path;
		params->mFactory->GetParameter().GetExistingFile(mName, "mat", path);
		File file(path.c_str(), FAT_Read | FAT_Text);
		if (!file.Valid())
		{
			Log::Error("[Material::Load] Failed to open material: %s", path.c_str());
			return false;
		}

		const_cast<ObjectLoadParams*>(params)->mFile = &file;

		char buffer[256];
		do
		{
			if (!ReadString(&file, buffer))
				return true; // reached EOF

			if (strcmpi(buffer, TECHNIQUE) == 0)
			{
				char techniqueName[256];
				if (!ReadString(&file, techniqueName))
				{
					Log::Error("[Material::Load] Material '%s' unexpected end of file", GetName().c_str());
					return false; // reached EOF
				}

				ReadString(&file, buffer);
				if (strcmp(buffer, START_BRACE) != 0)
				{
					Log::Error("[Material::Load] Expected %s after %s in material %s not found\n", START_BRACE, STR_PASS, GetName().c_str());
					return false;
				}

				if (ReadTechnique(params, techniqueName) == false)
					return false;
			}
			else
			{
				Log::Error("[Material::Load] Material '%s' unexpected line '%s', expecting '%s'", GetName().c_str(), buffer, TECHNIQUE);
				return false;
			}

			buffer[0] = '\0';
		} while (!file.EndOfFile());
	}

	return true;
}

bool MaterialBase::ReadTechnique(const ObjectLoadParams* params, const string& name)
{
	File* file = params->mFile;

	MaterialTechnique technique;
	technique.mName = name;
	technique.mMaterial = this;

	char buffer[256];
	do
	{
		ReadString(file, buffer);

		if (strcmpi(buffer, END_BRACE) == 0)
		{
			technique.mID = mTechnique.size(); // set default technique ID
			if (!technique.mEffect)
				Log::Error("[MaterialBase::ReadTechnique] Technique has no effect\n");

			mTechnique.push_back(technique);

			return true;
		}
		if (strcmp(buffer, STR_PASS) == 0)
		{
			ReadString(file, buffer);
			if (strcmp(buffer, START_BRACE) != 0)
			{
				Log::Error("ERROR: expected %s after %s in material %s not found\n", START_BRACE, STR_PASS, mName.c_str());
				return false;
			}

			if (ReadPass(params, technique) == false)
				return false;
		}
		else if (strcmp(buffer, EFFECT) == 0)
		{
			ReadString(file, buffer);
			technique.mEffect = params->mFactory->Load<Effect>(&ObjectLoadParams(params->mFactory, params->mDevice, buffer));
			//technique.mEffect = (Effect*)params->mFactory->Create(Effect::Type);
			//technique.mEffect->Load(&ObjectLoadParams(params->mFactory, params->mDevice, buffer));
			if (!technique.mEffect)
			{
				return false;
			}

			technique.mTechnique = technique.mEffect->GetCurrentTechnique();

			if (!technique.mTechnique)
			{
				Log::Error("Material '%s' has no an effect '%s' with no technique set\n", GetName().c_str(), buffer);
				return false;
			}
		}
		else if (strcmpi(buffer, EFFECTTECHNIQUE) == 0)
		{
			if (!technique.mEffect)
			{
				Log::Error("Material '%s' needs to declare the effect before the technique", GetName().c_str());
				return false;
			}

			ReadString(file, buffer);
			technique.mTechnique = technique.mEffect->GetTechniqueByName(buffer);
		}
		else if (strcmp(buffer, RENDERORDER) == 0)
		{
			ReadInt(file, technique.mRenderOrder);
		}
		else
		{
			Log::Error("ERROR: expected '%s' in material '%s' not found. Found '%s'.\n", STR_PASS, mName.c_str(), buffer);
			return false;
		}

		buffer[0] = '\0';
	} while (!file->EndOfFile());

	return false;
}

bool MaterialBase::ReadPass(const ObjectLoadParams* params, MaterialTechnique& technique)
{
	File* file = params->mFile;

	char buffer[256];
	MaterialPass pass;
	pass.mDeviceState.vertexFormat = VF_Position;

	do
	{
		ReadString(file, buffer);

		if (strcmpi(buffer, END_BRACE) == 0)
		{
			technique.mPass.push_back(pass);
			return true;
		}
		else if (strcmpi(buffer, VERTEXFORMAT) == 0)
		{
			ReadVertexFormat(file, pass.mDeviceState.vertexFormat);
		}
		else if (strcmpi(buffer, ALPHATOCOVERAGE) == 0)
		{
			ReadBool(file, pass.mDeviceState.alphaToCoverageEnable);
		}
		else if (strcmpi(buffer, CULLFACE) == 0)
		{
			ReadString(file, buffer);

			if (strcmpi(buffer, "none") == 0)
				pass.mDeviceState.cullFace = CF_None;
			else if (strcmpi(buffer, "front") == 0)
				pass.mDeviceState.cullFace = CF_Front;
			else if (strcmpi(buffer, "back") == 0)
				pass.mDeviceState.cullFace = CF_Back;
		}
		else if (strcmpi(buffer, DEPTHCOMPARE) == 0)
		{
			ReadString(file, buffer);

			if (strcmpi(buffer, "lessequal") == 0)
				pass.mDeviceState.depthCompare = C_LessEqual;
			else if (strcmpi(buffer, "less") == 0)
				pass.mDeviceState.depthCompare = C_Less;
			else if (strcmpi(buffer, "equal") == 0)
				pass.mDeviceState.depthCompare = C_Equal;
			else if (strcmpi(buffer, "greaterequal") == 0)
				pass.mDeviceState.depthCompare = C_GreaterEqual;
			else if (strcmpi(buffer, "greater") == 0)
				pass.mDeviceState.depthCompare = C_Greater;
			else if (strcmpi(buffer, "always") == 0)
				pass.mDeviceState.depthCompare = C_Always;
			else if (strcmpi(buffer, "never") == 0)
				pass.mDeviceState.depthCompare = C_Never;
		}
		else if (strcmpi(buffer, BLENDENABLE) == 0)
		{
			ReadBool(file, pass.mDeviceState.alphaBlendEnable);
		}
		else if (strcmpi(buffer, DEPTHTEST) == 0)
		{
			ReadBool(file, pass.mDeviceState.depthTestEnable);
		}
		else if (strcmpi(buffer, DEPTHWRITE) == 0)
		{
			ReadBool(file, pass.mDeviceState.depthWriteEnable);
		}
		else if (strcmpi(buffer, SOURCEBLEND) == 0)
		{
			ReadString(file, buffer);
			pass.mDeviceState.alphaBlendSource = ConvertBlendMode(buffer);
		}
		else if (strcmpi(buffer, DESTBLEND) == 0)
		{
			ReadString(file, buffer);
			pass.mDeviceState.alphaBlendDest = ConvertBlendMode(buffer);
		}
		else
		{
			EffectParameter* effectParam = technique.mEffect->GetParameterByName(buffer);

			if (!effectParam)
			{
				ReadString(file, buffer);
				Log::Warning("[Material::ReadPass] Effect Parameter '%s' not found in material '%s'\n", buffer, mName.c_str());
			}
			else
			{
				EffectParameterValue* effectParamData = technique.mEffect->CreateEffectParameterValue(effectParam);
				pass.mParameterData.push_back(effectParamData);

				switch (effectParamData->GetEffectParameter()->mType)
				{
				case EPT_Texture:
				case EPT_Texture2D:
				case EPT_TextureCube:
				case EPT_Texture3D:
					{
						ReadString(file, buffer);

						// check for render textures
						if (buffer[0] == '(' && buffer[strlen(buffer) - 1] == ')')
						{
							string userString = buffer;
							userString =  userString.substr(1, userString.length() - 2);
							effectParamData->SetUserString(userString.c_str());
							break;
						}

						InterpretString(buffer);

						ObjectLoadParams p;
						p.mFactory = params->mFactory;
						p.mDevice = params->mDevice;
						p.mName = buffer;

						Texture* texture = params->mFactory->Load<Texture>(&p);
						effectParamData->SetValue(&texture);
					}
					break;

				case EPT_Float:
					{
						float value;
						ReadFloat(file, value);
						effectParamData->SetValue(&value);
					}
					break;

				case EPT_Vector4:
					{
						Vector4 value;
						ReadVector4(file, value);
						effectParamData->SetValue(&value);
					}
					break;

				case EPT_Int:
					{
						int value;
						ReadInt(file, value);
						effectParamData->SetValue(&value);
					}
					break;

				default:
					Log::Error("Material::ReadPass] Failed to read the type for '%s'\n", buffer);
					break;
				}
			}
		}

		buffer[0] = '\0';
	} while (!file->EndOfFile());

	Log::Print("ERROR: expected %s in material %s not found\n", END_BRACE, mName.c_str());
	return false;
}

void MaterialBase::InterpretString(char* line)
{
	char name[256];
	char inner[256] = "\0";
	strcpy(name, line);

	string result;

	cmatch matches;
	regex expression("^(.*?)(\\(.*\\))(.*?)$");
	if (regex_match(line, matches, expression))
	{
		for (int i = 1; i < matches.size(); i++)
		{
			string match(matches[i].first, matches[i].second);
			if (match[0] == '(' && match[match.size() - 1] == ')')
			{
				if (strcmpi("(matName)", match.c_str()) == 0)
				{
					result += mName;
				}
				else
				{
					// now extract x and y from (x : y)
					cmatch nameMatches;
					regex nameExpression("^\\((.*?) : (.*)\\)$");
					if (regex_match(match.c_str(), nameMatches, nameExpression))
					{
						string regexString;
						for (int j = 1; j < matches.size(); j++)
						{
							string nameMatch(nameMatches[j].first, nameMatches[j].second);

							// so we now have the string we are doing a regex on
							if (regexString.size() && nameMatch.size())
							{
								cmatch replaceMatches;
								if (regex_match(regexString.c_str(), replaceMatches, regex(nameMatch)))
								{
									for (int k = 1; k < replaceMatches.size(); k++)
									{
										string replaceMatch(replaceMatches[k].first, replaceMatches[k].second);
										if (replaceMatch.size())
										{
											result += replaceMatch;
										}
									}
								}
							}

							if (strcmpi("matName", nameMatch.c_str()) == 0)
								regexString = mName;
						}
					}
				}
			}
			else
			{
				result += match;
			}
		}

		strcpy(line, result.c_str());
	}
}

BlendMode MaterialBase::ConvertBlendMode(const char* value)
{
	if (strcmpi(value, "zero") == 0)
		return BM_Zero;

	if (strcmpi(value, "one") == 0)
		return BM_One;

	if (strcmpi(value, "DestColour") == 0)
		return BM_DestColour;

	if (strcmpi(value, "SrcColour") == 0)
		return BM_SrcColour;

	if (strcmpi(value, "OneMinusDestColour") == 0)
		return BM_OneMinusDestColour;

	if (strcmpi(value, "OneMinusSrcColour") == 0)
		return BM_OneMinusSrcColour;

	if (strcmpi(value, "SrcAlpha") == 0)
		return BM_SrcAlpha;

	if (strcmpi(value, "OneMinusSrcAlpha") == 0)
		return BM_OneMinusSrcAlpha;

	if (strcmpi(value, "DestAlpha") == 0)
		return BM_DestAlpha;

	if (strcmpi(value, "OneMinusDestAlpha") == 0)
		return BM_OneMinusDestAlpha;

	if (strcmpi(value, "SrcAlphaSaturate") == 0)
		return BM_SrcAlphaSaturate;

	return BM_One;
}

bool MaterialBase::ReadVertexFormat(File* file, int& value)
{
	char buffer[256];

	// read till we reach newline
	file->ReadLine(buffer, 256);

	value = 0;
	if (strstr(buffer, "position"))
		value |= VF_Position;
	if (strstr(buffer, "normal"))
		value |= VF_Normal;
	if (strstr(buffer, "colour"))
		value |= VF_Colour;
	if (strstr(buffer, "binormal"))
		value |= VF_Binormal;
	if (strstr(buffer, "tangent"))
		value |= VF_Tangent;
	if (strstr(buffer, "bonedata"))
		value |= VF_BoneData;

	for (int i = 0; i < MAX_TEXCOORD; ++i)
	{
		char texBuffer[256];
		sprintf(texBuffer, "texcoord%i", i);

		if (strstr(buffer, texBuffer))
			value |= (VF_TexCoord0 << i);
	}


	return true;
}

bool MaterialBase::ReadLine(File* file, char* value)
{
	value[0] = '\0';
	file->ReadLine(value, 256);

	if (file->EndOfFile())
		return false;

	// we came across a comment
	if (value[0] == ';')
	{
		char discard[256];
		file->ReadLine(discard, 256);

		return ReadLine(file, value);
	}

	strcpy(value, ScriptFile::StripWhitespace(value).c_str());

	return true;
}

bool MaterialBase::ReadString(File* file, char* value)
{
	value[0] = '\0';
	file->ReadString("%s", value);

	if (file->EndOfFile())
		return false;

	// we came across a comment
	if (value[0] == ';')
	{
		char discard[256];
		file->ReadLine(discard, 256);

		return ReadString(file, value);
	}

	return true;
}

bool MaterialBase::ReadVector4(File* file, Vector4& value)
{
	file->ReadString("%f %f %f %f", &value[0], &value[1], &value[2], &value[3]);
	return true;
}

bool MaterialBase::ReadFloat(File* file, float& value)
{
	file->ReadString("%f", &value);
	return true;
}

bool MaterialBase::ReadInt(File* file, int& value)
{
	file->ReadString("%i", &value);
	return true;
}

bool MaterialBase::ReadBool(File* file, bool& value)
{
	char buffer[256];
	ReadString(file, buffer);
	if (strcmpi(buffer, "true") == 0)
		value = true;
	else
		value = false;

	return true;
}

bool MaterialBase::IsInstance(const ObjectLoadParams* params, SceneObject* loaded)
{
	MaterialBase* other = static_cast<MaterialBase*>(loaded);
	if (strcmpi(mName.c_str(), other ? other->mName.c_str() : params->mName.c_str()) == 0)
		return true;

	return false;
}

void MaterialBase::SetName(const string& name)
{
    mName = name;

    // FIX ME!!! WRITE MY OWN FKN STRING CLASS!
    //strlwr(mName.c_str());
    //std::transform(mName.begin(), mName.end(), mName.begin(), std::tolower);
}

MaterialTechnique* MaterialBase::GetTechniqueByID(unsigned int id)
{
	TechniqueIterator it;
	for (it = TechniqueBegin(); it != TechniqueEnd(); ++it)
	{
		if (it->mID == id)
			return &(*it);
	}

	return 0;
}


int MaterialBase::Begin()
{
	return GetCurrentTechnique()->Begin();
}

void MaterialBase::BeginPass(int idx)
{
	GetCurrentTechnique()->BeginPass(idx);
}

void MaterialBase::SetPassParameter()
{
	GetCurrentTechnique()->SetPassParameter();
}

void MaterialBase::EndPass()
{	
	GetCurrentTechnique()->EndPass();
}

void MaterialBase::End()
{
	GetCurrentTechnique()->End();
}

Effect* MaterialBase::GetEffect()									
{ 
	return GetCurrentTechnique()->mEffect; 
}

Device* MaterialBase::GetDevice()
{
	return mDevice;
}

void MaterialBase::SetCurrentTechnique(MaterialTechnique* technique)
{ 
	int idx = 0;
	TechniqueIterator it;
	for (it = TechniqueBegin(); it != TechniqueEnd(); ++it, ++idx)
	{
		if (&(*it) == technique)
		{
			mTechniqueIdx = idx;
			break;
		}
	}
}

MaterialTechnique* MaterialBase::GetCurrentTechnique()								
{ 
	return &mTechnique[mTechniqueIdx]; 
}

SceneObject* MaterialBase::Clone(SceneNode* owner)
{
	MaterialBase* clone = new MaterialBase();
	*clone = *this;
	clone->mReferenceCount = 0;
	clone->mTechnique.clear();
	clone->mMaterial.clear();

	/*
TechniqueIterator techIt;
	for (techIt = mMaterialBase->TechniqueBegin(); techIt != mMaterialBase->TechniqueEnd(); ++techIt)
	{
		MaterialTechnique technique;
		technique = *techIt;
		technique.mPass.clear();

		MaterialTechnique::PassIterator passIt;
		for (passIt = techIt->PassBegin(); passIt != techIt->PassEnd(); ++passIt)
		{
			MaterialPass pass;
			pass = *passIt;
			pass.mParameterData.clear();

			MaterialPass::EffectParameterValueIterator valueIt;
			for (valueIt = passIt->EffectParameterValueBegin(); valueIt != passIt->EffectParameterValueEnd(); ++valueIt)
			{
				EffectParameterValue* value = *valueIt;
				pass.mParameterData.push_back(techIt->mEffect->CreateEffectParameterValue(value->GetEffectParameter()));
			}

			technique.mPass.push_back(pass);
		}

		mTechniqueOverride.push_back(technique);
	}
	*/

	// extra cloney goodness
	TechniqueIterator techIt;
	for (techIt = TechniqueBegin(); techIt != TechniqueEnd(); ++techIt)
	{
		MaterialTechnique technique;
		technique = *techIt;
		technique.mPass.clear();
		technique.mMaterial = clone;
		technique.mEffect->AddReference();

		MaterialTechnique::PassIterator passIt;
		for (passIt = techIt->PassBegin(); passIt != techIt->PassEnd(); ++passIt)
		{
			MaterialPass pass;
			pass = *passIt;
			pass.mParameterData.clear();

			MaterialPass::EffectParameterValueIterator valueIt;
			for (valueIt = passIt->EffectParameterValueBegin(); valueIt != passIt->EffectParameterValueEnd(); ++valueIt)
			{
				EffectParameterValue* value = *valueIt;
				pass.mParameterData.push_back(techIt->mEffect->CreateEffectParameterValue(value->GetEffectParameter()));
			}

			technique.mPass.push_back(pass);
		}

		clone->mTechnique.push_back(technique);
	}

	return clone;
}

void MaterialBase::InsertMaterial(Material* material)
{
	mMaterial.push_back(material);
}

void MaterialBase::RemoveMaterial(Material* material)
{
	mMaterial.remove(material);
}


// ================================================

Material::Material() :
	mMaterialBase(0),
	mFlags(0)
{
}

bool Material::Save(const File& out, SceneNode* node)
{
	return mMaterialBase->Save(out, node);
}

bool Material::Load(const ObjectLoadParams* params, SceneNode* node)
{
	mMaterialBase = params->mFactory->Load<MaterialBase>(params);
	if (!mMaterialBase)
		return 0;

	mMaterialBase->InsertMaterial(this);
	Reset();
	return mMaterialBase != 0;
}

SceneObject* Material::Clone(SceneNode* owner)
{
	Material* clone = new Material;
	*clone = *this;
	clone->Reset(); // should this be reset?
	mMaterialBase->AddReference();
	mMaterialBase->InsertMaterial(clone);
	return clone;
}

void Material::Reset()
{
	ClearFlags(MF_OverridePass | MF_OverrideEffect);
	mTechniqueOverride.clear();

	TechniqueIterator techIt;
	for (techIt = mMaterialBase->TechniqueBegin(); techIt != mMaterialBase->TechniqueEnd(); ++techIt)
	{
		MaterialTechnique technique;
		technique = *techIt;
		technique.mPass.clear();

		MaterialTechnique::PassIterator passIt;
		for (passIt = techIt->PassBegin(); passIt != techIt->PassEnd(); ++passIt)
		{
			MaterialPass pass;
			pass = *passIt;
			pass.mParameterData.clear();

			MaterialPass::EffectParameterValueIterator valueIt;
			for (valueIt = passIt->EffectParameterValueBegin(); valueIt != passIt->EffectParameterValueEnd(); ++valueIt)
			{
				EffectParameterValue* baseValue = *valueIt;
				EffectParameterValue* value = techIt->mEffect->CreateEffectParameterValue(baseValue->GetEffectParameter());
				value->SetValue(baseValue);
				pass.mParameterData.push_back(value);
			}

			technique.mPass.push_back(pass);
		}

		mTechniqueOverride.push_back(technique);
	}
}

bool Material::IsInstance(const ObjectLoadParams* params, SceneObject* loaded)
{
	return false;
}

void Material::SetName(const string& name)
{
	mMaterialBase->SetName(name);
}

const string& Material::GetName()											
{ 
	return mMaterialBase->GetName(); 
}

MaterialTechnique* Material::GetTechniqueByID(unsigned int id)
{
	TechniqueIterator it;
	for (it = TechniqueBegin(); it != TechniqueEnd(); ++it)
	{
		if (it->mID == id)
			return &(*it);
	}

	return 0;
	//return mMaterialBase->GetTechniqueByID(id);
}

Material::TechniqueIterator Material::TechniqueBegin()											
{ 
	return mTechniqueOverride.begin();
}

Material::TechniqueIterator Material::TechniqueEnd()												
{ 
	return mTechniqueOverride.end();
}

void Material::AddReference()
{
	if (mMaterialBase)
		mMaterialBase->AddReference();

	SceneObject::AddReference();
}

bool Material::ReleaseReference()
{
	// clean up
	bool willRelease = SceneObject::ReleaseReference();
	if (willRelease)
	{
		TechniqueIterator techniqueIt;
		for (techniqueIt = TechniqueBegin(); techniqueIt != TechniqueEnd(); ++techniqueIt)
		{
			vector<MaterialPass>::iterator it;
			for (it = techniqueIt->mPass.begin(); it != techniqueIt->mPass.end(); ++it)
			{
				vector<EffectParameterValue*>::iterator paramIt;
				for (paramIt = it->mParameterData.begin(); paramIt != it->mParameterData.end(); ++paramIt)
				{
					techniqueIt->mEffect->ReleaseEffectParameterValue(&(*paramIt));
					//mEffect->DestroyParameter(factory, *paramIt);
				}
				it->mParameterData.clear();
			}
			techniqueIt->mPass.clear();
		}
	}

	if (mMaterialBase)
		mMaterialBase = mMaterialBase->mFactory->Release<MaterialBase>(&mMaterialBase);
	
	return willRelease;
}

void Material::SetCurrentTechnique(MaterialTechnique* technique)			
{
	int idx = 0;
	TechniqueIterator it;
	for (it = TechniqueBegin(); it != TechniqueEnd(); ++it, ++idx)
	{
		if (&(*it) == technique)
		{
			mMaterialBase->mTechniqueIdx = idx;
			return;
		}
	}

	// if this failed, maybe they really want to do this on the base material
	mMaterialBase->SetCurrentTechnique(technique);
}

MaterialTechnique* Material::GetCurrentTechnique()										
{ 
	return &mTechniqueOverride[mMaterialBase->GetCurrentTechniqueIndex()];
}

int Material::Begin()
{
	return GetCurrentTechnique()->Begin();
}

void Material::BeginPass(int idx)
{
	GetCurrentTechnique()->BeginPass(idx);
}

void Material::SetPassParameter()
{
	GetCurrentTechnique()->SetPassParameter();
}

void Material::EndPass()
{
	GetCurrentTechnique()->EndPass();
}

void Material::End()
{
	GetCurrentTechnique()->End();
}

Effect* Material::GetEffect()
{
	return GetCurrentTechnique()->mEffect;
}

MaterialBase* Material::GetMaterialBase()
{
	return mMaterialBase;
}

void Material::SetMaterialBase(MaterialBase* materialBase)
{
	if (mMaterialBase)
		mMaterialBase->mFactory->Release<MaterialBase>(&mMaterialBase);

	mMaterialBase = materialBase;
	mMaterialBase->AddReference();
	Reset();
}

bool Material::Lock()
{
	mLocked = true;
	return mLocked;
}

void Material::Unlock()
{
	mLocked = false;

	TechniqueIterator it;
	TechniqueIterator baseIt;
	for (it = TechniqueBegin(), baseIt = GetMaterialBase()->TechniqueBegin(); it != TechniqueEnd(); ++it, ++baseIt)
	{
		if (!it->Equals(*baseIt))
		{
			// set some flag to notify if this technique does or doesnt override the base
		}
	}
}
/*
bool Material::OverridesEffect()
{
	return GetCurrentTechnique()->mEffect != mMaterialBase->GetCurrentTechnique()->mEffect;
}

bool Material::OverridesPass()
{
	MaterialTechnique* technique = GetCurrentTechnique();
	MaterialTechnique* techniqueBase = mMaterialBase->GetCurrentTechnique();

	return !technique->Equals(*techniqueBase);


	//MaterialTechnique::PassIterator passIt;
	//MaterialTechnique::PassIterator passItBase;

	//technique->
}
*/
void Material::SetFlags(unsigned int flags)
{
	mFlags |= flags;	
}

void Material::ClearFlags(unsigned int flags)
{
	mFlags &= ~flags;
}

Device* Material::GetDevice()
{
	return mMaterialBase->GetDevice();
}