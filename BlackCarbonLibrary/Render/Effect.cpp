#include "Effect.h"
#include "Camera.h"
#include "Memory/Memory.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

struct EnumLookup
{
	EnumLookup(int _id, const char* _name) : id(_id), name(_name) {}

	int id;
	const char* name;
};

extern EnumLookup sEffectParamIDLookup[];

EnumLookup sEffectParamIDLookup[] =
{
	EnumLookup(EPID_Projection, "Projection"),
	EnumLookup(EPID_ProjectionInverse, "ProjectionInverse"),
	EnumLookup(EPID_ViewProjectionInverse, "ViewProjectionInverse"),
	EnumLookup(EPID_World, "World"),
	EnumLookup(EPID_WorldInverse, "WorldInverse"),
	EnumLookup(EPID_WorldInverseTranspose, "WorldInverseTranspose"),
	EnumLookup(EPID_Camera, "Camera"),
	EnumLookup(EPID_View, "View"),
	EnumLookup(EPID_ViewInverseTranspose, "ViewInverseTranspose"),
	EnumLookup(EPID_ViewInverse, "ViewInverse"),
	EnumLookup(EPID_ViewTranspose, "ViewTranspose"),
	EnumLookup(EPID_ViewProjection, "ViewProjection"),
	EnumLookup(EPID_WorldView, "WorldView"),
	EnumLookup(EPID_WorldViewInverseTranspose, "WorldViewInverseTranspose"),
	EnumLookup(EPID_WorldViewInverse, "WorldViewInverse"),
	EnumLookup(EPID_WorldViewProjection, "WorldViewProjection"),
	EnumLookup(EPID_WorldViewProjectionInverseTranspose, "WorldViewProjectionInverseTranspose"),
	EnumLookup(EPID_WorldViewProjectionInverseTranspose, "WorldViewProjectionInverse"),

	EnumLookup(EPID_SkinPalette, "SkinPalette"),

	EnumLookup(EPID_LightPosition, "LightPosition"),
	EnumLookup(EPID_LightDirection, "LightDirection"),
	EnumLookup(EPID_LightType, "LightType"),
	EnumLookup(EPID_LightDiffuseColour, "LightDiffuseColour"),
	EnumLookup(EPID_LightDecayType, "LightDecayType"),
	EnumLookup(EPID_LightFalloff, "LightFalloff"),

	EnumLookup(EPID_Time, "Time"),

	EnumLookup(EPID_Light, "Light"),
	EnumLookup(EPID_LightCount, "LightCount"),

	EnumLookup(EPID_ProjectorTexture, "ProjectorTexture"),
	EnumLookup(EPID_Projector, "Projector"),
	EnumLookup(EPID_ProjectorCount, "ProjectorCount"),

	EnumLookup(EPID_InstanceData, "InstanceData"),


	EnumLookup(0, 0),
};


EffectParameterValue::EffectParameterValue(EffectParameter* param) :
	mEffectParameter(param),
	mData(0),
	mSize(0),
	mType(EPT_Unknown),
	mDirty(false)
{
}

bool EffectParameterValue::GetValue(Matrix4* value, unsigned int count)
{
	return GetValue((void*)value, sizeof(Matrix4) * count);
}

bool EffectParameterValue::GetValue(float* value, unsigned int count)
{
	return GetValue((void*)value, sizeof(float) * count);
}
bool EffectParameterValue::GetValue(Texture** value, unsigned int count)
{
	return GetValue((void*)value, sizeof(Texture*) * count);
}

bool EffectParameterValue::GetValue(Vector4* value, unsigned int count)
{
	return GetValue((void*)value, sizeof(Vector4) * count);
}

bool EffectParameterValue::GetValue(int* value, unsigned int count)
{
	return GetValue((void*)value, sizeof(int) * count);
}

bool EffectParameterValue::GetValue(void* value, unsigned int size)
{
	if (size > mSize)
		memset(value, 0, size);

	memcpy(value, mData, Math::Min(size, mSize));
	return size <= mSize;
}


bool EffectParameterValue::SetValue(const Matrix4* value, unsigned int count)
{
	SetValue((void*)value, sizeof(Matrix4) * count);
	mType = EPT_Matrix4;
	return true;
}

bool EffectParameterValue::SetValue(const float* value, unsigned int count)
{
	SetValue((void*)value, sizeof(float) * count);
	mType = EPT_Float;
	return true;
}

bool EffectParameterValue::SetValue(Texture** value, unsigned int count)
{
	SetValue((void*)value, sizeof(Texture*) * count);
	mType = EPT_Texture;
	return true;
}

bool EffectParameterValue::SetValue(const Vector4* value, unsigned int count)
{
	SetValue((void*)value, sizeof(Vector4) * count);
	mType = EPT_Vector4;
	return true;
}

bool EffectParameterValue::SetValue(const int* value, unsigned int count)
{
	SetValue((void*)value, sizeof(int) * count);
	mType = EPT_Int;
	return true;
}

bool EffectParameterValue::SetValue(const void* value, unsigned int size)
{
	if (mSize != size)
	{
		free(mData);
		mData = malloc(size);
	}
	else
	{
		// memory is the same
		if (memcmp(mData, value, size) == 0)
			return true;
	}

	memcpy(mData, value, size);
	mSize = size;
	mType = EPT_Unknown;
	mDirty = true;
	return true;
}

bool EffectParameterValue::SetValue(EffectParameterValue* value)
{
	bool result = SetValue(value->mData, value->mSize);
	mType = value->mType;
	return result;
}

bool EffectParameterValue::Equals(EffectParameterValue& rhs)
{
	if (mSize != rhs.mSize)
		return false;

	return memcmp(mData, rhs.mData, mSize) == 0;
}

void EffectParameterValue::Apply()
{
	mEffectParameter->SetValue(this);
}

unsigned int EffectParameterValue::GetTypeSize(EffectParameterType type)
{
	switch (type)
	{
	case EPT_Texture:
	case EPT_Texture2D:
	case EPT_TextureCube:
	case EPT_Texture3D:
		return sizeof(Texture*);
	case EPT_Float:
		return sizeof(float);
	case EPT_Vector4:
		return sizeof(Vector4);
	case EPT_Int:
		return sizeof(int);
	case EPT_Matrix4:
		return sizeof(Matrix4);
	default:
		break;
	}

	return -1;
}




void EffectParameter::Apply()
{
	mEffect->SetValue(this);
}


EffectParameter* Effect::GetParameterBySemantic(const char* semantic)
{
	EffectParameterIterator it;
	for (it = BeginParameter(); it != EndParameter(); ++it)
	{
		if (strcmpi(semantic, (*it)->mSemantic.c_str()) == 0)
			return *it;
	}

	return 0;
}

EffectParameter* Effect::GetParameterByName(const char* name)
{
	EffectParameterIterator it;
	for (it = BeginParameter(); it != EndParameter(); ++it)
	{
		if (strcmpi(name, (*it)->mName.c_str()) == 0)
			return *it;
	}

	return 0;
}

EffectParameter* Effect::GetParameterByID(EffectParameterID id, bool searchChildren)
{
	EffectParameterIterator it;
	for (it = BeginParameter(); it != EndParameter(); ++it)
	{
		if (id == (*it)->mID)
			return *it;

		if (searchChildren)
		{
			EffectParameter* result = GetParameterByID(id, *it, true);
			if (result)
				return result;
		}
	}

	return 0;
}

EffectParameter* Effect::GetParameterByID(EffectParameterID id, EffectParameter* effectParam, bool searchChildren)
{
	if (effectParam->mID == id)
		return effectParam;

	EffectParameterIterator childIt;
	for (childIt = effectParam->mChild.begin(); childIt != effectParam->mChild.end(); ++childIt)
	{
		EffectParameter* result = GetParameterByID(id, *childIt, searchChildren);
		if (result)
			return result;
	}

	return 0;
}

EffectTechnique* Effect::GetTechniqueByName(const char* name)
{
	EffectTechniqueIterator it;
	for (it = BeginTechnique(); it != EndTechnique(); ++it)
	{
		if (strcmpi(name, (*it)->mName.c_str()) == 0)
			return *it;
	}

	return 0;
}

void Effect::SetCurrentTechnique(EffectTechnique* technique)
{
	mCurrentTechnique = technique;
}

EffectTechnique* Effect::GetCurrentTechnique()
{
	return mCurrentTechnique;
}

EffectParameterValue* Effect::CreateEffectParameterValue(EffectParameter* param)
{
	EffectParameterValue* data = new EffectParameterValue(param);
	return data;
}

void Effect::ReleaseEffectParameterValue(EffectParameterValue** data)
{
	if (!*data)
		return;

	delete *data;
	*data = 0;
}

int Effect::GetParameterIDByName(const char* name)
{
	if (!name)
		return EPID_Unknown;

	int j = 0;
	while (sEffectParamIDLookup[j].name != 0)
	{
		if (strcmpi(name, sEffectParamIDLookup[j].name) == 0)
			return j;

		++j;
	}

	return EPID_Unknown;
}

Matrix4 Effect::CalculateMatrix(EffectParameterID id, const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world)
{
	Matrix4 result(MI_Identity);

	switch (id)
	{
	case EPID_View:
		{
			result = view;
		}
		break;

	case EPID_ViewInverseTranspose:
		{
			Matrix4 viewIT = view;
			viewIT.Inverse();
			viewIT.Transpose();
			result = viewIT;
		}
		break;

	case EPID_ViewInverse:
		{
			Matrix4 viewInv = view;
			viewInv.Inverse();
			result = viewInv;
		}
		break;

	case EPID_ViewTranspose:
		{
			Matrix4 viewT = view;
			viewT.Transpose();
			result = viewT;
		}
		break;

	case EPID_World:
		{
			result = world;
		}
		break;

	case EPID_Camera:
		{
			result = camWorld;
		}
		break;

	case EPID_WorldInverseTranspose:
		{
			Matrix4 worldIT = world;
			worldIT.Inverse();
			worldIT.Transpose();
			result = worldIT;
		}
		break;

	case EPID_WorldView:
		{
			Matrix4 worldView = world * view;
			result = worldView;
		}
		break;

	case EPID_WorldInverse:
		{
			Matrix4 worldInv = world;
			worldInv.Inverse();
			result = worldInv;
		}
		break;

	case EPID_WorldViewInverseTranspose:
		{
			Matrix4 worldView = world * view;
			worldView.Inverse();
			worldView.Transpose();
			result = worldView;
		}
		break;

	case EPID_WorldViewInverse:
		{
			Matrix4 worldViewInv = world * view;
			worldViewInv.Inverse();
			result = worldViewInv;
		}
		break;

	case EPID_Projection:
		{
			result = projection;
		}
		break;

	case EPID_ProjectionInverse:
		{
			Matrix4 projInv = projection;
			projInv.Inverse();
			result = projInv;
		}
		break;

	case EPID_WorldViewProjection:
		{
			Matrix4 worldViewProj = world * view * projection;
			result = worldViewProj;
		}
		break;

	case EPID_WorldViewProjectionInverseTranspose:
		{
			Matrix4 worldViewProj = world * view * projection;
			worldViewProj.Inverse();
			worldViewProj.Transpose();
			result = worldViewProj;
		}
		break;

	case EPID_WorldViewProjectionInverse:
		{
			Matrix4 worldViewProj = world * view * projection;
			worldViewProj.Inverse();
			result = worldViewProj;
		}
		break;

	case EPID_ViewProjectionInverse:
		{
			Matrix4 viewProjInv = view * projection;
			viewProjInv.Inverse();
			result = viewProjInv;
		}
		break;

	case EPID_ViewProjection:
		{
			Matrix4 viewProj = view * projection;
			result = viewProj;
		}
		break;

	default:
		//Log::Print("[DX10Effect::SetMatrixParameters] Unknown parameter");
		break;
	}

	return result;
}

void Effect::SetMatrixParameters(const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world)
{
	// TESTING!
	//const_cast<Matrix4&>(view).SetIdentity();
	//const_cast<Matrix4&>(world).SetIdentity();

	unsigned int numParam = mParameter.size();
	for (unsigned int p = 0; p < numParam; ++p)
	{
		EffectParameter* effectParam = mParameter[p];

		switch (effectParam->mID)
		{
		case EPID_View:
			{
				effectParam->SetValue(&view);
				//SetValue(effectParam, &view);
			}
			break;

		case EPID_ViewInverseTranspose:
			{
				Matrix4 viewIT = view;
				viewIT.Inverse();
				viewIT.Transpose();
				effectParam->SetValue(&viewIT);
				//SetValue(effectParam, &viewIT);
			}
			break;

		case EPID_ViewInverse:
			{
				Matrix4 viewInv = view;
				viewInv.Inverse();
				effectParam->SetValue(&viewInv);
				//SetValue(effectParam, &viewInv);
			}
			break;

		case EPID_ViewTranspose:
			{
				Matrix4 viewT = view;
				viewT.Transpose();
				effectParam->SetValue(&viewT);
				//SetValue(effectParam, &viewT);
			}
			break;

		case EPID_World:
			{
				effectParam->SetValue(&world);
				//SetValue(effectParam, &world);
			}
			break;

		case EPID_Camera:
			{
				effectParam->SetValue(&camWorld);
				//SetValue(effectParam, &camWorld);
			}
			break;

		case EPID_WorldInverseTranspose:
			{
				Matrix4 worldIT = world;
				worldIT.Inverse();
				worldIT.Transpose();
				effectParam->SetValue(&worldIT);
				//SetValue(effectParam, worldIT);
			}
			break;

		case EPID_WorldView:
			{
				Matrix4 worldView = world * view;
				effectParam->SetValue(&worldView);
				//SetValue(effectParam, worldView);
			}
			break;

		case EPID_WorldInverse:
			{
				Matrix4 worldInv = world;
				worldInv.Inverse();
				effectParam->SetValue(&worldInv);
				//SetValue(effectParam, worldInv);
			}
			break;

		case EPID_WorldViewInverseTranspose:
			{
				Matrix4 worldView = world * view;
				worldView.Inverse();
				worldView.Transpose();
				effectParam->SetValue(&worldView);
				//SetValue(effectParam, worldView);
			}
			break;

		case EPID_WorldViewInverse:
			{
				Matrix4 worldViewInv = world * view;
				worldViewInv.Inverse();
				effectParam->SetValue(&worldViewInv);
				//SetValue(effectParam, worldViewInv);
			}
			break;

		case EPID_Projection:
			{
				effectParam->SetValue(&projection);
				//SetValue(effectParam, projection);
			}
			break;

		case EPID_ProjectionInverse:
			{
				Matrix4 projInv = projection;
				projInv.Inverse();
				effectParam->SetValue(&projInv);
				//SetValue(effectParam, projInv);
/*
				Vector4 test(0.f, 0.f, 1.0f, 1.f);
				test = projInv * test;
				test.z = test.z / test.w;
				int nothing = 0;*/
			}
			break;

		case EPID_WorldViewProjection:
			{
				Matrix4 worldViewProj = world * view * projection;
				effectParam->SetValue(&worldViewProj);
				//SetValue(effectParam, worldViewProj);
				int nothing = 0;
			}
			break;

		case EPID_WorldViewProjectionInverseTranspose:
			{
				Matrix4 worldViewProj = world * view * projection;
				worldViewProj.Inverse();
				worldViewProj.Transpose();
				effectParam->SetValue(&worldViewProj);
				//SetValue(effectParam, worldViewProj);
			}
			break;

		case EPID_WorldViewProjectionInverse:
			{
				Matrix4 worldViewProj = world * view * projection;
				worldViewProj.Inverse();
				effectParam->SetValue(&worldViewProj);
				//SetValue(effectParam, worldViewProj);
			}
			break;

		case EPID_ViewProjectionInverse:
			{
				Matrix4 viewProjInv = view * projection;
				viewProjInv.Inverse();
				effectParam->SetValue(&viewProjInv);
				//SetValue(effectParam, viewProjInv);
			}
			break;

		case EPID_ViewProjection:
			{
				Matrix4 viewProj = view * projection;
				effectParam->SetValue(&viewProj);
				//SetValue(effectParam, viewProj);
			}
			break;

		default:
			//Log::Print("[DX10Effect::SetMatrixParameters] Unknown parameter");
			break;
		}
	}
}

bool Effect::SetProjectorParameters(Camera** camera, unsigned int numCamera, const Matrix4& world)
{
	EffectParameter* structEffectParam = GetParameterByID(EPID_Projector, false);
	if (!structEffectParam)
		return false;

	int structSize = structEffectParam->mSize;
	char* buffer = new char[structSize * numCamera];

	for (unsigned int c = 0; c < numCamera; ++c)
	{
		const Matrix4& projection = camera[c]->GetProjection();
		const Matrix4& view = camera[c]->GetView();
		const Matrix4& camWorld = camera[c]->GetWorld();

		for (unsigned int p = 0; p < structEffectParam->mChild.size(); ++p)
		{
			EffectParameter* effectParam = structEffectParam->mChild[p];
			EffectParameterHandle handle = effectParam->mHandle;

			Matrix4 matrix;

			switch (effectParam->mID)
			{
			case EPID_View:
				{
					matrix = view;
				}
				break;

			case EPID_ViewInverseTranspose:
				{
					Matrix4 viewIT = view;
					viewIT.Inverse();
					viewIT.Transpose();
					matrix = viewIT;
				}
				break;

			case EPID_ViewInverse:
				{
					Matrix4 viewInv = view;
					viewInv.Inverse();
					matrix = viewInv;
				}
				break;

			case EPID_ViewTranspose:
				{
					Matrix4 viewT = view;
					viewT.Transpose();
					matrix = viewT;
				}
				break;

			case EPID_World:
				{
					matrix = world;
				}
				break;

			case EPID_Camera:
				{
					matrix = camWorld;
				}
				break;

			case EPID_WorldInverseTranspose:
				{
					Matrix4 worldIT = world;
					worldIT.Inverse();
					worldIT.Transpose();
					matrix = worldIT;
				}
				break;

			case EPID_WorldView:
				{
					Matrix4 worldView = world * view;
					matrix = worldView;
				}
				break;

			case EPID_WorldInverse:
				{
					Matrix4 worldInv = world;
					worldInv.Inverse();
					matrix = worldInv;
				}
				break;

			case EPID_WorldViewInverseTranspose:
				{
					Matrix4 worldView = world * view;
					worldView.Inverse();
					worldView.Transpose();
					matrix = worldView;
				}
				break;

			case EPID_WorldViewInverse:
				{
					Matrix4 worldViewInv = world * view;
					worldViewInv.Inverse();
					matrix = worldViewInv;
				}
				break;

			case EPID_Projection:
				{
					matrix = projection;
				}
				break;

			case EPID_ProjectionInverse:
				{
					Matrix4 projInv = projection;
					projInv.Inverse();
					matrix = projInv;
				}
				break;

			case EPID_WorldViewProjection:
				{
					Matrix4 worldViewProj = world * view * projection;
					matrix = worldViewProj;
				}
				break;

			case EPID_WorldViewProjectionInverseTranspose:
				{
					Matrix4 worldViewProj = world * view * projection;
					worldViewProj.Inverse();
					worldViewProj.Transpose();
					matrix = worldViewProj;
				}
				break;

			case EPID_WorldViewProjectionInverse:
				{
					Matrix4 worldViewProj = world * view * projection;
					worldViewProj.Inverse();
					matrix = worldViewProj;
				}
				break;

			case EPID_ViewProjectionInverse:
				{
					Matrix4 viewProjInv = view * projection;
					viewProjInv.Inverse();
					matrix = viewProjInv;
				}
				break;

			case EPID_ViewProjection:
				{
					Matrix4 viewProj = view * projection;
					matrix = viewProj;
				}
				break;

			default:
				//Log::Print("[DX10Effect::SetMatrixParameters] Unknown parameter");
				break;
			}

			matrix.Transpose();
			memcpy(&buffer[structSize * c + effectParam->mStructOffset], &matrix, sizeof(float) * 16);
		}
	}

	SetValue(structEffectParam, buffer, structSize * numCamera);
	delete[] buffer;
	return true;
}

bool Effect::Load(const ObjectLoadParams* params, SceneNode* node)
{
	mDevice = params->mDevice;
	mFactory = params->mFactory;
	return true;
}

void Effect::SetValue(EffectParameter* param)
{
	switch (param->mType)
	{
	case EPT_Texture:
	case EPT_Texture2D:
	case EPT_TextureCube:
	case EPT_Texture3D:
		{
			Texture* value;
			param->GetValue(&value);
			SetValue(param, &value);
		}
		break;

	case EPT_Matrix4:
		{
			int count = 1;
			Matrix4 value[128];
			count = param->GetDataSize() / sizeof(Matrix4);
			Assert(count <= 128);
			param->GetValue((Matrix4*)&value, count);
			SetValue(param, (Matrix4*)&value, count);
		}
		break;

	case EPT_Vector4:
		{
			Vector4 value;
			param->GetValue(&value);
			SetValue(param, &value);
		}
		break;

	case EPT_Int:
		{
			int value;
			param->GetValue(&value);
			SetValue(param, &value);
		}
		break;

	case EPT_Float:
		{
			float value;
			param->GetValue(&value);
			SetValue(param, &value);
		}
		break;

	case EPT_Unknown:
	default:
		{
			//Assert(false);
		}
		break;
	}
}

void Effect::SetPassParameter()
{
	EffectParameterIterator it;
	for (it = BeginParameter(); it != EndParameter(); ++it)
		(*it)->Apply();
}

bool Effect::IsInstance(const ObjectLoadParams* params, SceneObject* loaded)
{
	Effect* other = static_cast<Effect*>(loaded);
	if (strcmpi(mName.c_str(), other ? other->mName.c_str() : params->mName.c_str()) == 0)
		return true;

	return false;
}