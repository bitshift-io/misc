#include "AnimationController.h"
#include "SceneObject.h"
#include "RenderFactory.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

AnimationStream::AnimationStream() :
	mData(0),
	mTime(0),
	mCount(0),
	mMaxCount(0)
{
}

void AnimationStream::Destroy(RenderFactory* factory)
{
	if (mTime)
	{
		free(mTime);
		mTime = 0;
	}

	if (mData)
	{
		if (mType == ST_String)
		{
			string** strData = (string**)mData;
			for (unsigned int i = 0; i < mCount; ++i)
			{
				delete strData[i];
			}
		}

		_aligned_free(mData);
		mData = 0;
	}	
}

void AnimationStream::GetValue(Matrix4& value, float time) const
{

}

void AnimationStream::GetValue(float& value, float time) const
{

}

void AnimationStream::GetValue(string& value, float time)	const
{
}

void AnimationStream::GetValue(Quaternion& value, float time) const
{
	Quaternion* quatData = (Quaternion*)mData;

	if (mCount <= 1)
		value = quatData[0];

	unsigned int firstKey = 0;
	unsigned int secondKey = 0;
	float blend = 0.f;
	GetKeyIndex(time, firstKey, secondKey, blend);

	Quaternion first = quatData[firstKey];
	Quaternion second = quatData[secondKey];

	value = first.Interpolate(second, blend);
}

void AnimationStream::GetValue(Vector4& value, float time) const
{
	Vector4* vectorData = (Vector4*)mData;

	if (mCount <= 1)
		value = vectorData[0];

	unsigned int firstKey = 0;
	unsigned int secondKey = 0;
	float blend = 0.f;
	GetKeyIndex(time, firstKey, secondKey, blend);

	Vector4 first = vectorData[firstKey];
	Vector4 second = vectorData[secondKey];

	value = first * (1.0f - blend) + second * blend;
}

void AnimationStream::GetKeyIndex(float time, unsigned int& firstKey, unsigned int& secondKey, float& blend) const
{
	// pick the last key in the list incase no keys are valid
	unsigned int size = mCount;
	firstKey = Math::Max((unsigned int)0, size - 2);
	secondKey = Math::Max((unsigned int)0, size - 1);
	blend = 1.f;

	for (unsigned int i = 0; i < size; ++i)
	{
		float keyTime = mTime[i];
		if (keyTime > time)
		{
			firstKey = i - 1;
			secondKey = i;

			if (firstKey < 0)
			{
				firstKey = 0;
				blend = 1.0f;
				return;
			}

			float prevTime = mTime[firstKey];
			blend = (time - prevTime) / (keyTime - prevTime);
			return;
		}
	}
}

void AnimationStream::IncreaseSize(unsigned int size)
{
	// buffer aint big enough? alloc a new one twice the size
	unsigned int curSize = mCount * GetTypeSize(mType);
	unsigned int maxSize = mMaxCount * GetTypeSize(mType);
	if ((curSize + size) > maxSize)
	{
		float* oldTime = mTime;
		void* oldData = mData;
		
		unsigned int newMaxSize = max(size * 100, maxSize * 2);
		mData = _aligned_malloc(newMaxSize, 16);
		memcpy(mData, oldData, curSize);

		mMaxCount = newMaxSize / GetTypeSize(mType);

		mTime = (float*)malloc(sizeof(float) * mMaxCount);
		memcpy(mTime, oldTime, sizeof(float) * mCount);

		if (oldData)
		{
			 _aligned_free(oldData);
			 free(mTime);
		}
	}
}

void AnimationStream::AppendValue(Quaternion& value, float time)
{
	IncreaseSize(sizeof(Quaternion));

	mTime[mCount] = time;

	Quaternion* data = (Quaternion*)((unsigned char*)mData + mCount * sizeof(Quaternion));
	*data = value;
	++mCount;
}

void AnimationStream::AppendValue(Vector4& value, float time)
{
	IncreaseSize(sizeof(Vector4));

	mTime[mCount] = time;

	Vector4* data = (Vector4*)((unsigned char*)mData + mCount * sizeof(Vector4));
	*data = value;
	++mCount;
}

void AnimationStream::AppendValue(float value, float time)
{
	IncreaseSize(sizeof(float));

	mTime[mCount] = time;

	float* data = (float*)((unsigned char*)mData + mCount * sizeof(float));
	*data = value;
	++mCount;
}

unsigned int AnimationStream::GetTypeSize(StreamType type)
{
	Assert(sizeof(Vector4) == 16);
	Assert(sizeof(Quaternion) == 16);

	switch (type)
	{
	case ST_Position:
	case ST_Scale:
	case ST_Vector4:
		return sizeof(Vector4);
	case ST_Rotation:
		return sizeof(Quaternion);
	case ST_Alpha:
	case ST_Float:
		return sizeof(float);
	case ST_Matrix4:
		return sizeof(Matrix4);
	case ST_String:
		return sizeof(string*);
	default:
		break;
	}

	return -1;
}

bool AnimationStream::Save(const File& out)
{
	// write type
	out.Write(&mType, sizeof(StreamType));

	// write the count
	out.Write(&mCount, sizeof(unsigned int));

	// write time
	out.Write(mTime, sizeof(float) * mCount);

	// write data
	if (mType == ST_String)
	{
		string* strData = (string*)mData;
		for (unsigned int i = 0; i < mCount; ++i)
		{
			string& str = strData[i];

			unsigned int len = str.length() + 1;
			out.Write(&len, sizeof(unsigned int));
			out.Write(str.c_str(), len);
		}
	}
	else
	{
		unsigned int size = out.Write(mData, mCount * GetTypeSize(mType));
		Assert(size == mCount * GetTypeSize(mType));
	}

	return true;
}

bool AnimationStream::Load(const ObjectLoadParams* params)
{
	File* in = params->mFile;
	char buffer[1024];

	// read type
	in->Read(&mType, sizeof(StreamType));
	Assert(mType >= 0 && mType < ST_MAX);

	// read the count
	in->Read(&mCount, sizeof(unsigned int));

	// read time
	mTime = (float*)malloc(sizeof(float) * mCount);
	in->Read(mTime, sizeof(float) * mCount);

	// read the data
	if (mType == ST_String)
	{
		mData = _aligned_malloc(mCount * GetTypeSize(mType), 16);
		string** strData = (string**)mData;
		for (unsigned int i = 0; i < mCount; ++i)
		{
			unsigned int len = 0;
			in->Read(&len, sizeof(unsigned int));
			Assert(len < 1024);
			in->Read(buffer, len);
			strData[i] = new string(buffer); // FREE ME!!!!!!
		}
	}
	else
	{
		mData = _aligned_malloc(mCount * GetTypeSize(mType), 16);
		unsigned int size = in->Read(mData, mCount * GetTypeSize(mType));
		Assert(size == mCount * GetTypeSize(mType));
		if (in->EndOfFile())
		{
			int nothing = 0;
		}
	}

	return true;
}

AnimationStream* AnimationNode::GetStream(StreamType streamType)
{
	Iterator it;
	for (it = mStream.begin(); it != mStream.end(); ++it)
	{
		if ((*it).GetType() == streamType)
			return &(*it);
	}

	return 0;
}

AnimationStream* AnimationNode::CreateStream()
{
	mStream.push_back(AnimationStream());
	return &mStream.back();
}

void AnimationNode::Destroy(RenderFactory* factory)
{
	Iterator it;
	for (it = mStream.begin(); it != mStream.end(); ++it)
	{
		it->Destroy(factory);
	}

	mStream.clear();
	vector<AnimationStream>(mStream).swap(mStream);
}

bool AnimationNode::Save(const File& out)
{
	// write name
	unsigned int len = mName.length() + 1;
	out.Write(&len, sizeof(unsigned int));
	out.Write(mName.c_str(), len);

	// write count
	unsigned int size = mStream.size();
	out.Write(&size, sizeof(unsigned int));

	// write streams
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		(*it).Save(out);
	}


	return true;
}

bool AnimationNode::Load(const ObjectLoadParams* params)
{
	File* in = params->mFile;
	char buffer[256];

	// read name
	unsigned int len = 0;
	in->Read(&len, sizeof(unsigned int));
	in->Read(buffer, len);
	mName = buffer;

	// read count
	unsigned int size = 0;
	in->Read(&size, sizeof(unsigned int));

	// read streams
	for (unsigned int i = 0; i < size; ++i)
	{
		AnimationStream* stream = CreateStream();
		stream->Load(params);
	}

	return true;
}


bool Animation::Save(const File& out, SceneNode* node)
{/*
	// write name
	unsigned int len = mName.length() + 1;
	out.Write(&len, sizeof(unsigned int));
	out.Write(mName.c_str(), len);
*/
	// write anim length
	out.Write(&mLength, sizeof(float));	

	// write count
	unsigned int size = mNode.size();
	out.Write(&size, sizeof(unsigned int));

	// write nodes
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		(*it)->Save(out);
	}

	return true;
}

bool Animation::Load(const ObjectLoadParams* params, SceneNode* node)
{
	File* in = params->mFile;
	if (!in)
	{
		string path;
		params->mFactory->GetParameter().GetExistingFile(params->mName, "anm", path);
		File file(path.c_str(), FAT_Read | FAT_Binary);
		if (!file.Valid())
			return false;

		const_cast<ObjectLoadParams*>(params)->mFile = &file;
		return Load(params, node);
	}

	SetName(params->mName.c_str());
	/*
	char buffer[256];

	// read name
	unsigned int len = 0;
	in->Read(&len, sizeof(unsigned int));
	in->Read(buffer, len);
	mName = buffer;
*/
	// read anim length
	in->Read(&mLength, sizeof(float));	

	// read count
	unsigned int size = 0;
	in->Read(&size, sizeof(unsigned int));
	//mStream.resize(size);

	// read streams
	for (unsigned int i = 0; i < size; ++i)
	{
		AnimationNode* node = CreateNode();
		node->Load(params);
		mNode.push_back(node);
	}

	return true;
}

AnimationNode* Animation::GetAnimationNodeByName(const char* name)
{
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		if (strcmpi((*it)->GetName(), name) == 0)
			return (*it);
	}

	return 0;
}

bool Animation::IsInstance(const ObjectLoadParams* params, SceneObject* loaded)
{
	Animation* other = static_cast<Animation*>(loaded);
	if (strcmpi(mName.c_str(), other ? other->mName.c_str() : params->mName.c_str()) == 0)
		return true;

	return false;
}

void Animation::Destroy(RenderFactory* factory)
{
	vector<AnimationNode*>::iterator it = Begin();
	for (it = Begin(); it != End(); ++it)
	{
		(*it)->Destroy(factory);
		delete *it;
	}

	mNode.clear();
	vector<AnimationNode*>(mNode).swap(mNode);
}


/*

Animation* AnimationController::GetAnimation(const char* name)
{
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		if (strcmpi((*it)->GetName(), name) == 0)
			return *it;
	}

	return 0;
}

Animation* AnimationController::CreateAnimation()
{
	Animation* anim = new Animation();
	mAnimation.push_back(anim);
	return anim;
}

bool AnimationController::Save(const File& out)
{
	unsigned int count = GetNumAnimation();
	out.Write(&count, sizeof(unsigned int));

	Iterator it;
	for (it = Begin(); it != End(); ++it)
		(*it)->Save(out);

	return true;
}

bool AnimationController::Load(const ObjectLoadParams* params)
{
	unsigned int count = 0;
	params->mFile->Read(&count,  sizeof(unsigned int));

	for (unsigned int i = 0; i < count; ++i)
	{
		// when do we free this?
		Animation* anim = new Animation;
		anim->Load(params);
		mAnimation.push_back(anim);
	}

	// set default anim if it exists
	SetAnimation(GetAnimation("default"));
	return true;
}

void AnimationController::SetAnimation(Animation* anim, float weight)
{
	if (!anim)
		return;

	Blend blend;
	blend.anim = anim;
	blend.weight = weight;
	blend.time = 0.f;
	mBlend.push_back(blend);
}

void AnimationController::SetTime(float time)
{
	vector<Blend>::iterator it;
	for (it = mBlend.begin(); it != mBlend.end(); ++it)
	{
		it->time = time;
	}
}

Matrix4	AnimationController::GetTransform()
{
	Quaternion rotation(QI_Identity);
	Vector4 position(0.f, 0.f, 0.f);

	vector<Blend>::iterator it;
	for (it = mBlend.begin(); it != mBlend.end(); ++it)
	{
		Quaternion animRot(QI_Identity);
		Vector4 animPos(0.f, 0.f, 0.f);

		AnimationStream* rotStream = it->anim->GetStream(ST_Rotation);
		AnimationStream* posStream = it->anim->GetStream(ST_Position);

		if (rotStream)
			rotStream->GetValue(animRot, it->time);

		if (posStream)
			posStream->GetValue(animPos, it->time);

		rotation = rotation.Interpolate(animRot, it->weight);
		position = position * (1.0f - it->weight) + animPos * it->weight;
	}

	Matrix4 transform = rotation.GetRotationMatrix();
	transform.SetTranslation(position);
	return transform;
}
*/


void SceneAnimationNode::InsertBlend(AnimationNode* node, float weight, float time)
{
	if (!node)
		return;

	Blend blend;
	blend.weight = weight;
	blend.time = time;
	blend.node = node;
	mBlend.push_back(blend);
}

AnimationNode* SceneAnimationNode::GetAnimationNode(Animation* anim)
{
	vector<AnimationNode*>::iterator it;
	for (it = mAnimNode.begin(); it != mAnimNode.end(); ++it)
	{
		if ((*it)->GetAnimation() == anim)
			return (*it);
	}

	return 0;
}

Matrix4 SceneAnimationNode::GetTransform()
{
	Quaternion rotation(QI_Identity);
	Vector4 position(0.f, 0.f, 0.f);
	Vector4 scale(1.f, 1.f, 1.f);

	vector<Blend>::iterator it;
	for (it = mBlend.begin(); it != mBlend.end(); ++it)
	{
		Quaternion animRot(QI_Identity);
		Vector4 animPos(0.f, 0.f, 0.f);
		Vector4 animScale(1.f, 1.f, 1.f);

		AnimationStream* rotStream = it->node->GetStream(ST_Rotation);
		AnimationStream* posStream = it->node->GetStream(ST_Position);
		AnimationStream* scaleStream = it->node->GetStream(ST_Scale);

		if (rotStream)
			rotStream->GetValue(animRot, it->time);

		if (posStream)
			posStream->GetValue(animPos, it->time);

		if (scaleStream)
			scaleStream->GetValue(animScale, it->time);

		rotation = rotation.Interpolate(animRot, it->weight);
		position = position * (1.0f - it->weight) + animPos * it->weight;
		scale = scale * (1.0f - it->weight) + animScale * it->weight;
	}

	Matrix4 transform = rotation.GetRotationMatrix();
	//transform.Translate(position);
	transform.SetTranslation(position);

	// can we improve performance here?
	Matrix4 scaleMat(MI_Identity);
	scaleMat.SetScale(scale);
	transform = transform * scaleMat;

	return transform;
}

void SceneAnimationNode::ProgressTime(float deltaTime, Animation* animation)
{
	vector<Blend>::iterator it;
	for (it = mBlend.begin(); it != mBlend.end(); ++it)
	{
		if (!animation || it->node->GetAnimation() == animation)
			it->time += deltaTime;
	}
}

void SceneAnimationNode::SetTime(float time, Animation* animation)
{
	vector<Blend>::iterator it;
	for (it = mBlend.begin(); it != mBlend.end(); ++it)
	{
		if (!animation || it->node->GetAnimation() == animation)
			it->time = time;
	}
}