#ifndef _OGLWINDOW_H_
#define _OGLWINDOW_H_


// Standard Windows includes
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#define NOMINMAX
//#define UNICODE
#include <windows.h>
#undef SendMessage
#undef CreateWindow
#undef MessageBox
/*
#include <assert.h>
#include <wchar.h>
#include <mmsystem.h>
#include <commctrl.h> // for InitCommonControls() 
#include <shellapi.h> // for ExtractIcon()
#include <new.h>      // for placement new
#include <shlobj.h>
#include <math.h>      
#include <limits.h>      
#include <stdio.h>
*/
#include "../Window.h"


// CRT's memory leak detection
#if defined(DEBUG) || defined(_DEBUG)
#include <crtdbg.h>
#endif

//using namespace Win32;

class Win32Window : public Window
{
	friend class Device;
	friend LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

public:

	// 0 width and height indicates auto
	virtual bool	Init(const char* title, const WindowResolution* resolution = 0, bool fullscreen = false);
	virtual void	Deinit();
	virtual bool	Show();

	// pass NULL to restore default desktop resolution
	virtual bool						SetWindowResolution(const WindowResolution* resolution);
	virtual bool						GetWindowResolution(WindowResolution& resolution) const;
	virtual bool						GetDesktopResolution(WindowResolution& resolution) const;

	// returns true if quit message recived
	bool			HandleMessages();

	HWND			GetHandle()						{ return mHWnd; }
	HINSTANCE		GetHInstance()					{ return mHInst; }
	virtual bool	IsFullScreen()					{ return mFullScreen; }

	virtual bool	SetCursorPosition(int x, int y);
	virtual bool	GetCursorPosition(int& x, int& y);
	virtual int		SetCursorVisibility(bool visible);
	virtual bool	IsCursorVisible();
	virtual void	CenterCursor();

	virtual bool	IsResolutionSupported(int width, int height, int bits);
	virtual bool	HasFocus();

	virtual const char*		GetTitle();

	virtual int		MessageBox(const char* text, const char* title, int flags);

protected:

	LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	WindowResolution	mCurrentResolution;

	bool			mExit;
	HWND			mHWnd;
	HINSTANCE		mHInst;	
	bool			mFocus;
	bool			mFullScreen;
	string			mTitle;
	WCHAR*			mWideTitle;
};

#endif