#include "Win32Window.h"
#include "File/Log.h"

LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	Win32Window* window = (Win32Window*)GetWindowLong(hWnd, GWL_USERDATA);

	if (!window)
		return DefWindowProc(hWnd,uMsg,wParam,lParam);

	return window->WndProc(hWnd, uMsg, wParam, lParam);
}


bool Win32Window::Init(const char* title, const WindowResolution* resolution, bool fullscreen)
{
	// get a list of available window resolutions
	BOOL		retVal;
	DEVMODE		devMode;
	DWORD		mode = 0;

	do
	{
		retVal = EnumDisplaySettings(NULL, mode, &devMode);
		++mode;
		if (retVal)
		{
			WindowResolution res;
			res.bits = devMode.dmBitsPerPel;
			res.height = devMode.dmPelsHeight;
			res.width = devMode.dmPelsWidth;

			// check if we already have this resolution in the list
			vector<WindowResolution>::iterator it;
			for (it = mResolution.begin(); it != mResolution.end(); ++it)
			{
				if (it->bits == res.bits && it->width == res.width && it->height == res.height)
					break;
			}
			if (it != mResolution.end())
				continue;

			// sort insert resolution
			bool bInserted = false;
			for (it = mResolution.begin(); it != mResolution.end(); ++it)
			{
				if (it->bits >= res.bits && it->width >= res.width && it->height >= res.height)
				{
					mResolution.insert(it, res);
					bInserted = true;
					break;
				}
			}
			if (!bInserted)
				mResolution.push_back(res);
		}
	} while (retVal);

	// no resolution passed, so use desktop resolution
	if (!resolution)
		GetDesktopResolution(mCurrentResolution);
	else
		mCurrentResolution = *resolution;

	int len = strlen(title);
	mWideTitle = new WCHAR[len + 1];
	mWideTitle[len] = '\0';
	MultiByteToWideChar(CP_ACP, 0, title, len, mWideTitle, len);

	mTitle = title;
	mFullScreen = fullscreen;
	mExit = false;
	mHInst = GetModuleHandle(NULL); 

    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof(WNDCLASSEX); 
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc    = WindowProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = mHInst;
    wcex.hIcon          = LoadIcon(mHInst, IDI_WINLOGO);
    wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = NULL;
    wcex.lpszClassName  = mWideTitle; //L"RenderWindowClass";
    wcex.hIconSm        = LoadIcon(wcex.hInstance, IDI_WINLOGO);

    if (!RegisterClassEx(&wcex))
        return false;

	if (fullscreen)												// Attempt Fullscreen Mode?
	{
		DEVMODE dmScreenSettings;								// Device Mode
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));	// Makes Sure Memory's Cleared
		dmScreenSettings.dmSize=sizeof(dmScreenSettings);		// Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth	= mCurrentResolution.width;				// Selected Screen Width
		dmScreenSettings.dmPelsHeight	= mCurrentResolution.height;				// Selected Screen Height
		dmScreenSettings.dmBitsPerPel	= mCurrentResolution.bits;					// Selected Bits Per Pixel
		dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
		if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
		{
			Log::Error("Failed to set fullscreen, trying window mode");
			fullscreen = false;
		}
	}
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style

	if (fullscreen)												// Are We Still In Fullscreen Mode?
	{
		dwExStyle = WS_EX_APPWINDOW;								// Window Extended Style
		dwStyle = WS_POPUP | WS_MAXIMIZE;										// Windows Style
	}
	else
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
		dwStyle = WS_OVERLAPPEDWINDOW;							// Windows Style
	}

    // Create window
    RECT rc = {0, 0, mCurrentResolution.width, mCurrentResolution.height};
	AdjustWindowRectEx(&rc, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(mHWnd = CreateWindowEx(dwExStyle,							// Extended Style For The Window
								mWideTitle,							// Class Name
								mWideTitle,								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								rc.right-rc.left,	// Calculate Window Width
								rc.bottom-rc.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								mHInst,							// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		Deinit();								// Reset The Display
		Log::Error("Window Creation Error");
		return false;
	}
	
	SetWindowLong(mHWnd, GWL_USERDATA, (long)this);
	return true;
}

void Win32Window::Deinit()
{
	if (mFullScreen)										// Are We In Fullscreen Mode?
	{
		ChangeDisplaySettings(NULL, 0);					// If So Switch Back To The Desktop
	}

	DestroyWindow(mHWnd);
	UnregisterClass(mWideTitle /*L"RenderWindowClass"*/, NULL);
	mHWnd = 0;
	delete[] mWideTitle;
}

bool Win32Window::GetWindowResolution(WindowResolution& resolution) const
{
	resolution = mCurrentResolution;
	return true;
}

bool Win32Window::GetDesktopResolution(WindowResolution& resolution) const
{
	BOOL		retVal;
	DEVMODE		devMode;
	DWORD		mode = 0;

	retVal = EnumDisplaySettings(NULL, ENUM_REGISTRY_SETTINGS, &devMode);
	if (!retVal)
		return false;

	resolution.bits = devMode.dmBitsPerPel;
	resolution.height = devMode.dmPelsHeight;
	resolution.width = devMode.dmPelsWidth;
	return true;
}

bool Win32Window::SetWindowResolution(const WindowResolution* resolution)
{
	WindowResolution desiredRes;
	if (!resolution)
		GetDesktopResolution(desiredRes);
	else
		desiredRes = *resolution;

	DEVMODE		devMode;
	devMode.dmSize = sizeof(DEVMODE);
	devMode.dmBitsPerPel = desiredRes.bits;
	devMode.dmPelsWidth = desiredRes.width;
	devMode.dmPelsHeight = desiredRes.height;
	devMode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

	bool result = (ChangeDisplaySettings(&devMode, 0) == DISP_CHANGE_SUCCESSFUL);
	if (result)
		mCurrentResolution = desiredRes;

	return result;
}

bool Win32Window::Show()
{
	::ShowWindow(mHWnd, SW_SHOW);						// Show The Window
	SetForegroundWindow(mHWnd);						// Slightly Higher Priority
	SetFocus(mHWnd);									// Sets Keyboard Focus To The Window
	return true;
}

bool Win32Window::HandleMessages()
{
	MSG	msg;

	while (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
	{
		if (msg.message == WM_QUIT)				// Have We Received A Quit Message?
		{
			return true;
		}
		else									// If Not, Deal With Window Messages
		{
			TranslateMessage(&msg);				// Translate The Message
			DispatchMessage(&msg);				// Dispatch The Message
		}
	}

	return mExit;
}

LRESULT CALLBACK Win32Window::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)									// Check For Windows Messages
	{
	case WM_SIZE:
		{
			if (!IsFullScreen())
			{
				mCurrentResolution.width = LOWORD(lParam);
				mCurrentResolution.height = HIWORD(lParam);
			}
		}
		break;

	case WM_ACTIVATE:							// Watch For Window Activate Message
		{
			if (!HIWORD(wParam))					// Check Minimization State
			{
				//if (Input::IsValid())
				//	Input::GetInstance().NotifyActive(true);

				//while (IsCursorVisible())
				//	SetCursorVisibility(false);

				mFocus = true;
			}
			else
			{
				//if (Input::IsValid())
				//	Input::GetInstance().NotifyActive(false);
		
				//while (!IsCursorVisible())
				//	SetCursorVisibility(true);

				mFocus = false;
			}

		}
		return 0;

	case WM_SYSCOMMAND:							// Intercept System Commands
		{
			switch (wParam)							// Check System Calls
			{
				case SC_SCREENSAVE:					// Screensaver Trying To Start?
				case SC_MONITORPOWER:				// Monitor Trying To Enter Powersave?
					return 0;							// Prevent From Happening
			}
		}
		break;		

	case WM_CLOSE:								// Did We Receive A Close Message?
		{
			mExit = true;
		}
		return 0;		
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

bool Win32Window::SetCursorPosition(int x, int y)
{
	WINDOWINFO wndInfo;
	memset(&wndInfo, 0, sizeof(WINDOWINFO));
	wndInfo.cbSize = sizeof(WINDOWINFO);
	GetWindowInfo((HWND)GetHandle(), &wndInfo);

	x = x + wndInfo.rcClient.left;
	y = y + wndInfo.rcClient.top;

	return SetCursorPos(x, y) == TRUE;
}

bool Win32Window::GetCursorPosition(int& x, int& y)
{
	POINT point;
	GetCursorPos(&point);

	WINDOWINFO wndInfo;
	memset(&wndInfo, 0, sizeof(WINDOWINFO));
	wndInfo.cbSize = sizeof(WINDOWINFO);
	GetWindowInfo((HWND)GetHandle(), &wndInfo);

	x = point.x - wndInfo.rcClient.left;
	y = point.y - wndInfo.rcClient.top;

	if (x < 0 || y < 0)
		return false;

	if (x > mCurrentResolution.width || y > mCurrentResolution.height)
		return false;

	return true;
}

int	Win32Window::SetCursorVisibility(bool visible)
{
	return ShowCursor(visible ? TRUE : FALSE);
}

void Win32Window::CenterCursor()
{
	RECT rect;
	GetWindowRect(mHWnd, &rect); //GetWindowRect GetClientRect
	SetCursorPos((rect.right + rect.left) / 2, (rect.bottom + rect.top) / 2);
}

bool Win32Window::IsCursorVisible()
{
	CURSORINFO cursoInfo;
	cursoInfo.cbSize = sizeof(CURSORINFO);
	GetCursorInfo(&cursoInfo);

	return (cursoInfo.flags == CURSOR_SHOWING);
}

bool Win32Window::IsResolutionSupported(int width, int height, int bits)
{
	BOOL		retVal;
	DEVMODE		devMode;
	DWORD		mode = 0;

	do
	{
		retVal = EnumDisplaySettings(NULL, mode, &devMode);
		++mode;
		if (retVal)
		{
			if (devMode.dmPelsWidth == width 
				&& devMode.dmPelsHeight == height 
				&& devMode.dmBitsPerPel == bits)
				return true;
		}
	}
	while (retVal);	

	return false;
}

bool Win32Window::HasFocus()
{
	HWND hFocus = GetFocus();
	return hFocus == mHWnd;
}

const char*	Win32Window::GetTitle()
{
	return mTitle.c_str();
}

int Win32Window::MessageBox(const char* text, const char* title, int flags)
{
	UINT uType = 0;

	if (flags & (MBF_Ok | MBF_Cancel))
		uType = MB_OKCANCEL;
	else if (flags & MBF_Ok)
		uType = MB_OK;
	
	int titleLen = strlen(title);
	WCHAR* wideTitle = new WCHAR[titleLen + 1];
	wideTitle[titleLen] = '\0';
	MultiByteToWideChar(CP_ACP, 0, title, titleLen, wideTitle, titleLen);

	int textLen = strlen(text);
	WCHAR* wideText = new WCHAR[textLen + 1];
	wideText[textLen] = '\0';
	MultiByteToWideChar(CP_ACP, 0, text, textLen, wideText, textLen);

	int result = ::MessageBoxW(mHWnd, wideText, wideTitle, uType);

	delete[] wideText;
	delete[] wideTitle;

	int ret = 0;
	if (result |= IDOK)
		ret |= MBF_Ok;

	if (result |= IDCANCEL)
		ret |= MBF_Cancel;

	return ret;
}