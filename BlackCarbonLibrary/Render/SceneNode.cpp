#include "SceneNode.h"
#include "RenderFactory.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

SceneNode* SceneNode::GetNodeByName(const string& name)
{
	if (strcmpi(name.c_str(), GetName().c_str()) == 0)
		return this;

	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		SceneNode* ret = 0;
		if (ret = (*it)->GetNodeByName(name))
			return ret;
	}

	return 0;
}

bool SceneNode::Clone(ObjectLoadParams* params, SceneNode* copy, Scene* scene)
{
	SetName(copy->GetName());
	mTransform = copy->GetOrigonalLocalTransform();
	mOrigonalTransform = mTransform;

	PropertyIterator propIt;
	for (propIt = copy->PropertyBegin(); propIt != copy->PropertyEnd(); ++propIt)
		AddProperty(*propIt);

	SceneObject* obj = copy->GetObject();
	if (obj)
	{
		//SceneObject* clone = obj->Clone(this);
		SceneObject* clone = mOwner->GetRenderFactory()->Clone(obj, this);
		if (clone)
			obj = clone;
		else
			obj->AddReference();		
	}

	SetObject(obj);

	Iterator it;
	for (it = copy->Begin(); it != copy->End(); ++it)
	{
		if (copy->GetOwner() == scene)
		{
			SceneNode* child = mOwner->CreateNode();
			child->SetParent(this);
			child->Clone(params, *it, scene);
			InsertChild(child);
		}
		else
		{
			int nothng = 0;
		}
	}

	return true;
}

bool SceneNode::Load(ObjectLoadParams* params, Scene* scene)
{
	File* in = params->mFile;

	// read the node name
	char buffer[1024];
	int len = 0;
	in->Read(&len, sizeof(int));
	in->Read(buffer, sizeof(char) * len);
	buffer[len] = '\0';
	SetName(buffer);

	// read the flags
	in->Read(&mFlags, sizeof(unsigned int));

	// read the transform
	in->Read(&mTransform, sizeof(Matrix4));
	mOrigonalTransform = mTransform;

	// read number of properties
	unsigned int numProperty = 0;
	in->Read(&numProperty, sizeof(unsigned int));

	// write the properties
	for (unsigned int i = 0; i < numProperty; ++i)
	{
		char key[1024];
		char value[2048];

		int len = 0;
		in->Read(&len, sizeof(int));
		in->Read(key, sizeof(char) * len);
		key[len] = '\0';

		len = 0;
		in->Read(&len, sizeof(int));
		in->Read(value, sizeof(char) * len);
		value[len] = '\0';

		Property property;
		property.mKey = key;
		property.mValue = value;

		AddProperty(property);
	}

	// load the type
	int type = -1;
	if (in->Read(&type, sizeof(int)) != sizeof(int))
		return false;

	if (type != -1)
	{
		SceneObject* object = params->mFactory->Create(type);
		SetObject(object);

		// read in the object
		//if (!object->Load(params, this))
		//	return false;
	}

	// read the number of child nodes
	int numNodes = 0;
	in->Read(&numNodes, sizeof(int));

	for (int i = 0; i < numNodes; ++i)
	{
		SceneNode* childNode = scene->CreateNode();
		childNode->SetParent(this);
		InsertChild(childNode);
		childNode->Load(params, scene);
	}

	return true;
}

bool SceneNode::LoadObject(ObjectLoadParams* params, Scene* scene)
{
	if (mObject)
	{
		// read in the object
		if (!mObject->Load(params, this))
			return false;
	}

	// save children
	for (SceneNode::Iterator it = Begin(); it != End(); ++it)
	{
		if (!(*it)->LoadObject(params, scene))
			return false;
	}

	return true;
}

bool SceneNode::Save(File& out)
{
	// write the node name
	const string& name = GetName();
	int len = name.length();
	out.Write(&len, sizeof(int));
	out.Write(name.c_str(), sizeof(char) * len);

	// write the flags
	out.Write(&mFlags, sizeof(unsigned int));

	// write the transform
	out.Write(&mTransform, sizeof(Matrix4));

	// write number of properties
	unsigned int numProperty = GetNumProperty();
	out.Write(&numProperty, sizeof(unsigned int));

	// write the properties
	SceneNode::PropertyIterator propIt;
	for (propIt = PropertyBegin(); propIt != PropertyEnd(); ++propIt)
	{
		const Property& property = *propIt;

		int len = property.mKey.length();
		out.Write(&len, sizeof(int));
		out.Write(property.mKey.c_str(), sizeof(char) * len);

		len = property.mValue.length();
		out.Write(&len, sizeof(int));
		out.Write(property.mValue.c_str(), sizeof(char) * len);
	}

	// save the type
	int type = GetObject() ? GetObject()->GetType() : -1;
	out.Write(&type, sizeof(int));

	// save out the object
	//if (GetObject() && !GetObject()->Save(out, this))
	//	return false;

	// write the number of nodes
	int numNodes = GetNumChildNode();
	out.Write(&numNodes, sizeof(int));

	// save children
	for (SceneNode::Iterator it = Begin(); it != End(); ++it)
	{
		if (!(*it)->Save(out))
			return false;
	}

	return true;
}

bool SceneNode::SaveObject(File& out)
{
	// save out the object
	if (GetObject() && !GetObject()->Save(out, this))
		return false;

	// save children
	for (SceneNode::Iterator it = Begin(); it != End(); ++it)
	{
		if (!(*it)->SaveObject(out))
			return false;
	}

	return true;
}

void SceneNode::RemoveChild(SceneNode* child)
{ 
	for (SceneNode::Iterator it = Begin(); it != End(); ++it)
	{
		if (*it == child)
		{
			child->SetParent(0);
			mChild.erase(it);
			break;
		}
	}
}

void SceneNode::SetLocalTransform(const Matrix4& matrix)		
{ 
	mTransform = matrix; 
}

Matrix4 SceneNode::GetLocalTransform() const					
{
	return mTransform;
}

Matrix4 SceneNode::GetWorldTransform() const					
{ 
	Matrix4 transform = GetLocalTransform();

	if (mParent)
	{
		Matrix4 parentTransform = mParent->GetWorldTransform();
		transform = transform * parentTransform;
	}
	
	return transform; 
}

void SceneNode::SetWorldTransform(const Matrix4& matrix)
{
	if (GetParent())
	{
		Matrix4 parentWorldInv = GetParent()->GetWorldTransform();
		parentWorldInv.Inverse();
		SetLocalTransform(matrix * parentWorldInv);
		return;
	}

	SetLocalTransform(matrix);
}

void SceneNode::SetFlags(unsigned int flags, bool applyToChildren, Scene* owner)				
{ 
	// if (affectOwnedOnly) only change flags on nodes this scene owns, incase children have been attached 
	if (!owner || (owner && GetOwner() == owner))
		mFlags |= flags;	

	if (applyToChildren)
	{
		for (SceneNode::Iterator it = Begin(); it != End(); ++it)
			(*it)->SetFlags(flags, applyToChildren, owner);
	}
}

void SceneNode::ClearFlags(unsigned int flags, bool applyToChildren, Scene* owner)				
{ 
	// if (affectOwnedOnly) only change flags on nodes this scene owns, incase children have been attached 
	if (!owner || (owner && GetOwner() == owner))
		mFlags &= ~flags;	

	if (applyToChildren)
	{
		for (SceneNode::Iterator it = Begin(); it != End(); ++it)
			(*it)->ClearFlags(flags, applyToChildren, owner);
	}
}
