#include "Scene.h"
#include "RenderFactory.h"
#include "Device.h"
#include "AnimationController.h"
#include "Skin.h"
#include "File/Log.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

#define MAJOR_VERSION 1
#define MINOR_VERSION 1

void Scene::Destroy(const RenderFactory* factory)
{
	Destroy(factory, GetRootNode());
}

void Scene::Destroy(const RenderFactory* factory, SceneNode* node)
{
	// iterate through children
	Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
	{
		Destroy(factory, *it);
	}

	// clean up
	SceneObject* obj = node->GetObject();
	const_cast<RenderFactory*>(factory)->Release(&obj);
	DestroyNode(node);
}

bool Scene::Clone(ObjectLoadParams* params, Scene* copy)
{
	SetName(copy->GetName());
	return mRoot->Clone(params, copy->GetRootNode(), copy);
}

bool Scene::Load(ObjectLoadParams* params)
{
	SetName(params->mName);

	string path;
	params->mFactory->GetParameter().GetExistingFile(params->mName, "sce", path);

	File in(path.c_str(), FAT_Read | FAT_Binary);
	if (!in.Valid())
		return false;

	// read in version
	unsigned int majorVersion = -1;
	unsigned int minorVersion = -1;
	in.Read(&majorVersion, sizeof(unsigned int));
	in.Read(&minorVersion, sizeof(unsigned int));

	if (majorVersion != MAJOR_VERSION || minorVersion != MINOR_VERSION)
	{
		Log::Error("Tried to load scene version: '%i.%i', expecting latest version: '%i.%i'", majorVersion, minorVersion, MAJOR_VERSION, MINOR_VERSION);
		return false;
	}

	params->mFile = &in;
	if (!mRoot->Load(params, this))
		return false;

	return mRoot->LoadObject(params, this);
}

bool Scene::Save(const string& file)
{
	File out(file.c_str(), FAT_Write | FAT_Binary);

	// write out a version
	unsigned int majorVersion = MAJOR_VERSION;
	unsigned int minorVersion = MINOR_VERSION;
	out.Write(&majorVersion, sizeof(unsigned int));
	out.Write(&minorVersion, sizeof(unsigned int));

	if (!GetRootNode()->Save(out))
		return false;

	return mRoot->SaveObject(out);
}

SceneNode* Scene::GetNodeByName(const string& name)
{
	return GetRootNode()->GetNodeByName(name);
}

void Scene::SetFlags(unsigned int flags, SceneNode* node, bool affectOwnedOnly)
{
	if (node == 0)
		node = GetRootNode();

	node->SetFlags(flags, true, this);
}

void Scene::ClearFlags(unsigned int flags, SceneNode* node, bool affectOwnedOnly)
{
	if (node == 0)
		node = GetRootNode();

	node->ClearFlags(flags, true, this);
}

void Scene::DebugBoundBox(Device* device, SceneNode* node)
{
	if (node == 0)
		node = GetRootNode();

	Box bound;
	if (node->GetObject() && node->GetObject()->GetLocalBoundBox(bound))
	{
		bound.SetTransform(node->GetWorldTransform());
		device->DrawBox(bound);
	}

	for (SceneNode::Iterator it = node->Begin(); it != node->End(); ++it)
		DebugBoundBox(device, (*it));
}

void Scene::DebugNode(Device* device, SceneNode* node, float scale)
{
	if (node == 0)
		node = GetRootNode();

	Matrix4 worldTransform = node->GetWorldTransform();
	device->DrawMatrix(worldTransform, scale);

	for (SceneNode::Iterator it = node->Begin(); it != node->End(); ++it)
		DebugNode(device, (*it), scale);
}

void Scene::SetAnimation(Animation* anim, SceneNode* node)
{
	if (node == 0)
		node = GetRootNode();

	SceneAnimationNode* sceneAnimNode = node->GetSceneAnimationNode();
	if (sceneAnimNode)
	{
		sceneAnimNode->ClearBlend();
		sceneAnimNode->InsertBlend(sceneAnimNode->GetAnimationNode(anim));
	}
/*
	AnimationController* controller = node->GetAnimationController();
	if (controller)
		controller->SetAnimation(controller->GetAnimation(name));
*/
	for (SceneNode::Iterator it = node->Begin(); it != node->End(); ++it)
		SetAnimation(anim, (*it));
}
/*
void Scene::SetAnimationTime(float time, SceneNode* node)
{
	if (node == 0)
		node = GetRootNode();

	AnimationController* controller = node->GetAnimationController();
	if (controller)
		controller->SetTime(time);

	for (SceneNode::Iterator it = node->Begin(); it != node->End(); ++it)
		SetAnimationTime(time, (*it));
}*/

void Scene::InsertAnimation(Animation* anim, SceneNode* node, bool firstCall)
{
	if (firstCall)
	{
		mAnimation.push_back(anim);
		if (node == 0)
			node = GetRootNode();
	}

	// dont apply animation if there is none!
	AnimationNode* animNode = anim->GetAnimationNodeByName(node->GetName().c_str());
	SceneAnimationNode* sceneAnimNode = node->GetSceneAnimationNode();
	if (!sceneAnimNode && animNode)
	{
		sceneAnimNode = new SceneAnimationNode();
		node->SetSceneAnimationNode(sceneAnimNode);
	}

	if (sceneAnimNode)
		sceneAnimNode->InsertAnimationNode(animNode);

	for (SceneNode::Iterator it = node->Begin(); it != node->End(); ++it)
		InsertAnimation(anim, (*it), false);
}

Animation* Scene::GetAnimation(const char* name)
{
	vector<Animation*>::iterator it;
	for (it = mAnimation.begin(); it != mAnimation.end(); ++it)
	{
		if (strcmpi((*it)->GetName(), name) == 0)
			return (*it);
	}

	return 0;
}

void Scene::ApplyAnimation(SceneNode* node, bool bFirstCall)
{
	if (node == 0)
		node = GetRootNode();

	SceneAnimationNode* sceneAnimNode = node->GetSceneAnimationNode();
	if (sceneAnimNode)
		node->SetLocalTransform(sceneAnimNode->GetTransform());

	for (SceneNode::Iterator it = node->Begin(); it != node->End(); ++it)
		ApplyAnimation((*it), false);

	if (bFirstCall)
		ApplySkinPalette(node);
}

void Scene::ApplySkinPalette(SceneNode* node)
{
	if (node == 0)
		node = GetRootNode();

	SceneObject* object = node->GetObject();
	if (object)
	{
		switch (object->GetType())
		{
		case SkinMesh::Type:
			static_cast<SkinMesh*>(object)->ApplySkinPalette();
			break;
		}
	}

	for (SceneNode::Iterator it = node->Begin(); it != node->End(); ++it)
		ApplySkinPalette((*it));
}

void Scene::ProgressAnimation(float deltaTime, Animation* animation, SceneNode* node)
{
	if (node == 0)
		node = GetRootNode();

	SceneAnimationNode* sceneAnimNode = node->GetSceneAnimationNode();
	if (sceneAnimNode)
		sceneAnimNode->ProgressTime(deltaTime, animation);

	for (SceneNode::Iterator it = node->Begin(); it != node->End(); ++it)
		ProgressAnimation(deltaTime, animation, (*it));
}

void Scene::SetAnimationTime(float time, Animation* animation, SceneNode* node)
{
	if (node == 0)
		node = GetRootNode();

	SceneAnimationNode* sceneAnimNode = node->GetSceneAnimationNode();
	if (sceneAnimNode)
		sceneAnimNode->SetTime(time, animation);

	for (SceneNode::Iterator it = node->Begin(); it != node->End(); ++it)
		SetAnimationTime(time, animation, (*it));
}
