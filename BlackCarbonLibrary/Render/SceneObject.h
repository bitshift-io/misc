#ifndef _SCENE_OBJECT_H_
#define _SCENE_OBJECT_H_

#include <vector>
#include "File/File.h"
#include "Math/Matrix.h"
#include "Math/Box.h"

using namespace std;

class Device;
class RenderFactory;
class ObjectLoadParams;
class Scene;

#undef GetObject

#define TYPE(t) enum { Type = t }; virtual int GetType() const { return Type; }

enum ObjectLoadFlag
{
	OLF_None				= 0,
	OLF_LoadForInstance		= 1 << 0, // load only the required data from the file mFile, do not advanced processing
};

//
// Each class can extend this
//
class ObjectLoadParams
{
public:
	
	ObjectLoadParams()
	{
		mFactory = 0;
		mDevice = 0;
		mName = "";
		mFile = 0;
		mFlags = 0;
	}

	ObjectLoadParams(Device* device, const string& name) // use this if passing to the render factory
	{
		mFactory = 0;
		mDevice = device;
		mName = name;
		mFile = 0;
		mFlags = 0;
	}

	ObjectLoadParams(RenderFactory* factory, Device* device, const string& name)
	{
		mFactory = factory;
		mDevice = device;
		mName = name;
		mFile = 0;
		mFlags = 0;
	}

	File*					mFile;
	RenderFactory*			mFactory;
	Device*					mDevice;
	string					mName;
	unsigned int			mFlags;
};

class SceneNode;

//
// anything the engine can create that needs to be saved to disk in the SCE file
//
class SceneObject
{
	friend class RenderFactory;

public:

	SceneObject() : mReferenceCount(0)															{}

	virtual	int				GetType() const														= 0;

	virtual bool			Save(const File& out, SceneNode* node = 0)							= 0;
	virtual bool			Load(const ObjectLoadParams* params, SceneNode* node = 0)			= 0;

	virtual bool			IsInstance(const ObjectLoadParams* params, SceneObject* loaded)		{ return false; }
	virtual SceneObject*	Clone(const ObjectLoadParams* params, SceneObject* loaded)			{ this->AddReference(); return this; }
	virtual SceneObject*	Clone(SceneNode* owner = 0)											{ this->AddReference(); return this; }

	virtual void			ReloadResource()													{}

	virtual bool			GetLocalBoundBox(Box& box)											{ return false; }

	virtual void			AddReference()														{ ++mReferenceCount; }
	virtual bool			ReleaseReference()													{ --mReferenceCount; return mReferenceCount <= 0; }

protected:

	virtual void			Create(RenderFactory* factory)										{}
	virtual void			Destroy(RenderFactory* factory)										{}

	int						mReferenceCount;
};

#endif
