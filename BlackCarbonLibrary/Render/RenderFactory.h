#ifndef _RENDERFACTORY_H_
#define _RENDERFACTORY_H_

#include <vector>
#include <string>
#include <map>
#include <list>
#include "Scene.h"

using namespace std;

#undef CreateWindow

class Window;
class Scene;
class Device;
class SceneObject;
class Occluder;
class Renderer;

//
// sometime we need to make app changes based on what render factory we are using
// these are sorted worst to best
//
enum RenderFactoryType
{
	RFT_Unknown,
	RFT_Export,
	RFT_Software,
	RFT_OpenGL,
	RFT_DirectX10,
	RFT_RayTrace,
};


//
// this is like a registry, and other options that need to be stored
//
class RenderFactoryParameter
{
public:

	RenderFactoryParameter() :
		mCacheDir("Cache"),
		mUseCache(true),
#ifdef DEBUG
		mDebugMode(true)
#else
		mDebugMode(false)
#endif
	{
	}

	bool	GetExistingFile(const string& filename, const string& extension, string& outPath) const;

	string	mCacheDir;
	bool	mUseCache;
	bool	mDebugMode;
};

class RenderFactory
{
public:

	RenderFactory(const RenderFactoryParameter& parameter);

	virtual void			Init()				= 0;
	virtual void			Deinit()			= 0;

	virtual Window*			CreateWindow()		= 0;
	virtual void			ReleaseWindow(Window** window) = 0;

	virtual Device*			CreateDevice()		= 0;
	virtual void			ReleaseDevice(Device** device) = 0;

	virtual Scene*			LoadScene(const ObjectLoadParams* params);
	virtual Scene*			CreateScene();
	virtual void			ReleaseScene(Scene** scene);

	virtual Renderer*		CreateRenderer();
	virtual void			ReleaseRenderer(Renderer** renderer);

	virtual Occluder*		CreateOccluder();
	virtual void			ReleaseOccluder(Occluder** renderer);

	// the prefered method to create, load and release SceneObjects
	template <class T>		
	T*						Load(const ObjectLoadParams* params);

	template <class T>		
	T*						Create(bool addToList = true);

	template <class T>		
	T*						Clone(T* obj, SceneNode* owner = 0);

	// returns 0 if truley free'd, else it just returns the pointer again
	template <class T>		
	T*						Release(T** obj);

	// anything that can go in the scene file format should use the Create method
	virtual SceneObject*	Load(int type, const ObjectLoadParams* params);
	virtual SceneObject*	Create(int type, bool addToList = true);
	virtual SceneObject*	Release(SceneObject** obj);
	virtual SceneObject*	Clone(SceneObject* obj, SceneNode* owner = 0);

	virtual void			Register(FnObjectCreate createFn, int type);

	const RenderFactoryParameter&	GetParameter() const						{ return mParameter; }

	// use this when changing screen res or the like
	virtual void			ReloadResource(Device* device);

	virtual RenderFactoryType	GetRenderFactoryType()	= 0;

protected:

	struct CreatorInfo
	{
		FnObjectCreate	createFn;
		int				type;
	};

	vector<CreatorInfo>		mCreator;
	RenderFactoryParameter	mParameter;
	list<Scene*>			mSceneList;

	map<int, list<SceneObject*> >		mSceneObjectList;
};

template <class T>		
T* RenderFactory::Release(T** obj)
{
	SceneObject* sceneObj = static_cast<SceneObject*>(*obj); // this checks the cast is legal
	return static_cast<T*>(Release((SceneObject**)obj));
}


template <class T>		
T* RenderFactory::Create(bool addToList)
{
	return static_cast<T*>(Create(T::Type, addToList));
}

template <class T>		
T* RenderFactory::Clone(T* obj, SceneNode* owner)
{
	return static_cast<T*>(Clone(static_cast<SceneObject*>(obj), owner));
}

template <class T>		
T* RenderFactory::Load(const ObjectLoadParams* params)
{
	return static_cast<T*>(Load(T::Type, params));
}


#endif
