#include "Renderer.h"
#include "Material.h"
#include "Geometry.h"
#include "Mesh.h"
#include "Skin.h"
#include "Light.h"
#include "Device.h"
#include "Scene.h"
#include "Camera.h"
#include "Window.h"
#include "RenderTexture.h"
#include "Occluder.h"
#include "File/Log.h"

Renderer::Renderer() : mCamera(0), mRenderTexture(0), mProjector(0), mTime(0), mOccluder(0), mMaterialTechniqueID(0)
{
}

bool Renderer::AddChildRenderer(Renderer* child)
{
	mChild.push_back(child);
	return true;
}

bool Renderer::RemoveChildRenderer(Renderer* child)
{
	mChild.remove(child);
	return true;
}

bool Renderer::InsertScene(Scene* scene)
{
	return InsertSceneNode(scene->GetRootNode());
}

bool Renderer::RemoveScene(Scene* scene)
{
	return RemoveSceneNode(scene->GetRootNode());
}

bool Renderer::InsertSceneNode(SceneNode* node)
{
	// iterate through the hierachey, looking for materials, meshs, lights etc
	if (!node)
		return true;

	SceneObject* object = node->GetObject();
	if (object)
	{
		switch (object->GetType())
		{
		case SkinMesh::Type:
		case Mesh::Type:
			InsertMesh(node, static_cast<Mesh*>(object));
			break;

		case Light::Type:
			InsertLight(static_cast<Light*>(object));
			break;
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
		InsertSceneNode(*it);

	return true;
}

bool Renderer::RemoveSceneNode(SceneNode* node)
{
	if (!node)
		return true;

	SceneObject* object = node->GetObject();
	if (object)
	{
		switch (object->GetType())
		{
		case Mesh::Type:
			RemoveMesh(node, static_cast<Mesh*>(object));
			break;

		case Light::Type:
			RemoveLight(static_cast<Light*>(object));
		}
	}

	SceneNode::Iterator it;
	for (it = node->Begin(); it != node->End(); ++it)
		RemoveSceneNode(*it);

	return true;
}

bool Renderer::RemoveMesh(SceneNode* node, Mesh* mesh)
{
	// iterate through all mesh mats
	Mesh::MeshMatIterator meshMatIt;
	for (meshMatIt = mesh->Begin(); meshMatIt != mesh->End(); ++meshMatIt)
	{
		// iterate through all material base
		MaterialBaseNodeIterator matBaseNodeIt;
		for (matBaseNodeIt = mMaterialBaseNode.begin(); matBaseNodeIt != mMaterialBaseNode.end(); )
		{
			MaterialBaseNode& matBaseNode = *matBaseNodeIt;

			// iterate though all materials
			GeometryNodeIterator geometryNodeIt;
			for (geometryNodeIt = matBaseNode.geometryNode.begin(); geometryNodeIt != matBaseNode.geometryNode.end(); )
			{
				// find the mesh
				if (geometryNodeIt->geometry == meshMatIt->geometry)
				{
					// remove this instance (ie find the node)
					list<InstanceNode>::iterator instIt;
					for (instIt = geometryNodeIt->instanceNode.begin(); instIt != geometryNodeIt->instanceNode.end(); )
					{
						if (instIt->node == node)
						{
							instIt = geometryNodeIt->instanceNode.erase(instIt);

							// if this was the last instance, remove the mesh
							if (geometryNodeIt->instanceNode.size() <= 0)
							{
								geometryNodeIt = matBaseNode.geometryNode.erase(geometryNodeIt);
								break;
							}
						}
						else
						{
							++instIt;
						}
					}

					break;
				}
				else
				{
					++geometryNodeIt;
				}
			}

			// see if this was the last material in the material list
			if (matBaseNodeIt->geometryNode.size() <= 0)
				matBaseNodeIt = mMaterialBaseNode.erase(matBaseNodeIt);
			else
				++matBaseNodeIt;
		}
	}

	return true;
}

bool Renderer::InsertMesh(SceneNode* node, Mesh* mesh)
{
	// check to make sure its actually renderable
	if (mesh->GetFlags() & MF_NonRenderable)
		return false;

	// iterate over all mesh mats
	int idx = 0;
	Mesh::MeshMatIterator meshMatIt;
	for (meshMatIt = mesh->Begin(); meshMatIt != mesh->End(); ++meshMatIt, ++idx)
	{
		// get the material node from this material
		MaterialBaseNode* matBaseNode = InsertMaterialBase(meshMatIt->material->GetMaterialBase());
		Geometry* geometry = meshMatIt->geometry;

		list<GeometryNode>::iterator geometryNodeIt;
		for (geometryNodeIt = matBaseNode->geometryNode.begin(); geometryNodeIt != matBaseNode->geometryNode.end(); ++geometryNodeIt)
		{
			if (geometryNodeIt->geometry == geometry)
			{
				geometryNodeIt->instanceNode.push_back(InstanceNode(node, meshMatIt->material, mesh));
				break;
			}
		}

		// not found, so make a new mesh node
		if (geometryNodeIt == matBaseNode->geometryNode.end())
			matBaseNode->geometryNode.push_back(GeometryNode(geometry, InstanceNode(node, meshMatIt->material, mesh)));
	}

	return true;
}

bool Renderer::RemoveLight(Light* light)
{
	mLight.remove(light);
	return true;
}

bool Renderer::InsertLight(Light* light)
{
	LightIterator it;
	for (it = mLight.begin(); it != mLight.end(); ++it)
	{
		if ((*it) == light)
			return false;
	}

	mLight.push_back(light);
	return true;
}

bool Renderer::InsertProjector(Camera* projector)
{
	mProjector.push_back(projector);
	return true;
}

bool Renderer::RemoveProjector(Camera* projector)
{
	vector<Camera*>::iterator it;
	for (it = mProjector.begin(); it != mProjector.end(); ++it)
	{
		if (*it == projector)
		{
			mProjector.erase(it);
			return true;
		}
	}

	return false;
}

MaterialBaseNode* Renderer::InsertMaterialBase(MaterialBase* materialBase)
{
	// iterate through all materials
	MaterialBaseNodeIterator matBaseNodeIt;
	for (matBaseNodeIt = mMaterialBaseNode.begin(); matBaseNodeIt != mMaterialBaseNode.end(); ++matBaseNodeIt)
	{
		// if this material is already in the list...
		if (materialBase == matBaseNodeIt->materialBase)
		{
			return &(*matBaseNodeIt);
		}

		// if the material is less than the current material
		else if ((*materialBase->GetTechniqueByID(mMaterialTechniqueID)) < (*matBaseNodeIt->materialBase->GetTechniqueByID(mMaterialTechniqueID)))
		{
			// insert the material at this location
			matBaseNodeIt = mMaterialBaseNode.insert(matBaseNodeIt, MaterialBaseNode(materialBase));
			return &(*matBaseNodeIt);
		}
	}

	// not in the list and greater than all other materials,
	// so insert a new mat
	mMaterialBaseNode.push_back(MaterialBaseNode(materialBase));
	return &mMaterialBaseNode.back();
}

bool Renderer::Render(Device* device)
{
	if (!mCamera)
		return false;

	bool ret = true;
	int faceCount = 1;
	if (mRenderTexture)
		faceCount = mRenderTexture->Begin();

	mCamera->SetAspectRatio(device->GetWindow()->GetAspectRatio());

	bool colorToggle = true;

	for (int f = 0; f < faceCount; ++f)
	{
		if (mRenderTexture)
			mRenderTexture->BindTarget(f, true);

		// bind each material
		MaterialBaseNodeIterator matBaseNodeIt;
		for (matBaseNodeIt = mMaterialBaseNode.begin(); matBaseNodeIt != mMaterialBaseNode.end(); ++matBaseNodeIt)
		{
			bool materialBound = false;
			//Material* material = matNodeIt->material;
			MaterialBase* materialBase = matBaseNodeIt->materialBase;

			BEGINDEBUGEVENT(device, materialBase->GetName(), colorToggle ? Vector4(1.f, 0.f, 0.f, 1.f) : Vector4(0.f, 1.f, 0.f, 1.f));
			colorToggle = !colorToggle;

			// set the material and do appropriate passes
			int passes = materialBase->Begin();
			for (int i = 0; i < passes; ++i)
			{
				//material->BeginPass(device, i); -- done in RenderMeshNodePass

				// render each mesh
				GeometryNodeIterator geometryNodeIt;
				for (geometryNodeIt = matBaseNodeIt->geometryNode.begin(); geometryNodeIt != matBaseNodeIt->geometryNode.end(); ++geometryNodeIt)
				{
					materialBound |= RenderGeometryNodePass(device, materialBase, *geometryNodeIt, i, materialBound);
				}

				if (materialBound)
					materialBase->EndPass();

				// render materials that override pass settings
				for (geometryNodeIt = matBaseNodeIt->geometryNode.begin(); geometryNodeIt != matBaseNodeIt->geometryNode.end(); ++geometryNodeIt)
				{
					RenderPassOverride(device, materialBase, *geometryNodeIt, i);
				}
			}

			materialBase->End();

			// render materials that override effect settings
			GeometryNodeIterator geometryNodeIt;
			for (geometryNodeIt = matBaseNodeIt->geometryNode.begin(); geometryNodeIt != matBaseNodeIt->geometryNode.end(); ++geometryNodeIt)
			{
				RenderEffectOverride(device, materialBase, *geometryNodeIt);
			}

			ENDDEBUGEVENT(device);
		}

		/*
		// debug drawing
		for (matNodeIt = mMaterialNode.begin(); matNodeIt != mMaterialNode.end(); ++matNodeIt)
		{
			// each mesh
			MeshNodeIterator meshNodeIt;
			for (meshNodeIt = matNodeIt->meshNode.begin(); meshNodeIt != matNodeIt->meshNode.end(); ++meshNodeIt)
			{
				// upload transforms
				const Matrix4& transform = meshNodeIt->node->GetWorldTransform();
				//matNodeIt->material->GetEffect()->SetMatrixParameters(mCamera->GetProjection(), mCamera->GetView(), mCamera->GetWorld(), transform);

				// render
				meshNodeIt->mesh->Debug(device, transform); //Render(device, meshNodeIt->meshMatIdx, matNodeIt->material->GetVertexFormat(i));
			}
		}*/

		RendererIterator it;
		for (it = mChild.begin(); it != mChild.end(); ++it)
		{
			if (!(*it)->Render(device), false)
				ret = false;
		}
	}

	if (mRenderTexture)
		mRenderTexture->End();

	return ret;
}

bool Renderer::RenderPassOverride(Device* device, MaterialBase* materialBase, GeometryNode& geometryNode, int pass)
{
	Geometry* geometry = geometryNode.geometry;
	Effect* effect = materialBase->GetEffect();

	// render using non instance rendering
	InstanceNodeIterator instNodeIt;
	for (instNodeIt = geometryNode.instanceNode.begin(); instNodeIt != geometryNode.instanceNode.end(); ++instNodeIt)
	{
		if (!IsVisible(&*instNodeIt))
			continue;

		Material* material = instNodeIt->material;

		// ignore materials that dont override pass settings
		//if (!material->OverridesPass() || material->OverridesEffect())
		//	continue;
		if (!material->GetFlags() & MF_OverridePass || material->GetFlags() & (MF_OverrideEffect | MF_Hidden)) //OverridesEffect() || instNodeIt->material->OverridesPass())
			continue;

		material->BeginPass(pass);

		// upload transforms
		if (effect->GetParameterByID(EPID_InstanceData, false))
		{
			SetEffectInstanceData(device, *instNodeIt, materialBase);
		}
		else
		{
			const Matrix4& transform = instNodeIt->node->GetWorldTransform();
			effect->SetMatrixParameters(mCamera->GetProjection(), mCamera->GetView(), mCamera->GetWorld(), transform);

			if (mProjector.size())
				effect->SetProjectorParameters(&mProjector[0], 1, transform);

			HandleCustomParameter(effect, geometryNode, *instNodeIt, transform);
		}

		// set time
		EffectParameter* timeParam = effect->GetParameterByID(EPID_Time, false);
		if (timeParam)
			timeParam->SetValue(&mTime);

		material->SetPassParameter();
		geometry->Draw();
		material->EndPass();
	}

	return true;
}

bool Renderer::RenderEffectOverride(Device* device, MaterialBase* materialBase, GeometryNode& geometryNode)
{
	// TODO - do i want to allow this even?
	/*
	Geometry* geometry = geometryNode.geometry;

	// render using non instance rendering
	InstanceNodeIterator instNodeIt;
	for (instNodeIt = geometryNode.instanceNode.begin(); instNodeIt != geometryNode.instanceNode.end(); ++instNodeIt)
	{
		if (!IsVisible(&*instNodeIt))
			continue;

		Material* material = instNodeIt->material;
		Effect* effect = material->GetCurrentTechnique()->GetEffect();

		// ignore materials that dont override effect settings
		if (!material->OverridesEffect())
			continue;

		if (!effect->GetParameterByID(EPID_InstanceData, false))
		{
			// set the material and do appropriate passes
			int passes = material->Begin();
			for (int i = 0; i < passes; ++i)
			{
				instNodeIt->material->BeginPass(device, i);

				// upload transforms
				const Matrix4& transform = instNodeIt->node->GetWorldTransform();
				effect->SetMatrixParameters(mCamera->GetProjection(), mCamera->GetView(), mCamera->GetWorld(), transform);

				if (mProjector.size())
					effect->SetProjectorParameters(&mProjector[0], 1, transform);

				// set time
				EffectParameter* timeParam = effect->GetParameterByID(EPID_Time, false);
				if (timeParam)
					timeParam->SetValue(mTime);

				HandleCustomParameter(effect, geometryNode, *instNodeIt, transform);

				material->SetPassParameter();
				geometry->Draw();
				materialBase->EndPass();
			}

			material->End();
		}
		else
		{
			// TODO
		}
	}
*/
	return true;
}

bool Renderer::RenderGeometryNodePass(Device* device, MaterialBase* materialBase, GeometryNode& geometryNode, int pass, bool materialBound)
{
	Geometry* geometry = geometryNode.geometry;
	Effect* effect = materialBase->GetEffect();
	if (!effect->GetParameterByID(EPID_InstanceData, false))
	{
		// render using non instance rendering
		InstanceNodeIterator instNodeIt;
		for (instNodeIt = geometryNode.instanceNode.begin(); instNodeIt != geometryNode.instanceNode.end(); ++instNodeIt)
		{
			if (!IsVisible(&*instNodeIt))
				continue;

			// ignore materials that override any values
			if (instNodeIt->material->GetFlags() & (MF_OverrideEffect | MF_OverridePass | MF_Hidden)) //OverridesEffect() || instNodeIt->material->OverridesPass())
				continue;

			// bind material if not already bound
			if (!materialBound)
			{
				materialBase->BeginPass(pass);
				materialBound = true;
			}

			// upload transforms
			const Matrix4& transform = instNodeIt->node->GetWorldTransform();
			effect->SetMatrixParameters(mCamera->GetProjection(), mCamera->GetView(), mCamera->GetWorld(), transform);

			if (mProjector.size())
				effect->SetProjectorParameters(&mProjector[0], 1, transform);

			// set time
			EffectParameter* timeParam = effect->GetParameterByID(EPID_Time, false);
			if (timeParam)
				timeParam->SetValue(&mTime);

			HandleCustomParameter(effect, geometryNode, *instNodeIt, transform);

			if (instNodeIt->mesh->GetType() == SkinMesh::Type)
			{
				SkinMesh* mesh = static_cast<SkinMesh*>(instNodeIt->mesh);
				EffectParameter* skinPalette = effect->GetParameterByID(EPID_SkinPalette, false);
				if (skinPalette)
					skinPalette->SetValue(mesh->GetSkinPalette(), mesh->GetBoneCount());
			}

			materialBase->SetPassParameter();
			geometry->Draw();
		}
	}
	else
	{
		// render using instance rendering
		// if the shader has say a limit of 10 instances per render call
		// and there are say 15 instances, we need 2 draw calls
		InstanceNodeIterator instNodeIt = geometryNode.instanceNode.begin();
		do
		{
			int numInstanceToDraw = SetEffectInstanceData(device, geometryNode.instanceNode, materialBase, instNodeIt);
			if (numInstanceToDraw > 0)
			{
				// bind material if not already bound
				if (!materialBound)
				{
					materialBase->BeginPass(pass);
					materialBound = true;
				}

				// set time
				EffectParameter* timeParam = effect->GetParameterByID(EPID_Time, false);
				if (timeParam)
					timeParam->SetValue(&mTime);

				materialBase->SetPassParameter();
				//device->Draw(numInstanceToDraw, material, meshNode.mesh->GetMeshMat(meshNode.meshMatIdx).geometry, material->GetVertexFormat(pass));
				geometry->Draw(0, -1, numInstanceToDraw);
			}
		} while (instNodeIt != geometryNode.instanceNode.end());
	}

	return materialBound;
}

int Renderer::GetClosestLights(const Vector4& position, Light* lightList[], int maxLights)
{
	int idx = 0;
	LightIterator it;
	for (it = mLight.begin(); it != mLight.end(); ++it)
	{
		if ((*it)->InRange(position)) // this should be a sphere eventually
		{
			lightList[idx] = *it;
			++idx;

			if (idx >= maxLights)
				break;
		}
	}

	// look for closer lights
	for (; it != mLight.end(); ++it)
	{

	}

	/*
	LightNode* lightIt = lightNodes;

	// add the first maxLights lights
	while(lightIt && idx < maxLights)
	{
		// check to make sure this light is in range
		float dist = bounds.Intersect(*(lightIt->light));
		if (dist != -1) //lightIt in range of position)
		{
			lightList[idx] = lightIt->light;
			++idx;
		}

		lightIt = lightIt->pNext;
	}

	while (lightIt)
	{
		//if (lightIt in range of position)
		{
			int i = 0;
			for (; i < idx; ++i)
			{
				float dist = bounds.Intersect(*(lightIt->light));
				if (dist != -1)
				{

				}
				// if (lightIt closer to position than this light)
				//	break;
			}
			if (i < idx)
			{
				lightList[i] = lightIt->light;
			}
			else if (i < maxLights)
			{
				idx = i;
				lightList[i] = lightIt->light;
			}
		}

		lightIt = lightIt->pNext;
	}
*/
	return idx; // return how many we added
}

bool Renderer::IsVisible(InstanceNode* instNode)
{
	if (instNode->node->GetFlags() & SNF_Visible)
		return true;

	if (instNode->node->GetFlags() & SNF_Hidden)
		return false;

	if (mOccluder && mOccluder->GetVisibility(instNode->node) != V_Visible)
		return false;

	return true;
}

bool Renderer::SetEffectInstanceData(Device* device, InstanceNode& instanceNode, MaterialBase* materialBase)
{
	Effect* effect = materialBase->GetEffect();
	EffectParameter* effectParam = effect->GetParameterByID(EPID_InstanceData, false);

	Matrix4 world = instanceNode.node->GetWorldTransform();

	const Matrix4& projection = mCamera->GetProjection();
	const Matrix4& view = mCamera->GetView();
	const Matrix4& camWorld = mCamera->GetWorld();

	unsigned int structSize = effectParam->mSize / effectParam->mArraySize;
	char* buffer = new char[structSize];

	for (unsigned int p = 0; p < effectParam->mChild.size(); ++p)
	{
		EffectParameter* effectInstMemberParam = effectParam->mChild[p];
		//EffectParameterValue* effectInstMemberData = effectInstParam->mData;

		switch (effectInstMemberParam->mID)
		{
		case EPID_Projector:
			{
				SetInstanceProjectorParameters(effectInstMemberParam, projection, view, camWorld, world, &buffer[effectInstMemberParam->mStructOffset]);
			}
			break;

		case EPID_ProjectorCount:
			{
				unsigned int projectorCount = 1;
				memcpy(&buffer[effectInstMemberParam->mStructOffset], &projectorCount, sizeof(unsigned int));
			}
			break;

		default:
			{
				// is it a matrix?
				if (effectInstMemberParam->mType == EPT_Matrix4)
				{
					Matrix4 result = effect->CalculateMatrix(effectInstMemberParam->mID, projection, view, camWorld, world);
					result.Transpose();
					memcpy(&buffer[effectInstMemberParam->mStructOffset], &result, sizeof(float) * 16);
				}
			}
			break;
		}

		HandleCustomInstanceParameter(effect, effectInstMemberParam, instanceNode, world, buffer, 0);
	}

	effectParam->SetValue((void*)buffer, structSize);
	//effect->SetValue(effectParam, buffer, structSize);
	delete[] buffer;

	return true;
}

int	Renderer::SetEffectInstanceData(Device* device, list<InstanceNode>& instanceNode, MaterialBase* materialBase, InstanceNodeIterator& instNodeIt)
{
	Effect* effect = materialBase->GetEffect();
	EffectParameter* effectParam = effect->GetParameterByID(EPID_InstanceData, false);

	unsigned int instIdx = 0;
	vector<InstanceNode*> visibleInstance;
	Matrix4 world[512];
	if (effectParam->mArraySize > 512)
	{
		Log::Error("[Renderer::SetEffectInstanceData] Buffer overrun! increase size of wolrd matrices");
		return 0;
	}

	const Matrix4& projection = mCamera->GetProjection();
	const Matrix4& view = mCamera->GetView();
	const Matrix4& camWorld = mCamera->GetWorld();

	// render each instance of this mesh
	for ( ; instNodeIt != instanceNode.end(); ++instNodeIt)
	{
		if (!IsVisible(&(*instNodeIt)))
			continue;

		// ignore materials that override any values
		if (instNodeIt->material->GetFlags() & (MF_OverrideEffect | MF_OverridePass | MF_Hidden)) //OverridesEffect() || instNodeIt->material->OverridesPass())
			continue;
		//if (instNodeIt->material->OverridesEffect() || instNodeIt->material->OverridesPass())
		//	continue;

		world[instIdx] = instNodeIt->node->GetWorldTransform();
		visibleInstance.push_back(&(*instNodeIt));
		++instIdx;

		// max instances collected
		if (instIdx >= effectParam->mArraySize)
			break;
	}

	if (visibleInstance.size() <= 0)
		return 0;

	//SetInstanceMatrixParameters(effectInstParam, projection, view, camWorld, world, instIdx);

	// set uip the data to upload
	unsigned int worldCount = instIdx;
	if (worldCount > effectParam->mArraySize)
	{
		Log::Error("[Renderer::SetInstanceMatrixParameters] trying to upload to many instances");
	}

	unsigned int structSize = effectParam->mSize / effectParam->mArraySize;
	char* buffer = new char[structSize * worldCount];

	for (unsigned int s = 0; s < worldCount; ++s)
	{
		for (unsigned int p = 0; p < effectParam->mChild.size(); ++p)
		{
			EffectParameter* effectInstMemberParam = effectParam->mChild[p];
			//EffectParameterValue* effectInstMemberData = effectInstParam->mData;

			switch (effectInstMemberParam->mID)
			{
			case EPID_Projector:
				{
					SetInstanceProjectorParameters(effectInstMemberParam, projection, view, camWorld, world[s], &buffer[structSize * s + effectInstMemberParam->mStructOffset]);
				}
				break;

			case EPID_ProjectorCount:
				{
					unsigned int projectorCount = 1;
					memcpy(&buffer[structSize * s + effectInstMemberParam->mStructOffset], &projectorCount, sizeof(unsigned int));
				}
				break;

			default:
				{
					// is it a matrix?
					if (effectInstMemberParam->mType == EPT_Matrix4)
					{
						Matrix4 result = effect->CalculateMatrix(effectInstMemberParam->mID, projection, view, camWorld, world[s]);
						result.Transpose();
						memcpy(&buffer[structSize * s + effectInstMemberParam->mStructOffset], &result, sizeof(float) * 16);
					}
				}
				break;
			}

			HandleCustomInstanceParameter(effect, effectInstMemberParam, *visibleInstance[s], world[s], buffer, s);
		}
	}

	effectParam->SetValue((void*)buffer, structSize * worldCount);
	//effect->SetValue(effectParam, buffer, structSize * worldCount);
	delete[] buffer;

	return visibleInstance.size();
}

void Renderer::SetInstanceProjectorParameters(EffectParameter* effectParam, const Matrix4& projection, const Matrix4& view, const Matrix4& camWorld, const Matrix4& world, char* buffer)
{
	if (mProjector.size() > effectParam->mArraySize)
	{
		Log::Error("[Renderer::SetInstanceMatrixParameters] trying to upload to many instances");
	}

	Effect* effect = effectParam->mEffect;
	int structSize = effectParam->mSize / effectParam->mArraySize;

	unsigned int projectorCount = Math::Min((unsigned int)mProjector.size(), effectParam->mArraySize);
	for (unsigned int s = 0; s < projectorCount; ++s)
	{
		Camera* projector = mProjector[s];

		Matrix4 projectorProjection = projector->GetProjection();
		Matrix4 projectorView = projector->GetView();
		Matrix4 projectorWorld = projector->GetWorld();

		for (unsigned int p = 0; p < effectParam->mChild.size(); ++p)
		{
			EffectParameter* effectInstMemberParam = effectParam->mChild[p];
			//EffectParameterValue* effectInstMemberData = effectInstParam->mData;

			switch (effectInstMemberParam->mID)
			{
			default:
				{
					// is it a matrix?
					if (effectInstMemberParam->mType == EPT_Matrix4)
					{
						Matrix4 result = effect->CalculateMatrix(effectInstMemberParam->mID, projectorProjection, projectorView, projectorWorld, world);
						result.Transpose();
						memcpy(&buffer[structSize * s + effectInstMemberParam->mStructOffset], &result, sizeof(float) * 16);
					}
				}
				break;
			}
		}
	}
}
