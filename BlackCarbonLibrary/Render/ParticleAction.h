#ifndef _PARTICLEACTION_H_
#define _PARTICLEACTION_H_

#include "SceneObject.h"
#include <list>

using namespace std;

class ParticleActionGroup;
class ParticleSystem;
class Particle;

class ParticleAction
{
public:

	virtual	int		GetType() const											= 0;

	virtual void	Reset()													{}

	virtual bool	Save(const File& out, SceneNode* node)					= 0;
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node)	= 0;

	virtual void	Update(ParticleSystem* system)							{}
	virtual bool	Update(ParticleSystem* system, Particle* particle)		{ return true; }

	virtual ParticleAction* Clone(ParticleActionGroup* ownerGroup, SceneNode* owner) = 0;

	ParticleActionGroup*	mActionGroup;
};

enum BirthType
{
	BT_Amount,
	BT_Rate,
};

class ParticleBirth : public ParticleAction
{
public:

	TYPE(0)

	ParticleBirth();

	virtual void	Reset();
	virtual bool	Save(const File& out, SceneNode* node);
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node);

	virtual void	Update(ParticleSystem* system);

	virtual ParticleAction* Clone(ParticleActionGroup* ownerGroup, SceneNode* owner);

	BirthType		mType;

	float			mStartTime;
	float			mEndTime;
	float			mEmitCount;

	union
	{
		float		mRate;
		float		mAmount;
	};
};


enum DeathType
{
	DT_All,
	DT_Life,
};

class ParticleDeath : public ParticleAction
{
public:

	TYPE(1)

	virtual bool	Save(const File& out, SceneNode* node);
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node);

	virtual bool	Update(ParticleSystem* system, Particle* particle);

	virtual ParticleAction* Clone(ParticleActionGroup* ownerGroup, SceneNode* owner);

	DeathType		mType;
	float			mLife;
	float			mVariation;
};

enum SpeedDirection
{
	SD_ZAxis,
	SD_XAxis,
	SD_YAxis,
	SD_Random,
	SD_Horizontal,
};

class ParticleSpeed : public ParticleAction
{
public:

	TYPE(2)

	virtual bool	Save(const File& out, SceneNode* node);
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node);

	virtual void	Update(ParticleSystem* system);

	virtual ParticleAction* Clone(ParticleActionGroup* ownerGroup, SceneNode* owner);

	float			mSpeed;
	float			mVariation;
	float			mDivergence;
	SpeedDirection	mDirection;
};


enum PositionType
{
	PT_Centre,
	PT_Vertex,
	PT_Edge,
	PT_Surface,
};

class ParticlePosition : public ParticleAction
{
public:

	TYPE(3)

	ParticlePosition();

	virtual void	Reset();
	virtual bool	Save(const File& out, SceneNode* node);
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node);

	virtual void	Update(ParticleSystem* system);

	virtual ParticleAction* Clone(ParticleActionGroup* ownerGroup, SceneNode* owner);

	PositionType	mPositionType;
	float			mInheritSpeedMovementPercent;
	Matrix4			mPreviousTransform;
	//Mesh*			mMesh;

};

enum ForceType
{
	FT_Directional,
	FT_Point,
};

//
// TODO: these should have a pointer to a scene node so they can be moved around
//
class ParticleForce : public ParticleAction
{
public:

	TYPE(4)

	ParticleForce() : mNode(0)
	{
	}

	virtual bool	Save(const File& out, SceneNode* node);
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node);

	virtual bool	Update(ParticleSystem* system, Particle* particle);

	virtual ParticleAction* Clone(ParticleActionGroup* ownerGroup, SceneNode* owner);

	float			mInfluence;
	float			mStrength;
	float			mDecay;
	ForceType		mType;
	SceneNode*		mNode;
};

class ParticleWind : public ParticleForce
{
public:

	TYPE(5)

	virtual bool	Save(const File& out, SceneNode* node);
	virtual bool	Load(const ObjectLoadParams* params, SceneNode* node);

	virtual bool	Update(ParticleSystem* system, Particle* particle);

	virtual ParticleAction* Clone(ParticleActionGroup* ownerGroup, SceneNode* owner);

	float			mTurbulence;
	float			mFrequency;
	float			mScale;
};

#endif
