#include "DX10RenderBuffer.h"
#include "File/File.h"
#include "Math/Vector.h"
#include "Math/Matrix.h"
#include "File/Log.h"

DX10RenderBuffer::DX10RenderBuffer(DX10Device* device) :
	mSize(0),
	mDevice(device),
	mBuffer(0),
	mData(0)
{

}

void DX10RenderBuffer::Init(RenderBufferType type, unsigned int size, RenderBufferUseage useage)
{
	RenderBuffer::Init(type, size, useage);
	mSize = -1;
	mUseage = useage;

	if (size != -1)
	{
		unsigned int bufferSize = 0;
		switch (mType)
		{
		case RBT_Matrix4:
			bufferSize = size * sizeof(Matrix4);
			break;

		case RBT_Vector4:
			bufferSize = size * sizeof(Vector4);
			break;

		case RBT_UnsignedInt:
			bufferSize = size * sizeof(unsigned int);
			break;
		}

		mSize = bufferSize;
		mData = _aligned_malloc(bufferSize, 16);
	}
}

void DX10RenderBuffer::Deinit()
{
	if (mData)
		_aligned_free(mData);

	mData = 0;
}

bool DX10RenderBuffer::Save(const File& out)
{
	return true;
}

bool DX10RenderBuffer::Load(Device* device, const File& in)
{
	// read the type
	in.Read(&mType, sizeof(RenderBufferType));

	// read the data
	if ((mSize = LoadVector(in, &mData)) == -1)
		return false;

	GenerateBuffer(device, &mBuffer, mData, mSize, mType == RBT_UnsignedInt);
	return true;
}

int DX10RenderBuffer::GetStride()
{
	switch (mType)
	{
	case RBT_Vector4:
		return sizeof(Vector4);

	case RBT_UnsignedInt:
		return sizeof(unsigned int);

	case RBT_Matrix4:
		return sizeof(Matrix4);

	default:
		return -1;
	}
}

void DX10RenderBuffer::Draw(unsigned int startIndex, unsigned int numIndices)
{
	unsigned int size = numIndices == -1 ? GetSize() : numIndices;
	switch (mUseage)
	{
	case RBU_Static:
		{
			//glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, BUFFER_OFFSET(startIndex * sizeof(unsigned int)));
		}
		break;

	case RBU_Dynamic:
		{
			//glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, &mUInt[0]);
		}
		break;
	}
}

void DX10RenderBuffer::Bind(VertexFormat channel, int index)
{
}

void DX10RenderBuffer::Unbind()
{
}

bool DX10RenderBuffer::Lock(RenderBufferLock& lock, int lockFlags)
{
	lock.data = mData;
	return lock.data != 0;
}

void DX10RenderBuffer::Unlock(RenderBufferLock& lock)
{
	switch (mUseage)
	{
	case RBU_Static:
		{
			//if (mBuffer != -1)
			//{
			//	Log::Error("[DX10RenderBuffer::Unlock] Trying to modify a static render buffer\n");
			//	return;
			//}

			GenerateBuffer(mDevice, &mBuffer, mData, mSize, mType == RBT_UnsignedInt);
		}
		break;

	case RBU_Dynamic:
		CopyBuffer(mDevice, &mBuffer, mData, (lock.count == -1) ? mSize : (lock.count * GetTypeSize(mType)), lock.startIndex * GetTypeSize(mType), mType == RBT_UnsignedInt);
		break;
	}
}

void DX10RenderBuffer::ReloadResource(Device* device)
{
	GenerateBuffer(mDevice, &mBuffer, mData, mSize, mType == RBT_UnsignedInt);
}

void DX10RenderBuffer::GenerateBuffer(Device* device, ID3D10Buffer** buffer, void* vec, unsigned int size, bool indexBuffer)
{
	D3D10_BUFFER_DESC bufferDesc;
	bufferDesc.BindFlags = indexBuffer ? D3D10_BIND_INDEX_BUFFER : D3D10_BIND_VERTEX_BUFFER;
    bufferDesc.ByteWidth = size; //sizeof(T) * mSize;
	bufferDesc.CPUAccessFlags = (mUseage == RBU_Dynamic) ? D3D10_CPU_ACCESS_WRITE : 0;
    bufferDesc.MiscFlags = 0;
	bufferDesc.Usage = (mUseage == RBU_Dynamic) ? D3D10_USAGE_DYNAMIC : D3D10_USAGE_DEFAULT;

    D3D10_SUBRESOURCE_DATA vbInitData;
    ZeroMemory(&vbInitData, sizeof(D3D10_SUBRESOURCE_DATA));
	vbInitData.pSysMem = (mSize == 0) ? 0 : vec;

    HRESULT hr = static_cast<DX10Device*>(device)->GetD3DDevice()->CreateBuffer(&bufferDesc, &vbInitData, buffer);
	int nothing = 0;
}

void DX10RenderBuffer::CopyBuffer(Device* device, ID3D10Buffer** buffer, void* vec, unsigned int size, unsigned int offset, bool indexBuffer)
{
	if (!*buffer)
		GenerateBuffer(device, buffer, vec, size, indexBuffer);

	void* data = 0;
	(*buffer)->Map(D3D10_MAP_WRITE_DISCARD, NULL, (void **)&data);
	memcpy(data, vec, size);
	(*buffer)->Unmap();
}
