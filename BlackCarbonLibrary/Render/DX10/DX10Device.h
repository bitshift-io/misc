#ifndef _DX10DEVICE_H_
#define _DX10DEVICE_H_

// disable memtracker defines
#undef	new
#undef	delete
#undef	malloc
#undef	calloc
#undef	realloc
#undef	free

//#define UNICODE
#include <d3dx10.h>
#include <d3d10.h>
#include "../Device.h"

class Window;
class DX10RenderTexture;

class DX10Device : public Device
{
public:

	// TODO: implement multisampling
	bool					Init(RenderFactory* factory, Window* window, int multisample = -1);
	void					Deinit(RenderFactory* factory, Window* window);

	virtual int				GetMultisample()									{ return mMultisample; }

	void					Begin();
	void					End();

	virtual RenderBuffer*	CreateRenderBuffer();
	virtual void			DestroyRenderBuffer(RenderBuffer** buffer);

	virtual bool			Draw(Geometry* geometry, unsigned int startIndex = 0, unsigned int numIndices = -1, int instanceCount = DT_NotInstanced);

	//virtual void			Draw(int instanceCount, Material* material, Geometry* geometry, int vertexFormat = 0, unsigned int startIndex = 0, unsigned int numIndices = -1/*, vector<RenderBuffer*>* instanceBuffer = 0*/);
	virtual void			SetMatrix(const Matrix4& matrix, MatrixMode mode);

	virtual void			DrawPoint(const Vector4& p, const Vector4& colour = Vector4(VI_One));
	virtual void			DrawLine(const Vector4& p1, const Vector4& p2, const Vector4& colour = Vector4(VI_One));

	virtual Window*			GetWindow()											{ return mWindow; }
	virtual void			ReloadResource()									{}

	ID3D10Device*			GetD3DDevice() const								{ return mD3DDevice; }
	ID3D10InfoQueue*		GetInfoQueue() const								{ return mInfoQueue; }

	int						CheckErrors() const;

	
	virtual void			SetGeometryTopology(GeometryTopology topology);
	
	virtual bool			SetDeviceState(const DeviceState& state, bool force = false);
	virtual bool			GetDeviceState(DeviceState& state);

	virtual void			DrawStencil()										{}
	virtual void			SaveStencil()										{}
	unsigned int			GetFPS()											{ return mFPS; }
	unsigned int			GetDrawCallCount()									{ return mDrawCallCount; }
	unsigned int			GetPolyCount()										{ return mPolyCount; }

	virtual void			BeginDebugEvent(const string& name, const Vector4& colour);
	virtual void			EndDebugEvent();

	virtual Vector4			GetClearColour();
	virtual void			SetClearColour(const Vector4& colour);

	void					SetInputLayout(ID3D10InputLayout* pLayout);

protected:

	void					UpdateStates();

	int						LookupOperation(Operation operation);
	int						LookupCompare(Compare compare);
	int						LookupCullFace(CullFace face);
	int						LookupBlendMode(BlendMode blend);


	//DX10RenderTexture*		mRenderTexture;

	ID3D10Device*           mD3DDevice;
	IDXGISwapChain*         mSwapChain;
//	ID3D10RenderTarGetZAxis* mRenderTarGetZAxis;
	ID3D10Texture2D*		mRender;

	ID3D10RenderTargetView*	mRenderTargetView;
	ID3D10DepthStencilView*	mDepthStencilView;
	ID3D10Texture2D*		mDepthStencil;
	ID3D10RasterizerState*  mRasterState;
	ID3D10InfoQueue*		mInfoQueue;

	ID3D10DepthStencilState*	mDepthStencilState;
	ID3D10BlendState*			mBlendState;

	D3D10_DEPTH_STENCIL_DESC	mDepthStencilDesc;
	D3D10_BLEND_DESC			mBlendDesc;
	D3D10_RASTERIZER_DESC		mRasterizerState;

	bool						mGeometryTopologyDirty;
	D3D10_PRIMITIVE_TOPOLOGY	mGeometryTopology;

	Window*					mWindow;

	// stats
	unsigned int			mFramesThisSecond;
	unsigned int			mFPS;
	unsigned int			mFrameTime;
	unsigned int			mDrawCallCount;
	unsigned int			mPolyCount;

	bool					mDepthStencilDirty;
	bool					mBlendStateDirty;
	bool					mRasterizerStateDirty;

	Vector4					mClearColour;
	int						mMultisample;

	DeviceState				mDeviceState;
};

#endif