#ifndef _DX10RENDERFACTORY_H_
#define _DX10RENDERFACTORY_H_

#include "../RenderFactory.h"

class DX10RenderFactory : public RenderFactory
{
public:

	DX10RenderFactory(const RenderFactoryParameter& parameter);

	virtual void			Init();
	virtual void			Deinit();

	virtual Window*			CreateWindow();
	virtual void			ReleaseWindow(Window** window);

	virtual Device*			CreateDevice();
	virtual void			ReleaseDevice(Device** device);

	virtual RenderFactoryType	GetRenderFactoryType()				{ return RFT_DirectX10; }
protected:

};

#endif
