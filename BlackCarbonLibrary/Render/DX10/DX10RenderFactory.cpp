#include "DX10RenderFactory.h"
#include "../Win32/Win32Window.h"
#include "DX10Device.h"
#include "DX10Effect.h"
#include "DX10Texture.h"
#include "DX10RenderTexture.h"
#include "Memory/Memory.h"

#undef CreateWindow

DX10RenderFactory::DX10RenderFactory(const RenderFactoryParameter& parameter) :
	RenderFactory(parameter)
{
	Register(ObjectCreator<DX10Effect>,			Effect::Type);
	Register(ObjectCreator<DX10Texture>,		Texture::Type);
	Register(ObjectCreator<DX10RenderTexture>,	RenderTexture::Type);
}

void DX10RenderFactory::Init()
{

}

void DX10RenderFactory::Deinit()
{

}

Window* DX10RenderFactory::CreateWindow()
{
	return new Win32Window;
}

Device*	DX10RenderFactory::CreateDevice()
{
	return new DX10Device;
}

void DX10RenderFactory::ReleaseWindow(Window** window)
{
	Win32Window* win = static_cast<Win32Window*>(*window);
	delete win;
	*window = 0;
}

void DX10RenderFactory::ReleaseDevice(Device** device)
{
	DX10Device* dev = static_cast<DX10Device*>(*device);
	delete dev;
	*device = 0;
}