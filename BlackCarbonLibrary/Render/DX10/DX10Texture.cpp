#include "DX10Texture.h"
#include "../RenderFactory.h"
#include "File/Log.h"

bool DX10Texture::IsInstance(const ObjectLoadParams* params, SceneObject* loaded)
{
	if (strcmpi(params->mName.c_str(), mName.c_str()) == 0)
		return true;

	return false;
}

bool DX10Texture::Load(const ObjectLoadParams* params, SceneNode* node)
{
	mName = params->mName;
	ID3D10Device* d3dDevice = static_cast<const DX10Device*>(params->mDevice)->GetD3DDevice();
	
	string path;
	if (params->mFactory->GetParameter().GetExistingFile(params->mName, "hdr", path))
	{
		HDR hdr;
		if (!hdr.Load(path.c_str()))
			return false;

		if (hdr.GetResourceInfo(0)->width == (hdr.GetResourceInfo(0)->height * 6))
		{
			hdr.FlipVertical(hdr.GetResourceInfo(0));
			bool result = GenerateCubeTexture(d3dDevice, &hdr, params->mFlags);
			hdr.Deinit();
			return result;
		}

		bool result = GenerateTexture(d3dDevice, &hdr, params->mFlags);
		hdr.Deinit();
		return result;
	}
	else 
	{
		TGA tga;
		params->mFactory->GetParameter().GetExistingFile(params->mName, "tga", path);
		
		if (!tga.Load(path.c_str()))
			return false;

		tga.ConvertToFormat(TF_RGBA);

		if (tga.GetResourceInfo(0)->width == (tga.GetResourceInfo(0)->height * 6))
		{
			tga.FlipVertical(tga.GetResourceInfo(0));
			bool result = GenerateCubeTexture(d3dDevice, &tga, params->mFlags);
			tga.Deinit();
			return result;
		}

		bool result = GenerateTexture(d3dDevice, &tga, params->mFlags);
		tga.Deinit();
		return result;
	}
}

bool DX10Texture::Init(Device* device, unsigned char* image, int channels, int width, int height, int depth, int flags)
{
	ID3D10Device* d3dDevice = static_cast<const DX10Device*>(device)->GetD3DDevice();

	// generate a 3d texture
	if (flags & TF_3D)
	{	
		CD3D10_TEXTURE3D_DESC desc(DXGI_FORMAT_R8G8B8A8_UNORM, width, height, depth);
		desc.MipLevels = 1;

		if (flags & TF_Write)
		{
			desc.CPUAccessFlags |= D3D10_CPU_ACCESS_WRITE;
			desc.Usage = D3D10_USAGE_DYNAMIC;
		}

		if (flags & TF_Read)
		{
			desc.CPUAccessFlags |= D3D10_CPU_ACCESS_READ;
			desc.Usage = D3D10_USAGE_STAGING;
		}

		D3D10_SUBRESOURCE_DATA data[1];
		memset(&data, 0, sizeof(D3D10_SUBRESOURCE_DATA) * 1);
		data[0].pSysMem = image;
		data[0].SysMemPitch = channels * width;
		data[0].SysMemSlicePitch = channels * width * height;

		HRESULT hr;
		hr = d3dDevice->CreateTexture3D(&desc, data, &mTexture3D);

		if (FAILED(hr))
			return false;

		mTexture3D->GetDesc(&desc);
		D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
		ZeroMemory(&SRVDesc, sizeof(SRVDesc));
		SRVDesc.Format = desc.Format;
		SRVDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE3D; 
		SRVDesc.Texture3D.MipLevels = desc.MipLevels;
		hr = d3dDevice->CreateShaderResourceView(mTexture3D, &SRVDesc, &mTextureRV);

		if (FAILED(hr))
			return false;

		return true;
	}
	else if (flags & TF_2D)
	{
		CD3D10_TEXTURE2D_DESC desc(DXGI_FORMAT_R8G8B8A8_UNORM, width, height);
		desc.MipLevels = 1;

		if (flags & TF_Write)
		{
			desc.CPUAccessFlags |= D3D10_CPU_ACCESS_WRITE;
			desc.Usage = D3D10_USAGE_DYNAMIC;
		}

		if (flags & TF_Read)
		{
			desc.CPUAccessFlags |= D3D10_CPU_ACCESS_READ;
			desc.Usage = D3D10_USAGE_STAGING;
		}

		D3D10_SUBRESOURCE_DATA data[1];
		memset(&data, 0, sizeof(D3D10_SUBRESOURCE_DATA) * 1);
		data[0].pSysMem = image;
		data[0].SysMemPitch = channels * width;
		data[0].SysMemSlicePitch = channels * width * height;

		HRESULT hr;
		hr = d3dDevice->CreateTexture2D(&desc, data, &mTexture);

		if (FAILED(hr))
			return false;

		mTexture->GetDesc(&desc);
		D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
		ZeroMemory(&SRVDesc, sizeof(SRVDesc));
		SRVDesc.Format = desc.Format;
		SRVDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D; 
		SRVDesc.Texture2D.MipLevels = desc.MipLevels;
		hr = d3dDevice->CreateShaderResourceView(mTexture, &SRVDesc, &mTextureRV);

		if (FAILED(hr))
			return false;

		return true;
	}

	return false;
}
/*
bool DX10Texture::Update(unsigned char* image)
{
	if (mTexture)
	{
		D3D10_TEXTURE2D_DESC desc;
		mTexture->GetDesc(&desc);

		// check we can modify it
		if (desc.CPUAccessFlags != D3D10_CPU_ACCESS_WRITE)
			return false;

		D3D10_MAPPED_TEXTURE2D mappedTex;
		mTexture->Map(D3D10CalcSubresource(0, 0, 1), D3D10_MAP_WRITE_DISCARD, 0, &mappedTex);

		UCHAR* pTexels = (UCHAR*)mappedTex.pData;
		int channelSize = (desc.Format == DXGI_FORMAT_R8G8B8A8_UNORM) ? 4 : 3;
		memcpy(pTexels, image, desc.Width * desc.Height * channelSize);
		mTexture->Unmap(D3D10CalcSubresource(0, 0, 1));
		return true;
	}

	return false;
}*/

bool DX10Texture::Lock(TextureSubresource& resource, int lockFlags)
{
	if (!mTexture)
		return 0;

	D3D10_TEXTURE2D_DESC desc;
	mTexture->GetDesc(&desc);

	// check we can modify it
	if (lockFlags & TF_Write && !(desc.CPUAccessFlags & D3D10_CPU_ACCESS_WRITE))
		return false;

	if (lockFlags & TF_Read && !(desc.CPUAccessFlags & D3D10_CPU_ACCESS_READ))
		return false;

	if (!GetSubresource(resource))
		return false;

	// hrmm make sure this is right!
	D3D10_MAP mapFlags = D3D10_MAP_WRITE_DISCARD;
	if (lockFlags == TF_Write)
		mapFlags = D3D10_MAP_WRITE_DISCARD;
	else if (lockFlags == (TF_Write | TF_Read))
		mapFlags = D3D10_MAP_READ_WRITE;
	else if (lockFlags == TF_Read)
		mapFlags = D3D10_MAP_READ;

	D3D10_MAPPED_TEXTURE2D mappedTex;
	mTexture->Map(D3D10CalcSubresource(0, 0, 1), (D3D10_MAP)mapFlags, 0, &mappedTex);
	resource.pixel = mappedTex.pData;

	return true;
}

void DX10Texture::Unlock()
{
	mTexture->Unmap(D3D10CalcSubresource(0, 0, 1));
}

bool DX10Texture::GetSubresource(TextureSubresource& resource)
{
	resource.pixel = 0;

	// TODO: handle all formats
	DXGI_FORMAT format = (DXGI_FORMAT)-1;

	if (mTexture)
	{
		D3D10_TEXTURE2D_DESC desc;
		mTexture->GetDesc(&desc);
		format = desc.Format;

		resource.width = desc.Width;
		resource.height = desc.Height;
		resource.depth = 0;
	}
	
	if (mTexture3D)
	{
		D3D10_TEXTURE3D_DESC desc;
		mTexture3D->GetDesc(&desc);
		format = desc.Format;
	
		resource.width = desc.Width;
		resource.height = desc.Height;
		resource.depth = desc.Depth;
	}

	switch (format)
	{
	case DXGI_FORMAT_R8G8B8A8_UNORM:
	case DXGI_FORMAT_R8G8B8A8_TYPELESS:
	case DXGI_FORMAT_R24G8_TYPELESS:
		resource.format = TF_RGBA;
		break;
		
	//case 
	//	resource.format = TF_RGB;
	//	break;

	default:
		Log::Error("[DX10Texture::GetSubresource] unknown resource format, defaulting to TF_RGBA");
		resource.format = TF_RGBA;
		return false;
	}

	return true;
}

bool DX10Texture::GenerateTexture(ID3D10Device* d3dDevice, TextureResource* texRes, int flags)
{
	TextureSubresource* resource = texRes->GetResourceInfo(0);
	int bpp = texRes->GetChannelCount(resource->format);
	int numMip = min(4, texRes->GetAutoMipmapCount());
	texRes->GenerateMipmap(numMip);
	resource = texRes->GetResourceInfo(0);

	DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM;
	if (texRes->GetChannelSize(resource->format) == sizeof(float))
		format = DXGI_FORMAT_R32G32B32_FLOAT;

	CD3D10_TEXTURE2D_DESC desc(format, resource->width, resource->height);
	desc.MipLevels = numMip;

	D3D10_SUBRESOURCE_DATA data[10];
	memset(&data, 0, sizeof(D3D10_SUBRESOURCE_DATA) * 10);

	for (int i = 0; i < texRes->GetResourceInfoCount(); ++i)
	{
		TextureSubresource* resource = texRes->GetResourceInfo(i);

		data[i].pSysMem = resource->pixel;
		data[i].SysMemPitch = bpp * resource->width;
		data[i].SysMemSlicePitch = bpp * resource->width * resource->height;
	}

	HRESULT hr;
	hr = d3dDevice->CreateTexture2D(&desc, data, &mTexture);
	if (FAILED(hr))
	{
		//d3dDevice->CheckErrors();
        return false;
	}

	//D3D10_TEXTURE2D_DESC desc;
    mTexture->GetDesc(&desc);
    D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
    ZeroMemory(&SRVDesc, sizeof(SRVDesc));
    SRVDesc.Format = desc.Format;
    SRVDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D; 
    SRVDesc.Texture2D.MipLevels = desc.MipLevels;
    hr = d3dDevice->CreateShaderResourceView(mTexture, &SRVDesc, &mTextureRV);

    if (FAILED(hr))
        return false;

	return true;
}

bool DX10Texture::GenerateCubeTexture(ID3D10Device* d3dDevice, TextureResource* texRes, int flags)
{
	texRes->GenerateCubemap();
	TextureSubresource* resource = texRes->GetResourceInfo(0);
	int bpp = texRes->GetChannelCount(texRes->GetResourceInfo(0)->format);

	DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM;
	if (texRes->GetChannelSize(resource->format) == sizeof(float))
		format = DXGI_FORMAT_R32G32B32_FLOAT;

	CD3D10_TEXTURE2D_DESC desc(format, resource->width, resource->height);
	desc.MipLevels = 1;
	desc.ArraySize = 6;
	desc.MiscFlags = D3D10_RESOURCE_MISC_TEXTURECUBE;

	D3D10_SUBRESOURCE_DATA data[6];
	memset(&data, 0, sizeof(D3D10_SUBRESOURCE_DATA) * 6);

	for (int i = 0; i < 6; ++i)
	{
		TextureSubresource* resource = texRes->GetResourceInfo(i);

		data[i].pSysMem = resource->pixel;
		data[i].SysMemPitch = bpp * resource->width;
		data[i].SysMemSlicePitch = bpp * resource->width * resource->height;
	}

	HRESULT hr;
	hr = d3dDevice->CreateTexture2D(&desc, data, &mTexture);

	if (FAILED(hr))
	{
		//d3dDevice->CheckErrors();
        return false;
	}

	//D3D10_TEXTURE2D_DESC desc;
    mTexture->GetDesc(&desc);
    D3D10_SHADER_RESOURCE_VIEW_DESC SRVDesc;
    ZeroMemory(&SRVDesc, sizeof(SRVDesc));
    SRVDesc.Format = desc.Format;
    SRVDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURECUBE;
    SRVDesc.Texture2D.MipLevels = desc.MipLevels;
    hr = d3dDevice->CreateShaderResourceView(mTexture, &SRVDesc, &mTextureRV);

    if (FAILED(hr))
        return false;

	return true;
}
