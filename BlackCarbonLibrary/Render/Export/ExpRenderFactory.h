#ifndef _EXPORT_RENDERFACTORY_H_
#define _EXPORT_RENDERFACTORY_H_

#include "../RenderFactory.h"

class ExpRenderFactory : public RenderFactory
{
public:
	
	ExpRenderFactory(const RenderFactoryParameter& parameter);

	virtual void			Init();
	virtual void			Deinit();

	virtual Window*			CreateWindow();
	virtual void			ReleaseWindow(Window** window)		{}

	virtual Device*			CreateDevice();
	virtual void			ReleaseDevice(Device** device);

	virtual RenderFactoryType	GetRenderFactoryType()			{ return RFT_Export; }
	
protected:

};

#endif