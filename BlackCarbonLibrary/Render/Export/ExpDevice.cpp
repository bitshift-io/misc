#include "ExpDevice.h"
#include "ExpRenderBuffer.h"

RenderBuffer* ExpDevice::CreateRenderBuffer()
{
	return new ExpRenderBuffer;
}

void ExpDevice::DestroyRenderBuffer(RenderBuffer** buffer)
{
	if (*buffer == 0)
		return;

	delete *buffer;
	*buffer = 0;
}