#include "ExpRenderBuffer.h"

ExpRenderBuffer::ExpRenderBuffer() :
	mData(0),
	mMaxSize(0),
	mSize(0)
{
}

bool ExpRenderBuffer::Save(const File& out)
{
	// save the type
	out.Write(&mType, sizeof(RenderBufferType));

	if (!SaveVector(out, mData, mSize))
		return false;

	return true;
}

void ExpRenderBuffer::IncreaseSize(unsigned int size)
{
	// buffer aint big enough? alloc a new one twice the size
	if ((mSize + size) > mMaxSize)
	{
		void* oldData = mData;
		
		unsigned int newMaxSize = max(size * 100, mMaxSize * 2);
		mData = _aligned_malloc(newMaxSize, 16);
		memcpy(mData, oldData, mSize);

		mMaxSize = newMaxSize;
		if (oldData)
			 _aligned_free(oldData);
	}
}

void ExpRenderBuffer::Insert(unsigned int value)
{
	IncreaseSize(sizeof(unsigned int));
	
	unsigned int* data = (unsigned int*)((unsigned char*)mData + mSize);
	*data = value;
	mSize += sizeof(unsigned int);
}

void ExpRenderBuffer::Insert(const Vector4& value)
{
	IncreaseSize(sizeof(Vector4));
	
	Vector4* data = (Vector4*)((unsigned char*)mData + mSize);
	*data = value;
	mSize += sizeof(Vector4);
}

Vector4& ExpRenderBuffer::GetVector4(int idx)
{
	Vector4* data = (Vector4*)((unsigned char*)mData + idx * sizeof(Vector4));
	return *data;
}

unsigned int ExpRenderBuffer::GetUnsignedInt(int idx)
{
	unsigned int* data = (unsigned int*)((unsigned char*)mData + idx * sizeof(unsigned int));
	return *data;
}

unsigned int ExpRenderBuffer::GetSize()
{
	return mSize / GetTypeSize(mType);
}