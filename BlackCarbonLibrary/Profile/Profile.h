#ifndef _PROFILE_H_
#define _PROFILE_H_

#include "Template/Singleton.h"
#include <hash_map>
#include <string>

using namespace std;
using namespace stdext;

class ProfileGraph;

typedef hash_multimap<int, ProfileGraph*> ProfileGraphHash;

#define gProfile Profile::GetInstance()

class Profile : public Singleton<Profile>
{
public:

	~Profile();

	ProfileGraph*	GetGraph(const string& name);
	ProfileGraph*	CreateGraph();

	int				Hash(const string& name);

protected:

	ProfileGraphHash	mGraph;
	list<ProfileGraph*>	mCustomGraph;
};

#endif