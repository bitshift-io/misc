#ifndef _FUNCTIONGRAPH_H_
#define _FUNCTIONGRAPH_H_

#include "Time/Time.h"
#include "ProfileGraph.h"

#define PROFILE()	FunctionGraph f(__FUNCTION__); 

class FunctionGraph
{
public:

	FunctionGraph(const char* function)
	{
		mGraph = gProfile.GetGraph(function);
		mStartTime = gTime.GetTimeSeconds();
	}

	~Function()
	{
		mGraph->Insert(gTime.GetTimeSeconds() - mStartTime);
	}

	ProfileGraph*	mGraph;
	float			mStartTime;
};

#endif