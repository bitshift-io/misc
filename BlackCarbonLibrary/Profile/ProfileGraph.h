#ifndef _PROFILEGROUP_H_
#define _PROFILEGROUP_H_

#include <string>
#include <list>

using namespace std;

class ProfileGraph
{
public:
	
	enum Type
	{
		T_None,
		T_Int,
		T_Float,
		T_String,
	};

	ProfileGraph(const string& name) : mType(T_None), mName(name)	{}

	void			Insert(float time, int value);
	void			Insert(float time, const string& value);
	void			Insert(float time, float value);

	const string&	GetName()				{ return mName; }

protected:

	list<float>		mTime;

	list<int>		mInt;
	list<float>		mFloat;
	list<string>	mString;
	Type			mType;

	string			mName;
};

#endif