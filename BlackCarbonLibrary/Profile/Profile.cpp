#include "Profile.h"
#include "ProfileGraph.h"

Profile::~Profile()
{
	// clean up
}

int Profile::Hash(const string& name)
{
	int h = 0;
	int len = name.length();
	for (int i = 0; i < len; i++) 
	{
		h = 31 * h + name[i];
	}

	return h;
}

ProfileGraph* Profile::GetGraph(const string& name)
{
	int hash = Hash(name);
	pair<ProfileGraphHash::const_iterator, ProfileGraphHash::const_iterator> p = mGraph.equal_range(hash);
	for (ProfileGraphHash::const_iterator it = p.first; it != p.second; ++it)
	{
		if (name.compare(it->second->GetName()) == 0)
			return it->second;
	}

	ProfileGraph* pGraph = new ProfileGraph(name);
	mGraph.insert(ProfileGraphHash::value_type(hash, pGraph));
	return pGraph;
}

ProfileGraph* Profile::CreateGraph()
{
	ProfileGraph* pGraph = new ProfileGraph(name);
	mCustomGraph.insert(pGraph);
	return pGraph;
}
