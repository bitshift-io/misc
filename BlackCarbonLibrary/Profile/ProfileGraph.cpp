#include "ProfileGraph.h"

void ProfileGraph::Insert(float time, int value)
{
	mType = T_Int;
	mInt.push_back(value);
}

void ProfileGraph::Insert(float time, const string& value)
{
	mType = T_String;
	mString.push_back(value);
}

void ProfileGraph::Insert(float time, float value)
{
	mType = T_Float;
	mFloat.push_back(value);
}