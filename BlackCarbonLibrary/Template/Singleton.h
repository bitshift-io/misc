#ifndef _SINGLETON_H_
#define _SINGLETON_H_

//
// always good to a #define gMySingleton MySingleton::GetInstance()
// for ease of use
//
template<class T>
class Singleton
{
public:

    Singleton()
    {
		SetInstance(static_cast<T*>(this));
    }

    ~Singleton()
    {
    }

	static void SetInstance(T* instance)
	{
		mInstance = instance;
	}

	static bool IsValid()
	{
		return mInstance ? true : false;
	}

	static inline T& GetInstance()
	{
		if (!mInstance)
		{
			static T instance;
		}

		return *mInstance;
	}

protected:

	static T*					mInstance;
};

template<class T>
T* Singleton<T>::mInstance = 0;

#endif
