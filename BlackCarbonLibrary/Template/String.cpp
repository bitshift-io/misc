#include "String.h"

vector<string> String::Tokenize(const string& str, const string& delimiters, bool stripWhitespace)
{
	vector<string> tokens;
    	
	// skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    	
	// find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos)
	{
		// found a token, add it to the vector.
		if (stripWhitespace)
			tokens.push_back(StripWhitespace(str.substr(lastPos, pos - lastPos)));
		else
			tokens.push_back(str.substr(lastPos, pos - lastPos));
	
		// skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
	
		// find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

	return tokens;
}

vector<string> String::TokenizeSpecial(const string& str, const string& delimiters, bool stripWhitespace)
{
	vector<string> tokens;
    	
	// skip delimiters at beginning.
	string::size_type lastPos = 0; //str.find_first_not_of(delimiters, 0);
    	
	// find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos + 1);

	while (string::npos != pos || string::npos != lastPos)
	{
		// found a token, add it to the vector.
		if (stripWhitespace)
			tokens.push_back(StripWhitespace(str.substr(lastPos, pos - lastPos)));
		else
			tokens.push_back(str.substr(lastPos, pos - lastPos));
	
		if (string::npos == pos)
			break;

		// skip delimiters.  Note the "not_of"
		lastPos = pos + 1; //str.find_first_of(delimiters, pos);
	
		// find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

	return tokens;
}

string String::StripWhitespace(const string& str)
{
	string temp = str;
	int len = strlen(temp.c_str()); //temp.length();
	int i = 0;
	for (; i < len; ++i)
	{
		if (temp[i] != ' ' && temp[i] != '\n' && temp[i] != '\t' && temp[i] != '\r' && temp[i] != '\f')
		{
			if (i != 0)
				temp = temp.substr(i, len);

			break;
		}
	}

	if (i >= len)
		return "";

	len = strlen(temp.c_str()); //temp.length();
	for (i = len - 1; i >= 0; --i)
	{
		if (temp[i] != ' ' && temp[i] != '\n' && temp[i] != '\t' && temp[i] != '\r' && temp[i] != '\f')
		{
			if (i != len)
				temp = temp.substr(0, i + 1);

			break;
		}
	}

	return temp;
}