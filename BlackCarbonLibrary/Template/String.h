#ifndef _STRING_H__
#define _STRING_H__

#include <string>
#include <vector>

using namespace std;

class String : public string
{
public:

	static vector<string>	Tokenize(const string& str, const string& delimiters, bool stripWhitespace);
	static vector<string>	TokenizeSpecial(const string& str, const string& delimiters, bool stripWhitespace);
	static string			StripWhitespace(const string& str);
};

#endif
