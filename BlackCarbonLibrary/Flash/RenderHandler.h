#ifndef _RENDERHANDLER_H_
#define _RENDERHANDLER_H_

#include "gameswf/gameswf.h"
#include "gameswf/gameswf_log.h"
#include "gameswf/gameswf_types.h"
#include "gameswf/gameswf_styles.h"

#include "Render/RenderBuffer.h"
#include "Math/Matrix.h"

class RenderFactory;
class Device;
class Geometry;
class Material;
class Texture;
class EffectParameterValue;

// bitmap_info_xbox declaration
struct BitmapInfo : public gameswf::bitmap_info
{
	Texture*	texture;
	int			mWidth;
	int			mHeight;
};


struct FillStyle : public gameswf::fill_style
{
	enum Mode
	{
		M_Disabled,
		M_Bitmap,
		M_Colour,
	};

	void  set_bitmap(const gameswf::bitmap_info* bi, const gameswf::matrix& m, gameswf::render_handler::bitmap_wrap_mode wm, const gameswf::cxform& color_transform);

	Mode						mMode;
	BitmapInfo*					mBitmapInfo;
	Matrix4						mMatrix;
	float						mLineWidth;
};

// You must define a subclass of render_handler, and pass an
// instance to set_render_handler().
struct RenderHandler : public gameswf::render_handler
{
	virtual ~RenderHandler() {}

	bool Init(RenderFactory* factory, Device* device);
	void Deinit(RenderFactory* factory);
	void GenerateGeometryBuffer(RenderFactory* factory, Device* device, int size);



	// Your handler should return these with a ref-count of 0.  (@@ is that the right policy?)
	virtual gameswf::bitmap_info*	create_bitmap_info_empty();	// used when DO_NOT_LOAD_BITMAPS is set
	virtual gameswf::bitmap_info*	create_bitmap_info_alpha(int w, int h, unsigned char* data);
	virtual gameswf::bitmap_info*	create_bitmap_info_rgb(image::rgb* im);
	virtual gameswf::bitmap_info*	create_bitmap_info_rgba(image::rgba* im);

	virtual gameswf::video_handler*	create_video_handler();

	// Bracket the displaying of a frame from a movie.
	// Fill the background color, and set up default
	// transforms, etc.
	virtual void	begin_display(
		gameswf::rgba background_color,
		int viewport_x0, int viewport_y0,
		int viewport_width, int viewport_height,
		float x0, float x1, float y0, float y1);

	virtual void	end_display();

	// Geometric and color transforms for mesh and line_strip rendering.
	virtual void	set_matrix(const gameswf::matrix& m);
	virtual void	set_cxform(const gameswf::cxform& cx);

	// Draw triangles using the current fill-style 0.
	// Clears the style list after rendering.
	//
	// coords is a list of (x,y) coordinate pairs, in
	// triangle-strip order.  The type of the array should
	// be Sint16[vertex_count*2]
	virtual void	draw_mesh_strip(const void* coords, int vertex_count);

	// As above, but coords is in triangle list order.
	virtual void	draw_triangle_list(const void* coords, int vertex_count) {}

	// Draw a line-strip using the current line style.
	// Clear the style list after rendering.
	//
	// Coords is a list of (x,y) coordinate pairs, in
	// sequence.  Each coord is a 16-bit signed integer.
	virtual void	draw_line_strip(const void* coords, int vertex_count);

	// Set line and fill styles for mesh & line_strip
	// rendering.
	virtual void	fill_style_disable(int fill_side);
	virtual void	fill_style_color(int fill_side, const gameswf::rgba& color);
	virtual void	fill_style_bitmap(int fill_side, gameswf::bitmap_info* bi, const gameswf::matrix& m, bitmap_wrap_mode wm);

	virtual void	line_style_disable();
	virtual void	line_style_color(gameswf::rgba color);
	virtual void	line_style_width(float width);

	// Special function to draw a rectangular bitmap;
	// intended for textured glyph rendering.  Ignores
	// current transforms.
	virtual void	draw_bitmap(
		const gameswf::matrix&		m,
		gameswf::bitmap_info*	bi,
		const gameswf::rect&		coords,
		const gameswf::rect&		uv_coords,
		gameswf::rgba			color);

	virtual void	set_antialiased(bool enable) {}

	virtual bool test_stencil_buffer(const gameswf::rect& bound, Uint8 pattern)					{ return true; }
	virtual void begin_submit_mask();
	virtual void end_submit_mask();
	virtual void disable_mask();

	// Mouse cursor handling.
	virtual void set_cursor(cursor_type cursor) {}
	virtual bool is_visible(const gameswf::rect& bound)	{ return true; }
	virtual void open()										{}

	virtual void SetBufferPosition(unsigned int idx, const Vector4& pos);
	virtual void FlushRenderBuffer();

	// Style state.
	enum style_index
	{
		LEFT_STYLE = 0,
		RIGHT_STYLE,
		LINE_STYLE,

		STYLE_COUNT
	};

	FillStyle		mFillStyle[STYLE_COUNT];
	style_index		mLastStyle;

	RenderFactory*	mFactory;
	Device*			mDevice;
	Matrix4			mFlashToEngine;
	Matrix4			mMatrix;
	gameswf::cxform	mCXForm;
	Geometry*		mGeometry;
	Material*		mMaterial;

	EffectParameterValue* mTextureParam;
	EffectParameterValue* mColourParam[2];
	EffectParameterValue* mTextureMatrixParam;

	float			mFlashWidth;
	float			mFlashHeight;
	bool			mMaskEnabled;

	unsigned int	mBufferPolyCount;
	RenderBufferLock mPositionLock;
};

inline void RenderHandler::SetBufferPosition(unsigned int idx, const Vector4& pos)
{
	mMatrix.Transform(pos, &mPositionLock.vector4Data[idx]);
}
#endif