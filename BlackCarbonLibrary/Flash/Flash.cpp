#include "gameswf/gameswf.h"
#include "gameswf/gameswf_impl.h"
#include "Render/Window.h"
#include "RenderHandler.h"
#include "Network/NetworkFactory.h"
#include "Network/HTTP.h"
#include "Network/Packet.h"

#include "base/tu_file.h"
//#include "net/tu_net_file.h"
#include "Flash.h"
#include "File/Log.h"
#include "File/File.h"
#include "File/MemoryFile.h"
#include "Input/Input.h"

#include "gameswf/gameswf_root.h"
#include "Gameswf/gameswf_as_sprite.h"

#ifdef INTERFACE
	#undef INTERFACE
#endif

#define PLAYER (*((gameswf::gc_ptr<gameswf::player>*)mPlayer))
#define INSTANCE (*((gameswf::gc_ptr<gameswf::root>*)mInstance))
/*
#define DEFINITION (*((smart_ptr<gameswf::movie_definition>*)mDefinition))
#define INTERFACE (*((smart_ptr<gameswf::root>*)mInterface))
*/
class FlashFile : public tu_file
{
public:

	FlashFile() :
	  tu_file(
			this,
			Read,
			Write,
			Seek,
			SeekToEnd,
			Tell,
			GetEOF,
			Close)
	{	
		mMemoryFileContent.data = 0;
	}

	static int Read(void* dst, int bytes, void* appdata)
	{
		FlashFile* flashFile = (FlashFile*)appdata;
		File* file = &flashFile->mFile;
		return file->Read(dst, bytes);
	}

	static int Write(const void* src, int bytes, void* appdata)
	{
		FlashFile* flashFile = (FlashFile*)appdata;
		File* file = &flashFile->mFile;
		return file->Write(src, bytes);
	}

	static int Seek(int pos, void* appdata)
	{
		FlashFile* flashFile = (FlashFile*)appdata;
		File* file = &flashFile->mFile;
		return file->Seek(pos, FST_Begin);
	}

	static int SeekToEnd(void* appdata)
	{
		FlashFile* flashFile = (FlashFile*)appdata;
		File* file = &flashFile->mFile;
		return file->Seek(0, FST_End);
	}

	static int Tell(const void* appdata)
	{
		FlashFile* flashFile = (FlashFile*)appdata;
		File* file = &flashFile->mFile;
		return file->GetPosition();
	}

	static bool GetEOF(void* appdata)
	{
		FlashFile* flashFile = (FlashFile*)appdata;
		File* file = &flashFile->mFile;
		return file->EndOfFile();
	}

	static int Close(void* appdata)
	{
		FlashFile* flashFile = (FlashFile*)appdata;
		File* file = &flashFile->mFile;
		file->Close();

		if (flashFile->mMemoryFileContent.data != 0)
		{
			delete[] flashFile->mMemoryFileContent.data;
		}

		return 0;
	}

	MemoryFileContent	mMemoryFileContent;
	File				mFile;
};

// Error callback for handling gameswf messages.
static void	log_callback(bool error, const char* message)
{
#ifdef _DEBUG
	Log::Print("%s\n", message);
#endif
}


// Callback function.  This opens files for the gameswf library.
static tu_file*	file_opener(const char* url)
{/*
	static bool httpInit = false;
	static HTTP* http = gNetworkFactory.CreateHTTP();
	if (!httpInit)
	{
		http->InsertHTTPArg("x-flash-version: 6,0,47,0");
		http->InsertHTTPArg("User-Agent: Shockwave Flash");
		httpInit = true;
	}
*/
	
	// else open swf from disk
	string actualFile = url;
	actualFile += ".swf";

	FlashFile* file = new FlashFile;
/*
	// check if its trying to open a http swf
	string host;
	string protocol;
	string args;
	string path;
	int port;
	if (gNetworkFactory.ParseURL(url, &protocol, &host, &port, &path, &args))
	{
		gameswf::as_environment* env = target_value->m_property_target->get_environment();
		
		string line;
		for (stringi_hash<gameswf::as_value>::const_iterator it = env->m_variables.begin(); it != env->m_variables.end(); ++it)
		{
			const char* name = it->first.c_str();

			switch (it->second.get_type())
			{
			case gameswf::as_value::STRING:
				{
					if (line.length() > 0)
						line += "&";

					const char* value = it->second.to_string();
					line += string(name) + string("=") + string(value);
				}
				break;

			case gameswf::as_value::NUMBER:
				{
					if (line.length() > 0)
						line += "&";

					float value = it->second.to_number();
					char buffer[256];
					if (int(value) == value)
						sprintf(buffer, "%i", int(value));
					else
						sprintf(buffer, "%f", value);

					line += string(name) + string("=") + string(buffer);
				}
				break;

			case gameswf::as_value::AS_FUNCTION:
				{
					if (line.length() > 0)
						line += "&";

					gameswf::as_as_function* value = it->second.to_as_function();
					line += string(name) + string("=[type+Function]");
				}
				break;

			case gameswf::as_value::C_FUNCTION:
				{
					Log::Error("gameswf::as_value::C_FUNCTION not yet supported");
				}
				break;

			case gameswf::as_value::PROPERTY:
				{
					Log::Error("gameswf::as_value::PROPERTY not yet supported");
				}
				break;

			case gameswf::as_value::OBJECT:
				{
					gameswf::as_object_interface* value = it->second.to_object();
					//line += string(name) + string("=") + string(value->get_text_value());
				}
				break;

			case gameswf::as_value::BOOLEAN:
				{
					if (line.length() > 0)
						line += "&";

					line += string(name) + string("=") + (it->second.to_bool() ? string("1") : string("0"));
				}
				break;

			case gameswf::as_value::NULLTYPE:
				break;

			case gameswf::as_value::UNDEFINED:
				break;

			default:
				break;
			}
		}

		line = gNetworkFactory.ConvertStringToURL(line.c_str());
		if (!http->Connect(host.c_str()))
			return 0;

		Packet p;
		Packet data;
		//string line = "mav=2%2E2&swfv=7&regpt=o&color=16747008&background=16777161&outline=13994812&ad%5Fstarted=%5Btype+Function%5D&ad%5Ffinished=%5Btype+Function%5D&res=550x400&method=showPreloaderAd&clip=%5Flevel0&st=28";
		p.Append(line.c_str(), p.GetSize());
		if (!http->Post(path.c_str(), "application/x-www-form-urlencoded", &p, &data))
			return 0;

		http->Close();

		int size = data.GetSize() - data.GetCursor();
		file->mMemoryFileContent.data = new unsigned char[size];
		memcpy(file->mMemoryFileContent.data, data.GetBuffer() + data.GetCursor(), size);
		file->mMemoryFileContent.size = size;
		file->mFile.Open(new MemoryFile(&file->mMemoryFileContent), FAT_Read | FAT_Binary);

		File temp("temp.swf", FAT_Write | FAT_Binary);
		temp.Write(data.GetBuffer() + data.GetCursor(), size);
		temp.Close();
	}
	else */if (!file->mFile.Open(actualFile.c_str(), FAT_Read | FAT_Binary))
	{
		delete file;
		return 0;
	}

	return file;
}

void StaticFsCommand(gameswf::character* movie, const char* command, const char* arg)
{
	//gameswf::gc_ptr<gameswf::player> player = movie->get_player();
	void* userData = movie->get_root()->get_userdata();
	Flash* pFlash = (Flash*)userData;
	pFlash->FsCommand(command, arg);
}

void Flash::Init(RenderFactory* factory, Device* device, Window* window)
{
	mFactory = factory;

	mCallback = 0;
	mWindow = window;

	mPlayer = new gameswf::gc_ptr<gameswf::player>();
	mInstance = new gameswf::gc_ptr<gameswf::root>();

	mRenderHandler = new RenderHandler();
	mRenderHandler->Init(factory, device);
	SetAntialiased(true);
	SetLooping(false);
}

void Flash::Deinit()
{
	INSTANCE = 0;
	PLAYER = 0;

	delete mPlayer;
	mPlayer = 0;

	delete mInstance;
	mInstance = 0;

	mRenderHandler->Deinit(mFactory);
	delete mRenderHandler;
	mRenderHandler = 0;
}


void Flash::InitializeMovie()
{
	INSTANCE = 0;
	PLAYER = 0;

	mBackgroundAlpha = 1.f;
	mViewport.x = 0.f;
	mViewport.y = 0.f;

	gameswf::register_file_opener_callback(file_opener);

#ifdef DEBUG
//	gameswf::register_log_callback(log_callback);
#endif

	gameswf::register_fscommand_callback(StaticFsCommand);
/*
	mSoundHandler = gameswf::create_sound_handler_tge();
	gameswf::set_sound_handler(mSoundHandler);
	*/
	gameswf::set_render_handler(mRenderHandler); 
/*
	// Get info about the width & height of the movie.
	int	version = 0;
	float fps = 30.0f;
	gameswf::get_movie_info(mName.c_str(), &version, &mWidth, &mHeight, &fps, NULL, NULL);
	if (version == 0)
	{
		Log::Error("error: can't get info about %s\n", mName.c_str());
		return;
	}*/
	/*
	Log::Print("Flash file loaded successfully\nName %s\nVersion %i\nWidth %i\nHeight %i\nFPS %i", mName.c_str(), version, mWidth, mHeight, fps);
*/
	

	//computeMouseScale(mBounds.extent);

	
	gameswf::set_verbose_action(false);
	gameswf::set_verbose_parse(false);

	PLAYER = new gameswf::player();
	INSTANCE = PLAYER->load_file(mName.c_str());
	if (INSTANCE == NULL)
	{
		ERROR("Load flash failed: %s\n", mName.c_str());
	}
	
/*
	DEFINITION = gameswf::create_movie(mName.c_str());
	if (DEFINITION == NULL)
	{
		fprintf(stderr, "error: can't create a movie from '%s'\n", mName.c_str());
		return;
	}

	INTERFACE = DEFINITION->create_instance();
	if (INTERFACE == NULL)
	{
		fprintf(stderr, "error: can't create movie instance\n");
		return;
	}
*/
	//gameswf::set_current_root(INTERFACE.get_ptr());

	int	movie_version = INSTANCE->get_movie_version();
	float	movie_fps = INSTANCE->get_movie_fps();

	mRenderHandler->mFlashWidth = INSTANCE->get_movie_width();
	mRenderHandler->mFlashHeight = INSTANCE->get_movie_height();

	INSTANCE->set_userdata(this);

		/*

	// Load the actual movie.
	mDefinition = gameswf::create_movie(mName.c_str());
	if (mDefinition == NULL)
	{
		Log::Error("error: can't create a movie from '%s'\n", mName.c_str());
		return;
	}

	mInterface = mDefinition->create_instance();
	if (mInterface == NULL)
	{
		Log::Error("error: can't create movie instance\n");
		return;
	}
*/
	//last_ticks = Platform::getRealMilliseconds();	

	// this sets up globals so we can call action script functions without crashing before the first update ;)
	Update(0.f, 0);
}

void Flash::ReloadResource()
{
	// FMNOTE: do i need to clean up render handler before reloading
	// flash
//	Deinit();
//	Init(mFactory, mDevice, mWindow);
	Load(mName.c_str());
}

void Flash::Load(const char* name)
{
	mName = name;
	InitializeMovie();
}

void Flash::Play()
{
	if (INSTANCE->get_play_state() == gameswf::character::PLAY)
		return;

	INSTANCE->set_play_state(gameswf::character::PLAY);
}

void Flash::Render()
{
	gameswf::set_render_handler(mRenderHandler);

	INSTANCE->set_display_viewport(mViewport.x, mViewport.y, mViewport.width, mViewport.height);
	INSTANCE->set_background_alpha(mBackgroundAlpha);
	
	               
	INSTANCE->display(); 
}

void Flash::Update(float deltaTime, Input* input)
{
	int mouseButtons = 0;
	WindowResolution res;
	mWindow->GetWindowResolution(res);
	mViewport.width = mWidth; //res.width;
	mViewport.height = mHeight; //res.height;

	int xCursor, yCursor;
	float xPercent, yPercent;
	mWindow->GetCursorPosition(xCursor, yCursor);

	if (input)
	{
		if (input->GetJoystickCount() > 0)
		{
			float x = input->GetState(JOYSTICK0_AxisX);// - 0.5f;
			float y = input->GetState(JOYSTICK0_AxisY);// - 0.5f;

			xCursor += int(x * 5.f);
			yCursor += int(y * 5.f);
				
			mWindow->SetCursorPosition(xCursor, yCursor);

			if (input->GetStateBool(JOYSTICK0_Button1))
				mouseButtons |= (1 << 0);

			if (input->GetStateBool(JOYSTICK0_Button2))
				mouseButtons |= (1 << 1);
		}

		if (input->GetStateBool(MOUSE_Left))
			mouseButtons |= (1 << 0);

		if (input->GetStateBool(MOUSE_Right))
			mouseButtons |= (1 << 1);
	}

	// need to change this so it centers the flash even with stretched screen
	float actualAspectRatio = mWindow->GetAspectRatio();
	float flashAspectRatio = mRenderHandler->mFlashWidth / mRenderHandler->mFlashHeight;

	xPercent = float(xCursor) / float(res.width);
	yPercent = float(yCursor) / float(res.height);

	//xPercent *= (actualAspectRatio / flashAspectRatio);

	int movieWidth = INSTANCE->get_movie_width();
	int movieHeight = INSTANCE->get_movie_height();
	int actualWidth = movieWidth * (actualAspectRatio / flashAspectRatio);
	int widthDifference = (actualWidth - movieWidth) * 0.5f;
	int xAxis = int(float(movieWidth)* xPercent) + Math::Lerp(-widthDifference, widthDifference, xPercent);
	int yAxis = int(float(movieHeight) * yPercent);

	INSTANCE->notify_mouse_state(xAxis, yAxis, mouseButtons);
	INSTANCE->advance(deltaTime);

	if (HasLooped())
		Stop();
}

void Flash::GotoScene(const char* scene)
{
	gameswf::as_environment env(PLAYER.get_ptr());
	env.set_target(INSTANCE->get_root_movie());
	env.push(gameswf::as_value(scene));
	env.push(gameswf::as_value(1));
	gameswf::fn_call fn(0, INSTANCE.get_ptr(), &env, 2, 0);	
	gameswf::sprite_goto_and_play(fn);
}

void Flash::GotoFrame(unsigned int frame)
{
	gameswf::character::play_state playState = INSTANCE->get_play_state();
	INSTANCE->goto_frame(frame);
	INSTANCE->set_play_state(playState);
}

void Flash::GotoLabeledFrame(const char* frame)
{
	gameswf::character::play_state playState = INSTANCE->get_play_state();
	INSTANCE->goto_labeled_frame(frame);
	INSTANCE->set_play_state(playState);
}

void Flash::Stop()
{
	INSTANCE->set_play_state(gameswf::character::STOP);
}

void Flash::Restart()
{
	GotoFrame(0);
}

bool Flash::HasLooped()
{
	return INSTANCE->get_current_frame() + 1 >= INSTANCE->get_frame_count();
}

void Flash::SetLooping(bool looping)
{
	mLooping = looping;
}

void Flash::SetBackgroundColour(const Vector4& colour)
{
	gameswf::rgba col(colour.r * 255.f, colour.g * 255.f, colour.b * 255.f, colour.a * 255.f);
	INSTANCE->set_background_color(col);
}

Vector4	Flash::GetBackgroundColour()
{
	gameswf::rgba col = INSTANCE->get_background_color();
	return Vector4(float(col.m_r) / 255.f, float(col.m_g) / 255.f, float(col.m_b) / 255.f, float(col.m_a) / 255.f);
}

void Flash::SetCallback(FlashCallback* callback)
{
	mCallback = callback;
}

void Flash::FsCommand(const char* command, const char* arg)
{
	if (mCallback)
		mCallback->FsCommand(this, command, arg);
}

void Flash::SetAntialiased(bool enabled)
{
	mRenderHandler->set_antialiased(enabled);
}

void Flash::SetVariable(const char* pathToVariable, const char* newValue)
{
	INSTANCE->set_variable(pathToVariable, newValue);
}
  
string Flash::GetVariable(const char* pathToVariable)
{
	return string(INSTANCE->get_variable(pathToVariable));
}

string Flash::CallMethod(const char* methodName, const char* argFormat, ...)
{/*
	// Keep m_as_environment alive during any method calls!
	smart_ptr<as_object_interface>	this_ptr(this);
	return call_method_parsed(&m_as_environment, this, method_name, method_arg_fmt, args);

	
	
	va_list	argList;
	va_start(argList, argFormat);
	const char* result = mInterface->call_method_args(methodName, argFormat, argList);
	va_end(argList);
	return string(result);
	

	
/*
	// Invoke user defined event handler
	//gameswf::as_value function;
	if (!mInterface->get_member(methodName, &function))
	{
		int nothing = 0;
	}
	//	return string();
*/
	
	gameswf::as_value method;
	gameswf::character* movieRoot = INSTANCE->get_root_movie();
	if (!movieRoot || !INSTANCE->get_root_movie()->get_member(methodName, &method))
	{
		//Log::Error("[Flash::CallMethod] Method '%s' not found", methodName);
		return string();
	}

	gameswf::as_environment env(PLAYER.get_ptr());
	env.set_target(INSTANCE->get_root_movie());
	int numArgs = 0;
	int len = strlen(argFormat);

	va_list args;
	va_start(args, argFormat);

	const char* c = argFormat;
	while (*c)
	{
		 if (*c == '%')
		 {
			 ++c;
			switch (*c)
			{
			case 's':
				env.push(va_arg(args, const char *));
				break;

			case 'i':
			case 'd':
				env.push(va_arg(args, int));
				break;

			case 'f':
				env.push(va_arg(args, double));
				break;
			}

			++numArgs;
		}

		++c;
	}

	va_end(args);

	// Reverse the order of pushed args
	for (int i = 0; i < (numArgs >> 1); ++i)
	{
		int	i0 = i;
		int	i1 = (numArgs - 1) - i;
		assert(i0 < i1);

		swap(&(env.bottom(i0)), &(env.bottom(i1)));
	}

	gameswf::as_value result = gameswf::call_method(method, &env, INSTANCE.get_ptr(), numArgs, env.get_top_index());
	return string(result.to_string());
}
