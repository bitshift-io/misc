#ifndef _FLASH_H_
#define _FLASH_H_

#include <string>

using namespace std;

struct RenderHandler;
class Input;

struct Rect
{
	float	x;
	float	y;
	float	width;
	float	height;
};

class Flash;

class FlashCallback
{
public:

	virtual void	FsCommand(Flash* flash, const char* command, const char* arg) = 0;
};

class Flash
{
public:

	void	Init(RenderFactory* factory, Device* device, Window* window);
	void	Deinit();
	void	Load(const char* name);
	void	Play();
	void	Stop();
	void	Restart();
	void	SetLooping(bool looping);
	bool	HasLooped();
	void	Render();
	void	Update(float deltaTime, Input* input);
	void	SetAntialiased(bool enabled);

	void	ReloadResource();

	// this causes the movie to stop :-/ is that right?
	void	GotoScene(const char* scene);
	void	GotoFrame(unsigned int frame);
	void	GotoLabeledFrame(const char* frame);

	void	SetCallback(FlashCallback* callback);

	void	SetBackgroundColour(const Vector4& colour);
	Vector4	GetBackgroundColour();

	// Set's a variable in flash, example _root.myVar
	void		SetVariable(const char* pathToVariable, const char* newValue);

	// Gets a flash variable, the buffer returned is valid until the control
	// is deleted.  
	string		GetVariable(const char* pathToVariable);

	// Call method inside flash movie and return result as string
	string		CallMethod(const char* methodName, const char* argFormat = "", ...);

	int			GetHeight()							{ return mHeight; }
	int			GetWidth()							{ return mWidth; }

	void		FsCommand(const char* command, const char* arg);

protected:

	void		InitializeMovie();

	string		mName;
	int			mWidth;
	int			mHeight;

	Rect		mViewport;
	Window*		mWindow;
	Device*		mDevice;
	float		mBackgroundAlpha;
	bool		mLooping;

	RenderFactory*		mFactory;
	RenderHandler*		mRenderHandler;

	void*	 mPlayer;
	void*	 mInstance;
	/*
	// nasty hack to get around gameswf not linking nicely with projects
	void*				mDefinition;
	void*				mInterface;
*/
	FlashCallback*		mCallback;
};

#endif