#include "Spline.h"
#include "Memory/Memory.h"

Spline3::Spline3() :
	mKnot(0)
{
}

void Spline3::Init(int count)
{
	mCount = count;
	mKnot = (Knot3*)_aligned_malloc(sizeof(Knot3) * count, 16);
}

void Spline3::Deinit()
{
	if (mKnot)
		_aligned_free(mKnot);

	mKnot = 0;
}

Vector4 Spline3::Evaluate(float time)
{
	// http://mathworld.wolfram.com/BezierCurve.html
	// http://mathworld.wolfram.com/BernsteinPolynomial.html

	Vector4 ret(0.f, 0.f, 0.f);
/*
	int i = 0;
	int n = mKnot.size();
	//vector<Knot3>::iterator it;
	//for (it = mKnot.begin(); it != mKnot.end(); ++it, ++i)
	//{
	//	ret += (it->mPosition + it->mIn)
	//	ret += (it->mPosition + it->mOut)
	//}
*/
	return Vector4();
}
