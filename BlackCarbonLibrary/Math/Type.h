#ifndef _TYPE_H_
#define _TYPE_H_

struct Int3
{
	union
	{
		struct
		{
			int x;
			int y;
			int z;
		};

		int data[4];
	};
};

#endif