#ifndef _TRIANGLE_H_
#define _TRIANGLE_H_

class Triangle
{
public:

	Triangle() {}
	Triangle(const Vector4& a, const Vector4& b, const Vector4& c);

	LineSegment GetEdgeAsSegment(int index);

	Vector4 p[3];
};

#include "Triangle.inl"

#endif