#ifndef __LINE_H_
#define __LINE_H_
/*
#include "Sphere.h"

struct TriIntersect
{
	Vector4 intersection;
	float	rayDistance;
};

class Line
{
public:

	Line();
	Line(const Vector4& _point, const Vector4& _direction);

	Vector4	ClosetPoint(const Vector4& _point);
	float	DistanceToPoint(const Vector4& _point);
	bool	Intersect(const Line& other);
	bool	Intersect(const Sphere& sphere) const;

	// check to see where a line intersects a triangle, cull is for back face culling
	bool	IntersectTri(const Vector4& p0, const Vector4& p1, const Vector4& p2, bool cull = true, TriIntersect* intersect = 0) const;

	// check to see where a line intersects a tri through the side (ie. a line segment would be the result)
	bool	IntersectTriSide(const Vector4& p0, const Vector4& p1, const Vector4& p2);

	Vector4	point;
	Vector4	direction;
	float	distance;
};

*/
#include "Line.inl"

#endif
