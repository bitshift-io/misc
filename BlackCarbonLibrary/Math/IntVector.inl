/*
//=========================================================================
// IntVector4
//=========================================================================
inline IntVector4::IntVector4()
{
}

inline IntVector4::IntVector4(const Vector4& vec, int numDecimalPlaces)
{
	x = Math::FloatToFixed(vec.x, numDecimalPlaces);
	y = Math::FloatToFixed(vec.y, numDecimalPlaces);
}

inline Vector4 IntVector4::AsVector4(int numDecimalPlaces)
{
	return Vector4(Math::FixedToFloat(x, numDecimalPlaces), Math::FixedToFloat(y, numDecimalPlaces));
}*/