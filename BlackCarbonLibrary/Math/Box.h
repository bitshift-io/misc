#ifndef _BOX_H_
#define _BOX_H_

#include "Vector.h"
#include "Matrix.h"

class AABox
{
public:

	void	SetMin(const Vector4& _min)	{ min = _min; }
	void	SetMax(const Vector4& _max)	{ max = _max; }

	const	Vector4& GetMin() const			{ return min; }
	const	Vector4& GetMax() const			{ return max; }

	Vector4	GetCenter() const				{ return min + (max - min) * 0.5f; }

	Vector4 min;
	Vector4 max;
};

class Box : public AABox
{
public:

	Box() : transform(MI_Identity)
	{
	}

	void SetTransform(const Matrix4& _transform)	{ transform = _transform; }
	const Matrix4& GetTransform() const				{ return transform;	}

	Matrix4 transform;
};

#endif
