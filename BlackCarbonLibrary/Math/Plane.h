#ifndef __PLANE_H_
#define __PLANE_H_

#include "Vector.h"
#include "Matrix.h"

class Line;
class Ray;

class Plane
{
public:

	Plane() {}

	Plane(const Vector4& _point, const Vector4& _normal);

	float	DistanceToPoint(const Vector4& _point) const;
	float	DistanceToPlane(const Plane& other) const;

	Plane	Transform(const Matrix4& transform) const;

/*
	// the following are plane extractions from matrices
	// http://www2.ravensoft.com/users/ggribb/plane%20extraction.pdf
	void ExtractLeft(const Matrix4& transform);
	void ExtractRight(const Matrix4& transform);
	void ExtractTop(const Matrix4& transform);
	void ExtractBottom(const Matrix4& transform);
	void ExtractNear(const Matrix4& transform);
	void ExtractFar(const Matrix4& transform);
*/
	float	Normalize();

	Vector4 Intersect3(Plane& p1, Plane& p2);
	//float	Intersect(const Line& line) const;
	float	Intersect(const Ray& ray) const;

	float	distance;
	Vector4	normal;
};

#include "Plane.inl"

#endif
