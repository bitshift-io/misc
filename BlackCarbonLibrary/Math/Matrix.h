#ifndef _MATRIX_H_
#define _MATRIX_H_

#include "Math.h"
#include "Vector.h"

enum MatrixInit
{
	MI_Identity,
	MI_Zero,
	MI_Mirror,
};

//=========================================================================
// Matrix3
//=========================================================================
class Matrix3
{
public:

	float&			operator[](const int index);

	float			GetDeterminant() const;

	union
	{
		float	mat[3][3];
		float	m[9];
	};
};

//=========================================================================
// Matrix4
//=========================================================================
ALIGN16 class Matrix4
{
public:

	Matrix4();
	Matrix4(MatrixInit type);
	Matrix4(const float* data);

	void			SetScale(float scale);
	void			SetScale(float x, float y, float z);
	void			SetScale(const Vector4& scale);

	void			SetIdentity();
	void			SetZero();
	void			SetMirror();

	void			SetOrthographic(float width, float height, float znear, float zfar);
	void			SetOrthographic(float left, float right, float top, float bottom, float znear, float zfar);
	void			SetPerspective(float fovy, float aspect, float znear, float zfar);
	void			SetTranslation(float x, float y, float z);
	void			SetTranslation(const Vector4& trans);
	void			Translate(const Vector4& trans);
	const Vector4&	GetTranslation() const;

	void			SetRotateY(float angle);
	void			SetRotateX(float angle);
	void			SetRotateZ(float angle);
	void			SetRotateAxis(float angle, const Vector4& axis);

	void			RotateY(float angle);
	void			RotateX(float angle);
	void			RotateZ(float angle);
	void			RotateAxis(float angle, const Vector4& axis);

	void			Transform(const Vector4& vector, Vector4* result) const;
	void			Transform3(const Vector4& vector, Vector4* result) const;
	void			TransformRotation3(const Vector4& vector, Vector4* result) const;

	Matrix4			MirrorMatrix(const Matrix4& matrixToMirror) const;
	Matrix4			Multiply(const Matrix4& rhs) const;
	Matrix4			Multiply3(const Matrix4& rhs) const;

	bool			operator==(const Matrix4& rhs) const;
	bool			operator!=(const Matrix4& rhs) const;
	Matrix4			operator*(const Matrix4& rhs) const;
	Matrix4			operator*(const float rhs) const;
	void			operator=(const Matrix4& rhs);
	Vector4			operator*(const Vector4& vector) const;
	float&			operator[](const int index);
	const float&	operator[](const int index) const;
	Matrix4			operator-() const;

	//
	// This will return a 3x3 sub matrix of this matrix,
	// it defaults to return the roational part of the matrix
	//
	Matrix3			GetMatrix3(int row = 3, int col = 3) const;

	void			SetRotationIdentity();
	void			SetRotationZero();
	void			SetTranslationZero();
	void			CreateView(const Matrix4& world);
	void			CreateLookAt(const Vector4& pos, const Vector4& target);

	void			QuickInverse();
	Matrix4			GetQuickInverse() const;

	void			Transpose();
	Matrix4			GetTranspose() const;

	void			Inverse();
	Matrix4			GetInverse() const;

	float			GetOneNorm() const;
	float			GetInfiniteNorm() const;

	float			GetDeterminant() const;
	Matrix4			GetAdjoint() const;

	const Vector4&	GetXAxis() const									{ return Column(0); }
	const Vector4&	GetYAxis() const									{ return Column(1); }
	const Vector4&	GetZAxis() const									{ return Column(2); }

	const Vector4&	Column(int idx) const;
	Vector4&		Column(int idx);

	void	SetColumn(int idx, const Vector4& vec);
	void	SetXAxis(const Vector4& vec)								{ SetColumn(0, vec); }
	void	SetYAxis(const Vector4& vec)								{ SetColumn(1, vec); }
	void	SetZAxis(const Vector4& vec)								{ SetColumn(2, vec); }

	Vector4	GetRow(int idx) const;
	void	SetRow(int idx, const Vector4& vec);

	union
	{
		float	mat[4][4];
		float	m[16];
	};
};

#include "Matrix.inl"

#endif
