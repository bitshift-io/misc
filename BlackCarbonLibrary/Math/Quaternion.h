#ifndef _QUATERNION_H_
#define _QUATERNION_H_

#include "Math.h"
#include "Vector.h"
#include "Matrix.h"

enum QuaternionInit
{
	QI_Identity,
};

ALIGN16 class Quaternion
{
public:

	Quaternion()
	{
	}

	Quaternion(QuaternionInit type)
	{
		switch (type)
		{
		case QI_Identity:
			SetIdentity();
			break;
		}
	}

	Quaternion(float x, float y, float z, float w)
	{
		q[0] = x;
		q[1] = y;
		q[2] = z;
		q[3] = w;
	}

	Quaternion(const float* data)
	{
		q[0] = data[0];
		q[1] = data[1];
		q[2] = data[2];
		q[3] = data[3];
	}

	Quaternion(const Matrix4& mat)
	{
		CreateFromMatrix(mat);
	}

	Quaternion(const Vector4& axis, const float angle)
	{
		CreateFromAxisAngle(axis, angle);
	}

	Quaternion GetConjugate() const
	{
		Quaternion q;
		q.x = -x;
		q.y = -y;
		q.z = -z;
		q.w = w;
		return q;
	}

	Quaternion GetInverse() const
	{
		return GetConjugate();
	}

	void Inverse()
	{
		*this = GetConjugate();
	}

	Vector4 operator*(const Vector4& t) const
	{
		Quaternion inverse = GetInverse();
		Quaternion result = (*this * Quaternion(t.x, t.y, t.z, 1.f));
		result = result * inverse;
		return Vector4(result.x, result.y, result.z);
	}

	void CreateFromMatrix(const Matrix4& mat);
	void CreateFromAxisAngle(const Vector4& axis, const float angle);

	void CreateFromEuler(float x, float y, float z)
	{
		// http://www.flipcode.com/documents/matrfaq.html#Q47
		Vector4 right(1, 0, 0);
		Vector4 up(0, 1, 0);
		Vector4 view(0, 0, 1);
		Quaternion qx, qy, qz, qt;
		qx.CreateFromAxisAngle(right, x);
		qy.CreateFromAxisAngle(up, y);
		qz.CreateFromAxisAngle(view, z);

		qt = qx * qy;
		*this = qt * qz;		
	}

	// you should normalize before caling this?
	void GetAxisAngle(Vector4& axis, float& angle);

	Quaternion operator*(const Quaternion& rhs) const
	{
		Quaternion ret;

		ret.w = (w * rhs.w) - (x * rhs.x) - (y * rhs.y) - (z * rhs.z);
		ret.x = (w * rhs.x) + (x * rhs.w) + (y * rhs.z) - (z * rhs.y);
		ret.y = (w * rhs.y) + (y * rhs.w) + (z * rhs.x) - (x * rhs.z);
		ret.z = (w * rhs.z) + (z * rhs.w) + (x * rhs.y) - (y * rhs.x);

		return ret;
	}

	Quaternion operator*(const float& rhs)
	{
		Quaternion ret;

		for (int i = 0; i < 4; ++i)
			ret[i] = q[i] * rhs;

		return ret;
	}

	void operator=(const Quaternion& rhs)
	{
		for( int i = 0; i < 4; ++i )
			q[i] = rhs.q[i];
	}

	inline float& operator[](int index)
	{
		return q[index];
	}

	Quaternion operator+(const Quaternion& rhs)
	{
		return Quaternion(q[0] + rhs.q[0], q[1] + rhs.q[1], q[2] + rhs.q[2], q[3] + rhs.q[3]);
	}

	Quaternion operator-(const Quaternion& rhs)
	{
		return Quaternion(q[0] - rhs.q[0], q[1] - rhs.q[1], q[2] - rhs.q[2], q[3] - rhs.q[3]);
	}

	Matrix4 GetRotationMatrix()
	{
		Matrix4 m(MI_Identity);
		float y2 = y * y;
		float x2 = x * x;
		float z2 = z * z;

		m[0] = 1.0f - (2 * y2) - (2 * z2);
		m[1] = (2.0f * x * y) + (2 * w * z);
		m[2] = (2.0f * x * z) - (2 * w * y);

		m[4] = (2.0f * x * y) - (2 * w * z);
		m[5] = 1.0f - (2.0f * x2) - (2 * z2);
		m[6] = (2.0f * y * z) + (2 * w * x);

		m[8] = (2.0f * x * z) + (2 * w * y);
		m[9] = (2.0f * y * z) - (2 * w * x);
		m[10]= 1.0f - (2.0f * x2) - (2 * y2);

		return m;
	}

	float Magnitude() const;

	void Normalize()
	{
		float mag = Magnitude();
		q[0] /= mag;
		q[1] /= mag;
		q[2] /= mag;
		q[3] /= mag;
	}

	float Dot(const Quaternion& rhs) const
	{
		float dot = 0;

		for( int i = 0; i < 4; ++i )
			dot += q[i] * rhs.q[i];

		return dot;
	}

	float AngleBetween(const Quaternion& rhs) const;

	Quaternion Interpolate(const Quaternion& to, float weight) const;

	void SetIdentity()
	{
		x = y = z = 0.f;
		w = 1.f;
	}

	union
	{
		float q[4];

		struct
		{
			float x;
			float y;
			float z;
			float w;
		};
	};
};

#endif
