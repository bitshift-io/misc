/*
inline Line::Line()
{
}

inline Line::Line(const Vector4& _point, const Vector4& _direction) :
	point(_point),
	direction(_direction)
{
}

inline Line::Line(const Vector4& _point, const Vector4& _point2) :
	point(_point)
{
	direction = _point2 - _point;
	distance = direction.Normalize3();
}

inline Vector4 Line::ClosetPoint(const Vector4& _point)
{
	return direction.Project3(_point - point);
}

inline float Line::DistanceToPoint(const Vector4& _point)
{
	return (ClosetPoint(_point) - _point).Mag3();;
}

inline bool Line::Intersect(const Line& other)
{
	Vector4 temp = (point - other.point) / (other.direction - direction);
	return false;
}
*/
