#include "Sphere.h"

Sphere::Sphere()
{
}

Sphere::Sphere(const Vector4& _position, float _radius) :
	position(_position),
	radius(_radius)
{
}

float Sphere::Intersect(const Sphere& other) const
{
	Vector4 diff = other.position - position;
	float totalRange = radius + other.radius;
	totalRange *= totalRange;

	float diffSize = (diff.x * diff.x) + (diff.y * diff.y) +
		(diff.z * diff.z);

	if (diffSize < totalRange)
		return sqrtf(diffSize);

	return -1;
}

void Sphere::CreateFromTri(const Vector4& p0, const Vector4& p1, const Vector4& p2)
{
	// average position
	position = (p0 + p1 + p2) / 3.f;

	// large dist from average pos is the radius
	float p0Dist = (position - p0).MagSquared();
	float p1Dist = (position - p1).MagSquared();
	float p2Dist = (position - p2).MagSquared();
	radius = sqrtf(Math::Max(p0Dist, Math::Max(p1Dist, p2Dist)));
}
