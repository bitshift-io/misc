#ifndef _INTVECTOR_H_
#define _INTVECTOR_H_

#include "Math.h"
/*
//=========================================================================
// IntVector4
//=========================================================================
class IntVector4
{
public:

	IntVector4();

	// convert float vector to fixed point vector
	IntVector4(const Vector4& vec, int numDecimalPlaces);

	// convert from fixed point to float point
	Vector4 AsVector4(int numDecimalPlaces);

	union
	{
		struct
		{
			int u;
			int v;
		};

		struct
		{
			int x;
			int y;
		};

		int vec[2];
	};	
};

//=========================================================================
// IntVector4
//=========================================================================
class IntVector4
{
public:

	union
	{
		struct
		{
			int x;
			int y;
			int z;
		};

		int vec[3];

		struct
		{
			int yaw;
			int pitch;
			int roll;
		};
	};
};

//=========================================================================
// IntVector4
//=========================================================================
class IntVector4
{
public:
	
	union
	{
		struct
		{
			int x;
			int y;
			int z;
			int w;
		};

		int vec[4];
	};
};

#include "IntVector.inl"
*/
#endif
