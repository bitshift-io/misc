#ifndef _VECTOR_H_
#define _VECTOR_H_

#include "Math.h"

enum VectorInit
{
	VI_Zero,
	VI_One,
	VI_Up,
	VI_Down,
	VI_Right,
};

//=========================================================================
// Vector4
//=========================================================================
ALIGN16 class Vector4
{
public:

	Vector4();
	Vector4(float x, float y, float z = 0.f, float w = 1.f);
	Vector4(const float* data);
	Vector4(VectorInit init);

	Vector4			Cross(const Vector4& other) const;
	void			SetZero();
	void			SetOne();

	float			Mag() const;
	float			Mag3() const;
	float			MagSquared() const;
	float			MagSquared3() const;
	float			Normalize();
	float			Normalize3();
	float			Dot(const Vector4& other) const;
	float			Dot3(const Vector4& other) const;
	float			Angle(const Vector4& other) const;

	float			ProjectMag3(const Vector4& other) const;

	// project 'other' on to 'this'
	Vector4			Project3(const Vector4& other) const;

	float			DistanceSquared(const Vector4& other) const;
	float			Distance(const Vector4& other) const;

	bool			operator==(const Vector4& rhs) const;
	bool			operator!=(const Vector4& rhs) const;
	float&			operator[](int index);
	Vector4			operator-() const;
	Vector4			operator*(const float rhs) const;
	Vector4			operator*(const Vector4& rhs) const;
	void			operator*=(const float rhs);
	Vector4			operator*=(const Vector4& rhs);
	Vector4			operator+=(const Vector4& rhs);
	Vector4			operator/(const float rhs);
	Vector4			operator/=(const float rhs);
	Vector4			operator-(const Vector4& rhs) const;
	Vector4			operator+(const Vector4& rhs) const;
	Vector4			operator-=(const Vector4& rhs);
	Vector4			operator/(const Vector4& rhs) const;

	union
	{
		struct
		{
			float x;
			float y;
			float z;
			float w;
		};

		struct
		{
			float yaw;
			float pitch;
			float roll;
			float pad;
		};

		struct
		{
			float u;
			float v;
			float _w;
			//float pad;
		};

		struct
		{
			float r;
			float g;
			float b;
			float a;
		};

		float vec[4];
	};
};

#include "Vector.inl"

#endif
