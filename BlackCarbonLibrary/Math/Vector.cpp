#include "Vector.h"
#include "Math.h"

//=========================================================================
// Vector4
//=========================================================================
float Vector4::Mag() const
{
	return Math::Sqrt(MagSquared());
}

float Vector4::Mag3() const
{
	return Math::Sqrt(MagSquared3());
}

float Vector4::Angle(const Vector4& other) const
{
	return Math::ACos(Dot(other));
}

float Vector4::ProjectMag3(const Vector4& other) const
{
	float lenThis = Mag3();
	float dot = Dot3(other);
	float lengthProjection = dot / lenThis;
	return lengthProjection;
}

Vector4	Vector4::Project3(const Vector4& other) const
{
	Vector4 nThis = *this;
	float lenThis = nThis.Normalize();
	float dot = Dot3(other);
	float lengthProjection = dot / lenThis;
	return nThis * lengthProjection;
}