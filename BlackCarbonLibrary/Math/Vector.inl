
//=========================================================================
// Vector4
//=========================================================================
inline Vector4::Vector4()
{
}

inline Vector4::Vector4(float x, float y, float z, float w) :
	x(x),
	y(y),
	z(z),
	w(w)
{
}

inline Vector4::Vector4(const float* data) : 
	x(data[0]),
	y(data[1]),
	z(data[2]),
	w(data[3])
{

}

inline Vector4::Vector4(VectorInit init)
{
	switch (init)
	{
	case VI_Zero:
		SetZero();
		break;

	case VI_One:
		SetOne();

	case VI_Up:
		*this = Vector4(0.f, 1.f, 0.f);
		break;

	case VI_Down:
		*this = Vector4(0.f, -1.f, 0.f);
		break;

	case VI_Right:
		*this = Vector4(1.f, 0.f, 0.f);
		break;
	}
}

inline bool	Vector4::operator==(const Vector4& rhs) const
{
	return rhs.x == x && rhs.y == y && rhs.z == z && rhs.w == w;
}

inline bool	Vector4::operator!=(const Vector4& rhs) const
{
	return !operator==(rhs);
}

inline float& Vector4::operator[](int index)
{
	return vec[index];
}

inline Vector4 Vector4::operator-() const
{
	return Vector4(-x, -y, -z, -w);
}

inline Vector4 Vector4::operator*(const Vector4& rhs) const
{
	return Vector4(x * rhs.x, y * rhs.y, z * rhs.z, w * rhs.w);
}

inline Vector4 Vector4::operator*(const float rhs) const
{
	return Vector4(x * rhs, y * rhs, z * rhs, w * rhs);
}

inline void	Vector4::operator*=(const float rhs)
{
	x *= rhs;
	y *= rhs;
	z *= rhs;
	w *= rhs;
}

inline Vector4 Vector4::operator*=(const Vector4& rhs)
{
	x *= rhs.x;
	y *= rhs.y;
	z *= rhs.z;
	w *= rhs.w;

	return *this;
}

inline Vector4 Vector4::operator+=(const Vector4& rhs)
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	w += rhs.w;

	return *this;
}

inline Vector4	Vector4::operator/=(const float rhs)
{
	float invRhs = 1.f / rhs;
	x *= invRhs;
	y *= invRhs;
	z *= invRhs;
	w *= invRhs;
	return *this;
}

inline Vector4 Vector4::operator/(const float rhs)
{
	return operator*(1.f / rhs);
}

inline Vector4 Vector4::operator-(const Vector4& rhs) const
{
	return Vector4(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
}

inline Vector4 Vector4::operator+(const Vector4& rhs) const
{
	return Vector4(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w);
}


inline Vector4 Vector4::Cross(const Vector4& other) const
{
	Vector4 vNormal;

	// Calculate the cross product with the non communitive equation
	// this is the determinant of a 3x3 matrix
	vNormal.x = ((other.y * z) - (other.z * y));
	vNormal.y = ((other.z * x) - (other.x * z));
	vNormal.z = ((other.x * y) - (other.y * x));
	vNormal.w = 0.0f;

	return vNormal;
}

inline void	Vector4::SetZero()
{
	x = y = z = w = 0.f;
}

inline void	Vector4::SetOne()
{
	x = y = z = w = 1.f;
}

inline float Vector4::MagSquared() const
{
	return (x * x) + (y * y) + (z * z) + (w * w);
}

inline float Vector4::MagSquared3() const
{
	return (x * x) + (y * y) + (z * z);
}

inline float Vector4::Normalize()
{
	float mag = Mag();
	float invMagnitude = 1.f / mag;
	x *= invMagnitude;
	y *= invMagnitude;
	z *= invMagnitude;
	w *= invMagnitude;

	return mag;
}

inline float Vector4::Normalize3()
{
	float mag = Mag3();
	float invMagnitude = 1.f / mag;
	x *= invMagnitude;
	y *= invMagnitude;
	z *= invMagnitude;

	return mag;
}

inline Vector4 Vector4::operator-=(const Vector4& rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;
	w -= rhs.w;

	return *this;
}

inline Vector4 Vector4::operator/(const Vector4& rhs) const
{
	return Vector4(x / rhs.x, y / rhs.y, z / rhs.z, w / rhs.w);
}

inline float Vector4::Dot(const Vector4& other) const
{
	return (x * other.x) + (y * other.y) + (z * other.z) + (w * other.w);
}

inline float Vector4::Dot3(const Vector4& other) const
{
	return (x * other.x) + (y * other.y) + (z * other.z);
}

inline float Vector4::DistanceSquared(const Vector4& other) const
{
	Vector4 diff = *this - other;
	return diff.MagSquared();
}

inline float Vector4::Distance(const Vector4& other) const
{
	Vector4 diff = *this - other;
	return diff.Mag();
}