#ifndef _MATH_H_
#define _MATH_H_

#include <cstdlib>
#include <math.h>
#include <float.h>

// linux needs -msse compile option
#include <xmmintrin.h>

#ifndef WIN32
	#define ALIGN16  __attribute__((align(16) ))
#else
	#define ALIGN16 __declspec(align(16) )
#endif


class Vector4;

#undef PI

//
// Math helper functions
//
namespace Math
{

	static inline int IntRound(float x)
	{
		// performs a round to integer operation

		int t;
        #ifdef WIN32
            __asm
            {
                fld  x
                fistp t
            }
		#else/*
            asm("fld  %1\n\t"
                "fistp %0"
                : "r="(t)
                : "r"(x));*/
                return (int)x;
		#endif

		return t;
	}

	static inline int IntFloor(float x)
	{
		return IntRound(x);
	}

	static inline int IntCeil(float x)
	{
		return IntRound(x + 1.f);
	}

	static inline float Floor(float x)
	{
		return (float)IntRound(x);
	}

	static inline float Ceil(float x)
	{
		return (float)IntRound(x + 1.f);
	}

	static inline int FloatToFixed(float value, int numDecimalPlaces)
	{
		return Math::IntRound(value * float(1 << numDecimalPlaces));
	}

	static inline float FixedToFloat(int value, int numDecimalPlaces)
	{
		return float(value) / float(1 << numDecimalPlaces);
	}

	static inline float Clamp(float min, float value, float max)
	{
		if (value < min)
			return min;

		if (value > max)
			return max;

		return value;
	}

	static inline float DtoR(float degrees)
	{
		return (float)degrees * 0.01745f; //(PI / 180 )
	}

	static inline float RtoD(float radians)
	{
		return (float)radians * 57.29577f;//( 180 / PI )
	}

	static inline float Rand(float min, float max)
	{
		return min + ((max - min) * ((float)rand() / RAND_MAX));
	}

	template <class T>
	static inline T Max(const T& a, const T& b)
	{
		return a > b ? a : b;
	}

	template <class T>
	static inline T Max(const T& a, const T& b, const T& c)
	{
		return Max(Max(a, b), c);
	}

	template <class T>
	static inline T Min(const T& a, const T& b)
	{
		return a < b ? a : b;
	}

	template <class T>
	static inline T Min(const T& a, const T& b, const T& c)
	{
		return Min(Min(a, b), c);
	}

	template <class T>
	static inline T Lerp(const T& from, const T& to, float percent)
	{
		return from + (to - from) * percent;
	}

	template <class T>
	static inline void Swap(T& x, T& y)
	{
		T temp = x;
		x = y;
		y = temp;
	}

	template <class T>
	static inline T Abs(const T& x)
	{
		return x > 0 ? x : -x;
	}

	float Noise1(float arg);
	float Noise2(const Vector4& vec);
	float Noise3(const Vector4& vec);

	static inline float Cos(float theta)
	{
		return cosf(theta);
	}

	static inline float ACos(float theta)
	{
		return acosf(theta);
	}

	static inline float Sqrt(float theta)
	{
		return sqrtf(theta);
	}

	extern const float			Epsilon;
	extern const float			PI;
	extern const int			IntMax;
	extern const unsigned int	UIntMax;
}




#endif
