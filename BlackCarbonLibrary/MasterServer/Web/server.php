<?php

	// add or remove an ip address from a file
	$operation	= $_GET["operation"];
	$name		= $_GET["name"];
	$players	= $_GET["players"];
	$ip			= getenv('REMOTE_ADDR');
	$port		= $_SERVER['REMOTE_PORT'];
	$filename	= "servers.txt";
	 
	// only read data if it has data in it
	if (filesize($filename))
	{
		$fh = fopen($filename, "r");
		$data = fread($fh, filesize($filename));
		$array = explode("\n", $data);	 
		fclose($fh);
	}
	else
	{
		$array = array();
	}
		
	if ($operation == "add")
	{
		$time = time();
		$serverInfoString = $ip . /*":" . $port .*/ " " . $time . " " . $players . " \"" . $name . "\"";
		
		for ($i = 0; $i < count($array); ++$i)
		{
			$serverInfo = $array[$i];
			$serverInfoArray = explode(" ", $serverInfo);
			if ($serverInfoArray[0] == $ip)
				unset($array[$i]);

			// 2 hour time out
			if (($serverInfoArray[3] + (2 * 60 * 60 * 1000)) < $time)
			{
				unset($array[$i]);
				echo "<b>$serverInfoArray[0] has been removed to the server list due to timeout</b><br>";
			}
		}
		
		array_push($array, $serverInfoString);
		$data = implode("\n", $array);
		
		$fh = fopen($filename, "w");
		fwrite($fh, $data);
		fclose($fh);
		
		echo "<b>$ip:$port has been added to the server list</b><br>";	
		
		$data = implode("<br>", $array);
		echo "<br>$data";
	}	
	
	if ($operation == "remove")
	{
		$time = time();
		$serverInfoString = $ip . /*":" . $port .*/ " " . $time . " " . $players . " \"" . $name . "\"";
		
		for ($i = 0; $i < count($array); ++$i)
		{
			$serverInfo = $array[$i];
			$serverInfoArray = explode(" ", $serverInfo);
			if ($serverInfoArray[0] == $ip/*($ip . ":" . $port)*/)
			{
				unset($array[$i]);
				echo "<b>$ip has been removed from the server list</b><br>";
			}
				
			// 2 hour time out
			if ($time > ($serverInfoArray[3] + (60 * 60 * 1000)))
			{
				unset($array[$i]);
				echo "<b>$serverInfoArray[0] has been removed to the server list due to timeout</b><br>";
			}
		}
		
		$fh = fopen($filename, "w");
		$data = implode("\n", $array);
		fwrite($fh, $data);
		fclose($fh);
		
		$data = implode("<br>", $array);
		echo "<br>$data";
	}
	
	if ($operation == "get")
	{
		for ($i = 0; $i < count($array); ++$i)
		{
			$serverInfo = $array[$i];
			$serverInfoArray = explode(" ", $serverInfo);
			echo "$serverInfoArray[0] $serverInfoArray[2] ";
			
			for ($j = 3; $j < count($serverInfoArray); ++$j) 
				echo "$serverInfoArray[$j] ";
				
			echo "\r\n";
		}
	}
?>