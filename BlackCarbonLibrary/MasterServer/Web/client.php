<?php

	$operation	= $_GET["operation"];
	$ip			= getenv('REMOTE_ADDR');
	$port		= $_SERVER['REMOTE_PORT'];
	$serverip	= $_GET["ip"];
	$filename	= "clients.txt";
	
	// only read data if it has data in it
	if (filesize($filename))
	{
		$fh = fopen($filename, "r");
		$data = fread($fh, filesize($filename));
		$array = explode("\n", $data);	 
		fclose($fh);
	}
	else
	{
		$array = array();
	}
		
	if ($operation == "join")
	{
		$time = time();
		$requestedServerArray = explode(":", $serverip);
		$clientInfoString = $ip . " " . $time . " " . $requestedServerArray[0];
		
		for ($i = 0; $i < count($array); ++$i)
		{
			$clientInfo = $array[$i];
			$clientInfoArray = explode(" ", $clientInfo);
			
			if ($clientInfoArray[0] == $ip)
				unset($array[$i]);	
		}
		
		array_push($array, $clientInfoString);
		$data = implode("\n", $array);
		
		$fh = fopen($filename, "w");
		fwrite($fh, $data);
		fclose($fh);
	
		echo "<b>$ip:$port has requested to join server $serverip</b><br>";	
		$data = implode("<br>", $array);
		echo "$data";
	}
	
	if ($operation == "get")
	{
		$time = time();
		
		for ($i = 0; $i < count($array); ++$i)
		{
			$clientInfo = $array[$i];
			$clientInfoArray = explode(" ", $clientInfo);
			
			if ($clientInfoArray[2] == $ip)
			{
				unset($array[$i]);	
				echo "$clientInfoArray[0]";				
			}
			
			// 60 second timeout
			if ($time > $clientInfoArray[1] + (60 * 1000))
				unset($array[$i]);
				
			echo "\r\n";
		}
		
		$data = implode("\n", $array);
		
		$fh = fopen($filename, "w");
		fwrite($fh, $data);
		fclose($fh);
	}
	
	// for debugging
	if ($operation == "getall")
	{		
		for ($i = 0; $i < count($array); ++$i)
		{
			$clientInfo = $array[$i];
			$clientInfoArray = explode(" ", $clientInfo);
			
			echo "$clientInfoArray[0]";			
			echo "\r\n";
		}
	}
	
?>