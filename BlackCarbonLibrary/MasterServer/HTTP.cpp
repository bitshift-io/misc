#include "HTTP.h"
#include "Network/NetworkFactory.h"
#include "Network/TCPStream.h"
#include "Network/Packet.h"
#include "File/Log.h"

void HTTP::Init(NetworkFactory* factory, const char* host, int port)
{
	mFactory = factory;
	mHost = host;
	mPort = port;

	mStream = mFactory->CreateTCPStream();
}

void HTTP::Deinit()
{
	mFactory->Release(&mStream);
}

bool HTTP::Connect()
{
	if (!mStream->Connect(mFactory->GetAddress(mHost.c_str(), mPort)))
		return false;

	return true;
}

void HTTP::Close()
{
	mStream->Close();
}


bool HTTP::Get(const char* url, Packet* page)
{
	if (!Connect())
		return false;

	Packet head;

	char httpGetRequest[2048];
	//if (mContinue)
	//	sprintf(httpGetRequest, "HTTP/1.1 100 Continue\n\nGET /%s HTTP/1.1\nHost: %s:%i\n\n", url, mHost.c_str(), mPort);
	//else
		sprintf(httpGetRequest, "GET /%s HTTP/1.1\nHost: %s:%i\nConnection: Close\n\n", url, mHost.c_str(), mPort);

	//Log::Print(httpGetRequest);

	head.Append(httpGetRequest, strlen(httpGetRequest));
	if (mStream->Send(&head) == -1)
	{
		Close();
		return false;
	}

	while ((mStream->GetState() & eRead) != eRead)
	{
	}

	if (mStream->Receive(page) == -1)
	{
		Close();
		return false;
	}

	if (page->GetSize() <= 0)
	{
		Close();
		return false;
	}

	//Log::Print(page->GetBuffer());

	// strip off the http header
	char buffer;
	bool found = false;
	while (!found)
	{
		do 
		{
			page->Read(&buffer, sizeof(char));
		} while (buffer != '\r' && buffer != '\n');

		if (buffer == '\r')
		{
			page->Read(&buffer, sizeof(char));
			if (buffer == '\n')
			{
				page->Read(&buffer, sizeof(char));
				if (buffer == '\r')
				{
					page->Read(&buffer, sizeof(char));
					if (buffer == '\n')
						found = true;
				}
			}
		}
	}

	// read the size
	char contentSize[256];
	page->ReadLine(contentSize, 256);

	// clear the zero of the end (stupid http 1.1)
	page->SetSize(page->GetSize() - 2);

	Close();
	return true;
}

void HTTP::Post(const char* url, Packet* page)
{
}
