#include "MasterServer.h"
#include "Network/NetworkStream.h"
#include "Network/Packet.h"
#include "Network/TCPStream.h"
#include "Network/UDPStream.h"
#include "File/ScriptFile.h"
//#include "Server.h"

#ifndef WIN32
    #include <stdarg.h>

	#define strcmpi strcasecmp
#endif

//template <>
//Packet& operator &/*<ServerInfo>*/(Packet& packet, ServerInfo& data)

template <>
Packet& operator &<ServerInfo>(Packet& packet, ServerInfo& data)
{
	packet & data.mPort;
	packet & data.mServerName;
	packet & data.mIP;
	packet & data.mNumPlayers;
	packet & data.mGameMode;
	packet & data.mMapName;
	packet & data.mServerHandle;
	packet & data.mVersion;
	packet & data.mGameName;

    return packet;
}

/*
Packet& operator &(Packet& packet, ServerInfo& data)
{
	packet & data.mServerName;
	packet & data.mIP;
	packet & data.mPort;
	packet & data.mNumPlayers;
	packet & data.mGameMode;
	packet & data.mMapName;
	packet & data.mServerHandle;
	packet & data.mVersion;
	packet & data.mGameName;

    return packet;
}
*/
bool MasterServer::Init(NetworkFactory* factory, const string& gameName, const string& serverPath, int port)
{
	mFactory = factory;
	mGameName = gameName;
	mCompleteServerPath = serverPath;
	mPort = port;

	int length = serverPath.length();
	int lastOffset = 0;
	int offset = 0;
	while ((offset = serverPath.find("/", offset)) != string::npos)
	{
		string serverURL = serverPath.substr(lastOffset, offset - lastOffset);

		if (serverURL.length() > 0)
		{
			TCPStream stream;
			if (stream.Connect(mFactory->GetAddress(serverURL.c_str(), mPort)))
			{
				mServerURL = serverURL;
				mServerPath = serverPath.substr(offset, length - offset);
				stream.Close();
				break;
			}
		}

		++offset;
		lastOffset = offset;
	}

	mHTTP.Init(factory, mServerURL.c_str(), mPort);
	return true;
}

void MasterServer::Deinit()
{
	mHTTP.Deinit();
}

bool MasterServer::HttpGet(Packet* packet, const char* text, ...)
{
	va_list ap;

	char newText[2048] = "";

	va_start(ap, text);
	vprintf(text, ap);
	vsprintf(newText, text, ap);
	va_end(ap);

	string strNewText = newText;

	// fix spaces to internet friendly
	for (int i = 0; i < strlen(newText); ++i)
	{
		if (strNewText[i] == ' ')
		{
			strNewText.erase(i, 1);
			strNewText.insert(i, "%20");
		}
	}

	strNewText = mServerPath + strNewText;
	return mHTTP.Get(strNewText.c_str(), packet);
}

int MasterServer::ReadLine(Packet* packet, string& line)
{
	char buffer;
	int offset = 0;
	while (packet->Peek(&buffer, sizeof(char), offset) != 0)
	{
		if (buffer == '\r')
		{
			packet->Peek(&buffer, sizeof(char), offset + 1);
			if (buffer == '\n')
			{
				char buf[2048];
				buf[packet->Read(buf, offset)] = '\0';
				line = buf;

				packet->Read(buf, sizeof(char) * 2);
				return offset;
			}
		}

		++offset;
	}

	char buf[2048];
	buf[packet->Read(buf, offset)] = '\0';
	line = buf;

	return offset;
}

void MasterServer::GetInternetServerList(vector<ServerInfo>& server)
{
	Packet page;
	if (!HttpGet(&page, "?action=getservers&modname=&gamename=%s&version=1.0", mGameName.c_str()))
		return;


	//char line[256];
	do
	{
		string line;
		if (ReadLine(&page, line) <= 0)
			continue;

		//if (page.ReadLine(line, 256) <= 0)
		//	return;

		ServerInfo info;
		if (ReadServerInfo(info, line))
			server.push_back(info);

	} while (!page.EndOfStream());
}

bool MasterServer::JoinServer(unsigned int serverHandle, int clientPort)
{
	Packet page;
	if (!HttpGet(&page, "?action=clientjoinserver&serverid=%i&port=%i", serverHandle, clientPort))
		return false;

	return true;
}

bool MasterServer::GetJoinClientList(unsigned int serverHandle, vector<ClientInfo>& client)
{
	Packet page;
	if (!HttpGet(&page, "?action=getjoiningclients&serverid=%i", serverHandle))
		return false;

	string line;
	do
	{
		if (ReadLine(&page, line) <= 0)
			continue;

		ClientInfo info;
		if (ReadClientInfo(info, line))
			client.push_back(info);

	} while (!page.EndOfStream());

	return true;
}

bool MasterServer::ReadClientInfo(ClientInfo& info, const string& line)
{
	vector<string> token = ScriptFile::Tokenize(line, "|", false);
	if (token.size() != 2)
		return false;

	info.mIP = token[0];
	sscanf(token[1].c_str(), "%i", &info.mPort);

	return true;
}

bool MasterServer::ReadServerInfo(ServerInfo& info, const string& line)
{
	vector<string> token = ScriptFile::TokenizeSpecial(line, "|", false);
	if (token.size() != 5)
		return false;

	info.mServerName = token[0];
	info.mGameMode = token[1];
	info.mIP = token[2];
	sscanf(token[3].c_str(), "%i", &info.mPort);
	sscanf(token[4].c_str(), "%i", &info.mServerHandle);

	return true;
}

unsigned int MasterServer::RegisterServer(const string& serverName, int port, const string& mapName, const string& gameMode)
{
	Packet page;
	if (!HttpGet(&page, "?action=registerserver&servername=%s&modname=&gamename=%s&version=1.0&mapname=%s&gamemode=%s&port=%i", serverName.c_str(), mGameName.c_str(), mapName.c_str(), gameMode.c_str(), port))
		return -1;

	char buffer[256];
	buffer[page.ReadLine(buffer, 256)] = '\0';

	unsigned int serverId = -1;
	sscanf(buffer, "%i", &serverId);
	return serverId;
}

unsigned int MasterServer::UpdateServer(unsigned int serverHandle, const string& serverName, int port, const string& mapName, const string& gameMode)
{
	Packet page;
	HttpGet(&page, "?action=updateserver&serverid=%i&servername=%s&modname=&gamename=%s&version=1.0&mapname=%s&gamemode=%s&port=%i", serverHandle, serverName.c_str(), mGameName.c_str(), mapName.c_str(), gameMode.c_str(), port);

	char buffer[256];
	buffer[page.Read(buffer, 256)] = '\0';

	unsigned int serverId = serverHandle;
	sscanf(buffer, "%i", &serverId);
	return serverId;
}

void MasterServer::RemoveServer(unsigned int serverHandle)
{
	Packet page;
	HttpGet(&page, "?action=removeserver&serverid=%i", serverHandle);
}

void MasterServer::GetLANServerList(vector<ServerInfo>& server)
{
	UDPStream udpStream;
	udpStream.Bind(mFactory->GetBroadcastAddress(6466));

	// broadcast info just to spam around the info... this could be bad
	// if multiple LAN Servers, may just end up spamming each other!!!
	Packet broadcast;
	string dummy = "test";
	broadcast.Append(dummy.c_str(), dummy.length());

	for (int i = 0; i < 100; ++i)
	{
		udpStream.Send(mFactory->GetBroadcastAddress(6465), &broadcast);
	}

	while (udpStream.GetState() & eRead)
	{
		Packet packet;
		udpStream.Receive(&packet);

		ServerInfo serverInfo;
		packet & serverInfo;

		unsigned int port = 0;
		mFactory->ConvertToString(packet.GetSockAddress(), serverInfo.mIP, port);

		bool found = false;
		vector<ServerInfo>::iterator it;
		for (it = server.begin(); it != server.end(); ++it)
		{
			if (strcmpi(it ->mIP.c_str(), serverInfo.mIP.c_str()) == 0)
			{
				found = true;
				break;
			}
		}

		if (!found)
			server.push_back(serverInfo);
	}

	udpStream.Close();
}
