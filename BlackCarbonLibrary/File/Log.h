#ifndef _LOG_H
#define _LOG_H

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#ifdef WIN32
	#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
	#define NOMINMAX
	//#define UNICODE
	#include <windows.h>
	#undef GetObject
	#undef SendMessage
	#undef CreateWindow
#endif

#ifdef RELEASE
	#define PRINT (void)0
	#define ERROR (void)0
	#define WARN (void)0
#else
	#define PRINT Log::Print
	#define ERROR Log::Print
	#define WARN Log::Print
#endif

class Log
{
public:

	static bool SetLog(const char* file)
	{
		if (pLog)
			Close();

		return (pLog = fopen(file, "w")) != 0;
	}

	static void Close()
	{
		fclose(pLog);
		pLog = 0;
	}

	static void Print(const char* text, ... );
	static void Warning(const char* text, ... );
	static void Error(const char* text, ... );
	static void Success(const char* text, ... );

protected:

	static void CheckLog();

	static FILE* pLog;
};

#endif
