#include "Directory.h"


DirectoryIterator::DirectoryIterator(Directory* dir) :
	 mDirectory(dir),
	 mDir(0),
	 mEntry(0),
	 mCleanUp(false)
{
	if (dir)
	{
		mDir = opendir(dir->mName.c_str());
		operator++();
	}
}

DirectoryIterator::DirectoryIterator() :
	 mDirectory(0),
	 mDir(0),
	 mEntry(0),
	 mCleanUp(true)
{
}

DirectoryIterator::~DirectoryIterator()
{
	if (mDir && mCleanUp)
		closedir(mDir);
}

DirectoryEntry*	DirectoryIterator::operator->()
{
	return &mDirEntry;
}

bool DirectoryIterator::operator!=(const DirectoryIterator& rhs) const
{
	return mEntry;
}

void DirectoryIterator::operator++()
{
	mEntry = readdir(mDir);
	if (mEntry)
	{
		mDirEntry.mEntryName = mEntry->d_name;
		string directory = mDirectory->mName + "/" + string(mEntry->d_name);
		Directory test(directory.c_str());
		mDirEntry.mType = test.Valid() ? DET_Directory : DET_File;
	}
}




Directory::Directory() : mDir(0)
{
}

Directory::Directory(const char* directory) : mDir(0)
{
	Open(directory);
}

Directory::~Directory()
{
	Close();
}

bool Directory::Open(const char* directory)
{
	if (!directory || mDir)
		return false;

	mName = directory;
	mDir = opendir(directory);
	return Valid();
}

void Directory::Close()
{
	if (mDir)
		closedir(mDir);
}




