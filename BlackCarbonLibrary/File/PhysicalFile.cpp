#include "Memory/NoMemory.h"

#include <stdio.h>
#include <stdarg.h>
#include <stdarg.h>
#include <memory.h>
#include "PhysicalFile.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

//-----------------------------------------------------------------

#ifdef WIN32
int vfscanf(FILE* file, const char *format, va_list argPtr)
{
	// http://www.codeguru.com/Cpp/Cpp/string/comments.php/c5631/?thread=61724

	// Get an upper bound for the # of args
	size_t count = 0;
	const char* p = format;

	while(1)
	{
		char c = *(p++);
		if (c == 0)
			break;

		if (c == '%' && (p[0] != '*' && p[0] != '%'))
			++count;
	}

	if (count <= 0)
		return 0;

	int result;

	// copy stack pointer
	_asm
	{
		mov esi, esp;
	}

	// push variable parameters pointers on stack
	for (int i = count - 1; i >= 0; --i)
	{
		_asm
		{
			mov eax, dword ptr[i];
			mov ecx, dword ptr [argPtr];
			mov edx, dword ptr [ecx+eax*4];
			push edx;
		}
	}

	int stackAdvance = (2 + count) * 4;

	_asm
	{
		// now push on the fixed params
		mov eax, dword ptr [format];
		push eax;
		mov eax, dword ptr [file];
		push eax;

		// call fscanf, and more the result in to result
		call dword ptr [fscanf];
		mov result, eax;

		// restore stack pointer
		mov eax, dword ptr[stackAdvance];
		add esp, eax;
		//mov esp, esi;
	}
	return result;
}
#endif

//-------------------------------------------------------------------

PhysicalFile::PhysicalFile() : file(0)//, buffer(1024)
{
}

PhysicalFile::PhysicalFile(const char* file, int access) : file(0)
{
	Open(file, access);
}

PhysicalFile::~PhysicalFile()
{
	Close();
}

void PhysicalFile::Close()
{
	if (file)
	{
		fclose(file);
		file = 0;
	}

	name.clear();
	std::string(name).swap(name);
}



bool PhysicalFile::Open(const char* file, int access)
{
	if (!file)
		return false;

    string actualFile = file;
	//if (!FindFileInDirectory(file, GetCurrentDirectory(), actualFile))
	//	return false;

    name = std::string(file);

#ifndef WIN32
    // on linux we are case snesitive, which we do not want!
    // so, we can cheat a little by scanning through the directory and
    // finding the file manually :)
    using namespace boost::filesystem;
    string fullPath = current_path().string() + string("/") + file;
    path ph = path(fullPath);
    string filename = ph.leaf();
    ph = path(fullPath.substr(0, fullPath.length() - (filename.length() + 1)));
    string dir = ph.native_directory_string();

    if (exists(ph))
    {
        directory_iterator endIt;
        directory_iterator it(ph);
        for (; it != endIt; ++it)
        {
            if (is_directory(*it))
                continue;

            string curFilename = it->leaf();
            if (strcmpi(curFilename.c_str(), filename.c_str()) == 0)
            {
                // take in to account that file path passed in *may* contain directory
                unsigned int leafLen = it->leaf().length();
                unsigned int nameLen = name.length();

                actualFile = it->leaf();

                if (leafLen != nameLen)
                    actualFile = name.substr(0, nameLen - leafLen) + actualFile;

                break;
            }
        }
    }

#endif

	int idx = 0;
	char accessStr[10];
	memset(accessStr, 0, 10);
	if (access & FAT_Read)
	{
		accessStr[idx] = 'r';
		++idx;
	}

	if (access & FAT_Write)
	{
		accessStr[idx] = 'w';
		++idx;
	}

	if (access & FAT_Binary)
	{
		accessStr[idx] = 'b';
		++idx;
	}

	if (access & FAT_Text)
	{
		accessStr[idx] = 't';
		++idx;
	}

	this->file = fopen(actualFile.c_str(), accessStr);
	return this->file ? true : false;
}

bool PhysicalFile::EndOfFile() const
{
	return feof(file) ? true : false;
}

bool PhysicalFile::Valid() const
{
	return file ? true : false;
}

unsigned int PhysicalFile::Write(const void* data, unsigned int size)
{
	return fwrite(data, 1, size, file);
}

unsigned int PhysicalFile::Read(void* data, unsigned int size)
{
	return fread(data, 1, size, file);
}

unsigned int PhysicalFile::WriteString(const char* str, ...)
{
	va_list ap;
	char newText[1024];
  	va_start(ap, str);
  	unsigned int count = vsprintf(newText, str, ap);
  	va_end(ap);

	Write(newText, strlen(newText));

	return count;
}

unsigned int PhysicalFile::ReadString(const char* text, ...)
{
 	if( text == 0 )
    	return 0;

	va_list ap;
  	va_start(ap, text);
	int result = vfscanf(file, text, ap);
  	va_end(ap);

	return result;
}

unsigned int PhysicalFile::WriteString(const char* str, va_list ap)
{
	char newText[1024];
	unsigned int count = vsprintf(newText, str, ap);
	Write(newText, strlen(newText));
	return count;
}

unsigned int PhysicalFile::ReadString(const char* str, va_list ap)
{
	return vfscanf(file, str, ap);
}

unsigned int PhysicalFile::Seek(long offset, FileSeekType type)
{
	int t = SEEK_SET;
	switch (type)
	{
	case FST_Begin:
		t = SEEK_SET;
		break;

	case FST_Current:
		t = SEEK_CUR;
		break;

	case FST_End:
		t = SEEK_END;
		break;
	}

	return fseek(file, offset, t);
}

unsigned int PhysicalFile::GetPosition() const
{
	return ftell(file);
}

char* PhysicalFile::ReadLine(char* buffer, unsigned int size)
{
	char* ptr = fgets(buffer, size, file);

	// strip of bad characters
	for (int i = strlen(buffer) - 1; i >= 0; --i)
	{
		if (buffer[i] == '\r' || buffer[i] == '\n')
			buffer[i] = '\0';
		else
			break;
	}
	return ptr;
}

unsigned int PhysicalFile::Size()
{
	unsigned int curPos = GetPosition();
	Seek(curPos, FST_End);
	unsigned int size = GetPosition();
	Seek(curPos, FST_Begin);
	return size;
}

const char* PhysicalFile::GetName()
{
	return name.c_str();
}
