#ifndef _FILEMGR_H_
#define _FILEMGR_H_

#include "Template/Singleton.h"

enum FileAccess
{
	eRead		= 0x1 << 0,
	eWrite		= 0x1 << 1,
	eAppend		= 0x1 << 2,
	eBinary		= 0x1 << 3,
	eAtEnd		= 0x1 << 4,
	eTruncate	= 0x1 << 5,
};



/*
//
// Factory methods
// for handling file streams
//
class FileMgr : public Singleton<FileMgr>
{
public:



	FileStream&			OpenFile(const char* file, int access);
	void				CloseFile(FileStream* file);

protected:
	
};
*/
#endif