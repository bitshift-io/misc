#include "Memory/NoMemory.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdarg.h>
#include <memory.h>
#include <malloc.h>
#include "Log.h"
#include "MemoryFile.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

MemoryFile::MemoryFile() : mContent(0), mCursor(0)
{
}

MemoryFile::MemoryFile(const char* file, int access) : mContent(0), mCursor(0)
{
	Open(file, access);
}

MemoryFile::MemoryFile(MemoryFileContent* content) : mContent(0), mCursor(0)
{
	Open(content);
}

MemoryFile::~MemoryFile()
{
	Close();
}

void MemoryFile::Close()
{
	mContent = 0;
	mCursor = 0;
}

bool MemoryFile::Open(MemoryFileContent* content)
{
	if (!content)
		return false;

	mContent = content;
	return true;
}

bool MemoryFile::Open(const char* file, int access)
{
	if (!file)
		return false;

    // now to find out what package this is in
	FileTableEntry* fileTableEntry = gFilesystem.GetFileTableEntry(file);
	if (!fileTableEntry || fileTableEntry->location != FL_Package || (access & FAT_Write))
		return false;

	mContent = fileTableEntry->memoryFileContent;
	mCursor = 0;
	return mContent != 0;
}

bool MemoryFile::EndOfFile() const
{
	return mCursor >= mContent->size;
}

bool MemoryFile::Valid() const
{
	return mContent != 0;
}

unsigned int MemoryFile::Write(const void* data, unsigned int size)
{
	unsigned int writeSize = min(size, mContent->size - mCursor);
	memcpy(mContent->data + mCursor, data, writeSize);
	mCursor += writeSize;
	return writeSize;
}

unsigned int MemoryFile::Read(void* data, unsigned int size)
{
	unsigned int readSize = min(size, mContent->size - mCursor);
	memcpy(data, mContent->data + mCursor, readSize);
	mCursor += readSize;
	return readSize;
}

unsigned int MemoryFile::WriteString(const char* str, ...)
{
	va_list ap;
	char newText[1024];
  	va_start(ap, str);
  	unsigned int count = vsprintf(newText, str, ap);
  	va_end(ap);

	Write(newText, strlen(newText));

	return count;
}

unsigned int MemoryFile::ReadString(const char* text, ...)
{/*
 	if( text == 0 )
    	return 0;

	va_list ap;
  	va_start(ap, text);
	int result = vfscanf(file, text, ap);
  	va_end(ap);

	return result;*/
	Log::Error("[MemoryFile::ReadString] Not yet implemented");
	return 0;
}

unsigned int MemoryFile::WriteString(const char* str, va_list ap)
{
	char newText[1024];
	unsigned int count = vsprintf(newText, str, ap);
	Write(newText, strlen(newText));
	return count;
}

unsigned int MemoryFile::ReadString(const char* str, va_list ap)
{
	//return vfscanf(file, str, ap);
	Log::Error("[MemoryFile::ReadString] Not yet implemented");
	return 0;
}

unsigned int MemoryFile::Seek(long offset, FileSeekType type)
{
	switch (type)
	{
	case FST_Begin:
		mCursor = offset;
		break;

	case FST_Current:
		mCursor += offset;
		break;

	case FST_End:
		mCursor = mContent->size + offset;
		break;
	}

	if (mCursor <= 0)
		mCursor = 0;

	if (mCursor > mContent->size)
		mCursor = mContent->size;

	return 0;
}

unsigned int MemoryFile::GetPosition() const
{
	return mCursor;
}

char* MemoryFile::ReadLine(char* buffer, unsigned int size)
{/*
	char* ptr = fgets(buffer, size, file);

	// strip of bad characters
	for (int i = strlen(buffer) - 1; i >= 0; --i)
	{
		if (buffer[i] == '\r' || buffer[i] == '\n')
			buffer[i] = '\0';
		else
			break;
	}
	return ptr;*/
	Log::Error("[MemoryFile::ReadLine] Not yet implemented");
	return 0;
}

unsigned int MemoryFile::Size()
{
	return mContent->size;
}

const char* MemoryFile::GetName()
{
	return mContent->name.c_str();
}
