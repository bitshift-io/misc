#include "Package.h"
#include "File.h"
#include "Directory.h"
#include "ScriptFile.h"
#include "Log.h"

#define EOF		0x1A

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

struct PackageHeader
{
	unsigned int numFiles;
	unsigned int compressionType;
	unsigned int encryptionType;
};

Package::Package() : mAccess(0)
{
}

bool Package::Open(const char* file, int access)
{
	mName = file;
	mAccess = access;

	if (access & FAT_Read)
	{
		return Read(file);
	}
	else if (access & FAT_Write)
	{
		return true;
	}

	return false;
}

bool Package::Read(const char* file)
{
	File in(file, FAT_Read | FAT_Binary);
	if (!in.Valid())
		return false;

	mName = in.GetName();

	PackageHeader head;
	in.Read(&head, sizeof(PackageHeader));

	mFileTable.resize(head.numFiles);

	// read in the file entrys
	char buffer[2048];
	for (unsigned int i = 0; i < head.numFiles; ++i)
	{
		memset(buffer, 0, 2048);

		PackageFileEntry fileEntry;
		memset(&fileEntry, 0, sizeof(PackageFileEntry));

		// read string
		unsigned int size = 0;
		in.Read(&size, sizeof(unsigned int));
		in.Read(buffer, size);
		fileEntry.filename = string(buffer);

		// read offset
		in.Read(&fileEntry.offset, sizeof(unsigned int));

		// read size
		in.Read(&fileEntry.size, sizeof(unsigned int));

		mFileTable[i] = fileEntry;
	}

	return true;
}

void Package::Close()
{
	if (mAccess & FAT_Write)
	{
		File out(mName.c_str(), FAT_Write | FAT_Binary);

		// write header
		PackageHeader head;
		memset(&head, 0, sizeof(PackageHeader));
		head.numFiles = mFileTable.size();
		out.Write(&head, sizeof(PackageHeader));

		// write TOC
		Iterator it;
		for (it = Begin(); it != End(); ++it)
		{
			// write string name
			unsigned int size = it->filename.length();
			out.Write(&size, sizeof(unsigned int));
			out.Write(it->filename.c_str(), size);

			// store current file offset, as we need to come back here after
			it->offset = out.GetPosition();

			// write offset
			unsigned int offset = -1;
			out.Write(&offset, sizeof(unsigned int));

			// write size
			size = it->size;
			out.Write(&size, sizeof(unsigned int));
		}

		// write data
		for (it = Begin(); it != End(); )
		{
			// store current position
			unsigned int dataPos = out.GetPosition();

			// rewind and write the offset we never new above
			out.Seek(it->offset, FST_Begin);
			out.Write(&dataPos, sizeof(unsigned int));

			// seek back to data write location, and dump the file
			out.Seek(dataPos, FST_Begin);
			char* fileBuffer = new char[it->size];
			it->file->Read(fileBuffer, it->size);
			out.Write(fileBuffer, it->size);
			delete[] fileBuffer;

			// write a terminator, incrase we are reading a string from the file
			char* eofTerminator = "\0\0";
			out.Write(eofTerminator, 2);
			//out.Write(EOF,

			// close ande clean up
			it->file->Close();
			delete it->file;
			it = mFileTable.erase(it);
		}

		out.Close();

		// just incase we get called again
		mAccess = 0;
	}

	// empty file table
	mFileTable.clear();
	vector<PackageFileEntry>(mFileTable).swap(mFileTable);
}

bool Package::AddDirectoryContents(const string& directory, bool bAddSubdirectory, const string& extensionInclusionList)
{
	vector<string> extensionList = ScriptFile::Tokenize(extensionInclusionList, ",", true);
	return AddDirectoryContents(directory, bAddSubdirectory, extensionList);;
}

bool Package::AddDirectoryContents(const string& directory, bool bAddSubdirectory, const vector<string>& extensionList)
{
	Directory dir(directory.c_str());
	Directory::Iterator it;
	for (it = dir.Begin(); it != dir.End(); ++it)
	{
		const string& leafString = it->mEntryName;
		const string& nativeFileString = directory + "/" + leafString;

		if (it->mType == DET_Directory)
		{
			// skip folders starting with a . evil svn! skip subdirectories if requested
			if (!bAddSubdirectory || leafString[0] == '.')
				continue;

			if (!AddDirectoryContents(nativeFileString, bAddSubdirectory, extensionList))
				return false;
		}
		else
		{
			unsigned int extensionIdx = leafString.find_last_of('.') + 1;
			string extension = leafString.substr(extensionIdx, leafString.length() - extensionIdx);

			bool bExtensionAllowed = false;
			vector<string>::const_iterator extensionIt;
			for (extensionIt = extensionList.begin(); extensionIt != extensionList.end(); ++extensionIt)
			{
				if (strcmpi(extensionIt->c_str(), extension.c_str()) == 0)
				{
					bExtensionAllowed = true;
					break;
				}
			}

			if (!bExtensionAllowed)
				continue;

			// check that this filename doesnt already exist, and warn of duplicate filenames!
			if (FindFile(leafString.c_str()))
			{
				Log::Error("[Package::AddDirectoryContents] Trying to add duplicate file '%s' to package '%s'\n", leafString.c_str(), mName.c_str());
				continue;
			}

			PackageFileEntry fileEntry;
			memset(&fileEntry, 0, sizeof(PackageFileEntry));
			fileEntry.filename = leafString;
			fileEntry.file = new File(nativeFileString.c_str(), FAT_Read | FAT_Binary);

			if (!fileEntry.file->Valid())
			{
				delete fileEntry.file;
				continue;
			}

			fileEntry.size = fileEntry.file->Size();
			mFileTable.push_back(fileEntry);
		}
	}

	return true;
}

Package::Iterator Package::Begin()
{
	return mFileTable.begin();
}

Package::Iterator Package::End()
{
	return mFileTable.end();
}

const char* Package::GetName()
{
	return mName.c_str();
}

PackageFileEntry* Package::FindFile(const char* file)
{
	Iterator it;
	for (it = Begin(); it != End(); ++it)
	{
		if (strcmpi(file, it->filename.c_str()) == 0)
			return &(*it);
	}

	return 0;
}
