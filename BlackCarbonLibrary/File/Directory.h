#ifndef _DIRECTORY_H__
#define _DIRECTORY_H__

#ifdef WIN32
	#include "Win32/dirent.h"
	#undef Directory
#endif

#include <string>
#include <vector>

using namespace std;

enum DirectoryEntryType
{
	DET_File,
	DET_Directory,
};

class DirectoryEntry
{
public:

	const char*			mEntryName;
	DirectoryEntryType	mType;
};

class DirectoryIterator
{
	friend class	Directory;

public:

	DirectoryIterator();
	~DirectoryIterator();

	void			operator++();
	bool			operator!=(const DirectoryIterator& rhs) const;
	DirectoryEntry*	operator->();

protected:

	DirectoryIterator(Directory* dir);		

	DirectoryEntry	mDirEntry;
	bool			mCleanUp;
	dirent*			mEntry;
	Directory*		mDirectory;
	DIR*			mDir;
};

class Directory
{
	friend class DirectoryIterator;

public:

	typedef DirectoryIterator Iterator;	

	Directory();
	Directory(const char* directory);
	~Directory();

	bool	Open(const char* directory);
	void	Close();

	bool	Valid()														{ return mDir != 0; }

	Iterator		Begin()												{ return Iterator(this); }
	Iterator		End()												{ return Iterator(0); }

protected:

	string		mName;
	DIR*		mDir;
};

#endif