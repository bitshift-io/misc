#ifndef _SCRIPTCLASS_H_
#define _SCRIPTCLASS_H_

#include <string>
#include <vector>
#include <list>

using namespace std;

class ScriptVariable;

//
// a class
// has the form:
//
// [name : extends | property1 | property2]
// (
// )
//
class ScriptClass
{
public:

	typedef list<string>::const_iterator			PropertyIterator;
	typedef list<ScriptVariable>::const_iterator	VariableIterator;
	typedef list<ScriptClass>::const_iterator		ClassIterator;

	ScriptClass() {}
	ScriptClass(const string& name, const string& extend, const vector<string>& classProperty);

	ScriptClass(const ScriptClass& rhs);

	void AppendVariable(const ScriptVariable& variable) { mVariable.push_back(variable); }

	PropertyIterator	PropertyBegin()	const		{ return mProperty.begin(); }
	PropertyIterator	PropertyEnd() const			{ return mProperty.end(); }

	VariableIterator	VariableBegin()	const		{ return mVariable.begin(); }
	VariableIterator	VariableEnd() const			{ return mVariable.end(); }

	ClassIterator		ClassBegin() const			{ return mClass.begin(); }
	ClassIterator		ClassEnd() const			{ return mClass.end(); }

	const string&		GetExtendsName() const		{ return mExtend; }
	const string&		GetName() const				{ return mName; }

	ScriptClass* AppendClass(const ScriptClass& scriptClass);

	const ScriptClass* GetScriptClass(const string& name) const;

	ScriptVariable* GetScriptVariable(const char* name);

protected:

	string					mName;
	string					mExtend;
	list<string>			mProperty;
	list<ScriptVariable>	mVariable;
	list<ScriptClass>		mClass;
};





#endif
