#include "Memory/NoMemory.h"
#ifdef WIN32
	#include <windows.h>
	#include <direct.h>
#else
	#include <unistd.h>
#endif

#include "Filesystem.h"
#include "Package.h"
#include "MemoryFile.h"
#include "File.h"
#include "Directory.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

Filesystem::Filesystem()
{
	char currentDir[2048];
	mCurrentDirectory = getcwd(currentDir, 2048); //current_path().string();
}

Filesystem::~Filesystem()
{
	Deinit();
}

void Filesystem::Deinit()
{
	// close any open packages
	vector<Package*>::iterator it;
	for (it = mPackage.begin(); it != mPackage.end(); ++it)
	{
		(*it)->Close();
		delete (*it);
	}

	// empty file table
	mFileTable.clear();
	vector<FileTableEntry>(mFileTable).swap(mFileTable);
}

Package* Filesystem::RegisterPackage(const string& package)
{
	Package* p = new Package;
	if (!p->Open(package.c_str(), FAT_Read))
	{
		delete p;
		return 0;
	}

	Package::Iterator fileIt;
	for (fileIt = p->Begin(); fileIt != p->End(); ++fileIt)
	{
		// check for dups, and overwrite
		FileTableEntry*	existingFileTableEntry = GetFileTableEntry(fileIt->filename);
		if (existingFileTableEntry)
		{
			existingFileTableEntry->location = FL_Package;
			existingFileTableEntry->package = p;
		}
		else
		{
			FileTableEntry fileTableEntry;
			fileTableEntry.location = FL_Package;
			fileTableEntry.filename = fileIt->filename;
			fileTableEntry.numTimesLoaded = 0;
			fileTableEntry.package = p;

			mFileTable.push_back(fileTableEntry);
		}
	}

	mPackage.push_back(p);
	return p;
}

bool Filesystem::RegisterDirectoryContents(const string& directory, bool bAddSubdirectory)
{
	Directory dir(directory.c_str());
	Directory::Iterator it;
	for (it = dir.Begin(); it != dir.End(); ++it)
	{
		const string& leafString = it->mEntryName;
		const string& nativeFileString = directory + "/" + leafString;

		if (it->mType == DET_Directory)
		{
			// skip folders starting with a . evil svn! skip subdirectories if requested
			if (!bAddSubdirectory || leafString[0] == '.')
				continue;

			if (!RegisterDirectoryContents(nativeFileString, bAddSubdirectory))
				return false;
		}
		else
		{
			// check for dups, and overwrite
			FileTableEntry*	existingFileTableEntry = GetFileTableEntry(leafString);
			if (existingFileTableEntry)
			{
				existingFileTableEntry->location = FL_Physical;
				existingFileTableEntry->package = 0;
				existingFileTableEntry->path = nativeFileString;
			}
			else
			{
				FileTableEntry fileTableEntry;
				fileTableEntry.location = FL_Physical;
				fileTableEntry.filename = leafString;
				fileTableEntry.numTimesLoaded = 0;
				fileTableEntry.path = nativeFileString;

				mFileTable.push_back(fileTableEntry);
			}
		}
	}

	return true;
}

void Filesystem::SetBaseDirectory(const string& directory)
{
	mCurrentDirectory = directory;
}

string Filesystem::GetBaseDirectory()
{
	return mCurrentDirectory;
	//return current_path().string();
}

FileTableEntry*	Filesystem::GetFileTableEntry(const string& name)
{
	vector<FileTableEntry>::iterator it;
	for (it = mFileTable.begin(); it != mFileTable.end(); ++it)
	{
		if (strcmpi(it->filename.c_str(), name.c_str()) == 0)
			return &(*it);
	}

	return 0;
}


MemoryFileContent* Filesystem::RegisterMemoryFileContent(const char* name, unsigned char* data, unsigned int size)
{
	MemoryFileContent* file = new MemoryFileContent();
	file->data = data;
	file->name = name;
	file->size = size;
	mMemoryFileContent.push_back(file);

	FileTableEntry entry;
	entry.filename = name;
	entry.location = FL_Memory;
	entry.memoryFileContent = file;
	entry.numTimesLoaded = 0;
	mFileTable.push_back(entry);
	return file;
}

bool Filesystem::ReleaseMemoryFileContent(MemoryFileContent** file)
{
	if (!*file)
		return false;

	vector<MemoryFileContent*>::iterator it;
	for (it = mMemoryFileContent.begin(); it != mMemoryFileContent.end(); )
	{
		if (*it == *file)
		{
			it = mMemoryFileContent.erase(it);
			*file = 0;
			return true;
		}
		else
		{
			++it;
		}
	}

	return false;
}

void Filesystem::OutputUsageStats()
{
	// TODO: might be worth putting in to a different file
	// in a table (non HTML) so it can be passed by another tool?
	// maybe game cmd line option to remove unused files even?
	//ofstream file("filesystem.html");
	File file("filesystem.html", FAT_Write | FAT_Text);

	file << "<html><body>" << endl;
	file << "<b>Files not loaded:</b><br>" << endl;

	file << "<table border=1 bordercolor=gray cellpadding=5 cellspacing=0>";

	file << "<tr><td><b>location</b></td><td><b>name</b></td><td><b>physical location</b></td></td></tr>";


	vector<FileTableEntry>::iterator it;
	for (it = mFileTable.begin(); it != mFileTable.end(); ++it)
	{
		if (it->numTimesLoaded <= 0)
		{
			file << "<tr><td>";

			switch (it->location)
			{
			case FL_Package:
				{
					file << "Package";
					file << "</td><td>";
					file << it->filename.c_str();
					file << "</td><td>";
					file << it->package->GetName();
				}
				break;
			case FL_Physical:
				{
					file << "Physical";
					file << "</td><td>";
					file << it->filename.c_str();
					file << "</td><td>";
					file << it->path.c_str();
				}
				break;
			}

			file << "</td></tr>";
		}
	}

	file << "</body></html>" << endl;

	file.Close();
}

string Filesystem::GetFileNameFromPath(const string& path)
{
	for (int i = path.length(); i > 0; --i)
	{
		if (path[i - 1] == '\\' || path[i - 1] == '/')
			return path.substr(i, path.length() - i);
	}

	return path;
}

bool Filesystem::SetDLLDirectory(const string& path)
{
#ifdef WIN32
	WCHAR wideText[8192];
	unsigned int len = path.length();
	wideText[len] = '\0';		
	MultiByteToWideChar(CP_ACP, 0, path.c_str(), len, wideText, len);
	return SetDllDirectoryW((LPCWSTR)wideText) == TRUE;
#else
	return false;
#endif
}