#include "Memory/NoMemory.h"

#include <stdio.h>
#include <stdarg.h>
#include <stdarg.h>
#include <memory.h>
#include "PackageFile.h"
#include "Package.h"

#ifndef WIN32
	#define strcmpi strcasecmp
#endif

//-----------------------------------------------------------------

bool PackageFile::Open(const char* file, int access)
{
	if (!file)
		return false;

	// now to find out what package this is in
	FileTableEntry* fileTableEntry = gFilesystem.GetFileTableEntry(file);
	if (!fileTableEntry || fileTableEntry->location != FL_Package || (access & FAT_Write))
		return false;

	mVirtualFileName = file;
	Package* p = fileTableEntry->package;
	PackageFileEntry* packageFileEntry = p->FindFile(file);
	mSize = packageFileEntry->size;
	mOffset = packageFileEntry->offset;

	if (!Super::Open(p->GetName(), access))
		return false;

	// seek start of file
	Seek(0, FST_Begin);
	return true;
}

bool PackageFile::EndOfFile() const
{
	return GetPosition() >= mSize;
}

unsigned int PackageFile::Write(const void* data, unsigned int size)
{
	return 0; // no allowed!
}

unsigned int PackageFile::Read(void* data, unsigned int size)
{
	unsigned int actualSize = min(mSize - GetPosition(), size);
	return Super::Read(data, actualSize);
}

unsigned int PackageFile::WriteString(const char* str, ...)
{
	return 0; // not allowed
}

unsigned int PackageFile::ReadString(const char* text, ...)
{
	// TODO: add some file cursor clamping
 	if( text == 0 )
    	return 0;

	va_list ap;
  	va_start(ap, text);
	int result = Super::ReadString(text, ap);
  	va_end(ap);

	return result;
}

unsigned int PackageFile::WriteString(const char* str, va_list ap)
{
	return 0; // not allowed
}

unsigned int PackageFile::ReadString(const char* str, va_list ap)
{
	// TODO: add some file cursor clamping
	unsigned int ret = Super::ReadString(str, ap);
	return ret;
}

unsigned int PackageFile::Seek(long offset, FileSeekType type)
{
	unsigned int physicalPos = mOffset;
	int t = SEEK_SET;
	switch (type)
	{
	case FST_Begin:
		physicalPos = mOffset + offset;
		break;

	case FST_Current:
		physicalPos = Super::GetPosition() + offset;
		break;

	case FST_End:
		physicalPos = mOffset + mSize - offset;
		break;
	}

	// clamp to make sure we cant go outside the bounds of the virtual file
	physicalPos = max(mOffset, min(physicalPos, mOffset + mSize));
	return Super::Seek(physicalPos, FST_Begin);
}

unsigned int PackageFile::GetPosition() const
{
	unsigned int physicalPos = Super::GetPosition();
	return physicalPos - mOffset;
}

char* PackageFile::ReadLine(char* buffer, unsigned int size)
{
	return Super::ReadLine(buffer, size);
}

unsigned int PackageFile::Size()
{
	return mSize;
}

const char* PackageFile::GetName()
{
	return mVirtualFileName.c_str();
}
