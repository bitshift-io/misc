#include "ScriptVariable.h"

ScriptVariable::ScriptVariable(const string& name) :
  mName(name)
{

}

ScriptVariable::ScriptVariable(const ScriptVariable& rhs)
{
	mName = rhs.mName;

	ValueConstIterator valueIt;
	for (valueIt = rhs.ValueConstBegin(); valueIt != rhs.ValueConstEnd(); ++valueIt)
		mValue.push_back(*valueIt);
}

void ScriptVariable::AppendValue(const string& value)
{
	VariableValue val;
	val.mType = VVT_String;
	val.mString = value;
	mValue.push_back(val);
}

void ScriptVariable::AppendValue(const ScriptClass* value)
{
	VariableValue val;
	val.mType = VVT_Pointer;
	val.mScriptClassPtr = value;
	mValue.push_back(val);
}

void ScriptVariable::AppendValue(const ScriptClass& value)
{
	VariableValue val;
	val.mType = VVT_Instance;
	val.mScriptClass = value;
	mValue.push_back(val);
}

