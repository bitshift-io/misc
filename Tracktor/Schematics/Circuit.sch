EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-library
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ArduinoNano Arduino1
U 1 1 584A4709
P 5600 3650
F 0 "Arduino1" H 5600 2600 60  0000 C CNN
F 1 "ArduinoNano" H 5600 4250 60  0000 C CNN
F 2 "my-footprints:Arduino Nano" H 5600 3650 60  0001 C CNN
F 3 "" H 5600 3650 60  0001 C CNN
	1    5600 3650
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P1
U 1 1 584A4768
P 7750 3550
F 0 "P1" H 7750 3750 50  0000 C CNN
F 1 "CONN_01X03" V 7850 3550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 7750 3550 50  0001 C CNN
F 3 "" H 7750 3550 50  0000 C CNN
	1    7750 3550
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P2
U 1 1 584A4793
P 7750 4350
F 0 "P2" H 7750 4550 50  0000 C CNN
F 1 "CONN_01X03" V 7850 4350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 7750 4350 50  0001 C CNN
F 3 "" H 7750 4350 50  0000 C CNN
	1    7750 4350
	1    0    0    -1  
$EndComp
$Comp
L R_Small R1
U 1 1 584A47D4
P 4500 3600
F 0 "R1" H 4530 3620 50  0000 L CNN
F 1 "R_Small" H 4530 3560 50  0000 L CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" H 4500 3600 50  0001 C CNN
F 3 "" H 4500 3600 50  0000 C CNN
	1    4500 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3300 7200 3300
Wire Wire Line
	7200 3300 7200 4450
Wire Wire Line
	7200 4450 7550 4450
Wire Wire Line
	7550 3650 7200 3650
Connection ~ 7200 3650
Wire Wire Line
	6100 3500 7000 3500
Wire Wire Line
	7000 3500 7000 4350
Wire Wire Line
	7000 4350 7550 4350
Wire Wire Line
	7550 3550 7000 3550
Connection ~ 7000 3550
$Comp
L LED_Small D1
U 1 1 584A4C59
P 4850 3700
F 0 "D1" H 4800 3825 50  0000 L CNN
F 1 "LED_Small" H 4675 3600 50  0000 L CNN
F 2 "LEDs:LED-3MM" V 4850 3700 50  0001 C CNN
F 3 "" V 4850 3700 50  0000 C CNN
	1    4850 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4300 6750 4300
Wire Wire Line
	6750 4300 6750 4250
Wire Wire Line
	6750 4250 7550 4250
Wire Wire Line
	6100 4200 6650 4200
Wire Wire Line
	6650 4200 6650 3450
Wire Wire Line
	6650 3450 7550 3450
Wire Wire Line
	5100 3700 4950 3700
Wire Wire Line
	4750 3700 4500 3700
Wire Wire Line
	5100 3500 4500 3500
$Comp
L ir-receiver U1
U 1 1 584A63AD
P 5000 2450
F 0 "U1" H 5000 3400 60  0000 C CNN
F 1 "ir-receiver" H 5000 3400 60  0000 C CNN
F 2 "my-footprints:ir-receiver" H 5000 3400 60  0001 C CNN
F 3 "" H 5000 3400 60  0001 C CNN
	1    5000 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 2750 5000 3500
Connection ~ 5000 3500
Wire Wire Line
	4900 2750 4900 2900
Wire Wire Line
	4900 2900 6400 2900
Wire Wire Line
	6400 2900 6400 3500
Connection ~ 6400 3500
Wire Wire Line
	5100 2750 5100 3050
Wire Wire Line
	5100 3050 4200 3050
Wire Wire Line
	4200 3050 4200 4500
Wire Wire Line
	4200 4500 5100 4500
$EndSCHEMATC
