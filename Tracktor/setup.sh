# http://docs.opencv.org/trunk/d7/d9f/tutorial_linux_install.html

cd ..

# rxtx
sudo apt-get install wget unzip
sudo apt-get install librxtx-java

#wget rxtx.qbang.org/pub/rxtx/rxtx-2.1-7-bins-r2.zip
#unzip rxtx-2.1-7-bins-r2.zip
#cd rxtx-2.1-7-bins-r2
#sudo cp RXTXcomm.jar /usr/lib/jvm/java-8-oracle/jre/lib/ext/RXTXcomm.jar
#sudo cp Linux/x86_64-unknown-linux-gnu/librxtxSerial.so /usr/lib/jvm/java-8-oracle/jre/lib/librxtxSerial.so
#cd ..


# opencv
sudo apt-get install build-essential
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev

#git clone https://github.com/opencv/opencv.git
wget https://github.com/opencv/opencv/archive/3.2.0.zip
unzip 3.2.0.zip
cd opencv-3.2.0

cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local -DBUILD_SHARED_LIBS=NO -DBUILD_EXAMPLES=NO -DBUILD_TESTS=NO -DBUILD_PERF_TESTS=NO -DWITH_GSTREAMER=ON

make -j8
sudo make install


