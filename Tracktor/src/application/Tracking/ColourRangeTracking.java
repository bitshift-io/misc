package application.Tracking;

import application.MainController;
import application.TrackingTechnique;
import application.Utils;
import java.util.Arrays;
import java.util.List;
import javafx.scene.image.Image;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.TermCriteria;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;

/**
 * cheese
 */
public class ColourRangeTracking implements TrackingTechnique {
    
    private Rect trackingRect = null;
    private Mat	hsv = null;
    private Mat hue = null;
    private Mat hsv_mask = null;
        
    private Scalar hsvUpper = new Scalar(255, 128, 128, 0);
    private Scalar hsvLower = new Scalar(0, 0, 0, 0);

    private Mat prevFrameGray = null;
    private MatOfInt histSize = null;
    

    private Mat hist = new Mat();
    private Mat histImage = null;

        
    MainController controller;

    public ColourRangeTracking(MainController cont)
    {
        controller = cont;
    }
    
    public String getDisplayName()
    {
        return "Colour Range (Moving)";
    }

    /**
     * Attempt to track a target in a moving frame!
     */
    public void update()
    {
        Mat currentGray = new Mat();
        Imgproc.cvtColor(controller.currentMatFrame, currentGray, Imgproc.COLOR_BGR2GRAY);

        boolean firstFrame = false;
        if (prevFrameGray == null)
        {
            prevFrameGray = new Mat();
            firstFrame = true;
            currentGray.copyTo(prevFrameGray);

        }

        // draw mouse drag rect
        if (controller.trackingDebugCheck.isSelected() || controller.currentFrameMouseDragRect.isDragging())
        {
            controller.currentFrameMouseDragRect.drawRect(controller.currentMatFrame, new Scalar(0, 255, 0, 128), 3);
        }

        hsv = new Mat(); 
        Imgproc.cvtColor(controller.currentMatFrame, hsv, Imgproc.COLOR_BGR2HSV);

        hue = new Mat();
        hue.create(hsv.size(), hsv.depth());
        MatOfInt ch = new MatOfInt(0, 0);
        Core.mixChannels(Arrays.asList(hsv), Arrays.asList(hue), ch);
        MatOfFloat ranges = new MatOfFloat(0, 180);

        hsv_mask = new Mat(controller.currentMatFrame.size(),CvType.CV_8UC1);
        int vmin =65, vmax = 256, smin = 55;
        Core.inRange(hsv, new Scalar(0, smin,Math.min(vmin,vmax)),new Scalar(180, 256,Math.max(vmin, vmax)), hsv_mask);

        // https://github.com/opencv/opencv/blob/master/samples/cpp/camshiftdemo.cpp
        if (controller.currentFrameMouseDragRect.isFinished())
        {
                controller.currentFrameMouseDragRect.resetFinished();
                Point topLeft = controller.currentFrameMouseDragRect.getTopLeft(controller.currentMatFrame);
                Point bottomRight = controller.currentFrameMouseDragRect.getBottomRight(controller.currentMatFrame);
                trackingRect = new Rect(topLeft, bottomRight);

                Mat maskRoi = hsv_mask.submat(trackingRect);
                List<Mat> imgList = Arrays.asList(hue.submat(trackingRect));
                MatOfInt channels = new MatOfInt(0); // this is the hue channel

                histSize = new MatOfInt(25);

                boolean accumulate = false;
                Imgproc.calcHist(imgList, channels, maskRoi, hist, histSize, ranges, accumulate);
                Core.normalize(hist, hist, 0, 255, Core.NORM_MINMAX);

                // setup the hist image
                histImage = Mat.zeros(200, 320, CvType.CV_8UC3);

                int hsize = (int) histSize.get(0, 0)[0];
                int w = histImage.width(); 
                int h = histImage.height();
                int bin_w = (int) ( (double) w / hsize );


                // show colour in histogram
                Mat buf = new Mat(1, hsize, CvType.CV_8UC3);
                for( int i = 0; i < hsize; i++ )
                {
                        buf.put(0, i, new double[]{i*180./hsize, 255, 255});
                }
                Imgproc.cvtColor(buf, buf, Imgproc.COLOR_HSV2BGR);


                for( int i = 0; i < hsize; i++ )
                {                 
                        int histVal = (int) (hist.get(i,0)[0] * w/255);

                        Point tl = new Point( i*bin_w, h );
                        Point br = new Point( (i+1)*bin_w, h - histVal );
                        Imgproc.rectangle( histImage, tl, br, new Scalar(buf.get(0, i)), -1); 
                }
        }

        // so some object has been selected by the user... lets track it!
        if (histSize != null)
        {
                // Perform CAMShift
                double scale = 1.0;
                Mat backproj = new Mat();
                Imgproc.calcBackProject(Arrays.asList(hue), new MatOfInt(0), hist, backproj, ranges, scale);
                Core.bitwise_and(backproj, hsv_mask, backproj );
                RotatedRect trackBox = Video.CamShift(backproj, trackingRect, new TermCriteria( TermCriteria.EPS | TermCriteria.COUNT, 10, 1 ));

                if( trackingRect.area() <= 1 )
                {
                    int cols = backproj.width(), rows = backproj.height(), r = (Math.min(cols, rows) + 5)/6;
                    trackingRect = Utils.intersection(new Rect(trackingRect.x - r, trackingRect.y - r,
                                    trackingRect.x + r, trackingRect.y + r), new Rect(0, 0, cols, rows));
                }

                if (controller.trackingDebugCheck.isSelected())
                {
                        Imgproc.ellipse(controller.currentMatFrame, trackBox, new Scalar(0,0,255), 3, Imgproc.LINE_AA );
                }

                Utils.updateImageView(controller.debugView, Utils.mat2Image(backproj /*histImage*/));


                controller.camera.track(trackBox.boundingRect());
                controller.camera.update();
        }
        else
        {
            controller.setLeftStatus("Drag a rectangle, in the 'Primary Source' view around the coloured object you want to track...");
        }



        // convert and show the frame
        Image imageToShow = Utils.mat2Image(controller.currentMatFrame);
        Utils.updateImageView(controller.currentFrame, imageToShow);

    }
    
    
    // unused
    private void recordHSVValues(Point topLeft, Point bottomRight, Mat hsvImg)
    {
            hsvUpper = new Scalar(-Double.MAX_VALUE, -Double.MAX_VALUE, -Double.MAX_VALUE);
            hsvLower = new Scalar(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);

            for (int x = (int)topLeft.x; x < (int)bottomRight.x; ++x)
            {
                    for (int y = (int)topLeft.y; y < (int)bottomRight.y; ++y)
                    {
                            double[] hsv = hsvImg.get(x, y);
                            hsvUpper.val[0] = Math.max(hsvUpper.val[0], hsv[0]);
                            hsvUpper.val[1] = Math.max(hsvUpper.val[1], hsv[1]);
                            hsvUpper.val[2] = Math.max(hsvUpper.val[2], hsv[2]);

                            hsvLower.val[0] = Math.min(hsvLower.val[0], hsv[0]);
                            hsvLower.val[1] = Math.min(hsvLower.val[1], hsv[1]);
                            hsvLower.val[2] = Math.min(hsvLower.val[2], hsv[2]);
                    }
            }

            System.out.println("hsv upper: [" + hsvUpper.val[0] + "," + hsvUpper.val[1] + "," + hsvUpper.val[2] + "]");
            System.out.println("hsv lower: [" + hsvLower.val[0] + "," + hsvLower.val[1] + "," + hsvLower.val[2] + "]");
    }
}