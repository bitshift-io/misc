package application.Tracking;

import application.MainController;
import application.TrackingTechnique;
import application.Utils;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import static org.opencv.objdetect.Objdetect.CASCADE_SCALE_IMAGE;
import org.opencv.video.BackgroundSubtractor;
import org.opencv.video.Video;


/**
 * Haven't had success with this method based on this: https://github.com/Synaptitude/Smooth-Facial-Tracking
 * it uses cascades to identify faces and eyes
 */
public class SmoothFaceTracking implements TrackingTechnique {
    
    Point priorCenter = new Point(0, 0);
    
    CascadeClassifier face_cascade = new CascadeClassifier();
    CascadeClassifier eyes_cascade = new CascadeClassifier();

    MainController controller;

    public SmoothFaceTracking(MainController cont)
    {
        controller = cont;

        if (!face_cascade.load("data/haarcascades/haarcascade_frontalface_alt.xml"))
            controller.displayError("Failed to load face cascade");

        if (!eyes_cascade.load("data/haarcascades/haarcascade_eye_tree_eyeglasses.xml"))
            controller.displayError("Failed to load eye cascade");
    }

    public String getDisplayName()
    {
        return "Smooth Face Tracking (Static)";
    }
    
    public void update()
    {
        // Apply the classifier to the frame, i.e. find face
         priorCenter = detectFace(controller.currentMatFrame, priorCenter);  
    }

    // Rounds up to multiple
    int roundUp(int numToRound, int multiple)
    {
      if (multiple == 0)
        return numToRound;

      int remainder = Math.abs(numToRound) % multiple;
      if (remainder == 0)
        return numToRound;
      if (numToRound < 0)
        return -(Math.abs(numToRound) - remainder);
      return numToRound + multiple - remainder;
    }

    protected Point detectFace(Mat frame, Point priorCentre)
    {
        int h = (int)frame.size().height - 1;
        int w = (int)frame.size().width - 1;

        int minNeighbors = 2;
        boolean faceNotFound = false;

        MatOfRect faces = new MatOfRect();
        Mat currentGray = new Mat();
        Imgproc.cvtColor(controller.currentMatFrame, currentGray, Imgproc.COLOR_BGR2GRAY);

        Imgproc.equalizeHist(currentGray, currentGray);          // Equalize histogram

        // Detect face with open source cascade
        face_cascade.detectMultiScale(currentGray, faces,
                            1.1, minNeighbors,
                            0|CASCADE_SCALE_IMAGE, new Size(30, 30), new Size(100, 100));

        Rect[] faceList = faces.toArray();
        for (int i = 0; i < faceList.length; ++i)
        {
            Rect face = faceList[i];

            // Find center of face
            Point center = new Point(face.x + face.width/2,
             face.y + face.height/2);

            // Generate width and height of face, round to closest 1/4 of frame height
            h = roundUp(face.height, (int)frame.size().height / 4);
            w = 3 * h / 5;


            // If priorCenter not yet initialized, initialize
            if(priorCenter.x == 0) {
              priorCenter = center;
              //temp = outputFrame(frame, center, w, h); - dont need this for now, it extracts the face 
              break;
            }


            // Check to see if it's probably the same user
            if (Math.abs(center.x - priorCenter.x) < frame.size().width / 6 &&
               Math.abs(center.y - priorCenter.y) < frame.size().height / 6) {

              // Check to see if the user moved enough to update position
              if(Math.abs(center.x - priorCenter.x) < 7 &&
                 Math.abs(center.y - priorCenter.y) < 7){
                center = priorCenter;
              }

              // Smooth new center compared to old center
              center.x = (center.x + 2*priorCenter.x) / 3;
              center.y = (center.y + 2*priorCenter.y) / 3;
              priorCenter = center;

              // output frame of only face
              //temp = outputFrame(frame, center, w, h); - dont need this for now, it extracts the face 

              break; // exit, primary users face probably found

            } else {
              faceNotFound = true;
            }
        }


        if(faceNotFound) {

          // Findface from eyes
          Rect r = new Rect((int)priorCenter.x, (int)priorCenter.y, w, h);
          if(priorCenter.x + w > currentGray.size().width - 2 &&
             priorCenter.y + h > currentGray.size().height - 2){

            priorCenter = faceFromEyes(priorCenter, currentGray.submat(r));

            // Generate temporary face location
            //temp = outputFrame(frame, priorCenter, w, h); - dont need this for now, it extracts the face
          }    
        }


        // Check to see if new face found
        //if(temp.size().width > 2)
        //  output = temp;
        //else
        //  output = frame;


        Mat output = frame;

        // Display only face
        //imshow(face_window, output);

        if(output.size().width > 2)
          // Draw ellipse around face
          Imgproc.ellipse(frame, priorCenter, new Size(w/2, h/2),
                  0, 0, 360, new Scalar( 255, 0, 255 ), 4, 8, 0);

        // Display output
        //imshow( display_window, frame );

        if (controller.trackingDebugCheck.isSelected())
        {
                //Imgproc.ellipse(currentMatFrame, trackBox, new Scalar(0,0,255), 3, Imgproc.LINE_AA );
        }

        Utils.updateImageView(controller.debugView, Utils.mat2Image(output));

        return priorCenter;
    }

    // Find face from eyes
    Point faceFromEyes(Point priorCenter, Mat face) {
        MatOfRect eyes = new MatOfRect();
        int avg_x = 0;
        int avg_y = 0;

        // Try to detect eyes, if no face is found
        eyes_cascade.detectMultiScale(face, eyes, 1.1, 2,
                                      0 |CASCADE_SCALE_IMAGE, new Size(30, 30), new Size(100, 100));

        // Iterate over eyes
        Rect[] eyeList = eyes.toArray();
        for(int j = 0; j < eyeList.length; j++) {

            Rect eye = eyeList[j];

            // centerpoint of eyes
            Point eye_center = new Point(priorCenter.x + eye.x + eye.width/2,
                             priorCenter.y + eye.y + eye.height/2);

            // Average center of eyes
            avg_x += eye_center.x;
            avg_y += eye_center.y;
        }

        // Use average location of eyes
        if(eyeList.length > 0) {
          priorCenter.x = avg_x / eyeList.length;
          priorCenter.y = avg_y / eyeList.length;
        }

        return priorCenter;
    }
}
