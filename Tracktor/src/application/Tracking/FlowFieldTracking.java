package application.Tracking;

import application.MainController;
import application.TrackingTechnique;
import application.Utils;
import static com.sun.prism.impl.PrismSettings.debug;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.scene.image.Image;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.core.TermCriteria;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;


/**
 * Haven't had success with this method based on this: https://github.com/Synaptitude/Smooth-Facial-Tracking
 * it uses cascades to identify faces and eyes
 */
public class FlowFieldTracking implements TrackingTechnique {
    
    private Rect trackingRect = null;
    private Mat prevFrameGray = null;
    private Mat flowMask = new Mat();
        
    private MatOfPoint2f curPoints = new MatOfPoint2f();
    private MatOfPoint2f prevPoints = new MatOfPoint2f();
        
    MainController controller;

    public FlowFieldTracking(MainController cont)
    {
        controller = cont;

    }

    public String getDisplayName()
    {
        return "Flow Field Tracking (Static)";
    }
    
    public void update()
    {
        Mat currentGray = new Mat();
        Imgproc.cvtColor(controller.currentMatFrame, currentGray, Imgproc.COLOR_BGR2GRAY);

        boolean firstFrame = false;
        if (prevFrameGray == null)
        {
                prevFrameGray = new Mat();
                firstFrame = true;
                currentGray.copyTo(prevFrameGray);

        }
                
        // this experimental code uses flow fields - great for image stabalisation!
        // not so go for finding jeremy!

        flowMask = new Mat(controller.currentMatFrame.height(), controller.currentMatFrame.width(), CvType.CV_8UC3, new Scalar(0, 0, 0));	


        // on first frame, setup a grid which we will track over the frame
        //if (firstFrame)
        {
                double GRID_DIVISION = 40.0;
                List<Point> gridPoints = new ArrayList<Point>();

                //curPoints.reshape((int)GRID_DIVISION, (int)GRID_DIVISION);
                double xStep = currentGray.width() / (GRID_DIVISION);
                double yStep = currentGray.height() / (GRID_DIVISION);
                for (int x = 0; x < GRID_DIVISION; ++x)
                {
                        for (int y = 0; y < GRID_DIVISION; ++y)
                        {
                                double xPix = (x * xStep) + (xStep * 0.5);
                                double yPix = (y * yStep) + (yStep * 0.5);
                                gridPoints.add(new Point(xPix, yPix));
                        }
                }

                Point[] ptArr = new Point[gridPoints.size()];
                ptArr = gridPoints.toArray(ptArr);
                prevPoints = new MatOfPoint2f(ptArr);

                //prevPoints.copyTo(curPoints);


        }

        /*
        // on first frame, find some good features to try to track
        //if (prevFrameGray == null)
        {
                MatOfPoint corners = new MatOfPoint();
                int maxCorners = 400;
                double qualityLevel = 0.1;
                double minDistance = 10.0;
                int blockSize = 7;
                boolean useHarrisDetector = false;
                double k = 0.13;			  
                Imgproc.goodFeaturesToTrack(prevFrameGray, corners, maxCorners, qualityLevel, minDistance, new Mat(), blockSize, useHarrisDetector, k);
                prevPoints = new MatOfPoint2f();
                corners.convertTo(prevPoints, CvType.CV_32FC2);

                //prevFrameGray = new Mat();
                //prevPoints = new MatOfPoint2f();

                //flowMask = new Mat(currentMatFrame.height(), currentMatFrame.width(), CvType.CV_8UC3, new Scalar(0, 0, 0));	
        }*/



        Size winSize = new Size(15,15);
        int maxLevel = 2;
        int flags = 0;
        double minEigThreshold = 0;
        TermCriteria criteria = new TermCriteria(TermCriteria.EPS | TermCriteria.MAX_ITER, 20, 0.3);
        MatOfByte status = new MatOfByte();
        MatOfFloat err = new MatOfFloat();
        Video.calcOpticalFlowPyrLK(prevFrameGray, currentGray, prevPoints, curPoints, status, err, winSize, maxLevel, criteria, flags, minEigThreshold);


        MatOfPoint2f velMat = createVelocityMat();
        Point medianVel = computeMedianVelocity(velMat);

        //double MEDIAN_VELOCITY_MULTIPLIER = 2.5; // used to adjust the median velocity
        //medianVel.x *= MEDIAN_VELOCITY_MULTIPLIER;
        //medianVel.y *= MEDIAN_VELOCITY_MULTIPLIER;
        double medianVelMag = 5.0; //Math.sqrt((medianVel.x * medianVel.x) + (medianVel.y * medianVel.y));
        //subtractVelocity(velMat, medianVel);

        MatOfDouble velMagMat = createVelocityMagnitudeMat(velMat);

        //System.out.println("median vel: " + medianVel);

        // draw the tracks
        for (int x = 0; x < prevPoints.width(); ++x)
        {
                for (int y = 0; y < prevPoints.height(); ++y)
                {
                        Point start = new Point(prevPoints.get(y, x));
                        Point end = new Point();
                        end.x = velMat.get(y,  x)[0] + start.x;
                        end.y = velMat.get(y,  x)[1] + start.y;

                        double velMag = velMagMat.get(y, x)[0];

                        Imgproc.line(flowMask, start, end, new Scalar(0, 255, 0, 255), 2);

                        if (velMag > medianVelMag)
                        {
                                Imgproc.circle(controller.currentMatFrame, start, 2,  new Scalar(0, 0, 255, 255), 2);
                        }
                        else
                        {
                                Imgproc.circle(controller.currentMatFrame, start, 2,  new Scalar(255, 0, 0, 255), 2);
                        }
                }	
        }



        // track the rectangle, move it according to the flow field within the rectangle
        if (trackingRect != null)
        {
                Point avgVelInRect = new Point();
                int avgVelCount = 0;


                for (int x = 0; x < prevPoints.width(); ++x)
                {
                        for (int y = 0; y < prevPoints.height(); ++y)
                        {
                                Point start = new Point(prevPoints.get(y, x));
                                if (trackingRect.contains(start))
                                {
                                        ++avgVelCount;
                                        avgVelInRect.x += velMat.get(y,  x)[0];
                                        avgVelInRect.y += velMat.get(y,  x)[1];
                                }
                        }
                }

                avgVelInRect.x /= avgVelCount;
                avgVelInRect.y /= avgVelCount;

                trackingRect.x += avgVelInRect.x;
                trackingRect.y += avgVelInRect.y;

                Imgproc.rectangle(flowMask, trackingRect.tl(), trackingRect.br(), new Scalar(255, 0, 0, 255), 3);
        }

        Imgproc.circle(flowMask, new Point(100, 100), 20,  new Scalar(255, 0, 0, 255), 3);


        postFlowFieldStuff();
        
        Utils.updateImageView(controller.currentFrame, Utils.mat2Image(controller.currentMatFrame));
        Utils.updateImageView(controller.debugView, Utils.mat2Image(flowMask));
    }
    

    /**
     * Not sure what in here is needed for this to work
     */
    protected void postFlowFieldStuff()
    {
/*

            // draw mouse drag rect
            if (debug.isSelected() || currentFrameMouseDragRect.isDragging())
            {
                    currentFrameMouseDragRect.drawRect(currentMatFrame, new Scalar(0, 255, 0, 128), 3);
            }


            hsv = new Mat(); 
            Imgproc.cvtColor(currentMatFrame, hsv, Imgproc.COLOR_BGR2HSV);

            hue = new Mat();
            hue.create(hsv.size(), hsv.depth());
            MatOfInt ch = new MatOfInt(0, 0);
            Core.mixChannels(Arrays.asList(hsv), Arrays.asList(hue), ch);
            MatOfFloat ranges = new MatOfFloat(0, 180);

            hsv_mask = new Mat(currentMatFrame.size(),CvType.CV_8UC1);
            int vmin =65, vmax = 256, smin = 55;
            Core.inRange(hsv, new Scalar(0, smin,Math.min(vmin,vmax)),new Scalar(180, 256,Math.max(vmin, vmax)), hsv_mask);

            // https://github.com/opencv/opencv/blob/master/samples/cpp/camshiftdemo.cpp
            if (currentFrameMouseDragRect.isFinished())
            {
                    currentFrameMouseDragRect.resetFinished();
                    Point topLeft = currentFrameMouseDragRect.getTopLeft(currentMatFrame);
                    Point bottomRight = currentFrameMouseDragRect.getBottomRight(currentMatFrame);
                    trackingRect = new Rect(topLeft, bottomRight);

                    Mat maskRoi = hsv_mask.submat(trackingRect);
                    List<Mat> imgList = Arrays.asList(hue.submat(trackingRect));
                    MatOfInt channels = new MatOfInt(0); // this is the hue channel

                    histSize = new MatOfInt(25);

                    boolean accumulate = false;
                    Imgproc.calcHist(imgList, channels, maskRoi, hist, histSize, ranges, accumulate);
                    Core.normalize(hist, hist, 0, 255, Core.NORM_MINMAX);

                    // setup the hist image
                    histImage = Mat.zeros(200, 320, CvType.CV_8UC3);

                    int hsize = (int) histSize.get(0, 0)[0];
                    int w = histImage.width(); 
                    int h = histImage.height();
                    int bin_w = (int) ( (double) w / hsize );


                    // show colour in histogram
                    Mat buf = new Mat(1, hsize, CvType.CV_8UC3);
                    for( int i = 0; i < hsize; i++ )
                    {
                            buf.put(0, i, new double[]{i*180./hsize, 255, 255});
                    }
                    Imgproc.cvtColor(buf, buf, Imgproc.COLOR_HSV2BGR);


                    for( int i = 0; i < hsize; i++ )
                    {                 
                            int histVal = (int) (hist.get(i,0)[0] * w/255);

                            Point tl = new Point( i*bin_w, h );
                            Point br = new Point( (i+1)*bin_w, h - histVal );
                            Imgproc.rectangle( histImage, tl, br, new Scalar(buf.get(0, i)), -1); 
                    }
            }

            // so some object has been selected by the user... lets track it!
            if (histSize != null)
            {
                    // Perform CAMShift
                    double scale = 1.0;
                    Mat backproj = new Mat();
                    Imgproc.calcBackProject(Arrays.asList(hue), new MatOfInt(0), hist, backproj, ranges, scale);
                    Core.bitwise_and(backproj, hsv_mask, backproj );
                    RotatedRect trackBox = Video.CamShift(backproj, trackingRect, new TermCriteria( TermCriteria.EPS | TermCriteria.COUNT, 10, 1 ));

                    if( trackingRect.area() <= 1 )
                    {
                        int cols = backproj.width(), rows = backproj.height(), r = (Math.min(cols, rows) + 5)/6;
                        trackingRect = Utils.intersection(new Rect(trackingRect.x - r, trackingRect.y - r,
                                        trackingRect.x + r, trackingRect.y + r), new Rect(0, 0, cols, rows));
                    }

                    if (debug.isSelected())
                    {
                            Imgproc.ellipse(currentMatFrame, trackBox, new Scalar(0,0,255), 3, Imgproc.LINE_AA );
                    }

                    updateImageView(debugView, Utils.mat2Image(backproj));


                    camera.track(trackBox.boundingRect());
                    camera.update();
            }



            // add the mask to be displayed
            //Core.add(currentMatFrame, flowMask, currentMatFrame);

            // convert and show the frame
            Image imageToShow = Utils.mat2Image(currentMatFrame);
            updateImageView(currentFrame, imageToShow);

            // copy cur to prev
            currentGray.copyTo(prevFrameGray);
            curPoints.copyTo(prevPoints);

            // display what the camera sees
            Mat cameraFrame = currentMatFrame.submat(camera.getFrame());
            Image cameraImageToShow = Utils.mat2Image(cameraFrame);
            updateImageView(cameraView, cameraImageToShow);	

            // update the pan and tilt mechanism
            controller.panAndTilt.update(controller.camera.getFrameCentreAsPercent());
        */
    }

    
    

    // use prevPoints and curPoints to create a velocity matrix
    protected MatOfPoint2f createVelocityMat()
    {
            List<Point> ptList = new ArrayList<Point>();

            // compute median velocity of the entire frame
            for (int x = 0; x < curPoints.width(); ++x)
            {
                    for (int y = 0; y < curPoints.height(); ++y)
                    {
                            Point prev = new Point(prevPoints.get(y, x));
                            Point cur = new Point(curPoints.get(y,  x));

                            double dx = cur.x - prev.x;
                            double dy = cur.y - prev.y;

                            ptList.add(new Point(dx, dy));
                    }	
            }

            Point[] ptArr = new Point[ptList.size()];
            ptArr = ptList.toArray(ptArr);
            return new MatOfPoint2f(ptArr);
    }

    // convert velocity to a magnitude squared matrix
    protected MatOfDouble createVelocityMagnitudeMat(MatOfPoint2f velMat)
    {		
            double[] arr = new double[velMat.width() * velMat.height()];

            // compute median velocity of the entire frame
            for (int x = 0; x < velMat.width(); ++x)
            {
                    for (int y = 0; y < velMat.height(); ++y)
                    {
                            double vel[] = velMat.get(y,  x);				
                            arr[x + y * velMat.width()] = Math.sqrt((vel[0] * vel[0]) + (vel[1] * vel[1]));
                    }	
            }

            return new MatOfDouble(arr);
    }

    protected Point computeMedianVelocity(MatOfPoint2f velMat)
    {
            double frameDx = 0.0;
            double frameDy = 0.0;
            int count = velMat.width() * velMat.height();

            // compute median velocity of the entire frame
            for (int x = 0; x < velMat.width(); ++x)
            {
                    for (int y = 0; y < velMat.height(); ++y)
                    {				
                            double dx = velMat.get(y,  x)[0];
                            double dy = velMat.get(y,  x)[1];

                            frameDx += dx;
                            frameDy += dy;
                    }	
            }

            frameDx /= count;
            frameDy /= count;

            return new Point(frameDx, frameDy);
    }

    /**
     * Subtract vel to velMat. Modifies velMat
     */
    protected void subtractVelocity(MatOfPoint2f velMat, Point vel)
    {
            vel.x = 10.0; // test

            for (int x = 0; x < velMat.width(); ++x)
            {
                    for (int y = 0; y < velMat.height(); ++y)
                    {								
                            double vx = velMat.get(y,  x)[0] - vel.x;
                            double vy = velMat.get(y,  x)[1] - vel.y;

                            velMat.put(y,  x, new double[]{vx, vy});
                    }	
            }

    }
}
