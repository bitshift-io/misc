package application.Tracking;

import application.MainController;
import application.TrackingTechnique;
import application.Utils;
import static application.Utils.updateImageView;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;
import javax.rmi.CORBA.Util;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractor;
import org.opencv.video.Video;


/**
 * Stationary camera with moving target
 */
public class BackgroundSubtractorTracking implements TrackingTechnique {

    
    private Mat motionMask; // where are we looking for motion?
    
    BackgroundSubtractor bgSubMog = Video.createBackgroundSubtractorMOG2();

    MainController controller;

    public BackgroundSubtractorTracking(MainController cont)
    {
        controller = cont;
    }

    public String getDisplayName()
    {
        return "Background Subtractor Tracking (Static)";
    }
    
    
    public void update()
    {
                    // setup a blank mask with correct size
            // on the first try, or setup if there is a change in the frame size
            if (motionMask == null || 
                    (motionMask != null && (motionMask.width() != controller.currentMatFrame.width() || motionMask.height() != controller.currentMatFrame.height())))
            {
                    motionMask = new Mat(controller.currentMatFrame.height(), controller.currentMatFrame.width(), CvType.CV_8UC1, new Scalar(0, 0, 0));
                    
                    
            }

            Mat foregroundMask = new Mat();
            bgSubMog.apply(controller.currentMatFrame, foregroundMask);

            // the user let go of the drag/drop operation
            if (controller.currentFrameMouseDragRect.isFinished())
            {
                    motionMask = new Mat(controller.currentMatFrame.height(), controller.currentMatFrame.width(), CvType.CV_8UC1, new Scalar(0, 0, 0));	
                    controller.currentFrameMouseDragRect.fillPoly(motionMask, new Scalar(255, 255, 255));
                    controller.currentFrameMouseDragRect.resetFinished();
            }

            Mat motionInMask = new Mat();
            Core.bitwise_and(motionMask, foregroundMask, motionInMask);


            // morphOps or blur, do similar things - try to reduce noise yet
            morphOps(motionInMask);


            trackFilteredObject(motionInMask, controller.currentMatFrame);


            // draw the debug image
            Image testImageToShow = Utils.mat2Image(motionInMask); 
            Utils.updateImageView(controller.debugView, testImageToShow);


            // draw mouse drag rect
            if (controller.trackingDebugCheck.isSelected() || controller.currentFrameMouseDragRect.isDragging())
            {
                    controller.currentFrameMouseDragRect.drawRect(controller.currentMatFrame, new Scalar(0, 255, 0, 128), 3);
            }

            // convert and show the frame
            Image imageToShow = Utils.mat2Image(controller.currentMatFrame);
            Utils.updateImageView(controller.currentFrame, imageToShow);


            // display what the camera sees
            Mat cameraFrame = controller.currentMatFrame.submat(controller.camera.getFrame());
            Image cameraImageToShow = Utils.mat2Image(cameraFrame);
            updateImageView(controller.cameraView, cameraImageToShow);	

            // update the pan and tilt mechanism
            controller.panAndTilt.update(controller.camera.getFrameCentreAsPercent());
    }
    
    
    /*
     * perform morphological operations on thresholded image to eliminate noise
     * and emphasize the filtered object(s)
     */
    private void morphOps(Mat thresh)
    {


            int erosion_size = 3; //morphSeekBars[0].getRealProgress();
        int dilation_size = 20; //morphSeekBars[1].getRealProgress();

        //create structuring element that will be used to "dilate" and "erode" image.       
        Mat erodeElement = Imgproc.getStructuringElement( Imgproc.MORPH_RECT, new Size(erosion_size, erosion_size));
        Imgproc.erode(thresh, thresh, erodeElement);
        Imgproc.erode(thresh, thresh, erodeElement);

        int BLUR_SIZE = 10;
            Imgproc.blur(thresh, thresh, new Size(BLUR_SIZE, BLUR_SIZE));
            Imgproc.threshold(thresh, thresh, 1, 255, Imgproc.THRESH_BINARY);


        //dilate with larger element so make sure object is nicely visible
        Mat dilateElement = Imgproc.getStructuringElement( Imgproc.MORPH_RECT, new Size(dilation_size, dilation_size));
        Imgproc.dilate(thresh, thresh, dilateElement);
        Imgproc.dilate(thresh, thresh, dilateElement);

        Imgproc.blur(thresh, thresh, new Size(BLUR_SIZE, BLUR_SIZE));
        Imgproc.blur(thresh, thresh, new Size(BLUR_SIZE, BLUR_SIZE));

        Imgproc.threshold(thresh, thresh, 1, 255, Imgproc.THRESH_BINARY);
    }

    void trackFilteredObject(Mat threshold, Mat cameraFeed)
    {
            double refArea = 0;
            int x = 0;
            int y = 0;
            boolean objectFound = false;

            Point topLeft = new Point();
            Point bottomRight = new Point();

            // find objects in the threshold
            Mat hierarchy = new Mat();
            Mat temp = new Mat();
            threshold.copyTo(temp);
            List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
            Imgproc.findContours(temp, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);

            List<Rect> rects = new ArrayList<Rect>();


            // we fond something, so draw a rectangle around it
            if (contours.size() > 0)
            {
                    int MAX_NUM_OBJECTS = 50;

                    if (contours.size() > MAX_NUM_OBJECTS)
                    {
                            //System.err.println("Too much noise! Adjust filters.");
                    }


                    for (int index = 0; index < contours.size(); ++index)
                    {

                            Rect rect = Imgproc.boundingRect(contours.get(index));
                            rects.add(rect);

                    }

                    // merge overlapping rects
                    for (int i = 0; i < rects.size(); ++i)
                    {
                            for (int k = i + 1; k < rects.size(); ++k)
                            {
                                    Rect b = rects.get(k);
                                    Rect a = rects.get(i);
                                    Rect intersection = Utils.intersection(a, b);
                                    boolean intersects = (intersection.area() > 0);
                                    if (intersects)
                                    {
                                            // an intersection has been found, so merge the latter (b) into (a)
                                            // and restart the search
                                            Rect union = Utils.union(a, b);
                                            rects.set(i, union);
                                            rects.remove(k);
                                            //k = i;
                                            i = -1; // just reset!
                                            break;
                                    }
                            }
                    }

                    // find the largest rect and track it
                    int largestIdx = -1;
                    double largestArea = 0.0;
                    for (int i = 0; i < rects.size(); ++i)
                    {
                            double area = rects.get(i).area();
                            if (area > largestArea)
                            {
                                    largestIdx = i;
                            }	
                    }
                    if (largestIdx >= 0)
                    {
                            controller.camera.track(rects.get(largestIdx));
                    }
                    controller.camera.update();

                    if (controller.trackingDebugCheck.isSelected())
                    {
                            controller.camera.draw(cameraFeed);
                    }

                    /*
                    // finally draw any rects
                    for (int i = 0; i < rects.size(); ++i)
                    {
                            Rect rect = rects.get(i);
                            Imgproc.rectangle(cameraFeed, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height), new Scalar(255, 0, 0, 255), 3); 
                    }*/
            }
    }
}
