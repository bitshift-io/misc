package application;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public final class Utils
{
    public static boolean isPlatformWindows()
    {
        return System.getProperty("os.name").toLowerCase().indexOf("win") >= 0;
    }
    
    public static Image mat2Image(Mat frame)
    {
        try
        {
            return SwingFXUtils.toFXImage(matToBufferedImage(frame), null);
        }
        catch (Exception e)
        {
            System.err.println("Cannot convert the Mat obejct: " + e);
            return null;
        }
    }

    public static <T> void onFXThread(final ObjectProperty<T> property, final T value)
    {
        Platform.runLater(() -> {
            property.set(value);
        });
    }

    private static BufferedImage matToBufferedImage(Mat original)
    {

        BufferedImage image = null;
        int width = original.width(), height = original.height(), channels = original.channels();
        byte[] sourcePixels = new byte[width * height * channels];
        original.get(0, 0, sourcePixels);

        if (original.channels() > 1)
        {
            image = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        }
        else
        {
            image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
        }
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(sourcePixels, 0, targetPixels, 0, sourcePixels.length);

        return image;
    }
    
    
    public static Rect union(Rect a, Rect b)
    {
    	int x1 = Math.min(a.x, b.x);
	    int y1 = Math.min(a.y, b.y);
	    
	    Rect r = new Rect();
	    
	    r.width = Math.max(a.x + a.width, b.x + b.width) - x1;
    	r.height = Math.max(a.y + a.height, b.y + b.height) - y1;
	    
    	r.x = x1;
    	r.y = y1;
    	
    	return r;
    }
    
    public static Rect intersection(Rect a, Rect b)
    {
    	int x1 = Math.max(a.x, b.x);
	    int y1 = Math.max(a.y, b.y);
	    
    	Rect r = new Rect();
    	
    	if( a.width <= 0 || a.height <= 0 )
	        return r;
    	
    	r.width = Math.min(a.x + a.width, b.x + b.width) - x1;
    	r.height = Math.min(a.y + a.height, b.y + b.height) - y1;
    	
    	r.x = x1;
    	r.y = y1;
    	
    	return r;
    }

    /**
     * Update the {@link ImageView} in the JavaFX main thread
     * 
     * @param view
     *            the {@link ImageView} to update
     * @param image
     *            the {@link Image} to show
     */
    public static void updateImageView(ImageView view, Image image)
    {
        if (image == null || view == null)
            return;
        
        Utils.onFXThread(view.imageProperty(), image);
    }
}