/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Screen;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

/**
 *
 * @author Fabian Mathews
 */
public class CaptureSource 
{
    private static final String SOURCE_NAME[] = { 
            "Webcam 1",
            "Webcam 2",
            "HDMI Camera",
            "Monitor 1",
            "Monitor 2",
            "Test Video"
        };
        
    private VideoCapture capture;
    private String sourceName;
    private boolean loop;
    private Mat frame = new Mat();
    private String queuedSourceName;
    
    static public String[] getSourceNames()
    {
        return SOURCE_NAME;
    }
    
    /*
        This avoids threading issues when a combo box is changed
        it will call this and then next time readNext is called
        the actual open will occur in the correct thread
    */
    public void openNextRead(String sourceName)
    {
        queuedSourceName = sourceName;
    }
    
    public boolean open(String sourceName)
    {
        if (capture != null)
        {
            capture.release();
            capture = null;
        }
        
        this.sourceName = sourceName;
        loop = false;
        capture = new VideoCapture();
        if (sourceName == "Webcam 1")
        {
            // set this up to test capturing audio source
            if (Utils.isPlatformWindows())
                capture.open(0);
            else
                capture.open("v4l2src ! video/x-raw, framerate=30/1, width=640, height=480, format=RGB ! videoconvert ! appsink");
        }
        
        if (sourceName == "Webcam 2")
        {
            capture.open(1);
        }
        
        if (sourceName == "HDMI Camera")
        {
            capture.open("decklinkvideosrc connection=2 mode=11 ! videoconvert ! appsink");
        }
        
        if (sourceName == "Monitor 1")
        {
            //capture.open("ximagesrc startx=0 use-damage=0 ! video/x-raw,framerate=30/1 ! videoscale method=0 ! video/x-raw,width=800,height=600 ! appsink");
            capture.open("ximagesrc startx=0 use-damage=0 ! video/x-raw,framerate=30/1 ! videoscale method=0 ! video/x-raw,width=800,height=600 ! videoconvert ! appsink");
        }
        
        if (sourceName == "Monitor 2")
        {
            final ObservableList<Screen> monitors = Screen.getScreens();
            if (monitors.size() > 1)
            {
                System.out.println("Number of displays: {0}" + monitors.size());
                Rectangle2D bounds = monitors.get(1).getBounds();
                capture.open("ximagesrc startx=" + bounds.getMinX() + " use-damage=0 ! video/x-raw,framerate=30/1 ! videoscale method=0 ! video/x-raw,width=800,height=600 ! appsink");
            }
        }
        
        if (sourceName == "Test Video")
        {
            loop = true;
            capture.open("test/test.avi");
        }
        
        if (!capture.isOpened())
        {
            System.err.println("Failed to open the capture source: " + sourceName);
        }
        
        return capture.isOpened();
    }
    
    void readNext()
    {
        if (queuedSourceName != null)
        {
            open(queuedSourceName);
            queuedSourceName = null;
        }
        
        if (frame == null)
        {
            System.err.println("Passed in frame is null!");
        }
        
        boolean hasNextFrame = capture.read(frame);
        
        // loop support for test video
        if (!hasNextFrame && loop)
        {
           if (open(sourceName))
           {
               readNext();
           }
        }
    }
    
    public Mat getFrame()
    {
        return frame;
    }
    
    public boolean updateImageView(ImageView view)
    {
        if (frame.width() <= 0 || frame.height() <= 0)
            return false;
        
        Image img = Utils.mat2Image(frame);
        Utils.onFXThread(view.imageProperty(), img);
        return true;
    }
}
