package application;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.util.Enumeration;

import org.opencv.core.Point;


/*
 * Contains calibration info for pan and tilt and send appropriate commands to servos connected to arduino
 */
public class PanAndTiltController
{
	public SerialClass serial = new SerialClass();
	
	public double webcamFieldOfView = 60.0;
        
        int curPanAngle = 45;
        
        long lastMoved = System.currentTimeMillis();
        
        final long LAST_MOVED_TIMEOUT = 100; // need to give servos time to move to position
	
	public void setup(MainController controller)
	{
            serial.initialize(controller);
            setPanAngle(0);
	}
	
	public void close()
	{
            serial.close();
	}	
        
        // angle is between -45 and 45
        // this method will convert it from 0 to 90
        public void setPanAngle(int angle)
        {
            angle += 45;
            curPanAngle = angle;
            System.out.println(curPanAngle);
            serial.writeData(Integer.toString(curPanAngle) + "s");
        }
	
	public void update(Point percentPoint)
	{
		double halfAngle = webcamFieldOfView / 2;
		double movePanAngle = (int) (-halfAngle + (percentPoint.x * webcamFieldOfView));
                
                
                
                // if there is less than 10 degrees of movement, ignore the movement
                //if (Math.abs(movePanAngle) < 10)
                //    return;
                
                movePanAngle *= 0.15; // dull the movement
                
                // need to give the servos time to get the camera into place
                if ((System.currentTimeMillis() - lastMoved) < LAST_MOVED_TIMEOUT)
                    return;
                
                lastMoved = System.currentTimeMillis();
                curPanAngle += movePanAngle;
                curPanAngle = Math.max(0, Math.min(90, curPanAngle));
                int intPanAngle = curPanAngle;
                System.out.println(intPanAngle);
		serial.writeData(Integer.toString(intPanAngle) + "s");
	}
}
