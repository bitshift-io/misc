package application;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SerialClass implements SerialPortEventListener {

	private static final String PORT_NAMES[] = { 
		    "tty.usbmodem", // Mac OS X
		    "ttyUSB", // Linux
                    "COM", // Windows
		};

	public SerialPort serialPort;
	
	public BufferedReader input;
	public OutputStream output;
	/** Milliseconds to block while waiting for port open */
	public static final int TIME_OUT = 2000;
	/** Default bits per second for COM port. */
	public static final int DATA_RATE = 9600;
        
        boolean ready = false;
        
        private ScheduledExecutorService timer;
        
        List<CommPortIdentifier> availablePorts = new ArrayList<CommPortIdentifier>();

        MainController controller;
        
        // check the port is conected
        Runnable updater = new Runnable() {

            @Override
            public void run()
            {		
                // if we connected to a port and have no ready message
                // after a timeout, then disconnect and move onto the next port
                if (!ready)
                {
                    availablePorts.remove(0);
                    
                    if (availablePorts.size() <= 0)
                    {
                        displayFailureError();
                        return;
                    }

                    tryConnectToPort(availablePorts.get(0));
                    timer.schedule(updater, 2000, TimeUnit.MILLISECONDS);
                }
            }
        };
            
	public void initialize(MainController cont) 
	{
            controller = cont; // better to supply an error listener
            
            // this runs in a thread to not block the app from starting
            Runnable init = new Runnable() {

                @Override
                public void run()
                {		
                    controller.setLeftStatus("Searching for ports...");
                    CommPortIdentifier portId = null;
                    Enumeration<CommPortIdentifier> portEnum = CommPortIdentifier.getPortIdentifiers();

                    //First, Find an instance of serial port as set in PORT_NAMES.
                    while (portEnum.hasMoreElements()) 
                    {
                            CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
                            for (String portName : PORT_NAMES) 
                            {
                                    if (currPortId.getName().contains(portName)) 
                                    {
                                        availablePorts.add(currPortId);
                                    }
                            }
                    }

                    if (availablePorts.size() <= 0)
                    {
                        displayFailureError();
                        return;
                    }

                    tryConnectToPort(availablePorts.get(0));
                    timer = Executors.newSingleThreadScheduledExecutor();
                    timer.schedule(updater, 2000, TimeUnit.MILLISECONDS);
                }
            };
        
            new Thread(init).start();
        }
        
        void displayFailureError()
        {
            String errorMsg = "Could not find COM port.";
            if (!Utils.isPlatformWindows())
            {
                errorMsg +=" Try: sudo chmod 666 /dev/ttyUSB0";
            }
            
            controller.displayError(errorMsg);
        }
 
        void tryConnectToPort(CommPortIdentifier portId)
        {
		if (portId == null) 
		{
			
			return;
		}

		try 
		{
                        controller.setLeftStatus("Trying to connect to " + portId.getName() + "...");
                        
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(),
			TIME_OUT);
		
			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
                        
			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE,
			SerialPort.DATABITS_8,
			SerialPort.STOPBITS_1,
			SerialPort.PARITY_NONE);
		
			// open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			output = serialPort.getOutputStream();
			//char ch = 1;
			//output.write(ch);
		
		} 
		catch (Exception e) 
		{
			System.err.println(e.toString());
		}
	}

	public synchronized void close() 
	{
		if (serialPort != null) 
		{
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	public synchronized void serialEvent(SerialPortEvent oEvent) 
	{
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) 
		{
			try 
			{
				String inputLine=input.readLine();
				System.out.println(inputLine);
                                if (inputLine.equals("Tracktor Ready"))
                                {
                                    ready = true;
                                    controller.setLeftStatus("Connected to " + availablePorts.get(0).getName());
                                    //writeData("45s"); // test to ensure we can write to port - and centre the servo
                                }
			} 
			catch (Exception e) 
			{
				System.err.println(e.toString());
			}
		}
	}

	public synchronized void writeData(String data)
	{
            if (!ready)
                return;

            //System.out.println("Sent: " + data);
            try 
            {
                    output.write(data.getBytes());
            } 
            catch (Exception e) 
            {
                    System.out.println("Could not write to port");
            }
	}

	/*
	public static void main(String[] args) throws Exception 
	{
		SerialClass main = new SerialClass();
		main.initialize();
		Thread t=new Thread() {
			public void run() {
			//the following line will keep this app alive for 1000 seconds,
			//waiting for events to occur and responding to them (printing incoming messages to console).
			try {Thread.sleep(1500);
			writeData("2");} catch (InterruptedException ie) {}
			}
			};
			t.start();
			System.out.println("Started");
		}
	}*/
}
