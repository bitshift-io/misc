package application;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/*
 * Class to track a user dragging the mouse selection and operating with it
 */
public class MouseDragRect implements EventHandler<MouseEvent> {

	private Point start = new Point();
	private Point end = new Point();	
	private boolean dragging = false;
	private boolean finished = false; // a variable so external code can determine when the user just finished dragging to do stuff
	
	private Node node;
	
	public void addEventHandler(Node node)
	{
		this.node = node;
		node.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
		node.addEventHandler(MouseEvent.MOUSE_RELEASED, this);
		node.addEventHandler(MouseEvent.MOUSE_DRAGGED, this);
	}
	
	public void handle(MouseEvent event)
	{
		if (event.getEventType() == MouseEvent.MOUSE_PRESSED)
		{
			dragging = true;
			finished = false;
			start.x = event.getX();
			start.y = event.getY();
			end.x = event.getX();
			end.y = event.getY();
		}
		
		if (event.getEventType() == MouseEvent.MOUSE_DRAGGED)
		{
			end.x = event.getX();
			end.y = event.getY();
		}
		
		if (event.getEventType() == MouseEvent.MOUSE_RELEASED)
		{
			end.x = event.getX();
			end.y = event.getY();
			dragging = false;
			finished = true;
		}
		
		//System.out.println("[" + event.getX() + ", " + event.getY() + "]"); 
        //event.consume();
	}
	
	public boolean isDragging()
	{
		return dragging;
	}
	
	public boolean isFinished()
	{
		return finished;
	}
	
	public void resetFinished()
	{
		finished = false;
	}
	
	/*
	 * Get top left rectangle position in Mat space
	 */
	public Point getTopLeft(Mat img)
	{
		Point topLeft = new Point(Math.min(start.x, end.x), Math.min(start.y, end.y));
		return transformPointFromNodeSpaceToMatSpace(img, topLeft);
	}
	
	/*
	 * Get bottom right rectangle position in Mat space
	 */
	public Point getBottomRight(Mat img)
	{
		Point bottomRight = new Point(Math.max(start.x, end.x), Math.max(start.y, end.y));
		return transformPointFromNodeSpaceToMatSpace(img, bottomRight);
	}
	
	private Point transformPointFromNodeSpaceToMatSpace(Mat img, Point point)
	{
		// start and end points are are in node space
		// and need to be converted to img space
		double nodeWidth = node.getBoundsInParent().getWidth();
		double nodeHeight = node.getBoundsInParent().getHeight();
		
		// convert to %
		Point pct = new Point(point.x / nodeWidth, point.y / nodeHeight);
		
		// convert % to img space
		Point imgPoint = new Point(pct.x * img.width(), pct.y * img.height());
		return imgPoint;
	}
	
	public void drawRect(Mat img, Scalar colour, int thickness)
	{
		//if (dragging)
		//{
			Point startImg = getTopLeft(img);
			Point endImg = getBottomRight(img);
			Imgproc.rectangle(img, startImg, endImg, colour, thickness);
		//}
	}
	
	public void fillPoly(Mat img, Scalar colour)
	{
		Point startImg = getTopLeft(img);
		Point endImg = getBottomRight(img);
		
		List<MatOfPoint> border = new ArrayList<MatOfPoint>();
		border.add(new MatOfPoint(startImg, new Point(startImg.x, endImg.y), endImg, new Point(endImg.x, startImg.y)));
		Imgproc.fillPoly(img, border, colour);
	}
}
