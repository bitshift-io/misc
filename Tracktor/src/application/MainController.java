package application;

import application.Tracking.ColourRangeTracking;
import application.Tracking.BackgroundSubtractorTracking;
import application.Tracking.FlowFieldTracking;
import application.Tracking.SmoothFaceTracking;
import java.io.File;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import org.opencv.videoio.VideoWriter;

public class MainController 
{
        @FXML
	private Label leftStatus;
        @FXML
	private Label rightStatus;
        
        // broadcast options
        @FXML
	private ComboBox primarySourceCombo;
        @FXML
	private ComboBox secondarySourceCombo;
        @FXML
	private ComboBox liveViewCombo;
        @FXML
	private ComboBox audioSourceCombo;
        @FXML
	private CheckBox broadcastDebug;
        @FXML
	private ImageView liveViewImage;
        @FXML
	private ImageView primarySourceImage;
        @FXML
	private ImageView secondarySourceImage;
        @FXML
	private Slider panSlider;
        @FXML
	private Slider zoomSlider;
        @FXML
	private ToggleButton recordButton;
        
        // tracking options
        @FXML
	private ComboBox trackingTypeCombo;
	@FXML
	private Slider cameraHeightSlider;
	@FXML
	private Slider cameraZoomSlider;
	@FXML
	private Slider anticipationSlider;
	@FXML
	public CheckBox trackingDebugCheck;
	@FXML
	public ImageView currentFrame;
	@FXML
	public ImageView cameraView; // what the camera "sees"
	@FXML
	public ImageView debugView; // debug
	
	public PanAndTiltController panAndTilt = new PanAndTiltController();
	
	public MouseDragRect currentFrameMouseDragRect;

	// a timer for acquiring the video stream
	private ScheduledExecutorService timer;
	// the OpenCV object that realizes the video capture
	private VideoCapture capture = new VideoCapture();
	// a flag to change the button behavior
	private boolean cameraActive = false;
	// the id of the camera to be used
	private static int cameraId = 0;
	
	boolean firstCurrentMatFrameValid = false;
	public Mat currentMatFrame = new Mat();

	// I think ideally I want a camera attached to each source
        // so it can have its own tracking and zoom settings etc...
	public VirtualCamera camera = new VirtualCamera();

        private CaptureSource primarySource = new CaptureSource();
        private CaptureSource secondarySource = new CaptureSource();

        List<TrackingTechnique> trackingTechniques = new ArrayList<TrackingTechnique>();
        
        final String PRIMARY_WITH_SECONDARY = "Primary and small Secondary";
        final String SECONDARY_WITH_PRIMARY = "Secondary and small Primary";
        final String PRIMARY = "Primary";
        final String SECONDARY = "Secondary";
        
        
        VideoWriter outputVideo = new VideoWriter();
        
	public void setup()
	{
            leftStatus.setText("");
            rightStatus.setText("");
            trackingDebugCheck.setSelected(true);
                    
            trackingTechniques.add(new BackgroundSubtractorTracking(this));
            trackingTechniques.add(new SmoothFaceTracking(this));
            trackingTechniques.add(new ColourRangeTracking(this));
            trackingTechniques.add(new FlowFieldTracking(this));
            
            
            for (TrackingTechnique t : trackingTechniques)
            {
                trackingTypeCombo.getItems().add(t.getDisplayName());
            }
                        
            trackingTypeCombo.getItems().add("None");
            trackingTypeCombo.getSelectionModel().select("None");  
            
            
            liveViewCombo.getItems().add(PRIMARY_WITH_SECONDARY);
            liveViewCombo.getItems().add(SECONDARY_WITH_PRIMARY);
            liveViewCombo.getItems().add(PRIMARY);
            liveViewCombo.getItems().add(SECONDARY);
            liveViewCombo.getSelectionModel().select(PRIMARY_WITH_SECONDARY);  
            
            
            primarySourceCombo.getItems().addAll(Arrays.asList(CaptureSource.getSourceNames()));
            secondarySourceCombo.getItems().addAll(Arrays.asList(CaptureSource.getSourceNames()));

            // attempt to find a working source
            if (primarySource.open("HDMI Camera"))
            {
                primarySourceCombo.getSelectionModel().select("HDMI Camera");
            }

            if (secondarySource.open("Monitor 2"))
            {
                secondarySourceCombo.getSelectionModel().select("Monitor 2");
            }
            else
            {
                secondarySource.open("Monitor 1");
                secondarySourceCombo.getSelectionModel().select("Monitor 1");
            }

            panAndTilt.setup(this);

            primarySourceCombo.valueProperty().addListener(new ChangeListener<String>() {
                @Override public void changed(ObservableValue ov, String t, String t1) {
                    primarySource.openNextRead(t1);
                }    
            });

            secondarySourceCombo.valueProperty().addListener(new ChangeListener<String>() {
                @Override public void changed(ObservableValue ov, String t, String t1) {
                    secondarySource.openNextRead(t1);
                }    
            });

            currentFrameMouseDragRect = new MouseDragRect();
            currentFrameMouseDragRect.addEventHandler(currentFrame);	
            
            // real camera pan
            panSlider.valueProperty().addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue<? extends Number> ov,
                    Number old_val, Number new_val) {
                    panAndTilt.setPanAngle(new_val.intValue());
                }
            });
            
            // real camera zoom
            zoomSlider.valueProperty().addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue<? extends Number> ov,
                    Number old_val, Number new_val) {
                    System.out.println("camera zoom not implemented yet, needs IR code");
                }
            });
            
            // virtual camera zoom
            cameraZoomSlider.valueProperty().addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue<? extends Number> ov,
                    Number old_val, Number new_val) {
                    camera.scale = new_val.doubleValue() / 100.0;
                }
            });
		
            cameraHeightSlider.valueProperty().addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue<? extends Number> ov,
                    Number old_val, Number new_val) {
                    camera.tiltLockedPercent = new_val.doubleValue() / 100.0;
                }
            });
					
            anticipationSlider.valueProperty().addListener(new ChangeListener<Number>() {
                public void changed(ObservableValue<? extends Number> ov,
                    Number old_val, Number new_val) {
                    camera.anticipation = new_val.doubleValue();
                }
            });
                
            // grab a frame every 40 ms (25 frames/sec)
            Runnable updater = new Runnable() {

                    @Override
                    public void run()
                    {						
                            update();
                    }
            };

            this.timer = Executors.newSingleThreadScheduledExecutor();
            this.timer.scheduleAtFixedRate(updater, 0, 40, TimeUnit.MILLISECONDS);
	}
        
        public void recordButtonToggled()
        {
            if (recordButton.isSelected())
            {
                recordButton.setText("RECORDING");
                if (!outputVideo.isOpened())
                {
                    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                    String fullFileName = System.getProperty("user.dir") + "\\" + date + ".mp4";
                    int index = 2;
                    while (new File(fullFileName).exists())
                    {
                        fullFileName = System.getProperty("user.dir") + "\\" + date + "_" + Integer.toString(index) + ".mp4";
                        ++index;
                    }
                    
                    // gstreamer:
                    // http://stackoverflow.com/questions/37339184/how-to-write-opencv-mat-to-gstreamer-pipeline
                    fullFileName = "appsrc ! videoconvert ! x264enc noise-reduction=10000 tune=zerolatency byte-stream=true threads=4 ! mpegtsmux ! udpsink host=localhost port=1234";
                    // to receive this stream in VLC:
                    // utp://@:1234
                    if (outputVideo.open(fullFileName, VideoWriter.fourcc('F','M','P','4'), 25, currentMatFrame.size()))
                    {                    
                        setRightStatus("Recording: " + fullFileName);
                    }
                    else
                    {
                        displayError("Failed to start recording");
                    }
                }
                else
                {
                    setRightStatus("Recording resumed");
                }
            }
            else
            {
                recordButton.setText("Resume Recording");
                setRightStatus("Recording paused");
            }
        }
        
        public void setLeftStatus(String msg)
        {
            // because we can be called from a different thread!
            Platform.runLater(() -> {
                leftStatus.setText(msg);
            });
        }
        
        public void setRightStatus(String msg)
        {
            // because we can be called from a different thread!
            Platform.runLater(() -> {
                rightStatus.setText(msg);
            });
        }
        
        public void displayError(String msg)
        {
            // because we can be called from a different thread!
            Platform.runLater(() -> {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(msg);
                //alert.setContentText(msg);
                alert.showAndWait().ifPresent(rs -> {
                    if (rs == ButtonType.OK) {
                        System.out.println("Pressed OK.");
                    }
                });
            });
        }

        protected void update()
        {
            // effectively grab and process a single frame
            //currentMatFrame = grabFrame();
            primarySource.readNext();
            secondarySource.readNext();
 
            primarySource.getFrame().copyTo(currentMatFrame);
            
            // setup the camera when we have our first valid currentMatFrame
            if (!firstCurrentMatFrameValid && currentMatFrame.width() > 0 && currentMatFrame.height() > 0)
            {
                firstCurrentMatFrameValid = true;
                camera.setup(currentMatFrame);
            }
                
            // different types of camera tracking:
            boolean trackingOccured = false;
            String selectedTrackingTypeStr = trackingTypeCombo.getSelectionModel().getSelectedItem().toString();
            for (TrackingTechnique t : trackingTechniques)
            {
                if (selectedTrackingTypeStr.equals(t.getDisplayName()))
                {
                    t.update();
                    trackingOccured = true;
                    break;
                }
            }
            
            if (!trackingOccured)
            {
                noTracking();
            }
                      
            updateImageViews();
        }
        
        void noTracking()
        {
            // disable the virtual camera
            camera.enabled = false;
        }
        
        void updateImageViews()
        {
            if (currentMatFrame.width() <= 0 || currentMatFrame.height() <= 0)
                return;
            
            String selectedLiveViewStr = liveViewCombo.getSelectionModel().getSelectedItem().toString();
            
           
            // in the primary display we actually are showing the tracking result
            // not the native primary source
            Mat cameraFrame = currentMatFrame.submat(camera.getFrame());
            Utils.updateImageView(primarySourceImage, Utils.mat2Image(cameraFrame));
            //primarySource.updateImageView(primarySourceImage);
            
            boolean secondarySourceValid = secondarySource.updateImageView(secondarySourceImage);
            
            // update live view
            Mat liveMat = new Mat();
            if (selectedLiveViewStr == PRIMARY_WITH_SECONDARY || selectedLiveViewStr == PRIMARY)
            {
                primarySource.getFrame().copyTo(liveMat);
            }
            if (secondarySourceValid && (selectedLiveViewStr == SECONDARY || selectedLiveViewStr == SECONDARY_WITH_PRIMARY))
            {
                secondarySource.getFrame().copyTo(liveMat);
            }
                
            // only stick in the picture in picture live view if needed
            if (selectedLiveViewStr == PRIMARY_WITH_SECONDARY || selectedLiveViewStr == SECONDARY_WITH_PRIMARY)
            {
                Mat secondaryMat = new Mat();
                if (secondarySourceValid && (selectedLiveViewStr == PRIMARY_WITH_SECONDARY))
                {
                    secondarySource.getFrame().copyTo(secondaryMat);
                }
                if (selectedLiveViewStr == SECONDARY_WITH_PRIMARY)
                {
                    currentMatFrame.copyTo(secondaryMat); // currentMatFrame is the primary source frame, but with camera tracking applied
                }

                if (secondaryMat.width() > 0 && secondaryMat.height() > 0)
                {
                    int pad = 10;

                    int width = 320;
                    int height = 240;

                    int x = pad; //liveMat.width() - (width + pad);
                    int y = liveMat.height() -(height + pad);
                    
                    // (3) Resizing and inserting an arbitrary grey image into the rgba camera frame
                    Imgproc.resize(secondaryMat, secondaryMat, new Size (width, height));
                    Mat submat = liveMat.submat(y, y + height, x, x + width);
                    secondaryMat.copyTo(submat);
                }
            }

            Utils.updateImageView(liveViewImage, Utils.mat2Image(liveMat));
            
            if (recordButton.isSelected())
            {
                outputVideo.write(liveMat);
            }
        }

		
	/**
	 * Stop the acquisition from the camera and release all the resources
	 */
	private void stopAcquisition()
	{
		if (this.timer!=null && !this.timer.isShutdown())
		{
			try
			{
				// stop the timer
				this.timer.shutdown();
				this.timer.awaitTermination(40, TimeUnit.MILLISECONDS);
			}
			catch (InterruptedException e)
			{
				// log any exception
				System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
			}
		}
		
		if (this.capture.isOpened())
		{
			// release the camera
			this.capture.release();
		}
	}
	
        public void quit()
	{
            setClosed();
        }
	
	/**
	 * On application close, stop the acquisition from the camera
	 */
	protected void setClosed()
	{
		panAndTilt.close();
		this.stopAcquisition();
                outputVideo.release();
                Platform.exit();
	}
}
