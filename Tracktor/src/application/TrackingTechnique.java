package application;

public interface TrackingTechnique {
    public String getDisplayName();
    public void update();
}
