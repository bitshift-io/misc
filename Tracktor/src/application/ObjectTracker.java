package application;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class ObjectTracker 
{
	private List<Rect> history = new LinkedList<Rect>();
	
	private int HISTORY_LENGTH = 60; // 1/30th of a frame = 2 second
	private double HISTORY_STEP = 1.0 / 30.0;
	
	//private Point position;
	//private Point centre = new Point();
	//private Point target = new Point();
	
	void track(Rect rect)
	{
		if (history.size() >= HISTORY_LENGTH)
		{
			history.remove(0);
		}
		
		history.add(rect.clone());
		
		/*
		
		Point prevCentre = new Point(centre.x, centre.y);
		centre.x = rect.x + rect.width/2;
		centre.y = rect.y + rect.height/2;
		
		// compute velocity of the moving object
		double dx = centre.x - prevCentre.x;
		double dy = centre.y - prevCentre.y;
		
		double LEAD = 10.0;
		target.x = centre.x + dx * LEAD;
		target.y = centre.y + dy * LEAD;
		
		if (position == null)
		{
			position = new Point(centre.x, centre.y);
		}*/
	}
	
	void update()
	{
		/*
		// move position towards target
		double dx = target.x - position.x;
		double dy = target.y - position.y;
		
		double STEP = 0.1;
		position.x += dx * STEP;
		position.y += dy * STEP;
		*/
	}
	
	public Rect getBounds()
	{
		if (history.size() <= 0)
			return new Rect();
		
		return history.get(history.size() - 1);
	}
	
	public Rect getAnticipatedBounds(double futureTime)
	{
		// compute velocity
		Point velocity = new Point(); // average velocity based on history
		Point pos = new Point(); // the centre of the current rectangle
		Point prevPos = new Point();
		int steps = 0;
		Point rectSize = new Point(); // used to compute average rectangle size based on history
		
		for (int i = 0; i < history.size(); ++i)
		{
			Rect h = history.get(i);
			pos.x = h.x + (h.width / 2);
			pos.y = h.y + (h.height / 2);
			
			if (i > 0)
			{
				rectSize.x += h.width;
				rectSize.y += h.height;
						
				velocity.x += pos.x - prevPos.x;
				velocity.y += pos.y - prevPos.y;
				++steps;
			}			
			
			prevPos.x = pos.x;
			prevPos.y = pos.y;
		}
		
		velocity.x /= steps;
		velocity.y /= steps;
		
		rectSize.x /= steps;
		rectSize.y /= steps;
		
		// at this stage pos = centre of last rectangle! perfect!
		double velocityTimeMultiplier = futureTime / HISTORY_STEP;
		Rect r = new Rect();
		r.x = (int) (pos.x + (velocity.x * velocityTimeMultiplier) - (rectSize.x / 2)); 
		r.y = (int) (pos.y + (velocity.y * velocityTimeMultiplier) - (rectSize.x / 2)); 
		r.width = (int) rectSize.x;
		r.height = (int) rectSize.y;
		
		return r;
	}
	
	void draw(Mat img)
	{
		/*
		int RADIUS = 10;
		Imgproc.circle(img, position, RADIUS + 100, new Scalar(0, 0, 255, 255) , 3); // red
		//Imgproc.circle(img, centre, RADIUS - 6, new Scalar(0, 255, 0, 255), 3); // green
		//Imgproc.circle(img, target, RADIUS, new Scalar(255, 0, 0, 255), 3); // blue
		
		//Imgproc.rectangle(img, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height), new Scalar(255, 0, 0, 255), 3); 
		 
		 */
	}
}
