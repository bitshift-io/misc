package application;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class VirtualCamera {

	private double aspectRatio = 9.0/16.0;
	private double safeFramePercent = 0.66;
	public double scale = 0.5;
	
	private Point centre = new Point();
	private Point target = new Point();
	
	private Rect parentRect = new Rect(); // the rect what this virtual camera is contrained too
	
	
	double tiltLockedPercent = 0.45; // if positive then the tilt is locked and will not allow the virtual camera to move up and down
	
	double anticipation = 0.25; //seconds
        
        public boolean enabled = false; // enable the virtual camera?
	
	private ObjectTracker tracker = new ObjectTracker();
		
	public void setup(Mat img)
	{		
		parentRect.width = img.width();
		parentRect.height = img.height();
		
		centre.x = parentRect.width / 2;
		centre.y = parentRect.height / 2;
		
		target.x = centre.x;
		target.y = centre.y;
		
		if (tiltLockedPercent >= 0.0)
		{
			centre.y = target.y = tiltLockedPercent * parentRect.height;
		}
	}
	
	public void track(Rect rect)
	{
		tracker.track(rect);
	}
	
	public void update()
	{
		// move position towards target - smoothly
		{
			double dx = target.x - centre.x;
			double dy = target.y - centre.y;
			
			double SMOOTHING_STEP = 0.1;
			centre.x += dx * SMOOTHING_STEP;
			centre.y += dy * SMOOTHING_STEP;
		}
		
		tracker.update();
		
		// get the anticipated bounds 1 second ahead of time, so the camera tracks ahead of the actor
		Rect actorBounds = tracker.getAnticipatedBounds(anticipation);
		Rect safeFrame = getSafeFrame();
		
		// if the actor is in the safe frame in the x axis, no need to move the camera
		if (actorBounds.x >= safeFrame.x && (actorBounds.x + actorBounds.width) <= (safeFrame.x + safeFrame.width))
		{
			return;
		}
		
		// the tracker doesnt care about restricting movement to within the parent frame
		// so we need to ensure "centre" is constrained to be inside parentRect
		target.x = actorBounds.x + (actorBounds.width / 2);
		target.y = actorBounds.y + (actorBounds.height / 2);
		
		if (tiltLockedPercent >= 0.0)
		{
			target.y = tiltLockedPercent * parentRect.height;
		}

		Rect unrestrictedFrame = getScaledFrame(target, scale);
		if (unrestrictedFrame.x < 0)
		{
			target.x += Math.abs(unrestrictedFrame.x);
		}
		
		if (unrestrictedFrame.y < 0)
		{
			target.y += Math.abs(unrestrictedFrame.y);
		}
		
		double rightSideX = unrestrictedFrame.x + unrestrictedFrame.width;
		if (rightSideX > parentRect.width)
		{
			target.x -= Math.abs(rightSideX - parentRect.width);
		}
		
		double rightSideY = unrestrictedFrame.y + unrestrictedFrame.height;
		if (rightSideY > parentRect.height)
		{
			target.y -= Math.abs(rightSideY - parentRect.height);
		}
	}
	
	private Rect getScaledFrame(Point position, double scale)
	{
            if (!enabled)
                return parentRect;
            
            double width = parentRect.width * scale;
            double height = width * aspectRatio;

            double halfWidth = width / 2;
            double halfHeight = height / 2;

            Rect rect = new Rect();
            rect.x = (int) Math.max(0, (position.x - halfWidth));
            rect.y = (int) Math.max(0,(position.y - halfHeight));
            rect.width = (int) Math.min(width, parentRect.width);
            rect.height = (int) Math.min(height, parentRect.height);

            return rect;
	}
	
	// Get centre of frame as a percent of parent rect
	public Point getFrameCentreAsPercent()
	{
		Point p = new Point();
		p.x = centre.x / parentRect.width;
		p.y = centre.y / parentRect.height;
		return p;
	}
	
	public Rect getFrame()
	{		
		return getScaledFrame(centre, scale);
	}
	
	public Rect getSafeFrame()
	{
		return getScaledFrame(centre, scale * safeFramePercent);
	}
	
	public void draw(Mat img)
	{
		tracker.draw(img);
		
		// draw the anticipated bounds
		Rect actorBounds = tracker.getAnticipatedBounds(anticipation);
		Imgproc.rectangle(img, new Point(actorBounds.x,actorBounds.y), new Point(actorBounds.x+actorBounds.width,actorBounds.y+actorBounds.height), new Scalar(200, 0, 0, 255), 3); 
		
		// draw the current bounds
		actorBounds = tracker.getBounds();
		Imgproc.rectangle(img, new Point(actorBounds.x,actorBounds.y), new Point(actorBounds.x+actorBounds.width,actorBounds.y+actorBounds.height), new Scalar(100, 0, 0, 255), 3); 
		
		
		Rect frame = getFrame();
		Imgproc.rectangle(img, new Point(frame.x,frame.y), new Point(frame.x+frame.width,frame.y+frame.height), new Scalar(0, 200, 0, 255), 3); 
		
		Rect safeFrame = getSafeFrame();
		Imgproc.rectangle(img, new Point(safeFrame.x,safeFrame.y), new Point(safeFrame.x+safeFrame.width,safeFrame.y+safeFrame.height), new Scalar(0, 100, 0, 255), 3); 
	}
	
}
