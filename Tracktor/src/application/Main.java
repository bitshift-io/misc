/*

Tracktor

Track-Actor - Track an actor on stage using a fixed webcam
Which then controls a servo hooked up to an Arduino which 
has a video camera on top.


ECLIPSE:

How to setup eclipse and opencv:
	http://docs.opencv.org/2.4/doc/tutorials/introduction/java_eclipse/java_eclipse.html

Install javafx eclipse plugin and scene builder:
	https://opencv-java-tutorials.readthedocs.io/en/latest/03-first-javafx-application-with-opencv.html
	
Based on this tutorial:
	https://opencv-java-tutorials.readthedocs.io/en/latest/03-first-javafx-application-with-opencv.html


NETBEANS:

for blackmagic support, install gstreamer - THIS MUST BE DONE BEFORE OPENCV:
    https://gstreamer.freedesktop.org/documentation/installing/on-linux.html
    http://askubuntu.com/questions/279509/how-can-i-install-gstreamer-1-0-in-ubuntu-12-04
    sudo apt-get install ubuntu-restricted-extras
    sudo apt install gstreamer1.0*
    sudo apt install libgstreamer-plugins-base1.0-dev

to install opencv on linux (ensure gstreamer is supported before you run make!):
        http://docs.opencv.org/2.4/doc/tutorials/introduction/desktop_java/java_dev_intro.html
        sudo apt install build-dep opencv
        cmake -DBUILD_SHARED_LIBS=OFF -DWITH_GSTREAMER=ON
        make -j8

configure netbeans project:
        https://www.codeproject.com/tips/717283/how-to-use-opencv-with-java-under-netbeans-ide

install scene builder from:
        http://gluonhq.com/labs/scene-builder/
	
	
Ensure dll's from C:\Apps\opencv\build\bin are added to C:\Apps\opencv\build\java\x64


For arduino control we need RXTX library:
	http://socialmaharaj.com/2013/05/03/arduino-serial-communication-using-java-and-rxtx/





*/


package application;
	
import java.lang.reflect.InvocationTargetException;
import java.net.URL;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import org.opencv.core.Core;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
                        URL url = getClass().getResource("Main.fxml");
			FXMLLoader loader = new FXMLLoader(url);
			
			Parent root = loader.load(); //FXMLLoader.load(getClass().getResource("Main.fxml"));
			Scene scene = new Scene(root); //, 800, 800);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
			
			// set the proper behavior on closing the application
			MainController controller = loader.getController();
			primaryStage.setOnCloseRequest((new EventHandler<WindowEvent>() {
				public void handle(WindowEvent we)
				{
					controller.setClosed();
				}
			}));
			
			controller.setup();
						
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {

            // load the native OpenCV library
            System.out.println(System.getProperty("java.library.path"));
            System.out.println(Core.NATIVE_LIBRARY_NAME);
            
            // if we get a crash here on windows, ensure -Djava.library.path="C:\\Projects\\Tracktor\\lib\\win64"
            // is added to the run command, or you add that to your java/path environment variable path
            // adding it via code does not work
            // for linux: /usr/local/share/OpenCV/java
            try
            {
                System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

                // windows fix for ffmpeg
                if (Utils.isPlatformWindows())
                {
                        System.loadLibrary("opencv_ffmpeg310_64"); // crash here means opencv_ffmpeg310_64.dll is not found
                }
            }
            catch (Throwable e)
            {
                System.err.println("Failed to load OpenCV library");
                
                if (Utils.isPlatformWindows())
                {
                    
                    System.err.println("Run with argument: -Djava.library.path=\"C:\\\\Projects\\\\Tracktor\\\\lib\\\\win64\"");
                    //System.setProperty("java.library.path", "C:\\\\Projects\\\\Tracktor\\\\lib\\win64;" + System.getProperty("java.library.path"));
                }
                else
                {
                    System.err.println("Run with argument: -Djava.library.path=/usr/local/share/OpenCV/java");
                    //System.setProperty("java.library.path", "/usr/local/share/OpenCV/java:" + System.getProperty("java.library.path"));
                }
                
                Platform.exit();
                return;
            }

            launch(args);
	}
}
