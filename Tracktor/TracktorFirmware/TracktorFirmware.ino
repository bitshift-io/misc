





/**
* Precision servo positioning.
*
* Uses timer1 in phase and frequency correct PWM mode
* Doesn't use interrupts, so should be immune to interrupt issues.
*
* Chris Parish 2010
*
*/


//The standard timings for servos are 1.5ms for center, 1ms and 2ms for the full extents of travel
//however, most modern servos can operate an extended range, this can be as wide as 0.5ms to 2.5ms
//Consult the documentation for your servo for further details
// http://www.servodatabase.com/servo/hitec/hs-311
#define SERVO_LOWER_LIMIT  1600
#define SERVO_UPPER_LIMIT  2600
#define SERVO_CENTER       2100

//Set your servo travel here. In standard mode most servos travel approx 90 degrees (45 either side of center)
#define SERVO_ANGLE_LOWER  0    
#define SERVO_ANGLE_UPPER  90

const int servo = 9;
int angle;

void setup()
{
 //Set up the timer------------------------
 //TCCR1A register
 TCCR1A = 0; //clear the register completly
 TCCR1A |= _BV(COM1A1) | _BV(COM1B1);  //set the required bits

 //TCCR1B register
 TCCR1B = 0; //clear the register completley  
 TCCR1B |= _BV(WGM13) | _BV(CS11);  //set the required bits

 //TCCR1C register
 TCCR1C = 0; //clear the register completley

 //Set the top limit - Complete frame length
 ICR1 = 20000;  //In this mode (assuming that you are running at 16mhz) The values are set in microseconds.

 //Set the output compare registers which dictate the servo position
 OCR1A = SERVO_CENTER; //1.5ms pulse, center the servo
 OCR1B = SERVO_CENTER; //1.5ms pulse, center the servo

 //Set pins 9 and 10 to output the waveform to the servos
 pinMode(9, OUTPUT);
 pinMode(10, OUTPUT);
 
 Serial.begin(9600);
 Serial.println("Tracktor Ready");
}

void setServoAngle(int servo, int angle)
{
 //constrain the angle measurement
 angle = constrain(angle, SERVO_ANGLE_LOWER, SERVO_ANGLE_UPPER);
 //convert the angle measurement into a pulse length
 angle = map(angle, SERVO_ANGLE_LOWER, SERVO_ANGLE_UPPER, SERVO_LOWER_LIMIT, SERVO_UPPER_LIMIT);

 //write the angle to the appropreate servo output (1 or 2  -aka- 9 or 10)
 if (servo == 1 || servo == 9)
 {
   OCR1A = angle;
 }
 else if (servo == 2 || servo == 10)
 {
   OCR1B = angle;
 }
}

int getServoAngle(int servo)
{
 int pulse;
 int angle;
   if (servo == 1 || servo == 9)
 {
   pulse = OCR1A;
 }
 else if (servo == 2 || servo == 10)
 {
   pulse = OCR1B;
 }
 return map(pulse, SERVO_LOWER_LIMIT, SERVO_UPPER_LIMIT, SERVO_ANGLE_LOWER, SERVO_ANGLE_UPPER);

}


void loop() {

  static int v = 0;

  if ( Serial.available()) {
    char ch = Serial.read();

    switch(ch) {
      case '0'...'9':
        v = v * 10 + ch - '0';
        break;
      case 's':
        Serial.println(v);
        setServoAngle(1, v);
        v = 0;
        break;
    }
  }

  //Servo::refresh();

} 
