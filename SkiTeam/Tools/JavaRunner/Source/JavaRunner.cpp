#ifndef __cplusplus
#define __cplusplus
#endif

#include "jni.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <direct.h>

#pragma comment (lib, "jvm.lib")

bool FindJAR(char* jarFile)
{
	char cwd[256];
	_getcwd(cwd, 256);

	char findFilter[512];
	sprintf(findFilter, "%s\\*.jar", cwd);

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind = FindFirstFile(findFilter, &FindFileData);

	if (hFind == INVALID_HANDLE_VALUE) 
		return false;

	strcpy(jarFile, FindFileData.cFileName);
	  
	FindClose(hFind);
}

int main(int argc, const char* args[])
{
	bool debug = false;

#ifdef DEBUG
	debug = true;
	/*
	if (argc > 1)
	{
		if (_stricmp((const char*)args[1], "debug") == 0)
			debug = true;
	}*/
#endif

	printf(args[0]);

	char cwd[256];
	_getcwd(cwd, 256);

	const char* exePathAndName = (const char*)args[0];
	const char* lastSlash = strrchr(exePathAndName, '\\');
	const char* exeAndExtensionName = lastSlash ? (lastSlash + 1) : exePathAndName;
	const char* extensionName = strrchr(exeAndExtensionName, '.');

	char exeName[256];
	memset(exeName, 0, 256);
	memcpy(exeName, exeAndExtensionName, extensionName - exeAndExtensionName);

	char* pathVar = getenv("PATH");

	char* newPathvar = new char[strlen(pathVar) + 2048];
	sprintf(newPathvar, "PATH=%s\\JRE\\bin\\client;%s\\JRE\\bin;%s", cwd, cwd, pathVar);
	if (putenv(newPathvar) == -1)
	{
		fprintf(stderr, "Failed to set path env variable");
		exit(1);
	}

	JavaVM *jvm;
	JNIEnv *env;

	JavaVMInitArgs vm_args;
	JavaVMOption options[4];

	options[0].optionString = "-Djava.compiler=NONE";

	char jarFile[512];
	char jarOption[512];
	if (FindJAR(jarFile))
	{
		sprintf(jarOption, "-Djava.class.path=%s", jarFile);
		options[1].optionString = jarOption;
	}
	else
	{
		"-Djava.classpath=.";
	}

	vm_args.nOptions = 2;

	if (debug)
	{
		options[vm_args.nOptions].optionString = "-verbose:jni";
		++vm_args.nOptions;
	}

	vm_args.version = JNI_VERSION_1_4;
	vm_args.options = options;
	vm_args.ignoreUnrecognized = JNI_TRUE;

	jint res = JNI_CreateJavaVM(&jvm, (void**)&env, &vm_args);
	if (res < 0) 
	{
		fprintf(stderr, "Can't create Java VM");
		exit(1);
	}

	// find a class with the same name as the exe
	// if not found look for a class called 'Main'
	jclass cls = env->FindClass(exeName);
	if (cls == NULL)
	{
		cls = env->FindClass("Desktop/Main");

		if (cls == NULL)
			cls = env->FindClass("Main");
	}

	if (cls != NULL)
	{
		// run the main function
		jmethodID get_main_id = env->GetStaticMethodID(cls, "main", "([Ljava/lang/String;)V");

		if (get_main_id != NULL)
		{
			jclass string = env->FindClass("java/lang/String");
			jobjectArray args = env->NewObjectArray(0,string, NULL);

			env->CallStaticVoidMethod(cls, get_main_id, args);
		}
		else
		{
			fprintf(stderr, "Failed to call 'public static void main(String arr[])'");
			exit(1);
		}
	}
	else
	{
		fprintf(stderr, "Failed to find class '%s'", exeName);
		exit(1);
	}

	jvm->DestroyJavaVM();

	delete[] newPathvar;
	return 0;
}

#ifdef RELEASE

int WINAPI WinMain(      
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,
    int nCmdShow)
{
	char appPath[MAX_PATH] = "";
	::GetModuleFileName(0, appPath, MAX_PATH);
	const char* pArgs[] = { (const char*)appPath };
	main(1, pArgs);
}

#endif