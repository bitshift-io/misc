package BlackCarbon.UI;

import BlackCarbon.API.Factory;
import BlackCarbon.API.Input;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;
import BlackCarbon.API.Input.Control;

public class UICursor extends UIComponent
{
	enum Action
	{
		LeftCursor,
		RightCursor,

		MAX,
	}
	
	public void Update(UIComponent active)
	{
		Input input = Factory.Get().GetInput();
		
		mPositionLastFrame = GetTranslation();
		/*
		float x = input.GetState(Control.MouseAxisX);
		float y = input.GetState(Control.MouseAxisY);
		Vector4 translation = new Vector4(x, y, 0.f);
		mTransform.SetTranslation(translation);
		*/
		
		// handle tab index
		int lastTabIndex = mTabIndex;
		
		if (mTabIndex < 0)
			mTabIndex = 0;
		
		if (input.WasPressed(Input.Control.KeyUp))
			--mTabIndex;
		
		if (input.WasPressed(Input.Control.KeyDown))
			++mTabIndex;
		
		if (lastTabIndex != mTabIndex)
		{
			int maxTabIndex = 0;
			for (int i = 0; i < active.mChild.size(); ++i)
			{
				int tabIndex = active.mChild.get(i).GetTabIndex();
				if (tabIndex > maxTabIndex)
					maxTabIndex = tabIndex;
			}
			++maxTabIndex;
			
			if (maxTabIndex > 0)
			{
				if (mTabIndex < 0)
					mTabIndex = maxTabIndex - 1;
				
				mTabIndex = mTabIndex % maxTabIndex;
				for (int i = 0; i < active.mChild.size(); ++i)
				{
					int tabIndex = active.mChild.get(i).GetTabIndex();
					if (tabIndex == mTabIndex)
					{
						Matrix4 transform = active.mChild.get(i).GetTransform();
						SetTranslation(transform.GetTranslation());
					}
				}
			}
			
			//System.out.println("tab index: " + mTabIndex);
		}
	}
	
	public boolean GetActionPressed(Action input)
	{
		switch (input)
		{
		case LeftCursor:
			return Factory.Get().GetInput().WasPressed(Input.Control.MouseLeft) ||
					Factory.Get().GetInput().WasPressed(Input.Control.KeyRight) ||
					Factory.Get().GetInput().WasPressed(Input.Control.KeyEnter);
			
		case RightCursor:
			return Factory.Get().GetInput().WasPressed(Input.Control.MouseRight);
		}
		
		return false;
	}
	
	public Vector4 GetTranslationLastFrame()
	{
		return mPositionLastFrame;
	}
	
	protected Vector4 mPositionLastFrame;
}
