package BlackCarbon.Ext;

import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.XMLSerializable;
import BlackCarbon.API.XMLSerialize;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.API.RenderBuffer;
import BlackCarbon.API.Texture;
import BlackCarbon.Math.Vector4;

//
// simplifies handling of multiple frames of animation from a texture atlas
//
public class Sprite implements XMLSerializable
{
	// Animation flags
	static public int AF_None				= 0;
	static public int AF_Horizontal			= (1 << 0);
	static public int AF_Vertical			= (1 << 1);
	static public int AF_NoLoop				= (1 << 2);
	static public int AF_Paused				= (1 << 3);
	
	public Sprite() //String texture, int numWidth, int numHeight)
	{/*
		Factory factory = Game.Get().GetFactory();
		
		// set up a background
		mGeometry = factory.CreateRenderBuffer();
		mGeometry.CreateQuad(null);
		mTexture = factory.AcquireTexture(texture);
		
		mNumWidth = numWidth;
		mNumHeight = numHeight;
		*/
		mTransform.SetIdentity();
	}
	
	public void ResetAnimation()
	{
		mTime = 0.f;
		mCurWidth = 0;
		mCurHeight = 0;
		mAnimationFlags = mAnimationFlags & ~AF_Paused;
	}
	
	public void Update(float deltaTime)
	{
		if (mAnimationFlags == AF_None || (mAnimationFlags & AF_Paused) != 0)
			return;
		
		// deal with sprite animation
		mTime += deltaTime;
		
		float frameTime = 1.f / mFrameRate;
		if (mTime > frameTime)
		{
			mTime -= frameTime;
			
			if ((mAnimationFlags & AF_Horizontal) != 0)
			{
				if ((mCurWidth + 1) >= mNumWidth)
				{
					if ((mAnimationFlags & AF_NoLoop) != 0)
						mAnimationFlags |= AF_Paused;
					else
						mCurWidth = 0;
				}
				else
				{
					++mCurWidth;
				}
			}
				
			if ((mAnimationFlags & AF_Vertical) != 0)
			{
				if ((mCurHeight + 1) >= mNumHeight)
				{
					if ((mAnimationFlags & AF_NoLoop) != 0)
						mAnimationFlags |= AF_Paused;
					else
						mCurHeight = 0;
				}
				else
				{
					++mCurHeight;
				}
			}
		}
	}
	
	public void Render()
	{
		Device device = Game.Get().GetFactory().GetDevice();
		
		// set new texture matrix to scroll the texture
		Vector4 translation = new Vector4((float) mCurWidth / (float)mNumWidth, (float)mCurHeight / (float)mNumHeight);
		Vector4 scale = new Vector4(1.f / (float)mNumWidth, 1.f / (float)mNumHeight, 1.f);
		Matrix4 t = new Matrix4();
    	t.SetIdentity();
    	t.SetScale(scale);
    	t.SetTranslation(translation);
    	device.SetMatrix(t, Device.MatrixMode.Texture);
    	
    	// set the world matrix, apply sprite scale here
    	Matrix4 transform = mTransform;
    	Matrix4 modelScale = new Matrix4(Matrix4.Init.Identity);
    	modelScale.SetScale(new Vector4(mScaleX, mScaleY, 1.f, 1.f));
    	transform = transform.MultiplyRotation(modelScale);
    	device.SetMatrix(transform, Device.MatrixMode.Model);
    	
    	// set device state - enable alpha blending
    	Device.DeviceState state = device.new DeviceState();
    	state.alphaBlendEnable = true;
    	state.alphaCompare = Device.Compare.Greater;
    	device.SetDeviceState(state);
    	
    	// bind texture and draw
    	mTexture.Bind();
    	mGeometry.Bind();
    	mGeometry.Render();
    	mGeometry.Unbind();
    	mTexture.Unbind();
		
		// restore original texture matrix
    	t.SetIdentity();
    	device.SetMatrix(t, Device.MatrixMode.Texture);
	}
	
	public void Resolve(XMLSerialize serialize)
	{
		if (mTextureName != null)
		{
			mTexture = Factory.Get().AcquireTexture(mTextureName);

			mGeometry = Factory.Get().CreateRenderBuffer();
			mGeometry.CreateQuad(null);
		}
	}

	public XMLSerializable Clone()
	{
		return null;
	}
	
	public int		mAnimationFlags 		= AF_None;
	
	public float	mTime					= 0.f;
	public float	mFrameRate				= 12.f;
	
	public int		mCurWidth				= 0;
	public int		mCurHeight				= 0;
	
	public int		mNumWidth;
	public int		mNumHeight;
	
	public float	mScaleX					= 1.f;
	public float	mScaleY					= 1.f;
	
	public String			mTextureName;
	private Texture 		mTexture;
	private RenderBuffer 	mGeometry;
	
	public Matrix4			mTransform		= new Matrix4();
}
