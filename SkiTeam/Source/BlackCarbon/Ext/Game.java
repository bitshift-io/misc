package BlackCarbon.Ext;

import java.util.concurrent.TimeUnit;

import BlackCarbon.API.Factory;
import BlackCarbon.Math.Math;

//
// extend from this class to automatically get your game running
//
public class Game// implements Runnable
{/*
	public void run() 
	{
		boolean bRunning = true;
        while (bRunning) 
        {
        	bRunning = Update();
        	
        	try 
        	{
        		TimeUnit.MILLISECONDS.sleep(5);
			} 
        	catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }*/
	
	//
	// call this to get your game running
	//
	public void Run()
	{
		mGame = this;


		boolean bRunning = Init();
		while (bRunning) 
        {
			long startTime = System.currentTimeMillis();

        	bRunning = Update();
        	Render();

        	
        	long endTime = System.currentTimeMillis();
        	long deltaTime = endTime - startTime;
        	float idealLoopTime = GetDeltaTime() * 1000.f; // lock frame rate to 30
        	long sleepTime = (long)Math.Max(0.f, idealLoopTime - (float)deltaTime);
        	
        	try 
        	{
        		TimeUnit.MILLISECONDS.sleep(sleepTime);
			} 
        	catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
        }
		
		System.exit(0);
	}
	
	public float GetDeltaTime()
    {
    	return 1.f / 30.f;
    }
	
	public synchronized boolean Init()
	{
		return true;
	}

	public synchronized boolean Update()
	{	
		++mUpdateCount;
		long curTime = System.currentTimeMillis();
		if (curTime >= (mUpdateTimer + 1000))
		{
			mUpdateTimer = curTime;
			mUpdateThisSecond = mUpdateCount;
			mUpdateCount = 0;
		}
		
		return GetFactory().Update();
	}
	
	public synchronized void Render()
	{
		++mRenderCount;
		long curTime = System.currentTimeMillis();
		if (curTime >= (mRenderTimer + 1000))
		{
			mRenderTimer = curTime;
			mRenderThisSecond = mRenderCount;
			mRenderCount = 0;
		}	
	}
	
	static public Game Get()
	{
		return mGame;
	}
	
	public Factory GetFactory()
	{
		return mFactory;
	}
	
	public boolean IsDebug()
	{
		return mDebug;
	}
	
	public boolean			mDebug					= false;
	
	public long				mRenderTimer			= 0;
	public int 				mRenderCount 			= 0;
	public int 				mRenderThisSecond 		= 0;
	
	public long				mUpdateTimer			= 0;
	public int 				mUpdateCount 			= 0;
	public int 				mUpdateThisSecond 		= 0;
	
	public Factory 			mFactory;
	public Thread			mThread;
	
	static public Game		mGame;
}

