package BlackCarbon.Math;

public class Line 
{
	public enum Type
	{
		Line,
		Segment,
		Ray
	}
	
	public Line(Vector4 v0, Vector4 v1, Type _type)
	{
		v[0] = v0;
		v[1] = v1;
		type = _type;
	}
	
	public Line ShortestSegmentBetweenTwoSegments(Line other, boolean clamp)
	{
		// http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline3d/
		Vector4 v1 = v[0];
		Vector4 v2 = v[1];
		Vector4 v3 = other.v[0];
		Vector4 v4 = other.v[1];

		Vector4 v13 = v1.Subtract(v3);
		Vector4 v43 = v4.Subtract(v3);

		if (Math.Abs(v43.GetX()) < Math.Epsilon && Math.Abs(v43.GetY()) < Math.Epsilon && Math.Abs(v43.GetZ()) < Math.Epsilon)
	      return null;

		Vector4 v21 = v2.Subtract(v1);

		if (Math.Abs(v21.GetX()) < Math.Epsilon && Math.Abs(v21.GetY()) < Math.Epsilon && Math.Abs(v21.GetZ()) < Math.Epsilon)
	      return null;

		float d1343 = v13.Dot(v43);
		float d4321 = v43.Dot(v21);
		float d1321 = v13.Dot(v21);
		float d4343 = v43.Dot(v43);
		float d2121 = v21.Dot(v21);

		float denom = d2121 * d4343 - d4321 * d4321;
		if (Math.Abs(denom) < Math.Epsilon)
			return null;
		
		float numer = d1343 * d4321 - d1321 * d4343;

		float mua = numer / denom;
		float mub = (d1343 + d4321 * mua) / d4343;

		if (clamp)
		{
			mua = Math.Clamp(0.f, mua, 1.f);
			mub = Math.Clamp(0.f, mub, 1.f);
		}
		
		// clamp locks to end points of line segments
		// put intersect time in to w component
		Vector4 pa = v1.Add(v21.Multiply(mua));
		pa.SetW(mua);
		
		Vector4 pb = v3.Add(v43.Multiply(mub));
		pb.SetW(mub);

		Line result = new Line(pa, pb, Line.Type.Segment);		
		return result;
	}
	
	public Vector4[] 	v 		= new Vector4[2];
	public Type			type;
}

