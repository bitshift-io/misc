package BlackCarbon.Math;

import java.io.Serializable;
import java.util.StringTokenizer;

public class Vector4 implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6327176359154383345L;
	public float[] v = new float[4];
	
	public Vector4()
	{
	}
	
	public Vector4(String stringValue)
	{
		StringTokenizer st = new StringTokenizer(stringValue, " ,");
		int index = 0;
		while (st.hasMoreTokens())
		{
			float value = Float.valueOf(st.nextToken().trim()).floatValue();
	 		v[index] = value;
	 		
			++index;
		}
	}
	
	public Vector4(float x, float y)
	{
		v[0] = x;
		v[1] = y;
		v[2] = 0.f;
		v[3] = 1.f;
	}
	
	public Vector4(float x, float y, float z)
	{
		v[0] = x;
		v[1] = y;
		v[2] = z;
		v[3] = 1.f;
	}
	
	public Vector4(float x, float y, float z, float w)
	{
		v[0] = x;
		v[1] = y;
		v[2] = z;
		v[3] = w;
	}
	
	public Vector4(Quaternion quaternion)
	{
		v[0] = quaternion.q[0];
		v[1] = quaternion.q[1];
		v[2] = quaternion.q[2];
		v[3] = quaternion.q[3];
	}
	
	public void Copy(Vector4 other)
	{
		v[0] = other.GetX();
		v[1] = other.GetY();
		v[2] = other.GetZ();
		v[3] = other.GetW();
	}
	
	public float Dot(Vector4 other)
	{
		return (GetX() * other.GetX()) + (GetY() * other.GetY()) + (GetZ() * other.GetZ()) + (GetW() * other.GetW());
	}
	
	public Vector4 Add(Vector4 other)
	{
		Vector4 r = new Vector4(v[0] + other.v[0], v[1] + other.v[1], v[2] + other.v[2], v[3] + other.v[3]);
		return r;
	}
	
	public Vector4 Subtract(Vector4 other)
	{
		Vector4 r = new Vector4(v[0] - other.v[0], v[1] - other.v[1], v[2] - other.v[2], v[3] - other.v[3]);
		return r;
	}
	
	public Vector4 Multiply(Vector4 other)
	{
		Vector4 r = new Vector4(v[0] * other.v[0], v[1] * other.v[1], v[2] * other.v[2], v[3] * other.v[3]);
		return r;
	}
	
	public Vector4 Multiply(float other)
	{
		Vector4 r = new Vector4(v[0] * other, v[1] * other, v[2] * other, v[3] * other);
		return r;
	}
	
	public Vector4 Divide(float other)
	{
		float inverseOther = 1.f / other;
		return Multiply(inverseOther);
	}
	
	public float MagnitudeSqrd()
	{
		return (v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]) + (v[3] * v[3]);
	}

	public float MagnitudeSqrd3()
	{
		return (v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]);
	}
	
	public float Magnitude3()
	{
		return Math.Sqrt(MagnitudeSqrd3());
	}
	
	public float Normalize()
	{
		float mag = MagnitudeSqrd();
		float invMagnitude = 1.f / mag;
		v[0] *= invMagnitude;
		v[1] *= invMagnitude;
		v[2] *= invMagnitude;
		v[3] *= invMagnitude;

		return mag;
	}
	
	public float Normalize3()
	{
		float mag = MagnitudeSqrd3();
		float invMagnitude = 1.f / mag;
		v[0] *= invMagnitude;
		v[1] *= invMagnitude;
		v[2] *= invMagnitude;
		v[3] *= invMagnitude;

		return mag;
	}
	
	public float GetX()
	{
		return v[0];
	}
	
	public float GetY()
	{
		return v[1];
	}
	
	public float GetZ()
	{
		return v[2];
	}
	
	public float GetW()
	{
		return v[3];
	}
	
	public void SetX(float x)
	{
		v[0] = x;
	}
	
	public void SetY(float y)
	{
		v[1] = y;
	}
	
	public void SetZ(float z)
	{
		v[2] = z;
	}
	
	public void SetW(float w)
	{
		v[3] = w;
	}
}

