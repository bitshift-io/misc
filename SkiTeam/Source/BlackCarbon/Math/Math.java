package BlackCarbon.Math;

public class Math
{
	static public float Epsilon = 0.001f;
	
	public static float Sqrt(float value)
	{
		return (float)java.lang.Math.sqrt(value);
	}
	
	public static float Abs(float value)
	{
		return (float)java.lang.Math.abs(value);
	}
	
	public static float Sin(float value)
	{
		return (float)java.lang.Math.sin(value);
	}
	
	public static float Cos(float value)
	{
		return (float)java.lang.Math.cos(value);
	}
	
	public static float ACos(float value)
	{
		return (float)java.lang.Math.acos(value);
	}
	
	public static float Tan(float value)
	{
		return (float)java.lang.Math.tan(value);
	}
	
	public static float DegToRad(float value)
	{
		return (float)java.lang.Math.toRadians(value);
	}
	
	public static float Clamp(float min, float value, float max)
	{
		if (value < min)
			return min;

		if (value > max)
			return max;

		return value;
	}
	
	public static float Floor(float value)
	{
		return (float)java.lang.Math.floor(value);
	}
	
	// get decimal value from float
	public static float Decimal(float value)
	{
		int integer = (int)value;
		float decimal = value - integer;
		return decimal;
	}
	
	public static float Max(float a, float b)
	{
		return (float)java.lang.Math.max(a, b);
	}
}
