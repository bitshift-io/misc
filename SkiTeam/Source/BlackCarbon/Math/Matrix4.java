package BlackCarbon.Math;

import java.util.StringTokenizer;

import BlackCarbon.Math.Matrix3;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;
import BlackCarbon.Math.Matrix4.Init;


/*
 * 	0 3 6
	1 4 7
	2 5 8
 */
class Matrix3
{
	public float[][] m = new float[3][3];
	
	float GetDeterminant()
	{
		return	(m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1])) -
				(m[1][0] * (m[0][1] * m[2][2] - m[0][2] * m[2][1])) +
				(m[2][0] * (m[0][1] * m[1][2] - m[1][1] * m[0][2]));
	}
}

/*
 * 	0 4 8  12
	1 5 9  13
	2 6 10 14
	3 7 11 15
 */
public class Matrix4 
{
	public enum Init
	{
		Identity,
		Zero,
		Mirror,
	}
	
	public float[][] m = new float[4][4];
	
	public Matrix4() 
	{
	}

	public Matrix4(Init type)
	{
		if (type == Init.Identity)
			SetIdentity();
		else if (type == Init.Zero)
			SetZero();
		else
			SetMirror();
	}
	
	public Matrix4(String stringValue)
	{
		System.out.println("Matrix4(String stringValue) - NYI");
		/*
		StringTokenizer st = new StringTokenizer(stringValue, " ,");
		
		int index = 0;
		while (st.hasMoreTokens())
		{
			float value = Float.valueOf(st.nextToken().trim()).floatValue();
	 		v[index] = value;
	 		
			++index;
		}*/
	}
	
	public Matrix4(Matrix4 other) 
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				m[i][j] = other.m[i][j];
			}
		}		
	}
	
	public void Copy(Matrix4 other)
	{
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				m[i][j] = other.m[i][j];
			}
		}
	}
	
	public Vector4 Multiply(Vector4 vector)
	{
		Vector4 result = new Vector4();
		
		result.v[0] = (m[0][0] * vector.v[0]) + (m[1][0] * vector.v[1]) + (m[2][0] * vector.v[2]) + (m[3][0] * vector.v[3]);
		result.v[1] = (m[0][1] * vector.v[0]) + (m[1][1] * vector.v[1]) + (m[2][1] * vector.v[2]) + (m[3][1] * vector.v[3]);
		result.v[2] = (m[0][2] * vector.v[0]) + (m[1][2] * vector.v[1]) + (m[2][2] * vector.v[2]) + (m[3][2] * vector.v[3]);
		result.v[3] = (m[0][3] * vector.v[0]) + (m[1][3] * vector.v[1]) + (m[2][3] * vector.v[2]) + (m[3][3] * vector.v[3]);
		
		return result;
	}
	
	public Matrix4 Multiply(Matrix4 other)
	{
		Matrix4 result = new Matrix4();
		float[][] m1 = m;
		float[][] m2 = other.m;
		
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				result.m[i][j] = m1[i][0]*m2[0][j] + m1[i][1]*m2[1][j] + m1[i][2]*m2[2][j] + m1[i][3]*m2[3][j];
			}
		}
		
		return result;
	}
	
	public Matrix4 MultiplyRotation(Matrix4 other)
	{
		Matrix4 result = new Matrix4(this);
		float[][] m1 = m;
		float[][] m2 = other.m;
		
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				result.m[i][j] = m1[i][0]*m2[0][j] + m1[i][1]*m2[1][j] + m1[i][2]*m2[2][j]; // + m1[i][3]*m2[3][j];
			}
		}
		
		return result;
	}
	
	public void SetIdentity()
	{		
		m[0][0] = m[1][1]  = m[2][2] = m[3][3] = 1.0f;
		
		m[0][1] = m[0][2] = m[0][3] =
			m[1][0] = m[1][2] = m[1][3] =
			m[2][0] = m[2][1] = m[2][3] =
			m[3][0] = m[3][1] = m[3][2] = 0.f;
	}
	
	public void SetZero()
	{
		m[0][0] = m[1][1]  = m[2][2] = m[3][3] = 0.0f;
		
		m[0][1] = m[0][2] = m[0][3] =
			m[1][0] = m[1][2] = m[1][3] =
			m[2][0] = m[2][1] = m[2][3] =
			m[3][0] = m[3][1] = m[3][2] = 0.f;
	}

	public void SetMirror()
	{
		m[1][1] = -1.f;
		m[0][0] =  m[2][2] = m[3][3] = 1.0f;
		
		m[0][1] = m[0][2] = m[0][3] =
			m[1][0] = m[1][2] = m[1][3] =
			m[2][0] = m[2][1] = m[2][3] =
			m[3][0] = m[3][1] = m[3][2] = 0.f;
	}
	
	public void SetRotateY(float angle)
	{
		float s = (float)Math.Sin(angle);
		float c = (float)Math.Cos(angle);

		m[0][0] = c;
		m[0][2] = s;
		m[2][0] = -s;
		m[2][2] = c;
	}

	public void SetRotateX(float angle)
	{		
		float s = (float)Math.Sin(angle);
		float c = (float)Math.Cos(angle);

		m[1][1] = c;
		m[1][2] = -s;
		m[2][1] = s;
		m[2][2] = c;
	}

	public void SetRotateZ(float angle)
	{
		float s = (float)Math.Sin(angle);
		float c = (float)Math.Cos(angle);

		m[0][0] = c;
		m[0][1] = -s;
		m[1][0] = s;
		m[1][1] = c;
	}
	
	public void SetTranslation(Vector4 translation)
	{
		m[3][0] = translation.v[0];
		m[3][1] = translation.v[1];
		m[3][2] = translation.v[2];
	}
	
	public void SetScale(Vector4 scale)
	{
		m[0][0] = scale.v[0];
		m[1][1] = scale.v[1];
		m[2][2] = scale.v[2];
	}

	public void SetOrthographic(float width, float height, float znear, float zfar)
	{
		float halfWidth = width * 0.5f;
		float halfHeight = height * 0.5f;
		SetOrthographic(-halfWidth, halfWidth, halfHeight, -halfHeight, znear, zfar);
	}

	public void SetOrthographic(float left, float right, float top, float bottom, float znear, float zfar)
	{
		SetIdentity();

		m[0][0] = 2.f / (right - left);
		m[1][1] = 2.f / (top - bottom);
		m[2][2] = -1.f / (zfar - znear); // m[10] = -2.f / (zfar - znear);

		//m[12] = -(right + left) / (right - left);
		//m[13] = -(top + bottom) / (top - bottom);
		//m[14] = (znear + zfar) / (znear - zfar);

		m[3][2] = znear / (znear - zfar);

	/*
		2/w  0    0           0
		0    2/h  0           0
		0    0    1/(zf-zn)   zn/(zn-zf)
		0    0    0			  1
		*/
	}

	public void SetPerspective(float fovy, float aspect, float znear, float zfar)
	{
		SetZero();

		float yScale = (float) (1.0f / Math.Tan(Math.DegToRad(fovy / 2.0f)));
		float xScale = yScale / aspect;

		m[0][0]  = xScale;
		m[1][1]  = yScale;
		m[2][2] = zfar / (znear - zfar);
		//m[10] = (zfar + znear) / (znear - zfar);

		m[2][3] = -1.0f;
		m[3][2] = (zfar * znear) / (znear - zfar);
		//m[14] = (2.0f * zfar * znear) / (znear - zfar);
	}
	
	public Vector4 GetXAxis()
	{
		return GetColumn(0);
	}
	
	public Vector4 GetYAxis()
	{
		return GetColumn(1);
	}
	
	public Vector4 GetZAxis()
	{
		return GetColumn(2);
	}

	public Vector4 GetTranslation()
	{
		return GetColumn(3);
	}
	
	public Vector4 GetColumn(int index)
	{
		return new Vector4(m[index][0], m[index][1], m[index][2], m[index][3]);
	}
	
	public void SetRow(int index, Vector4 row)
	{
		m[0][index] = row.GetX();
		m[1][index] = row.GetY();
		m[2][index] = row.GetZ();
		m[3][index] = row.GetW();
	}
	
	public void CreateView(Matrix4 world)
	{
		Vector4 right = world.GetXAxis();
		Vector4 up = world.GetYAxis();
		Vector4 view = world.GetZAxis();
		Vector4 position = world.GetTranslation();
		
		view = view.Multiply(-1.f);

		SetIdentity();

		// set the camera view matrix
		SetRow(0, right);
		SetRow(1, up);
		SetRow(2, view);
		SetTranslation(new Vector4(-position.Dot(right), -position.Dot(up), -position.Dot(view), 1.f));
		/*
		m[ 0] = right.GetX();
		m[ 4] = right.GetY();
		m[ 8] = right.GetZ();

		m[ 1] = up.GetX();
		m[ 5] = up.GetY();
		m[ 9] = up.GetZ();

		m[ 2] = -view.GetX();
		m[ 6] = -view.GetY();
		m[10] = -view.GetZ();

		m[12] = -position.Dot(right);
		m[13] = -position.Dot(up);
		m[14] = -position.Dot(view);
*/
	}
	
	Matrix4 GetAdjoint()
	{
		// http://www.cs.virginia.edu/~cs551dl/assignment3/matinv.c

		Matrix4 ret = new Matrix4();
		Matrix3 mat3;

		// we are effectively doing a transpose here as well

		/*
		 * 	0 4 8  12
			1 5 9  13
			2 6 10 14
			3 7 11 15
		 */
		
		// column 0
		mat3 = GetMatrix3(0, 0);
		ret.m[0][0] = mat3.GetDeterminant();

		mat3 = GetMatrix3(1, 0);
		ret.m[1][0] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(2, 0);
		ret.m[2][0] = mat3.GetDeterminant();

		mat3 = GetMatrix3(3, 0);
		ret.m[0][3] = -mat3.GetDeterminant();

		// column 1
		mat3 = GetMatrix3(0, 1);
		ret.m[0][1] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(1, 1);
		ret.m[1][1] = mat3.GetDeterminant();

		mat3 = GetMatrix3(2, 1);
		ret.m[2][1] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(3, 1);
		ret.m[3][1] = mat3.GetDeterminant();

		// column 2
		mat3 = GetMatrix3(0, 2);
		ret.m[0][2] = mat3.GetDeterminant();

		mat3 = GetMatrix3(1, 2);
		ret.m[0][2] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(2, 2);
		ret.m[2][2] = mat3.GetDeterminant();

		mat3 = GetMatrix3(3, 2);
		ret.m[3][2] = -mat3.GetDeterminant();

		// column 3
		mat3 = GetMatrix3(0, 3);
		ret.m[0][3] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(1, 3);
		ret.m[1][3] = mat3.GetDeterminant();

		mat3 = GetMatrix3(2, 3);
		ret.m[2][3] = -mat3.GetDeterminant();

		mat3 = GetMatrix3(3, 3);
		ret.m[3][3] = mat3.GetDeterminant();

		return ret;
	}
	
	// gets a matrix and skips the row/col passed in
	Matrix3 GetMatrix3(int row, int col)
	{
		Matrix3 mat = new Matrix3();
		
		for (int c = 0, i = 0; c < 4; ++c, ++i)
		{
			if (c == col)
			{
				++c;

				if (c >= 4)
					break;
			}

			for (int r = 0, j = 0; r < 4; ++r, ++j)
			{
				if (r == row)
				{
					++r;

					if (r >= 4)
						break;
				}

				mat.m[i][j] = m[c][r];
			}
		}

		return mat;
	}
	
	float GetDeterminant()
	{
		return (m[0][0] * m[1][1] * m[2][2] * m[3][3]) + (m[0][0] * m[1][2] * m[2][3] * m[3][1]) + (m[0][0] * m[1][3] * m[2][1] * m[3][2])
			 + (m[0][1] * m[1][0] * m[2][3] * m[3][2]) + (m[0][1] * m[1][2] * m[2][0] * m[3][3]) + (m[0][1] * m[1][3] * m[2][2] * m[3][0])
			 + (m[0][2] * m[1][0] * m[2][1] * m[3][3]) + (m[0][2] * m[1][1] * m[2][3] * m[3][0]) + (m[0][2] * m[1][3] * m[2][0] * m[3][1])
			 + (m[0][3] * m[1][0] * m[2][2] * m[3][1]) + (m[0][3] * m[1][1] * m[2][0] * m[3][2]) + (m[0][3] * m[1][2] * m[2][1] * m[3][0])
			 - (m[0][0] * m[1][1] * m[2][3] * m[3][2]) - (m[0][0] * m[1][2] * m[2][1] * m[3][3]) - (m[0][0] * m[1][3] * m[2][2] * m[3][1])
			 - (m[0][1] * m[1][0] * m[2][2] * m[3][3]) - (m[0][1] * m[1][2] * m[2][3] * m[3][0]) - (m[0][1] * m[1][3] * m[2][0] * m[3][2])
			 - (m[0][2] * m[1][0] * m[2][3] * m[3][1]) - (m[0][2] * m[1][1] * m[2][0] * m[3][3]) - (m[0][2] * m[1][3] * m[2][1] * m[3][0])
			 - (m[0][3] * m[1][0] * m[2][1] * m[3][2]) - (m[0][3] * m[1][1] * m[2][2] * m[3][0]) - (m[0][3] * m[1][2] * m[2][0] * m[3][1]);
	}
	
	public Matrix4 GetInverse()
	{
		Matrix4 ret = new Matrix4();
		Matrix4 adjoint = GetAdjoint();
		float invDet = 1.0f / GetDeterminant();

		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				ret.m[i][j] = invDet * adjoint.m[i][j];
			}
		}

		return ret;
	}
	
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder("[ ");
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				builder.append(m[i][j]);
				builder.append(" ");
			}
			
			if (i < 2)
				builder.append("\n  ");
		}
		builder.append(" ]");
		return builder.toString();
	}
}
