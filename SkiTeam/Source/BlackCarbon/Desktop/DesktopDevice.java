package BlackCarbon.Desktop;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ARBTransposeMatrix;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;


import BlackCarbon.API.Device;
import BlackCarbon.API.Device.GeometryTopology;
import BlackCarbon.API.Device.MatrixMode;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;
import BlackCarbon.Ext.Game;

public class DesktopDevice extends Device
{	
	
	
	public DesktopDevice(DesktopFactory.InitParam param)
	{
		
		try
		{
			Display.setDisplayMode(new DisplayMode(param.width, param.height));
			Display.setTitle(param.windowName);
			Display.create();
			
			
			
			SetWindowVisible(param.windowVisible);

			  
			/*
			Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() 
				{
					public void uncaughtException(Thread t, Throwable e) 
					{
						System.out.println("[UncaughtException] Exception: " + e.getMessage());
					}
			    });
			
			AWTExceptionHandler.RegisterExceptionHandler();
			*/
			mFactory = param.factory;
			/*
			mFrame = new Frame(param.windowName);
			
			GLCapabilities caps = new GLCapabilities();
			
			mCanvas = new GLCanvas(caps);
			mGL = mCanvas.getGL();
			
			//	GLDrawableFactory.getFactory().chooseGraphicsConfiguration(null, null, null);
				//new GLCanvas(new GLCapabilities()); //GLDrawableFactory.getFactory().. .createGLCanvas(new GLCapabilities());
			
			mGLU = new GLU();
			mCanvas.addGLEventListener(this);
			
			// make events come back to this class
			DesktopInput input = (DesktopInput)mFactory.GetInput();
			
			// hrmm this really should be in input
			mCanvas.addKeyListener(input);
			mCanvas.addMouseListener(input);
			mCanvas.addMouseMotionListener(input);
			/*
			// add an event listener for the "close window" event
			mFrame.addWindowListener(new WindowAdapter() 
			{
				public void windowClosing(WindowEvent e) 
				{
					System.exit(0);
				}
			});
			
			
			
			mFrame.add(mCanvas);
			mFrame.setSize(param.width, param.height);
			mFrame.setBackground(Color.white);
			
			SetWindowVisible(param.windowVisible);
			mFrame.toFront();
			mCanvas.requestFocus();
				*/

			
			//Thread.currentThread().sleep(1000);//sleep for 1000 ms
			
			//while (mInit == false)
			//{
			//	TimeUnit.MILLISECONDS.sleep(1000);
			//} 
			
			//
			//mCanvas.display(); // force OGL to initialise
			//mFrame.repaint();
			
			//mFrame.
			//mFrame.paintAll(mFrame.getGraphics());
			//Rectangle r = mFrame.getGraphics() .bounds();
			//PaintEvent event = new PaintEvent(null, 0, r);
			//mFrame.dispatchEvent(event);
			System.out.println("ogl should hopefully be inited");
			
			System.out.println("ogl init, textures now loadable");
			//Game.Get().Init();
			
			
			
			//mTexture = mFactory.AcquireTexture("actor_sprite");
			

	  //      final GLU glu = glDrawable.getGLU();

			GL11.glShadeModel(GL11.GL_SMOOTH);              // Enable Smooth Shading
	        
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glLoadIdentity();
	 //       glu.gluOrtho2D(-1.0f, 1.0f, -1.0f, 1.0f); // drawing square
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			GL11.glLoadIdentity();
	        
			GL11.glClearColor(0.2f, 0.2f, 0.5f, 1.0f);
			GL11.glClearDepth(1.0);                    // Enables Clearing Of The Depth Buffer
			GL11.glEnable(GL11.GL_DEPTH_TEST);           // Enables Depth Testing
			GL11.glDepthFunc(GL11.GL_LEQUAL);            // The Type Of Depth Test To Do
			GL11.glDisable(GL11.GL_LIGHTING);
		}
		catch (Exception e)
		{
			System.out.println("[DesktopDevice::DesktopDevice] Exception: " + e.getMessage());
		}
	}
	
	public void SetWindowVisible(boolean visible)
	{
		//Display.
		//mFrame.setVisible(visible);
	}
	
	public boolean Update()
	{
		Begin(); // to bind context to thread
		GL11.glViewport(0, 0, GetWidth(), GetHeight()); // update the viewport;
		Display.update();
		boolean result = !Display.isCloseRequested();
		End();
		return result;
	}
	
	public void Clear(int clearFlags)
	{
        int flags = 0;
        if ((clearFlags & CF_Colour) != 0)
        	flags |= GL11.GL_COLOR_BUFFER_BIT;
        
        if ((clearFlags & CF_Depth) != 0)
        	flags |= GL11.GL_DEPTH_BUFFER_BIT;
        
        
        GL11.glClear(flags);
	}
	
	public void Begin()
	{
		AcquireSemaphore();
		
		try 
		{
			//Display.releaseContext();
			Display.makeCurrent();
		} 
		catch (Exception e)
		{
			System.out.println("[DesktopDevice::Begin] Exception: " + e.getMessage());
		}
		
		// restore default rendering
		Matrix4 identity = new Matrix4(Matrix4.Init.Identity);
		SetMatrix(identity, MatrixMode.Model);
		SetMatrix(identity, MatrixMode.Texture);
		SetMatrix(identity, MatrixMode.View);
		SetMatrix(identity, MatrixMode.Projection);
		
	}
	
	public void End()
	{
		try
		{
			Display.releaseContext();
		} 
		catch (Exception e)
		{
			System.out.println("[DesktopDevice::End] Exception: " + e.getMessage());
		}
		
		ReleaseSemaphore();
	}
	
	public void SetMatrix(Matrix4 matrix, MatrixMode mode)
	{
		switch (mode)
		{
		case Texture:
			mTexture = matrix;
			GL11.glMatrixMode(GL11.GL_TEXTURE);
			break;
			
		case Model:
		{
			mWorld = matrix;
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			matrix = mWorld.Multiply(mView); //mView.Multiply(mWorld);
			break;
		}
		
		case View:
		{
			mView = matrix;
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			matrix = mWorld.Multiply(mView); //mView.Multiply(mWorld);
			break;
		}
		
		case Projection:
			mProjection = matrix;
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			break;
		}
		
		FloatBuffer fb = FloatBuffer.allocate(16);
		
		fb.put(matrix.m[0]);
		fb.put(matrix.m[1]);
		fb.put(matrix.m[2]);
		fb.put(matrix.m[3]);
		
		fb.position(0);
		
		GL11.glLoadMatrix(fb);
	}
	
	public Matrix4 GetMatrix(MatrixMode mode)
	{
		switch (mode)
		{
		case Texture:
			return mTexture;
			
		case Model:
			return mWorld;
		
		case View:
			return mView;
		
		case Projection:
			return mProjection;
		}
		
		return null;
	}
	
	
	/*
	public void display(GLAutoDrawable glDrawable) 
	{
		//if (!mInit)
		//	return;
		
		GL gl = GetGL();
		gl.glLoadIdentity();
		
		//mTexture.Bind();
		Game.Get().Render();
	}
	*/
	public int GetWidth()
	{
		return Display.getDisplayMode().getWidth();
	}
	
	public int GetHeight()
	{
		return Display.getDisplayMode().getHeight();
	}
	
	
	public void SetDeviceState(DeviceState state)
	{
		if (state.alphaBlendEnable)
			GL11.glEnable(GL11.GL_BLEND);
		else
			GL11.glDisable(GL11.GL_BLEND);
			
		int alphaBlendSource = GetBlendMode(state.alphaBlendSource);
		int alphaBlendDest = GetBlendMode(state.alphaBlendDest);
		GL11.glBlendFunc(alphaBlendSource, alphaBlendDest);
		
		if (state.alphaCompare == Device.Compare.Always)
			GL11.glDisable(GL11.GL_ALPHA_TEST);
		else
			GL11.glEnable(GL11.GL_ALPHA_TEST);
		
		int alphaCompare = GetCompare(state.alphaCompare);
		GL11.glAlphaFunc(alphaCompare, state.alphaCompareValue);
	}
	
	int GetCompare(Compare value)
	{/*
		public enum Compare
		{
			Less,
			LessEqual,
			Equal,
			Greater,
			GreaterEqual,
			Always, // TODO: fix device enums
			Never,
			NotEqual,
		}*/	
		
		int lookup[] = {
				GL11.GL_LESS, 
				GL11.GL_LEQUAL,
				GL11.GL_EQUAL, 
				GL11.GL_GREATER, 
				GL11.GL_GEQUAL,
				GL11.GL_ALWAYS,
				GL11.GL_NEVER,
				GL11.GL_NOTEQUAL, 
		};
		
		return lookup[value.ordinal()];
	}
	
	int GetBlendMode(BlendMode value)
	{/*
		public enum BlendMode
		{
			Zero,
			One,
			DestColour,
			SrcColour,
			OneMinusDestColour,
			OneMinusSrcColour,
			SrcAlpha,
			OneMinusSrcAlpha,
			DestAlpha,
			OneMinusDestAlpha,
			SrcAlphaSaturate,
		}
		*/
		int lookup[] = {
				GL11.GL_ZERO, 
				GL11.GL_ONE,
				GL11.GL_DST_COLOR, 
				GL11.GL_SRC_COLOR, 
				GL11.GL_ONE_MINUS_DST_COLOR,
				GL11.GL_ONE_MINUS_SRC_COLOR,
				GL11.GL_SRC_ALPHA,
				GL11.GL_ONE_MINUS_SRC_ALPHA, 
				GL11.GL_DST_ALPHA,
				GL11.GL_ONE_MINUS_DST_ALPHA, 
				GL11.GL_SRC_ALPHA_SATURATE
		};
		
		return lookup[value.ordinal()];
	}
	
	int GetTopology(GeometryTopology value)
	{
		/*
		public enum GeometryTopology
	{
		LineList,
		PointList,
		TriangleList,
		TriangleListAdjacent,
	}
	
		 */
		int lookup[] = {
				GL11.GL_LINES, 
				GL11.GL_POINT,
				GL11.GL_TRIANGLES, 
				//GL11.GL_TRIANGLES_ADJACENCY_EXT, 
				GL11.GL_TRIANGLE_STRIP,
				GL11.GL_QUADS,
		};
		
		return lookup[value.ordinal()];
	}
	
	public DeviceState GetDeviceState()
	{
		return null;
	}
	
	public void DrawLine(Vector4 start, Vector4 end, Vector4 colour)
	{
		GL11.glBegin(GL11.GL_LINES);
		
		if (colour != null)
			GL11.glColor4f(colour.GetX(), colour.GetY(), colour.GetZ(), 1.0f);
		else
			GL11.glColor4f(1.f, 1.f, 1.f, 1.f);
		
		GL11.glVertex3f(start.GetX(), start.GetY(), start.GetZ());
		GL11.glVertex3f(end.GetX(), end.GetY(), end.GetZ());
		GL11.glEnd();
		GL11.glColor4f(1.f, 1.f, 1.f, 1.f);
	}
	
	public void Primitive_Begin(GeometryTopology topology)
	{
		GL11.glBegin(GetTopology(topology));
	}
	
	public void Primitive_End()
	{
		GL11.glEnd();
	}
	
	public void Primitive_Colour(Vector4 value)
	{
		GL11.glColor4f(value.GetX(), value.GetY(), value.GetZ(), value.GetW());
	}
	
	public void Primitive_Vertex(Vector4 value)
	{
		GL11.glVertex3f(value.GetX(), value.GetY(), value.GetZ());//, value.GetW());
	}
	
	public void Primitive_TexCoord(Vector4 value)
	{
		GL11.glTexCoord4f(value.GetX(), value.GetY(), value.GetZ(), value.GetW());
	}

	private Matrix4		mView					= new Matrix4(Matrix4.Init.Identity);
	private Matrix4		mWorld					= new Matrix4(Matrix4.Init.Identity);
	private Matrix4		mTexture				= new Matrix4(Matrix4.Init.Identity);
	private Matrix4		mProjection				= new Matrix4(Matrix4.Init.Identity);
}
