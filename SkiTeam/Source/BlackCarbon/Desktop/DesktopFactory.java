package BlackCarbon.Desktop;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import BlackCarbon.API.*;
import BlackCarbon.API.Factory.InitParam;

public class DesktopFactory extends Factory
{
	static public class InitParam extends Factory.InitParam
	{		

	}
	
	public DesktopFactory(InitParam param)
	{	
		if (param == null)
			param = new InitParam();
			
		param.factory = this;
		mInitParam = param;
		
		mInput = new DesktopInput();
		mInput.mFactory = this;
		
		mNetwork = new Network();
		
		
		mDevice = new DesktopDevice(param);
	}
	
	public InitParam GetInitParam()
	{
		return mInitParam;
	}
	
	public RenderBuffer CreateRenderBuffer()
	{
		RenderBuffer resource = new DesktopRenderBuffer();
		resource.mFactory = this;
		return resource;
	}
	
	public Texture AcquireTexture(String name)
	{
		Texture resource = new DesktopTexture();
		resource.mFactory = this;
		resource.mName = name;
		resource.Load();
		return resource;
	}
	
	public InputStream GetFileResource(String name, String extension)
	{
		String resourceDir = mFactory.GetInitParam().resourceDir;
		String fileName = name + "_" + extension + "." + extension;
		if (resourceDir != null && resourceDir.length() > 0)
			fileName = resourceDir + "/" + fileName;
		
		InputStream is = null;
		try 
		{
			is = new FileInputStream(fileName);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		return is;
	}
	
	protected InitParam	mInitParam;
}
