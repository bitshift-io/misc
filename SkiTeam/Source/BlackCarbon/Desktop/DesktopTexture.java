package BlackCarbon.Desktop;

import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;


import BlackCarbon.API.Texture;

public class DesktopTexture extends Texture
{
	public void Load()
	{
		//TextureReader.Texture texture = 
        //    TextureReader.readTexture("demos/data/images/glass.png");
		
		DesktopDevice device = (DesktopDevice)mFactory.GetDevice();
		
		int width = 0;
		int height = 0;
		int pixels[];
		
		try
		{
			String resourceDir = mFactory.GetInitParam().resourceDir;
			
			File f = new File(resourceDir + "/" + mName + "_tex.png");
			if (!f.exists())
				return;
			
			BufferedImage bi = ImageIO.read(f);
			width = bi.getWidth();
			height = bi.getHeight();
			
			
			pixels = bi.getRGB(0, 0, width, height, null, 0, width);  
			
			// this is in the format ARGB and i need to convert it 
			// to ABGR 
			for (int i = 0; i < pixels.length; ++i)
			{				
				int r = (pixels[i] >> 16) & 0x000000ff;
				int b = pixels[i] & 0x000000ff;
				
				pixels[i] &= 0xff00ff00;
				pixels[i] |= r;
				pixels[i] |= (b << 16);
			}
			
		}
		catch (Exception e)
		{
			System.out.println("[DesktopTexture::Load1] Exception: " + e.getMessage());
			return;
		}
		
		try
		{
			device.Begin();

			IntBuffer ib = ByteBuffer.allocateDirect(4).order(ByteOrder.nativeOrder()).asIntBuffer();
			GL11.glGenTextures(ib);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, ib.get(0));
			mID = ib.get(0);
			
			IntBuffer tbuf = IntBuffer.wrap(pixels);
			tbuf.position(0);	
			
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);

			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, tbuf);	
			
			//
		}
		catch (Exception e)
		{
			
			System.out.println("[DesktopTexture::Load2] Exception: " + e.getMessage());
		}
		
		device.End();
	}
	
	public void Bind()
	{
		if (mID == -1)
			return;
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, mID);
	}
	
	public void Unbind()
	{
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, -1);
	}
	
	private int mID 	= 	-1;
}
