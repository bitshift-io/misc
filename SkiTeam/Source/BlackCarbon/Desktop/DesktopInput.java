package BlackCarbon.Desktop;

import org.lwjgl.input.Keyboard;

import BlackCarbon.API.Input;

public class DesktopInput extends Input
{	
	DesktopInput()
	{
		super();
		
		try
		{
			Keyboard.create();
		}
		catch (Exception e)
		{
			System.out.println("[Input.Input] " + e.getMessage());
		}
	}
	
	public void Update()
	{
		super.Update();
		
		Keyboard.poll();
		//int count = Keyboard.getNumKeyboardEvents();
		while (Keyboard.next()) 
		{/*
			int character_code = ((int)Keyboard.getEventCharacter()) & 0xffff;

			System.out.println("Checking key:" + Keyboard.getKeyName(Keyboard.getEventKey()));
			System.out.println("Pressed:" + Keyboard.getEventKeyState());
			System.out.println("Key character code: 0x" + Integer.toHexString(character_code));
			System.out.println("Key character: " + Keyboard.getEventCharacter());
			System.out.println("Repeat event: " + Keyboard.isRepeatEvent());
*/
			Control control = ControlLookup(Keyboard.getEventKey());
			float curState = mState[control.ordinal()].state;
			float newState = Keyboard.getEventKeyState() ? 1.f : 0.f;
			mState[control.ordinal()].state = newState;
			if (curState != newState && newState <= 0.f)
				mState[control.ordinal()].pressed = true;
		}

	}
	
	public Control ControlLookup(int VK)
	{
		for (int i = 0; i < mControlLookup.length; ++i)
		{
			if (mControlLookup[i].VK == VK)
				return mControlLookup[i].control;
		}
		
		return Control.Unknown;
	}
	
	class ControlLookup
	{
		public ControlLookup(int _VK, Control _control)
		{
			VK = _VK;
			control = _control;
		}
		
		public int 		VK;
		public Control 	control;
	}
	
	protected ControlLookup[] mControlLookup =
	{
			new ControlLookup(Keyboard.KEY_ESCAPE, Control.KeyEscape),
			new ControlLookup(Keyboard.KEY_RETURN, Control.KeyEnter),
			new ControlLookup(Keyboard.KEY_LEFT, Control.KeyLeft),
			new ControlLookup(Keyboard.KEY_RIGHT, Control.KeyRight),
			new ControlLookup(Keyboard.KEY_UP, Control.KeyUp),
			new ControlLookup(Keyboard.KEY_DOWN, Control.KeyDown),
	};
}
