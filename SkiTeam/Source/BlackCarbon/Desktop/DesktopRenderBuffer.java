package BlackCarbon.Desktop;

import org.lwjgl.opengl.GL11;

import BlackCarbon.API.RenderBuffer;

public class DesktopRenderBuffer extends RenderBuffer
{
	public void Bind()
	{
		if (vertex != null)
		{
			GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
			GL11.glVertexPointer(3, 0, vertex);
		}
		else
		{
			GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		}
		
		if (colour != null)
		{
			GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
			GL11.glColorPointer(4, 0, colour);
		}
		else
		{
			GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
		}
        
		if (uv != null)
		{
			GL11.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
			GL11.glTexCoordPointer(2, 0, uv);
		}
		else
		{
			GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		}
	}
	
	public void Unbind()
	{	
		GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
		GL11.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
	}
	
	public void Render()
	{	
		GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		GL11.glDisable(GL11.GL_CULL_FACE); // double sided plz for a bit
		GL11.glFrontFace(GL11.GL_CW);
		
		GL11.glDrawElements(GL11.GL_TRIANGLES, index);
	}
}
