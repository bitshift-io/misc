package BlackCarbon.Android;

import javax.microedition.khronos.opengles.GL11;

import BlackCarbon.API.Factory;
import BlackCarbon.API.RenderBuffer;

public class AndroidRenderBuffer extends RenderBuffer
{
	public void Bind()
	{
		GL11 gl = ((AndroidDevice)Factory.Get().GetDevice()).GetGL();
		if (vertex != null)
		{
			gl.glEnableClientState(GL11.GL_VERTEX_ARRAY);
			gl.glVertexPointer(3, GL11.GL_FLOAT, 0, vertex);
		}
		else
		{
			gl.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		}
		
		if (colour != null)
		{
			gl.glEnableClientState(GL11.GL_COLOR_ARRAY);
			gl.glColorPointer(4, GL11.GL_FLOAT, 0, colour);
		}
		else
		{
			gl.glDisableClientState(GL11.GL_COLOR_ARRAY);
		}
        
		if (uv != null)
		{
			gl.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
			gl.glTexCoordPointer(2, GL11.GL_FLOAT, 0, uv);
		}
		else
		{
			gl.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		}
	}
	
	public void Unbind()
	{	
		GL11 gl = ((AndroidDevice)Factory.Get().GetDevice()).GetGL();
		gl.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL11.GL_COLOR_ARRAY);
		gl.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
	}
	
	public void Render()
	{	
		GL11 gl = ((AndroidDevice)Factory.Get().GetDevice()).GetGL();
		gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		gl.glDisable(GL11.GL_CULL_FACE); // double sided plz for a bit
//		gl.glFrontFace(GL11.GL_CW);
		
		gl.glDrawElements(GL11.GL_TRIANGLES, index.capacity(), GL11.GL_UNSIGNED_BYTE, index);
	}
}