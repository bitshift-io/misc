package BlackCarbon.Android;

import java.io.InputStream;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import BlackCarbon.API.*;
import BlackCarbon.API.Factory.InitParam;

public class AndroidFactory extends Factory
{
	static public class InitParam extends Factory.InitParam
	{		
		public Activity 	activity;
		public View			view;
	}
	
	public AndroidFactory(InitParam param)
	{	
		if (param == null)
			param = new InitParam();
			
		param.factory = this;
		mInitParam = param;
		
		mDevice = new AndroidDevice(param);
		
		mInput = new AndroidInput();
		mInput.mFactory = this;
		
		mNetwork = new AndroidNetwork(param);
	}
	
	public InitParam GetInitParam()
	{
		return mInitParam;
	}
	
	public RenderBuffer CreateRenderBuffer()
	{
		RenderBuffer resource = new AndroidRenderBuffer();
		resource.mFactory = this;
		return resource;
	}
	
	public Texture AcquireTexture(String name)
	{
		Texture resource = new AndroidTexture();
		resource.mFactory = this;
		resource.mName = name;
		resource.Load();
		return resource;
	}
	
	// On Android, there are no extensions, hence filenames
	// are named with name_extension.extension
	// on Android this is loaded as name_extension
	public InputStream GetFileResource(String name, String extension)
	{
		int resID = GetResourcesID(name, extension);
		if (resID == 0)
			return null;

		InputStream is = GetResources().openRawResource(resID);
		return is;
	}
	
	// Android specific
	public Resources GetResources()
	{
		return mInitParam.activity.getResources();
	}
	
	// Android specific
	public int GetResourcesID(String name, String extension)
	{
		Resources res = GetResources();
		
		String resourceDir = mFactory.GetInitParam().resourceDir;
		String fileName = name + "_" + extension;
		if (resourceDir != null && resourceDir.length() > 0)
			fileName = resourceDir + "/" + fileName;
		
		int resID = res.getIdentifier(fileName, null, null);
		return resID;
	}
	
	// Android specific
	public void OnPause()
	{
		((BlackCarbon.Android.GLSurfaceView)mInitParam.view).onPause();
	}
	
	// Android specific
	public void OnResume()
	{
		((BlackCarbon.Android.GLSurfaceView)mInitParam.view).onResume();
	}
	
	protected 	InitParam	mInitParam;
}
