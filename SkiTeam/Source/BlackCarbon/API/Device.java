package BlackCarbon.API;

import java.util.concurrent.Semaphore;

import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;

//
// handles window and 3d functionality
//
public class Device 
{
	// Clear flags
	static public int CF_Depth			= (1 << 0);
	static public int CF_Colour			= (1 << 1);
	
	public enum GeometryTopology
	{
		LineList,
		PointList,
		TriangleList,
		//TriangleListAdjacent,
		TriangleStrip,
		QuadList,
	}

	public enum MatrixMode
	{
		Texture,
		Model,
		View,
		Projection,
	}

	public enum BlendMode
	{
		Zero,
		One,
		DestColour,
		SrcColour,
		OneMinusDestColour,
		OneMinusSrcColour,
		SrcAlpha,
		OneMinusSrcAlpha,
		DestAlpha,
		OneMinusDestAlpha,
		SrcAlphaSaturate,
	}

	public enum Compare
	{
		Less,
		LessEqual,
		Equal,
		Greater,
		GreaterEqual,
		Always, // TODO: fix device enums
		Never,
		NotEqual,
	}

	public enum Operation
	{
		Zero, // biatch!
		Keep,
		Replace,
		Increment,
		Decrement,
		Invert,
	}

	public enum CullFace
	{
		Front,
		Back,
		None,
	}

	public enum DrawType
	{
		NotInstanced// = -1,
	}

	//TODO: have a clear it takes a ptr to a clear state
	//and flags on what to clear
	public enum CleaFlags
	{
		Stencil,
		Colour,
	}

	public class Viewport
	{/*
		Viewport() :
			nearFarDepth(0.f, 1.f)
		{
		}

		Vector4 topLeft;
		Vector4 bottomRight;
		Vector4 nearFarDepth;*/
	};

	public class ClearState
	{/*
		Vector4			clearColour;
		unsigned int	stencilClear;
		float			clearDepth;*/
	}

	public class StencilFace
	{
		Compare		compare		=	Compare.LessEqual;
		Operation	pass		=	Operation.Zero;
		Operation	fail		=	Operation.Zero;
		Operation	depthFail	=	Operation.Zero;			
	}

	public class DeviceState
	{
		// geometry
		public int					vertexFormat			=	-1;
		public GeometryTopology		topology				=	GeometryTopology.TriangleList;
		public CullFace				cullFace				= 	CullFace.Back;

		// alpha
		public boolean				alphaToCoverageEnable	=	false;
		public boolean				alphaBlendEnable		=	false;
		public BlendMode			alphaBlendSource		=	BlendMode.SrcAlpha;
		public BlendMode			alphaBlendDest			=	BlendMode.OneMinusSrcAlpha;
		public Compare				alphaCompare			=	Compare.Always;
		public float				alphaCompareValue		= 	0.5f;
		
		// depth
		public boolean				depthTestEnable			=	true;
		public boolean				depthWriteEnable		=	true;
		public Compare				depthCompare			=	Compare.LessEqual;

		// stencil
		public boolean				stencilEnable			=	false;
		public int					stencilReadMask			=	-1;
		public int					stencilWriteMask		=	-1;

		public StencilFace			stencilFace[]			=	new StencilFace[2];
	}
	
	public Device()
	{
		ReleaseSemaphore();
	}

	public void SetWindowVisible(boolean visible)
	{

	}
	
	public void Clear(int clearFlags)
	{
		
	}
	
	public void Begin()
	{
		AcquireSemaphore();
		
		// restore default rendering
		Matrix4 identity = new Matrix4(Matrix4.Init.Identity);
		SetMatrix(identity, MatrixMode.Model);
		SetMatrix(identity, MatrixMode.Texture);
		SetMatrix(identity, MatrixMode.View);
		SetMatrix(identity, MatrixMode.Projection);
	}
	
	public void End()
	{
		ReleaseSemaphore();
	}

	public void SetMatrix(Matrix4 matrix, MatrixMode mode)
	{
	
	}

	public Matrix4 GetMatrix(MatrixMode mode)
	{
		return null;
	}
	
	public int GetWidth()
	{
		return -1;
	}
	
	public int GetHeight()
	{
		return -1;
	}
	
	public float GetAspectRatio()
	{
		return (float)(GetWidth()) / (float)(GetHeight());
	}
	
	public void Repaint()
	{

	}
	
	public void Resize(int width, int height)
	{
	
	}
	
	public void SetDeviceState(DeviceState state)
	{
	
	}
	
	public void DrawLine(Vector4 start, Vector4 end, Vector4 colour)
	{
		
	}
	
	public void DrawMatrix(Matrix4 matrix, float scale)
	{
		Vector4 position = matrix.GetTranslation();
		DrawLine(position, position.Add(matrix.GetZAxis().Multiply(scale)), new Vector4(0.f, 0.f, 1.f));
		DrawLine(position, position.Add(matrix.GetXAxis().Multiply(scale)), new Vector4(1.f, 0.f, 0.f));
		DrawLine(position, position.Add(matrix.GetYAxis().Multiply(scale)), new Vector4(0.f, 1.f, 0.f));
	}
	
	public void DrawSphere(Vector4 sphere, Vector4 colour)
	{
		Vector4 cur;
		Vector4 last = new Vector4((float)Math.sin(0), (float)Math.cos(0), 0.f, 0.f);	
		last = sphere.Add(last.Multiply(sphere.GetW()));
		
		for (float a = 0.1f; a <= (Math.PI * 2) + 0.1f; a += 0.1f)
		{
			cur = sphere.Add(new Vector4((float)Math.sin(a), (float)Math.cos(a), 0.f, 0.f).Multiply(sphere.GetW()));
			DrawLine(last, cur, colour);
			last = cur;
		}

		last = sphere.Add(new Vector4((float)Math.sin(0), 0.f, (float)Math.cos(0), 0.f).Multiply(sphere.GetW()));
		for (float a = 0.1f; a <= (Math.PI * 2) + 0.1f; a += 0.1f)
		{
			cur = sphere.Add(new Vector4((float)Math.sin(a), 0.f, (float)Math.cos(a), 0.f).Multiply(sphere.GetW()));
			DrawLine(last, cur, colour);
			last = cur;
		}

		last = sphere.Add(new Vector4(0.f, (float)Math.sin(0), (float)Math.cos(0), 0.f).Multiply(sphere.GetW()));
		for (float a = 0.1f; a <= (Math.PI * 2) + 0.1f; a += 0.1f)
		{
			cur = sphere.Add(new Vector4(0.f, (float)Math.sin(a), (float)Math.cos(a), 0.f).Multiply(sphere.GetW()));
			DrawLine(last, cur, colour);
			last = cur;
		}
	}
	
	public DeviceState GetDeviceState()
	{
		return null;
	}
	
	public void Primitive_Begin(GeometryTopology topology)
	{
		
	}
	
	public void Primitive_End()
	{
		
	}
	
	public void Primitive_Colour(Vector4 value)
	{
		
	}
	
	public void Primitive_Vertex(Vector4 value)
	{
		
	}
	
	public void Primitive_TexCoord(Vector4 value)
	{
		
	}
	
	public boolean Update()
	{
		return true;
	}
	

	public void AcquireSemaphore()
	{
		//try
		//{
			mSemaphore.acquireUninterruptibly();
		//} 
		//catch (InterruptedException e) 
		//{
		//	e.printStackTrace();
		//}
	}
	
	public void ReleaseSemaphore()
	{
		mSemaphore.release();
	}
	
	
	public Semaphore	mSemaphore					= new Semaphore(0, true); // for multithreaded loading
	public Factory		mFactory;
}
