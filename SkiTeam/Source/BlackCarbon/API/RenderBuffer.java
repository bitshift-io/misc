package BlackCarbon.API;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import BlackCarbon.Math.Vector4;

public class RenderBuffer extends Resource
{
	public void Bind()
	{

	}
	
	public void Unbind()
	{
		
	}
	
	public void Render()
	{
		
	}
	
	public void CreateQuad(Vector4 normal)
	{
		Vector4 vertices[] = {
	           	new Vector4(-0.5f, -0.5f, 0),
	           	new Vector4( 0.5f, -0.5f, 0),
			   	new Vector4(-0.5f,  0.5f, 0),
			   	new Vector4( 0.5f,  0.5f, 0),
	        };
		
		Vector4 uvs[] = {
				new Vector4(0,   1.f),
				new Vector4(1.f, 1.f),
				new Vector4(  0, 0.f),
				new Vector4(1.f, 0.f),
	        };

		byte indices[] = {
		        0, 1, 3,
		        0, 3, 2,
	        };
	        
		SetIndicies(indices);
		SetVertices(vertices);
		SetUVs(uvs);
	}
	
	public void CreateUnitCube()
	{
		float one = 1.f;

        Vector4 vertices[] = {
               new Vector4(-one, -one, -one),
               new Vector4( one, -one, -one),
    		   new Vector4( one,  one, -one),
			   new Vector4(-one,  one, -one),
			   new Vector4(-one, -one,  one),
			   new Vector4( one, -one,  one),
			   new Vector4( one,  one,  one),
			   new Vector4(-one,  one,  one),
            };

        Vector4 colours[] = {
        		new Vector4(  0,    0,    0,  one),
        		new Vector4(one,    0,    0,  one),
        		new Vector4(one,  one,    0,  one),
        		new Vector4(  0,  one,    0,  one),
        		new Vector4(  0,    0,  one,  one),
        		new Vector4(one,    0,  one,  one),
        		new Vector4(one,  one,  one,  one),
        		new Vector4(  0,  one,  one,  one),
            };
        
        byte indices[] = {
                0, 4, 5,
                0, 5, 1,
                1, 5, 6,
                1, 6, 2,
                2, 6, 7,
                2, 7, 3,
                3, 7, 4,
                3, 4, 0,
                4, 7, 6,
                4, 6, 5,
                3, 0, 1,
                3, 1, 2
        };
        
        SetIndicies(indices);
        SetVertices(vertices);
        SetColours(colours);
	}


	// Buffers to be passed to gl*Pointer() functions
    // must be direct, i.e., they must be placed on the
    // native heap where the garbage collector cannot
    // move them.
    //
    // Buffers with multi-byte datatypes (e.g., short, int, float)
    // must have their byte order set to native order
	
	public void SetUVs(Vector4 uvs[])
	{
		ByteBuffer uvbb = ByteBuffer.allocateDirect(uvs.length * 3 * 4);
        uvbb.order(ByteOrder.nativeOrder());
	    uv = uvbb.asFloatBuffer();
	    for (int i = 0; i < uvs.length; ++i)
	    {
	    	uv.put(uvs[i].v[0]);
	    	uv.put(uvs[i].v[1]);
	    }
        uv.position(0);
	}
	
	public void SetVertices(Vector4 vertices[])
	{
		ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 3 * 4);
	    vbb.order(ByteOrder.nativeOrder());
	    vertex = vbb.asFloatBuffer();
	    for (int i = 0; i < vertices.length; ++i)
	    {
	    	vertex.put(vertices[i].v[0]);
	    	vertex.put(vertices[i].v[1]);
	    	vertex.put(vertices[i].v[2]);
	    }
        vertex.position(0);
	}
	
	public void SetColours(Vector4 colours[])
	{
		ByteBuffer cbb = ByteBuffer.allocateDirect(colours.length * 4 * 4);
	    cbb.order(ByteOrder.nativeOrder());
        colour = cbb.asFloatBuffer();
        for (int i = 0; i < colours.length; ++i)
        	colour.put(colours[i].v);
        colour.position(0);
	}
	
	public void SetIndicies(byte indices[])
	{
		index = ByteBuffer.allocateDirect(indices.length);
        index.put(indices);
        index.position(0);
	}
	
	public FloatBuffer   	vertex;
	public FloatBuffer   	uv;
    public FloatBuffer   	colour;
    public ByteBuffer  		index;
}
