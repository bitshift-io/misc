package BlackCarbon.API;

public class Input
{
	public enum Control
	{
		KeyEscape,
		KeyEnter,
		
		KeyLeft,
		KeyRight,
		KeyUp,
		KeyDown,
		
		MouseLeft,
		MouseRight,
		MouseMiddle,

		MouseAxisX,
		MouseAxisY,
		MouseWheel,
		
		AccelerometerX,
		AccelerometerY,
		AccelerometerZ,
		
		// special controls
		Accelerometer,
		Touch,
		Mouse,
		
		Unknown,
		MAX,
	}
	
	public class ControlState
	{	
		public Control		control		= Control.Unknown;
		public float		state		= 0.f;
		public boolean		pressed	 	= false;
	}
	
	public class TouchState extends ControlState
	{
		public float x;
		public float y;
		public float size;
		public float pressure;
	}
	
	public Input()
	{
		for (int i = 0; i < Control.MAX.ordinal(); ++i)
			mState[i] = new ControlState();
		
		// special ones
		mState[Control.Touch.ordinal()] = new TouchState();
	}
	
	public ControlState GetControlState(Control input)
	{
		int idx = input.ordinal();
		return mState[idx];
	}
	
	public float GetState(Control input)
	{
		int idx = input.ordinal();
		return mState[idx].state;
	}
	
	public boolean WasPressed(Control input)
	{
		return mState[input.ordinal()].pressed;
	}
	
	public void Update()
	{
		for (int i = 0; i < Control.MAX.ordinal(); ++i)
		{
			mState[i].pressed = false;
		}
	}
	
	
	
	/*
	
	should i do something like this?
	how to handle mapping of input then in data?
	
	public class ControlState
	{
		
	}
	
	public class KeyState extends ControlState
	{
	
	}
	
	public class MouseState extends ControlState
	{
		int x;
		int y;
		int wheel;
		KeyState buttons;
	}
	
	public class TouchState extends ControlState
	{
		int x;
		int y;
		int size;
		int pressure;
	}
	 */
	
	public ControlState[] mState = new ControlState[Control.MAX.ordinal()];
	
	public Factory	mFactory;
}
