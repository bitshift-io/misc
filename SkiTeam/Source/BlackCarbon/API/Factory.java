package BlackCarbon.API;

import java.io.InputStream;

//
// Create an instance of this,
// holds pointers to all framework resources
//
// create implies creating a new one regardless of name
// acquire implies looking for an existing resource with said name and returning that, else creating a new one
//
public class Factory
{
	static public class InitParam
	{		
		// device param
		public String 	windowName		= "BlackCarbon";
		public int 		width			= 320;
		public int 		height			= 480;
		public boolean	windowVisible	= true;	// window initially visible
		
		public String	resourceDir		= "";
		
		public Factory 	factory; // internal only
	}
	
	public Factory()
	{
		mFactory = this;
	}

	public RenderBuffer CreateRenderBuffer()
	{
		return null;
	}
	
	public RenderBuffer AcquireRenderBuffer()
	{
		return null;
	}
	
	public void ReleaseRenderBuffer(RenderBuffer resource)
	{
		
	}
	
	public Texture AcquireTexture(String name)
	{
		return null;
	}
	
	public void ReleaseTexture(Texture resource)
	{

	}
	
	public Font AcquireFont(String name)
	{
		Font font = new Font();
		font.mName = name;
		font.mFactory = this;
		font.Load();
		return font;
	}
	
	public void ReleaseFont(Font resource)
	{

	}
	
	public Device GetDevice()
	{
		return mDevice;
	}
	
	public Network GetNetwork()
	{
		return mNetwork;
	}
	
	public Input GetInput()
	{
		return mInput;
	}
	
	public InitParam GetInitParam()
	{
		return null;
	}
	
	// returns false if the application should close
	public boolean Update()
	{
		if (mInput != null)
			mInput.Update();
		
		if (mNetwork != null)
			mNetwork.Update();
		
		if (mDevice != null)
			return mDevice.Update();
		
		return true;
	}
	
	static public Factory Get()
	{
		return mFactory;
	}
	
	public InputStream GetFileResource(String name, String extension)
	{
		return null;
	}
	
	protected Device		mDevice;
	protected Input			mInput;
	protected Network		mNetwork;
	
	protected static Factory	mFactory;
}


class Resource
{
	public String	mName;
	public Factory	mFactory;
}