package BlackCarbon.API;

import BlackCarbon.API.XMLSerialize;

//
// need to implement this if your class supports serialisation
//
public interface XMLSerializable 
{
	// return false to indicate to xml serializer
	// to handle it automatically
	//public boolean 	ParseAttribute(XMLSerialize serialize, String name, String value);
	//public void		SetParent(XMLSerialize serialize, Object parent);
	//public void		InsertChild(XMLSerialize serialize, Object child);
	
	// called after all classes have been instanced fro mthe xml file
	public void		Resolve(XMLSerialize serialize);
	
	// generate a clone
	public XMLSerializable 	Clone();
}
