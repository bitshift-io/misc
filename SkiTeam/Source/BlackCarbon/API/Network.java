package BlackCarbon.API;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Vector;

public class Network
{	
	public class Connection
	{/*
		class ConnectionInputStream extends InputStream
		{
			@Override
			public int read() throws IOException
			{
				// TODO Auto-generated method stub
				return 0;
			}
		}
		
		class ConnectionOutputStream extends OutputStream
		{
			@Override
			public void write(int oneByte) throws IOException
			{
				// TODO Auto-generated method stub
				outputArray.
				
			}
		}

		
		ConnectionInputStream GetInputStream()
		{
			return inputStream;
		}
		
		ConnectionOutputStream GetOutputStream()
		{
			return outputStream;
		}
		*/
		
		public DatagramSocket GetUDPSocket()
		{
			return udp;
		}
		
		public Socket GetTcpSocket()
		{
			return tcp;
		}
		
		public InetAddress GetAddress()
		{
			return tcp.getInetAddress();
		}
		
		DatagramSocket 	udp;
		Socket			tcp;
		
		public int				udpBytesSent;
		public int				udpBytesReceived;
		
		public int				tcpBytesSent;
		public int				tcpBytesReceived;
		
		public int				lastPacket; // number of frames since last packet received
		
		//Socket			tcp;
		
		//ConnectionInputStream	inputStream			= new ConnectionInputStream();
		//ConnectionOutputStream	outputStream		= new ConnectionOutputStream();
		
		//DatagramPacket packet = new DatagramPacket();
		//List<Byte>			outputArray 			= new List<Byte>();
	}
	
	
	public interface Callback
	{
		public void NotifyAddConnection(Connection connection);
		public void NotifyRemoveConnection(Connection connection);
	}
	

	int WORKING_UDP_PORT = 2527;
	
	//
	// a thread that
	// sits there discovering clients
	//
	class BroadcastThread extends Thread
	{
		// ports must be above 1024
		 // TODO: make this set able
		int DISCOVERY_PORT = 2526;
		
       int WORKING_UDP_PORT = 2527;
       int WORKING_TCP_PORT = 2528;
       
		public BroadcastThread(Network network)
		{
			mNetwork = network;
			
			int TIMEOUT_MS = 500;
			
			try 
			{
				mSocket = new DatagramSocket(DISCOVERY_PORT);
				mSocket.setBroadcast(true);
				mSocket.setSoTimeout(TIMEOUT_MS);
				
				mServerSocket = new ServerSocket(WORKING_TCP_PORT);
				mServerSocket.setSoTimeout(TIMEOUT_MS);
			} 
			catch (Exception e) 
			{
				System.out.print(e.getLocalizedMessage());
	    		e.printStackTrace();
			}
		}
		
		protected void finalize()
		{
			try 
			{
				mSocket.close();
				mServerSocket.close();
			}
			catch (IOException e) 
			{
				System.out.print(e.getLocalizedMessage());
	    		e.printStackTrace();
			}
		}
		
		public void AddNewConnection(Socket socket)
		{
			try 
			{
				Connection connection = new Connection();
		        //connection.address = socket.getInetAddress();
		       
		        connection.tcp = socket;
		        connection.udp = new DatagramSocket(WORKING_UDP_PORT);
				
		        connection.udp.setSoTimeout(1); // block for as little as possible
		        
		        mNetwork.AddNewConnection(connection);
			} 
			catch (Exception e)
			{
				System.out.print(e.getLocalizedMessage());
	    		e.printStackTrace();
			}
		}
		
		public void run()
		{	
	        String data = "<language=Java company=BlackCarbon packetType=Broadcast>";

			while (true)
			{
				// send broadcast packet
				try 
			    {
					InetAddress inetAddr = GetBroadcastAddress();
					DatagramPacket packet = new DatagramPacket(data.getBytes(), data.length(),
							inetAddr, DISCOVERY_PORT);
					mSocket.send(packet);
			    } 
			    catch (Exception e) 
			    {
			    	System.out.print(e.getLocalizedMessage());
		    		e.printStackTrace();
			    }
		
				// receive broadcast packets
				byte[] buf = new byte[1024];
			    try 
			    {
			    		
			      while (true)
			      {
			    	DatagramPacket packet = new DatagramPacket(buf, buf.length);
			        mSocket.receive(packet);
			        String s = new String(packet.getData(), 0, packet.getLength());
			        //Log.d(TAG, "Received response " + s);
			        
			        // make sure we dont register ourself if the packet came from us
			        InetAddress localAddr = mNetwork.GetLocalAddress();
			        InetAddress remoteAddr = packet.getAddress();
			        if (localAddr.equals(remoteAddr))
			        {
			        	continue;
			        }
			        
			        boolean existingConnectionFound = false;
			        Vector<Connection> connectionList = mNetwork.GetConnectionList();
			        for (int i = 0; i < connectionList.size(); ++i)
			        {
			        	Connection connection =  connectionList.get(i);
			        	if (connection.GetAddress().equals(packet.getAddress()))
			        	{
			        		existingConnectionFound = true;
			        		break;
			        	}
			        }
			        
			        connectionList = mNetwork.mNewConnectionList;
			        for (int i = 0; i < connectionList.size(); ++i)
			        {
			        	Connection connection =  connectionList.get(i);
			        	if (connection.GetAddress().equals(packet.getAddress()))
			        	{
			        		existingConnectionFound = true;
			        		break;
			        	}
			        }
			        
			        if (!existingConnectionFound)
			        {
			        	// the person with the lowest ip address will connect to the person with the highest
			        	// ip address
			        	byte[] localByteAddress = localAddr.getAddress();
			        	byte[] remoteByteAddress = remoteAddr.getAddress();
			        	
			        	int localSummedAddress = 0;
			        	for (int i = 0; i < localByteAddress.length; ++i)
			        		localSummedAddress += localByteAddress[i];
			        	
			        	int remoteSummedAddress = 0;
			        	for (int i = 0; i < remoteByteAddress.length; ++i)
			        		remoteSummedAddress += remoteByteAddress[i];
			        	
			        	if (localSummedAddress < remoteSummedAddress)
			        	{
			        		Socket clientSocket = new Socket(remoteAddr, WORKING_TCP_PORT);
			        		AddNewConnection(clientSocket);
			        	}		 
			        }
			      }
			    } 
			    catch (SocketTimeoutException e) 
			    {
			    	//System.out.print("Broadcast socket timed out");
			    }
			    catch (Exception e) 
			    {
			    	System.out.print(e.getLocalizedMessage());
		    		e.printStackTrace();
			    }
			    
			    // accept any tcp connections
			    try
			    {
			    	Socket clientSocket = mServerSocket.accept();
			    	AddNewConnection(clientSocket);
			    }
			    catch(IOException e)
			    {
			    	//System.out.print("Socket timed out");
			    }
			    catch (Exception e) 
			    {
			    	System.out.print(e.getLocalizedMessage());
		    		e.printStackTrace();
			    }
			}
		}
		
		public ServerSocket 	mServerSocket;
		public DatagramSocket 	mSocket;
		public Network 			mNetwork;
		
		//public boolean 			mLocalHostDiscoverable	= true; // only for testing purposes
	}
	
	public Network()
	{

	}
	
	public void finalize()
	{
		for (int i = 0; i < mConnectionList.size(); ++i)
		{
			Connection connection = mConnectionList.get(i);
				
			try 
			{
				connection.tcp.close();
				connection.udp.close();
				
			}
			catch (IOException e) 
			{
				System.out.print(e.getLocalizedMessage());
	    		e.printStackTrace();
			}
		}
	}
	
	public InetAddress GetLocalAddress()
	{
		try 
		{
			return InetAddress.getLocalHost();
		} 
		catch (UnknownHostException e) 
		{
			System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
		}
		
		return null;
	}
	
	//
	// discoverable mode will search for other games
	// which are also in discoverable mode
	//
	public void SetDiscoverable(boolean discoverable)
	{
		if (discoverable)
		{
			mConnectionList.clear();
			mBroadcastThread.start();
		}
		else 
		{
			mBroadcastThread.stop();
		}
	}
	
	public InetAddress GetBroadcastAddress()
	{
		try 
		{/*
			WifiManager wifi = (WifiManager) mFactory.GetInitParam().activity.getSystemService(Context.WIFI_SERVICE);
			
		    DhcpInfo dhcp = wifi.getDhcpInfo();
		    // handle null somehow
	
		    int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
		    byte[] quads = new byte[4];
		    for (int k = 0; k < 4; k++)
		      quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
	    */
		    byte[] quads = {(byte) 255, (byte) 255, (byte) 255, (byte) 255};
		    InetAddress inetAddr = InetAddress.getByAddress(quads);
			return inetAddr;
		}
		catch (UnknownHostException e) 
		{
			System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
		}
		
		return null;
	}
	
	public void Broadcast(DatagramPacket packet)
	{
		for (int i = 0; i < mConnectionList.size(); ++i)
		{
			Connection connection = mConnectionList.get(i);
			try 
			{
				packet.setAddress(connection.GetAddress());
				packet.setPort(WORKING_UDP_PORT);
				connection.udp.send(packet);
				connection.udpBytesSent += packet.getLength();
			}
			catch (Exception e) 
			{
				System.out.print(e.getLocalizedMessage());
	    		e.printStackTrace();
			}
		}
	}
	
	public synchronized void AddNewConnection(Connection connection)
	{
		mNewConnectionList.add(connection);
	}
	
	public synchronized void Update()
	{
		int CONNECTION_FRAME_TIMEOUT = 60; // 2 seconds, this should be a time! and tweakable
		

		// notify of new connections, done here so its done in the same thread doing the updating
		// and potentially rendering, to fix any OGL context/thread issues
		for (int i = 0; i < mNewConnectionList.size(); ++i)
		{
			Connection connection = mNewConnectionList.get(i);
	        if (mCallback != null)
	        {
	        	mCallback.NotifyAddConnection(connection);
	        }
	        
	        mNewConnectionList.clear();
	        mConnectionList.add(connection);
		}
        
		//  send ping packets?
		for (int i = 0; i < mConnectionList.size(); ++i)
		{
			Connection connection = mConnectionList.get(i);
			connection.lastPacket++;
			
			if (connection.lastPacket >= CONNECTION_FRAME_TIMEOUT)
			{
				mCallback.NotifyRemoveConnection(connection);
				
				try 
				{
					connection.udp.close();
					connection.tcp.close();
				} 
				catch (Exception e) 
				{
					System.out.print(e.getLocalizedMessage());
		    		e.printStackTrace();
				}
				
				
				mConnectionList.remove(i);
				--i;
			}
		}
	}
	
	public Vector<Connection> GetConnectionList()
	{
		return mConnectionList;
	}
	
	public void SetCallback(Callback callback)
	{
		mCallback = callback;
	}
	
	protected Callback				mCallback;
	protected BroadcastThread 		mBroadcastThread		= new BroadcastThread(this);
	protected Vector<Connection>	mConnectionList			= new Vector<Connection>();
	protected Vector<Connection>	mNewConnectionList		= new Vector<Connection>();
}
