package SkiTeam;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import BlackCarbon.API.Camera;
import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Font;
import BlackCarbon.API.Input;
import BlackCarbon.API.Network;
import BlackCarbon.API.RenderBuffer;
import BlackCarbon.API.Device.DeviceState;
import BlackCarbon.API.Device.GeometryTopology;
import BlackCarbon.API.Device.MatrixMode;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;

public class Game extends BlackCarbon.Ext.Game
{
	class LoadingThread extends Thread
	{
		public void run()
		{
			Game game = (Game) Game.Get();
			Network network = game.GetFactory().GetNetwork();
			World world = game.GetWorld();
			
			//game.GetFactory().GetDevice().Begin();
			
			network.SetCallback(world);
			
			
			world.LoadGlobalResources();
			
			Actor actor = (Actor)world.LoadFromTemplate(Actor.class);
	    	if (actor != null)
	    	{	
		    	actor.SetDirector(Director.Create(Director.Type.Player));
		    	world.InsertActor(actor);
	    	}
	    	
	    	/*
	    	 * now dont by the callback, until we have a lobby screen
	    	// now if we have any players connecting, create actors for them
	    	//network.SetDiscoverable(false); 										// disable temporarily
	    	Vector<Network.Connection> connectionList = network.GetConnectionList();
	    	for (int i = 0; i < connectionList.size(); ++i)
	    	{
		    	NetworkDirector netDirector = (NetworkDirector) Director.Create(Director.Type.Network);
		    	netDirector.SetConnection(connectionList.get(i));
		    	
		    	Actor netActor = (Actor)world.LoadFromTemplate(Actor.class);
		    	netActor.SetDirector(netDirector);
		    	world.InsertActor(netActor);
	    	}*/
	    	
	    	network.SetDiscoverable(true); // enable us to find other players
	    	/*
	    	// sleep for a while to test loading screen works
	    	try 
        	{
        		TimeUnit.MILLISECONDS.sleep(1000 * 10);
			} 
        	catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
        	
        	
	    	world.SetState(World.State.Loaded);
	    	
	    	// loading complete, so disable UI and show the game
	    	UserInterface ui = game.GetUserInterface();
	    	ui.SetVisible(false);
	    	
	    	//game.GetFactory().GetDevice().End();
		}
	}
	
    @Override
    public synchronized boolean Init()
    {
    	System.out.println("Init");
    	
    	/*
    	// testing network code
    	try {
    		
    		ServerSocket server = new ServerSocket(25);
    		server.setSoTimeout(1000);
    		
    		
			//Socket client = new Socket("localhost", 25);
			
			Socket self = server.accept();
			
			int nothing = 0;

			
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
    	 
    	
    	mDebug = true;
        mDebugFont = Factory.Get().AcquireFont("mini");
    	mDebugFont.SetHorizontalAlign(Font.HorizontalAlign.Right);
    	mDebugFont.SetVerticalAlign(Font.VerticalAlign.Below);	
    		
    	mWorld = new World();
    	mUserInterface = new UserInterface();
    	
    	// set up the loading UI
    	mUserInterface.LoadGlobalResources();
    	mUserInterface.ShowUI("loading");
    	
    	// temp
    	GetFactory().GetDevice().End();
    	
    	// set up a loading thread to load game
    	LoadingThread loadingThread = new LoadingThread();
    	
    	// is this causing textures to not work correctly?
    	loadingThread.start();
    	/*
    	*/
    	
    	//GetFactory().GetDevice().Begin();
    	/*
    	try 
    	{
    		TimeUnit.MILLISECONDS.sleep(1000 * 100);
		} 
    	catch (InterruptedException e) 
		{
			e.printStackTrace();
		}*/
    	
    	//GetFactory().GetDevice().End();
    	
    	//while (loadingThread.i)
    	//loadingThread.run();
    	
    	/*
    	Actor actor = (Actor)mWorld.LoadFromTemplate(Actor.class);
    	if (actor != null)
    	{	
	    	actor.SetDirector(Director.Create(Director.Type.Player));
	    	mWorld.InsertActor(actor);
    	}
    	
    	// debugging
    	//AndroidFactory factory = (AndroidFactory) GetFactory();
    	//AlertDialog alertDialog = new AlertDialog.Builder(factory.GetInitParam().activity).create();
    	//alertDialog.setTitle("Init complete");
    	//alertDialog.show();
    	*/
    	// super.Init MUST BE LAST
    	return super.Init();
    }
    
    @Override
    public synchronized boolean Update()
	{	
    	boolean result = super.Update();
    	
    	mWorld.Update();
    	
    	if (mUserInterface != null)
    		mUserInterface.Update();

		return result;
	}
	
    @Override
	public synchronized void Render()
	{	
    	super.Render();
    	
    	Device device = GetFactory().GetDevice();
    	device.Begin();
    	device.Clear(Device.CF_Colour | Device.CF_Depth);

    	mWorld.Render();    
    	
    	if (mUserInterface != null)
    		mUserInterface.Render();

    	if (IsDebug())
    	{
	    	// render frame rate
	    	Camera uiCamera = new Camera();
	    	uiCamera.SetOrthographic(2.f, 2.f, 1.f, 15.f);
	    	uiCamera.Bind();

			Matrix4 transform = new Matrix4(Matrix4.Init.Identity);
			float scale = 0.006f;
			transform.SetScale(new Vector4(scale, scale));
			transform.SetTranslation(new Vector4(-1.f, 0.f));
			device.SetMatrix(transform, MatrixMode.Model);
			
			// set device state - enable alpha blending
	    	Device.DeviceState state = device.new DeviceState();
	    	state.alphaBlendEnable = true;
	    	device.SetDeviceState(state);

	    	String debugString = "" + mRenderThisSecond; //"FPS " + mRenderThisSecond + "\nUPS " + mUpdateThisSecond;
	    	Vector<Network.Connection> connectionList = GetFactory().GetNetwork().GetConnectionList();
	    	if (connectionList.size() > 0)
	    	{
	    		debugString += "\nNetCon " + connectionList.size();
	    		/*
	    		for (int i = 0; i < connectionList.size(); ++i)
	    		{
	    			Network.Connection connection = connectionList.get(i);
	    			debugString += "\n " + i + ". " + connection.GetAddress().toString() 
	    				+ "\n      in: " + Integer.toString(connection.udpBytesReceived)
	    				+ "\n      out: " + Integer.toString(connection.udpBytesSent);
	    		}*/
	    	}
			mDebugFont.Render(debugString);
			/*
			// output accelerometer readings
			transform.SetTranslation(new Vector4(-1.f, 0.5f));
			device.SetMatrix(transform, MatrixMode.Model);
			
			try
			{
				float accelX = GetFactory().GetInput().GetState(Input.Control.AccelerometerX);
				float accelY = GetFactory().GetInput().GetState(Input.Control.AccelerometerY);
				float accelZ = GetFactory().GetInput().GetState(Input.Control.AccelerometerZ);
				
				mDebugFont.Render("Accel X " + accelX
						+ "\nAccel Y " + accelY
						+ "\nAccel Z " + accelZ);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}*/
			
			// disable alpha blending
			state.alphaBlendEnable = false;
	    	device.SetDeviceState(state);
    	}
    	
        device.End();
	}
    
    World GetWorld()
    {
    	return mWorld;
    }
    
    UserInterface GetUserInterface()
    {
    	return mUserInterface;
    }
    
	public String GetPrimaryFontName()
	{
		return mPrimaryFontName;
	}
	
	static public SkiTeam.Game Get()
	{
		return (SkiTeam.Game) mGame;
	}
	
	private String			mPrimaryFontName	= "mini";
	
    private Font			mDebugFont;
    private World			mWorld;
    private UserInterface	mUserInterface;
}