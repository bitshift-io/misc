package SkiTeam;

import java.util.Vector;

import BlackCarbon.Math.Vector4;
import BlackCarbon.API.XMLSerialize;
import SkiTeam.Game;
import BlackCarbon.Ext.Sprite;

//
// the representation of what your controlling in the world
//
public class Actor extends Obstruction
{
	public Actor()
	{
		super();
		
		mType = Type.Actor;
		//mSprite = null;
		
		
		/*
		// set up collision
		mSphere.add(new Vector4(0.f, 0.f, 0.f, 0.3f));
		
		mState.add(new LiveState(this));
		mState.add(new DeathState(this));
		mState.add(new JumpState(this));
		*/
		
		
		mTrail = new Trail(30);
	}
	
	public void	Resolve(XMLSerialize serialize)
	{
		for (int s = 0; s < mState.size(); ++s)
		{
			mState.get(s).SetOwner(this);
		}
		
		SetState(LiveState.class);
	}
	
	public void SetDirector(Director director)
	{
		mDirector = director;
		mDirector.SetActor(this);
	}
	
	public Director GetDirector()
	{
		return mDirector;
	}
	
	public void Update()
	{
		mPositionLastFrame = GetPosition();
		 
		super.Update();
		
		if (mCurrentState != null)
			mCurrentState.Update();
		
		// only add node if we have moved so far
		Vector4 pos =  GetPosition();
		Vector4 lastNode = mTrail.GetLastNode();
		if (lastNode == null)
		{
			Vector4 dir = mVelocity;
			dir.Normalize3();
			mTrail.AddNode(pos, dir);
		}
		else
		{	
			float distanceFromLastNode = pos.Subtract(lastNode).Magnitude3();
			if (distanceFromLastNode >= mDistanceBetweenTrailNodes)
			{
				Vector4 dir = mVelocity;
				dir.Normalize3();
				mTrail.AddNode(pos, dir);
			}
		}
		
		Vector4 deltaPos = GetPosition().Subtract(mPositionLastFrame);
		float distanceTraveled = Math.abs(deltaPos.GetY());
		GetDirector().GetScore().AddDistance(distanceTraveled);
		
		float deltaTime = ((Game)Game.Get()).GetDeltaTime();
		GetDirector().GetScore().AddTime(deltaTime);
		
		Obstruction checkpoint = PassedThroughCheckPoint();
		if (checkpoint != null)
		{
			GetDirector().GetScore().PassedCheckpoint(checkpoint);
		}
		
		GetDirector().GetScore().Update();
	}
	
	public Vector4 GetCameraPosition()
	{
		return GetPosition().Add(mCameraOffset);
	}
	
	public Vector4 GetPosition()
	{
		return mSprite.mTransform.GetTranslation();
	}
	
	public void Render()
	{
		//mTrail.Render();
		super.Render();
		
		//GetDirector().GetScore().Render(); // to slow on android
	}
	
	public void SetSprite(Sprite sprite)
	{
		if (mSprite != null)
			sprite.mTransform = mSprite.mTransform;
		
		mSprite = sprite;
	}
		
	public Vector4 GetDesiredVelocity()
	{
		return mDesiredVelocity;
	}
	
	public Vector4 GetVelocity()
	{
		return mVelocity;
	}
	
	public void SetState(Class c)
	{
		for (int s = 0; s < mState.size(); ++s)
		{
			ActorState state = mState.get(s);
			if (state.getClass() == c)
			{
				state.Enter(mCurrentState);
				mCurrentState = state;
			}
		}
	}
	
	public Obstruction CheckForCheckPoint()
	{
		Game game = Game.Get();
		
		// check for obstructions/collisions
    	for (int i = 0; i < game.GetWorld().mObstruction.size(); ++i)
    	{
    		Obstruction other = game.GetWorld().mObstruction.get(i);
    		if (other.GetType() == Obstruction.Type.Checkpoint)
    		{
    			if (other.PassedThroughCheckPoint(mPositionLastFrame, GetPosition()))
    			{
    				return other;
    			}
    		}
    	}
    	
    	return null;
	}
	
	public Obstruction CheckForCollision(Vector4 collisionVec)
	{
		Game game = (Game)Game.Get();
		
		Vector4 result = null;
		
		// check for obstructions/collisions
    	for (int i = 0; i < game.GetWorld().mObstruction.size(); ++i)
    	{
    		Obstruction other = game.GetWorld().mObstruction.get(i);
    		if ((result = CollidesWith(other)) != null)
    		{
    			if (collisionVec != null)
    				collisionVec.Copy(result);
    			
    			return other;
    		}
    	}
    	
    	// check for collision against other actors
    	for (int i = 0; i < game.GetWorld().mActor.size(); ++i)
    	{
    		Actor other = game.GetWorld().mActor.get(i);
    		if (other != this && (result = CollidesWith(other)) != null)
    		{
    			if (collisionVec != null)
    				collisionVec.Copy(result);
    			
    			return other;
    		}
    	}
    	
    	return null;
	}
	
	public Obstruction PassedThroughCheckPoint()
	{
		Game game = (Game)Game.Get();
		
		for (int i = 0; i < game.GetWorld().mObstruction.size(); ++i)
    	{
    		Obstruction other = game.GetWorld().mObstruction.get(i);
    		boolean result = other.PassedThroughCheckPoint(mPositionLastFrame, GetPosition());
    		if (result)
    			return other;
    	}
		
		return null;
	}
	
	public void SetRotation(float rotation)
	{
		mRotation = rotation;
	}
	
	public float GetRotation()
	{
		return mRotation;
	}
	
	public ActorState GetState()
	{	
		return mCurrentState;
	}
	
	private Trail		mTrail;
	public float		mDistanceBetweenTrailNodes	= 0.5f;
	
	public Vector4		mCameraOffset		= new Vector4(0.f, -4.f, 3.f);
	
	public float		mMaxSpeed			= 17.f;		// max speed, when going down the slope (meters per second)
	public float		mMinSpeed			= 5.f;		// min speed when at 90 degrees to the slope (meters per second)
	
	public float		mVelocitySlerp		= 5.f; // % per second
	public Vector4		mVelocity			= new Vector4(0.f, 0.f, 0.f);
	public Vector4		mDesiredVelocity	= new Vector4(0.f, 0.f, 0.f);
	
	public float		mRotation			= 90.f;		// in degrees (start stationary)
	public float		mRotationSpeed		= 80.f;		// degrees per second

	public Vector4		mPositionLastFrame;
	
	private Director 	mDirector;
	
	public Vector<ActorState>		mState 				= new Vector<ActorState>();	// list of states
	private ActorState				mCurrentState;
}
