package SkiTeam;

import BlackCarbon.Math.Quaternion;
import BlackCarbon.Math.Vector4;
import BlackCarbon.Ext.Game;
import BlackCarbon.Ext.Sprite;

public class LiveState extends ActorState
{
	/*
	public LiveState(Actor owner)
	{
		super(owner);
		
		mSprite = new Sprite("actorlive", 16, 1);
		mSprite.mTransform.SetScale(new Vector4(2.f, 2.f, -2.f));
		mSprite.mTransform.SetTranslation(new Vector4(0.f, 0.f, -9.0f));
		mSprite.mCurWidth = 7; // middle sprite moving strait down
	}*/
	
	public void Enter(ActorState prev)
	{
		Actor owner = GetOwner();
		owner.SetSprite(mSprite);
		
		// reset
		if (prev != null && prev.getClass() == DeathState.class)
		{
			owner.mVelocity			= new Vector4(0.f, 0.f, 0.f);
			owner.mDesiredVelocity	= new Vector4(0.f, 0.f, 0.f);
			owner.mRotation			= 0.f;
		}
		
		mTransitionState = null;
	}

	public void Update()
	{
		Actor owner = GetOwner();
		Game game = Game.Get();
		
		float deltaTime = game.GetDeltaTime();
		owner.GetDirector().Update();
		
		//
		// rotate actor
		//
		
		if (mTransitionState == JumpState.class)
		{
			float desiredRotation = 0.f; //(float)(Math.PI * 0.5f); // strait down
			float deltaRotation = owner.mRotation - desiredRotation;
			if (deltaRotation > 0.f)
				owner.mRotation -= mTransitionRotationSpeed * deltaTime;
			else
				owner.mRotation += mTransitionRotationSpeed * deltaTime;
				
			if (Math.abs(deltaRotation) < (mTransitionRotationSpeed * deltaTime) + 0.1f)
			{
				owner.mRotation = desiredRotation;
				owner.SetState(mTransitionState);
				return;
			}
		}
		else
		{
			if (owner.GetDirector().GetActionHeld(Director.Action.Left) > 0.5f)
				owner.mRotation -= owner.mRotationSpeed * deltaTime;
			
			if (owner.GetDirector().GetActionHeld(Director.Action.Right) > 0.5f)
				owner.mRotation += owner.mRotationSpeed * deltaTime;
		}

		
		// clamp to -90 90 range
		owner.mRotation = Math.max(owner.mRotation, -89.99f);
		owner.mRotation = Math.min(owner.mRotation, 89.99f);
		
		float result = (owner.mRotation + 90.f); // normalise -90 > 90 to..... 0 > 180, -1 to stop jumping in to the wrong sector
		float sector = result / (180.f / 15.f); // there are 13 different animations, so divide 180 in to 13 sectors
		owner.mSprite.mCurWidth = (int)Math.floor(sector);
		
		//
		// calculate desired velocity
		//
		
		// the more we point downwards, the more we use max velocity, the closer we
		// are to 90 degrees away from the slope, the more towards min speed we use
		float rotRad = (float)Math.toRadians(owner.mRotation);
		float cosRot = (float)Math.cos(rotRad);
		float sinRot = (float)Math.sin(rotRad);
		
		float speedMultiplier = owner.mMinSpeed + (cosRot * (owner.mMaxSpeed - owner.mMinSpeed));
		owner.mDesiredVelocity = new Vector4(sinRot * speedMultiplier, cosRot * -speedMultiplier, 0.f);
		
		//
		// calculate current velocity
		//
		
		// do a SLERP between desired velocity and current velocity
		Quaternion desired = new Quaternion(owner.mDesiredVelocity);
		Quaternion current = new Quaternion(owner.mVelocity);
		Quaternion qResult = current.Interpolate(desired, owner.mVelocitySlerp * deltaTime);
		owner.mVelocity = new Vector4(qResult);
		
		//
		// apply velocity ie. move actor
		//
		Vector4 oldPos = owner.mSprite.mTransform.GetTranslation();
		owner.mSprite.mTransform.SetTranslation(owner.mSprite.mTransform.GetTranslation().Add(owner.mVelocity.Multiply(deltaTime)));

		// check for collisions
		Obstruction collision = owner.CheckForCollision(null);
		if (collision != null)
		{
			if (collision.GetType() == Obstruction.Type.Jump)
			{
				mTransitionState = JumpState.class;
				owner.GetDirector().GetScore().HitJump(collision);
			}
			else
			{
				owner.SetState(DeathState.class);
			}
		}
	}

	public float	mTransitionRotationSpeed 	= 160.f;
	public Class	mTransitionState			= null;
	public Sprite	mSprite;
}
