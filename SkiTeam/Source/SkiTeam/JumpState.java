package SkiTeam;

import BlackCarbon.Math.Vector4;
import BlackCarbon.Ext.Game;
import BlackCarbon.Ext.Sprite;

public class JumpState extends ActorState
{/*
	public JumpState(Actor owner)
	{
		super(owner);
		
		mSprite = new Sprite("actorjump", 8, 1);
		mSprite.mAnimationFlags = Sprite.AF_Horizontal | Sprite.AF_NoLoop;
		mSprite.mFrameRate = 12;
		mSprite.mTransform.SetScale(new Vector4(2.f, 2.f, -2.f));
		mSprite.mTransform.SetTranslation(new Vector4(0.f, 0.f, -9.0f));
	}*/
	
	public void Enter(ActorState prev)
	{
		Actor owner = GetOwner();
		owner.SetSprite(mSprite);
		mSprite.ResetAnimation();
		mTimeInState = 0.f;
	}
	
	public void Update()
	{
		Actor owner = GetOwner();
		Game game = (Game)Game.Get();
		
		float deltaTime = game.GetDeltaTime();
		
		mTimeInState += deltaTime;
		
		if (mTimeInState > mMaxTimeInState)
			owner.SetState(LiveState.class);
		
		//
		// apply velocity ie. move actor
		//
		
		owner.mSprite.mTransform.SetTranslation(owner.mSprite.mTransform.GetTranslation().Add(owner.mVelocity.Multiply(deltaTime)));
	}

	float 	mTimeInState;
	float 	mMaxTimeInState 		= 3.f;
	Sprite	mSprite;
}
