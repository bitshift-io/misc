package SkiTeam;

import java.util.Date;
import java.util.Random;
import java.util.Vector;

import BlackCarbon.API.Network;
import BlackCarbon.API.Camera;
import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.XMLSerializable;
import BlackCarbon.API.XMLSerialize;
import BlackCarbon.Math.BezierSpline;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.API.RenderBuffer;
import BlackCarbon.API.Texture;
import BlackCarbon.API.Network.Connection;
import BlackCarbon.Math.Vector4;
import BlackCarbon.Math.BezierSpline.ControlPoint;
import BlackCarbon.Ext.Game;

class Background
{
	public Texture			texture;
	public RenderBuffer		geometry;
	public Vector4			scroll 		= new Vector4(0.f, 0.f, 0.f);
	public Vector4			position	= new Vector4(0.f, 0.f, 0.f);
}



public class World implements Network.Callback
{
	public enum State
	{
		Unloaded,
		Loaded,
		Loading
	}
	
	class RaceTrack
	{
		void Init()
		{
			// create a pool of flags
			for (int i = 0; i < mObjectTemplates.size(); ++i)
			{
				Obstruction obj = (Obstruction)mObjectTemplates.get(i);
				if (obj.GetType() == Obstruction.Type.Checkpoint)
				{
					XMLSerialize objectCloner = new XMLSerialize();
					Obstruction clone = (Obstruction)objectCloner.Clone(obj);
					mObstruction.add(clone);
				}
				else if (obj.GetType() == Obstruction.Type.FinishCheckpoint)
				{
					XMLSerialize objectCloner = new XMLSerialize();
					Obstruction clone = (Obstruction)objectCloner.Clone(obj);
					mFinishCheckpoint = clone;
				}
			}
		}
		
		void GenerateTrack()
		{
			mSpline.type = BezierSpline.Type.Linear;
			mSpline.Clear();
			
			// start point is at 0,0,0
			ControlPoint prev = mSpline.new ControlPoint();
			prev.position = new Vector4(0.f, 0.f, 0.f);
			mSpline.Add(prev);
			
			for (int i = 0; i < 10; ++i)
			{
				ControlPoint cur = mSpline.new ControlPoint();
				
				// add a new control point every 20m to 40m
				// and have a x offset of += 50m
				Random r = new Random();
				float yOffset = 20.f + r.nextFloat() * 20.f;
				float xOffset = -50.f + r.nextFloat() * 100.f;
				
				cur.position = prev.position.Add(new Vector4(xOffset, -yOffset, 0.f));
				mSpline.Add(cur);
				
				// set tangent
				//Vector4 midpoint = prev.position.Subtract(cur.position);
				//cur.tangent[0] = new Vector4();
				
				prev = cur;
			}
		}
		
		void Update()
		{
			Vector4 camPos = mCamera.GetPosition();
			
			// update obstructions
	    	for (int i = 0; i < mActiveObstruction.size(); ++i)
	    	{
	    		Obstruction obstruction = mActiveObstruction.get(i);
	    		obstruction.Update();
	    		
	    		// should this obstruction be recycled
	    		Vector4 pos = obstruction.GetTranslation();
	    		if (pos.GetY() > camPos.GetY() + 15.f) // add padding
	    		{
	    			mCheckpointPool.add(obstruction);
	    			mActiveObstruction.remove(i);
	    			--i;
	    		}
	    	}
		}
		
		void Render()
		{
			if (!Game.Get().IsDebug())
				return;
				
			Device device = Factory.Get().GetDevice();
			
			Matrix4 m = new Matrix4(Matrix4.Init.Identity);
	    	device.SetMatrix(m, Device.MatrixMode.Model);
	    	
	    	Vector4 colour = new Vector4(1.f, 1.f, 0.f, 1.f);
	    	
	    	float increment = 0.001f;
			for (float p = 0.f; p < 1.f; p += increment)
			{
				Vector4 pos0 = mSpline.Evaluate(p);
				Vector4 pos1 = mSpline.Evaluate(p + increment);
				
				device.DrawLine(pos0, pos1, colour);
			}
		}
		
		Vector<Obstruction>		mActiveObstruction 	= new Vector<Obstruction>();
		Vector<Obstruction>		mCheckpointPool		= new Vector<Obstruction>();
		Obstruction				mFinishCheckpoint	= null;
		BezierSpline			mSpline 			= new BezierSpline();
	}
	
	public World()
	{
		Factory factory = Game.Get().GetFactory();
		//mCamera.mFactory = factory;
	}
	
	public void LoadGlobalResources()
	{
		Factory factory = Game.Get().GetFactory();
		float aspectRatio = factory.GetDevice().GetAspectRatio();
		
		// set up a background
		mBackground.geometry = factory.CreateRenderBuffer();
		mBackground.geometry.CreateQuad(null);
		mBackground.texture = factory.AcquireTexture("ground");
		
		// set background quad to fit ortho projection to just cover the screen
		float scale = 1.0f; //scale in abit for testing
		Vector4 vertices[] = {
	           	new Vector4(-0.5f * mOrthoScale * aspectRatio * scale, -0.5f * mOrthoScale * scale, 0),
	           	new Vector4( 0.5f * mOrthoScale * aspectRatio * scale, -0.5f * mOrthoScale * scale, 0),
			   	new Vector4(-0.5f * mOrthoScale * aspectRatio * scale,  0.5f * mOrthoScale * scale, 0),
			   	new Vector4( 0.5f * mOrthoScale * aspectRatio * scale,  0.5f * mOrthoScale * scale, 0),
	        };
		
		mBackground.geometry.SetVertices(vertices);
		//mBackground.geometry.SetUVs(vertices);
/*		
		// spawn obstructions, these will get recycled once they go off screen
        Date date = new Date();
        long time = date.getTime();
		Random rand = new Random(time);
		for (int i = 0; i < mDesiredNumObstructions; ++i)
		{
	    	Obstruction obstruction = new Obstruction();
	    	float rand1 = (float)rand.nextFloat();
	    	float rand2 = (float)rand.nextFloat();
	    	obstruction.SetTranslation(new Vector4((rand1 - 0.5f) * 15.f, (rand2 - 0.5f) * 15.f, -8.0f));
	    	mObstruction.add(obstruction);  
		}
*/
		// load obstruction templates
		XMLSerialize objectTemplates = new XMLSerialize(factory, "obstructions", "SkiTeam");
		for (int i = 0; i < objectTemplates.GetSize(); ++i)
		{
			mObjectTemplates.add(objectTemplates.Get(i));
		}
		
		mTrack.Init();
		mTrack.GenerateTrack();
	}
	
	public synchronized void Update()
	{
		if (mState != State.Loaded)
			return;
		
		Device device = Game.Get().GetFactory().GetDevice();
		
		// adjust camera
		int width = device.GetWidth();
		int height = device.GetHeight();
		float aspectRatio = device.GetAspectRatio();
		mCamera.SetOrthographic(mOrthoScale * aspectRatio, mOrthoScale, 1.f, 15.f);
		//mCamera.
	
		// update actors
    	for (int i = 0; i < mActor.size(); ++i)
    	{
    		Actor actor = mActor.get(i);
    		actor.Update();
    		
    		// the first actor we take his velocity as this it the main player
        	// and use that for the background scroll
    		if (i == 0)
    		{
    			Vector4 cameraPos = actor.GetCameraPosition();
    			
    			//swap z any as camera is looking down z axis
    			//float temp = cameraPos.GetZ();
    			//cameraPos.SetZ(-cameraPos.GetY()); 
    			//cameraPos.SetY(temp);
    			
    			mCamera.SetPosition(cameraPos);
    			
    			mBackground.scroll = mBackground.scroll.Add(actor.GetVelocity());
    			mBackground.position = cameraPos;
    			mBackground.position.SetZ(10.f); // move it to eh background
    		}
    	}
    	
    	Vector4 camPos = mCamera.GetPosition();
    	
    	/*
    	
    	// update obstructions
    	for (int i = 0; i < mObstruction.size(); ++i)
    	{
    		Obstruction obstruction = mObstruction.get(i);
    		obstruction.Update();
    		
    		// should this obstruction be recycled
    		Vector4 pos = obstruction.GetTranslation();
    		if (pos.GetY() > camPos.GetY() + 15.f) // add padding
    		{
    			obstruction.SetTranslation(new Vector4(camPos.GetX() + ((float)Math.random() - 0.5f) * 10.f, 
    					camPos.GetY() - 15.f + ((float)Math.random() - 0.5f) * 5.f, pos.GetZ()));
    		}
    	}*/
 /*   	
    	mTrack.Update();*/
	}
	
	public synchronized void Render()
	{
		if (mState != State.Loaded)
			return;
		
        mCamera.Bind();
        
        // TODO: disabling on android for speed ATM
        //RenderGround();
        //mTrack.Render();
        
        //draw debug spline
        //mSpline.DrawDebug(new Vector4(1.f, 0.f, 1.f, 1.f));
    	
    	// draw actors
    	for (int i = 0; i < mActor.size(); ++i)
    	{
    		Actor actor = mActor.get(i);
    		actor.Render();
    	}
    	
    	/*
    	// draw obstructions
    	for (int i = 0; i < mObstruction.size(); ++i)
    	{
    		Obstruction obstruction = mObstruction.get(i);
    		obstruction.Render();
    	}*/
	}
	
	public void RenderGround()
	{
		Device device = Game.Get().GetFactory().GetDevice();
		
		float aspectRatio = device.GetAspectRatio();

		// set new texture matrix to scroll the texture
		Matrix4 t = new Matrix4();
    	t.SetIdentity();
    	t.SetScale(new Vector4(1.f, 1.f, 0.f));
    	t.SetTranslation(mBackground.scroll.Multiply(new Vector4(1.f / (mOrthoScale * aspectRatio * 2.0f), 1.f / (mOrthoScale * 2.f), 0.f)));
    	device.SetMatrix(t, Device.MatrixMode.Texture);
    	
		/*
    	Vector4 uvs[] = {
    			(new Vector4(0.f, 1.f)).Subtract(mBackground.position),
    			(new Vector4(1.f, 1.f)).Subtract(mBackground.position),
    			(new Vector4(0.f, 0.f)).Subtract(mBackground.position),
    			(new Vector4(1.f, 0.f)).Subtract(mBackground.position),
	        };  	
		
		mBackground.geometry.SetUVs(uvs);
		*/
		
    	// set the background matrix
		Matrix4 m = new Matrix4();
    	m.SetIdentity();
    	m.SetTranslation(mBackground.position);
    	device.SetMatrix(m, Device.MatrixMode.Model);
    	
    	// bind texture and draw
    	mBackground.texture.Bind();
    	mBackground.geometry.Bind();
    	mBackground.geometry.Render();
    	mBackground.geometry.Unbind();
    	mBackground.texture.Unbind();
		
		// restore original texture matrix
    	t.SetIdentity();
    	device.SetMatrix(t, Device.MatrixMode.Texture);
	}
	
	public synchronized void InsertActor(Actor actor)
	{
		mActor.add(actor);
	}
	
	public Object LoadFromTemplate(Class type)
	{
		for (int i = 0; i < mObjectTemplates.size(); ++i)
		{
			Object obj = mObjectTemplates.get(i);
			if (obj.getClass() == type)
			{
				XMLSerialize objectCloner = new XMLSerialize();
				
				return objectCloner.Clone(obj);
			}
		}
		
		return null;
	}
	
	public void SetState(State state)
	{
		mState = state;
	}
	
	@Override
	public void NotifyAddConnection(Connection connection)
	{
		System.out.println("A new client has connected");
		
		// create a new actor and director for this network player to control
		NetworkDirector netDirector = (NetworkDirector) Director.Create(Director.Type.Network);
    	netDirector.SetConnection(connection);
    	
    	Actor netActor = (Actor)LoadFromTemplate(Actor.class);
    	netActor.SetDirector(netDirector);
    	InsertActor(netActor);
	}
	
	@Override
	public void NotifyRemoveConnection(Connection connection)
	{
		System.out.println("A client has disconnected");
		
		for (int i = 0; i < mActor.size(); ++i)
		{
			try
	    	{
				Actor actor = mActor.get(i);
				Director director = actor.GetDirector();
				NetworkDirector netDirector = NetworkDirector.class.cast(director);
	
				if (netDirector != null && netDirector.GetConnection() == connection)
				{
					mActor.remove(i);
				}
	    	}
	    	catch (Exception e)
	    	{
	    		// not a network director class
	    	}

		}
	}
	
	State					mState				= State.Unloaded;
	
	float					mOrthoScale			= 15.f;
	
	int						mDesiredNumObstructions = 2; // number of obstructions we want around the player at once
	
	Camera					mCamera				= new Camera();
	Background				mBackground 		= new Background();
	Vector<Actor>			mActor				= new Vector<Actor>();
	Vector<Obstruction>		mObstruction 		= new Vector<Obstruction>();
	
	Vector<Object> 			mObjectTemplates 	= new Vector<Object>();
	
	BezierSpline			mSpline				= new BezierSpline();
	RaceTrack				mTrack				= new RaceTrack();
}
