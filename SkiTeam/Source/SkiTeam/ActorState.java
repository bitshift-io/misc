package SkiTeam;

public class ActorState
{
	public void SetOwner(Actor owner)
	{
		mOwner = owner;
	}
	
	public Actor GetOwner()
	{
		return mOwner;
	}
	
	public void Enter(ActorState prev)
	{
		
	}
	
	public void Update()
	{

	}
	
	private Actor mOwner;
}
