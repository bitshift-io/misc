package SkiTeam;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.RenderBuffer;
import BlackCarbon.API.Texture;
import BlackCarbon.API.Device.DeviceState;
import BlackCarbon.Math.BezierSpline;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;

//
// trail fx for actor
//
public class Trail 
{
	Trail(int numNode)
	{
		mMaxNodes = numNode;
		mSpline.controlPoints = new LinkedList<BezierSpline.ControlPoint>();
		mSpline.type = BezierSpline.Type.Quadratic;
		
		//mRenderBuffer = Factory.Get().CreateRenderBuffer();
		
		mTexture = Factory.Get().AcquireTexture("actortrail");
	}
	
	void AddNode(Vector4 node, Vector4 velocity)
	{
		LinkedList<BezierSpline.ControlPoint> list = (LinkedList<BezierSpline.ControlPoint>)mSpline.controlPoints;
		
		BezierSpline.ControlPoint controlPoint = mSpline.new ControlPoint();
		controlPoint.position = node;
		
		BezierSpline.ControlPoint lastControlPoint = null;
		if (list.size() > 0)
		{
			lastControlPoint = list.getLast();
	
			// set tangent point as half the distance
			float distanceToLast = node.Subtract(lastControlPoint.position).Magnitude3();
			controlPoint.tangent[0] = velocity.Multiply(-distanceToLast * 0.5f);
		}
		
		mSpline.Add(controlPoint);
		
		if (mSpline.GetSize() >= mMaxNodes)
		{
			list.remove();
		}
	}
	
	Vector4 GetLastNode()
	{
		LinkedList<BezierSpline.ControlPoint> list = (LinkedList<BezierSpline.ControlPoint>)mSpline.controlPoints;
		if (list.size() <= 0)
			return null;
		
		return list.getLast().position;
	}
	
	void Render()
	{
		Device device = Factory.Get().GetDevice();
		
		// set device state - enable alpha blending
    	Device.DeviceState state = device.new DeviceState();
    	state.alphaBlendEnable = true;
    	device.SetDeviceState(state);
    	
		Matrix4 m = new Matrix4(Matrix4.Init.Identity);
    	device.SetMatrix(m, Device.MatrixMode.Model);
    	
    	Vector4 colour = new Vector4(1.f, 1.f, 1.f, 1.f);
    	
    	float halfWidth = mWidth * 0.5f;
    	float z = 7.f;
    	
    	mTexture.Bind();
    	
    	float increment = 0.005f;
		for (float p = 0.f; p < 1.f; p += increment)
		{
			
	    	
			Vector4 pos0 = mSpline.Evaluate(p);
			Vector4 pos1 = mSpline.Evaluate(p + increment);
			
			device.Primitive_Begin(Device.GeometryTopology.QuadList);
	    	device.Primitive_Colour(colour);
	    	
	    	device.Primitive_TexCoord(new Vector4(p, 0.f));
			device.Primitive_Vertex(pos0.Add(new Vector4(-halfWidth, 0.f, z)));
			
			device.Primitive_TexCoord(new Vector4(p, 1.f));
			device.Primitive_Vertex(pos0.Add(new Vector4(halfWidth, 0.f, z)));
			
			
			
			device.Primitive_TexCoord(new Vector4(p + increment, 1.f));
			device.Primitive_Vertex(pos1.Add(new Vector4(halfWidth, 0.f, z)));
			
			device.Primitive_TexCoord(new Vector4(p + increment, 0.f));
			device.Primitive_Vertex(pos1.Add(new Vector4(-halfWidth, 0.f, z)));
			
			
			device.Primitive_End();
			
			
			//device.DrawLine(pos0.Add(new Vector4(-halfWidth, 0.f, 0.f)), pos0.Add(new Vector4(halfWidth, 0.f, 0.f)), colour);
			//device.DrawLine(pos1.Add(new Vector4(halfWidth, 0.f, 0.f)), pos1.Add(new Vector4(halfWidth, 0.f, 0.f)), colour);
		}
		
		mTexture.Unbind();
		
		// disable alpha blending
		state.alphaBlendEnable = false;
    	device.SetDeviceState(state);
    	
    	//mSpline.DrawDebug(new Vector4(1.f, 0.f, 0.f, 1.f));
	}
	
	float			mWidth	=	0.5f;
	BezierSpline	mSpline	= new BezierSpline();
	Factory 		mFactory;
	int				mMaxNodes;
	//RenderBuffer	mRenderBuffer;	
	Texture			mTexture;
}
