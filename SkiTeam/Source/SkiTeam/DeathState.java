package SkiTeam;

import BlackCarbon.Math.Vector4;
import BlackCarbon.Ext.Game;
import BlackCarbon.Ext.Sprite;

public class DeathState extends ActorState
{/*
	public DeathState(Actor owner)
	{
		super(owner);
		
		mSprite = new Sprite("actordeath", 4, 1);
		mSprite.mAnimationFlags = Sprite.AF_Horizontal | Sprite.AF_NoLoop;
		mSprite.mFrameRate = 12;
		mSprite.mTransform.SetScale(new Vector4(2.f, 2.f, -2.f));
		mSprite.mTransform.SetTranslation(new Vector4(0.f, 0.f, -9.0f));
	}*/
	
	public void Enter(ActorState prev)
	{
		Actor owner = GetOwner();
		owner.SetSprite(mSprite);
		mSprite.ResetAnimation();
		mTimeInState = 0.f;
	}

	public void Update()
	{
		Actor owner = GetOwner();
		Game game = (Game)Game.Get();
		
		float deltaTime = game.GetDeltaTime();
		
		mTimeInState += deltaTime;
		
		if (mTimeInState > mMaxTimeInState)
			owner.SetState(LiveState.class);
		
		// check for collisions, so if we collide with something
		// we need to reflect away from that,
		// so once we get back up
		// we are clear of obstructions
		Vector4 colVec = new Vector4();
		Obstruction collision = owner.CheckForCollision(colVec);
		if (collision != null)
		{
			// only push left and right
			colVec.SetY(0.f);
			colVec.SetZ(0.f);
			owner.mVelocity = owner.mVelocity.Add(colVec);
			
			// stay dead till we slide out of collisions
			mTimeInState = 0.f;
		}
		else
		{
			owner.mVelocity = owner.mVelocity.Multiply(0.9f); // slow down to no speed
		}
		
		// apply velocity ie. move actor
		owner.mSprite.mTransform.SetTranslation(owner.mSprite.mTransform.GetTranslation().Add(owner.mVelocity.Multiply(deltaTime)));
	}
	
	float 	mTimeInState;
	float 	mMaxTimeInState 		= 3.f;
	Sprite	mSprite;
}

