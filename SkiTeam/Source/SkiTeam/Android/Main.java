package SkiTeam.Android;

import java.util.concurrent.TimeUnit;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import BlackCarbon.Android.AndroidDevice;
import BlackCarbon.Android.AndroidFactory;
import BlackCarbon.Math.Math;
import SkiTeam.Game;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;

public class Main extends Activity implements BlackCarbon.Android.GLSurfaceView.Renderer
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
    	try
    	{
	        super.onCreate(savedInstanceState);

	        Game game = new Game();
			AndroidFactory.InitParam init = new AndroidFactory.InitParam();
			init.resourceDir = "SkiTeam.Android:raw";
			init.activity = this;
			game.mFactory = new AndroidFactory(init);
			game.mGame = game;
    	}
    	catch (Exception e)
    	{
    		System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
    	}
    }

    @Override
    protected void onPause() 
    {
        super.onPause();
        
        AndroidFactory factory = (AndroidFactory) Game.Get().GetFactory();
        factory.OnPause();
    }

    @Override
    protected void onResume() 
    {
        super.onResume();
        
        AndroidFactory factory = (AndroidFactory) Game.Get().GetFactory();
        factory.OnResume();
    }
    
    @Override 
    public boolean onKeyUp(int keyCode, KeyEvent event) 
    { 
        switch(keyCode) 
        { 
        case KeyEvent.KEYCODE_DPAD_CENTER: 
            finish();
            return true; 
        } 
        
        return false; 
    } 

	@Override
	public void onDrawFrame(GL10 gl)
	{
		long startTime = System.currentTimeMillis();
    	
    	//bRunning = 
    	Game.Get().Update();
    	Game.Get().Render();

    	long endTime = System.currentTimeMillis(); // need to account for how long it takes to draw here
    	long deltaTime = endTime - startTime;
    	float idealLoopTime = Game.Get().GetDeltaTime() * 1000.f; // lock frame rate to 30
    	long sleepTime = (long)Math.Max(0.f, idealLoopTime - (float)deltaTime);
    	
    	sleepTime -= 8; // hrmm to account for androidage?
    	
    	try 
    	{
    		TimeUnit.MILLISECONDS.sleep(sleepTime);
		} 
    	catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height)
	{
		Game.Get().GetFactory().GetDevice().Resize(width, height);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
    	AndroidDevice device = (AndroidDevice)Game.Get().GetFactory().GetDevice();
    	if (gl instanceof GL11)
    		device.mGL = (GL11)gl;
    	
    	Game.Get().Init();
	}
	
    Game	mSkiTeam;
}
