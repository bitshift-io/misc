package SkiTeam;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;

import BlackCarbon.API.Device;
import BlackCarbon.API.Input;
import BlackCarbon.API.Network;
import BlackCarbon.API.Input.TouchState;
import BlackCarbon.Ext.Game;
import BlackCarbon.Math.Vector4;

//
// directs an actor
//
public class Director
{
	enum Type
	{
		Player,
		AI,
		Network
	}
	
	enum Action
	{
		Left,
		Right,
		Accelerate,
		Brake,
		
		MAX,
	}
	
	public Director()
	{
		for (int i = 0; i < Action.MAX.ordinal(); ++i)
		{
			mActionState[i] = new ActionState();
		}
	}
	
	public float GetActionHeld(Action action)
	{
		return mActionState[action.ordinal()].value;
	}
	
	public boolean GetActionPressed(Action action)
	{
		return mActionState[action.ordinal()].value > 0.5f && mActionState[action.ordinal()].lastValue <= 0.5f;
	}
	
	// DO NOT CALL THIS IN THE NetworkDirector, as it modifies the timestamp
	public void Update()
	{
		++mTimeStamp;
		
		for (int i = 0; i < Action.MAX.ordinal(); ++i)
		{
			mActionState[i].lastValue = mActionState[i].value;
		}
	}
	
	public Score GetScore()
	{
		return mScore;
	}
	
	public void SetActor(Actor actor)
	{
		mActor = actor;
	}
	
	static Director Create(Type type)
	{
		switch (type)
		{
		case Player:
			return new PlayerDirector();
			
		case Network:
			return new NetworkDirector();
		}
		
		return null;
	}
	
	class ActionState
	{
		float lastValue;
		float value;
	}
	
	ActionState 	mActionState[] 	= new ActionState[Action.MAX.ordinal()];
	Actor			mActor;
	Score			mScore			= new Score();
	long			mTimeStamp		= 0;
}

class PacketData implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2328105976847146556L;
	
	public long 	timeStamp;			// for discarding out of order packets
	public Vector4 	translation;
	public Class 	state;
	public float 	rotation;
}

class NetworkDirector extends Director
{	
	public void SetConnection(Network.Connection connection)
	{
		mConnection = connection;
	}
	
	public Network.Connection GetConnection()
	{
		return mConnection;
	}
	
	@Override
	public void Update()
	{
		try 
		{
			// read packet from network
			byte[] buf = new byte[1024];
			DatagramSocket udp = mConnection.GetUDPSocket();
			
			while (true)
		    {
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				udp.receive(packet);
				mConnection.udpBytesReceived += packet.getLength();
				mConnection.lastPacket = 0; // we received a packet, so clear this
				
				// deserialize from a byte array - can treat as an object, then query its class type
				ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(buf));
				PacketData packetData = (PacketData) in.readObject();
			    in.close();
				
			    // modify actor, if this is a newer packet than we have previously received
			    if (packetData.timeStamp > mTimeStamp)
			    {
			    	mTimeStamp = packetData.timeStamp;
			    
				    mActor.SetState(packetData.state);
				    mActor.SetTranslation(packetData.translation);
				    mActor.SetRotation(packetData.rotation);
			    }
		    }
		    
		} 
		catch (SocketTimeoutException e) 
	    {
	    	// out of packets
	    }
		catch (Exception e)
		{
			System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
		}
	}
	
	Network.Connection 	mConnection;
}

class PlayerDirector extends Director
{
	@Override
	public void Update()
	{
		super.Update();
		
		Device device = Game.Get().GetFactory().GetDevice();
		Input input = Game.Get().GetFactory().GetInput();
			
		mActionState[Action.Left.ordinal()].value = input.GetState(Input.Control.KeyLeft);
		mActionState[Action.Right.ordinal()].value = input.GetState(Input.Control.KeyRight);
		
		mActionState[Action.Accelerate.ordinal()].value = input.GetState(Input.Control.KeyDown);
		mActionState[Action.Brake.ordinal()].value = input.GetState(Input.Control.KeyUp);
		/*
		// accelerometer controls for Android
		if (input.GetState(Input.Control.AccelerometerX) < 0.f)
			mActionState[Action.Right.ordinal()].value = Math.abs(input.GetState(Input.Control.AccelerometerX)) * 0.5f;
		
		if (input.GetState(Input.Control.AccelerometerX) > 0.f)
			mActionState[Action.Left.ordinal()].value = Math.abs(input.GetState(Input.Control.AccelerometerX)) * 0.5f;
		*/
		// touch for Android
		TouchState touch = (TouchState) input.GetControlState(Input.Control.Touch);
		if (touch.state > 0.f)
		{
			float xTouchPercent = touch.x / device.GetWidth();
			if (xTouchPercent < 0.33f)
				mActionState[Action.Left.ordinal()].value = 1.f;
			else if (xTouchPercent > 0.66f)
				mActionState[Action.Right.ordinal()].value = 1.f;
		}
			
		
		/*
		mActionState[Action.Left.ordinal()].value = Game.Get().mKeyState[KeyEvent.KEYCODE_DPAD_LEFT];
		mActionState[Action.Right.ordinal()].value = Game.Get().mKeyState[KeyEvent.KEYCODE_DPAD_RIGHT];
		
		mActionState[Action.Accelerate.ordinal()].value = Game.Get().mKeyState[KeyEvent.KEYCODE_DPAD_DOWN];
		mActionState[Action.Brake.ordinal()].value = Game.Get().mKeyState[KeyEvent.KEYCODE_DPAD_UP];*/
		
		// sent packet once every half a second, so we dont flood the network and slow the fps down
		mNetworkSendTimer += Game.Get().GetDeltaTime();
		if (mNetworkSendTimer <= 0.1f)
			return;
		
		mNetworkSendTimer = 0.f;
		
		// broadcast our data to all network connections
		try 
		{
			PacketData packetData = new PacketData();
			packetData.timeStamp = mTimeStamp;
			packetData.state = mActor.GetState().getClass();
			packetData.rotation = mActor.GetRotation();
			packetData.translation = mActor.GetTranslation();
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos;
			oos = new ObjectOutputStream(baos);
			
			oos.writeObject(packetData);
			byte[] buf = baos.toByteArray();
			
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			Game.Get().GetFactory().GetNetwork().Broadcast(packet);
		} 
		catch (Exception e)
		{
			System.out.print(e.getLocalizedMessage());
    		e.printStackTrace();
		}
	}
	
	float mNetworkSendTimer;
}