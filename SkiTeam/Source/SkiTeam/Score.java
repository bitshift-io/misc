package SkiTeam;

import java.util.LinkedList;
import java.util.List;

import BlackCarbon.API.Camera;
import BlackCarbon.API.Device;
import BlackCarbon.API.Factory;
import BlackCarbon.API.Font;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;
import BlackCarbon.API.Device.DeviceState;
import BlackCarbon.API.Device.MatrixMode;
import SkiTeam.Game;

public class Score
{
	public class ScoreAnim
	{
		public ScoreAnim(String text, float score, Obstruction obstruction)
		{
			mText = text;
			mScore = score;
			mObstruction = obstruction;
		}
		
		public Vector4 ConvertToScreenSpace(Vector4 worldPos)
		{
			Device device = Factory.Get().GetDevice();
			
			Matrix4 projection = device.GetMatrix(Device.MatrixMode.Projection);
			Matrix4 view = device.GetMatrix(Device.MatrixMode.View);

			Vector4 screenPos = view.Multiply(worldPos);
			screenPos = projection.Multiply(screenPos);
			screenPos.SetZ(0.f);
			
			Matrix4 newProjection = new Matrix4();
			newProjection.SetOrthographic(-1.f, 1.f, 1.f, -1.f, 0.f, 1.f);
			
			Matrix4 newProjectionInv = newProjection.GetInverse();
			screenPos = newProjectionInv.Multiply(screenPos);
			
			return screenPos;
		}
		
		public boolean Update()
		{
			if (mScrollToScore)
			{
				mPosition.SetX(mPosition.GetX() - mMovementSpeed * Game.Get().GetDeltaTime());
				
				// add to score
				if (mPosition.GetX() < (-1.f + (mScoreDimensions.GetX() * mFontScale)))
				{
					AddScore(mScore);
					return false;
				}
			}
			else
			{
				mPosition = ConvertToScreenSpace(mObstruction.GetTranslation());
				if (mPosition.GetY() >= 1.f)
					mScrollToScore = true;
			}

			return true;
		}
		
		public void Render()
		{
			Device device = Factory.Get().GetDevice();
			
			
			Matrix4 transform = new Matrix4(Matrix4.Init.Identity);
			transform.SetScale(new Vector4(mFontScale, mFontScale));
			transform.SetTranslation(mPosition);
			device.SetMatrix(transform, MatrixMode.Model);
			mFont.Render(mText);
		}
		
		public Obstruction mObstruction;
		
		public boolean  mScrollToScore			= false;
		public float	mMovementSpeed 			= 1.0f;
		public float	mTime;
		public Vector4	mPosition;
		public String 	mText;
		public float 	mScore;
	}
	
	
	Score()
	{
		Game game = (Game)Game.Get();
		mFont = Factory.Get().AcquireFont(game.GetPrimaryFontName());
		mFont.SetHorizontalAlign(Font.HorizontalAlign.Right);
		mFont.SetVerticalAlign(Font.VerticalAlign.Below);	
	}
	
	public void Update()
	{
		for (int i = 0; i < mScoreAnim.size(); ++i)
		{
			ScoreAnim scoreAnim = mScoreAnim.get(i);
			if (!scoreAnim.Update())
			{
				mScoreAnim.remove(i);
				--i;
			}
		}
	}
	
	public void Render()
	{
		Device device = Factory.Get().GetDevice();
		
		Matrix4 projection = device.GetMatrix(Device.MatrixMode.Projection);
		Matrix4 view = device.GetMatrix(Device.MatrixMode.View);
		
    	Camera uiCamera = new Camera();
    	uiCamera.mFactory = Factory.Get();
    	uiCamera.SetOrthographic(2.f, 2.f, 1.f, 15.f);
    	uiCamera.Bind();
    	
		// set device state - enable alpha blending
    	Device.DeviceState state = device.new DeviceState();
    	state.alphaBlendEnable = true;
    	device.SetDeviceState(state);
    	
    	Matrix4 transform = new Matrix4(Matrix4.Init.Identity);
		transform.SetScale(new Vector4(mFontScale, mFontScale));
		transform.SetTranslation(new Vector4(-1.f, 1.f, 0.f));
		device.SetMatrix(transform, MatrixMode.Model);
		
		String text = String.format("Score %.0f", mScore);
		mScoreDimensions = mFont.GetDimensions(text);
		mFont.Render(text);
		
		//String text = String.format("%i", mNumCheckpoints);
		//mFont.Render("" + mScore);
		
		for (int i = 0; i < mScoreAnim.size(); ++i)
		{
			ScoreAnim scoreAnim = mScoreAnim.get(i);
			scoreAnim.Render();
		}

		// disable alpha blending
		state.alphaBlendEnable = false;
    	device.SetDeviceState(state);
    	
    	// restore matricies
    	device.SetMatrix(projection, Device.MatrixMode.Projection);
    	device.SetMatrix(view, Device.MatrixMode.View);
	}

	public void AddDistance(float distance)
	{
		mDistance += distance;
	}
	
	public void AddTime(float time)
	{
		mTime += time;
	}
	
	public void AddScore(float score)
	{
		mScore += score;
	}

	public void PassedCheckpoint(Obstruction obstruction)
	{	
		mScoreAnim.add(new ScoreAnim("10", 10.f, obstruction));
		++mNumCheckpoints;
	}
	
	public void HitJump(Obstruction obstruction)
	{
		mScoreAnim.add(new ScoreAnim("1", 1.f, obstruction));
		++mNumJumps;
	}
	
	public Vector4	mScoreDimensions;
	
	public Font		mFont;
	public float 	mFontScale 				= 0.01f;
	
	public float	mScore;
	public float	mTime;
	public int		mNumCheckpoints;
	public int		mNumDeaths;
	public int		mNumJumps;
	public float 	mDistance;
	
	public List<ScoreAnim>	mScoreAnim = new LinkedList<ScoreAnim>();
}
