package SkiTeam;

import java.util.Random;
import java.util.Vector;

import BlackCarbon.API.Device;
import BlackCarbon.API.XMLSerializable;
import BlackCarbon.API.XMLSerialize;
import BlackCarbon.Math.Math;
import BlackCarbon.Math.Line;
import BlackCarbon.Math.Matrix4;
import BlackCarbon.Math.Vector4;
import BlackCarbon.API.Device.MatrixMode;
import BlackCarbon.Ext.Game;
import BlackCarbon.Ext.Sprite;


//
// stuff you can collide with
//
public class Obstruction implements XMLSerializable
{
	public enum Type
	{
		Obstruction,
		Actor,
		Jump,
		Checkpoint,
		FinishCheckpoint,
	}
	
	public boolean ParseAttribute(XMLSerialize serialize, String name, String value)
	{
		return false;
	}
	
	public void SetParent(XMLSerialize serialize, Object parent)
	{
		
	}
	
	public void InsertChild(XMLSerialize serialize, Object child)
	{
		
	}
	
	public void	Resolve(XMLSerialize serialize)
	{
		
	}
	
	public XMLSerializable Clone()
	{
		return null;
	}
	
	public Obstruction()
	{
		//Load();
	}
	/*
	// todo: load from xml
	public void Load()
	{
		
		Random r = new Random();
		int i = r.nextInt(5);
		
		switch (i)
		{
		default:
			/*
		case 0:
			mSprite = new Sprite("snowman", 1, 1);
			mSprite.mTransform.SetScale(new Vector4(2.f, 2.f, -2.f));
			mSphere.add(new Vector4(0.f, 0.f, 0.f, 0.3f));
			break;
			* /
		case 1:
			mSprite = new Sprite("finishflag", 4, 1);
			mSprite.mTransform.SetScale(new Vector4(8.f, 4.f, -4.f));
			mType = Type.FinishCheckpoint;
			mSprite.mAnimationFlags = Sprite.AF_Horizontal;
			mSprite.mFrameRate = 4;
			mSphere.add(new Vector4(3.f, 0.f, 0.f, 0.01f));
			mSphere.add(new Vector4(-3.f, 0.f, 0.f, 0.01f));
			break;
			/*
		case 2:
			mSprite = new Sprite("markerflag", 4, 1);
			mSprite.mTransform.SetScale(new Vector4(2.f, 2.f, -2.f));
			mType = Type.FinishCheckpoint;
			mSprite.mAnimationFlags = Sprite.AF_Horizontal;
			mSprite.mFrameRate = 8;
			mSphere.add(new Vector4(3.f, 0.f, 0.f, 0.01f));
			mSphere.add(new Vector4(-3.f, 0.f, 0.f, 0.01f));
			break;
			
		case 3:
			mSprite = new Sprite("pinetree", 16, 1);
			mSprite.mTransform.SetScale(new Vector4(2.f, 2.f, -2.f));
			mSprite.mAnimationFlags = Sprite.AF_Horizontal;
			mSprite.mFrameRate = 12;
			mSphere.add(new Vector4(0.f, 0.f, 0.f, 0.3f));
			break;
			
		case 4:
			mSprite = new Sprite("jump", 1, 1);
			mSprite.mTransform.SetScale(new Vector4(2.f, 2.f, -2.f));
			mType = Type.Jump;
			mSphere.add(new Vector4(0.f, 0.f, 0.f, 0.5f));
			break;* /
		}
	}*/
	
	public void SetTranslation(Vector4 translation)
	{
		mSprite.mTransform.SetTranslation(translation);
	}
	
	public Vector4 GetTranslation()
	{
		return mSprite.mTransform.GetTranslation();
	}
	
	public void Update()
	{
		float deltaTime = ((Game)Game.Get()).GetDeltaTime();
		mSprite.Update(deltaTime);
	}
	
	public void Render()
	{
		Device device = Game.Get().GetFactory().GetDevice();
		
		mSprite.Render();
		
		// debug rendering
		if (Game.Get().IsDebug())
		{
			// set device state - disable alpha blending
	    	Device.DeviceState state = device.new DeviceState();
	    	state.alphaBlendEnable = false;
	    	device.SetDeviceState(state);
	    	
	    	device.SetMatrix(new Matrix4(Matrix4.Init.Identity), MatrixMode.Model);
			for (int i = 0; i < mSphere.size(); ++i)
			{
				Vector4 sphere = GetWorldSpaceSphere(i);
				Game.Get().GetFactory().GetDevice().DrawSphere(sphere, new Vector4(1.f, 0.f, 0.f));
			}
		}
	}
	
	// returns vector to move this obstruction
	// out of collision
	public Vector4 CollidesWith(Obstruction other)
	{
		for (int o = 0; o < other.mSphere.size(); ++o)
		{
			for (int t = 0; t < mSphere.size(); ++t)
			{
				Vector4 thisSphere = GetWorldSpaceSphere(t);
				Vector4 otherSphere = other.GetWorldSpaceSphere(o);
				
				Vector4 vecBetween = thisSphere.Subtract(otherSphere);
				float distBetweenSqrd = vecBetween.MagnitudeSqrd3();
				float colDistSqrd = (thisSphere.GetW() * thisSphere.GetW()) + (otherSphere.GetW() * otherSphere.GetW());
				if (distBetweenSqrd < colDistSqrd)
				{
					float distToMoveOutOfCol = Math.Sqrt(colDistSqrd - distBetweenSqrd);
					Vector4 vecToMoveOutOfCol = vecBetween.Multiply(distToMoveOutOfCol);
					return vecToMoveOutOfCol;
				}
			}
		}
		
		return null;
	}
	
	public boolean PassedThroughCheckPoint(Vector4 prevPos, Vector4 curPos)
	{
		if (GetType() != Type.Checkpoint && GetType() != Type.FinishCheckpoint)
			return false;
		
		// we expect 2 obstruction spheres
		Vector4 sphere0 = GetWorldSpaceSphere(0);
		Vector4 sphere1 = GetWorldSpaceSphere(1);
		
		// remove any z-height
		sphere0.SetZ(0.f);
		sphere1.SetZ(0.f);
		
		prevPos.SetZ(0.f);
		curPos.SetZ(0.f);
		
		Line checkPoint = new Line(sphere0, sphere1, Line.Type.Line);
		Line movingObj = new Line(prevPos, curPos, Line.Type.Line);
		
		Line result = checkPoint.ShortestSegmentBetweenTwoSegments(movingObj, false);
		if (result == null)
			return false;
		
		// time is in w component, if outside of the range 0 to 1, 
		// then the shortest line between the two lines
		// is outside of the segment
		if (result.v[0].GetW() < 0.f || result.v[0].GetW() > 1.f
			|| result.v[1].GetW() < 0.f || result.v[1].GetW() > 1.f)
			return false;
		
		return true;
	}
	
	public Vector4 GetWorldSpaceSphere(int index)
	{
		Vector4 translation = mSprite.mTransform.GetTranslation();
		translation.SetZ(0.f);
		return translation.Add(mSphere.get(index));
	}
	
	public Type GetType()
	{
		return mType;
	}
	
	public Type					mType			= Type.Obstruction;
	public String				mName;										// for debugging
	public Sprite 				mSprite;									// visual
	public Vector<Vector4>		mSphere 		= new Vector<Vector4>();	// list of obstruction spheres in local space
}
