package SkiTeam.Desktop;

import SkiTeam.Game;

import BlackCarbon.Desktop.DesktopFactory;

// http://www.cs.umd.edu/~meesh/kmconroy/JOGLTutorial/
public class Main extends Game
{
	public static void main(String[] args)
	{		
		Main game = new Main();
		game.Run();
	}
	
	public Main()
	{
		DesktopFactory.InitParam init = new DesktopFactory.InitParam();
		init.resourceDir = "Data";
		mFactory = new DesktopFactory(init); 
	}
}