﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WebCam
{
    public partial class Form1 : Form
    {
        NNet m_nnet;

        WebCam webCam;
        Bitmap m_previousImage;

        public Form1()
        {
            InitializeComponent();

            m_nnet = new NNet();
            NeuronLayer[] layer = new NeuronLayer[1];
            layer[0] = new NeuronLayer(50 * 50);
            //layer[1] = new NeuronLayer(50 * 50);
            //layer[2] = new NeuronLayer(320 * 240);
            m_nnet.create(50 * 50, layer);
            m_nnet.mLearnRate = 0.01;
        }


        private void WebCamEventHandler(object source, WebcamEventArgs e)
        {
            pictureBox1.Image = e.WebCamImage;

            /*
            Bitmap downsample = (Bitmap)e.WebCamImage.GetThumbnailImage(50, 50, null, System.IntPtr.Zero);
            pictureBox4.Image = ImageManipulation.ToGrayScale(downsample);
            
            double[] dbleImage = ImageManipulation.ToGrayScaleDoubleArray(downsample);
            double[] result = m_nnet.Train(dbleImage, dbleImage); //m_nnet.Compute(dbleImage);
            pictureBox5.Image = ImageManipulation.ToGrayScaleImage(result, downsample.Width, downsample.Height);
            */
            
            pictureBox3.Image = ImageManipulation.EdgeDetection(e.WebCamImage);

            if (m_previousImage != null)
            {
                Bitmap a = ImageManipulation.Subtract(m_previousImage, e.WebCamImage);
                a = ImageManipulation.ToGrayScale(a);
                a =  ImageManipulation.Multiply(a, 10);
                pictureBox2.Image = a;
            }

            m_previousImage = e.WebCamImage;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            webCam = new WebCam();
            webCam.ImageCaptured += WebCamEventHandler;
            //webCam.Connect();
            //webCam.Container = pictureBox1;
        }
        

        private void btnStart_Click(object sender, EventArgs e)
        {
            webCam.Connect();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            webCam.Disconnect();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            //webCam.Connect();
        }
    }
}
