﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace WebCam
{
    class ImageManipulation
    {
        public static Bitmap Add(Bitmap a, Bitmap b)
        {
            if (a.Width != b.Width || a.Height != b.Height)
                return null;

            Bitmap result = new Bitmap(a.Width, a.Height);

            for (int y = 0; y < a.Height; ++y)
            {
                for (int x = 0; x < a.Width; ++x)
                {
                    Color colorA = a.GetPixel(x, y);
                    Color colorB = b.GetPixel(x, y);
                    Color color = Color.FromArgb(Math.Min(255, colorA.R + colorB.R), Math.Min(255, colorA.G + colorB.G), Math.Min(255, colorA.B + colorB.B));
                    result.SetPixel(x, y, color);
                }
            }

            return result;
        }

        public static Bitmap Subtract(Bitmap a, Bitmap b)
        {
            if (a.Width != b.Width || a.Height != b.Height)
                return null;

            Bitmap result = new Bitmap(a.Width, a.Height);

            for (int y = 0; y < a.Height; ++y)
            {
                for (int x = 0; x < a.Width; ++x)
                {
                    Color colorA = a.GetPixel(x, y);
                    Color colorB = b.GetPixel(x, y);
                    Color color =  Color.FromArgb(Math.Max(0, colorA.R - colorB.R), Math.Max(0, colorA.G - colorB.G), Math.Max(0, colorA.B - colorB.B));
                    result.SetPixel(x, y, color);
                }
            }

            return result;
        }

        public static Bitmap Multiply(Bitmap a, float value)
        {
            Bitmap result = new Bitmap(a.Width, a.Height);

            for (int y = 0; y < a.Height; ++y)
            {
                for (int x = 0; x < a.Width; ++x)
                {
                    Color colorA = a.GetPixel(x, y);
                    Color color = Color.FromArgb(Math.Min(255, (int)((float)colorA.R * value)), Math.Min(255, (int)((float)colorA.G * value)), Math.Min(255, (int)((float)colorA.B * value)));
                    result.SetPixel(x, y, color);
                }
            }

            return result;
        }

        public static Bitmap ToGrayScale(Bitmap a)
        {
            Bitmap result = new Bitmap(a.Width, a.Height);

            for (int y = 0; y < a.Height; ++y)
            {
                for (int x = 0; x < a.Width; ++x)
                {
                    Color colorA = a.GetPixel(x, y);
                    int brightness = (colorA.R + colorA.G + colorA.B) / 3;
                    Color color = Color.FromArgb(brightness, brightness, brightness);
                    result.SetPixel(x, y, color);
                }
            }

            return result;
        }

        public static Bitmap EdgeDetection(Bitmap a)
        {
            Bitmap result = new Bitmap(a.Width, a.Height);

            for (int y = 0; y < a.Height; ++y)
            {
                for (int x = 0; x < a.Width; ++x)
                {
                    Color colorA = a.GetPixel(x, y);

                    // this should be done and passed in as a point array( filter parameter)
                    int edgeDelta = 0;

                    if ((x - 2) >= 0)
                        edgeDelta = Math.Max(edgeDelta, EdgeDelta(colorA, a.GetPixel(x - 1, y)));

                    if ((x + 2) < a.Width)
                        edgeDelta = Math.Max(edgeDelta, EdgeDelta(colorA, a.GetPixel(x + 1, y)));

                    if ((y + 2) < a.Height)
                        edgeDelta = Math.Max(edgeDelta, EdgeDelta(colorA, a.GetPixel(x, y + 1)));

                    if ((y - 2) >= 0)
                        edgeDelta = Math.Max(edgeDelta, EdgeDelta(colorA, a.GetPixel(x, y - 1)));

                    Color color = Color.FromArgb(edgeDelta, edgeDelta, edgeDelta);
                    result.SetPixel(x, y, color);
                }
            }

            // reduce noise, if its a lonesome pixel, kill it
            for (int y = 0; y < a.Height; ++y)
            {
                for (int x = 0; x < a.Width; ++x)
                {
                    Color colorA = result.GetPixel(x, y);

                    if (colorA.R <= 0)
                        continue;

                    // this should be done and passed in as a point array( filter parameter)
                    int edgeDelta = 0;

                    if ((x - 2) >= 0)
                        edgeDelta = Math.Max(edgeDelta, result.GetPixel(x - 2, y).R);

                    if ((x + 2) < a.Width)
                        edgeDelta = Math.Max(edgeDelta, result.GetPixel(x + 2, y).R);

                    if ((y + 2) < a.Height)
                        edgeDelta = Math.Max(edgeDelta, result.GetPixel(x, y + 2).R);

                    if ((y - 2) >= 0)
                        edgeDelta = Math.Max(edgeDelta, result.GetPixel(x, y - 2).R);

                    if (edgeDelta > 0)
                        continue;


                    Color color = Color.FromArgb(0, 0, 0);
                    result.SetPixel(x, y, color);
                }
            }

            return result;
        }

        protected static int EdgeDelta(Color center, Color pixel)
        {
            // edges are where there is a high delta brightness
            // or high delta colour change
            int centerBrightness = (center.R + center.G + center.B) / 3;
            int pixelBrightness = (pixel.R + pixel.G + pixel.B) / 3;

            int redDelta = Math.Abs(center.R - pixel.R);
            int greenDelta = Math.Abs(center.G - pixel.G);
            int blueDelta = Math.Abs(center.B - pixel.B);

            //int edgeDelta = (redDelta + greenDelta + blueDelta) / 3;
            int edgeDelta = 0;
            edgeDelta = Math.Max(edgeDelta, redDelta);
            edgeDelta = Math.Max(edgeDelta, greenDelta);
            edgeDelta = Math.Max(edgeDelta, blueDelta);

            if (edgeDelta > 15)
                edgeDelta = 255;
            else
                edgeDelta = 0;

                //+ greenDelta + blueDelta) / 3;
            return edgeDelta;

            //int brightnessDelta = Math.Abs(centerBrightness - pixelBrightness);
            //return brightnessDelta;
        }

        public static double[] ToGrayScaleDoubleArray(Bitmap a)
        {
            double[] result = new double[a.Width * a.Height];
            for (int y = 0; y < a.Height; ++y)
            {
                for (int x = 0; x < a.Width; ++x)
                {
                    Color c = a.GetPixel(x, y);
                    int brightness = (c.R + c.G + c.B) / 3;
                    result[x + (y * a.Width)] = ((double)brightness) / 255.0;
                }
            }

            return result;
        }

        public static Bitmap ToGrayScaleImage(double[] doubleArray, int width, int height)
        {
            Bitmap result = new Bitmap(width, height);
            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    int brightness = (int)(doubleArray[(x + (y * width))] * 255.0);
                    Color c = Color.FromArgb(brightness, brightness, brightness);
                    result.SetPixel(x, y, c);
                }
            }

            return result;
        }
    }
}
