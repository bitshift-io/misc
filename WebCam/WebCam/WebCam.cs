﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;

namespace WebCam
{
    /// <summary>
    /// EventArgs for the webcam control
    /// </summary>
    public class WebcamEventArgs : System.EventArgs
    {
        private System.Drawing.Bitmap m_Image;
        private ulong m_FrameNumber = 0;

        public WebcamEventArgs()
        {
        }

        /// <summary>
        ///  WebCamImage
        ///  This is the image returned by the web camera capture
        /// </summary>
        public System.Drawing.Bitmap WebCamImage
        {
            get
            { return m_Image; }

            set
            { m_Image = value; }
        }

        /// <summary>
        /// FrameNumber
        /// Holds the sequence number of the frame capture
        /// </summary>
        public ulong FrameNumber
        {
            get
            { return m_FrameNumber; }

            set
            { m_FrameNumber = value; }
        }
    }

    public class WebCam
    {
        #region API Declarations

        //This function enables enumerate the web cam devices
        [DllImport("avicap32.dll")]
        protected static extern bool capGetDriverDescriptionA(short wDriverIndex,
            [MarshalAs(UnmanagedType.VBByRefStr)]ref String lpszName,
           int cbName, [MarshalAs(UnmanagedType.VBByRefStr)] ref String lpszVer, int cbVer);

        [DllImport("user32", EntryPoint = "SendMessage")]
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);

        [DllImport("avicap32.dll", EntryPoint = "capCreateCaptureWindowA")]
        public static extern int capCreateCaptureWindowA(string lpszWindowName, int dwStyle, int X, int Y, int nWidth, int nHeight, int hwndParent, int nID);

        [DllImport("user32", EntryPoint = "OpenClipboard")]
        public static extern int OpenClipboard(int hWnd);

        [DllImport("user32", EntryPoint = "EmptyClipboard")]
        public static extern int EmptyClipboard();

        [DllImport("user32", EntryPoint = "CloseClipboard")]
        public static extern int CloseClipboard();

        #endregion

        #region API Constants

        public const int WM_USER = 1024;

        public const int WM_CAP_CONNECT = 1034;
        public const int WM_CAP_DISCONNECT = 1035;
        public const int WM_CAP_GET_FRAME = 1084;
        public const int WM_CAP_COPY = 1054;

        public const int WM_CAP_START = WM_USER;

        public const int WM_CAP_DLG_VIDEOFORMAT = WM_CAP_START + 41;
        public const int WM_CAP_DLG_VIDEOSOURCE = WM_CAP_START + 42;
        public const int WM_CAP_DLG_VIDEODISPLAY = WM_CAP_START + 43;
        public const int WM_CAP_GET_VIDEOFORMAT = WM_CAP_START + 44;
        public const int WM_CAP_SET_VIDEOFORMAT = WM_CAP_START + 45;
        public const int WM_CAP_DLG_VIDEOCOMPRESSION = WM_CAP_START + 46;
        public const int WM_CAP_SET_PREVIEW = WM_CAP_START + 50;

        #endregion

        private int m_TimeToCapture_milliseconds = 100;
        private int m_Width = 320;
        private int m_Height = 240;
        private int mCapHwnd;
        private ulong m_FrameNumber = 0;

        public PictureBox Container;


        // global variables to make the video capture go faster
        private WebcamEventArgs x = new WebcamEventArgs();
        private IDataObject tempObj;
        private System.Drawing.Image tempImg;
        private bool bStopped = true;

        // event delegate
        public delegate void WebCamEventHandler(object source, WebcamEventArgs e);
        // fired when a new image is captured
        public event WebCamEventHandler ImageCaptured;

        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Timer timer1;

        //The devices list
        ArrayList ListOfDevices = new ArrayList();

        public WebCam()
        {
            Load();
        }

        public void Load()
        {
            string Name = String.Empty.PadRight(100);
            string Version = String.Empty.PadRight(100);
            bool EndOfDeviceList = false;
            short index = 0;

            // Load name of all avialable devices into the lstDevices .
            do
            {
                // Get Driver name and version
                EndOfDeviceList = capGetDriverDescriptionA(index, ref Name, 100, ref Version, 100);
                // If there was a device add device name to the list
                if (EndOfDeviceList)
                {
                    int actualLen = 0;
                    while (Name[actualLen] != '\0')
                    {
                        ++actualLen;
                    }
                    string trimmedName = Name.Remove(actualLen); //.Trim();
                    int len = trimmedName.Count();

                    ListOfDevices.Add(trimmedName);
                }
                index += 1;
            }
            while (!(EndOfDeviceList == false));
        }

        public void Connect()
        {
            try
            {
                // for safety, call stop, just in case we are already running
                this.Disconnect();

                // setup a capture window
                mCapHwnd = capCreateCaptureWindowA("WebCap", 0, 0, 0, m_Width, m_Height, 0, 0);

                // connect to the capture device
                Application.DoEvents();
                int device = 0;
                int result = SendMessage(mCapHwnd, WM_CAP_CONNECT, device, 0);
                result = SendMessage(mCapHwnd, WM_CAP_SET_PREVIEW, 0, 0);

                // set the frame number
                //m_FrameNumber = FrameNum;

                // set the timer information
                this.components = new System.ComponentModel.Container();
                this.timer1 = new System.Windows.Forms.Timer(this.components);
                this.timer1.Tick += new System.EventHandler(this.timer1_Tick);

                this.timer1.Interval = m_TimeToCapture_milliseconds;
                bStopped = false;
                this.timer1.Start();
            }

            catch (Exception excep)
            {
                MessageBox.Show("An error ocurred while starting the video capture. Check that your webcamera is connected properly and turned on.\r\n\n" + excep.Message);
                this.Disconnect();
            }
        }

        public void Disconnect()
        {
            try
            {
                // stop the timer
                bStopped = true;
                this.timer1.Stop();

                // disconnect from the video source
                Application.DoEvents();
                SendMessage(mCapHwnd, WM_CAP_DISCONNECT, 0, 0);
            }

            catch (Exception excep)
            { // don't raise an error here.
            }
        }

        private void timer1_Tick(object sender, System.EventArgs e)
        {
            // get the next frame;
			SendMessage(mCapHwnd, WM_CAP_GET_FRAME, 0, 0);

			// copy the frame to the clipboard
			SendMessage(mCapHwnd, WM_CAP_COPY, 0, 0);
            
			// paste the frame into the event args image
			if (ImageCaptured != null)
			{
				// get from the clipboard
				tempObj = Clipboard.GetDataObject();
				tempImg = (System.Drawing.Bitmap) tempObj.GetData(System.Windows.Forms.DataFormats.Bitmap);

                if (tempImg == null)
                {
                    return;
                }

				/*
				* For some reason, the API is not resizing the video
				* feed to the width and height provided when the video
				* feed was started, so we must resize the image here
				*/
				x.WebCamImage = (Bitmap)tempImg.GetThumbnailImage(m_Width, m_Height, null, System.IntPtr.Zero);

                // set the picturebox picture
                //this.Container.Image = x.WebCamImage;

				// raise the event
				this.ImageCaptured(this, x);
			}

            // restart the timer
            Application.DoEvents();
            if (!bStopped)
                this.timer1.Start();
        }
    }
}
