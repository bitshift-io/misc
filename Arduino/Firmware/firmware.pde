#include <SoftwareSerial.h>
#include <Servo.h> 

//
// MOTOR
//
struct MotorInfo
{
  int speedPin;
  int directionPin;
};

const int MOTOR_COUNT = 2;

MotorInfo motor[MOTOR_COUNT] =
{
  {6, 8},
  {5, 7}
};


//
// SERVO
//
const int SERVO_COUNT = 2;
const bool DETACH_SERVOS = false;  // only have servos attached to move to position
                                  // this means they are stuck pulling current to hold its position
int servoPin[SERVO_COUNT] =
{
  2,
  3
};

Servo servo[SERVO_COUNT];



//
// ULTRASOUND
//
const int ULTRASOUND_COUNT = 2;

int ultrasoundPin[ULTRASOUND_COUNT] =
{
  9,
  10
};

SoftwareSerial ultrasound[ULTRASOUND_COUNT] = 
{
  SoftwareSerial(ultrasoundPin[0], ultrasoundPin[0]),
  SoftwareSerial(ultrasoundPin[1], ultrasoundPin[1])
};

void setup() 
{
  // setup serial
  Serial.begin(9600); 
  
  // motor setup
  for (int i = 0; i < MOTOR_COUNT; ++i) 
  {
    pinMode(motor[i].speedPin, OUTPUT); 
    pinMode(motor[i].directionPin, OUTPUT); 
  }

  // setup servos
  for (int i = 0; i < SERVO_COUNT; ++i)
  {
    servo[i].attach(servoPin[i]);
  }
  
  if (DETACH_SERVOS)
  {
    delay(500);
    for (int i = 0; i < SERVO_COUNT; ++i)
    {
      servo[i].detach();
    }
  }
  
  // setup ultrasound
  for (int i = 0; i < ULTRASOUND_COUNT; ++i)
  {
    ultrasound[i].begin(9700);
  }
}

typedef void (*CommandFn)();

void SetMotorSpeed()
{
  if (Serial.available() < 3) 
    return;
    
  unsigned char operation = Serial.read();
  unsigned char index = Serial.read();
  char speed = Serial.read();
  
  const MotorInfo& mi = motor[index];
  
  if (speed == 0)
  {
    digitalWrite(mi.speedPin, LOW); 
  }
  else if (speed < 0)
  {
    int actualSpeed = -speed * 2;
    analogWrite(mi.speedPin, actualSpeed); 
    digitalWrite(mi.directionPin, LOW);
  }
  else
  {
    int actualSpeed = speed * 2;
    analogWrite(mi.speedPin, actualSpeed); 
    digitalWrite(mi.directionPin, HIGH);
  }
}

void ReturnUltrasoundRange(int i)
{
  const int ADDRESS = 0x01;
  const int GET_RANGE_COMMAND = 0x54;
  
  int pin = ultrasoundPin[i];
  pinMode(pin, OUTPUT);                                 // Set pin to output and send break by sending pin low, waiting 2ms and sending it high again for 1ms
  digitalWrite(pin, LOW);
  delay(2);                                               
  digitalWrite(pin, HIGH);                            
  delay(1);
  ultrasound[i].print(ADDRESS, BYTE);                  // Send the address of the SRF01
  ultrasound[i].print(GET_RANGE_COMMAND, BYTE);        // Send commnd byte to SRF01
  pinMode(pin, INPUT);                                 // make input ready for Rx
  
  byte highByte = ultrasound[i].read();
  byte lowByte = ultrasound[i].read();
  
  //int range = ((highByte<<8)+lowByte);                     // Put them together
  //Serial.print(range);

  Serial.print(highByte); 
  Serial.print(lowByte); 
}

void GetUltrasoundRange()
{
  if (Serial.available() < 2) 
    return;
    
  unsigned char operation = Serial.read();
  unsigned char index = Serial.read();
  
  ReturnUltrasoundRange(index);
}

void SetServoAngle()
{
  // TODO: detach servo once it reaches position or after delay, we dont need to hold it there
  // ultra sound is light ;)
  if (Serial.available() < 3) 
    return;
      
  unsigned char operation = Serial.read();
  unsigned char index = Serial.read();
  unsigned char angle = Serial.read();
  
  if (DETACH_SERVOS)
  {
    servo[index].attach(servoPin[index]);
  }
  
  servo[index].write(angle);
  
  if (DETACH_SERVOS)
  {
    delay(500);
    servo[index].detach();
  }
}

CommandFn commandList[] =
{
  &SetMotorSpeed,
  &GetUltrasoundRange,
  &SetServoAngle
};


void loop() 
{  
  if (Serial.available())
  {
    unsigned char operation = Serial.peek();
    commandList[operation]();
  }
}
