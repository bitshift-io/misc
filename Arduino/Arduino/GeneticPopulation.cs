﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arduino
{
    [Serializable]
    class GeneticPopulation
    {
        private const int m_bestCount = 4;
        private int m_childCount = m_bestCount; // force each parent to make a kid
        private int m_mutationCount = m_bestCount; // force each parent to make mutation
        private int m_randomCount = m_bestCount;
        private int m_iterations = 0;
        private GeneticNetworkBreeder m_breeder = new GeneticNetworkBreeder();
        public List<GeneticNetwork> m_networks = new List<GeneticNetwork>();
        //private int m_currentNetwork = 0;

        public GeneticPopulation()
        {
            while (m_networks.Count < (m_bestCount + m_childCount + m_mutationCount + m_randomCount))
            {
                m_networks.Add(m_breeder.GenerateRandom());
            }
        }

        public List<GeneticNetwork> Networks()
        {
            return m_networks;
        }

        /*
        public int GetNetworkCount()
        {
            return m_networks.Count;
        }

        public int GetCurrentNetworkIndex()
        {
            return m_currentNetwork;
        }

        public GeneticNetwork GetNextNetwork()
        {
            if (m_currentNetwork >= m_networks.Count)
            {
                return null;
            }

            GeneticNetwork network = m_networks[m_currentNetwork];
            ++m_currentNetwork;

            return network;
        }*/

        public void PerformIteration()
        {
            //m_currentNetwork = 0;
            ++m_iterations;
            
            // the older we get the less variation we want to see
            double variation = 0.2; // (1.0 / m_iterations);

            // sort by fitness
            // discard lowest
            // keep heighest
            // time to make babies!
            // throw in some new randoms to keep things sexy!


            m_networks.Sort(GeneticNetwork.SortByFitness);
            /*
            // if any network is fitter than thier parent, kill thier parent off
            // to stop flocking, gives other fairly fit networks to take the place
            // of very similar networks
            // sort first as indexes will change as we iterate the list
            // and some might be missed!
            for (int i = 0; i < m_networks.Count; ++i)
            {
                GeneticNetwork child = m_networks[i];
                if (child.m_parentList == null)
                    continue;

                List<GeneticNetwork> parentList = child.m_parentList;

                int parentRemoveCount = 0;
                foreach (GeneticNetwork parent in parentList)
                {
                    if (child.Fitness > parent.Fitness)
                    {
                        m_networks.Remove(parent);
                        ++parentRemoveCount;
                    }
                }

                // ops, we bread a retard! exterminate!
                if (parentRemoveCount <= 0 && parentList.Count > 0)
                {
                    m_networks.Remove(child);
                }

                child.m_parentList = null; // no need for further checking against parents
            }*/

            m_networks.RemoveRange(m_bestCount, m_networks.Count - m_bestCount);



            double variationMultiplier = 1.0;
            for (int i = 0; i < m_childCount; ++i)
            {
                variationMultiplier = (double)(i + 1) / (double)m_childCount;

                // 50% of these will just be mutations of thier parent, while
                // 50% are actually breeded
                int father = i;
                int mother = m_childCount - (i + 1);
                //if (i + 1 == m_childCount)
                //    mother = 0;
                /*
                while (father == mother)
                {
                    father = m_breeder.Random.Next(m_bestCount);
                    mother = m_breeder.Random.Next(m_bestCount);
                }*/

                // children get less variation than mutations
                GeneticNetwork child = m_breeder.GenerateChild(0/*variation * variationMultiplier * 0.1*/, m_networks[father], m_networks[mother]);
                m_networks.Add(child);
            }

            for (int i = 0; i < m_mutationCount; ++i)
            {
                variationMultiplier = (double)(i + 1) / (double)m_childCount;

                // 50% of these will just be mutations of thier parent, while
                // 50% are actually breeded
                GeneticNetwork child = m_breeder.GenerateMutation(m_networks[i/*m_breeder.Random.Next(m_bestCount)*/], variation * variationMultiplier);
                m_networks.Add(child);
            }

            for (int i = 0; i < m_randomCount; ++i)
            {
                GeneticNetwork child = m_breeder.GenerateRandom();
                m_networks.Add(child);
            }
        }
    }
}
