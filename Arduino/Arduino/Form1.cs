﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ABT;
using System.Threading;

namespace Arduino
{
    public partial class Form1 : Form
    {
        ArduinoBT m_abt = new ArduinoBT();
        Simulation m_simulation;

        public Form1()
        {
            InitializeComponent();
            
        }

        public ArduinoBT GetAbt()
        {
            return m_abt;
        }

        private void connect_Click(object sender, EventArgs e)
        {
            try
            {
                if (connect.Text == "Connect")
                {
                    int port = Convert.ToInt32(comPort.Text);
                    if (m_abt.Connect(port))
                    {
                        connect.Text = "Disconnect";
                    }

                    ultrasoundRangeTimer.Start();
                }
                else
                {
                    ultrasoundRangeTimer.Stop();

                    m_abt.Disconnect();
                    connect.Text = "Connect";
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void stop_Click(object sender, EventArgs e)
        {
            right_motor.Value = 0;
            left_motor.Value = 0;
        }

        private void ultrasoundRangeTimer_Tick(object sender, EventArgs e)
        {
            ultrasoundRange1.Text = m_abt.GetUltrasoundRange(0).ToString();
            ultrasoundRange2.Text = m_abt.GetUltrasoundRange(1).ToString();
        }

        private void servo_tilt_slider_ValueChanged(object sender, EventArgs e)
        {
            m_abt.SetServoAngle(0, servo_tilt_slider.Value);
        }

        private void servo_pan_slider_ValueChanged(object sender, EventArgs e)
        {
            m_abt.SetServoAngle(1, servo_pan_slider.Value);
        }

        private void servo_center_Click(object sender, EventArgs e)
        {
            servo_tilt_slider.Value = 90;
            servo_pan_slider.Value = 90;
        }

        private void left_motor_Scroll(object sender, EventArgs e)
        {
            m_abt.SetMotorSpeed(0, (float)left_motor.Value / (float)128.0);
        }

        private void right_motor_Scroll(object sender, EventArgs e)
        {
            m_abt.SetMotorSpeed(1, (float)right_motor.Value / (float)128.0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            m_simulation = new Simulation(this);
        }

        private void Draw_Tick(object sender, EventArgs e)
        {
            m_simulation.Draw();
        }
    }
}
