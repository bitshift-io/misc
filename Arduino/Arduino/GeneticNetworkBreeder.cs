﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arduino
{
    [Serializable]
    class GeneticNetworkBreeder
    {
        public GeneticNetwork GenerateRandom()
        {
            // 2 inputs 2 outputs, no middle layers for the time being
            GeneticNetwork network = new GeneticNetwork(Random);

            /*
            for (int l = 0; l < network.LayersCount; ++l)
            {
                ActivationLayer networkLayer = network[l];

                for (int n = 0; n < networkLayer.NeuronsCount; ++n)
                {
                    ActivationNeuron networkNeuron = networkLayer[n];
                    networkNeuron.Threshold = (Random.NextDouble() - 0.5) * 2.0;
                }
            }*/

            MakeSymetric(network);
            return network;
        }

        public GeneticNetwork GenerateChild(double variation, params GeneticNetwork[] parent)
        {
            GeneticNetwork network = GenerateRandom();
            
            for (int l = 0; l < network.mLayer.Length; ++l)
            {
                NeuronLayer networkLayer = network.mLayer[l];

                for (int n = 0; n < networkLayer.neuron.Length; ++n)
                {
                    Neuron networkNeuron = networkLayer.neuron[n];

                    // take the whole neruon from one parent
                    int randomParentIndex = Random.Next(parent.Length);
                    GeneticNetwork randomParent = parent[randomParentIndex];
                    Neuron parentNeuron = randomParent.mLayer[l].neuron[n];

                    for (int w = 0; w < networkNeuron.weight.Length; ++w)
                    {
                        networkNeuron.weight[w] = parentNeuron.weight[w];// +(Random.NextDouble() - 0.5) * 2.0 * variation * 2.0;
                    }
                }
            }

            //MakeSymetric(network);
            return GenerateMutation(network, variation);
        }

        public GeneticNetwork GenerateMutation(GeneticNetwork origonal, double variation)
        {
            GeneticNetwork network = GenerateRandom();

            // copy parent
            for (int l = 0; l < network.mLayer.Length; ++l)
            {
                NeuronLayer networkLayer = network.mLayer[l];

                for (int n = 0; n < networkLayer.neuron.Length; ++n)
                {
                    Neuron networkNeuron = networkLayer.neuron[n];
                    Neuron parentNeuron = origonal.mLayer[l].neuron[n];

                    // vary the weights
                    for (int w = 0; w < networkNeuron.weight.Length; ++w)
                    {
                        networkNeuron.weight[w] = parentNeuron.weight[w];
                    }

                }
            }

            // only change one weight 
            int ml = Random.Next(0, network.mLayer.Length);
            int mn = Random.Next(0, network.mLayer[ml].neuron.Length);
            int mw = Random.Next(0, network.mLayer[ml].neuron[mn].weight.Length);
            network.mLayer[ml].neuron[mn].weight[mw] = (float)((Random.NextDouble() - 0.5) * 2.0 * variation * 2.0);

            /*
            for (int l = 0; l < network.mLayer.Length; ++l)
            {
                NeuronLayer networkLayer = network.mLayer[l];

                for (int n = 0; n < networkLayer.neuron.Length; ++n)
                {
                    Neuron networkNeuron = networkLayer.neuron[n];
                    Neuron parentNeuron = origonal.mLayer[l].neuron[n];

                    // vary the weights
                    for (int w = 0; w < networkNeuron.weight.Length; ++w)
                    {
                        networkNeuron.weight[w] = parentNeuron.weight[w] + (Random.NextDouble() - 0.5) * 2.0 * variation * 2.0;
                    }

                }
            }*/
            MakeSymetric(network);
            return network;
        }

        void MakeSymetric(GeneticNetwork network)
        {
            if (!m_symetric)
                return;

            for (int l = 0; l < network.mLayer.Length; ++l)
            {
                NeuronLayer networkLayer = network.mLayer[l];

                int halfNeruonCount = networkLayer.neuron.Length / 2;
                int n2 = networkLayer.neuron.Length - 1;
                for (int n = 0; n < halfNeruonCount; ++n)
                {
                    Neuron networkNeuron = networkLayer.neuron[n];
                    Neuron networkNeuron2 = networkLayer.neuron[n2];

                    // vary the weights
                    int w2 = networkNeuron.weight.Length - 1;
                    for (int w = 0; w < networkNeuron.weight.Length; ++w)
                    {
                        networkNeuron.weight[w] = networkNeuron2.weight[w2];
                        --w2;
                    }

                    --n2;
                }
            }
        }

        public bool m_symetric = false;
        public Random Random = new Random();
    }
}
