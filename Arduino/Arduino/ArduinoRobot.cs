﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ABT;

namespace Arduino
{
    class ArduinoRobot : Robot
    {
        ArduinoBT m_abt;

        public ArduinoRobot(Simulation sim, ArduinoBT abt)
            : base(sim)
        {
            m_abt = abt;
        }

        public override void SetServoAngle(int index, int angle)
        {
            m_abt.SetServoAngle(index, angle);
        }

        public override float GetUltrasoundRange(int index)
        {
            return (float)m_abt.GetUltrasoundRange(index);
        }

        public override void SetMotorSpeed(int index, float speed)
        {
            m_abt.SetMotorSpeed(index, speed);
        }

        public override bool IsConnected()
        {
            return m_abt.IsConnected();
        }
    }
}
