﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace Arduino
{
    // virtual robot environment
    class VirtualEnvironment
    {
        Simulation m_simulation;
        PictureBox m_terrain;
        private Bitmap m_originalMap;
        private int LastX;
        private int LastY;

        private byte[] m_bitmapCopy;
        private int m_width;
        private int m_height;

        Form1 m_form;
        Bitmap m_map; // read only version of the map

        Semaphore m_drawSemaphore = new Semaphore(1, 1);

        
        public VirtualEnvironment(Simulation sim, Form1 form)
        {
            m_simulation = sim;
            m_form = form;
            m_terrain = m_form.pbTerrain;
            m_terrain.MouseUp += pbTerrain_MouseUp;
            m_terrain.MouseMove += pbTerrain_MouseMove;
            m_terrain.MouseDown += pbTerrain_MouseDown;

            // fill white
            try
            {
                m_terrain.Image = new Bitmap(500, 500);
                Graphics g = Graphics.FromImage(m_terrain.Image);
                g.FillRectangle(new SolidBrush(Color.White), 0, 0, m_terrain.Width, m_terrain.Height);
                g.Dispose();
                m_terrain.Refresh();
            }
            catch (Exception ex)
            {
            }

            m_originalMap = sim.LoadObject<Bitmap>("map.ser");
            if (m_originalMap == null)
            {
                m_originalMap = new Bitmap(m_terrain.Image);
            }

            //m_terrain.Refresh();
            //m_map = new Bitmap(Terrain().Image);

            m_width = m_terrain.Image.Width;
            m_height = m_terrain.Image.Height;
            CopyBitmap();
        }

        public PictureBox Terrain()
        {
            return m_terrain;
        }

        public int GetDistance(Point p1, Point p2)
        {
            return (Convert.ToInt32(Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2))));
        }

        public int GetMeasure(Point position, double direction)
        {
            try
            {
                //m_drawSemaphore.WaitOne();

                //Bitmap map = m_map; // Terrain().Image as Bitmap;
                float increment = 5;
                double rad = (direction * Math.PI) / 180;
                float xIncrement = (float)(increment * Math.Cos(rad));
                float yIncrement = (float)(increment * Math.Sin(rad));
                
                PointF p = new PointF(position.X, position.Y);
                Point pi = new Point();
                
                // to a course forward search
                while (true)
                {
                    p.X = p.X + xIncrement;
                    p.Y = p.Y + yIncrement;

                    pi.X = Convert.ToInt32(p.X);
                    pi.Y = Convert.ToInt32(p.Y);

                    if (pi.X < 0 || pi.Y < 0 || pi.X >= m_width || pi.Y >= m_height)
                        break;
                    
                    Color pixel = GetPixel(pi.X, pi.Y);
                    if (pixel.R == 0 && pixel.G == 0 && pixel.B == 0)
                        break;
                }

                pi.X = Convert.ToInt32(p.X);
                pi.Y = Convert.ToInt32(p.Y);

                // now search backwards with a fine search
                increment = -1;
                xIncrement = (float)(increment * Math.Cos(rad));
                yIncrement = (float)(increment * Math.Sin(rad));
                
                while (true)
                {
                    p.X = p.X + xIncrement;
                    p.Y = p.Y + yIncrement;

                    pi.X = Convert.ToInt32(p.X);
                    pi.Y = Convert.ToInt32(p.Y);

                    if (pi.X < 0 || pi.Y < 0 || pi.X >= m_width || pi.Y >= m_height)
                        break;

                    Color pixel = GetPixel(pi.X, pi.Y);
                    if (pixel.R == 255 && pixel.G == 255 && pixel.B == 255)
                        break;
                }

                //m_drawSemaphore.Release();

                int distance = GetDistance(position, pi);
                return distance;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
                return 0;
            }
            
        }



        // terrain drawing
        private void pbTerrain_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //pbTerrain.Image = new Bitmap(OriginalMap);
            LastX = e.X;
            LastY = e.Y;
        }

        // terrain drawing
        private void pbTerrain_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            

            Color c = Color.Yellow;

            int size = 10;
            if (e.Button == MouseButtons.Left)
            {
                c = Color.White;
                size = 15;
            }
            else if (e.Button == MouseButtons.Right)
            {
                c = Color.Black;
            }

            if (c != Color.Yellow)
            {
                m_drawSemaphore.WaitOne();

                Graphics g = Graphics.FromImage(m_terrain.Image);
                g.DrawImage(m_originalMap, 0, 0, m_terrain.Width, m_terrain.Height);

                int halfSize = size / 2;
                g.FillEllipse(new SolidBrush(c), e.X - halfSize, e.Y - halfSize, size, size);

                LastX = e.X;
                LastY = e.Y;

                g.DrawImage(m_terrain.Image, 0, 0);
                m_originalMap = new Bitmap(m_terrain.Image as Bitmap);
                m_terrain.Refresh();
                g.Dispose();

                // make copy to read from
                //m_map = new Bitmap(m_terrain.Image);
                CopyBitmap();

                m_drawSemaphore.Release();
            }
        }

        private void pbTerrain_MouseUp(object sender, MouseEventArgs e)
        {
            m_simulation.SaveObject(m_originalMap, "map.ser");
        }

        public Graphics DrawBegin()
        {
            m_drawSemaphore.WaitOne();

            Graphics g = Graphics.FromImage(m_terrain.Image);
            g.DrawImage(m_originalMap, 0, 0, m_terrain.Width, m_terrain.Height);
            return g;
        }

        public void DrawEnd(Graphics g)
        {
            g.Dispose();
            m_terrain.Refresh();

            m_drawSemaphore.Release();
        }

        public void CopyBitmap()
        {
            Bitmap bmp = (Bitmap)m_originalMap;

            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            System.Drawing.Imaging.BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                bmp.PixelFormat);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes  = Math.Abs(bmpData.Stride) * bmp.Height;
            m_bitmapCopy = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, m_bitmapCopy, 0, bytes);

            // Unlock the bits.
            bmp.UnlockBits(bmpData);
        }

        public Color GetPixel(int x, int y)
        {
            int pixelStart = (m_width * y + x) * 4;
            byte r = m_bitmapCopy[pixelStart];
            byte g = m_bitmapCopy[pixelStart + 1];
            byte b = m_bitmapCopy[pixelStart + 2];

            return Color.FromArgb(r, g, b);
        }
    }
}
