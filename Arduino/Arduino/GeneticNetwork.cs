﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arduino
{
    [Serializable]
    public class GeneticNetwork : NNet
    {
        public GeneticNetwork()
        {
            m_id = m_idCount;
            ++m_idCount;

            mRound = 2;
            Create();
        }

        public GeneticNetwork(Random random)
        {
            m_id = m_idCount;
            ++m_idCount;

            mRandom = random;
            Create();
            /*
            mLayer[0].neuron[0].feedThrough = true;
            mLayer[0].neuron[1].feedThrough = true;
            mLayer[0].neuron[2].enabled = false; // disable momentarily*/
        }

        public void Create()
        {
            /*
            NeuronLayer[] layer = new NeuronLayer[2];
            layer[0] = new NeuronLayer(3);
            layer[1] = new NeuronLayer(2);
            create(2, layer);
            */

            NeuronLayer[] layer = new NeuronLayer[3];
            layer[0] = new NeuronLayer(6);
            layer[1] = new NeuronLayer(6);
            layer[2] = new NeuronLayer(2);
            create(2, layer);
        }

        public static int SortByFitness(GeneticNetwork x, GeneticNetwork y)
        {
            if (x.Fitness == y.Fitness)
                return 0;

            if (x.Fitness < y.Fitness)
                return 1;

            return -1;
        }

        public double Fitness   = 0.0;
        public int m_id;

        public static int m_idCount = 0;
    }
}
