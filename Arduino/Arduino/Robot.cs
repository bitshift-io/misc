﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using ABT;
using System.Windows.Forms;

namespace Arduino
{
    class Robot
    {
        enum Instruction
        {
            Tilt,
            Pan,
            Delay,
            LeftMotorSpeed,
            RightMotorSpeed,
        };

        protected class SensorEntry
        {
            public float[] distance = new float[2];
        };


        /*
        struct MemoryEntry
        {
            float[] distance = new float[2];
        };

        List<MemoryEntry> m_memory = new List<MemoryEntry>();
        GeneticNetwork m_servoNetwork = new GeneticNetwork();
        GeneticNetwork m_motorNetwork = new GeneticNetwork();*/
        protected GeneticNetwork m_network;

        const int SENSOR_DATA_LIST_SIZE = 1;
        protected Simulation m_simulation;
        protected List<SensorEntry> m_sensorData = new List<SensorEntry>();
        protected Thread m_thread;


        public Robot(Simulation simulation)
        {
            m_simulation = simulation;
            //m_network.Create();
            /*
            NeuronLayer[] layer = new NeuronLayer[2];
            layer[0] = new NeuronLayer(3);
            layer[1] = new NeuronLayer(2);
            m_network.create(2, layer);
            */
            m_sensorData.Capacity = 1;
        }

        public virtual void SetServoAngle(int index, int angle)
        {
        }

        public virtual float GetUltrasoundRange(int index)
        {
            return -1;
        }

        public virtual void SetMotorSpeed(int index, float speed)
        {
        }

        public virtual bool IsConnected()
        {
            return true;
        }

        public void StartThread()
        {
            m_thread = new Thread(new ThreadStart(RunThread));
            m_thread.IsBackground = true;
            m_thread.Priority = ThreadPriority.AboveNormal;
            m_thread.Start();
        }

        public void StopThread()
        {
            m_thread.Interrupt();
        }

        void RunThread()
        {
            try
            {
                while (Thread.CurrentThread.IsAlive)
                {
                    Step();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
        }

        void UpdateSensors()
        {
            // read in sensor values

            SensorEntry entry = new SensorEntry();
            for (int i = 0; i < 2; ++i)
            {
                entry.distance[i] = GetUltrasoundRange(i);
            }

            m_sensorData.Add(entry);
            if (m_sensorData.Count > SENSOR_DATA_LIST_SIZE)
            {
                m_sensorData.RemoveAt(0);
            }
        }

        virtual public void Reset()
        {
        }

        virtual public void UpdateNetworks()
        {
            if (m_network == null)
            {
                m_network = m_simulation.NextNetwork();
                Reset();
            }
        }

        virtual public void Step()
        {
            UpdateNetworks();
            UpdateSensors();

            float[] output = m_network.Compute(m_sensorData[0].distance);

            for (int i = 0; i < 2; ++i)
            {
                SetMotorSpeed(i, output[i]);
            }
        }


    }
}
