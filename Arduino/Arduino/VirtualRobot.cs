﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading;
using ABT;

namespace Arduino
{
    class VirtualRobot : Robot
    {
        ArduinoBT m_abt;

        const float ARDUINO_ULTRASOUND_RANGE = 700.0f;
        const float VIRTUAL_ULTRASOUND_RANGE = 300.0f;

        const float ENGINE_SPEED_MULTIPLIER = 1f;
        const float ROTATION_SPEED_MULTIPLIER = 20f;

        long m_age;
        float m_direction;
        Point m_position;
        Color m_color;
        float[] m_motorSpeed = new float[2];

        public VirtualRobot(Simulation sim)
            : base(sim)
        {
            m_color = Color.Red;
            Reset();
        }

        public override void Reset()
        {
            // start in the center
            m_position = StartPosition();

            m_direction = m_simulation.Random().Next(0, 360);

            m_age = 0;
        }

        Point StartPosition()
        {
            Point position = new Point(m_simulation.Form().pbTerrain.Location.X + m_simulation.Form().pbTerrain.Width / 2,
                 m_simulation.Form().pbTerrain.Location.Y + m_simulation.Form().pbTerrain.Height / 2);
            return position;
        }


        virtual public void Draw(Graphics g)
        {
            /*
            int size = 6;
            int halfSize = size / 2;
            g.FillEllipse(new SolidBrush(m_color), m_position.X - halfSize, m_position.Y - halfSize, size, size);*/

            int halfWidth = 6;
            int halfHeight = 3;
            g.TranslateTransform(m_position.X, m_position.Y);
            g.RotateTransform(m_direction);
            
            g.FillRectangle(new SolidBrush(m_color), -halfWidth, -halfHeight, halfWidth * 2, halfHeight * 2);
            g.ResetTransform();
        }

        public override void SetMotorSpeed(int index, float speed)
        {
            m_motorSpeed[index] = speed;

            if (m_abt != null)
                m_abt.SetMotorSpeed(index, speed);
        }

        public override void SetServoAngle(int index, int angle)
        {
            if (m_abt != null)
                m_abt.SetServoAngle(index, angle);
        }

        // normalised to range 0 to 1
        public override float GetUltrasoundRange(int index)
        {
            if (m_abt != null)
            {
                return m_abt.GetUltrasoundRange(index) / ARDUINO_ULTRASOUND_RANGE;
            }

            if (index == 0)
                return m_simulation.Environment().GetMeasure(m_position, m_direction + 45) / VIRTUAL_ULTRASOUND_RANGE;

            return m_simulation.Environment().GetMeasure(m_position, m_direction - 45) / VIRTUAL_ULTRASOUND_RANGE;
        }

        public override bool IsConnected()
        {
            if (m_abt != null)
                return m_abt.IsConnected();

            return true;
        }

        override public void Step()
        {
            base.Step();

            // update position
            float engineDelta = m_motorSpeed[0] - m_motorSpeed[1];
            float engineNetMovement = m_motorSpeed[0] + m_motorSpeed[1];

            m_direction += engineDelta * ROTATION_SPEED_MULTIPLIER;
            float distance = engineNetMovement * ENGINE_SPEED_MULTIPLIER;
            m_position = MoveTo(m_position, m_direction, distance);
            
            // update fitness
            float closestRange = Math.Min(GetUltrasoundRange(0), GetUltrasoundRange(1));
            if (closestRange <= 0.02)
            {
                m_network.Fitness -= 10.0;
                m_network = null; // will get a new brain
            }
            else
            {
                float fitnessIncrement = m_simulation.Environment().GetDistance(StartPosition(), m_position);
                m_network.Fitness += fitnessIncrement;
            }

            // time for a new brain, died of old age
            if (m_age > 20000)
            {
                m_network = null;
            }
            
            Thread.Sleep(5);
            m_age += 5;
        }

        public virtual Point MoveTo(Point fromPosition, float direction, float distance)
        {
            float rad = (float)(direction * Math.PI) / 180f;
            int xMovement = Convert.ToInt32(distance * Math.Cos(rad));
            int yMovement = Convert.ToInt32(distance * Math.Sin(rad));

            return new Point(fromPosition.X + xMovement, fromPosition.Y + yMovement);
        }
    }
}
