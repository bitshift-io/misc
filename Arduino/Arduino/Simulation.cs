﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Threading;

namespace Arduino
{
    class Simulation
    {
        VirtualEnvironment m_environment;
        ArduinoRobot m_arduinoRobot;
        List<VirtualRobot> m_virtualRobotList = new List<VirtualRobot>();
        public Random m_random = new Random();
        List<GeneticPopulation> m_populationList = new List<GeneticPopulation>();

        Semaphore m_networkSemaphore = new Semaphore(1, 1);

        int m_populationIndex;
        int m_networkIndex;

        const int m_populationCount = 5;
        const int m_virtualRobotCount = 110;

        Form1 m_form;

        public Simulation(Form1 form)
        {
            m_form = form;
            //m_robot

            m_environment = new VirtualEnvironment(this, form);

            m_arduinoRobot = new ArduinoRobot(this, form.GetAbt());
            //m_arduinoRobot.StartThread();

            


            m_populationList = LoadObject<List<GeneticPopulation>>("population_list.ser");
            if (m_populationList == null)
            {
                m_populationList = new List<GeneticPopulation>();
                for (int i = 0; i < m_populationCount; ++i)
                {
                    m_populationList.Add(new GeneticPopulation());
                }
            }

            for (int i = 0; i < m_virtualRobotCount; ++i )
            {
                VirtualRobot vr = new VirtualRobot(this);
                vr.StartThread();
                m_virtualRobotList.Add(vr);
            }
        }

        public GeneticNetwork NextNetwork()
        {
            m_networkSemaphore.WaitOne();

            GeneticPopulation population = m_populationList[m_populationIndex];
            GeneticNetwork result = population.Networks()[m_networkIndex];

            ++m_networkIndex;
            int networkCount = population.Networks().Count;
            if (m_networkIndex >= networkCount)
            {
                // breed population
                population.PerformIteration();
                SaveObject(m_populationList, "population_list.ser");

                ++m_populationIndex;
                m_networkIndex = 0;
            }

            if (m_populationIndex >= m_populationList.Count)
            {
                m_networkIndex = 0;
                m_populationIndex = 0;

                // perform breeding
            }

            m_networkSemaphore.Release();
            return result;
        }

        public Random Random()
        {
            return m_random;
        }

        public Form1 Form()
        {
            return m_form;
        }

        public VirtualEnvironment Environment()
        {
            return m_environment;
        }

        public void Draw()
        {
            Graphics g = Environment().DrawBegin();

            foreach (VirtualRobot r in m_virtualRobotList)
            {
                r.Draw(g);
            }

            Environment().DrawEnd(g);
        }

        public void SaveObject<T>(T List, string FileName)
        {
            try
            {

                //create a backup
                string backupName = Path.ChangeExtension(FileName, ".old");
                if (File.Exists(FileName))
                {
                    if (File.Exists(backupName))
                        File.Delete(backupName);
                    File.Move(FileName, backupName);
                }

                using (FileStream fs = new FileStream(FileName, FileMode.Create))
                {
                    //XmlSerializer ser = new XmlSerializer(typeof(T));
                    BinaryFormatter ser = new BinaryFormatter();
                    ser.Serialize(fs, List);
                    fs.Flush();
                    fs.Close();
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public T LoadObject<T>(string FileName)
        {
            if (!File.Exists(FileName))
            {
                return default(T);
            }

            T result = default(T);

            try
            {
                using (FileStream fs = new FileStream(FileName, FileMode.Open))
                {
                    //XmlSerializer ser = new XmlSerializer(typeof(T));
                    BinaryFormatter ser = new BinaryFormatter();
                    result = (T)ser.Deserialize(fs);
                }
            }
            catch
            {
            }
            return result;
        }
    }
}
