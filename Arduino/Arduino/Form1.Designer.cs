﻿namespace Arduino
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.connect = new System.Windows.Forms.Button();
            this.stop = new System.Windows.Forms.Button();
            this.ultrasoundRange1 = new System.Windows.Forms.TextBox();
            this.ultrasoundRangeTimer = new System.Windows.Forms.Timer(this.components);
            this.ultrasoundRange2 = new System.Windows.Forms.TextBox();
            this.servo_tilt_slider = new System.Windows.Forms.TrackBar();
            this.servo_pan_slider = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.servo_center = new System.Windows.Forms.Button();
            this.left_motor = new System.Windows.Forms.TrackBar();
            this.right_motor = new System.Windows.Forms.TrackBar();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbTerrain = new System.Windows.Forms.PictureBox();
            this.Simulation = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.Draw = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.servo_tilt_slider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo_pan_slider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.left_motor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.right_motor)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbTerrain)).BeginInit();
            this.Simulation.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // comPort
            // 
            this.comPort.Location = new System.Drawing.Point(122, 15);
            this.comPort.Name = "comPort";
            this.comPort.Size = new System.Drawing.Size(100, 20);
            this.comPort.TabIndex = 1;
            this.comPort.Text = "13";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "COM";
            // 
            // connect
            // 
            this.connect.Location = new System.Drawing.Point(228, 13);
            this.connect.Name = "connect";
            this.connect.Size = new System.Drawing.Size(75, 23);
            this.connect.TabIndex = 3;
            this.connect.Text = "Connect";
            this.connect.UseVisualStyleBackColor = true;
            this.connect.Click += new System.EventHandler(this.connect_Click);
            // 
            // stop
            // 
            this.stop.Location = new System.Drawing.Point(122, 148);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(75, 23);
            this.stop.TabIndex = 7;
            this.stop.Text = "Stop";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // ultrasoundRange1
            // 
            this.ultrasoundRange1.Location = new System.Drawing.Point(122, 365);
            this.ultrasoundRange1.Name = "ultrasoundRange1";
            this.ultrasoundRange1.Size = new System.Drawing.Size(100, 20);
            this.ultrasoundRange1.TabIndex = 8;
            this.ultrasoundRange1.Text = "0";
            // 
            // ultrasoundRangeTimer
            // 
            this.ultrasoundRangeTimer.Tick += new System.EventHandler(this.ultrasoundRangeTimer_Tick);
            // 
            // ultrasoundRange2
            // 
            this.ultrasoundRange2.Location = new System.Drawing.Point(122, 405);
            this.ultrasoundRange2.Name = "ultrasoundRange2";
            this.ultrasoundRange2.Size = new System.Drawing.Size(100, 20);
            this.ultrasoundRange2.TabIndex = 9;
            this.ultrasoundRange2.Text = "0";
            // 
            // servo_tilt_slider
            // 
            this.servo_tilt_slider.Location = new System.Drawing.Point(122, 204);
            this.servo_tilt_slider.Maximum = 180;
            this.servo_tilt_slider.Name = "servo_tilt_slider";
            this.servo_tilt_slider.Size = new System.Drawing.Size(334, 45);
            this.servo_tilt_slider.TabIndex = 14;
            this.servo_tilt_slider.Value = 90;
            this.servo_tilt_slider.ValueChanged += new System.EventHandler(this.servo_tilt_slider_ValueChanged);
            // 
            // servo_pan_slider
            // 
            this.servo_pan_slider.Location = new System.Drawing.Point(122, 265);
            this.servo_pan_slider.Maximum = 180;
            this.servo_pan_slider.Name = "servo_pan_slider";
            this.servo_pan_slider.Size = new System.Drawing.Size(332, 45);
            this.servo_pan_slider.TabIndex = 15;
            this.servo_pan_slider.Value = 90;
            this.servo_pan_slider.ValueChanged += new System.EventHandler(this.servo_pan_slider_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 204);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Tilt";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 265);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Pan";
            // 
            // servo_center
            // 
            this.servo_center.Location = new System.Drawing.Point(122, 316);
            this.servo_center.Name = "servo_center";
            this.servo_center.Size = new System.Drawing.Size(75, 23);
            this.servo_center.TabIndex = 18;
            this.servo_center.Text = "Center";
            this.servo_center.UseVisualStyleBackColor = true;
            this.servo_center.Click += new System.EventHandler(this.servo_center_Click);
            // 
            // left_motor
            // 
            this.left_motor.Location = new System.Drawing.Point(122, 46);
            this.left_motor.Maximum = 128;
            this.left_motor.Minimum = -128;
            this.left_motor.Name = "left_motor";
            this.left_motor.Size = new System.Drawing.Size(334, 45);
            this.left_motor.TabIndex = 19;
            this.left_motor.Scroll += new System.EventHandler(this.left_motor_Scroll);
            // 
            // right_motor
            // 
            this.right_motor.Location = new System.Drawing.Point(122, 97);
            this.right_motor.Maximum = 128;
            this.right_motor.Minimum = -128;
            this.right_motor.Name = "right_motor";
            this.right_motor.Size = new System.Drawing.Size(334, 45);
            this.right_motor.TabIndex = 20;
            this.right_motor.Scroll += new System.EventHandler(this.right_motor_Scroll);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Left Motor";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Right Motor";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 365);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Left Ultrasound";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 405);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Right Ultrasound";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.servo_center);
            this.groupBox1.Controls.Add(this.right_motor);
            this.groupBox1.Controls.Add(this.stop);
            this.groupBox1.Controls.Add(this.ultrasoundRange2);
            this.groupBox1.Controls.Add(this.servo_pan_slider);
            this.groupBox1.Controls.Add(this.ultrasoundRange1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.servo_tilt_slider);
            this.groupBox1.Controls.Add(this.left_motor);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 104);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(472, 437);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Robot IO";
            // 
            // pbTerrain
            // 
            this.pbTerrain.BackColor = System.Drawing.Color.White;
            this.pbTerrain.ErrorImage = null;
            this.pbTerrain.InitialImage = null;
            this.pbTerrain.Location = new System.Drawing.Point(6, 17);
            this.pbTerrain.Name = "pbTerrain";
            this.pbTerrain.Size = new System.Drawing.Size(500, 500);
            this.pbTerrain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbTerrain.TabIndex = 26;
            this.pbTerrain.TabStop = false;
            // 
            // Simulation
            // 
            this.Simulation.Controls.Add(this.pbTerrain);
            this.Simulation.Location = new System.Drawing.Point(490, 12);
            this.Simulation.Name = "Simulation";
            this.Simulation.Size = new System.Drawing.Size(967, 529);
            this.Simulation.TabIndex = 27;
            this.Simulation.TabStop = false;
            this.Simulation.Text = "Simulation";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.connect);
            this.groupBox2.Controls.Add(this.comPort);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(472, 86);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Robot Control";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 47);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Control";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Manual",
            "Simulation"});
            this.comboBox1.Location = new System.Drawing.Point(122, 47);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 5;
            // 
            // Draw
            // 
            this.Draw.Enabled = true;
            this.Draw.Tick += new System.EventHandler(this.Draw_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1463, 549);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Simulation);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.servo_tilt_slider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servo_pan_slider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.left_motor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.right_motor)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbTerrain)).EndInit();
            this.Simulation.ResumeLayout(false);
            this.Simulation.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox comPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button connect;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.TextBox ultrasoundRange1;
        private System.Windows.Forms.Timer ultrasoundRangeTimer;
        private System.Windows.Forms.TextBox ultrasoundRange2;
        private System.Windows.Forms.TrackBar servo_tilt_slider;
        private System.Windows.Forms.TrackBar servo_pan_slider;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button servo_center;
        private System.Windows.Forms.TrackBar left_motor;
        private System.Windows.Forms.TrackBar right_motor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.PictureBox pbTerrain;
        private System.Windows.Forms.GroupBox Simulation;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Timer Draw;
    }
}

