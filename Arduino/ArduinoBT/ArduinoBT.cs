﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.IO;
using System.Threading;

namespace ABT
{
    public class ArduinoBT
    {
        enum Command
        {
            SetMotorSpeed,
            GetUltrasoundRange,
            SetServoAngle,
        };

        public void SetServoAngle(int index, int angle)
        {
            byte[] data = new byte[3];
            data[0] = (byte)Command.SetServoAngle;
            data[1] = (byte)index;
            data[2] = (byte)angle;
            Write(data);
        }

        public int GetUltrasoundRange(int index)
        {
            byte[] data = new byte[2];
            data[0] = (byte)Command.GetUltrasoundRange;
            data[1] = (byte)index;
            Write(data);

            Thread.Sleep(100);

            // read two bytes and convert to int
            byte[] read = null;
            while (read == null)
            {
                read = Read(2);
            }

            int value = (read[0] << 8) + read[1];
            return value;
        }

        // speed = -1.0 to 1.0
        public void SetMotorSpeed(int index, float speed)
        {
            byte[] data = new byte[3];
            data[0] = (byte)Command.SetMotorSpeed;
            data[1] = (byte)index;
            data[2] = (byte)(speed * 127.0);
            Write(data);
        }

        public bool IsConnected()
        {
            if (m_serialPort == null)
                return false;

            return m_serialPort.IsOpen;
        }

        public bool Connect(int comPort, int baudRate = -1)
        {
            if (baudRate == -1)
            {
                m_serialPort = new SerialPort("COM" + comPort.ToString());
            }
            else
            {
                m_serialPort = new SerialPort("COM" + comPort.ToString(), baudRate);
            }
            m_serialPort.Encoding = Encoding.Default;
            m_serialPort.Parity = Parity.None;
            m_serialPort.DataBits = 8;
            m_serialPort.StopBits = StopBits.One;
            m_serialPort.ReceivedBytesThreshold = 1;
            //m_serialPort.DataReceived += new SerialDataReceivedEventHandler(OnDataReceived);

            try
            {
                m_serialPort.Open();

                if (m_serialPort.IsOpen)
                {
                    ClearConnection(); // incase we receive a bunch of packets from last our last connection
                    //VerifyConnection();
                    return true;
                }
            }
            catch (IOException e)
            {
                //if ((c + 1) == retryCount)
                {
                    Console.WriteLine("IOException - Make sure NXT brick is on and connected. Program possibly did not shut down correctly last time?");
                    Console.WriteLine(e.ToString());
                }
            }
            catch (UnauthorizedAccessException e)
            {
                //if ((c + 1) == retryCount)
                {
                    Console.WriteLine("UnauthorizedAccessException - Make sure another program is not already using this comport");
                    Console.WriteLine(e.ToString());
                }
            }

            Disconnect();
            return false;
        }

        public void Disconnect()
        {
            if (m_serialPort == null || !m_serialPort.IsOpen)
                return;

            ClearConnection();
            m_serialPort.Close();
            m_serialPort.Dispose();
            m_serialPort = null;
        }

        // read in all data from serial port and throw away
        protected void ClearConnection()
        {
            while (m_serialPort.BytesToRead > 0)
            {
                byte[] data = new byte[m_serialPort.BytesToRead];
                m_serialPort.Read(data, 0, m_serialPort.BytesToRead);
            }
        }

        public void Update()
        {
            while (m_serialPort.BytesToRead > 0)
            {
                OnDataReceived(null, null);
            }
        }

        protected void OnDataReceived(object sender, SerialDataReceivedEventArgs evt)
        {
        }

        public void Write(byte[] data)
        {
            if (!m_serialPort.IsOpen)
                return;

            m_serialPort.Write(data, 0, data.Length);
        }

        public byte[] Read(int minBytes = 1)
        {
            if (!m_serialPort.IsOpen)
                return null;

            byte[] data = null;
            if (m_serialPort.BytesToRead >= minBytes)
            {
                data = new byte[m_serialPort.BytesToRead];
                m_serialPort.Read(data, 0, m_serialPort.BytesToRead);
            }

            return data;
        }

        public SerialPort m_serialPort = new System.IO.Ports.SerialPort();
    }
}
