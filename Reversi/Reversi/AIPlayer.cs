﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Collections;

namespace Reversi
{
    // super compressed version of the environment
    // it can be recreated from this data
    [Serializable]
    public class EnvironmentHash
    {
        [Serializable]
        public class Comparer : IEqualityComparer<EnvironmentHash>
        {
            public bool Equals(EnvironmentHash x, EnvironmentHash y)
            {
                return x.a == y.a && x.b == y.b;
            }

            public int GetHashCode(EnvironmentHash x)
            {
                return (int)x.a ^ (int)x.b;
            }
        }

        public void SetBit(int index, bool value = true)
        {
            if (index > 64)
            {
                index -= 64;
                if (value)
                    b |= (1u << index);
                else
                    b &= ~(1u << index);
            }
            else
            {
                if (value)
                    a |= (1u << index);
                else
                    a &= ~(1u << index);
            }
        }

        public bool Equals(EnvironmentHash other)
        {
            return a == other.a && b == other.b;
        }

        public int Compare(EnvironmentHash other)
        {
            ulong deltaA = a - other.a;
            ulong deltaB = b - other.b;

            ulong delta = deltaA + deltaB;
            return (int)delta;
        }

        ulong a;
        ulong b;
    }

    

    class AIEnvironment
    {
        public class ValidMoveData
        {
            public Point validMove;
            public Board board;
            public EnvironmentHash hash;
            public Player winner;
        };

        Game m_game;

        public AIEnvironment(Game game)
        {
            m_game = game;

        }

        //
        // Generate a unique hash for the state of the game
        //
        // if mirror = true, mirrors the game, this is game specific
        //
        //
        public EnvironmentHash GetHash(Board board = null, bool mirror = false)
        {
            if (board == null)
            {
                board = m_game.GetBoard();
            }

            EnvironmentHash hash = new EnvironmentHash();
            int boardSize = board.GetBoardSize();

            for (int y = 0; y < boardSize; ++y)
            {
                for (int x = 0; x < boardSize; ++x)
                {
                    Board.Tile tile = board.GetTile(new Point(x, y));
                    if (tile != null)
                    {
                        // mirroring is game specific, reversi mirror x and y
                        // reverse player indexes as well
                        if (mirror)
                        {
                            int mirrorY = (boardSize - 1) - y;
                            int mirrorX = (boardSize - 1) - x;
                            hash.SetBit((mirrorY * boardSize * 2) + (mirrorX * 2) + tile.player.GetIndex());
                        }
                        else
                        {
                            hash.SetBit((y * boardSize * 2) + (x * 2) + tile.player.GetIndex());
                        }
                    }
                }
            }

            return hash;
        }

        public List<ValidMoveData> GetValidMoveDataList()
        {
            List<ValidMoveData> validMoveDataList = new List<ValidMoveData>();
            List<Point> validMoveList = m_game.GetValidMoveList();
            foreach (Point validMove in validMoveList)
            {
                ValidMoveData validMoveData = new ValidMoveData();
                validMoveData.validMove = validMove;
                validMoveData.board = m_game.GetBoardAfterMove(validMove);
                validMoveData.hash = GetHash(validMoveData.board);
                validMoveData.winner = null; // m_game.CheckForWinner(validMoveData.board, m_game.CalculateValidMovesForPlayer(m_game.GetCurrentPlayer(), validMoveData.board));
                validMoveDataList.Add(validMoveData);
            }

            return validMoveDataList;
        }
    }

    [Serializable]
    public class MemoryNode
    {
        public int FindChildIndex(EnvironmentHash hash)
        {
            int index = 0;
            foreach (MemoryNode child in childList)
            {
                if (child.hash.Equals(hash))
                    return index;

                ++index;
            }
            return -1;
        }

        public MemoryNode GetChildNode(int index)
        {
            if (index < 0)
                return null;

            return childList[index];
        }

        public int InsertChild(MemoryNode child)
        {
            childList.Add(child);
            return childList.Count() - 1;
        }

        public int GetChildCount()
        {
            return childList.Count();
        }

        public double GetBestValue(int lookAheadCount)
        {
            if (lookAheadCount <= 0)
                return value;

            double bestValue = value;
            foreach (MemoryNode child in childList)
            {
                double val = GetBestValue(lookAheadCount - 1);
                if (val > bestValue)
                    bestValue = val;
            }

            return bestValue;
        }
        /*
        // this might best be done as a hash table or a seperate binary tree
        public MemoryNode FindNode(EnvironmentHash hash)
        {
            if (this.hash != null && hash.Equals(this.hash))
                return this;

            foreach (MemoryNode child in childList)
            {
                MemoryNode result = child.FindNode(hash);
                if (result != null)
                    return result;
            }

            return null;
        }*/

        public double value;
        public EnvironmentHash hash;
        public List<MemoryNode> childList = new List<MemoryNode>();
    }

    [Serializable]
    class MemoryGraph : MemoryNode
    {
        public MemoryGraph()
        {
        }

        public MemoryNode FindAndInsertNode(MemoryNode parent, MemoryNode child)
        {
            // is it faster to look to see if its aleady a member of the family?
            try
            {
                if (lookupTable.ContainsKey(child.hash))
                    return lookupTable[child.hash];

                parent.InsertChild(child);
                lookupTable.Add(child.hash, child);
                return child;

            }
            catch (Exception ex)
            {
            }

            return null;
        }

        Dictionary<EnvironmentHash, MemoryNode> lookupTable = new Dictionary<EnvironmentHash, MemoryNode>(new EnvironmentHash.Comparer());
    }

    class AIPlayer : Player
    {
        Random m_random;
        AIEnvironment m_environment;
        MemoryGraph m_memory;
        MemoryNode m_memoryCursor;
        MemoryNode m_opponentMemoryCursor;
        int m_lookAheadCount = 2;
        double m_winValue = 1.0;
        double m_loseValue = -1.0;
        double m_randomChance = 0.1;

        List<MemoryNode> m_history = new List<MemoryNode>();
        List<MemoryNode> m_opponentHistory = new List<MemoryNode>();

        public AIPlayer(int index, AIEnvironment environment) 
            : base(index)
        {
            m_environment = environment;
            m_random = new Random();

            m_memory = LoadObject<MemoryGraph>("memory_graph_" + m_index.ToString() + ".ser");
            if (m_memory == null)
            {
                m_memory = new MemoryGraph();
            }
        }

        ~AIPlayer()
        {
            //SaveObject<MemoryGraph>(m_memory, "memory_graph_" + m_index.ToString() + ".ser");
        }

        private void SaveObject<T>(T List, string FileName)
        {
            try
            {

                //create a backup
                string backupName = Path.ChangeExtension(FileName, ".old");
                if (File.Exists(FileName))
                {
                    if (File.Exists(backupName))
                        File.Delete(backupName);
                    File.Move(FileName, backupName);
                }

                using (FileStream fs = new FileStream(FileName, FileMode.Create))
                {
                    //XmlSerializer ser = new XmlSerializer(typeof(T));
                    BinaryFormatter ser = new BinaryFormatter();
                    ser.Serialize(fs, List);
                    fs.Flush();
                    fs.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private static T LoadObject<T>(string FileName)
        {
            if (!File.Exists(FileName))
            {
                return default(T);
            }

            T result = default(T);

            try
            {
                using (FileStream fs = new FileStream(FileName, FileMode.Open))
                {
                    //XmlSerializer ser = new XmlSerializer(typeof(T));
                    BinaryFormatter ser = new BinaryFormatter();
                    result = (T)ser.Deserialize(fs);
                }
            }
            catch
            {
            }
            return result;
        }

        public Point? HaveTurn()
        {
            // I wonder if we should age nodes
            // nodes that havent been explored for ages are more likely to be explored and also culled
            // why remember anything but winning paths? or how to get to winning paths?
            // this will also encourage a continous exploration insted of having to force it 
            // manually

            UpdateMemoryCursor();

            // iterate over potential moves
            // find one with high value, even do look ahead
            // to find future reward
            // - possibly leave running in a thread to scan for the ultimate kill?
            // - maybe weight what the opponent is likely to do?
            // - possibly cull low scoring solutions, or could this cause us to explore too much. 
            //      perhaps track how much we have explored from a node, and only keep the good solutions

            // the more we have explored, the less likely we are to explore
            List<AIEnvironment.ValidMoveData> validMoveDataList = m_environment.GetValidMoveDataList();
            int childCount = m_memoryCursor.GetChildCount();
            double exploreChance = 1.0 - ((double)childCount / (double)validMoveDataList.Count());
            bool explore = (m_random.NextDouble() < exploreChance);

            /*
            // DO I EVEN NEED THIS? WILL LEARN THIS EVENTUALLY
            // if we find  winning solution DO IT! DO IT NOW!
            // also insert any winning or lossing solutions with appropriate weights
            AIEnvironment.ValidMoveData winningValidMove = null;
            //MemoryNode winningNode = null;
            foreach (AIEnvironment.ValidMoveData validMoveData in validMoveDataList)
            {
                if (validMoveData.winner == null)
                    continue;

                int index = m_memory.FindChildIndex(currentEnvironmentHash);
                if (validMoveData.winner == this)
                {
                    winningValidMove = validMoveData;
                    /*
                    if (index < 0)
                    {
                        MemoryNode node = new MemoryNode();
                        node.hash = validMoveData.hash;
                        node.value = m_winValue;
                        m_memoryCursor.InsertChild(node);
                        winningNode = node;
                    }
                    else
                    {
                        winningNode = m_memoryCursor.GetChildNode(index);
                    }* /
                }
                else // we have a winner but its not us, we will loose!
                {/*
                    if (index >= 0)
                        continue;

                    // hrmm what can we do with this data?
                    MemoryNode node = new MemoryNode();
                    node.hash = validMoveData.hash;
                    node.value = m_loseValue;
                    m_memoryCursor.InsertChild(node);* /
                }
            }

            if (winningValidMove != null)
            {
                return ReturnValidMove(winningValidMove);
            }
            */

            // generate a list of valid moves that we havent explored
            List<AIEnvironment.ValidMoveData> unexploredValidMoveDataList = GetUnexploredValidMoveDataList(m_memoryCursor, validMoveDataList);
            if (explore && unexploredValidMoveDataList.Count() > 0)
                return ReturnValidMove(unexploredValidMoveDataList[m_random.Next(0, unexploredValidMoveDataList.Count())]);

            // pick a random one, we need to do this so we dont always pick the winning solution
            // sometimes you need to pick something with low reward even if your going to loose
            // as you might find a better solution
            if (m_random.NextDouble() < m_randomChance)
                ReturnValidMove(validMoveDataList[m_random.Next(0, validMoveDataList.Count())]);

            int bestHashIndex = GetBestHashIndex(validMoveDataList, m_memoryCursor, m_lookAheadCount);

            // if we get -1 here, it means we havent explored this way, ie. this is new node
            // so we are forced to explore anyway
            if (bestHashIndex == -1 && unexploredValidMoveDataList.Count() > 0)
                return ReturnValidMove(unexploredValidMoveDataList[m_random.Next(0, unexploredValidMoveDataList.Count())]);

            // if here, we've probably only got a loosing move left, return any move we can
            if (bestHashIndex == -1)
                return ReturnValidMove(validMoveDataList[m_random.Next(0, validMoveDataList.Count())]);

            // return best move we know about
            // progress memory cursor and keep history of path through the network

            return ReturnValidMove(validMoveDataList[bestHashIndex]);
        }

        Point ReturnValidMove(AIEnvironment.ValidMoveData validMoveData)
        {
            // returns the move as valid, also
            // adds the valid move to memory 
            // and sets the cursor as the new memory
            MemoryNode node = new MemoryNode();
            node.hash = validMoveData.hash;
            m_memoryCursor = m_memory.FindAndInsertNode(m_memoryCursor, node);
            m_history.Add(m_memoryCursor);

            // now for the opponent
            MemoryNode reverseNode = new MemoryNode();
            reverseNode.hash = m_environment.GetHash(validMoveData.board, true);
            m_opponentMemoryCursor = m_memory.FindAndInsertNode(m_opponentMemoryCursor, reverseNode);
            m_opponentHistory.Add(m_opponentMemoryCursor);

            return validMoveData.validMove;
        }

        List<AIEnvironment.ValidMoveData> GetUnexploredValidMoveDataList(MemoryNode node, List<AIEnvironment.ValidMoveData> validMoveDataList)
        {
            List<AIEnvironment.ValidMoveData> unexploredValidMoveDataList = new List<AIEnvironment.ValidMoveData>();

            foreach (AIEnvironment.ValidMoveData validMoveData in validMoveDataList)
            {
                if (validMoveData.winner == null && node.FindChildIndex(validMoveData.hash) == -1)
                    unexploredValidMoveDataList.Add(validMoveData);
            }

            return unexploredValidMoveDataList;
        }

        int GetBestHashIndex(List<AIEnvironment.ValidMoveData> validMoveDataList, MemoryNode node, int lookAheadCount)
        {
            int bestHashIndex = -1;
            double bestValue = double.NegativeInfinity;
            for (int i = 0; i < validMoveDataList.Count(); ++i)
            {
                MemoryNode childNode = node.GetChildNode(node.FindChildIndex(validMoveDataList[i].hash));
                if (childNode == null)
                    continue;

                double value = childNode.GetBestValue(lookAheadCount);
                if (value > bestValue)
                {
                    bestValue = value;
                    bestHashIndex = i;
                }
            }

            return bestHashIndex;
        }

        public void StartGame()
        {
            m_memoryCursor = m_memory;
            m_opponentMemoryCursor = m_memory;

            UpdateMemoryCursor();
        }

        void UpdateMemoryCursor()
        {
            // hash the environment, and find it in memory
            EnvironmentHash currentEnvironmentHash = m_environment.GetHash();
            EnvironmentHash reverseEnvironmentHash = m_environment.GetHash(null, true);

            // if not found, insert it, we are learning something new!
            MemoryNode node = new MemoryNode();
            node.hash = currentEnvironmentHash;
            m_memoryCursor = m_memory.FindAndInsertNode(m_memoryCursor, node);
            m_history.Add(m_memoryCursor);

            /*
            int childIndex = m_memoryCursor.FindChildIndex(currentEnvironmentHash);
            if (childIndex == -1)
            {
                MemoryNode node = FindNode(currentEnvironmentHash);
                if (node == null)
                {
                    node = new MemoryNode();
                    node.hash = currentEnvironmentHash;
                    m_memoryCursor.InsertChild(node);
                }
                m_memoryCursor = node;
            }
            else
            {
                m_memoryCursor = m_memoryCursor.GetChildNode(childIndex);
            }
            m_history.Add(m_memoryCursor);*/

            // if not found, insert it, we are learning something new!
            MemoryNode reverseNode = new MemoryNode();
            reverseNode.hash = reverseEnvironmentHash;
            m_opponentMemoryCursor = m_memory.FindAndInsertNode(m_opponentMemoryCursor, reverseNode);
            m_opponentHistory.Add(m_opponentMemoryCursor);
            /*
            childIndex = m_opponentMemoryCursor.FindChildIndex(reverseEnvironmentHash);
            if (childIndex == -1)
            {
                MemoryNode node = FindNode(reverseEnvironmentHash);
                if (node == null)
                {
                    node = new MemoryNode();
                    node.hash = reverseEnvironmentHash;
                    int index = m_opponentMemoryCursor.InsertChild(node);
                }
                m_opponentMemoryCursor = node;
            }
            else
            {
                m_opponentMemoryCursor = m_opponentMemoryCursor.GetChildNode(childIndex);
            }
            m_opponentHistory.Add(m_opponentMemoryCursor);*/
        }

        public void EndGame(bool winner)
        {
            m_memoryCursor = null;

            PropagateWeights(m_history, winner ? m_winValue : m_loseValue);

            // assumes 2 player game
            PropagateWeights(m_opponentHistory, winner ? m_loseValue : m_winValue);

            m_opponentHistory.Clear();
            m_history.Clear();
        }

        public void Save()
        {
            SaveObject<MemoryGraph>(m_memory, "memory_graph_" + m_index.ToString() + ".ser");
        }
        /*
        public MemoryNode FindNode(EnvironmentHash hash)
        {
            return m_memory.FindNode(hash);
        }
        */

        public void PropagateWeights(List<MemoryNode> history, double startValue)
        {
            // go through history and update values
            // strengthen if we win, reduce if we loose
            // - possibly modify siblings weights like a self oranising map?

            MemoryNode lastMove = m_history[m_history.Count() - 1];
            lastMove.value = startValue;

            // propagate changes back through the network
            for (int i = m_history.Count() - 2; i >= 0; --i)
            {
                MemoryNode prevNode = m_history[i + 1];
                MemoryNode node = m_history[i];

                double valueDelta = prevNode.value - node.value;
                node.value += valueDelta * 0.8; // sigmoid or squaring or something, the further they are apparet the more they move together, the closer they are the smaller they should move together
            }
        }
    }
}
