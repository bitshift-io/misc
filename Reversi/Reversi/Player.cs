﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Reversi
{
    class Player
    {
        int m_score;
        protected int m_index;

        public Player(int index)
        {
            m_index = index;
        }

        public Color GetColor()
        {
            switch (m_index)
            {
                case 0:
                    return Color.Black;

                case 1:
                    return Color.White;
            }

            return Color.HotPink;
        }

        public void SetScore(int score)
        {
            m_score = score;
        }

        public int GetScore()
        {
            return m_score;
        }

        public int GetIndex()
        {
            return m_index;
        }
    }
}
