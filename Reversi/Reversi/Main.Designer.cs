﻿namespace Reversi
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.board = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.player0_score = new System.Windows.Forms.Label();
            this.player1_score = new System.Windows.Forms.Label();
            this.game_state = new System.Windows.Forms.Label();
            this.human_v_ai = new System.Windows.Forms.Button();
            this.ai_v_ai = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.board)).BeginInit();
            this.SuspendLayout();
            // 
            // board
            // 
            this.board.Location = new System.Drawing.Point(12, 12);
            this.board.Name = "board";
            this.board.Size = new System.Drawing.Size(400, 400);
            this.board.TabIndex = 0;
            this.board.TabStop = false;
            this.board.Click += new System.EventHandler(this.board_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(418, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "P1 (Black) Score";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(421, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "P2 (White) Score";
            // 
            // player0_score
            // 
            this.player0_score.AutoSize = true;
            this.player0_score.Location = new System.Drawing.Point(548, 12);
            this.player0_score.Name = "player0_score";
            this.player0_score.Size = new System.Drawing.Size(13, 13);
            this.player0_score.TabIndex = 3;
            this.player0_score.Text = "0";
            // 
            // player1_score
            // 
            this.player1_score.AutoSize = true;
            this.player1_score.Location = new System.Drawing.Point(548, 29);
            this.player1_score.Name = "player1_score";
            this.player1_score.Size = new System.Drawing.Size(13, 13);
            this.player1_score.TabIndex = 4;
            this.player1_score.Text = "0";
            // 
            // game_state
            // 
            this.game_state.AutoSize = true;
            this.game_state.Location = new System.Drawing.Point(419, 56);
            this.game_state.Name = "game_state";
            this.game_state.Size = new System.Drawing.Size(52, 13);
            this.game_state.TabIndex = 5;
            this.game_state.Text = "P1\'s Turn";
            // 
            // human_v_ai
            // 
            this.human_v_ai.Location = new System.Drawing.Point(424, 388);
            this.human_v_ai.Name = "human_v_ai";
            this.human_v_ai.Size = new System.Drawing.Size(75, 23);
            this.human_v_ai.TabIndex = 6;
            this.human_v_ai.Text = "Human v AI";
            this.human_v_ai.UseVisualStyleBackColor = true;
            this.human_v_ai.Click += new System.EventHandler(this.new_game_Click);
            // 
            // ai_v_ai
            // 
            this.ai_v_ai.Location = new System.Drawing.Point(424, 359);
            this.ai_v_ai.Name = "ai_v_ai";
            this.ai_v_ai.Size = new System.Drawing.Size(75, 23);
            this.ai_v_ai.TabIndex = 7;
            this.ai_v_ai.Text = "AI v AI";
            this.ai_v_ai.UseVisualStyleBackColor = true;
            this.ai_v_ai.Click += new System.EventHandler(this.ai_v_ai_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 423);
            this.Controls.Add(this.ai_v_ai);
            this.Controls.Add(this.human_v_ai);
            this.Controls.Add(this.game_state);
            this.Controls.Add(this.player1_score);
            this.Controls.Add(this.player0_score);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.board);
            this.Name = "Main";
            this.Text = "Reversi";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.board)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox board;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label player0_score;
        private System.Windows.Forms.Label player1_score;
        private System.Windows.Forms.Label game_state;
        private System.Windows.Forms.Button human_v_ai;
        private System.Windows.Forms.Button ai_v_ai;
    }
}

