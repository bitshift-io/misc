﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;

namespace Reversi
{
    //
    // Responsible for manipulating the board
    // ie. handling rules and telling the board where the peices are
    //
    class Game
    {
        Main m_form;
        Board m_board;
        int m_playerCount = 2;
        Player[] m_playerList;
        int m_currentPlayerIndex;
        System.Timers.Timer m_updateTimer;
        Point m_cursorTile;
        bool m_mouseClick = false;

        List<Point> m_validMoveList = new List<Point>();

        //bool[,] m_validMoves;


        Point[] m_scanDirectionList = new Point[] {
                new Point(1, 0),
                new Point(1, 1),
                new Point(0, 1),
                new Point(-1, 1),
                new Point(-1, 0),
                new Point(-1, -1),
                new Point(0, -1),
                new Point(1, -1),
            };

        public Game(Main form)
        {
            m_form = form;
            m_updateTimer = new System.Timers.Timer();
            m_updateTimer.Elapsed += new ElapsedEventHandler(UpdateTimerElapsed);
            m_updateTimer.Interval = 1000 / 30; // 30 times per second

            m_playerList = new Player[m_playerCount];

            StartHumanAI();
        }

        int GetAIPlayerCount()
        {
            int aiPlayerCount = 0;
            foreach (Player player in m_playerList)
            {
                if (player.GetType() == typeof(AIPlayer))
                {
                    ++aiPlayerCount;
                }
            }

            return aiPlayerCount;
        }

        public void StartHumanAI()
        {
            m_playerList[0] = new Player(0);
            m_playerList[1] = new AIPlayer(1, new AIEnvironment(this));

            StartNewGame();
        }

        public void StartAIAI()
        {
            m_playerList[0] = new AIPlayer(0, new AIEnvironment(this));
            m_playerList[1] = new AIPlayer(1, new AIEnvironment(this));

            StartNewGame();
        }

        public void StartNewGame()
        {
            m_board = new Board(m_form.GetGamePictureBox());
            m_currentPlayerIndex = 0;

            // set up the initial state of the board
            Board.Tile tile = new Board.Tile();
            tile.player = m_playerList[0];
            tile.index = new Point(3, 3);
            m_board.SetTile(tile);

            tile = new Board.Tile();
            tile.player = m_playerList[0];
            tile.index = new Point(4, 4);
            m_board.SetTile(tile);

            tile = new Board.Tile();
            tile.player = m_playerList[1];
            tile.index = new Point(3, 4);
            m_board.SetTile(tile);

            tile = new Board.Tile();
            tile.player = m_playerList[1];
            tile.index = new Point(4, 3);
            m_board.SetTile(tile);

            int boardSize = m_board.GetBoardSize();
            m_validMoveList = CalculateValidMovesForPlayer(GetCurrentPlayer(), m_board);

            SetGameMessage("Player 1's Turn");
            CalculateScore(m_board);

            Draw();

            foreach (Player player in m_playerList)
            {
                if (player.GetType() == typeof(AIPlayer))
                {
                    AIPlayer ai = (AIPlayer)player;
                    ai.StartGame();
                }
            }

            m_updateTimer.Start();
        }

        public List<Point> GetValidMoveList()
        {
            return m_validMoveList;
        }

        public Board GetBoard()
        {
            return m_board;
        }

        public Player[] GetPlayerList()
        {
            return m_playerList;
        }

        public Player GetCurrentPlayer()
        {
            return m_playerList[m_currentPlayerIndex];
        }

        public void SaveAI()
        {
            foreach (Player player in m_playerList)
            {
                if (player.GetType() == typeof(AIPlayer))
                {
                    AIPlayer ai = (AIPlayer)player;
                    ai.Save();
                }
            }
        }

        void EndCurrentPlayerTurn()
        {
            ++m_currentPlayerIndex;
            if (m_currentPlayerIndex >= m_playerList.Count())
            {
                m_currentPlayerIndex = 0;
            }
        }

        public void UpdateTimerElapsed(object source, ElapsedEventArgs e)
        {
            MethodInvoker mi = new MethodInvoker(Update);
            m_form.BeginInvoke(mi);
        }

        public void MouseClick()
        {
            m_mouseClick = true;
        }

        public bool IsValidMove(Point tile)
        {
            foreach (Point validMove in m_validMoveList)
            {
                if (tile.X == validMove.X && tile.Y == validMove.Y)
                    return true;
            }

            return false;
        }

        public List<Point> CalculateValidMovesForPlayer(Player player, Board board)
        {
            List<Point> validMoveList = new List<Point>();
            //Player player = GetCurrentPlayer();

            int boardSize = m_board.GetBoardSize();
            for (int y = 0; y < boardSize; ++y)
            {
                for (int x = 0; x < boardSize; ++x)
                {
                    Board.Tile tile = m_board.GetTile(new Point(x, y));
                    if (tile != null && tile.player == player)
                    {
                        foreach (Point scanDirection in m_scanDirectionList)
                        {
                            Point? validMove = ScanValidMove(tile, scanDirection);
                            if (validMove.HasValue)
                                validMoveList.Add(validMove.Value);
                        }
                    }
                }
            }

            return validMoveList;
        }

        public Point? ScanValidMove(Board.Tile tile, Point scanDirection)
        {
            Point currentIndex = new Point(tile.index.X, tile.index.Y);

            int opponentTileCount = -1;
            Board.Tile currentTile = null;
            do
            {
                currentIndex.X += scanDirection.X;
                currentIndex.Y += scanDirection.Y;

                currentTile = m_board.GetTile(currentIndex);

                if (currentTile != null && currentTile.player == tile.player)
                    return null;

                ++opponentTileCount;
            } 
            while (currentTile != null && currentTile.player != tile.player);

            if (opponentTileCount > 0 && m_board.IsValidIndex(currentIndex))
            {
                return currentIndex;
            }

            return null;
        }

        public void ConvertToPlayer(Board board, Player player, Point index, Point scanDirection)
        {
            // place the new tile
            Board.Tile tile = new Board.Tile();
            tile.player = player;
            tile.index = index;
            board.SetTile(tile);

            // go through and convert opponent tiles
            Point currentIndex = new Point(tile.index.X, tile.index.Y);
            Board.Tile currentTile = null;

            // scan through to the end, need a valid tile belonging to us to be able to convert
            do
            {
                currentIndex.X += scanDirection.X;
                currentIndex.Y += scanDirection.Y;

                currentTile = board.GetTile(currentIndex);

                if (currentTile == null)
                    return;

            } while (currentTile != null && currentTile.player != tile.player);

            int opponentTileCount = -1;
            
            // iterate back convert opponent tiles
            do
            {
                currentIndex.X -= scanDirection.X;
                currentIndex.Y -= scanDirection.Y;

                currentTile = board.GetTile(currentIndex);

                if (currentTile != null && currentTile.player == tile.player)
                    return;

                if (currentTile != null)
                    currentTile.player = player;

                ++opponentTileCount;
            }
            while (currentTile != null);
        }

        public void PerformMove(Point index)
        {
            m_board = GetBoardAfterMove(index);
        }

        public Board GetBoardAfterMove(Point index)
        {
            Board postMoveBoard = m_board.Clone();

            Player player = GetCurrentPlayer();
            foreach (Point scanDirection in m_scanDirectionList)
            {
                ConvertToPlayer(postMoveBoard, player, index, scanDirection);
            }

            return postMoveBoard;
        }

        public void CalculateScore(Board board)
        {
            foreach (Player player in m_playerList)
            {
                player.SetScore(0);
            }

            int boardSize = board.GetBoardSize();
            for (int y = 0; y < boardSize; ++y)
            {
                for (int x = 0; x < boardSize; ++x)
                {
                    Board.Tile tile = board.GetTile(new Point(x, y));
                    if (tile == null || tile.player == null)
                        continue;

                    tile.player.SetScore(tile.player.GetScore() + 1);
                }
            }

            // update score UI
            foreach (Player player in m_playerList)
            {
                m_form.SetPlayerScore(player.GetIndex(), player.GetScore());
            }
        }

        public void SetGameMessage(String message)
        {
            m_form.SetGameMessage(message);
        }
        /*
        public Player CheckForWinner(Board board, List<Point> validMoveList)
        {
            CalculateScore(board);
            
            int emptyTileCount = 0;
            int boardSize = board.GetBoardSize();
            for (int y = 0; y < boardSize; ++y)
            {
                for (int x = 0; x < boardSize; ++x)
                {
                    Board.Tile tile = board.GetTile(new Point(x, y));
                    if (tile == null)
                        ++emptyTileCount;
                }
            }

            Player winner = null;

            if (emptyTileCount == 0)
            {
                // previous player wins, assumes only 2 players
                EndCurrentPlayerTurn();
                winner = GetCurrentPlayer();
            }

            if (validMoveList.Count() == 0)
            {
                foreach (Player player in m_playerList)
                {
                    if (winner == null || player.GetScore() > winner.GetScore())
                        winner = player;
                }
            }

            return winner;
        }*/

        public void EndGame(Player winner)
        {
            SetGameMessage("Player " + (winner.GetIndex() + 1) + " Wins");
            m_updateTimer.Stop();

            foreach (Player player in m_playerList)
            {
                if (player.GetType() == typeof(AIPlayer))
                {
                    AIPlayer ai = (AIPlayer)player;
                    ai.EndGame(winner == player);
                }
            }

            // AI dont need to wait for new game, keep going!
            if (GetAIPlayerCount() >= 2)
            {
                StartNewGame();
            }
        }

        public void Update()
        {
            bool draw = false;
            Point cursorTile = m_board.PointToTile(System.Windows.Forms.Cursor.Position);
            if (cursorTile != m_cursorTile)
            {
                m_cursorTile = cursorTile;
                draw = true;
            }

            Player player = GetCurrentPlayer();
            if (player.GetType() == typeof(AIPlayer))
            {
                AIPlayer ai = (AIPlayer)player;

                Point? aiCursor = ai.HaveTurn();
                if (aiCursor.HasValue)
                {
                    m_cursorTile = aiCursor.Value;
                    m_mouseClick = true;
                }
            }

            if (m_mouseClick)
            {
                if (IsValidMove(m_cursorTile))
                {
                    PerformMove(m_cursorTile);
                    CalculateScore(m_board);

                    Draw(); // for debugging

                    Player winner = null;
                    int playerCantMoveCount = 0;
                    for (int i = 0; i < m_playerCount; ++i)
                    {
                        EndCurrentPlayerTurn();
                        m_validMoveList = CalculateValidMovesForPlayer(GetCurrentPlayer(), m_board);
                        if (m_validMoveList.Count() <= 0)
                            ++playerCantMoveCount;
                        else
                            break;
                    } 

                    if (playerCantMoveCount >= m_playerCount)
                    {
                        foreach (Player p in m_playerList)
                        {
                            if (winner == null || p.GetScore() > winner.GetScore())
                                winner = p;
                        }
                    }
                    
                    
                    /*

                    Player winner = CheckForWinner(m_board, m_validMoveList);*/
                    if (winner != null)
                    {
                        EndGame(winner);
                    }
                    else
                    {
                        SetGameMessage("Player " + (GetCurrentPlayer().GetIndex() + 1) + "'s Turn");
                    }

                    draw = true;
                }
                m_mouseClick = false;
            }

            if (draw)
                Draw();
        }

        public void Draw()
        {
            Graphics g = m_board.DrawBegin();
            m_board.DrawBoard(g);
            DrawValidMoves(g);
            m_board.DrawCursor(g, m_cursorTile, Color.Green);
            m_board.DrawEnd(g);
        }

        public void DrawValidMoves(Graphics g)
        {
            Player player = GetCurrentPlayer();
            Pen pen = new Pen(player.GetColor());

            foreach (Point validMove in m_validMoveList)
            {
                Rectangle rect = m_board.GetRectangle(validMove);
                g.DrawEllipse(pen, rect);
            }
        }
    }
}
