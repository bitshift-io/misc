/*
 * Copyright (C) 2009 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package SlideShowLiveWallpaper.Android;

import SlideShowLiveWallpaper.Android.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class Settings extends PreferenceActivity
    implements SharedPreferences.OnSharedPreferenceChangeListener {

	protected FilePreference mActivePreferenceIntent;
	
    @Override
    protected void onCreate(Bundle icicle)
    {
        super.onCreate(icicle);
        getPreferenceManager().setSharedPreferencesName(
                Wallpaper.SHARED_PREFS_NAME);
        addPreferencesFromResource(R.xml.settings);
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(
                this);
        
        FilePreference pref = (FilePreference)this.findPreference("FilePreference");
        pref.setSettings(this);
        // test
        //this.startActivityForResult(intent, requestCode)
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
    	//this.onActivityResult(requestCode, resultCode, data)
    	if (requestCode == FilePreference.FILE_INTENT)
    	{
    		if (mActivePreferenceIntent.onActivityResult(requestCode, resultCode, data))
    		{
    			mActivePreferenceIntent = null;
    		}
    	}
    	else
    	{
    		super.onActivityResult(requestCode, resultCode, data);
    	}
    }
    
    public void filePreferenceIntentStarted(FilePreference preference)
    {
    	mActivePreferenceIntent = preference;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(
                this);
        super.onDestroy();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
            String key) {
    }
}
