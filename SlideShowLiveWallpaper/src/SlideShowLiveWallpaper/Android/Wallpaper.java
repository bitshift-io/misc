/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package SlideShowLiveWallpaper.Android;

import java.io.File;
import java.io.FilenameFilter;

import SlideShowLiveWallpaper.Android.R;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.SystemClock;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.ads.AdSenseSpec;
import com.google.ads.GoogleAdView;
import com.google.ads.AdSenseSpec.AdType;

/*
 * This animated wallpaper draws a rotating wireframe cube.
 */
public class Wallpaper extends WallpaperService 
{

    private final Handler mHandler = new Handler();
    public static final String SHARED_PREFS_NAME = "slide_show_wallpaper_settings";

    @Override
    public void onCreate()
    {
        super.onCreate();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public Engine onCreateEngine() 
    {
        return new WallpaperEngine(this);
    }

    class WallpaperEngine extends Engine implements SharedPreferences.OnSharedPreferenceChangeListener
    {
    	class BitmapFilter implements FilenameFilter
    	{
    	    public boolean accept(File dir, String name)
    	    {
    	        return name.endsWith(".jpg") || name.endsWith(".png") || name.endsWith(".bmp");
    	    }
    	}
    	
    	class BitmapLoader extends Thread
    	{
    		// cant restart threads, so we just new a new thread every time
			public BitmapLoader(String filename)
			{
				try
        		{
					mBitmap = null;
					mFilename = filename;
					
					start();
        		}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			
    		@Override
			public void run() 
			{
    			try
        		{
    				int MAX_WIDTH = 960;
    				int MAX_HEIGHT = 800;
    				
    				// first extract dimensions of image we are loading
    				BitmapFactory.Options options = new BitmapFactory.Options();
    				options.inJustDecodeBounds = true;
    				mBitmap = BitmapFactory.decodeFile(mFilename, options);
    				
    				// calculate a scale and load the actual image
    				options.inJustDecodeBounds = false;
    				options.inSampleSize = Math.min(options.outHeight / MAX_HEIGHT, options.outWidth / MAX_WIDTH);
    				//mBitmap = BitmapFactory.decodeFile(mFilename, options);
    				Bitmap immutableBitmap = BitmapFactory.decodeFile(mFilename, options);
    				
    				mBitmap = immutableBitmap;
    				/*
    				
    				Bitmap mutableBitmap = immutableBitmap.copy(Bitmap.Config.ARGB_8888, true);
    				immutableBitmap.recycle();
    				
    				// tint bitmap, 40% visible
    				// OPTIMISE!
    				float tint = 0.4f;
    				for (int x = 0; x < options.outWidth; ++x)
    				{
    					for (int y = 0; y < options.outHeight; ++y)
    					{
    						int pixel = mutableBitmap.getPixel(x, y);
    						int red = (int)((float)Color.red(pixel) * tint);
    						int green = (int)((float)Color.green(pixel) * tint);
    						int blue = (int)((float)Color.blue(pixel) * tint);
    						int alpha = 255;
    						//intBitmap[y * immutableBitmap.getWidth() + x] = Color.rgb(red, green, blue);
    						mutableBitmap.setPixel(x, y, Color.argb(alpha, red, green, blue));
    					}
    				}
    				
    				// create as immutable
    				mBitmap = mutableBitmap.copy(Bitmap.Config.ARGB_8888, false);
    				mutableBitmap.recycle();
    				
    				//mBitmap = Bitmap.createBitmap(intBitmap, mBitmap.getWidth(), mBitmap.getHeight(), Bitmap.Config.RGB_565);
    				
    				
    				//mBitmap = BitmapFactory.decodeFile(mFilename);
    				//mBitmap = Bitmap.createScaledBitmap(mBitmap, 960, 800, true);*/
        		}
				catch (Exception e)
				{
					// out of memory most likely
					e.printStackTrace();
				}
			}
			
    		
			String	mFilename;
			Bitmap 	mBitmap;
    	}
    	
    
    	private SharedPreferences mPrefs;
    	
    	private boolean			mFullVersion	= true;
    	private BitmapLoader 	mBitmapLoader;
    	private String[]		mFilenameList;
    	
        private final Paint mPaint = new Paint();
        private float mOffset;
        private float mTouchX = -1;
        private float mTouchY = -1;
        private long mStartTime;
        private float mCenterX;
        private float mCenterY;
        
        // touch events cause the background to shift around
        float mOffsetX = 0.f;
        float mOffsetY = 0.f;
        
        float mRotationDegrees = 0.f;
        
        Bitmap 	mNext;
        Bitmap 	mCurrent;
        int		mCurrentIndex = 0;
        long 	mTimer = 0;
        long	mTimeDelay; // time between photos seconds
        
        float 	mTintPercent = 0.f;
        long 	mTransitionTime = 300; // transition time, transitions cause lag so make em fast
        long	mTransitionTimer = 0;

        long 	mLastTouchEventTime = 0;
        
        // a high fps and low fps, touch and image transitions are done at high speed
        long	mSleepSlow = 1000 / 10;
        long	mSleepFast = 1000 / 60;
        long	mTouchTime = 1000; // 1 second from touch event causes high fps, incase more touch events come in
        
        Wallpaper mApplication;
        
        RelativeLayout mRelativeView;
        AbsoluteLayout mLayout;
        GoogleAdView mAdView;
        WebView mWebView;
        
        Button mTestButton;

        private final Runnable mDrawCube = new Runnable() {
            public void run() {
                drawFrame();
            }
        };
        private boolean mVisible;


        WallpaperEngine(Wallpaper application) {
        	//application.setContentView(R.layout.main);
        	//TextView temp = (TextView)this.findViewById(R.id.advert);
        	
        	if (!mFullVersion)
        	{
	        	// set up ads
	        	// http://www.google.com/codesearch/p?hl=en#XXteUaxV20E/trunk/ZombieRun/src/net/peterd/zombierun/activity/Util.java&q=GoogleAdView%20package:http://zombie-run%5C.googlecode%5C.com&sa=N&cd=1&ct=rc
	        	// http://code.google.com/intl/zh-CN/mobile/ads/docs/android/
	        	AdSenseSpec adSenseSpec =
	        		new AdSenseSpec("pub-9246408363565140")
	            .setCompanyName("Black Carbon")
	            .setAppName("Slide Show Live Wallpaper")
	            //.setKeywords(activity.getString(R.string.adsense_keywords))
	            .setChannel("2510425528")
	            .setAdType(AdType.TEXT_IMAGE)
	            //.setWebEquivalentUrl(activity.getString(R.string.about_url))
	            .setAdTestEnabled(true)
	            .setExpandDirection(AdSenseSpec.ExpandDirection.TOP)
	            .setAdType(AdSenseSpec.AdType.TEXT_IMAGE)
	            .setAdFormat(AdSenseSpec.AdFormat.FORMAT_320x50);
	        	
	        	// Set up GoogleAdView and fetch ad
	        	mAdView = new GoogleAdView(application.getApplicationContext());
	        	/*
	        	
	        	new Handler().post(new Runnable() {
	                public void run() {
	                  AdSenseSpec adSenseSpec =
	                      new AdSenseSpec("pub-9246408363565140")
	                          .setCompanyName("Black Carbon")
	                          .setAppName("SteamPunk Live Wallpaper")
	                          //.setKeywords(activity.getString(R.string.adsense_keywords))
	                          .setChannel("2510425528")
	                          .setAdType(AdType.TEXT_IMAGE)
	                          //.setWebEquivalentUrl(activity.getString(R.string.about_url))
	                          .setAdTestEnabled(true)
	                          .setExpandDirection(AdSenseSpec.ExpandDirection.TOP)
	                          .setAdType(AdSenseSpec.AdType.TEXT_IMAGE)
	                          .setAdFormat(AdSenseSpec.AdFormat.FORMAT_320x50);
	
	                  // Fetch Google ad.
	                  // PLEASE DO NOT CLICK ON THE AD UNLESS YOU ARE IN TEST MODE.
	                  // OTHERWISE, YOUR ACCOUNT MAY BE DISABLED.
	                  mAdView.showAds(adSenseSpec);
	                }
	              });
	        	
	        	*/
	        
	        	
	        	mLayout = new AbsoluteLayout(application.getApplicationContext());
	        	//mLayout.
	        	//AbsoluteLayout.LayoutParams p;arams = new AbsoluteLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	        	//LayoutParams params = absoluteLayout.generateDefaultLayoutParams();
	        	LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	            
	            mAdView.setLayoutParams(params);
	            mAdView.showAds(adSenseSpec);
	            mAdView.setEnabled(true);
	            mAdView.setVisibility(View.VISIBLE);
	            mAdView.bringToFront(); 
	            mAdView.requestFocus(); 
	            mAdView.invalidate();
	            

	            mRelativeView = new RelativeLayout(application.getApplicationContext());
	            	
	            // http://www.anddev.org/sell_your_app_or_run_ads_in_it-t4376.html
	            // http://developer.android.com/reference/android/webkit/WebView.html
	            mWebView = new WebView(application.getApplicationContext());
	            mWebView.getSettings().setJavaScriptEnabled(true);
	            mWebView.setLayoutParams(params);
	            mWebView.loadUrl("file:///android_asset/adsense.html");
	            //mWebView.loadData("<html><body bgcolor=red>Hi dude<br><br>Hi again</body></html>", "text/html", "UTF-8");
	            
	            
	            mRelativeView.addView(mWebView);
	            //mRelativeView.addView(mTestButton);
            
        	}
        	
            // Create a Paint to draw the lines for our cube
            final Paint paint = mPaint;
            paint.setColor(0xffffffff);
            paint.setAntiAlias(true);
            paint.setStrokeWidth(2);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStyle(Paint.Style.STROKE);

            mStartTime = SystemClock.elapsedRealtime();
            mApplication = application;

            mPrefs = Wallpaper.this.getSharedPreferences(SHARED_PREFS_NAME, 0);
            mPrefs.registerOnSharedPreferenceChangeListener(this);
            onSharedPreferenceChanged(mPrefs, null);
            
        }
        
        public void onSharedPreferenceChanged(SharedPreferences prefs, String key) 
        {
        	mTimer = System.currentTimeMillis();
        	mCurrentIndex = 0;
        	mTimeDelay = prefs.getInt("duration", 20) * 1000;
            String path = prefs.getString("FilePreference", "/sdcard/photos/");
            boolean subfolder = prefs.getBoolean("subfolder", true);
            
            if (key != null)
            {
            	// only reload bitmaps if something that affects them changes
            	if (!key.equals("FilePreference") && !key.equals("subfolder"))
            	{
            		return;
            	}
            }
            
            try 
            {
	            BitmapFilter bitmapFilter = new BitmapFilter();
	            File currentDirectory = new File(path);
	            mFilenameList = currentDirectory.list(bitmapFilter);
	            if (mFilenameList != null && mFilenameList.length > 0)
	            {
	            	for (int i = 0; i < mFilenameList.length; ++i)
	            	{
	            		mFilenameList[i] = path + mFilenameList[i];
	            	}
	            	
	            	mBitmapLoader = new BitmapLoader(mFilenameList[mCurrentIndex]);
					mBitmapLoader.join();
		
	            	mCurrent = mBitmapLoader.mBitmap;
	            	++mCurrentIndex;
	            	
	            	if (mCurrentIndex >= mFilenameList.length)
	            		mCurrentIndex = 0;
	
	            	mBitmapLoader = new BitmapLoader(mFilenameList[mCurrentIndex]);
	            	++mCurrentIndex;
	            }
            } 
            catch (InterruptedException e)
            {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        
        void drawWallpaper(Canvas c)
        {        
        	if (mFilenameList != null && mFilenameList.length <= 0)
        		return;
        	
        	// if the time has expired, and the bitmap is loaded,
        	// start streaming the next bitmap
        	long currentTime = System.currentTimeMillis();
        	if ((currentTime - mTimer) > mTimeDelay && mBitmapLoader != null && !mBitmapLoader.isAlive())
        	{
        		mTransitionTimer = 0;
        		
        		// only reset the timer if the bitmap is valid, in the case we run out of memory loading a bitmap
        		// it is null, this code will simply load the next one without the delay
        		if (mBitmapLoader.mBitmap != null)
        		{
        			mTimer = currentTime;
        			mNext = mBitmapLoader.mBitmap;
        		}
        		
        		if (mCurrentIndex >= mFilenameList.length)
            		mCurrentIndex = 0;

        		mBitmapLoader = new BitmapLoader(mFilenameList[mCurrentIndex]);
            	++mCurrentIndex;
        	}
        	
        	float transitionPercent = (float)(currentTime - mTimer) / (float)mTransitionTime;
        	if (transitionPercent >= 1.f && mNext != null)
        	{
        		mCurrent.recycle(); // free bitmap memory
        		mCurrent = mNext;
        		mNext = null;
        	}
        	
        	if (transitionPercent > 0.f && transitionPercent < 1.f)
        	{
        		int nothing = 0;
        		nothing++;
        	}
        	/*
        	if (mNext != null && mCurrent != null)
        	{
        		
        	}*/

        	
        	
        	//c.drawColor(0xffff0000);
        	
        	float parallax = -200.f;
        	

    		
        	if (mCurrent != null)
        	{
        		mPaint.setAlpha(255);
        		
            	float width = c.getWidth();
        		float height = c.getHeight();
        		

        		float backgroundWidth =  mCurrent.getWidth();
        		float backgroundHeight =  mCurrent.getHeight();
        		
        		//float xScale = width / backgroundWidth;
        		float yScale = height / backgroundHeight;
        		
        		// mOffset is a percentage, subtract width as its 1 screen size reduced
        		float translation = -((backgroundWidth * yScale) - width) * mOffset;
        		
        		Matrix matrix = new Matrix();
        		matrix.setScale(yScale, yScale);
        		matrix.postTranslate(translation, 0.f);
        		c.drawBitmap(mCurrent, matrix, mPaint);
        	}
        	
        	if (mNext != null)
    		{
        		float width = c.getWidth();
        		float height = c.getHeight();
        		

        		float backgroundWidth =  mNext.getWidth();
        		float backgroundHeight =  mNext.getHeight();
        		
        		//float xScale = width / backgroundWidth;
        		float yScale = height / backgroundHeight;
        		
        		// mOffset is a percentage, subtract width as its 1 screen size reduced
        		float translation = -((backgroundWidth * yScale) - width) * mOffset;
        		
        		Matrix matrix = new Matrix();
        		matrix.setScale(yScale, yScale);
        		matrix.postTranslate(translation, 0.f);
        		
    			mPaint.setAlpha((int)(transitionPercent * 255.f));
    			c.drawBitmap(mNext, matrix, mPaint);
    			
    			// profiling info
            	long time2 = System.currentTimeMillis() - currentTime;
            	currentTime = System.currentTimeMillis();
    		}
        	
        	
        	// profiling info
        	long time2 = System.currentTimeMillis() - currentTime;
        	currentTime = System.currentTimeMillis();
        	
        	// draw a overlay to tint the screen
        	// 50% black
        	//float tintPercent = 0.6f;
        	if (mTintPercent > 0.f)
        	{
        		c.drawColor(Color.argb((int)(255.f * mTintPercent), 0, 0, 0));
        	}
        	
        	// profiling info
        	long time3 = System.currentTimeMillis() - currentTime;
        	
            if (!mFullVersion)
            {
	            int minWidth = getDesiredMinimumWidth();
	            int minHeight = getDesiredMinimumHeight();
	                        
	            int pageHeight = 75;
	            boolean adAtTop = true;
	            
	            {
	            	int width = c.getWidth();
	            	
	            	//mWebView.setPadding(10, 10, 10, 10); // pads the view
	            	mWebView.measure(width, minHeight); // let view calculate how big it should be such that it fits inside this view
		
		            int measuredWidth = width; //mWebView.getMeasuredWidth();
		            int measuredHeight = pageHeight; //mWebView.getMeasuredHeight();
		            int top = adAtTop ? 40 : (minHeight - measuredHeight) - 100;
		            mWebView.layout(0, top, measuredWidth, top + measuredHeight);
		            //mWebView.draw(c);
	            }
	            
	            mRelativeView.draw(c);
            }
        }


        /*
         * Draw a circle around the current touch point, if any.
         */
        void drawTouchPoint(Canvas c) 
        {
        	// do nothing
        	/*
            if (mTouchX >=0 && mTouchY >= 0) {
                c.drawCircle(mTouchX, mTouchY, 80, mPaint);
            }*/
        }

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);

            // By default we don't get touch events, so enable them.
            setTouchEventsEnabled(true);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mHandler.removeCallbacks(mDrawCube);
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            mVisible = visible;
            if (visible) {
                drawFrame();
            } else {
                mHandler.removeCallbacks(mDrawCube);
            }
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            // store the center of the surface, so we can draw the cube in the right spot
            mCenterX = width/2.0f;
            mCenterY = height/2.0f;
            drawFrame();
        }

        @Override
        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);
        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            mVisible = false;
            mHandler.removeCallbacks(mDrawCube);
        }

        @Override
        public void onOffsetsChanged(float xOffset, float yOffset,
                float xStep, float yStep, int xPixels, int yPixels) {
            mOffset = xOffset;
            drawFrame();
        }

        /*
         * Store the position of the touch event so we can use it for drawing later
         */
        @Override
        public void onTouchEvent(MotionEvent event)
        {
        	if (event.getAction() == MotionEvent.ACTION_MOVE)
        	{
        		mLastTouchEventTime = System.currentTimeMillis();
        	}
        	/*
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
            	
            	// offset based on mouse movement from last frame
            	if (mTouchX != -1)
            	{
	            	mOffsetX += (event.getX() - mTouchX) * 0.1f;
	            	mOffsetY += (event.getY() - mTouchY) * 0.1f;
            	}
            	
                mTouchX = event.getX();
                mTouchY = event.getY();
            } else {
                mTouchX = -1;
                mTouchY = -1;
            }*/
            super.onTouchEvent(event);
        }

        /*
         * Draw one frame of the animation. This method gets called repeatedly
         * by posting a delayed Runnable. You can do any drawing you want in
         * here. This example draws a wireframe cube.
         */
        void drawFrame() {
            final SurfaceHolder holder = getSurfaceHolder();

            Canvas c = null;
            try {
                c = holder.lockCanvas();
                if (c != null) {
                    // draw something
                	drawWallpaper(c);
                    drawTouchPoint(c);
                }
            } finally {
                if (c != null) holder.unlockCanvasAndPost(c);
            }

            // Reschedule the next redraw
            mHandler.removeCallbacks(mDrawCube);
            if (mVisible) 
            {
            	// up the FPS when doing transition
            	// or there was a touch event within 1 second
            	long timeSinceLastTouch = System.currentTimeMillis() - mLastTouchEventTime;
            	
            	long delay = mSleepSlow; //1000 / 10;
            	if (timeSinceLastTouch < mTouchTime && mNext != null)
            		delay = mSleepFast; //1000 / 60;
            	
                mHandler.postDelayed(mDrawCube, delay); //1000 / 25);
            }
        }


    }
}
