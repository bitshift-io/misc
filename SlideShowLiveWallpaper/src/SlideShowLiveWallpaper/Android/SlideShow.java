package SlideShowLiveWallpaper.Android;

import java.io.File;
import java.io.FilenameFilter;

import SlideShowLiveWallpaper.Android.Wallpaper.WallpaperEngine.BitmapFilter;
import SlideShowLiveWallpaper.Android.Wallpaper.WallpaperEngine.BitmapLoader;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;

public class SlideShow 
{
	class BitmapFilter implements FilenameFilter
	{
	    public boolean accept(File dir, String name)
	    {
	        return name.endsWith(".jpg") || name.endsWith(".png") || name.endsWith(".bmp");
	    }
	}
	
	class BitmapLoader extends Thread
	{
		// cant restart threads, so we just new a new thread every time
		public BitmapLoader(String filename)
		{
			try
    		{
				mBitmap = null;
				mFilename = filename;
				
				start();
    		}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		@Override
		public void run() 
		{
			try
    		{
				// first extract dimensions of image we are loading
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				mBitmap = BitmapFactory.decodeFile(mFilename, options);
				
				// calculate a scale and load the actual image
				options.inJustDecodeBounds = false;
				options.inSampleSize = Math.min(options.outHeight / MAX_HEIGHT, options.outWidth / MAX_WIDTH);
				//mBitmap = BitmapFactory.decodeFile(mFilename, options);
				Bitmap immutableBitmap = BitmapFactory.decodeFile(mFilename, options);
				
				mBitmap = immutableBitmap;
    		}
			catch (Exception e)
			{
				// out of memory most likely
				e.printStackTrace();
			}
		}
		
		String	mFilename;
		Bitmap 	mBitmap;
	}
	
	void setDuration(int duration)
	{
		mTimeDelay = duration * 1000;
	}
	
	void setPath(String path, boolean includeSubfolders)
	{
		try 
        {
            BitmapFilter bitmapFilter = new BitmapFilter();
            File currentDirectory = new File(path);
            mFilenameList = currentDirectory.list(bitmapFilter);
            if (mFilenameList != null && mFilenameList.length > 0)
            {
            	for (int i = 0; i < mFilenameList.length; ++i)
            	{
            		mFilenameList[i] = path + mFilenameList[i];
            	}
            	
            	mBitmapLoader = new BitmapLoader(mFilenameList[mCurrentIndex]);
				mBitmapLoader.join();
	
            	mCurrent = mBitmapLoader.mBitmap;
            	++mCurrentIndex;
            	
            	if (mCurrentIndex >= mFilenameList.length)
            		mCurrentIndex = 0;

            	mBitmapLoader = new BitmapLoader(mFilenameList[mCurrentIndex]);
            	++mCurrentIndex;
            }
        } 
        catch (InterruptedException e)
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void setOffset(float offset)
	{
		mOffset = offset;
	}

    void draw(Canvas c)
    {        
    	if (mFilenameList != null && mFilenameList.length <= 0)
    		return;
    	
    	// if the time has expired, and the bitmap is loaded,
    	// start streaming the next bitmap
    	long currentTime = System.currentTimeMillis();
    	if ((currentTime - mTimer) > mTimeDelay && mBitmapLoader != null && !mBitmapLoader.isAlive())
    	{
    		mTransitionTimer = 0;
    		
    		// only reset the timer if the bitmap is valid, in the case we run out of memory loading a bitmap
    		// it is null, this code will simply load the next one without the delay
    		if (mBitmapLoader.mBitmap != null)
    		{
    			mTimer = currentTime;
    			mNext = mBitmapLoader.mBitmap;
    		}
    		
    		if (mCurrentIndex >= mFilenameList.length)
        		mCurrentIndex = 0;

    		mBitmapLoader = new BitmapLoader(mFilenameList[mCurrentIndex]);
        	++mCurrentIndex;
    	}
    	
    	float transitionPercent = (float)(currentTime - mTimer) / (float)mTransitionTime;
    	if (transitionPercent >= 1.f && mNext != null)
    	{
    		mCurrent.recycle(); // free bitmap memory
    		mCurrent = mNext;
    		mNext = null;
    	}
    	
    	if (transitionPercent > 0.f && transitionPercent < 1.f)
    	{
    		int nothing = 0;
    		nothing++;
    	}
    	/*
    	if (mNext != null && mCurrent != null)
    	{
    		
    	}*/

    	
    	
    	//c.drawColor(0xffff0000);
    	
    	float parallax = -200.f;
    	

		
    	if (mCurrent != null)
    	{
    		mPaint.setAlpha(255);
    		
        	float width = c.getWidth();
    		float height = c.getHeight();
    		

    		float backgroundWidth =  mCurrent.getWidth();
    		float backgroundHeight =  mCurrent.getHeight();
    		
    		//float xScale = width / backgroundWidth;
    		float yScale = height / backgroundHeight;
    		
    		// mOffset is a percentage, subtract width as its 1 screen size reduced
    		float translation = -((backgroundWidth * yScale) - width) * mOffset;
    		
    		Matrix matrix = new Matrix();
    		matrix.setScale(yScale, yScale);
    		matrix.postTranslate(translation, 0.f);
    		c.drawBitmap(mCurrent, matrix, mPaint);
    	}
    	
    	if (mNext != null)
		{
    		float width = c.getWidth();
    		float height = c.getHeight();
    		

    		float backgroundWidth =  mNext.getWidth();
    		float backgroundHeight =  mNext.getHeight();
    		
    		//float xScale = width / backgroundWidth;
    		float yScale = height / backgroundHeight;
    		
    		// mOffset is a percentage, subtract width as its 1 screen size reduced
    		float translation = -((backgroundWidth * yScale) - width) * mOffset;
    		
    		Matrix matrix = new Matrix();
    		matrix.setScale(yScale, yScale);
    		matrix.postTranslate(translation, 0.f);
    		
			mPaint.setAlpha((int)(transitionPercent * 255.f));
			c.drawBitmap(mNext, matrix, mPaint);
			
			// profiling info
        	long time2 = System.currentTimeMillis() - currentTime;
        	currentTime = System.currentTimeMillis();
		}
    	
    	
    	// profiling info
    	long time2 = System.currentTimeMillis() - currentTime;
    	currentTime = System.currentTimeMillis();
    	
    	// draw a overlay to tint the screen
    	// 50% black
    	//float tintPercent = 0.6f;
    	if (mTintPercent > 0.f)
    	{
    		c.drawColor(Color.argb((int)(255.f * mTintPercent), 0, 0, 0));
    	}
    	
    	// profiling info
    	long time3 = System.currentTimeMillis() - currentTime;
    }
    
    Bitmap getCurrentBitmap()
    {
    	return mCurrent;
    }
	
	int 			MAX_WIDTH 			= 960;
	int 			MAX_HEIGHT 			= 800;
	
	Paint 			mPaint 				= new Paint();
	float			mOffset;
	
	BitmapLoader 	mBitmapLoader;
	
	Bitmap 			mNext;
    Bitmap 			mCurrent;
    int				mCurrentIndex;
	String[]		mFilenameList;
	
	long 			mTimer;
    long			mTimeDelay; 		// time between photos seconds
    
    float 			mTintPercent 		= 0.f;
    long 			mTransitionTime 	= 300; // transition time, transitions cause lag so make em fast
    long			mTransitionTimer 	= 0;
}
