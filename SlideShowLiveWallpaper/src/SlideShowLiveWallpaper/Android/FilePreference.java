package SlideShowLiveWallpaper.Android;

import java.io.File;
import java.net.URI;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.net.Uri;
import android.preference.DialogPreference;
import android.preference.PreferenceActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.LinearLayout;



/*
// To handle when an image is selected from the browser, add the following to your Activity
@Override
public void onActivityResult(int requestCode, int resultCode, Intent data) {
if (resultCode == RESULT_OK) {
if (requestCode == 1) {
// currImageURI is the global variable I�m using to hold the content:// URI of the image
currImageURI = data.getData();
}
}
}
// And to convert the image URI to the direct file system path of the image file
public String getRealPathFromURI(Uri contentUri) {
// can post image
String [] proj={MediaStore.Images.Media.DATA};
Cursor cursor = managedQuery( contentUri,
proj, // Which columns to return
null, // WHERE clause; which rows to return (all rows)
null, // WHERE clause selection arguments (none)
null); // Order-by clause (ascending by name)
int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
cursor.moveToFirst();
return cursor.getString(column_index);
}
*/
public class FilePreference extends DialogPreference implements View.OnClickListener
{
  private static final String androidns="http://schemas.android.com/apk/res/android";

  public static int FILE_INTENT = 12;
  
  private EditText 	mTextEdit;
  private Button	mBrowseButton;
  
  //private SeekBar mSeekBar;
  private TextView mSplashText,mValueText;
  private Context mContext;

  private String mDialogMessage, mSuffix;
  private String mDefault;
  private String mValue;
  
  private Settings mSettings;

  public FilePreference(Context context, AttributeSet attrs)
  { 
    super(context,attrs);
    mContext = context;
    
    mDialogMessage = attrs.getAttributeValue(androidns, "dialogMessage");
    mSuffix = attrs.getAttributeValue(androidns, "text");
    mDefault = attrs.getAttributeValue(androidns, "defaultValue");
  }
  
  void setSettings(Settings settings)
  {
	  mSettings = settings;
  }
  
  Settings getSettings()
  {
	  return mSettings;
  }
  
  @Override 
  protected View onCreateDialogView()
  {
    LinearLayout.LayoutParams params;
    LinearLayout layout = new LinearLayout(mContext);
    layout.setOrientation(LinearLayout.VERTICAL);
    layout.setPadding(6,6,6,6);

    params = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.FILL_PARENT, 
        LinearLayout.LayoutParams.WRAP_CONTENT);

    mBrowseButton = new Button(mContext);
    mBrowseButton.setText("Browse");
    mBrowseButton.setOnClickListener(this);
    
    mTextEdit = new EditText(mContext);
    
    if (shouldPersist())
    	mValue = this.getPersistedString(mDefault);
    
    mTextEdit.setText(mValue);
    
    layout.addView(mTextEdit, params);
    layout.addView(mBrowseButton);
    return layout;
  }
  @Override 
  protected void onBindDialogView(View v)
  {
    super.onBindDialogView(v);
    //mSeekBar.setMax(mMax);
    //mSeekBar.setProgress(mValue);
  }
  @Override
  protected void onSetInitialValue(boolean restore, Object defaultValue)  
  {
    super.onSetInitialValue(restore, defaultValue);
    //if (restore) 
    //  mValue = shouldPersist() ? getPersistedInt(mDefault) : 0;
    //else 
    //  mValue = (Integer)defaultValue;
  }

  public void onProgressChanged(SeekBar seek, int value, boolean fromTouch)
  {/*
    String t = String.valueOf(value);
    mValueText.setText(mSuffix == null ? t : t.concat(mSuffix));
    if (shouldPersist())
      persistInt(value);
    callChangeListener(new Integer(value));*/
  }
  public void onStartTrackingTouch(SeekBar seek) {}
  public void onStopTrackingTouch(SeekBar seek) {}

  public void onClick(View v)
  {
	// open up a gallery/file browser
	Intent intent = new Intent();
	intent.setType("image/*"); 
	intent.setAction(Intent.ACTION_GET_CONTENT);
	getSettings().startActivityForResult(intent, FILE_INTENT);
	getSettings().filePreferenceIntentStarted(this);
  }
  
  @Override
  public void onClick(DialogInterface dialog, int which)
  {
	  if (which == DialogInterface.BUTTON_POSITIVE)
	  {
		  String path = mTextEdit.getText().toString();
		  if (!path.endsWith("/"))
			  path += "/";
		  
		  persistString(path);
	  }
  }
  
  public boolean onActivityResult(int requestCode, int resultCode, Intent data)
  {
	  if (resultCode == Activity.RESULT_OK)
	  {
	      Uri uri = data.getData();
	      String type = data.getType();
	      
	      if (uri != null)
	      {
	    	  String path = uri.getPath();
	    	  /*
	    	  //String path = uri.toString();
	    	  //if (path.toLowerCase().startsWith("file://"))
	    	  {
	    		  // Selected file/directory path is below
	    		  path = (new File(URI.create(path))).getAbsolutePath();
	    		  //(new File(URI.create(path))).getName();
	    	  }*/
	    	  int lastSlash = path.lastIndexOf('/');
	    	  if (lastSlash != -1)
	    	  {
	    		  ++lastSlash;
	    		  path = path.substring(0, lastSlash);
	    	  }
	    	  mTextEdit.setText(path);
	      }
	  }
	  
	  return true;
  }
}
