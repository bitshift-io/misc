package SlideShowLiveWallpaper.Android;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.Toast;

// in widget.xml
// android:configure="SlideShowLiveWallpaper.Android.Settings"
public class Widget extends AppWidgetProvider
{	
	SlideShow mSlideShow;
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) 
	{
		if (mSlideShow == null)
		{
			mSlideShow = new SlideShow();
			mSlideShow.setDuration(1);
			mSlideShow.setPath("sdcard/photos/", true);
		}
		
		RemoteViews remoteViews;
        ComponentName watchWidget;
        DateFormat format = SimpleDateFormat.getTimeInstance( SimpleDateFormat.MEDIUM, Locale.getDefault() );

        remoteViews = new RemoteViews( context.getPackageName(), R.layout.widget_layout );
        watchWidget = new ComponentName( context, Widget.class );
        remoteViews.setTextViewText( R.id.widget_textview, "Time = " + format.format( new Date()));
        Bitmap bmp = mSlideShow.getCurrentBitmap();
        //remoteViews.setImageViewBitmap(R.id.widget_imageview, bmp);
        appWidgetManager.updateAppWidget( watchWidget, remoteViews );
        
        /*
		mSlideShow.setDuration(1);
		mSlideShow.setPath("sdcard/photos/", true);
		
		RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
		remoteView.setImageViewBitmap(R.id.widget_imageview, mSlideShow.getCurrentBitmap());
		
		/*
		Toast.makeText(context, "onUpdate", Toast.LENGTH_SHORT).show();
		* /
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new MyTime(context, appWidgetManager), 1, 1000);
		
		super.onUpdate(context, appWidgetManager, appWidgetIds);*/
	}
	/*
	private class MyTime extends TimerTask {
		
		RemoteViews remoteViews;
		AppWidgetManager appWidgetManager;
		ComponentName thisWidget;
		//DateFormat format = SimpleDateFormat.getTimeInstance(SimpleDateFormat.MEDIUM, Locale.getDefault());
		
		public MyTime(Context context, AppWidgetManager appWidgetManager) {
			this.appWidgetManager = appWidgetManager;
			remoteViews = new RemoteViews(context.getPackageName(), R.layout.main);
			thisWidget = new ComponentName(context, Widget.class);
		}
		
		@Override
		public void run() {
			//currentTime = new Date();
			
			remoteViews.setImageViewBitmap(R.id.widget_imageview, mSlideShow.getCurrentBitmap());
			
			remoteViews.setTextViewText(R.id.widget_textview, "Time = " + System.currentTimeMillis());
			appWidgetManager.updateAppWidget(thisWidget, remoteViews);
		}
	}
	
	@Override
	public void onReceive(Context context, Intent intent)
	{
		
		// v1.5 fix that doesn't call onDelete Action
		final String action = intent.getAction();
		if (AppWidgetManager.ACTION_APPWIDGET_DELETED.equals(action)) {
			final int appWidgetId = intent.getExtras().getInt(
					AppWidgetManager.EXTRA_APPWIDGET_ID,
					AppWidgetManager.INVALID_APPWIDGET_ID);
			if (appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
				this.onDeleted(context, new int[] { appWidgetId });
			}
		} else {
			super.onReceive(context, intent);
		}
	}
	
	@Override
	public void onDeleted(Context context, int[] appWidgetIds)
	{
		Toast.makeText(context, "onDelete", Toast.LENGTH_SHORT).show();
		super.onDeleted(context, appWidgetIds);
	}*/
}
