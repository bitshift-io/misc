--###############################################################################
--## Includes...
--## Include seperate files...
--###############################################################################
include "uv_tools_globals.ms" --globals
include "uv_tools_functions.ms" --functions
include "uv_tools_licence.ms" --licence stuff
include "uv_tools_buttons.ms" --button functions

--###############################################################################
--## Destroy Dialog & Clear listener!
--## Only allow 1 copy to run at once!
--###############################################################################
if myUVToolsRollout!= undefined do (DestroyDialog myUVToolsRollout) --close any rollout already running...
clearListener()
options.printAllElements = true -- for backing up long values!
makeDir (maxScriptDir + "\UVTools")

--###############################################################################
--## Get/SetDialog Height
--## Resizes the dialog and shiz!
--###############################################################################
fn fnGetDialogHeight = 
(
    try
    (
        myUVToolsHeight = 0
        for i = 1 to myUVToolsRollout.mySubRollout.rollouts.count do
        (
            if myUVToolsRollout.mySubRollout.rollouts[i].open == true do (myUVToolsHeight += myUVToolsRollout.mySubRollout.rollouts[i].height + 3.5)
        )
        myTempDesktopHeight = (sysInfo.DesktopSize)[2] --point2
        myTempDialogPosition = (getdialogpos myUVToolsRollout) --point2
        if (myTempDialogPosition[2] + (myUVToolsHeight + UVToolsHeight)) > (myTempDesktopHeight-80) do
        (
            myUVToolsHeight = (myTempDesktopHeight - myTempDialogPosition[2]) - 80 - UVToolsHeight
        )
        myUVToolsRollout.height =  myUVToolsHeight + UVToolsHeight
        myUVToolsRollout.mySubRollout.height = myUVToolsRollout.height + 12
    )
    catch(format "Get UVToolsHeight Error: % \n" (getCurrentException()))
)

--###############################################################################
--## Test Selection Change
--## Whew, we need this to pull of some nice shit!
--###############################################################################
fn fnGetSelectionChanged = 
(
    try
    (
        fnGetSelection()
        if myLastSubObjectLevel == subobjectlevel then
        (
            if mySelection.numberset == myLastSelection.numberset then
            (
                myDifference = #{}
                myDifference = mySelection-myLastSelection
                if myDifference.isempty == true then
                (
                    myLastSelection = mySelection
                    mySelectionChanged = False
                )
                else
                (
                    myLastSelection = mySelection
                    mySelectionChanged = True
                )
            )
            else
            (
                myLastSelection = mySelection
                mySelectionChanged = True
            ) 
            myLastSubObjectLevel = subobjectlevel             
        )
        else 
        (
            mySelectionChanged = True
            myLastSubObjectLevel = subobjectlevel
        )        
       
        if mySelectionChanged == True do
        (
            myTransformSelectionChange = true
            fnResetSpinners(false) --rest spinners but dont update
            if mySelection.numberset == 0 do
            (
                myGlobalMatID = 1
                mySecondSelection = #()
            )
            if mySelection.numberset > 0 do
            (	
                fnGetMaterialID()
            		fnGetMaterialList()
            )
            if mySelection.numberset==1 do
            (
                myFirstSelection = mySelectionArray[1]
                mySecondSelection = #()
            )
            if mySelection.Numberset==2 do
            (	
                if myFirstSelection == mySelectionArray[1] then (mySecondSelection = mySelectionArray[2])
                else (mySecondSelection = mySelectionArray[1])
            )
        )   
    )
    catch (format "Get Selection Changed Error % \n" (getCurrentException()))            
)

--###############################################################################
--## The Clock/Selection Shiz
--## Whew, we need this to pull of some nice shit!
--###############################################################################
fn fnTimer =
(
    try
    (
        undo off
        (
            myTempModifier = modPanel.getCurrentObject()
            if classof myTempModifier == Editable_Poly or classof myTempModifier == Editable_Mesh or classof myTempModifier == Unwrap_UVW then 
            (
                if subobjectlevel != 0 do
                (
                    fnGetSelectionChanged()
                )
            )
            else
            (	
                myUVToolsRollout.mySubRollout.myRolloutMaterial.dropMaterialName.items = #()
                myGlobalMatID = 1
            )
        )
    )
    catch (format "Timer Error % \n" (getCurrentException()))  
)

--###############################################################################
--## Rollouts and Shiz
--## Main UI here with buttons and stuff...
--###############################################################################
rollout myUVToolsRollout "UV Tools"
( 
  timer myClock interval:200 
  on myClock tick do(fnTimer())
  
	subrollout mySubRollout "HiddenRollout" width:(110+17)
	on myUVToolsRollout close do (setIniSetting UVToolsINI "UV Tools" "Position" ((getdialogpos myUVToolsRollout) as string))
	on myUVToolsRollout open do 
	(
		try
		(
			myVal = execute (getIniSetting UVToolsINI "UV Tools" "Position")
			print myVal
			SetDialogPos myUVToolsRollout myVal
	  )
	  catch (format "Main Rollout Error % \n" (getCurrentException())) 
	)	
)

rollout myRolloutMisc "Misc"
( 
	local uiY = 2
	local uiX = 7
	
	button btnAbout "About" width:33 height:20 pos:[uiX,uiY] tooltip:"About"
	button btnGarbageCollection "Hist" width:33 height:20 pos:[uiX+33,uiY] tooltip:"Clear History/Garbage collection"
	button btnChannelInfo "Chan" width:34 height:20 pos:[uiX+66,uiY] tooltip:"Backup UV's"
		
	button btnBackupUV "Backup" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Backup UV's"
	button btnRestoreUV "Restore" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Restore previous UV backup"
	
	on myRolloutMisc close do (setIniSetting UVToolsINI "Misc" "Rolledup" (myRolloutMisc.open as string))
	on myRolloutMisc open do (if (myVal = execute (getIniSetting UVToolsINI "Misc" "Rolledup")) != OK do myRolloutMisc.open = myVal)	
	on myRolloutMisc rolledUp X do (fnGetDialogHeight())
	
	on btnAbout pressed do (fnUIAbout())   
	on btnGarbageCollection pressed do (fnUIGarbageCollection())  
	on btnChannelInfo pressed do (fnUIChannelInfoDialog())
	
	on btnBackupUV pressed do (fnUIBackupUV())
	on btnRestoreUV pressed do (fnUIRestoreBackup())     
)
 
rollout myRolloutTransforms "Transforms"
( 
	local uiY = 2
	local uiX = 7
	
	button btnUVCopy "Copy" width:50 height:20 pos:[uiX,uiY] tooltip:"Copy UV's from selected"
	button btnUVPaste "Paste" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Paste UV's to selected"

	button btnFlipU "Flip >" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Flip selected horizontally"
	button btnFlipV "Flip ^" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Flip selected vertically"

	button btnAlignU "Align >" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Align selection U"
	button btnAlignV "Align ^" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Align selection V"	

	button btnUZero "<" width:25 height:20 pos:[uiX,uiY+=20] tooltip:"Align U to 0"
	button btnUOne ">" width:25 height:20 pos:[uiX+25,uiY] tooltip:"Align U to 1"
	button btnVZero "\/" width:25 height:20 pos:[uiX+50,uiY] tooltip:"Align V to 0"
	button btnVOne "/\ " width:25 height:20 pos:[uiX+75,uiY] tooltip:"Align V to 1"	
	
	button btnFitU "Fit >" width:33 height:20 pos:[uiX,uiY+=20] tooltip:"Normalize Selection U"
	button btnFitV "Fit ^" width:33 height:20 pos:[uiX+33,uiY] tooltip:"Normalize selection V"
	button btnFit "Fit" width:34 height:20 pos:[uiX+66,uiY] tooltip:"Normalize Selection"		

	button btnRotateRight "90� Scale" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Rotate selection right 90 degrees and keep scale"
	button btnAutoRotate "Auto Rot" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Automatic rotate to best fit"

	button btnBreak "Break" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Break UV's | Break Verts"		
	button btnBorderAlign "Border" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Border Align 0-1"	

	button btnStitch "Stitch" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Stitch selected elements"
	button btnExplode "Explode" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Explode UV Elements"
	
	button btnGizmo "Gizmo" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Create UV Gizmo on selection"
	button btnRelax "Relax" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Relax UV's"	

	on myRolloutTransforms close do (setIniSetting UVToolsINI "Transforms" "Rolledup" (myRolloutTransforms.open as string))
 	on myRolloutTransforms open do (if (myVal = execute (getIniSetting UVToolsINI "Transforms" "Rolledup")) != OK do myRolloutTransforms.open = myVal)	
  on myRolloutTransforms rolledUp X do (fnGetDialogHeight())	

	on btnUVCopy pressed do (fnUICopy())
	on btnUVPaste pressed do (fnUIPaste())
	  
  on btnFlipU pressed do (fnUIFlipU())
  on btnFlipV pressed do (fnUIFlipV())
  
  on btnAlignU pressed do (fnUIAlignU())
  on btnAlignV pressed do (fnUIAlignV())
  
  on btnUZero pressed do (fnUIUZero())
  on btnUOne pressed do (fnUIUOne())
  on btnVZero pressed do (fnUIVZero())
  on btnVOne pressed do (fnUIVOne())
  
  on btnFit pressed do (fnUIFit())
  on btnFitU pressed do (fnUIFitU())
  on btnFitV pressed do (fnUIFitV())

  on btnBreak pressed do (fnUIBreak())
  on btnBorderAlign pressed do (fnUIBorderAlign())

  on btnStitch pressed do (fnUIStitch())
  on btnExplode pressed do (fnUIExplode())
	
  on btnGizmo pressed do (fnUIGizmo())
  on btnRelax pressed do (fnUIRelax())
)
 
rollout myRolloutMapping "Mapping"
( 
	local uiY = 2
	local uiX = 7

	button btnUnfold "Unfold" width:50 height:20 pos:[uiX,uiY] tooltip:"Unfold selection"
	button btnUltimap "Quadmap" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Quadmap selection"

	button btnClear "Clear" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Clear selected UV's"		
	button btnFaceMap "FaceMap" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Facemap selected faces"	
		
	button btnBoxMap "BoxMap" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Boxmap around the Z-Axis"
	button btnWeld "Weld" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Well all UV edges"	
	
	button btnPlanarX "X" width:20 height:20 pos:[uiX,uiY+=20] tooltip:"Planarmap X"
	button btnPlanarY "Y" width:20 height:20 pos:[uiX+20,uiY] tooltip:"Planarmap Y"
	button btnPlanarZ "Z" width:20 height:20 pos:[uiX+40,uiY] tooltip:"Planarmap Z"
	button btnPlanarView "V" width:20 height:20 pos:[uiX+60,uiY] tooltip:"Planarmap View Align"
	button btnPlanarNormal"N" width:20 height:20 pos:[uiX+80,uiY] tooltip:"Planarmap Normal Align"	

	spinner spinFlattenMapAngle "" width:50 height:20 type:#integer range:[0,180,45]  pos:[uiX,uiY+=20] 	
	button btnFlattenMap "Flatten" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Flatten Map by angle"
		
	button btnUVMap "UV Map" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"UV Map modifider"
	button btnPickPoint "Point UV" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Tweak Individual UV by Vertex"

	button btnUVXform "UV Xform" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"UV Xform modifier"
	button btnQuickPelt "Q-Pelt" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Quick Pelt Brings up the pelt map tool"

	button btnNurbs "Nurbs" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Nurbs Projection Mapping"
	button btnSpline "Spline" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Spline Projection Mapping"
     	

	pickbutton pickMesh "Mesh" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Mesh Projection Mapping"
	button btnUnwrap "Edit UV's" width:50 height:20 pos:[uiX+50,uiY] tooltip:"UVW Unwrap Modifier"	

	on myRolloutMapping close do (setIniSetting UVToolsINI "Mapping" "Rolledup" (myRolloutMapping.open as string))
 	on myRolloutMapping open do (if (myVal = execute (getIniSetting UVToolsINI "Mapping" "Rolledup")) != OK do myRolloutMapping.open = myVal)	
	on myRolloutMapping rolledUp X do (fnGetDialogHeight())	
	
	on btnUnwrap pressed do (fnUVWUnwrap())
	
	on pickMesh picked myVal do (fnMeshProjection(myVal))
)

rollout myRolloutSelection "Selection"
( 
	local uiY = 2
	local uiX = 7
	
	spinner spinSelectAngle "" width:50 height:20 type:#integer range:[0,180,30]  pos:[uiX,uiY] 	
	button btnSelectByAngle "Angle" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Select By Angle"	

	spinner spinSelectArea "" width:50 height:20 type:#integer range:[0,10000,100]  pos:[uiX,uiY+=20] 	
	button btnSelectByArea "Area" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Select By Polygon Area"	
	
  button btnExpand "Expand" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Expand UV selection"
	button btnElement "Element" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Select UV element"
	
	on spinSelectArea changed myVal do (fnSelecyByArea())
	on btnSelectByArea pressed do (fnSelecyByArea())
	
  on myRolloutSelection close do (setIniSetting UVToolsINI "Selection" "Rolledup" (myRolloutSelection.open as string))
 	on myRolloutSelection open do (if (myVal = execute (getIniSetting UVToolsINI "Selection" "Rolledup")) != OK do myRolloutSelection.open = myVal)	
	on myRolloutSelection rolledUp X do (fnGetDialogHeight())	
)

rollout myRolloutModifiers "Modifiers"
( 
	local uiY = 2
	local uiX = 7

  label lblU "  U" pos:[uiX+15,uiY]
  label lblV "  V" pos:[uiX+65,uiY]      
	spinner spinUTile ">" width:48 type:#float scale:0.001 range:[-100,100,1]  pos:[uiX,uiY+=20] 
	spinner spinVTile "^" width:48 type:#float scale:0.001 range:[-100,100,1]  pos:[uiX+52,uiY-=2] 

	spinner spinUOffset ">" width:48 type:#float scale:0.0001 range:[-9,9,0]  pos:[uiX,uiY+=25] 
	spinner spinVOffset "^" width:48 type:#float scale:0.0001 range:[-9,9,0]  pos:[uiX+52,uiY] 

  spinner spinUValue ">" width:48 type:#float scale:0.001 range:[-100,100,0]  pos:[uiX,uiY+=25] indeterminate:true
  spinner spinVValue "^" width:48 type:#float scale:0.001 range:[-100,100,0]  pos:[uiX+52,uiY-=2] indeterminate:true
  
  label lblUVRot "Rotate:" pos:[uiX+60,uiY+=25]    
  spinner spinUVRotate "" width:40 type:#integer scale:0.1 range:[-360,360,00]  pos:[uiX+60,uiY+=20] 
  
  button btnResetModifiers "Reset" width:40 height:20 pos:[uiX+60,uiY += 25] tooltip:"Reset Spinners"		
  
  groupBox grpBox1 pos:[uiX+7,uiY-=37] width:20 height:25
  groupBox grpBox2 pos:[uiX+26,uiY] width:20 height:25	
  groupBox grpBox3 pos:[uiX+7,uiY+=18] width:20 height:25
  groupBox grpBox4 pos:[uiX+26,uiY] width:20 height:25
  
  checkbutton btnPivotTopLeft "" width:15 height:15 pos:[uiX,uiY-=20] tooltip:"Pivot"
  checkbutton btnPivotTopMiddle "" width:15 height:15 pos:[uiX+20,uiY] tooltip:"Pivot"
  checkbutton btnPivotTopRight "" width:15 height:15 pos:[uiX+40,uiY] tooltip:"Pivot"     
  
  checkbutton btnPivotMiddleLeft "" width:15 height:15 pos:[uiX,uiY+=20] tooltip:"Pivot"
  checkbutton btnPivotMiddleMiddle "" width:15 height:15 pos:[uiX+20,uiY] tooltip:"Pivot" checked:true
  checkbutton btnPivotMiddleRight "" width:15 height:15 pos:[uiX+40,uiY] tooltip:"Pivot"         	
  
  checkbutton btnPivotBottomLeft "" width:15 height:15 pos:[uiX,uiY+=20] tooltip:"Pivot"
  checkbutton btnPivotBottomMiddle "" width:15 height:15 pos:[uiX+20,uiY] tooltip:"Pivot"
  checkbutton btnPivotBottomRight "" width:15 height:15 pos:[uiX+40,uiY] tooltip:"Pivot"   
  
  on myRolloutModifiers close do (setIniSetting UVToolsINI "Modifiers" "Rolledup" (myRolloutModifiers.open as string))
  on myRolloutModifiers open do (if (myVal = execute (getIniSetting UVToolsINI "Modifiers" "Rolledup")) != OK do myRolloutModifiers.open = myVal)	
  on myRolloutModifiers rolledUp X do (fnGetDialogHeight())	
  
  on btnPivotTopLeft changed myVal do (fnTrasformGrid("btnPivotTopLeft"))
  on btnPivotTopMiddle changed myVal do (fnTrasformGrid("btnPivotTopMiddle"))
  on btnPivotTopRight changed myVal do (fnTrasformGrid("btnPivotTopRight"))
  
  on btnPivotMiddleLeft changed myVal do (fnTrasformGrid("btnPivotMiddleLeft"))
  on btnPivotMiddleMiddle changed myVal do (fnTrasformGrid("btnPivotMiddleMiddle"))
  on btnPivotMiddleRight changed myVal do (fnTrasformGrid("btnPivotMiddleRight"))
  
  on btnPivotBottomLeft changed myVal do (fnTrasformGrid("btnPivotBottomLeft"))
  on btnPivotBottomMiddle changed myVal do (fnTrasformGrid("btnPivotBottomMiddle"))
  on btnPivotBottomRight changed myVal do (fnTrasformGrid("btnPivotBottomRight")) 
  
  on spinUTile changed myVal do (fnTransformSpinners()) 
  on spinVTile changed myVal do (fnTransformSpinners()) 
  
  on spinUOffset changed myVal do (fnTransformSpinners()) 
  on spinVOffset changed myVal do(fnTransformSpinners()) 
  
  on spinUValue changed myVal do (fnTransformSpinners()) 
  on spinVValue changed myVal do (fnTransformSpinners())  
  
  on spinUVRotate changed myVal do (fnTransformSpinners())
  on btnResetModifiers pressed do (fnResetSpinners("true"))
    
)

rollout myRolloutMaterial "Material"
( 
	local uiY = 2
	local uiX = 7

	checkbutton btnVertexColors "Vert Col" width:50 height:20 pos:[uiX,uiY] tooltip:"Toggle Vertex Colours on/off"
	checkbutton btnMaterialColors "Obj Col" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Toggle Material/Object Colors"

	button btnUnusedMaterials "Unused" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Unused/Used Material ID's"
	button btnMaterialEdit "Mat Edit" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Edit Material"
	
	bitmap bmpBitmap "Texture Preview" width:110 height:110 fileName:(maxScriptDir + "\mypreview.bmp") color:gray pos:[uiX-5,uiY+=20]
	
	dropdownlist dropMaterialName "" width:90 height:40 pos:[uiX,uiY+=110] items:#(".................................................")
	button btnMaterialApply ">" width:10 height:20 pos:[uiX+90,uiY]
	checkbox chkUpdateTexture "Update Texture" pos:[uiX,uiY+=20] checked:true	
	
	spinner spinMaterialID "" width:50 height:20 type:#integer range:[1,999,1]  pos:[uiX,uiY+=15] 
	button btnSelectByID "Mat ID" width:50 height:16 pos:[uiX+50,uiY] tooltip:"Select by Material Id"
	spinner spinMapChannel "" width:50 height:16 type:#integer range:[-2,99,1]  pos:[uiX,uiY+=20] 
	label lblChannel " Channel" pos:[uiX+50,uiY] 
	
	on myRolloutMaterial close do (setIniSetting UVToolsINI "Material" "Rolledup" (myRolloutMaterial.open as string))
 	on myRolloutMaterial open do (if (myVal = execute (getIniSetting UVToolsINI "Material" "Rolledup")) != OK do myRolloutMaterial.open = myVal)	
	on myRolloutMaterial rolledUp X do (fnGetDialogHeight())	
	
	on btnVertexColors changed myVal do (fnVertexColors(myVal))
	on btnMaterialColors changed myVal do (fnMaterialColors(myVal))	

	on btnMaterialEdit pressed do (fnMaterialEdit())
)

rollout myRolloutGeometry "Geometry Tools"
( 
	local uiY = 2
	local uiX = 7
	
	button btnMirrorX "mir X" width:33 height:20 pos:[uiX,uiY] tooltip:"Mirror X"
	button btnMirrorY "mir Y" width:33 height:20 pos:[uiX+33,uiY] tooltip:"Mirror Y"
	button btnMirrorZ "mir Z" width:34 height:20 pos:[uiX+66,uiY] tooltip:"Mirror Z"

	button btnCenterPivot "Center" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Center Pivot"
	button btnGroundPivot "Ground" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Ground Pivot"
	
	button btnAlignCamera "AlignCam" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Align Camera to face normal"	
	dropdownlist dropUnhideID width:51 height:17 items:#() pos:[uiX+50,uiY]
     	
	on myRolloutGeometry close do (setIniSetting UVToolsINI "Geometry Tools" "Rolledup" (myRolloutGeometry.open as string))
 	on myRolloutGeometry open do (if (myVal = execute (getIniSetting UVToolsINI "Geometry Tools" "Rolledup")) != OK do myRolloutGeometry.open = myVal)	
	on myRolloutGeometry rolledUp X do (fnGetDialogHeight())	
	
	on btnMirrorX pressed do (fnMirrorGeometry([-1,1,1]))
	on btnMirrorY pressed do (fnMirrorGeometry([1,-1,1]))
	on btnMirrorZ pressed do (fnMirrorGeometry([1,1,-1]))
	
	on btnCenterPivot pressed do (fnMovePivot("center"))
	on btnGroundPivot pressed do (fnMovePivot("ground"))
      	
	on btnAlignCamera pressed do (fnAlignCamera())
	on dropUnhideID selected myVal do (fnUnhideID(MyVal))
)

rollout myRolloutVertex "Vertex Tools"
( 
	local uiY = 2
	local uiX = 7
	
	checkbutton chkCompareVC "Compare" width:50 height:20 pos:[uiX,uiY] tooltip:"Difrence between vertex colors and changes"
	button btnRestoreVC "Restore" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Restore Vertex color changes from difrence"

	button btnAnimateVC "Animate" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Animate VertexColors/lighting/radiosity"
	button btnAssignVC "Bake" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Assign/Bake Vertex Colors"

  on btnAssignVC pressed do (fnAssignVertexColors())
	
	on myRolloutVertex close do (setIniSetting UVToolsINI "Vertex Tools" "Rolledup" (myRolloutVertex.open as string))
 	on myRolloutVertex open do (if (myVal = execute (getIniSetting UVToolsINI "Vertex Tools" "Rolledup")) != OK do myRolloutVertex.open = myVal)
	on myRolloutVertex rolledUp X do (fnGetDialogHeight())	
)

--###############################################################################
--## Create Dialogs & Rollouts
--## These put the UI onscreen!
--###############################################################################
createDialog myUVToolsRollout width:110 height:UVToolsHeight style:#(#style_titlebar,#style_sysmenu)

AddSubRollout myUVToolsRollout.mySubRollout myRolloutMisc
AddSubRollout myUVToolsRollout.mySubRollout myRolloutTransforms
AddSubRollout myUVToolsRollout.mySubRollout myRolloutMapping
AddSubRollout myUVToolsRollout.mySubRollout myRolloutSelection
AddSubRollout myUVToolsRollout.mySubRollout myRolloutModifiers
AddSubRollout myUVToolsRollout.mySubRollout myRolloutMaterial
AddSubRollout myUVToolsRollout.mySubRollout myRolloutGeometry
AddSubRollout myUVToolsRollout.mySubRollout myRolloutVertex

myUVToolsRollout.mySubRollout.height = myUVToolsRollout.height + 12
myUVToolsRollout.mySubRollout.width = myUVToolsRollout.width + 14
myUVToolsRollout.mySubRollout.pos = [-7,-5]

fnGetDialogHeight()
myUVToolsRollout.mySubRollout.myRolloutMaterial.dropMaterialName.items = #()
