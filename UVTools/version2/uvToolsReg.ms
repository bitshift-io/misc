mySourceFile = "C:\\export_uvtools.txt"
myOutputPath = "C:\\"
myLine = #()
myUserEmails = #()
myOrderID = #()

-- read in source file(s)(6lines)
myOpen = openFile mySourceFile

i = 1
while not eof myOpen do 
(
	MyLine[i] = ReadLine myOpen

	if i == 6 then --output to file
	(
		--put into orderID
		append myOrderID myLine[6]
		
		--make dir/txt file
		makeDir (myOutputPath + myLine[5])
		myLicenceOutput = createFile (myOutputPath + myLine[5] + "\licence.txt")
		
		--append users to a list
		append myUserEmails myLine[5]
		
		--write to txt file
		for k = 1 to 5 do
		(
			format "%\n" myLine[k] to:myLicenceOutput
		)
		
		--close txt file
		close myLicenceOutput
		
		--encrypt txt file
		encryptFile (myOutputPath + myLine[5] + "\licence.txt") (myOutputPath + myLine[5] + "\licence.dat") 957487354987456987248656509
		
		--delete txt file
		deleteFile (myOutputPath + myLine[5] + "\licence.txt")
		
		--set i to 1 for next section
		i = 1
	)
	else (i+=1)
)

-- now we write all the licence to one file for fabs php....
myLicencesCombined = createFile (myOutputPath + "\LicencesCombined.txt")

for i = 1 to myUserEmails.count do 
(
	format "%\n" myUserEmails[i] to:myLicencesCombined
	format "%\n" myOrderID[i] to:myLicencesCombined
	
	myUser = openFile (myOutputPath + myUserEmails[i] + "\licence.dat")

	while not eof myUser do
	(
		myLine = readLine myUser
		format "%\n" myLine to:myLicencesCombined
	) 
	close myUser	
)

close myLicencesCombined