--###############################################################################
--## Define Globals
--## Without Globals we cany use functions here there and everywhere!
--###############################################################################

--rollouts
global myUVToolsRollout

--uv tools ui
global UVToolsVersion = 2.0
global UVToolsHeight = (20*8)--number of rollouts

--path stuff
global maxScriptDir = getdir #scripts
global UVToolsINI = ((getdir #scripts) + "\UVTools\uvToolsSettings.ini")

--uvtools 2.0 globals
global myModifier
global myGlobalSelection
global myObj
global mySelection = #{}
global mySelectionArray = #()

global myTransformSelectionChange = true

--fngettextureverts
global myTextureVertexSelection = #()
global myTextureVertexPosition = #()
global myTextureVertexPositionU = #()
global myTextureVertexPositionV = #()
global myTextureVertexPositionW = #()

--fngetuvwmatrix
global myUVWMatrixArray
global myULength
global myVLength
global myWLength
global myUVWCenter
global myUVWTopLeft
global myUVWTopMiddle
global myUVWTopRight
global myUVWMiddleLeft
global myUVWMiddleMiddle
global myUVWMiddleRight
global myUVWBottomLeft
global myUVWBottomMiddle
global myUVWBottomRight
global myUVWCenterMatrix
global myUVWTopLeftMatrix
global myUVWTopMiddleMatrix
global myUVWTopRightMatrix
global myUVWMiddleLeftMatrix
global myUVWMiddleMiddleMatrix
global myUVWMiddleRightMatrix
global myUVWBottomLeftMatrix
global myUVWBottomMiddleMatrix
global myUVWBottomRightMatrix

--grid buttons
global myGridButtons = #("btnPivotTopLeft","btnPivotTopMiddle","btnPivotTopRight","btnPivotMiddleLeft","btnPivotMiddleMiddle","btnPivotMiddleRight","btnPivotBottomLeft","btnPivotBottomMiddle","btnPivotBottomRight")  
global myCurrentGridButton = 5 --center button

global myGlobalMapChannel = 1

global myAverageNormal = [0,0,0]

--backup timer
global myNumberBackups = 5
global myBackupIncrement = myNumberBackups
global myRestoreIncrement = 0
global myBackupPressed = true

--settings for edit poly
global SettingsCoordSystem
global SettingsByVertex
global SettingsIgnoreBackfacing
global SettingsSelectByAngle
global SettingsSelectAngle
global SettingsSubObjectLevel
global SettingsVertexSelection
global SettingsEdgeSelection
global SettingsFaceSelection	
global SettingsAutoSmoothThreshold
global SettingsPreserveUV

--timer stuff
global myLastSubObjectLevel = 0
global myLastSelection = #{}
global myFirstSelection = #()
global mySecondSelection = #()
global mySelectionChanged = False
global myGlobalMatID = 1 -- sets id when copy

--old globals(needs cleaning!)
--lic stuff
--local myLicPath = "//Nw-proj2/projects/Hellboy/Graphics/Max scripts/"
global myLicPath = ""
global myLicenceType = "single"
global myLicenceChecked = false
global myCompany = "Krome Studios"
global myDemoTimer = 0
global myLicencedCopy = false
global myDemoTime = 6000 --time for demo mode...




global myGlobalMatIDCopy --only used for copy/paste 

global myMaterialList = #() --define array


global myObj
global myMaterial = #()
global myMaterialListName = #()
global myMaterialList = #()
global myNumMaterials

global myTVVertexSelection
global myVertexPositionU = #()
global myVertexPositionV = #()

global myBeforeCompareArray = #()

--stuff for matid dialog
global myUnusedMaterialIDs
global myUsedMaterialIDs
