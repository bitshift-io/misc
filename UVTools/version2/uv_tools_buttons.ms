--###############################################################################
--## Misc Rollout Buttons
--## Main UI here with buttons and stuff...
--###############################################################################

fn fnUIAbout = 
(
	rollout myAboutRollout "About UV Tools"
	(
    label labelVersion "UV Tools 2.0"
    label labelName "\xa9 Bronson Mathews 2005-2006"
    edittext labelmaxid "ID:" text:(hardwareLockID as string) readOnly:true
    label labelWeb "http://www.blackcarbon.net"
    label labelContact "support@blackcarbon.net"
    button btnURL "Buy UV Tools"
    on btnURL pressed do (ShellLaunch "explorer.exe" ("\"http://www.blackcarbon.net/uvtools/prepurchase.php?maxId=" + (hardwareLockID as string)+ "\""))
	)
	createDialog myAboutRollout width:160 height:125
)

fn fnUIGarbageCollection = 
(
  try
  (
    clearUndoBuffer()
    myTempFreeMem = gc()
    format "Cleared % MB Memory \n" ((myTempFreeMem/1024/1024)-5)     
  )
  catch (format "Garbage Collection Error % \n" (getCurrentException()))
)

fn fnUIChannelInfoDialog =
(
	channelInfo.Dialog()
)

fn fnUIBackupUV =
(
	fnBackupUV()
)

fn fnUIRestoreUV =
(
	fnRestoreUV()
) 

--###############################################################################
--## Transforms Rollout Buttons
--## Main UI here with buttons and stuff...
--###############################################################################

fn fnUIFlipU =
(
	fnFlip([-1,1,1])
)

fn fnUIFlipV = 
(
	fnFlip([1,-1,1])
)

fn fnFit =
(
	fnFit("UV")
)

fn fnFitU = 
(
	fnFit("U")
)

fn fnFitV =
(
	fnFit("V")
)

fn fnUIBreak =
(
	fnBreak()
)

fn fnUIAlignU =
(
)

fn fnUIAlignV =
(
)

fn fnUIUZero =
(
)

fn fnUIUOne =
(
)

fn fnUIVZero =
(
)

fn fnUIVOne =
(
)

fn fnUIBorderAlign =
(
)

fn fnUIStitch =
(
)

fn fnUIExplode =
(
)

fn fnUIGizmo =
(
)

fn fnUIRelax =
(
)
