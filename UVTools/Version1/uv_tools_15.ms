-- ## SETUP UI ##--
rollout rolloutTransforms "Transforms"
(
	on rolloutTransforms rolledUp myState do
	(
  		if myState then 
		(
			rolloutUVTools.size = rolloutUVTools.size + [0,115]
		)
		else 
		(
			rolloutUVTools.size = rolloutUVTools.size - [0,115]
		)
	)
	
	local uiY = 0
	local uiX = 1
	
	button myBackupUV "Backup" width:50 height:20 pos:[uiX,uiY+=5] tooltip:"Backup UV's"
	button myRetriveBackup "Retrive" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Retrive previous backup"
	button myUVCopy "Copy" width:50 height:20 pos:[uiX+100,uiY] tooltip:"Copy UV's from selected"
	button myUVPaste "Paste" width:50 height:20 pos:[uiX+150,uiY] tooltip:"Paste UV's to selected"
	
	button myFlipH "Flip >" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Flip selected horizontally"
	button myFlipV "Flip ^" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Flip selected vertically"
	button myAlignU "Align >" width:50 height:20 pos:[uiX+100,uiY] tooltip:"Align selection U"
	button myAlignV "Align ^" width:50 height:20 pos:[uiX+150,uiY] tooltip:"Align selection V"	
	
	button myUZero "<" width:25 height:20 pos:[uiX,uiY+=20] tooltip:"Align U to 0"
	button myUOne ">" width:25 height:20 pos:[uiX+25,uiY] tooltip:"Align U to 1"
	button myVZero "\/" width:25 height:20 pos:[uiX+50,uiY] tooltip:"Align V to 0"
	button myVOne "/\ " width:25 height:20 pos:[uiX+75,uiY] tooltip:"Align V to 1"	
	
	button myFitU "Fit >" width:33 height:20 pos:[uiX+100,uiY] tooltip:"Normalize Selection U"
	button myFitV "Fit ^" width:33 height:20 pos:[uiX+133,uiY] tooltip:"Normalize selection V"
	button myFit "Fit" width:34 height:20 pos:[uiX+166,uiY] tooltip:"Normalize Selection"		
	
	button myRotateRight "90� Scale" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Rotate selection right 90 degrees and keep scale"
	button myRelax "Relax" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Relax UV's"
	button myBreak "Break" width:50 height:20 pos:[uiX+100,uiY] tooltip:"Break UV's | Break Verts"		
	button myBorderAlign "Border" width:50 height:20 pos:[uiX+150,uiY] tooltip:"Border Align 0-1"	
	
	button myStitch "Stitch" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Stitch selected elements"
	button myExplode "Explode" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Explode UV Elements"
	button myClear "Clear" width:50 height:20 pos:[uiX+100,uiY] tooltip:"Clear selected UV's"		
	button myWeld "Weld" width:50 height:20 pos:[uiX+150,uiY] tooltip:"Well all UV edges"
)

rollout rolloutMapping "Mapping"
(
	on rolloutMapping rolledUp myState do
	(
  		if myState then 
		(
			rolloutUVTools.size = rolloutUVTools.size + [0,95]
		)
		else 
		(
			rolloutUVTools.size = rolloutUVTools.size - [0,95]
		)
	)
	
	local uiY = 0	
	local uiX = 1

	button myUnfold "Unfold" width:50 height:20 pos:[uiX,uiY+=5] tooltip:"Unfold selection"
	button myUltimap "Quadmap" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Quadmap selection"
	button myBoxMap "BoxMap" width:50 height:20 pos:[uiX+100,uiY] tooltip:"Boxmap around the Z-Axis"		
	button myFaceMap "FaceMap" width:50 height:20 pos:[uiX+150,uiY] tooltip:"Facemap selected faces"
	
	button myPlanarView "View"  width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Planarmap viewalign"		
	button myPlanarX "X" width:33 height:20 pos:[uiX+100,uiY] tooltip:"Planarmap X"
	button myPlanarY "Y" width:33 height:20 pos:[uiX+133,uiY] tooltip:"Planarmap Y"
	button myPlanarZ "Z" width:34 height:20 pos:[uiX+166,uiY] tooltip:"Planarmap Z"
	
	spinner myFlattenMapAngle "" width:50 type:#integer range:[0,180,45]  pos:[uiX+100,uiY+=20] 	
	button myFlattenMap "Flatten" width:50 height:20 pos:[uiX+150,uiY] tooltip:"Flatten Map by angle"
	
	
	button myUVXform "UV Xform" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"UV Xform modifier"
	button myUnwrap "Edit UV's" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Unwrap Dialog/Modifier"	

	button myUVMap "UV Map" width:50 height:20 pos:[uiX+150,uiY] tooltip:"UV Map modifider"	
	
)

rollout rolloutSelection "Selection"
(
	on rolloutSelection rolledUp myState do
	(
  		if myState then 
		(
			rolloutUVTools.size = rolloutUVTools.size + [0,25]
		)
		else 
		(
			rolloutUVTools.size = rolloutUVTools.size - [0,25]
		)
	)
	
	local uiY = 0	
	local uiX = 1
	
	button myElement "Element" width:50 height:20 pos:[uiX,uiY+=5] tooltip:"Select UV element"
	spinner mySelectAngle "" width:50 type:#integer range:[0,180,30]  pos:[uiX+50,uiY] 	
	button mySelectByAngle "Angle" width:50 height:20 pos:[uiX+100,uiY] tooltip:"Select By Angle"	
	dropdownlist myUnhideID width:50 height:17 items:#() pos:[uiX+150,uiY]
	
)

rollout rolloutModifiers "Modifiers"
(
	on rolloutModifiers rolledUp myState do
	(
  		if myState then 
		(
			rolloutUVTools.size = rolloutUVTools.size + [0,55]
		)
		else 
		(
			rolloutUVTools.size = rolloutUVTools.size - [0,55]
		)
	)
	
	local uiY = 0
	local uiX = 1
		
	label lab1 "Tile >"	pos:[uiX+15,uiY+=5] 
	spinner myUTile "" width:50 type:#float scale:0.001 range:[-100,100,1]  pos:[uiX+50,uiY] 
	label lab2 "Tile ^" pos:[uiX+115,uiY] 
	spinner myVTile "" width:50 type:#float scale:0.001 range:[-100,100,1]  pos:[uiX+150,uiY] 

	label lab3 "Offset >" pos:[uiX+5,uiY+=20] 
	spinner myUOffset "" width:50 type:#float scale:0.0001 range:[-9,9,0]  pos:[uiX+50,uiY] 
	label lab4 "Offset ^" pos:[uiX+105,uiY] 
	spinner myVOffset "" width:50 type:#float scale:0.0001 range:[-9,9,0]  pos:[uiX+150,uiY] 
)

rollout rolloutMaterial "Material"
(
	on rolloutMaterial rolledUp myState do
	(
  		if myState then 
		(
			rolloutUVTools.size = rolloutUVTools.size + [0,160]
		)
		else 
		(
			rolloutUVTools.size = rolloutUVTools.size - [0,160]
		)
	)
	
	local uiY = 0
	local uiX = 1
	
	dropdownlist myMaterialName "" width:150 pos:[uiX,uiY+=5] items:#("")
	button myMaterialApply "Apply" width:50 height:20 pos:[uiX+150,uiY]
	checkbox myUpdateTexture "Update Texture" pos:[uiX,uiY+=20] checked:true	

	bitmap myBitmap "" width:110 height:110 color:gray pos:[uiX,uiY+=20]
	
	spinner myMaterialID "" width:40 type:#integer range:[1,999,1]  pos:[uiX+110,uiY] 
	button mySelectByID "Mat ID" width:50 height:16 pos:[uiX+150,uiY] tooltip:"Select by Material Id"

	label temp2 " Channel" pos:[uiX+110,uiY+=20] 
	spinner myMapChannel "" width:40 type:#integer range:[1,99,1]  pos:[uiX+160,uiY] 
		

	button myMaterialEdit "Material Edit" width:90 height:20 pos:[uiX+110,uiY+=20] tooltip:"Edit Material"	
	button myUnusedMaterials "Unused Materials" width:90 height:20 pos:[uiX+110,uiY+=20] tooltip:"Unused/Used Material ID's"

)	
	
rollout rolloutVertexTools "Vertex Tools"
(
	on rolloutVertexTools rolledUp myState do
	(
  		if myState then 
		(
			rolloutUVTools.size = rolloutUVTools.size + [0,35]
		)
		else 
		(
			rolloutUVTools.size = rolloutUVTools.size - [0,35]
		)
	)
	
	local uiY = 0
	local uiX = 1
	
	button myAnimateVC "Animate" width:50 height:20 pos:[uiX,uiY+=5] tooltip:"Animate VertexColors/lighting/radiosity"
	button myPickPoint "Point UV" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Tweak Individual UV by Vertex"
	checkbutton myCompareVC "Compare" width:50 height:20 pos:[uiX+100,uiY] tooltip:"Difrence between vertex colors and changes"
	button myRestoreVC "Restore" width:50 height:20 pos:[uiX+150,uiY] tooltip:"Restore Vertex color changes from difrence"
)

rollout rolloutGeometryTools "Geometry Tools"
(
	on rolloutGeometryTools rolledUp myState do
	(
  		if myState then 
		(
			rolloutUVTools.size = rolloutUVTools.size + [0,55]
		)
		else 
		(
			rolloutUVTools.size = rolloutUVTools.size - [0,55]
		)
	)
	
	local uiY = 0
	local uiX = 1

	
	button myMirrorX "Mirror X" width:50 height:20 pos:[uiX,uiY+=5] tooltip:"Mirror X"
	button myMirrorY "Mirror Y" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Mirror Y"
	button myMirrorZ "Mirror Z" width:50 height:20 pos:[uiX+100,uiY] tooltip:"Mirror Z"

	button myCenterPivot "Center" width:50 height:20 pos:[uiX,uiY+=20] tooltip:"Center Pivot"
	button myGroundPivot "Ground" width:50 height:20 pos:[uiX+50,uiY] tooltip:"Ground Pivot"
)
 
-- ## OPEN ROLLOUTS ##--
rolloutUVTools = newRolloutFloater "UV Tools 1.5" 218 710
		
addRollout rolloutTransforms rolloutUVTools
addRollout rolloutMapping rolloutUVTools
addRollout rolloutSelection rolloutUVTools
addRollout rolloutModifiers rolloutUVTools
addRollout rolloutMaterial rolloutUVTools
addRollout rolloutVertexTools rolloutUVTools
addRollout rolloutGeometryTools rolloutUVTools

-- ## CODE FROM HERE?? ##--


--save to ini on close, read form ini on load
--print rolloutUVTools.pos
--print rolloutUVTools.size
