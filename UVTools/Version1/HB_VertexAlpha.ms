-- enables vertex alpha on standard material!
-- Bronson Mathews 06

myMEditSelected = medit.GetActiveMtlSlot()
meditMaterials[myMEditSelected].opacityMap = Gradient_Ramp ()
meditMaterials[myMEditSelected].opacitymap.coordinates.U_Offset = 0.01
meditMaterials[myMEditSelected].opacitymap.coordinates.V_Offset = 0.01
meditMaterials[myMEditSelected].opacitymap.coordinates.U_Tiling = 0.9
meditMaterials[myMEditSelected].opacitymap.coordinates.V_Tiling = 0.9
meditMaterials[myMEditSelected].opacityMap.coordinates.mapchannel = -2
