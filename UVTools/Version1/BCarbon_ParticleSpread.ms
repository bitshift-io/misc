rollout myRollout "Particle Spread"
(
	--button myCameraButton "Camera"
	button myParticleButton "Particle"
	button myTargetButton "Targets"
	button myGoButton "Go"
	
	local myCamera = #()
	local myParticle = #()
	local myTargets = #()

	on myCameraButton Pressed do
	(
		myCamera = getcurrentselection()
		myCamera= myCamera[1]
	)

	on myParticleButton Pressed do
	(
		myParticle = getcurrentselection()
		myParticle= myParticle[1]
	)

	on myTargetButton Pressed do
	(
		myTargets = getcurrentselection()
	)	
	
	on myGoButton Pressed do
	(
		for i = 1 to myTargets.count do
		(
		 	myObj = myTargets[i] --select the target
			myNumVerts = polyop.getnumverts myObj
			for z = 1 to myNumVerts do
			(
				myVertPos = polyOp.getVert myObj z --get vertex position
				
				myLinkedFaces = polyOp.getFacesUsingVert myObj z
				myLinkedFaces = myLinkedFaces as array
				myFace = myLinkedFaces[1] --get face to conver to uv
				print myFace
				
				myUVVerts = polyOp.getMapFace myObj 0 myFace
				myUVVerts = myUVVerts as array
				
				myVerts = polyOp.getVertsUsingFace myObj myFace
				myVerts = myVerts as array
				
				myIndex = finditem myVerts z
				print myIndex
				
				myVertCol = polyOp.getMapVert myObj 0 myUVVerts[myIndex]
				--myVertCol = polyOp.getMapVert myObj 0 z --get vertex color
				
				--clone mesh, move, and camera align
				myCopy = copy myParticle
				myCopy.pos = myVertPos
				myCopy.showVertexColors = on
				
				--color
				polyOp.defaultMapFaces myCopy 0
				myNumVertsCopy = polyOp.getNumVerts myCopy
				
				polyOp.defaultMapFaces myCopy 0
				
				for k = 1 to myNumVertsCopy do
				(
					if z==53 do (print (myVertCol * 255))
					polyop.setVertColor myCopy 0 k (myVertCol * 255)
				)
			)		
		)
	)		
	
)
createDialog myRollout "Particle Spread" width:110 height:120