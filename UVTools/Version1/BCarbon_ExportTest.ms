--print exporterPlugin.classes

myToolsPath = "D:\BlackCarbon\Tool\Binary\SceneViewer.exe" as string
myExportPath = "c:\Temp\\" as string
myMaxSceneName = maxfileName as string
myMaxSceneName = (filterstring myMaxSceneName ".")[1]

MySceneLoad = myExportPath + myMaxSceneName
myTempSceneSave = myExportPath + myMaxSceneName + ".sce"

exportFile myTempSceneSave #noPrompt selectedOnly:True using:exporter

ShellLaunch myToolsPath ("-width 800 -height 500 -scene " + mySceneLoad)