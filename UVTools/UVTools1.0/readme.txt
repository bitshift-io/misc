1. Installation

	a. Extract the files in the archive to your \autodesk\3dxmax8\scripts\ folder
	b. Launch 3D Studio max if it is not already running
	c. Open maxscript > run script and open "uv_tools_button"
	d. Right click on the main toolbar > customize OR open customize > customize > toolbars
	e. From the category dropdown menu goto "UV Tools"
	f. Drag and drop the "UV Tools" from the action box to your Main Toolbar
	g. To launch UV Tool at any time, click on the button labeled "UV Tools" on the main toolbar
	h. The demo is limited to 20 uses each of 30 minutes


2. How to buy UV Tools

	a. Launch UV Tools
	b. Press the about button on the UV Tools rollout. It is the button marked with a "?".
	c. On the about dialog press the "buy now" button, OR use the max ID from the about dialog on the orderform on the blackcarbon website.


3. Contact
	www.blackcarbon.net
	support@blackcarbon.net


4. Conditions of use

	This program is copyright � 2006 - 2006 Black Carbon. You are authorised to use the program for your own private use but you may not sell the program or remove any copyright information from it. The software may not be distributed without prior permission from Black Carbon.
	
	You use the software at your own risk - the author accepts no responsibility for any problems or losses which might arise from its use, and support for the software is not guaranteed.


5. Brief Feature Guide

	Backup/Retrive
	Backup will save the current uvw layout. Retrive will cycle through the past 5 backup's of uvw layouts. Press these buttons while in EditablePoly mode, no selection is needed.
	
	Copy/Paste
	This can be used in editable poly mode, a face selection is required. To copy uv's make a selection of faces and press the copy button. You can then make another selection elsewhere on your object and paste the uv layout.
	
	Flip >/Flip ^
	These buttons can be used in Editable poly mode. Make a selection, press the flip button and is will flip/mirror your uvw layout for the selected faces.
	
	Align >/Align ^
	These buttons can be used in Editable poly mode. Make a selection of verts/edges or faces and align will align the selection either in the U or V axis.
	
	< > \/ /\
	As above, but these will align to the zero or one line in the uvw editor.
	
	Fit> / Fit ^ / Fit
	Works in Editable poly or UVW unwrap modes. These will fit your selection inside the 0-1 range. Fit > will fit your selection between the 0-1 range in the U axis(and keep scale). Fit ^ will fit your selection between the 0-1 range in the V axis(and keep scale).
	
	90 Scale
	This wil rotate your selection right 90 degrees and scale to the original dimensions.
	
	Relax
	This will to a uvw relax on your selection using predefined settings.
	
	Break
	This will break the selected faces uv's.
	
	Border
	This will attempt to make your selected faces border fit on the 0-1 uvw ranges.
	
	Stitch
	Stitch will stitch uv's. Select your first face(the source face), then hold crtl and select a target face. The taget face(element) will move align and scale to the source face.
	
	Explode
	No selection Required. This will seperate your mesh into elements defined by material ID
	
	Unfold
	Unfold will attempt to "unfold" your mesh and keep it continuous.
	
	Quadmap
	This only works on quads, it will map your quads evenly.
	
	Clear
	This will clear the uv's in your selection
	
	Face Map
	Facemaps the current selection
	
	Boxmap
	Boxmaps the current selection
	
	Weld
	Weld attempts to weld your selected faces making the selection seamless
	
	X
	Planar map selection from the X axis
	
	Y
	Planar map selection from the Y axis
	
	Z 
	Planar map selection from the Z axis
	
	V
	Planar map selection from the camera View(view align)
	
	Flatten
	This will map each face in the selection acording to the angle in the flatten spinner.
	
	UV Xform
	Applys a uvw xfrom modifier to the stack
	
	Edit UVW's
	Applys a unwrap uvw selection modifier and takes your selection into the modifier.
	
	UV Map
	Applys a uv map modifier to the selection.
	
	Element
	Element will make a UV element selection the selected face(s)
	
	Angle
	Select by angle set in the spinner from the selected face(s)
	
	Tile/Offset
	Allows you to tile/offset your selected faces, verts or edges while in editable poly mode.
	
	Unused
	Displays unused material ID's
	
	Mat Edit
	Opens the material applyed to the current obect in the material editor
	
	(bitmap preview)
	Shows a preview of the currently selected material id.
	
	(Dropdown menu under bitmap preview)
	select material ID by name rather than id
	
	Update Texture
	untick this if you do not want the bitmap preview to change eachtime you make a selection of faces.
	
	Mat ID
	change material id. Pressing the Mat ID button will select by material ID.
	
	Channel
	Select which uvw channel uv tools will work with.
	
	Animate
	Animate uvw colors after you have animated lights in a scene.
	
	Point UV
	Select a vert(s) and click Point UV. Then click in the viewport to move uv's around with the mouse. Click again to apply, right click to cancel.
	
	Compare
	Compares uv colors(no selection required)
	
	Restore
	This will restore changed colors if you have manually set colors(requires the above button first)
	
	(dropdown menu)
	Unhide by material ID
	
	Xm/Ym/Zm
	Mirror selected geometry in the X/Y/Z axis.
	
	Center
	centers pivot of selected object
	
	Ground
	Grounds pivot of selected object
