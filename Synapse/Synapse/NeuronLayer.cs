﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Synapse
{
    //
    // Creates an array of parallel neurons
    // and supplies an interface to connect this parallel
    // array to another parallel array (ie. another NeuronLayer)
    //
    class NeuronLayer
    {
        public enum ConnectionFlags
        {
            None = 0x0,
            Forward = 0x1 << 0,
            Backward = 0x1 << 1,
            UniDirectional = Forward | Backward,
        }

        public NeuronLayer(int neuronCount)
        {
            for (int n = 0; n < neuronCount; ++n)
            {
                m_neuronList.Add(new Neuron());
            }
        }

        //
        // Connect two neruon layers, this is treated as the previous layer, forward is assumed to be the next layer
        //
        public void Connect(NeuronLayer forward, ConnectionFlags flags = ConnectionFlags.Forward)
        {
            List<Neuron> forwardNeuronList = forward.GetNeuronList();
            List<Neuron> backwardNeuronList = this.GetNeuronList();

            if ((flags & ConnectionFlags.Forward) != 0)
            {
                for (int f = 0; f < forwardNeuronList.Count; ++f)
                {
                    for (int b = 0; b < backwardNeuronList.Count; ++b)
                    {
                        new Synapse(forwardNeuronList[f], backwardNeuronList[b]);
                    }
                }
            }

            if ((flags & ConnectionFlags.Backward) != 0)
            {
                for (int f = 0; f < forwardNeuronList.Count; ++f)
                {
                    for (int b = 0; b < backwardNeuronList.Count; ++b)
                    {
                        new Synapse(backwardNeuronList[b], forwardNeuronList[f]);
                    }
                }
            }
        }

        public void Connect(Astrocyte astrocyte)
        { 

        }

        //
        // Create a chain of neurons
        //
        static public List<NeuronLayer> CreateSimplyChain(int length, int startCount, int endCount = -1, ConnectionFlags flags = ConnectionFlags.Forward)
        {
            // create neuron layers
            if (endCount < 0)
                endCount = startCount;

            double increment = (double)(endCount - startCount) / (double)(length - 1.0);
            double count = startCount;
            List<NeuronLayer> neuronLayerList = new List<NeuronLayer>(length);
            for (int l = 0; l < length; ++l)
            {
                int iCount = (int)count;
                neuronLayerList.Add(new NeuronLayer(iCount));
                count += increment;
            }

            // connect neuron layers
            for (int l = 0; l < length - 1; ++l)
            {
                neuronLayerList[l].Connect(neuronLayerList[l + 1], flags);
            }

            return neuronLayerList;
        }

        static public NeuronLayer CreateFullyConnected(int neuronCount)
        {
            // create neurons
            NeuronLayer layer = new NeuronLayer(neuronCount);
            List<Neuron> neuronList = layer.GetNeuronList();

            // create connections
            for (int n = 0; n < neuronList.Count(); ++n)
            {
                for (int cn = 0; cn < neuronList.Count(); ++cn)
                {
                    if (cn == n)
                        continue;

                    Synapse connection = new Synapse(neuronList[n], neuronList[cn]);
                }
            }

            return layer;
        }

        public List<Neuron> GetNeuronList()
        {
            return m_neuronList;
        }

        List<Neuron> m_neuronList = new List<Neuron>();
    }
}
