﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Synapse
{
    //
    // Draws neural network for visualisation
    //
    class NeuralNetworkView
    {
        class SynapseView
        {
            Synapse m_synapse;
            NeuronView m_fromNeuronView;
            NeuronView m_toNeuronView;

            public SynapseView(Synapse synapse, NeuronView fromNeuronView, NeuronView toNeuronView)
            {
                m_synapse = synapse;
                m_fromNeuronView = fromNeuronView;
                m_toNeuronView = toNeuronView;
            }

            public void Draw(Graphics g)
            {
                if (m_fromNeuronView == null || m_toNeuronView == null)
                {
                    // TODO:
                    return;
                }

                // TODO: take in to account neuron radius
                PointF from = m_fromNeuronView.m_position;
                PointF to = m_toNeuronView.m_position;

                int divisions = m_synapse.m_length;
                PointF direction = to.Subtract(from);
                PointF position = from;
                double length = direction.Length();
                direction = direction.Normalize();
                double increment = length / divisions;
                direction = direction.Multiply(increment);

                for (int i = 0; i < divisions; ++i)
                {
                    PointF nextPosition = position.Add(direction);
                    double stimulus = m_synapse.m_stimulus[(divisions - 1) - i];
                    Color color = Color.FromArgb(50, 0, 255, 0);
                    if (stimulus >= 1.0) // temporary till we work out how to draw unidirectional stuffs
                        color = Color.FromArgb(255, 0, 0);

                    Pen pen = new Pen(new SolidBrush(color));
                    g.DrawLine(pen, position, nextPosition);
  
                    position = nextPosition;
                }
            }
        }

        class NeuronView
        {
            public Neuron m_neuron;
            public PointF m_position;

            public NeuronView(Neuron neuron)
            {
                m_neuron = neuron;
            }

            public void SetPosition(PointF position)
            {
                m_position = position;
            }

            public void Draw(Graphics g)
            {
                float radius = 10.0f;
                Color color = Color.FromArgb(0, 255, 0);
                if (m_neuron.GetOutputStimulus() >= 1.0)
                    color = Color.FromArgb(255, 0, 0);
   
                g.FillEllipse(new SolidBrush(color), new RectangleF(m_position.X - radius, m_position.Y - radius, radius * 2.0f, radius * 2.0f));
            }
        }

        PictureBox m_view;
        NeuralNetwork m_network;

        Dictionary<Neuron, NeuronView> m_neuronViewDictionary = new Dictionary<Neuron, NeuronView>(new Neuron.IDComparer());
        Dictionary<Synapse, SynapseView> m_synapseViewDictionary = new Dictionary<Synapse, SynapseView>();

        public NeuralNetworkView(NeuralNetwork network, PictureBox view)
        {
            m_network = network;
            m_view = view;

            // set up a default view
            List<NeuronLayer> layerList = m_network.GetNeuronLayerList();

            double widthIncrement = (double)m_view.Width / (double)(layerList.Count + 1);
            double x = widthIncrement;

            try
            {
                // add/create neuron views
                foreach (NeuronLayer layer in layerList)
                {
                    List<Neuron> neuronList = layer.GetNeuronList();
                    double heightIncrement = (double)m_view.Height / (double)(neuronList.Count + 1);
                    double y = heightIncrement;

                    foreach (Neuron neuron in neuronList)
                    {
                        NeuronView neuronView = new NeuronView(neuron);
                        neuronView.SetPosition(new PointF((float)x, (float)y));
                        m_neuronViewDictionary.Add(neuron, neuronView);
                        y += heightIncrement;
                    }

                    x += widthIncrement;
                }

                // add/create synapse views
                foreach (NeuronLayer layer in layerList)
                {
                    List<Neuron> neuronList = layer.GetNeuronList();

                    foreach (Neuron neuron in neuronList)
                    {
                        List<Synapse> inputList = neuron.GetInputConnectionList();
                        List<Synapse> outputList = neuron.GetOutputConnectionList();

                        foreach (Synapse synapse in inputList)
                            AddAndCreateSynapseView(synapse);

                        foreach (Synapse synapse in outputList)
                            AddAndCreateSynapseView(synapse);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void AddAndCreateSynapseView(Synapse synapse)
        {
            if (!m_synapseViewDictionary.ContainsKey(synapse))
            {
                NeuronView fromView = null;
                NeuronView toView = null;

                if (synapse.m_from != null && m_neuronViewDictionary.ContainsKey(synapse.m_from))
                {
                    fromView = m_neuronViewDictionary[synapse.m_from];
                }

                if (synapse.m_to != null && m_neuronViewDictionary.ContainsKey(synapse.m_to))
                {
                    toView = m_neuronViewDictionary[synapse.m_to];
                }

                SynapseView synapseView = new SynapseView(synapse, fromView, toView);
                m_synapseViewDictionary.Add(synapse, synapseView);
            }
        }

        public Graphics DrawBegin()
        {
            m_view.Image = new Bitmap(m_view.Width, m_view.Height);
            return Graphics.FromImage(m_view.Image);
        }

        public void DrawEnd(Graphics g)
        {
            g.Dispose();
            m_view.Refresh();
        }

        public void Draw()
        {
            Graphics g = DrawBegin();

            for (int i = 0; i < m_synapseViewDictionary.Count; ++i)
            {
                m_synapseViewDictionary.ElementAt(i).Value.Draw(g);
            }

            for (int i = 0; i < m_neuronViewDictionary.Count; ++i)
            {
                m_neuronViewDictionary.ElementAt(i).Value.Draw(g);
            }

            DrawEnd(g);
        }
    }
}
