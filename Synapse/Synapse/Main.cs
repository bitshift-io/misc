﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Synapse
{
    public partial class Main : Form
    {
        NeuronGraphTest m_neuronGraphTest;
        FluidSolverTest m_fluidSolverTest;

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            //m_neuronGraphTest = new NeuronGraphTest(this);
            m_fluidSolverTest = new FluidSolverTest(this);
        }

        private void board_Click(object sender, EventArgs e)
        {
            //m_game.MouseClick(sender, e);
        }

        public PictureBox GetGraphPictureBox()
        {
            return board;
        }

        public PictureBox GetFluidView()
        {
            return fluid;
        }

        public PictureBox GetNeuronViewPictureBox()
        {
            return neuronView;
        }

        public PictureBox GetGamePictureBox()
        {
            return board;
        }
    }
}
