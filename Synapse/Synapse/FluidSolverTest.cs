﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Timers;
using System.Drawing;
using System.Diagnostics;

namespace Synapse
{
    class FluidSolverTest
    {
        FluidSolver m_fluid;
        FluidSolverView m_view;

        Main m_form;
        System.Timers.Timer m_updateTimer;

        MouseEventArgs m_mouseEventPrev;

        public FluidSolverTest(Main form)
        {
            m_form = form;

            PictureBox pictureBox = m_form.GetFluidView();
            pictureBox.MouseClick += new MouseEventHandler(View_MouseClick);
            pictureBox.MouseMove += new MouseEventHandler(View_MouseMove);

            m_fluid = new FluidSolver(40, 40);
            m_view = new FluidSolverView(m_fluid, pictureBox);

            m_updateTimer = new System.Timers.Timer();
            m_updateTimer.Elapsed += new ElapsedEventHandler(UpdateTimerElapsed);
            m_updateTimer.Interval = 1000 / 15; // 30 times per second

            m_updateTimer.Start();
        }

        public void UpdateTimerElapsed(object source, ElapsedEventArgs e)
        {
            try
            {
                MethodInvoker mi = new MethodInvoker(Update);
                m_form.BeginInvoke(mi);
            }
            catch (Exception ex)
            {
            }
        }

        public void Update()
        {
            Stopwatch timeSpan = new Stopwatch();
            timeSpan.Start();
            

            // if timestep is too big, it seems to fill up and not disperse correctly!
            m_fluid.Step(1.0 / 60.0);
            Draw();

            timeSpan.Stop();
            long updateTime = timeSpan.ElapsedMilliseconds;
            int nothing = 0;
            ++nothing;
        }

        public void Draw()
        {
            m_view.Draw();
        }

        public void View_MouseClick(object sender, EventArgs e)
        {
            MouseEventArgs mouseArgs = (MouseEventArgs)e;
            m_view.AddDensity(m_view.PointToIndex(System.Windows.Forms.Cursor.Position), 1.0);
        }

        public void View_MouseMove(object sender, EventArgs e)
        {
            MouseEventArgs mouseArgs = (MouseEventArgs)e;

            if (m_mouseEventPrev != null && mouseArgs.Button == MouseButtons.Right)
            {
                PointF vel = mouseArgs.Location.ToPointF().Subtract(m_mouseEventPrev.Location.ToPointF());// -m_mouseEventPrev.Location;
                vel = vel.Normalize();
                m_view.SetVelocity(m_view.PointToIndex(System.Windows.Forms.Cursor.Position), vel);
            }

            if (mouseArgs.Button == MouseButtons.Left)
                m_view.AddDensity(m_view.PointToIndex(System.Windows.Forms.Cursor.Position), 1.0);

            m_mouseEventPrev = mouseArgs;
        }

    }
}
