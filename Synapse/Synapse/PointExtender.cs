﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Synapse
{
    public static class PointExtender
    {
        public static PointF ToPointF(this Point p)
        {
            return new PointF(p.X, p.Y);
        }

        public static PointF Subtract(this PointF p, PointF other)
        {
            return new PointF(p.X - other.X, p.Y - other.Y);
        }

        public static PointF Add(this PointF p, PointF other)
        {
            return new PointF(other.X + p.X, other.Y + p.Y);
        }

        public static double Length(this PointF p)
        {
            return Math.Sqrt((p.X * p.X) + (p.Y * p.Y));
        }

        public static PointF Normalize(this PointF p)
        {
            float length = (float)Length(p);
            return new PointF(p.X / length, p.Y / length);
        }

        public static PointF Multiply(this PointF p, double length)
        {
            return new PointF(p.X * (float)length, p.Y * (float)length);
        }

    }
}
