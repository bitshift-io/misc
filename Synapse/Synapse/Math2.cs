﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Synapse
{
    class Math2
    {
        public static double Clamp(double min, double value, double max)
        {
            return Math.Max(min, Math.Min(max, value));
        }

        public static double Lerp(double from, double to, double percent)
        {
            return from * (1.0 - percent) + to * percent;
        }

        public static int Random(int min, int max)
        {
            return s_random.Next(min, max);
        }

        public static double Random(double min, double max)
        {
            return (s_random.NextDouble() * (max - min)) + min;
        }

        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp;
            temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        static Random s_random = new Random();
    }
}
