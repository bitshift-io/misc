﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Synapse
{
    class FluidSolverView
    {
        PictureBox m_view;
        FluidSolver m_fluid;
        bool m_drawGrid = true;

        public FluidSolverView(FluidSolver fluid, PictureBox view)
        {
            m_fluid = fluid;
            m_view = view;
        }

        public Graphics DrawBegin()
        {
            m_view.Image = new Bitmap(m_view.Width, m_view.Height);
            return Graphics.FromImage(m_view.Image);
        }

        public void DrawEnd(Graphics g)
        {
            g.Dispose();
            m_view.Refresh();
        }

        public Rectangle GetRectangle(Point index)
        {
            int gridSizeX = m_view.Width / m_fluid.GetX();
            int gridSizeY = m_view.Height / m_fluid.GetY();
            Rectangle rect = new Rectangle(index.X * gridSizeX, index.Y * gridSizeY, gridSizeX, gridSizeY);
            return rect;
        }

        public void Draw()
        {
            Graphics g = DrawBegin();

            int gridSizeX = m_view.Width / m_fluid.GetX();
            int gridSizeY = m_view.Height / m_fluid.GetY();

            Pen velocityPen = new Pen(Color.Blue);
            for (int y = 0; y < m_fluid.GetY(); ++y)
            {
                for (int x = 0; x < m_fluid.GetX(); ++x)
                {
                    // draw density
                    Point position = new Point(x, y);
                    double density = m_fluid.GetDensity(position);
                    density = Math2.Clamp(0.0, density, 1.0);
                    Color color = Color.FromArgb((int)(density * 255.0), (int)(255.0 * (1.0 - density)), 0);
                    Brush brush = new SolidBrush(color);
                    Rectangle rect = GetRectangle(position);
                    g.FillRectangle(brush, rect);

                    // draw velocity, scale velocity to the size of the grid so its not like 1 pixel!
                    PointF centerPoint = new PointF((float)(x + 0.5) * gridSizeX, (float)(y + 0.5) * gridSizeY);
                    PointF velocity = m_fluid.GetVelocity(position);
                    //velocity = velocity.Multiply(10.0); // ampify
                    velocity.X *= gridSizeX;
                    velocity.Y *= gridSizeY;

                    PointF endPoint = centerPoint.Add(velocity);
                    g.DrawLine(velocityPen, centerPoint, endPoint);
                }
            }

            DrawEnd(g);
        }

        public void SetVelocity(Point index, PointF velocity)
        {
            m_fluid.SetVelocity(index, velocity);
        }

        public void AddDensity(Point index, double density)
        {
            m_fluid.AddDensity(index, density);
        }

        public Point GetIndexFromPoint(Point point)
        {
            Point tile = new Point();
            int gridSizeX = m_view.Width / m_fluid.GetX();
            int gridSizeY = m_view.Height / m_fluid.GetY();
            tile.X = point.X / gridSizeX;
            tile.Y = point.Y / gridSizeY;
            return tile;
        }

        // Convert a point like the mouse cursor position
        // to a tile index point
        public Point PointToIndex(Point point)
        {
            Point clientPoint = m_view.PointToClient(point);
            return GetIndexFromPoint(clientPoint);
        }
    }
}
