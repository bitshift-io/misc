﻿//#define IX(i, j) ((i)+(N+2)*(j))
//#define SWAP(x0,x) {float *tmp=x0;x0=x;x=tmp;}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Synapse
{
    class FluidSolver
    {
        float[,] m_velocityPrevX;
        float[,] m_velocityPrevY;

        float[,] m_velocityX;
        float[,] m_velocityY;

        float[,] m_density;
        float[,] m_densityPrev;

        int m_x;
        int m_y;

        public FluidSolver(int xSize, int ySize)
        {
            m_x = xSize;
            m_y = ySize;

            m_velocityX = new float[m_x, m_y];
            m_velocityY = new float[m_x, m_y];

            m_velocityPrevX = new float[m_x, m_y];
            m_velocityPrevY = new float[m_x, m_y];

            m_density = new float[m_x, m_y];
            m_densityPrev = new float[m_x, m_y];
        }

        public int GetX()
        {
            return m_x;
        }

        public int GetY()
        {
            return m_y;
        }

        public PointF GetVelocity(Point index)
        {
            return new PointF(m_velocityX[index.X, index.Y], m_velocityY[index.X, index.Y]);
        }

        public double GetDensity(Point index)
        {
            return m_density[index.X, index.Y];
        }

        public void AddDensity(Point index, double density)
        {
            if (index.X < 0 || index.X >= m_x || index.Y < 0 || index.Y >= m_y)
                return;

            m_density[index.X, index.Y] += (float)density;
        }

        public void SetVelocity(Point index, PointF velocity)
        {
            if (index.X < 0 || index.X >= m_x || index.Y < 0 || index.Y >= m_y)
                return;

            m_velocityX[index.X, index.Y] = velocity.X;
            m_velocityY[index.X, index.Y] = velocity.Y;
        }

        void Swap(ref float[,] a, ref float[,] b)
        {
            float[,] temp = a;
            a = b;
            b = temp;
        }

        public void Step(double delta)
        {
            float deltaTime = (float)delta;

            // diffuse density
            Swap(ref m_density, ref m_densityPrev);
            Diffuse(ref m_density, m_densityPrev, deltaTime);
            Swap(ref m_density, ref m_densityPrev);
            Move(deltaTime);
        }

        void Move(float deltaTime)
        {
            float multiplier = deltaTime;// *0.1f; // velocity multipler

            // takes particles using the negative velocity
            for (int y = 1; y < (m_y - 1); ++y)
            {
                for (int x = 1; x < (m_x - 1); ++x)
                {
                    // is the problem here?
                    float velX = m_velocityX[x, y] * multiplier;
                    float velY = m_velocityY[x, y] * multiplier;

                    // hrmm we have some issue here, do some form of blend based on the difference as per the GDC03.pdf
                    int vxIndex = (int)Math.Floor(((float)x) - velX); // rounding issues!
                    int vyIndex = (int)Math.Floor(((float)y) - velY);

                    if (vxIndex != x)
                    {
                        int nothing = 0;
                        ++nothing;
                    }

                    m_density[x, y] = m_densityPrev[vxIndex, vyIndex] * 0.5f;
                }
            }
        }

        protected void Diffuse(ref float[,] current, float[,] previous, float deltaTime)
        {
            float multiplier = deltaTime * 10;// *0.1f; // density

            // thread friendly :D
            for (int y = 1; y < (m_y - 1); ++y)
            {
                for (int x = 1; x < (m_x - 1); ++x)
                {
                    float incomming = previous[x - 1, y] + previous[x + 1, y] + previous[x, y - 1] + previous[x, y + 1];
                    float outgoing = 4 * previous[x, y];
                    current[x, y] =  previous[x, y] + (multiplier * (incomming - outgoing));

                    // diffusion of density results in velocity field changing
                    // this is odd, its drifting down and left
                    m_velocityX[x, y] *= 0.1f;
                    m_velocityY[x, y] *= 0.1f;

                   // m_velocityX[x, y] += multiplier * ((previous[x - 1, y] - previous[x, y]) + (previous[x, y] - previous[x + 1, y]));
                    m_velocityX[x, y] = -10.1f; // ((previous[x - 1, y] - previous[x, y]));
                    //m_velocityY[x, y] += multiplier * ((previous[x, y - 1] - previous[x, y]) + (previous[x, y] - previous[x, y + 1]));
                }
            }
        }
    }
 /*
    class FluidSolver
    {
        int N = 10;

        float[] u;
        float[] v;
        float[] u_prev;
        float[] v_prev;

        float[] dens;
        float[] dens_prev;

        public FluidSolver(int xSize, int ySize)
        {
            N = xSize;
            int size = (N + 2) * (N + 2);
            u = new float[size];
            v = new float[size];
            u_prev = new float[size];
            v_prev = new float[size];

            dens = new float[size];
            dens_prev = new float[size];
            /*
            // set up a test velocity field
            Random r = new Random();
            for (int i = 0; i < size; ++i)
            {
                u[i] = (float)((r.NextDouble() * 2.0) - 1.0);
                v[i] = (float)((r.NextDouble() * 2.0) - 1.0);
            }* /

            set_bnd(N, 1, u); set_bnd(N, 2, v);
        }

        int IX(int i, int j)
        {
            return ((i)+(N+2)*(j));
        }

        



        void add_source ( int N, float[] x, float[] s, float dt )  
        { 
          int i, size=(N+2)*(N+2); 
 
          for ( i=0 ; i<size ; i++ ) x[i] += dt*s[i];  
        } 

        void diffuse ( int N, int b, float[] x, float[] x0, float diff, float dt ) 
        { 
            int i, j, k; 
            float a=dt*diff*N*N; 
 
            for ( k=0 ; k<20 ; k++ ) { 
                for ( i=1 ; i<=N ; i++ ) { 
                    for ( j=1 ; j<=N ; j++ ) { 
                            x[IX(i,j)] = (x0[IX(i,j)] + a*(x[IX(i-1,j)]+x[IX(i+1,j)]+ 
                      x[IX(i,j-1)]+x[IX(i,j+1)]))/(1+4*a); 
                    } 
                } 
                set_bnd ( N, b, x ); 
            } 
        } 

        void advect ( int N, int b, float[] d, float[] d0, float[] u, float[] v, float dt ) 
        { 
          int i, j, i0, j0, i1, j1; 
          float x, y, s0, t0, s1, t1, dt0; 
 
          dt0 = dt*N; 
          for ( i=1 ; i<=N ; i++ ) { 
            for ( j=1 ; j<=N ; j++ ) {       x = i-dt0*u[IX(i,j)]; y = j-dt0*v[IX(i,j)]; 
              if (x<0.5) x=0.5f; if (x>N+0.5) x=N+ 0.5f; i0=(int)x; i1=i0+1; 
              if (y<0.5) y=0.5f; if (y>N+0.5) y=N+ 0.5f; j0=(int)y; j1=j0+1; 
              s1 = x-i0; s0 = 1-s1; t1 = y-j0; t0 = 1-t1; 
              d[IX(i,j)] = s0*(t0*d0[IX(i0,j0)]+t1*d0[IX(i0,j1)])+ 
                      s1*(t0*d0[IX(i1,j0)]+t1*d0[IX(i1,j1)]); 
            } 
          } 
          set_bnd ( N, b, d ); 
        } 

        void dens_step ( int N, float[] x, float[] x0,  float[] u, float[] v, float diff,  
        float dt ) 
        { 
          add_source ( N, x, x0, dt ); 
          SWAP ( ref x0, ref x ); diffuse ( N, 0, x, x0, diff, dt ); 
          SWAP ( ref x0, ref x ); advect ( N, 0, x, x0, u, v, dt ); 
        }

        void vel_step ( int N, float[] u, float[] v, float[] u0, float[] v0,  
        float visc, float dt ) 
        { 
            add_source ( N, u, u0, dt ); add_source ( N, v, v0, dt ); 
            SWAP ( ref u0, ref u ); diffuse ( N, 1, u, u0, visc, dt ); 
            SWAP ( ref v0, ref v ); diffuse ( N, 2, v, v0, visc, dt ); 
            project ( N, u, v, u0, v0 ); 
            SWAP ( ref u0, ref u ); SWAP ( ref v0, ref v ); 
            advect ( N, 1, u, u0, u0, v0, dt ); advect ( N,  2, v, v0, u0, v0, dt ); 
            project ( N, u, v, u0, v0 ); 
        } 

        void project ( int N, float[] u, float[] v, float[] p, float[] div )  
        { 
            int i, j, k; 
            float h; 
 
            h = 1.0f/N; 
            for ( i=1 ; i<=N ; i++ ) { 
                for ( j=1 ; j<=N ; j++ ) { 
                  div[IX(i,j)] = -0.5f*h*(u[IX(i+1,j)]-u[IX(i-1,j)]+ 
             v[IX(i,j+1)]-v[IX(i,j-1)]); 
                  p[IX(i,j)] = 0; 
            } 
            } 
            set_bnd ( N, 0, div ); set_bnd ( N, 0, p ); 
 
            for ( k=0 ; k<20 ; k++ ) { 
                for ( i=1 ; i<=N ; i++ ) { 
                  for ( j=1 ; j<=N ; j++ ) { 
                    p[IX(i,j)] = (div[IX(i,j)]+p[IX(i-1,j)]+p[IX(i+1,j)]+ 
              p[IX(i,j-1)]+p[IX(i,j+1)])/4; 
            } 
                } 
                set_bnd ( N, 0, p ); 
              } 
 
              for ( i=1 ; i<=N ; i++ ) { 
                for ( j=1 ; j<=N ; j++ ) { 
                  u[IX(i,j)] -= 0.5f*(p[IX(i+1,j)]-p[IX(i-1,j)])/h; 
                  v[IX(i,j)] -= 0.5f*(p[IX(i,j+1)]-p[IX(i,j-1)])/h; 
                } 
              } 
              set_bnd ( N, 1, u ); set_bnd ( N, 2, v );  
        } 

        void set_bnd ( int N, int b, float[] x ) 
        { 
            int i; 
 
            for ( i=1 ; i<=N ; i++ ) { 
                x[IX(0  ,i)] = b==1 ? -x[IX(1,i)] : x[IX(1,i)]; 
                x[IX(N+1,i)] = b==1 ? -x[IX(N,i)] : x[IX(N,i)]; 
                x[IX(i,0  )] = b==2 ? -x[IX(i,1)] : x[IX(i,1)];     x[IX(i,N+1)] = b==2 ? -x[IX(i,N)] : x[IX(i,N)]; 
            } 
            x[IX(0  ,0  )] = 0.5f*(x[IX(1,0  )]+x[IX(0  ,1)]); 
            x[IX(0  ,N+1)] = 0.5f*(x[IX(1,N+1)]+x[IX(0  ,N )]); 
            x[IX(N+1,0  )] = 0.5f*(x[IX(N,0  )]+x[IX(N+1,1)]); 
            x[IX(N+1,N+1)] = 0.5f*(x[IX(N,N+1)]+x[IX(N+1,N )]); 
        } 

        public void Step(double delta)
        {
            float visc = 0.0001f;
            float dt = (float)(delta);
            float diff = 0.001f;
            
            //get_from_UI ( dens_prev, u_prev, v_prev ); 
         //   vel_step ( N, u, v, u_prev, v_prev, visc, dt ); 
            dens_step ( N, dens, dens_prev, u, v, diff, dt );
            //draw_dens ( N, dens ); 
             
        }

        public PointF GetVelocity(Point index)
        {
            return new PointF((float)u[IX(index.X, index.Y)], (float)v[IX(index.X, index.Y)]);
        }

        public double GetDensity(Point index)
        {
            return dens[IX(index.X, index.Y)]; //new PointF((float)x[index.X], (float)y[index.Y]);
        }

        public void AddDensity(Point index, double density)
        {
            if (index.X < 0 || index.X >= N || index.Y < 0 || index.Y >= N)
                return;

            dens[IX(index.X, index.Y)] += (float)density;
            //m_density[index.X, index.Y] = Math2.Clamp(0.0, m_density[index.X, index.Y] + density, 1.0);
        }

        public void SetVelocity(Point index, PointF velocity)
        {
            u[IX(index.X, index.Y)] = velocity.X;
            v[IX(index.X, index.Y)] = velocity.Y;
        }

        public int GetX()
        {
            return N;
        }

        public int GetY()
        {
            return N;
        }
    }
    */
    /*
    class FluidSolver
    {
        //PointF[,] m_velocityPrev;
        //PointF[,] m_velocity;

        double[,] m_velocityPrevX;
        double[,] m_velocityPrevY;

        double[,] m_velocityX;
        double[,] m_velocityY;

        double[,] m_densityPrev;
        double[,] m_density;

        int m_x;
        int m_y;

        double m_viscosity = 0.01;

        public FluidSolver(int xSize, int ySize)
        {
            m_x = xSize;
            m_y = ySize;

            m_densityPrev = new double[xSize, ySize];
            m_density = new double[xSize, ySize];

            m_velocityPrevY = new double[xSize, ySize];
            m_velocityPrevX = new double[xSize, ySize];

            m_velocityY = new double[xSize, ySize];
            m_velocityX = new double[xSize, ySize]; 

            // set up a test velocity field
            Random r = new Random();
            for (int y = 0; y < ySize; ++y)
            {
                for (int x = 0; x < xSize; ++x)
                {
                    m_velocityX[x, y] = (r.NextDouble() * 2.0) - 1.0;
                    m_velocityY[x, y] = (r.NextDouble() * 2.0) - 1.0;
                }
            }
            /*
            for (int y = 0; y < ySize; ++y)
            {
                for (int x = 0; x < xSize; ++x)
                {
                    m_velocityX[x, y] = 0.0;
                    m_velocityY[x, y] = 1.0;
                }
            }* /
        }

        public int GetX()
        {
            return m_x;
        }

        public int GetY()
        {
            return m_y;
        }

        void AddSource(double[,] to, double[,] from, double delta)
        {
            for (int x = 0; x < m_x; ++x)
            {
                for (int y = 0; y < m_y; ++y)
                {
                    to[x, y] += from[x, y] * delta;
                }
            }
        }

        void Diffuse(double[,] to, double[,] from, double delta)
        {
            double a = delta * m_viscosity * m_x * m_y;
 
            for (int k = 0; k < 20; ++k)
            {
                for (int y = 1; y < (m_y - 1); ++y)
                {
                    for (int x = 1; x < (m_x - 1); ++x)
                    {
                        to[x, y] = (from[x, y] + a * (to[x - 1, y] + to[x + 1, y] + to[x, y - 1] + to[x, y + 1])) / (1 + (4 * a));
                    }
                }

                SetBound(to);
            }
        }

        void SetBound(int count, int index, double[,] to)
        {
            for (int i = 1; i <= count; ++i)
            {
            }
        }

        void Advect(int index, double[,] to, double[,] from, double[,] velocityX, double[,] velocityY, double delta)
        {
            double deltaX = delta * m_x;
            double deltaY = delta * m_y;

            for (int y = 1; y < (m_y - 1); ++y)
            {
                for (int x = 1; x < (m_x - 1); ++x)
                {
                    double xOffset = ((double)x) - deltaX * velocityX[x, y];
                    double yOffset = ((double)y) - deltaY * velocityY[x, y];

                    xOffset = Math2.Clamp(0.5, xOffset, m_x - 1.5);
                    yOffset = Math2.Clamp(0.5, yOffset, m_y - 1.5);

                    int i0 = (int)xOffset;
                    int j0 = (int)yOffset;

                    int i1 = i0 + 1;
                    int j1 = j0 + 1;

                    int s1 = x - i0;
                    int t1 = y - j0;

                    int s0 = 1 - s1;
                    int t0 = 1 - t1;


                    to[x, y] = s0 * (t0 * from[i0, j0] + t1 * from[i0, j1]) +
                               s1 * (t0 * from[i1, j0] + t1 * from[i1, j1]);
                }

                SetBound(index, to);
            }
        }

        void Project(double[,] xFrom, double[,] yFrom, double[,] p, double[,] div)
        {
            double h = 1.0 / m_x;

            for (int y = 1; y < (m_y - 1); ++y)
            {
                for (int x = 1; x < (m_x - 1); ++x)
                {
                    div[x, y] = -0.5 * h * (xFrom[x + 1, y] - xFrom[x - 1, y]
                                          + yFrom[x, y + 1] - yFrom[x, y - 1]);
                    p[x, y] = 0.0;
                }
            }

            SetBound(div);
            SetBound(p);

            for (int k = 0; k < 20; ++k)
            {
                for (int y = 1; y < (m_y - 1); ++y)
                {
                    for (int x = 1; x < (m_x - 1); ++x)
                    {
                        p[x, y] = (div[x, y] + p[x - 1, y] + p[x + 1, y] + p[x, y - 1] + p[x, y + 1]) / 4.0;
                    }
                }

                SetBound(p);
            }

            for (int y = 1; y < (m_y - 1); ++y)
            {
                for (int x = 1; x < (m_x - 1); ++x)
                {
                    xFrom[x, y] -= 0.5 * (p[x + 1, y] - p[x - 1, y]) / h;
                    yFrom[x, y] -= 0.5 * (p[x, y + 1] - p[x, y - 1]) / h;
                }
            }

            SetBound(xFrom);
            SetBound(yFrom);
        }

        void VelocityStep(double delta)
        {
            AddSource(m_velocityX, m_velocityPrevX, delta);
            AddSource(m_velocityY, m_velocityPrevY, delta);
            
            Math2.Swap<double[,]>(ref m_velocityX, ref m_velocityPrevX);
            Math2.Swap<double[,]>(ref m_velocityY, ref m_velocityPrevY);
            
            Diffuse(m_velocityX, m_velocityPrevX, delta);
            Diffuse(m_velocityY, m_velocityPrevY, delta);
            Project(m_velocityX, m_velocityY, m_velocityPrevX, m_velocityPrevY);

            Math2.Swap<double[,]>(ref m_velocityX, ref m_velocityPrevX);
            Math2.Swap<double[,]>(ref m_velocityY, ref m_velocityPrevY);
            
            Advect(1, m_velocityX, m_velocityPrevX, m_velocityPrevX, m_velocityPrevY, delta);
            Advect(2, m_velocityY, m_velocityPrevY, m_velocityPrevX, m_velocityPrevY, delta);
            Project(m_velocityX, m_velocityY, m_velocityPrevX, m_velocityPrevY);
        }

        void DensityStep(double delta)
        {
            AddSource(m_density, m_densityPrev, delta);

            Math2.Swap<double[,]>(ref m_density, ref m_densityPrev);

            Diffuse(m_density, m_densityPrev, delta);

            Math2.Swap<double[,]>(ref m_density, ref m_densityPrev);

            Advect(m_density, m_densityPrev, m_velocityX, m_velocityY, delta);
        }

        public void Step(double delta)
        {
            VelocityStep(delta);
            //DensityStep(delta);
        }

        public PointF GetVelocity(Point index)
        {
            return new PointF((float)m_velocityX[index.X, index.Y], (float)m_velocityY[index.X, index.Y]);
        }

        public double GetDensity(Point index)
        {
            return m_density[index.X, index.Y];
        }

        public void AddDensity(Point index, double density)
        {
            if (index.X < 0 || index.X >= m_x || index.Y < 0 || index.Y >= m_y)
                return;

            m_density[index.X, index.Y] += density;
            //m_density[index.X, index.Y] = Math2.Clamp(0.0, m_density[index.X, index.Y] + density, 1.0);
        }
    }*/
}
