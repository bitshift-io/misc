﻿namespace Synapse
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.board = new System.Windows.Forms.PictureBox();
            this.neuronView = new System.Windows.Forms.PictureBox();
            this.fluid = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.board)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.neuronView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluid)).BeginInit();
            this.SuspendLayout();
            // 
            // board
            // 
            this.board.Location = new System.Drawing.Point(12, 12);
            this.board.Name = "board";
            this.board.Size = new System.Drawing.Size(200, 200);
            this.board.TabIndex = 2;
            this.board.TabStop = false;
            this.board.Click += new System.EventHandler(this.board_Click);
            // 
            // neuronView
            // 
            this.neuronView.Location = new System.Drawing.Point(12, 12);
            this.neuronView.Name = "neuronView";
            this.neuronView.Size = new System.Drawing.Size(800, 800);
            this.neuronView.TabIndex = 3;
            this.neuronView.TabStop = false;
            // 
            // fluid
            // 
            this.fluid.Location = new System.Drawing.Point(910, 157);
            this.fluid.Name = "fluid";
            this.fluid.Size = new System.Drawing.Size(400, 400);
            this.fluid.TabIndex = 4;
            this.fluid.TabStop = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1542, 827);
            this.Controls.Add(this.fluid);
            this.Controls.Add(this.board);
            this.Controls.Add(this.neuronView);
            this.Name = "Main";
            this.Text = "Synapse";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.board)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.neuronView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox board;
        private System.Windows.Forms.PictureBox neuronView;
        private System.Windows.Forms.PictureBox fluid;
    }
}

