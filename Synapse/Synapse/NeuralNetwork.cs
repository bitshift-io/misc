﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

//
// better attempts to immitade real brain nerual networks
//
// http://www.csupomona.edu/~seskandari/physiology/lecture_6.html
//
namespace Synapse
{
    class NeuralNetwork
    {

        public void Add(NeuronLayer layer)
        {
            m_neuronLayerList.Add(layer);
            List<Neuron> neuronList = layer.GetNeuronList();
            m_neuronList.AddRange(neuronList.ToArray());
        }

        public void Add(List<NeuronLayer> layerList)
        {
            foreach (NeuronLayer layer in layerList)
            {
                Add(layer);
            }
        }

        //
        //
        //
        public void Step()
        {
            // should be multithread friendly too i would imagine
            for (int n = 0; n < m_neuronList.Count(); ++n)
            {
                m_neuronList[n].Step();
            }
        }

        static public void Test()
        {
            /*
            //
            // Neuron test
            // ----------------------------
            // Create a single neuron, apply a fixed stimulus
            // which is greater than its threshold
            // which should cause the neuron to emit pulses of a certain frequency
            //
            NeuralNetwork network = new NeuralNetwork();
            Neuron neuron = network.Create(1).ElementAt(0);
            Synapse inputConnection = new Synapse(neuron);
            inputConnection.PushStimulus(1.0);

            // 100 millisecond simulation
            // output to CSV file
            TextWriter file = new StreamWriter("neuron_test.csv");
            double deltaTime = 0.25 / 1000.0;
            for (double i = 0; i < 10.0; i += deltaTime)
            {
                network.Step();

                // time, threshold, voltage
                file.WriteLine(i + "," + neuron.GetThreshold() + "," + neuron.GetVoltage());
                //Console.WriteLine(i + "," + neuron.GetThreshold() + "," + neuron.GetVoltage());
            }
            file.Close();*/
        }

        public List<Neuron> GetNeuronList()
        {
            return m_neuronList;
        }

        public List<NeuronLayer> GetNeuronLayerList()
        {
            return m_neuronLayerList;
        }

        List<NeuronLayer> m_neuronLayerList = new List<NeuronLayer>();
        List<Neuron> m_neuronList = new List<Neuron>();
    }
}
