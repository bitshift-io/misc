﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Synapse
{
    // Connects one Neuron to another for one way communication
    // Also connects an Astrocyte in a 3 way junction
    //
    // Input and Output synapses dont need to worry about time delays
    // so have a fixed single value that doesnt act like a queue
    class Synapse
    {
        public enum Type
        {
            Input,
            Output,
            Internal
        }

        const int DEFAULT_LENGTH = 10;
        const int LENGTH_MIN = 5;
        const int LENGTH_MAX = 100;

        public Synapse(Neuron to, Type type)
        {
            m_to = to;
            m_length = 1;
            m_type = type;
            m_stimulus.Add(0.0);

            if (m_to != null)
                m_to.AddInputConnection(this);
        }

        public Synapse(Neuron to, Neuron from = null)
        {
            m_from = from;
            m_to = to;

            if (m_from != null)
                m_from.AddOutputConnection(this);

            if (m_to != null)
                m_to.AddInputConnection(this);

            m_length = Math2.Random(LENGTH_MIN, LENGTH_MAX); // DEFAULT_LENGTH;
            for (int i = 0; i < m_length; ++i)
            {
                m_stimulus.Add(0.0);
            }
        }

        public void PushStimulus(double voltage)
        {
            if (m_type == Type.Internal)
                m_stimulus.Add(voltage);
            else
                m_stimulus[0] = voltage;
        }

        public double PopStimulus()
        {
            double voltage = m_stimulus[0];

            if (m_type == Type.Internal)
            {
                m_stimulus.RemoveAt(0);
                voltage = voltage * m_strength;
            }

            return voltage;
        }

        public List<double> m_stimulus = new List<double>();
        public int m_length; // length of voltage queue
        public Type m_type = Type.Internal;
        public double m_strength = 1.0; // connection strength, and inhibits or excites stimulus

        public Neuron m_from;
        public Neuron m_to;
        public Astrocyte m_astrocyte;
    }
}
