﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Synapse
{
    //
    // Lets assume these are some form of memory
    // like an angalogue wave
    // will be affected by the neuron inputs and do the reverse
    // so as the voltage goes up over time, this will inhibit the neurons
    // this i figure will stop looping.
    //
    // probably a better analogy as to what this cell might do is to rewire the weights
    // based on the inputs.
    //
    // and even another analogy ,provide long term memory using cell automaton
    //
    //
    // look in to brain vortex model
    // http://coe.bri.niigata-u.ac.jp/files/vtheory/VortexII_Brain_Chip_R.pdf
    class Astrocyte
    {
        List<Synapse> m_connectionList = new List<Synapse>();
    }
}
