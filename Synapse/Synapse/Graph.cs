﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Synapse
{
    class GraphLine
    {
        Pen m_pen;
        List<PointF> m_pointList = new List<PointF>();

        public GraphLine()
        {
            SetColor(Color.Blue);
        }

        public void Clear()
        {
            m_pointList.Clear();
        }

        public void Add(PointF point)
        {
            m_pointList.Add(point);
        }

        public void SetColor(Color color)
        {
            m_pen = new Pen(new SolidBrush(color));
            m_pen.Width = 0.0001f;
        }

        public void Draw(Graphics g, Graph graph)
        {
            PointF? previousPoint = null;
            foreach (PointF point in m_pointList)
            {
                if (previousPoint.HasValue)
                {
                    g.DrawLine(m_pen, previousPoint.Value, point);
                }

                previousPoint = point;
            }
        }
    }

    class Graph
    {
        const int AXIS_MARGIN = 20;

        PictureBox m_graph;
        Rectangle m_plotArea;

        double[] m_yMinMax = new double[] { -1.0f, 1.0f };
        double[] m_xMinMax = new double[] { 0.0f, 1.0f };

        public Graph(PictureBox graph)
        {
            m_graph = graph;
            m_plotArea = new Rectangle(AXIS_MARGIN, 0, m_graph.Width - AXIS_MARGIN, m_graph.Height - AXIS_MARGIN);
        }

        public Rectangle GetPlotArea()
        {
            return m_plotArea;
        }

        public Graphics DrawBegin()
        {
            m_graph.Image = new Bitmap(m_graph.Width, m_graph.Height);
            return Graphics.FromImage(m_graph.Image);
        }

        public void DrawEnd(Graphics g)
        {
            g.Dispose();
            m_graph.Refresh();
        }

        public void DrawAxis(Graphics g)
        {
            Pen pen = new Pen(new SolidBrush(Color.Red));

            // X Axis (time)
            Point p0 = new Point(m_plotArea.Left, m_plotArea.Bottom);
            Point p1 = new Point(m_plotArea.Right, m_plotArea.Bottom);
            g.DrawLine(pen, p0, p1);

            // Y Axis (voltage)
            p0 = new Point(m_plotArea.Left, m_plotArea.Bottom);
            p1 = new Point(m_plotArea.Left, m_plotArea.Top);
            g.DrawLine(pen, p0, p1);
        }

        public void DrawInPlotAreaBegin(Graphics g)
        {
            g.SetClip(m_plotArea);
            g.TranslateTransform(m_plotArea.Left, m_plotArea.Bottom);

            // TODO: get x component working
            float yDelta = (float)(m_yMinMax[1] - m_yMinMax[0]);

            // -ve y scale to make 0,0 bottom left instead of top left
            float xScale = (float)m_plotArea.Width;
            float yScale = (float)m_plotArea.Height / yDelta;

            g.ScaleTransform(xScale, -yScale);
            g.TranslateTransform(0.0f, -(float)m_yMinMax[0]);
        }

        public PointF ScreenPointToPlotArea(Point screen)
        {
            // TODO: get x component working
            Point p = m_graph.PointToClient(System.Windows.Forms.Cursor.Position);
            Rectangle plotArea = GetPlotArea();

            p.X -= plotArea.Left;
            p.Y -= plotArea.Top;

            float yDelta = (float)(m_yMinMax[1] - m_yMinMax[0]);
            float yScale = (float)m_plotArea.Height / yDelta;
            
            double y = plotArea.Height - p.Y;
            y /= yScale;
            y += (float)m_yMinMax[0];

            return new PointF(0.0f, (float)y);
        }
    }
}
