﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Synapse
{

    class Neuron
    {
        const double ACTION_POTENTIAL_TIME = 10.0; // 1ms
        const double ACTION_POTENTIAL_VOLTAGE = 1.0;
        const double THRESHOLD_DELAY = 10.0; // time steps
        /*
        public enum State
        {
            Resting,
            ActionPotential,
            RefactorPeriod
        }

        // values are in millivolts
        const double PEAK_VOLTAGE = 50.0;
        const double THRESHOLD_VOLTAGE_MIN = -40.0;
        const double THRESHOLD_VOLTAGE_MAX = -50.0;
        const double ACTION_POTENTIAL_VOLTAGE_MAX = 50.0;
        const double IDLE_VOLTAGE = -70.0;

        const double REFACTOR_TIME_MIN = 1.0 / 1000.0;
        const double REFACTOR_TIME_MAX = 15.0 / 1000.0;

        const double ACTION_POTENTIAL_TIME = 1.0 / 1000.0;

        public void Resting_Enter()
        {
        }
        */
        public double GetInputStimulus()
        {
            return m_inputStimulus;
        }
        /*
        public void Resting_Progress(double deltaTime)
        {
            double stimulus = GetStimulus();

            // TODO: fix me, currently just doing a linear speed
            // can we leave it like this?
            //double x = m_stateTime / ACTION_POTENTIAL_TIME;
            //double y =  1 - Math.Pow(1.0 - x, 3.0);
            //m_voltage = Math2.Lerp(m_voltage, stimulus, 0.5);
            double changeRate = 10.0;
            double deltaVoltage = stimulus - m_voltage;
            m_voltage += Math2.Clamp(-changeRate, deltaVoltage, changeRate);

            if (m_voltage > m_threshold)
            {
                m_voltage = m_threshold;
                SetState(State.ActionPotential);
            }
        }


        public void ActionPotential_Enter()
        {
            m_stateTime = 0;
        }

        public void ActionPotential_Progress(double deltaTime)
        {
            m_stateTime += deltaTime;

            double x = m_stateTime / ACTION_POTENTIAL_TIME;
            double y = ((-4 * x * x) + (4 * x)) * (ACTION_POTENTIAL_VOLTAGE_MAX - m_threshold);

            m_voltage = m_threshold + y;

            // TODO: handle sustained supra thresholds
            if (x >= 1.0)
                SetState(State.RefactorPeriod);
        }

        public void RefactorPeriod_Enter()
        {
            m_stateTime = 0;
            m_voltage = IDLE_VOLTAGE;
        }

        public void RefactorPeriod_Progress(double deltaTime)
        {
             m_stateTime += deltaTime;

             if (m_stateTime >= m_refactorTime)
                 SetState(State.Resting);
        }

        public void ProgressSimulation(double deltaTime)
        {
            switch (m_state)
            {
                case State.Resting:
                    Resting_Progress(deltaTime);
                    break;

                case State.ActionPotential:
                    ActionPotential_Progress(deltaTime);
                    break;

                case State.RefactorPeriod:
                    RefactorPeriod_Progress(deltaTime);
                    break;
            }
        }

        public void SetState(State state)
        {
            m_state = state;

            switch (m_state)
            {
                case State.Resting:
                    Resting_Enter();
                    break;

                case State.ActionPotential:
                    ActionPotential_Enter();
                    break;

                case State.RefactorPeriod:
                    RefactorPeriod_Enter();
                    break;
            }
        }*/

        public enum State
        {
            Idle,
            ThresholdReached,
            ActionPotential,
            Recover
        }

        public void Step()
        {
            // Soliton therey points that
            // perhaps waves just move through neruons
            // unidirectional?
            // delay determined by connection strength? to represent distance

            m_time += 1.0;

            // needs to happen regardless of state to keep other neuron input at correct queue length
            m_inputStimulus = 0.0;
            foreach (Synapse connection in m_inputConnectionList)
            {
                m_inputStimulus += connection.PopStimulus();
            }

            switch (m_state)
            {
                case State.Idle:
                    {
                        // convert stimulus to a frequency
                        m_frequency = m_inputStimulus;

                        // queue an action potential
                        if (m_inputStimulus > m_threshold)
                        {
                            m_state = State.ThresholdReached;
                            m_time = 0.0;
                        }

                        break;
                    }

                case State.ThresholdReached:
                    {
                        if (m_time > THRESHOLD_DELAY)
                        {
                            m_state = State.ActionPotential;
                            m_outputStimulus = 1.0;
                            
                            m_time = 0.0;
                        }

                        break;
                    }

                case State.ActionPotential:
                    {
                        // if we exceed peak time, turn back down
                        if (m_time > ACTION_POTENTIAL_TIME)
                        {
                            m_outputStimulus = 0.0;
                            m_time = 0.0;
                            m_state = State.Recover;

                            // calculate recovery time
                            // this is the frequency, take the action potential time 
                            // take the threshold delay such that if the 
                            // stimulus to the neuron remains on
                            // the neruon will oscillate  at the correct frequency
                            m_recoverTime = 1000.0 / m_frequency;
                            m_recoverTime -= (ACTION_POTENTIAL_TIME + THRESHOLD_DELAY);
                        }
                        break;
                    }

                case State.Recover:
                    {
                        /*
                        double stimulus = GetStimulus();

                        // convert stimulus to a frequency
                        m_frequency = stimulus;

                        // queue an action potential
                        if (m_voltage > m_threshold)
                        {
                            m_state = State.ThresholdReached;
                            m_time = 0.0;
                        }

                        double toggleTime = 1000.0 / m_frequency;
                        toggleTime = (toggleTime - ACTION_POTENTIAL_TIME);
                        if (m_time > toggleTime)
                        {
                            SetOutputStimulus(100.0);
                            m_time = 0.0;
                        }
                        */
                        if (m_time >= 20.0) //m_recoverTime) // this should be a function of the input voltage
                        {
                            m_state = State.Idle;
                        }
                        break;
                    }
            }


            // needs to happen regardless of state to keep feeding other neurons
            foreach (Synapse connection in m_outputConnectionList)
            {
                connection.PushStimulus(m_outputStimulus);
            }
        }

        public double GetOutputStimulus()
        {
            return m_outputStimulus;
        }

        public void AddInputConnection(Synapse connection)
        {
            m_inputConnectionList.Add(connection);
        }

        public void AddOutputConnection(Synapse connection)
        {
            m_outputConnectionList.Add(connection);
        }

        public double GetThreshold()
        {
            return m_threshold;
        }

        public Neuron()
        {
            m_id = s_id;
            ++s_id;
            /*
            m_refactorTime = Math2.Random(REFACTOR_TIME_MIN, REFACTOR_TIME_MAX);
            m_threshold = Math2.Random(THRESHOLD_VOLTAGE_MIN, THRESHOLD_VOLTAGE_MAX);*/
        }

        public List<Synapse> GetInputConnectionList()
        {
            return m_inputConnectionList;
        }

        public List<Synapse> GetOutputConnectionList()
        {
            return m_outputConnectionList;
        }

        /*
        public double m_stateTime = 0.0;
        public State m_state = State.Resting;
        public double m_voltage = IDLE_VOLTAGE;
        public double m_threshold = THRESHOLD_VOLTAGE_MAX;
        public double m_refactorTime;*/

        public State m_state = State.Idle;
        public double m_time = 0.0;
        public double m_threshold = 0.0;
        public double m_frequency = 0.0; // Hertz
        public double m_recoverTime = 0.0;

        public List<Synapse> m_inputConnectionList = new List<Synapse>();
        public List<Synapse> m_outputConnectionList = new List<Synapse>();

        double m_inputStimulus = 0.0;
        double m_outputStimulus = 0.0;

        // for debugging
        int m_id;
        static int s_id = 0;

        public class IDComparer : IEqualityComparer<Neuron>
        {
            public int GetHashCode(Neuron a)
            {
                return a.m_id;
            }

            public bool Equals(Neuron a, Neuron b)
            {
                return a.m_id == b.m_id;
            }
        }
    }
}
