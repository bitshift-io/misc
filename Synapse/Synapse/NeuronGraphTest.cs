﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using System.Drawing;
using Synapse;

namespace Synapse
{
    //
    // Theories: Soliton
    //
    // Astrocyte stores memorys (how stored and retrived/used by neruon?), neurons are for learned behaviours + communication
    // Fabs theorey:
    // input cause neurons to fire, this patten is stored in astrocte. When pattern is trigggered again, it causes
    // a recall. such that inputs are mapped to certain ouputs.
    // could astrocytes seperate input neuron networks from output neuron networks. or simply feed thoughts back to nnets?
    //
    // or this? http://www.antanitus.com/hypothesis/ refer: the input changes the hardware

    class NeuronGraphTest
    {
        GraphLine m_line;
        Graph m_graph;
        Main m_form;
        System.Timers.Timer m_updateTimer;

        double I = 0.1; // magnitude of simulus
        double W = 0.0; // recovery rate (-1 to 1)
        double V = 0.0; // Voltage

        const int historyLength = 100;

        List<List<double>> m_voltageHistory = new List<List<double>>();
        List<double> m_stimulusHistory = new List<double>();

        NeuralNetworkView m_networkView;
        NeuralNetwork m_network;
        List<Neuron> m_neuronList;
        Synapse m_inputConnection;

        public NeuronGraphTest(Main form)
        {
            //Synapse.NeuralNetwork.Test();

            m_form = form;

            m_network = new NeuralNetwork();
            List<NeuronLayer> chain = NeuronLayer.CreateSimplyChain(10, 1, 10, NeuronLayer.ConnectionFlags.Forward);
            m_network.Add(chain);
            m_neuronList = m_network.GetNeuronList();

            for (int i = 0; i < m_neuronList.Count; ++i)
            {
                m_voltageHistory.Add(new List<double>());
            }

            m_inputConnection = new Synapse(m_neuronList[0], Synapse.Type.Input);
            m_inputConnection.PushStimulus(1.0);

            PictureBox pictureBox = m_form.GetGraphPictureBox();
            pictureBox.MouseClick += new MouseEventHandler(Graph_MouseClick);
            pictureBox.MouseMove += new MouseEventHandler(Graph_MouseMove);
            m_graph = new Graph(pictureBox);
            m_line = new GraphLine();

            m_networkView = new NeuralNetworkView(m_network, m_form.GetNeuronViewPictureBox());

            m_updateTimer = new System.Timers.Timer();
            m_updateTimer.Elapsed += new ElapsedEventHandler(UpdateTimerElapsed);
            m_updateTimer.Interval = 1000 / 30; // 30 times per second

            m_updateTimer.Start();
        }

        public void UpdateTimerElapsed(object source, ElapsedEventArgs e)
        {
            try
            {
                MethodInvoker mi = new MethodInvoker(Update);
                m_form.BeginInvoke(mi);
            }
            catch (Exception ex)
            {
            }
        }

        public void Update()
        {
            /*
            // http://epfellow.posterous.com/python-vs-mathematica-numerical-solving
            const double G_L = 0.3;
            const double E_L = 10.6;
            const double G_Na = 120;
            const double E_Na = 115;
            const double G_K = 36;
            const double E_K = -12;
            const double e =  Math.E; //math.e # Euler's constant
               
            double v0 = 0.0;
            double alpham = (0.1*(25-v0)) / (Math.Exp((25-v0)/10)-1);
            double betam = 4 * Math.Exp(-v0 / 18);
            double alphah = 0.07 * Math.Exp(-v0 / 20);
            double betah = 1 / (Math.Exp((30 - v0) / 10) + 1);
            double alphan = (0.01 * (10 - v0)) / (Math.Exp((10 - v0) / 10) - 1);
            double betan = 0.125 * Math.Exp(-v0 / 80);

            double m0 = alpham/(alpham + betam);
            double n0 = alphan/(alphan + betan);
            double h0 = alphah / (alphah + betah);



            double t = 0.0; //linspace(0,50,1000);	
            int i = 1;
            while (i < 500)
            {
	            y = odeint(diff_eqns, array([m0,h0,n0,v0]), t);
	            i = i+1;  
            }*/


            // http://www.scholarpedia.org/article/FitzHugh-Nagumo_model

            
            /*
            m_line.Clear();

            for (double t = 0.0; t < 1.0; t += 0.01)
            {
                m_line.Add(new PointF((float)t, ((float)V + 1.0f) * 0.5f)); // move up and scale V down

                V = V - Math.Pow(V, 3.0) / 3.0 - W + I;
                W = 0.08 * (V + 0.7 - 0.8 * W);
            }*/

            /*
            for (double t = 0.0; t < 1.0; t += 0.1)
            {
                m_line.Add(new PointF((float)t, (float)Math.Pow(t, 2.0)));
            }*/

            /*
            V = V - Math.Pow(V, 3.0) / 3.0 - W + I;
            W = 0.08 * (V + 0.7 - 0.8 * W);

            m_voltageHistory.Add(V);
            m_stimulusHistory.Add(I);
            */


            m_network.Step();

            for (int i = 0; i < m_neuronList.Count; ++i )
            {
                m_voltageHistory[i].Add(m_neuronList[i].GetOutputStimulus());
            }
            //m_stimulusHistory.Add(m_inputConnection.GetVoltage());

            // should have pushed enough to activate first neuron, lets see the results
            m_inputConnection.PushStimulus(0.0);

            // clear old history
            
            for (int i = 0; i < m_voltageHistory.Count; ++i)
            {
                while (m_voltageHistory[i].Count > historyLength)
                {
                    m_voltageHistory[i].RemoveAt(0);
                }
            }

            while (m_stimulusHistory.Count > historyLength)
            {
                m_stimulusHistory.RemoveAt(0);
            }

            Draw();
        }

        public void Draw()
        {
            m_networkView.Draw();

            Graphics g = m_graph.DrawBegin();
            m_graph.DrawAxis(g);

            m_graph.DrawInPlotAreaBegin(g);
            {

                Pen pen = null;
                double increment = 1.0 / (double)historyLength;
                for (int i = 0; i < m_voltageHistory.Count; ++i)
                {
                    Color color = Color.Black;

                    switch (i)
                    {
                        case 0:
                            color = Color.Blue;
                            break;

                        case 1:
                            color = Color.Green;
                            break;

                        case 2:
                            color = Color.Red;
                            break;
                    }
                    //color = Color.FromArgb(50, color);
                    pen = new Pen(new SolidBrush(color));
                    pen.Width = 0.0001f;

                    PointF? prevPoint = null;
                    for (int h = 0; h < m_voltageHistory[i].Count; ++h)
                    {
                        double t = increment * h;

                        PointF point = new PointF((float)t, (float)m_voltageHistory[i][h]);

                        if (prevPoint.HasValue)
                        {
                            g.DrawLine(pen, prevPoint.Value, point);
                        }

                        prevPoint = point;
                    }
                }

                /*
                // draw stimulus
                pen = new Pen(new SolidBrush(Color.Orange));
                pen.Width = 0.0001f;

                PointF? prevPoint = null;
                for (int i = 0; i < m_stimulusHistory.Count; ++i)
                {
                    double t = increment * i;

                    PointF point = new PointF((float)t, (float)m_stimulusHistory[i]);

                    if (prevPoint.HasValue)
                    {
                        g.DrawLine(pen, prevPoint.Value, point);
                    }

                    prevPoint = point;
                }*/

                // draw 0 line
                pen = new Pen(new SolidBrush(Color.Gray));
                pen.Width = 0.0001f;
                g.DrawLine(pen, new PointF(0.0f, 0.0f), new PointF(1.0f, 0.0f));

                /*
                // http://www.scholarpedia.org/article/FitzHugh-Nagumo_model

                double I = -0.1; // magnitude of simulus
                double W = 0.0; // recovery rate (-1 to 1)
                double V = 0.0; // Voltage

                Pen pen = new Pen(new SolidBrush(Color.Blue)));

                Point? prevPoint = null;
                for (double t = 0.0; t < 1.0; t += 0.01)
                {
                    //m_line.Add(new PointF((float)t, ((float)V + 1.0f) * 0.5f)); // move up and scale V down

                    PointF point = new PointF((float)t, (float)V);
                    g.DrawLine(pen, previousPoint.Value, point);

                    
                    
                }*/

                //m_line.Draw(g, m_graph);
            }

            m_graph.DrawEnd(g);
        }

        public void Graph_MouseClick(object sender, EventArgs e)
        {
            PointF point = m_graph.ScreenPointToPlotArea(System.Windows.Forms.Cursor.Position);
            I = point.Y;
        }

        public void Graph_MouseMove(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            if (me.Button != MouseButtons.Left)
                return;

            PointF point = m_graph.ScreenPointToPlotArea(System.Windows.Forms.Cursor.Position);
            I = point.Y;
        }
    }
}
