#include "System.h"

unsigned char inportb(unsigned short _port)
{
    unsigned char rv;
    __asm__ __volatile__ ("inb %1, %0" : "=a" (rv) : "dN" (_port));
    return rv;
}

void outportb(unsigned short _port, unsigned char _data)
{
    __asm__ __volatile__ ("outb %1, %0" : : "dN" (_port), "a" (_data));
}

unsigned long int inportl(unsigned short _port)
{
	unsigned long rv;
	__asm__ __volatile__("inl %%dx, %%eax" : "=a" (rv) : "dN" (_port)); 
    return rv;
}

void outportl(unsigned short _port, unsigned long int _data)
{
	__asm__ __volatile__("outl %%eax, %%dx" : : "d" (_port), "a" (_data));
}

unsigned short int inportw(unsigned short _port)
{
	unsigned int rv;
    __asm__ __volatile__ ("inw %1, %0" : "=a" (rv) : "dN" (_port));
    return rv;
}

void outportw(unsigned short _port, unsigned short int _data)
{
	__asm__ __volatile__ ("outw %1, %0" : : "dN" (_port), "a" (_data));
}



unsigned short ConvertEndian(unsigned short i)
{
    return ((i>>8)&0xff)+((i << 8)&0xff00);
}

unsigned long ConvertEndian(unsigned long i)
{
    return((i&0xff)<<24)+((i&0xff00)<<8)+((i&0xff0000)>>8)+((i>>24)&0xff);
}