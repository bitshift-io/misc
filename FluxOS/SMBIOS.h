#ifndef _SMBIOS_H_
#define _SMBIOS_H_

//
// This provides extended BIOS info, to determine
// extra info about your machine
// http://www.dmtf.org/standards/documents/SMBIOS/DSP0134.pdf
//
namespace SMBIOS
{

struct SystemInfo
{
	int		memorySize; // amount of ram
	int		cpuSpeed;	// cpu speed
	int		cpuCores;	// num cpu cores
};

void Init();

const char* GetString(const char* stringTableAddr, int index);
};

#endif
