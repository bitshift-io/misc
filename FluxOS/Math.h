#ifndef _MATH_H_
#define _MATH_H_

#define Min(x, y)	(x > y) ? y : x
#define Max(x, y)	(x > y) ? x : y

#define Clamp(x, y, z)	Min(Max(x, y), z)

#endif
