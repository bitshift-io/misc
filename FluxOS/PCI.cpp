#include "PCI.h"
#include "String.h"
#include "Video.h"
#include "Memory.h"

namespace PCI
{

#define PCI_READ_CONFIG_BYTE	0xB108
#define PCI_READ_CONFIG_WORD	0xB109
#define PCI_READ_CONFIG_DWORD	0xB10A
#define PCI_WRITE_CONFIG_BYTE	0xB10B
#define PCI_WRITE_CONFIG_WORD	0xB10C
#define PCI_WRITE_CONFIG_DWORD	0xB10D

#define	PCI_ADR_REG		0xCF8
#define	PCI_DATA_REG		0xCFC

enum PCI
{
	PCI_Multifunction = 0x80,
};

struct BIOS32Info
{
	char			anchorString[4];
	long int		entryAddress;	// BIOS32 entry point 
	char			revision;		// BIOS32 revision
	unsigned char	length;			// structure len (in 16-byte paragraphs)
	unsigned char	checksum;		// checksum (checksum of entire structure must be 0)
} __attribute__((packed));

// http://www.acm.uiuc.edu/sigops/roll_your_own/7.c.0.html
struct PCIConfigHeader
{
	unsigned short vendor;
	unsigned short device;
	short int	command;
	short int	status;
	char		revision;

	// Class Code Register
	char		progIF;
	char		subClassCode;
	char		classCode;

	char		cacheLineSize;
	char		latency;
	char		header;
	char		builtInSelfTest;
	long long	baseAddressRegister[6];
	long long	cardBusCISPointer;
	short int	subSystemVendor;
	short int	subSystem;
	long long	expansionROMAddress;
	long long	reserved[2];
	char		irqLine;
	char		interuptPin;
	char		minGrant;
	char		maxLatency;
} __attribute__((packed));

Device sDevice[256];
unsigned short sDeviceCount;


// http://my.execpc.com/~geezer/osd/pnp/pci.c
// http://www.acm.uiuc.edu/sigops/roll_your_own/pci.h <-- linux stuff
static int pci_detect(void)
{
	Video_Format("PCI controller...");
	// poke 32-bit I/O register at 0xCF8 to see if
	// there's a PCI controller there 
	outportl(PCI_ADR_REG, 0x80000000L); // bus 0, dev 0, fn 0, reg 0 
	if (inportl(PCI_ADR_REG) != 0x80000000L)
	{
		Video_Format("not found\n");
		return -1;
	}
	Video_Format("found\n");
	return 0;
}

static int pci_read_config_word(Device *pci, unsigned reg,
		unsigned short *val)
{
	outportl(PCI_ADR_REG, 0x80000000L |
		((unsigned long)pci->bus << 16) |
		((unsigned)pci->dev << 11) |
		((unsigned)pci->fn << 8) | (reg & ~3));
	*val = inportw(PCI_DATA_REG + (reg & 2));
	return 0;
}

static int pci_read_config_dword (Device *pci, unsigned reg, unsigned long *val)
{
	outportl(PCI_ADR_REG, 0x80000000L |
		((unsigned long)pci->bus << 16) |
		((unsigned)pci->dev << 11) |
		((unsigned)pci->fn << 8) | (reg & ~3));
	*val = inportl(PCI_DATA_REG + 0);
	return 0;
}

static int pci_write_config_byte(Device *pci, unsigned reg,
		unsigned char val)
{
	outportl(PCI_ADR_REG, 0x80000000L |
		((unsigned long)pci->bus << 16) |
		((unsigned)pci->dev << 11) |
		((unsigned)pci->fn << 8) | (reg & ~3));
	outportb(PCI_DATA_REG + (reg & 3), val);
	return 0;
}

static int pci_read_config_byte(Device *pci, unsigned reg,
		unsigned char *val)
{
	outportl(PCI_ADR_REG,
		0x80000000L | /* "enable configuration space mapping" */
		((unsigned long)pci->bus << 16) |	/* b23-b16=bus */
		((unsigned)pci->dev << 11) |		/* b15-b11=dev */
		((unsigned)pci->fn << 8) |		/* b10-b8 =fn  */
		(reg & ~3));				/* b7 -b2 =reg */
	*val = inportb(PCI_DATA_REG + (reg & 3));// xxx - is this legit?
	return 0;
}

bool GetPCIHeader(PCIConfigHeader& header, Device& pci)
{
	for (unsigned int i = 0; i < sizeof(PCIConfigHeader); i += sizeof(char))
	{
		int err = pci_read_config_byte(&pci, i, (unsigned char*)&header + i);
		if (err)
			return false;
	}

	// check if its valid
	if (header.vendor == 0xFFFF && header.device == 0xFFFF)
		return false;

	return true;
}


Base Device::GetBase(unsigned short index)
{
	long long baseAddr = GetBaseAddress(index);
	Base base;
	if (baseAddr & 0x1)
	{
		base.space = S_IO;
		base.type = 0;
		base.prefetch = 0;
		base.address = baseAddr & (~0x03);
	}
	else
	{
		base.space = S_Memory;
		base.type = (baseAddr >> 1) & 0x03;
		base.prefetch = (baseAddr >> 3) & 0x01; 
		base.address = baseAddr & (~0x0f);
	}
	return base;
}

long long Device::GetBaseAddress(unsigned short index)
{
	PCIConfigHeader header;
	unsigned long baseAddrOffset = (unsigned long)&header.baseAddressRegister[index] - (unsigned long)&header;

	for (unsigned long i = baseAddrOffset; i < baseAddrOffset + sizeof(long long); i += sizeof(char))
	{
		int err = pci_read_config_byte(this, i, (unsigned char*)&header + i);
		if (err)
			return -1;
	}

	return header.baseAddressRegister[index];
}

bool Device::SetBaseAddress(unsigned short index, long long value)
{
	PCIConfigHeader header;
	header.baseAddressRegister[index] = value;
	unsigned long baseAddrOffset = (unsigned long)&header.baseAddressRegister[index] - (unsigned long)&header;

	for (unsigned long i = baseAddrOffset; i < baseAddrOffset + sizeof(long long); i += sizeof(char))
	{
		int err = pci_write_config_byte(this, i, *((unsigned char*)&header + i));
		if (err)
			return false;
	}

	return true;
}



bool GetNextDevice(Device& pci, PCIConfigHeader& header);

bool Init()
{
	//Video_Format("---------------------------------\n");

	

	bool bFound = false;
	BIOS32Info* pInfo = (BIOS32Info*)0xE0000;
	while (pInfo < (BIOS32Info*)0xFFFFF)
	{
		pInfo = (BIOS32Info*)(((char*)pInfo) + 16); // add 16bytes
		
		if (String::Compare("_32_", pInfo->anchorString, 4) == -1)
		{
			if (pInfo->length == 0)
				continue;

			/*
			unsigned char csum = 0;
			for (int i = 0; i < pInfo->length; i++)
				csum += (unsigned char)(((char*)pInfo)[i]);

			if (csum != 0)
				continue;
			*/

			Video_Format("BIOS32 found\n");
			bFound = true;
			break;
		}
	}

	if (!bFound)
		return false;


	pci_detect();

	Device pci;
	PCIConfigHeader header;
	int err;

	/* display numeric ID of all PCI devices detected */
	memset(&pci, 0, sizeof(pci));
	
	sDeviceCount = 0;

	do
	{
		if (!GetPCIHeader(header, pci))
		{
			continue;
		}

		// generate device structs
		Device* pDevice = &sDevice[sDeviceCount];
		*pDevice = pci;
		pDevice->vendor = header.vendor;
		pDevice->device = header.device;

		//for (int i = 0; i < 6; ++i)
		//	pDevice->baseAddressRegister[i] = header.baseAddressRegister[i];

		++sDeviceCount;

		Video_Format("bus %u, device %u, function %u: "
			"vendor %x, device=%x\n", pci.bus,
			pci.dev, pci.fn, header.vendor, header.device);

	} while (!GetNextDevice(pci, header));

	return true;
}

bool ScanBus(int bus)
{
	PCIConfigHeader header;
	Device pci;
	for (int d = 0; d < 32; ++d)
	{
		pci.dev = d;
		pci.fn = 0;
		pci.bus = bus;

		if (!GetPCIHeader(header, pci))
			continue;

		// check if its multifunction
		if (header.header & PCI_Multifunction == PCI_Multifunction)
		{
			for (int f = 0; f < 7; ++f)
				ScanFunction(bus, d, f);
		}
		else
		{
			ScanFunction(bus, d, 0);
		}
	}
}

bool ScanFunction(int bus, int device, int function)
{
	PCIConfigHeader header;
	Device pci;
	pci.dev = device;
	pci.fn = function;
	pci.bus = bus;

	if (bus == 0 && device == 0)
		return HandleHostController(bus, device, function);

	if (!GetPCIHeader(header, pci))
		return true;

	// check if its multifunction
	if (header.header & PCI_Multifunction == PCI_Multifunction)
	{
		HandlePCIToPCIBridge(bus, device, function);
	}

	Video_Format("bus %u, device %u, function %u: "
				"vendor %x, device=%x\n", pci.bus,
				pci.dev, pci.fn, header.vendor, header.device);

	return true;
}

bool HandlePCIToPCIBridge(int bus, int device, int function)
{
	ScanBus(bus + 1); 
}

bool HandleHostController(int bus, int device, int function)
{
	ScanBus(bus + 1); 
}

bool GetNextDevice(Device& pci, PCIConfigHeader& header)
{

	unsigned char hdr_type = 0x80;

	/* if first function of this device, check if multi-function device
	(otherwise fn==0 is the _only_ function of this device) */
	if(pci.fn == 0)
	{
		if(pci_read_config_byte(&pci, 0x0E, &hdr_type))
		{
			return false;	/* error */
		}
	}
/* increment iterators
fn (function) is the least significant, bus is the most significant */
	pci.fn++;
	if (pci.fn >= 8 || (hdr_type & 0x80) == 0)
	{
		pci.fn = 0;
		pci.dev++;
		if(pci.dev >= 32)
		{
			pci.dev = 0;
			pci.bus++;
//			if(pci->bus > g_last_pci_bus)
			if(pci.bus > 7)
				return true; /* done */
		}
	}
	return false;	
}

Device* FindDevice(unsigned short int device, unsigned short int vendor)
{
	for (int i = 0; i < sDeviceCount; ++i)
	{
		if (sDevice[i].device == device && sDevice[i].vendor == vendor)
			return &sDevice[i];
	}

	return 0;
}

};
