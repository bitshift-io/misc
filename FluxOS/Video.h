#ifndef _VIDEO_H_
#define _VIDEO_H_

enum Video_Color
{
	C_Black,
	C_Blue,
	C_Green,
	C_Cyan,
	C_Red,
	C_Magenta,
	C_Brown,
	C_LightGrey,
	C_DarkGrey,
	C_LightBlue,
	C_LightGreen,
	C_LightCyan,
	C_LightRed,
	C_LightMagenta,
	C_LightBrown,
	C_White
};

extern void Video_Scroll();
extern void Video_MoveCursor();
extern void Video_Clear();
extern void Video_PutChar(char c);
extern void Video_PutString(const char *text);
extern void Video_Format(const char *text, ...);
extern void Video_SetTextColor(char /*Video_Color*/ forecolor, char /*Video_Color*/ backcolor);
extern void Video_Init();

#endif
