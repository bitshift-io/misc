export PATH=$PATH:/usr/cross/bin

del *.bin

nasm -f elf -o start.o start.asm

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o main.o main.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o video.o video.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o gdt.o gdt.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o idt.o idt.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o isrs.o isrs.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o irq.o irq.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o harddrive.o harddrive.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o timer.o timer.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o kb.o kb.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o string.o string.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o memory.o memory.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o system.o system.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o command.o command.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o smbios.o smbios.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o pci.o pci.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o fat32.o fat32.cpp

g++ -b i586-elf -V 4.0.2 -Wall -O -fno-rtti -fno-exceptions -fstrength-reduce -fomit-frame-pointer -finline-functions -nostdinc -fno-builtin -I./include -c -o svga.o svga.cpp

i586-elf-ld -T link.ld -o kernel.bin start.o main.o timer.o video.o gdt.o idt.o isrs.o irq.o kb.o string.o memory.o system.o harddrive.o command.o smbios.o pci.o fat32.o svga.o

del *.o

