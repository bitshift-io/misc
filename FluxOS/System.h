#ifndef _SYSTEM_H_
#define _SYSTEM_H_


typedef char* va_list;

#define _INTSIZEOF(n)   ( (sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1) )

#define va_start(ap,v)  ( ap = (va_list)&(v) + _INTSIZEOF(v) )
#define va_arg(ap,t)    ( *(t *)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t)) )
#define va_end(ap)      ( ap = (va_list)0 )




/* This defines what the stack looks like after an ISR was running */
struct regs
{
    unsigned int gs, fs, es, ds;
    unsigned int edi, esi, ebp, esp, ebx, edx, ecx, eax;
    unsigned int int_no, err_code;
    unsigned int eip, cs, eflags, useresp, ss;    
};

extern unsigned char inportb(unsigned short _port);
extern void outportb(unsigned short _port, unsigned char _data);

extern unsigned long int inportl(unsigned short _port);
extern void outportl(unsigned short _port, unsigned long int _data);

extern unsigned short int inportw(unsigned short _port);
extern void outportw(unsigned short _port, unsigned short int _data);


/* GDT.C */
extern void gdt_set_gate(int num, unsigned long base, unsigned long limit, unsigned char access, unsigned char gran);
extern void gdt_install();

/* IDT.C */
extern void idt_set_gate(unsigned char num, unsigned long base, unsigned short sel, unsigned char flags);
extern void idt_install();

/* ISRS.C */
extern void isrs_install();

/* IRQ.C */
extern void irq_install_handler(int irq, void (*handler)(struct regs *r));
extern void irq_uninstall_handler(int irq);
extern void irq_install();

/* TIMER.C */
//extern void timer_wait(int ticks);
//extern void timer_install();

/* KEYBOARD.C */
extern void keyboard_install();

#define cli() __asm__ __volatile__ ("cli\n\t")
#define sti() __asm__ __volatile__ ("sti\n\t")

#define halt() __asm__ __volatile__ ("cli;hlt\n\t");
#define idle() __asm__ __volatile__ ("jmp .\n\t");

// 2-byte number
unsigned short ConvertEndian(unsigned short i);
// 4-byte number
unsigned long ConvertEndian(unsigned long i);

#define CONVERT_ENDIAN(x)	x = ConvertEndian(x)

#define LOWORD(x)			0xffff & x
#define HIWORD(x)			0xffff & (x >> 8)

#endif
