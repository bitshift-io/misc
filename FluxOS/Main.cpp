#include "System.h"
#include "Video.h"
#include "Command.h"
#include "HardDrive.h"
#include "Memory.h"
#include "SMBIOS.h"
#include "PCI.h"
#include "Timer.h"
#include "FAT32.h"
#include "SVGA.h"

void Display_InitTitle(const char* str)
{
	Video_Format("------ [");
	Video_SetTextColor(C_Green, C_Black);	
	Video_Format("%s", str);
	Video_SetTextColor(C_LightGrey, C_Black);
	Video_Format("] -------\n");
}

void Display_InitResult(bool status)
{	
	Video_Format("------ [");
	if (status)
	{
		Video_SetTextColor(C_Green, C_Black);
		Video_Format("SUCCESSFUL");
		Video_SetTextColor(C_LightGrey, C_Black);
	}
	else
	{
		Video_SetTextColor(C_Red, C_Black);
		Video_Format("FAILED");
		Video_SetTextColor(C_LightGrey, C_Black);
	}

	Video_Format("] -------\n\n");
}

int main()
{
    gdt_install();
    idt_install();
    isrs_install();
    irq_install();

    Video_Init();	
	Display_InitTitle("Video Init");
	Display_InitResult(true);
	
	Display_InitTitle("Timer Init");
	Timer::Init();
	Display_InitResult(true);

	Display_InitTitle("Keyboard Init");
    keyboard_install();
	Display_InitResult(true);

	Display_InitTitle("PCI Init");
	Display_InitResult(PCI::Init());

	Display_InitTitle("SVGA Init");
	Display_InitResult(SVGA::Init());

	sti();

	Display_InitTitle("Hard Drive Init");
	HardDrive::Init();
	Display_InitResult(true);

	Display_InitTitle("Memory Init");
	Memory::Init();
	Display_InitResult(true);

	Display_InitTitle("FAT32 Init");
	Display_InitResult(FS::Init());

	//Display_InitTitle("SMBIOS");
	//SMBIOS::Init();
	//Display_InitResult(true);

	//SVGA::SetVideoMode(800, 600, 32);


	Command::DrawPrompt();
	

    for (;;);

	return 0;
}
