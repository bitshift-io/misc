#include "Memory.h"
#include "System.h"
#include "Video.h"
#include "String.h"

void *memcpy(void *dest, const void *src, int count)
{
    const char *sp = (const char *)src;
    char *dp = (char *)dest;
    for(; count != 0; count--) *dp++ = *sp++;
    return dest;
}

void *memset(void *dest, char val, int count)
{
    char *temp = (char *)dest;
    for( ; count != 0; count--) *temp++ = val;
    return dest;
}

unsigned short *memsetw(unsigned short *dest, unsigned short val, int count)
{
    unsigned short *temp = (unsigned short *)dest;
    for( ; count != 0; count--) *temp++ = val;
    return dest;
}


namespace Memory
{
	
//static char mmap[M_MemorySize / M_PageSize] = {M_Reversed, };

struct PageDirectory
{
	unsigned int value;
};

struct PageEntry
{
	unsigned int value;
};

struct PageDirectory pageDirectory[M_MaxTasks] __attribute__ ((__aligned__ (4096)));
struct PageEntry pageTable[M_NumPages];

static char mmap[M_MemorySize / M_PageSize] = {M_Reversed, };

void PageFaultHandler(struct regs *r)
{
	unsigned int cr2, cr3;
	__asm__ ("movl %%cr2, %%eax":"=a"(cr2));
	__asm__ ("movl %%cr3, %%eax":"=a"(cr3));

	if (cr2 != 0)
	{
		char buffer[256];
		String::Format(buffer, 256, "Page Fault - Virtual Address: %i, caused by %i\n", cr2, cr3);
		Video_PutString(buffer);
	}

	AllocPage(M_Normal);
}

void Init()
{
	/* Installs 'timer_handler' to IRQ0 */
    irq_install_handler(0, PageFaultHandler);

	unsigned int address = 0;

	int i;
	for (i = 0; i < M_NumPages; ++i) 
	{
		// attribute set to: kernel, r/w, present
		pageTable[i].value = address | 7;
		address += M_PageSize;
	}

	pageDirectory[0].value = (unsigned int)pageTable | 7;

	for (i = 1; i < M_MaxTasks; ++i)
		pageDirectory[i].value = 6;

	// set lower 1MB memory to used
	for (i = ((1*1024*1024) / M_PageSize) - 1; i >= 0; --i)
		mmap[i] = M_Reversed;

	__asm__ 
	(
		"movl	%%eax,	%%cr3\n\t"
		"movl	%%cr0,	%%eax\n\t"
		"orl	$0x80000000,	%%eax\n\t"
		"movl	%%eax,	%%cr0" :: "a" (pageDirectory)
	);
}


unsigned int AllocPage(int type) 
{/*
	int i;
	for (i = (sizeof mmap) - 1; i >= 0 && mmap[i]; --i)
	{
	}


	if (i < 0) 
	{
		Video_PutString("Out of Memory");
		while (1) {}
	}

	mmap[i] = type;
	return i;*/
	return 1;
}

};
