#ifndef _COMMAND_H_
#define _COMMAND_H_

namespace Command
{
	void	DrawPrompt();
	void	Execute(const char* str);

	void	ListDirectory(const char* args);
	void	ChangeDirectory(const char* args);
	void	AddDirectory(const char* args);
	void	RemoveDirectory(const char* args);

	void	DumpFile(const char* args);
	void	DumpFileString(const char* args);

};

#endif
