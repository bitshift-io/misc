#include "SVGA.h"
#include "PCI.h"
#include "System.h"
#include "Video.h"
#include "Memory.h"

namespace SVGA
{

#define VMWARE_VENDOR_ID 0x15AD 
#define SVGA_DEVICE_ID	 0x405

#define SVGA_VERSION_2     2
#define SVGA_ID_2          SVGA_MAKE_ID(SVGA_VERSION_2)
#define SVGA_VERSION_1     1
#define SVGA_ID_1          SVGA_MAKE_ID(SVGA_VERSION_1)
#define SVGA_MAGIC         0x900000
#define SVGA_MAKE_ID(ver)  (SVGA_MAGIC << 8 | (ver))
#define SVGA_VERSION_0     0
#define SVGA_ID_0          SVGA_MAKE_ID(SVGA_VERSION_0)

#define SVGA_INDEX_PORT      0x0
#define SVGA_VALUE_PORT      0x1 

enum 
{
   SVGA_REG_ID = 0,
   SVGA_REG_ENABLE = 1,
   SVGA_REG_WIDTH = 2,
   SVGA_REG_HEIGHT = 3,
   SVGA_REG_MAX_WIDTH = 4,
   SVGA_REG_MAX_HEIGHT = 5,
   SVGA_REG_DEPTH = 6,
   SVGA_REG_BITS_PER_PIXEL = 7,
   SVGA_REG_PSEUDOCOLOR = 8,
   SVGA_REG_RED_MASK = 9,
   SVGA_REG_GREEN_MASK = 10,
   SVGA_REG_BLUE_MASK = 11,
   SVGA_REG_BYTES_PER_LINE = 12,
   SVGA_REG_FB_START = 13,
   SVGA_REG_FB_OFFSET = 14,
   SVGA_REG_VRAM_SIZE = 15,
   SVGA_REG_FB_SIZE = 16,

   SVGA_REG_CAPABILITIES = 17,
   SVGA_REG_MEM_START = 18,
   SVGA_REG_MEM_SIZE = 19,
   SVGA_REG_CONFIG_DONE = 20,
   SVGA_REG_SYNC = 21,
   SVGA_REG_BUSY = 22,
   SVGA_REG_GUEST_ID = 23,
   SVGA_REG_CURSOR_ID = 24,
   SVGA_REG_CURSOR_X = 25,
   SVGA_REG_CURSOR_Y = 26,
   SVGA_REG_CURSOR_ON = 27,
   SVGA_REG_HOST_BITS_PER_PIXEL = 28,
   SVGA_REG_TOP = 30,
   SVGA_PALETTE_BASE = 1024
};

//PCI::Device* sDevice;
unsigned short sIndexPort;
unsigned short sValuePort;
unsigned long* sFrameBuffer;

unsigned short	sWidth;
unsigned short	sHeight;
unsigned char	sBPP;

void SVGAOut(unsigned short index, unsigned long value)
{
	outportl(sIndexPort, index);
	outportl(sValuePort, value);
}

unsigned long SVGAIn(unsigned short index)
{
	outportl(sIndexPort, index);
	return inportl(sValuePort);
} 

bool Init()
{
	PCI::Device* pDevice = PCI::FindDevice(SVGA_DEVICE_ID, VMWARE_VENDOR_ID);

	if (!pDevice)
		return false;

	PCI::Base base = pDevice->GetBase(0);
	sIndexPort = base.address + SVGA_INDEX_PORT;
    sValuePort = base.address + SVGA_VALUE_PORT; 

	Video_Format("Index port: 0x%x		Value port: 0x%x\n", sIndexPort, sValuePort);

	SVGAOut(SVGA_REG_ID, SVGA_ID_2);
	if (SVGAIn(SVGA_REG_ID) !=  SVGA_ID_2)
	{
		SVGAOut(SVGA_REG_ID, SVGA_ID_1);
		if (SVGAIn(SVGA_REG_ID) != SVGA_ID_1)
		{
			SVGAOut(SVGA_REG_ID, SVGA_ID_0);
			if (SVGAIn(SVGA_REG_ID) != SVGA_ID_0)
			{
				return false;
			}
		}
	}

	sFrameBuffer = (unsigned long*)SVGAIn(SVGA_REG_FB_START);
	Video_Format("Frame buffer 0x%lX\n", sFrameBuffer);
	return true;
}

bool SetVideoMode(unsigned short width, unsigned short height, unsigned char bpp)
{
	SVGAOut(SVGA_REG_WIDTH, width);
    SVGAOut(SVGA_REG_HEIGHT, height);
    SVGAOut(SVGA_REG_BITS_PER_PIXEL, bpp);

	sWidth = width;
	sHeight = height;
	sBPP = bpp;

	SVGAOut(SVGA_REG_ENABLE, 1);

	Clear(Colour(0, 255, 0, 0));
	return true;
}

void Clear(const Colour& colour)
{
	for (unsigned short h = 0; h < sHeight; ++h)
		for (unsigned short w = 0; w < sWidth; ++w)
			PutPixel(w, h, colour);
}

void PutPixel(unsigned short x, unsigned short y, const Colour& colour)
{
	unsigned char* pFrameBuffer = (unsigned char*)sFrameBuffer;
	unsigned char* pPixel = &(pFrameBuffer[((sWidth * y) + x) * (sBPP / 8)]);
	pPixel[0] = colour.r;
	pPixel[1] = colour.g;
	pPixel[2] = colour.b;

	if (sBPP == 32)
		pPixel[3] = colour.a;
}

};
