#ifndef _HARDDRIVE_H_
#define _HARDDRIVE_H_

namespace HardDrive
{

#define HD_PORT_DATA         0
#define HD_PORT_ERROR        1
#define HD_PORT_SECT_COUNT   2
#define HD_PORT_SECT_NUM     3
#define HD_PORT_CYL_LOW      4
#define HD_PORT_CYL_HIGH     5
#define HD_PORT_DRV_HEAD     6
#define HD_PORT_STATUS       7
#define HD_PORT_COMMAND      7

#define HD_READ              0x20
#define HD_WRITE             0x30

#define HD_BSY		1 << 7
#define HD_DRDY		1 << 6
#define HD_DF		1 << 5
#define HD_DSC		1 << 4
#define HD_DRQ		1 << 3
#define HD_CORR		1 << 2
#define HD_IDX		1 << 1
#define HD_ERR		1 << 0



struct Device
{
	unsigned short	cylinders;
	unsigned short	heads;
	unsigned short	sectors;
	unsigned short	portStart;
	unsigned short	driveNum;
	unsigned long	totalSectors;	
};

void			Init();
bool			InitDevice(unsigned short port, unsigned short writeReg, unsigned short readReg, unsigned short portStart);

int				GetDeviceCount();
const Device*	GetDevice(int index);

void			ReadSector(const Device* device, unsigned int lbaAddr, char* buffer, unsigned int sectorCount = 1);
void			WriteSector(const Device* device, unsigned int lbaAddr, char* buffer, unsigned int sectorCount = 1);

void			LBAToCHS(const Device* device, unsigned int lbaAddr, unsigned int& cylinder, unsigned int& head, unsigned int& sector);
void			CHSToLBA(const Device* device, unsigned int cylinder, unsigned int head, unsigned int sector, unsigned int& lbaAddr);

};

#endif
