#include "FAT32.h"
#include "HardDrive.h"
#include "Video.h"
#include "Memory.h"
#include "System.h"
#include "String.h"
#include "Math.h"

namespace FS
{

#define MBR_BOOT_SIGNATURE		0xAA55
#define FIS_SIGNATURE			0x52526141

#define PARTITION_ACTIVESTATE	0x80
#define PARTITION_TYPE_FAT32	0x0B

enum FATType
{
	FT_FAT12,
	FT_FAT16,
	FT_FAT32,
};

FATType fatType;

MasterBootRecord mbr;
BootRecord bootRecord;

bool Init()
{
	
	HardDrive::ReadSector(HardDrive::GetDevice(0), 0, (char*)&mbr);

	// prove it
	for (int i = 0; i < 4; ++i)
	{
		if (mbr.partitions[i].lbaStart != 0)
			Video_Format("Partition #%i - lba address: %u - state: %x - heads: %u\n", i, mbr.partitions[i].lbaStart, mbr.partitions[i].state, mbr.partitions[i].startHead);
	}

	if (mbr.bootRecordSignature == MBR_BOOT_SIGNATURE)
	{
		Video_Format("MBR is valid\n");

		// prove it
		for (int i = 0; i < 4; ++i)
		{
			if (mbr.partitions[i].state == PARTITION_ACTIVESTATE && mbr.partitions[i].type == PARTITION_TYPE_FAT32)
			{
				unsigned int partitionSize = (mbr.partitions[i].numSectors * 512) / (1024 * 1024 * 1024);
				Video_Format("Partition #%i Active - FAT32 - size: %uMB\n", i, partitionSize);
			}
		}
	}
	else
	{
		Video_Format("MBR is invalid. Please format the drive and reboot\n");
		return false;
	}

	// check for valid FAT32
	// if not valid, format the drive to be valid
	HardDrive::ReadSector(HardDrive::GetDevice(0), mbr.partitions[0].lbaStart, (char*)&bootRecord);

	if (bootRecord.bootRecordSignature != MBR_BOOT_SIGNATURE)
	{
		Video_Format("FAT32 Not detected\n");
		return false;
	}

	unsigned long rootDirSectors = 0;

	// let read the first partitions FIS, second sector of the partition
	FileInformationSystem fis;
	HardDrive::ReadSector(HardDrive::GetDevice(0), mbr.partitions[0].lbaStart + bootRecord.FSInfoSectorNum, (char*)&fis);

	//Video_Format("FIS Sig: %lX\n", fis.signature);
	//Video_Format("BRS Sig: %lX\n", fis.bootRecordSignature);

	if (fis.signature != FIS_SIGNATURE || fis.bootRecordSignature != MBR_BOOT_SIGNATURE)
	{
		Video_Format("File information system is invalid in partition #0\n");
		//return false;
	}

	unsigned long fatSize = 0;
	unsigned long totalSectors = 0;
	if (bootRecord.sectorsPerFAT16 != 0)
		fatSize = bootRecord.sectorsPerFAT16;
	else
		fatSize = bootRecord.sectorsPerFAT32; 

	if (bootRecord.totalSectors16 != 0)
		totalSectors = bootRecord.totalSectors16;
	else
		totalSectors = bootRecord.totalSectors32;
		
	unsigned long dataSectors = totalSectors - (bootRecord.reservedSectors + (bootRecord.numberOfCopiesOfFAT * fatSize));
	unsigned long clusterCount = dataSectors / bootRecord.sectorsPerCluster;

	if (clusterCount < 4085)
	{
		Video_Format("Volume is FAT12\n");
		fatType = FT_FAT12;
	}
	else if (clusterCount < 65525)
	{
		Video_Format("Volume is FAT16\n");
		fatType = FT_FAT16;
	}
	else
	{
		Video_Format("Volume is FAT32\n");
		fatType = FT_FAT32;
	}

	return true;
}

unsigned long GetSector(unsigned long cluster)
{
	unsigned long fatSize = 0;
	if (bootRecord.sectorsPerFAT16 != 0)
		fatSize = bootRecord.sectorsPerFAT16;
	else
		fatSize = bootRecord.sectorsPerFAT32; 

	unsigned long firstDataSector = bootRecord.reservedSectors + (bootRecord.numberOfCopiesOfFAT * fatSize);
	unsigned long firstSectorOfCluster = ((cluster-2) * bootRecord.sectorsPerCluster) + firstDataSector;

	firstSectorOfCluster += mbr.partitions[0].lbaStart;
	return firstSectorOfCluster;
}

bool GetDirectory(Directory& directory, unsigned long sector, unsigned long offset)
{
	char buffer[512];
	HardDrive::ReadSector(HardDrive::GetDevice(0), sector, buffer);
	memcpy(&directory, &buffer[offset], sizeof(Directory));
	return true;
}

bool SetDirectory(Directory& directory, unsigned long sector, unsigned long offset)
{
	Video_Format("Setting directory '%s'\n", directory.name);
	if (directory.name[0] == 0xE5)
		Video_Format("Directory is being invalidated (0xe5), offset [%u]\n", offset);

	char buffer[512];
	HardDrive::ReadSector(HardDrive::GetDevice(0), sector, buffer);
	memcpy(&buffer[offset], &directory, sizeof(Directory));
	HardDrive::WriteSector(HardDrive::GetDevice(0), sector, buffer);
	return true;
}

bool SetDirectoryName(Directory& directory, char* name)
{
	memset(directory.name, ' ', 11);

	if (directory.attributes & A_Directory)
	{				
		memcpy(directory.name, name, Min(11, String::Length(name)));
	}
	else
	{
		unsigned long nameLen = String::Length(name);
		memcpy(&directory.name[8], &name[nameLen - 3], 3);
		memcpy(directory.name, name, Min(8, nameLen - 3));		
	}

	return true;
}

bool GetDirectoryName(Directory& directory, char* name, unsigned long size)
{
	if (directory.attributes & A_Directory)
	{				
		memcpy(name, directory.name, 11);

		int i = 10;
		for (; i >= 0; --i)
		{
			if (name[i] != ' ')
				break;
		}
		name[i+1] = '\0';
	}
	else
	{
		memcpy(name, directory.name, 8);

		int i = 7;
		for (; i >= 0; --i)
		{
			if (name[i] != ' ')
				break;
		}
		name[i+1] = '\0';

		char ext[4];
		memcpy(ext, &directory.name[8], 3);
		ext[3] = '\0';

		//Video_Format("prefix: [%s] postfix: [%s]\n", name, ext);
		
		String::Format(name, size, "%s.%s", name, ext);				
	}

	return true;
}

bool IsEOF(unsigned long fatEntry)
{
	if (fatType == FT_FAT32)
	{
		//Video_Format("eof: %i\n", (fatEntry >= 0x0FFFFFF8) ? 1 : 0);
		return (fatEntry >= 0x0FFFFFF8) ? true : false;
	}
	
	// fat-16
	return (fatEntry >= 0xFFF8) ? true : false;
}

unsigned long GetFileClusterCount(unsigned long startCluster)
{
	unsigned long numClusters = 1;
	unsigned long fatEntry = GetFATEntry(startCluster);

	// read in while not eof
	while (!IsEOF(fatEntry))
	{
		// get next fat entry in the list
		fatEntry = GetFATEntry(fatEntry);
		numClusters++;
	}

	return numClusters;
}

bool SetFATEntry(unsigned long cluster, unsigned long fatEntry)
{
	unsigned long fatOffset = 0;
	unsigned long fatSize = 0;
	if (bootRecord.sectorsPerFAT16 != 0)
		fatSize = bootRecord.sectorsPerFAT16;
	else
		fatSize = bootRecord.sectorsPerFAT32; 

	if (fatType == FT_FAT16)
		fatOffset = cluster * 2;
	else if(fatType == FT_FAT32)
		fatOffset = cluster * 4;
	else
		return 0;

	unsigned long sector = bootRecord.reservedSectors + (fatOffset / bootRecord.bytesPerSector);
	unsigned long offset = fatOffset % bootRecord.bytesPerSector;

	sector += mbr.partitions[0].lbaStart; // assume partition 0 for the time being

	// this could be cached
	char buffer[512];
	HardDrive::ReadSector(HardDrive::GetDevice(0), sector, buffer);

	(*(unsigned long*)&buffer[offset]) = fatEntry & 0x0FFFFFFF;
	HardDrive::WriteSector(HardDrive::GetDevice(0), sector, buffer);

	return true; //(*(unsigned long*)&buffer[offset]) & 0x0FFFFFFF;
}

unsigned long FindFreeCluster()
{
	// here we find the first and last entry of the fat entry table,
	// iterate till we find a unused entry

	unsigned long fatOffset = 0;
	unsigned long fatSize = 0;
	if (bootRecord.sectorsPerFAT16 != 0)
		fatSize = bootRecord.sectorsPerFAT16;
	else
		fatSize = bootRecord.sectorsPerFAT32; 

	if (fatType == FT_FAT16)
		fatOffset = 2 * 2;
	else if(fatType == FT_FAT32)
		fatOffset = 2 * 4;
	else
		return -1;

	unsigned long firstSector = bootRecord.reservedSectors + (fatOffset / bootRecord.bytesPerSector);
	unsigned long firstOffset = fatOffset % bootRecord.bytesPerSector;

	firstSector += mbr.partitions[0].lbaStart; // assume partition 0 for the time being

	// now find the last sector/offset
	if (fatType == FT_FAT16)
		fatOffset = bootRecord.totalSectors32 * 2;
	else if(fatType == FT_FAT32)
		fatOffset = bootRecord.totalSectors32 * 4;

	unsigned long lastSector = bootRecord.reservedSectors + (fatOffset / bootRecord.bytesPerSector);
	unsigned long lastOffset = fatOffset % bootRecord.bytesPerSector;

	lastSector += mbr.partitions[0].lbaStart; // assume partition 0 for the time being

	//unsigned long sector = firstSector;
	//unsigned long offset = firstOffset;

	Video_Format("fat table start sector: %u offset: %u, last sector %u\n", firstSector, firstOffset, lastSector);
	
	unsigned long freeCluster = 2;
	for (unsigned long sector = firstSector; sector < lastSector; ++sector)
	{
		//Video_Format("reading sector %u\n", sector);

		char buffer[512];
		HardDrive::ReadSector(HardDrive::GetDevice(0), sector, buffer);

		unsigned long startOffset = 0;
		if (sector == firstSector)
			startOffset = firstOffset;

		for (unsigned long offset = startOffset; offset < 512; offset += sizeof(unsigned long))
		{
			
			if (sector == (lastSector - 1) && offset > lastOffset)
			{
				Video_Format("last sector of FAT table\n");
				break;
			}
			
			// we found a free cluster? if so, return the cluster number
			unsigned long fatEntry = (*(unsigned long*)&buffer[offset]) & 0x0FFFFFFF;

			//Video_Format("0x%lX\t", fatEntry);

			if (fatEntry == 0x0)
			{
				/*
				unsigned long freeSector = sector - mbr.partitions[0].lbaStart;
				unsigned long freeCluster = (freeSector -  bootRecord.reservedSectors) * bootRecord.bytesPerSector;

				if (fatType == FT_FAT16)
					freeCluster = freeCluster / 2;
				else if(fatType == FT_FAT32)
					freeCluster = freeCluster / 4;

				if (freeCluster < 2)
					Video_Format("error, cluster value is invalid!\n");

				Video_Format("free cluster found: %u\n", freeCluster);
				return freeCluster;*/
				return freeCluster;
			}

			++freeCluster;
		}

		//return 0;
	}

	// uh oh!
	return -1;
}

unsigned long GetFATEntry(unsigned long cluster)
{
	unsigned long fatOffset = 0;
	unsigned long fatSize = 0;
	if (bootRecord.sectorsPerFAT16 != 0)
		fatSize = bootRecord.sectorsPerFAT16;
	else
		fatSize = bootRecord.sectorsPerFAT32; 

	if (fatType == FT_FAT16)
		fatOffset = cluster * 2;
	else if(fatType == FT_FAT32)
		fatOffset = cluster * 4;
	else
		return 0;

	unsigned long sector = bootRecord.reservedSectors + (fatOffset / bootRecord.bytesPerSector);
	unsigned long offset = fatOffset % bootRecord.bytesPerSector;

	sector += mbr.partitions[0].lbaStart; // assume partition 0 for the time being

	// this could be cached
	char buffer[512];
	HardDrive::ReadSector(HardDrive::GetDevice(0), sector, buffer);
	return (*(unsigned long*)&buffer[offset]) & 0x0FFFFFFF;
}

bool FormatDrive()
{
	//
	// set up master boot record
	//
	MasterBootRecord mbr;
	memset(&mbr, 0, sizeof(MasterBootRecord));
	mbr.bootRecordSignature = MBR_BOOT_SIGNATURE;

	// lba address for end of partition
	unsigned int lbaAddr = (100 * (1024 * 1024 * 1024)) / 512; // 100mb

	mbr.partitions[0].state = PARTITION_ACTIVESTATE;
	mbr.partitions[0].type = PARTITION_TYPE_FAT32;
	mbr.partitions[0].lbaStart = 1;
	mbr.partitions[0].numSectors = lbaAddr;

	unsigned int head;
	unsigned int sector;
	unsigned int cylinder;

	// set up start values
	HardDrive::LBAToCHS(HardDrive::GetDevice(0), 1, cylinder, head, sector);
	mbr.partitions[0].startHead = head;
	mbr.partitions[0].startCylinderSector = ((head << 2) | ((head >> 8) & 0x3)) | (sector & 0x3F); // encode heads and sectors

	// set up end values
	HardDrive::LBAToCHS(HardDrive::GetDevice(0), lbaAddr, cylinder, head, sector);
	mbr.partitions[0].endHead = head;
	mbr.partitions[0].endCylinderSector = ((head << 2) | ((head >> 8) & 0x3)) | (sector & 0x3F); // encode heads and sectors

	HardDrive::WriteSector(HardDrive::GetDevice(0), 0, (char*)&mbr);

	//
	// set up FAT32 boot record
	//
	BootRecord bootRecord;
	memset(&mbr, 0, sizeof(BootRecord));

	HardDrive::WriteSector(HardDrive::GetDevice(0), 0, (char*)&bootRecord);

	//
	// Set up FileInformationSystem
	//
	FileInformationSystem fis;
	memset(&mbr, 0, sizeof(FileInformationSystem));

	HardDrive::WriteSector(HardDrive::GetDevice(0), 0, (char*)&fis);

	return true;
}


bool Node::Open(const char* path, FileFlags accessFlags)
{
	char nodeName[256];
	char deviceStr[256];
	unsigned int firstSlash = String::FindFirstOf(path, "/");
	String::SubString(path, deviceStr, firstSlash);

	//Video_Format("path string: [%s], length: %u\n", path, String::Length(path));
	//Video_Format("Device string: [%s], length: %u, firstFlash at: %u\n", deviceStr, String::Length(deviceStr), firstSlash);

	Video_Format("attempting to open '%s'\n", path);

	const HardDrive::Device* device = 0;
	if (String::Compare(deviceStr, "hd0") == -1)
	{
		device = HardDrive::GetDevice(0);
	}

	if (!device)
	{
		Video_Format("device '%s' not found\n", deviceStr);
		return false;
	}

	cursor = 0;
	fileSize = 0;
	handle = bootRecord.rootDirCluster;

	if (!device)
		return false;

	String::Copy(nodeName, deviceStr);

	// must be a root directory
	if (path[firstSlash] == '\0')
	{
		flags = FF_Directory | FF_Volume;
		fileSize = GetFileClusterCount(handle) * bootRecord.sectorsPerCluster * 512;

		String::Copy(name, nodeName);
		
		Video_Format("found directory [%s] handle(cluster) [%u] sector [%u] filesize [%u]\n", nodeName, handle, GetSector(handle), fileSize);
		return true;
	}

	// find the directory
	bool bFound = false;
	Directory directory;
	do
	{
		unsigned int lastSlash = firstSlash + 1;
		firstSlash = lastSlash + String::FindFirstOf(&path[lastSlash], "/");

		//Video_Format("next first slash: %u\n", firstSlash);

		unsigned long sector = GetSector(handle);
		unsigned long numFileClusters = GetFileClusterCount(handle); // num clusters in this directory file

		char dirName[256];
		String::SubString(&path[lastSlash], dirName, firstSlash - lastSlash);

		//Video_Format("looking for dir: [%s], firstSlash [%u]\n", dirName, firstSlash);

		bFound = false;
		for (unsigned long s = 0; s < (numFileClusters * bootRecord.sectorsPerCluster); ++s)
		{
			for (unsigned long o = 0; o < 512; o += sizeof(Directory))
			{
				GetDirectory(directory, sector + s, o);

				if (directory.attributes & A_VolumeId || directory.attributes & A_System)
					continue;

				if (directory.name[0] == 0xE5 || directory.name[0] == 0x00)
					continue;

				// create a proper string from the name
				char curDirName[13];
				GetDirectoryName(directory, curDirName, 13);

				//Video_Format("cur directory [%s]\n", curDirName);

				if (String::CaseCompare(curDirName, dirName) == -1)
				{
					if (String::Compare(dirName, "..") == -1)
					{
						unsigned long lastNodeNameSlash = String::FindLastOf(nodeName, "/");
						nodeName[lastNodeNameSlash] = '\0';
					}
					else if (String::Compare(dirName, ".") == -1)
					{
					}
					else
					{
						String::Format(nodeName, 256, "%s/%s", nodeName, dirName);
					}

					handle = (directory.firstClusterHigh << 8) | directory.firstClusterLow;
					bFound = true;
					Video_Format("found directory [%s] handle [%u]\n", dirName, handle);
					break;
				}
			}

			if (bFound)
				break;
		}

		// reached end of string
		//if (path[firstSlash] == '\0' && !bFound)
		//	return false;

	} while (path[firstSlash] != '\0');

	if (!bFound)
		return false;

	if (directory.attributes & A_Directory)
	{
		flags = FF_Directory;

		// directorys dont have a size, so just use the number of sectors the directory consumes
		fileSize = GetFileClusterCount(handle) * bootRecord.sectorsPerCluster * 512;

		Video_Format("DIR filesize [%u]\n", fileSize);
	}
	else
	{
		flags = FF_File;
		fileSize = directory.fileSize;
	}

	

	Video_Format("found directory [%s] handle(cluster) [%u] sector [%u] filesize [%u]\n", nodeName, handle, GetSector(handle), fileSize);

	String::Copy(name, nodeName);
	return true;
}

void Node::Close()
{

}

unsigned long Node::GetChildCount()
{
	if (flags & FF_File)
		return 0;

	Seek(FF_SeekStart, 0);

	unsigned long childCount = 0;
	while (!IsEOF())
	{
		Directory directory;
		if (Read((char*)&directory, sizeof(Directory)) != sizeof(Directory))
			Video_Format("Failed to read directory\n");

		if (directory.attributes & A_VolumeId || directory.attributes & A_System)
			continue;

		// a free directory
		if (directory.name[0] == 0xE5)
			continue;

		// this indicates there are no more directories
		if (directory.name[0] == 0x00)
			break;

		++childCount;
	}

	/*
	unsigned long sector = GetSector(handle);
	unsigned long numFileClusters = GetFileClusterCount(handle); // num clusters in this directory file

	unsigned long childCount = 0;
	for (unsigned long s = 0; s < (numFileClusters * bootRecord.sectorsPerCluster); ++s)
	{
		for (unsigned long o = 0; o < 512; o += sizeof(Directory))
		{
			Directory directory;
			GetDirectory(directory, sector + s, o);

			if (directory.attributes & A_VolumeId || directory.attributes & A_System)
				continue;

			// a free directory
			if (directory.name[0] == 0xE5)
				continue;

			// this indicates there are no more directories
			if (directory.name[0] == 0x00)
				return childCount;		

			++childCount;
		}
	}*/

	return childCount;
}

bool Node::GetChild(Node& child, unsigned long index)
{
	if (flags & FF_File)
		return false;

	Seek(FF_SeekStart, 0);

	unsigned long childCount = 0;
	while (!IsEOF())
	{
		Directory directory;
		if (Read((char*)&directory, sizeof(Directory)) != sizeof(Directory))
			Video_Format("Failed to read directory\n");

		if (directory.attributes & A_VolumeId || directory.attributes & A_System)
			continue;

		// a free directory
		if (directory.name[0] == 0xE5)
			continue;

		// this indicates there are no more directories
		if (directory.name[0] == 0x00)
			return false;

		if (childCount == index)
		{
			// create a proper string from the name
			char curDirName[13];
			GetDirectoryName(directory, curDirName, 13);

			//Video_Format("cur directory [%s]\n", curDirName);

			child.handle = (directory.firstClusterHigh << 8) | directory.firstClusterLow;
			String::Format(child.name, 256, "%s/%s", name, curDirName);
			return true;
		}

		++childCount;
	}

	return false;


	/*
	unsigned long sector = GetSector(handle);
	unsigned long numFileClusters = GetFileClusterCount(handle); // num clusters in this directory file

	unsigned long childCount = 0;
	for (unsigned long s = 0; s < (numFileClusters * bootRecord.sectorsPerCluster); ++s)
	{
		for (unsigned long o = 0; o < 512; o += sizeof(Directory))
		{
			Directory directory;
			GetDirectory(directory, sector + s, o);

			if (directory.attributes & A_VolumeId || directory.attributes & A_System)
				continue;

			// a free directory
			if (directory.name[0] == 0xE5)
				continue;

			// this indicates there are no more directories
			if (directory.name[0] == 0x00)
				return false;

			if (childCount == index)
			{
				// create a proper string from the name
				char curDirName[13];
				GetDirectoryName(directory, curDirName, 13);

				//Video_Format("cur directory [%s]\n", curDirName);

				child.handle = (directory.firstClusterHigh << 8) | directory.firstClusterLow;
				String::Format(child.name, 256, "%s/%s", name, curDirName);
				return true;
			}

			++childCount;
		}
	}

	return false;*/
}

bool Node::GetParent(Node& parent)
{
	return false;
}

bool Node::RemoveChild(unsigned long index)
{
	if (flags & FF_File)
		return false;

	Seek(FF_SeekStart, 0);

	unsigned long childCount = 0;
	while (!IsEOF())
	{
		Directory directory;
		if (Read((char*)&directory, sizeof(Directory)) != sizeof(Directory))
			Video_Format("Failed to read directory\n");

		if (directory.attributes & A_VolumeId || directory.attributes & A_System)
			continue;

		// a free directory
		if (directory.name[0] == 0xE5)
			continue;

		// this indicates there are no more directories
		if (directory.name[0] == 0x00)
			return false;

		if (childCount == index)
		{
			Video_Format("[Node::RemoveChild] directory found\n");

			directory.name[0] = 0xE5;

			Directory nextDirectory;
			if (IsEOF())
			{
				Video_Format("removing the last directory entry\n");
				directory.name[0] = 0x0;
			}
			else if (Read((char*)&nextDirectory, sizeof(Directory)) == sizeof(Directory) && nextDirectory.name[0] == 0x0)
			{
				Video_Format("removing the last directory entry\n");
				directory.name[0] = 0x0;
				Seek(FF_SeekCurrent, -sizeof(Directory));
			}
/*
			Video_Format("directory:");
			for (int i = 0; i < sizeof(Directory); i += 2)
			{
				Video_Format("%x", *(((unsigned short*)&directory) + i));
			}
			Video_Format("\n");*/

			Seek(FF_SeekCurrent, -sizeof(Directory));
			Write((char*)&directory, sizeof(Directory));
			//SetDirectory(directory, sector + s, o);
			return true;
		}

		++childCount;
	}

	/*
	unsigned long sector = GetSector(handle);
	unsigned long numFileClusters = GetFileClusterCount(handle); // num clusters in this directory file

	unsigned long childCount = 0;
	for (unsigned long s = 0; s < (numFileClusters * bootRecord.sectorsPerCluster); ++s)
	{
		for (unsigned long o = 0; o < 512; o += sizeof(Directory))
		{
			Directory directory;
			GetDirectory(directory, sector + s, o);

			if (directory.attributes & A_VolumeId || directory.attributes & A_System)
				continue;

			// a free directory
			if (directory.name[0] == 0xE5)
				continue;

			// this indicates there are no more directories
			if (directory.name[0] == 0x00)
				return false;

			if (childCount == index)
			{
				Video_Format("[Node::RemoveChild] directory found");

				memset(&directory, 0xE5, sizeof(Directory)); // TODO: memset to zero if this is last directory

				if (directory.name[0] == 0xE5)
					Video_Format("[Node::RemoveChild] invalidating directory");

				Video_Format("directory:");
				for (int i = 0; i < sizeof(Directory); i += 2)
				{
					Video_Format("%x", *(((unsigned short*)&directory) + i));
				}
				Video_Format("\n");

				SetDirectory(directory, sector + s, o);
				return true;
			}

			++childCount;
		}
	}
*/
	// not found
	return false;
}

bool Node::AddChild(Node& child)
{
	if (flags & FF_File)
		return false;

	Seek(FF_SeekStart, 0);

	unsigned long childCount = 0;
	while (!IsEOF())
	{
		Directory directory;
		if (Read((char*)&directory, sizeof(Directory)) != sizeof(Directory))
			Video_Format("Failed to read directory\n");

		if (directory.attributes & A_VolumeId || directory.attributes & A_System)
			continue;

		// a free directory
		if (directory.name[0] == 0xE5)
			continue;

		// this indicates there are no more directories
		if (directory.name[0] == 0x00)
		{
			Seek(FF_SeekCurrent, -sizeof(Directory));
			break;
		}

		++childCount;
	}

	Video_Format("end of current directory found\n");

	// set up a single cluster sized directory
	unsigned long freeCluster = FindFreeCluster();
	if (freeCluster == -1)
	{
		Video_Format("free cluster not found, hd is full\n");
		return false;
	}

	Video_Format("free cluster found %u (sector %u)\n", freeCluster, GetSector(freeCluster));
	SetFATEntry(freeCluster, -1);

	Directory directory;
	memset(&directory, 0, sizeof(Directory));
	directory.attributes = A_Directory;
	directory.firstClusterHigh = HIWORD(freeCluster);
	directory.firstClusterLow = LOWORD(freeCluster);

	// hack in creation/access date/time
	directory.createTimeTeenth = 0x41;
	directory.createTime = 0xa591;
	directory.createDate = 0x36a5;
	directory.lastAccessDate = 0x0000;
	directory.lastWriteTime = 0xa592;
	directory.lastWriteDate = 0x36a5;

	Video_Format("writing new directory\n");

	SetDirectoryName(directory, child.name);
	Write((char*)&directory, sizeof(Directory));

	Video_Format("writing done\n");

	// write the null directory if needed
	bool bAddNullDir = false;
	if (cursor < fileSize)
	{
		memset(&directory, 0, sizeof(Directory));
		Write((char*)&directory, sizeof(Directory));
		bAddNullDir = true;
	}

	// now open the directory to create the . and .. directories
	{
		char temp[256];
		String::Copy(temp, child.name);
		String::Format(child.name, 256, "%s/%s", name, temp);

		Node newDir;
		//File newDir;
		if (!newDir.Open(child.name, FF_Write))
		{
			Video_Format("FAILED TO OPEN DIRECTORY '%s'\n", child.name);

			// undo the change
			Seek(FF_SeekCurrent, -sizeof(Directory) * (bAddNullDir ? 1 : 2));
			memset(&directory, 0, sizeof(Directory));
			Write((char*)&directory, sizeof(Directory));
			return false;
		}

		// clear memory for debugging purposes
		char buffer[512];
		memset(buffer, 0, 512);
		newDir.Write(buffer, 512);
		newDir.Seek(FF_SeekStart, 0);

		Video_Format("writing . directory\n");

		Directory systemDirectory;
		memset(&systemDirectory, 0, sizeof(Directory));
		systemDirectory.attributes = A_Directory;
		memset(systemDirectory.name, ' ', 11);
		systemDirectory.name[0] = '.';

		// hack in creation/access date/time
		systemDirectory.createTimeTeenth = 0x41;
		systemDirectory.createTime = 0xa591;
		systemDirectory.createDate = 0x36a5;
		systemDirectory.lastAccessDate = 0x0000;
		systemDirectory.lastWriteTime = 0xa592;
		systemDirectory.lastWriteDate = 0x36a5;

		//SetDirectoryName(systemDirectory, ".");
		Video_Format("dir name: '%s'\n", systemDirectory.name);
		systemDirectory.firstClusterHigh = HIWORD(freeCluster);
		systemDirectory.firstClusterLow = LOWORD(freeCluster);
		newDir.Write((char*)&systemDirectory, sizeof(Directory));

		Video_Format("writing done\n");

		Video_Format("writing .. directory\n");

		//SetDirectoryName(systemDirectory, "..");
		systemDirectory.name[1] = '.';
		Video_Format("dir name: '%s'\n", systemDirectory.name);
		systemDirectory.firstClusterHigh = HIWORD(handle);
		systemDirectory.firstClusterLow = LOWORD(handle);
		newDir.Write((char*)&systemDirectory, sizeof(Directory));

		Video_Format("writing done\n");

		Video_Format("writing null directory\n");

		memset(&systemDirectory, 0, sizeof(Directory));
		newDir.Write((char*)&systemDirectory, sizeof(Directory));

		Video_Format("writing done\n");
	}


	

/*
	unsigned long sector = GetSector(handle);
	unsigned long numFileClusters = GetFileClusterCount(handle); // num clusters in this directory file

	unsigned long sectorCount = numFileClusters * bootRecord.sectorsPerCluster;
	for (unsigned long s = 0; s < sectorCount; ++s)
	{
		for (unsigned long o = 0; o < 512; o += sizeof(Directory))
		{
			Directory directory;
			GetDirectory(directory, sector + s, o);

			if (directory.attributes & A_VolumeId || directory.attributes & A_System)
				continue;

			bool bClearNextDirEntry = (directory.name[0] == 0x00);
			if (directory.name[0] == 0xE5 || bClearNextDirEntry)
			{
				Video_Format("free directory found\n");
				directory.attributes = A_Directory;
				SetDirectoryName(directory, child.name);				
				SetDirectory(directory, sector + s, o);
				Video_Format("directory name will be '%s'\n", directory.name);
			}

			// set next directory to be null
			if (bClearNextDirEntry)
			{
				Video_Format("clearing next directory\n");
				memset(&directory, 0, sizeof(Directory));

				// eek, add a new cluster to the directory
				if ((o + sizeof(Directory)) >= 512)
				{
					if (s >= sectorCount)
					{
						unsigned long freeCluster = FindFreeCluster();
						Video_Format("directory is full, need to assign a new cluster, cluster found [%u]\n", freeCluster);
					}
					else
					{
						Video_Format("sector is full, clearing next sector\n");
						SetDirectory(directory, sector + s + 1, 0);
					}
					return false;
				}
				else
				{
					SetDirectory(directory, sector + s, o + sizeof(Directory));
					return true;
				}
			}
		}
	}

	// no free directory slots, so add another cluster to the directory
	unsigned long freeCluster = FindFreeCluster();
	Video_Format("directory is full, need to assign a new cluster, cluster found [%u]\n", freeCluster);
	return false;*/
}

bool File::Open(Node& node)
{
	handle = 0;
	fileSize = 0;
	cursor = 0;

	if (!(node.flags & FF_File))
	{
		Video_Format("file not a file, cant open\n");
		return false;
	}

	fileSize = node.fileSize;
	handle = node.handle;

	//Video_Format("opening file with handle '%u'\n", node.handle);
	//Video_Format("file consumes [%u] clusters\n", GetFileClusterCount(node.handle));
	return true;
}

bool File::Open(const char* path, FileFlags accessFlags)
{
	Video_Format("attempting to opening file '%s'\n", path);
	Node node;
	if (!node.Open(path))
	{
		Video_Format("failed to open\n");

		// file doesnt exists...
		if (accessFlags == FF_Read)
			return false;

		// create the file
		if (accessFlags == FF_Append || accessFlags == FF_Write)
		{
			Video_Format("attempting to create new file\n");

			Node directory;
			unsigned int fileStart = String::FindLastOf(path, "/");
			char directoryName[256];
			String::Copy(directoryName, path);
			directoryName[fileStart] = '\0';

			// check if directory exists
			if (!directory.Open(directoryName))
				return false;

			node.flags = FF_File;
			node.fileSize = 0;
			node.handle = FindFreeCluster();
			if (node.handle == -1)
				return false;

			String::Copy(node.name, path);
			directory.AddChild(node);

			directory.Close();
		}

		return false;
	}

	Video_Format("file exists\n");
	bool bOpen = Open(node);
	node.Close();
	return bOpen;
}

void File::Close()
{

}

unsigned long File::Read(char* buffer, unsigned long size)
{
	unsigned long cluster;
	unsigned long sector;
	unsigned long offset;
	if (!GetClusterSectorOffset(cluster, sector, offset))
		return 0;

	//Video_Format("cso %u %u %u\n", cluster, sector, offset);
	//Video_Format("filesize: %u\n", fileSize);

	char sectorBuffer[512];
	HardDrive::ReadSector(HardDrive::GetDevice(0), sector, sectorBuffer);

	// copy to the end of the first sector
	unsigned long copySize = Min(size, 512 - offset);
	memcpy(buffer, &sectorBuffer[offset], copySize);
	cursor += copySize;

	// now copy while we have any size remaning - read a sector at a time
	unsigned long targetBufferOffset = copySize;
	unsigned long sizeRemaining = size - copySize;
	unsigned long sectorReadCount = 1;
	while (sizeRemaining > 0)
	{
		++sector;
		++sectorReadCount;

		// reached end of cluster, so move to the next cluster
		if (sectorReadCount > bootRecord.sectorsPerCluster)
		{
			sectorReadCount = 0;
			cluster = GetFATEntry(cluster);
			sector = GetSector(cluster);

			if (FS::IsEOF(cluster))
				return (size - sizeRemaining);
		}

		copySize = Min(sizeRemaining, 512);
		copySize = Min(copySize, fileSize - cursor);
		HardDrive::ReadSector(HardDrive::GetDevice(0), sector, sectorBuffer);
		memcpy(&buffer[targetBufferOffset], sectorBuffer, copySize);
		targetBufferOffset += copySize;
		sizeRemaining -= copySize;
		cursor += copySize;

		// reached end of file
		if (cursor >= fileSize)
			return (size - sizeRemaining);
	}

	return size;
}

unsigned long File::Write(char* buffer, unsigned long size)
{
	unsigned long cluster;
	unsigned long sector;
	unsigned long offset;
	if (!GetClusterSectorOffset(cluster, sector, offset))
		return 0;

	//Video_Format("WRITE BEGIN: sec: %u clus: %u offs: %u\n", sector, cluster, offset);

	char sectorBuffer[512];
	HardDrive::ReadSector(HardDrive::GetDevice(0), sector, sectorBuffer);

	// copy to the end of the first sector
	unsigned long copySize = Min(size, 512 - offset);
	memcpy(&sectorBuffer[offset], buffer, copySize);
	HardDrive::WriteSector(HardDrive::GetDevice(0), sector, sectorBuffer);
	cursor += copySize;

	// now copy while we have any size remaning - read a sector at a time
	unsigned long targetBufferOffset = copySize;
	unsigned long sizeRemaining = size - copySize;
	unsigned long sectorReadCount = 1;

	//Video_Format("writing %u, remaning: %u\n", copySize, sizeRemaining);

	while (sizeRemaining >= 512)
	{
		++sector;
		++sectorReadCount;

		//Video_Format("size remaning %u\n", sizeRemaining);

		// reached end of cluster, so move to the next cluster
		if (sectorReadCount > bootRecord.sectorsPerCluster)
		{
			sectorReadCount = 0;
			cluster = GetFATEntry(cluster);
			sector = GetSector(cluster);

			// file is full, so add a new cluster
			if (FS::IsEOF(cluster))
			{
				if (!Expand())
				{
					//Video_Format("WRITE END - HD FULL\n");
					return (size - sizeRemaining);
				}
			}
		}

		HardDrive::WriteSector(HardDrive::GetDevice(0), sector, &buffer[targetBufferOffset]);
		sizeRemaining -= 512;
		cursor += 512;
		targetBufferOffset += 512;
		//Video_Format("writing 512\n");

		// reached end of file, add a new cluster
		if (cursor >= fileSize)
		{
			if (!Expand())
			{
				//Video_Format("WRITE END - HD FULL\n");
				return (size - sizeRemaining);
			}
		}
	}

	if (sizeRemaining <= 0)
	{
		//Video_Format("WRITE END - multiple of 512\n");
		return (size - sizeRemaining);
	}

	++sector;
	++sectorReadCount;

	// reached end of cluster, so move to the next cluster
	if (sectorReadCount > bootRecord.sectorsPerCluster)
	{
		sectorReadCount = 0;
		cluster = GetFATEntry(cluster);
		sector = GetSector(cluster);

		// file is full, so add a new cluster
		if (FS::IsEOF(cluster))
		{
			if (!Expand())
			{
				//Video_Format("WRITE END - HD FULL\n");
				return (size - sizeRemaining);
			}
		}
	}

	// now write any left overs
	HardDrive::ReadSector(HardDrive::GetDevice(0), sector, sectorBuffer);

	copySize = Min(size, 512 - offset);
	memcpy(&sectorBuffer, &buffer[targetBufferOffset], copySize);
	HardDrive::WriteSector(HardDrive::GetDevice(0), sector, sectorBuffer);
	cursor += copySize;
	//Video_Format("writing %u\n", copySize);

	//Video_Format("WRITE END\n");
	return size;
}

bool File::GetClusterSectorOffset(unsigned long& cluster, unsigned long& sector, unsigned long& offset)
{
	// find the appropriate cluster
	unsigned long numClusters = cursor / (bootRecord.sectorsPerCluster * 512);
	cluster = handle;

	//Video_Format("\nnum clusters %u, cursor: %u, handle: %u\n", numClusters, cursor, cluster);

	// read to the appropriate cluster
	for (unsigned long c = 0; c < numClusters; ++c)
	{
		// get next fat entry in the list if current cluster is not the end of file
		if (FS::IsEOF(cluster))
		{
			Video_Format("\nEOF cluster found: %u\n", cluster);
			return false;
		}

		cluster = GetFATEntry(cluster);		
	}

	// get next fat entry in the list if current cluster is not the end of file
	if (FS::IsEOF(cluster))
	{
		Video_Format("\nEOF cluster found 2: %u\n", cluster);
		return false;
	}

	unsigned long numSectors = cursor / 512;
	sector = GetSector(cluster) + (numSectors - (numClusters * bootRecord.sectorsPerCluster/* * 512*/));

	//Video_Format("\nnum sectors [%u] custor [%u] num cluster [%u]\n", numSectors, cursor, numClusters);
	//Video_Format("\ncluster num [%u] raw sector [%u] appendage [%u]\n", cluster, GetSector(cluster), (numSectors - (numClusters * bootRecord.sectorsPerCluster/* * 512*/)));

	// now calculate the offset in the sector
	offset = cursor - (numSectors * 512);

	return true;
}

bool File::Expand()
{
	Video_Format("expanding file size\n");

	unsigned long freeCluster = FindFreeCluster();
	if (freeCluster == -1)
		return false;

	unsigned long fatEntry = GetFATEntry(handle);

	// read in while not eof
	while (!FS::IsEOF(fatEntry))
	{
		// get next fat entry in the list
		fatEntry = GetFATEntry(fatEntry);
	}

	// set the last cluster to point to the new cluster
	SetFATEntry(fatEntry, freeCluster); 

	// Set new free cluster to EOF
	SetFATEntry(freeCluster, -1); 

	return true;
}

bool File::Contract()
{
	return true;
}

bool File::IsEOF()
{
	return cursor >= fileSize;
}

bool File::Seek(FileFlags seekType, unsigned long offset)
{
	if (seekType == FF_SeekStart)
	{
		cursor = offset;
		cursor = Clamp(0, cursor, fileSize);
		return true;
	}

	if (seekType == FF_SeekCurrent)
	{
		cursor += offset;
		cursor = Clamp(0, cursor, fileSize);
		return true;
	}

	if (seekType == FF_SeekEnd)
	{
		cursor = fileSize + offset;
		cursor = Clamp(0, cursor, fileSize);
		return true;
	}

	return false;
}

};
