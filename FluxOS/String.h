#ifndef _STRING_H_
#define _STRING_H_

#include "System.h"

namespace String
{
// returns bytes read from the string, the int value is put in to "value"
unsigned int	ToInt(const char* str, int& value);

//
// searches through the string 'src' for any 'tokens' and returns the index of the first token,
// check src[return value] == '\0' means it wasnt found
//
unsigned int	FindFirstOf(const char* src, char* tokens);
unsigned int	FindLastOf(const char* src, char* tokens);

//
// copy 'len' bytes from 'src' to 'dest' and terminate
//
void SubString(const char* src, char* dest, int len);

void Copy(char* dest, const char* src);

unsigned int Length(const char* str);

//
// compares string 1 to string 2, returns -1 if they are the same, else returns the index of the first difference
// (case sensitive)
//
int Compare(const char* str1, const char* str2);
int Compare(const char* str1, const char* str2, int length);

// case insensitive compare
int CaseCompare(const char* str1, const char* str2);
int CaseCompare(const char* str1, const char* str2, int length);

//
// Returns the length of the string copied in to dest
//
unsigned int FromInt(char* dest, unsigned int destMaxLen, int value);
unsigned int FromUInt(char* dest, unsigned int destMaxLen, unsigned int value);
unsigned int FromHex(char* dest, unsigned int destMaxLen, unsigned long value);
unsigned int FromHex(char* dest, unsigned int destMaxLen, unsigned short value);

unsigned int Format(char* dest, unsigned int destMaxLen, const char* format, ...);
unsigned int Format(char* dest, unsigned int destMaxLen, const char* format, va_list ap);

};

#endif
