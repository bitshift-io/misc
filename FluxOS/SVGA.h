#ifndef _SVGA_H_
#define _SVGA_H_

namespace SVGA
{

struct Colour
{
	Colour(unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a) :
		r(_r), 
		g(_g),
		b(_b),
		a(_a)
	{
	}

	unsigned char	r;
	unsigned char	g;
	unsigned char	b;
	unsigned char	a;
};

bool Init();
bool SetVideoMode(unsigned short width, unsigned short height, unsigned char bpp = 32);
void Clear(const Colour& colour);
void PutPixel(unsigned short x, unsigned short y, const Colour& colour);

};

#endif
