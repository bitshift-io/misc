#ifndef _MEMORY_H_
#define _MEMORY_H_

extern void *memcpy(void *dest, const void *src, int count);
extern void *memset(void *dest, char val, int count);
extern unsigned short *memsetw(unsigned short *dest, unsigned short val, int count);

namespace Memory
{
	enum
	{
		//M_PageDirectory = 0x2090A20, // 2mb mark... need a better way to calc this
		M_PageSize = 4 * 1024, // 4k
		//M_PageTablePhysicalAddress = (M_PageDirectory + M_PageSize),
		M_NumPages = 1024,
		M_MemorySize = 4 * 1024 * 1024, // 4gb
		M_MaxTasks = 256,

		M_Unsued = 0,
		M_Normal = 1,
		M_Task = 2,
		M_Reversed = -1,
	};

	void Init();

	unsigned int AllocPage(int type);

};

#endif
