#ifndef _PCI_H_
#define _PCI_H_

namespace PCI
{

enum Space
{
	S_IO,
	S_Memory,
};

struct Base
{
   Space			space;
   unsigned char	type;
   unsigned char	prefetch;
   unsigned long	address;
}; 


class Device
{
public:

	Base			GetBase(unsigned short index);

	long long		GetBaseAddress(unsigned short index);
	bool			SetBaseAddress(unsigned short index, long long value);

//protected:

	unsigned short int	vendor;
	unsigned short int	device;

	unsigned char		bus;
	unsigned char		dev;
	unsigned char		fn;
};

bool		Init();

bool		ScanBus(int bus);
bool		ScanFunction(int bus, int device, int function);
bool		HandleHostController(int bus, int device, int function);
bool		HandlePCIToPCIBridge(int bus, int device, int function);


Device*	FindDevice(unsigned short int device, unsigned short int vendor);

};

#endif
