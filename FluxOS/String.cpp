#include "String.h"
#include "Memory.h"
#include "Video.h"

namespace String
{

unsigned int ToInt(const char* str, int& value)
{
	value = 0;

	// count till the first invalid character
	int charCount = 0;
	while (str[charCount] != '\0')
	{
		int charValue = str[charCount] - 48;
		if (charValue < 0 || charValue > 9)
			break;

		++charCount;
	}

	for (int i = 0; i < charCount; ++i)
	{
		int charValue = str[i] - 48;
		int units = 0;
		for (int u = 0; u < ((charCount - 1) - i); ++u)
			charValue *= 10;

		value += charValue;
	}

	return charCount;
}

unsigned int FindFirstOf(const char* src, char* tokens)
{
	unsigned int srcLen = Length(src);
	unsigned int tokenLen = Length(tokens);

	for (unsigned int s = 0; s < srcLen; ++s)
	{
		for (unsigned int t = 0; t < tokenLen; ++t)
		{
			if (src[s] == tokens[t])
				return s;
		}
	}

	return srcLen;
}

unsigned int FindLastOf(const char* src, char* tokens)
{
	unsigned int srcLen = Length(src);
	unsigned int tokenLen = Length(tokens);

	for (unsigned int s = srcLen - 1; s >= 0; --s)
	{
		for (unsigned int t = 0; t < tokenLen; ++t)
		{
			if (src[s] == tokens[t])
				return s;
		}
	}

	return -1;
}

void SubString(const char* src, char* dest, int len)
{
	memcpy(dest, src, len);
	dest[len] = '\0';
}

void Copy(char* dest, const char* src)
{
	memcpy(dest, src, Length(src) + 1);
}

unsigned int Length(const char* str)
{
    unsigned int retval;
    for (retval = 0; *str != '\0'; ++str) 
		++retval;

    return retval;
}

int CaseCompare(const char* str1, const char* str2)
{
	int str1Len = Length(str1);
	int str2Len = Length(str2);

	int minLen = (str1Len > str2Len) ? str2Len : str1Len;

	int resultLen = CaseCompare(str1, str2, minLen);
	if (resultLen == -1)
	{
		if (str1Len == str2Len)
			return -1;
		else
			return minLen;
	}

	return resultLen;
}

int CaseCompare(const char* str1, const char* str2, int length)
{
	// to convert to upper or lower case we need to add/subtract 32
	for (int i = 0; i < length; ++i)
	{
		// convert to lower case
		// then compare
		char char1 = str1[i];
		char char2 = str2[i];

		if (char1 >= 65 && char1 <= 90)
			char1 += 32;

		if (char2 >= 65 && char2 <= 90)
			char2 += 32;

		if (char1 != char2)
			return i;
	}

	return -1;
}

int Compare(const char* str1, const char* str2)
{
	int str1Len = Length(str1);
	int str2Len = Length(str2);

	int minLen = (str1Len > str2Len) ? str2Len : str1Len;

	int resultLen = Compare(str1, str2, minLen);
	if (resultLen == -1)
	{
		if (str1Len == str2Len)
			return -1;
		else
			return minLen;
	}

	return resultLen;
}

int Compare(const char* str1, const char* str2, int length)
{
	for (int i = 0; i < length; ++i)
	{
		if (str1[i] != str2[i])
			return i;
	}

	return -1;
}


unsigned int Format(char* dest, unsigned int destMaxLen, const char* format, ...)
{
	unsigned int count = 0;

	va_list ap;
  	va_start(ap, format);
	count = Format(dest, destMaxLen, format, ap);
	va_end(ap);

	return count;
}

unsigned int Format(char* dest, unsigned int destMaxLen, const char* format, va_list ap)
{
	unsigned int count = 0;
	unsigned int sLen;
	unsigned int curDest = 0;
	unsigned int cur = 0;

	unsigned int formatLen = Length(format);

	while (format[cur] != '\0')
	{
		if (format[cur] == '%')
		{
			//dest[curDest] = '\0';

			++cur;
			bool bTerminate = false;
			bool bLong = false;
			while (!bTerminate && format[cur] != '\0')
			{
				switch (format[cur])
				{
				case 'l':
					{
						bLong = true;
						++cur;
					}
					break;

				case 'u':
					{
						unsigned int u = va_arg(ap, unsigned int);
						sLen = FromUInt(&dest[curDest], destMaxLen - curDest, u); 
						curDest += sLen;
						++cur;
						bTerminate = true;
					}
					break;

				case 'd':
				case 'i':
					{
						int i = va_arg(ap, int);
						sLen = FromInt(&dest[curDest], destMaxLen - curDest, i); 
						curDest += sLen;
						++cur;
						bTerminate = true;
					}
					break;

				case 'X':
				case 'x':
					{
						if (bLong)
						{
							unsigned long l = va_arg(ap, unsigned long);
							sLen = FromHex(&dest[curDest], destMaxLen - curDest, l); 
						}
						else
						{
							unsigned short l = va_arg(ap, unsigned short);
							sLen = FromHex(&dest[curDest], destMaxLen - curDest, l); 
						}
						
						curDest += sLen;
						++cur;
						bTerminate = true;
					}
					break;

				case 's':
					{
						const char* s = va_arg(ap, const char*);
						sLen = Length(s);
						
						memcpy(&dest[curDest], s, sLen);
						curDest += sLen;
						++cur;
						bTerminate = true;
					}
					break;

				case 'c':
					{
						char c = va_arg(ap, char);
						dest[curDest] = c;
						++curDest;
						++cur;
						bTerminate = true;
					}
					break;

				case 'f':
					{
						float f = va_arg(ap, float);
						++cur;
						bTerminate = true;
					}
					break;

				default:
					++cur;
					break;
				}
			}
		}
		else
		{
			dest[curDest] = format[cur];
			++curDest;
			++cur;
		}		
	}

	dest[curDest] = '\0';
	return count;
}

unsigned int FromInt(char* dest, unsigned int destMaxLen, int value)
{
	// NOTE: 48 is a bias to convert the number to the appropriate ascii character
	unsigned int i = 0;
	unsigned int remainder = 0;
	unsigned int digit = 0;
	unsigned int digitCount = 0;
	unsigned int charCount = 0;

	// if negative, prefix a -ve and offset so the number is appended after this
	if (value < 0)
	{
		dest[0] = '-';
		value = -value;
		++dest;
		++charCount;
	}

	digit = value;
	while (digit > 9)
	{
		digit /= 10;
		++digitCount;
	}

	digit = value;
	for (i = 0; i < digitCount; ++i)
	{
		remainder = digit - ((digit / 10) * 10);
		dest[digitCount - i] = remainder + 48;
		digit /= 10;
	}

	dest[0] = digit + 48;
	dest[digitCount+1] = '\0';
	return charCount + digitCount + 1;
}

unsigned int FromUInt(char* dest, unsigned int destMaxLen, unsigned int value)
{
	// NOTE: 48 is a bias to convert the number to the appropriate ascii character
	unsigned int i = 0;
	unsigned int remainder = 0;
	unsigned int digit = 0;
	unsigned int digitCount = 0;
	unsigned int charCount = 0;

	digit = value;
	while (digit > 9)
	{
		digit /= 10;
		++digitCount;
	}

	//Video_PutChar(digitCount + 48);

	digit = value;
	for (i = 0; i < digitCount; ++i)
	{
		remainder = digit - ((digit / 10) * 10);
		dest[digitCount - i] = remainder + 48;
		//Video_PutChar(remainder + 48);
		digit /= 10;
	}

	dest[0] = digit + 48;
	dest[digitCount+1] = '\0';
	return charCount + digitCount + 1;
}

unsigned int FromHex(char* dest, unsigned int destMaxLen, unsigned long value)
{
	int i = 8;
	int ptr = 0;

	while (i-- > 0)
	{
		dest[ptr] = "0123456789abcdef"[(value >> (i * 4)) & 0xf];
		++ptr;
	}

	return 8;
}

unsigned int FromHex(char* dest, unsigned int destMaxLen, unsigned short value)
{
	int i = 4;
	int ptr = 0;

	while (i-- > 0)
	{
		dest[ptr] = "0123456789abcdef"[(value >> (i * 4)) & 0xf];
		++ptr;
	}

	return 4;
}


};
