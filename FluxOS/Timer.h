#ifndef _TIMER_H_
#define _TIMER_H_

namespace Timer
{
	void Init();
	void Wait(unsigned long ticks);
};

#endif
