#include "HardDrive.h"
#include "System.h"
#include "Video.h"
#include "String.h"
#include "Timer.h"

namespace HardDrive
{

Device sDevice[4];
unsigned short sDeviceCount;

enum AccessType
{
	AT_Read,
	AT_Write,
};

void Init()
{
	// http://www.osdever.net/tutorials/lba.php
	// http://mattise.cvs.sourceforge.net/mattise/hdd_pio/hdd_main.c?revision=1.5&view=markup
	// http://www.t13.org/Documents/UploadedDocuments/project/d1410r3b-ATA-ATAPI-6.pdf
	sDeviceCount = 0;
	if (InitDevice(0x1F6, 0xA0, 0x1F7, 0x1F0))
		Video_Format("Primary IDE Master on port: %x\n", sDevice[sDeviceCount - 1].portStart);

	if (InitDevice(0x1F6, 0xB0, 0x1F7, 0x1F0))
		Video_Format("Primary IDE Slave on port: %x\n", sDevice[sDeviceCount - 1].portStart);

	if (InitDevice(0x176, 0xA0, 0x177, 0x170))
		Video_Format("Secondary IDE Master on port: %x\n", sDevice[sDeviceCount - 1].portStart);

	if (InitDevice(0x176, 0xB0, 0x177, 0x170))
		Video_Format("Secondary IDE Slave on port: %x\n", sDevice[sDeviceCount - 1].portStart);
}

bool InitDevice(unsigned short port, unsigned short writeReg, unsigned short readReg, unsigned short portStart)
{
	outportb(port, writeReg);
	Timer::Wait((unsigned long)1);
	short int tmpword = inportb(readReg); // read the status port
	if (tmpword & 0x40) // see if the busy bit is set
	{
		sDevice[sDeviceCount].driveNum = 0x80 + sDeviceCount;
		sDevice[sDeviceCount].portStart = portStart; //0x1F0;

		// http://www.t13.org/Documents/UploadedDocuments/project/d1410r3b-ATA-ATAPI-6.pdf
		// page 113 

		// which drive to work with?
		outportb(sDevice[sDeviceCount].portStart + HD_PORT_DRV_HEAD, 0xE0 | (sDevice[sDeviceCount].driveNum << 4));

		// write the identify device
		outportb(sDevice[sDeviceCount].portStart + HD_PORT_STATUS, 0xEC);

		// read in the signature
		unsigned char seccnt = inportb(sDevice[sDeviceCount].portStart + HD_PORT_SECT_COUNT);
		unsigned char secnum = inportb(sDevice[sDeviceCount].portStart + HD_PORT_SECT_NUM);
		unsigned char cyllo = inportb(sDevice[sDeviceCount].portStart + HD_PORT_CYL_LOW);
		unsigned char cylhi = inportb(sDevice[sDeviceCount].portStart + HD_PORT_CYL_HIGH);

		// check for packet devicesignature
		if (seccnt == 0x01 && secnum == 0x01 && cyllo == 0x14 && cylhi == 0xEB)
		{
			// TODO: handle packeted device
			Video_Format("Device is packet device, TODO\n");
			return false;
		}

		// wait for bytes to arrive
		while (!(inportb(sDevice[sDeviceCount].portStart + HD_PORT_STATUS) & 0x08)) 
		{
		}

		// read the bytes
		inportb(sDevice[sDeviceCount].portStart + HD_PORT_STATUS);
		inportb(sDevice[sDeviceCount].portStart + HD_PORT_STATUS);

		// read in 60 words
		for (int idx = 0; idx < 60; ++idx)
			inportw(sDevice[sDeviceCount].portStart);

		// read in total count of sectors
		unsigned short tsc[2];
		tsc[0] = inportw(sDevice[sDeviceCount].portStart);
		tsc[1] = inportw(sDevice[sDeviceCount].portStart);

		// read in till we get 256 words
		for (int idx = 0; idx < 194; ++idx)
			inportw(sDevice[sDeviceCount].portStart);

		// combine the two
		unsigned long totsect = *((unsigned long*) &tsc[0]);

		Video_Format("Total sectors: %u\n", totsect);

		// add to the array
		sDevice[sDeviceCount].totalSectors = totsect;

		++sDeviceCount;
		return true;
	}

	return false;
}

int	GetDeviceCount()
{
	return sDeviceCount;
}

const Device* GetDevice(int index)
{
	return &sDevice[index];
}

void LBAToCHS(const Device* device, unsigned int lbaAddr, unsigned int& cylinder, unsigned int& head, unsigned int& sector)
{
	cylinder = lbaAddr / (device->heads * device->sectors);
	head = (lbaAddr % (device->heads * device->sectors)) / device->sectors;
	sector = (lbaAddr % (device->heads * device->sectors)) % device->sectors + 1;
}

void CHSToLBA(const Device* device, unsigned int cylinder, unsigned int head, unsigned int sector, unsigned int& lbaAddr)
{
	lbaAddr = ((cylinder * device->heads + head) * device->sectors) + sector - 1;
}

void BeginDriveAccess(const Device* device, unsigned int lbaAddr, unsigned int sectorCount, AccessType accessType)
{
	// lba28
	// http://www.osdever.net/tutorials/lba.php
	// http://mattise.cvs.sourceforge.net/mattise/hdd_pio/hdd_main.c?view=markup

	unsigned char drive = device->driveNum;
	unsigned long addr = lbaAddr;
	outportb(device->portStart + 1, 0x00);
	outportb(device->portStart + 2, sectorCount);
	outportb(device->portStart + 3, (unsigned char)addr);
	outportb(device->portStart + 4, (unsigned char)(addr >> 8));
	outportb(device->portStart + 5, (unsigned char)(addr >> 16));
	outportb(device->portStart + 6, 0xE0 | (drive << 4) | ((addr >> 24) & 0x0F));

	if (accessType == AT_Read)
		outportb(0x1F7, HD_READ);
	else if (accessType == AT_Write)
		outportb(0x1F7, HD_WRITE);

	while ((inportb(device->portStart + HD_PORT_STATUS) & 0x08) != 0x08) 
	{
	}
}

void ReadSector(const Device* device, unsigned int lbaAddr, char* buffer, unsigned int sectorCount)
{
	BeginDriveAccess(device, lbaAddr, sectorCount, AT_Read);

	for (unsigned short idx = 0; idx < (512 * sectorCount); idx += sizeof(unsigned short))
	{
		unsigned short tmpword = inportw(device->portStart + HD_PORT_DATA);
		*((short*) &buffer[idx]) = tmpword;

		/*
		if (idx % 32 == 0)
			Video_Format("\n");
		else if (idx % 16 == 0)
			Video_Format(" ");

		Video_Format("%x", tmpword);*/		
	}
}

void WriteSector(const Device* device, unsigned int lbaAddr, char* buffer, unsigned int sectorCount)
{
	BeginDriveAccess(device, lbaAddr, sectorCount, AT_Write);

	for (unsigned short idx = 0; idx < (512 * sectorCount); idx += sizeof(unsigned short))
	{
		unsigned short tmpword = *((short*) &buffer[idx]);
		outportw(device->portStart + HD_PORT_DATA, tmpword);
	}
}

};
