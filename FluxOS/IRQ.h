#ifndef _IRQ_H_
#define _IRQ_H_

enum IRQ
{
	I_SystemTimer,
	I_Keyboard,
	I_CascadeInterrupt,
	I_SerialPort2,
	I_SerialPort1,
	I_SoundCard,
	I_FloppyDiskController,
	I_ParallelPort,
	I_RealTimeClock,

	I_IRQ9,
	I_USBHostController,
	I_IRQ11,

	I_PS2Mouse,
	I_Coprocessor,
	I_PrimaryIDE,
	I_SecondaryIDE,
};

#endif