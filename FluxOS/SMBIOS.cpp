#include "SMBIOS.h"
#include "Video.h"
#include "String.h"

namespace SMBIOS
{

enum SMType
{
	SMT_BIOS = 0,
	SMT_CPU = 4,
	SMT_RAM = 17,
	SMT_Motherboard = 2,
};

struct StringTable
{
	int			id;
	const char* name;
};

StringTable familyTable[] =
{
	{0x1, "Other"},
	{0x2, "Unknown"},
	{0x3, "8086"},
	{0x4, "80286"},
	{0x5, "Intel386� processor"},
	{0x6, "Intel486� processor"},
	{0x7, "8087"},
	{0x8, "80287"},
	{0x9, "80387"},
	{0xA, "80487"},
	{0xB, "Pentium� processor Family"},
	{0xC, "Pentium�  Pro processor"},
	{0xD, "Pentium�  II processor"},
	{0xE, "Pentium� processor with MMX� technology"},
	{0xF, "Celeron� processor"},
	{0x10, "Pentium�  II Xeon� processor"},
	{0x11, "Pentium�  III processor"},
	{0x12, "M1 Family"},
	{0x13, "M2 Family"},

	{0x18, "AMD Duron� Processor Family"},
	{0x19, "K5 Family"},
	{0x1A, "K6 Family"},
	{0x1B, "K6-2"},
	{0x1C, "K6-3"},
	{0x1D, "AMD Athlon� Processor Family"},
	{0x1E, "AMD2900 Family"},
	{0x1F, "K6-2+"},
	{0x20, "Power PC Family"},
	{0x21, "Power PC 601"},
	{0x22, "Power PC 603"},
	{0x23, "Power PC 603+"},
	{0x24, "Power PC 604"},
	{0x25, "Power PC 620"},
	{0x26, "Power PC x704"},
	{0x27, "Power PC 750"},

	{0x30, "Alpha Family3"},
	{0x31, "Alpha 21064"},

	{0x32, "Alpha 21066"},
	{0x33, "Alpha 21164"},
	{0x34, "Alpha 21164PC"},
	{0x35, "Alpha 21164a"},
	{0x36, "Alpha 21264"},
	{0x37, "Alpha 21364"},

	{0x40, "MIPS Family"},
	{0x41, "MIPS R4000"},
	{0x42, "MIPS R4200"},
	{0x43, "MIPS R4400"},
	{0x44, "MIPS R4600"},
	{0x45, "MIPS R10000"},

	{0x50, "SPARC Family"},
	{0x51, "SuperSPARC"},
	{0x52, "microSPARC II"},
	{0x53, "microSPARC IIep"},
	{0x54, "UltraSPARC"},
	{0x55, "UltraSPARC II"},
	{0x56, "UltraSPARC IIi"},
	{0x57, "UltraSPARC III"},
	{0x58, "UltraSPARC IIIi"},

	{0x60, "68040 Family"},
	{0x61, "68xxx"},
	{0x62, "68000"},
	{0x63, "68010"},
	{0x64, "68020"},
	{0x65, "68030"},

	{0x70, "Hobbit Family"},

	{0x78, "Crusoe� TM5000 Family"},
	{0x79, "Crusoe� TM3000 Family"},
	{0x7A, "Efficeon� TM8000 Family"},

	{0x80, "Weitek"},

	{0x82, "Itanium� processor"},
	{0x83, "AMD Athlon� 64 Processor Family"},
	{0x84, "AMD Opteron� Processor Family"},

	{0x90, "PA-RISC Family"},
	{0x91, "PA-RISC 8500"},
	{0x92, "PA-RISC 8000"},
	{0x93, "PA-RISC 7300LC"},
	{0x94, "PA-RISC 7200"},
	{0x95, "PA-RISC 7100LC"},
	{0x96, "PA-RISC 7100"},

	{0xA0, "V30 Family"},

	{0xB0, "Pentium�  III Xeon� processor"},
	{0xB1, "Pentium�  III Processor with Intel � SpeedStep� Technology"},
	{0xB1, "Pentium�  4 Processor"},

	{0xB3, "Intel� Xeon�"},
	{0xB4, "AS400 Family"},
	{0xB5, "Intel� Xeon� processor MP"},
	{0xB6, "AMD Athlon� XP Processor Family"},
	{0xB7, "AMD Athlon� MP Processor Family"},
	{0xB8, "Intel� Itanium� 2 processor"},
	{0xB9, "Intel� Pentium� M processor"},

	{0xC8, "IBM390 Family"},
	{0xC9, "G4"},
	{0xCA, "G5"},

	{0xFA, "i860"},
	{0xFB, "i960"},

	{0xFF, 0},
};

StringTable ramType[] =
{
	{0x1, "Other"},
	{0x2, "Unknown"},
	{0x3, "DRAM"},
	{0x4, "EDRAM"},
	{0x5, "VRAM"},
	{0x6, "SRAM"},
	{0x7, "RAM"},
	{0x8, "ROM"},
	{0x9, "FLASH"},
	{0xA, "EEPROM"},
	{0xB, "FEPROM"},
	{0xC, "EPROM"},
	{0xD, "CDRAM"},
	{0xE, "3DRAM"},
	{0xF, "SDRAM"},
	{0x10, "SGRAM"},
	{0x11, "RDRAM"},
	{0x12, "DDR"},
	{0x13, "DDR2"},

	{0xFF, 0},
};

struct SMBIOSInfo
{
	char		anchorString[4];
	char		checksum;
	char		length;
	char		majorVersion;
	char		minorVersion;
	short int	structSize;
	char		entryPointRevision;
	char		formattedArea[5];
	char		intermediateAnchorString[5];
	char		intermediateChecksum;
	short int	strucTableLength;
	int			structTableAddress;
	short int	numStructs;
	char		bcdRevision;
} __attribute__((packed));

struct SMBIOSStructHeader
{
	char		type;
	char		length;
	short int	handle;
} __attribute__((packed));

struct BIOSStruct
{
	char		vendor;
	char		version;
	short int	startAddress;
	char		releaseDate;
	char		ROMSize;
	long int	characteristics;
	char		extraCharacteristics[2];
	char		majorRelease;
	char		minorRelease;
	char		firmwareMajorRelease;
	char		firmwareMinorRelease;
} __attribute__((packed));

struct CPUStruct
{
	char		socketDesignation;
	char		type;
	char		family;
	char		manufacturer;
	long long int	id;
	char		version;
	char		voltage;
	short int	externalClock; // MHz
	short int	maxSpeed;	// MHz
	short int	currentSpeed; // MHz
	char		status;
	char		upgrade;
	short int	l1CacheHandle;
	short int	l2CacheHandle;
	short int	l3CacheHandle;
	char		serialNumber;
	char		assetTag;
	char		partNumber;
} __attribute__((packed));


struct MotherboardStruct
{
	char		manufacturer;
	char		product;
	char		version;
	char		serialNumber;
	char		assetTag;
	char		featureFlags;
	char		locationInChassis;
	short int	chassisHandle;
	char		boardType;
	char		numberOfHandles;
} __attribute__((packed));

struct RAMStruct
{
	short int	arrayHandle;
	short int	errorInfoHandle;
	short int	totalWidth;
	short int	dataWidth;
	short int	size;
	char		formFactor;
	char		deviceSet;
	char		deviceLocator;
	char		byteLocator;
	char		type;
	short int	typeDetail;
	short int	speed;
	char		manufacturer;
	char		serialNumber;
	char		assetTag;
	char		partNumber;
};

void Init()
{
	// search along 16byte boundries between
	// 0x000F0000 -> 0x000FFFFF
	// for the 4 byte string _SM_
	SMBIOSInfo* pSMBIOSInfo = (SMBIOSInfo*)0x000F0000;
	while (pSMBIOSInfo < (SMBIOSInfo*)0x000FFFFF)
	{
		pSMBIOSInfo = (SMBIOSInfo*)(((char*)pSMBIOSInfo) + 16); // add 16bytes
		
		if (String::Compare("_SM_", pSMBIOSInfo->anchorString, 4) == -1)
		{
			//Video_Format("SMBIOS v%i.%i\n", pSMBIOSInfo->majorVersion, pSMBIOSInfo->minorVersion);
			break;
		}
	}

	//Video_Format("Number of structs: %i\n", pSMBIOSInfo->numStructs);
	SMBIOSStructHeader* pHeader = (SMBIOSStructHeader*)pSMBIOSInfo->structTableAddress;
	for (int i = 0; i < pSMBIOSInfo->numStructs; ++i)
	{
		switch (pHeader->type)
		{
		case SMT_BIOS:
			{
				BIOSStruct* pBIOS = (BIOSStruct*)(((char*)pHeader) + sizeof(SMBIOSStructHeader));
				const char* stringTable = (const char*)(((char*)pBIOS) + sizeof(BIOSStruct));
				Video_Format("BIOS: %s, v%s\n", GetString(stringTable, pBIOS->vendor), GetString(stringTable, pBIOS->version));
			}
			break;

		case SMT_CPU:
			{
				CPUStruct* pCPU = (CPUStruct*)(((char*)pHeader) + sizeof(SMBIOSStructHeader));
				//const char* stringTable = (const char*)(((char*)pCPU) + sizeof(CPUStruct));

				int f = 0;
				while (familyTable[f].id != pCPU->family && familyTable[f].name)
					++f;

				// if uknown
				if (!familyTable[f].name)
					f = 2;

				Video_Format("Processor: %s, %iMHz, %iMHz FSB\n", familyTable[f].name, pCPU->currentSpeed, pCPU->externalClock);
			}
			break;

		//default:
		//case 16:
			//Video_Format("HType: %i\t", pHeader->type);
			//break;

		case SMT_RAM:
			{
				RAMStruct* pRAM = (RAMStruct*)(((char*)pHeader) + sizeof(SMBIOSStructHeader));
				//const char* stringTable = (const char*)(((char*)pRAM) + sizeof(RAMStruct));

				int r = 0;
				while (ramType[r].id != pRAM->type && ramType[r].name)
					++r;

				// if uknown
				if (!ramType[r].name)
					r = 2;

				Video_Format("RAM: %iMB %s, %iMHz\n", pRAM->size, ramType[r].name, pRAM->speed);
			}
			break;

		case SMT_Motherboard:
			{
				MotherboardStruct* pMobo = (MotherboardStruct*)(((char*)pHeader) + sizeof(SMBIOSStructHeader));
				const char* stringTable = (const char*)(((char*)pMobo) + sizeof(MotherboardStruct));
				Video_Format("Motherboard: %s, %s\n", GetString(stringTable, pMobo->product), GetString(stringTable, pMobo->manufacturer));
			}
			break;
		}

		pHeader += pHeader->length;
	}
}

const char* GetString(const char* stringTableAddr, int index)
{
	const char* str = stringTableAddr;
	for (int i = 0; i < (index - 1); ++i)
	{
		int len = String::Length(str);
		str += len + 1;
	}

	return str;
}

}
