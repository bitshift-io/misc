#ifndef _FAT32_H_
#define _FAT32_H_

//
// File Allocation Table 32
// http://home.teleport.com/~brainy/fat32.htm
//
//		FAT32 works like so:
// The first sector of the partiton is the FAT Boot record, which gives info about sectors per cluster etc.
// The second cluster in the partition is the start of the data area, this is raw fil information
// Each cluster has a corrisponding 32-bit FAT entry (in front of the data area probably) which either
// says if the cluster is the last cluster of the file OR the next cluster of the file
//
namespace FS
{

struct PartitionEntry
{
	unsigned char	state;
	unsigned char	startHead;
	unsigned short	startCylinderSector;
	unsigned char	type;
	unsigned char	endHead;
	unsigned short	endCylinderSector;
	unsigned long	lbaStart;
	unsigned long	numSectors;
}  __attribute__((packed));

struct MasterBootRecord
{
	unsigned char	executableCode[446];
	PartitionEntry	partitions[4];
	unsigned short	bootRecordSignature;
}  __attribute__((packed));

struct BootRecord
{
	unsigned char	jumpCode[3];
	char			OEMName[8];
	unsigned short	bytesPerSector;
	unsigned char	sectorsPerCluster;
	unsigned short	reservedSectors;
	unsigned char	numberOfCopiesOfFAT;
	unsigned short	maxRootDirEntries;
	unsigned short	totalSectors16;
	unsigned char	mediaDescriptor;
	unsigned short	sectorsPerFAT16;
	unsigned short	sectorsPerTrack;
	unsigned short	numHeads;
	unsigned long	hiddenSectors;
	unsigned long	totalSectors32;

	unsigned long	sectorsPerFAT32;
	unsigned short	flags;
	unsigned char	majorVersion;
	unsigned char	minorVersion;
	unsigned long	rootDirCluster;
	unsigned short	FSInfoSectorNum;
	unsigned short	backupBootSectorNum;
	unsigned char	reserved[12];
	unsigned char	logicalDriveNumOfPartition;
	unsigned char	unused;
	unsigned char	extendedSignature;
	unsigned long	serialNum;
	char			volumeLabel[11];
	char			FATName[8];
	unsigned char	executableCode[420];
	unsigned short	bootRecordSignature;
}  __attribute__((packed));

struct FileInformationSystem
{
	unsigned long	firstSignature;
	unsigned char	unknown[480];
	unsigned long	signature;
	unsigned long	numFreeSectors;
	unsigned long	mostRecentlyAllocatedClusterNum;
	unsigned char	reserved[12];
	unsigned char	unknown2[2];
	unsigned short	bootRecordSignature;
} __attribute__((packed));

struct Directory
{
	unsigned char	name[11];
	unsigned char	attributes;
	unsigned char	NTReserved;
	unsigned char	createTimeTeenth;
	unsigned short	createTime;
	unsigned short	createDate;
	unsigned short	lastAccessDate;
	unsigned short	firstClusterHigh;
	unsigned short	lastWriteTime;
	unsigned short	lastWriteDate;
	unsigned short	firstClusterLow;
	unsigned long	fileSize;
} __attribute__((packed));

enum Attributes
{
	A_ReadOnly		= 0x1,
	A_Hidden		= 0x2,
	A_System		= 0x4,
	A_VolumeId		= 0x8,
	A_Directory		= 0x10,
	A_Archive		= 0x20,
	A_LongName		= A_ReadOnly | A_System | A_Hidden | A_VolumeId,
};


bool			Init();
bool			FormatDrive();

// returns the number of clusters a file takes up
unsigned long	GetFileClusterCount(unsigned long startCluster);

// remember, cluster 2 is the start of the data area
unsigned long	GetSector(unsigned long cluster);
unsigned long	GetFATEntry(unsigned long cluster);
bool			SetFATEntry(unsigned long cluster, unsigned long fatEntry);

// this is used when creating or enlarging files/directories
unsigned long	FindFreeCluster();

bool			SetDirectory(Directory& directory, unsigned long sector, unsigned long offset);
bool			SetDirectoryName(Directory& directory, char* name);

bool			GetDirectory(Directory& directory, unsigned long sector, unsigned long offset);
bool			GetDirectoryName(Directory& directory, char* name, unsigned long size);
bool			IsEOF(unsigned long fatEntry);



enum FileFlags
{
	FF_File			= 0x1 << 0,
	FF_Directory	= 0x1 << 1,
	FF_Volume		= 0x1 << 2, // root dir

	FF_Read			= 0x1 << 3, 
	FF_Write		= 0x1 << 4, 
	FF_Append		= 0x1 << 5, 

	FF_SeekStart	= 0x1 << 6,
	FF_SeekCurrent	= 0x1 << 7,
	FF_SeekEnd		= 0x1 << 8,

};

class Node;

class File
{
public:

	virtual bool	Open(const char* path, FileFlags accessFlags);
	void			Close();

	unsigned long	Read(char* buffer, unsigned long size);
	unsigned long	Write(char* buffer, unsigned long size);

	bool			IsEOF();
	bool			Seek(FileFlags seekType, unsigned long offset);

//protected:

	// expand the file or contract it
	bool			Expand();
	bool			Contract();

	bool			Open(Node& node);

	// uses the cursor to find the current cluster sector and offset
	// returns false if end of file was reached
	bool			GetClusterSectorOffset(unsigned long& cluster, unsigned long& sector, unsigned long& offset);

	char			name[256];
	unsigned long	handle;		// handle to the file
	unsigned long	cursor;
	unsigned long	fileSize;	// size of file
	unsigned short	flags;
};

//
// represents a directory and or file
// 
class Node : public File
{
public:

	// a file or folder/directory are just nodes
	virtual bool	Open(const char* path, FileFlags accessFlags = FF_Read);
	void			Close();

	//
	// get a list of files inside the directory
	//
	unsigned long	GetChildCount();
	bool			GetChild(Node& child, unsigned long index);
	bool			GetParent(Node& parent);

	bool			RemoveChild(unsigned long index);
	bool			AddChild(Node& child);

//protected:

	//unsigned long	handle;		// handle to the directory
	//unsigned long	fileSize;
};

};

#endif
