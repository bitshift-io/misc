#include "Command.h"
#include "Video.h"
#include "String.h"
#include "FAT32.h"
#include "HardDrive.h"
#include "Memory.h"

namespace Command
{

// hdx = hard drive partition x
// cdx = cd/dvd drive x
// msx = memstick x
char currentDir[2048] = "hd0";

void DrawPrompt()
{
	// draw current directory then the prompt
	Video_SetTextColor(C_Green, C_Black);
	Video_Format("\n%s", currentDir);
	Video_SetTextColor(C_LightGrey, C_Black);
	Video_Format("\n>", currentDir);
}

void Execute(const char* str)
{
	Video_PutString("\n");

	int offset = String::FindFirstOf(str, " ");

	char command[256];
	String::SubString(str, command, offset);

	if (str[offset] == '\0') // not found
	{
		offset = String::Length(str);
	}
	else
	{
		++offset;
	}

	//Video_Format("line: '%s'\n", str);
	//Video_Format("command: '%s'\n", command);
	//Video_Format("args: '%s'\n", &str[offset]);

	if (String::Compare(command, "ld") == -1)
	{
		ListDirectory(&str[offset]);
	}
	else if (String::Compare(command, "cd") == -1)
	{
		ChangeDirectory(&str[offset]);
	}
	else if (String::Compare(command, "outportb") == -1)
	{
		int port = 0;
		int addr = 0;
		offset += String::ToInt(&str[offset], port) + 1;
		String::ToInt(&str[offset], addr);

		Video_Format("outportb %i %i\n", port, addr);
		outportb(port, addr);
	}
	else if (String::Compare(command, "inportb") == -1)
	{
		int port = 0;
		int addr = 0;
		offset += String::ToInt(&str[offset], port) + 1;

		Video_Format("inportb: 0x%x\n", inportb(port));
	}
	else if (String::Compare(command, "format") == -1)
	{
		Video_Format("Formating...");
		const HardDrive::Device* device = HardDrive::GetDevice(0);
		char buffer[512];
		memset(buffer, 0, 512);
		for (unsigned long s = 0; s < device->totalSectors; ++s)
		{
			HardDrive::WriteSector(device, s, buffer);
			Video_Format(".");
		}
		//FAT32::FormatDrive();
		Video_Format("[SUCESSFUL]\n");
	}
	else if (String::Compare(command, "clearmbr") == -1)
	{
		Video_Format("Invalidating the master boot record");
		char buffer[512];
		memset(buffer, 0, 512);
		HardDrive::WriteSector(HardDrive::GetDevice(0), 0, buffer);
	}
	else if (String::Compare(command, "dumpsector") == -1)
	{
		int lbaAddr;
		String::ToInt(&str[offset], lbaAddr);
		Video_Format("dumping sector %i:\n", lbaAddr);

		char buffer[512];
		memset(buffer, 0, 512);
		HardDrive::ReadSector(HardDrive::GetDevice(0), lbaAddr, buffer);

		unsigned short* shortBuffer = (unsigned short*)buffer;
		for (int s = 0; s < 256; ++s)
		{
			if (s % 16 == 0)
			{
				Video_Format("\n");
				Video_Format("%x | ", s*2);				
			}
			else if (s % 2 == 0)
			{
				Video_Format(" ");			
			}

			Video_Format("%x", CONVERT_ENDIAN(shortBuffer[s]));			
		}
	}
	else if (String::Compare(command, "dumpsectorstring") == -1)
	{
		int lbaAddr;
		String::ToInt(&str[offset], lbaAddr);
		Video_Format("dumping sector %i:\n", lbaAddr);

		char buffer[512];
		memset(buffer, 0, 512);
		HardDrive::ReadSector(HardDrive::GetDevice(0), lbaAddr, buffer);

		for (int s = 0; s < 512; ++s)
		{
			if (s % 32 == 0)
			{
				Video_Format("\n");
				Video_Format("%x | ", s);				
			}
			
			Video_Format("%c", buffer[s]);			
		}
	}
	else if (String::Compare(command, "dumpfile") == -1)
	{
		DumpFile(&str[offset]);
	}
	else if (String::Compare(command, "dumpfilestring") == -1)
	{
		DumpFileString(&str[offset]);
	}
	else if (String::Compare(command, "ad") == -1)
	{
		AddDirectory(&str[offset]);
	}
	else if (String::Compare(command, "rd") == -1)
	{
		RemoveDirectory(&str[offset]);
	}
	else if (String::Compare(command, "reboot") == -1)
	{
		Video_Format("to do...");
	}
	else
	{
		Video_SetTextColor(C_Red, C_Black);
		Video_PutString("Command '");
		Video_PutString(command);
		Video_PutString("' not found\n");
		Video_SetTextColor(C_LightGrey, C_Black);
	}
	
	Command::DrawPrompt();
}

void AddDirectory(const char* args)
{
	Video_Format("Adding directory '%s'\n", args);

	FS::Node child;
	child.flags = FS::FF_Directory;
	String::Copy(child.name, args);
	child.fileSize = 0;
	child.handle = 0;

	FS::Node parent;
	parent.Open(currentDir);
	if (!parent.AddChild(child))
	{
	}

	parent.Close();
	
}

void RemoveDirectory(const char* args)
{
	FS::Node curDirNode;
	curDirNode.Open(currentDir);
	
	unsigned long childCount = curDirNode.GetChildCount(); //FAT32::GetNodeChildCount(curDirNode);
	for (int i = 0; i < childCount; ++i)
	{
		FS::Node childNode;
		curDirNode.GetChild(childNode, i);

		char name[256];
		unsigned long lastSlash = String::FindLastOf(childNode.name, "/");
		if (String::Compare(&childNode.name[lastSlash + 1], args) == -1)
		{
			Video_Format("Directory/file '%s' found, Removing\n", args);
			curDirNode.RemoveChild(i);
			curDirNode.Close();
			return;
		}		
	}

	curDirNode.Close();
	Video_Format("Directory/file '%s' does not exist\n", args);
}

void DumpFile(const char* args)
{
	Video_Format("Dumping file '%s'\n", args);

	char fileName[256];
	String::Format(fileName, 256, "%s/%s", currentDir, args);
	FS::File file;
	if (!file.Open(fileName, FS::FF_Read))
	{
		Video_Format("File '%s' does not exist\n", fileName);
		return;
	}

	//Video_Format("yo0\n");

	while (!file.IsEOF())
	{
		// dump file
		char buffer[512];
		memset(buffer, 0, 512);
		file.Read(buffer, 512);

		//Video_Format("yo1\n");

		unsigned short* shortBuffer = (unsigned short*)buffer;
		for (int s = 0; s < 256; ++s)
		{
			if (s % 16 == 0)
			{
				Video_Format("\n");
				Video_Format("%x | ", s*2);				
			}
			else if (s % 2 == 0)
			{
				Video_Format(" ");			
			}

			Video_Format("%x", shortBuffer[s]);			
		}
	}

	//Video_Format("yo2\n");
}

void DumpFileString(const char* args)
{
	Video_Format("Dumping file '%s'\n", args);

	char fileName[256];
	String::Format(fileName, 256, "%s/%s", currentDir, args);
	FS::File file;
	if (!file.Open(fileName, FS::FF_Read))
	{
		Video_Format("File '%s' does not exist\n", fileName);
		return;
	}

	//Video_Format("yo0\n");



	//Video_Format("yo1\n");

	while (!file.IsEOF())
	{
		// dump file
		char buffer[512];
		memset(buffer, 0, 512);
		file.Read(buffer, 512);

		for (int s = 0; s < 512; ++s)
		{/*
			if (s % 32 == 0)
			{
				Video_Format("\n");
				Video_Format("%x | ", s*2);				
			}*/
			
			Video_Format("%c", buffer[s]);			
		}
	}

	//Video_Format("yo2\n");
}

void ListDirectory(const char* args)
{
	FS::Node curDirNode;
	if (!curDirNode.Open(currentDir)) //FAT32::GetNode(currentDir, curDirNode))
	{
		Video_Format("Directory '%s' does not exist\n", currentDir);
		return;
	}

	Video_Format("\n");

	unsigned long childCount = curDirNode.GetChildCount(); //FAT32::GetNodeChildCount(curDirNode);
	for (int i = 0; i < childCount; ++i)
	{
		FS::Node childNode;
		curDirNode.GetChild(childNode, i);

		//FAT32::GetChild(curDirNode, childNode, i);

		Video_Format("%s\n", childNode.name);
	}

	Video_Format("\n%u Files and/or Directories\n", childCount);

	Video_Format("handle(cluster) [%u] sector [%u]\n", curDirNode.handle, FS::GetSector(curDirNode.handle));
	curDirNode.Close();
}

void ChangeDirectory(const char* args)
{
	char newDir[2048];
	char device[256];
	unsigned int firstSlash = String::FindFirstOf(args, "/");
	String::SubString(args, device, firstSlash);

	// check if the first part specifiees a device
	if (String::Compare(device, "hd0") == -1)
	{
		// it does, so the whole path is absolute path, assume its ccorrect for the time being
		String::Copy(newDir, args);
	}
	else
	{
		// append it (ignore . and .. for the time being)
		String::Format(newDir, 2048, "%s/%s", currentDir, args);		
	}

	FS::Node curDirNode;
	if (!curDirNode.Open(newDir) || (curDirNode.flags & FS::FF_File)) //FAT32::GetNode(newDir, curDirNode))
	{
		Video_Format("Directory '%s' does not exist or is not a directory\n", args);
		return;
	}

	String::Copy(currentDir, curDirNode.name); //newDir);
	curDirNode.Close();
}

};
