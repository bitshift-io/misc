
// Install Time library referenced here: http://playground.arduino.cc/Code/Time
// Install u8g2 library: https://github.com/olikraus/u8g2/wiki


#include <Arduino.h>
#include <SPI.h>
#include <U8g2lib.h>
#include <TimeLib.h> 

/* Constructor */

#define PICTURE_BUFFER_MODE

// picture buffer mode
#ifdef PICTURE_BUFFER_MODE
  U8G2_SH1106_128X64_NONAME_1_SW_I2C u8g2(U8G2_R0, /* clock=*/ A5, /* data=*/ A4);
#else
  // full screen buffer mode
  U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, /* clock=*/ A5, /* data=*/ A4);
#endif

#define TIME_MSG_LEN  11   // time sync to PC is HEADER followed by Unix time_t as ten ASCII digits
#define TIME_HEADER  'T'   // Header tag for serial time sync message
#define TIME_REQUEST  7    // ASCII bell character requests a time sync message

// T1262347200  //noon Jan 1 2010


/* u8g2.begin() is required and will sent the setup/init sequence to the display */
void setup(void) {
  Serial.begin(9600);
  u8g2.begin();
}

/* draw something on the display with the `firstPage()`/`nextPage()` loop*/
void loop(void) {

  if(Serial.available())
  {
    processSyncMessage();
  }

  String timeStr = formatTimeString(hour()) + ":" + formatTimeString(minute()) + ":" + formatTimeString(second());
  timeStr += isAM() ? " AM" : " PM";
  
  String dateStr = formatTimeString(day()) + "/" + formatTimeString(month()) + "/" + year();

  if(timeStatus() == timeNotSet)
  {
      timeStr = "waitn 4 sync";
      dateStr = "msg";
  }
  
#ifdef PICTURE_BUFFER_MODE
  u8g2.firstPage();
  do 
  {
    draw(timeStr, dateStr);    
  } while (u8g2.nextPage());
#else
  u8g2.clearBuffer();
  draw(timeStr, dateStr);    
  u8g2.sendBuffer();
#endif
  
  delay(1000);
}

void draw(String timeStr, String dateStr)
{
    u8g2.setFont(u8g2_font_ncenB14_tr); 
    u8g2.drawStr(0, 20, timeStr.c_str()); 
    u8g2.setFont(u8g2_font_ncenB10_tr); 
    u8g2.drawStr(0, 40, dateStr.c_str()); 
}

String formatTimeString(int value)
{
  String result = "";
  if (value < 10)
    result += "0";

  result += value;
  return result;
}


void digitalClockDisplay(){

  String timeStr = String(hour()) + ":" + minute() + "." + second();
  u8g2.drawStr(0, 20, timeStr.c_str());
  
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" ");
  Serial.print(day());
  Serial.print(" ");
  Serial.print(month());
  Serial.print(" ");
  Serial.print(year());
  Serial.println();

  
  
}

void printDigits(int digits){
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void processSyncMessage() {
  // if time sync available from serial port, update time and return true
  while(Serial.available() >=  TIME_MSG_LEN ){  // time message consists of header & 10 ASCII digits
    char c = Serial.read() ;
    Serial.print(c);  
    if( c == TIME_HEADER ) {      
      time_t pctime = 0;
      for(int i=0; i < TIME_MSG_LEN -1; i++){  
        c = Serial.read();          
        if( c >= '0' && c <= '9'){  
          pctime = (10 * pctime) + (c - '0') ; // convert digits to a number    
        }
      }  
      setTime(pctime);   // Sync Arduino clock to the time received on the serial port
    }  
  }
}
