#include "engine/stdafx.h"
#include "AutoCam.h"

AutoCam::AutoCam( Direct3D *pD3D, INIFile *ini, FileManager *pFM, std::string meshPath ) : Camera(pD3D, ini)
{
	SetPosition( 0, 0, 0);

	Camera::Camera(pD3D, ini);	

	SKAFile s5;
	s5.LoadAnimation( meshPath, "camera01.SKA");
	s5.LoadIntoObject( (Object*)this );

	SKAFile s;
	s.LoadAnimation( meshPath, "camera02.SKA");
	s.LoadIntoObject( (Object*)this );

	SKAFile s1;
	s1.LoadAnimation( meshPath, "camera03.SKA");
	s1.LoadIntoObject( (Object*)this );

	SKAFile s2;
	s2.LoadAnimation( meshPath, "camera04.SKA");
	s2.LoadIntoObject( (Object*)this );

	SKAFile s4;
	s4.LoadAnimation( meshPath, "camera05.SKA");
	s4.LoadIntoObject( (Object*)this );	

	SKAFile s6;
	s6.LoadAnimation( meshPath, "camera06.SKA");
	s6.LoadIntoObject( (Object*)this );

	SKAFile s3;
	s3.LoadAnimation( meshPath, "camera07.SKA");
	s3.LoadIntoObject( (Object*)this );

	SKAFile s7;
	s7.LoadAnimation( meshPath, "camera08.SKA");
	s7.LoadIntoObject( (Object*)this );

	//if( animations.size() >= 2 )
	Log("num Anims:%i", animations.size());

	bFreeCam = false;
	//PlayAnimation( 0, false );
	StopAnimation();

	
}

AutoCam::~AutoCam()
{
	Camera::~Camera();
}

void AutoCam::StopAnimation()
{
	//play another animation but, not this one
	if( !bFreeCam )
	{
		int newAnim = rand() % GetNumAnimations();

		while( newAnim == currentAnimation )
		{
			newAnim = rand() % GetNumAnimations();	
		}

		PlayAnimation( newAnim , false);
	}
	else
	{
		Camera::StopAnimation();
	}
}

void AutoCam::Update( float const deltaTime, World* pW, DirectInput* pDI )
{
	Camera::Update(deltaTime, pW, pDI);

	if( pDI->KeyPressed( DIK_1 ) )
		PlayAnimation( 0, false );
	if( pDI->KeyPressed( DIK_2 ) )
		PlayAnimation( 1, false );
	if( pDI->KeyPressed( DIK_3 ) )
		PlayAnimation( 2, false );
	if( pDI->KeyPressed( DIK_4 ) )
		PlayAnimation( 3, false );
	if( pDI->KeyPressed( DIK_5 ) )
		PlayAnimation( 4, false );
	if( pDI->KeyPressed( DIK_6 ) )
		PlayAnimation( 5, false );
	if( pDI->KeyPressed( DIK_7 ) )
		PlayAnimation( 6, false );
	if( pDI->KeyPressed( DIK_8 ) )
		PlayAnimation( 7, false );

	if( pDI->KeyPressed( DIK_F ) )
	{
		bFreeCam = !bFreeCam;
		StopAnimation();
	}

	//if( pDI->KeyPressed( DIK_W ) )
	//	SetWireFrame( true );
}