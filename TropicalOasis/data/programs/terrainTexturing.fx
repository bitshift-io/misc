
float4x4 worldViewProj : WorldViewProjection; 
float4x4 boneMatrix[24];

texture testTexture; // This texture will be loaded by the application

sampler Sampler = sampler_state
{
    Texture   = (testTexture);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

struct vertex
{
    float3 position : POSITION;
    float2 texture0 : TEXCOORD0;
    float4 boneId   : TEXCOORD1; //BLENDINDICES;
    float4 weight   : TEXCOORD2; //BLENDWEIGHT;    
};

struct fragment
{
    float4 position : POSITION;
    float2 texture0 : TEXCOORD0;
    float4 color    : COLOR;
};

struct pixel
{
    float4 color : COLOR;
};

fragment vertexSkin( vertex IN )
{
    fragment OUT;

    float3 netPos = 0;      
    float3x4 model2;
    float idx = 0;
    int i = 0;
    float3 bonePos = 0;
    
    i = 0;
    idx = IN.boneId[i]; 

    model2 = float3x4( boneMatrix[idx][0],
    			   boneMatrix[idx][1],
			   boneMatrix[idx][2] );
			   
    bonePos = mul( model2, float4(IN.position, 1) );
    netPos += IN.weight[i] * bonePos;

    
    
    for(int i=0; i < 4; i++)
    {
    	if( IN.weight[i] != 0 && IN.boneId[i] >= 0)
    	{
		int idx = IN.boneId[i];

		//float4x4 model = boneMatrix[idx];
/*
		float3x4 model2 = float3x4( boneMatrix[idx][0],
					    boneMatrix[idx][1],
					    boneMatrix[idx][2] );*/

			
		model2 = float3x4( boneMatrix[i][0],
				boneMatrix[i][1],
				boneMatrix[i][2] );

		/*
		model2 = float3x4( float4(1,0,0,0),
				   float4(0,1,0,0),
				   float4(0,0,1,0) );*/
/*
		float3 bonePos = mul( model2, float4(IN.position, 1) );
		netPos += IN.weight[i] * bonePos;*/
    	}
    }
    
    //OUT.position = mul( worldViewProj, float4(IN.position, 1) );
    OUT.position = mul( worldViewProj, float4(netPos,1) );
    OUT.texture0 = IN.texture0;

    OUT.color.r = idx;
    OUT.color.g = 0;
    OUT.color.b = 0;

    OUT.color.a = 1;

    return OUT;
}

pixel fragmentSkin( fragment IN )
{
    pixel OUT;

    //OUT.color = tex2D( Sampler, IN.texture0 ) * IN.color;
    
    OUT.color = IN.color;
    

    return OUT;
}

//-----------------------------------------------------------------------------
// Simple Effect (1 technique with 1 pass)
//-----------------------------------------------------------------------------

technique Technique0
{
    pass Pass0
    {
		Lighting = FALSE;

		VertexShader = compile vs_1_1 vertexSkin();
		PixelShader  = compile ps_1_1 fragmentSkin();
    }
}

