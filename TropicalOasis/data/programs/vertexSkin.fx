
float4x4 worldViewProj : WorldViewProjection; 
float4x4 boneMatrices[24];

texture testTexture; // This texture will be loaded by the application

sampler Sampler = sampler_state
{
    Texture   = (testTexture);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

struct vertex
{
    float3 position : POSITION;
    float2 texture0 : TEXCOORD0;
    float4 boneId     : BLENDINDICES;
    float4 weight   : BLENDWEIGHT;
 //   float4 numBones  : COLOR1;
};

struct fragment
{
    float4 position : POSITION;
    float2 texture0 : TEXCOORD0;
    float4 color    : COLOR;
};

struct pixel
{
    float4 color : COLOR;
};


float4 skinPoint( inout float4 io_value, float4x4 blendMats[24],  float4 indices, float4 weights)
{
    int i;
    float4 incoming_io_value = io_value;

    //if(weights[0] != -1 )
    //{
    //    io_value = 0;
        // skin
        i = 0;
        //for(i=0; i <4 ; i++)
        //{
            //if( weights[i] > 0 )
            //{
            	//io_value  += mul( incoming_io_value, blendMats[indices[i]]) * weights[i];
            	
            	for( i =0; i < 4; i++)
            	{
            		float idx = indices[i];
            		float wght = weights[i];
            		
            		
            		if( indices[i] <= 20 )
            		{
            			io_value  += mul( incoming_io_value, blendMats[idx]);
            		}
            		//io_value  += mul( incoming_io_value, blendMats[i]);
            	}
            //}
        //}
    //}
  //  return io_value;
  return io_value;
}

fragment vertexSkin( vertex IN )
{
    fragment OUT;
    
    
    float4 index = IN.boneId;
    float4 weight = IN.weight;
    
    float4 position;
    float3 normal;
    
    //for (float i = 0; i < IN.numBones.x; i += 1)
    //{
    /*
            // transform the offset by bone i
            position = position + weight.x * float4(mul(boneMatrices[index.x], IN.position).xyz, 1.0);
    
            // transform normal by bone i
            normal = normal + weight.x * mul((float3x3)boneMatrices[index.x], IN.normal.xyz).xyz;
    
            // shift over the index/weight variables, this moves the index and 
            // weight for the current bone into the .x component of the index 
            // and weight variables
            index = index.yzwx;
            weight = weight.yzwx;*/
    //}
    
    //normal = normalize(normal);
    
    float4 pos = float4( IN.position, 1);
    skinPoint( pos, boneMatrices, IN.boneId, IN.weight); 
    OUT.position = mul( pos, worldViewProj );
    //OUT.color = dot(normal, lightPos.xyz) * color;
    

/*
    float4 pos = float4( IN.position, 1);
    //skinPoint( pos, boneMatrices, IN.boneId, IN.weight); // pos is changed.

    OUT.position = mul( pos, worldViewProj );
    */
    OUT.texture0 = IN.texture0;
    
    //if( IN.boneId[0] )
    OUT.color = float4( 1, 0, 0, 1);
    
    return OUT;
}

pixel fragmentSkin( fragment IN )
{
    pixel OUT;

    //OUT.color = tex2D( Sampler, IN.texture0 ) * IN.color;
    
    OUT.color = IN.color;
    

    return OUT;
}

//-----------------------------------------------------------------------------
// Simple Effect (1 technique with 1 pass)
//-----------------------------------------------------------------------------

technique Technique0
{
    pass Pass0
    {
		VertexShader = compile vs_1_1 vertexSkin();
		PixelShader  = compile ps_1_1 fragmentSkin();
    }
}

