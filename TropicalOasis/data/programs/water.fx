
float4x4 worldViewProj : WorldViewProjection; 
float4x4 boneMatrices[24];

texture testTexture; // This texture will be loaded by the application
texture normalTexture;

int time;

sampler Sampler = sampler_state
{
    Texture   = (testTexture);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

sampler NormalSampler = sampler_state
{
    Texture   = (normalTexture);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

struct vertex
{
    float3 position : POSITION;
    float2 texture0 : TEXCOORD0;
    //float4 boneId     : BLENDINDICES;
    //float4 weight   : BLENDWEIGHT;    
};

struct fragment
{
    float4 position : POSITION;
    float2 texture0 : TEXCOORD0;
    float4 color    : COLOR;
    float2 cube	: TEXCOORD1;
};

struct pixel
{
    float4 color : COLOR;
};

fragment vertexSkin( vertex IN )
{
    fragment OUT;

    float4 pos = float4( IN.position, 1);
  
    OUT.position = mul( pos, worldViewProj );
    
    OUT.texture0 = IN.texture0;
    OUT.color = float4( 1, 0, 0, 1);
    
    OUT.cube = IN.texture0 * cos( time );//mul( pos, worldViewProj );
    
    return OUT;
}

pixel fragmentSkin( fragment IN )
{
    pixel OUT;


	OUT.color = tex2D( NormalSampler, IN.texture0 );	
	float4 color = tex2D( Sampler, IN.cube);//IN.cube ); 	
	OUT.color = lerp( OUT.color, color, 0.2);
	
	
	//OUT.color = lerp( OUT.color, color, 0.2);
/*
	float3 normal = float3(1,1,1);//tex2D( NormalSampler, IN.texture0 );
    
    //OUT.color = tex2D( Sampler, IN.texture0 ) * IN.color;
    
    OUT.color = texCUBE( Sampler, normal ) * IN.color;
    
    //OUT.color = IN.color;*/
    

    return OUT;
}

//-----------------------------------------------------------------------------
// Simple Effect (1 technique with 1 pass)
//-----------------------------------------------------------------------------

technique Technique0
{
    pass Pass0
    {
		VertexShader = compile vs_1_1 vertexSkin();
		PixelShader  = compile ps_1_1 fragmentSkin();
    }
}

