
float4x4 world : World;
float4x4 worldViewProj : WorldViewProjection; // This matrix will be loaded by the application

texture testTexture; // This texture will be loaded by the application
texture cubeTexture; // This texture will be loaded by the application

sampler Sampler = sampler_state
{
    Texture   = (testTexture);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

sampler Sampler2 = sampler_state
{
    Texture   = (cubeTexture);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

struct vertex
{
    float3 position	: POSITION;
    float3 normal	: NORMAL;
    float2 texture0 : TEXCOORD0;
};

struct fragment
{
    float4 hposition : POSITION;
    float2 texture0  : TEXCOORD0;
    float3 cube	: TEXCOORD1;
};

struct pixel
{
	float4 color : COLOR;
};

fragment myvs( vertex IN )
{
    fragment OUT;

	OUT.hposition = mul( worldViewProj, float4(IN.position, 1) );
	OUT.texture0 = IN.texture0;

	OUT.cube = mul( world, float4(-IN.position, 1) );	

	return OUT;
}

pixel myps( fragment IN )
{
    pixel OUT;

	OUT.color = tex2D( Sampler, IN.texture0 );	
	float4 color = texCUBE( Sampler2, IN.cube ); 	
	OUT.color = lerp( OUT.color, color, 0.2);

    return OUT;
}

technique Technique0
{
    pass Pass0
    {
		Lighting = FALSE;

		Sampler[0] = (Sampler); // Needed by pixel shader

		VertexShader = compile vs_1_1 myvs();
		PixelShader  = compile ps_1_1 myps();
    }
}

