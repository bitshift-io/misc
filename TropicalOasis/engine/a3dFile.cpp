#include "stdafx.h"

/*
A3DFile::A3DFile()
{
}

A3DFile::A3DFile(std::string path)
{
	LoadModel(path);
}
*/
/*
A3DFile::A3DFile(std::string path, Mesh* mesh)
{
	LoadModel(path);
	LoadIntoMesh(mesh);
}*/

A3DFile::A3DFile( Direct3D* pD3D, FileManager* pFM, std::string path, MESH* mesh)
{
	this->pFM = pFM;
	this->pD3D = pD3D;

	LoadModel(path);
	LoadIntoMesh(mesh);
}

/*
A3DFile::A3DFile(std::string path, SkeletalMesh* mesh)
{
	LoadModel(path);
	LoadIntoMesh(mesh);
}

void A3DFile::LoadIntoMesh( SkeletalMesh* mesh )
{
//cos*	Engine::GetInstance().GetConsole().Log("loading skeletal mesh :)");
	mesh->CreateFromStruct( dataSkeletal[0] );

	std::vector<A3DBone>::iterator boneIt;// bones;
	for( boneIt = bones.begin(); boneIt != bones.end(); boneIt++ )
	{
		Bone b(0);
		b.id = boneIt->id;
		b.parentId = boneIt->parentId;
		mesh->AddBone(b);
	}
}*/

void A3DFile::LoadIntoMesh(MESH* mesh)
{
	//need to determine some FVF and vertex type
	// from the file :-/

	if( HasBoneData() )
	{
		mesh->fvf = VERT_BONE_FVF; //VERT_BONE::FVF;
		mesh->vertexSize = sizeof( VERT_BONE );

		mesh->numOfIndices = dataSkeletal[0].indexList.size();
		mesh->numOfVertices = dataSkeletal[0].vertexList.size();
		mesh->numOfPolygons = dataSkeletal[0].indexList.size() / 3;

		//FILL INDEX BUFFER
		if( FAILED( pD3D->GetDevice()->CreateIndexBuffer( 
			sizeof(WORD)* dataSkeletal[0].indexList.size(), D3DUSAGE_WRITEONLY,
			D3DFMT_INDEX16, D3DPOOL_DEFAULT, &mesh->pIndexBuffer, 0) )) //D3DPOOL_DEFAULT
		{
			return;
		}

		WORD* index;
		if( FAILED( mesh->pIndexBuffer->Lock(0, 0, (void**)&index, 0) ))
		{
			return;
		}

		for( int n = 0; n < dataSkeletal[0].indexList.size(); n++ )
		{
			index[n] = dataSkeletal[0].indexList[n];
		}

		mesh->pIndexBuffer->Unlock();

		//FILL VERTEX BUFFER
		if( FAILED( pD3D->GetDevice()->CreateVertexBuffer( 
			mesh->vertexSize * dataSkeletal[0].vertexList.size(), D3DUSAGE_WRITEONLY,
			mesh->fvf , D3DPOOL_DEFAULT, &mesh->pVertexBuffer, 0) ) ) //D3DPOOL_DEFAULT
		{	
			return;
		}

		VERT_BONE* vertex;	
		if( FAILED( mesh->pVertexBuffer->Lock(0, 0, (void**)&vertex, 0) ))
		{
			return;
		}

		for( int v = 0; v < dataSkeletal[0].vertexList.size(); v++ )
		{
			D3DXVECTOR4 zero = D3DXVECTOR4(1,0,1,0);
			vertex[v].boneId = dataSkeletal[0].vertexList[v].boneId;
			vertex[v].weight = dataSkeletal[0].vertexList[v].weight;

			vertex[v].boneId = zero;
			vertex[v].weight = zero;

			//vertex[v].numBones.x = dataSkeletal[0].vertexList[v].numBones;

			vertex[v].x = dataSkeletal[0].vertexList[v].p.x;
			vertex[v].y = dataSkeletal[0].vertexList[v].p.y;
			vertex[v].z = dataSkeletal[0].vertexList[v].p.z;

			vertex[v].tu = dataSkeletal[0].vertexList[v].tu;
			vertex[v].tv = dataSkeletal[0].vertexList[v].tv;

			vertex[v].nx = dataSkeletal[0].vertexList[v].n.x;
			vertex[v].ny = dataSkeletal[0].vertexList[v].n.y;
			vertex[v].nz = dataSkeletal[0].vertexList[v].n.z;

			Log("pre processed weight: %f %f %f %f", dataSkeletal[0].vertexList[v].weight.x, dataSkeletal[0].vertexList[v].weight.y, dataSkeletal[0].vertexList[v].weight.z, dataSkeletal[0].vertexList[v].weight.w);
			Log("ore processed boneId: %f %f %f %f", dataSkeletal[0].vertexList[v].boneId.x, dataSkeletal[0].vertexList[v].boneId.y, dataSkeletal[0].vertexList[v].boneId.z, dataSkeletal[0].vertexList[v].boneId.w);
			Log("processed weight: %f %f %f %f", vertex[v].weight.x, vertex[v].weight.y, vertex[v].weight.z, vertex[v].weight.w);
			Log("processed boneId: %f %f %f %f", vertex[v].boneId.x, vertex[v].boneId.y, vertex[v].boneId.z, vertex[v].boneId.w);
		}

		mesh->pVertexBuffer->Unlock();
	}
	else
	{
		mesh->fvf = VERT_POS_NORM_TEX::FVF;
		mesh->vertexSize = sizeof( VERT_POS_NORM_TEX );
		mesh->numOfIndices = data[0].indexList.size();
		mesh->numOfVertices = data[0].vertexList.size();
		mesh->numOfPolygons = data[0].indexList.size() / 3;

		//FILL INDEX BUFFER
		if( FAILED( pD3D->GetDevice()->CreateIndexBuffer( 
			sizeof(WORD)* data[0].indexList.size(), D3DUSAGE_WRITEONLY,
			D3DFMT_INDEX16, D3DPOOL_DEFAULT, &mesh->pIndexBuffer, 0) )) //D3DPOOL_DEFAULT
		{
			return;
		}

		WORD* index;
		if( FAILED( mesh->pIndexBuffer->Lock(0, 0, (void**)&index, 0) ))
		{
			return;
		}

		for( int n = 0; n < data[0].indexList.size(); n++ )
		{
			index[n] = data[0].indexList[n];
		}

		mesh->pIndexBuffer->Unlock();

		//FILL VERTEX BUFFER
		if( FAILED( pD3D->GetDevice()->CreateVertexBuffer( 
			mesh->vertexSize * data[0].vertexList.size(), D3DUSAGE_WRITEONLY,
			mesh->fvf , D3DPOOL_DEFAULT, &mesh->pVertexBuffer, 0) ) ) //D3DPOOL_DEFAULT
		{	
			return;
		}

		VERT_POS_NORM_TEX* vertex;	
		if( FAILED( mesh->pVertexBuffer->Lock(0, 0, (void**)&vertex, 0) ))
		{
			return;
		}

		for( int v = 0; v < data[0].vertexList.size(); v++ )
		{
			vertex[v].x = data[0].vertexList[v].p.x;
			vertex[v].y = data[0].vertexList[v].p.y;
			vertex[v].z = data[0].vertexList[v].p.z;

			vertex[v].tu = data[0].vertexList[v].tu;
			vertex[v].tv = data[0].vertexList[v].tv;

			vertex[v].nx = data[0].vertexList[v].n.x;
			vertex[v].ny = data[0].vertexList[v].n.y;
			vertex[v].nz = data[0].vertexList[v].n.z;
		}

		mesh->pVertexBuffer->Unlock();
	}
	mesh->sphereBoundRadius = data[0].sphereBoundRadius;
	mesh->pTexture = data[0].texture;
	mesh->pMaterial = data[0].material;
	mesh->bDoubleSided = data[0].bDoubleSided;
}
/*
void A3DFile::LoadIntoMesh(Mesh* mesh)
{
	mesh->CreateFromStruct( data[0] );
	/*
	//make sure something has been loaded
	if( data.size() <= 0 )
		return;

	//now create texture batches and fill up the mesh
	std::vector<TextureBatch>& meshData = mesh->GetMeshData();

	meshData.empty();
	meshData.resize( data.size() );

	for( int i=0; i < data.size(); i++)
	{
		meshData[i].CreateFromStruct( data[i] );
	}* /
}*/

bool A3DFile::HasNormals()
{
	return meshes[0].normal.size();
}

bool A3DFile::HasBoneData()
{
	return bones.size();
}

/*
bool A3DFile::ConatinsBoneData()
{
	return bones.size();
}*/

void A3DFile::LoadModel( std::string path )
{
	std::string actualPath = path;
	pFile = fopen(actualPath.c_str(), "r");

//cos*	Engine::GetInstance().GetConsole().Log("loading mesh: %s", actualPath.c_str() );

	//start reading till we find a *something
	while( !feof(pFile) )
	{
		char strWord[255] = {0};
		// Get each word in the file
		fscanf(pFile, "%s", &strWord);

		// Check if we hit a new mesh
		if( strcmp(strWord, A3D_MESH) == 0 )
		{
			ReadMesh();
		}
		if( strcmp(strWord, A3D_MATERIAL) == 0 )
		{
			ReadMaterial();
		}
		if( strcmp(strWord, A3D_BONE) == 0 )
		{
			ReadBone();
		}
		else
		{
			// Go to the next line
			fgets(strWord, 100, pFile);
		}
	}

	fclose(pFile);

	ProcessData();
}

void A3DFile::ProcessData()
{
//cos*	Engine::GetInstance().GetConsole().Log("---------processing data-------------------");
	/*
std::vector<A3DMaterial> materials; //raw file data
	std::vector<A3DBone> bones;
	std::vector<A3DMesh> meshes;

	std::vector<STL_TEXTUREBATCH> data; //compiled data*/

	/*
	struct STL_TEXTUREBATCH
{
	std::vector<STANDARD_VERTEX> vertexList;
	std::vector<WORD> indexList;

	LPDIRECT3DTEXTURE9 texture;
	D3DMATERIAL9 material;

	int numVertices;
	int numFaces;

	DWORD fvf;
	UINT vertexSize;

	STL_TEXTUREBATCH()
	{
		fvf = STANDARD_FVF;
		vertexSize = sizeof(STANDARD_VERTEX);
	}
};
	 */

	/*
struct A3DMesh
{
	std::vector<A3DSkin> skin;
	std::vector<A3DFloat3> vertex;
	std::vector<A3DFloat3> normal;
	std::vector<A3DInt3> face;
	std::vector<A3DFloat3> textureVert;
	std::vector<A3DInt3> textureFace;
	int materialId;
};
  */
	

	data.resize( meshes.size() );
	dataSkeletal.resize( meshes.size() );

//cos*	Engine::GetInstance().GetConsole().Log("mesh size: %i", data.size() );

	//loop through all meshes
	std::vector<A3DMesh>::iterator meshIt;

	int m;
	for( meshIt = meshes.begin(), m = 0; meshIt != meshes.end(); meshIt++, m++ )
	{
		//material stuffs
		{
			data[m].bDoubleSided = materials[ meshIt->materialId ].bDoubleSided;
			data[m].material.Diffuse.r = materials[ meshIt->materialId ].diffuse.a;
			data[m].material.Diffuse.g = materials[ meshIt->materialId ].diffuse.b;
			data[m].material.Diffuse.b = materials[ meshIt->materialId ].diffuse.c;

			data[m].material.Ambient.r = materials[ meshIt->materialId ].ambient.a;
			data[m].material.Ambient.g = materials[ meshIt->materialId ].ambient.b;
			data[m].material.Ambient.b = materials[ meshIt->materialId ].ambient.c;

			data[m].material.Specular.r = materials[ meshIt->materialId ].specular.a;
			data[m].material.Specular.g = materials[ meshIt->materialId ].specular.b;
			data[m].material.Specular.b = materials[ meshIt->materialId ].specular.c;

			data[m].material.Power = materials[ meshIt->materialId ].shine;	
			
			data[m].sphereBoundRadius = meshIt->sphereBoundRadius;
		}
		std::string texture = std::string( materials[ meshIt->materialId ].texture );

		//skeletal data
		if( HasBoneData() )
		{
			dataSkeletal[m].sphereBoundRadius = meshIt->sphereBoundRadius;

			dataSkeletal[m].fvf = SKELETALMESH_FVF;
			dataSkeletal[m].vertexSize = sizeof(BONE_VERTEX);

			dataSkeletal[m].material.Diffuse.r = materials[ meshIt->materialId ].diffuse.a;
			dataSkeletal[m].material.Diffuse.g = materials[ meshIt->materialId ].diffuse.b;
			dataSkeletal[m].material.Diffuse.b = materials[ meshIt->materialId ].diffuse.c;

			dataSkeletal[m].material.Ambient.r = materials[ meshIt->materialId ].ambient.a;
			dataSkeletal[m].material.Ambient.g = materials[ meshIt->materialId ].ambient.b;
			dataSkeletal[m].material.Ambient.b = materials[ meshIt->materialId ].ambient.c;

			dataSkeletal[m].material.Specular.r = materials[ meshIt->materialId ].specular.a;
			dataSkeletal[m].material.Specular.g = materials[ meshIt->materialId ].specular.b;
			dataSkeletal[m].material.Specular.b = materials[ meshIt->materialId ].specular.c;

			dataSkeletal[m].material.Power = materials[ meshIt->materialId ].shine;
		}


		if( texture.size() == 0 )
		{
//cos*			Engine::GetInstance().GetConsole().Log("WARNING: No Texture set on model");
			data[m].texture = 0;
			dataSkeletal[m].texture = 0;
		}
		else
		{
			data[m].texture = pFM->GetTexture( std::string( materials[ meshIt->materialId ].texture ) );
			dataSkeletal[m].texture = pFM->GetTexture( std::string( materials[ meshIt->materialId ].texture ) );
		}

		//loop through all faces
		for( int i=0; i < meshIt->face.size(); i++ )
		{
			//mesh stuffs
			if( !HasBoneData() )
			{
				STANDARD_VERTEX vert;
				WORD idx = meshIt->face[i].a;
				WORD texIdx = meshIt->textureFace[i].a;

				vert.p.x = meshIt->vertex[ idx ].a;
				vert.p.y = meshIt->vertex[ idx ].b;
				vert.p.z = meshIt->vertex[ idx ].c;

				vert.n.x = meshIt->normal[ idx ].a;
				vert.n.y = meshIt->normal[ idx ].b;
				vert.n.z = meshIt->normal[ idx ].c;

				vert.tu = meshIt->textureVert[ texIdx ].a;
				vert.tv = meshIt->textureVert[ texIdx ].b;

				data[m].vertexList.push_back( vert );
				data[m].indexList.push_back( data[m].vertexList.size() - 1 );


				idx = meshIt->face[i].b;
				texIdx = meshIt->textureFace[i].b;

				vert.p.x = meshIt->vertex[ idx ].a;
				vert.p.y = meshIt->vertex[ idx ].b;
				vert.p.z = meshIt->vertex[ idx ].c;

				vert.n.x = meshIt->normal[ idx ].a;
				vert.n.y = meshIt->normal[ idx ].b;
				vert.n.z = meshIt->normal[ idx ].c;

				vert.tu = meshIt->textureVert[ texIdx ].a;
				vert.tv = meshIt->textureVert[ texIdx ].b;

				data[m].vertexList.push_back( vert );
				data[m].indexList.push_back( data[m].vertexList.size() - 1 );


				idx = meshIt->face[i].c;
				texIdx = meshIt->textureFace[i].c;

				vert.p.x = meshIt->vertex[ idx ].a;
				vert.p.y = meshIt->vertex[ idx ].b;
				vert.p.z = meshIt->vertex[ idx ].c;

				vert.n.x = meshIt->normal[ idx ].a;
				vert.n.y = meshIt->normal[ idx ].b;
				vert.n.z = meshIt->normal[ idx ].c;

				vert.tu = meshIt->textureVert[ texIdx ].a;
				vert.tv = meshIt->textureVert[ texIdx ].b;
				
				data[m].vertexList.push_back( vert );
				data[m].indexList.push_back( data[m].vertexList.size() - 1 );
			}

			//skeletal mesh stuffs
			if( HasBoneData() )
			{
				std::vector<A3DSkin>::iterator skinIt;

				BONE_VERTEX vert;
				WORD idx = meshIt->face[i].a;
				WORD texIdx = meshIt->textureFace[i].a;

				vert.p.x = meshIt->vertex[ idx ].a;
				vert.p.y = meshIt->vertex[ idx ].b;
				vert.p.z = meshIt->vertex[ idx ].c;

				vert.n.x = meshIt->normal[ idx ].a;
				vert.n.y = meshIt->normal[ idx ].b;
				vert.n.z = meshIt->normal[ idx ].c;

				vert.tu = meshIt->textureVert[ texIdx ].a;
				vert.tv = meshIt->textureVert[ texIdx ].b;
			
				//loop through skinweights looking for our vertex
				for( skinIt = meshIt->skin.begin(); skinIt != meshIt->skin.end(); skinIt++ )
				{
					if( skinIt->vertIdx == idx )
					{
						vert.weight = D3DXVECTOR4( skinIt->weight[0], skinIt->weight[1], skinIt->weight[2], skinIt->weight[3]);
						vert.boneId = D3DXVECTOR4( skinIt->boneId[0], skinIt->boneId[1], skinIt->boneId[2], skinIt->boneId[3]);
						vert.numBones = skinIt->num;
						break;
					}
				}

				Log("weight: %f %f %f %f", vert.weight.x, vert.weight.y, vert.weight.z, vert.weight.w);
				Log("boneId: %f %f %f %f", vert.boneId.x, vert.boneId.y, vert.boneId.z, vert.boneId.w);
				//Engine::GetInstance().GetConsole().Log("v.x  w:%f b:%f",vert.weight.x, vert.boneId.x );
				//Engine::GetInstance().GetConsole().Log("v.y  w:%f b:%f",vert.weight.y, vert.boneId.y );
				//Engine::GetInstance().GetConsole().Log("v.z  w:%f b:%f",vert.weight.z, vert.boneId.z );
				//Engine::GetInstance().GetConsole().Log("v.w  w:%f b:%f",vert.weight.w, vert.boneId.w );
//cos*				Engine::GetInstance().GetConsole().Log("v.x  w:%.2f b:%.2f | v.y  w:%.2f b:%.2f | v.z  w:%.2f b:%.2f | v.w  w:%.2f b:%.2f",vert.weight.x, vert.boneId.x,vert.weight.y, vert.boneId.y 
//cos*					,vert.weight.z, vert.boneId.z ,vert.weight.w, vert.boneId.w);

				dataSkeletal[m].vertexList.push_back( vert );
				dataSkeletal[m].indexList.push_back( dataSkeletal[m].vertexList.size() - 1 );


				idx = meshIt->face[i].b;
				texIdx = meshIt->textureFace[i].b;

				vert.p.x = meshIt->vertex[ idx ].a;
				vert.p.y = meshIt->vertex[ idx ].b;
				vert.p.z = meshIt->vertex[ idx ].c;

				vert.n.x = meshIt->normal[ idx ].a;
				vert.n.y = meshIt->normal[ idx ].b;
				vert.n.z = meshIt->normal[ idx ].c;

				vert.tu = meshIt->textureVert[ texIdx ].a;
				vert.tv = meshIt->textureVert[ texIdx ].b;

				//loop through skinweights looking for our vertex
				for( skinIt = meshIt->skin.begin(); skinIt != meshIt->skin.end(); skinIt++ )
				{
					if( skinIt->vertIdx == idx )
					{
						vert.weight = D3DXVECTOR4( skinIt->weight[0], skinIt->weight[1], skinIt->weight[2], skinIt->weight[3]);
						vert.boneId = D3DXVECTOR4( skinIt->boneId[0], skinIt->boneId[1], skinIt->boneId[2], skinIt->boneId[3]);
						break;
					}
				}
				//Engine::GetInstance().GetConsole().Log("v.x  w:%f b:%f",vert.weight.x, vert.boneId.x );
				//Engine::GetInstance().GetConsole().Log("v.y  w:%f b:%f",vert.weight.y, vert.boneId.y );
				//Engine::GetInstance().GetConsole().Log("v.z  w:%f b:%f",vert.weight.z, vert.boneId.z );
				//Engine::GetInstance().GetConsole().Log("v.w  w:%f b:%f",vert.weight.w, vert.boneId.w );
//cos*				Engine::GetInstance().GetConsole().Log("v.x  w:%.2f b:%.2f | v.y  w:%.2f b:%.2f | v.z  w:%.2f b:%.2f | v.w  w:%.2f b:%.2f",vert.weight.x, vert.boneId.x,vert.weight.y, vert.boneId.y 
//cos*					,vert.weight.z, vert.boneId.z ,vert.weight.w, vert.boneId.w);

				dataSkeletal[m].vertexList.push_back( vert );
				dataSkeletal[m].indexList.push_back( dataSkeletal[m].vertexList.size() - 1 );


				idx = meshIt->face[i].c;
				texIdx = meshIt->textureFace[i].c;

				vert.p.x = meshIt->vertex[ idx ].a;
				vert.p.y = meshIt->vertex[ idx ].b;
				vert.p.z = meshIt->vertex[ idx ].c;

				vert.n.x = meshIt->normal[ idx ].a;
				vert.n.y = meshIt->normal[ idx ].b;
				vert.n.z = meshIt->normal[ idx ].c;

				vert.tu = meshIt->textureVert[ texIdx ].a;
				vert.tv = meshIt->textureVert[ texIdx ].b;

				//loop through skinweights looking for our vertex
				for( skinIt = meshIt->skin.begin(); skinIt != meshIt->skin.end(); skinIt++ )
				{
					if( skinIt->vertIdx == idx )
					{
						vert.weight = D3DXVECTOR4( skinIt->weight[0], skinIt->weight[1], skinIt->weight[2], skinIt->weight[3]);
						vert.boneId = D3DXVECTOR4( skinIt->boneId[0], skinIt->boneId[1], skinIt->boneId[2], skinIt->boneId[3]);
						break;
					}
				}
//cos*				Engine::GetInstance().GetConsole().Log("v.x  w:%.2f b:%.2f | v.y  w:%.2f b:%.2f | v.z  w:%.2f b:%.2f | v.w  w:%.2f b:%.2f",vert.weight.x, vert.boneId.x,vert.weight.y, vert.boneId.y 
//cos*					,vert.weight.z, vert.boneId.z ,vert.weight.w, vert.boneId.w);
			
				//Engine::GetInstance().GetConsole().Log("p.x: %f, weight: %f, boneId: %f", vert.p.x, vert.weight.x, vert.boneId.x);

				dataSkeletal[m].vertexList.push_back( vert );
				dataSkeletal[m].indexList.push_back( dataSkeletal[m].vertexList.size() - 1 );
			}
			/*
			struct A3DSkin
{
	int vertIdx;
	int boneId;
	float weight;
};

			*/
		}

		/*
		for( i = 0; i < data[m].indexList.size(); i++)
		{
			Engine::GetInstance().GetConsole().Log( "%i) %f %f %f | uv: %f %f", i, data[m].vertexList[i].p.x, data[m].vertexList[i].p.y, data[m].vertexList[i].p.z, data[m].vertexList[i].tu, data[m].vertexList[i].tv );
		}*/
	}

//cos*	Engine::GetInstance().GetConsole().Log("-------------------------------------");
}

void A3DFile::ReadBone()
{
	char strWord[255] = {0};
	int num = 0;

	A3DBone bone;

	fscanf(pFile, "%i", &bone.id );

	while( strcmp(strWord, A3D_END) != 0 && !feof(pFile) )
	{
		// Get each word in the file
		fscanf(pFile, "%s", &strWord);

		if( strcmp(strWord, A3D_PARENT) == 0 )
		{
			fscanf(pFile, "%i", &bone.parentId );
		}
	}

	bones.push_back( bone );

//cos*	Engine::GetInstance().GetConsole().Log("bone size: %i", bones.size() );
}

void A3DFile::ReadMesh()
{
	char strWord[255] = {0};
	int num = 0;

	A3DMesh mesh;

	while( strcmp(strWord, A3D_END) != 0 && !feof(pFile) )
	{
		// Get each word in the file
		fscanf(pFile, "%s", &strWord);

		
		if( strcmp(strWord, A3D_END) != 0 && strcmp(strWord, A3D_SPHEREBOUND ) != 0 )
			fscanf(pFile, "%i", &num);

		if( strcmp(strWord, A3D_SKIN) == 0 )
		{
			ReadSkins( mesh, num );

			/*
			for( int i=0; i < mesh.skin.size(); i++)
			{
				Engine::GetInstance().GetConsole().Log("%i) bid: %f, w: %f", i, mesh.skin[i].boneId, mesh.skin[i].weight );
			}*/
		}
		if( strcmp(strWord, A3D_NORMAL) == 0 )
		{
			ReadNormals( mesh, num );
		}
		if( strcmp(strWord, A3D_VERTEX) == 0 )
		{
			ReadVertices( mesh, num );
		}
		if( strcmp(strWord, A3D_FACE) == 0 )
		{
			ReadFaces( mesh, num );
		}
		if( strcmp(strWord, A3D_MATERIALID) == 0 )
		{
			mesh.materialId = num;
		}
		if( strcmp(strWord, A3D_SPHEREBOUND ) == 0 )
		{	
			fscanf(pFile, "%f", &mesh.sphereBoundRadius);
		}
		if( strcmp(strWord, A3D_TEXTUREFACE) == 0 )
		{
			ReadTextureFaces( mesh, num );		
		}
		if( strcmp(strWord, A3D_TEXTUREVERTEX) == 0 )
		{
			ReadTextureVertices( mesh, num );
		}
	}
	meshes.push_back( mesh );
}

void A3DFile::ReadVertices( A3DMesh& mesh, int num )
{
	char strWord[255] = {0};

	for(int i=0; i < num; i++)
	{
		A3DFloat3 vertex;

		fgets(strWord, 100, pFile);
		fscanf(pFile, "%f %f %f", &vertex.a, &vertex.b, &vertex.c );
		
		mesh.vertex.push_back( vertex );
	}
}

void A3DFile::ReadNormals( A3DMesh& mesh, int num )
{
	char strWord[255] = {0};

	for(int i=0; i < num; i++)
	{
		A3DFloat3 normal;

		fgets(strWord, 100, pFile);
		fscanf(pFile, "%f %f %f", &normal.a, &normal.b, &normal.c );
		
		mesh.normal.push_back( normal );
	}
}

void A3DFile::ReadSkins( A3DMesh& mesh, int num )
{
	char strWord[255] = {0};

	for(int i=0; i < num; i++)
	{
		int vertIdx = 0;
		float weight = 0;

		fgets(strWord, 100, pFile);
		int boneId = 0;

		fscanf(pFile, "%i %i %f", &vertIdx, &boneId, &weight );

		//find our skin
		std::vector<A3DSkin>::iterator skinIt;
		bool bFound = false;
		for( skinIt = mesh.skin.begin(); skinIt != mesh.skin.end(); skinIt++ )
		{
			if( skinIt->vertIdx == vertIdx && skinIt->num < 4 )
			{
				bFound = true;
				
				skinIt->boneId[ skinIt->num ] = (float)boneId;
				skinIt->weight[ skinIt->num ] = weight;

				skinIt->num++;
			}
		}

		if( !bFound )
		{
			A3DSkin skin;
			skin.vertIdx = vertIdx;
			skin.boneId[0] = boneId;
			skin.weight[0] = weight;
			skin.num++;
			mesh.skin.push_back( skin );
		}		
	}
}

void A3DFile::ReadFaces( A3DMesh& mesh, int num )
{
	char strWord[255] = {0};

	for(int i=0; i < num; i++)
	{
		A3DInt3 face;

		fgets(strWord, 100, pFile);
		fscanf(pFile, "%i %i %i", &face.a, &face.b, &face.c );
		
		mesh.face.push_back( face );
	}
}

void A3DFile::ReadTextureFaces( A3DMesh& mesh, int num )
{
	char strWord[255] = {0};

	for(int i=0; i < num; i++)
	{
		A3DInt3 texFace;

		fgets(strWord, 100, pFile);
		fscanf(pFile, "%i %i %i", &texFace.a, &texFace.b, &texFace.c );
		
		mesh.textureFace.push_back( texFace );
	}
}

void A3DFile::ReadTextureVertices( A3DMesh& mesh, int num )
{
	char strWord[255] = {0};

	for(int i=0; i < num; i++)
	{
		A3DFloat3 texVert;

		fgets(strWord, 100, pFile);
		fscanf(pFile, "%f %f %f", &texVert.a, &texVert.b, &texVert.c );
		
		mesh.textureVert.push_back( texVert );
	}
}

void A3DFile::ReadMaterial()
{
	char strWord[255] = {0};

	A3DMaterial mat;

	mat.bDoubleSided = false;
	//mat.texture = 0;

	fscanf(pFile, "%i", &mat.id );

	while( strcmp(strWord, A3D_END) != 0 && !feof(pFile) )
	{		
		// Get each word in the file
		fscanf(pFile, "%s", &strWord);

		if( strcmp(strWord, A3D_AMBIENT) == 0 )
		{
			fscanf(pFile, "%f %f %f", &mat.ambient.a, &mat.ambient.b, &mat.ambient.c);
		}
		if( strcmp(strWord, A3D_DIFFUSE) == 0 )
		{
			fscanf(pFile, "%f %f %f", &mat.diffuse.a, &mat.diffuse.b, &mat.diffuse.c);
		}
		if( strcmp(strWord, A3D_SHINE) == 0 )
		{
			fscanf(pFile, "%f", &mat.shine);
		}
		if( strcmp(strWord, A3D_SPECULAR) == 0 )
		{
			fscanf(pFile, "%f %f %f", &mat.specular.a, &mat.specular.b, &mat.specular.c);
		}
		if( strcmp(strWord, A3D_TEXTURE) == 0 )
		{
			fscanf(pFile, "%s", &mat.texture );
		}
		if( strcmp(strWord, A3D_DOUBLESIDED) == 0 )
		{
			//double sided texture
			mat.bDoubleSided = true;
		}
	}

	materials.push_back( mat );
}
