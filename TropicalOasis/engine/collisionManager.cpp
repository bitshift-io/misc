/* ====================================
	COLLISIONMANAGER
Functions are called are called from here
that do the function collision math, and return stuff
==================================== */

#include "stdafx.h"
#include "collisionManager.h"

CollisionManager::CollisionManager()
{

}

CollisionManager::~CollisionManager()
{

}

void CollisionManager::Update( float deltaTime, World* pW )
{
	for( int i = 0; i < pW->GetNumberCollisionCells(); i++)
	{
		if( pW->GetCollisionCell( i )->contains.size() > 0 )
		{
			ComputeCollisions( pW->GetCollisionCell( i ) );
		}
	}
}


//go through each actor and check it against every other actor
void CollisionManager::ComputeCollisions(CollisionCell* cell)
{
	std::list<Object*>::iterator objectIt1;
	std::list<Object*>::iterator objectIt2;
	
	for( objectIt1 = cell->contains.begin(); objectIt1 != cell->contains.end(); objectIt1++)
	{
		
		if( (*objectIt1)->bCollideWorld )
		{
			SphereToPolys( &cell->terrain, (*objectIt1) );
		}

		if( (*objectIt1)->bCollideObjects )
		{
			for( objectIt2 = objectIt1; objectIt2 != cell->contains.end(); objectIt2++)
			{
				if( objectIt2 != objectIt1 ) //have to do this as a little hack
					if( (*objectIt2)->bCollideObjects )
						SphereToSphere( (*objectIt1), (*objectIt2) );
			}
		}
	}
}
/*
float CollisionManager::LengthValue(D3DXVECTOR3 p1, D3DXVECTOR3 p2)
{
	return (p1.x + p2.x) + (p1.y + p2.y) + (p1.z + p2.z);
}*/

void CollisionManager::SphereToPolys( COLLISIONMESH* pObj1, Object* pObj2 )
{
	//loop through all batches
	std::vector<D3DXVECTOR3>::iterator vertIt; // vertices;

	
	//loop through vertices
	D3DXVECTOR3 p[3];

	//Console::GetInstance().Log("size of vertices: %i", pObj1->vertices.size());
/*
	for( int i = 0; i < pObj1->vertices.size(); i++ )
	{
		//dump to file to see if its all good
		Console::GetInstance().Log("%i) %f %f %f", i,pObj1->vertices[i].x, pObj1->vertices[i].y,
			pObj1->vertices[i].z);
	}*/


	//for( vertIt = pObj1->vertices.begin(); vertIt != pObj1->vertices.end(); vertIt += 3)
	for( int i = 0; i < pObj1->vertices.size(); i += 3 )
	{
		p[0] = pObj1->vertices[i + 0];
		p[1] = pObj1->vertices[i + 1];
		p[2] = pObj1->vertices[i + 2];

		//calculate plane triangle lies on
		D3DXVECTOR3 v1 = p[0] - p[1];
		D3DXVECTOR3 v2 = p[0] - p[2];

		D3DVECTOR planePoint = p[0];
		D3DXVECTOR3 planeNormal;				
		D3DXVec3Cross( &planeNormal, &v1, &v2);

		D3DXVec3Normalize( &planeNormal, &planeNormal);

		// shoot ray along the velocity vector
		//float distToPlaneIntersection = RayIntersectPlane( pObj2->velocity, sphereIntersectPoint, planeNormal, planePoint);

		//plugin -ve plane normal
		float distToPlaneIntersection = RayIntersectPlane( -planeNormal, pObj2->position, planeNormal, planePoint);

		if( distToPlaneIntersection < pObj2->boundSphereRadius )
		{
			//calculate sphere-plane intersection point
			D3DXVECTOR3 sphereIntersectPoint = pObj2->position - (planeNormal * distToPlaneIntersection);

			//pObj2->SetPosition( pObj2->oldPosition );

			//now to see if the point of intersection is inside the triangle
			if( CheckPointInTriangle(sphereIntersectPoint, p[0], p[1], p[2]) )
			{
				//move the object out of poly
				float distToMoveObj = pObj2->boundSphereRadius - distToPlaneIntersection;

				D3DXVECTOR3 newPos = pObj2->position + (distToMoveObj * planeNormal);
				pObj2->SetPosition( newPos );

				//set velocity
				//pObj2->velocity = D3DXVECTOR3(0,0,0);

				pObj2->Collision(0, planeNormal);
			}
	
			/*
			//see if any of the points are inside the sphere
			D3DXVECTOR3 dist[3];
			dist[0] = pObj2->position - p[0];
			dist[1] = pObj2->position - p[1];
			dist[2] = pObj2->position - p[2];
			if( D3DXVec3Length( &dist[0] ) <  pObj2->boundSphereRadius ||
				D3DXVec3Length( &dist[1] ) >  pObj2->boundSphereRadius ||
				D3DXVec3Length( &dist[2] ) >  pObj2->boundSphereRadius 
				)
			{
				//pObj2->SetPosition( pObj2->oldPosition );

				if( pObj2->IsA(CLASS_CAMERA) )
					Console::GetInstance().Log("vertex inside sphere (radius:%f)(cam pos:%f %f %f)(verts:%f %f %f)", 
								pObj2->boundSphereRadius, pObj2->position.x, pObj2->position.y, pObj2->position.z,
								p[1].x, p[1].y, p[1].z);
			}*/
		}
		
		/*
		//float distToPlaneIntersection = RayIntersectPlane( -planeNormal, sphereIntersectPoint, planeNormal, planePoint);

		D3DXVECTOR3 normalizedVelocity;
		D3DXVec3Normalize( &normalizedVelocity, &pObj2->velocity );

		D3DXVECTOR3 planeIntersectionPoint;

		planeIntersectionPoint.x = sphereIntersectPoint.x + distToPlaneIntersection * normalizedVelocity.x;
		planeIntersectionPoint.y = sphereIntersectPoint.y + distToPlaneIntersection * normalizedVelocity.y;
		planeIntersectionPoint.z = sphereIntersectPoint.z + distToPlaneIntersection * normalizedVelocity.z;

		//see if plane intersect sphere (if so, then we need to do some extra stuff to get the point
		// of intersection)
		if( ClassifyPoint( planeIntersectionPoint, planeNormal, planePoint ) == PLANE_BACKSIDE )
		{
			// find plane intersection point by shooting a ray from the 
			// sphere intersection point along the planes normal.
			distToPlaneIntersection = RayIntersectPlane( planeNormal, sphereIntersectPoint, planeNormal, planePoint);

			// calculate plane intersection point
			planeIntersectionPoint.x = sphereIntersectPoint.x + distToPlaneIntersection * planeNormal.x;
			planeIntersectionPoint.y = sphereIntersectPoint.y + distToPlaneIntersection * planeNormal.y;
			planeIntersectionPoint.z = sphereIntersectPoint.z + distToPlaneIntersection * planeNormal.z;
		}

		//calculate polygon intersection, if any (section 4 of tutorial)
		//just because we intersect the plane doesnt mean we intersect the poly
		if( CheckPointInTriangle(planeIntersectionPoint, p[0], p[1], p[2]) )
		{
			//if( pObj2->IsA( CLASS_BALL ) )
			//{
				//Console::GetInstance().Log("collision between poly and sphere");
			//}

			D3DXVECTOR3 zero(0,0,0);
			//pObj1->Collision(pObj2, zero); //TODO: DONT FORGET TO ADD NORMAL
			//pObj2->Collision(pObj1, zero);
		}*/
	}


	//std::vector<TextureBatch> meshData = pObj2->GetMeshData();
	//std::vector<TextureBatch>::iterator meshIt;
	/*
	for( meshIt = meshData.begin(); meshIt != meshData.end(); meshIt++ )
	{
		//loop through all triangles
		for( int t = 0; (t+3) < meshIt->indexList.size() ; t += 3)
		{	
			D3DXVECTOR3 p1 = meshIt->vertexList[ meshIt->indexList[t+0] ];
			D3DXVECTOR3 p2 = meshIt->vertexList[ meshIt->indexList[t+1] ];
			D3DXVECTOR3 p3 = meshIt->vertexList[ meshIt->indexList[t+2] ];

			/*
			//SHOULD WE GENERATE A SPHERE AROUND THIS POLY,
			//AND SEE IF IT INTERSECTS THE SPHERE? - seems to take to much time
			//poly sphere
			D3DXVECTOR3 polyCenter = (p1 + p2 + p3) / 3;
			float polyRadius = LengthValue( p1 ,polyCenter );

			if( LengthValue( p2 ,polyCenter ) > polyRadius )
				polyRadius = LengthValue( p2 ,polyCenter );
			if( LengthValue( p3 ,polyCenter ) > polyRadius )
				polyRadius = LengthValue( p3 ,polyCenter );
				

			//check to see for sphere to sphere collision
			D3DXVECTOR3 vectorBetweenSpheres = polyCenter - pObj2->position;

			float distanceBetweenSpheres = 
				(float)(pow(vectorBetweenSpheres.x,2)+pow(vectorBetweenSpheres.y,2)+pow(vectorBetweenSpheres.z,2) );

			//float distanceBetweenSpheres = 
			//	(float)sqrt( pow(vectorBetweenSpheres.x,2)+pow(vectorBetweenSpheres.y,2)+pow(vectorBetweenSpheres.z,2) );

			float minDistance = polyRadius + pObj2->boundSphereRadius;
			minDistance *= minDistance;

			//if no collision occurs
			if( distanceBetweenSpheres > minDistance  )
				break;	
			* /

			//calculate plane triangle lies on
			D3DXVECTOR3 v1 = p1 - p2;
			D3DXVECTOR3 v2 = p1 - p3;

			D3DVECTOR planePoint = p1;
			D3DXVECTOR3 planeNormal;				
			D3DXVec3Cross( &planeNormal, &v1, &v2);

			D3DXVec3Normalize( &planeNormal, &planeNormal);

			//calculate sphere-plane intersection point
			D3DXVECTOR3 sphereIntersectPoint = pObj2->position - planeNormal;

			// shoot ray along the velocity vector
			float distToPlaneIntersection = RayIntersectPlane( pObj2->velocity, sphereIntersectPoint, planeNormal, planePoint);

			D3DXVECTOR3 normalizedVelocity;
			D3DXVec3Normalize( &normalizedVelocity, &pObj2->velocity );

			D3DXVECTOR3 planeIntersectionPoint;

			planeIntersectionPoint.x = sphereIntersectPoint.x + distToPlaneIntersection * normalizedVelocity.x;
			planeIntersectionPoint.y = sphereIntersectPoint.y + distToPlaneIntersection * normalizedVelocity.y;
			planeIntersectionPoint.z = sphereIntersectPoint.z + distToPlaneIntersection * normalizedVelocity.z;

			//see if plane intersect sphere (if so, then we need to do some extra stuff to get the point
			// of intersection)
			if( ClassifyPoint( planeIntersectionPoint, planeNormal, planePoint ) == PLANE_BACKSIDE )
			{
				// find plane intersection point by shooting a ray from the 
				// sphere intersection point along the planes normal.
				distToPlaneIntersection = RayIntersectPlane( planeNormal, sphereIntersectPoint, planeNormal, planePoint);

				// calculate plane intersection point
				planeIntersectionPoint.x = sphereIntersectPoint.x + distToPlaneIntersection * planeNormal.x;
				planeIntersectionPoint.y = sphereIntersectPoint.y + distToPlaneIntersection * planeNormal.y;
				planeIntersectionPoint.z = sphereIntersectPoint.z + distToPlaneIntersection * planeNormal.z;
			}

			//calculate polygon intersection, if any (section 4 of tutorial)
			//just because we intersect the plane doesnt mean we intersect the poly
			if( CheckPointInTriangle(planeIntersectionPoint, p1, p2, p3) )
			{
				if( pObj2->IsA( CLASS_BALL ) )
				{
					systemLog.WriteLog("well, we are testing with the terrain");
					systemLog.WriteLog("collision between poly and sphere");
				}

				D3DXVECTOR3 zero(0,0,0);
				pObj1->Collision(pObj2, zero); //TODO: DONT FORGET TO ADD NORMAL
				pObj2->Collision(pObj1, zero);
			}* /				
		}
	}*/
}

/*
 * this is from: http://www.dr-code.com/modules/MrGamemaker/Content/PointInPoly.html
 */
bool CollisionManager::CheckPointInTriangle(const D3DXVECTOR3& pointIntersect, 
				const D3DXVECTOR3& p1, const D3DXVECTOR3& p2, const D3DXVECTOR3& p3)
{  
  
     D3DXVECTOR3 e10=p2-p1;  
     D3DXVECTOR3 e20=p3-p1;  
     float a,b,c,ac_bb;  
  
     a = D3DXVec3Dot(&e10,&e10);  
     b = D3DXVec3Dot(&e20,&e10);  
     c = D3DXVec3Dot(&e20,&e20);  
     ac_bb=(a*c)-(b*b);  
  
     D3DXVECTOR3 vp(pointIntersect.x-p1.x, pointIntersect.y-p1.y, pointIntersect.z-p1.z);  
  
     float d = D3DXVec3Dot(&e10,&vp);  
     float e = D3DXVec3Dot(&e20,&vp);  
  
     float x = (d*c)-(e*b);  
     float y = (e*a)-(d*b);  
     float z = x+y-ac_bb;  
  
	 return (( ((unsigned int&)z)& ~( ((unsigned int&)x)| ((unsigned int&)y) ) ) & 0x80000000);

     //return (( (unsigned int&)z & ~( ((unsigned int&)x | ((unsigned int&)y ) ) & 0x80000000);  
} 

/* // this is my old version
bool CollisionManager::CheckPointInTriangle(D3DXVECTOR3 pointIntersect, D3DXVECTOR3 p1, D3DXVECTOR3 p2, D3DXVECTOR3 p3)
{
	/*
	 * we are given where the polys plane intersects the sphere, and the 3 points of the poly
	 * /
		double totalAngles = 0.0f;
		// make the 3 vectors
	D3DXVECTOR3 v1 = pointIntersect - p1;
	D3DXVECTOR3 v2 = pointIntersect - p2;
	D3DXVECTOR3 v3 = pointIntersect - p3;
 
	D3DXVec3Normalize( &v1, &v1 );
	D3DXVec3Normalize( &v2, &v2 );
	D3DXVec3Normalize( &v3, &v3 );

	totalAngles += acos( D3DXVec3Dot( &v1, &v2 ) );   
	totalAngles += acos( D3DXVec3Dot( &v2, &v3 ) );
	totalAngles += acos( D3DXVec3Dot( &v3, &v1 ) ); 
    
	// allow a small margin because of the limited precision of
	// floating point math.
	if ( fabs( totalAngles - 2*PI ) <= 0.005 )
		return true;

	return false;
}*/

void CollisionManager::SphereToSphere(Object* pObj1, Object* pObj2)
{
	if( pObj1->boundSphereRadius <= 0 || pObj2->boundSphereRadius <= 0 )
		return;

	//Console::GetInstance().Log("sphere1 r: %f, s2 r: %f", pObj1->boundSphereRadius, pObj2->boundSphereRadius);
	//Console::GetInstance().Log("sp1: %f %f %f, sp2: %f %f %f", 
	//	pObj1->position.x, pObj1->position.y, pObj1->position.z,
	//	pObj2->position.x, pObj2->position.y, pObj2->position.z);

	D3DXVECTOR3 vectorBetweenSpheres = pObj1->position - pObj2->position;

	float distanceBetweenSpheres = D3DXVec3Length( &vectorBetweenSpheres );
		//(float)(pow(vectorBetweenSpheres.x,2)+pow(vectorBetweenSpheres.y,2)+pow(vectorBetweenSpheres.z,2) );

	//float distanceBetweenSpheres = 
	//	(float)sqrt( pow(vectorBetweenSpheres.x,2)+pow(vectorBetweenSpheres.y,2)+pow(vectorBetweenSpheres.z,2) );

	float minDistance = pObj1->boundSphereRadius + pObj2->boundSphereRadius;
	//minDistance *= minDistance;

	//if a collision occurs
	if( distanceBetweenSpheres <= minDistance  )
	{
		//move objects out a bit
		float distToMove = (minDistance - distanceBetweenSpheres)/2;

		//Console::GetInstance().Log("vect between: %f", vectorBetweenSpheres.x, vectorBetweenSpheres.y, vectorBetweenSpheres.z);


		//Console::GetInstance().Log("dist to move: %f, minDist: %f, actual dist: %f", distToMove, minDistance, distanceBetweenSpheres);

		//notify objects, with the position and normal
		D3DXVec3Normalize( &vectorBetweenSpheres, &vectorBetweenSpheres);

		if( pObj2->IsA(OBJ_CAMERA) )
		{
			pObj1->SetPosition( pObj1->position + ( (vectorBetweenSpheres) * distToMove * 2 ) );
		}
		else if( pObj1->IsA(OBJ_CAMERA) )
		{
			pObj2->SetPosition( pObj2->position + (-vectorBetweenSpheres * distToMove * 2 ) );
		}
		else
		{
			pObj2->SetPosition( pObj2->position + (-vectorBetweenSpheres * distToMove ) );
			pObj1->SetPosition( pObj1->position + ( (vectorBetweenSpheres) * distToMove ) );
		}

		pObj1->Collision(pObj2, vectorBetweenSpheres);
		pObj2->Collision(pObj1, -vectorBetweenSpheres);
		//systemLog.WriteLog("collision between 2 spheres");
	}
}

//inline float RayIntersectPlane(const D3DXVECTOR3& rayVector, const D3DXVECTOR3& rayPoint, const D3DXVECTOR3& planeNormal, const D3DXVECTOR3& planePoint) const;
//	inline int ClassifyPoint(const D3DXVECTOR3& point, const D3DXVECTOR3& planeNormal, const D3DXVECTOR3& planePoint) const;


//float CollisionManager::RayIntersectPlane(D3DXVECTOR3 rayVector, D3DXVECTOR3 rayPoint, D3DXVECTOR3 planeNormal, D3DXVECTOR3 planePoint)
//inline float RayIntersectPlane(const D3DXVECTOR3& rayVector, const D3DXVECTOR3& rayPoint, const D3DXVECTOR3& planeNormal, const D3DXVECTOR3& planePoint)// const
float CollisionManager::RayIntersectPlane(const D3DXVECTOR3& rayVector, const D3DXVECTOR3& rayPoint, const D3DXVECTOR3& planeNormal, const D3DXVECTOR3& planePoint)
{
	/*
	 * dot product - 
	 * supplies a measure of the difference between the the directions in which 
	 * 2 vectors point
	 */

	float d = - D3DXVec3Dot( &planeNormal, &planePoint );
	float numer = D3DXVec3Dot( &planeNormal, &rayPoint ) + d;
	float denom = D3DXVec3Dot( &planeNormal, &rayVector );
  
	if (denom == 0)  // normal is parallel to vector, cant intersect
		return (-1.0f);

	return -(numer / denom);
}

//int CollisionManager::ClassifyPoint(D3DXVECTOR3 point, D3DXVECTOR3 planeNormal, D3DXVECTOR3 planePoint)
int CollisionManager::ClassifyPoint(const D3DXVECTOR3& point, const D3DXVECTOR3& planeNormal, const D3DXVECTOR3& planePoint)
{
	 D3DXVECTOR3 dir = planePoint - point;
	 double d = D3DXVec3Dot( &dir, &planeNormal );
 
	 if ( d < -0.001f )
		return PLANE_FRONT;	
	 else if ( d > 0.001f )
		return PLANE_BACKSIDE;	

	return PLANE_ON;
}


