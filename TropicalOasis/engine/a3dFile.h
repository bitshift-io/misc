#ifndef _SIPHON_A3DFILE2_
#define _SIPHON_A3DFILE2_

#include "stdafx.h"

#define A3D_PARENT "*parent"
#define A3D_END "*end"
#define A3D_BONE "*bone"
#define A3D_MESH "*mesh"
#define A3D_FACE "*faces"
#define A3D_VERTEX "*vertex"
#define A3D_NORMAL "*normal"
#define A3D_SKIN "*skin"
#define A3D_TEXTURE "*texture"
#define A3D_TEXTUREVERTEX "*texturevert"
#define A3D_TEXTUREFACE "*textureface"
#define A3D_MATERIALID "*materialid"
#define A3D_MATERIAL "*material"
#define A3D_SHINE "*shine"
#define A3D_SPECULAR "*specular"
#define A3D_DIFFUSE "*diffuse"
#define A3D_AMBIENT "*ambient"
#define A3D_DOUBLESIDED "*doublesided"
#define A3D_SPHEREBOUND "*sphereBoundRadius"

#define STANDARD_FVF (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1)

#define SKELETALMESH_FVF (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX3 | D3DFVF_TEXCOORDSIZE2(0) | D3DFVF_TEXCOORDSIZE4(1) | D3DFVF_TEXCOORDSIZE4(2)) 


struct STANDARD_VERTEX
{
	static int FVF;

    D3DXVECTOR3 p;
    D3DXVECTOR3 n;
    FLOAT       tu, tv;

	STANDARD_VERTEX()
	{
		tu = tv = 0;
		n = D3DXVECTOR3(0,0,0);
		p = D3DXVECTOR3(0,0,0);
	}

	bool operator==(STANDARD_VERTEX const& rhs) const
	{
		if( p == rhs.p && n == rhs.n && tu == rhs.tu && tv == rhs.tv) //
			return true;

		return false;
	}
};

//THIS HSOULD BE A SECOND STREAM WITH JUST BONE DATA!!!! see:
// "Programmable Stream Model" dx9 sdk help
struct BONE_VERTEX : public STANDARD_VERTEX
{
	static int FVF;

	D3DXVECTOR4 boneId; //4 bone id's
	D3DXVECTOR4 weight; //4 bone weights
	int numBones;

	BONE_VERTEX()
	{
		STANDARD_VERTEX::STANDARD_VERTEX();

		boneId = D3DXVECTOR4(0,0,0,0);
		weight = D3DXVECTOR4(0,0,0,0);
		numBones = 0;
	}

	bool operator==(BONE_VERTEX const& rhs) const
	{
		if( STANDARD_VERTEX::operator==(rhs) && rhs.boneId == boneId && rhs.weight == weight)
			return true;

		return false;
	}
};

//used when loading files
struct STL_SKELETALMESH
{
	float sphereBoundRadius;

	std::vector<BONE_VERTEX> vertexList;
	std::vector<WORD> indexList;

	TEXTURE* texture;
	D3DMATERIAL9 material;

	int numVertices;
	int numFaces;

	DWORD fvf;
	UINT vertexSize;

	STL_TEXTUREBATCH()
	{
		fvf = STANDARD_FVF;
		vertexSize = sizeof(STANDARD_VERTEX);
	}
};

//used when loading files
struct STL_TEXTUREBATCH
{
	float sphereBoundRadius;

	bool bDoubleSided;

	std::vector<STANDARD_VERTEX> vertexList;
	std::vector<WORD> indexList;

	TEXTURE* texture;
	D3DMATERIAL9 material;

	int numVertices;
	int numFaces;

	DWORD fvf;
	UINT vertexSize;

	STL_TEXTUREBATCH()
	{
		fvf = STANDARD_FVF;
		vertexSize = sizeof(STANDARD_VERTEX);
		bDoubleSided = false;
	}
};



struct A3DFloat3
{
	float a,b,c;
};

struct A3DInt3
{
	int a,b,c;
};

//each vertex can have up to 4 bones
struct A3DSkin
{
	int vertIdx;
	float boneId[4];
	float weight[4];

	int num; //how many bones are there?

	A3DSkin()
	{
		vertIdx = 0;
		num = 0;

		for( int i=0; i < 4; i++)
		{
			boneId[i] = 0;
			weight[i] = 0;
		}
		//boneId = 0;
		//weight = 0;
	}
};

struct A3DMesh
{
	std::vector<A3DSkin> skin;
	std::vector<A3DFloat3> vertex;
	std::vector<A3DFloat3> normal;
	std::vector<A3DInt3> face;
	std::vector<A3DFloat3> textureVert;
	std::vector<A3DInt3> textureFace;
	int materialId;
	float sphereBoundRadius;
};

struct A3DMaterial
{
	int id;
	A3DFloat3 ambient;
	A3DFloat3 diffuse;
	A3DFloat3 specular;
	int shine;
	char texture[255];
	bool bDoubleSided; 
};

struct A3DBone
{
	int id;
	int parentId;
};

/*
//used when loading files
struct STL_TEXTUREBATCH
{
	std::vector<STANDARD_VERTEX> vertexList;
	std::vector<WORD> indexList;

	LPDIRECT3DTEXTURE9 texture;
	D3DMATERIAL9 material;

	int numVertices;
	int numFaces;
};
 */

class A3DFile
{
public:
	//A3DFile();
	//A3DFile(std::string path);
	A3DFile( Direct3D* pD3D, FileManager* pFM, std::string path, MESH* mesh);
	//A3DFile(std::string path, Mesh* mesh);
	//A3DFile(std::string path, SkeletalMesh* mesh);

	void LoadModel(std::string path); //loads model from file
	//void LoadIntoMesh(Mesh* mesh); //loads data into mesh
	void LoadIntoMesh(MESH* mesh); //loads data into mesh
	//void LoadIntoMesh( SkeletalMesh* mesh ); //loads data into mesh
	//void LoadIntoMesh(SkeletalAnimMesh* skeltalMesh); //loads data into mesh

	bool ConatinsBoneData();
	bool HasNormals();
	bool HasBoneData();

protected:

	std::vector<A3DMaterial> materials; //raw file data
	std::vector<A3DBone> bones;
	std::vector<A3DMesh> meshes;

	std::vector<STL_TEXTUREBATCH> data; //compiled data

	std::vector<STL_SKELETALMESH> dataSkeletal; //compiled data
	//std::vector<Bone> dataBone; //for skeletal bones

	void ProcessData(); //this function take what we have read in, and fills data vector

	void ReadBone();
	void ReadMesh();
	void ReadMaterial();

	void ReadNormals( A3DMesh& mesh, int num );
	void ReadSkins( A3DMesh& mesh, int num );
	void ReadVertices( A3DMesh& mesh, int num );
	void ReadFaces( A3DMesh& mesh, int num );
	void ReadTextureFaces( A3DMesh& mesh, int num );
	void ReadTextureVertices( A3DMesh& mesh, int num );

	FILE* pFile;
	fpos_t currentMesh; // the current mesh, save it because we rewind to it all the time

	Direct3D* pD3D;
	FileManager* pFM;
};

#endif