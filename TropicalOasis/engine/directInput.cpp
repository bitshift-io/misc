#include "stdafx.h"
#include "directInput.h"

DirectInput::DirectInput(HWND hWnd, HINSTANCE hInstance)
{
	HRESULT r = 0;

	for(int i=0; i < 256; i++)
	{
		keyLock[i] = false;
	}

	pDI = 0;
	pMouse = 0;
	pKeyboard = 0;

	// Create the D3D object. //hInstance
    r = DirectInput8Create( hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8A,
		 (void**)&pDI, NULL);
	if( FAILED( r ) )
	{
//cos*		Engine::GetInstance().GetConsole().Log("Creating Direct Input Interface [FAILED]");
//		return E_FAIL;
	}

	r = pDI->CreateDevice(GUID_SysMouse, &pMouse,NULL);
	if( FAILED( r ) )
	{
//cos*		Engine::GetInstance().GetConsole().Log("Creating Direct Input Mouse Device [FAILED]");
//		return E_FAIL;
	}

	r = pDI->CreateDevice(GUID_SysKeyboard, &pKeyboard,NULL);
	if( FAILED( r ) )
	{
//cos*		Engine::GetInstance().GetConsole().Log("Creating Direct Input Keyboard Device [FAILED]");
//		return E_FAIL;
	}

	r = pMouse->SetDataFormat(&c_dfDIMouse2);
	if( FAILED( r ) )
	{
//cos*		Engine::GetInstance().GetConsole().Log("Setting Mouse Format [FAILED]");
//		return E_FAIL;
	}

	r = pKeyboard->SetDataFormat(&c_dfDIKeyboard);
	if( FAILED( r ) )
	{
//cos*		Engine::GetInstance().GetConsole().Log("Setting Keyboard Format [FAILED]");
//		return E_FAIL;
	}

	r = pMouse->SetCooperativeLevel( hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE );
	if( FAILED( r ) )
	{
//cos*		Engine::GetInstance().GetConsole().Log("Setting Mouse Cooperative Level [FAILED]");
//		return E_FAIL;
	}

	r = pKeyboard->SetCooperativeLevel( hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE );
	if( FAILED( r ) )
	{
//cos*		Engine::GetInstance().GetConsole().Log("Setting Keyboard Cooperative Level [FAILED]");
//		return E_FAIL;
	}

	r = pMouse->Acquire();
	if( FAILED( r ) )
	{
//cos*		Engine::GetInstance().GetConsole().Log("Acquiring Mouse [FAILED]");
//		return E_FAIL;
	}

	r = pKeyboard->Acquire();
	if( FAILED( r ) )
	{
//cos*		Engine::GetInstance().GetConsole().Log("Acquiring Keyboard [FAILED]");
//		return E_FAIL;
	}

//	return S_OK;
}

DirectInput::~DirectInput()
{
	SAFE_RELEASE( pMouse );
	SAFE_RELEASE( pKeyboard );
	SAFE_RELEASE( pDI );
}

DIMOUSESTATE2& DirectInput::GetMouseState()
{
	DirectInput::pMouse->GetDeviceState( sizeof(DIMOUSESTATE2), &dims2 );

	return dims2;
}

bool DirectInput::KeyDown(int key) 
{ 
	char keyBuffer[256];

    DirectInput::pKeyboard->GetDeviceState(sizeof(keyBuffer),(LPVOID)&keyBuffer);

	if( keyBuffer[key] & 0x80 )
		return true;
	else
		return false;		
}

bool DirectInput::KeyPressed(int key)
{
	if(	KeyDown(key) && keyLock[key] == false )
	{
		keyLock[key] = true;
		return true;
	}
	if( !KeyDown(key) )
	{
		keyLock[key] = false;
	}

	return false;
}

std::string DirectInput::GetPressedKey( bool alphaNumeric )
{
	for(int i=0; i < 256; i++)
	{
		if( KeyPressed(i) )
		{
			return std::string( GetAsciiCode(i, alphaNumeric) );
		}
	}

	return std::string("");
}

std::string DirectInput::GetAsciiCode( int keyCode, bool alphaNumeric )
{
	//alpha numeric keys
	switch( keyCode )
	{
	case DIK_A:
		return std::string("a");
	case DIK_B:
		return std::string("b");
	case DIK_C:
		return std::string("c");
	case DIK_D:
		return std::string("d");
	case DIK_E:
		return std::string("e");
	case DIK_F:
		return std::string("f");
	case DIK_G:
		return std::string("g");
	case DIK_H:
		return std::string("h");
	case DIK_I:
		return std::string("i");
	case DIK_J:
		return std::string("j");
	case DIK_K:
		return std::string("k");
	case DIK_L:
		return std::string("l");
	case DIK_M:
		return std::string("m");
	case DIK_N:
		return std::string("n");
	case DIK_O:
		return std::string("o");
	case DIK_P:
		return std::string("p");
	case DIK_Q:
		return std::string("q");
	case DIK_R:
		return std::string("r");
	case DIK_S:
		return std::string("s");
	case DIK_T:
		return std::string("t");
	case DIK_U:
		return std::string("u");
	case DIK_V:
		return std::string("v");
	case DIK_W:
		return std::string("w");
	case DIK_X:
		return std::string("x");
	case DIK_Y:
		return std::string("y");
	case DIK_Z:
		return std::string("z");

	case DIK_0:
		return std::string("0");
	case DIK_1:
		return std::string("1");
	case DIK_2:
		return std::string("2");
	case DIK_3:
		return std::string("3");
	case DIK_4:
		return std::string("4");
	case DIK_5:
		return std::string("5");
	case DIK_6:
		return std::string("6");
	case DIK_7:
		return std::string("7");
	case DIK_8:
		return std::string("8");
	case DIK_9:
		return std::string("9");
	}

	if( alphaNumeric )
		return std::string("");

	//special keys
	switch( keyCode )
	{
	case DIK_SPACE:
		return std::string("space");
	}
	//TODO: finish adding special keys

	return std::string("");
}

int DirectInput::GetKeyCode( std::string str )
{
	if( str == "a" )
		return DIK_A;
	else if( str == "b" )
		return DIK_B;
	else if( str == "c" )
		return DIK_C;
	else if( str == "d" )
		return DIK_D;
	else if( str == "e" )
		return DIK_E;
	else if( str == "f" )
		return DIK_F;
	else if( str == "g" )
		return DIK_G;
	else if( str == "h" )
		return DIK_H;
	else if( str == "i" )
		return DIK_I;
	else if( str == "j" )
		return DIK_J;
	else if( str == "k" )
		return DIK_K;
	else if( str == "l" )
		return DIK_L;
	else if( str == "m" )
		return DIK_M;
	else if( str == "n" )
		return DIK_N;
	else if( str == "o" )
		return DIK_O;
	else if( str == "p" )
		return DIK_P;
	else if( str == "q" )
		return DIK_Q;
	else if( str == "r" )
		return DIK_R;
	else if( str == "s" )
		return DIK_S;
	else if( str == "t" )
		return DIK_T;
	else if( str == "u" )
		return DIK_U;
	else if( str == "v" )
		return DIK_V;
	else if( str == "w" )
		return DIK_W;
	else if( str == "x" )
		return DIK_X;
	else if( str == "y" )
		return DIK_Y;
	else if( str == "z" )
		return DIK_Z;

	if( str == "space" )
		return DIK_SPACE;
	//TODO: finish adding special keys
}
