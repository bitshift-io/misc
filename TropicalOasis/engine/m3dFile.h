#ifndef _SIPHON_M3DFILE_
#define _SIPHON_M3DFILE_

#define A3D_MESH "*mesh"
#define A3D_INDEX "*index"
#define A3D_VERTEX "*vertex"
#define A3D_TEXTURE "*texture"
#define A3D_MATERIAL "*material"
#define M3D_SUBMATERIAL "*submaterial"
#define M3D_SECTION "*section"
#define M3D_NUMBERCELLS "*numbercells"
#define M3D_CELLDIMENSION "*dimension"
#define M3D_STARTVALUE "*startvalue"
#define M3D_HEAD "*head"
#define M3D_RENDER "*render"
#define M3D_COLLISION "*collision"
#define M3D_STATIC "*static"
#define M3D_MODEL "*mesh"
#define M3D_UTILE "*utile"
#define M3D_VTILE "*vtile"
#define M3D_BLENDTEXTURE "*blendmap"

//temp from ska fileloader
#define SKA_END "*end"
#define SKA_BONE "*bone"
#define SKA_KEYFRAME "*keyframe"
#define SKA_ANIMATION "*animation"
#define SKA_ROTATION "*rotation"
#define SKA_POSITION "*position"
#define SKA_SCALE "*scale"

struct M3DSubMaterial
{
	std::string textureFile;
	std::string blendTextureFile;
	float uTileTexture, vTileTexture;
	float uTileBlendTexture, vTileBlendTexture; //should this be tilable?
};

struct M3DStatic
{
	std::string file;
	float x, y, z;
	float yaw, pitch, roll;
	float scalex, scaley, scalez;

	M3DStatic()
	{
		x = 0;
		y = 0;
		z = 0;
		yaw = 0;
		pitch = 0;
		roll = 0;
		scalex = 0;
		scaley = 0;
		scalez = 0;
	}
};


class M3DFile
{
public:
	M3DFile( Direct3D* pD3D, FileManager* pFM, std::string fileName, std::string mapPath, std::string meshPath );

	void LoadModel(std::string fileName); //loads model from file
	//void LoadIntoMesh(Mesh* mesh, int section); //loads data into mesh
	void LoadIntoMesh(MESH* mesh, int section); //loads data into mesh
	void LoadIntoModel(RenderCell& model, int section); //loads data into mesh
	void LoadIntoCollisionMesh(COLLISIONMESH* mesh, int section); //loads collision data into mesh

	bool LoadStatics( Model* mesh, int number ); //this loads the statics read from the map
						//and adds them to the world

	int GetNumberCollisionCells();
	float GetCollisionCellDimensions();

	int GetNumberCells();
	float GetCellDimensions();
	float GetStartValue();

	int GetNumStatics();

protected:
	void ProcessData(); //this function take what we have read in, and fills data vector

	void ReadRenderData();
	void ReadCollisionData();

	void ReadStatic();

	void ReadRenderHeader();
	void ReadRenderMesh();

	void ReadCollisionHeader();
	void ReadCollisionMesh();
	//void ReadMaterial();

	void ReadNormals( A3DMesh& mesh, int num );
	void ReadVertices( A3DMesh& mesh, int num );
	void ReadFaces( A3DMesh& mesh, int num );
	void ReadTextureFaces( A3DMesh& mesh, int num );
	void ReadTextureVertices( A3DMesh& mesh, int num );
	void ReadMaterial();
	void ReadSubMaterial();
	void ReadTexture( M3DSubMaterial& material, int textureType );

	std::vector<M3DSubMaterial> subMaterials;
	std::vector<M3DStatic> statics;
	std::vector<A3DMesh> meshes; //raw data
	std::vector<STL_TEXTUREBATCH> data; //compiled data
	std::vector<A3DMesh> colMeshes; //raw collision data
	std::vector<COLLISIONMESH> colData;

	std::vector<TERRAINTEXTURE*> terrainTextures; //processed terrain textures

	int numberCells;
	float cellDimension;

	float startValue;

	int colNumberCells;
	float colCellDimension;

	FILE* pFile;
	Direct3D* pD3D;
	FileManager* pFM;
	std::string mapPath;
	std::string meshPath;
};

#endif