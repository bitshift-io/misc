#include "stdafx.h"

void Log(const char *lpszText, ...)
{
	FILE* pFile = fopen("data\\emergency.log", "a");

	va_list argList;

	//Initialize variable argument list
	va_start(argList, lpszText);

	//Write the text and a newline
	vfprintf(pFile, lpszText, argList);
	putc('\n', pFile);

	//char buffer[255] = {0};
	//vsprintf( buffer, lpszText, argList);

	va_end(argList);

	fclose(pFile);
}

INIFile::INIFile( std::string path )
{
	this->path = path;

	FILE* pFile = 0;
	pFile = fopen( path.c_str(), "r" );

	if( !pFile )
		return;

	//read in data
	char strSection[255] = {0};
	char strKey[255] = {0};
	char strValue[255] = {0};
	char strTemp[255] = {0};

	while( !feof(pFile) )
	{
		strTemp[0] = '\0'; 
		fscanf( pFile, "%s", &strTemp );

		if( strTemp[0] ==  '[' )
		{
			strcpy( strSection, (strTemp + 1) );
			strSection[ strlen(strSection) - 1 ] = '\0';	
		}
		else if( strTemp[0] != '\0' && strTemp[0] != '\n' )
		{			
			strcpy( strKey, strTemp );
			fscanf( pFile, " = %s", &strValue );	
			SetValue(strSection, strKey, strValue);
		}
	}

	fclose( pFile );
}

INIFile::~INIFile()
{
	FILE* pFile = 0;
	pFile = fopen( path.c_str(), "r" );
	
	std::map<std::string, std::map<std::string, std::string> >::iterator sectionIt;
	std::map<std::string, std::string>::iterator keyIt;

	for( sectionIt = data.begin(); sectionIt != data.end(); sectionIt++ )
	{
		fprintf(pFile, "[%s]", sectionIt->first );

		for( keyIt = sectionIt->second.begin(); keyIt != sectionIt->second.end(); keyIt++ )
		{
			fprintf(pFile, "%s = %s", keyIt->first.c_str(), keyIt->second.c_str() );
		}
	}

	fclose( pFile );
}

std::string INIFile::GetValue( std::string section, std::string key )
{
	return data[section][key];
}

void INIFile::SetValue( std::string section, std::string key, std::string value )
{
	data[section][key] = value;
}
/*
void INIFile::SetValue( std::string section, std::string key, float value )
{

}

void INIFile::SetValue( std::string section, std::string key, int value )
{

}

void INIFile::SetValue( std::string section, std::string key, bool value )
{

}

void INIFile::GetValue( std::string section, std::string key, std::string& value )
{
	value = std::string( data[section][key] );
}

void INIFile::GetValue( std::string section, std::string key, float& value  )
{
	sscanf( data[section][key].c_str(), "%f", &value);
}

void INIFile::GetValue( std::string section, std::string key, int& value  )
{
	sscanf( data[section][key].c_str(), "%i", &value);
}

void INIFile::GetValue( std::string section, std::string key, bool& value  )
{
	sscanf( data[section][key].c_str(), "%b", &value);
}*/