#include "stdafx.h"

ParticleSystem::ParticleSystem( FileManager* pFM, Direct3D* pD3D ) : Model()
{
	SetPosition(0,100,0);
	pTexture = pFM->GetTexture( "blood01.dds" );

	pMesh = new MESH();

	pMesh->fvf = VERT_TPOS_TEX::FVF;
	pMesh->vertexSize = sizeof( VERT_TPOS_TEX );
	pMesh->numOfIndices = 6;
	pMesh->numOfPolygons = 6;
	pMesh->numOfVertices = 6;
	pMesh->renderType = D3DPT_POINTLIST;

	WORD idx[ 6 ];

	for( int ci = 0; ci < pMesh->numOfIndices; ci++ )
	{
		idx[ ci ] = ci;
	}

	VERT_POS_NORM_TEX verts[ 6 ];

	for( int cv = 0; cv < pMesh->numOfVertices; cv++ )
	{
		verts[ cv ].x = 100;
		verts[ cv ].y = 100;
		verts[ cv ].z = 100;

		verts[ cv ].nx = 1;
		verts[ cv ].ny = 1;
		verts[ cv ].nz = 1;

		verts[ cv ].tu = 1;
		verts[ cv ].tv = 1;
	}

	if( FAILED( pD3D->GetDevice()->CreateIndexBuffer( sizeof(WORD)*6, D3DUSAGE_WRITEONLY,
    D3DFMT_INDEX16, D3DPOOL_DEFAULT, &pMesh->pIndexBuffer, 0) ))
	{
		return;
	}

	WORD* index;
	if( FAILED( pMesh->pIndexBuffer->Lock(0, 0, (void**)&index, 0) ))
	{
		return;
	}

	for( int n = 0; n < 6; n++ )
	{
		index[n] = idx[n];
	}

	pMesh->pIndexBuffer->Unlock();


	if( FAILED( pD3D->GetDevice()->CreateVertexBuffer( sizeof( VERT_POS_NORM_TEX ) * 6, D3DUSAGE_WRITEONLY,
    VERT_POS_NORM_TEX::FVF, D3DPOOL_DEFAULT, &pMesh->pVertexBuffer, 0) ) )
	{	
		return;
	}

	VERT_POS_NORM_TEX* vertex;	
	if( FAILED( pMesh->pVertexBuffer->Lock(0, 0, (void**)&vertex, 0) ))
	{
		return;
	}

	for( int v = 0; v < 6; v++ )
	{
		vertex[v] = verts[v];
	}

	pMesh->pVertexBuffer->Unlock();
}

ParticleSystem::~ParticleSystem()
{
	SAFE_RELEASE( pMesh->pIndexBuffer );
	SAFE_RELEASE( pMesh->pVertexBuffer );
	SAFE_DELETE( pMesh );
}

int ParticleSystem::Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT pEffect, TEXTURE* pTexture )
{
//	Model::Render( pD3D, pCamera, setMesh, pEffect, pTexture );
//	exit(0);

	pD3D->GetDevice()->SetIndices( 0 );

	HRESULT hr;
	if (FAILED(hr = pD3D->GetDevice()->DrawPrimitive( setMesh->renderType,
					  0, 
					  setMesh->numOfPolygons )))
	{
		return 0;
	}

	return setMesh->numOfPolygons;

//	return 0;
}