#include "stdafx.h"
#include "object.h"

unsigned int Object::objectCount = 0;

Object::Object( const OBJECT_TYPE objType ) // Engine* pEngine )
{
//	SetEngine( pEngine );

	this->objType = objType;

	classType = OBJ_OBJECT;

	bRender = true;
	bDestroy = false;
	bAddToWorldOnUpdate = true;

	position = D3DXVECTOR3(0,0,0);
	velocity = D3DXVECTOR3(0,0,0);
	rotation = D3DXVECTOR3(0,0,0);
	scale = D3DXVECTOR3(1,1,1);

	
	bCollideWorld = true;
	bCollideStatics = true;
	bCollideObjects = true;

	boundSphereRadius = 0; //no collision detection

	parent = 0;

	frameRate = 30; //30 frames in 1000 milliseconds
	playingAnimation = false;
	timeTillNextFrame = 1000/frameRate;

	objId = objectCount;
	objectCount++;
}

Object::~Object()
{

}

void Object::Update( float const deltaTime, World* pW, DirectInput* pDI )
{
	if( bAddToWorldOnUpdate )
	{
		pW->AddObject( this );
	}

	if( playingAnimation )	
	{	
		timeTillNextFrame -= deltaTime;

		position += tweenKey.position * deltaTime;
		rotation += tweenKey.rotation * deltaTime;

		//change frame
		if( timeTillNextFrame <= 0 )
		{
			//rotation = animations[currentAnimation][currentFrame].rotation;

			currentFrame++;
			if( currentFrame >= animations[currentAnimation].size() )
			{
				if( loopAnimation == false ) 
				{
					StopAnimation();
				}
				else
				{
					StopAnimation();
					PlayAnimation( currentAnimation );
				}

				currentFrame = 0;
			}
			
			nextFrame = currentFrame + 1;
			if( nextFrame >= animations[currentAnimation].size() )
			{
				nextFrame = 1;
			}

			timeTillNextFrame = animations[ currentAnimation ][ currentFrame ].time;

			tweenKey.position = (animations[currentAnimation][nextFrame].position - position ) / timeTillNextFrame;		
			tweenKey.rotation = (animations[currentAnimation][nextFrame].rotation - rotation) / timeTillNextFrame;
			/*
			tweenKey.position = (animations[currentAnimation][nextFrame].position - position ) / timeTillNextFrame;

			float r1 = animations[currentAnimation][nextFrame].rotation.x;
			float r2 = rotation.x;

			tweenKey.rotation.x = (r2 - r1) / timeTillNextFrame;
			
			Log("tween rotation:%f %f %f", tweenKey.rotation.x,
				tweenKey.rotation.y,
				tweenKey.rotation.z);*/
		}
	}
}

void Object::PreRender( RenderManager* pRM, BOUNDS* cameraBounds, bool alwaysRendered )
{
//	exit(0);
}

bool Object::Destroy()
{
	return bDestroy;
}

//void Object::Transform()
D3DXMATRIXA16* Object::Transform()
{
	D3DXMATRIXA16 matRotation;
	D3DXMATRIXA16 matTranslation;
	D3DXMATRIXA16 matScale;

	D3DXMatrixIdentity( &matWorld );

	D3DXMatrixRotationYawPitchRoll( &matRotation, rotation.x, rotation.y, rotation.z);
	D3DXMatrixTranslation( &matTranslation, position.x, position.y, position.z);

	D3DXMatrixScaling( &matScale, scale.x, scale.y, scale.z);

	D3DXMatrixMultiply( &matWorld, &matRotation, &matScale);
	D3DXMatrixMultiply( &matWorld, &matWorld, &matTranslation );

	return &matWorld;
}

int Object::Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT pEffect, LPDIRECT3DTEXTURE9 pTexture )
{
	HRESULT hr;
	if (FAILED(hr = pD3D->GetDevice()->DrawIndexedPrimitive( setMesh->renderType,
					  0, 0, setMesh->numOfVertices ,
					  0,  setMesh->numOfPolygons )))
	{
		return 0;
	}

	return setMesh->numOfPolygons;
}


void Object::Collision( Object* pOtherObj, D3DXVECTOR3 normal )
{

}

bool Object::IsA( OBJECT_TYPE type )
{
	if( type == classType )
		return true;
	else
		return false;
}

D3DXMATRIXA16* Object::GetMatrix()
{
	return &matWorld;
}

void Object::SetPosition(D3DXVECTOR3 position)
{
	oldPosition = this->position;
	this->position = position;
}

void Object::SetPosition(float x, float y, float z)
{
	oldPosition = position;

	position.x = x;
	position.y = y;
	position.z = z;
}

void Object::SetRotation(float yaw, float pitch, float roll)
{
	rotation.x = yaw;
	rotation.y = pitch;
	rotation.z = roll;
}

void Object::SetScale(float value)
{
	scale.x = scale.y = scale.z = value;
}

void Object::SetScale(float x, float y, float z)
{
	scale.x = x;
	scale.y = y;
	scale.z = z;
}

AnimationData* Object::GetAnimationData()
{
	return &animations;
}

int Object::GetNumAnimations()
{
	return animations.size();
}

int Object::GetNumFrames( int animId )
{
	return animations[animId].size();
}

void Object::AddKeyFrame(int animId, KeyFrame frame)
{
	if( animations.size() <= animId )
		animations.resize( animId + 1 );

	animations[ animId ].push_back(frame);
}

void Object::PlayAnimation( int animId, bool loop )
{
	currentAnimation = animId;
	this->loopAnimation = loop;
	playingAnimation = true;

	position = animations[animId][0].position;
	rotation = animations[animId][0].rotation;

	currentFrame = -1;
	timeTillNextFrame = 0;

	tweenKey.position = D3DXVECTOR3(0,0,0);
	tweenKey.rotation = D3DXVECTOR3(0,0,0);
}

bool Object::IsPlayingAnimation()
{
	return playingAnimation;
}

void Object::StopAnimation()
{
	playingAnimation = false;
	currentFrame = 0;
}
