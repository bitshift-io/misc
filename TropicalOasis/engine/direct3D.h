#ifndef _SIPHON_DIRECT3D_
#define _SIPHON_DIRECT3D_

/*! \class Direct3D
 *  \brief direct3d methods
 *
 * Initialisation, shutdown and methods related to
 * direct 3d
 */
class Direct3D
{
public:
	Direct3D(HWND hWnd, int newDeviceWidth = 800, int newDeviceHeight = 600, bool bHardware = true, bool bFullScreen = true);
	~Direct3D();

	//void Shutdown();

	//HRESULT Initialise(HWND hWnd, int newDeviceWidth = 800, int newDeviceHeight = 600, bool bHardware = true, bool bFullScreen = true);
	
	bool InitShader();

	LPDIRECT3DDEVICE9 GetDevice();

	void BeginScene( bool clearTarget = false );
	void EndScene();

	void SetWireFrame(bool bWireFrame);
	bool GetWireFrame();

	void SetTransparent();
	void SetOpaque();

	LPDIRECT3D9 pD3D;
	LPDIRECT3DDEVICE9 pD3DDevice;

	int deviceHeight;
	int deviceWidth;
	bool bHardware;

protected:
	bool CheckFormat(D3DFORMAT adapterFormat, D3DFORMAT depthFormat);

	bool bWireFrame;
};


#endif