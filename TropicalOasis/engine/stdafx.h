#ifndef _SIPHON_STDAFX_
#define _SIPHON_STDAFX_

#define DIRECTINPUT_VERSION     0x0800

#define SAFE_DELETE(p){ if(p){delete p; p = 0;} }
#define SAFE_RELEASE(p){ if(p){p->Release(); p = 0;} }

#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "dxerr9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "strmiids.lib")
#pragma comment(lib, "scrnsave.lib")


//tokamak
#pragma comment(lib, "engine\\tokamak\\lib\\tokamak.lib")
#include "tokamak\\include\\tokamak.h"

#include <windows.h>
#include <assert.h>

#include <d3d9.h>
#include <d3dx9math.h>
#include <d3d9types.h>
#include <d3dx9mesh.h>
#include <d3dx9core.h>
#include <dxerr9.h>
#include <dinput.h>
#include <dsound.h>

#include <time.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>

#include <list>
#include <vector>
#include <map>
#include <string>

#include "mmgr.h"


#include "singleton.h"
#include "iniFile.h"
#include "vertexTypes.h"
#include "font.h"
#include "direct3D.h"
#include "console.h"
#include "directInput.h"
#include "object.h"
#include "light.h"
#include "camera.h"
#include "fileManager.h"
#include "renderManager.h"
#include "model.h"
#include "skeletalModel.h"
#include "particleSystem.h"
#include "panel.h"
#include "world.h"
#include "collisionManager.h"
#include "skaFile.h"
#include "a3dFile.h"
#include "m3dFile.h"
#include "objectManager.h"
#include "game.h"
#include "engine.h"

#endif