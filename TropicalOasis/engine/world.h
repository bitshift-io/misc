#ifndef _SIPHON_WORLD_
#define _SIPHON_WORLD_

struct TERRAINTEXTURE
{
	TEXTURE* pBlendTexture;
	TEXTURE* pTexture;
	float uTile, vTile;

	TERRAINTEXTURE()
	{
		pBlendTexture = 0;
		pTexture = 0;
		uTile = 0;
		vTile = 0;
	}
};

class RenderCell : public Model
{
public:

	RenderCell();
	~RenderCell();

	void PreRender();
	void Update( float const deltaTime ); //used to cleear stuff and reset stuff
	
	virtual int Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT pEffect, TEXTURE* pTexture );

	/**
	 * here we do culling eventually
	 */
	void PreRender( RenderManager* pRM, BOUNDS* cameraBounds );

	bool AddObject( Object* pObject );
	bool AddStatic( Object* pObject );

//protected:
	//Model terrain;
//	TerrainTexture* pTerrainBase;
	std::vector<TERRAINTEXTURE*> pTerrainTextures;


	TEXTURE* pBlendMap;
	TEXTURE* pLightMap;

	std::list<Object*> statics;//these arent tested for CD with terrain
								// and arent cleared

	std::list<Object*> contains;//any way to loose the std crap?
};

class CollisionCell
{
public:
	CollisionCell();
	~CollisionCell();

	void Update( float const deltaTime ); //replaced "endscene" and clears

	bool AddObject( Object* pObject );
	bool AddStatic( Object* pObject );

//protected:
	COLLISIONMESH terrain;
	std::list<Object*> contains;//any way to loose the std crap?
	std::list<Object*> statics;//these arent tested for CD with terrain
								// and arent cleared
};

class Water : public Model
{
public:
	Water();
	~Water();

	void LoadWater( Direct3D* pD3D, FileManager* pFM, std::string texture, int size);

	virtual int Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT pEffect, TEXTURE* pTexture );

	TEXTURE* pSkyReflection; 

	TEXTURE* pNormalMap;

	LPDIRECT3DVERTEXDECLARATION9 pVertexDeclaration;
};

/*! \class World
 *  \brief brings together terrain and objects
 *
 * This class tracks all renderable and collision cells
 * for the map, and places objects in the appropriate cell
 */
class World
{
public:

	World( Direct3D* pD3D, FileManager* pFM, std::string mapPath, std::string meshPath );
	~World();

	/**
	 * forces the sky to be rendered
	 */
	int RenderSky( D3DXVECTOR3& position );

	/**
	 * Loads a map
	 */
	bool LoadMap( std::string file );

	/**
	 * unloads the current map
	 */
	void UnloadMap();

	/**
	 * adds an object in the proper cells
	 */
	bool AddObject(Object* pObject); 
	bool AddStatic(Object* pObject);

	
	int GetNumberCollisionCells();
	CollisionCell* GetCollisionCell( int no );

	/**
	 * prerenders the appropriate cells
	 */
	void PreRender( RenderManager* pRM, BOUNDS* cameraBounds );

	float GetCellGap();
	float GetStartValue();

	void Update( float const deltaTime );
protected:
	std::vector<RenderCell> renderCells;
	std::vector<CollisionCell> collisionCells;
	
	float colCellGap;
	float colWidth;
	int colRows;
	int colCols;

	float startValue;
	float cellGap;
	float width;
	int rows;
	int cols;

	Model sky;
	Water water;

	Direct3D* pD3D;
	FileManager* pFM;
	std::string mapPath;
	std::string meshPath;
};

#endif