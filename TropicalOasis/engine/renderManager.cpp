#include "stdafx.h"
#include "renderManager.h"

RenderManager::RenderManager( Direct3D* pD3D )
{
	this->pD3D = pD3D;

	LONGLONG Counter;
	
	QueryPerformanceFrequency((LARGE_INTEGER*)&Counter);
    Scale=1.0f/Counter;
    QueryPerformanceCounter((LARGE_INTEGER*)&curTime);

	previousTime = curTime = timeGetTime();
	lastSecondTime = timeGetTime();
	fpsLastSecond = 0;

	LOGFONT font = {14,0,0,0,FW_NORMAL,false,false,false,DEFAULT_CHARSET,OUT_TT_PRECIS,CLIP_DEFAULT_PRECIS,PROOF_QUALITY,DEFAULT_PITCH,"Arial"};
	RECT FontPosition = {0,0,800,600};

	if(FAILED(D3DXCreateFontIndirect(pD3D->GetDevice(),&font,&pFont)))
	{
		 //Console::GetInstance().Log("Create Font [FAILED]");
	}

	fontPosition.top = 10;
	fontPosition.left = 10;
	fontPosition.bottom = 600;
	fontPosition.right = 800;

	fpsOkay=0.0f;
}

RenderManager::~RenderManager()
{

}

void RenderManager::Update(float deltaTime)
{


}

void RenderManager::Render( ObjectManager* pOM, Camera* pCamera, World* pW ) //object manager is temporary, it should be world
{
	int polys = 0;

	pD3D->GetDevice()->SetTransform( D3DTS_VIEW, pCamera->Transform() );
	pD3D->BeginScene();

	polys += pW->RenderSky( pCamera->position );

	polys += DoRender( RENDER_SOLID, sceneData, pCamera );
	polys += DoRender( RENDER_TRANSPARENT, transparentSceneData, pCamera );
	polys += DoRender( RENDER_HUD, hudSceneData, pCamera );

	//lets calculate some fps
	//and display them
	previousTime = curTime;
    QueryPerformanceCounter((LARGE_INTEGER*)&curTime);

    float deltaTime = (curTime - previousTime)*Scale;
	fpsOkay+=deltaTime;

	//float cfps = 0;
	float cfps = 0.0f;
	if( deltaTime > 0 ) 
	{
		cfps = 1.0/(float)deltaTime;
	}

	if(fpsOkay>=0.5f)
	{	
		sprintf(txt,"Polys: %d\nFPS: %.2f", polys, cfps);
		
		fpsOkay=0.0f;
	}

	pFont->DrawText( txt, -1, &fontPosition, DT_LEFT , 0xffffff00);
	pD3D->EndScene();

	sceneData.clear();
	transparentSceneData.clear();
	hudSceneData.clear();
}

int RenderManager::DoRender( RENDER_TYPE type, AdjacencyList renderList, Camera* pCamera )
{
	int polys = 0;

	if( type == RENDER_SOLID )
	{
		pD3D->GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
		pD3D->GetDevice()->SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );
		//pD3D->GetDevice()->SetRenderState(D3DRS_ZENABLE, TRUE);
	}
	else
	{   
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1 );
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
		//pD3D->GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		//pD3D->GetDevice()->SetRenderState(D3DRS_ZENABLE, FALSE);

		//pD3D->GetDevice()->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE);
		pD3D->GetDevice()->SetRenderState( D3DRS_ALPHATESTENABLE, TRUE );  
		pD3D->GetDevice()->SetRenderState( D3DRS_ALPHAREF, 0x2f ); //0x0000002f );  
		pD3D->GetDevice()->SetRenderState( D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL );
	}

	AdjacencyList::iterator effectIt;
	std::map<TEXTURE*, std::map<WORD, std::map<MESH*, std::list<Model*> > > >::iterator texIt;
	std::map<WORD, std::map<MESH*, std::list<Model*> > >::iterator fvfIt;
	std::map<MESH*, std::list<Model*> >::iterator meshIt;
	std::list<Model*>::iterator modelIt;

	for( effectIt = renderList.begin(); effectIt != renderList.end(); effectIt++)
	{
		if( effectIt->first == 0 )
		{ 
			//loop through textures
			for( texIt = effectIt->second.begin(); texIt != effectIt->second.end(); texIt++)
			{	
				//pD3D->pD3DDevice->SetTexture(0, texIt->first );
				pD3D->pD3DDevice->SetTexture(0, texIt->first->pTexture );

				//loop through fvf
				for( fvfIt = texIt->second.begin(); fvfIt != texIt->second.end(); fvfIt++)
				{
					pD3D->GetDevice()->SetFVF( fvfIt->first );

					//loop through mesh
					for( meshIt = fvfIt->second.begin(); meshIt != fvfIt->second.end(); meshIt++)
					{						
						//render mesh												
						if (FAILED(pD3D->GetDevice()->SetIndices( meshIt->first->pIndexBuffer ) ))
						{
							return polys;
						}

						if (FAILED(pD3D->GetDevice()->SetStreamSource(0, meshIt->first->pVertexBuffer , 0, meshIt->first->vertexSize ) ))
						{
							return polys;
						}
					
						//loop through models
						for( modelIt = meshIt->second.begin(); modelIt != meshIt->second.end(); modelIt++)
						{
							//pD3D->GetDevice()->SetTransform( D3DTS_WORLD, (*modelIt)->GetMatrix() );
							pD3D->GetDevice()->SetTransform( D3DTS_WORLD, (*modelIt)->Transform() );
							
							polys += (*modelIt)->Render( pD3D, pCamera, meshIt->first, 0, texIt->first);
							/*
							HRESULT hr;
							if (FAILED(hr = pD3D->GetDevice()->DrawIndexedPrimitive( meshIt->first->renderType,
												  0, 0, meshIt->first->numOfVertices ,
												  0,  meshIt->first->numOfPolygons )))
							{
								return;
							}


							polys += meshIt->first->numOfPolygons;*/

							
						}
					}
				}
			}
			
		}
		else
		{
			effectIt->first->SetTechnique( "Technique0" );

			UINT uPasses;
			effectIt->first->Begin( &uPasses, 0 );
    
			for( UINT uPass = 0; uPass < uPasses; uPass++ )
			{
				effectIt->first->Pass( uPass );

				//loop through textures
				for( texIt = effectIt->second.begin(); texIt != effectIt->second.end(); texIt++)
				{
					pD3D->pD3DDevice->SetTexture(0, texIt->first->pTexture );

					//loop through fvf
					for( fvfIt = texIt->second.begin(); fvfIt != texIt->second.end(); fvfIt++)
					{
						pD3D->GetDevice()->SetFVF( fvfIt->first );
						
						//loop through mesh
						for( meshIt = fvfIt->second.begin(); meshIt != fvfIt->second.end(); meshIt++)
						{
							//render mesh												
							if (FAILED(pD3D->GetDevice()->SetIndices( meshIt->first->pIndexBuffer ) ))
							{
								return polys;
							}

							if (FAILED(pD3D->GetDevice()->SetStreamSource(0, meshIt->first->pVertexBuffer , 0, meshIt->first->vertexSize ) ))
							{
								return polys;
							}

							//loop through models
							for( modelIt = meshIt->second.begin(); modelIt != meshIt->second.end(); modelIt++)
							{
								pD3D->GetDevice()->SetTransform( D3DTS_WORLD, (*modelIt)->Transform() );
							
								polys += (*modelIt)->Render( pD3D, pCamera, meshIt->first, effectIt->first, texIt->first);
							}
						}
					}
				}
			}

			effectIt->first->End();
		}
	}

	return polys;
}

void RenderManager::PreRender( Model* mdl, bool alwaysRendered )
{
	if( mdl->GetMesh() != 0 )
	{
		if( alwaysRendered )
			hudSceneData[ mdl->pEffect ][ mdl->pTexture ][ mdl->pMesh->fvf ][  mdl->pMesh ].push_back( mdl );
		if( !mdl->GetTexture()->bTransparent )
			sceneData[ mdl->pEffect ][ mdl->pTexture ][ mdl->pMesh->fvf ][  mdl->pMesh ].push_back( mdl );
		else
			transparentSceneData[ mdl->pEffect ][ mdl->pTexture ][ mdl->pMesh->fvf ][  mdl->pMesh ].push_back( mdl );
	}
}

void RenderManager::PreRender( LPD3DXEFFECT pEffect, TEXTURE* pTexture, WORD fvf, MESH* pMesh, Model* mdl, bool alwaysRendered )
{
	if( pMesh != 0 )
	{
		if( alwaysRendered )
			hudSceneData[ pEffect ][ pTexture ][ fvf ][  pMesh ].push_back( mdl );
		else if( !pTexture->bTransparent )
			sceneData[ 0 ][ pTexture ][ fvf ][  pMesh ].push_back( mdl );//pEffect
		else
			transparentSceneData[ 0 ][ pTexture ][ fvf ][  pMesh ].push_back( mdl ); //pEffect
	}
}
