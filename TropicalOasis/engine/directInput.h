#ifndef _SIPHON_DIRECTINPUT_
#define _SIPHON_DIRECTINPUT_

/*! \class DirectInput
 *  \brief Methods dealing with direct input
 *
 * This class deals with intiliasation and shutting
 * down direct input, it also includes methods
 * related to input
 */
class DirectInput
{
public:
	LPDIRECTINPUT8 pDI;
	LPDIRECTINPUTDEVICE8 pMouse;
	LPDIRECTINPUTDEVICE8 pKeyboard;

	DIMOUSESTATE2 dims2;

	~DirectInput();
	DirectInput(HWND hWnd, HINSTANCE hInstance);

	DIMOUSESTATE2& GetMouseState();

	/**
	 * Determines if they key is currently down
	 */
	bool KeyDown(int key);

	/**
	 * determines if the key was pressed
	 */
	bool KeyPressed(int key);

	/**
	 * looks for any pressed keys, and returns it
	 * as a string, but is alphaNumeric is set to true
	 * then it will only return a value if a-z or 0-9 is pressed
	 */
	std::string GetPressedKey( bool alphaNumeric = false ); 

	/**
	 * give this a string like "a" or "shift"
	 * and this function will return the
	 * dx key code eg. DIK_A or DIK_SHIFT
	 */
	int GetKeyCode( std::string str );

	/**
	 * give this something like DIK_A
	 * and it will return "a"
	 */
	std::string GetAsciiCode( int keyCode, bool alphaNumeric = false );

protected:
	bool keyLock[256];
};

#endif