//handle screen saver code here
#include "stdafx.h"
#include <windows.h>
#include <scrnsave.h>
#include "../autocam.h"

//#define SCREENSAVER

#ifdef SCREENSAVER
/*
#define szAppName	"Tropical Oasis - Siphon Engine 2.0"
#define szAuthor	"Written by Fabian Mathews, � 2003"
#define szPreview	"Tropical Oasis - Siphon Engine 2.0"

Engine* engine;

LRESULT CALLBACK ScreenSaverProc( HWND hWnd, UINT uMsg, WPARAM wParam,
								  LPARAM lParam )
{
	
	static UINT  uTimer = 0;
	//static DWORD dwFakeMouseMoveMsgAllowed = 5;
	//HRESULT      hr;

    switch( uMsg )
    {
		case WM_CREATE:
			{
				//Engine e(hInst, std::string( cmdLine ) );
				//Engine e( hWnd );
				engine = new Engine( hWnd );
				
				AutoCam* c = new AutoCam( engine->GetDirect3D(), engine->GetSystemINI(), engine->GetFileManager(), engine->GetMeshPath() ); 
				engine->SetCamera( (Camera*)c );

				Light l;// = new Light(
				D3DLIGHT9* light = l.GetLight();

				light->Position = D3DXVECTOR3(0, 50, 0);
				light->Direction = D3DXVECTOR3(0, 0, 0);

				light->Range = 50000.0f;

				l.ResetLight( engine->GetDirect3D() );

				Panel* p = new Panel();
				p->CreatePanel( engine->GetFileManager(), engine->GetDirect3D(), 120, 120, 1, 2, "banner.dds");
				engine->GetObjectManager()->AddObject( (Object*)p, true );

				// Create a timer
				uTimer = SetTimer( hWnd, 1, 0, NULL );

				break;
			}
		case WM_MOUSEMOVE:
			break;
		case WM_KEYDOWN:
			break;
		case WM_ERASEBKGND:
			break;
		
		case WM_TIMER:
			{
				engine->DoGameStuff();
			}
			break;
			//// Draw the scene
            //if( g_bReady )
            //    Render3DEnvironment( hWnd );
			//return 0;
		
		case WM_DESTROY:
			{
				SAFE_DELETE( engine );
			// Clean everything up and exit
			//KillTimer( hWnd, 1 );
			//Cleanup3DEnvironment( hWnd );
				PostQuitMessage(0);
				break;
			}
		default:
			// Pass all unhandled msgs to the default msg handler
			return DefScreenSaverProc( hWnd, uMsg, wParam, lParam );			
	}

	return 0;
}

BOOL WINAPI RegisterDialogClasses( HANDLE hInstance )
{
	return TRUE;
}

BOOL WINAPI ScreenSaverConfigureDialog( HWND hDlg, UINT uMsg, WPARAM wParam,
							            LPARAM lParam )
{
	EndDialog(hDlg, FALSE);
    return TRUE;
  
//	return FALSE;

/*
	if( WM_INITDIALOG == uMsg )
    {
	    // Handle the initialization message
		ReadSettings( &g_CurrentOptions );

		if( TRUE == g_CurrentOptions.bUse640x480Mode )
			SendDlgItemMessage( hDlg, IDC_640x480_MODE, BM_SETCHECK, BST_CHECKED, 0 ); 
		else
			SendDlgItemMessage( hDlg, IDC_DEFAULT_MODE, BM_SETCHECK, BST_CHECKED, 0 ); 

		if( TRUE == g_CurrentOptions.bUseHardware )
			SendDlgItemMessage( hDlg, IDC_HARDWARE, BM_SETCHECK, BST_CHECKED, 0 ); 
		else
			SendDlgItemMessage( hDlg, IDC_SOFTWARE, BM_SETCHECK, BST_CHECKED, 0 ); 

        return TRUE;
    }
    
    if( WM_COMMAND == uMsg )
    {
        if( IDOK == LOWORD(wParam) )
        {
	        // Handle the case when the user hits the OK button
			g_CurrentOptions.bUse640x480Mode = SendDlgItemMessage( hDlg, IDC_640x480_MODE, BM_GETCHECK, 0, 0 ); 
			g_CurrentOptions.bUseHardware = SendDlgItemMessage( hDlg, IDC_HARDWARE, BM_GETCHECK, 0, 0 ); 
			WriteSettings( &g_CurrentOptions );
    
			EndDialog( hDlg, IDOK );
            return TRUE;
        }
        else if( IDCANCEL == LOWORD(wParam) )
        {
	        // Handle the case when the user hits the Cancel button
            EndDialog( hDlg, IDCANCEL );
            return TRUE;
        }
    }

    return FALSE;* /
}*/

#endif