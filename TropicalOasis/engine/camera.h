/* ====================================
	CAMERA
This is the viewing camera
==================================== */

#ifndef _SIPHON_CAMERA_
#define _SIPHON_CAMERA_

struct BOUNDS
{
	D3DXVECTOR3 points[8]; //8 points to our camera's bounds, used to determine what chunks of map to draw
	D3DXPLANE planes[6]; //frsutum made of 6 planes
};

/*! \class Camera
 *  \brief Viewing camera
 * 
 * This is a viewing camera
 */
class Camera : public Object
{
public:
	Camera( Direct3D* pD3D, INIFile* ini );
	virtual ~Camera();

	virtual void Update( float const deltaTime, World* pW, DirectInput* pDI );
	D3DXMATRIXA16* Transform();

	BOUNDS* TransformBounds(); //transofrm bounding box, to world for render determination
	void UpdateCullInfo( BOUNDS* pCullInfo, D3DXMATRIXA16* pMatView, D3DXMATRIXA16* pMatProj );

	void TransformOtho(); //set camera up for 2d rendering
	GetTransposeMatrix(D3DXMATRIX& returnMat); //for sprites, transposed matrix points in oposite direction to camera :)
	D3DXVECTOR3 GetDirection();

	D3DXMATRIXA16& GetView();
	D3DXMATRIXA16& GetProjection();

	BOUNDS frustum; //object coordinates
	BOUNDS worldFrustum; //world coordinates

	D3DXVECTOR3 eye; //lookAt

	D3DXVECTOR3 up; 
	D3DXVECTOR3 right;
	D3DXVECTOR3 lookAt;

	//D3DXVECTOR3 rotation;

	D3DXMATRIXA16 matView;
	D3DXMATRIXA16 matProjection;
	D3DXMATRIXA16 matCamera;

	D3DXVECTOR3 GetMatrix();
	//bool IsA(int type);

	bool bFreeCam; //calls the free cam function 
private:
	Direct3D* pD3D;

	bool bInvertMouse;
	float sensitivity;
	
	void FreeCam( float deltaTime, DirectInput* pDI );
	
};

#endif