#include "stdafx.h"

/*
Unknown::Unknown()
{
	Console::GetInstance().RegisterCmd( "test", test );
}
*/
//Unknown u;
//Console::GetInstance().RegisterCmd( "test", u.test );

/**
 * only to be used if the console log is failing!!!
 */
void EmergencyLog(const char *lpszText, ...)
{
	FILE* pFile = fopen("data\\emergency.log", "wa");

	va_list argList;

	//Initialize variable argument list
	va_start(argList, lpszText);
	//Write the text and a newline
	vfprintf(pFile, lpszText, argList);
	putc('\n', pFile);

	//char buffer[255] = {0};
	//vsprintf( buffer, lpszText, argList);

	va_end(argList);

	fclose(pFile);
}

Console::Console()
{
	logFile = 0;
	logFile = fopen("data\\system.log", "w");

	if( !logFile )
	{
		EmergencyLog("Cannot open log file");
		exit(0);
	}

	bShow = false;

	Log("testing");

	linesPerPage = 39;
	page = 0; //this is the end of the list

	currentMsg = "";
}

Console::~Console()
{
	if( logFile != 0 )
		fclose(logFile);

	delete font;
	font = 0;

	logFile = 0;
}

bool Console::Initialise(RenderManager* pRM, Direct3D* pD3D) //called when d3d device is ready
{
	pRM = pRM;
	font = new Font(pD3D); //Font(20, "Arial");

	return true;
}

void Console::Shutdown()
{
	if( logFile != 0 )
		fclose(logFile);

	delete font;
	font = 0;

	logFile = 0;
}


void Console::Log(const char *lpszText, ...)
{
		va_list argList;

		//Initialize variable argument list
		va_start(argList, lpszText);

		//Write the text and a newline
		vfprintf(logFile, lpszText, argList);
		putc('\n', logFile);

		char buffer[255] = {0};
		vsprintf( buffer, lpszText, argList);

		std::string newStr( buffer );
		messages.push_back( newStr );

		va_end(argList);
}

void Console::Update( float deltaTime, DirectInput* pDI )
{
	static bool showLock = false;
	static bool upLock = false;
	static bool downLock = false;

	//check to see if we should render or disable the console
	if( pDI->KeyDown(DIK_GRAVE) && !showLock )
	{
		showLock = true;
		bShow = !bShow;
	}
	if( !pDI->KeyDown(DIK_GRAVE) )
	{
		showLock = false;
	}

	//check for page up and down
	if( pDI->KeyDown(DIK_PGUP) && !upLock )
	{
		upLock = true;

		if( !(((page+1) * linesPerPage) > messages.size()) )
			page++;

	}
	if( !pDI->KeyDown(DIK_PGUP) )
	{
		upLock = false;
	}

	if( pDI->KeyDown(DIK_PGDN) && !downLock )
	{
		downLock = true;
		page--;

		if( page < 0 )
			page = 0;
	}
	if( !pDI->KeyDown(DIK_PGDN) )
	{
		downLock = false;
	}

	//check for any alpha numeric key presses
	if( bShow )
	{
		//check for special keys
		if( pDI->KeyPressed(DIK_RETURN) )
		{
			Log( currentMsg.c_str() );
			ProcessCommand( currentMsg );
			currentMsg = "";
		}
		if( pDI->KeyPressed(DIK_SPACE) )
		{
			currentMsg += " ";
		}
		if( pDI->KeyPressed(DIK_BACK) )
		{
			currentMsg = currentMsg.substr(0, (currentMsg.size() - 1));
		}

		currentMsg += pDI->GetPressedKey(true);
	}
}

void Console::ProcessCommand( std::string currentMsg )
{
	if( currentMsg == "exit" || currentMsg == "quit" )
	{
		//Engine::GetInstance().Exit();
	}

	//if( currentMsg == "fps" )
	//	pEngine->GetRenderManager().showFps = !pEngine->GetRenderManager().showFps;
 
	if( currentMsg == "clear" )
	{
		messages.resize(0);
		
		if( logFile != 0 )
		{
			fclose(logFile);
			logFile = fopen("system.log", "w");
		}
	}
/*
	if( currentMsg == "wireframe" )
	{
		Direct3D::GetInstance().SetWireFrame( !Direct3D::GetInstance().GetWireFrame() );	
	}*/

	//look up cmd in command map
	int pos = currentMsg.find(" ");
	std::string cmd = currentMsg.substr(0, pos);

	//StringCallback ptr = commands[ cmd ].second;
	//std::string params = commands[ cmd ].first;

	//process parameters and pass them in
	int space = 0;
	int lastSpace = 0;
	bool bEnd = false;
	std::string param = "";
/*
	while( !bEnd )
	{
		lastSpace = space;
		space = params.find(" ");

		if( space >= params.size() || space < 0 )
			bEnd = true;
		else
		{
			param = params.substr( lastSpace, space );
			Log( param.c_str() );
		}
	}*/
}
/*
void Console::RegisterCmd( std::string cmd, StringCallback ptr )
{
	int space = cmd.find(" ");
	
	std::string command = cmd.substr( 0, space );
	std::string params = cmd.substr( space, cmd.size() );
	
	commands[ command ].second = ptr;
	commands[ command ].first = params;
}*/

int Console::Render()
{
	if( !bShow )
		return 0;

	//20 lines per page
	int end = messages.size() - ( page * linesPerPage );
	int start = (end - linesPerPage) - ( page * linesPerPage );

	if( start < 0 )
		start = 0;
	if( end < 0 )
		end = 0;

	char buffer[9] = {0};
	for(int i = start, int j = (600 - 15) - (end - start)*15 ; i < end; i++, j+= 15)
	{		
		sprintf(buffer, "%i:  ", i);
		std::string str = std::string(buffer) + messages[i];
		if( i%2 )
			font->DrawText( str, 5, j, 0xffffffff );
		else
			font->DrawText( str, 5, j, 0xffdfdfdf );
	}
	std::string curCmd = std::string(">") + currentMsg;
	font->DrawText( curCmd, 5, j, 0xffffffff );

	return 0;
}