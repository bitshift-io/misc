#include "stdafx.h"
#include "direct3d.h"

//HRESULT Direct3D::Initialise(HWND hWnd,  int newDeviceWidth, int newDeviceHeight,bool bHardware, bool bFullScreen)
Direct3D::Direct3D(HWND hWnd,  int newDeviceWidth, int newDeviceHeight,bool bHardware, bool bFullScreen)
{
	pD3DDevice = 0;
	pD3D = 0;

	deviceHeight = newDeviceWidth;
	deviceWidth = newDeviceHeight;

    //First of all, create the main D3D object. If it is created successfully we
    //should get a pointer to an IDirect3D8 interface.
    pD3D = Direct3DCreate9(D3D_SDK_VERSION);
    if(pD3D == NULL)
    {
//cos*		Engine::GetInstance().GetConsole().Log("Create D3D Inteface [FAILED]");
        //return E_FAIL;
    }

	HRESULT hr;


	//get device capabilities
	D3DCAPS9 caps;

	if( FAILED( hr = pD3D->GetDeviceCaps( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps ) ) )
	{
		EmergencyLog("D3D: Failed to retrive device capabilities");
		return;
	}

	//check for z buffer support
	D3DFORMAT depthFormat = D3DFMT_D16;
	D3DFORMAT adapterFormat = D3DFMT_UNKNOWN;

	if( CheckFormat( D3DFMT_X8R8G8B8, D3DFMT_D24S8) )
	{
		depthFormat = D3DFMT_D24S8;
		adapterFormat = D3DFMT_X8R8G8B8;
	}
	else if( CheckFormat( D3DFMT_X8R8G8B8, D3DFMT_D16) )
	{
		depthFormat = D3DFMT_D16;
		adapterFormat = D3DFMT_X8R8G8B8;
	}
	else
	{
		EmergencyLog("D3D: Formats not supported");
	}

    //Create a structure to hold the settings for our device
    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory(&d3dpp, sizeof(d3dpp));

	d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
    d3dpp.Windowed =FALSE; //TRUE;
    d3dpp.SwapEffect = D3DSWAPEFFECT_FLIP; //D3DSWAPEFFECT_DISCARD; //
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	d3dpp.BackBufferCount = 1;
	d3dpp.BackBufferFormat = adapterFormat;// bFullScreen ? D3DFMT_X8R8G8B8 : D3DFMT_UN
	d3dpp.EnableAutoDepthStencil = TRUE;
    d3dpp.AutoDepthStencilFormat = depthFormat;
	d3dpp.BackBufferWidth = deviceWidth;
	d3dpp.BackBufferHeight = deviceHeight;

	ShowCursor(false);

    //Create a Direct3D device.
	

	//bHardware = false;
///	if( bHardware ) //SHOULD BE USING HARDWARE!!!!! if black, need to set texture stage states
//	{
	if(	SUCCEEDED( hr = pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
                                   D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pD3DDevice) ) )
	{
		EmergencyLog("D3D: Using HAL + hardware vertex processing");	
	}
	else if( SUCCEEDED( hr = pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
                                   D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &pD3DDevice) ) )
	{
		EmergencyLog("D3D: Using HAL + software vertex processing");
	}
	else if( SUCCEEDED( hr = pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, hWnd,
                                   D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &pD3DDevice) ) )
	{
		EmergencyLog("D3D: Using REF + software vertex processing");
	}
	else
	{
		EmergencyLog("D3D: Cant make hardware or software???");
	}

/*	}
	else
	{
		hr = pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
                                   D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &pD3DDevice);
	}
*/
	//depth buffering
	pD3DDevice->SetRenderState( D3DRS_ZENABLE, TRUE );

	//culling
	pD3DDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW ); //D3DCULL_CCW

	//specular lighting
	pD3DDevice->SetRenderState( D3DRS_SPECULARENABLE, TRUE );

	//enable dithering
	pD3DDevice->SetRenderState( D3DRS_DITHERENABLE, TRUE );

	//alpha settigns
	pD3DDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	pD3DDevice->SetRenderState(D3DRS_ALPHAREF, 255);//0x00000000);
	pD3DDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL); //D3DCMP_ALWAYS);

	//Set how the texture should be blended (use alpha)
	pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	/*
	//point sprites
	pD3DDevice->SetRenderState( D3DRS_POINTSPRITEENABLE, TRUE );
	pD3DDevice->SetRenderState(D3DRS_POINTSCALEENABLE, TRUE);

	pD3DDevice->SetRenderState( D3DRS_POINTSIZE,     (DWORD)(0.01f) );
    pD3DDevice->SetRenderState( D3DRS_POINTSIZE_MIN, (DWORD)(0.00f) );
	
    pD3DDevice->SetRenderState( D3DRS_POINTSCALE_A,  (DWORD)(0.00f) );
    pD3DDevice->SetRenderState( D3DRS_POINTSCALE_B,  (DWORD)(0.00f) );
    pD3DDevice->SetRenderState( D3DRS_POINTSCALE_C,  (DWORD)(0.00f) );
	*/

	//for texture tiling/scaling/rotations
	pD3DDevice->SetTextureStageState( 0, D3DTSS_TEXTURETRANSFORMFLAGS, 
                                 D3DTTFF_COUNT2 );
	pD3DDevice->SetTextureStageState( 1, D3DTSS_TEXTURETRANSFORMFLAGS, 
                                 D3DTTFF_COUNT2 );
	pD3DDevice->SetTextureStageState( 2, D3DTSS_TEXTURETRANSFORMFLAGS, 
                                 D3DTTFF_COUNT2 );


    // Turn on ambient lighting
    //pD3DDevice->SetRenderState( D3DRS_AMBIENT, 0x1f1f1f1f );
	pD3DDevice->SetRenderState( D3DRS_AMBIENT, 0xffafafaf );

	//nice filtering
	pD3DDevice->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR );
    pD3DDevice->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR );
	pD3DDevice->SetSamplerState( 0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR );

	pD3DDevice->SetSamplerState( 1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR );
    pD3DDevice->SetSamplerState( 1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR );
	pD3DDevice->SetSamplerState( 1, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR );

	pD3DDevice->SetSamplerState( 2, D3DSAMP_MINFILTER, D3DTEXF_LINEAR );
    pD3DDevice->SetSamplerState( 2, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR );
	pD3DDevice->SetSamplerState( 2, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR );

	//mipmapping //higher is less quality closer
//	pD3DDevice->SetSamplerState(0, D3DSAMP_MIPMAPLODBIAS, -3);

	/*
	//set up some fog
    FLOAT fFogStart =  500.0f;
    FLOAT fFogEnd   = 1000.0f;

	unsigned long fogColor = 0x00000080;

    pD3DDevice->SetRenderState( D3DRS_FOGENABLE,      TRUE );
    pD3DDevice->SetRenderState( D3DRS_FOGCOLOR,       fogColor );
    pD3DDevice->SetRenderState( D3DRS_FOGTABLEMODE,   D3DFOG_NONE );
    pD3DDevice->SetRenderState( D3DRS_FOGVERTEXMODE,  D3DFOG_LINEAR );
    pD3DDevice->SetRenderState( D3DRS_RANGEFOGENABLE, FALSE );
    pD3DDevice->SetRenderState( D3DRS_FOGSTART,       (DWORD)fFogStart );
    pD3DDevice->SetRenderState( D3DRS_FOGEND,         (DWORD)fFogEnd );
	*/


	//make sure we are opaque
	SetOpaque();

	SetWireFrame(false);

	BeginScene(true);

	LPD3DXFONT pFont;
	RECT fontPosition = {0,0,800,600};;
	LOGFONT font = {18,0,0,0,FW_NORMAL,false,false,false,DEFAULT_CHARSET,OUT_TT_PRECIS,CLIP_DEFAULT_PRECIS,PROOF_QUALITY,DEFAULT_PITCH,"Arial"};
	D3DXCreateFontIndirect( GetDevice(),&font,&pFont);
	pFont->DrawText( "...Loading...", -1, &fontPosition, DT_CENTER , 0xffffffff);

	EndScene();

	//SET UP A STANDARD MATERIAL
	D3DMATERIAL9 standardMat;

	D3DCOLORVALUE color;
	color.r = 0.5;
	color.g = 0.5;
	color.b = 0.5;
	color.a = 0.5;

	standardMat.Diffuse = color; //D3DCOLORVALUE(100,100,100, 100);
	standardMat.Ambient = color; //D3DCOLORVALUE(100,100,100, 100);
	standardMat.Specular = color; //D3DCOLORVALUE(100,100,100, 100);
	standardMat.Emissive = color; //D3DCOLORVALUE(100,100,100, 100);
	standardMat.Power = 0.5;
	GetDevice()->SetMaterial( &standardMat );


	/*
    D3DXMATRIX matProj;
    D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/3, 0.1f, 1.0f, 10000.0f );
    pD3DDevice->SetTransform( D3DTS_PROJECTION, &matProj);
	*/

	pD3DDevice->SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );
}

bool Direct3D::CheckFormat(D3DFORMAT adapterFormat, D3DFORMAT depthFormat)
{
	HRESULT hr;
 
     // verify that the depth format exists
     hr = pD3D->CheckDeviceFormat(D3DADAPTER_DEFAULT,
                                        D3DDEVTYPE_HAL,
                                        adapterFormat,
                                        D3DUSAGE_DEPTHSTENCIL,
                                        D3DRTYPE_SURFACE,
                                        depthFormat);

	return SUCCEEDED(hr);
}

void Direct3D::BeginScene( bool clearTarget )
{
	if( bWireFrame || clearTarget )
		GetDevice()->Clear(0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);	
	else
		GetDevice()->Clear(0, NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 200, 0), 1.0f, 0);

//	GetDevice()->SetRenderState( D3DRS_ZFUNC, D3DCMP_ALWAYS );
		
	GetDevice()->BeginScene();
}

void Direct3D::EndScene()
{
	GetDevice()->EndScene();
	GetDevice()->Present(NULL, NULL, NULL, NULL);
}

void Direct3D::SetWireFrame( bool bWireFrame )
{
	this->bWireFrame = bWireFrame;

	if( bWireFrame )
		GetDevice()->SetRenderState(D3DRS_FILLMODE,D3DFILL_WIREFRAME);
	else
		GetDevice()->SetRenderState(D3DRS_FILLMODE,D3DFILL_SOLID);
}

bool Direct3D::GetWireFrame()
{
	return bWireFrame;
}

LPDIRECT3DDEVICE9 Direct3D::GetDevice()
{
	return pD3DDevice;
}

Direct3D::~Direct3D()
{
	SAFE_RELEASE( pD3DDevice );
    SAFE_RELEASE( pD3D );

	ShowCursor(true);
}


void Direct3D::SetTransparent()
{
	//Enable alpha blending so we can use transparent textures
	pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pD3DDevice->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
}

void Direct3D::SetOpaque()
{
	//Enable alpha blending so we can use transparent textures
	pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); //FALSE
	pD3DDevice->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
}
