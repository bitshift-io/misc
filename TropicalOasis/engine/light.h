#ifndef _SIPHON_LIGHT_
#define _SIPHON_LIGHT_

/*! \class Light
 *  \brief Lighting
 * 
 * This will probably be replaced purely by shader based
 * lights - should probably extend object
 */
class Light
{
public:
	Light();
	~Light();

	D3DLIGHT9* GetLight();

	void ResetLight( Direct3D* pD3D );

	void Update(float deltaTime);
private:
	bool bDestroy;
	D3DLIGHT9 light;

	unsigned int lightIndex;

	static int lightCount;
};

#endif