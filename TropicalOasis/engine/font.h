#ifndef _SIPHON_FONT_
#define _SIPHON_FONT_

class Direct3D;

/*! \class Font
 *  \brief GDI font class
 *
 * GDI font class, deals with creating a font
 * and drawing text to the screen
 */
class Font
{
public:
	Font( Direct3D* pD3D );
	Font(int size, std::string font, Direct3D* pD3D );
	~Font();

	DrawText(std::string text, int x, int y, D3DCOLOR color);

	LPD3DXFONT	pFont;
};

#endif