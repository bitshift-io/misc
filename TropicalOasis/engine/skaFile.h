#ifndef _SIPHON_SKAFILE_
#define _SIPHON_SKAFILE_

#define SKA_END "*end"
#define SKA_BONE "*bone"
#define SKA_KEYFRAME "*keyframe"
#define SKA_ANIMATION "*animation"
#define SKA_ROTATION "*rotation"
#define SKA_POSITION "*position"
#define SKA_SCALE "*scale"


struct SKABone
{
	int id;
	std::vector<KeyFrame> keyframes;
};

/*! \class SKAFile
 *  \brief Loads skeletal animations
 *
 * Loads skeletal animations
 */
class SKAFile
{
public:
	SKAFile();

	bool LoadAnimation( std::string modelPath, std::string fileName );
	bool LoadIntoObject( Object* obj );
	//bool LoadIntoMesh( SkeletalMesh* mesh );
protected:
	void ReadBone();
	void ReadKeyFrame( SKABone& bone );

	std::vector<SKABone> bones;

	FILE* pFile;
};

#endif