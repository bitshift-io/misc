#include "stdafx.h"
#include "objectManager.h"

ObjectManager::ObjectManager()
{
}

ObjectManager::~ObjectManager()
{
	//delete objects?
	std::list<Object*>::iterator objIt;

	for(objIt = objectList.begin(); objIt != objectList.end(); objIt++)
	{
		SAFE_DELETE( (*objIt) );
	}

	for(objIt = permObjectList.begin(); objIt != permObjectList.end(); objIt++)
	{
		SAFE_DELETE( (*objIt) );
	}
}

bool ObjectManager::AddObject(Object* newObject, bool alwaysRender )
{
	if( !alwaysRender )
		objectList.push_front(newObject);
	else
		permObjectList.push_front(newObject);

	return true;
}

void ObjectManager::Update( float deltaTime, World* pW, DirectInput* pDI )
{
	std::list<Object*>::iterator objIt;

	for(objIt = objectList.begin(); objIt != objectList.end(); objIt++)
	{
		(*objIt)->Update( deltaTime, pW, pDI );
		
		//if( (*objIt)->Destroy() )
		//	objectList.erase(objIt);
	}

	for(objIt = permObjectList.begin(); objIt != permObjectList.end(); objIt++)
	{
		(*objIt)->Update(deltaTime, pW, pDI );
	}
}

void ObjectManager::PreRender( RenderManager* pRM, BOUNDS* cameraBounds )
{
	std::list<Object*>::iterator objIt;

	for(objIt = permObjectList.begin(); objIt != permObjectList.end(); objIt++)
	{
		(*objIt)->PreRender( pRM, cameraBounds, true );
	}
}
