#include "stdafx.h"

SkeletalModel::SkeletalModel(FileManager* pFM, Direct3D* pD3D )
{
	Model::Model();

	LoadModel(pFM, "bones.A3D");

	SetEffect( pFM->GetEffect("vertexSkin.fx") );

	if( pEffect == 0 )
		exit(0);

	SetScale(5);

	pVertexDeclaration = 0;

	/*
	D3DVERTEXELEMENT9 decl[] =
    {
        { 0, 0,  D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,     0 },//size =12
        { 0, 12, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,       0 },//size =12
        { 0, 24, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,     0 },//size =12
        { 0, 40, D3DDECLTYPE_FLOAT4,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0 },//size=16
        { 0, 56, D3DDECLTYPE_FLOAT4,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT,  0 },//size=16
		//{ 0, 72, D3DDECLTYPE_FLOAT4,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,  0 },//size=16
        D3DDECL_END()
    };*/

	D3DVERTEXELEMENT9 decl[] = 
	{ 
		{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },//size =12 
		{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },//size =12 
		{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },//size =8 
		{ 0, 32, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0 },//size=16 
		{ 0, 48, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0 },//size=16 
		D3DDECL_END() 
	}; 


    /*
    D3DVERTEXELEMENT9 veDeclNorm[] =
	        {
		        { 0, 0,  D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
		        { 0, 12, D3DDECLTYPE_FLOAT1,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0 },
		        { 0, 16, D3DDECLTYPE_UBYTE4,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0 },
		        { 0, 20, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
		        { 0, 32, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
		        D3DDECL_END()
        };
    */

	HRESULT hr;
	if( FAILED( hr = pD3D->GetDevice()->CreateVertexDeclaration( decl, &pVertexDeclaration ) ) )
    {
		DXTRACE_ERR( "CreateVertexDeclaration", hr );
    }
}

int SkeletalModel::Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT setEffect, TEXTURE* pTexture )
{
	HRESULT hr;

	if( FAILED( hr = pEffect->SetTechnique( "Technique0" ) ) )
	{
		DXTRACE_ERR( "SetTechnique sk4", hr );
		return 0;
	}

//	DXTRACE_ERR( "we made it here", 0 );

	pD3D->GetDevice()->SetFVF( NULL ); //VERT_BONE::FVF );
	pD3D->GetDevice()->SetVertexDeclaration( pVertexDeclaration );

	Transform();
	D3DXMATRIX objMat;// = *Transform();

	D3DXMATRIX worldViewProjection = matWorld * pCamera->matView * pCamera->matProjection;
	//D3DXMatrixTranspose( &worldViewProjection, &worldViewProjection );

	//D3DXMatrixIdentity( &worldViewProjection );

	D3DXMATRIX arrays[24];
	for(int i=0; i < 24; i++)
	{
		D3DXMatrixIdentity( &arrays[i] );

		//Log("%f", (float)(rand()%100)/100);
		//D3DXMatrixTranslation( &arrays[i], (float)(rand()%100)/100, (float)(rand()%100)/100, (float)(rand()%100)/100);
		//arrays[i]._14 =
	}

	if( FAILED(hr = pEffect->SetMatrixArray( "boneMatrices", arrays, 24 ) ) )
	{
		DXTRACE_ERR( "SetMatrixArray", hr );
		return 0;
	}

	if( FAILED(hr = pEffect->SetMatrix( "worldViewProj", &worldViewProjection ) ) )
	{
		DXTRACE_ERR( "SetMatrix", hr );
		return 0;
	}

	if( FAILED( hr = pEffect->SetTexture( "testTexture",  pTexture->pTexture) ) )
	{
		DXTRACE_ERR( "SetTexture", hr );
		return 0;
	}
/*
	if( setMesh->bDoubleSided )
		pD3D->GetDevice()->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
*/
	UINT uPasses;
	if( FAILED( hr = pEffect->Begin( &uPasses, 0 ) ))
	{
		DXTRACE_ERR( "Begin", hr );
		return 0;
	}

	for( UINT uPass = 0; uPass < uPasses; uPass++ )
	{
		if( FAILED( hr = pEffect->Pass( uPass ) ) )
		{
			DXTRACE_ERR( "Pass", hr );
			return 0;
		}

		if (FAILED(hr = pD3D->GetDevice()->DrawIndexedPrimitive( setMesh->renderType,
						  0, 0, setMesh->numOfVertices ,
						  0,  setMesh->numOfPolygons )))
		{
			DXTRACE_ERR( "DrawIndexedPrimitive", hr );
			return 0;
		}

	}

	if( FAILED( hr = pEffect->End() ))
	{
		DXTRACE_ERR( "End", hr );
		return 0;
	}
/*
	if( setMesh->bDoubleSided )
		pD3D->GetDevice()->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
*/
	return setMesh->numOfPolygons;
}

/*
D3DXMATRIXA16* Object::Transform()
{
	D3DXMATRIXA16 matRotation;
	D3DXMATRIXA16 matTranslation;
	D3DXMATRIXA16 matScale;

	D3DXMatrixIdentity( &matWorld );

	D3DXMatrixRotationYawPitchRoll( &matRotation, rotation.x, rotation.y, rotation.z);
	D3DXMatrixTranslation( &matTranslation, position.x, position.y, position.z);

	D3DXMatrixScaling( &matScale, scale.x, scale.y, scale.z);

	D3DXMatrixMultiply( &matWorld, &matRotation, &matScale);
	D3DXMatrixMultiply( &matWorld, &matWorld, &matTranslation );

	return &matWorld;
}*/