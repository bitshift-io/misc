#ifndef _SIPHON_PARTICLESYSTEM_
#define _SIPHON_PARTICLESYSTEM_

/*! \class ParticleSystem
 *  \brief For particle effects
 *
 * This is a group of point sprite particles
 * a collection of similar particles
 */
class ParticleSystem : public Model
{
public:
	ParticleSystem( FileManager* pFM, Direct3D* pD3D );
	~ParticleSystem();

	
	int Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT pEffect, TEXTURE* pTexture );

};

#endif