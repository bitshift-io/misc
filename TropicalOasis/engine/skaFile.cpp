#include "stdafx.h"
#include "skaFile.h"

SKAFile::SKAFile()
{
	pFile = 0;
}
/*
bool SKAFile::LoadIntoMesh( SkeletalMesh* mesh )
{
	//loop through all our bones
	std::vector<SKABone>::iterator boneIt;

	int animNo = mesh->GetNumAnimations();

	for( boneIt = bones.begin(); boneIt != bones.end(); boneIt++ )
	{
		Bone* b = mesh->GetBone( boneIt->id );

		//loop through all our key frames
		std::vector<KeyFrame>::iterator keyIt;

//cos*		Engine::GetInstance().GetConsole().Log("no keys: %i", boneIt->keyframes.size());
		for( keyIt = boneIt->keyframes.begin(); keyIt != boneIt->keyframes.end(); keyIt++ )
		{
			b->AddKeyFrame( animNo, (*keyIt) );	
		}
	}

	return true;
}*/

bool SKAFile::LoadIntoObject( Object* obj )
{
	//loop through all our bones
	std::vector<SKABone>::iterator boneIt;
	std::vector<KeyFrame>::iterator keyIt;
	
	int animId = obj->GetNumAnimations();
	for( boneIt = bones.begin(); boneIt != bones.end(); boneIt++ )
	{
		for( keyIt = boneIt->keyframes.begin(); keyIt != boneIt->keyframes.end(); keyIt++)
		{
			//obj->AddKeyFrame( boneIt->id, *keyIt );
			obj->AddKeyFrame( animId, *keyIt );
		}
	}

	return true;
}

bool SKAFile::LoadAnimation( std::string modelPath, std::string fileName )
{
//cos*	Engine::GetInstance().GetConsole().Log("loading animation");

	std::string actualPath = modelPath + fileName;
	pFile = fopen(actualPath.c_str(), "r");
	if( !pFile )
		return false;

	while( !feof(pFile) )
	{
		char strWord[255] = {0};
		fscanf(pFile, "%s", &strWord );

		if( strcmp( strWord, SKA_BONE ) == 0 )
			ReadBone();
	}
	
	fclose( pFile );
	return true;
}

void SKAFile::ReadBone()
{
	SKABone bone;
	fscanf(pFile, "%i", &bone.id );

	int num = 0;
	char strWord[255] = {0};

	while( strcmp( strWord, SKA_END ) != 0 && !feof(pFile) )
	{
		fscanf(pFile, "%s", &strWord );

		if( strcmp( strWord, SKA_KEYFRAME ) == 0 )
		{
			ReadKeyFrame( bone );
		}
	}

	bones.push_back( bone );
}

void SKAFile::ReadKeyFrame( SKABone& bone )
{
	KeyFrame kFrame;

	fscanf(pFile, "%i", &kFrame.time );

	char strWord[255] = {0};
	while( strcmp( strWord, SKA_END ) != 0 && !feof(pFile) )
	{		
		fscanf(pFile, "%s", &strWord );
		
		if( strcmp( strWord, SKA_ROTATION ) == 0 )	
		{
			fscanf(pFile, "%f %f %f", &kFrame.rotation.x, &kFrame.rotation.y, &kFrame.rotation.z);
		}
		if( strcmp( strWord, SKA_POSITION ) == 0 )	
		{
			fscanf(pFile, "%f %f %f", &kFrame.position.x, &kFrame.position.y, &kFrame.position.z);
		}
	}

	bone.keyframes.push_back( kFrame );
}