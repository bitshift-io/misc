Tokamak Game Physics SDK Version 1.0.10
--------------------------------------

Please refer to license.txt in this distribution for license
agreement.

History
-------

November 8 2003 (1.0.10)
	This distribution include Tokamak in DLL format. 
		To use TokamakDll.dll, include the header file
		Tokamak.h, and define TOKAMAK_USE_DLL in your
		compiler setting, and link TokamakDll.lib.

	Modify parameter to IDirect3D8::GetAdapterIdentifier so that
		it doesn't try to connect to internet. This is in
		the sample framework only.

October 27 2003 (1.0.9)
	New Feature
	- Custom Collision Detection Callback functions
		neCustomCDRB2RBCallback and
		neCustomCDRB2ABCallback

	Bug fixes
	- GetTransform now return D3d and Opengl binary 
	  compatible matrix.

	- Fix bug where neTerrainTriangleQueryCallback
	  would not work with neSensor.

September 21 2003 (1.0.8)
	Bug fixes
	- Fixed collision detection bug with terrain.
	- Fixed sphere to box collision bug which occasionally 
	  send the raddude flying off.
	- Disable pre-compiled header in all libraries and samples
	  files, as same users reports problems.

September 15 2003 (1.0.7)
	New Features
	- Motor for hinge joint.
	- Improved constraint solver.
	- Improved sleeping logic
	- Reduce boxes resting on it's edge.
	- Add new parameter (neRigidBody *) to neTerrainTriangleQueryCallback

	Bug fixes
	- Joint Controller has no effect.
	- neJoint::Enable(0) has no effect.
	- neJoint::Enable(1) freeze the simulator.
	- Sphere vs Box collision bug.
	- Cylinder vs Box collision bug.
	- World with Z-up instability.

August 03 2003 (1.0.6)
	- Terrain Mesh callback function allow user to supply the set of
	  potentially collidable triangles.
	- Fixed terrain mesh collision fall through bug.
	- Fixed allocating too many rigid bodies crash bug.
	- Fixed freeing rigid bodies not freeing up space bug.
	- Manual breaking of geometry. (See BreakGeometry())
	- ApplyImpulseWithTwist now renamed to ApplyImpulse
	- SetForceWithTorque now renamed to SetForce

June 10 2003 (1.0.5)
	- Fixed sensor not detecting terrain bug
	- Fixed sensor return NULL for detected body
	- Include multithreaded lib (in separate download). 
		These are tokamak_m.lib for release multithreaded and
		tokamak_md.lib for debug multithreaded.

May 27 2003 (1.0.4)
	- Fixed particle not sleeping bugs
	- Fixed jointed objects penetrating non-jointed objects bugs
	- Some new set of demos.

May 25 2003 (1.0.3)
	- Improve sleeping behaviour for jointed rigid bodies.
	- Improve speed of framework renderer (by alot!).
	- Slight change some of the demos.
	- Fixed various internal bugs.

May 18 2003 (1.0.2)
	- tokamak.h change include "ne_math.h" to "math/ne_math.h", so now you
	  only need one include path for Tokamak.
	- change s32 from long to int
	- prefix PI, ZERO with NE_
	- Added neJoint::SetDampingFactor and neJoint::GetDampingFactor. The joint 
	  damping factor specify the value to damp the relative velocity of a joint.
	  Valid values are >= 0. Default is 0.

May 15 2003 (1.0.1)
	- Fixed neRigidBody joint to neAnimatedBody crash.


Directory
---------

(root) 		- Visual Studio 6 Workspace for all samples
|
|- common	- Common application frame work code for all the samples
|
|- documentation- documenation for Tokamak
|
|- include 	- Tokamak inlcude files
|
|- lib 		- Tokamak lib files
|
|- samples 	- Sample demo programs


Notes
-----

Tokamak Headers and Lib
-----------------------
The only headers you require to integrate Tokamak into your application
are under the (root)/include directory. That is, TOKAMAK.H and all the maths 
and inline maths headers. The only library you need is TOKAMAK.LIB 
under the (root)/lib. 

tokamak.lib single threaded version
tokamak_d.lib same as tokamak.lib.
tokamak_m.lib is multithreaded version.
tokamak_md.lib same as tokamak_m.lib.

Sample Framework
----------------
The sample programs are inherited from the sample framework, but the sample
framework is not part of the Tokamak library. To see how to use Tokamak, 
study the sample1-sample10.cpp and the samplebase.cpp.



