#ifndef _SIPHON_INIFILE_
#define _SIPHON_INIFILE_

//temporary
void Log(const char *lpszText, ...);

class INIFile
{
public:
	INIFile( std::string path );
	~INIFile();

	std::string GetValue( std::string section, std::string key );

	//overloaded stuffs
	/*
	void GetValue( std::string section, std::string key, std::string& value );
	void GetValue( std::string section, std::string key, float& value  );
	void GetValue( std::string section, std::string key, int& value  );
	void GetValue( std::string section, std::string key, bool& value  );*/

	void SetValue( std::string section, std::string key, std::string value );
	/*
	void SetValue( std::string section, std::string key, float value );
	void SetValue( std::string section, std::string key, int value );
	void SetValue( std::string section, std::string key, bool value );*/

protected:
	std::map<std::string, std::map<std::string, std::string> > data;
	std::string path;
};

#endif