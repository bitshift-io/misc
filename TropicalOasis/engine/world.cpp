#include "stdafx.h"

World::World( Direct3D* pD3D, FileManager* pFM, std::string mapPath, std::string meshPath )
{
	this->pD3D = pD3D;
	this->pFM = pFM;
	this->mapPath = mapPath;
	this->meshPath = meshPath;

	LoadMap("oasis.M3D");

	sky.LoadModel(pFM, "sky.A3D");
	//sky.LoadModel(pFM, "sky.A3D");
	//MESH* t;
	//sky.SetMesh( pFM->GetMesh("sky.A3D", t) );
	sky.SetScale( 25 );
}

World::~World()
{
	UnloadMap();
}

int World::GetNumberCollisionCells()
{
	return collisionCells.size();
}

CollisionCell* World::GetCollisionCell( int no )
{
	return &collisionCells[no];
}

bool World::LoadMap( std::string file )
{
	M3DFile map( pD3D, pFM, file, mapPath, meshPath );
	renderCells.resize( map.GetNumberCells() );
	
	for( int i=0; i < map.GetNumberCells(); i++ )
	{
		map.LoadIntoModel( renderCells[i], i);

		/*
		MESH* m = new MESH();
		renderCells[i].SetMesh(m);*/
		renderCells[i].SetTexture( pFM->GetTexture("terrainmap.dds") );
		//renderCells[i].pBlendMap = pFM->GetTexture("rock_alpha.dds");
		renderCells[i].pLightMap = pFM->GetTexture("detail2.dds"); //grass.dds
		//map.LoadIntoMesh( (renderCells[i].GetMesh() ), i );
	}

	rows = sqrt( map.GetNumberCells() );
	cols = sqrt( map.GetNumberCells() );
	cellGap = map.GetCellDimensions();

	startValue = map.GetStartValue();

	colRows = sqrt( map.GetNumberCollisionCells() );
	colCols = sqrt( map.GetNumberCollisionCells() );
	colCellGap = map.GetCollisionCellDimensions();

	collisionCells.resize( map.GetNumberCollisionCells() );

	for( i=0; i < map.GetNumberCollisionCells(); i++ )
	{
		map.LoadIntoCollisionMesh( &(collisionCells[i].terrain), i );
	}

	//load statics
	for( i=0; i < map.GetNumStatics(); i++ )
	{
		Model* m = new Model();
		map.LoadStatics(m, i);
		AddStatic(m);
	}

	water.LoadWater( pD3D, pFM, "water.dds", startValue );
	//water.pSkyReflection = pFM->GetTexture("skyReflection.dds");

	return true;
}

void World::UnloadMap()
{
	std::vector<RenderCell>::iterator rIt; // renderCells;

	for( rIt = renderCells.begin(); rIt != renderCells.end(); rIt++ )
	{
		MESH* m = rIt->GetMesh();

		SAFE_RELEASE( m->pIndexBuffer );
		SAFE_RELEASE( m->pVertexBuffer );
		SAFE_DELETE( m );
	}
}

bool World::AddObject(Object* pObject)
{
	//this stops the player leaving the edge of the map
	float safteyCatch = colCellGap/2;
	if( pObject->position.z - safteyCatch < startValue )
	{
		pObject->position.z = startValue + safteyCatch;
		pObject->Collision(0, D3DXVECTOR3(0,0,1));
	}
	if( pObject->position.z + safteyCatch > -startValue )
	{
		pObject->position.z = -startValue - safteyCatch;
		pObject->Collision(0, D3DXVECTOR3(0,0,-1));
	}

	if( pObject->position.x - safteyCatch < startValue )
	{
		pObject->position.x = startValue + safteyCatch;
		pObject->Collision(0, D3DXVECTOR3(1,0,0));
	}
	if( pObject->position.x + safteyCatch > -startValue )
	{
		pObject->position.x = -startValue - safteyCatch;
		pObject->Collision(0, D3DXVECTOR3(-1,0,0));
	}

	//place into render cells
	int z = (pObject->position.z - startValue)/cellGap;
	int x = (pObject->position.x - startValue)/cellGap;

	renderCells[ ((x * rows)+z) ].contains.push_back(pObject);

	//and place into collision cells
	int cz = (pObject->position.z - startValue)/colCellGap;
	int cx = (pObject->position.x - startValue)/colCellGap;

	collisionCells[ ((cx * colRows)+cz) ].contains.push_back(pObject);

	return true;
}

bool World::AddStatic(Object* pObject)
{
	//this stops the player leaving the edge of the map
	float safteyCatch = colCellGap/2;
	if( pObject->position.z - safteyCatch < startValue )
	{
		pObject->position.z = startValue + safteyCatch;
		pObject->Collision(0, D3DXVECTOR3(0,0,1));
	}
	if( pObject->position.z + safteyCatch > -startValue )
	{
		pObject->position.z = -startValue - safteyCatch;
		pObject->Collision(0, D3DXVECTOR3(0,0,-1));
	}

	if( pObject->position.x - safteyCatch < startValue )
	{
		pObject->position.x = startValue + safteyCatch;
		pObject->Collision(0, D3DXVECTOR3(1,0,0));
	}
	if( pObject->position.x + safteyCatch > -startValue )
	{
		pObject->position.x = -startValue - safteyCatch;
		pObject->Collision(0, D3DXVECTOR3(-1,0,0));
	}

	//place into render cells
	int z = (pObject->position.z - startValue)/cellGap;
	int x = (pObject->position.x - startValue)/cellGap;

	renderCells[ ((x * rows)+z) ].statics.push_back(pObject);

	//and place into collision cells
	int cz = (pObject->position.z - startValue)/colCellGap;
	int cx = (pObject->position.x - startValue)/colCellGap;

//	collisionCells[ ((cx * colRows)+cz) ].statics.push_back(pObject);

	return true;
}

void World::PreRender( RenderManager* pRM, BOUNDS* cameraBounds )
{
	//if 0 is passed for camera bounds, render all cells
	if( cameraBounds == 0 )
	{
		std::vector<RenderCell>::iterator rIt;

		for( rIt = renderCells.begin(); rIt != renderCells.end(); rIt++ )
		{
			rIt->PreRender( pRM, cameraBounds );
		}
	}
	else
	{
		float posZ = cameraBounds->points[0].z, negZ = cameraBounds->points[0].z;
		float posX = cameraBounds->points[0].x, negX = cameraBounds->points[0].x;

		for(int i=0; i < 8; i++)
		{
			//Log("Z: %f X: %f", cameraBounds->points[i].z, cameraBounds->points[i].x );

			if( cameraBounds->points[i].z > posZ )
				posZ = cameraBounds->points[i].z;
			if( cameraBounds->points[i].z < negZ )
				negZ = cameraBounds->points[i].z;

			if( cameraBounds->points[i].x > posX )
				posX = cameraBounds->points[i].x;
			if( cameraBounds->points[i].x < negX )
				negX = cameraBounds->points[i].x;
		}

		//Log("posZ: %f negZ: %f, posX: %f negX: %f (startValue: %f cellGap: %f)", posZ, negZ, posX, negX, startValue, cellGap );

		for( int x=(negX - startValue)/cellGap; x <= (posX - startValue)/cellGap; x++)
		{
			for( int z=(negZ - startValue)/cellGap; z <= (posZ - startValue)/cellGap; z++)
			{
				//Log("we made it here: X: %i, Z:%i", x,z);
				if( x >= 0 && z >= 0 )						
				{
					if( (x <= rows-1 && z <= cols-1) )
					{
						//Log("rendering cell: %i", ((z * (rows))+x) );
						renderCells[ ((z * (rows))+x) ].PreRender( pRM, cameraBounds );
					}
				}
			}
		}
	}

	water.PreRender( pRM, cameraBounds );
}

void World::Update( float const deltaTime )
{
	std::vector<RenderCell>::iterator rIt;

	for( rIt = renderCells.begin(); rIt != renderCells.end(); rIt++ )
	{
		rIt->Update( deltaTime );
	}

	std::vector<CollisionCell>::iterator cIt;

	for( cIt = collisionCells.begin(); cIt != collisionCells.end(); cIt++ )
	{
		cIt->Update( deltaTime );
	}
}

int World::RenderSky( D3DXVECTOR3& position )
{
	pD3D->GetDevice()->SetRenderState( D3DRS_LIGHTING,     FALSE );
    pD3D->GetDevice()->SetRenderState( D3DRS_SRCBLEND,     D3DBLEND_ONE );
    pD3D->GetDevice()->SetRenderState( D3DRS_DESTBLEND,    D3DBLEND_ZERO );
    pD3D->GetDevice()->SetRenderState( D3DRS_ZENABLE,      FALSE );
    pD3D->GetDevice()->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
    pD3D->GetDevice()->SetRenderState( D3DRS_FOGENABLE,    FALSE );
   
	//render sky
	MESH* sMesh = 0;
	sMesh = sky.GetMesh();

	sky.SetPosition( position );
	pD3D->GetDevice()->SetTransform( D3DTS_WORLD, sky.Transform() );

	if( sMesh == 0 )
	{
		pD3D->GetDevice()->SetRenderState( D3DRS_LIGHTING,     TRUE );
		pD3D->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		pD3D->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		pD3D->GetDevice()->SetRenderState( D3DRS_ZENABLE,      TRUE );
		pD3D->GetDevice()->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
		pD3D->GetDevice()->SetRenderState( D3DRS_FOGENABLE,    FALSE );
		return 0;
	}

	pD3D->pD3DDevice->SetTexture(0, sky.GetTexture()->pTexture );
	pD3D->GetDevice()->SetFVF( sMesh->fvf );

	//render mesh												
	if (FAILED(pD3D->GetDevice()->SetIndices( sMesh->pIndexBuffer ) ))
	{
		pD3D->GetDevice()->SetRenderState( D3DRS_LIGHTING,     TRUE );
		pD3D->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		pD3D->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		pD3D->GetDevice()->SetRenderState( D3DRS_ZENABLE,      TRUE );
		pD3D->GetDevice()->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
		pD3D->GetDevice()->SetRenderState( D3DRS_FOGENABLE,    FALSE );
		return 0;
	}

	if (FAILED(pD3D->GetDevice()->SetStreamSource(0, sMesh->pVertexBuffer , 0, sMesh->vertexSize ) ))
	{
		pD3D->GetDevice()->SetRenderState( D3DRS_LIGHTING,     TRUE );
		pD3D->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		pD3D->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		pD3D->GetDevice()->SetRenderState( D3DRS_ZENABLE,      TRUE );
		pD3D->GetDevice()->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
		pD3D->GetDevice()->SetRenderState( D3DRS_FOGENABLE,    FALSE );
		return 0;
	}

	if (FAILED(pD3D->GetDevice()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST,
												  0, 0, sMesh->numOfVertices ,
												  0,  sMesh->numOfPolygons )))
	{
		pD3D->GetDevice()->SetRenderState( D3DRS_LIGHTING,     TRUE );
		pD3D->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		pD3D->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		pD3D->GetDevice()->SetRenderState( D3DRS_ZENABLE,      TRUE );
		pD3D->GetDevice()->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
		pD3D->GetDevice()->SetRenderState( D3DRS_FOGENABLE,    FALSE );
		return 0;
	}	

	pD3D->GetDevice()->SetRenderState( D3DRS_LIGHTING,     TRUE );
	pD3D->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pD3D->GetDevice()->SetRenderState( D3DRS_ZENABLE,      TRUE );
	pD3D->GetDevice()->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
	pD3D->GetDevice()->SetRenderState( D3DRS_FOGENABLE,    FALSE );

	return sMesh->numOfPolygons;
}

RenderCell::RenderCell()
{
	//render cells dont have to register meshes with filemanager as each
	// mesh here is unique
//	MESH* m = new MESH();
//	SetMesh(m);

	pBlendMap = 0;
}

RenderCell::~RenderCell()
{
	std::list<Object*>::iterator oIt;

	for( oIt = statics.begin(); oIt != statics.end(); oIt++ )
	{
		SAFE_DELETE( *oIt );
	}
}

void RenderCell::PreRender( RenderManager* pRM, BOUNDS* cameraBounds )
{
	std::list<Object*>::iterator oIt;

	for( oIt = contains.begin(); oIt != contains.end(); oIt++ )
	{
		(*oIt)->PreRender( pRM, cameraBounds );
	}

	for( oIt = statics.begin(); oIt != statics.end(); oIt++ )
	{
		(*oIt)->PreRender( pRM, cameraBounds  );
	}

	pRM->PreRender( this );
}

int RenderCell::Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT setEffect, TEXTURE* setTexture )
{
	int polys = 0;

	pD3D->GetDevice()->SetTexture( 1, pLightMap->pTexture ); //this is my base texture
	pD3D->GetDevice()->SetTexture( 0, pTexture->pTexture ); //secondary trexture

	D3DXMATRIXA16 matScale;

	D3DXMATRIX matTrans1;
	D3DXMatrixIdentity( &matTrans1 );
	D3DXMatrixScaling( &matScale, 100, 100, 1);
	D3DXMatrixMultiply( &matTrans1, &matTrans1, &matScale);

	pD3D->GetDevice()->SetTransform( D3DTS_TEXTURE1, &matTrans1 );

	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_TEXCOORDINDEX, 0);
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG2 );
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );

	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_TEXCOORDINDEX, 0);
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_MODULATE );
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLORARG1, D3DTA_TEXTURE );
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLORARG2, D3DTA_CURRENT );
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1 );
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_ALPHAARG2, D3DTA_TEXTURE );
	//pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_RESULTARG, D3DTA_TEMP );

	/*
	pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_TEXCOORDINDEX, 0);
	pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_COLOROP,   D3DTOP_MODULATE );
	pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_COLORARG1, D3DTA_TEMP );
	pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_COLORARG2, D3DTA_CURRENT );
	pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1 );
	pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );*/
/*
	//detail map
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_TEXCOORDINDEX, 0);
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE ); 
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );   
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );   
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE );   
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );   
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );

	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_TEXCOORDINDEX, 0);   
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_MODULATE );   
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLORARG1, D3DTA_TEXTURE );    
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLORARG2, D3DTA_DIFFUSE);   
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);   
	//pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_RESULTARG, D3DTA_TEMP );
*/
	polys += Model::Render(pD3D, pCamera, setMesh, setEffect, setTexture);

	/* DO NOT DELETE!!!!!
	pD3D->GetDevice()->SetTexture( 0, pLightMap->pTexture ); //this is my base texture
	pD3D->GetDevice()->SetTexture( 1, pTexture->pTexture ); //secondary trexture
	pD3D->GetDevice()->SetTexture( 2, pBlendMap->pTexture ); //blend texture

	if( pTerrainTextures.size() > 0 )
	{
		D3DXMATRIXA16 matScale;

		D3DXMATRIX matTrans1;
		D3DXMatrixIdentity( &matTrans1 );
		D3DXMatrixScaling( &matScale, pTerrainTextures[0]->uTile, pTerrainTextures[0]->vTile, 1);
		D3DXMatrixMultiply( &matTrans1, &matTrans1, &matScale);

		pD3D->GetDevice()->SetTransform( D3DTS_TEXTURE0, &matTrans1 );

		pD3D->GetDevice()->SetTexture( 0, pTerrainTextures[0]->pTexture->pTexture );
	}

	if( pTerrainTextures.size() > 1 )
	{
		D3DXMATRIXA16 matScale;

		D3DXMATRIX matTrans1;
		D3DXMatrixIdentity( &matTrans1 );
		D3DXMatrixScaling( &matScale, pTerrainTextures[1]->uTile, pTerrainTextures[1]->vTile, 1);
		D3DXMatrixMultiply( &matTrans1, &matTrans1, &matScale);
		pD3D->GetDevice()->SetTransform( D3DTS_TEXTURE1, &matTrans1 );

		pD3D->GetDevice()->SetTexture( 1, pTerrainTextures[1]->pTexture->pTexture );
		pD3D->GetDevice()->SetTexture( 2, pTerrainTextures[1]->pBlendTexture->pTexture );
	}


	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_TEXCOORDINDEX, 0);
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE ); 
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );   
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );   
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE );   
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );   
	pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE ); 

	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_TEXCOORDINDEX, 0);   
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_MODULATE );   
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLORARG1, D3DTA_TEXTURE );    
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLORARG2, D3DTA_DIFFUSE);   
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_DISABLE);   
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_RESULTARG, D3DTA_TEMP );  

	pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_TEXCOORDINDEX, 0);   
	pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_COLOROP,   D3DTOP_BLENDTEXTUREALPHA );   
	pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_COLORARG1, D3DTA_TEMP  );   
	pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_COLORARG2, D3DTA_CURRENT  );   
	pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_ALPHAOP,   D3DTOP_DISABLE );

	polys += Model::Render(pD3D, pCamera, setMesh, setEffect, setTexture);
	*/


	/*
	if( pTerrainTextures.size() > 2 )
	{
		//pD3D->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_INVSRCALPHA);
		//pD3D->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_SRCALPHA);
		pD3D->GetDevice()->SetRenderState (D3DRS_ALPHABLENDENABLE, TRUE);

		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_TEXCOORDINDEX, 0);
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE ); 
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );   
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE );   
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );   
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );

		pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_TEXCOORDINDEX, 0);   
		pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_SELECTARG1 );   
		pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLORARG1, D3DTA_CURRENT );      
		pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1 );
		pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
 
		pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_COLOROP,   D3DTOP_DISABLE );
		pD3D->GetDevice()->SetTextureStageState( 2, D3DTSS_ALPHAOP,   D3DTOP_DISABLE );

		pD3D->GetDevice()->SetTexture( 0, pTerrainTextures[2]->pTexture->pTexture );
		pD3D->GetDevice()->SetTexture( 1, pBlendMap->pTexture );
		pD3D->GetDevice()->SetTexture( 0, pBlendMap->pTexture );
		pD3D->GetDevice()->SetTexture( 2, 0 );
	}

	polys += Model::Render(pD3D, pCamera, setMesh, setEffect, setTexture);

	pD3D->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pD3D->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pD3D->GetDevice()->SetRenderState (D3DRS_ALPHABLENDENABLE, FALSE);*/
/*
	D3DXMATRIXA16 matRestore;
	D3DXMatrixIdentity(&matRestore);
	pD3D->GetDevice()->SetTransform( D3DTS_TEXTURE0, &matRestore);
	pD3D->GetDevice()->SetTexture( 0, pTerrainTextures[1]->pBlendTexture->pTexture );
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_DISABLE ); 

	polys += Model::Render(pD3D, pCamera, setMesh, setEffect, setTexture);*/

	D3DXMATRIXA16 matRestore;
	D3DXMatrixIdentity(&matRestore);
	pD3D->GetDevice()->SetTransform( D3DTS_TEXTURE0, &matRestore);
	pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLOROP, D3DTOP_DISABLE );
	return polys;
}

void RenderCell::Update( float const deltaTime )
{
	contains.clear();
}

CollisionCell::CollisionCell()
{

}

CollisionCell::~CollisionCell()
{

}

void CollisionCell::Update( float const deltaTime )
{
	contains.clear();
}

Water::Water()
{
	Model::Model();
	pSkyReflection = 0;
	bAddToWorldOnUpdate = false;
};

Water::~Water()
{
	SAFE_RELEASE( pMesh->pIndexBuffer );
	SAFE_RELEASE( pMesh->pVertexBuffer );
	SAFE_DELETE( pMesh );
}

void Water::LoadWater(Direct3D* pD3D, FileManager* pFM, std::string texture, int size)
{
	pMesh = new MESH();

	WORD idx[] = { 3,2,1, 1,0,3 };  

	VERT_POS_NORM_TEX verts[] =
	{
		{ size,  0.0f, -size, 0.0f, 1.0f, 0.0f,  1.00, 0.00 }, // x, y, z, nx, ny, nz, tu, tv
		{ size,  0.0f, size, 0.0f, 1.0f, 0.0f,  1.00, 1.00  },
		{  -size, 0.0f, size, 0.0f, 1.0f, 0.0f,  0.00, 0.00  },
		{  -size, 0.0f, -size, 0.0f, 1.0f, 0.0f, 0.00, 1.0  },		
	};

	SetTexture( pFM->GetTexture( texture ) );
	pTexture->bTransparent = true;
	pMesh->bDoubleSided = true;

	pMesh->fvf = VERT_POS_NORM_TEX::FVF;
	pMesh->vertexSize = sizeof( VERT_POS_NORM_TEX );
	pMesh->numOfIndices = 6;
	pMesh->numOfPolygons = 2;
	pMesh->numOfVertices = 4;

	if( FAILED( pD3D->GetDevice()->CreateIndexBuffer( sizeof(WORD)*2*3, D3DUSAGE_WRITEONLY,
    D3DFMT_INDEX16, D3DPOOL_DEFAULT, &pMesh->pIndexBuffer, 0) ))
	{
		return;
	}

	WORD* index;
	if( FAILED( pMesh->pIndexBuffer->Lock(0, 0, (void**)&index, 0) ))
	{
		return;
	}

	for( int n = 0; n < 2*3; n++ )
	{
		index[n] = idx[n];
	}

	pMesh->pIndexBuffer->Unlock();


	if( FAILED( pD3D->GetDevice()->CreateVertexBuffer( sizeof( VERT_POS_NORM_TEX ) * 4, D3DUSAGE_WRITEONLY,
    VERT_POS_NORM_TEX::FVF, D3DPOOL_DEFAULT, &pMesh->pVertexBuffer, 0) ) )
	{	
		return;
	}

	VERT_POS_NORM_TEX* vertex;	
	if( FAILED( pMesh->pVertexBuffer->Lock(0, 0, (void**)&vertex, 0) ))
	{
		return;
	}

	for( int v = 0; v < 4; v++ )
	{
		vertex[v] = verts[v];
	}

	pMesh->pVertexBuffer->Unlock();
}

int Water::Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT pSetEffect, TEXTURE* pTexture )
{
	int polys = 0;

	D3DXMATRIXA16 matScale;
	D3DXMATRIX matTrans;

	position.x = cos( (float)( (float)timeGetTime() / (float)2000) ) / 100.0f;//+= 0.001;
	position.z = cos( (float)( (float)timeGetTime() / (float)2000) ) / 100.0f;

	D3DXMatrixIdentity( &matTrans );
	D3DXMatrixScaling( &matScale, 10, 10, 1);
	D3DXMatrixMultiply( &matTrans, &matTrans, &matScale);

	matTrans._31 = position.z;
	matTrans._32 = position.x;

	pD3D->GetDevice()->SetTransform( D3DTS_TEXTURE0, &matTrans );

	//pD3D->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	//pD3D->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	pD3D->GetDevice()->SetRenderState (D3DRS_ALPHABLENDENABLE, TRUE);



/*
	if( !pSkyReflection )
	{
		D3DXMATRIXA16 matScale;
		D3DXMATRIX matTrans;
		D3DXMatrixIdentity( &matTrans );
		D3DXMatrixScaling( &matScale, 10, 10, 1);
		D3DXMatrixTranslation( &matTrans, pCamera->position.x, pCamera->position.y, pCamera->position.z);
		D3DXMatrixMultiply( &matTrans, &matTrans, &matScale);

		pD3D->GetDevice()->SetTransform( D3DTS_TEXTURE0, &matTrans );
		pD3D->GetDevice()->SetTexture( 0, pSkyReflection->pTexture );

		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_TEXCOORDINDEX, 0);
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE ); 
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );   
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );   
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE );   
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );   
		pD3D->GetDevice()->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE ); 

		pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_TEXCOORDINDEX, 0);   
		pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLOROP,   D3DTOP_BLENDTEXTUREALPHA );   
		pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLORARG1, D3DTA_TEXTURE );    
		pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_COLORARG2, D3DTA_CURRENT);   
		pD3D->GetDevice()->SetTextureStageState( 1, D3DTSS_ALPHAOP,   D3DTOP_DISABLE); 
	}*/

	polys += Model::Render( pD3D, pCamera, setMesh, pEffect, pTexture);

	pD3D->GetDevice()->SetRenderState (D3DRS_ALPHABLENDENABLE, FALSE);

	D3DXMatrixIdentity( &matTrans );
	pD3D->GetDevice()->SetTransform( D3DTS_TEXTURE0, &matTrans );

	return polys;
}

