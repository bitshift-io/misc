#ifndef _SIPHON_PANEL_
#define _SIPHON_PANEL_



/*! \class Panel
 *  \brief 2D objects
 *
 * 2D HUD elements
 */
class Panel : public Model
{
public:
	Panel();
	~Panel();

	virtual bool IsA( OBJECT_TYPE type );
	void CreatePanel( FileManager* pFM, Direct3D* pD3D, float width, float height, float x, float y, std::string texture);
};

#endif