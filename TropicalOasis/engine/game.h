#ifndef _SIPHON_GAME_
#define _SIPHON_GAME_

class Engine;

/*! \class Game
 *	\brief A class for game modes
 *
 * This class contains code for the current game mode
 */
class Game
{
public:
	Game( Engine* pEngine );
	~Game();

	/**
	 * Called each frame to do game related stuff
	 */
	Update( float const deltaTime, Engine* pEngine );
};

#endif