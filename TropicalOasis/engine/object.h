/* ====================================
	OBJECT
All things are objects, everything is an 
object, everything must inherit from this,
They can be rendered or not
==================================== */
#ifndef _SIPHON_OBJECT_
#define _SIPHON_OBJECT_

#define CLASS_OBJECT 0
#define CLASS_MESH 1
#define CLASS_SKELETALMESH 2
#define CLASS_BALL 3
#define CLASS_PANEL 4
#define CLASS_CAMERA 5

enum OBJECT_TYPE
{ 
    OBJ_OBJECT, 
    OBJ_MODEL, 
    OBJ_CAMERA,
    OBJ_PANEL,
    OBJ_SKELETALMESH,
};

struct MESH;
class RenderManager;
class World;

/*! \class KeyFrame
 *  \brief A key animations frame
 *
 * A key frame used for animations
 */
class KeyFrame
{
public:
	long int time;

	D3DXVECTOR3 position;
	D3DXVECTOR3 rotation;
	D3DXVECTOR3 scale;

	KeyFrame()
	{
		time = 0;
		position = D3DXVECTOR3(0,0,0);
		rotation = D3DXVECTOR3(0,0,0);
		scale = D3DXVECTOR3(1,1,1);
	}

	KeyFrame(D3DXVECTOR3 position, D3DXVECTOR3 rotation, long int time)
	{
		this->time = time;
		this->position = position;
		this->rotation = rotation;
	}
};

typedef std::vector< std::vector<KeyFrame> > AnimationData;
class Camera;
struct BOUNDS;

/*! \class Object
 *  \brief The basis of all world objects
 *
 * The basis of all world objects, and object
 * that can be placed in the world, can be rendered
 * or uses a position
 * objects also have the feature of being able to use keyframes
 * to set thier position/rotation/scale and in future will be used
 * to record demos
 */
class Object
{
public:
	Object( const OBJECT_TYPE objType = OBJ_OBJECT );
	virtual ~Object();

	virtual void Update( float const deltaTime, World* pW, DirectInput* pDI ); //virtual 

	/**
	 * This function allows us to que up the models
	 * that need to be rendered
	 */
	virtual void PreRender( RenderManager* pRM, BOUNDS* cameraBounds, bool alwaysRendered = false ); //virtual 

	/**
	 * this function is called to render the primitives
	 * this gives the model a chance to set up some special
	 * effects or what ever it needs to
	 * as well as restoring them after
	 */
	virtual int Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT pEffect, LPDIRECT3DTEXTURE9 pTexture );
	
	bool Destroy();

	void SetPosition(D3DXVECTOR3 position);
	void SetPosition(float x, float y, float z);
	void SetRotation(float yaw, float pitch, float roll);

	void SetScale(float x, float y, float z);
	void SetScale(float value);

	virtual D3DXMATRIXA16* Transform();

	virtual bool IsA( OBJECT_TYPE type );

	D3DXVECTOR3 oldPosition;
	D3DXVECTOR3 velocity;
	D3DXVECTOR3 position;
	D3DXVECTOR3 rotation;
	D3DXVECTOR3 scale; //scale along x,y,z

	D3DXVECTOR3 positionOld; //a back up of our last position for collision detection

	bool bAddToWorldOnUpdate; //add this object to the world on update?
	bool bAlwaysRender; //should this be rendered every frame?

	//this function is called is a collision occured
	virtual void Collision( Object* pOtherObj, D3DXVECTOR3 normal );
	
	bool bCollideWorld; //collides with world?
	bool bCollideStatics; //collides with statics?
	bool bCollideObjects; //collides with other objects/actors

	float boundSphereRadius; //what radius bound sphere before we collide

	D3DXMATRIXA16* GetMatrix();

	/*
	 * adds a frame to an animation ID
	 */
	void AddKeyFrame(int animId, KeyFrame frame);
	int GetNumAnimations();
	int GetNumFrames( int animId );
	AnimationData* GetAnimationData();

	virtual bool IsPlayingAnimation();
	virtual void PlayAnimation(int animId, bool loop = true);
	virtual void StopAnimation();

protected:
	OBJECT_TYPE objType; //used for "IsA" function
	OBJECT_TYPE classType;

	bool bRender;
	bool bDestroy;
	long int timeToLive; //time remaning before object is destroyed	

	D3DXMATRIXA16 matWorld;
	Object* parent; //if not null, inherit from parents matrix

	AnimationData animations; // a vector of aniamtions, each animation has a vector og keyframes
	KeyFrame tweenKey;
	long timeTillNextFrame;
	int currentFrame;
	int nextFrame;
	int currentAnimation;
	bool loopAnimation;
	bool playingAnimation;
	float frameRate; 

	unsigned int objId; //unique id to identitfy objects
	static unsigned int objectCount;
};

#endif