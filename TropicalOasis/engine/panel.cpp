#include "stdafx.h"

Panel::Panel() : Model()
{
	classType = OBJ_PANEL;

	bAddToWorldOnUpdate = false;
}

Panel::~Panel()
{
	SAFE_RELEASE( pMesh->pIndexBuffer );
	SAFE_RELEASE( pMesh->pVertexBuffer );
	SAFE_DELETE( pMesh );
}

/*
Panel::Panel( Engine* pEngine )
{
	Mesh::Mesh( pEngine );

	classType = CLASS_PANEL;
	SetFVF( PANEL_FVF );

	//set up some sample data
	WORD idx[] = { 0,1,2, 0,2,3 }; //, 
			//	0,2,3};

	/*
	PANEL_VERTEX verts[3];
	verts[0].p = D3DXVECTOR4(150.0f,  50.0f, 0.5f, 1.0f);
	verts[0].tu = 1;
	verts[0].tv = 0;

	verts[1].p = D3DXVECTOR4(250.0f, 250.0f, 0.5f, 1.0f);
	verts[1].tu = 0;
	verts[1].tv = 0;

	verts[2].p = D3DXVECTOR4(250.0f, 250.0f, 0.5f, 1.0f);
	verts[2].tu = 0;
	verts[2].tv = 1;* /

	PANEL_VERTEX verts[] =
	{
		{ 800.0f,  0.0f, 0.0f, 1.0f,  1.00, 0.01,  }, // x, y, z, rhw, color
		{ 800.0f, 65.0f, 0.0f, 1.0f,  1.00, 0.965,  },
		{  640.0f, 65.0f, 0.0f, 1.0f, 0.03, 0.965,  },
		{  640.0f, 0.0f, 0.0f, 1.0f,  0.03, 0.01,  },
	};


	//verts[3].p = D3DXVECTOR4(1,1,0.5,1);
	//verts[3].tu = 1;
	//verts[3].tv = 1;


	SetTexture( GetEngine().GetFileManager().GetTexture("small_logo.dds") );
	SetNumVertices( 4 );
	SetNumIndices( 6 );
	SetNumPrimitives( 2 );

	SetSizeOfVertex( sizeof(PANEL_VERTEX) );

	//create, and lock index buffer, and copy info in
	if( FAILED( Direct3D::GetInstance().GetDevice()->CreateIndexBuffer( sizeof(WORD)*GetNumPrimitives()*3, D3DUSAGE_WRITEONLY,
    D3DFMT_INDEX16, D3DPOOL_DEFAULT, &pIndexBuffer, 0) ))
	{
//cos*		GetEngine().GetConsole().Log("Create Mesh Index Buffer [FAILED]");
		return;
	}

	WORD* index;
	if( FAILED( GetIndexBuffer()->Lock(0, 0, (void**)&index, 0) ))
	{
//cos*		GetEngine().GetConsole().Log("Lock Mesh Index buffer [FAILED]");
		return;
	}

	for( int n = 0; n < GetNumPrimitives()*3; n++ )
	{
		index[n] = idx[n];
		//indexList.push_back( createFrom.indexList[n] ); 
	}

	GetIndexBuffer()->Unlock();

	//create and lock vertex buffer and copy info in
	if( FAILED( Direct3D::GetInstance().GetDevice()->CreateVertexBuffer( GetSizeOfVertex() * GetNumVertices(), D3DUSAGE_WRITEONLY,
    GetFVF() , D3DPOOL_DEFAULT, &pVertexBuffer, 0) ) )
	{	
//cos*		GetEngine().GetConsole().Log("Error creating texture batch vertex buffer [FAILED]");
		return;
	}

	PANEL_VERTEX* vertex;	
	if( FAILED( GetVertexBuffer()->Lock(0, 0, (void**)&vertex, 0) ))
	{
//cos*		GetEngine().GetConsole().Log("Lock Mesh Vertex buffer [FAILED]");
		return;
	}

	for( int v = 0; v < GetNumVertices(); v++ )
	{
		vertex[v] = verts[v];
	}
}*/

void Panel::CreatePanel( FileManager* pFM, Direct3D* pD3D, float width, float height, float x, float y, std::string texture)
{
	pMesh = new MESH();

	WORD idx[] = { 0,1,2, 0,2,3 };  

	VERT_TPOS_TEX verts[] =
	{
		{ x + width,  y, 0.0f, 1.0f,  1.00, 0.00,  }, // x, y, z, rhw, color
		{ x + width, y + height, 0.0f, 1.0f,  1.00, 1.00,  },
		{  x, y + height, 0.0f, 1.0f, 0.00, 1.00,  },
		{  x, y, 0.0f, 1.0f,  0.00, 0.00,  },
	};
/*
	VERT_TPOS_TEX verts[] =
	{
		{ 800.0f,  0.0f, 0.0f, 1.0f,  1.00, 0.01,  }, // x, y, z, rhw, color
		{ 800.0f, 65.0f, 0.0f, 1.0f,  1.00, 0.965,  },
		{  640.0f, 65.0f, 0.0f, 1.0f, 0.03, 0.965,  },
		{  640.0f, 0.0f, 0.0f, 1.0f,  0.03, 0.01,  },
	};*/

	SetTexture( pFM->GetTexture( texture ) );
	pTexture->bTransparent = true;

	pMesh->fvf = VERT_TPOS_TEX::FVF;
	pMesh->vertexSize = sizeof( VERT_TPOS_TEX );
	pMesh->numOfIndices = 6;
	pMesh->numOfPolygons = 2;
	pMesh->numOfVertices = 4;

	if( FAILED( pD3D->GetDevice()->CreateIndexBuffer( sizeof(WORD)*2*3, D3DUSAGE_WRITEONLY,
    D3DFMT_INDEX16, D3DPOOL_DEFAULT, &pMesh->pIndexBuffer, 0) ))
	{
		return;
	}

	WORD* index;
	if( FAILED( pMesh->pIndexBuffer->Lock(0, 0, (void**)&index, 0) ))
	{
		return;
	}

	for( int n = 0; n < 2*3; n++ )
	{
		index[n] = idx[n];
	}

	pMesh->pIndexBuffer->Unlock();


	if( FAILED( pD3D->GetDevice()->CreateVertexBuffer( sizeof( VERT_TPOS_TEX ) * 4, D3DUSAGE_WRITEONLY,
    VERT_TPOS_TEX::FVF, D3DPOOL_DEFAULT, &pMesh->pVertexBuffer, 0) ) )
	{	
		return;
	}

	VERT_TPOS_TEX* vertex;	
	if( FAILED( pMesh->pVertexBuffer->Lock(0, 0, (void**)&vertex, 0) ))
	{
		return;
	}

	for( int v = 0; v < 4; v++ )
	{
		vertex[v] = verts[v];
	}

	pMesh->pVertexBuffer->Unlock();
}

bool Panel::IsA( OBJECT_TYPE type )
{
	if( type == classType )
		return true;
	else
		return Model::IsA( type );
}
