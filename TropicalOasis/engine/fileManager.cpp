#include "stdafx.h"
#include "fileManager.h"

FileManager::FileManager( Direct3D* pD3D, std::string meshPath, std::string effectPath, std::string texturePath )
{
	this->pD3D = pD3D;
	this->meshPath = meshPath;
	this->effectPath = effectPath;
	this->texturePath = texturePath;
}

FileManager::~FileManager()
{
	//release all textures
	STL_CONTAINER_TEXTURE::iterator texIt;

	for( texIt = textures.begin(); texIt != textures.end(); texIt++ )
	{
		SAFE_RELEASE( texIt->pTexture->pTexture );
		SAFE_DELETE( texIt->pTexture );
	}

	//TODO:
	//release shaders
	STL_CONTAINER_EFFECT::iterator effectIt;

	for( effectIt = effects.begin(); effectIt != effects.end(); effectIt++ )
	{
		SAFE_RELEASE( effectIt->pEffect );
	}

	//release meshes
	STL_CONTAINER_MESH::iterator meshIt;

	for( meshIt = meshes.begin(); meshIt != meshes.end(); meshIt++ )
	{
		SAFE_RELEASE( meshIt->pMesh->pVertexBuffer );
		SAFE_RELEASE( meshIt->pMesh->pIndexBuffer );
		SAFE_DELETE( meshIt->pMesh );
	}
}

//non animated mesh?
MESH* const FileManager::GetMesh( std::string fileName )
{
	STL_CONTAINER_MESH::iterator meshIt;

	for( meshIt = meshes.begin(); meshIt < meshes.end(); meshIt++)
	{
		if( meshIt->name == fileName ) //strcmp( texIt->fileName, fileName) == 0)
		{
			meshIt->references++;
			return meshIt->pMesh;
		}
	}

	MESHCONT newMesh;
	newMesh.pMesh = new MESH();

	std::string actualPath = meshPath + fileName;
	newMesh.name = fileName;

	//load mesh here
	A3DFile meshFile( pD3D, this, actualPath, newMesh.pMesh );

	newMesh.references++;
	meshes.push_back( newMesh );

	return meshes[meshes.size() - 1].pMesh;
}

LPD3DXEFFECT FileManager::GetEffect( std::string fileName )
{
	STL_CONTAINER_EFFECT::iterator effectIt;

	for( effectIt = effects.begin(); effectIt < effects.end(); effectIt++)
	{
		if( effectIt->name == fileName ) //strcmp( texIt->fileName, fileName) == 0)
		{
			effectIt->references++;
			return effectIt->pEffect;
		}
	}

	EFFECT newEffect;

	std::string actualPath = effectPath + fileName;
	newEffect.name = fileName;

	HRESULT hr;
	LPD3DXBUFFER pBufferErrors = 0;
	if( FAILED( hr = D3DXCreateEffectFromFile( pD3D->GetDevice(), 
		                           actualPath.c_str(),
		                           NULL, 
		                           NULL, 
		                           D3DXSHADER_DEBUG, 
		                           NULL, 
		                           &newEffect.pEffect, 
		                           &pBufferErrors ) ) )
	{
	//	exit(0);
		DXTRACE_ERR( "D3DXCreateEffectFromFile", hr );
		Log("Compile FX file [FAILED] %s", pBufferErrors->GetBufferPointer() );
//cos*		pEngine->GetConsole().Log( "Compile FX file [FAILED] %s", pBufferErrors->GetBufferPointer());
		return 0;

	}	

//cos*	pEngine->GetConsole().Log("Loading effect %s", fileName.c_str() );

	newEffect.references++;
	effects.push_back(newEffect);
	
	return effects[effects.size() - 1].pEffect;
}

TEXTURE* FileManager::GetTexture(std::string fileName, bool bIsTransparent)
{
	//firstly, check to see if it already exists
	//std::map<char*, LOADEDFILE, stringCmp>::iterator texIt;
	
	STL_CONTAINER_TEXTURE::iterator texIt;

	//for(int i =0; i < strlen(fileName); i++)
	//{
		//fileName[i] = tolower( fileName[i] );
	//}

	
	for( texIt = textures.begin(); texIt < textures.end(); texIt++)
	{
		if( texIt->name == fileName )
		{
			texIt->references++;
			return texIt->pTexture;
		}
	}

	TEXTURECONT newTexture;
	newTexture.pTexture = new TEXTURE();

	std::string actualPath = texturePath + fileName;

	if( FAILED( D3DXCreateTextureFromFile( pD3D->GetDevice(),
													actualPath.c_str(),
												   &newTexture.pTexture->pTexture ) ) )
	{
		Log("ERROR loading texture: %s", actualPath.c_str() );
		SAFE_DELETE( newTexture.pTexture );
//cos*		pEngine->GetConsole().Log("Load/Locate Mesh Texture [FAILED] %s", actualPath.c_str());
		return 0;
	}

	//see if alpha is in here
	D3DSURFACE_DESC sDesc;
	newTexture.pTexture->pTexture->GetLevelDesc(0, &sDesc );

//	newTexture.pTexture->pTexture->GetPixelFormat();
	if( sDesc.Format == D3DFMT_DXT1 || sDesc.Format == D3DFMT_DXT3 )
	{
		//no alpha
		newTexture.pTexture->bTransparent = false;
	}
	else if( sDesc.Format == D3DFMT_DXT5 )
	{
		newTexture.pTexture->bTransparent = true;
	}

	newTexture.name = fileName;

//cos*	pEngine->GetConsole().Log("Loading texture %s", fileName.c_str() );

	//newTexture.pTexture->bTransparent = bTransparent;
	newTexture.references++;

	textures.push_back(newTexture);
	
	return textures[textures.size() - 1].pTexture;
}

