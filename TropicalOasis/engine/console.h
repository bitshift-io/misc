#ifndef _SIPHON_CONSOLE_
#define _SIPHON_CONSOLE_

#include "font.h"

void EmergencyLog(const char *lpszText, ...);

class RenderManager;
class DirectInput;
class Object;

//typedef const char* (*StringCallback)(void* ptr, int argc, const char* argv[]);
/*
class Unknown
{
public:
	Unknown();

	const char* test(int argc, const char* argv[]){ return 0;};
};

typedef const char* (Unknown::*StringCallback)(int argc, const char* argv[]);
*/

//typedef const char* (*StringCallback)(void* pThis, int argc, const char* argv[]);

/*! \class Console
 *  \brief Console and Log message reporting
 *
 * This class is responsible for logging errors
 * to log file as well as displaying messages
 * to the in-game console
 * it is also for passing user commands
 */
class Console : public Singleton<Console>
{
public:
	Console();
	~Console();

	/**
	 * Puts this text in console
	 */
	void Log(const char *lpszText, ...);

	/**
	 * same as log, except uses a different colour
	 */
	void Warn(const char *lpszText, ...);

	void Update( float deltaTime, DirectInput* pDI );
	int Render();

	void ProcessCommand( std::string currentMsg );

	bool Initialise( RenderManager* pRM, Direct3D* pD3D );
	void Shutdown();

	// the following functions let the user change registered
	// variables using the console
	void SetBooleanVariable( std::string cmd, bool* pBool );

	/**
	 * This function should be called to bind a new console command
	 * and function, the cmd string should also contain formatting
	 * for example: RegisterCmd("run", fnPtr );
	 * this will call char* [class].run( int argc, char* argv[] );
	 * the returned string will be dumped to console
	 */
	//void RegisterCmd( std::string cmd, StringCallback ptr );

private:
	friend class Singleton<Console>;

	FILE* logFile;
	bool bShow;
	int page; //what page to show

	Font* font;

	int linesPerPage;

	std::string currentMsg;
	std::vector< std::string > messages;

	//std::map< std::string, std::pair<std::string, StringCallback> > commands;

	RenderManager* pRM;
};

#endif