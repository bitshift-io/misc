/* ====================================
	RENDERMANAGER
This takes care of rendering all visiible objects,
using the camera
==================================== */

#ifndef _SIPHON_RENDERMANAGER_
#define _SIPHON_RENDERMANAGER_

//because its the world matrix :)
class Model;
class ObjectManager;

//note: the WORd is the FVF
typedef std::map<LPD3DXEFFECT, std::map<TEXTURE*, std::map<WORD, std::map<MESH*, std::list<Model*> > > > > AdjacencyList;

/*! \class RenderManager
 *  \brief Renders visible objects
 *
 * This class takes the set camera,
 * and renders the visible objects
 */
class RenderManager
{
public:
	RenderManager( Direct3D* pD3D );
	~RenderManager();

	void Render( ObjectManager* pOM, Camera* pCamera, World* pW );
	void Update( float const deltaTime );

	/**
	 * This assume that a model has one effect/texture/fvf/mesh
	 * so will only add one object to be rendered
	 */
	void PreRender( Model* mdl, bool alwaysRendered = false );

	/**
	 * this assumes nothing, call this functions as many times
	 * as a model needs to que up models (good for special effects eg. multitexturing)
	 */
	void PreRender( LPD3DXEFFECT pEffect, TEXTURE* pTexture, WORD fvf, MESH* pMesh, Model* mdl, bool alwaysRendered = false );

protected:

	enum RENDER_TYPE
	{ 
		RENDER_SOLID, 
		RENDER_TRANSPARENT, 
		RENDER_HUD
	};

	inline int DoRender( RENDER_TYPE type, AdjacencyList renderList, Camera* pCamera );

	bool showFps;
	bool bLoading; //loading or rendering scenes?

	AdjacencyList sceneData;
	AdjacencyList transparentSceneData;
	AdjacencyList hudSceneData; //rendered last hud/gui stuffs

	LPDIRECT3DINDEXBUFFER9 indexCache;
	LPDIRECT3DVERTEXBUFFER9 vertexCache;

	int numFaces;
	int numVertices;
	int numIndices;

	int maxVertices;
	int maxIndices;


	//bool bDefaultCamera; //are we using the camera we spawned with?
	long unsigned int startTime;
	long unsigned int frames;
	float polygonsRendered;

	Direct3D* pD3D;

	LONGLONG previousTime;
	LONGLONG curTime;

	LPD3DXFONT	pFont;
	RECT fontPosition;

	long lastSecondTime;
	int noFramesThisSecond;
	int fpsLastSecond;
	float Scale;

	float fpsOkay;
	char txt[255];
};

#endif