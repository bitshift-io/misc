#include "stdafx.h"
#include "camera.h"

Camera::Camera( Direct3D* pD3D, INIFile* ini ) : Object( OBJ_CAMERA )
{
	Object::Object();

	this->pD3D = pD3D;

	bInvertMouse = false;

	if( ini->GetValue("mouse", "invertMouse" ) == "true" )
		bInvertMouse = true;

	sensitivity = 10;
	sscanf( (ini->GetValue("mouse", "sensitivity" )).c_str(), "%f", &sensitivity );

	boundSphereRadius = 40;
	bAddToWorldOnUpdate = true;

	up = D3DXVECTOR3(0,1,0);
	right = D3DXVECTOR3(1,0,0);
	lookAt = D3DXVECTOR3(0,0,1);

	eye = D3DXVECTOR3(0,0,0);
	position = D3DXVECTOR3(20,10,20);
	rotation = D3DXVECTOR3(0,0,0);

/*
	frustum.points[0] = D3DXVECTOR3(-5,5,15); //top foward left
	frustum.points[1] = D3DXVECTOR3(5,5,15); //top foward right
	frustum.points[2] = D3DXVECTOR3(-5,-5,15); //bottom foward left
	frustum.points[3] = D3DXVECTOR3(5,-5,15); //bottom foward right

	frustum.points[4] = D3DXVECTOR3(-5,5,0); //top back left
	frustum.points[5] = D3DXVECTOR3(5,5,0); //top back right
	frustum.points[6] = D3DXVECTOR3(-5,-5,0); //bottom back left
	frustum.points[7] = D3DXVECTOR3(5,-5,0); //bottom back right
*/

	//hrmm, this works, but rotation seems funky on it
	int cameraRadius = 2000;
	frustum.points[0] = D3DXVECTOR3(-cameraRadius,cameraRadius,cameraRadius); //top foward left
	frustum.points[1] = D3DXVECTOR3(cameraRadius,cameraRadius,cameraRadius); //top foward right
	frustum.points[2] = D3DXVECTOR3(-cameraRadius,-cameraRadius,cameraRadius); //bottom foward left
	frustum.points[3] = D3DXVECTOR3(cameraRadius,-cameraRadius,cameraRadius); //bottom foward right

	frustum.points[4] = D3DXVECTOR3(0,0,0); //back point //top back left
	//frustum.points[5] = D3DXVECTOR3(cameraRadius,cameraRadius,-cameraRadius); //top back right
	//frustum.points[6] = D3DXVECTOR3(-cameraRadius,-cameraRadius,-cameraRadius); //bottom back left
	//frustum.points[7] = D3DXVECTOR3(cameraRadius,-cameraRadius,-cameraRadius); //bottom back right
	
	/*
	frustum.points[0] = D3DXVECTOR3(-cameraRadius,cameraRadius,cameraRadius); //top foward left
	frustum.points[1] = D3DXVECTOR3(cameraRadius,cameraRadius,cameraRadius); //top foward right
	frustum.points[2] = D3DXVECTOR3(-cameraRadius,-cameraRadius,cameraRadius); //bottom foward left
	frustum.points[3] = D3DXVECTOR3(cameraRadius,-cameraRadius,cameraRadius); //bottom foward right

	frustum.points[4] = D3DXVECTOR3(-cameraRadius,cameraRadius,-cameraRadius); //top back left
	frustum.points[5] = D3DXVECTOR3(cameraRadius,cameraRadius,-cameraRadius); //top back right
	frustum.points[6] = D3DXVECTOR3(-cameraRadius,-cameraRadius,-cameraRadius); //bottom back left
	frustum.points[7] = D3DXVECTOR3(cameraRadius,-cameraRadius,-cameraRadius); //bottom back right
	*/
	bFreeCam = true;

	//D3DXMATRIX matProj;
    D3DXMatrixPerspectiveFovLH( &matProjection, D3DX_PI/3, 1.33f, 0.5f, 50000.0f );
    pD3D->GetDevice()->SetTransform( D3DTS_PROJECTION, &matProjection);
}

Camera::~Camera()
{

}

D3DXMATRIXA16* Camera::Transform()
{
	/*remove after!
//	position.x = 2*sin(Time::GetTime()/800);
//	position.y = 2*cos(Time::GetTime()/800);

//	DIMOUSESTATE2 dims2;
//	DirectInput::GetInstance().pMouse->GetDeviceState( sizeof(DIMOUSESTATE2), &dims2 );


	//rotation.y = 2*cos(Time::GetTime()/8); //D3DXToRadian( 50 );//(float)dims2.lX/200;
	//rotation.x = 0; //(float)dims2.lY/200;
	//rotation.z = 0;
*/
	D3DXMATRIXA16 matRotation;
	D3DXMATRIXA16 matTranslation;
    
	//D3DXMATRIXA16 matScale;

	D3DXMatrixRotationYawPitchRoll( &matRotation, rotation.x, rotation.y, rotation.z);
	D3DXMatrixTranslation( &matTranslation, position.x, position.y, position.z);

//	systemLog.WriteLog("camera rotation with vector (%f, %f, %f)", rotation.x, rotation.y, rotation.z);
//	systemLog.WriteLog(" %f %f %f ", matRotation._11, matRotation._12, matRotation._13);
//	systemLog.WriteLog(" %f %f %f ", matRotation._21, matRotation._22, matRotation._23);
//	systemLog.WriteLog(" %f %f %f ", matRotation._31, matRotation._32, matRotation._33);

	D3DXMatrixMultiply( &matCamera,  &matRotation, &matTranslation);

	D3DXMatrixInverse( &matView, 0, &matCamera );

	UpdateCullInfo( &frustum, &matView, &matProjection );

	return &matView;

	
/*

	eye.x = matRotation._31;
	eye.y = matRotation._32;
	eye.z = matRotation._33;*/

//	D3DXMATRIXA16 matCamera;
//	D3DXMATRIXA16 matRotation;

  //  D3DXMatrixLookAtLH( &matView, &position, &eye, &up );

//	D3DXMatrixRotationYawPitchRoll(&matRotation, rotation.x, rotation.y, rotation.z);
/*	D3DXMatrixMultiply( &matCamera, &matView, &matRotation );

    pD3D->GetDevice()->SetTransform( D3DTS_VIEW, &matView );

    // For the projection matrix, we set up a perspective transform (which
    // transforms geometry from 3D view space to 2D viewport space, with
    // a perspective divide making objects smaller in the distance). To build
    // a perpsective transform, we need the field of view (1/4 pi is common),
    // the aspect ratio, and the near and far clipping planes (which define at
    // what distances geometry should be no longer be rendered).

	//Make sure that the z-buffer and lighting are enabled
	pD3D->GetDevice()->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
    pD3D->GetDevice()->SetRenderState(D3DRS_LIGHTING, TRUE);

	//combine view-projection matrices
	D3DXMATRIXA16 matCombined;
	D3DXMatrixMultiply( &matCombined, &matView, &matProjection);

	TransformBounds();*/
}

void Camera::TransformOtho()
{
	D3DXMATRIX matOrtho; 
    
    //Setup the orthogonal projection matrix and the default world/view matrix
    D3DXMatrixOrthoLH(&matOrtho, pD3D->deviceWidth, pD3D->deviceHeight, 0.0f, 1.0f);

    pD3D->GetDevice()->SetTransform(D3DTS_PROJECTION, &matOrtho);

    //Make sure that the z-buffer and lighting are disabled
	pD3D->GetDevice()->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
    pD3D->GetDevice()->SetRenderState(D3DRS_LIGHTING, FALSE);
}

BOUNDS* Camera::TransformBounds()
{
    // For our world matrix, we will just leave it as the identity
	D3DXMATRIXA16 matRotation;
	D3DXMATRIXA16 matTranslation;
    D3DXMATRIXA16 matWorld;
	D3DXMatrixIdentity( &matWorld );

	D3DXMatrixRotationYawPitchRoll( &matRotation, rotation.x, rotation.y, rotation.z);
	D3DXMatrixTranslation( &matTranslation, position.z, position.y, position.x);

	D3DXMatrixMultiply( &matWorld, &matRotation, &matTranslation );

	//transform object bounds to world bounds
	D3DXVECTOR4 temp, temp2;
	for(int i=0; i < 5; i++)
	{
		D3DXVec3Transform( &temp, &frustum.points[i], &matWorld );
		worldFrustum.points[i].x = temp.x;
		worldFrustum.points[i].y = temp.y;
		worldFrustum.points[i].z = temp.z;
	}

	return &worldFrustum;
}

void Camera::UpdateCullInfo( BOUNDS* pCullInfo, D3DXMATRIXA16* pMatView, D3DXMATRIXA16* pMatProj )
{
	//taken from dx cull sample and modified

    D3DXMATRIXA16 mat;

    D3DXMatrixMultiply( &mat, pMatView, pMatProj );
    D3DXMatrixInverse( &mat, NULL, &mat );

    pCullInfo->points[0] = D3DXVECTOR3(-1.0f, -1.0f,  0.0f); // xyz
    pCullInfo->points[1] = D3DXVECTOR3( 1.0f, -1.0f,  0.0f); // Xyz
    pCullInfo->points[2] = D3DXVECTOR3(-1.0f,  1.0f,  0.0f); // xYz
    pCullInfo->points[3] = D3DXVECTOR3( 1.0f,  1.0f,  0.0f); // XYz
    pCullInfo->points[4] = D3DXVECTOR3(-1.0f, -1.0f,  1.0f); // xyZ
    pCullInfo->points[5] = D3DXVECTOR3( 1.0f, -1.0f,  1.0f); // XyZ
    pCullInfo->points[6] = D3DXVECTOR3(-1.0f,  1.0f,  1.0f); // xYZ
    pCullInfo->points[7] = D3DXVECTOR3( 1.0f,  1.0f,  1.0f); // XYZ

    for( INT i = 0; i < 8; i++ )
        D3DXVec3TransformCoord( &pCullInfo->points[i], &pCullInfo->points[i], &mat );

    D3DXPlaneFromPoints( &pCullInfo->planes[0], &pCullInfo->points[0], 
        &pCullInfo->points[1], &pCullInfo->points[2] ); // Near
    D3DXPlaneFromPoints( &pCullInfo->planes[1], &pCullInfo->points[6], 
        &pCullInfo->points[7], &pCullInfo->points[5] ); // Far
    D3DXPlaneFromPoints( &pCullInfo->planes[2], &pCullInfo->points[2], 
        &pCullInfo->points[6], &pCullInfo->points[4] ); // Left
    D3DXPlaneFromPoints( &pCullInfo->planes[3], &pCullInfo->points[7], 
        &pCullInfo->points[3], &pCullInfo->points[5] ); // Right
    D3DXPlaneFromPoints( &pCullInfo->planes[4], &pCullInfo->points[2], 
        &pCullInfo->points[3], &pCullInfo->points[6] ); // Top
    D3DXPlaneFromPoints( &pCullInfo->planes[5], &pCullInfo->points[1], 
        &pCullInfo->points[0], &pCullInfo->points[4] ); // Bottom
}

void Camera::FreeCam( float deltaTime, DirectInput* pDI )
{
	DIMOUSESTATE2 dims2 = pDI->GetMouseState();
	//DirectInput::pMouse->GetDeviceState( sizeof(DIMOUSESTATE2), &dims2 );

	float speed = 600; //units per second

	if( bInvertMouse )
		rotation.y -= (float)(dims2.lY * sensitivity)/1000;
	else
		rotation.y += (float)(dims2.lY * sensitivity)/1000;

	rotation.x += (float)(dims2.lX * sensitivity)/500;

	//get camera fowards
	D3DXVECTOR3 lookAt;
	lookAt.x = matCamera._31;
	lookAt.y = matCamera._32;
	lookAt.z = matCamera._33;

	D3DXVECTOR3 right;
	right.x = matCamera._11;
	right.y = matCamera._12;
	right.z = matCamera._13;

	D3DXVec3Normalize( &lookAt, &lookAt);
	D3DXVec3Normalize( &right, &right);


	if( pDI->KeyDown(DIK_W) )
	{
		SetPosition( position.x + lookAt.x * speed * deltaTime/1000,
					 position.y + lookAt.y * speed * deltaTime/1000,
					 position.z + lookAt.z * speed * deltaTime/1000 );

		//position.x += lookAt.x * speed * deltaTime/1000;
		//position.y += lookAt.y * speed * deltaTime/1000;
		//position.z += lookAt.z * speed * deltaTime/1000;
	}
	if( pDI->KeyDown(DIK_S) )
	{
		SetPosition( position.x - lookAt.x * speed * deltaTime/1000,
					 position.y - lookAt.y * speed * deltaTime/1000,
					 position.z - lookAt.z * speed * deltaTime/1000 );

		//position.x -= lookAt.x * speed * deltaTime/1000;
		//position.y -= lookAt.y * speed * deltaTime/1000;
		//position.z -= lookAt.z * speed * deltaTime/1000;
	}
	if( pDI->KeyDown(DIK_A) )
	{
		SetPosition( position.x - right.x * speed * deltaTime/1000,
					 position.y - right.y * speed * deltaTime/1000,
					 position.z - right.z * speed * deltaTime/1000 );

		//position.x -= right.x * speed * deltaTime/1000;
		//position.y -= right.y * speed * deltaTime/1000;
		//position.z -= right.z * speed * deltaTime/1000;
	}
	if( pDI->KeyDown(DIK_D) )
	{
		SetPosition( position.x + right.x * speed * deltaTime/1000,
					 position.y + right.y * speed * deltaTime/1000,
					 position.z + right.z * speed * deltaTime/1000 );

		//position.x += right.x * speed * deltaTime/1000;
		//position.y += right.y * speed * deltaTime/1000;
		//position.z += right.z * speed * deltaTime/1000;
	}
	if( pDI->KeyDown(DIK_SPACE) )
	{
		SetPosition( position.x,
					 position.y + speed * deltaTime/1000,
					 position.z );

		//position.y += speed * deltaTime/1000;
	}
	if( pDI->KeyDown(DIK_LCONTROL) )
	{
		SetPosition( position.x,
					 position.y - speed * deltaTime/1000,
					 position.z );
		//position.y -= speed * deltaTime/1000;
	}
}

void Camera::Update( float const deltaTime, World* pW, DirectInput* pDI )
{
	//static float angle = 0; //temporary
	//angle += 0.001;
	Object::Update(deltaTime, pW, pDI );

	TransformBounds();

	if( bFreeCam && !IsPlayingAnimation() )
		FreeCam( deltaTime, pDI );
}

D3DXMATRIXA16& Camera::GetView()
{
	return matView;
}

D3DXMATRIXA16& Camera::GetProjection()
{
	return matProjection;
}

Camera::GetTransposeMatrix(D3DXMATRIX& returnMat)
{
	D3DXMatrixTranspose( &returnMat, &matView);
}

D3DXVECTOR3 Camera::GetDirection()
{
	D3DXVECTOR3 lookAt;
	lookAt.x = matCamera._31;
	lookAt.y = matCamera._32;
	lookAt.z = matCamera._33;

	D3DXVec3Normalize( &lookAt, &lookAt);
	return lookAt;
}

D3DXVECTOR3 Camera::GetMatrix()
{
	return matCamera;
}

/*
bool Camera::IsA(int type)
{

	if( type == CLASS_CAMERA )
		return true;
	else
		return Object::IsA(type);

}*/

