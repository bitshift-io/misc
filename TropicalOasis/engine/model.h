#ifndef _SIPHON_MODEL_
#define _SIPHON_MODEL_

/*! \class Model
 *	\brief renderable polys with a texture/material/effect
 *
 * This class combines a mesh(which is a bunch of polys)
 * with a texture and/or material and/or an effect
 */
class Model : public Object
{
public:
	Model();
	virtual ~Model();

	/**
	 * Uses the filemanger to load the MESH
	 * and also sets the texture and material
	 * from this MESH
	 */
	bool LoadModel( FileManager* pFM, std::string file );


	MESH* GetMesh();
	void SetMesh( MESH* pMesh );

	LPD3DXEFFECT GetEffect();
	TEXTURE* GetTexture();
	D3DMATERIAL9 GetMaterial();

	void SetEffect( LPD3DXEFFECT pEffect );
	void SetTexture( TEXTURE* pTexture );
	void SetMaterial( D3DMATERIAL9& pMaterial );

	virtual void PreRender( RenderManager* pRM, BOUNDS* cameraBounds, bool alwaysRendered = false );

	virtual int Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT pEffect, TEXTURE* pTexture );
	
	virtual bool IsA( OBJECT_TYPE type );

	/**
	 * Calling this function sets this mesh to be "always rendered"
	 */
//	void CreatePanel( Direct3D* pD3D, float width, float height, float x, float y, std::string texture);

	/**
	 * This function gives the opportunity before
	 * rendering to set the variables for the shader
	 */
	virtual void SetVariables( int pass, LPD3DXEFFECT pEffect );

//protected:
	LPD3DXEFFECT pEffect;
	MESH* pMesh;

	TEXTURE* pTexture;
	D3DMATERIAL9 pMaterial;

	bool bDoubleSided;
/*
	//should we allow multiple meshes/textures/effects per model?
	std::vector<LPD3DXEFFECT> pEffects;
	std::vector<MESH*> pMeshs;
	std::vector<LPDIRECT3DTEXTURE9> pTextures;
	std::vector<D3DMATERIAL9> pMaterials;*/
};

#endif