#include "stdafx.h"
#include "font.h"

Font::Font( Direct3D* pD3D )
{
	LOGFONT LogFont = {16,0,0,0,FW_NORMAL,false,false,false,DEFAULT_CHARSET,OUT_TT_PRECIS,CLIP_DEFAULT_PRECIS,PROOF_QUALITY,DEFAULT_PITCH,"Arial"};
	RECT FontPosition = {0,0,800,600};

	if(FAILED(D3DXCreateFontIndirect(pD3D->GetDevice(),&LogFont,&pFont)))
	{
		 //Console::GetInstance().Log("Create Font [FAILED]");
	}
}

Font::Font(int size, std::string font, Direct3D* pD3D )
{
	LOGFONT LogFont = {size,0,0,0,FW_NORMAL,false,false,false,DEFAULT_CHARSET,OUT_TT_PRECIS,CLIP_DEFAULT_PRECIS,PROOF_QUALITY,DEFAULT_PITCH,*(font.c_str())};
	RECT FontPosition = {0,0,800,600};

	if(FAILED(D3DXCreateFontIndirect(pD3D->GetDevice(),&LogFont,&pFont)))
	{
		 //Console::GetInstance().Log("Create Font [FAILED]");
	}

	//fnt->font->DrawText(txt,-1,&FontPosition,DT_CENTER,0xffffffff);
}

Font::DrawText(std::string text, int x, int y, D3DCOLOR color)
{
	RECT fontPosition = {0,0,800,600};
	fontPosition.left = x;
	fontPosition.top = y;

	pFont->DrawText( text.c_str(), -1, &fontPosition, DT_LEFT , color);
}

Font::~Font()
{

}

	