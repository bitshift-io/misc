#ifndef _SIPHON_VERTEXTYPES_
#define _SIPHON_VERTEXTYPES_

#define VERT_BONE_FVF (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX3 | D3DFVF_TEXCOORDSIZE2(0) | D3DFVF_TEXCOORDSIZE4(1) | D3DFVF_TEXCOORDSIZE4(2));

struct VERT_POS
{
	float x,y,z;

	static int FVF;
};

struct VERT_POS_NORM_TEX
{
	float x,y,z;
	float nx, ny, nz;
    float tu, tv;

	static int FVF;
};

struct VERT_TPOS_TEX
{
	FLOAT x,y,z, rhw;
    FLOAT       tu, tv;

	static int FVF;
};

//THIS HSOULD BE A SECOND STREAM WITH JUST BONE DATA!!!! see:
// "Programmable Stream Model" dx9 sdk help
struct VERT_BONE
{
	float x,y,z;
	float nx, ny, nz;
    float tu, tv;

	D3DXVECTOR4 boneId; //4 bone id's
	D3DXVECTOR4 weight; //4 bone weights
	//D3DXVECTOR4 numBones; //x is num of bones

	//static int FVF;
};

#endif