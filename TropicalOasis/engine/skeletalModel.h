#ifndef _SIPHON_SKELETALMODEL_
#define _SIPHON_SKELETALMODEL_

class SkeletalModel : public Model
{
public:
	SkeletalModel(FileManager* pFM, Direct3D* pD3D );

	int Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT pEffect, TEXTURE* pTexture );

	LPDIRECT3DVERTEXDECLARATION9 pVertexDeclaration;
};

#endif