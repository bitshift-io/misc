#include "stdafx.h"
#include "engine.h"


#include "../resource.h"

// HIGH_PRIORITY_CLASS is in most cases a better choice, then the REALTIME_PRIORITY_CLASS
// ****************************************************
//  Fuction changes priority of current process:
//
//  following arguments can be used:
//       IDLE_PRIORITY_CLASS, NORMAL_PRIORITY_CLASS,
//       HIGH_PRIORITY_CLASS, REALTIME_PRIORITY_CLASS
// ****************************************************
void SetAppPriority(DWORD dwPiority)
{
   // Detemine Process Handle using OpenProcess() and pass it to
   // SetPriorityClass():

   if(!::SetPriorityClass(GetCurrentProcess(),dwPiority))
   {
    // ShowMessage("Cannot change the Process Priority!");
   }
}

LRESULT WINAPI WinProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

    switch(msg)
    {
        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;
        break;
        case WM_KEYDOWN: 
            switch (wParam)
            { 
                case VK_ESCAPE:
                    //User has pressed the escape key, so quit
                    DestroyWindow(hWnd);
                    return 0;
                break;
            } 
        break;

    }

    return DefWindowProc(hWnd, msg, wParam, lParam);
}

const char* Engine::Con( int argc, const char* argv[])
{
	return 0;
}

Engine::Engine( HWND hWnd )
{
	bDefaultCamera = true;

	HINSTANCE hInstance = (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE);;
	int height = 600;
	int width = 800;

	systemIni = new INIFile("data\\system.ini");

	//set paths
	texturePath = systemIni->GetValue( "engine", "texturePath" );
	mapPath = systemIni->GetValue( "engine", "mapPath" );
	meshPath = systemIni->GetValue( "engine", "meshPath" );
	effectPath = systemIni->GetValue( "engine", "effectPath" );

	//set up direct3d
	direct3d = new Direct3D( hWnd, height, width );

	directInput = new DirectInput( hWnd, hInstance );
	fileManager = new FileManager( direct3d, meshPath, effectPath, texturePath );
	renderManager = new RenderManager( direct3d ); //filemanager is temporary
	objectManager = new ObjectManager();
		
	world = new World( direct3d, fileManager, mapPath, meshPath);

	collisionManager = new CollisionManager();

	camera = new Camera( direct3d, systemIni );
}

Engine::Engine( HINSTANCE hInstance, std::string cmdLine )
{
	bExit = false;
	bDefaultCamera = true;

	FILE* f = fopen("data\\emergency.log", "w"); //clear log
	fclose(f);

	//check for screen saver stuffs to exit
	//if( cmdLine.size() > 0 )
	//{/*
		//if( cmdLine == "/c" || cmdLine == "/p" )
		//{
			//bExit = true;
			//return;
		//}
		//else
		//{*/
			EmergencyLog("CMD LINE: %s", cmdLine.c_str() );
		//}
	//}


	//do this first so we can change window settings
	systemIni = new INIFile("data\\system.ini");

	std::string res = systemIni->GetValue( "video", "resolution" );

	int height = 600;
	int width = 800;

	if( res == "640x480" )
	{
		height = 480;
		width = 640;
	}
	else if( res == "1024x768" )
	{
		height = 768;
		width = 1024;
	}
	else if( res == "1280x1024" )
	{
		height = 1024;
		width = 1280;
	}

	//Register the window class
   WNDCLASSEX wc2 = {sizeof(WNDCLASSEX), CS_CLASSDC, WinProc, 0L, 0L, 
                     GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                     "SIPHON", NULL};

   	wc2.hIcon	   = LoadIcon(hInstance, (LPCTSTR)IDI_ICON1);
    wc2.hIconSm	   = LoadIcon(hInstance, (LPCTSTR)IDI_ICON1);
	
	//Set the mouse pointer to an arrow
    wc2.hCursor = LoadCursor(NULL, IDC_ARROW);	
	RegisterClassEx(&wc2);

    //Create the application's window
    HWND hWnd = CreateWindow("SIPHON", "SIPHON: The Second Comming", 
                              WS_OVERLAPPEDWINDOW, 20, 20, height, width,
                              NULL, NULL, hInstance, NULL);

	hInstance = wc2.hInstance;
	wc = wc2;

	SetCursor( NULL );
    SetFocus( hWnd );
  //  SetForegroundWindow( hWnd );

	ShowWindow(hWnd, SW_SHOWNORMAL);
    UpdateWindow(hWnd);

	//set paths
	texturePath = systemIni->GetValue( "engine", "texturePath" );
	mapPath = systemIni->GetValue( "engine", "mapPath" );
	meshPath = systemIni->GetValue( "engine", "meshPath" );
	effectPath = systemIni->GetValue( "engine", "effectPath" );

	//set up direct3d
	direct3d = new Direct3D( hWnd, height, width );

	directInput = new DirectInput( hWnd, hInstance );
	fileManager = new FileManager( direct3d, meshPath, effectPath, texturePath );
	renderManager = new RenderManager( direct3d ); //filemanager is temporary
	objectManager = new ObjectManager();
		
	world = new World( direct3d, fileManager, mapPath, meshPath);

	collisionManager = new CollisionManager();

	camera = new Camera( direct3d, systemIni );
}

Engine::~Engine()
{
	SAFE_DELETE( direct3d );
	SAFE_DELETE( directInput );
	SAFE_DELETE( fileManager );
	SAFE_DELETE( renderManager );
	SAFE_DELETE( objectManager );
	SAFE_DELETE( world );
	SAFE_DELETE( camera );
	SAFE_DELETE( collisionManager );
	SAFE_DELETE( systemIni );

	Console::Destroy();
//	Console::Get().Log("is this working?");
}

void Engine::GameLoop()
{
    //Enter the game loop
    MSG msg; 
    BOOL fMessage;	
    
    while( msg.message != WM_QUIT && !bExit )
    {
        if(PeekMessage(&msg,0,NULL,NULL,PM_REMOVE))  
        {
            //Process message
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else
        {
			DoGameStuff();
        }
    }
}

void Engine::DoGameStuff()
{
	world->Update( deltaTime );
	objectManager->Update( deltaTime, world, directInput );
	//collisionManager->Update( deltaTime, world );
	camera->Update( deltaTime, world, directInput );
	world->PreRender( renderManager, &(camera->frustum) ); //remember to pass camera bounds eventually
	objectManager->PreRender( renderManager, &(camera->frustum) );
	renderManager->Update( deltaTime );
	renderManager->Render( objectManager, camera, world );

	deltaTime = (timeGetTime() - time);
	time = timeGetTime();

	if( directInput->KeyPressed( DIK_Q ) ) //DIK_SYSRQ
	{
		SaveScreenShot( direct3d );
	}
	if( directInput->KeyPressed( DIK_E ) )
		direct3d->SetWireFrame( !direct3d->GetWireFrame() );
}

HWND Engine::GetHandle()
{
	return hWnd;
}

HINSTANCE Engine::GetHInstance()
{
	return hInstance;
}

ObjectManager* const Engine::GetObjectManager()
{
	return objectManager;
}

FileManager* const Engine::GetFileManager()
{
	return fileManager;
}

World* const Engine::GetWorld()
{
	return world;
}

CollisionManager* const Engine::GetCollisionManager()
{
	return collisionManager;
}

Direct3D* const Engine::GetDirect3D()
{
	return direct3d;
}

bool Engine::SaveScreenShot( Direct3D* pD3D )
{
    HRESULT retval     = S_OK;
    LPDIRECT3DSURFACE9 src = NULL;
    FILE *fptr;
    bool  doneLooping = FALSE;
    int   currentShotNum = 0;
    char  numBuffer[4];
    char *nameString   = "Screen";
    char  buffer[80];

    while( !doneLooping )
    {
        // get name ready
        if( currentShotNum < 10)
        {
            sprintf(numBuffer,"00%d",currentShotNum);
        }
        else if( currentShotNum <100)
        {
            sprintf(numBuffer,"0%d",currentShotNum);
        }
        else
        {
            sprintf(numBuffer,"%d",currentShotNum);
        }
    
        sprintf(buffer, "%s%s.bmp",nameString,numBuffer);
        
        fptr = fopen( buffer, "r");
        if( fptr != NULL)
        {        
            fclose(fptr);
            currentShotNum++;
        }
        else
        {
            doneLooping = TRUE;
        }
    }

    pD3D->GetDevice()->GetBackBuffer(0,0, D3DBACKBUFFER_TYPE_MONO, &src);
    retval = D3DXSaveSurfaceToFile(buffer, D3DXIFF_BMP, src, NULL,NULL);

    src->Release();

	return true;
}

void Engine::SetCamera( Camera* pCamera )
{
	if( bDefaultCamera )
	{
		SAFE_DELETE( camera );
		bDefaultCamera = false;
	}

	camera = pCamera;
}

Camera* const Engine::GetCamera()
{
	return camera;
}

INIFile* const Engine::GetSystemINI()
{
	return systemIni;
}

std::string Engine::GetMeshPath()
{
	return meshPath;
}