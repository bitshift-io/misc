/* ====================================
	ACTORMANAGER
This manages all the actors in the world,
and calls update on them, also allows
to add and delete actors
==================================== */

#ifndef _SIPHON_COLLISIONMANAGER_
#define _SIPHON_COLLISIONMANAGER_

#define PLANE_BACKSIDE 0
#define PLANE_FRONT    1
#define PLANE_ON       2

#define PI 3.1415

/*! \class CollisionManager
 *  \brief Deals with collision an response
 *
 * Deals with collision and response of actors
 * and terrain
 */
class CollisionManager
{
public:
	CollisionManager();
	~CollisionManager();

	void Update( float deltaTime, World* pW );
	void ComputeCollisions( CollisionCell* cell );

	void SphereToPolys(COLLISIONMESH* pObj1, Object* pObj2);
	void SphereToSphere(Object* pObj1, Object* pObj2);

	//most of the below functions are taken from:
	//http://www.peroxide.dk/download/tutorials/tut10/pxdtut10.html

	//Return: distance to plane in world units, -1 if no intersection.
	static float RayIntersectPlane(const D3DXVECTOR3& rayVector, const D3DXVECTOR3& rayPoint, const D3DXVECTOR3& planeNormal, const D3DXVECTOR3& planePoint);
	static int ClassifyPoint(const D3DXVECTOR3& point, const D3DXVECTOR3& planeNormal, const D3DXVECTOR3& planePoint);

	//bool CheckPointInTriangle(D3DXVECTOR3 pointIntersect, D3DXVECTOR3 p1, D3DXVECTOR3 p2, D3DXVECTOR3 p3);
	static bool CheckPointInTriangle(const D3DXVECTOR3& pointIntersect, 
				const D3DXVECTOR3& p1, const D3DXVECTOR3& p2, const D3DXVECTOR3& p3);
protected:
};

#endif