#include "stdafx.h"

M3DFile::M3DFile( Direct3D* pD3D, FileManager* pFM, std::string fileName, std::string mapPath, std::string meshPath )
{
	this->pD3D = pD3D;

	this->mapPath = mapPath;
	this->meshPath = meshPath;

	numberCells = 0;
	cellDimension = 0;
	startValue = 0;

	this->pFM = pFM;

	LoadModel(fileName);
}

int M3DFile::GetNumStatics()
{
	return statics.size();
}

void M3DFile::LoadIntoCollisionMesh(COLLISIONMESH* mesh, int section)
{
	//mesh->vertices.resize( colData[section].vertices.size() );

	for(int i=0; i < colData[section].vertices.size(); i++ )
	{
		mesh->vertices.push_back( colData[section].vertices[i] );
	}

	//Console::GetInstance().Log("col vert size: %i", colData[section].vertices.size());	
}

void M3DFile::LoadIntoMesh(MESH* mesh, int section)
{
	mesh->fvf = VERT_POS_NORM_TEX::FVF;
	mesh->vertexSize = sizeof( VERT_POS_NORM_TEX );
	mesh->numOfIndices = data[section].indexList.size();
	mesh->numOfVertices = data[section].vertexList.size();
	mesh->numOfPolygons = data[section].indexList.size() / 3;

	//FILL INDEX BUFFER
	if( FAILED( pD3D->GetDevice()->CreateIndexBuffer( 
		sizeof(WORD)* data[section].indexList.size(), D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16, D3DPOOL_DEFAULT, &mesh->pIndexBuffer, 0) ))
	{
		return;
	}

	WORD* index;
	if( FAILED( mesh->pIndexBuffer->Lock(0, 0, (void**)&index, 0) ))
	{
		return;
	}

	for( int n = 0; n < data[section].indexList.size(); n++ )
	{
		index[n] = data[0].indexList[n];
	}

	mesh->pIndexBuffer->Unlock();

	//FILL VERTEX BUFFER
	if( FAILED( pD3D->GetDevice()->CreateVertexBuffer( 
		mesh->vertexSize * data[section].vertexList.size(), D3DUSAGE_WRITEONLY,
		mesh->fvf , D3DPOOL_DEFAULT, &mesh->pVertexBuffer, 0) ) )
	{	
		return;
	}

	VERT_POS_NORM_TEX* vertex;	
	if( FAILED( mesh->pVertexBuffer->Lock(0, 0, (void**)&vertex, 0) ))
	{
		return;
	}

	for( int v = 0; v < data[section].vertexList.size(); v++ )
	{
		vertex[v].x = data[section].vertexList[v].p.x;
		vertex[v].y = data[section].vertexList[v].p.y;
		vertex[v].z = data[section].vertexList[v].p.z;

		vertex[v].tu = data[section].vertexList[v].tu;
		vertex[v].tv = data[section].vertexList[v].tv;

		vertex[v].nx = data[section].vertexList[v].n.x;
		vertex[v].ny = data[section].vertexList[v].n.y;
		vertex[v].nz = data[section].vertexList[v].n.z;
	}

	mesh->pVertexBuffer->Unlock();
}

void M3DFile::LoadIntoModel(RenderCell& model, int section)
{
	//load up model
	MESH* m = new MESH();
	model.SetMesh(m);
	LoadIntoMesh( model.GetMesh(), section);

	//now load up texture data
	model.pTerrainTextures.resize( terrainTextures.size() );

	for( int t = 0; t < terrainTextures.size(); t++ )
	{
		model.pTerrainTextures[t] = terrainTextures[t];	
	}
}
/*
void M3DFile::LoadIntoMesh(Mesh* mesh, int section)
{
	mesh->CreateFromStruct( data[section] );
}*/

bool M3DFile::LoadStatics(Model* mesh, int number)
{
	mesh->LoadModel( pFM, statics[number].file );
	//mesh->SetMesh( pFM->GetMesh( statics[number].file ) );

	/*
	MESH* m = new MESH();
	mesh->SetMesh(m);

	std::string actualPath = meshPath + statics[number].file;
	A3DFile staticFile( pD3D, pFM, actualPath, m); //mesh->GetMesh() */

	mesh->SetPosition( statics[number].x, statics[number].y, statics[number].z);
	mesh->SetRotation( statics[number].yaw, statics[number].pitch, statics[number].roll);
	mesh->SetScale( statics[number].scalex, statics[number].scaley, statics[number].scalez);
	
	/*
	std::vector<M3DStatic>::iterator staticIt;
	for( staticIt = statics.begin(); staticIt != statics.end(); staticIt++ )
	{
		
		Mesh* meshStatic = new Mesh();
		A3DFile2 staticFile( staticIt->file, meshStatic );

		Console::GetInstance().Log("static position:%f %f %f", staticIt->x, staticIt->y, staticIt->z);
		Console::GetInstance().Log("static rotation:%f %f %f", staticIt->yaw, staticIt->pitch, staticIt->roll);
		meshStatic->SetPosition( staticIt->x, staticIt->y, staticIt->z);
		meshStatic->SetRotation( staticIt->yaw, staticIt->pitch, staticIt->roll);

		//ObjectManager::GetInstance().AddObject( (Object*)meshStatic );
	}*/

	return true;
}

void M3DFile::ProcessData()
{
	data.resize( meshes.size() );

	std::vector<A3DMesh>::iterator meshIt;

	//loop through meshes
	int m;
	
	for( meshIt = meshes.begin(), m = 0; meshIt != meshes.end(); meshIt++, m++ )
	{
		
		//loop through all faces
		for( int i=0; i < meshIt->face.size(); i++ )
		{
			STANDARD_VERTEX vert;
			WORD idx = meshIt->face[i].a;
			WORD texIdx = meshIt->textureFace[i].a;
			
			vert.p.x = meshIt->vertex[ idx ].a;
			vert.p.y = meshIt->vertex[ idx ].b;
			vert.p.z = meshIt->vertex[ idx ].c;

			vert.n.x = meshIt->normal[ idx ].a;
			vert.n.y = meshIt->normal[ idx ].b;
			vert.n.z = meshIt->normal[ idx ].c;

			vert.tu = meshIt->textureVert[ texIdx ].a;
			vert.tv = meshIt->textureVert[ texIdx ].b;

			data[m].vertexList.push_back( vert );
			data[m].indexList.push_back( data[m].vertexList.size() - 1 );


			idx = meshIt->face[i].b;
			texIdx = meshIt->textureFace[i].b;

			vert.p.x = meshIt->vertex[ idx ].a;
			vert.p.y = meshIt->vertex[ idx ].b;
			vert.p.z = meshIt->vertex[ idx ].c;

			vert.n.x = meshIt->normal[ idx ].a;
			vert.n.y = meshIt->normal[ idx ].b;
			vert.n.z = meshIt->normal[ idx ].c;

			vert.tu = meshIt->textureVert[ texIdx ].a;
			vert.tv = meshIt->textureVert[ texIdx ].b;

			data[m].vertexList.push_back( vert );
			data[m].indexList.push_back( data[m].vertexList.size() - 1 );


			idx = meshIt->face[i].c;
			texIdx = meshIt->textureFace[i].c;

			vert.p.x = meshIt->vertex[ idx ].a;
			vert.p.y = meshIt->vertex[ idx ].b;
			vert.p.z = meshIt->vertex[ idx ].c;

			vert.n.x = meshIt->normal[ idx ].a;
			vert.n.y = meshIt->normal[ idx ].b;
			vert.n.z = meshIt->normal[ idx ].c;

			vert.tu = meshIt->textureVert[ texIdx ].a;
			vert.tv = meshIt->textureVert[ texIdx ].b;
				
			data[m].vertexList.push_back( vert );
			data[m].indexList.push_back( data[m].vertexList.size() - 1 );
		}
		
		//set up some temporary material/texture
		//data[m].texture = pFM->GetTexture("grass.dds");
	
		//SET UP A STANDARD MATERIAL
		D3DMATERIAL9 standardMat;

		D3DCOLORVALUE color;
		color.r = 0.5;
		color.g = 0.5;
		color.b = 0.5;
		color.a = 0.5;

		standardMat.Diffuse = color; //D3DCOLORVALUE(100,100,100, 100);
		standardMat.Ambient = color; //D3DCOLORVALUE(100,100,100, 100);
		standardMat.Specular = color; //D3DCOLORVALUE(100,100,100, 100);
		standardMat.Emissive = color; //D3DCOLORVALUE(100,100,100, 100);
		standardMat.Power = 0.5;

		data[m].material = standardMat;

		data[m].fvf = STANDARD_FVF;
		data[m].vertexSize = sizeof(STANDARD_VERTEX);
	}


	//process collision data
	std::vector<A3DMesh>::iterator colMeshIt; // colData

	colData.resize( colMeshes.size() );

	for( colMeshIt = colMeshes.begin(), m = 0; colMeshIt != colMeshes.end(); colMeshIt++, m++ )
	{
		//loop through faces
		for( int i=0; i < colMeshIt->face.size(); i++ )
		{
			D3DXVECTOR3 vert = D3DXVECTOR3(0,0,0);
			vert.x = colMeshIt->vertex[ colMeshIt->face[i].a ].a;
			vert.y = colMeshIt->vertex[ colMeshIt->face[i].a ].b;
			vert.z = colMeshIt->vertex[ colMeshIt->face[i].a ].c;

			colData[m].vertices.push_back( vert );

			vert.x = colMeshIt->vertex[ colMeshIt->face[i].b ].a;
			vert.y = colMeshIt->vertex[ colMeshIt->face[i].b ].b;
			vert.z = colMeshIt->vertex[ colMeshIt->face[i].b ].c;

			colData[m].vertices.push_back( vert );

			vert.x = colMeshIt->vertex[ colMeshIt->face[i].c ].a;
			vert.y = colMeshIt->vertex[ colMeshIt->face[i].c ].b;
			vert.z = colMeshIt->vertex[ colMeshIt->face[i].c ].c;

			colData[m].vertices.push_back( vert );
		}
	}

	//process terrain textures
	terrainTextures.resize( subMaterials.size() );

	for( int t = 0; t < subMaterials.size(); t++ )
	{
		terrainTextures[t] = new TERRAINTEXTURE();
		terrainTextures[t]->pTexture = pFM->GetTexture( subMaterials[t].textureFile );
		terrainTextures[t]->pBlendTexture = pFM->GetTexture( subMaterials[t].blendTextureFile );
		terrainTextures[t]->uTile = subMaterials[t].uTileTexture;
		terrainTextures[t]->vTile = subMaterials[t].vTileTexture;
	}

	Log( "number subtextures loaded: %i", terrainTextures.size() ); 
}

void M3DFile::LoadModel(std::string fileName)
{
	pFile = 0;
	std::string actualPath = mapPath + fileName;
	pFile = fopen(actualPath.c_str(), "r");

	if( !pFile )
	{
		exit(0);
	}


//cos*	Engine::GetInstance().GetConsole().Log("loading map: %s", actualPath.c_str() );

	//start reading till we find a *something
	while( !feof(pFile) )
	{
		char strWord[255] = {0};
		// Get each word in the file
		fscanf(pFile, "%s", &strWord);

//cos*		Engine::GetInstance().GetConsole().Log("%s", strWord);

		// Check if we hit a new mesh
		if( strcmp(strWord, M3D_RENDER) == 0 )
		{
			ReadRenderData();
		}
		if( strcmp(strWord, M3D_COLLISION) == 0 )
		{
			ReadCollisionData();
		}
		/*
		if( strcmp(strWord, A3D_MESH) == 0 )
		{
			ReadMesh();
		}*/
		if( strcmp(strWord, A3D_MATERIAL) == 0 )
		{
			ReadMaterial();
		}
		if( strcmp(strWord, M3D_STATIC) == 0 )
		{
//cos*			Engine::GetInstance().GetConsole().Log("READING STATIC");
			ReadStatic();
		}
		else
		{
			// Go to the next line
			fgets(strWord, 100, pFile);
		}	
	}

	fclose(pFile);

	ProcessData();
}

void M3DFile::ReadRenderData()
{
	char strWord[255] = {0};

	while( !feof(pFile) && strcmp(strWord, A3D_END) != 0  )
	{
		fscanf(pFile, "%s", &strWord);

//cos*		Engine::GetInstance().GetConsole().Log("RNDR: %s", strWord);

		// Check if we hit a new mesh
		if( strcmp(strWord, A3D_MESH) == 0 )
		{
//cos*			Engine::GetInstance().GetConsole().Log("READING RENDER MESH");
			ReadRenderMesh();
		}
		if( strcmp(strWord, M3D_HEAD) == 0 )
		{
//cos*			Engine::GetInstance().GetConsole().Log("READING HEADER");
			ReadRenderHeader();
		}
		if( strcmp(strWord, A3D_END) == 0 )
		{
			return;
		}
		else
		{
			// Go to the next line
			fgets(strWord, 100, pFile);
		}
	}
}

void M3DFile::ReadCollisionData()
{
	char strWord[255] = {0};

	while( !feof(pFile) && strcmp(strWord, A3D_END) != 0  )
	{
		fscanf(pFile, "%s", &strWord);

		// Check if we hit a new mesh
		if( strcmp(strWord, A3D_MESH) == 0 )
		{
			//READ COLLSION MESH
			ReadCollisionMesh();
		}
		if( strcmp(strWord, M3D_HEAD) == 0 )
		{
			//READ COLLISION HEADER
			ReadCollisionHeader();
		}
		if( strcmp(strWord, A3D_END) == 0 )
		{
			return;
		}
		else
		{
			// Go to the next line
			fgets(strWord, 100, pFile);
		}
	}
}

void M3DFile::ReadCollisionHeader()
{
	char strWord[255] = {0};

	while( !feof(pFile) && strcmp(strWord, A3D_END) != 0 )
	{
		fscanf(pFile, "%s", &strWord);

		if( strcmp(strWord, M3D_NUMBERCELLS) == 0 )
		{
			fscanf(pFile, "%i", &colNumberCells);
		}
		if( strcmp(strWord, M3D_CELLDIMENSION) == 0 )
		{
			fscanf(pFile, "%f", &colCellDimension);
		}
		if( strcmp(strWord, M3D_STARTVALUE) == 0 )
		{
			fscanf(pFile, "%f", &startValue);
		}
	}
}

void M3DFile::ReadCollisionMesh()
{
	char strWord[255] = {0};
	int num = 0;

	A3DMesh mesh;

	while( strcmp(strWord, A3D_END) != 0 && !feof(pFile) )
	{
		// Get each word in the file
		fscanf(pFile, "%s", &strWord);

		if( strcmp(strWord, A3D_END) != 0 )
			fscanf(pFile, "%i", &num);

		/*
		if( strcmp(strWord, A3D_NORMAL) == 0 )
		{
			ReadNormals( mesh, num );
		}*/
		if( strcmp(strWord, A3D_VERTEX) == 0 )
		{
			ReadVertices( mesh, num );
		}
		if( strcmp(strWord, A3D_FACE) == 0 )
		{
			ReadFaces( mesh, num );
		}/*
		if( strcmp(strWord, A3D_TEXTUREFACE) == 0 )
		{
			ReadTextureFaces( mesh, num );		
		}
		if( strcmp(strWord, A3D_TEXTUREVERTEX) == 0 )
		{
			ReadTextureVertices( mesh, num );
		}*/
	}
	colMeshes.push_back( mesh );
}

void M3DFile::ReadRenderHeader()
{
	char strWord[255] = {0};

	while( !feof(pFile) && strcmp(strWord, A3D_END) != 0 )
	{
		fscanf(pFile, "%s", &strWord);

		if( strcmp(strWord, M3D_NUMBERCELLS) == 0 )
		{
			fscanf(pFile, "%i", &numberCells);
		}
		if( strcmp(strWord, M3D_CELLDIMENSION) == 0 )
		{
			fscanf(pFile, "%f", &cellDimension);
		}
		if( strcmp(strWord, M3D_STARTVALUE) == 0 )
		{
			fscanf(pFile, "%f", &startValue);
		}
	}
}

void M3DFile::ReadRenderMesh()
{
	char strWord[255] = {0};
	int num = 0;

	A3DMesh mesh;

	while( strcmp(strWord, A3D_END) != 0 && !feof(pFile) )
	{
		// Get each word in the file
		fscanf(pFile, "%s", &strWord);

		if( strcmp(strWord, A3D_END) != 0 )
			fscanf(pFile, "%i", &num);

		
		if( strcmp(strWord, A3D_NORMAL) == 0 )
		{
			ReadNormals( mesh, num );
		}
		if( strcmp(strWord, A3D_VERTEX) == 0 )
		{
			ReadVertices( mesh, num );
		}
		if( strcmp(strWord, A3D_FACE) == 0 )
		{
			ReadFaces( mesh, num );
		}
		if( strcmp(strWord, A3D_TEXTUREFACE) == 0 )
		{
			ReadTextureFaces( mesh, num );		
		}
		if( strcmp(strWord, A3D_TEXTUREVERTEX) == 0 )
		{
			ReadTextureVertices( mesh, num );
		}
	}
	meshes.push_back( mesh );
}

void M3DFile::ReadVertices( A3DMesh& mesh, int num )
{
	char strWord[255] = {0};

	for(int i=0; i < num; i++)
	{
		A3DFloat3 vertex;

		fgets(strWord, 100, pFile);
		fscanf(pFile, "%f %f %f", &vertex.a, &vertex.b, &vertex.c );
		
		mesh.vertex.push_back( vertex );
	}
}

void M3DFile::ReadFaces( A3DMesh& mesh, int num )
{
	char strWord[255] = {0};

	for(int i=0; i < num; i++)
	{
		A3DInt3 face;

		fgets(strWord, 100, pFile);
		fscanf(pFile, "%i %i %i", &face.a, &face.b, &face.c );
		
		mesh.face.push_back( face );
	}
}

void M3DFile::ReadTextureFaces( A3DMesh& mesh, int num )
{
	char strWord[255] = {0};

//cos*	Engine::GetInstance().GetConsole().Log("reading %i texture faces", num);

	for(int i=0; i < num; i++)
	{
		A3DInt3 texFace;

		fgets(strWord, 100, pFile);
		fscanf(pFile, "%i %i %i", &texFace.a, &texFace.b, &texFace.c );
		
		mesh.textureFace.push_back( texFace );
	}
}

void M3DFile::ReadTextureVertices( A3DMesh& mesh, int num )
{
	char strWord[255] = {0};

//cos*	Engine::GetInstance().GetConsole().Log("reading %i texture verts", num);

	for(int i=0; i < num; i++)
	{
		A3DFloat3 texVert;

		fgets(strWord, 100, pFile);
		fscanf(pFile, "%f %f %f", &texVert.a, &texVert.b, &texVert.c );
		
		mesh.textureVert.push_back( texVert );
	}
}

void M3DFile::ReadNormals( A3DMesh& mesh, int num )
{
	char strWord[255] = {0};

//cos*	Engine::GetInstance().GetConsole().Log("reading %i normals", num);

	for(int i=0; i < num; i++)
	{
		A3DFloat3 normal;

		fgets(strWord, 100, pFile);
		fscanf(pFile, "%f %f %f", &normal.a, &normal.b, &normal.c );
		
		mesh.normal.push_back( normal );
	}
}

void M3DFile::ReadStatic()
{
	M3DStatic model;

	char strWord[255] = {0};

	while( !feof(pFile) && strcmp(strWord, A3D_END) != 0 )
	{
		fscanf(pFile, "%s", &strWord);

		if( strcmp(strWord, SKA_ROTATION) == 0 )
		{
			fscanf(pFile, "%f %f %f", &model.yaw, &model.pitch, &model.roll);
		}
		if( strcmp(strWord, SKA_POSITION) == 0 )
		{
			fscanf(pFile, "%f %f %f", &model.x, &model.y, &model.z);
		}
		if( strcmp(strWord, M3D_MODEL) == 0 )
		{
			fscanf(pFile, "%s", &strWord);
			model.file = std::string( strWord );
		}
		if( strcmp(strWord, SKA_SCALE) == 0 )
		{
			fscanf(pFile, "%f %f %f", &model.scalex, &model.scaley, &model.scalez);
		}
	}

	statics.push_back( model );
}

int M3DFile::GetNumberCells()
{
	return numberCells;
}

float M3DFile::GetCellDimensions()
{
	return cellDimension;
}

float M3DFile::GetStartValue()
{
	return startValue;
}

int M3DFile::GetNumberCollisionCells()
{
	return colNumberCells;
}

float M3DFile::GetCollisionCellDimensions()
{
	return colCellDimension;
}

void M3DFile::ReadMaterial()
{
	char strWord[255] = {0};

	while( !feof(pFile) && strcmp(strWord, A3D_END) != 0 )
	{
		fscanf(pFile, "%s", &strWord);

		if( strcmp(strWord, M3D_SUBMATERIAL) == 0 )
		{
			ReadSubMaterial();
		}
	}
}

void M3DFile::ReadSubMaterial()
{
	M3DSubMaterial material;

	char strWord[255] = {0};

	while( !feof(pFile) && strcmp(strWord, A3D_END) != 0 )
	{
		fscanf(pFile, "%s", &strWord);

		if( strcmp(strWord, A3D_TEXTURE) == 0 )
		{
			ReadTexture( material, 0 );
		}
		if(  strcmp(strWord, M3D_BLENDTEXTURE) == 0 )
		{
			ReadTexture( material, 1 );
		}
	}

	subMaterials.push_back( material );
}

void M3DFile::ReadTexture( M3DSubMaterial& material, int textureType )
{
	char strWord[255] = {0};

	fscanf(pFile, "%s", &strWord);
	if( textureType == 0 )
	{
		material.textureFile = std::string( strWord );
		Log("texture: %s", material.textureFile.c_str() );
	}
	else
	{
		material.blendTextureFile = std::string( strWord );
		Log("texture: %s", material.blendTextureFile.c_str() );
	}

	while( !feof(pFile) && strcmp(strWord, A3D_END) != 0 )
	{
		fscanf(pFile, "%s", &strWord);

		if( strcmp(strWord, M3D_UTILE) == 0 )
		{
			if( textureType == 0 )
				fscanf( pFile, "%f", &material.uTileTexture );
		}
		if( strcmp(strWord, M3D_VTILE) == 0 )
		{
			if( textureType == 0 )
				fscanf( pFile, "%f", &material.vTileTexture );
		}
	}
}

//std::vector<M3DSubMaterial> subMaterials;