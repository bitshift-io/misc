/* ====================================
	OBJECTMANAGER
This manages all the objects in the world,
and calls update on them, also allows
to add and delete actors
==================================== */

#ifndef _SIPHON_OBJECTMANAGER_
#define _SIPHON_OBJECTMANAGER_

/*! \class ObjectManager
 *  \brief Tracks and updates objects
 *
 * Tracks and updates all objects in the world
 * by adding an object to this it will update the object with
 * the appropriate engine pointer
 */
class ObjectManager
{
public:
	ObjectManager();
	~ObjectManager();

	std::list<Object*> objectList; //out list of actors
	std::list<Object*> permObjectList; //objects that are always rendered (eg. hud)
						// these are rendered very last with z disabled

	void PreRender( RenderManager* pRM, BOUNDS* camera ); //pre render those objects that always need to be rendered
	void Update( float const deltaTime, World* pW, DirectInput* pDI ); //deletes actors that need deleting, and updates actors
	bool AddObject(Object* newObject, bool alwaysRender = false );
	bool RemoveObject();

protected:
	RenderManager* pRM;
};

#endif