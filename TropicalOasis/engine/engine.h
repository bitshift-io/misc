#ifndef _SIPHON_ENGINE_
#define _SIPHON_ENGINE_

#include "stdafx.h"

/*! \class Engine
 *	\brief Engine glue
 *
 * This class is responsible for setting up all parts of the engine
 * as well as shutting everything down
 * it is then responsible for doing the guts of everything
 * leaving the programmer just to extend the game class
 * for making custom games and by extending this class
 * you could make a main menu to choose which game to load
 */
class Engine //: public Unknown
{
public:
	Engine( HINSTANCE hInstance, std::string cmdLine );
	Engine( HWND hWnd );
	~Engine();

	//void Start();
	void GameLoop();
	//void Render();
	//void Update();

	/*
	//this should be part of the file manager!!!
	std::string GetTexturePath();
	std::string GetMapPath();
	std::string GetMeshPath();
	std::string GetEffectPath();

	//this should be part of the file manager!!!
	void SetTexturePath( std::string path );
	void SetMapPath( std::string path );
	void SetMeshPath( std::string path );
	void SetEffectPath( std::string path );

	std::string GetCmdLineParam( std::string param );*/

	HWND GetHandle();
	HINSTANCE GetHInstance();

	bool SaveScreenShot( Direct3D* pD3D );

	//void Exit();

	//void Shutdown();
/*
	Time& getTime();
	RenderManager& getRenderManager();
	
	Console& getConsole();
	Direct3D& getDirect3D();
	DirectInput& getDirectInput();
	
	
	
*/
	void DoGameStuff();

	std::string GetMeshPath();

	INIFile* const GetSystemINI();

	void SetCamera( Camera* pCamera );
	Camera* const GetCamera();

	ObjectManager* const GetObjectManager();
	FileManager* const GetFileManager();
	World* const GetWorld();
	CollisionManager* const GetCollisionManager();
	Direct3D* const GetDirect3D();

protected:
	
	const char* Con(int argc, const char* argv[]);

	bool bDefaultCamera;
	Camera* camera;

	HWND hWnd;
	HINSTANCE hInstance;
	WNDCLASSEX wc;

	float deltaTime;
	long unsigned int time;

	bool bExit;

	int screenNum;
	std::string curScreenshot;

	//paths
	std::string texturePath;
	std::string mapPath;
	std::string meshPath;
	std::string effectPath;



	std::map< std::string, std::string > cmdLineParams;

	//pointer to a list of game modes
	std::vector<Game*> games;
	int activeGame;

	/*
	Time* timer;
	
	
	
	
	
	Console* console;
	*/
	Direct3D* direct3d;
	FileManager* fileManager;
	RenderManager* renderManager;
	DirectInput* directInput;
	ObjectManager* objectManager;
	World* world;
	CollisionManager* collisionManager; 

	INIFile* systemIni; 
};

#endif