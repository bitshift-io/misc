#include "stdafx.h"

Model::Model() : Object( OBJ_MODEL )
{
	classType = OBJ_MODEL;

	pEffect = 0;
	pTexture = 0;
	pMesh = 0;
	bDoubleSided = false;

	if( pEffect != 0 || pTexture != 0 || pMesh != 0 )
		exit(0);
}

Model::~Model()
{

}

void Model::SetVariables( int pass, LPD3DXEFFECT pEffect )
{

}

bool Model::LoadModel( FileManager* pFM2, std::string file )
{
	pMesh = pFM2->GetMesh( file );

//	SetMesh( m );
	pTexture = pMesh->pTexture;
	pMaterial = pMesh->pMaterial;
	boundSphereRadius = pMesh->sphereBoundRadius;

	return true;
}

MESH* Model::GetMesh()
{
	return pMesh;
}

void Model::SetMesh( MESH* pMesh )
{
	this->pMesh = pMesh;
}

LPD3DXEFFECT Model::GetEffect()
{
	return pEffect;
}

TEXTURE* Model::GetTexture()
{
	return pTexture;
}

D3DMATERIAL9 Model::GetMaterial()
{
	return pMaterial;
}

void Model::PreRender( RenderManager* pRM, BOUNDS* cameraBounds, bool alwaysRendered )
{
	if( boundSphereRadius <= 0 )
	{
		pRM->PreRender( pEffect, pTexture, pMesh->fvf, pMesh, this, alwaysRendered );
	}
	else //cull
	{ 
		//see if sphere is inside camerabounds OR if it intersects a plane
		D3DXVECTOR3 normal;
		
		//near plane of frustum
		normal = D3DXVECTOR3( cameraBounds->planes[0].a, cameraBounds->planes[0].b, cameraBounds->planes[0].c);
		if( CollisionManager::ClassifyPoint( position, normal, cameraBounds->points[0] ) == PLANE_FRONT )
		{
			//far clip plane
			normal = D3DXVECTOR3( cameraBounds->planes[1].a, cameraBounds->planes[1].b, cameraBounds->planes[1].c);
			if( CollisionManager::ClassifyPoint( position, normal, cameraBounds->points[6] ) == PLANE_FRONT )
			{
				//left clip plane
				normal = D3DXVECTOR3( cameraBounds->planes[2].a, cameraBounds->planes[2].b, cameraBounds->planes[2].c);
				if( CollisionManager::ClassifyPoint( position, normal, cameraBounds->points[2] ) == PLANE_FRONT )
				{
					//right clip plane
					normal = D3DXVECTOR3( cameraBounds->planes[3].a, cameraBounds->planes[3].b, cameraBounds->planes[3].c);
					if( CollisionManager::ClassifyPoint( position, normal, cameraBounds->points[7] ) == PLANE_FRONT )
					{
						//top clip plane
						normal = D3DXVECTOR3( cameraBounds->planes[4].a, cameraBounds->planes[4].b, cameraBounds->planes[4].c);
						if( CollisionManager::ClassifyPoint( position, normal, cameraBounds->points[2] ) == PLANE_FRONT )
						{
							//bottom clip plane
							normal = D3DXVECTOR3( cameraBounds->planes[5].a, cameraBounds->planes[5].b, cameraBounds->planes[5].c);
							if( CollisionManager::ClassifyPoint( position, normal, cameraBounds->points[1] ) == PLANE_FRONT )
							{
								pRM->PreRender( pEffect, pTexture, pMesh->fvf, pMesh, this, alwaysRendered );
							}
							else
							{
								D3DXVec3Normalize( &normal, &normal);
								float distToPlane = CollisionManager::RayIntersectPlane( -normal, position, normal, cameraBounds->points[1]);

								if( distToPlane <= boundSphereRadius )
								{
									//calculate sphere-plane intersection point
									D3DXVECTOR3 sphereIntersectPoint = position - (normal * distToPlane);

									if( CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[1], cameraBounds->points[0], cameraBounds->points[4]) ||
										CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[1], cameraBounds->points[4], cameraBounds->points[5]) )
									{
										pRM->PreRender( pEffect, pTexture, pMesh->fvf, pMesh, this, alwaysRendered );
									}
								}
							}
						}
						else
						{
							D3DXVec3Normalize( &normal, &normal);
							float distToPlane = CollisionManager::RayIntersectPlane( -normal, position, normal, cameraBounds->points[2]);

							if( distToPlane <= boundSphereRadius )
							{
								//calculate sphere-plane intersection point
								D3DXVECTOR3 sphereIntersectPoint = position - (normal * distToPlane);

								if( CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[2], cameraBounds->points[3], cameraBounds->points[6]) ||
									CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[3], cameraBounds->points[6], cameraBounds->points[7]) )
								{
									pRM->PreRender( pEffect, pTexture, pMesh->fvf, pMesh, this, alwaysRendered );
								}
							}
						}
					}
					else
					{
						D3DXVec3Normalize( &normal, &normal);
						float distToPlane = CollisionManager::RayIntersectPlane( -normal, position, normal, cameraBounds->points[7]);

						if( distToPlane <= boundSphereRadius )
						{
							//calculate sphere-plane intersection point
							D3DXVECTOR3 sphereIntersectPoint = position - (normal * distToPlane);

							if( CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[7], cameraBounds->points[3], cameraBounds->points[5]) ||
								CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[5], cameraBounds->points[3], cameraBounds->points[1]) )
							{
								pRM->PreRender( pEffect, pTexture, pMesh->fvf, pMesh, this, alwaysRendered );
							}
						}					
					}
				}
				else
				{
					D3DXVec3Normalize( &normal, &normal);
					float distToPlane = CollisionManager::RayIntersectPlane( -normal, position, normal, cameraBounds->points[2]);

					if( distToPlane <= boundSphereRadius )
					{
						//calculate sphere-plane intersection point
						D3DXVECTOR3 sphereIntersectPoint = position - (normal * distToPlane);
						if( CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[2], cameraBounds->points[6], cameraBounds->points[4]) ||
							CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[2], cameraBounds->points[4], cameraBounds->points[0]) )
						{
							pRM->PreRender( pEffect, pTexture, pMesh->fvf, pMesh, this, alwaysRendered );
						}
					}
				}
			}
			else
			{
				D3DXVec3Normalize( &normal, &normal);
				float distToPlane = CollisionManager::RayIntersectPlane( -normal, position, normal, cameraBounds->points[6]);

				if( distToPlane <= boundSphereRadius )
				{
					//calculate sphere-plane intersection point
					D3DXVECTOR3 sphereIntersectPoint = position - (normal * distToPlane);

					if( CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[6], cameraBounds->points[7], cameraBounds->points[5]) ||
						CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[6], cameraBounds->points[5], cameraBounds->points[4]) )
					{
						pRM->PreRender( pEffect, pTexture, pMesh->fvf, pMesh, this, alwaysRendered );
					}
				}
			}
		}
		else
		{
			D3DXVec3Normalize( &normal, &normal);
			float distToPlane = CollisionManager::RayIntersectPlane( -normal, position, normal, cameraBounds->points[0]);

			if( distToPlane <= boundSphereRadius )
			{
				//calculate sphere-plane intersection point
				D3DXVECTOR3 sphereIntersectPoint = position - (normal * distToPlane);

				if( CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[0], cameraBounds->points[1], cameraBounds->points[2]) ||
					CollisionManager::CheckPointInTriangle(sphereIntersectPoint, cameraBounds->points[1], cameraBounds->points[2], cameraBounds->points[3]) )
				{
					//exit(0);
					pRM->PreRender( pEffect, pTexture, pMesh->fvf, pMesh, this, alwaysRendered );
				}
			}
		}
	}
}

void Model::SetEffect( LPD3DXEFFECT pEffect )
{
	this->pEffect = pEffect;
}

void Model::SetTexture( TEXTURE* pTexture )
{
	this->pTexture = pTexture;
}

void Model::SetMaterial( D3DMATERIAL9& pMaterial )
{
	this->pMaterial = pMaterial;
}

int Model::Render( Direct3D* pD3D, Camera* pCamera, MESH* setMesh, LPD3DXEFFECT pEffect, TEXTURE* pTexture )
{
	if( setMesh->bDoubleSided )
		pD3D->GetDevice()->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

	HRESULT hr;
	if (FAILED(hr = pD3D->GetDevice()->DrawIndexedPrimitive( setMesh->renderType,
					  0, 0, setMesh->numOfVertices ,
					  0,  setMesh->numOfPolygons )))
	{
		DXTRACE_ERR( "DrawIndexedPrimitive", hr );
		return 0;
	}

	if( setMesh->bDoubleSided )
		pD3D->GetDevice()->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );

	return setMesh->numOfPolygons;
}

bool Model::IsA( OBJECT_TYPE type )
{
	if( type == classType )
		return true;
	else
		return Object::IsA( type );
}
