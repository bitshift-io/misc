#ifndef _SIPHON_FILEMANAGER_
#define _SIPHON_FILEMANAGER_

struct LOADEDFILE
{
	std::string name;
	unsigned short int references; //how many times this is referenced

	LOADEDFILE()
	{
		references = 0;
	}
};

struct TEXTURE
{
	LPDIRECT3DTEXTURE9 pTexture;
	bool bTransparent;

	TEXTURE()
	{
		pTexture = 0;
		bTransparent = false;
	}
};

struct TEXTURECONT : LOADEDFILE
{
	TEXTURE* pTexture;
	//LPDIRECT3DTEXTURE9 pTexture;
	//bool bIsTransparent;

	TEXTURECONT()
	{
		LOADEDFILE::LOADEDFILE();
		//pTexture = 0;
		//bIsTransparent = false;
		pTexture = 0;
	}
};

struct COLLISIONMESH
{
	std::vector<D3DXVECTOR3> vertices;
};

struct MESH
{
	WORD fvf;
	WORD vertexSize;
	WORD numOfIndices;
	WORD numOfPolygons;
	WORD numOfVertices;

	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer;
	LPDIRECT3DINDEXBUFFER9 pIndexBuffer;

	//this texture and material are only the "defaults"
	TEXTURE* pTexture;
	D3DMATERIAL9 pMaterial;

	float sphereBoundRadius;

	D3DPRIMITIVETYPE renderType; //so we can use point sprites
	bool bDoubleSided;

	MESH()
	{
		renderType = D3DPT_TRIANGLELIST;//D3DPT_TRIANGLELIST;
		fvf = VERT_POS_NORM_TEX::FVF;
		vertexSize = sizeof(VERT_POS_NORM_TEX);
		bDoubleSided = false;
		sphereBoundRadius = 0;
	}
};

struct MESHCONT : LOADEDFILE
{
	MESH *pMesh;

	MESHCONT() : LOADEDFILE()
	{
		pMesh = 0;
	}
};

struct SOUND : LOADEDFILE
{

};

struct EFFECT : LOADEDFILE
{
	LPD3DXEFFECT pEffect;

	EFFECT()
	{
		LOADEDFILE::LOADEDFILE();
		pEffect = 0;
	}
};

enum ETex
{
	TEX_SOLID,
	TEX_TRANSPARENT,
	TEX_ALL
};

typedef std::vector<TEXTURECONT> STL_CONTAINER_TEXTURE;
typedef std::vector<EFFECT> STL_CONTAINER_EFFECT;
typedef std::vector<MESHCONT> STL_CONTAINER_MESH;

/*! \class FileManager
 *	\brief makes sure we only load a single file once
 *
 * This takes care of loading the files, 
 * and storing the data, and giving the data
 * to those that request it, this is a static class
 */
class FileManager
{
public:
	FileManager( Direct3D* pD3D, std::string meshPath, std::string effectPath, std::string texturePath );
	~FileManager();

	TEXTURE* GetTexture( std::string fileName, bool bIsTransparent = false );
	LPD3DXEFFECT GetEffect( std::string fileName );
	MESH* const GetMesh( std::string fileName );

protected:
	STL_CONTAINER_EFFECT effects;
	STL_CONTAINER_TEXTURE textures;
	STL_CONTAINER_MESH meshes;

	std::string meshPath;
	std::string effectPath;
	std::string texturePath;

	Direct3D* pD3D;
};



#endif