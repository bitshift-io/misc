#include "stdafx.h"
#include "light.h"

int Light::lightCount = 0;

Light::Light()
{
	ZeroMemory( &light, sizeof(light) );
	light.Type = D3DLIGHT_POINT;

	light.Falloff = 1.0f;

	light.Diffuse.r = 1.0f;
	light.Diffuse.g = 1.0f;
	light.Diffuse.b = 1.0f;
	light.Diffuse.a = 1.0f;

	light.Ambient.r = 1.0f;
	light.Ambient.g = 1.0f;
	light.Ambient.b = 1.0f;
	light.Ambient.a = 1.0f;

	light.Specular.r = 1.0f;
	light.Specular.g = 1.0f;
	light.Specular.b = 1.0f;
	light.Specular.a = 1.0f;

	light.Attenuation0 = 0.0f; 
	light.Attenuation1 = 0.01f; 
	light.Attenuation2 = 0.0f; 

	light.Position = D3DXVECTOR3(0, 10, 0);
	light.Direction = D3DXVECTOR3(0, 0, 0);

	light.Range = 10000.0f;

	lightIndex = Light::lightCount;
	Light::lightCount++;	
}

Light::~Light()
{

}

D3DLIGHT9* Light::GetLight()
{
	return &light;
}

void Light::ResetLight( Direct3D* pD3D )
{
	pD3D->GetDevice()->SetLight( lightIndex, &light );
	pD3D->GetDevice()->LightEnable( lightIndex, TRUE);
}

void Light::Update(float deltaTime)
{

}