# Microsoft Developer Studio Project File - Name="SiphonEngine" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=SiphonEngine - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "SiphonEngine.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "SiphonEngine.mak" CFG="SiphonEngine - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "SiphonEngine - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "SiphonEngine - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "SiphonEngine - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0xc09 /d "NDEBUG"
# ADD RSC /l 0xc09 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# SUBTRACT LINK32 /debug

!ELSEIF  "$(CFG)" == "SiphonEngine - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /w /W0 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0xc09 /d "_DEBUG"
# ADD RSC /l 0xc09 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "SiphonEngine - Win32 Release"
# Name "SiphonEngine - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\engine\a3dFile.cpp
# End Source File
# Begin Source File

SOURCE=.\AutoCam.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\camera.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\collisionManager.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\console.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\direct3D.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\directInput.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\engine.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\fileManager.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\font.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\game.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\iniFile.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\light.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\m3dFile.cpp
# End Source File
# Begin Source File

SOURCE=.\main.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\mmgr.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\model.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\object.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\objectManager.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\panel.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\particleSystem.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\renderManager.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\screensaver.cpp
# End Source File
# Begin Source File

SOURCE=.\siphon.rc
# End Source File
# Begin Source File

SOURCE=.\engine\skaFile.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\skeletalModel.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\vertexTypes.cpp
# End Source File
# Begin Source File

SOURCE=.\engine\world.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\engine\a3dFile.h
# End Source File
# Begin Source File

SOURCE=.\AutoCam.h
# End Source File
# Begin Source File

SOURCE=.\engine\camera.h
# End Source File
# Begin Source File

SOURCE=.\engine\collisionManager.h
# End Source File
# Begin Source File

SOURCE=.\engine\console.h
# End Source File
# Begin Source File

SOURCE=.\engine\direct3D.h
# End Source File
# Begin Source File

SOURCE=.\engine\directInput.h
# End Source File
# Begin Source File

SOURCE=.\engine\engine.h
# End Source File
# Begin Source File

SOURCE=.\engine\fileManager.h
# End Source File
# Begin Source File

SOURCE=.\engine\font.h
# End Source File
# Begin Source File

SOURCE=.\engine\game.h
# End Source File
# Begin Source File

SOURCE=.\engine\iniFile.h
# End Source File
# Begin Source File

SOURCE=.\engine\light.h
# End Source File
# Begin Source File

SOURCE=.\engine\m3dFile.h
# End Source File
# Begin Source File

SOURCE=.\main.h
# End Source File
# Begin Source File

SOURCE=.\engine\mmgr.h
# End Source File
# Begin Source File

SOURCE=.\engine\model.h
# End Source File
# Begin Source File

SOURCE=.\engine\nommgr.h
# End Source File
# Begin Source File

SOURCE=.\engine\object.h
# End Source File
# Begin Source File

SOURCE=.\engine\objectManager.h
# End Source File
# Begin Source File

SOURCE=.\engine\panel.h
# End Source File
# Begin Source File

SOURCE=.\engine\particleSystem.h
# End Source File
# Begin Source File

SOURCE=.\engine\renderManager.h
# End Source File
# Begin Source File

SOURCE=.\engine\singleton.h
# End Source File
# Begin Source File

SOURCE=.\engine\skaFile.h
# End Source File
# Begin Source File

SOURCE=.\engine\skeletalModel.h
# End Source File
# Begin Source File

SOURCE=.\engine\stdafx.h
# End Source File
# Begin Source File

SOURCE=.\engine\vertexTypes.h
# End Source File
# Begin Source File

SOURCE=.\engine\world.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\siphon.ico
# End Source File
# End Group
# End Target
# End Project
