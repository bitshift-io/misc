#ifndef _SIPHON_AUTOCAM_
#define _SIPHON_AUTOCAM_

class AutoCam : public Camera
{
public:
	AutoCam( Direct3D* pD3D, INIFile* ini, FileManager* pFM, std::string meshPath  );
	~AutoCam();

	virtual void Update( float const deltaTime, World* pW, DirectInput* pDI );

	virtual void StopAnimation();
protected:
	bool bFreeCam;
};

#endif