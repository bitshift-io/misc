
//#define SCREENSAVER

#include <windows.h>
#include "engine/engine.h"
#include "autocam.h"



#ifndef SCREENSAVER

INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR cmdLine, INT)
{
	//Engine* pEngine = new Engine(hInst, cmdLine);

	Engine e(hInst, std::string( cmdLine ) );

	//Model m( e.GetFileManager() );

	//Model* m = new Model();
	//m->LoadModel( e.GetFileManager(), "uzi.A3D" );
	//e.GetFileManager()->GetMesh("uzi.A3D");
	//m->SetMesh( e.GetFileManager()->GetMesh("uzi.A3D") );


	AutoCam* c = new AutoCam( e.GetDirect3D(), e.GetSystemINI(), e.GetFileManager(), e.GetMeshPath() ); 
	e.SetCamera( (Camera*)c );

	/*
	Light l;// = new Light(
	D3DLIGHT9* light = l.GetLight();

	light->Position = D3DXVECTOR3(0, 50, 0);
	light->Direction = D3DXVECTOR3(0, 0, 0);

	light->Range = 50000.0f;

	l.ResetLight( e.GetDirect3D() );*/

	Panel* p = new Panel();
	p->CreatePanel( e.GetFileManager(), e.GetDirect3D(), 120, 120, 1, 2, "banner.dds");
	e.GetObjectManager()->AddObject( (Object*)p, true );

	/*
	//try a skeletal mesh
	SkeletalModel* sm = new SkeletalModel( e.GetFileManager(), e.GetDirect3D() );
	e.GetObjectManager()->AddObject( (Object*)sm );
	*/
	/*
	//try a praticle system
	ParticleSystem* ps = new ParticleSystem( e.GetFileManager(), e.GetDirect3D() );
	e.GetObjectManager()->AddObject( (Object*)ps );*/ 

	e.GameLoop();

	//SAFE_DELETE( c );

    return 0;
}

#endif
