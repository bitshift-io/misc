
#ifndef __CVDEXP__H
#define __CVDEXP__H


//#pragma comment(lib, "ctrl.lib")

//#include "ctrl\ctrl.h"
#include "max.h"
#include "resource.h"
#include "iparamm2.h"
#include "modstack.h"
#include "bmmlib.h"
#include "stdmat.h"

//#include <\MAXSDK\SAMPLES\CONTROLLERS\EULRCTRL.CPP>
//#include "eulrctrl.h"

#include <iskin.h>

#include <map>
#include <list>
#include <vector>


#define	CVDEXP_CLASSID		Class_ID(0x3e816f85, 0x92f7904)


TCHAR *GetString(int id);
extern ClassDesc* GetCVDExportDesc();


struct MATERIAL
{

};

struct VECTOR3
{
	float x,y,z;

	VECTOR3()
	{
		x = y = z = 0;
	}

	VECTOR3(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	bool operator==(VECTOR3 const& rhs) const
	{
		if( rhs.x == x && rhs.y == y && rhs.z == z)
			return true;

		return false;
	}
};

struct STANDARD_VERTEX
{
    VECTOR3 p;
    VECTOR3 n;
    float       tu, tv;

	STANDARD_VERTEX()
	{
		tu = tv = 0;
		n = VECTOR3(0,0,0);
		p = VECTOR3(0,0,0);
	}

	bool operator==(STANDARD_VERTEX const& rhs) const
	{
		if( p == rhs.p && n == rhs.n && tu == rhs.tu && tv == rhs.tv) //
			return true;

		return false;
	}
};

struct STL_TEXTUREBATCH
{
	std::vector<STANDARD_VERTEX> vertexList;
	std::vector<VECTOR3> faceList; //face data
	std::vector<VECTOR3> textureVert;
	std::vector<VECTOR3> textureFace;

	char* texture;
	MATERIAL material;

	int numVertices;
	int numFaces;
};

/*===========================================================================*\
 |	CVDExporter class defn
\*===========================================================================*/

class CVDExporter : public SceneExport {
public:
	CVDExporter();
	~CVDExporter();

	// Preferences values
	int searchtype;

	TCHAR* animFileName;

	bool bExportAsMap;
	bool bExportAsMesh;

	// Used in DoExport
	BOOL exportSelected;
	FILE *fileStream;

	FILE *animFile;
	Interface* ip;

	//map specific data
	std::vector<STL_TEXTUREBATCH> useableData;
	std::vector< std::vector<STL_TEXTUREBATCH> > outputData; //vector of map sections
 

	// Number of extensions we support
	int ExtCount();
	const TCHAR * Ext(int n);

	// The bookkeeping functions
	const TCHAR * LongDesc();
	const TCHAR * ShortDesc();
	const TCHAR * AuthorName();
	const TCHAR * CopyrightMessage();
	const TCHAR * OtherMessage1();
	const TCHAR * OtherMessage2();

	// Version number of this exporter
	unsigned int Version();

	// Show an about box
	void ShowAbout(HWND hWnd);

	// Do the actual export
	int DoExport(const TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts=FALSE, DWORD options=0);

	//export data as a map
	int ExportMap(TCHAR *name, ExpInterface *ei,Interface *i, BOOL suppressPrompts=FALSE, DWORD options=0);

	//export data as a mesh
	int ExportMesh(TCHAR *name, ExpInterface *ei,Interface *i, BOOL suppressPrompts=FALSE, DWORD options=0);

	bool ExportBone(INode* node, Interface *ip, int boneIdx);
	bool ExportBone(INode* node, Interface *ip);
	bool ExportGeomObject(INode* node, Interface *ip);
	bool ExportMapGeomObject(INode* node, Interface *ip);
	bool ExportHelperObject(INode* node, Interface *ip);

	bool ExportMapRenderData( STL_TEXTUREBATCH& newMesh );
	bool ExportMapCollisionData( STL_TEXTUREBATCH& newMesh );

	bool ExportMaterials( Interface *ip );

	//bool ExportMaterials( INode* node, Interface *ip );

	bool ExportHeaderAnimation(Interface *ip);

	// Returns whether we support the extended exporter options
	BOOL SupportsOptions(int ext, DWORD options);

	// Scene enumeration
	BOOL nodeEnum(INode* node, Interface *ip);
	BOOL MapNodeEnum(INode* node, Interface *ip);


	// Configuration file management
	BOOL LoadExporterConfig();
	void SaveExporterConfig();
	TSTR GetConfigFilename();

	std::map<int, INode*> bones;

	std::list<TCHAR*> materialID;

protected:
	static int averageVertsPerRenderCell;
	static int averageVertsPerCollisionCell;
};

#endif