/*===========================================================================*\
 |
 |  FILE:	DoExport.cpp
 |			A simple exporter that scans the scene for our custom data
 |			and if found, exports it to an ASCII file
 |			3D Studio MAX R3.0
 |
 |  AUTH:   Harry Denholm
 |			Developer Consulting Group
 |			Copyright(c) Discreet 1999
 |
 |  HIST:	Started 16-4-99
 |
\*===========================================================================*/
/*===========================================================================*\
 |	This is where all the finding of exportable Custom Vertex Data gets done
 |	All the main processing is done on each node during the enumeration
\*===========================================================================*/


#include "CVDExport.h"

#include <vector>
#include <queue>
#include <set>


int CVDExporter::averageVertsPerRenderCell = 5000;
int CVDExporter::averageVertsPerCollisionCell = 10;

#define THRESHHOLD		1.0f

struct keyFrame
{
	int time;

	bool position;
	bool rotation;

	float x,y,z;
	float yaw, pitch, roll;

	keyFrame()
	{
		position = false;
		rotation = false;
	};

	bool operator<(const keyFrame& rhs) const
	{
		if( time > rhs.time )
			return true;
		else
			return false;
	};
};

/*===========================================================================*\
 |  Determine what options we support
\*===========================================================================*/

BOOL CVDExporter::SupportsOptions(int ext, DWORD options)
{
	switch(ext) {
		case 0:
			if(options & SCENE_EXPORT_SELECTED) return TRUE;
			else return FALSE;
			break;
		}
	return FALSE;
}



/*===========================================================================*\
 |  Preferences dialog handler
\*===========================================================================*/

static INT_PTR CALLBACK PrefsDlgProc(
		HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
	CVDExporter *se = (CVDExporter*)GetWindowLongPtr(hWnd,GWLP_USERDATA);
	if (!se && msg!=WM_INITDIALOG) return FALSE;

	switch (msg) {
		case WM_INITDIALOG:
			// Update class pointer
			se = (CVDExporter*)lParam;
			SetWindowLongPtr(hWnd,GWLP_USERDATA,lParam);

			// Setup preferences initial state
			SetCheckBox(hWnd,IDC_MESH+se->searchtype,TRUE);

			break;

		case WM_DESTROY:
			break;

		case WM_COMMAND:
			switch (LOWORD(wParam)) {
				case IDC_CANCEL:
					EndDialog(hWnd,1);
					break;

				case IDC_OK:
					// Retrieve preferences
					se->bExportAsMap = GetCheckBox(hWnd,IDC_MAP)?1:0;
					se->bExportAsMesh = GetCheckBox(hWnd,IDC_MESH)?1:0;

					se->searchtype = GetCheckBox(hWnd,IDC_MESH)?0:1;

					EndDialog(hWnd,0);
				break;
			}
			break;

		default:
			return FALSE;
		}
	return TRUE;
}



/*===========================================================================*\
 |  For every node we get, try and find some custom data
 |	'channel' method will just see if the channel needed is present
 |	'modifier' method will see if the modifier exists in the modstack
\*===========================================================================*/

// from the CVD exporter
#define MY_CHANNEL		5

BOOL CVDExporter::nodeEnum(INode* node,Interface *ip)
{
	if(exportSelected && node->Selected() == FALSE)
		return TREE_CONTINUE;

	// Check for user cancel
	if (ip->GetCancel())
		return FALSE;

	ObjectState os = node->EvalWorldState(ip->GetTime());

	if (os.obj )
	{
		switch( os.obj->SuperClassID() )
		{
		case GEOMOBJECT_CLASS_ID:
			ExportGeomObject( node, ip );
			break;
		case BONE_CLASS_ID:
			fprintf(fileStream, "hey we have a bone up here!!");
			break;
		case HELPER_CLASS_ID:
			//ExportHelperObject( node, ip );
			ExportBone(node, ip, 0);
		default:
			//is it a camera?
			ExportBone(node, ip, 0);
			break;
		}
	}

	// Recurse through this node's children, if any
	for (int c = 0; c < node->NumberOfChildren(); c++) {
		if (!nodeEnum(node->GetChildNode(c), ip))
			return FALSE;
	}

	return TRUE;
}

bool CVDExporter::ExportHelperObject(INode* node, Interface *ip)
{
	Object* helperObj = node->EvalWorldState(0).obj;
	if (helperObj)
	{
		TSTR className;
		helperObj->GetClassName(className);

		if( strcmp(className, "Dummy") == 0 )
		{
			TimeValue currtime = ip->GetTime();
			Matrix3 tm = node->GetObjTMBeforeWSM(currtime); // GetObjTMAfterWSM(currtime);

			Point3 p = tm.GetTrans();
			float yaw, pitch, roll;

			tm.GetYawPitchRoll(&yaw, &pitch, &roll);

			/*
			fprintf(fileStream, "3D MAX SAYS YAW, PITCH, ROLL: %f %f %f\n", yaw, pitch, roll);

			Point3 mat[3];
			for( int m = 0; m < 3; m++)
			{
				mat[m] = tm.GetRow(m);
			}

			yaw = asin( - mat[0][2] );
			roll = acos( mat[0][0] / cosf(yaw) );
			pitch = asin(mat[1][2] / cosf(yaw) );
			fprintf(fileStream, "I SAYS YAW, PITCH, ROLL: %f %f %f\n", yaw, pitch, roll);
			*/
			//float rotation[3];
			//MatrixToEuler(tm, rotation, EULERTYPE_YXZ);

			//Quat quat = node->GetObjOffsetRot();
			//QuatToEuler(quat, rotation, EULERTYPE_YXZ); //, BOOL b=FALSE);

			fprintf(fileStream, "*static\n");

			fprintf(fileStream, "\t*mesh %s.A3D\n", node->GetName() );
			fprintf(fileStream, "\t*position %f %f %f\n", p.x, p.z, p.y );
			//fprintf(fileStream, "\t*rotation %f %f %f\n", rotation[0], (rotation[1] - (3.1415/2)), rotation[2]);
			fprintf(fileStream, "\t*rotation %f %f %f\n", roll, -pitch, -yaw);

			//export scale
			fprintf(fileStream, "\t*scale %f %f %f\n", 1.0f,
				1.0f, 1.0f);
			//fprintf(fileStream, "\t*scale %f %f %f\n", tm.GetRow(0).x,
			//	tm.GetRow(1).y, tm.GetRow(2).z);


			fprintf(fileStream, "*end\n");
		}
	}

	return true;
}

BOOL CVDExporter::MapNodeEnum(INode* node,Interface *ip)
{
	if(exportSelected && node->Selected() == FALSE)
		return TREE_CONTINUE;

	// Check for user cancel
	if (ip->GetCancel())
		return FALSE;

	ObjectState os = node->EvalWorldState(ip->GetTime());

	if (os.obj )
	{
		switch( os.obj->SuperClassID() )
		{
		case GEOMOBJECT_CLASS_ID:
			ExportMapGeomObject( node, ip );
			break;
		case BONE_CLASS_ID:
			fprintf(fileStream, "hey we have a bone up here!!");
			break;
		case HELPER_CLASS_ID:
			ExportHelperObject( node, ip );
			break;
		}
	}

	// Recurse through this node's children, if any
	for (int c = 0; c < node->NumberOfChildren(); c++) {
		if (!nodeEnum(node->GetChildNode(c), ip))
			return FALSE;
	}

	return TRUE;
}

//class EulerRotation;

bool CVDExporter::ExportBone(INode* node, Interface *ip, int boneIdx)
{
	fprintf(fileStream, "*bone %i\n", boneIdx);
	fprintf(animFile, "*bone %i\n", boneIdx);

	Control* pC = node->GetTMController()->GetPositionController();
	Control* rC = node->GetTMController()->GetRotationController();
	Control* sC = node->GetTMController()->GetScaleController();

	//if( pC != NULL && pC->NumKeys() >= 1 )

	//std::vector<keyFrame> frameData;
	std::vector<keyFrame> frameData;

	Interval range = ip->GetAnimRange();
	int lastTime = 0;

	//fprintf(animFile, "animation range: %i", (int)(range.End() - range.Start() ) );
/*
	if( pC != NULL )
	{
		for(int i = 0; i < pC->NumKeys(); i++)
		{
			//fprintf(fileStream, "first key time: %i", pC->GetKeyTime(i) );

			Point3 trans;
			Matrix3 pmat;
			Interval ivalid;
			ivalid = FOREVER;
			pmat = node->GetParentTM( pC->GetKeyTime(i) );
			pC->GetValue( pC->GetKeyTime(i) , &pmat, ivalid, CTRL_RELATIVE);
			trans = pmat.GetTrans();

			//fprintf(fileStream, "\t\t*frame %d position %.1f, %.1f, %.1f\n",
			// i, trans.x, trans.y, trans.z);

			keyFrame newKeyFrame;

			/*
			if( i == 0 )
			{
				newKeyFrame.time = range.End() - pC->GetKeyTime( (pC->NumKeys() - 1) );
			}*//*
			if( (i+1) >= pC->NumKeys() )
			{
				newKeyFrame.time = range.End() - pC->GetKeyTime(i);
			}
			else
			{
				newKeyFrame.time = pC->GetKeyTime(i + 1) - pC->GetKeyTime(i);
			}* /
			//lastTime = pC->GetKeyTime(i);

			newKeyFrame.time = pC->GetKeyTime(i);
			newKeyFrame.position = true;
			newKeyFrame.x = trans.x;
			newKeyFrame.y = trans.z;
			newKeyFrame.z = trans.y;

			frameData.push_back( newKeyFrame );
			/*
			//THIS SHOULD BE A PRIORITY QUE?!?!
			bool inserted = false;
			std::vector<keyFrame>::iterator frameIt;
			for( frameIt = frameData.begin(); frameIt != frameData.end(); frameIt++ )
			{
				if( newKeyFrame.time > frameIt->time )
				{
					//frameData.push_back( newKeyFrame );
					frameData.insert( (frameIt+1), newKeyFrame );
					inserted = true;
					break;
				}
			}

			if( !inserted )* /
			//	frameData.push_back( newKeyFrame );

		}
	}*/
	if( rC != NULL && pC != NULL )
	{
		if( rC->ClassID() == Class_ID(EULER_CONTROL_CLASS_ID, 0) )
		{
			//for( int a = 0; a < rC->NumKeys(); a++ )
			//{
				//sample each frame and sum up the rotations
				//check for sign flips

				Quat qPrev;
				Quat qCur;
				Interval iv;
				Matrix3 mPrev;
				Matrix3 mCur;
				Matrix3 rmat;
				float rotation[3] = {0,0,0};
				float curRotation[3] = {0,0,0};
				float prevRotation[3] = {0,0,0};
				float eaCur[3] = {0,0,0};
				float eaPrev[3] = {0,0,0};
				float EulerAng[3] = {0,0,0};
				float dEuler[3] = {0,0,0};

				range.SetEnd( rC->GetKeyTime( rC->NumKeys() - 1 ) );

				rC->GetValue(range.Start(),&qPrev,iv);

				Matrix3 tm;
				qPrev.MakeMatrix(tm);
				MatrixToEuler(tm,EulerAng, EULERTYPE_XYZ);

				for( TimeValue t = range.Start(); t <= range.End(); t++)
				{
					//qPrev = qCur;
					rC->GetValue( t, &qCur , iv);//rC->GetKeyTime(i)

					qCur.MakeMatrix(mCur);
					qPrev.MakeMatrix(mPrev);

					//mPrev = mCur;
					//rC->GetValue( t, &mCur , iv);

					for(int j=0 ; j < 3 ; j++)
					{
						dEuler[j] = 0;
					}

					//check sign flip
					float f = GetEulerMatAngleRatio(mPrev,mCur,eaPrev,eaCur,EULERTYPE_XYZ);
					if( f > PI)
					{
						// We found a flip here
						for(int j=0 ; j < 3 ; j++)
						{
							// find the sign flip :
							if( fabs((eaCur[j] - eaPrev[j])) < 2*PI - THRESHHOLD )
								dEuler[j] = eaCur[j] - eaPrev[j];
							else
								// unflip the flip
								dEuler[j] = (2*PI - (float) (fabs(eaCur[j]) + fabs(eaPrev[j]))) * (eaPrev[j] > 0 ? 1 : -1);

							EulerAng[j] += dEuler[j];
						}


					}
					else
					{
						// Add up the angle difference
						for(int j=0 ; j < 3 ; j++)
						{
							dEuler[j] = eaCur[j] - eaPrev[j];
							EulerAng[j] += dEuler[j];
						}
					}

					//fprintf(animFile, "(time: %i) %f %f %f\n", (int)t, dEuler[0], dEuler[1], dEuler[2] );

					if( rC->IsKeyAtTime(t,KEYAT_ROTATION) || pC->IsKeyAtTime(t,KEYAT_POSITION) )
					{


						//fprintf(animFile, "(time: %i) fixed roation: %f %f %f\n", (int)t, EulerAng[0], EulerAng[1], EulerAng[2] );
						/*
						//now we need to push the data onto the frame data
						std::vector<keyFrame>::iterator frameIt;
						bool bFound = false;
						for(frameIt = frameData.begin(); frameIt != frameData.end(); frameIt++)
						{
							if( frameIt->time == t )//rC->GetKeyTime(a) )
							{
								if( frameIt->rotation == true )
								{
									bFound = true;
									break;
								}

								frameIt->rotation = true;
								frameIt->yaw = -EulerAng[2] ;//yaw;
								frameIt->pitch = EulerAng[0] - PI/2;//pitch;
								frameIt->roll = (EulerAng[1]);//roll;

								bFound = true;
							}
						}
						if( !bFound )
						{*/
							keyFrame newKeyFrame;

							newKeyFrame.time = t; //rC->GetKeyTime(a);
							newKeyFrame.rotation = true;
							newKeyFrame.yaw = -EulerAng[2];
							newKeyFrame.pitch = - EulerAng[0] + PI/2;
							newKeyFrame.roll = (EulerAng[1] );

							Point3 trans;
							Matrix3 pmat;
							Interval ivalid;
							ivalid = FOREVER;
							pmat = node->GetParentTM( t );
							pC->GetValue( t , &pmat, ivalid, CTRL_RELATIVE);
							trans = pmat.GetTrans();

							newKeyFrame.position = true;
							newKeyFrame.x = trans.x;
							newKeyFrame.y = trans.z;
							newKeyFrame.z = trans.y;

							frameData.push_back( newKeyFrame );
						//}

					}

					qPrev = qCur;
				}

				/*
				//now we need to push the data onto the frame data
				std::vector<keyFrame>::iterator frameIt;
				bool bFound = false;
				for(frameIt = frameData.begin(); frameIt != frameData.end(); frameIt++)
				{
					if( frameIt->time == rC->GetKeyTime(a) )
					{
						if( frameIt->rotation == true )
						{
							bFound = true;
							break;
						}

						frameIt->rotation = true;
						frameIt->yaw = EulerAng[2];//yaw;
						frameIt->pitch = EulerAng[0];//pitch;
						frameIt->roll = (EulerAng[1]);//roll;

						bFound = true;
					}
				}
				if( !bFound )
				{
					keyFrame newKeyFrame;

					newKeyFrame.time = rC->GetKeyTime(a);
					newKeyFrame.rotation = true;
					newKeyFrame.yaw = EulerAng[2];
					newKeyFrame.pitch = EulerAng[0];
					newKeyFrame.roll = (EulerAng[1] );

					frameData.push_back( newKeyFrame );
				}
				fprintf(animFile, "%i) (time: %i) fixed roation: %f %f %f\n", a, rC->GetKeyTime(a), EulerAng[0], EulerAng[1], EulerAng[2] );
				*/


				//fprintf(animFile, "%i) (time: %i) fixed matrix: %f %f %f\n", a, rC->GetKeyTime(a), rotation[0], rotation[1], rotation[2] );
			//}



			//so here we should tally up the rotations from every frame?
			//check for sign flips also during each frame?
			/*
			EulerRotation* er = (EulerRotation*)rC;

			for(int i = 0; i < rC->NumKeys(); i++)
			{
				Matrix3 rmat;
				Interval ivalid;
				ivalid = FOREVER;

				er->GetValue( er.GetKeyTime(i), rmat, ivalid, CTRL_RELATIVE);
			}*/
		}
/*
		if(rC->ClassID() == Class_ID(TCBINTERP_ROTATION_CLASS_ID, 0))
		{
			fprintf(animFile, "TCB!!!");
		}
		else if(rC->ClassID() == Class_ID(HYBRIDINTERP_ROTATION_CLASS_ID, 0))
		{
			fprintf(animFile, "HYP!!!");
		}
		else if(rC->ClassID() == Class_ID(LININTERP_ROTATION_CLASS_ID, 0))
		{
			fprintf(animFile, "LIN!!!");
		}*/

/*
		for(int i = 0; i < rC->NumKeys(); i++)
		{
			Matrix3 tm = node->GetNodeTM( rC->GetKeyTime(i) );

			float y, p, r;
			tm.GetYawPitchRoll(&y,&p,&r);

			fprintf(animFile, "i) yaw: %f pitch: %f roll: %f\n", rC->GetKeyTime(i), y * PI/180 ,p * PI/180, r * PI/180);



			//fprintf(fileStream, "first key time: %i", rC->GetKeyTime(i) );

			float yaw, pitch, roll;
			Matrix3 rmat;
			Quat q;

			Interval ivalid;

			ivalid = FOREVER;
			rmat = node->GetParentTM( rC->GetKeyTime(i) );
			//rmat.Invert();
			rC->GetValue( rC->GetKeyTime(i) , &rmat, ivalid, CTRL_RELATIVE);//relative
			//rC->GetValue( rC->GetKeyTime(i) , &q, ivalid);
			//rmat.GetYawPitchRoll(&yaw, &pitch, &roll);

			float rotation[3];
			MatrixToEuler(rmat, rotation, EULERTYPE_YXZ);
			//QuatToEuler( q, rotation);

			//Quat quat = node->GetObjOffsetRot();
			//QuatToEuler(quat, rotation, EULERTYPE_YXZ); //, BOOL b=FALSE);

			//fprintf(fileStream, "\t\t*frame %d position %.1f, %.1f, %.1f\n",
			// i, yaw, pitch, roll);

			std::vector<keyFrame>::iterator frameIt;
			//frameData;

			//frameData.find( pC->GetKeyTime(i) );

			bool bFound = false;/*
			for(frameIt = frameData.begin(); frameIt != frameData.end(); frameIt++)
			{
				if( frameIt->time == rC->GetKeyTime(i) )
				{
					frameIt->rotation = true;
					frameIt->yaw = rotation[0];//yaw;
					frameIt->pitch = (rotation[1] - (3.1415/2));//pitch;
					frameIt->roll = rotation[2];//roll;

					/*
					frameIt->yaw = yaw;//yaw;
					frameIt->pitch = pitch;//pitch;
					frameIt->roll = roll;//roll;* /

					bFound = true;
				}
			}* /
			if( !bFound )
			{
				keyFrame newKeyFrame;
				/*
				if( (i+1) >= pC->NumKeys() )
				{
					newKeyFrame.time = range.End() - pC->GetKeyTime(i);
				}
				else
				{
					newKeyFrame.time = pC->GetKeyTime(i + 1) - pC->GetKeyTime(i);
				}* /

				newKeyFrame.time = rC->GetKeyTime(i);
				newKeyFrame.rotation = true;
				newKeyFrame.yaw = yaw;
				newKeyFrame.pitch = pitch;
				newKeyFrame.roll = roll;

				frameData.push_back( newKeyFrame );

				/*
				//frameData.push_back( newKeyFrame );
				bool inserted = false;
				std::vector<keyFrame>::iterator frameIt;
				for( frameIt = frameData.begin(); frameIt != frameData.end(); frameIt++ )
				{
					if( newKeyFrame.time > frameIt->time )
					{
						//frameData.push_back( newKeyFrame );
						frameData.insert( (frameIt+1), newKeyFrame );
						inserted = true;
						break;
					}
				}

				if( !inserted )* /
				//	frameData.push_back( newKeyFrame );
			}* /

			//frameData[cur].x = trans.x;
			//frameData[cur].y = trans.y;
			//frameData[cur].z = trans.z;
		}*/
	}


	//now fudge the time for us
	std::priority_queue<keyFrame> sortedFrame;
	std::vector<keyFrame>::iterator frameIt;
	for(frameIt = frameData.begin(); frameIt != frameData.end(); frameIt++)
	{
		sortedFrame.push( (*frameIt) );
	}


	//finally output the data to file

	//std::vector<keyFrame>::iterator frameIt; // frameData(noSamples);
	//int nextTime = 0;
	//int curTime = 0;
	//for( frameIt = frameData.begin(); frameIt != frameData.end(); frameIt++ )
	int firstFrameTime = sortedFrame.top().time;

	while( !sortedFrame.empty() )
	{
		keyFrame f = sortedFrame.top();
		sortedFrame.pop();

	//	GetFrameRate() );
	//fprintf(animFile, "\t*frametime %i\n", GetTicksPerFrame()
		if( !sortedFrame.empty() )
		{
			f.time = ((sortedFrame.top().time - f.time) * GetFrameRate()) / GetTicksPerFrame();
		}
		else
		{
			f.time = (((range.End() - f.time) + firstFrameTime) * GetFrameRate()) / GetTicksPerFrame();
			f.time = 0;
		}

		/*
		if( (frameIt+1) == frameData.end() )
		{
			f.time = range.End() - frameIt->time;
		}
		else
		{
			fprintf(animFile, "current: %i  next: %i", frameIt->time, (frameIt+1)->time );
			f.time = (frameIt+1)->time - f.time;
		}*/

		fprintf(animFile, "\t*keyframe %i\n", f.time);

		if( f.position )
		{
			//fprintf(fileStream, "\t\t*position %f %f %f\n", frameIt->x, frameIt->y, frameIt->z);
			fprintf(animFile, "\t\t*position %f %f %f\n", f.x, f.y, f.z);
		}

		if( f.rotation )
		{
			//fprintf(fileStream, "\t\t*rotation %f %f %f\n", frameIt->yaw, frameIt->pitch, frameIt->roll);
			//make them in human readable degrees for a bit
			fprintf(animFile, "\t\t*rotation %f %f %f\n", f.yaw, f.pitch, f.roll);
		}

		fprintf(animFile, "\t*end\n");
	}


	//output heirachey
	std::map<int, INode*>::iterator boneIt;

	int b = 0;
	bool bFound = false;
	for(  boneIt = bones.begin(); boneIt != bones.end(); boneIt++, b++ )
	{
		if( !node->IsRootNode() && (boneIt->second) == node->GetParentNode() )
		{
			fprintf(fileStream, "\t*parent %i\n", b);
			bFound = true;
		}
	}
	if( !bFound )
		fprintf(fileStream, "\t*parent -1\n");

	fprintf(fileStream, "*end\n");
	fprintf(animFile, "*end\n");

	return TRUE;
}

/*===========================================================================*\
 |  Export mesh data
\*===========================================================================*/

bool CVDExporter::ExportGeomObject(INode* node, Interface *ip)
{
	int i; // for loops
    Point3 vert;


	TimeValue currtime = ip->GetTime();
    ObjectState os = node->EvalWorldState(currtime);
    if (!os.obj)
        return FALSE;

	//we dont want to export bones or helpers or cameras
	if( os.obj->ClassID() == Class_ID(BONE_CLASS_ID,0) )
	{
		fprintf(fileStream,"vedy vedy sneaky!!!");
		return TRUE;
	}
	if( os.obj->ClassID() == BONE_OBJ_CLASSID )
	{
		//ExportBone(node, ip);
		return TRUE;
	}
	if( os.obj->ClassID() == Class_ID(HELPER_CLASS_ID,0) )
	{
		fprintf(fileStream, "we found a helper, lets return!!!");
		return TRUE;
	}
    if (os.obj->ClassID() == Class_ID(TARGET_CLASS_ID, 0))
        return TRUE;

	fprintf(fileStream, "*mesh\n");

	Matrix3 tm = node->GetObjTMAfterWSM(currtime);

	//lets try some skined export data
	ISkinContextData* skincontext = NULL;
	ISkin* skin = NULL;

	Object* object = node->GetObjectRef();
	if (object->SuperClassID() == GEN_DERIVOB_CLASS_ID)
	{
		IDerivedObject* derivedobject = (IDerivedObject*)object;
		int modcount = derivedobject->NumModifiers();
		for(int i = 0; i < modcount; i++)
		{
			Modifier* modifier = derivedobject->GetModifier(i);
			if (modifier->ClassID() == SKIN_CLASSID)
			{
				skin = (ISkin*)modifier->GetInterface(I_SKIN);
				if(skin)
				{
					skincontext = skin->GetContextInterface(node);
				}
			}
			if( modifier->ClassID() == BONE_OBJ_CLASSID )
			{
				fprintf(fileStream,"we found a bone!!!");
			}
		}
	}
	if( skincontext )
	{
		int count = 0;

		for(int v = 0; v < skincontext->GetNumPoints(); v++) //loop through vertices
			for(int b = 0; b < skincontext->GetNumAssignedBones( v ); b++) //loop through bones
				count++;

		fprintf(fileStream, "\t*skin %i\n", count);

		for(v = 0; v < skincontext->GetNumPoints(); v++) //loop through vertices
		{
			for(int b = 0; b < skincontext->GetNumAssignedBones( v ); b++) //loop through bones
			{
				float weight = skincontext->GetBoneWeight(v, b);

				int bone = skincontext->GetAssignedBone(v,b);

				bones[bone] = skin->GetBone( bone );

				//INode* bone =  skin->GetBone( b );
				//ExportBone(INode* node, Interface *ip)
				//ExportBone( skin->GetBone( b ) , ip, b);
				/*
				bool bFound = false;
				std::list<INode*>::iterator boneIt;
				int boneNo = 0;

				fprintf(fileStream, "bone id: %i, v:%i   b:%i", skincontext->GetAssignedBone(v,b) , v, b);
				for( boneIt = bones.begin(); boneIt != bones.end(); boneIt++, boneNo++ )
				{
				//bones
					//if( skin->GetBone( b ) == *boneIt ) //compare addresses of bones
					//if( (skin->GetBone( skincontext->GetAssignedBone(v,b) ) )->GetHandle() == (*boneIt)->GetHandle() ) //compare addresses of bones
					/*if( skincontext->GetAssignedBone(v,b) )
					{
						fprintf(fileStream,"we found out bone: %i", boneNo);
						bFound = true;
						break;
					}* /
				}
				if( !bFound )
				{
					bones.push_back( skin->GetBone( b ) );
					boneNo = bones.size() - 1;
					//fprintf(fileStream,"we adding new bone: %i", boneNo);
				}*/

				//fprintf(fileStream, "\t\t%i %i %f\n", v,b,weight);
				fprintf(fileStream, "\t\t%i %i %f\n", v, bone,weight);
			}
		}
	}

	/*
	Modifier* mod = FindSkinModifier(node);
	ISkin* skin = (ISkin*)mod->GetInterface(I_SKIN);
	ISkinContextData *skinMC = skin->GetContextInterface(node);
	*/

	//collapse mesh
	Object *obj = os.obj;
	TriObject* tri = NULL;
	if (obj && obj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0)))
	{

            tri = (TriObject *) obj->ConvertToType(0, Class_ID(TRIOBJ_CLASS_ID, 0));
	}

	Mesh* mesh = &(tri->GetMesh());
    assert(mesh);
    mesh->buildNormals();

	float sphereBoundRadius = 0;
	Point3 origin = tm.GetTrans();

	// Export the vertices
	fprintf(fileStream,"\t*vertex\t%d\n", mesh->getNumVerts());
    for (i = 0; i < mesh->getNumVerts(); i++)
	{
		vert = tm * mesh->verts[i];

		//max is ghey
		float temp = vert.y;
		vert.y = vert.z;
		vert.z = temp;

		Point3 sub = vert - origin;
		float dist = sqrt( sub.x * sub.x + sub.y * sub.y + sub.z * sub.z );

		if( dist > sphereBoundRadius )
			sphereBoundRadius = dist;

        fprintf(fileStream, "\t\t%f %f %f\n", vert.x, vert.y, vert.z);
    }

	//export normals
	fprintf(fileStream,"\t*normal\t%d\n", mesh->getNumVerts());
	for (i = 0; i < mesh->getNumVerts(); i++)
	{
		vert = tm * mesh->getNormal(i);

		//max is ghey
		float temp = vert.y;
		vert.y = vert.z;
		vert.z = temp;

        fprintf(fileStream, "\t\t%f %f %f\n", vert.x, vert.y, vert.z);
    }

	// export faces
	fprintf(fileStream,"\t*faces\t%d\n", mesh->getNumFaces());
    for (i = 0; i < mesh->getNumFaces(); i++)
	{
		//also gotta reverse the face winding
		fprintf( fileStream, "\t\t%d %d %d\n",
			mesh->faces[i].v[2],
			mesh->faces[i].v[1],
            mesh->faces[i].v[0]);
    }

	//export texture data
	int numTVerts = mesh->getNumTVerts();
    if (numTVerts)
	{
		fprintf(fileStream, "\t*texturevert\t%d\n", numTVerts);
		for (i = 0; i < numTVerts; i++)
		{
			UVVert tvert = mesh->tVerts[i];
			//need to fudge the v's because max is ghey ;p
			tvert.y = 1 - tvert.y;
			fprintf(fileStream, "\t\t%f %f %f\n", tvert.x, tvert.y, tvert.z);
		}

		// now, print tvert indices used by tvfaces
		fprintf(fileStream, "\t*textureface\t%d\n", mesh->getNumFaces());
		for (i = 0; i < mesh->getNumFaces(); i++)
		{
			TVFace tface = mesh->tvFace[i];
			fprintf(fileStream, "\t\t%d %d %d\n",
				tface.t[2], tface.t[1], tface.t[0]);
		}
	}


	//we do this at the end so we dont have to trough through the other data
	fprintf(fileStream, "\t*sphereBoundRadius %f\n", sphereBoundRadius );

	///////////////////////////////////
    // dump mtl info for this node
    Mtl * nodemtl = node->GetMtl();
    if (nodemtl)
	{
        //fprintf(fileStream, "    mtl -- name:<%s>\n", nodemtl->GetName());

		//look up material name in our list
		std::list<TCHAR*>::iterator lookup;

		int index = 0;
		for( lookup = materialID.begin(); lookup != materialID.end(); lookup++, index++ )
		{
			if( strcmp( *lookup, nodemtl->GetName()) == 0 )
					fprintf(fileStream, "\t*materialid %i\n", index);
		}

    } else
	{
        // no mat assigned to node, print out 0-255 RGB color
        DWORD col = node->GetWireColor();
        fprintf(fileStream,"    *NO MATERIAL -- rgb:<%d,%d,%d>\n",
            GetRValue(col), GetGValue(col), GetBValue(col));
    }

	//animtaiotn
	/*
	animatable class
		NumKeys()
		GetKeyTime(int index)
		GetKeyIndex(TimeValue t)
	*/
/*
	//Control* xPosC = node->GetTMController()->GetXController();
	Control* pC = node->GetTMController()->GetPositionController();
	Control* rC = node->GetTMController()->GetRotationController();
	Control* sC = node->GetTMController()->GetScaleController();

	//if( pC != NULL && pC->NumKeys() >= 1 )

	std::vector<keyFrame> frameData;

	if( pC != NULL )
		for(i = 0; i < pC->NumKeys(); i++)
		{
			//fprintf(fileStream, "first key time: %i", pC->GetKeyTime(i) );

			Point3 trans;

			Matrix3 pmat;

			Interval ivalid;

			ivalid = FOREVER;
			pmat = node->GetParentTM( pC->GetKeyTime(i) );
			pC->GetValue( pC->GetKeyTime(i) , &pmat, ivalid, CTRL_RELATIVE);
			trans = pmat.GetTrans();

			//fprintf(fileStream, "\t\t*frame %d position %.1f, %.1f, %.1f\n",
			// i, trans.x, trans.y, trans.z);

			keyFrame newKeyFrame;
			newKeyFrame.time = pC->GetKeyTime(i);
			newKeyFrame.position = true;
			newKeyFrame.x = trans.x;
			newKeyFrame.y = trans.y;
			newKeyFrame.z = trans.z;

			frameData.push_back( newKeyFrame );
		}

	if( rC != NULL )
		for(i = 0; i < rC->NumKeys(); i++)
		{
			//fprintf(fileStream, "first key time: %i", rC->GetKeyTime(i) );

			float yaw, pitch, roll;
			Matrix3 rmat;

			Interval ivalid;

			ivalid = FOREVER;
			rmat = node->GetParentTM( rC->GetKeyTime(i) );
			rC->GetValue( rC->GetKeyTime(i) , &rmat, ivalid, CTRL_RELATIVE);
			rmat.GetYawPitchRoll(&yaw, &pitch, &roll);

			//fprintf(fileStream, "\t\t*frame %d position %.1f, %.1f, %.1f\n",
			// i, yaw, pitch, roll);

			std::vector<keyFrame>::iterator frameIt;
			//frameData;

			bool bFound = false;
			for(frameIt = frameData.begin(); frameIt != frameData.end(); frameIt++)
			{
				if( frameIt->time == rC->GetKeyTime(i) )
				{
					frameIt->rotation = true;
					frameIt->yaw = yaw;
					frameIt->pitch = pitch;
					frameIt->roll = roll;

					bFound = true;
				}
			}

			if( !bFound )
			{
				keyFrame newKeyFrame;
				newKeyFrame.time = rC->GetKeyTime(i);
				newKeyFrame.rotation = true;
				newKeyFrame.yaw = yaw;
				newKeyFrame.pitch = pitch;
				newKeyFrame.roll = roll;

				frameData.push_back( newKeyFrame );
			}

			//frameData[cur].x = trans.x;
			//frameData[cur].y = trans.y;
			//frameData[cur].z = trans.z;
		}
*/
	//OutputBone();
//	ExportBone(node, ip, 0);
	/*
	//finally output the data to file
	std::vector<keyFrame>::iterator frameIt; // frameData(noSamples);
	for( frameIt = frameData.begin(); frameIt != frameData.end(); frameIt++ )
	{
		fprintf(fileStream, "\t*frame time %i\n", frameIt->time);

		if( frameIt->position )
			fprintf(fileStream, "\t\t*position %f %f %f\n", frameIt->x, frameIt->y, frameIt->z);

		if( frameIt->rotation )
			fprintf(fileStream, "\t\t*rotation %f %f %f\n", frameIt->yaw, frameIt->pitch, frameIt->roll);
	}*/

	fprintf(fileStream, "*end\n");

	return TRUE;
}

/*===========================================================================*\
 |  Export map data
\*===========================================================================*/
bool CVDExporter::ExportMapGeomObject(INode* node, Interface *ip)
{
	int i; // for loops
    Point3 vert;

	//fprintf(fileStream, "*cell\n");

	TimeValue currtime = ip->GetTime();
    ObjectState os = node->EvalWorldState(currtime);
    if (!os.obj)
        return FALSE;

	//we dont want to export bones or helpers or cameras
	if( os.obj->ClassID() == Class_ID(BONE_CLASS_ID,0) )
	{
		fprintf(fileStream,"vedy vedy sneaky!!!");
		return TRUE;
	}
	if( os.obj->ClassID() == Class_ID(HELPER_CLASS_ID,0) )
	{
		fprintf(fileStream, "we found another helper int he map, lets return");
		return TRUE;
	}
    if (os.obj->ClassID() == Class_ID(TARGET_CLASS_ID, 0))
        return TRUE;

	Matrix3 tm = node->GetObjTMAfterWSM(currtime);

	/*
	struct STANDARD_VERTEX
{
    VECTOR3 p;
    VECTOR3 n;
    float       tu, tv;

	STANDARD_VERTEX()
	{
		tu = tv = 0;
		n = VECTOR3(0,0,0);
		p = VECTOR3(0,0,0);
	}

	bool operator==(STANDARD_VERTEX const& rhs) const
	{
		if( p == rhs.p && n == rhs.n && tu == rhs.tu && tv == rhs.tv) //
			return true;

		return false;
	}
};

struct STL_TEXTUREBATCH
{
	std::vector<STANDARD_VERTEX> vertexList;
	std::vector<int> indexList;

	char* texture;
	MATERIAL material;

	int numVertices;
	int numFaces;
};

	*/
	//what we have to do here is to
	// create a new texture batch, fill it with info for this peice of geom
	// and add it to useableData
	STL_TEXTUREBATCH newMesh;


	///////////////////////////////////
    // dump mtl info for this node
    Mtl * nodemtl = node->GetMtl();
    if (nodemtl) {
        //fprintf(fileStream, "    mtl -- name:<%s>\n", nodemtl->GetName());

		//look up material name in our list
		std::list<TCHAR*>::iterator lookup;

		int index = 0;
		for( lookup = materialID.begin(); lookup != materialID.end(); lookup++, index++ )
		{
			//if( strcmp( *lookup, nodemtl->GetName()) )
			//		fprintf(fileStream, "\t*materialid %i\n", index);
		}

    } else {
        // no mat assigned to node, print out 0-255 RGB color
        DWORD col = node->GetWireColor();
        //fprintf(fileStream,"    color -- rgb:<%d,%d,%d>\n",
        //    GetRValue(col), GetGValue(col), GetBValue(col));
    }

	//collapse mesh
	Object *obj = os.obj;
	TriObject* tri = NULL;
	if (obj && obj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0)))
	{

            tri = (TriObject *) obj->ConvertToType(0, Class_ID(TRIOBJ_CLASS_ID, 0));
	}

	Mesh * mesh = &(tri->GetMesh());
    assert(mesh);
    mesh->buildNormals();

	// Export the vertices
	//fprintf(fileStream,"\t*vertex\t%d\n", mesh->getNumVerts());
    for (i = 0; i < mesh->getNumVerts(); i++) {
		vert = tm * mesh->verts[i];

		STANDARD_VERTEX stdVert;
		stdVert.p.x = vert.x;
		stdVert.p.y = vert.z;
		stdVert.p.z = vert.y;


		//export normals
		vert = tm * mesh->getNormal(i);

		//max is ghey
		float temp = vert.y;
		vert.y = vert.z;
		vert.z = temp;

		stdVert.n.x = vert.x;
		stdVert.n.y = vert.y;
		stdVert.n.z = vert.z;

		newMesh.vertexList.push_back( stdVert );


        //fprintf(fileStream, "\t\t%f %f %f\n", vert.x, vert.y, vert.z);
    }


	// export faces
	//fprintf(fileStream,"\t*faces\t%d\n", mesh->getNumFaces());
    for (i = 0; i < mesh->getNumFaces(); i++) {
		//fprintf( fileStream, "\t\t%d %d %d\n",
		//	mesh->faces[i].v[0],
		//	mesh->faces[i].v[1],
        //    mesh->faces[i].v[2]);

		VECTOR3 face( mesh->faces[i].v[2], mesh->faces[i].v[1], mesh->faces[i].v[0] );

		//fprintf( fileStream, "\t\t%d %d %d\n",
		//	face.x,
		//	face.y,
        //    face.z);

		newMesh.faceList.push_back( face );
    }


	/**
	//export texture data
	int numTVerts = mesh->getNumTVerts();
    if (numTVerts)
	{
		fprintf(fileStream, "\t*texturevert\t%d\n", numTVerts);
		for (i = 0; i < numTVerts; i++)
		{
			UVVert tvert = mesh->tVerts[i];
			//need to fudge the v's because max is ghey ;p
			tvert.y = 1 - tvert.y;
			fprintf(fileStream, "\t\t%f %f %f\n", tvert.x, tvert.y, tvert.z);
		}

		// now, print tvert indices used by tvfaces
		fprintf(fileStream, "\t*textureface\t%d\n", mesh->getNumFaces());
		for (i = 0; i < mesh->getNumFaces(); i++)
		{
			TVFace tface = mesh->tvFace[i];
			fprintf(fileStream, "\t\t%d %d %d\n",
				tface.t[2], tface.t[1], tface.t[0]);
		}
	}
  **/
	//export texture data
	int numTVerts = mesh->getNumTVerts();
    if (numTVerts) {
		//fprintf(fileStream, "\t*texturevert\t%d\n", numTVerts);
		for (i = 0; i < numTVerts; i++) {
			UVVert tvert = mesh->tVerts[i];
			tvert.y = 1 - tvert.y;
			//	fprintf(fileStream, "\t\t%f %f %f\n", tvert.x, tvert.y, tvert.z);

			VECTOR3 textureVert( tvert.x, tvert.y, tvert.z );
			newMesh.textureVert.push_back( textureVert );
		}

		// now, print tvert indices used by tvfaces
		//fprintf(fileStream, "\t*textureface\t%d\n", numTVerts);
		for (i = 0; i < mesh->getNumFaces(); i++) {
			TVFace tface = mesh->tvFace[i];
			//fprintf(fileStream, "\t\t%d %d %d\n",
			//	tface.t[0], tface.t[1], tface.t[2]);

			VECTOR3 textureFace( tface.t[2], tface.t[1], tface.t[0] );
			newMesh.textureFace.push_back( textureFace );
		}
	}

	useableData.push_back( newMesh );

	//temporarily blanked these out

	ExportMapRenderData( newMesh );
	ExportMapCollisionData( newMesh );

	return TRUE;
}

bool CVDExporter::ExportMapRenderData( STL_TEXTUREBATCH& newMesh )
{
	/******************************
	 THIS FOLLOWING SECTION EXPORTS
	 THE RENDERABLE DATA
	*******************************/
	//now use my alogarithm to divide up the map
	int numVerts = newMesh.vertexList.size();

	float maxPerimeter = 0; //our map is always a cube

	for( int v = 0; v < numVerts; v++ )
	{
		if( maxPerimeter < abs( newMesh.vertexList[v].p.x ) )
			maxPerimeter = abs( newMesh.vertexList[v].p.x );
		if( maxPerimeter < abs( newMesh.vertexList[v].p.z ) )
			maxPerimeter = abs( newMesh.vertexList[v].p.z );
	}
	maxPerimeter++; //add one just in case we round down

	//now we can calculate the number of cells
	// as each chunk should have about 2500-3000 vertices
	//times by 6 to account for vertices created by other texture vertices
	int totalNumCells = ((numVerts * 6) / averageVertsPerRenderCell);
	totalNumCells++; //add one just incase numVerts is uber small

	//now that we know how many cells to have,
	//we can determine the rough cell dimensions
	int cellsWide = (int)sqrt( (float)totalNumCells );

	totalNumCells = cellsWide * cellsWide; //we have to fix total num of cells
						//to be a proper number

	float cellWidth = ((maxPerimeter * 2) / cellsWide);
	cellWidth++;

	//maxPerimeter = (cellWidth * cellsWide)/2;

	//export header info for render data
	fprintf(fileStream, "*render\n" );

	fprintf(fileStream, "\t*head\n");
	fprintf(fileStream, "\t\t*numbercells %i\n", totalNumCells);
	fprintf(fileStream, "\t\t*dimension %f\n", cellWidth );
	fprintf(fileStream, "\t\t*startvalue %f\n", -maxPerimeter );
	fprintf(fileStream, "\t*end\n");

	//fprintf(fileStream, "\nnumVerts: %i", numVerts);
	//fprintf(fileStream, "\nmaxPerimeter: %i\n totalNumCells: %i", maxPerimeter, totalNumCells);
	//fprintf(fileStream, "\ncellsWide: %i\n cellWidth: %i\n\n", cellsWide, cellWidth);

	std::list<STL_TEXTUREBATCH> renderCells;
	//now we have to find which verts are in each cell
	//and create the specifics
	for( float x = -maxPerimeter; x < maxPerimeter; x += cellWidth )
	{
		for( float z = -maxPerimeter; z < maxPerimeter; z += cellWidth )
		{
			STL_TEXTUREBATCH curCell;

			//loop through faces, if any vertex of the face is inside the cell
			//push it all back
			for( int f = 0; f < newMesh.faceList.size(); f++ )
			{
				if( newMesh.vertexList[ newMesh.faceList[f].x ].p.x > x &&
					newMesh.vertexList[ newMesh.faceList[f].x ].p.x <= (x + cellWidth) &&
					newMesh.vertexList[ newMesh.faceList[f].x ].p.z > z &&
					newMesh.vertexList[ newMesh.faceList[f].x ].p.z <= (z + cellWidth) ||

					newMesh.vertexList[ newMesh.faceList[f].y ].p.x > x &&
					newMesh.vertexList[ newMesh.faceList[f].y ].p.x <= (x + cellWidth) &&
					newMesh.vertexList[ newMesh.faceList[f].y ].p.z > z &&
					newMesh.vertexList[ newMesh.faceList[f].y ].p.z <= (z + cellWidth) ||

					newMesh.vertexList[ newMesh.faceList[f].z ].p.x > x &&
					newMesh.vertexList[ newMesh.faceList[f].z ].p.x <= (x + cellWidth) &&
					newMesh.vertexList[ newMesh.faceList[f].z ].p.z > z &&
					newMesh.vertexList[ newMesh.faceList[f].z ].p.z <= (z + cellWidth) )
				{
					//int face[3];
					VECTOR3 face;
					VECTOR3 tFace;
					VECTOR3 tVert[3];
					bool bFace[3] = {false, false, false};
					bool bTVert[3] = {false, false, false};

					//before pushing on any new vertices, we need to make sure they
					//arent already in the list
					for( int v = 0; v < curCell.vertexList.size(); v++)
					{
						if( newMesh.vertexList[ newMesh.faceList[f].x ].p == curCell.vertexList[v].p )
						{
							face.x = v;
							//tVert1 = newMesh.textureVert[ newMesh.textureFace[f].x ];
							//tFace.x = newMesh.textureFace[f].x;
							bFace[0] = true;
						}
						if( newMesh.vertexList[ newMesh.faceList[f].y ].p == curCell.vertexList[v].p )
						{
							face.y = v;
							//tVert2 = newMesh.textureVert[ newMesh.textureFace[f].y ];
							bFace[1] = true;
						}
						if( newMesh.vertexList[ newMesh.faceList[f].z ].p == curCell.vertexList[v].p )
						{
							face.z = v;
							//tVert3 = newMesh.textureVert[ newMesh.textureFace[f].z ];
							bFace[2] = true;
						}
					}

					//check to make sure we have a vertex otherwise push in a new 1
					if( !bFace[0] )
					{
						face.x = curCell.vertexList.size();
						curCell.vertexList.push_back(
							newMesh.vertexList[ newMesh.faceList[f].x ] );

						//tFace.x = curCell.textureVert.size();
						//curCell.textureVert.push_back( tVert1 );
					}
					if( !bFace[1] )
					{
						face.y = curCell.vertexList.size();
						curCell.vertexList.push_back(
							newMesh.vertexList[ newMesh.faceList[f].y ] );

						//tFace.y = curCell.textureVert.size();
						//curCell.textureVert.push_back( tVert2 );
					}
					if( !bFace[2] )
					{
						face.z = curCell.vertexList.size();
						curCell.vertexList.push_back(
							newMesh.vertexList[ newMesh.faceList[f].z ] );

						//tFace.z = curCell.textureVert.size();
						//curCell.textureVert.push_back( tVert3 );
					}

					curCell.faceList.push_back( face );

					//NOW we need to get the texture vertices
					// and make sure they arent already existing
					tVert[0] = newMesh.textureVert[ newMesh.textureFace[f].x ];
					tVert[1] = newMesh.textureVert[ newMesh.textureFace[f].y ];
					tVert[2] = newMesh.textureVert[ newMesh.textureFace[f].z ];

					for( int tv = 0; tv < curCell.textureVert.size(); tv++)
					{
						if( tVert[0] == curCell.textureVert[tv] )
						{
							tFace.x = tv;
							bTVert[0] = true;
						}
						if( tVert[1] == curCell.textureVert[tv] )
						{
							tFace.y = tv;
							bTVert[1] = true;
						}
						if( tVert[2] == curCell.textureVert[tv] )
						{
							tFace.z = tv;
							bTVert[2] = true;
						}
					}

					//if we enter these if statements,
					// we have failed to find an existing texture face
					// so make a new one
					if( !bTVert[0] )
					{
						tFace.x = curCell.textureVert.size();
						curCell.textureVert.push_back( tVert[0] );
					}
					if( !bTVert[1] )
					{
						tFace.y = curCell.textureVert.size();
						curCell.textureVert.push_back( tVert[1] );
					}
					if( !bTVert[2] )
					{
						tFace.z = curCell.textureVert.size();
						curCell.textureVert.push_back( tVert[2] );
					}

					curCell.textureFace.push_back( tFace );
				}
			}

			fprintf(fileStream, "\t*mesh\n" );

			//now dump this mesh to file
			//export faces
			fprintf(fileStream, "\t\t*faces %i\n", curCell.faceList.size());
			for( int ef = 0; ef < curCell.faceList.size(); ef++ )
			{
				fprintf(fileStream, "\t\t\t%i %i %i\n", (int)curCell.faceList[ef].x,
								(int)curCell.faceList[ef].y, (int)curCell.faceList[ef].z );
			}

			//export vertices
			fprintf(fileStream, "\t\t*vertex %i\n", curCell.vertexList.size());
			for( int ev = 0; ev < curCell.vertexList.size(); ev++ )
			{
				fprintf(fileStream, "\t\t\t%f %f %f\n", curCell.vertexList[ev].p.x,
								curCell.vertexList[ev].p.y, curCell.vertexList[ev].p.z );
			}

			//export normals
			fprintf(fileStream, "\t\t*normal %i\n", curCell.vertexList.size());
			for( int en = 0; en < curCell.vertexList.size(); en++ )
			{
				fprintf(fileStream, "\t\t\t%f %f %f\n", curCell.vertexList[en].n.x,
								curCell.vertexList[en].n.y, curCell.vertexList[en].n.z );
			}

			//export texture faces
			fprintf(fileStream, "\t\t*textureface %i\n", curCell.textureFace.size());
			for( int etf = 0; etf < curCell.textureFace.size(); etf++ )
			{
				fprintf(fileStream, "\t\t\t%i %i %i\n", (int)curCell.textureFace[etf].x,
								(int)curCell.textureFace[etf].y, (int)curCell.textureFace[etf].z );
			}

			//export texture vertices
			fprintf(fileStream, "\t\t*texturevert %i\n", curCell.textureVert.size());
			for( int etv = 0; etv < curCell.textureVert.size(); etv++ )
			{
				fprintf(fileStream, "\t\t\t%f %f %f\n", curCell.textureVert[etv].x,
								curCell.textureVert[etv].y, curCell.textureVert[etv].z );
			}

			fprintf(fileStream, "\t*end\n" );
		}
	}

	fprintf(fileStream, "*end\n" );

	return true;
}

bool CVDExporter::ExportMapCollisionData( STL_TEXTUREBATCH& newMesh )
{
	/******************************
	 THIS FOLLOWING SECTION EXPORTS
	 THE COLLISION DATA
	*******************************/
	//now use my alogarithm to divide up the map
	//here we times by 3 to roughly take in to acount for
	//uvw mapping coords creating extra coords
	int numVerts = newMesh.vertexList.size();

	int maxPerimeter = 0; //our map is always a cube

	for( int v = 0; v < numVerts; v++ )
	{
		if( maxPerimeter < abs( newMesh.vertexList[v].p.x ) )
			maxPerimeter = abs( newMesh.vertexList[v].p.x );
		if( maxPerimeter < abs( newMesh.vertexList[v].p.z ) )
			maxPerimeter = abs( newMesh.vertexList[v].p.z );
	}
	maxPerimeter++; //add one just in case we round down

	//now we can calculate the number of cells
	// as each chunk should have about 2 polys
	// as collision detection is expansive to compute
	int totalNumCells = (numVerts / averageVertsPerCollisionCell);
	totalNumCells++; //add one just incase numVerts is uber small

	//now that we know how many cells to have,
	//we can determine the rough cell dimensions
	int cellsWide = (int)sqrt( (float)totalNumCells );

	totalNumCells = cellsWide * cellsWide; //we have to fix total num of cells
						//to be a proper number

	int cellWidth = (int)((maxPerimeter * 2) / cellsWide);

	int count = 0;
	//export header info for render data
	fprintf(fileStream, "*collision\n" );

	fprintf(fileStream, "\t*head\n");
	fprintf(fileStream, "\t\t*numbercells %i\n", totalNumCells);
	fprintf(fileStream, "\t\t*dimension %i\n", cellWidth );
	fprintf(fileStream, "\t\t*startvalue %i\n", -maxPerimeter );
	fprintf(fileStream, "\t*end\n");

	std::list<STL_TEXTUREBATCH> renderCells;
	//now we have to find which verts are in each cell
	//and create the specifics
	for( int x = -maxPerimeter; x < (maxPerimeter-cellWidth); x += cellWidth )
	{
		for( int z = -maxPerimeter; z < (maxPerimeter-cellWidth); z += cellWidth )
		{
			STL_TEXTUREBATCH curCell;

			//loop through faces, if any vertex of the face is inside the cell
			//push it all back
			for( int f = 0; f < newMesh.faceList.size(); f++ )
			{
				if( newMesh.vertexList[ newMesh.faceList[f].x ].p.x > x &&
					newMesh.vertexList[ newMesh.faceList[f].x ].p.x <= (x + cellWidth) &&
					newMesh.vertexList[ newMesh.faceList[f].x ].p.z > z &&
					newMesh.vertexList[ newMesh.faceList[f].x ].p.z <= (z + cellWidth) ||

					newMesh.vertexList[ newMesh.faceList[f].y ].p.x > x &&
					newMesh.vertexList[ newMesh.faceList[f].y ].p.x <= (x + cellWidth) &&
					newMesh.vertexList[ newMesh.faceList[f].y ].p.z > z &&
					newMesh.vertexList[ newMesh.faceList[f].y ].p.z <= (z + cellWidth) ||

					newMesh.vertexList[ newMesh.faceList[f].z ].p.x > x &&
					newMesh.vertexList[ newMesh.faceList[f].z ].p.x <= (x + cellWidth) &&
					newMesh.vertexList[ newMesh.faceList[f].z ].p.z > z &&
					newMesh.vertexList[ newMesh.faceList[f].z ].p.z <= (z + cellWidth) )
				{
					//int face[3];
					VECTOR3 face;
					VECTOR3 tFace;
					VECTOR3 tVert[3];
					bool bFace[3] = {false, false, false};
					bool bTVert[3] = {false, false, false};

					//before pushing on any new vertices, we need to make sure they
					//arent already in the list
					for( int v = 0; v < curCell.vertexList.size(); v++)
					{
						if( newMesh.vertexList[ newMesh.faceList[f].x ].p == curCell.vertexList[v].p )
						{
							face.x = v;
							//tVert1 = newMesh.textureVert[ newMesh.textureFace[f].x ];
							//tFace.x = newMesh.textureFace[f].x;
							bFace[0] = true;
						}
						if( newMesh.vertexList[ newMesh.faceList[f].y ].p == curCell.vertexList[v].p )
						{
							face.y = v;
							//tVert2 = newMesh.textureVert[ newMesh.textureFace[f].y ];
							bFace[1] = true;
						}
						if( newMesh.vertexList[ newMesh.faceList[f].z ].p == curCell.vertexList[v].p )
						{
							face.z = v;
							//tVert3 = newMesh.textureVert[ newMesh.textureFace[f].z ];
							bFace[2] = true;
						}
					}

					//check to make sure we have a vertex otherwise push in a new 1
					if( !bFace[0] )
					{
						face.x = curCell.vertexList.size();
						curCell.vertexList.push_back(
							newMesh.vertexList[ newMesh.faceList[f].x ] );

						//tFace.x = curCell.textureVert.size();
						//curCell.textureVert.push_back( tVert1 );
					}
					if( !bFace[1] )
					{
						face.y = curCell.vertexList.size();
						curCell.vertexList.push_back(
							newMesh.vertexList[ newMesh.faceList[f].y ] );

						//tFace.y = curCell.textureVert.size();
						//curCell.textureVert.push_back( tVert2 );
					}
					if( !bFace[2] )
					{
						face.z = curCell.vertexList.size();
						curCell.vertexList.push_back(
							newMesh.vertexList[ newMesh.faceList[f].z ] );

						//tFace.z = curCell.textureVert.size();
						//curCell.textureVert.push_back( tVert3 );
					}

					curCell.faceList.push_back( face );
				}
			}

			fprintf(fileStream, "\t*mesh %i\n", count );
			count++;

			//now dump this mesh to file
			//export faces
			fprintf(fileStream, "\t\t*faces %i\n", curCell.faceList.size());
			for( int ef = 0; ef < curCell.faceList.size(); ef++ )
			{
				fprintf(fileStream, "\t\t\t%i %i %i\n", (int)curCell.faceList[ef].x,
								(int)curCell.faceList[ef].y, (int)curCell.faceList[ef].z );
			}

			//export vertices
			fprintf(fileStream, "\t\t*vertex %i\n", curCell.vertexList.size());
			for( int ev = 0; ev < curCell.vertexList.size(); ev++ )
			{
				fprintf(fileStream, "\t\t\t%f %f %f\n", curCell.vertexList[ev].p.x,
								curCell.vertexList[ev].p.y, curCell.vertexList[ev].p.z );
			}
			/* // DO WE NEED NORMALS???
			//export normals
			fprintf(fileStream, "\t\t*normal %i\n", curCell.vertexList.size());
			for( int en = 0; en < curCell.vertexList.size(); en++ )
			{
				fprintf(fileStream, "\t\t\t%f %f %f\n", curCell.vertexList[en].n.x,
								curCell.vertexList[en].n.y, curCell.vertexList[en].n.z );
			}*/

			fprintf(fileStream, "\t*end\n" );

		}
	}

	fprintf(fileStream, "*end\n" );

	return true;
}


/*===========================================================================*\
 |  Do the export to the file
\*===========================================================================*/

int	CVDExporter::DoExport(const TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts, DWORD options)
{
	//return 1;

	// Set local interface pointer
	ip = i;



	// load the configuration from disk
	// so that it can be used in our dialog box
	if(!LoadExporterConfig()) return 0;

	if(!suppressPrompts)
	{

		// Show preferences setup dialog
		int res = DialogBoxParam(
			hInstance,
			MAKEINTRESOURCE(IDD_CVDEXP),
			i->GetMAXHWnd(),
			PrefsDlgProc,
			(LPARAM)this);

		// User clicked 'Cancel'
		if(res!=0) return 0;

	}

	exportSelected = (options & SCENE_EXPORT_SELECTED) ? TRUE : FALSE;

	TCHAR* realName = (TCHAR*)malloc( sizeof(TCHAR) * (strlen(name) + 1) );
	memcpy( realName, name, sizeof(TCHAR) * (strlen(name) + 1) );

	animFileName = (TCHAR*)malloc( sizeof(TCHAR) * (strlen(name) + 1) );
	memcpy( animFileName, name, sizeof(TCHAR) * (strlen(name) + 1) );

	animFileName[ strlen( name ) - 3 ] = 'S';
	animFileName[ strlen( name ) - 2 ] = 'K';
	animFileName[ strlen( name ) - 1 ] = 'A';

	//change file name according the check box selected
	if( bExportAsMesh )
	{
		realName[ strlen( name ) - 3 ] = 'A';
		realName[ strlen( name ) - 2 ] = '3';
		realName[ strlen( name ) - 1 ] = 'D';

		ExportMesh(realName, ei,i, suppressPrompts, options);
	}
	if( bExportAsMap )
	{
		realName[ strlen( name ) - 3 ] = 'M';
		realName[ strlen( name ) - 2 ] = '3';
		realName[ strlen( name ) - 1 ] = 'D';

		ExportMap(realName, ei,i, suppressPrompts, options);
	}




/*
	// Open a filestream for writing out to
	fileStream = _tfopen(realName,_T("wt"));
	if (!fileStream) {
		return 0;
	}

	int ExportMap(ExpInterface *ei,Interface *i, BOOL suppressPrompts=FALSE, DWORD options=0);

	//export data as a mesh
	int ExportMesh(ExpInterface *ei,Interface *i, BOOL suppressPrompts=FALSE, DWORD options=0);

	fprintf(fileStream, "%s\n", realName);

	if( bExportAsMesh )
	{
		fprintf(fileStream, "export as mesh");
	}
	if( bExportAsMap )
		fprintf(fileStream, "export as map");



	// Print out a title for the file header
	fprintf(fileStream, "Results of the Custom Data Per Vertex Search\n");

	// Print out the state of the search parameter
	fprintf(fileStream, "Search Type Chosen: %s\n\n", searchtype?"Channel":"Modifier");


	// Simple root node -> children enumeration
	// This will get the root node, and then cycle through its children (ie, the basic scene nodes)
	// It will then recurse to search their children, and then their children, etc
	int numChildren = i->GetRootNode()->NumberOfChildren();

	for (int idx=0; idx<numChildren; idx++) {
		if (i->GetCancel())
			break;
		nodeEnum(i->GetRootNode()->GetChildNode(idx), i);
	}

	fclose(fileStream);
*/

	// Save the current configuration back out to disk
	// for use next time the exporter is run
	SaveExporterConfig();

	delete realName;
	realName = 0;
	delete animFileName;
	animFileName = 0;

	return 1;
}

//export data as a map
int CVDExporter::ExportMap(TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts, DWORD options)
{
	// Open a filestream for writing out to
	fileStream = _tfopen(name,_T("wt"));
	if (!fileStream) {
		return 0;
	}

	ExportMaterials(i);

	int numChildren = i->GetRootNode()->NumberOfChildren();

	/*
	for(int idx=0; idx < numChildren; idx++)
	{
		if (i->GetCancel())
			break;
		ExportMaterials(i->GetRootNode()->GetChildNode(idx), i);
	}*/

	for(int idx=0; idx < numChildren; idx++)
	{
		if (i->GetCancel())
			break;
		MapNodeEnum(i->GetRootNode()->GetChildNode(idx), i);
	}

	fclose(fileStream);
	return 1;
}

//export header animation data
bool CVDExporter::ExportHeaderAnimation(Interface *ip)
{
	fprintf(animFile, "*animation\n");
	fprintf(animFile, "\t*framerate %i\n", GetFrameRate() );
	fprintf(animFile, "\t*frametime %i\n", GetTicksPerFrame() );
	fprintf(animFile, "*end\n");

	return true;
}

//export data as a mesh
int CVDExporter::ExportMesh(TCHAR *name,ExpInterface *ei,Interface *i, BOOL suppressPrompts, DWORD options)
{
	// Open a filestream for writing out to
	fileStream = _tfopen(name,_T("wt"));
	if (!fileStream) {
		return 0;
	}

	//open another file to export animation data only!!!
	animFile = _tfopen(animFileName,_T("wt"));
	if (!fileStream) {
		return 0;
	}

	ExportMaterials(i);
	ExportHeaderAnimation(i);

	int numChildren = i->GetRootNode()->NumberOfChildren();

	/*
	for(int idx=0; idx < numChildren; idx++)
	{
		if (i->GetCancel())
			break;
		ExportMaterials(i->GetRootNode()->GetChildNode(idx), i);
	}*/

	for(int idx=0; idx < numChildren; idx++)
	{
		if (i->GetCancel())
			break;
		nodeEnum(i->GetRootNode()->GetChildNode(idx), i);
	}

	//export bone data
	std::map<int, INode*>::iterator boneIt;

	int b = 0;
	for(  boneIt = bones.begin(); boneIt != bones.end(); boneIt++, b++ )
	{
		ExportBone( (boneIt->second) , i, b);
	}

	fclose(fileStream);
	fclose(animFile);
	return 1;
}

bool CVDExporter::ExportMaterials(Interface *ip)
{
	int i,j; // for loops

	//MtlBaseLib* scenemats = ip->GetSceneMtls();

	//make a vector of our materials
	std::set<Mtl*> nodeMats;

	int numChildren = ip->GetRootNode()->NumberOfChildren();

	for(int idx=0; idx < numChildren; idx++)
	{
		if (ip->GetCancel())
			break;

		INode* node = ip->GetRootNode()->GetChildNode(idx);

		if( node->Selected() )
		{					
			Mtl *nodemtl = node->GetMtl();
			if (nodemtl)
			{
				nodeMats.insert( 0, nodemtl );
			}
		}
	}

	//loop through materials and export them
	std::set<Mtl*>::iterator matIt;

	for( matIt = nodeMats.begin(); matIt != nodeMats.end(); matIt++ )
	{
		int len = strlen( (*matIt)->GetName() );
		TCHAR* path = (TCHAR*)malloc( sizeof(TCHAR) * (len + 1) );
		memcpy(path, (*matIt)->GetName(), sizeof(TCHAR) * (len + 1) );

		materialID.push_back( path );
		fprintf(fileStream, "*material %i\n", (materialID.size() - 1) );

		if( (*matIt)->NumSubMtls() > 0 )
		{
			for(j=0; j < (*matIt)->NumSubMtls(); j++ )
			{
				Mtl* subMtl = (*matIt)->GetSubMtl(j);
				if( subMtl )
				{
					fprintf(fileStream,"\t*submaterial %d\n", j);
					//DumpMaterial(subMtl, 0, i, indentLevel+1);
					for (i=0; i<subMtl->NumSubTexmaps(); i++)
					{
						Texmap* subTex = subMtl->GetSubTexmap(i);
						float amt = 1.0f;
						if (subTex)
						{
							// If it is a standard material we can see if the map is enabled.
							if (subMtl->ClassID() == Class_ID(DMTL_CLASS_ID, 0))
							{
								if (!((StdMat*)subMtl)->MapEnabled(i))
									continue;
								amt = ((StdMat*)subMtl)->GetTexmapAmt(i, 0);

							}

							if (subTex->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00))
							{
								TSTR mapName = ((BitmapTex *)subTex)->GetMapName();
								//find our first "/" from the back
								int b = strlen( mapName );
								while( b >= 0 && mapName[b] != '\\' )
								{
									b--;
								}
								b++; //add one more
								int len = strlen( mapName ) - b;
								TCHAR* path = (TCHAR*)malloc( sizeof(TCHAR) * (len + 1) );
								memcpy(path, (mapName + b), sizeof(TCHAR) * (len + 1) );

								//see if we have an opacity map or not
								if( i != ID_OP )
								{
									fprintf(fileStream,"\t\t*texture %s\n", path);
								}
								else
								{
									fprintf(fileStream,"\t\t*blendmap %s\n", path);
								}

								StdUVGen* uvGen = ((BitmapTex *)subTex)->GetUVGen();
								if (uvGen)
								{
									fprintf(fileStream, "\t\t\t*vtile %f\n", uvGen->GetVScl(0));
									fprintf(fileStream, "\t\t\t*utile %f\n", uvGen->GetUScl(0));
								}
								fprintf(fileStream, "\t\t*end\n");
							}
						}
					}
					fprintf(fileStream, "\t*end\n");
				}
			}
		}

		if( (*matIt)->ClassID() == Class_ID(DMTL_CLASS_ID, 0) )
		{
			StdMat* std = (StdMat*)(*matIt);
			if( std->GetTwoSided() )
			{
				fprintf(fileStream, "\t*doublesided\n");
			}
		}

		Color col = (*matIt)->GetAmbient(j);
		fprintf(fileStream, "\t*ambient %f %f %f\n", col.r, col.g, col.b);

		col = (*matIt)->GetDiffuse(j);
		fprintf(fileStream, "\t*diffuse %f %f %f\n", col.r, col.g, col.b);

		col = (*matIt)->GetSpecular(j);
		fprintf(fileStream, "\t*specular %f %f %f\n", col.r, col.g, col.b);

		float shine = (*matIt)->GetShininess(j); //GetSpecular(j);
		fprintf(fileStream, "\t*shine %f\n", shine);

		// submtls or texmaps
		for (j = 0; j < (*matIt)->NumSubTexmaps(); j++)
		{
			Texmap * tmap = (*matIt)->GetSubTexmap(j);

			if (tmap)
			{
				if( tmap->ClassID() == Class_ID( COMPOSITE_CLASS_ID, 0 ) )
				{
						fprintf(fileStream, "this is what we ar elooking for :) 3\n");
				}


				if( tmap->ClassID() == Class_ID( MULTI_CLASS_ID, 0 ) )
				{
					fprintf(fileStream, "this is what we ar elooking for :) 4\n");
				}
				// NOTE: For this tutorial, we only go one subtex "level"
				// deep, although in reality, we can go much deeper
				// (e.g. mtl with checker diffuse, that in turn has another
				// checker as a subtex, etc)
				//fprintf(fileStream, "    subtex -- Name:<%s> UVWSrc:<%d> MapChannel:<%d> \n", tmap->GetName(), tmap->GetUVWSource(), tmap->GetMapChannel());
				// for this sample, if one of the first level subtex's is a
				// bitmaptex, dump out some more info.  It should be clear
				// that similar actions can be taken with other typical subtexs
				// (RGBMult, etc)

				if (tmap->ClassID() == Class_ID(BMTEX_CLASS_ID, 0))
				{
					BitmapTex *bmt = (BitmapTex*) tmap;
					//find our first "/" from the back
					int b = strlen( bmt->GetMapName() );
					while( b >= 0 && bmt->GetMapName()[b] != '\\' )
					{
						b--;
					}
					b++; //add one more

					int len = strlen( bmt->GetMapName() ) - b;
					TCHAR* path = (TCHAR*)malloc( sizeof(TCHAR) * (len + 1) );

					memcpy(path, (bmt->GetMapName() + b), sizeof(TCHAR) * (len + 1) );

					fprintf(fileStream, "\t*texture %s\n", path);

					// (Sidenote: There are supposedly some cases where this
					// might not return the full filename + path)
				}
			}
		}

		fprintf(fileStream, "\t*end\n");
	}
/*
	if(scenemats)
	{
        for (i = 0; i < scenemats->Count(); i++)
		{
            // DoSomething(scenemats[i]);
            MtlBase * mtl = (*scenemats)[i];

			//std::list<TSTR> materialID;
			int len = strlen( mtl->GetName() );
			TCHAR* path = (TCHAR*)malloc( sizeof(TCHAR) * (len + 1) );
			memcpy(path, mtl->GetName(), sizeof(TCHAR) * (len + 1) );

			fprintf(fileStream, "*matname: %s ard: %i\n", path, mtl );
			/*
			//see if any selected models use this material
			bool bMatUsed = false;

			if( exportSelected )
			{
				int numChildren = ip->GetRootNode()->NumberOfChildren();

				for(int idx=0; idx < numChildren; idx++)
				{
					if (ip->GetCancel())
						break;

					INode* node = ip->GetRootNode()->GetChildNode(idx);

					if( node->GetMtl() == mtl )
					{
						bMatUsed = true;
						break;
					}
				}
			}
			else
			{
				bMatUsed = true;
			}*/

			

				//Color ambient = mtl->GetAmbient();
				//fprintf(fileStream, "\t\t*ambient %i %i %i\n", ambient.R, ambient.G, ambient.B);

/*
				//fprintf(fileStream, "BEGIN MTL Name:<%s> FullName:<%s>\n my name<%s>", mtl->GetName(), mtl->GetFullName(), path);
			if( IsMtl(mtl) )
			{		
				Mtl *m = (Mtl*)mtl;

				//see if any selected models use this material
				bool bMatUsed = false;

				if( exportSelected )
				{
					int numChildren = ip->GetRootNode()->NumberOfChildren();

					for(int idx=0; idx < numChildren; idx++)
					{
						if (ip->GetCancel())
								break;

						INode* node = ip->GetRootNode()->GetChildNode(idx);

						if( node->Selected() )
						{					
							Mtl * nodemtl = node->GetMtl();
							if (nodemtl)
							{
								if( strcmp( path, nodemtl->GetName()) == 0 )
								{
									bMatUsed = true;
									break;
								}
							}
						
							

							if( node->GetMtl() == m )
							{
								bMatUsed = true;
								break;
							}
						}
					}
				}
				else
				{
					bMatUsed = true;
				}

				if( !bMatUsed )
				{
					materialID.push_back( path );

					//materialID = list();

					//std::vector<TCHAR*>::iterator test;
					TCHAR* test = materialID.back();

					//if( strcmp(test2, path) == 0 )
					//	fprintf(fileStream, "last is equal to path (thats good)");

					fprintf(fileStream, "*material %i\n", (materialID.size() - 1) );

					if (m->NumSubMtls() > 0)
					{
						for (j=0; j<m->NumSubMtls(); j++)
						{
							Mtl* subMtl = m->GetSubMtl(j);
							if (subMtl)
							{
								fprintf(fileStream,"\t*submaterial %d\n", j);
								//DumpMaterial(subMtl, 0, i, indentLevel+1);
								for (i=0; i<subMtl->NumSubTexmaps(); i++)
								{

									Texmap* subTex = subMtl->GetSubTexmap(i);
									float amt = 1.0f;
									if (subTex)
									{
										// If it is a standard material we can see if the map is enabled.
										if (subMtl->ClassID() == Class_ID(DMTL_CLASS_ID, 0))
										{
											if (!((StdMat*)subMtl)->MapEnabled(i))
												continue;
											amt = ((StdMat*)subMtl)->GetTexmapAmt(i, 0);

										}

										if (subTex->ClassID() == Class_ID(BMTEX_CLASS_ID, 0x00))
										{
											TSTR mapName = ((BitmapTex *)subTex)->GetMapName();

											//find our first "/" from the back
											int b = strlen( mapName );
											while( b >= 0 && mapName[b] != '\\' )
											{
												b--;
											}
											b++; //add one more
											int len = strlen( mapName ) - b;
											TCHAR* path = (TCHAR*)malloc( sizeof(TCHAR) * (len + 1) );
											memcpy(path, (mapName + b), sizeof(TCHAR) * (len + 1) );

											//see if we have an opacity map or not
											if( i != ID_OP )
											{
												fprintf(fileStream,"\t\t*texture %s\n", path);
											}
											else
											{
												fprintf(fileStream,"\t\t*blendmap %s\n", path);
											}

											StdUVGen* uvGen = ((BitmapTex *)subTex)->GetUVGen();
											if (uvGen)
											{
												fprintf(fileStream, "\t\t\t*vtile %f\n", uvGen->GetVScl(0));
												fprintf(fileStream, "\t\t\t*utile %f\n", uvGen->GetUScl(0));
											}
											fprintf(fileStream, "\t\t*end\n");
										}
									}
								}
								fprintf(fileStream, "\t*end\n");
							}
						}
					}

					if (mtl->ClassID() == Class_ID(DMTL_CLASS_ID, 0))
					{
						StdMat* std = (StdMat*)mtl;
						if( std->GetTwoSided() )
						{
							fprintf(fileStream, "\t*doublesided\n");
						}
					}

					Color col = m->GetAmbient(j);
					fprintf(fileStream, "\t*ambient %f %f %f\n", col.r, col.g, col.b);

					col = m->GetDiffuse(j);
					fprintf(fileStream, "\t*diffuse %f %f %f\n", col.r, col.g, col.b);

					col = m->GetSpecular(j);
					fprintf(fileStream, "\t*specular %f %f %f\n", col.r, col.g, col.b);


					float shine = m->GetShininess(j); //GetSpecular(j);
					fprintf(fileStream, "\t*shine %f\n", shine);

					// submtls or texmaps
					for (j = 0; j < mtl->NumSubTexmaps(); j++)
					{
						Texmap * tmap = mtl->GetSubTexmap(j);

						if (tmap)
						{
							if( tmap->ClassID() == Class_ID( COMPOSITE_CLASS_ID, 0 ) )
							{
								fprintf(fileStream, "this is what we ar elooking for :) 3\n");
							}


							if( tmap->ClassID() == Class_ID( MULTI_CLASS_ID, 0 ) )
							{
								fprintf(fileStream, "this is what we ar elooking for :) 4\n");
							}
							// NOTE: For this tutorial, we only go one subtex "level"
							// deep, although in reality, we can go much deeper
							// (e.g. mtl with checker diffuse, that in turn has another
							// checker as a subtex, etc)
							//fprintf(fileStream, "    subtex -- Name:<%s> UVWSrc:<%d> MapChannel:<%d> \n", tmap->GetName(), tmap->GetUVWSource(), tmap->GetMapChannel());
							// for this sample, if one of the first level subtex's is a
							// bitmaptex, dump out some more info.  It should be clear
							// that similar actions can be taken with other typical subtexs
							// (RGBMult, etc)

							if (tmap->ClassID() == Class_ID(BMTEX_CLASS_ID, 0))
							{
								BitmapTex *bmt = (BitmapTex*) tmap;

								//find our first "/" from the back
								int b = strlen( bmt->GetMapName() );
								while( b >= 0 && bmt->GetMapName()[b] != '\\' )
								{
									b--;
								}
								b++; //add one more

								int len = strlen( bmt->GetMapName() ) - b;

								TCHAR* path = (TCHAR*)malloc( sizeof(TCHAR) * (len + 1) );

								memcpy(path, (bmt->GetMapName() + b), sizeof(TCHAR) * (len + 1) );

								fprintf(fileStream, "\t*texture %s\n", path);

								// (Sidenote: There are supposedly some cases where this
								// might not return the full filename + path)
							}
						}
					}

					fprintf(fileStream, "*end\n");
				}
				
			}	* /		
            //fprintf(fileStream, "END MTL\n");
        }
    }*/

	return TRUE;
}
