# Microsoft Developer Studio Generated NMAKE File, Based on CVDExport.dsp
!IF "$(CFG)" == ""
CFG=CVDExport - Win32 Hybrid
!MESSAGE No configuration specified. Defaulting to CVDExport - Win32 Hybrid.
!ENDIF 

!IF "$(CFG)" != "CVDExport - Win32 Release" && "$(CFG)" != "CVDExport - Win32 Debug" && "$(CFG)" != "CVDExport - Win32 Hybrid"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CVDExport.mak" CFG="CVDExport - Win32 Hybrid"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CVDExport - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "CVDExport - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "CVDExport - Win32 Hybrid" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CVDExport - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

ALL : "..\..\..\..\..\plugin\CVDExport.dle"


CLEAN :
	-@erase "$(INTDIR)\ConfigMgr.obj"
	-@erase "$(INTDIR)\CVDExport.obj"
	-@erase "$(INTDIR)\CVDExport.res"
	-@erase "$(INTDIR)\DoExport.obj"
	-@erase "$(INTDIR)\Plugin.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\CVDExport.exp"
	-@erase "$(OUTDIR)\CVDExport.lib"
	-@erase "..\..\..\..\..\plugin\CVDExport.dle"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /G6 /MD /W3 /GX /O2 /I "..\..\..\..\..\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CVDExport_EXPORTS" /Fp"$(INTDIR)\CVDExport.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\CVDExport.res" /i "..\..\..\..\..\include" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\CVDExport.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=core.lib geom.lib gfx.lib bmm.lib maxutil.lib mesh.lib paramblk2.lib comctl32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /base:"0x027d0000" /dll /incremental:no /pdb:"$(OUTDIR)\CVDExport.pdb" /machine:I386 /def:".\Plugin.def" /out:"..\..\..\..\..\plugin\CVDExport.dle" /implib:"$(OUTDIR)\CVDExport.lib" /libpath:"..\..\..\..\..\lib" /release 
DEF_FILE= \
	".\Plugin.def"
LINK32_OBJS= \
	"$(INTDIR)\ConfigMgr.obj" \
	"$(INTDIR)\CVDExport.obj" \
	"$(INTDIR)\DoExport.obj" \
	"$(INTDIR)\Plugin.obj" \
	"$(INTDIR)\CVDExport.res"

"..\..\..\..\..\plugin\CVDExport.dle" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "CVDExport - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "..\..\..\..\..\plugin\CVDExport.dle"


CLEAN :
	-@erase "$(INTDIR)\ConfigMgr.obj"
	-@erase "$(INTDIR)\CVDExport.obj"
	-@erase "$(INTDIR)\CVDExport.res"
	-@erase "$(INTDIR)\DoExport.obj"
	-@erase "$(INTDIR)\Plugin.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\CVDExport.exp"
	-@erase "$(OUTDIR)\CVDExport.lib"
	-@erase "$(OUTDIR)\CVDExport.pdb"
	-@erase "..\..\..\..\..\plugin\CVDExport.dle"
	-@erase "..\..\..\..\..\plugin\CVDExport.ilk"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /G6 /MDd /W3 /Gm /GX /ZI /Od /I "..\..\..\..\..\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CVDExport_EXPORTS" /Fp"$(INTDIR)\CVDExport.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\CVDExport.res" /i "..\..\..\..\..\include" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\CVDExport.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=core.lib geom.lib gfx.lib bmm.lib maxutil.lib mesh.lib paramblk2.lib comctl32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /base:"0x027d0000" /dll /incremental:yes /pdb:"$(OUTDIR)\CVDExport.pdb" /debug /machine:I386 /def:".\Plugin.def" /out:"..\..\..\..\..\plugin\CVDExport.dle" /implib:"$(OUTDIR)\CVDExport.lib" /pdbtype:sept /libpath:"..\..\..\..\..\lib" 
DEF_FILE= \
	".\Plugin.def"
LINK32_OBJS= \
	"$(INTDIR)\ConfigMgr.obj" \
	"$(INTDIR)\CVDExport.obj" \
	"$(INTDIR)\DoExport.obj" \
	"$(INTDIR)\Plugin.obj" \
	"$(INTDIR)\CVDExport.res"

"..\..\..\..\..\plugin\CVDExport.dle" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "CVDExport - Win32 Hybrid"

OUTDIR=.\Hybrid
INTDIR=.\Hybrid

ALL : "..\..\..\..\..\plugin\CVDExport.dle"


CLEAN :
	-@erase "$(INTDIR)\ConfigMgr.obj"
	-@erase "$(INTDIR)\CVDExport.obj"
	-@erase "$(INTDIR)\CVDExport.res"
	-@erase "$(INTDIR)\DoExport.obj"
	-@erase "$(INTDIR)\Plugin.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\CVDExport.exp"
	-@erase "$(OUTDIR)\CVDExport.lib"
	-@erase "$(OUTDIR)\CVDExport.pdb"
	-@erase "..\..\..\..\..\plugin\CVDExport.dle"
	-@erase "..\..\..\..\..\plugin\CVDExport.ilk"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /G6 /MD /W3 /Gm /GX /ZI /Od /I "..\..\..\..\..\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CVDExport_EXPORTS" /Fp"$(INTDIR)\CVDExport.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x809 /fo"$(INTDIR)\CVDExport.res" /i "..\..\..\..\..\include" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\CVDExport.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=core.lib geom.lib gfx.lib bmm.lib maxutil.lib mesh.lib paramblk2.lib comctl32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /base:"0x027d0000" /dll /incremental:yes /pdb:"$(OUTDIR)\CVDExport.pdb" /debug /machine:I386 /def:".\Plugin.def" /out:"..\..\..\..\..\plugin\CVDExport.dle" /implib:"$(OUTDIR)\CVDExport.lib" /pdbtype:sept /libpath:"..\..\..\..\..\lib" 
DEF_FILE= \
	".\Plugin.def"
LINK32_OBJS= \
	"$(INTDIR)\ConfigMgr.obj" \
	"$(INTDIR)\CVDExport.obj" \
	"$(INTDIR)\DoExport.obj" \
	"$(INTDIR)\Plugin.obj" \
	"$(INTDIR)\CVDExport.res"

"..\..\..\..\..\plugin\CVDExport.dle" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("CVDExport.dep")
!INCLUDE "CVDExport.dep"
!ELSE 
!MESSAGE Warning: cannot find "CVDExport.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "CVDExport - Win32 Release" || "$(CFG)" == "CVDExport - Win32 Debug" || "$(CFG)" == "CVDExport - Win32 Hybrid"
SOURCE=.\ConfigMgr.cpp

"$(INTDIR)\ConfigMgr.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\CVDExport.cpp

"$(INTDIR)\CVDExport.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\DoExport.cpp

"$(INTDIR)\DoExport.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\Plugin.cpp

"$(INTDIR)\Plugin.obj" : $(SOURCE) "$(INTDIR)"


SOURCE=.\CVDExport.rc

"$(INTDIR)\CVDExport.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)



!ENDIF 

