import std.stdio;
import std.system;
import std.process;
import std.string;
import std.file;
import std.path;
import serial.device; // serial-port lib
import core.time;
import std.array;
import std.json;
import core.thread;

auto g_jsonFileName = "record.json";
SerialPort g_serialPort;


// clear terminal window
void clearScreen()
{
	version (Windows) 
	{
		executeProcess("cls");
	}
    version (linux) 
    {
		executeProcess("reset");
	}
}

// run command
void executeProcess(string cmd)
{
	auto cmdList = cmd.split(" ");
	//writeln("Proc: %s\n".format(cmd));
	auto pid = spawnProcess(cmdList);
	scope(exit) wait(pid);
}

void init()
{
}

// get list of serial ports
auto getSerialPorts()
{
	auto ports = SerialPort.ports;
	
	//writeln(ser.ports());
	/*
	// use this to search for device using console commands
	version(linux)
	{
		auto exec = executeShell("ls /dev/ttyUSB*");
		if (exec.status != 0)
			writeln("Error, no serial devices found!");
			
		writeln(exec);
	}
	*/
	
	return ports;
}

// open serial port
auto openSerialPort()
{
	auto timeout = dur!("msecs")(2000000);
	version(linux)
	{
		g_serialPort = new SerialPort("/dev/ttyUSB0", timeout, timeout);
	}
	version(Windows)
	{
		g_serialPort = new SerialPort("COM4", timeout, timeout);
	}
	
	writeln("Connected device: %s @ %s".format(g_serialPort.toString, g_serialPort.speed));
	//g_serialPort.getBaudRates // get list of available rates
	
	// sleep
	Thread.sleep(dur!("seconds")(2));
}

// close
auto closeSerialPort()
{
	g_serialPort.close();
}

// will continue reading serial data till newline char
auto readSerialLine()
{
	auto read = true;
	auto appender = appender!(char[])();
	ubyte[1] buff;
	ubyte[1] lastbuff;
	ubyte[2] pattern = [13,10];
	char c;
	
	//writeln("New Line Pattern: %s".format(pattern));
	
	while (read)
	{		
		// only read 1 byte at a time
		g_serialPort.read(buff);
		
		// search for new line (consists of ubyte[13,10])
		if (buff[0] == pattern[1] && lastbuff[0] == pattern[0])
		{
			// remove last char && return
			return appender.data[0 .. appender.data.length-1];
		}
			
		lastbuff[0] = buff[0];
		appender.put(buff[0]);
	}
	
	return null;
}


// send command via serial to IR
void sendCommand(string cmd)
{
	writeln("Sending signal: %s".format(cmd));

	// time to allow port to configure, arduino req this
	//Thread.sleep(dur!("seconds")(2));
	
	// read json
	auto cwd = dirName(thisExePath());
	auto f = File(buildPath(cwd, g_jsonFileName), "r");
    auto jsonData = parseJSON(f.readln);
	ubyte[] pattern = ['\n'];
	auto ba = cast(ubyte[]) jsonData[cmd].str ~ pattern; // add newline pattern
	
	writeln("Signal: %s".format(jsonData[cmd].str));
	//writeln("Bytes: %s".format(ba));
		
	// GO!
	g_serialPort.write(ba);
}

auto recordSignal()
{
	writeln("Running IRTrans in record mode.");
	
	writeln("Waiting for input signal...");
	
	// this will hold
	auto line = readSerialLine();
		
	writeln("Signal: %s".format(line));
	
	writeln("Enter name to save:");
	auto x = strip(stdin.readln());

	// read json
	auto cwd = dirName(thisExePath());
	auto f = File(buildPath(cwd, g_jsonFileName), "r");
    auto jsonData = parseJSON(f.readln());
    
    // add json
	jsonData[x] = cast(string) line;
	
	// write json
	f = File(buildPath(cwd, g_jsonFileName), "w"); // open for write
	f.writeln(jsonData.toString());
	f.close();
}

// toggle when monitor on/off
auto screenToggle()
{
	writeln("Running IRTrans in screen saver mode.");
	auto command = "power";
	
	// monitor is 0 when off
	auto curState = executeShell("xset -q | grep -c \"Monitor is On\"").output;
	auto prevState = curState;
	
	// DPMS is 0 when using youtube
	auto dpms_cur_state = executeShell("xset -q | grep -c \"DPMS is Enabled\"").output;
	auto dpms_prev_state = dpms_cur_state;
	
	writeln("Monitor: %s | DPMS: %s".format(curState, dpms_cur_state));

	
	while (true)
	{
        // check if screen off is inhibited
        // Also note DPMS is disabled so we could possibly check that too!
		//auto hasInhibit = executeShell("qdbus org.freedesktop.PowerManagement /org/freedesktop/PowerManagement/Inhibit  org.freedesktop.PowerManagement.Inhibit.HasInhibit").output;
		
		//if (hasInhibit == "false")
         //   continue;
            
        //writeln("Inhibit %s".format(hasInhibit));
        
		curState = executeShell("xset -q | grep -c \"Monitor is On\"").output;

		if (curState != prevState)
		{
            //prevState = curState;
            writeln("Monitor: %s | DPMS: %s".format(curState, dpms_cur_state));
            
            dpms_cur_state = executeShell("xset -q | grep -c \"DPMS is Enabled\"").output;
            
            if (dpms_cur_state == dpms_prev_state)
            {
                prevState = curState;
                writeln("TRIGGER | Monitor: %s | DPMS: %s".format(curState, dpms_cur_state));
                sendCommand(command);
            }
		}
		
		// sleep
		Thread.sleep(dur!("seconds")(2));
	}
}

// main fn
void main(string[] args)
{
	init();   
	clearScreen();
	openSerialPort();
	
	// commandline supplied, run it
	if (args.length == 2)
	{
		switch(args[1])
		{
			case "-r":
				recordSignal();
				break;
				
			case "-s":
				screenToggle();
				break;
			
			default:
				sendCommand(args[1]);
				break;
		}
		
		return;
	}	
	else // no commandline
	{
		screenToggle();
	}
	
	closeSerialPort();
}
