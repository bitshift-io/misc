#!/bin/sh

# get init sate, 1 = on, 0 == off
PREVSTATE=$(xset -q | grep -c "Monitor is On")
CURSTATE=$(xset -q | grep -c "Monitor is On")

# infinite loop
while true; do
# get init sate, 1 = on, 0 == off
CURSTATE=$(xset -q | grep -c "Monitor is On")

if [ "$PREVSTATE" != "$CURSTATE" ]
then
	PREVSTATE=$(xset -q | grep -c "Monitor is On")
	echo "Change monitor state."
	./Projects/Electronics/IRTrans/irtrans power
fi

# x sec
sleep 1
done
