#!/usr/bin/env python3

#
# 2016 - Fabian Mathews
#
# Send an IR command when monitor power state changes
# sits in a loop 
# works on linux only

import os
import serial
import json
import sys
import time
import subprocess
import IRTrans

command = 'power'

def runCommand(command):
    p = subprocess.Popen(command,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
    return iter(p.stdout.readline, b'')
	
def runOSCommand(command):
	output = ""
	for line in runCommand(command):
		output += line.decode() 

	return output

def monitorState():
	monitorState = False
	if runOSCommand(["xset","-q"]).find("Monitor is On") != -1:
		monitorState = True		
		
	return monitorState
			
if __name__ == '__main__':
	print ('Running')
	prevMonitorState = monitorState()
	while True:
		time.sleep(2)
		if prevMonitorState != monitorState():
			prevMonitorState = monitorState()
			print("Monitor is on: " + str(prevMonitorState))
			IRTrans.sendCommand(command)
			
