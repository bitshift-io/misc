#!/usr/bin/env python3

#
# 2016 - Fabian Mathews
#
# Read saves and replays IR commands
#
# apt-get install python3
# apt-get install python3-pip
# pip3 install setuptools
# pip3 install pyserial

import os
import serial
import json
import sys
import time

jsonFileName = 'record.json'

def openSerialPort():
	try:
		if os.name == 'nt':
			ser = serial.Serial('COM4', 9600)
		else:
			ser = serial.Serial('/dev/ttyUSB0', 9600)
	except Exception as e:
		print(e.args)
		sys.exit()
	except:
		print(sys.exc_info()[0])
		sys.exit()
		
	return ser

def sendCommand(cmd):
	print ('\nSending signal:' + cmd)
	
	ser = openSerialPort()	
	
	# time to allow port to configure
	time.sleep(2)
	
	with open(jsonFileName, 'r') as dataFile:  
		jsonData = json.load(dataFile)
	
	ba = bytes(jsonData[cmd] + "\n", encoding="ascii")
	#ba = bytes("4550,4450, 600\n", encoding="ascii")
	print(ba)
	#ser.write(ba)
	
	#for b in bytearray("10,20","UTF-8"):
	#	ser.write(str(b).encode("latin_1"))
		
	
	ser.write(ba)
	ser.flush()
	
	# wait for output to be sent
	#while (ser.out_waiting):
	#	ser.flush()
	
	#time.sleep(2)
		
	# check the arduino got it
	#line = ser.readline()
	#line = line.strip()		
	
	print("")
	
	#while True:
	line = ser.readline()
	line = line.strip()	
	print(line)
	return

if __name__ == '__main__':  

	if len(sys.argv) == 2:
		sendCommand(sys.argv[1])
		sys.exit()

	print("Running IRTrans in record mode, waiting for signal...")
	ser = openSerialPort()
	while True:
		line = ser.readline()
		line = line.strip()
		
		if len(line) <= 0:
			continue
			
		print(line)
		
		print ('\nSignal recorded, enter name to save:')
		x = input(' -> ')
		print('\n')
			
		# read existing data
		jsonData = {}
		try:
			with open(jsonFileName, 'r') as dataFile:
				jsonData = json.load(dataFile)
		except:
			pass
			
		# append new data
		jsonData[x] = line.decode()
		
		# write data
		with open(jsonFileName, 'w') as dataFile:
			json.dump(jsonData, dataFile)
			
		sys.exit()
